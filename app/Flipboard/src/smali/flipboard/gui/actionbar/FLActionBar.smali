.class public Lflipboard/gui/actionbar/FLActionBar;
.super Landroid/view/ViewGroup;
.source "FLActionBar.java"

# interfaces
.implements Lflipboard/activities/FlipboardActivity$OnBackPressedListener;


# instance fields
.field protected a:Landroid/view/View;

.field protected b:Landroid/view/View;

.field final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field e:Landroid/view/View;

.field protected f:Lflipboard/gui/actionbar/FLActionBarMenu;

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/actionbar/FLActionBarMenuItem;",
            ">;"
        }
    .end annotation
.end field

.field h:Lflipboard/gui/actionbar/FLActionBarPopover;

.field public i:Z

.field private j:Lflipboard/gui/FLCameleonImageView;

.field private final k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/gui/actionbar/FLActionBarMenuItem;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/gui/actionbar/FLActionBarMenuItem;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lflipboard/activities/FlipboardActivity;

.field private n:Lflipboard/gui/dialog/FLDialogFragment;

.field private o:Z

.field private p:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

.field private q:Z

.field private r:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/actionbar/FLActionBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 101
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 106
    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->m:Lflipboard/activities/FlipboardActivity;

    .line 111
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->g:Ljava/util/List;

    .line 112
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v2, "always_show_overflow"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 115
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 116
    :goto_0
    if-nez v2, :cond_1

    if-eqz v0, :cond_2

    .line 117
    :cond_1
    invoke-direct {p0, p1}, Lflipboard/gui/actionbar/FLActionBar;->a(Landroid/content/Context;)V

    .line 119
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->k:Ljava/util/ArrayList;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->l:Ljava/util/ArrayList;

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->c:Ljava/util/List;

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    .line 123
    return-void

    :cond_3
    move v0, v1

    .line 115
    goto :goto_0
.end method

.method static synthetic a(Lflipboard/gui/actionbar/FLActionBar;)Lflipboard/activities/FlipboardActivity;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->m:Lflipboard/activities/FlipboardActivity;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 130
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030006

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLCameleonImageView;

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    .line 132
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setScaleX(F)V

    .line 135
    :cond_0
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    const v1, 0x7f020010

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    .line 136
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    new-instance v1, Lflipboard/gui/actionbar/FLActionBar$1;

    invoke-direct {v1, p0}, Lflipboard/gui/actionbar/FLActionBar$1;-><init>(Lflipboard/gui/actionbar/FLActionBar;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {p0, v0}, Lflipboard/gui/actionbar/FLActionBar;->addView(Landroid/view/View;)V

    .line 154
    return-void
.end method

.method static synthetic a(Lflipboard/gui/actionbar/FLActionBar;Z)Z
    .locals 0

    .prologue
    .line 57
    iput-boolean p1, p0, Lflipboard/gui/actionbar/FLActionBar;->i:Z

    return p1
.end method

.method static synthetic b(Lflipboard/gui/actionbar/FLActionBar;)Lflipboard/gui/dialog/FLDialogFragment;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->n:Lflipboard/gui/dialog/FLDialogFragment;

    return-object v0
.end method


# virtual methods
.method public final a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 250
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(ZLflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ZLflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;Z)Landroid/view/View;
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const v4, 0x7f020004

    const/4 v3, 0x0

    .line 255
    iput-object p2, p0, Lflipboard/gui/actionbar/FLActionBar;->p:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    .line 256
    if-eqz p1, :cond_9

    .line 257
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 258
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 259
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->c:Ljava/util/List;

    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 263
    :goto_0
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lflipboard/gui/actionbar/FLActionBar;->removeView(Landroid/view/View;)V

    .line 265
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030003

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLCameleonImageView;

    .line 266
    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    .line 268
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 269
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->c:Ljava/util/List;

    iget-object v2, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    invoke-interface {v1, v3, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 273
    :goto_1
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    invoke-virtual {p0, v1}, Lflipboard/gui/actionbar/FLActionBar;->addView(Landroid/view/View;)V

    .line 274
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->c:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    if-ne p2, v1, :cond_4

    .line 275
    const v1, 0x7f02007c

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    .line 276
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 297
    :goto_2
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 298
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    .line 301
    :cond_1
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    new-instance v1, Lflipboard/gui/actionbar/FLActionBar$2;

    invoke-direct {v1, p0}, Lflipboard/gui/actionbar/FLActionBar$2;-><init>(Lflipboard/gui/actionbar/FLActionBar;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 335
    :goto_3
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    return-object v0

    .line 261
    :cond_2
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 271
    :cond_3
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    iget-object v2, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    invoke-interface {v1, v3, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 277
    :cond_4
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    if-ne p2, v1, :cond_6

    .line 278
    if-eqz p3, :cond_5

    .line 279
    const v1, 0x7f02005d

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    goto :goto_2

    .line 281
    :cond_5
    invoke-virtual {v0, v4}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    .line 282
    invoke-virtual {v0}, Lflipboard/gui/FLCameleonImageView;->b()V

    goto :goto_2

    .line 284
    :cond_6
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->d:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    if-ne p2, v1, :cond_7

    .line 285
    const v1, 0x7f0200dd

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    .line 286
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09007b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 287
    invoke-virtual {v0, v1, v1, v1, v1}, Lflipboard/gui/FLCameleonImageView;->setPadding(IIII)V

    goto :goto_2

    .line 289
    :cond_7
    if-eqz p3, :cond_8

    .line 290
    const v1, 0x7f02005c

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    goto :goto_2

    .line 292
    :cond_8
    invoke-virtual {v0, v4}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    .line 293
    const v1, 0x7f08009b

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setDefaultColor(I)V

    goto :goto_2

    .line 325
    :cond_9
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    if-eqz v0, :cond_a

    .line 326
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 327
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->c:Ljava/util/List;

    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 331
    :goto_4
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lflipboard/gui/actionbar/FLActionBar;->removeView(Landroid/view/View;)V

    .line 333
    :cond_a
    iput-object v2, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    goto :goto_3

    .line 329
    :cond_b
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_4
.end method

.method public final a(ZZ)Landroid/view/View;
    .locals 1

    .prologue
    .line 245
    iget-boolean v0, p0, Lflipboard/gui/actionbar/FLActionBar;->r:Z

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    :goto_0
    invoke-virtual {p0, p1, v0, p2}, Lflipboard/gui/actionbar/FLActionBar;->a(ZLflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;Z)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    goto :goto_0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 163
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getOverflowItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->h:Lflipboard/gui/actionbar/FLActionBarPopover;

    if-nez v0, :cond_0

    .line 164
    new-instance v0, Lflipboard/gui/actionbar/FLActionBarPopover;

    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->m:Lflipboard/activities/FlipboardActivity;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getOverflowItems()Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v1, p0, v2, v3}, Lflipboard/gui/actionbar/FLActionBarPopover;-><init>(Landroid/app/Activity;Lflipboard/gui/actionbar/FLActionBar;Landroid/view/View;Ljava/util/List;)V

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->h:Lflipboard/gui/actionbar/FLActionBarPopover;

    .line 165
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->h:Lflipboard/gui/actionbar/FLActionBarPopover;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarPopover;->a()V

    .line 170
    :goto_0
    return-void

    .line 167
    :cond_0
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->m:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0, p0}, Lflipboard/activities/FlipboardActivity;->b(Lflipboard/activities/FlipboardActivity$OnBackPressedListener;)V

    .line 168
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->b()Z

    goto :goto_0
.end method

.method public final a(IIII)V
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lflipboard/gui/FLCameleonImageView;->setPadding(IIII)V

    .line 380
    :cond_0
    return-void
.end method

.method public final a(Lflipboard/activities/FlipboardActivity;Lflipboard/gui/dialog/FLDialogFragment;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lflipboard/gui/actionbar/FLActionBar;->m:Lflipboard/activities/FlipboardActivity;

    .line 175
    iput-object p2, p0, Lflipboard/gui/actionbar/FLActionBar;->n:Lflipboard/gui/dialog/FLDialogFragment;

    .line 176
    return-void
.end method

.method public final a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V
    .locals 3

    .prologue
    const v0, 0x7f020010

    .line 349
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    if-eqz v1, :cond_0

    .line 351
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$5;->a:[I

    invoke-virtual {p1}, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 368
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    const v2, 0x7f08009b

    invoke-virtual {v1, v2}, Lflipboard/gui/FLCameleonImageView;->setDefaultColor(I)V

    .line 371
    :goto_0
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    .line 373
    :cond_0
    return-void

    .line 353
    :pswitch_0
    const v0, 0x7f02019f

    .line 354
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09007b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 355
    iget-object v2, p0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v2, v1, v1, v1, v1}, Lflipboard/gui/FLCameleonImageView;->setPadding(IIII)V

    goto :goto_0

    .line 359
    :pswitch_1
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    const v2, 0x7f08005a

    invoke-virtual {v1, v2}, Lflipboard/gui/FLCameleonImageView;->setDefaultColor(I)V

    goto :goto_0

    .line 363
    :pswitch_2
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLCameleonImageView;->b()V

    goto :goto_0

    .line 351
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lflipboard/service/Section;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v5, -0x2

    const/4 v2, 0x0

    .line 835
    iget-boolean v0, p0, Lflipboard/gui/actionbar/FLActionBar;->q:Z

    if-eqz v0, :cond_1

    .line 880
    :cond_0
    :goto_0
    return-void

    .line 838
    :cond_1
    invoke-static {}, Lflipboard/util/AndroidUtil;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 842
    iput-boolean v1, p0, Lflipboard/gui/actionbar/FLActionBar;->q:Z

    .line 843
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getHomeButtonType()Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    move-result-object v0

    .line 845
    sget-object v3, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    if-ne v0, v3, :cond_3

    .line 846
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->m:Lflipboard/activities/FlipboardActivity;

    const v3, 0x7f030005

    invoke-static {v0, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    move-object v3, v0

    .line 850
    :goto_1
    if-eqz p3, :cond_4

    .line 851
    :goto_2
    if-eqz p3, :cond_5

    .line 852
    invoke-virtual {v3, p3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 856
    :goto_3
    invoke-virtual {v3}, Landroid/widget/Button;->getPaddingLeft()I

    move-result v0

    invoke-virtual {v3}, Landroid/widget/Button;->getPaddingRight()I

    move-result v4

    invoke-virtual {v3, v0, v2, v4, v2}, Landroid/widget/Button;->setPadding(IIII)V

    .line 857
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 858
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f09007b

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, v4, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 859
    const v0, 0x800007

    iput v0, v4, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 861
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v5, 0x8

    if-ne v0, v5, :cond_6

    :cond_2
    move v0, v2

    .line 862
    :goto_4
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 863
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->c:Ljava/util/List;

    invoke-interface {v1, v0, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 867
    :goto_5
    invoke-virtual {p0, v3, v0, v4}, Lflipboard/gui/actionbar/FLActionBar;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 868
    invoke-virtual {v3, v4}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 869
    new-instance v0, Lflipboard/gui/actionbar/FLActionBar$4;

    invoke-direct {v0, p0, p1, p2}, Lflipboard/gui/actionbar/FLActionBar$4;-><init>(Lflipboard/gui/actionbar/FLActionBar;Lflipboard/service/Section;Landroid/os/Bundle;)V

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 848
    :cond_3
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->m:Lflipboard/activities/FlipboardActivity;

    const v3, 0x7f030004

    invoke-static {v0, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    move-object v3, v0

    goto :goto_1

    .line 850
    :cond_4
    invoke-virtual {p1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object p3

    goto :goto_2

    .line 854
    :cond_5
    invoke-virtual {p1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_6
    move v0, v1

    .line 861
    goto :goto_4

    .line 865
    :cond_7
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    invoke-interface {v1, v0, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_5
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->h:Lflipboard/gui/actionbar/FLActionBarPopover;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->m:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0, p0}, Lflipboard/activities/FlipboardActivity;->b(Lflipboard/activities/FlipboardActivity$OnBackPressedListener;)V

    .line 196
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->h:Lflipboard/gui/actionbar/FLActionBarPopover;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarPopover;->dismiss()V

    .line 197
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->h:Lflipboard/gui/actionbar/FLActionBarPopover;

    .line 198
    const/4 v0, 0x1

    .line 200
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->h:Lflipboard/gui/actionbar/FLActionBarPopover;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->h:Lflipboard/gui/actionbar/FLActionBarPopover;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarPopover;->dismiss()V

    .line 182
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->h:Lflipboard/gui/actionbar/FLActionBarPopover;

    .line 184
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 387
    instance-of v0, p1, Landroid/widget/FrameLayout$LayoutParams;

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {p0, v0}, Lflipboard/gui/actionbar/FLActionBar;->removeView(Landroid/view/View;)V

    .line 207
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    .line 209
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 227
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getPaddingRight()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lflipboard/gui/actionbar/FLActionBar;->setPadding(IIII)V

    .line 229
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->a:Landroid/view/View;

    if-nez v0, :cond_0

    .line 230
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030009

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 231
    const v0, 0x7f0a0048

    invoke-virtual {p0, v0}, Lflipboard/gui/actionbar/FLActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->a:Landroid/view/View;

    .line 234
    :cond_0
    iget-boolean v0, p0, Lflipboard/gui/actionbar/FLActionBar;->r:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08007a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 235
    :goto_0
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 236
    return-void

    .line 234
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080079

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public final f()Landroid/view/View;
    .locals 3

    .prologue
    .line 240
    iget-boolean v0, p0, Lflipboard/gui/actionbar/FLActionBar;->r:Z

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    :goto_0
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Lflipboard/gui/actionbar/FLActionBar;->a(ZLflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;Z)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 344
    iget-boolean v0, p0, Lflipboard/gui/actionbar/FLActionBar;->r:Z

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    :goto_0
    invoke-virtual {p0, v0}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    .line 345
    return-void

    .line 344
    :cond_0
    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    goto :goto_0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 392
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 402
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 397
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getHomeButtonType()Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->p:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    return-object v0
.end method

.method public getMenu()Lflipboard/gui/actionbar/FLActionBarMenu;
    .locals 1

    .prologue
    .line 796
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->f:Lflipboard/gui/actionbar/FLActionBarMenu;

    return-object v0
.end method

.method public getOverflowItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/actionbar/FLActionBarMenuItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->g:Ljava/util/List;

    return-object v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 771
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->f:Lflipboard/gui/actionbar/FLActionBarMenu;

    if-eqz v0, :cond_0

    .line 772
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->f:Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 773
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->f:Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-virtual {v1, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->c(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    .line 774
    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lflipboard/gui/actionbar/FLActionBar;->removeView(Landroid/view/View;)V

    .line 772
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 777
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 806
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->f:Lflipboard/gui/actionbar/FLActionBarMenu;

    if-eqz v0, :cond_4

    .line 807
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->f:Lflipboard/gui/actionbar/FLActionBarMenu;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 808
    if-eqz v0, :cond_0

    .line 809
    sget-object v1, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    const-string v2, "likeButton"

    invoke-virtual {v1, v0, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 811
    :cond_0
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->f:Lflipboard/gui/actionbar/FLActionBarMenu;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 812
    if-eqz v0, :cond_1

    .line 813
    sget-object v1, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    const-string v2, "shareButton"

    invoke-virtual {v1, v0, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 815
    :cond_1
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->f:Lflipboard/gui/actionbar/FLActionBarMenu;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 816
    if-eqz v0, :cond_2

    .line 817
    sget-object v1, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    const-string v2, "commentButton"

    invoke-virtual {v1, v0, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 819
    :cond_2
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->f:Lflipboard/gui/actionbar/FLActionBarMenu;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 820
    if-eqz v0, :cond_3

    .line 822
    sget-object v1, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    const-string v2, "actionButton"

    invoke-virtual {v1, v0, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 824
    :cond_3
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->f:Lflipboard/gui/actionbar/FLActionBarMenu;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 825
    if-eqz v0, :cond_4

    .line 826
    sget-object v1, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    const-string v2, "humanoidButton"

    invoke-virtual {v1, v0, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 829
    :cond_4
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 830
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    const-string v2, "detailViewBackButton"

    invoke-virtual {v0, v1, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 832
    :cond_5
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 12

    .prologue
    .line 655
    sub-int v5, p4, p2

    .line 656
    sub-int v6, p5, p3

    .line 657
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 658
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 659
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 660
    iget-object v2, p0, Lflipboard/gui/actionbar/FLActionBar;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 661
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getPaddingLeft()I

    move-result v3

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v0, v3

    .line 663
    iget-object v3, p0, Lflipboard/gui/actionbar/FLActionBar;->a:Landroid/view/View;

    sub-int v2, v6, v2

    add-int/2addr v1, v0

    invoke-virtual {v3, v0, v2, v1, v6}, Landroid/view/View;->layout(IIII)V

    .line 666
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getPaddingLeft()I

    move-result v2

    .line 667
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/view/View;

    .line 668
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v4, 0x8

    if-eq v0, v4, :cond_7

    .line 669
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 670
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 671
    iget v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v7, v2

    .line 672
    div-int/lit8 v8, v6, 0x2

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    sub-int/2addr v8, v9

    .line 673
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v8

    .line 674
    add-int v10, v7, v4

    invoke-virtual {v1, v7, v8, v10, v9}, Landroid/view/View;->layout(IIII)V

    .line 676
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v4

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    add-int/2addr v0, v2

    :goto_1
    move v2, v0

    .line 678
    goto :goto_0

    .line 681
    :cond_1
    const/4 v0, 0x0

    move v3, v0

    move v4, v5

    :goto_2
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 683
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 684
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 685
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    .line 686
    iget v8, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v8, v4, v8

    sub-int/2addr v8, v7

    .line 687
    div-int/lit8 v9, v6, 0x2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    .line 688
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    add-int/2addr v10, v9

    .line 689
    add-int v11, v8, v7

    invoke-virtual {v0, v8, v9, v11, v10}, Landroid/view/View;->layout(IIII)V

    .line 691
    iget v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v0, v7

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    sub-int v1, v4, v0

    .line 681
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v4, v1

    goto :goto_2

    .line 695
    :cond_2
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->e:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    if-lez v0, :cond_4

    .line 696
    div-int/lit8 v3, v5, 0x2

    .line 698
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 699
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 700
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 701
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    const/4 v7, -0x1

    if-ne v0, v7, :cond_6

    move v0, v6

    .line 707
    :goto_3
    div-int/lit8 v1, v5, 0x2

    add-int/2addr v1, v3

    if-le v1, v4, :cond_5

    .line 709
    sub-int v2, v4, v5

    .line 720
    :cond_3
    :goto_4
    sub-int v1, v6, v0

    div-int/lit8 v1, v1, 0x2

    .line 721
    iget-object v3, p0, Lflipboard/gui/actionbar/FLActionBar;->e:Landroid/view/View;

    add-int v4, v2, v5

    add-int/2addr v0, v1

    invoke-virtual {v3, v2, v1, v4, v0}, Landroid/view/View;->layout(IIII)V

    .line 723
    :cond_4
    return-void

    .line 712
    :cond_5
    div-int/lit8 v1, v5, 0x2

    sub-int v1, v3, v1

    if-gt v2, v1, :cond_3

    .line 717
    div-int/lit8 v1, v5, 0x2

    sub-int v2, v3, v1

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_3

    :cond_7
    move v0, v2

    goto/16 :goto_1
.end method

.method protected onMeasure(II)V
    .locals 16

    .prologue
    .line 407
    const/4 v9, 0x0

    .line 408
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v14

    .line 410
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->e:Landroid/view/View;

    .line 411
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 412
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 413
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 414
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 418
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 419
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->f:Lflipboard/gui/actionbar/FLActionBarMenu;

    if-eqz v1, :cond_5

    .line 420
    const/4 v1, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/actionbar/FLActionBar;->f:Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-virtual {v2}, Lflipboard/gui/actionbar/FLActionBarMenu;->size()I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 421
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/actionbar/FLActionBar;->f:Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-virtual {v2, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->c(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v2

    .line 422
    iget-boolean v3, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    if-eqz v3, :cond_4

    .line 423
    iget v3, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->k:I

    if-nez v3, :cond_2

    .line 424
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/actionbar/FLActionBar;->g:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420
    :cond_0
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 416
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 425
    :cond_2
    iget v3, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->k:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 426
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/actionbar/FLActionBar;->k:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 428
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/actionbar/FLActionBar;->l:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 431
    :cond_4
    iget-object v3, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->m:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 432
    iget-object v2, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->m:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 439
    :cond_5
    const/4 v1, 0x0

    move v2, v1

    :goto_3
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/actionbar/FLActionBar;->getChildCount()I

    move-result v1

    if-ge v2, v1, :cond_b

    .line 440
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lflipboard/gui/actionbar/FLActionBar;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 441
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v4, 0x8

    if-eq v1, v4, :cond_7

    .line 442
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    if-eq v3, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->a:Landroid/view/View;

    if-eq v3, v1, :cond_7

    .line 445
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 448
    iget v4, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    const/16 v5, 0x11

    if-ne v4, v5, :cond_6

    .line 450
    move-object/from16 v0, p0

    iput-object v3, v0, Lflipboard/gui/actionbar/FLActionBar;->e:Landroid/view/View;

    .line 451
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lflipboard/gui/actionbar/FLActionBar;->o:Z

    if-eqz v4, :cond_6

    .line 452
    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/gui/actionbar/FLActionBar;->e:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 456
    :cond_6
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 457
    iget v4, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    const v5, 0x800003

    if-ne v4, v5, :cond_8

    .line 458
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->c:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 459
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->c:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 439
    :cond_7
    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 461
    :cond_8
    iget v1, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    const/4 v4, 0x3

    if-ne v1, v4, :cond_7

    .line 462
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 463
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 467
    :cond_9
    iget v4, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    const/4 v5, 0x3

    if-eq v4, v5, :cond_a

    iget v1, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    const v4, 0x800003

    if-ne v1, v4, :cond_7

    .line 468
    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 469
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 477
    :cond_b
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 478
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/actionbar/FLActionBar;->getPaddingRight()I

    move-result v1

    .line 484
    :goto_5
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 485
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/actionbar/FLActionBar;->c:Ljava/util/List;

    .line 489
    :goto_6
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_e

    .line 490
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v8, v1

    :goto_7
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 491
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v3, 0x8

    if-eq v1, v3, :cond_2a

    .line 492
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 493
    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move/from16 v3, p1

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v6}, Lflipboard/gui/actionbar/FLActionBar;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 494
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v3, v7, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v3

    iget v3, v7, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v1, v3

    add-int/2addr v1, v8

    .line 495
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget v3, v7, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v2, v3

    iget v3, v7, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v2, v3

    invoke-static {v9, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    :goto_8
    move v8, v1

    move v9, v2

    .line 497
    goto :goto_7

    .line 480
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/actionbar/FLActionBar;->getPaddingLeft()I

    move-result v1

    goto :goto_5

    .line 487
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    goto :goto_6

    :cond_e
    move v8, v1

    .line 501
    :cond_f
    const/4 v2, 0x0

    .line 502
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_10

    .line 503
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->k:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/actionbar/FLActionBar;->k:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;

    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 504
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 505
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/actionbar/FLActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09007b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 511
    :cond_10
    :goto_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v7, v2

    :goto_a
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;

    .line 512
    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v2

    .line 513
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 514
    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move/from16 v3, p1

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v6}, Lflipboard/gui/actionbar/FLActionBar;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 515
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int v3, v7, v1

    .line 516
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 517
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v4, v5

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v4

    invoke-static {v9, v1}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 518
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 519
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 520
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v7, v3

    goto :goto_a

    .line 507
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/actionbar/FLActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09007b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_9

    .line 523
    :cond_12
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 524
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_13
    move v7, v3

    .line 527
    goto :goto_a

    .line 533
    :cond_14
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    if-eqz v1, :cond_16

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_15

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_16

    .line 534
    :cond_15
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lflipboard/gui/FLCameleonImageView;->setVisibility(I)V

    .line 535
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move/from16 v3, p1

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v6}, Lflipboard/gui/actionbar/FLActionBar;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 536
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lflipboard/gui/FLCameleonImageView;->setVisibility(I)V

    .line 537
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLCameleonImageView;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v7

    move v10, v1

    .line 550
    :goto_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->e:Landroid/view/View;

    if-eqz v1, :cond_17

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lflipboard/gui/actionbar/FLActionBar;->o:Z

    if-nez v1, :cond_17

    .line 551
    sub-int v1, v14, v8

    sub-int v2, v1, v10

    .line 552
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 553
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/actionbar/FLActionBar;->e:Landroid/view/View;

    const/high16 v4, -0x80000000

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move/from16 v0, p2

    invoke-virtual {v3, v2, v0}, Landroid/view/View;->measure(II)V

    .line 554
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/actionbar/FLActionBar;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v2, v3

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v2

    invoke-static {v9, v1}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 555
    const/4 v1, 0x0

    sub-int v2, v14, v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/actionbar/FLActionBar;->e:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v8, v1

    .line 560
    :goto_c
    const/4 v2, 0x0

    .line 561
    const/4 v1, 0x0

    .line 564
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/actionbar/FLActionBar;->l:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    move v11, v2

    move v12, v7

    move v13, v9

    move v9, v1

    :goto_d
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1c

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lflipboard/gui/actionbar/FLActionBarMenuItem;

    .line 565
    invoke-virtual {v7}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v2

    .line 566
    if-eqz v11, :cond_18

    .line 567
    const/16 v1, 0x8

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 568
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->g:Ljava/util/List;

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 539
    :cond_16
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_29

    .line 540
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->l:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;

    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v2

    .line 541
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 542
    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move/from16 v3, p1

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v6}, Lflipboard/gui/actionbar/FLActionBar;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 543
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v7

    move v10, v1

    goto/16 :goto_b

    .line 557
    :cond_17
    sub-int v1, v14, v8

    move v8, v1

    goto :goto_c

    .line 570
    :cond_18
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 571
    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move/from16 v3, p1

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v6}, Lflipboard/gui/actionbar/FLActionBar;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 572
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int v3, v12, v1

    .line 573
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 574
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v4, v5

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v4

    invoke-static {v13, v1}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 575
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    if-eqz v1, :cond_19

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLCameleonImageView;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v3

    if-le v1, v8, :cond_19

    .line 578
    const/16 v1, 0x8

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 579
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->g:Ljava/util/List;

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 580
    const/4 v4, 0x1

    .line 581
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v3, v1

    move v11, v4

    move v12, v3

    move v13, v9

    move v9, v1

    goto/16 :goto_d

    .line 583
    :cond_19
    if-le v3, v8, :cond_1a

    .line 584
    const/16 v1, 0x8

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 585
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->g:Ljava/util/List;

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 586
    const/4 v4, 0x1

    .line 587
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v3, v1

    move v11, v4

    move v12, v3

    move v13, v9

    move v9, v1

    goto/16 :goto_d

    .line 589
    :cond_1a
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 590
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_e
    move v12, v3

    move v13, v9

    move v9, v3

    .line 598
    goto/16 :goto_d

    .line 592
    :cond_1b
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_e

    .line 601
    :cond_1c
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    if-eqz v1, :cond_28

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_28

    .line 602
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v1

    if-eqz v1, :cond_21

    .line 603
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    invoke-interface {v1, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 607
    :goto_f
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lflipboard/gui/FLCameleonImageView;->setVisibility(I)V

    .line 608
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLCameleonImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 609
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLCameleonImageView;->getMeasuredHeight()I

    move-result v2

    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v2, v3

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v2

    invoke-static {v13, v1}, Ljava/lang/Math;->max(II)I

    move-result v13

    move v6, v13

    .line 613
    :goto_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->a:Landroid/view/View;

    if-eqz v1, :cond_1d

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_1d

    .line 614
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 615
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/actionbar/FLActionBar;->a:Landroid/view/View;

    const/4 v4, 0x0

    move-object/from16 v1, p0

    move/from16 v3, p1

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v6}, Lflipboard/gui/actionbar/FLActionBar;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 616
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v2, v7, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v2

    iget v2, v7, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v2

    add-int/2addr v6, v1

    .line 626
    :cond_1d
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_27

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_27

    .line 627
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    if-eqz v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLCameleonImageView;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v9, v1

    .line 628
    :cond_1e
    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v7, v1

    .line 632
    :goto_11
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    if-ne v1, v2, :cond_22

    .line 633
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 637
    :goto_12
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v8

    .line 638
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1f
    :goto_13
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_23

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 639
    const/4 v4, 0x0

    move-object/from16 v1, p0

    move/from16 v3, p1

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v6}, Lflipboard/gui/actionbar/FLActionBar;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 640
    if-eqz v8, :cond_20

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    if-ne v2, v1, :cond_1f

    .line 641
    :cond_20
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v6, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v1, v3}, Landroid/view/View;->measure(II)V

    goto :goto_13

    .line 605
    :cond_21
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->c:Ljava/util/List;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    invoke-interface {v1, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto/16 :goto_f

    .line 635
    :cond_22
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/actionbar/FLActionBar;->getSuggestedMinimumHeight()I

    move-result v1

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    goto :goto_12

    .line 644
    :cond_23
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBar;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_24
    :goto_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_26

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 645
    if-nez v8, :cond_25

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/actionbar/FLActionBar;->b:Landroid/view/View;

    if-ne v1, v3, :cond_24

    .line 646
    :cond_25
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v6, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/view/View;->measure(II)V

    goto :goto_14

    .line 649
    :cond_26
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/actionbar/FLActionBar;->getSuggestedMinimumWidth()I

    move-result v1

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v6}, Lflipboard/gui/actionbar/FLActionBar;->setMeasuredDimension(II)V

    .line 650
    return-void

    :cond_27
    move v7, v14

    goto/16 :goto_11

    :cond_28
    move v6, v13

    goto/16 :goto_10

    :cond_29
    move v10, v7

    goto/16 :goto_b

    :cond_2a
    move v1, v8

    move v2, v9

    goto/16 :goto_8
.end method

.method public setCenterViewHidden(Z)V
    .locals 0

    .prologue
    .line 158
    iput-boolean p1, p0, Lflipboard/gui/actionbar/FLActionBar;->o:Z

    .line 159
    return-void
.end method

.method public setInverted(Z)V
    .locals 0

    .prologue
    .line 89
    iput-boolean p1, p0, Lflipboard/gui/actionbar/FLActionBar;->r:Z

    .line 90
    return-void
.end method

.method public setMenu(Lflipboard/gui/actionbar/FLActionBarMenu;)V
    .locals 5

    .prologue
    const v4, 0x7f0a039f

    const/4 v1, 0x0

    .line 732
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->f:Lflipboard/gui/actionbar/FLActionBarMenu;

    if-nez v0, :cond_1

    .line 734
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getId()I

    move-result v0

    const v2, 0x7f0a004c

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    .line 736
    :goto_0
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v2, v2, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 737
    iget-object v2, p0, Lflipboard/gui/actionbar/FLActionBar;->m:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v2}, Lflipboard/activities/FlipboardActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    const v3, 0x7f0f0002

    invoke-virtual {v2, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v2, 0x7f0a03a0

    invoke-virtual {p1, v2}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v2

    invoke-virtual {v2, v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    const v2, 0x7f0a03a1

    invoke-virtual {p1, v2}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v2

    invoke-virtual {v2, v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v2, v2, Lflipboard/service/FlipboardManager;->ah:Z

    if-eqz v2, :cond_4

    invoke-virtual {p1, v4}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v2

    invoke-virtual {v2, v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 740
    :cond_0
    :goto_1
    if-nez v0, :cond_1

    .line 744
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar;->j:Lflipboard/gui/FLCameleonImageView;

    if-nez v0, :cond_1

    .line 745
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/gui/actionbar/FLActionBar;->a(Landroid/content/Context;)V

    .line 751
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBar;->h()V

    .line 752
    iput-object p1, p0, Lflipboard/gui/actionbar/FLActionBar;->f:Lflipboard/gui/actionbar/FLActionBarMenu;

    .line 753
    :goto_2
    invoke-virtual {p1}, Lflipboard/gui/actionbar/FLActionBarMenu;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 754
    invoke-virtual {p1, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->c(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 755
    iget v2, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->k:I

    if-eqz v2, :cond_2

    .line 756
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v2

    .line 757
    new-instance v3, Lflipboard/gui/actionbar/FLActionBar$3;

    invoke-direct {v3, p0, v0}, Lflipboard/gui/actionbar/FLActionBar$3;-><init>(Lflipboard/gui/actionbar/FLActionBar;Lflipboard/gui/actionbar/FLActionBarMenuItem;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 765
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/actionbar/FLActionBar;->addView(Landroid/view/View;)V

    .line 753
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    move v0, v1

    .line 734
    goto :goto_0

    .line 737
    :cond_4
    invoke-virtual {p1, v4}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v2

    iput-boolean v1, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    goto :goto_1

    .line 768
    :cond_5
    return-void
.end method
