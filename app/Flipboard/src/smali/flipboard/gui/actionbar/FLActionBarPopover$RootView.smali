.class Lflipboard/gui/actionbar/FLActionBarPopover$RootView;
.super Landroid/widget/FrameLayout;
.source "FLActionBarPopover.java"


# instance fields
.field final synthetic a:Lflipboard/gui/actionbar/FLActionBarPopover;

.field private b:Z

.field private c:I

.field private d:I


# direct methods
.method constructor <init>(Lflipboard/gui/actionbar/FLActionBarPopover;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->a:Lflipboard/gui/actionbar/FLActionBarPopover;

    .line 133
    invoke-direct {p0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 134
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    .line 170
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->a:Lflipboard/gui/actionbar/FLActionBarPopover;

    iget-object v0, v0, Lflipboard/gui/actionbar/FLActionBarPopover;->e:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getMeasuredWidth()I

    move-result v2

    .line 171
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->a:Lflipboard/gui/actionbar/FLActionBarPopover;

    iget-object v0, v0, Lflipboard/gui/actionbar/FLActionBarPopover;->e:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getMeasuredHeight()I

    move-result v3

    .line 172
    sub-int v4, p4, p2

    .line 173
    sub-int v5, p5, p3

    .line 177
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->a:Lflipboard/gui/actionbar/FLActionBarPopover;

    iget-object v0, v0, Lflipboard/gui/actionbar/FLActionBarPopover;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->a:Lflipboard/gui/actionbar/FLActionBarPopover;

    iget-object v0, v0, Lflipboard/gui/actionbar/FLActionBarPopover;->b:Landroid/view/View;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->c(Landroid/view/View;)[I

    move-result-object v1

    .line 180
    new-instance v0, Landroid/graphics/Rect;

    const/4 v6, 0x0

    aget v6, v1, v6

    const/4 v7, 0x1

    aget v7, v1, v7

    const/4 v8, 0x0

    aget v8, v1, v8

    iget-object v9, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->a:Lflipboard/gui/actionbar/FLActionBarPopover;

    iget-object v9, v9, Lflipboard/gui/actionbar/FLActionBarPopover;->b:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    const/4 v9, 0x1

    aget v1, v1, v9

    iget-object v9, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->a:Lflipboard/gui/actionbar/FLActionBarPopover;

    iget-object v9, v9, Lflipboard/gui/actionbar/FLActionBarPopover;->b:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v9

    add-int/2addr v1, v9

    invoke-direct {v0, v6, v7, v8, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v1, v0

    .line 185
    :goto_0
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->a:Lflipboard/gui/actionbar/FLActionBarPopover;

    iget-object v0, v0, Lflipboard/gui/actionbar/FLActionBarPopover;->e:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 186
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 187
    sub-int/2addr v4, v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v0, v4, v0

    .line 188
    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    div-int/lit8 v7, v2, 0x2

    sub-int/2addr v4, v7

    invoke-static {v4, v6, v0}, Lflipboard/util/JavaUtil;->a(III)I

    move-result v0

    iput v0, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->c:I

    .line 191
    iget v0, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v0, v4

    div-int/lit8 v4, v5, 0x2

    if-lt v0, v4, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->b:Z

    .line 192
    iget-boolean v0, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->b:Z

    if-eqz v0, :cond_2

    iget v0, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v3

    :goto_2
    iput v0, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->d:I

    .line 195
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->a:Lflipboard/gui/actionbar/FLActionBarPopover;

    iget-object v0, v0, Lflipboard/gui/actionbar/FLActionBarPopover;->e:Landroid/widget/ScrollView;

    iget v1, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->c:I

    iget v4, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->d:I

    iget v5, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->c:I

    add-int/2addr v2, v5

    iget v5, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->d:I

    add-int/2addr v3, v5

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/widget/ScrollView;->layout(IIII)V

    .line 196
    return-void

    .line 182
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    div-int/lit8 v1, v4, 0x2

    div-int/lit8 v6, v4, 0x2

    invoke-direct {v0, v1, v5, v6, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v1, v0

    goto :goto_0

    .line 191
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 192
    :cond_2
    iget v0, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 11

    .prologue
    const/high16 v10, -0x80000000

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 148
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 149
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 150
    invoke-virtual {p0, v3, v4}, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->setMeasuredDimension(II)V

    .line 153
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->a:Lflipboard/gui/actionbar/FLActionBarPopover;

    iget-object v0, v0, Lflipboard/gui/actionbar/FLActionBarPopover;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->a:Lflipboard/gui/actionbar/FLActionBarPopover;

    iget-object v0, v0, Lflipboard/gui/actionbar/FLActionBarPopover;->b:Landroid/view/View;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->c(Landroid/view/View;)[I

    move-result-object v5

    .line 156
    new-instance v0, Landroid/graphics/Rect;

    aget v6, v5, v2

    aget v7, v5, v1

    aget v8, v5, v2

    iget-object v9, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->a:Lflipboard/gui/actionbar/FLActionBarPopover;

    iget-object v9, v9, Lflipboard/gui/actionbar/FLActionBarPopover;->b:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    aget v5, v5, v1

    iget-object v9, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->a:Lflipboard/gui/actionbar/FLActionBarPopover;

    iget-object v9, v9, Lflipboard/gui/actionbar/FLActionBarPopover;->b:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v9

    add-int/2addr v5, v9

    invoke-direct {v0, v6, v7, v8, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 161
    :goto_0
    iget v5, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    div-int/lit8 v6, v4, 0x2

    if-lt v5, v6, :cond_1

    :goto_1
    iput-boolean v1, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->b:Z

    .line 162
    iget-boolean v1, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->b:Z

    if-eqz v1, :cond_2

    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 163
    :goto_2
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->a:Lflipboard/gui/actionbar/FLActionBarPopover;

    iget-object v1, v1, Lflipboard/gui/actionbar/FLActionBarPopover;->d:Lflipboard/gui/actionbar/FLActionBarPopover$RootView;

    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09007b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    .line 165
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->a:Lflipboard/gui/actionbar/FLActionBarPopover;

    iget-object v1, v1, Lflipboard/gui/actionbar/FLActionBarPopover;->e:Landroid/widget/ScrollView;

    invoke-static {v3, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v0, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/widget/ScrollView;->measure(II)V

    .line 166
    return-void

    .line 158
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    div-int/lit8 v5, v3, 0x2

    div-int/lit8 v6, v3, 0x2

    invoke-direct {v0, v5, v4, v6, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0

    :cond_1
    move v1, v2

    .line 161
    goto :goto_1

    .line 162
    :cond_2
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v4, v0

    goto :goto_2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->a:Lflipboard/gui/actionbar/FLActionBarPopover;

    iget-object v0, v0, Lflipboard/gui/actionbar/FLActionBarPopover;->e:Landroid/widget/ScrollView;

    invoke-static {p1, v0}, Lflipboard/util/AndroidUtil;->a(Landroid/view/MotionEvent;Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->a:Lflipboard/gui/actionbar/FLActionBarPopover;

    iget-object v0, v0, Lflipboard/gui/actionbar/FLActionBarPopover;->c:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->b()Z

    .line 141
    const/4 v0, 0x1

    .line 143
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
