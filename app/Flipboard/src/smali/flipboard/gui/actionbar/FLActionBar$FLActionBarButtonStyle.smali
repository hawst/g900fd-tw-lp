.class public final enum Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;
.super Ljava/lang/Enum;
.source "FLActionBar.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

.field public static final enum b:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

.field public static final enum c:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

.field public static final enum d:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

.field private static final synthetic e:[Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 93
    new-instance v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    new-instance v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    const-string v1, "INVERTED"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    new-instance v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    const-string v1, "GREY"

    invoke-direct {v0, v1, v4}, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->c:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    new-instance v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    const-string v1, "SMALL_GRAY"

    invoke-direct {v0, v1, v5}, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->d:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    .line 92
    const/4 v0, 0x4

    new-array v0, v0, [Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->c:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->d:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    aput-object v1, v0, v5

    sput-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->e:[Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;
    .locals 1

    .prologue
    .line 92
    const-class v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->e:[Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    invoke-virtual {v0}, [Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    return-object v0
.end method
