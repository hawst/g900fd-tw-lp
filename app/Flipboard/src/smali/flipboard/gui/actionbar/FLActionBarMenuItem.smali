.class public Lflipboard/gui/actionbar/FLActionBarMenuItem;
.super Ljava/lang/Object;
.source "FLActionBarMenuItem.java"

# interfaces
.implements Landroid/view/MenuItem;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field a:I

.field b:I

.field c:I

.field d:Ljava/lang/CharSequence;

.field public e:Ljava/lang/CharSequence;

.field f:Landroid/content/Context;

.field g:Landroid/graphics/drawable/Drawable;

.field h:C

.field i:C

.field j:Landroid/content/Intent;

.field k:I

.field l:Lflipboard/gui/FLCameleonImageView;

.field public m:Landroid/view/View;

.field public n:Z

.field o:I

.field public p:Z

.field q:Z

.field r:Z

.field public s:Z

.field public t:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private u:Lflipboard/gui/FLLabelTextView;

.field private v:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->k:I

    .line 34
    iput-boolean v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->v:I

    .line 51
    iput p2, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a:I

    .line 52
    iput p3, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->b:I

    .line 53
    iput p4, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->c:I

    .line 54
    iput-object p5, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->d:Ljava/lang/CharSequence;

    .line 55
    iput-object p1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->f:Landroid/content/Context;

    .line 56
    return-void
.end method


# virtual methods
.method public final a()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->l:Lflipboard/gui/FLCameleonImageView;

    if-nez v0, :cond_0

    .line 338
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    .line 340
    :cond_0
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->l:Lflipboard/gui/FLCameleonImageView;

    return-object v0
.end method

.method public final a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;
    .locals 2

    .prologue
    .line 126
    iput p1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->o:I

    .line 127
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->l:Lflipboard/gui/FLCameleonImageView;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->l:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 130
    :cond_0
    return-object p0
.end method

.method public final a(Z)Lflipboard/gui/actionbar/FLActionBarMenuItem;
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->p:Z

    if-eqz v0, :cond_0

    .line 216
    iput-boolean p1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->q:Z

    .line 217
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->l:Lflipboard/gui/FLCameleonImageView;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->l:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLCameleonImageView;->setChecked(Z)V

    .line 221
    :cond_0
    return-object p0
.end method

.method public final a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V
    .locals 5

    .prologue
    const v4, 0x7f08005a

    const/4 v3, 0x1

    .line 182
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 183
    instance-of v1, v0, Lflipboard/gui/FLCameleonImageView;

    if-eqz v1, :cond_0

    .line 184
    check-cast v0, Lflipboard/gui/FLCameleonImageView;

    .line 185
    sget-object v1, Lflipboard/gui/actionbar/FLActionBarMenuItem$2;->a:[I

    invoke-virtual {p1}, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 195
    const v1, 0x7f08009b

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setDefaultAndCheckedColors$255f295(I)V

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 187
    :pswitch_0
    invoke-virtual {v0}, Lflipboard/gui/FLCameleonImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800aa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Lflipboard/gui/FLCameleonImageView;->a:I

    iput-boolean v3, v0, Lflipboard/gui/FLCameleonImageView;->e:Z

    invoke-virtual {v0}, Lflipboard/gui/FLCameleonImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Lflipboard/gui/FLCameleonImageView;->c:I

    invoke-virtual {v0}, Lflipboard/gui/FLCameleonImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080054

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Lflipboard/gui/FLCameleonImageView;->b:I

    iput-boolean v3, v0, Lflipboard/gui/FLCameleonImageView;->f:Z

    invoke-virtual {v0}, Lflipboard/gui/FLCameleonImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Lflipboard/gui/FLCameleonImageView;->d:I

    iput-boolean v3, v0, Lflipboard/gui/FLCameleonImageView;->g:Z

    invoke-virtual {v0}, Lflipboard/gui/FLCameleonImageView;->a()V

    goto :goto_0

    .line 191
    :pswitch_1
    invoke-virtual {v0, v4}, Lflipboard/gui/FLCameleonImageView;->setDefaultAndCheckedColors$255f295(I)V

    goto :goto_0

    .line 185
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public collapseActionView()Z
    .locals 2

    .prologue
    .line 367
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public expandActionView()Z
    .locals 2

    .prologue
    .line 362
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getActionProvider()Landroid/view/ActionProvider;
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 357
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getActionView()Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 300
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->m:Landroid/view/View;

    if-nez v0, :cond_1

    .line 301
    iget-boolean v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->s:Z

    if-eqz v0, :cond_2

    .line 302
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->f:Landroid/content/Context;

    const v1, 0x7f030008

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->m:Landroid/view/View;

    .line 320
    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->l:Lflipboard/gui/FLCameleonImageView;

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->l:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {p0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 323
    :cond_0
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->m:Landroid/view/View;

    iget v1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->b:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 324
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->m:Landroid/view/View;

    new-instance v1, Lflipboard/gui/actionbar/FLActionBarMenuItem$1;

    invoke-direct {v1, p0}, Lflipboard/gui/actionbar/FLActionBarMenuItem$1;-><init>(Lflipboard/gui/actionbar/FLActionBarMenuItem;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 333
    :cond_1
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->m:Landroid/view/View;

    return-object v0

    .line 303
    :cond_2
    iget-boolean v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->r:Z

    if-eqz v0, :cond_4

    .line 304
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->f:Landroid/content/Context;

    const v1, 0x7f030001

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    .line 305
    iget-object v1, v0, Lflipboard/gui/section/AttributionButtonWithText;->b:Lflipboard/gui/FLLabelTextView;

    iput-object v1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->u:Lflipboard/gui/FLLabelTextView;

    .line 306
    iget v1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->v:I

    if-lez v1, :cond_3

    .line 307
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->u:Lflipboard/gui/FLLabelTextView;

    iget-object v2, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->v:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lflipboard/gui/FLLabelTextView;->setTextColor(I)V

    .line 309
    :cond_3
    iget-object v1, v0, Lflipboard/gui/section/AttributionButtonWithText;->b:Lflipboard/gui/FLLabelTextView;

    iget-object v2, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->d:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 310
    iget-object v1, v0, Lflipboard/gui/section/AttributionButtonWithText;->b:Lflipboard/gui/FLLabelTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 311
    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->m:Landroid/view/View;

    goto :goto_0

    .line 313
    :cond_4
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->f:Landroid/content/Context;

    const v1, 0x7f030006

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLCameleonImageView;

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->l:Lflipboard/gui/FLCameleonImageView;

    .line 315
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 316
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->l:Lflipboard/gui/FLCameleonImageView;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v0, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Lflipboard/gui/FLCameleonImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 317
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->l:Lflipboard/gui/FLCameleonImageView;

    iget-boolean v1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->q:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setChecked(Z)V

    .line 318
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->l:Lflipboard/gui/FLCameleonImageView;

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->m:Landroid/view/View;

    goto/16 :goto_0
.end method

.method public getAlphabeticShortcut()C
    .locals 1

    .prologue
    .line 178
    iget-char v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->h:C

    return v0
.end method

.method public getGroupId()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a:I

    return v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->g:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    iget v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->o:I

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->o:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->g:Landroid/graphics/drawable/Drawable;

    .line 138
    :cond_0
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->g:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->j:Landroid/content/Intent;

    return-object v0
.end method

.method public getItemId()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->b:I

    return v0
.end method

.method public getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 2

    .prologue
    .line 272
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getNumericShortcut()C
    .locals 2

    .prologue
    .line 167
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getOrder()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->c:I

    return v0
.end method

.method public getSubMenu()Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitleCondensed()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public hasSubMenu()Z
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x0

    return v0
.end method

.method public isActionViewExpanded()Z
    .locals 2

    .prologue
    .line 372
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isCheckable()Z
    .locals 1

    .prologue
    .line 210
    iget-boolean v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->p:Z

    return v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 226
    iget-boolean v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->q:Z

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 251
    iget-boolean v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 240
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 21
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic setActionView(I)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->f:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->m:Landroid/view/View;

    return-object p0
.end method

.method public bridge synthetic setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->m:Landroid/view/View;

    return-object p0
.end method

.method public bridge synthetic setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 21
    iput-char p1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->h:C

    return-object p0
.end method

.method public bridge synthetic setCheckable(Z)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 21
    iput-boolean p1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->p:Z

    return-object p0
.end method

.method public synthetic setChecked(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Z)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setEnabled(Z)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 21
    iput-boolean p1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    return-object p0
.end method

.method public synthetic setIcon(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->g:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public bridge synthetic setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->j:Landroid/content/Intent;

    return-object p0
.end method

.method public bridge synthetic setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 21
    iput-char p1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->i:C

    return-object p0
.end method

.method public synthetic setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 21
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    return-object p0
.end method

.method public bridge synthetic setShortcut(CC)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 21
    iput-char p1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->i:C

    iput-char p2, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->h:C

    return-object p0
.end method

.method public setShowAsAction(I)V
    .locals 0

    .prologue
    .line 277
    iput p1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->k:I

    .line 278
    return-void
.end method

.method public bridge synthetic setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 21
    iput p1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->k:I

    return-object p0
.end method

.method public synthetic setTitle(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->f:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->d:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public bridge synthetic setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->d:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public bridge synthetic setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 21
    return-object p0
.end method

.method public bridge synthetic setVisible(Z)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 21
    return-object p0
.end method
