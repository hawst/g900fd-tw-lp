.class public Lflipboard/gui/FLExpandableLayout;
.super Landroid/widget/LinearLayout;
.source "FLExpandableLayout.java"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:I

.field private d:Z

.field private e:I

.field private f:I

.field private g:Lflipboard/gui/FLExpandableLayout$OnExpandClickListener;

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/FLExpandableLayout;->h:Z

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/FLExpandableLayout;->i:Z

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/FLExpandableLayout;->h:Z

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/FLExpandableLayout;->i:Z

    .line 66
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLExpandableLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/FLExpandableLayout;->h:Z

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/FLExpandableLayout;->i:Z

    .line 61
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLExpandableLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 74
    sget-object v0, Lflipboard/app/R$styleable;->FLExpandableLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 75
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lflipboard/gui/FLExpandableLayout;->e:I

    .line 76
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lflipboard/gui/FLExpandableLayout;->f:I

    .line 77
    const/4 v1, 0x2

    const/16 v2, 0x258

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lflipboard/gui/FLExpandableLayout;->c:I

    .line 78
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 80
    invoke-virtual {p0, v3}, Lflipboard/gui/FLExpandableLayout;->setOrientation(I)V

    .line 81
    return-void
.end method

.method static synthetic a(Lflipboard/gui/FLExpandableLayout;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lflipboard/gui/FLExpandableLayout;->d:Z

    return v0
.end method

.method static synthetic a(Lflipboard/gui/FLExpandableLayout;Z)Z
    .locals 0

    .prologue
    .line 35
    iput-boolean p1, p0, Lflipboard/gui/FLExpandableLayout;->d:Z

    return p1
.end method

.method static synthetic b(Lflipboard/gui/FLExpandableLayout;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lflipboard/gui/FLExpandableLayout;->h:Z

    return v0
.end method

.method static synthetic c(Lflipboard/gui/FLExpandableLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout;->b:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lflipboard/gui/FLExpandableLayout;)Lflipboard/gui/FLExpandableLayout$OnExpandClickListener;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout;->g:Lflipboard/gui/FLExpandableLayout$OnExpandClickListener;

    return-object v0
.end method

.method static synthetic e(Lflipboard/gui/FLExpandableLayout;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lflipboard/gui/FLExpandableLayout;->c:I

    return v0
.end method

.method static synthetic f(Lflipboard/gui/FLExpandableLayout;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lflipboard/gui/FLExpandableLayout;->i:Z

    return v0
.end method

.method private setExpandAnimations(Landroid/animation/LayoutTransition;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 145
    const/4 v0, 0x0

    const-string v1, "translateY"

    new-array v2, v3, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 146
    invoke-virtual {p1, v3, v0}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 147
    new-instance v1, Lflipboard/gui/FLExpandableLayout$2;

    invoke-direct {v1, p0}, Lflipboard/gui/FLExpandableLayout$2;-><init>(Lflipboard/gui/FLExpandableLayout;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 153
    return-void

    .line 145
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 100
    if-eqz p1, :cond_0

    .line 101
    iput-object p1, p0, Lflipboard/gui/FLExpandableLayout;->a:Landroid/view/View;

    .line 104
    :cond_0
    if-nez p2, :cond_1

    .line 105
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must pass a valid view to expand"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout;->a:Landroid/view/View;

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    .line 109
    iput-object p2, p0, Lflipboard/gui/FLExpandableLayout;->b:Landroid/view/View;

    .line 110
    iput-boolean v1, p0, Lflipboard/gui/FLExpandableLayout;->d:Z

    .line 111
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    .line 112
    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->setAnimateParentHierarchy(Z)V

    .line 113
    iget v1, p0, Lflipboard/gui/FLExpandableLayout;->c:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(J)V

    .line 114
    invoke-direct {p0, v0}, Lflipboard/gui/FLExpandableLayout;->setExpandAnimations(Landroid/animation/LayoutTransition;)V

    .line 115
    invoke-virtual {p0, v0}, Lflipboard/gui/FLExpandableLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 117
    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout;->a:Landroid/view/View;

    new-instance v1, Lflipboard/gui/FLExpandableLayout$1;

    invoke-direct {v1, p0}, Lflipboard/gui/FLExpandableLayout$1;-><init>(Lflipboard/gui/FLExpandableLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    :cond_2
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 161
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 162
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLExpandableLayout;->g:Lflipboard/gui/FLExpandableLayout$OnExpandClickListener;

    .line 163
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 86
    iget v0, p0, Lflipboard/gui/FLExpandableLayout;->e:I

    invoke-virtual {p0, v0}, Lflipboard/gui/FLExpandableLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FLExpandableLayout;->a:Landroid/view/View;

    .line 88
    iget v0, p0, Lflipboard/gui/FLExpandableLayout;->f:I

    if-eqz v0, :cond_0

    .line 89
    iget v0, p0, Lflipboard/gui/FLExpandableLayout;->f:I

    invoke-virtual {p0, v0}, Lflipboard/gui/FLExpandableLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FLExpandableLayout;->b:Landroid/view/View;

    .line 90
    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout;->a:Landroid/view/View;

    iget-object v1, p0, Lflipboard/gui/FLExpandableLayout;->b:Landroid/view/View;

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/FLExpandableLayout;->a(Landroid/view/View;Landroid/view/View;)V

    .line 92
    :cond_0
    return-void
.end method

.method public setExpandClickListener(Lflipboard/gui/FLExpandableLayout$OnExpandClickListener;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lflipboard/gui/FLExpandableLayout;->g:Lflipboard/gui/FLExpandableLayout$OnExpandClickListener;

    .line 157
    return-void
.end method
