.class public Lflipboard/gui/SocialBarTablet$1;
.super Ljava/lang/Object;
.source "SocialBarTablet.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/service/Section;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic c:Lflipboard/gui/SocialBarTablet;


# direct methods
.method public constructor <init>(Lflipboard/gui/SocialBarTablet;Lflipboard/service/Section;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lflipboard/gui/SocialBarTablet$1;->c:Lflipboard/gui/SocialBarTablet;

    iput-object p2, p0, Lflipboard/gui/SocialBarTablet$1;->a:Lflipboard/service/Section;

    iput-object p3, p0, Lflipboard/gui/SocialBarTablet$1;->b:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 98
    iget-object v0, p0, Lflipboard/gui/SocialBarTablet$1;->c:Lflipboard/gui/SocialBarTablet;

    invoke-virtual {v0}, Lflipboard/gui/SocialBarTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 99
    iget-object v1, p0, Lflipboard/gui/SocialBarTablet$1;->a:Lflipboard/service/Section;

    iget-object v2, p0, Lflipboard/gui/SocialBarTablet$1;->b:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v2}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    .line 100
    const-string v2, "launched_by_flipboard_activity"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 101
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 102
    const v1, 0x7f040013

    const v2, 0x7f040014

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 103
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 104
    return-void
.end method
