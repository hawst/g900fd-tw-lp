.class Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;
.super Ljava/lang/Object;
.source "FLSliderHider.java"


# instance fields
.field final synthetic a:Lflipboard/gui/FLSliderHider;

.field private b:Landroid/util/SparseIntArray;

.field private c:I


# direct methods
.method private constructor <init>(Lflipboard/gui/FLSliderHider;)V
    .locals 1

    .prologue
    .line 351
    iput-object p1, p0, Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;->a:Lflipboard/gui/FLSliderHider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lflipboard/gui/FLSliderHider;B)V
    .locals 0

    .prologue
    .line 351
    invoke-direct {p0, p1}, Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;-><init>(Lflipboard/gui/FLSliderHider;)V

    return-void
.end method

.method static synthetic a(Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;)I
    .locals 1

    .prologue
    .line 351
    iget v0, p0, Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;->c:I

    return v0
.end method

.method static synthetic a(Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;Landroid/util/SparseIntArray;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/high16 v5, -0x80000000

    .line 351
    iget-object v1, p0, Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;->b:Landroid/util/SparseIntArray;

    iput-object p1, p0, Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;->b:Landroid/util/SparseIntArray;

    iget-object v2, p0, Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0, v5}, Landroid/util/SparseIntArray;->get(II)I

    move-result v2

    if-eq v2, v5, :cond_1

    iput v2, p0, Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;->c:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz v1, :cond_0

    :goto_1
    invoke-virtual {v1}, Landroid/util/SparseIntArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v2, v5}, Landroid/util/SparseIntArray;->get(II)I

    move-result v2

    if-eq v2, v5, :cond_2

    sub-int v0, v2, v3

    iget v1, p0, Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;->c:I

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
