.class public Lflipboard/gui/firstrun/CategoryPickerCloud;
.super Landroid/widget/ListView;
.source "CategoryPickerCloud.java"


# instance fields
.field public a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lflipboard/gui/FLTextView;

.field public g:Lflipboard/gui/FLTextView;

.field public h:I

.field public i:Lflipboard/gui/FLTextView;

.field private j:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m:Z

.field private n:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private o:Landroid/view/View$OnTouchListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->b:Ljava/util/List;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->c:Ljava/util/Map;

    .line 79
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->j:Ljava/util/HashSet;

    .line 85
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->n:Ljava/util/HashSet;

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->b:Ljava/util/List;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->c:Ljava/util/Map;

    .line 79
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->j:Ljava/util/HashSet;

    .line 85
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->n:Ljava/util/HashSet;

    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->b:Ljava/util/List;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->c:Ljava/util/Map;

    .line 79
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->j:Ljava/util/HashSet;

    .line 85
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->n:Ljava/util/HashSet;

    .line 94
    return-void
.end method

.method static synthetic a(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->f:Lflipboard/gui/FLTextView;

    return-object v0
.end method

.method static synthetic a(Lflipboard/gui/firstrun/CategoryPickerCloud;Ljava/util/List;)Ljava/util/List;
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/List;Z)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/firstrun/CategoryPickerCloud$ItemInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 319
    invoke-virtual {p0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002c

    invoke-virtual {v0, v1, p0, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 320
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 321
    invoke-virtual {p0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09007e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 322
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;

    .line 323
    new-instance v6, Lflipboard/gui/firstrun/CategoryPickerCloud$ItemInfo;

    invoke-direct {v6, p0}, Lflipboard/gui/firstrun/CategoryPickerCloud$ItemInfo;-><init>(Lflipboard/gui/firstrun/CategoryPickerCloud;)V

    .line 325
    const/4 v2, 0x0

    .line 327
    if-nez p2, :cond_1

    iget-object v7, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->k:Ljava/util/Map;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->k:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    .line 328
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->k:Ljava/util/Map;

    iget-object v7, v1, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->remoteid:Ljava/lang/String;

    invoke-interface {v2, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;

    .line 331
    :cond_1
    if-eqz v1, :cond_0

    iget-object v7, v1, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->sectionTitle:Ljava/lang/String;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->j:Ljava/util/HashSet;

    iget-object v8, v1, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->remoteid:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    if-nez v2, :cond_0

    .line 332
    iget-object v2, v1, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->sectionTitle:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 334
    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v0, v2, v7}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 335
    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v2

    mul-int/lit8 v7, v4, 0x2

    add-int/2addr v2, v7

    iput v2, v6, Lflipboard/gui/firstrun/CategoryPickerCloud$ItemInfo;->b:I

    .line 336
    iput-object v1, v6, Lflipboard/gui/firstrun/CategoryPickerCloud$ItemInfo;->a:Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;

    .line 337
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 340
    :cond_2
    return-object v3
.end method

.method private a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 233
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_3

    .line 234
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;

    .line 235
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->l:Ljava/util/Map;

    iget-object v3, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->remoteid:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->l:Ljava/util/Map;

    iget-object v3, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->sectionTitle:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 236
    :cond_1
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->sectionTitle:Ljava/lang/String;

    aput-object v4, v2, v3

    iget-object v3, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->remoteid:Ljava/lang/String;

    aput-object v3, v2, v5

    .line 237
    iput-boolean v5, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->m:Z

    .line 241
    :goto_1
    iget-object v2, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->children:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->children:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 242
    iget-object v0, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->children:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Ljava/util/List;)V

    goto :goto_0

    .line 239
    :cond_2
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->l:Ljava/util/Map;

    iget-object v3, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->remoteid:Ljava/lang/String;

    iget-object v4, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->sectionTitle:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 246
    :cond_3
    return-void
.end method

.method static synthetic b(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->g:Lflipboard/gui/FLTextView;

    return-object v0
.end method

.method static synthetic b(Lflipboard/gui/firstrun/CategoryPickerCloud;Ljava/util/List;)Ljava/util/List;
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lflipboard/gui/firstrun/CategoryPickerCloud;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/util/List;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/firstrun/CategoryPickerCloud$ItemInfo;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/firstrun/CategoryPickerCloud$TopicRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 253
    invoke-virtual {p0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 254
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v0

    mul-int/lit8 v1, v4, 0x2

    sub-int v2, v0, v1

    .line 257
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 258
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/firstrun/CategoryPickerCloud$ItemInfo;

    .line 259
    iget v7, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$ItemInfo;->b:I

    mul-int/lit8 v8, v4, 0x2

    add-int/2addr v7, v8

    .line 260
    sub-int/2addr v1, v7

    .line 263
    if-lez v1, :cond_1

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/4 v9, 0x3

    if-ge v8, v9, :cond_1

    .line 265
    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$ItemInfo;->a:Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 267
    :cond_1
    new-instance v1, Lflipboard/gui/firstrun/CategoryPickerCloud$TopicRow;

    invoke-direct {v1, p0, v5}, Lflipboard/gui/firstrun/CategoryPickerCloud$TopicRow;-><init>(Lflipboard/gui/firstrun/CategoryPickerCloud;Ljava/util/ArrayList;)V

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 269
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 272
    sub-int v1, v2, v7

    .line 273
    if-lez v1, :cond_0

    .line 274
    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$ItemInfo;->a:Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 279
    :cond_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 280
    new-instance v0, Lflipboard/gui/firstrun/CategoryPickerCloud$TopicRow;

    invoke-direct {v0, p0, v5}, Lflipboard/gui/firstrun/CategoryPickerCloud$TopicRow;-><init>(Lflipboard/gui/firstrun/CategoryPickerCloud;Ljava/util/ArrayList;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    :cond_3
    return-object v3
.end method

.method static synthetic c(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->i:Lflipboard/gui/FLTextView;

    return-object v0
.end method

.method static synthetic d(Lflipboard/gui/firstrun/CategoryPickerCloud;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->n:Ljava/util/HashSet;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;ZZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 295
    invoke-direct {p0, p1, p2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v0

    .line 297
    invoke-direct {p0, v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 298
    new-instance v1, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    invoke-virtual {p0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0, p3}, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;-><init>(Lflipboard/gui/firstrun/CategoryPickerCloud;Landroid/content/Context;Ljava/util/List;Z)V

    iput-object v1, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    .line 299
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    invoke-virtual {p0, v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 300
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 131
    const/4 v1, 0x0

    .line 133
    :try_start_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->B()Lflipboard/model/FirstLaunchTopicInfo;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v3, v0

    .line 138
    :goto_0
    if-eqz v3, :cond_7

    .line 139
    new-instance v0, Lflipboard/gui/section/ListSingleThreadWrapper;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1}, Lflipboard/gui/section/ListSingleThreadWrapper;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->d:Ljava/util/List;

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->e:Ljava/util/List;

    .line 142
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->l:Ljava/util/Map;

    .line 143
    iget-object v0, v3, Lflipboard/model/FirstLaunchTopicInfo;->alwaysFirstTrancheTopics:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Ljava/util/List;)V

    .line 144
    iget-object v0, v3, Lflipboard/model/FirstLaunchTopicInfo;->secondTrancheTopics:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Ljava/util/List;)V

    .line 145
    iget-object v0, v3, Lflipboard/model/FirstLaunchTopicInfo;->thirdTrancheTopics:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Ljava/util/List;)V

    .line 146
    iget-object v0, v3, Lflipboard/model/FirstLaunchTopicInfo;->topicHierarchy:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Ljava/util/List;)V

    .line 147
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 149
    iget-boolean v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->m:Z

    if-eqz v0, :cond_0

    .line 150
    iget-boolean v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->m:Z

    invoke-static {v6, v0}, Ljunit/framework/Assert;->assertEquals(ZZ)V

    .line 153
    :cond_0
    if-eqz p1, :cond_4

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 156
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->b:Ljava/util/List;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 158
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 159
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->k:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 162
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;

    .line 163
    iget-object v1, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->k:Ljava/util/Map;

    iget-object v5, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->remoteid:Ljava/lang/String;

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 164
    iget-object v1, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->k:Ljava/util/Map;

    iget-object v5, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->remoteid:Ljava/lang/String;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;

    iget-boolean v1, v1, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->isSelected:Z

    iput-boolean v1, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->isSelected:Z

    .line 165
    iget-object v1, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->k:Ljava/util/Map;

    iget-object v0, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->remoteid:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 134
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v1

    goto/16 :goto_0

    .line 169
    :cond_2
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v2, v6, v0}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-object v0, v2

    .line 181
    :goto_2
    iget-object v1, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 182
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->e:Ljava/util/List;

    iget-object v1, v3, Lflipboard/model/FirstLaunchTopicInfo;->thirdTrancheTopics:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 184
    iget-object v0, v3, Lflipboard/model/FirstLaunchTopicInfo;->topicHierarchy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;

    .line 185
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->d:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    iget-object v2, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->children:Ljava/util/List;

    if-eqz v2, :cond_3

    .line 189
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->d:Ljava/util/List;

    iget-object v0, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->children:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    .line 172
    :cond_4
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 174
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->b:Ljava/util/List;

    iget-object v1, v3, Lflipboard/model/FirstLaunchTopicInfo;->alwaysFirstTrancheTopics:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 176
    iget-object v0, v3, Lflipboard/model/FirstLaunchTopicInfo;->secondTrancheTopics:Ljava/util/List;

    .line 178
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->shuffle(Ljava/util/List;Ljava/util/Random;)V

    goto :goto_2

    .line 194
    :cond_5
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 195
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->e:Ljava/util/List;

    .line 196
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 197
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->j:Ljava/util/HashSet;

    iget-object v0, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 200
    :cond_6
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->d:Ljava/util/List;

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->shuffle(Ljava/util/List;Ljava/util/Random;)V

    .line 202
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->b:Ljava/util/List;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, v6}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Ljava/util/List;ZZ)V

    .line 204
    :cond_7
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->o:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->o:Landroid/view/View$OnTouchListener;

    invoke-interface {v0, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 127
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public getSelectedTopics()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 310
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->c:Ljava/util/Map;

    return-object v0
.end method

.method public getShownItemsCount()I
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->n:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 102
    invoke-super {p0}, Landroid/widget/ListView;->onFinishInflate()V

    .line 103
    invoke-virtual {p0, v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 104
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->k:Ljava/util/Map;

    .line 105
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget v0, v0, Lflipboard/model/ConfigSetting;->MinimumTopicPickerCount:I

    iput v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->h:I

    .line 107
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v0

    const v1, 0x3fe66666    # 1.8f

    mul-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->setFriction(F)V

    .line 108
    invoke-virtual {p0, v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Ljava/util/Map;)V

    .line 111
    invoke-virtual {p0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040010

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 112
    new-instance v1, Landroid/view/animation/LayoutAnimationController;

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-direct {v1, v0, v2}, Landroid/view/animation/LayoutAnimationController;-><init>(Landroid/view/animation/Animation;F)V

    .line 113
    invoke-virtual {p0, v1}, Lflipboard/gui/firstrun/CategoryPickerCloud;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    .line 114
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lflipboard/gui/firstrun/CategoryPickerCloud;->o:Landroid/view/View$OnTouchListener;

    .line 120
    return-void
.end method
