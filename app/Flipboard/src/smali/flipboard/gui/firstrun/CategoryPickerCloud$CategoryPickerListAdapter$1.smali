.class Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;
.super Ljava/lang/Object;
.source "CategoryPickerCloud.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;


# direct methods
.method constructor <init>(Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;)V
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const v7, 0x7f0d0158

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 393
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    invoke-virtual {v0}, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x7f040000

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 394
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;

    .line 395
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud;->c:Ljava/util/Map;

    iget-object v3, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->remoteid:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 396
    const v2, 0x7f02021c

    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 397
    iput-boolean v6, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->isSelected:Z

    .line 399
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud;->c:Ljava/util/Map;

    iget-object v3, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->remoteid:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->e:Ljava/util/Stack;

    invoke-virtual {v2, v0}, Ljava/util/Stack;->remove(Ljava/lang/Object;)Z

    .line 410
    :goto_0
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->c(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 411
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 412
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->c(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;

    move-result-object v2

    iget-object v3, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v3, v3, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-virtual {v3}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 418
    :cond_0
    :goto_1
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->b(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 419
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 420
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2}, Lflipboard/service/User;->t()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 421
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->b(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;

    move-result-object v0

    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-virtual {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v4, v4, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v4, v4, Lflipboard/gui/firstrun/CategoryPickerCloud;->c:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 441
    :cond_1
    :goto_2
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 442
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud;->h:I

    if-lt v0, v2, :cond_a

    .line 443
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;

    move-result-object v0

    const v2, 0x7f0201bf

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setBackgroundResource(I)V

    .line 444
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;

    move-result-object v0

    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-virtual {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080033

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    .line 452
    :cond_2
    :goto_3
    invoke-virtual {p1, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 453
    return-void

    .line 402
    :cond_3
    const v2, 0x7f0201c3

    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 403
    iput-boolean v5, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->isSelected:Z

    .line 405
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud;->c:Ljava/util/Map;

    iget-object v3, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->remoteid:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->e:Ljava/util/Stack;

    invoke-virtual {v2, v0}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 414
    :cond_4
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->c(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;

    move-result-object v2

    iget-object v3, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v3, v3, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-virtual {v3}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d023f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 424
    :cond_5
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-ne v2, v5, :cond_6

    .line 425
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->b(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;

    move-result-object v0

    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-virtual {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 426
    :cond_6
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    iget-object v3, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v3, v3, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget v3, v3, Lflipboard/gui/firstrun/CategoryPickerCloud;->h:I

    if-ne v2, v3, :cond_7

    iget-boolean v0, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->isSelected:Z

    if-eqz v0, :cond_7

    .line 427
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->b(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;

    move-result-object v0

    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-virtual {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0108

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 428
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->b(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_2

    .line 429
    :cond_7
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud;->h:I

    if-ge v0, v2, :cond_1

    .line 430
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->b(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;

    move-result-object v0

    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-virtual {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v4, v4, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v4, v4, Lflipboard/gui/firstrun/CategoryPickerCloud;->c:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 434
    :cond_8
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-virtual {v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 435
    const v0, 0x7f0d0106

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v4, v4, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget v4, v4, Lflipboard/gui/firstrun/CategoryPickerCloud;->h:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 436
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v3}, Lflipboard/service/User;->c()Z

    move-result v3

    if-eqz v3, :cond_9

    const v0, 0x7f0d0152

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 437
    :cond_9
    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->b(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;

    move-result-object v2

    invoke-virtual {v2, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 445
    :cond_a
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud;->h:I

    if-ge v0, v2, :cond_2

    .line 446
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;

    move-result-object v0

    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-virtual {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080037

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    .line 447
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;

    move-result-object v0

    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;->a:Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-virtual {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setBackgroundColor(I)V

    goto/16 :goto_3
.end method
