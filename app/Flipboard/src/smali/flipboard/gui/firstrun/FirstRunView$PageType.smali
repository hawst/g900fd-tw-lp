.class public final enum Lflipboard/gui/firstrun/FirstRunView$PageType;
.super Ljava/lang/Enum;
.source "FirstRunView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/firstrun/FirstRunView$PageType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/firstrun/FirstRunView$PageType;

.field public static final enum b:Lflipboard/gui/firstrun/FirstRunView$PageType;

.field private static final synthetic c:[Lflipboard/gui/firstrun/FirstRunView$PageType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55
    new-instance v0, Lflipboard/gui/firstrun/FirstRunView$PageType;

    const-string v1, "cover"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/firstrun/FirstRunView$PageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/firstrun/FirstRunView$PageType;->a:Lflipboard/gui/firstrun/FirstRunView$PageType;

    new-instance v0, Lflipboard/gui/firstrun/FirstRunView$PageType;

    const-string v1, "category_selector"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/firstrun/FirstRunView$PageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/firstrun/FirstRunView$PageType;->b:Lflipboard/gui/firstrun/FirstRunView$PageType;

    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/gui/firstrun/FirstRunView$PageType;

    sget-object v1, Lflipboard/gui/firstrun/FirstRunView$PageType;->a:Lflipboard/gui/firstrun/FirstRunView$PageType;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/firstrun/FirstRunView$PageType;->b:Lflipboard/gui/firstrun/FirstRunView$PageType;

    aput-object v1, v0, v3

    sput-object v0, Lflipboard/gui/firstrun/FirstRunView$PageType;->c:[Lflipboard/gui/firstrun/FirstRunView$PageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/firstrun/FirstRunView$PageType;
    .locals 1

    .prologue
    .line 55
    const-class v0, Lflipboard/gui/firstrun/FirstRunView$PageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/firstrun/FirstRunView$PageType;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/firstrun/FirstRunView$PageType;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lflipboard/gui/firstrun/FirstRunView$PageType;->c:[Lflipboard/gui/firstrun/FirstRunView$PageType;

    invoke-virtual {v0}, [Lflipboard/gui/firstrun/FirstRunView$PageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/firstrun/FirstRunView$PageType;

    return-object v0
.end method
