.class public Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "CategoryPickerCloud.java"


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/firstrun/CategoryPickerCloud$TopicRow;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field c:Z

.field d:I

.field e:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;",
            ">;"
        }
    .end annotation
.end field

.field f:Landroid/view/View$OnClickListener;

.field final synthetic g:Lflipboard/gui/firstrun/CategoryPickerCloud;


# direct methods
.method public constructor <init>(Lflipboard/gui/firstrun/CategoryPickerCloud;Landroid/content/Context;Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .line 383
    iput-object p1, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    .line 384
    const v0, 0x7f03002a

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 377
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->b:Ljava/util/HashSet;

    .line 379
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->d:I

    .line 381
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->e:Ljava/util/Stack;

    .line 390
    new-instance v0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;

    invoke-direct {v0, p0}, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$1;-><init>(Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;)V

    iput-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->f:Landroid/view/View$OnClickListener;

    .line 385
    iput-object p3, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->a:Ljava/util/List;

    .line 386
    iput-boolean p4, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->c:Z

    .line 387
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 532
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 533
    new-instance v1, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$2;

    invoke-direct {v1, p0, p1, p2, v0}, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$2;-><init>(Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;Landroid/view/View;ILandroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 546
    return-void
.end method

.method private static a(Lflipboard/gui/FLTextView;Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;)V
    .locals 1

    .prologue
    .line 549
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 550
    iget-object v0, p1, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->sectionTitle:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 551
    invoke-virtual {p0, p1}, Lflipboard/gui/FLTextView;->setTag(Ljava/lang/Object;)V

    .line 554
    iget-boolean v0, p1, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->isSelected:Z

    if-eqz v0, :cond_0

    .line 555
    const v0, 0x7f0201c3

    invoke-virtual {p0, v0}, Lflipboard/gui/FLTextView;->setBackgroundResource(I)V

    .line 559
    :goto_0
    return-void

    .line 557
    :cond_0
    const v0, 0x7f02021c

    invoke-virtual {p0, v0}, Lflipboard/gui/FLTextView;->setBackgroundResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/16 v5, 0x8

    const/4 v3, 0x0

    .line 459
    if-eqz p2, :cond_1

    .line 460
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;

    move-object v1, v0

    .line 479
    :goto_0
    iget-object v0, v1, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->a:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 480
    iget-object v0, v1, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 481
    iget-object v0, v1, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 482
    iget-object v0, v1, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 484
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/firstrun/CategoryPickerCloud$TopicRow;

    iget-object v4, v0, Lflipboard/gui/firstrun/CategoryPickerCloud$TopicRow;->a:Ljava/util/ArrayList;

    .line 485
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-virtual {v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09007e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    move v2, v3

    .line 486
    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 487
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;

    .line 488
    iget-object v6, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->b:Ljava/util/HashSet;

    iget-object v7, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->remoteid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 489
    iget-object v6, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v6}, Lflipboard/gui/firstrun/CategoryPickerCloud;->d(Lflipboard/gui/firstrun/CategoryPickerCloud;)Ljava/util/HashSet;

    move-result-object v6

    iget-object v7, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->remoteid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 490
    if-nez v2, :cond_2

    .line 491
    iget-object v6, v1, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->a:Lflipboard/gui/FLTextView;

    invoke-static {v6, v0}, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->a(Lflipboard/gui/FLTextView;Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;)V

    .line 492
    iget-object v0, v1, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->a:Lflipboard/gui/FLTextView;

    invoke-direct {p0, v0, v5}, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->a(Landroid/view/View;I)V

    .line 486
    :cond_0
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 462
    :cond_1
    new-instance v2, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;

    invoke-direct {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;-><init>()V

    .line 463
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v1, 0x7f03002a

    invoke-static {v0, v1, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 464
    const v1, 0x7f0a00b0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextView;

    iput-object v1, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->a:Lflipboard/gui/FLTextView;

    .line 465
    const v1, 0x7f0a00b1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextView;

    iput-object v1, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->b:Lflipboard/gui/FLTextView;

    .line 466
    const v1, 0x7f0a00b2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextView;

    iput-object v1, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->c:Lflipboard/gui/FLTextView;

    .line 467
    const v1, 0x7f0a00b3

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextView;

    iput-object v1, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->d:Lflipboard/gui/FLTextView;

    .line 470
    iget-object v1, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->a:Lflipboard/gui/FLTextView;

    iget-object v4, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Lflipboard/gui/FLTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 471
    iget-object v1, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->b:Lflipboard/gui/FLTextView;

    iget-object v4, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Lflipboard/gui/FLTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 472
    iget-object v1, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->c:Lflipboard/gui/FLTextView;

    iget-object v4, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Lflipboard/gui/FLTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 473
    iget-object v1, v2, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->d:Lflipboard/gui/FLTextView;

    iget-object v4, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Lflipboard/gui/FLTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 476
    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    move-object p2, v0

    goto/16 :goto_0

    .line 493
    :cond_2
    const/4 v6, 0x1

    if-ne v2, v6, :cond_3

    .line 494
    iget-object v6, v1, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->b:Lflipboard/gui/FLTextView;

    invoke-static {v6, v0}, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->a(Lflipboard/gui/FLTextView;Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;)V

    .line 495
    iget-object v0, v1, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->b:Lflipboard/gui/FLTextView;

    invoke-direct {p0, v0, v5}, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->a(Landroid/view/View;I)V

    goto :goto_2

    .line 496
    :cond_3
    const/4 v6, 0x2

    if-ne v2, v6, :cond_4

    .line 497
    iget-object v6, v1, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->c:Lflipboard/gui/FLTextView;

    invoke-static {v6, v0}, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->a(Lflipboard/gui/FLTextView;Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;)V

    .line 498
    iget-object v0, v1, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->c:Lflipboard/gui/FLTextView;

    invoke-direct {p0, v0, v5}, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->a(Landroid/view/View;I)V

    goto :goto_2

    .line 499
    :cond_4
    const/4 v6, 0x3

    if-ne v2, v6, :cond_0

    .line 500
    iget-object v6, v1, Lflipboard/gui/firstrun/CategoryPickerCloud$Holder;->d:Lflipboard/gui/FLTextView;

    invoke-static {v6, v0}, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->a(Lflipboard/gui/FLTextView;Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;)V

    goto/16 :goto_2

    .line 505
    :cond_5
    iget v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->d:I

    if-ge v0, p1, :cond_6

    .line 507
    invoke-virtual {p0}, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040010

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 508
    const-wide/16 v4, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 509
    const-wide/16 v4, 0xfa

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 510
    invoke-virtual {p2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 511
    iput p1, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->d:I

    .line 514
    :cond_6
    iget-boolean v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->c:Z

    if-nez v0, :cond_d

    .line 518
    invoke-virtual {p0}, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x5

    if-eq p1, v0, :cond_7

    invoke-virtual {p0}, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_d

    :cond_7
    move v2, v3

    move v1, v3

    .line 519
    :goto_3
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    const/16 v0, 0xa

    if-ge v1, v0, :cond_9

    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;

    iget-object v0, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->children:Ljava/util/List;

    if-eqz v0, :cond_8

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_8

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;

    iget-object v5, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->b:Ljava/util/HashSet;

    iget-object v6, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->remoteid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_e

    iget-object v5, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v5, v5, Lflipboard/gui/firstrun/CategoryPickerCloud;->e:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v5, v5, Lflipboard/gui/firstrun/CategoryPickerCloud;->d:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    :goto_5
    move v1, v0

    goto :goto_4

    :cond_8
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_9
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x16

    if-ge v0, v1, :cond_c

    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    rsub-int/lit8 v0, v0, 0x16

    iget-object v1, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v1, v1, Lflipboard/gui/firstrun/CategoryPickerCloud;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v2, v2, Lflipboard/gui/firstrun/CategoryPickerCloud;->d:Ljava/util/List;

    invoke-interface {v2, v3, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a
    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;

    iget-object v3, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->b:Ljava/util/HashSet;

    iget-object v4, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->remoteid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    iget-object v3, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v3, v3, Lflipboard/gui/firstrun/CategoryPickerCloud;->e:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_b
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    :cond_c
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;->g:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud;->e:Ljava/util/List;

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->shuffle(Ljava/util/List;Ljava/util/Random;)V

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$3;

    invoke-direct {v1, p0}, Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter$3;-><init>(Lflipboard/gui/firstrun/CategoryPickerCloud$CategoryPickerListAdapter;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 524
    :cond_d
    invoke-virtual {p2, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 525
    return-object p2

    :cond_e
    move v0, v1

    goto/16 :goto_5
.end method
