.class Lflipboard/gui/firstrun/ScrollingTagCloud$1;
.super Ljava/lang/Object;
.source "ScrollingTagCloud.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Lflipboard/gui/firstrun/ScrollingLinearLayout;

.field final synthetic b:Landroid/view/ViewGroup;

.field final synthetic c:Lflipboard/gui/firstrun/ScrollingTagCloud;


# direct methods
.method constructor <init>(Lflipboard/gui/firstrun/ScrollingTagCloud;Lflipboard/gui/firstrun/ScrollingLinearLayout;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->c:Lflipboard/gui/firstrun/ScrollingTagCloud;

    iput-object p2, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->a:Lflipboard/gui/firstrun/ScrollingLinearLayout;

    iput-object p3, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->b:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 7

    .prologue
    .line 92
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->a:Lflipboard/gui/firstrun/ScrollingLinearLayout;

    invoke-virtual {v0}, Lflipboard/gui/firstrun/ScrollingLinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 94
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->c:Lflipboard/gui/firstrun/ScrollingTagCloud;

    iget-object v1, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->a:Lflipboard/gui/firstrun/ScrollingLinearLayout;

    iget-object v2, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->c:Lflipboard/gui/firstrun/ScrollingTagCloud;

    invoke-virtual {v2}, Lflipboard/gui/firstrun/ScrollingTagCloud;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iget-object v3, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->a:Lflipboard/gui/firstrun/ScrollingLinearLayout;

    iget v3, v3, Lflipboard/gui/firstrun/ScrollingLinearLayout;->a:I

    invoke-static {v0, v1, v2, v3}, Lflipboard/gui/firstrun/ScrollingTagCloud;->a(Lflipboard/gui/firstrun/ScrollingTagCloud;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;I)V

    .line 96
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->c:Lflipboard/gui/firstrun/ScrollingTagCloud;

    iget-object v1, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->c:Lflipboard/gui/firstrun/ScrollingTagCloud;

    invoke-static {v1}, Lflipboard/gui/firstrun/ScrollingTagCloud;->a(Lflipboard/gui/firstrun/ScrollingTagCloud;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x6

    invoke-static {v0, v1}, Lflipboard/gui/firstrun/ScrollingTagCloud;->a(Lflipboard/gui/firstrun/ScrollingTagCloud;I)I

    .line 97
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->c:Lflipboard/gui/firstrun/ScrollingTagCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->b(Lflipboard/gui/firstrun/ScrollingTagCloud;)I

    move-result v0

    mul-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    .line 99
    iget-object v1, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->c:Lflipboard/gui/firstrun/ScrollingTagCloud;

    iget-object v2, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->a:Lflipboard/gui/firstrun/ScrollingLinearLayout;

    const-string v3, "y"

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    iget-object v6, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->a:Lflipboard/gui/firstrun/ScrollingLinearLayout;

    invoke-virtual {v6}, Lflipboard/gui/firstrun/ScrollingLinearLayout;->getTop()I

    move-result v6

    int-to-float v6, v6

    aput v6, v4, v5

    const/4 v5, 0x1

    aput v0, v4, v5

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-static {v1, v0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->a(Lflipboard/gui/firstrun/ScrollingTagCloud;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    .line 100
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->c:Lflipboard/gui/firstrun/ScrollingTagCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->c(Lflipboard/gui/firstrun/ScrollingTagCloud;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 101
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->c:Lflipboard/gui/firstrun/ScrollingTagCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->c(Lflipboard/gui/firstrun/ScrollingTagCloud;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 102
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->c:Lflipboard/gui/firstrun/ScrollingTagCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->c(Lflipboard/gui/firstrun/ScrollingTagCloud;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 103
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->c:Lflipboard/gui/firstrun/ScrollingTagCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->c(Lflipboard/gui/firstrun/ScrollingTagCloud;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 104
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->c:Lflipboard/gui/firstrun/ScrollingTagCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->c(Lflipboard/gui/firstrun/ScrollingTagCloud;)Landroid/animation/ObjectAnimator;

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Landroid/animation/ObjectAnimator;->setFrameDelay(J)V

    .line 105
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->c:Lflipboard/gui/firstrun/ScrollingTagCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->c(Lflipboard/gui/firstrun/ScrollingTagCloud;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 107
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 108
    iget-object v1, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->a:Lflipboard/gui/firstrun/ScrollingLinearLayout;

    iget v1, v1, Lflipboard/gui/firstrun/ScrollingLinearLayout;->a:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 109
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 111
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->c:Lflipboard/gui/firstrun/ScrollingTagCloud;

    invoke-virtual {v0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 112
    const/high16 v1, 0x3f000000    # 0.5f

    const v2, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setGradientCenter(FF)V

    .line 113
    iget-object v1, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->a:Lflipboard/gui/firstrun/ScrollingLinearLayout;

    iget v1, v1, Lflipboard/gui/firstrun/ScrollingLinearLayout;->b:I

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x32

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setGradientRadius(F)V

    .line 114
    iget-object v1, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->a:Lflipboard/gui/firstrun/ScrollingLinearLayout;

    iget v1, v1, Lflipboard/gui/firstrun/ScrollingLinearLayout;->b:I

    iget-object v2, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->a:Lflipboard/gui/firstrun/ScrollingLinearLayout;

    iget v2, v2, Lflipboard/gui/firstrun/ScrollingLinearLayout;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setSize(II)V

    .line 115
    iget-object v1, p0, Lflipboard/gui/firstrun/ScrollingTagCloud$1;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 116
    return-void
.end method
