.class public Lflipboard/gui/firstrun/FirstRunView;
.super Lflipboard/gui/flipping/FlipTransitionViews;
.source "FirstRunView.java"


# instance fields
.field private final K:Lflipboard/gui/flipping/FlipTransitionViews$HopTask;

.field private volatile L:Z

.field private M:Landroid/widget/Button;

.field private N:Landroid/view/View;

.field private O:Lflipboard/service/RemoteWatchedFile$Observer;

.field private P:Lflipboard/service/RemoteWatchedFile;

.field private Q:Lflipboard/gui/flipping/FlippingContainer;

.field private R:Lflipboard/gui/firstrun/FirstRunView$PageType;

.field private final a:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-direct {p0, p1}, Lflipboard/gui/flipping/FlipTransitionViews;-><init>(Landroid/content/Context;)V

    .line 41
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    iput-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->a:Ljava/util/Timer;

    .line 45
    iput-boolean v1, p0, Lflipboard/gui/firstrun/FirstRunView;->L:Z

    .line 64
    iput-boolean v1, p0, Lflipboard/gui/firstrun/FirstRunView;->x:Z

    .line 66
    const v0, 0x7f030157

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lflipboard/gui/firstrun/FirstRunView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 67
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_0

    .line 68
    const v0, 0x7f0a0194

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 69
    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getPaintFlags()I

    move-result v2

    or-int/lit8 v2, v2, 0x8

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setPaintFlags(I)V

    .line 71
    :cond_0
    sget-boolean v0, Lflipboard/service/FlipboardManager;->n:Z

    if-eqz v0, :cond_1

    .line 72
    const v0, 0x7f0a0192

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 73
    const v2, 0x7f0200e8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 76
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->Q:Lflipboard/gui/flipping/FlippingContainer;

    .line 77
    iget-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->Q:Lflipboard/gui/flipping/FlippingContainer;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/gui/flipping/FlippingContainer;->k:Z

    .line 79
    new-instance v0, Lflipboard/gui/firstrun/FirstRunView$1;

    invoke-direct {v0, p0}, Lflipboard/gui/firstrun/FirstRunView$1;-><init>(Lflipboard/gui/firstrun/FirstRunView;)V

    iput-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->K:Lflipboard/gui/flipping/FlipTransitionViews$HopTask;

    .line 112
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_2

    .line 113
    iget-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->a:Ljava/util/Timer;

    iget-object v1, p0, Lflipboard/gui/firstrun/FirstRunView;->K:Lflipboard/gui/flipping/FlipTransitionViews$HopTask;

    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0x1388

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 116
    :cond_2
    invoke-virtual {p0}, Lflipboard/gui/firstrun/FirstRunView;->getCurrentPageType()Lflipboard/gui/firstrun/FirstRunView$PageType;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->R:Lflipboard/gui/firstrun/FirstRunView$PageType;

    .line 117
    return-void
.end method

.method static synthetic a(Lflipboard/gui/firstrun/FirstRunView;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lflipboard/gui/firstrun/FirstRunView;->L:Z

    return v0
.end method

.method static synthetic b(Lflipboard/gui/firstrun/FirstRunView;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lflipboard/gui/firstrun/FirstRunView;->u()V

    return-void
.end method

.method private u()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 286
    iget-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->N:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 287
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object v1, v0, Lflipboard/model/ConfigSetting;->TermsOfUseURLString:Ljava/lang/String;

    .line 288
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object v2, v0, Lflipboard/model/ConfigSetting;->PrivacyPolicyURLString:Ljava/lang/String;

    .line 289
    iget-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->N:Landroid/view/View;

    const v3, 0x7f0a0061

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 290
    invoke-virtual {v0, v5}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 291
    invoke-virtual {p0}, Lflipboard/gui/firstrun/FirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d00fd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 293
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/model/ConfigFirstLaunch;Ljava/util/Set;)V
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0xd
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/model/ConfigFirstLaunch;",
            "Ljava/util/Set",
            "<",
            "Lflipboard/model/FirstRunSection;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 122
    if-eqz p1, :cond_5

    .line 123
    const/4 v0, 0x0

    .line 124
    const/4 v1, 0x0

    .line 125
    iget-object v2, p1, Lflipboard/model/ConfigFirstLaunch;->SectionsToChooseFrom:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-double v2, v2

    const-wide/high16 v4, 0x4032000000000000L    # 18.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v6, v2

    .line 127
    iget-object v2, p1, Lflipboard/model/ConfigFirstLaunch;->SectionsToChooseFrom:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v0

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lflipboard/model/FirstRunSection;

    .line 128
    int-to-double v4, v2

    const-wide/high16 v8, 0x4032000000000000L    # 18.0

    rem-double/2addr v4, v8

    const-wide/16 v8, 0x0

    cmpl-double v0, v4, v8

    if-nez v0, :cond_0

    .line 129
    invoke-virtual {p0}, Lflipboard/gui/firstrun/FirstRunView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03006d

    const/4 v4, 0x0

    invoke-static {v0, v1, v4}, Lflipboard/gui/firstrun/FirstRunView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 130
    const v0, 0x7f0a018f

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflipboard/gui/firstrun/SelectCategoriesGrid;

    .line 132
    int-to-double v8, v2

    const-wide/high16 v10, 0x4032000000000000L    # 18.0

    div-double/2addr v8, v10

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    add-double/2addr v8, v10

    double-to-int v0, v8

    .line 133
    const v5, 0x7f0a0191

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 134
    if-ge v0, v6, :cond_3

    .line 135
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 146
    :goto_1
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v4}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    :cond_0
    move-object v4, v1

    .line 149
    add-int/lit8 v5, v2, 0x1

    .line 150
    invoke-interface {p2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    invoke-virtual {v4}, Lflipboard/gui/firstrun/SelectCategoriesGrid;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03006c

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lflipboard/gui/firstrun/SelectCategoriesGrid;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, v3, Lflipboard/model/FirstRunSection;->selectedImageName:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, v3, Lflipboard/model/FirstRunSection;->maskImageName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, v3, Lflipboard/model/FirstRunSection;->maskImageName:Ljava/lang/String;

    const-string v1, "mask"

    const-string v2, "active"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lflipboard/model/FirstRunSection;->selectedImageName:Ljava/lang/String;

    :cond_1
    invoke-virtual {v4}, Lflipboard/gui/firstrun/SelectCategoriesGrid;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FirstRunActivity;

    const v1, 0x7f0a018e

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLStaticTextView;

    iget-object v2, v3, Lflipboard/model/FirstRunSection;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v9}, Lflipboard/gui/firstrun/SelectCategoriesGrid;->addView(Landroid/view/View;)V

    iget-boolean v2, v4, Lflipboard/gui/firstrun/SelectCategoriesGrid;->a:Z

    if-eqz v2, :cond_2

    const v2, 0x7f0a018d

    invoke-virtual {v9, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_2
    invoke-static {v0, v9, v1, v3, v8}, Lflipboard/gui/firstrun/SelectCategoriesGrid;->a(Landroid/content/Context;Landroid/view/View;Lflipboard/gui/FLStaticTextView;Lflipboard/model/FirstRunSection;Z)V

    move-object v1, v4

    move v2, v5

    .line 151
    goto/16 :goto_0

    .line 137
    :cond_3
    const v0, 0x7f0a0190

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->M:Landroid/widget/Button;

    .line 138
    invoke-static {}, Lflipboard/abtest/testcase/ForcedAccountCreation;->a()Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    move-result-object v0

    .line 139
    if-eqz v0, :cond_4

    iget-boolean v0, v0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;->h:Z

    if-eqz v0, :cond_4

    .line 140
    iget-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->M:Landroid/widget/Button;

    const v5, 0x7f0d021c

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setText(I)V

    .line 142
    :cond_4
    iget-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->M:Landroid/widget/Button;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 143
    iput-object v4, p0, Lflipboard/gui/firstrun/FirstRunView;->N:Landroid/view/View;

    .line 144
    invoke-direct {p0}, Lflipboard/gui/firstrun/FirstRunView;->u()V

    goto/16 :goto_1

    .line 153
    :cond_5
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->M:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 266
    return-void
.end method

.method protected final d()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 169
    invoke-super {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->d()V

    .line 170
    invoke-virtual {p0}, Lflipboard/gui/firstrun/FirstRunView;->getCurrentPageType()Lflipboard/gui/firstrun/FirstRunView$PageType;

    move-result-object v0

    .line 171
    iget-object v1, p0, Lflipboard/gui/firstrun/FirstRunView;->R:Lflipboard/gui/firstrun/FirstRunView$PageType;

    if-eq v1, v0, :cond_2

    .line 172
    new-instance v1, Lflipboard/objs/UsageEventV2;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->d:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v3, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v1, v2, v3}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 173
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, p0, Lflipboard/gui/firstrun/FirstRunView;->R:Lflipboard/gui/firstrun/FirstRunView$PageType;

    invoke-virtual {v1, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 174
    iget-object v2, p0, Lflipboard/gui/firstrun/FirstRunView;->R:Lflipboard/gui/firstrun/FirstRunView$PageType;

    sget-object v3, Lflipboard/gui/firstrun/FirstRunView$PageType;->b:Lflipboard/gui/firstrun/FirstRunView$PageType;

    if-ne v2, v3, :cond_3

    .line 176
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->B:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 181
    :cond_0
    :goto_0
    invoke-virtual {v1}, Lflipboard/objs/UsageEventV2;->c()V

    .line 182
    new-instance v1, Lflipboard/objs/UsageEventV2;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->b:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v3, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v1, v2, v3}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 183
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v1, v2, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 184
    sget-object v2, Lflipboard/gui/firstrun/FirstRunView$PageType;->a:Lflipboard/gui/firstrun/FirstRunView$PageType;

    invoke-virtual {p0}, Lflipboard/gui/firstrun/FirstRunView;->getCurrentPageType()Lflipboard/gui/firstrun/FirstRunView$PageType;

    move-result-object v3

    if-ne v2, v3, :cond_1

    .line 186
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v3, "app_view_count"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 187
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "app_view_count"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 188
    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->E:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 190
    :cond_1
    invoke-virtual {v1}, Lflipboard/objs/UsageEventV2;->c()V

    .line 191
    iput-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->R:Lflipboard/gui/firstrun/FirstRunView$PageType;

    .line 193
    :cond_2
    return-void

    .line 177
    :cond_3
    iget-object v2, p0, Lflipboard/gui/firstrun/FirstRunView;->R:Lflipboard/gui/firstrun/FirstRunView$PageType;

    sget-object v3, Lflipboard/gui/firstrun/FirstRunView$PageType;->a:Lflipboard/gui/firstrun/FirstRunView$PageType;

    if-ne v2, v3, :cond_0

    .line 179
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->B:Lflipboard/objs/UsageEventV2$CommonEventData;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 259
    invoke-super {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->e()V

    .line 260
    iget-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->K:Lflipboard/gui/flipping/FlipTransitionViews$HopTask;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews$HopTask;->cancel()Z

    .line 261
    return-void
.end method

.method public final getCurrentPageType()Lflipboard/gui/firstrun/FirstRunView$PageType;
    .locals 1

    .prologue
    .line 305
    iget v0, p0, Lflipboard/gui/firstrun/FirstRunView;->h:I

    packed-switch v0, :pswitch_data_0

    .line 312
    sget-object v0, Lflipboard/gui/firstrun/FirstRunView$PageType;->b:Lflipboard/gui/firstrun/FirstRunView$PageType;

    .line 314
    :goto_0
    return-object v0

    .line 307
    :pswitch_0
    sget-object v0, Lflipboard/gui/firstrun/FirstRunView$PageType;->a:Lflipboard/gui/firstrun/FirstRunView$PageType;

    goto :goto_0

    .line 305
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 236
    invoke-super {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->onAttachedToWindow()V

    .line 237
    new-instance v0, Lflipboard/gui/firstrun/FirstRunView$2;

    invoke-direct {v0, p0}, Lflipboard/gui/firstrun/FirstRunView$2;-><init>(Lflipboard/gui/firstrun/FirstRunView;)V

    iput-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->O:Lflipboard/service/RemoteWatchedFile$Observer;

    .line 254
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "config.json"

    iget-object v2, p0, Lflipboard/gui/firstrun/FirstRunView;->O:Lflipboard/service/RemoteWatchedFile$Observer;

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/RemoteWatchedFile$Observer;)Lflipboard/service/RemoteWatchedFile;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->P:Lflipboard/service/RemoteWatchedFile;

    .line 255
    return-void
.end method

.method protected declared-synchronized onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 224
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->onDetachedFromWindow()V

    .line 225
    iget-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->K:Lflipboard/gui/flipping/FlipTransitionViews$HopTask;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews$HopTask;->cancel()Z

    .line 226
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/firstrun/FirstRunView;->L:Z

    .line 227
    iget-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->O:Lflipboard/service/RemoteWatchedFile$Observer;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->P:Lflipboard/service/RemoteWatchedFile;

    iget-object v1, p0, Lflipboard/gui/firstrun/FirstRunView;->O:Lflipboard/service/RemoteWatchedFile$Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/RemoteWatchedFile;->a(Lflipboard/service/RemoteWatchedFile$Observer;)V

    .line 229
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->O:Lflipboard/service/RemoteWatchedFile$Observer;

    .line 230
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->P:Lflipboard/service/RemoteWatchedFile;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    :cond_0
    monitor-exit p0

    return-void

    .line 224
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 198
    iget-boolean v0, p0, Lflipboard/gui/firstrun/FirstRunView;->L:Z

    if-nez v0, :cond_0

    .line 199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/firstrun/FirstRunView;->L:Z

    .line 200
    iget-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->K:Lflipboard/gui/flipping/FlipTransitionViews$HopTask;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews$HopTask;->cancel()Z

    .line 203
    iget-boolean v0, p0, Lflipboard/gui/firstrun/FirstRunView;->w:Z

    if-eqz v0, :cond_1

    .line 204
    iget v0, p0, Lflipboard/gui/firstrun/FirstRunView;->f:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v1, v0

    .line 205
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    .line 210
    :goto_0
    sub-float/2addr v0, v1

    neg-float v2, v1

    invoke-static {v0, v2, v1}, Lflipboard/util/JavaUtil;->a(FFF)F

    move-result v0

    .line 211
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_2

    .line 212
    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, p0, Lflipboard/gui/firstrun/FirstRunView;->o:F

    .line 216
    :goto_1
    iget v1, p0, Lflipboard/gui/firstrun/FirstRunView;->o:F

    sub-float/2addr v0, v1

    iput v0, p0, Lflipboard/gui/firstrun/FirstRunView;->p:F

    .line 218
    :cond_0
    invoke-super {p0, p1}, Lflipboard/gui/flipping/FlipTransitionViews;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 207
    :cond_1
    iget v0, p0, Lflipboard/gui/firstrun/FirstRunView;->g:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v1, v0

    .line 208
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    goto :goto_0

    .line 214
    :cond_2
    const/high16 v2, -0x40000000    # -2.0f

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iput v1, p0, Lflipboard/gui/firstrun/FirstRunView;->o:F

    goto :goto_1
.end method

.method public setCurrentViewIndex(I)V
    .locals 1

    .prologue
    .line 158
    invoke-super {p0, p1}, Lflipboard/gui/flipping/FlipTransitionViews;->setCurrentViewIndex(I)V

    .line 161
    iget-boolean v0, p0, Lflipboard/gui/firstrun/FirstRunView;->L:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    .line 162
    :cond_0
    iget-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->K:Lflipboard/gui/flipping/FlipTransitionViews$HopTask;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews$HopTask;->cancel()Z

    .line 164
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/firstrun/FirstRunView;->getCurrentPageType()Lflipboard/gui/firstrun/FirstRunView$PageType;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->R:Lflipboard/gui/firstrun/FirstRunView$PageType;

    .line 165
    return-void
.end method

.method public setTouched(Z)V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lflipboard/gui/firstrun/FirstRunView;->K:Lflipboard/gui/flipping/FlipTransitionViews$HopTask;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews$HopTask;->a()V

    .line 281
    iput-boolean p1, p0, Lflipboard/gui/firstrun/FirstRunView;->L:Z

    .line 282
    return-void
.end method
