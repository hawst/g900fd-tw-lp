.class public Lflipboard/gui/firstrun/SelectCategoriesGrid;
.super Landroid/view/ViewGroup;
.source "SelectCategoriesGrid.java"


# static fields
.field private static e:Landroid/graphics/ColorMatrixColorFilter;


# instance fields
.field a:Z

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xd
    .end annotation

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x3

    const/high16 v3, 0x3f800000    # 1.0f

    .line 32
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 34
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 35
    new-instance v1, Landroid/graphics/ColorMatrix;

    invoke-direct {v1}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 36
    const v2, 0x3ecccccd    # 0.4f

    invoke-virtual {v1, v3, v3, v3, v2}, Landroid/graphics/ColorMatrix;->setScale(FFFF)V

    .line 37
    invoke-virtual {v0, v1}, Landroid/graphics/ColorMatrix;->postConcat(Landroid/graphics/ColorMatrix;)V

    .line 38
    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v1, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    sput-object v1, Lflipboard/gui/firstrun/SelectCategoriesGrid;->e:Landroid/graphics/ColorMatrixColorFilter;

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->a:Z

    .line 40
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x258

    if-ge v0, v1, :cond_0

    .line 41
    iput v5, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->c:I

    .line 42
    iput v4, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->b:I

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->a:Z

    .line 51
    :goto_0
    return-void

    .line 44
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 45
    iput v4, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->c:I

    .line 46
    iput v5, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->b:I

    goto :goto_0

    .line 48
    :cond_1
    const/4 v0, 0x5

    iput v0, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->c:I

    .line 49
    const/4 v0, 0x4

    iput v0, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->b:I

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;Lflipboard/gui/FLStaticTextView;Lflipboard/model/FirstRunSection;Z)V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/16 v6, 0x10

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 78
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 79
    const v0, 0x7f0a018d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 80
    if-eqz p4, :cond_2

    .line 81
    const v3, 0x7f020073

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 82
    const v0, 0x7f080089

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p2, v0}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    .line 88
    :goto_0
    const v0, 0x7f0a018b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 90
    iget-object v1, p3, Lflipboard/model/FirstRunSection;->selectedImageName:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 91
    iget-object v1, p3, Lflipboard/model/FirstRunSection;->selectedImageName:Ljava/lang/String;

    const-string v3, "-"

    const-string v4, "_"

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p3, Lflipboard/model/FirstRunSection;->selectedImageName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 94
    :goto_1
    if-eqz v1, :cond_1

    .line 95
    const v3, 0x7f0a018c

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 96
    if-eqz p4, :cond_3

    .line 97
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-gt v4, v6, :cond_0

    .line 98
    invoke-virtual {v0, v5, v2}, Landroid/widget/ImageView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 100
    :cond_0
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 101
    const v2, 0x7f02010f

    invoke-virtual {v3, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 109
    :goto_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "drawable"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 111
    :cond_1
    return-void

    .line 84
    :cond_2
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 85
    const v0, 0x7f080008

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p2, v0}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    goto :goto_0

    .line 103
    :cond_3
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-gt v4, v6, :cond_4

    .line 104
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v2}, Landroid/widget/ImageView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 106
    :cond_4
    sget-object v2, Lflipboard/gui/firstrun/SelectCategoriesGrid;->e:Landroid/graphics/ColorMatrixColorFilter;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 107
    const v2, 0x7f020110

    invoke-virtual {v3, v2}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2

    :cond_5
    move-object v1, v2

    goto :goto_1
.end method


# virtual methods
.method public onLayout(ZIIII)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 137
    invoke-virtual {p0}, Lflipboard/gui/firstrun/SelectCategoriesGrid;->getChildCount()I

    move-result v1

    .line 138
    sub-int v2, p4, p2

    .line 139
    sub-int v3, p5, p3

    .line 140
    iget v4, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->b:I

    invoke-virtual {p0, v0}, Lflipboard/gui/firstrun/SelectCategoriesGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    mul-int/2addr v4, v5

    sub-int/2addr v2, v4

    iget v4, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->b:I

    add-int/lit8 v4, v4, -0x1

    iget v5, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->d:I

    mul-int/2addr v4, v5

    sub-int/2addr v2, v4

    div-int/lit8 v2, v2, 0x2

    .line 141
    iget v4, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->c:I

    invoke-virtual {p0, v0}, Lflipboard/gui/firstrun/SelectCategoriesGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    mul-int/2addr v4, v5

    sub-int/2addr v3, v4

    iget v4, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->c:I

    add-int/lit8 v4, v4, -0x1

    iget v5, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->d:I

    mul-int/2addr v4, v5

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    .line 142
    :goto_0
    if-ge v0, v1, :cond_0

    .line 143
    iget v4, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->b:I

    rem-int v4, v0, v4

    .line 144
    iget v5, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->b:I

    div-int v5, v0, v5

    .line 145
    invoke-virtual {p0, v0}, Lflipboard/gui/firstrun/SelectCategoriesGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    iget v7, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->d:I

    add-int/2addr v6, v7

    mul-int/2addr v4, v6

    add-int/2addr v4, v2

    .line 146
    invoke-virtual {p0, v0}, Lflipboard/gui/firstrun/SelectCategoriesGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    iget v7, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->d:I

    add-int/2addr v6, v7

    mul-int/2addr v5, v6

    add-int/2addr v5, v3

    .line 147
    invoke-virtual {p0, v0}, Lflipboard/gui/firstrun/SelectCategoriesGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v4

    .line 148
    invoke-virtual {p0, v0}, Lflipboard/gui/firstrun/SelectCategoriesGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v5

    .line 149
    invoke-virtual {p0, v0}, Lflipboard/gui/firstrun/SelectCategoriesGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v4, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    .line 142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 151
    :cond_0
    return-void
.end method

.method public onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 116
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 118
    invoke-virtual {p0}, Lflipboard/gui/firstrun/SelectCategoriesGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09002b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->d:I

    .line 119
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 120
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 122
    invoke-virtual {p0}, Lflipboard/gui/firstrun/SelectCategoriesGrid;->getChildCount()I

    move-result v2

    .line 123
    iget v3, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->d:I

    iget v4, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->b:I

    add-int/lit8 v4, v4, -0x1

    mul-int/2addr v3, v4

    sub-int/2addr v0, v3

    iget v3, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->b:I

    div-int/2addr v0, v3

    .line 124
    iget v3, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->d:I

    iget v4, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->c:I

    add-int/lit8 v4, v4, -0x1

    mul-int/2addr v3, v4

    sub-int/2addr v1, v3

    iget v3, p0, Lflipboard/gui/firstrun/SelectCategoriesGrid;->c:I

    div-int/2addr v1, v3

    .line 127
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 128
    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 129
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 130
    invoke-virtual {p0, v0}, Lflipboard/gui/firstrun/SelectCategoriesGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v3, v1}, Landroid/view/View;->measure(II)V

    .line 129
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 132
    :cond_0
    return-void
.end method
