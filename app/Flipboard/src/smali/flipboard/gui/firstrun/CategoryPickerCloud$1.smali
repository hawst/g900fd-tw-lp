.class public Lflipboard/gui/firstrun/CategoryPickerCloud$1;
.super Ljava/lang/Object;
.source "CategoryPickerCloud.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/FLSearchBar;

.field final synthetic b:Lflipboard/gui/firstrun/CategoryPickerCloud;


# direct methods
.method public constructor <init>(Lflipboard/gui/firstrun/CategoryPickerCloud;Lflipboard/gui/FLSearchBar;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$1;->b:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iput-object p2, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$1;->a:Lflipboard/gui/FLSearchBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 211
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$1;->a:Lflipboard/gui/FLSearchBar;

    if-eqz v0, :cond_0

    .line 213
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 214
    iget-object v1, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$1;->a:Lflipboard/gui/FLSearchBar;

    invoke-virtual {v1}, Lflipboard/gui/FLSearchBar;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 215
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$1;->a:Lflipboard/gui/FLSearchBar;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lflipboard/gui/FLSearchBar;->setText(Ljava/lang/CharSequence;)V

    .line 219
    :cond_0
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$1;->b:Lflipboard/gui/firstrun/CategoryPickerCloud;

    iget-object v1, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$1;->b:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-virtual {v1}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getSelectedTopics()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Ljava/util/Map;)V

    .line 221
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 222
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$1;->b:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 223
    iget-object v0, p0, Lflipboard/gui/firstrun/CategoryPickerCloud$1;->b:Lflipboard/gui/firstrun/CategoryPickerCloud;

    invoke-static {v0}, Lflipboard/gui/firstrun/CategoryPickerCloud;->b(Lflipboard/gui/firstrun/CategoryPickerCloud;)Lflipboard/gui/FLTextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 224
    return-void
.end method
