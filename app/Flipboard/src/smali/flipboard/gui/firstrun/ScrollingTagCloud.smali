.class public Lflipboard/gui/firstrun/ScrollingTagCloud;
.super Landroid/widget/FrameLayout;
.source "ScrollingTagCloud.java"


# instance fields
.field private a:Z

.field private b:I

.field private c:Landroid/animation/ObjectAnimator;

.field private d:Landroid/animation/ObjectAnimator;

.field private e:Landroid/animation/ObjectAnimator;

.field private f:Landroid/animation/ObjectAnimator;

.field private g:I

.field private h:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->a:Z

    .line 53
    invoke-virtual {p0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030157

    invoke-static {v0, v1, p0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->a:Z

    .line 58
    return-void
.end method

.method static synthetic a(Lflipboard/gui/firstrun/ScrollingTagCloud;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->b:I

    return v0
.end method

.method static synthetic a(Lflipboard/gui/firstrun/ScrollingTagCloud;I)I
    .locals 0

    .prologue
    .line 34
    iput p1, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->g:I

    return p1
.end method

.method private static a(Landroid/widget/ImageView;J)Landroid/animation/ObjectAnimator;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 122
    const v0, 0x3dcccccd    # 0.1f

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 124
    const-string v0, "alpha"

    new-array v1, v4, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 125
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 126
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 127
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 128
    invoke-virtual {v0, v4}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 129
    invoke-virtual {v0, p1, p2}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 130
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 132
    return-object v0

    .line 124
    nop

    :array_0
    .array-data 4
        0x3dcccccd    # 0.1f
        0x3f333333    # 0.7f
    .end array-data
.end method

.method static synthetic a(Lflipboard/gui/firstrun/ScrollingTagCloud;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->c:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method private static a(Landroid/animation/ObjectAnimator;)V
    .locals 0

    .prologue
    .line 227
    if-eqz p0, :cond_0

    .line 228
    invoke-virtual {p0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 230
    :cond_0
    return-void
.end method

.method static synthetic a(Lflipboard/gui/firstrun/ScrollingTagCloud;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;I)V
    .locals 11

    .prologue
    const/4 v10, -0x2

    const/4 v4, 0x0

    .line 34
    mul-int/lit8 v7, p3, 0x2

    move v2, v4

    :goto_0
    if-ge v2, v7, :cond_4

    move v6, v4

    :goto_1
    const/4 v0, 0x6

    if-ge v6, v0, :cond_2

    const v0, 0x7f03014b

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    move v3, v4

    move v5, v6

    :goto_2
    const/4 v1, 0x5

    if-ge v3, v1, :cond_0

    const v1, 0x7f03014a

    invoke-virtual {p2, v1, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    rem-int/lit8 v5, v5, 0x6

    mul-int/lit8 v8, v5, 0x5

    add-int/2addr v8, v3

    iget-object v9, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->h:[Ljava/lang/String;

    aget-object v8, v9, v8

    invoke-virtual {v1, v8}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_0
    iget-boolean v1, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->a:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->a:Z

    invoke-virtual {v0, v10, v10}, Landroid/widget/LinearLayout;->measure(II)V

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v1

    iput v1, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->b:I

    :cond_1
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->b:I

    add-int/2addr v0, v2

    if-ge v0, v7, :cond_3

    add-int/lit8 v6, v6, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v2

    :cond_3
    move v2, v0

    goto :goto_0

    :cond_4
    return-void
.end method

.method static synthetic b(Lflipboard/gui/firstrun/ScrollingTagCloud;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->g:I

    return v0
.end method

.method static synthetic c(Lflipboard/gui/firstrun/ScrollingTagCloud;)Landroid/animation/ObjectAnimator;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->c:Landroid/animation/ObjectAnimator;

    return-object v0
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 219
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 220
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->c:Landroid/animation/ObjectAnimator;

    invoke-static {v0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->a(Landroid/animation/ObjectAnimator;)V

    .line 221
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->d:Landroid/animation/ObjectAnimator;

    invoke-static {v0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->a(Landroid/animation/ObjectAnimator;)V

    .line 222
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->e:Landroid/animation/ObjectAnimator;

    invoke-static {v0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->a(Landroid/animation/ObjectAnimator;)V

    .line 223
    iget-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->f:Landroid/animation/ObjectAnimator;

    invoke-static {v0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->a(Landroid/animation/ObjectAnimator;)V

    .line 224
    return-void
.end method

.method protected onFinishInflate()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 62
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 64
    sget-boolean v0, Lflipboard/service/FlipboardManager;->n:Z

    if-eqz v0, :cond_0

    .line 65
    const v0, 0x7f0a0192

    invoke-virtual {p0, v0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 66
    const v1, 0x7f0200e8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 69
    :cond_0
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v1, 0x7f0d032d

    invoke-virtual {v0, v1}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->h:[Ljava/lang/String;

    .line 71
    const v0, 0x7f0a0196

    invoke-virtual {p0, v0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/firstrun/ScrollingLinearLayout;

    .line 73
    const v1, 0x7f0a0197

    invoke-virtual {p0, v1}, Lflipboard/gui/firstrun/ScrollingTagCloud;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 75
    const v2, 0x7f0a0199

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLTextView;

    .line 76
    invoke-virtual {p0}, Lflipboard/gui/firstrun/ScrollingTagCloud;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v5, 0x7f0d0110

    invoke-virtual {v4, v5}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v6, 0x7f0d00ff

    invoke-virtual {v5, v6}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v5, v6, v10

    invoke-static {v4, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v6, Landroid/text/SpannableString;

    invoke-direct {v6, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v7, Lflipboard/gui/firstrun/ScrollingTagCloud$2;

    invoke-direct {v7, p0, v3}, Lflipboard/gui/firstrun/ScrollingTagCloud$2;-><init>(Lflipboard/gui/firstrun/ScrollingTagCloud;Landroid/content/Context;)V

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v3, v4

    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    sget-object v8, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v8}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080086

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-direct {v5, v8}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int v8, v3, v4

    invoke-virtual {v6, v5, v3, v8, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    add-int/2addr v4, v3

    invoke-virtual {v6, v7, v3, v4, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflipboard/gui/FLTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    sget-object v3, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v2, v6, v3}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 77
    invoke-static {v1}, Lflipboard/activities/FirstRunActivity;->setTOSLinks(Landroid/view/View;)V

    .line 79
    const v2, 0x7f0a019b

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 80
    const v3, 0x7f0a019c

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 81
    const v4, 0x7f0a019d

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 84
    const-wide/16 v6, 0x708

    invoke-static {v2, v6, v7}, Lflipboard/gui/firstrun/ScrollingTagCloud;->a(Landroid/widget/ImageView;J)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->d:Landroid/animation/ObjectAnimator;

    .line 85
    const-wide/16 v6, 0x5dc

    invoke-static {v3, v6, v7}, Lflipboard/gui/firstrun/ScrollingTagCloud;->a(Landroid/widget/ImageView;J)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->e:Landroid/animation/ObjectAnimator;

    .line 86
    const-wide/16 v2, 0x4b0

    invoke-static {v4, v2, v3}, Lflipboard/gui/firstrun/ScrollingTagCloud;->a(Landroid/widget/ImageView;J)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lflipboard/gui/firstrun/ScrollingTagCloud;->f:Landroid/animation/ObjectAnimator;

    .line 88
    invoke-virtual {v0}, Lflipboard/gui/firstrun/ScrollingLinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    .line 89
    new-instance v3, Lflipboard/gui/firstrun/ScrollingTagCloud$1;

    invoke-direct {v3, p0, v0, v1}, Lflipboard/gui/firstrun/ScrollingTagCloud$1;-><init>(Lflipboard/gui/firstrun/ScrollingTagCloud;Lflipboard/gui/firstrun/ScrollingLinearLayout;Landroid/view/ViewGroup;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 118
    return-void
.end method
