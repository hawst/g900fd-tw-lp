.class final enum Lflipboard/gui/FLSearchView$Input;
.super Ljava/lang/Enum;
.source "FLSearchView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/FLSearchView$Input;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/FLSearchView$Input;

.field public static final enum b:Lflipboard/gui/FLSearchView$Input;

.field public static final enum c:Lflipboard/gui/FLSearchView$Input;

.field public static final enum d:Lflipboard/gui/FLSearchView$Input;

.field public static final enum e:Lflipboard/gui/FLSearchView$Input;

.field public static final enum f:Lflipboard/gui/FLSearchView$Input;

.field public static final enum g:Lflipboard/gui/FLSearchView$Input;

.field private static final synthetic h:[Lflipboard/gui/FLSearchView$Input;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 94
    new-instance v0, Lflipboard/gui/FLSearchView$Input;

    const-string v1, "ENTER_PRESSED"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/FLSearchView$Input;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchView$Input;->a:Lflipboard/gui/FLSearchView$Input;

    new-instance v0, Lflipboard/gui/FLSearchView$Input;

    const-string v1, "TEXT_CHANGED"

    invoke-direct {v0, v1, v4}, Lflipboard/gui/FLSearchView$Input;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchView$Input;->b:Lflipboard/gui/FLSearchView$Input;

    new-instance v0, Lflipboard/gui/FLSearchView$Input;

    const-string v1, "DELAY_TIMER_FIRED"

    invoke-direct {v0, v1, v5}, Lflipboard/gui/FLSearchView$Input;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchView$Input;->c:Lflipboard/gui/FLSearchView$Input;

    new-instance v0, Lflipboard/gui/FLSearchView$Input;

    const-string v1, "SEARCH_ONE_RESULT"

    invoke-direct {v0, v1, v6}, Lflipboard/gui/FLSearchView$Input;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchView$Input;->d:Lflipboard/gui/FLSearchView$Input;

    new-instance v0, Lflipboard/gui/FLSearchView$Input;

    const-string v1, "SEARCH_RESULTS"

    invoke-direct {v0, v1, v7}, Lflipboard/gui/FLSearchView$Input;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchView$Input;->e:Lflipboard/gui/FLSearchView$Input;

    new-instance v0, Lflipboard/gui/FLSearchView$Input;

    const-string v1, "SEARCH_COMPLETE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLSearchView$Input;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchView$Input;->f:Lflipboard/gui/FLSearchView$Input;

    new-instance v0, Lflipboard/gui/FLSearchView$Input;

    const-string v1, "LOCAL_SEARCH_COMPLETE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLSearchView$Input;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchView$Input;->g:Lflipboard/gui/FLSearchView$Input;

    .line 93
    const/4 v0, 0x7

    new-array v0, v0, [Lflipboard/gui/FLSearchView$Input;

    sget-object v1, Lflipboard/gui/FLSearchView$Input;->a:Lflipboard/gui/FLSearchView$Input;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/gui/FLSearchView$Input;->b:Lflipboard/gui/FLSearchView$Input;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/gui/FLSearchView$Input;->c:Lflipboard/gui/FLSearchView$Input;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/gui/FLSearchView$Input;->d:Lflipboard/gui/FLSearchView$Input;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/gui/FLSearchView$Input;->e:Lflipboard/gui/FLSearchView$Input;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/gui/FLSearchView$Input;->f:Lflipboard/gui/FLSearchView$Input;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lflipboard/gui/FLSearchView$Input;->g:Lflipboard/gui/FLSearchView$Input;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/gui/FLSearchView$Input;->h:[Lflipboard/gui/FLSearchView$Input;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 93
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/FLSearchView$Input;
    .locals 1

    .prologue
    .line 93
    const-class v0, Lflipboard/gui/FLSearchView$Input;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLSearchView$Input;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/FLSearchView$Input;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lflipboard/gui/FLSearchView$Input;->h:[Lflipboard/gui/FLSearchView$Input;

    invoke-virtual {v0}, [Lflipboard/gui/FLSearchView$Input;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/FLSearchView$Input;

    return-object v0
.end method
