.class public Lflipboard/gui/FLStaticTextView;
.super Landroid/view/View;
.source "FLStaticTextView.java"

# interfaces
.implements Lflipboard/gui/FLTextIntf;
.implements Lflipboard/gui/FLViewIntf;


# static fields
.field static final a:Lflipboard/util/Log;

.field private static final b:Lflipboard/util/Log;

.field private static final s:[F


# instance fields
.field private A:Z

.field private B:Z

.field private C:Lflipboard/gui/FLStaticTextView$BlockType;

.field private D:Z

.field private E:I

.field private F:J

.field private final c:Landroid/graphics/Paint;

.field private d:I

.field private e:F

.field private f:F

.field private g:F

.field private h:Ljava/lang/CharSequence;

.field private i:I

.field private j:I

.field private k:I

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private p:I

.field private q:I

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/FLStaticTextView$Segment;",
            ">;"
        }
    .end annotation
.end field

.field private t:[C

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:I

.field private z:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-string v0, "text"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/FLStaticTextView;->b:Lflipboard/util/Log;

    .line 77
    const/16 v0, 0x2000

    new-array v0, v0, [F

    sput-object v0, Lflipboard/gui/FLStaticTextView;->s:[F

    .line 872
    const-string v0, "measure"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/FLStaticTextView;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/FLStaticTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 100
    const v0, 0x7f0e0015

    invoke-direct {p0, p1, p2, v0}, Lflipboard/gui/FLStaticTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 101
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 17
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 104
    invoke-direct/range {p0 .. p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 84
    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iput v2, v0, Lflipboard/gui/FLStaticTextView;->z:F

    .line 106
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v12

    .line 111
    new-instance v2, Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    .line 116
    const/16 v11, 0xe

    .line 117
    const/high16 v10, -0x1000000

    .line 118
    const/4 v9, 0x0

    .line 119
    const/4 v8, 0x0

    .line 120
    const/4 v7, 0x0

    .line 121
    const/4 v6, 0x0

    .line 122
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    .line 123
    const-string v2, ""

    move-object/from16 v0, p0

    iput-object v2, v0, Lflipboard/gui/FLStaticTextView;->h:Ljava/lang/CharSequence;

    .line 125
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    invoke-static {v2}, Lflipboard/util/AndroidUtil;->a(Landroid/graphics/Paint;)V

    .line 130
    if-eqz p3, :cond_1

    const v2, 0x7f0e0015

    .line 131
    :goto_0
    if-eqz p3, :cond_2

    .line 132
    :goto_1
    sget-object v13, Lflipboard/app/R$styleable;->FLStaticTextView:[I

    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v12, v0, v13, v2, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v12

    .line 134
    invoke-virtual {v12}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v13

    .line 135
    const/4 v2, 0x0

    move/from16 v16, v2

    move v2, v3

    move v3, v4

    move v4, v5

    move v5, v6

    move-object v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move v10, v11

    move/from16 v11, v16

    :goto_2
    if-ge v11, v13, :cond_3

    .line 136
    invoke-virtual {v12, v11}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v14

    .line 137
    packed-switch v14, :pswitch_data_0

    .line 135
    :cond_0
    :goto_3
    :pswitch_0
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_1
    move/from16 v2, p3

    .line 130
    goto :goto_0

    .line 131
    :cond_2
    const p3, 0x7f0e0015

    goto :goto_1

    .line 139
    :pswitch_1
    const/4 v8, 0x0

    invoke-virtual {v12, v14, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v8

    goto :goto_3

    .line 143
    :pswitch_2
    const/4 v7, 0x0

    invoke-virtual {v12, v14, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v7

    goto :goto_3

    .line 147
    :pswitch_3
    invoke-virtual {v12, v14}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lflipboard/gui/FLStaticTextView;->h:Ljava/lang/CharSequence;

    goto :goto_3

    .line 151
    :pswitch_4
    invoke-virtual {v12, v14, v10}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v10

    goto :goto_3

    .line 155
    :pswitch_5
    const/16 v6, 0x14

    invoke-virtual {v12, v6}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    .line 159
    :pswitch_6
    const/16 v14, 0x12

    const/4 v15, 0x0

    invoke-virtual {v12, v14, v15}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lflipboard/gui/FLStaticTextView;->y:I

    goto :goto_3

    .line 163
    :pswitch_7
    const/16 v14, 0x13

    invoke-virtual {v12, v14}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 164
    const-string v15, "full"

    invoke-virtual {v15, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 165
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lflipboard/gui/FLStaticTextView;->B:Z

    goto :goto_3

    .line 171
    :pswitch_8
    const/high16 v15, 0x3f800000    # 1.0f

    invoke-virtual {v12, v14, v15}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lflipboard/gui/FLStaticTextView;->z:F

    goto :goto_3

    .line 175
    :pswitch_9
    const/4 v9, 0x0

    invoke-virtual {v12, v14, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v9

    goto :goto_3

    .line 179
    :pswitch_a
    const/4 v5, 0x0

    invoke-virtual {v12, v14, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v5

    goto :goto_3

    .line 182
    :pswitch_b
    const/4 v4, 0x0

    invoke-virtual {v12, v14, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    goto :goto_3

    .line 185
    :pswitch_c
    const/4 v3, 0x0

    invoke-virtual {v12, v14, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    goto :goto_3

    .line 188
    :pswitch_d
    const/4 v2, 0x0

    invoke-virtual {v12, v14, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    goto :goto_3

    .line 191
    :pswitch_e
    const/16 v14, 0x11

    const/4 v15, 0x0

    invoke-virtual {v12, v14, v15}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v14

    packed-switch v14, :pswitch_data_1

    goto :goto_3

    .line 193
    :pswitch_f
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lflipboard/gui/FLStaticTextView;->setRtlAlignment(Z)V

    goto/16 :goto_3

    .line 196
    :pswitch_10
    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lflipboard/gui/FLStaticTextView;->setRtlAlignment(Z)V

    goto/16 :goto_3

    .line 199
    :pswitch_11
    invoke-static {}, Lflipboard/util/AndroidUtil;->j()Z

    move-result v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lflipboard/gui/FLStaticTextView;->setRtlAlignment(Z)V

    goto/16 :goto_3

    .line 205
    :cond_3
    invoke-virtual {v12}, Landroid/content/res/TypedArray;->recycle()V

    .line 214
    if-nez v6, :cond_5

    .line 215
    const-string v6, "normal"

    .line 216
    if-eqz p2, :cond_5

    .line 217
    const-string v11, "http://schemas.android.com/apk/res/android"

    const-string v12, "textStyle"

    move-object/from16 v0, p2

    invoke-interface {v0, v11, v12}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 218
    if-eqz v11, :cond_5

    const-string v12, "0x1"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_4

    const-string v12, "0x3"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 219
    :cond_4
    const-string v6, "bold"

    .line 227
    :cond_5
    move-object/from16 v0, p0

    iput v8, v0, Lflipboard/gui/FLStaticTextView;->j:I

    .line 228
    move-object/from16 v0, p0

    iput v7, v0, Lflipboard/gui/FLStaticTextView;->k:I

    .line 229
    move-object/from16 v0, p0

    iput v10, v0, Lflipboard/gui/FLStaticTextView;->d:I

    .line 232
    move-object/from16 v0, p0

    iget-object v7, v0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    sget-object v8, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v8, v6}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 233
    move-object/from16 v0, p0

    iget-object v6, v0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    int-to-float v7, v10

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 235
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    .line 236
    if-eqz v5, :cond_6

    .line 237
    move-object/from16 v0, p0

    iget-object v6, v0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    invoke-virtual {v6, v2, v4, v3, v5}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 239
    :cond_6
    return-void

    .line 137
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_8
        :pswitch_e
        :pswitch_6
        :pswitch_7
        :pswitch_5
    .end packed-switch

    .line 191
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method private a([CIIF)F
    .locals 2

    .prologue
    .line 1032
    iget-boolean v0, p0, Lflipboard/gui/FLStaticTextView;->A:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 1033
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, p3}, Landroid/graphics/Paint;->measureText([CII)F

    move-result v0

    float-to-int v0, v0

    int-to-float p4, v0

    .line 1035
    :cond_0
    return p4
.end method

.method static a(Ljava/lang/CharSequence;I[CIZ)I
    .locals 11

    .prologue
    const/16 v6, 0x20

    const/16 v5, 0xa

    const/4 v2, 0x0

    .line 520
    invoke-static {p0, v2, p1, p2, v2}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    move v4, v2

    move v0, v2

    move v1, v2

    .line 525
    :goto_0
    if-ge v4, p1, :cond_a

    .line 526
    invoke-static {p2, v4, p1}, Ljava/lang/Character;->codePointAt([CII)I

    move-result v3

    .line 527
    invoke-static {v3}, Ljava/lang/Character;->charCount(I)I

    move-result v7

    add-int/2addr v7, v4

    .line 529
    const/16 v4, 0xd

    if-ne v3, v4, :cond_4

    .line 530
    if-ge v7, p1, :cond_0

    aget-char v3, p2, v7

    if-eq v3, v5, :cond_8

    :cond_0
    move v3, v5

    .line 554
    :cond_1
    :goto_1
    if-ne v3, v5, :cond_3

    .line 557
    add-int/lit8 v0, v0, 0x1

    .line 558
    if-lez p3, :cond_2

    if-ge v0, p3, :cond_a

    .line 559
    :cond_2
    if-lez v1, :cond_3

    add-int/lit8 v4, v1, -0x1

    aget-char v4, p2, v4

    if-ne v4, v6, :cond_3

    .line 563
    add-int/lit8 v1, v1, -0x1

    .line 568
    :cond_3
    invoke-static {v3}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v8

    .line 569
    array-length v9, v8

    move v3, v1

    move v1, v2

    :goto_2
    if-ge v1, v9, :cond_9

    aget-char v10, v8, v1

    .line 570
    add-int/lit8 v4, v3, 0x1

    aput-char v10, p2, v3

    .line 569
    add-int/lit8 v1, v1, 0x1

    move v3, v4

    goto :goto_2

    .line 534
    :cond_4
    if-eq v3, v5, :cond_1

    .line 536
    if-le v3, v6, :cond_5

    const/16 v4, 0x7f

    if-lt v3, v4, :cond_1

    .line 538
    :cond_5
    const/16 v4, 0xad

    if-ne v3, v4, :cond_6

    .line 539
    if-nez p4, :cond_1

    move v4, v7

    .line 540
    goto :goto_0

    .line 542
    :cond_6
    const/16 v4, 0x200b

    if-eq v3, v4, :cond_1

    .line 544
    const/16 v4, 0xa0

    if-eq v3, v4, :cond_1

    .line 546
    invoke-static {v3}, Ljava/lang/Character;->isSpaceChar(I)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 547
    if-eqz v1, :cond_8

    add-int/lit8 v3, v1, -0x1

    aget-char v3, p2, v3

    invoke-static {v3}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v3

    if-nez v3, :cond_8

    move v3, v6

    .line 548
    goto :goto_1

    .line 551
    :cond_7
    invoke-static {v3}, Ljava/lang/Character;->isISOControl(I)Z

    move-result v4

    if-nez v4, :cond_8

    .line 552
    invoke-static {v3}, Ljava/lang/Character;->getType(I)I

    move-result v4

    const/16 v8, 0x12

    if-ne v4, v8, :cond_1

    :cond_8
    move v4, v7

    goto :goto_0

    :cond_9
    move v4, v7

    move v1, v3

    .line 572
    goto :goto_0

    .line 575
    :cond_a
    :goto_3
    if-lez v1, :cond_b

    add-int/lit8 v0, v1, -0x1

    aget-char v0, p2, v0

    invoke-static {v0}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v0

    if-eqz v0, :cond_b

    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    .line 577
    :cond_b
    return v1
.end method

.method private a(I)V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 998
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    if-eqz v0, :cond_5

    .line 999
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView$Segment;

    .line 1000
    iget v2, v0, Lflipboard/gui/FLStaticTextView$Segment;->e:F

    iget v3, v0, Lflipboard/gui/FLStaticTextView$Segment;->g:F

    add-float/2addr v2, v3

    int-to-float v3, p1

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 1001
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "invalid line width: %f > %f, %s"

    new-array v4, v10, [Ljava/lang/Object;

    iget v5, v0, Lflipboard/gui/FLStaticTextView$Segment;->e:F

    iget v6, v0, Lflipboard/gui/FLStaticTextView$Segment;->g:F

    add-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    aput-object v0, v4, v9

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1003
    :cond_1
    iget v2, v0, Lflipboard/gui/FLStaticTextView$Segment;->e:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    .line 1004
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "invalid x position: %s"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v0, v4, v7

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1006
    :cond_2
    iget v2, v0, Lflipboard/gui/FLStaticTextView$Segment;->g:F

    iget-object v3, p0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView$Segment;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    .line 1007
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "invalid string width: %f != %f, %s"

    new-array v4, v10, [Ljava/lang/Object;

    iget v5, v0, Lflipboard/gui/FLStaticTextView$Segment;->g:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v7

    iget-object v5, p0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView$Segment;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v8

    aput-object v0, v4, v9

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1010
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1011
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView$Segment;

    .line 1012
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_4

    .line 1013
    const-string v3, "|"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1015
    :cond_4
    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView$Segment;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1018
    :cond_5
    return-void
.end method

.method static synthetic a(Lflipboard/gui/FLStaticTextView;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lflipboard/gui/FLStaticTextView;->A:Z

    return v0
.end method

.method private b(II)V
    .locals 34

    .prologue
    .line 582
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->p:I

    move/from16 v0, p1

    if-ne v3, v0, :cond_1

    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->q:I

    move/from16 v0, p2

    if-ne v3, v0, :cond_1

    .line 866
    :cond_0
    :goto_0
    return-void

    .line 586
    :cond_1
    sget-object v3, Lflipboard/gui/FLStaticTextView;->b:Lflipboard/util/Log;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lflipboard/gui/FLStaticTextView;->p:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget v5, v0, Lflipboard/gui/FLStaticTextView;->q:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 588
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v26

    .line 589
    move-object/from16 v0, v26

    iget v3, v0, Landroid/graphics/Paint$FontMetrics;->bottom:F

    move-object/from16 v0, v26

    iget v4, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->m:F

    .line 590
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->m:F

    move-object/from16 v0, p0

    iget v4, v0, Lflipboard/gui/FLStaticTextView;->z:F

    mul-float/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->n:F

    .line 592
    move-object/from16 v0, v26

    iget v3, v0, Landroid/graphics/Paint$FontMetrics;->leading:F

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->o:F

    .line 594
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lflipboard/gui/FLStaticTextView;->p:I

    .line 595
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lflipboard/gui/FLStaticTextView;->q:I

    .line 597
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    .line 598
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->i:I

    .line 599
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->l:F

    .line 601
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/FLStaticTextView;->getPaddingLeft()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/FLStaticTextView;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    sub-int v25, p1, v3

    .line 602
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/FLStaticTextView;->getPaddingTop()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/FLStaticTextView;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    sub-int v27, p2, v3

    .line 603
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/FLStaticTextView;->h:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v28

    .line 605
    if-lez v25, :cond_0

    move/from16 v0, v27

    int-to-float v3, v0

    move-object/from16 v0, p0

    iget v4, v0, Lflipboard/gui/FLStaticTextView;->n:F

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_0

    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->k:I

    if-ltz v3, :cond_0

    if-eqz v28, :cond_0

    .line 609
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/FLStaticTextView;->t:[C

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/FLStaticTextView;->t:[C

    array-length v3, v3

    move/from16 v0, v28

    if-ge v3, v0, :cond_3

    .line 610
    :cond_2
    move/from16 v0, v28

    new-array v3, v0, [C

    move-object/from16 v0, p0

    iput-object v3, v0, Lflipboard/gui/FLStaticTextView;->t:[C

    .line 612
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/gui/FLStaticTextView;->h:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget-object v5, v0, Lflipboard/gui/FLStaticTextView;->t:[C

    move-object/from16 v0, p0

    iget v6, v0, Lflipboard/gui/FLStaticTextView;->k:I

    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->k:I

    const/4 v7, 0x1

    if-eq v3, v7, :cond_9

    const/4 v3, 0x1

    :goto_1
    move/from16 v0, v28

    invoke-static {v4, v0, v5, v6, v3}, Lflipboard/gui/FLStaticTextView;->a(Ljava/lang/CharSequence;I[CIZ)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->u:I

    .line 614
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v3, v3, Lflipboard/service/FlipboardManager;->ag:Z

    if-nez v3, :cond_4

    .line 615
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->u:I

    const/16 v4, 0x2000

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->u:I

    .line 617
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->u:I

    if-eqz v3, :cond_0

    .line 621
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lflipboard/gui/FLStaticTextView;->A:Z

    if-eqz v3, :cond_5

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v3, v4, :cond_a

    .line 622
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/gui/FLStaticTextView;->t:[C

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lflipboard/gui/FLStaticTextView;->u:I

    sget-object v7, Lflipboard/gui/FLStaticTextView;->s:[F

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Paint;->getTextWidths([CII[F)I

    .line 633
    :cond_6
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->e:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-nez v3, :cond_7

    .line 634
    const/4 v3, 0x3

    new-array v3, v3, [F

    .line 635
    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    const-string v5, " -\u2026"

    const/4 v6, 0x0

    const/4 v7, 0x3

    invoke-virtual {v4, v5, v6, v7, v3}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;II[F)I

    .line 636
    const/4 v4, 0x0

    aget v4, v3, v4

    move-object/from16 v0, p0

    iput v4, v0, Lflipboard/gui/FLStaticTextView;->e:F

    .line 637
    const/4 v4, 0x1

    aget v4, v3, v4

    move-object/from16 v0, p0

    iput v4, v0, Lflipboard/gui/FLStaticTextView;->f:F

    .line 638
    const/4 v4, 0x2

    aget v3, v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->g:F

    .line 641
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lflipboard/gui/FLStaticTextView;->u:I

    move/from16 v29, v0

    .line 642
    move-object/from16 v0, p0

    iget-object v0, v0, Lflipboard/gui/FLStaticTextView;->t:[C

    move-object/from16 v30, v0

    .line 643
    const/4 v11, 0x0

    .line 644
    move-object/from16 v0, v26

    iget v3, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    neg-float v9, v3

    .line 645
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->i:I

    .line 646
    const/4 v8, 0x0

    .line 647
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->k:I

    if-nez v3, :cond_b

    const v3, 0x7fffffff

    :goto_2
    move/from16 v0, v27

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget v5, v0, Lflipboard/gui/FLStaticTextView;->n:F

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lflipboard/gui/FLStaticTextView;->n:F

    move-object/from16 v0, p0

    iget v6, v0, Lflipboard/gui/FLStaticTextView;->o:F

    add-float/2addr v5, v6

    div-float/2addr v4, v5

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v31

    .line 648
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->i:I

    move/from16 v0, v31

    if-lt v3, v0, :cond_c

    const/4 v3, 0x1

    .line 649
    :goto_3
    const/4 v7, 0x1

    .line 650
    const/4 v5, 0x0

    .line 651
    const/4 v10, 0x0

    .line 653
    const/4 v6, 0x0

    .line 654
    const/4 v4, 0x0

    aget-char v4, v30, v4

    move v14, v5

    move v15, v3

    move/from16 v16, v11

    move v3, v4

    move-object v5, v8

    move v8, v9

    move v4, v7

    .line 658
    :goto_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lflipboard/gui/FLStaticTextView;->C:Lflipboard/gui/FLStaticTextView$BlockType;

    sget-object v9, Lflipboard/gui/FLStaticTextView$BlockType;->a:Lflipboard/gui/FLStaticTextView$BlockType;

    if-ne v7, v9, :cond_e

    .line 659
    move-object/from16 v0, v26

    iget v7, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    add-float/2addr v7, v8

    move-object/from16 v0, p0

    iget v9, v0, Lflipboard/gui/FLStaticTextView;->x:I

    int-to-float v9, v9

    cmpg-float v7, v7, v9

    if-gez v7, :cond_d

    const/4 v7, 0x1

    .line 665
    :goto_5
    move-object/from16 v0, p0

    iget v9, v0, Lflipboard/gui/FLStaticTextView;->l:F

    move/from16 v0, v25

    int-to-float v11, v0

    invoke-static {v9, v11}, Ljava/lang/Math;->min(FF)F

    move-result v9

    move-object/from16 v0, p0

    iput v9, v0, Lflipboard/gui/FLStaticTextView;->l:F

    .line 666
    if-eqz v7, :cond_11

    move-object/from16 v0, p0

    iget v7, v0, Lflipboard/gui/FLStaticTextView;->w:I

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/FLStaticTextView;->getPaddingLeft()I

    move-result v9

    sub-int/2addr v7, v9

    move/from16 v24, v7

    .line 668
    :goto_6
    if-nez v3, :cond_12

    .line 669
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->l:F

    move/from16 v0, v16

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->l:F

    .line 863
    :cond_8
    :goto_7
    sget-object v3, Lflipboard/gui/FLStaticTextView;->b:Lflipboard/util/Log;

    iget-boolean v3, v3, Lflipboard/util/Log;->f:Z

    if-eqz v3, :cond_0

    .line 864
    sget-object v3, Lflipboard/gui/FLStaticTextView;->b:Lflipboard/util/Log;

    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget v5, v0, Lflipboard/gui/FLStaticTextView;->k:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget v5, v0, Lflipboard/gui/FLStaticTextView;->i:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    move-object/from16 v0, p0

    iget v5, v0, Lflipboard/gui/FLStaticTextView;->l:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    move-object/from16 v0, p0

    iget-object v5, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/FLStaticTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x41

    invoke-static {v5, v6}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    goto/16 :goto_0

    .line 612
    :cond_9
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 625
    :cond_a
    const/4 v3, 0x5

    new-array v4, v3, [F

    .line 626
    const/4 v3, 0x0

    :goto_8
    move-object/from16 v0, p0

    iget v5, v0, Lflipboard/gui/FLStaticTextView;->u:I

    if-ge v3, v5, :cond_6

    .line 627
    const/4 v5, 0x5

    move-object/from16 v0, p0

    iget v6, v0, Lflipboard/gui/FLStaticTextView;->u:I

    sub-int/2addr v6, v3

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 628
    move-object/from16 v0, p0

    iget-object v6, v0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v7, v0, Lflipboard/gui/FLStaticTextView;->t:[C

    invoke-virtual {v6, v7, v3, v5, v4}, Landroid/graphics/Paint;->getTextWidths([CII[F)I

    .line 629
    const/4 v6, 0x0

    sget-object v7, Lflipboard/gui/FLStaticTextView;->s:[F

    invoke-static {v4, v6, v7, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 626
    add-int/lit8 v3, v3, 0x5

    goto :goto_8

    .line 647
    :cond_b
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->k:I

    goto/16 :goto_2

    .line 648
    :cond_c
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 659
    :cond_d
    const/4 v7, 0x0

    goto/16 :goto_5

    .line 660
    :cond_e
    move-object/from16 v0, p0

    iget-object v7, v0, Lflipboard/gui/FLStaticTextView;->C:Lflipboard/gui/FLStaticTextView$BlockType;

    sget-object v9, Lflipboard/gui/FLStaticTextView$BlockType;->b:Lflipboard/gui/FLStaticTextView$BlockType;

    if-ne v7, v9, :cond_10

    .line 661
    move-object/from16 v0, v26

    iget v7, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float v7, v8, v7

    move-object/from16 v0, p0

    iget v9, v0, Lflipboard/gui/FLStaticTextView;->x:I

    sub-int v9, v27, v9

    int-to-float v9, v9

    cmpl-float v7, v7, v9

    if-lez v7, :cond_f

    const/4 v7, 0x1

    goto/16 :goto_5

    :cond_f
    const/4 v7, 0x0

    goto/16 :goto_5

    .line 663
    :cond_10
    const/4 v7, 0x0

    goto/16 :goto_5

    :cond_11
    move/from16 v24, v25

    .line 666
    goto/16 :goto_6

    .line 674
    :cond_12
    const/16 v7, 0x20

    if-eq v3, v7, :cond_13

    const/16 v7, 0x200b

    if-ne v3, v7, :cond_14

    .line 675
    :cond_13
    add-int/lit8 v6, v6, 0x1

    move/from16 v0, v29

    if-ne v6, v0, :cond_15

    const/4 v3, 0x0

    .line 679
    :cond_14
    :goto_9
    move-object/from16 v0, p0

    iget-object v7, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v23

    .line 680
    const/16 v18, 0x0

    .line 684
    const/4 v11, 0x0

    .line 685
    const/4 v7, 0x0

    .line 686
    const/4 v9, 0x0

    .line 687
    const/4 v12, 0x1

    move v13, v7

    move/from16 v17, v11

    move/from16 v19, v6

    move/from16 v20, v18

    move/from16 v21, v3

    move v11, v6

    move/from16 v18, v23

    move v3, v4

    move-object v4, v5

    move v5, v6

    .line 692
    :goto_a
    if-eqz v21, :cond_40

    const/16 v7, 0xa

    move/from16 v0, v21

    if-eq v0, v7, :cond_40

    const/16 v7, 0x20

    move/from16 v0, v21

    if-eq v0, v7, :cond_40

    const/16 v7, 0x200b

    move/from16 v0, v21

    if-eq v0, v7, :cond_40

    .line 693
    const/16 v7, 0xad

    move/from16 v0, v21

    if-ne v0, v7, :cond_16

    .line 694
    add-float v7, v16, v20

    add-float/2addr v7, v9

    move-object/from16 v0, p0

    iget v0, v0, Lflipboard/gui/FLStaticTextView;->f:F

    move/from16 v22, v0

    add-float v7, v7, v22

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v22, v0

    cmpg-float v7, v7, v22

    if-gtz v7, :cond_3f

    .line 695
    move-object/from16 v0, p0

    iget-object v0, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    move-object/from16 v22, v0

    new-instance v3, Lflipboard/gui/FLStaticTextView$Segment;

    add-float v7, v16, v20

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v9}, Lflipboard/gui/FLStaticTextView$Segment;-><init>(Lflipboard/gui/FLStaticTextView;IIFFF)V

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 696
    add-int/lit8 v3, v6, 0x1

    .line 697
    const/16 v22, 0x0

    .line 698
    add-float v4, v20, v9

    .line 699
    const/4 v9, 0x0

    .line 700
    const/16 v20, 0x0

    .line 701
    add-float v5, v16, v4

    move-object/from16 v0, p0

    iget v7, v0, Lflipboard/gui/FLStaticTextView;->f:F

    add-float/2addr v5, v7

    move/from16 v0, v24

    int-to-float v7, v0

    cmpg-float v5, v5, v7

    if-gtz v5, :cond_44

    .line 704
    move-object/from16 v0, p0

    iget-object v5, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v18

    .line 706
    const/4 v13, 0x1

    move v5, v3

    move v7, v13

    move v11, v4

    move/from16 v17, v3

    move/from16 v13, v18

    move/from16 v18, v4

    .line 713
    :goto_b
    add-int/lit8 v19, v6, 0x1

    move/from16 v0, v19

    move/from16 v1, v29

    if-ne v0, v1, :cond_17

    const/4 v6, 0x0

    .line 715
    :goto_c
    if-eqz v6, :cond_3e

    sparse-switch v6, :sswitch_data_0

    sparse-switch v21, :sswitch_data_1

    sparse-switch v21, :sswitch_data_2

    const/16 v4, 0x4e00

    move/from16 v0, v21

    if-lt v0, v4, :cond_18

    const v4, 0x9faf

    move/from16 v0, v21

    if-gt v0, v4, :cond_18

    const/4 v4, 0x1

    :goto_d
    if-eqz v4, :cond_3e

    .line 716
    sget-object v4, Lflipboard/gui/FLStaticTextView;->b:Lflipboard/util/Log;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v21

    aput-object v21, v4, v12

    const/4 v12, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v4, v12

    .line 717
    const/4 v4, 0x0

    move v12, v6

    move-object/from16 v21, v22

    move/from16 v6, v19

    move/from16 v19, v7

    move v7, v11

    move/from16 v11, v20

    move/from16 v20, v13

    move/from16 v13, v17

    move/from16 v17, v18

    move/from16 v18, v4

    move v4, v3

    .line 718
    :goto_e
    if-eqz v11, :cond_2b

    add-float v3, v16, v17

    sub-int v22, v6, v4

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move/from16 v2, v22

    invoke-direct {v0, v1, v4, v2, v9}, Lflipboard/gui/FLStaticTextView;->a([CIIF)F

    move-result v22

    add-float v3, v3, v22

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v22, v0

    cmpl-float v3, v3, v22

    if-lez v3, :cond_2b

    move v10, v12

    .line 724
    :goto_f
    if-le v6, v5, :cond_26

    add-float v3, v16, v17

    sub-int v7, v6, v4

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1, v4, v7, v9}, Lflipboard/gui/FLStaticTextView;->a([CIIF)F

    move-result v7

    add-float/2addr v3, v7

    move/from16 v0, v24

    int-to-float v7, v0

    cmpl-float v3, v3, v7

    if-lez v3, :cond_26

    .line 725
    add-int/lit8 v6, v6, -0x1

    aget-char v10, v30, v6

    .line 726
    sget-object v3, Lflipboard/gui/FLStaticTextView;->s:[F

    aget v3, v3, v6

    sub-float/2addr v9, v3

    goto :goto_f

    .line 675
    :cond_15
    aget-char v3, v30, v6

    goto/16 :goto_9

    .line 710
    :cond_16
    sget-object v7, Lflipboard/gui/FLStaticTextView;->s:[F

    aget v7, v7, v6

    add-float/2addr v9, v7

    move v7, v13

    move-object/from16 v22, v4

    move/from16 v13, v18

    move/from16 v18, v20

    move/from16 v20, v3

    move v3, v11

    move/from16 v11, v17

    move/from16 v17, v19

    goto/16 :goto_b

    .line 713
    :cond_17
    aget-char v6, v30, v19

    goto/16 :goto_c

    .line 715
    :sswitch_0
    const/4 v4, 0x0

    goto/16 :goto_d

    :sswitch_1
    const/4 v4, 0x0

    goto/16 :goto_d

    :sswitch_2
    const/4 v4, 0x1

    goto/16 :goto_d

    :cond_18
    const/16 v4, 0x4e00

    if-lt v6, v4, :cond_19

    const v4, 0x9faf

    if-gt v6, v4, :cond_19

    const/4 v4, 0x1

    goto/16 :goto_d

    :cond_19
    const/16 v4, 0x3040

    move/from16 v0, v21

    if-lt v0, v4, :cond_1c

    const/16 v4, 0x309f

    move/from16 v0, v21

    if-gt v0, v4, :cond_1c

    const/16 v4, 0x3040

    if-lt v6, v4, :cond_1a

    const/16 v4, 0x309f

    if-le v6, v4, :cond_1b

    :cond_1a
    const/4 v4, 0x1

    goto/16 :goto_d

    :cond_1b
    const/4 v4, 0x0

    goto/16 :goto_d

    :cond_1c
    const/16 v4, 0x3040

    if-lt v6, v4, :cond_1f

    const/16 v4, 0x309f

    if-gt v6, v4, :cond_1f

    const/16 v4, 0x3040

    move/from16 v0, v21

    if-lt v0, v4, :cond_1d

    const/16 v4, 0x309f

    move/from16 v0, v21

    if-le v0, v4, :cond_1e

    :cond_1d
    const/4 v4, 0x1

    goto/16 :goto_d

    :cond_1e
    const/4 v4, 0x0

    goto/16 :goto_d

    :cond_1f
    const/16 v4, 0x31f0

    move/from16 v0, v21

    if-lt v0, v4, :cond_22

    const/16 v4, 0x31ff

    move/from16 v0, v21

    if-gt v0, v4, :cond_22

    const/16 v4, 0x31f0

    if-lt v6, v4, :cond_20

    const/16 v4, 0x31ff

    if-le v6, v4, :cond_21

    :cond_20
    const/4 v4, 0x1

    goto/16 :goto_d

    :cond_21
    const/4 v4, 0x0

    goto/16 :goto_d

    :cond_22
    const/16 v4, 0x31f0

    if-lt v6, v4, :cond_25

    const/16 v4, 0x31ff

    if-gt v6, v4, :cond_25

    const/16 v4, 0x31f0

    move/from16 v0, v21

    if-lt v0, v4, :cond_23

    const/16 v4, 0x31ff

    move/from16 v0, v21

    if-le v0, v4, :cond_24

    :cond_23
    const/4 v4, 0x1

    goto/16 :goto_d

    :cond_24
    const/4 v4, 0x0

    goto/16 :goto_d

    :cond_25
    const/4 v4, 0x0

    goto/16 :goto_d

    .line 729
    :cond_26
    if-eqz v15, :cond_29

    .line 730
    sub-int v3, v6, v4

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1, v4, v3, v9}, Lflipboard/gui/FLStaticTextView;->a([CIIF)F

    move-result v3

    move/from16 v33, v3

    move v3, v9

    move/from16 v9, v33

    .line 731
    :goto_10
    if-le v6, v5, :cond_27

    add-float v7, v16, v17

    add-float/2addr v7, v9

    move-object/from16 v0, p0

    iget v10, v0, Lflipboard/gui/FLStaticTextView;->g:F

    add-float/2addr v7, v10

    move/from16 v0, v24

    int-to-float v10, v0

    cmpl-float v7, v7, v10

    if-lez v7, :cond_27

    .line 732
    add-int/lit8 v6, v6, -0x1

    .line 733
    sget-object v7, Lflipboard/gui/FLStaticTextView;->s:[F

    aget v7, v7, v6

    sub-float/2addr v3, v7

    .line 734
    sub-int v7, v6, v4

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1, v4, v7, v3}, Lflipboard/gui/FLStaticTextView;->a([CIIF)F

    move-result v9

    goto :goto_10

    .line 737
    :cond_27
    if-ge v5, v6, :cond_28

    .line 738
    move-object/from16 v0, p0

    iget-object v10, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    new-instance v3, Lflipboard/gui/FLStaticTextView$Segment;

    add-float v7, v16, v17

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v9}, Lflipboard/gui/FLStaticTextView$Segment;-><init>(Lflipboard/gui/FLStaticTextView;IIFFF)V

    invoke-interface {v10, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 740
    :cond_28
    sget-object v3, Lflipboard/gui/FLStaticTextView;->b:Lflipboard/util/Log;

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v3, v4

    const/4 v4, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    move-object/from16 v0, p0

    iget-object v5, v0, Lflipboard/gui/FLStaticTextView;->h:Ljava/lang/CharSequence;

    aput-object v5, v3, v4

    .line 741
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    new-instance v10, Lflipboard/gui/FLStaticTextView$FixedSegment;

    const-string v12, "\u2026"

    add-float v4, v16, v17

    add-float v13, v4, v9

    move-object/from16 v0, p0

    iget v15, v0, Lflipboard/gui/FLStaticTextView;->g:F

    move-object/from16 v11, p0

    move v14, v8

    invoke-direct/range {v10 .. v15}, Lflipboard/gui/FLStaticTextView$FixedSegment;-><init>(Lflipboard/gui/FLStaticTextView;Ljava/lang/String;FFF)V

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 742
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->l:F

    add-float v4, v16, v17

    add-float/2addr v4, v9

    move-object/from16 v0, p0

    iget v5, v0, Lflipboard/gui/FLStaticTextView;->g:F

    add-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->l:F

    goto/16 :goto_7

    .line 745
    :cond_29
    move-object/from16 v0, p0

    iget-object v11, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    new-instance v3, Lflipboard/gui/FLStaticTextView$Segment;

    add-float v7, v16, v17

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v9}, Lflipboard/gui/FLStaticTextView$Segment;-><init>(Lflipboard/gui/FLStaticTextView;IIFFF)V

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 746
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->l:F

    add-float v4, v16, v17

    add-float/2addr v4, v9

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->l:F

    .line 747
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->o:F

    move-object/from16 v0, p0

    iget v4, v0, Lflipboard/gui/FLStaticTextView;->n:F

    add-float/2addr v3, v4

    add-float v9, v8, v3

    .line 748
    const/4 v11, 0x0

    .line 749
    const/4 v5, 0x0

    .line 750
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 751
    const/4 v8, 0x0

    .line 752
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->i:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->i:I

    move/from16 v0, v31

    if-lt v3, v0, :cond_2a

    const/4 v3, 0x1

    .line 753
    :goto_11
    const/4 v7, 0x1

    move v14, v5

    move v15, v3

    move/from16 v16, v11

    move-object v5, v8

    move v3, v10

    move v8, v9

    move v10, v4

    move v4, v7

    .line 754
    goto/16 :goto_4

    .line 752
    :cond_2a
    const/4 v3, 0x0

    goto :goto_11

    .line 758
    :cond_2b
    sub-int v3, v6, v4

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1, v4, v3, v9}, Lflipboard/gui/FLStaticTextView;->a([CIIF)F

    move-result v9

    .line 761
    if-eqz v15, :cond_2f

    add-float v3, v16, v17

    add-float v4, v3, v9

    if-nez v12, :cond_2c

    const/4 v3, 0x0

    :goto_12
    add-float/2addr v3, v4

    move/from16 v0, v24

    int-to-float v4, v0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_2f

    .line 762
    sget-object v3, Lflipboard/gui/FLStaticTextView;->b:Lflipboard/util/Log;

    const/16 v3, 0xa

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    aput-object v22, v3, v4

    const/4 v4, 0x1

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    aput-object v22, v3, v4

    const/4 v4, 0x2

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    aput-object v22, v3, v4

    const/4 v4, 0x3

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v3, v4

    const/4 v4, 0x4

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v3, v4

    const/4 v4, 0x5

    add-float v22, v16, v17

    add-float v22, v22, v9

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    aput-object v22, v3, v4

    const/4 v4, 0x6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v3, v4

    const/4 v4, 0x7

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v3, v4

    const/16 v4, 0x8

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v3, v4

    const/16 v4, 0x9

    move-object/from16 v0, p0

    iget-object v0, v0, Lflipboard/gui/FLStaticTextView;->h:Ljava/lang/CharSequence;

    move-object/from16 v22, v0

    aput-object v22, v3, v4

    .line 764
    if-nez v12, :cond_2d

    const/high16 v3, -0x40800000    # -1.0f

    :goto_13
    move v4, v3

    move v3, v6

    .line 765
    :goto_14
    move/from16 v0, v29

    if-ge v3, v0, :cond_2e

    const/16 v22, 0x0

    cmpl-float v22, v4, v22

    if-ltz v22, :cond_2e

    .line 766
    sget-object v22, Lflipboard/gui/FLStaticTextView;->s:[F

    aget v22, v22, v3

    sub-float v4, v4, v22

    .line 765
    add-int/lit8 v3, v3, 0x1

    goto :goto_14

    .line 761
    :cond_2c
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->g:F

    goto :goto_12

    .line 764
    :cond_2d
    move/from16 v0, v24

    int-to-float v3, v0

    add-float v4, v16, v17

    add-float/2addr v4, v9

    sub-float/2addr v3, v4

    goto :goto_13

    .line 768
    :cond_2e
    sget-object v3, Lflipboard/gui/FLStaticTextView;->b:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/16 v22, 0x0

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v32

    aput-object v32, v3, v22

    .line 769
    const/4 v3, 0x0

    cmpg-float v3, v4, v3

    if-gez v3, :cond_2f

    .line 770
    sget-object v3, Lflipboard/gui/FLStaticTextView;->b:Lflipboard/util/Log;

    const/16 v3, 0x8

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lflipboard/gui/FLStaticTextView;->n:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget v5, v0, Lflipboard/gui/FLStaticTextView;->i:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    move-object/from16 v0, p0

    iget v5, v0, Lflipboard/gui/FLStaticTextView;->k:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    add-float v5, v16, v17

    add-float/2addr v5, v9

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x7

    move-object/from16 v0, p0

    iget v5, v0, Lflipboard/gui/FLStaticTextView;->g:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    .line 771
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v23

    invoke-interface {v3, v0, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 772
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    new-instance v4, Lflipboard/gui/FLStaticTextView$FixedSegment;

    const-string v6, "\u2026"

    move-object/from16 v0, p0

    iget v9, v0, Lflipboard/gui/FLStaticTextView;->g:F

    move-object/from16 v5, p0

    move/from16 v7, v16

    invoke-direct/range {v4 .. v9}, Lflipboard/gui/FLStaticTextView$FixedSegment;-><init>(Lflipboard/gui/FLStaticTextView;Ljava/lang/String;FFF)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 773
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->l:F

    move-object/from16 v0, p0

    iget v4, v0, Lflipboard/gui/FLStaticTextView;->g:F

    add-float v4, v4, v16

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->l:F

    .line 774
    sget-object v3, Lflipboard/gui/FLStaticTextView;->b:Lflipboard/util/Log;

    iget-boolean v3, v3, Lflipboard/util/Log;->f:Z

    if-eqz v3, :cond_8

    .line 775
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lflipboard/gui/FLStaticTextView;->a(I)V

    goto/16 :goto_7

    .line 782
    :cond_2f
    add-float v3, v16, v17

    add-float/2addr v3, v9

    move/from16 v0, v24

    int-to-float v4, v0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_35

    .line 783
    move/from16 v0, v23

    move/from16 v1, v20

    if-ge v0, v1, :cond_3d

    move v3, v7

    .line 787
    :goto_15
    move/from16 v0, v29

    if-ne v13, v0, :cond_31

    const/4 v11, 0x0

    .line 788
    :goto_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v5, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    move/from16 v0, v20

    invoke-interface {v4, v0, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 789
    if-eqz v19, :cond_3c

    .line 790
    move-object/from16 v0, p0

    iget-object v12, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    new-instance v4, Lflipboard/gui/FLStaticTextView$FixedSegment;

    const-string v6, "-"

    add-float v7, v7, v16

    move-object/from16 v0, p0

    iget v9, v0, Lflipboard/gui/FLStaticTextView;->f:F

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v9}, Lflipboard/gui/FLStaticTextView$FixedSegment;-><init>(Lflipboard/gui/FLStaticTextView;Ljava/lang/String;FFF)V

    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 791
    move-object/from16 v0, p0

    iget v4, v0, Lflipboard/gui/FLStaticTextView;->f:F

    add-float/2addr v3, v4

    move v6, v3

    .line 793
    :goto_17
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lflipboard/gui/FLStaticTextView;->B:Z

    if-eqz v3, :cond_33

    if-nez v15, :cond_33

    move/from16 v0, v24

    int-to-float v3, v0

    move-object/from16 v0, p0

    iget v4, v0, Lflipboard/gui/FLStaticTextView;->e:F

    const/high16 v5, 0x42480000    # 50.0f

    mul-float/2addr v4, v5

    cmpl-float v3, v3, v4

    if-lez v3, :cond_33

    .line 794
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lflipboard/gui/FLStaticTextView;->B:Z

    if-eqz v3, :cond_30

    if-eqz v21, :cond_30

    move-object/from16 v0, v21

    iget-boolean v3, v0, Lflipboard/gui/FLStaticTextView$Segment;->h:Z

    if-eqz v3, :cond_30

    .line 795
    add-int/lit8 v14, v14, -0x1

    .line 797
    :cond_30
    if-lez v14, :cond_33

    .line 798
    move/from16 v0, v24

    int-to-float v3, v0

    add-float v4, v16, v6

    sub-float/2addr v3, v4

    int-to-float v4, v14

    div-float v7, v3, v4

    .line 799
    const/4 v4, 0x0

    .line 800
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->e:F

    const/high16 v5, 0x40400000    # 3.0f

    mul-float/2addr v3, v5

    cmpg-float v3, v7, v3

    if-gez v3, :cond_33

    move v5, v10

    .line 801
    :goto_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v5, v3, :cond_32

    .line 802
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflipboard/gui/FLStaticTextView$Segment;

    .line 803
    iget v9, v3, Lflipboard/gui/FLStaticTextView$Segment;->e:F

    add-float/2addr v9, v4

    iput v9, v3, Lflipboard/gui/FLStaticTextView$Segment;->e:F

    .line 804
    iget-boolean v3, v3, Lflipboard/gui/FLStaticTextView$Segment;->h:Z

    if-eqz v3, :cond_43

    .line 805
    add-float v3, v4, v7

    .line 801
    :goto_19
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v3

    goto :goto_18

    .line 787
    :cond_31
    aget-char v11, v30, v13

    goto/16 :goto_16

    .line 808
    :cond_32
    move/from16 v0, v24

    int-to-float v3, v0

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->l:F

    .line 812
    :cond_33
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->l:F

    add-float v4, v16, v6

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->l:F

    .line 813
    if-nez v15, :cond_8

    .line 814
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->o:F

    move-object/from16 v0, p0

    iget v4, v0, Lflipboard/gui/FLStaticTextView;->n:F

    add-float/2addr v3, v4

    add-float/2addr v8, v3

    .line 817
    const/4 v7, 0x0

    .line 818
    const/4 v4, 0x0

    .line 819
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v10

    .line 820
    const/4 v6, 0x0

    .line 821
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->i:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->i:I

    move/from16 v0, v31

    if-lt v3, v0, :cond_34

    const/4 v3, 0x1

    .line 822
    :goto_1a
    const/4 v5, 0x1

    move v14, v4

    move v15, v3

    move/from16 v16, v7

    move v4, v5

    move v3, v11

    move-object v5, v6

    move v6, v13

    .line 823
    goto/16 :goto_4

    .line 821
    :cond_34
    const/4 v3, 0x0

    goto :goto_1a

    .line 827
    :cond_35
    if-ge v5, v6, :cond_3b

    .line 828
    if-eqz v21, :cond_36

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lflipboard/gui/FLStaticTextView;->B:Z

    if-nez v3, :cond_36

    .line 829
    move-object/from16 v0, v21

    iput v6, v0, Lflipboard/gui/FLStaticTextView$Segment;->d:I

    .line 830
    add-float v3, v16, v17

    add-float/2addr v3, v9

    move-object/from16 v0, v21

    iget v4, v0, Lflipboard/gui/FLStaticTextView$Segment;->e:F

    sub-float/2addr v3, v4

    move-object/from16 v0, v21

    iput v3, v0, Lflipboard/gui/FLStaticTextView$Segment;->g:F

    move v5, v14

    move-object/from16 v7, v21

    .line 839
    :goto_1b
    add-float v3, v17, v9

    add-float v4, v16, v3

    .line 842
    const/16 v3, 0xa

    if-ne v12, v3, :cond_39

    .line 843
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->l:F

    add-float v4, v4, v17

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->l:F

    .line 844
    if-nez v15, :cond_8

    .line 845
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->o:F

    move-object/from16 v0, p0

    iget v4, v0, Lflipboard/gui/FLStaticTextView;->n:F

    add-float/2addr v3, v4

    add-float v9, v8, v3

    .line 848
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->y:I

    int-to-float v11, v3

    .line 849
    const/4 v4, 0x0

    .line 850
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v10

    .line 851
    const/4 v8, 0x0

    .line 852
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/FLStaticTextView;->i:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lflipboard/gui/FLStaticTextView;->i:I

    move/from16 v0, v31

    if-lt v3, v0, :cond_37

    const/4 v7, 0x1

    .line 853
    :goto_1c
    add-int/lit8 v6, v6, 0x1

    move/from16 v0, v29

    if-ne v6, v0, :cond_38

    const/4 v3, 0x0

    .line 854
    :goto_1d
    const/4 v5, 0x1

    move v14, v4

    move v15, v7

    move/from16 v16, v11

    move v4, v5

    move-object v5, v8

    move v8, v9

    goto/16 :goto_4

    .line 832
    :cond_36
    move-object/from16 v0, p0

    iget-object v13, v0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    new-instance v3, Lflipboard/gui/FLStaticTextView$Segment;

    add-float v7, v16, v17

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v9}, Lflipboard/gui/FLStaticTextView$Segment;-><init>(Lflipboard/gui/FLStaticTextView;IIFFF)V

    invoke-interface {v13, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 833
    const/16 v4, 0x20

    if-ne v12, v4, :cond_42

    .line 834
    const/4 v4, 0x1

    iput-boolean v4, v3, Lflipboard/gui/FLStaticTextView$Segment;->h:Z

    .line 835
    add-int/lit8 v14, v14, 0x1

    move v5, v14

    move-object v7, v3

    goto :goto_1b

    .line 852
    :cond_37
    const/4 v7, 0x0

    goto :goto_1c

    .line 853
    :cond_38
    aget-char v3, v30, v6

    goto :goto_1d

    .line 855
    :cond_39
    if-eqz v12, :cond_41

    .line 856
    const/4 v3, 0x0

    .line 857
    const/16 v9, 0x200b

    if-eq v12, v9, :cond_3a

    if-eqz v18, :cond_3a

    .line 858
    move-object/from16 v0, p0

    iget v9, v0, Lflipboard/gui/FLStaticTextView;->e:F

    add-float/2addr v4, v9

    :cond_3a
    :goto_1e
    move v14, v5

    move/from16 v16, v4

    move v4, v3

    move-object v5, v7

    move v3, v12

    .line 861
    goto/16 :goto_4

    :cond_3b
    move v5, v14

    move-object/from16 v7, v21

    goto/16 :goto_1b

    :cond_3c
    move v6, v3

    goto/16 :goto_17

    :cond_3d
    move/from16 v3, v17

    goto/16 :goto_15

    :cond_3e
    move/from16 v21, v6

    move-object/from16 v4, v22

    move/from16 v6, v19

    move/from16 v19, v17

    move/from16 v17, v11

    move v11, v3

    move/from16 v3, v20

    move/from16 v20, v18

    move/from16 v18, v13

    move v13, v7

    goto/16 :goto_a

    :cond_3f
    move v7, v13

    move-object/from16 v22, v4

    move/from16 v13, v18

    move/from16 v18, v20

    move/from16 v20, v3

    move v3, v11

    move/from16 v11, v17

    move/from16 v17, v19

    goto/16 :goto_b

    :cond_40
    move/from16 v7, v17

    move/from16 v17, v20

    move/from16 v20, v18

    move/from16 v18, v12

    move/from16 v12, v21

    move-object/from16 v21, v4

    move v4, v11

    move v11, v3

    move/from16 v33, v13

    move/from16 v13, v19

    move/from16 v19, v33

    goto/16 :goto_e

    :cond_41
    move v3, v11

    goto :goto_1e

    :cond_42
    move v5, v14

    move-object v7, v3

    goto/16 :goto_1b

    :cond_43
    move v3, v4

    goto/16 :goto_19

    :cond_44
    move v5, v3

    move v7, v13

    move v3, v11

    move/from16 v13, v18

    move/from16 v11, v17

    move/from16 v18, v4

    move/from16 v17, v19

    goto/16 :goto_b

    .line 715
    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0x21 -> :sswitch_0
        0x25 -> :sswitch_0
        0x29 -> :sswitch_0
        0x2c -> :sswitch_0
        0x2e -> :sswitch_0
        0x3a -> :sswitch_0
        0x3b -> :sswitch_0
        0x3e -> :sswitch_0
        0x3f -> :sswitch_0
        0x5d -> :sswitch_0
        0x7d -> :sswitch_0
        0xa2 -> :sswitch_0
        0xa8 -> :sswitch_0
        0xb0 -> :sswitch_0
        0xb7 -> :sswitch_0
        0xbb -> :sswitch_0
        0x2c7 -> :sswitch_0
        0x2c9 -> :sswitch_0
        0x2015 -> :sswitch_0
        0x2016 -> :sswitch_0
        0x2019 -> :sswitch_0
        0x201d -> :sswitch_0
        0x201e -> :sswitch_0
        0x201f -> :sswitch_0
        0x2020 -> :sswitch_0
        0x2021 -> :sswitch_0
        0x203a -> :sswitch_0
        0x2103 -> :sswitch_0
        0x2236 -> :sswitch_0
        0x3001 -> :sswitch_0
        0x3002 -> :sswitch_0
        0x3003 -> :sswitch_0
        0x3006 -> :sswitch_0
        0x3009 -> :sswitch_0
        0x300b -> :sswitch_0
        0x300c -> :sswitch_0
        0x300d -> :sswitch_0
        0x300e -> :sswitch_0
        0x300f -> :sswitch_0
        0x3011 -> :sswitch_0
        0x3015 -> :sswitch_0
        0x3017 -> :sswitch_0
        0x3019 -> :sswitch_0
        0x301e -> :sswitch_0
        0x301f -> :sswitch_0
        0xfe35 -> :sswitch_0
        0xfe39 -> :sswitch_0
        0xfe3d -> :sswitch_0
        0xfe3f -> :sswitch_0
        0xfe43 -> :sswitch_0
        0xfe58 -> :sswitch_0
        0xfe5a -> :sswitch_0
        0xfe5c -> :sswitch_0
        0xff01 -> :sswitch_0
        0xff02 -> :sswitch_0
        0xff05 -> :sswitch_0
        0xff07 -> :sswitch_0
        0xff09 -> :sswitch_0
        0xff0c -> :sswitch_0
        0xff0e -> :sswitch_0
        0xff1a -> :sswitch_0
        0xff1b -> :sswitch_0
        0xff1f -> :sswitch_0
        0xff3d -> :sswitch_0
        0xff40 -> :sswitch_0
        0xff5c -> :sswitch_0
        0xff5d -> :sswitch_0
        0xff5e -> :sswitch_0
        0xff60 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x24 -> :sswitch_1
        0x28 -> :sswitch_1
        0x2a -> :sswitch_1
        0x2c -> :sswitch_1
        0x5b -> :sswitch_1
        0xa3 -> :sswitch_1
        0xa5 -> :sswitch_1
        0xab -> :sswitch_1
        0xb7 -> :sswitch_1
        0x2018 -> :sswitch_1
        0x201c -> :sswitch_1
        0x3008 -> :sswitch_1
        0x300a -> :sswitch_1
        0x300c -> :sswitch_1
        0x300e -> :sswitch_1
        0x3010 -> :sswitch_1
        0x3014 -> :sswitch_1
        0x3016 -> :sswitch_1
        0x3018 -> :sswitch_1
        0x301d -> :sswitch_1
        0xfe57 -> :sswitch_1
        0xfe59 -> :sswitch_1
        0xfe5b -> :sswitch_1
        0xff04 -> :sswitch_1
        0xff08 -> :sswitch_1
        0xff0e -> :sswitch_1
        0xff3b -> :sswitch_1
        0xff5b -> :sswitch_1
        0xff5f -> :sswitch_1
        0xffe1 -> :sswitch_1
        0xffe5 -> :sswitch_1
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0xb7 -> :sswitch_2
        0x3001 -> :sswitch_2
        0x3002 -> :sswitch_2
        0x3009 -> :sswitch_2
        0x300b -> :sswitch_2
        0x3011 -> :sswitch_2
        0xff02 -> :sswitch_2
        0xff07 -> :sswitch_2
        0xff09 -> :sswitch_2
        0xff0c -> :sswitch_2
        0xff0e -> :sswitch_2
        0xff1f -> :sswitch_2
        0xff5d -> :sswitch_2
    .end sparse-switch
.end method

.method static synthetic b(Lflipboard/gui/FLStaticTextView;)[C
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView;->t:[C

    return-object v0
.end method

.method static synthetic c(Lflipboard/gui/FLStaticTextView;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    return-object v0
.end method

.method private c(II)V
    .locals 10

    .prologue
    const v0, 0x7fffffff

    const/4 v9, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    .line 896
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 897
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 898
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 899
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 901
    iget v6, p0, Lflipboard/gui/FLStaticTextView;->j:I

    if-lez v6, :cond_0

    .line 902
    sparse-switch v3, :sswitch_data_0

    .line 917
    :cond_0
    :goto_0
    :sswitch_0
    if-ne v5, v4, :cond_1

    if-ne v3, v4, :cond_1

    .line 918
    invoke-virtual {p0, v2, v1}, Lflipboard/gui/FLStaticTextView;->setMeasuredDimension(II)V

    .line 941
    :goto_1
    return-void

    .line 906
    :sswitch_1
    iget v3, p0, Lflipboard/gui/FLStaticTextView;->j:I

    int-to-float v1, v1

    iget v6, p0, Lflipboard/gui/FLStaticTextView;->n:F

    sub-float/2addr v1, v6

    iget v6, p0, Lflipboard/gui/FLStaticTextView;->n:F

    iget v7, p0, Lflipboard/gui/FLStaticTextView;->o:F

    add-float/2addr v6, v7

    div-float/2addr v1, v6

    float-to-int v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 907
    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingTop()I

    move-result v3

    iget v6, p0, Lflipboard/gui/FLStaticTextView;->n:F

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    iget v7, p0, Lflipboard/gui/FLStaticTextView;->n:F

    iget v8, p0, Lflipboard/gui/FLStaticTextView;->o:F

    add-float/2addr v7, v8

    mul-float/2addr v1, v7

    add-float/2addr v1, v6

    float-to-double v6, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v1, v6

    add-int/2addr v1, v3

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v1, v3

    move v3, v4

    .line 909
    goto :goto_0

    .line 911
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingTop()I

    move-result v1

    iget v3, p0, Lflipboard/gui/FLStaticTextView;->n:F

    iget v6, p0, Lflipboard/gui/FLStaticTextView;->j:I

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    iget v7, p0, Lflipboard/gui/FLStaticTextView;->n:F

    iget v8, p0, Lflipboard/gui/FLStaticTextView;->o:F

    add-float/2addr v7, v8

    mul-float/2addr v6, v7

    add-float/2addr v3, v6

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v3, v6

    add-int/2addr v1, v3

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v1, v3

    move v3, v4

    .line 912
    goto :goto_0

    .line 921
    :cond_1
    if-nez v5, :cond_2

    move v2, v0

    .line 924
    :cond_2
    if-nez v3, :cond_8

    .line 928
    :goto_2
    iget v1, p0, Lflipboard/gui/FLStaticTextView;->q:I

    if-ne v0, v1, :cond_4

    iget v1, p0, Lflipboard/gui/FLStaticTextView;->p:I

    if-eq v2, v1, :cond_3

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingLeft()I

    move-result v1

    iget v6, p0, Lflipboard/gui/FLStaticTextView;->l:F

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v6, v6

    add-int/2addr v1, v6

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingRight()I

    move-result v6

    add-int/2addr v1, v6

    if-ne v2, v1, :cond_4

    :cond_3
    iget-boolean v1, p0, Lflipboard/gui/FLStaticTextView;->D:Z

    if-eqz v1, :cond_5

    .line 929
    :cond_4
    invoke-direct {p0, v2, v0}, Lflipboard/gui/FLStaticTextView;->b(II)V

    .line 930
    iput-boolean v9, p0, Lflipboard/gui/FLStaticTextView;->D:Z

    .line 933
    :cond_5
    if-eq v5, v4, :cond_7

    .line 934
    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingLeft()I

    move-result v1

    iget v2, p0, Lflipboard/gui/FLStaticTextView;->l:F

    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v2, v6

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    .line 936
    :goto_3
    if-eq v3, v4, :cond_6

    .line 937
    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingTop()I

    move-result v0

    iget v2, p0, Lflipboard/gui/FLStaticTextView;->n:F

    iget v3, p0, Lflipboard/gui/FLStaticTextView;->i:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    iget v4, p0, Lflipboard/gui/FLStaticTextView;->n:F

    iget v5, p0, Lflipboard/gui/FLStaticTextView;->o:F

    add-float/2addr v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    .line 940
    :cond_6
    invoke-static {v9, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v9, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lflipboard/gui/FLStaticTextView;->setMeasuredDimension(II)V

    goto/16 :goto_1

    :cond_7
    move v1, v2

    goto :goto_3

    :cond_8
    move v0, v1

    goto :goto_2

    .line 902
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_2
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 316
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    int-to-float v1, p2

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {p1, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 318
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/FLStaticTextView;->e:F

    .line 319
    iput p2, p0, Lflipboard/gui/FLStaticTextView;->d:I

    .line 320
    return-void
.end method

.method public final a(Lflipboard/gui/FLStaticTextView$BlockType;II)V
    .locals 1

    .prologue
    .line 355
    iget v0, p0, Lflipboard/gui/FLStaticTextView;->w:I

    if-ne p2, v0, :cond_0

    iget v0, p0, Lflipboard/gui/FLStaticTextView;->x:I

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLStaticTextView;->C:Lflipboard/gui/FLStaticTextView$BlockType;

    if-eq p1, v0, :cond_1

    .line 357
    :cond_0
    iput p2, p0, Lflipboard/gui/FLStaticTextView;->w:I

    .line 358
    iput p3, p0, Lflipboard/gui/FLStaticTextView;->x:I

    .line 359
    iput-object p1, p0, Lflipboard/gui/FLStaticTextView;->C:Lflipboard/gui/FLStaticTextView$BlockType;

    .line 362
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    .line 363
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/FLStaticTextView;->D:Z

    .line 365
    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->forceLayout()V

    .line 367
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 252
    if-nez p1, :cond_0

    .line 257
    const-string p1, ""

    .line 260
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView;->h:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 282
    :cond_1
    :goto_0
    return-void

    .line 266
    :cond_2
    invoke-virtual {p0, p1}, Lflipboard/gui/FLStaticTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 269
    iput-object p1, p0, Lflipboard/gui/FLStaticTextView;->h:Ljava/lang/CharSequence;

    .line 270
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    .line 271
    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->invalidate()V

    .line 272
    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->requestLayout()V

    .line 275
    if-eqz p2, :cond_3

    const-string v0, "ar"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 276
    iput-boolean v1, p0, Lflipboard/gui/FLStaticTextView;->B:Z

    .line 277
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/FLStaticTextView;->A:Z

    .line 279
    :cond_3
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-nez v0, :cond_4

    if-eqz p2, :cond_1

    const-string v0, "zh"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280
    :cond_4
    iput-boolean v1, p0, Lflipboard/gui/FLStaticTextView;->B:Z

    goto :goto_0
.end method

.method public final a(ZI)V
    .locals 0

    .prologue
    .line 994
    return-void
.end method

.method public getLineCount()I
    .locals 1

    .prologue
    .line 343
    iget v0, p0, Lflipboard/gui/FLStaticTextView;->i:I

    return v0
.end method

.method public getLineHeight()F
    .locals 2

    .prologue
    .line 339
    iget v0, p0, Lflipboard/gui/FLStaticTextView;->n:F

    iget v1, p0, Lflipboard/gui/FLStaticTextView;->o:F

    add-float/2addr v0, v1

    return v0
.end method

.method public getMaxHeight()F
    .locals 3

    .prologue
    .line 375
    iget v0, p0, Lflipboard/gui/FLStaticTextView;->n:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 378
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    .line 379
    iget v1, v0, Landroid/graphics/Paint$FontMetrics;->bottom:F

    iget v2, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float/2addr v1, v2

    iget v2, p0, Lflipboard/gui/FLStaticTextView;->z:F

    mul-float/2addr v1, v2

    iput v1, p0, Lflipboard/gui/FLStaticTextView;->n:F

    .line 380
    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->leading:F

    iput v0, p0, Lflipboard/gui/FLStaticTextView;->o:F

    .line 382
    :cond_0
    iget v0, p0, Lflipboard/gui/FLStaticTextView;->k:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getLineHeight()F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method public getMaxLines()I
    .locals 1

    .prologue
    .line 335
    iget v0, p0, Lflipboard/gui/FLStaticTextView;->k:I

    return v0
.end method

.method public getPaint()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    return-object v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView;->h:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTextColor()I
    .locals 1

    .prologue
    .line 300
    iget v0, p0, Lflipboard/gui/FLStaticTextView;->v:I

    return v0
.end method

.method public getTextSize()F
    .locals 1

    .prologue
    .line 323
    iget v0, p0, Lflipboard/gui/FLStaticTextView;->d:I

    int-to-float v0, v0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 950
    sget-object v0, Lflipboard/gui/FLStaticTextView;->b:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_1

    .line 951
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 952
    const v0, 0x25e04020

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 953
    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 954
    const v0, 0x25f05040

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 955
    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingLeft()I

    move-result v0

    int-to-float v7, v0

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingTop()I

    move-result v0

    int-to-float v8, v0

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    int-to-float v9, v0

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v0, v2

    int-to-float v10, v0

    move-object v6, p1

    move-object v11, v5

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 956
    const v0, -0xffff01

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 957
    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 958
    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getHeight()I

    move-result v0

    int-to-float v2, v0

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    move-object v0, p1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 963
    iget v0, p0, Lflipboard/gui/FLStaticTextView;->w:I

    if-gtz v0, :cond_0

    iget v0, p0, Lflipboard/gui/FLStaticTextView;->x:I

    if-lez v0, :cond_1

    .line 964
    :cond_0
    const v0, -0x77cc12

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 965
    iget v0, p0, Lflipboard/gui/FLStaticTextView;->w:I

    int-to-float v7, v0

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getWidth()I

    move-result v0

    int-to-float v9, v0

    iget v0, p0, Lflipboard/gui/FLStaticTextView;->x:I

    int-to-float v10, v0

    move-object v6, p1

    move v8, v1

    move-object v11, v5

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 969
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getHeight()I

    move-result v2

    invoke-direct {p0, v0, v2}, Lflipboard/gui/FLStaticTextView;->b(II)V

    .line 974
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    iget v2, p0, Lflipboard/gui/FLStaticTextView;->v:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 975
    iget-boolean v0, p0, Lflipboard/gui/FLStaticTextView;->A:Z

    if-eqz v0, :cond_3

    .line 976
    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingRight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 980
    :goto_0
    iget v0, p0, Lflipboard/gui/FLStaticTextView;->z:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_2

    .line 981
    iget v0, p0, Lflipboard/gui/FLStaticTextView;->n:F

    iget v2, p0, Lflipboard/gui/FLStaticTextView;->m:F

    sub-float/2addr v0, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 983
    :cond_2
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView$Segment;

    .line 984
    invoke-virtual {v0, p1}, Lflipboard/gui/FLStaticTextView$Segment;->a(Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 978
    :cond_3
    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0

    .line 986
    :cond_4
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    .line 878
    sget-object v0, Lflipboard/gui/FLStaticTextView;->a:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_0

    .line 885
    iget v0, p0, Lflipboard/gui/FLStaticTextView;->E:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/gui/FLStaticTextView;->E:I

    .line 886
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 887
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLStaticTextView;->c(II)V

    .line 888
    iget-wide v2, p0, Lflipboard/gui/FLStaticTextView;->F:J

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long v0, v4, v0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lflipboard/gui/FLStaticTextView;->F:J

    .line 892
    :goto_0
    return-void

    .line 890
    :cond_0
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLStaticTextView;->c(II)V

    goto :goto_0
.end method

.method public setMaxLines(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 327
    iput v0, p0, Lflipboard/gui/FLStaticTextView;->q:I

    .line 328
    iput v0, p0, Lflipboard/gui/FLStaticTextView;->p:I

    .line 329
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLStaticTextView;->r:Ljava/util/List;

    .line 330
    iput p1, p0, Lflipboard/gui/FLStaticTextView;->k:I

    .line 331
    return-void
.end method

.method public setRtlAlignment(Z)V
    .locals 1

    .prologue
    .line 1041
    iput-boolean p1, p0, Lflipboard/gui/FLStaticTextView;->A:Z

    .line 1042
    iget-boolean v0, p0, Lflipboard/gui/FLStaticTextView;->A:Z

    if-eqz v0, :cond_0

    .line 1043
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/FLStaticTextView;->B:Z

    .line 1045
    :cond_0
    return-void
.end method

.method public setShadowLayer(FFFI)V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 305
    return-void
.end method

.method public setText(I)V
    .locals 1

    .prologue
    .line 243
    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lflipboard/gui/FLStaticTextView;->a(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 249
    return-void
.end method

.method public setTextColor(I)V
    .locals 0

    .prologue
    .line 291
    iput p1, p0, Lflipboard/gui/FLStaticTextView;->v:I

    .line 292
    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->invalidate()V

    .line 293
    return-void
.end method

.method public setTextColorResource(I)V
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    .line 297
    return-void
.end method

.method public setTextSize(I)V
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lflipboard/gui/FLStaticTextView;->a(II)V

    .line 313
    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 287
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/FLStaticTextView;->e:F

    .line 288
    return-void
.end method
