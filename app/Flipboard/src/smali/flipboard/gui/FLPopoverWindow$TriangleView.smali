.class Lflipboard/gui/FLPopoverWindow$TriangleView;
.super Landroid/view/View;
.source "FLPopoverWindow.java"


# instance fields
.field a:I

.field b:Landroid/graphics/Paint;

.field final synthetic c:Lflipboard/gui/FLPopoverWindow;


# direct methods
.method private constructor <init>(Lflipboard/gui/FLPopoverWindow;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 424
    iput-object p1, p0, Lflipboard/gui/FLPopoverWindow$TriangleView;->c:Lflipboard/gui/FLPopoverWindow;

    .line 425
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 426
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLPopoverWindow$TriangleView;->b:Landroid/graphics/Paint;

    .line 427
    return-void
.end method

.method public constructor <init>(Lflipboard/gui/FLPopoverWindow;Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 431
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLPopoverWindow$TriangleView;-><init>(Lflipboard/gui/FLPopoverWindow;Landroid/content/Context;)V

    .line 432
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 439
    iget-object v0, p0, Lflipboard/gui/FLPopoverWindow$TriangleView;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 440
    iget-object v0, p0, Lflipboard/gui/FLPopoverWindow$TriangleView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 441
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 450
    invoke-virtual {p0}, Lflipboard/gui/FLPopoverWindow$TriangleView;->getMeasuredWidth()I

    move-result v1

    .line 451
    invoke-virtual {p0}, Lflipboard/gui/FLPopoverWindow$TriangleView;->getMeasuredHeight()I

    move-result v2

    .line 454
    const/4 v0, 0x0

    .line 455
    iget v3, p0, Lflipboard/gui/FLPopoverWindow$TriangleView;->a:I

    if-eqz v3, :cond_0

    .line 456
    const/4 v0, 0x1

    .line 457
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 458
    iget v3, p0, Lflipboard/gui/FLPopoverWindow$TriangleView;->a:I

    int-to-float v3, v3

    int-to-float v4, v1

    div-float/2addr v4, v7

    int-to-float v5, v2

    div-float/2addr v5, v7

    invoke-virtual {p1, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 462
    :cond_0
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 463
    int-to-float v4, v2

    invoke-virtual {v3, v6, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 464
    int-to-float v4, v1

    int-to-float v2, v2

    invoke-virtual {v3, v4, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 465
    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v3, v1, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 466
    invoke-virtual {v3}, Landroid/graphics/Path;->close()V

    .line 467
    iget-object v1, p0, Lflipboard/gui/FLPopoverWindow$TriangleView;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 469
    if-eqz v0, :cond_1

    .line 470
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 472
    :cond_1
    return-void
.end method
