.class Lflipboard/gui/ViewWithDrawerLayout$AnimatorListener;
.super Ljava/lang/Object;
.source "ViewWithDrawerLayout.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:Lflipboard/gui/ViewWithDrawerLayout;


# direct methods
.method constructor <init>(Lflipboard/gui/ViewWithDrawerLayout;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lflipboard/gui/ViewWithDrawerLayout$AnimatorListener;->a:Lflipboard/gui/ViewWithDrawerLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 172
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout$AnimatorListener;->a:Lflipboard/gui/ViewWithDrawerLayout;

    invoke-static {v0}, Lflipboard/gui/ViewWithDrawerLayout;->a(Lflipboard/gui/ViewWithDrawerLayout;)Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout$AnimatorListener;->a:Lflipboard/gui/ViewWithDrawerLayout;

    invoke-static {v0}, Lflipboard/gui/ViewWithDrawerLayout;->b(Lflipboard/gui/ViewWithDrawerLayout;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lflipboard/gui/ViewWithDrawerLayout$AnimatorListener;->a:Lflipboard/gui/ViewWithDrawerLayout;

    invoke-static {v1}, Lflipboard/gui/ViewWithDrawerLayout;->b(Lflipboard/gui/ViewWithDrawerLayout;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 174
    iget-object v1, p0, Lflipboard/gui/ViewWithDrawerLayout$AnimatorListener;->a:Lflipboard/gui/ViewWithDrawerLayout;

    invoke-static {v1}, Lflipboard/gui/ViewWithDrawerLayout;->a(Lflipboard/gui/ViewWithDrawerLayout;)Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;

    move-result-object v1

    int-to-float v2, v0

    iget-object v3, p0, Lflipboard/gui/ViewWithDrawerLayout$AnimatorListener;->a:Lflipboard/gui/ViewWithDrawerLayout;

    invoke-static {v3}, Lflipboard/gui/ViewWithDrawerLayout;->b(Lflipboard/gui/ViewWithDrawerLayout;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-interface {v1, v2, v0}, Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;->a(FI)V

    .line 176
    :cond_0
    return-void
.end method
