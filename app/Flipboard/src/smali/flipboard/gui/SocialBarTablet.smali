.class public Lflipboard/gui/SocialBarTablet;
.super Landroid/widget/LinearLayout;
.source "SocialBarTablet.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;


# static fields
.field public static final a:Lflipboard/util/Log;


# instance fields
.field public b:Lflipboard/objs/FeedItem;

.field public c:Lflipboard/objs/FeedItem;

.field public d:Lflipboard/objs/ConfigService;

.field public e:Lflipboard/service/Section;

.field public f:Lflipboard/gui/actionbar/FLActionBar;

.field public g:Z

.field public h:Lflipboard/objs/FeedItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-string v0, "social-bar"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/SocialBarTablet;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 12

    .prologue
    const v11, 0x7f0a00a3

    const v8, 0x7f02005a

    const/4 v4, 0x0

    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 115
    iget-object v0, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v1, p0, Lflipboard/gui/SocialBarTablet;->e:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v5

    invoke-virtual {v5}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eq v5, v0, :cond_3

    iget-object v0, v5, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0a0171

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-static {v6, v9}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    invoke-virtual {v6, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    const v1, 0x7f0a0173

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLLabelTextView;

    const v2, 0x7f0a0174

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLImageView;

    const v3, 0x7f0a0175

    invoke-virtual {v6, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lflipboard/gui/FLLabelTextView;

    iget-object v7, v5, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v7, :cond_7

    iget-object v7, v5, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v7}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_7

    iget-object v7, v5, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v7}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v5}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v5, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lflipboard/objs/ConfigService;->k:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lflipboard/objs/ConfigService;->k:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    :cond_0
    iget-object v0, v5, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v0, :cond_17

    iget-object v0, v5, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_17

    iget-object v0, v5, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    const-string v7, "magazine"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    if-eqz v0, :cond_2

    iget-object v1, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v6}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d01ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v0, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    aput-object v0, v2, v9

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-virtual {v6, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    :cond_3
    iget-object v0, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v3

    .line 120
    const v0, 0x7f0a0177

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 122
    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 123
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 127
    iget-object v1, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->ag()Z

    move-result v5

    .line 128
    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v2

    .line 130
    if-eqz v5, :cond_9

    .line 131
    iget-object v1, v3, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v1, :cond_8

    iget-object v1, v3, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v1}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 132
    iget-object v1, p0, Lflipboard/gui/SocialBarTablet;->h:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v1}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    move-object v1, v2

    .line 165
    :goto_2
    if-eqz v1, :cond_4

    .line 166
    const v0, 0x7f0a00a6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 169
    :cond_4
    const v0, 0x7f0a00a7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 171
    if-eqz v5, :cond_f

    .line 172
    invoke-static {v0, v9}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 173
    iget-object v1, p0, Lflipboard/gui/SocialBarTablet;->d:Lflipboard/objs/ConfigService;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lflipboard/gui/SocialBarTablet;->d:Lflipboard/objs/ConfigService;

    iget-object v1, v1, Lflipboard/objs/ConfigService;->k:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 174
    iget-object v1, p0, Lflipboard/gui/SocialBarTablet;->d:Lflipboard/objs/ConfigService;

    iget-object v1, v1, Lflipboard/objs/ConfigService;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 182
    :goto_3
    const v0, 0x7f0a0178

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 185
    iget-object v1, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    if-eqz v1, :cond_14

    iget-object v1, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_14

    iget-object v1, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->bv:Ljava/lang/String;

    if-eqz v1, :cond_14

    .line 186
    iget-object v1, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    .line 188
    :goto_4
    iget-object v2, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v4, "audio"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 189
    iget-object v1, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->by:Ljava/lang/String;

    .line 195
    :goto_5
    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_13

    .line 197
    const/16 v2, 0x3e8

    invoke-static {v1, v2}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 198
    iget-object v2, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_12

    .line 199
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 200
    const-string v4, "source"

    const-string v5, "sectionLink"

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-string v4, "originSectionIdentifier"

    iget-object v5, p0, Lflipboard/gui/SocialBarTablet;->e:Lflipboard/service/Section;

    iget-object v5, v5, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v5, v5, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const-string v4, "linkType"

    const-string v5, "magazine"

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iget-object v4, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-static {v0, v1, v4, v2}, Lflipboard/util/SocialHelper;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/List;Landroid/os/Bundle;)V

    .line 212
    :goto_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 213
    invoke-virtual {p0}, Lflipboard/gui/SocialBarTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-wide v2, v3, Lflipboard/objs/FeedItem;->Z:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lflipboard/util/JavaUtil;->d(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 214
    iget-object v0, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bo:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 215
    invoke-virtual {p0}, Lflipboard/gui/SocialBarTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d01e3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    iget-object v0, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bo:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    :cond_5
    const v0, 0x7f0a00a8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 220
    const v0, 0x7f0a00ab

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 221
    const v1, 0x7f0a00aa

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLCameleonImageView;

    .line 225
    invoke-virtual {p0}, Lflipboard/gui/SocialBarTablet;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lflipboard/gui/SocialBarTablet;->c:Lflipboard/objs/FeedItem;

    invoke-static {v2, v3}, Lflipboard/gui/SocialFormatter;->a(Landroid/content/Context;Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v2

    .line 227
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_6

    .line 228
    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 229
    invoke-interface {v0, v9}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 230
    iget-object v0, p0, Lflipboard/gui/SocialBarTablet;->d:Lflipboard/objs/ConfigService;

    if-eqz v0, :cond_6

    .line 231
    iget-object v0, p0, Lflipboard/gui/SocialBarTablet;->d:Lflipboard/objs/ConfigService;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->d(Lflipboard/objs/ConfigService;)I

    move-result v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    .line 232
    invoke-static {v1, v9, v9}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLCameleonImageView;ZZ)V

    .line 233
    invoke-virtual {v1, v9}, Lflipboard/gui/FLCameleonImageView;->setVisibility(I)V

    .line 236
    :cond_6
    return-void

    .line 115
    :cond_7
    invoke-virtual {v0, v8}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    goto/16 :goto_0

    .line 135
    :cond_8
    invoke-virtual {v0, v8}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    move-object v1, v2

    goto/16 :goto_2

    .line 139
    :cond_9
    iget-object v1, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v1, :cond_16

    iget-object v1, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_16

    .line 140
    iget-object v1, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_a
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedSectionLink;

    .line 141
    iget-object v7, v1, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v7, :cond_a

    iget-object v7, v1, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    const-string v8, "author"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 148
    :goto_7
    if-eqz v1, :cond_b

    iget-object v6, v1, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    if-eqz v6, :cond_b

    iget-object v6, v1, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    invoke-virtual {v6}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_b

    .line 149
    invoke-static {v0, v9}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 150
    iget-object v6, v1, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    invoke-virtual {v6}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 155
    :goto_8
    if-eqz v1, :cond_c

    .line 156
    iget-object v0, v1, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    move-object v1, v0

    goto/16 :goto_2

    .line 152
    :cond_b
    invoke-static {v0, v10}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto :goto_8

    .line 157
    :cond_c
    iget-object v0, p0, Lflipboard/gui/SocialBarTablet;->h:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->av:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 158
    iget-object v0, p0, Lflipboard/gui/SocialBarTablet;->h:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->av:Ljava/lang/String;

    move-object v1, v0

    goto/16 :goto_2

    .line 159
    :cond_d
    iget-object v0, p0, Lflipboard/gui/SocialBarTablet;->h:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 160
    iget-object v0, p0, Lflipboard/gui/SocialBarTablet;->h:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/HttpUtil;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_2

    .line 176
    :cond_e
    invoke-static {v0, v10}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_3

    .line 179
    :cond_f
    invoke-static {v0, v10}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_3

    .line 190
    :cond_10
    if-eqz v1, :cond_11

    iget-object v2, v1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v4, "audio"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 191
    iget-object v1, v1, Lflipboard/objs/FeedItem;->by:Ljava/lang/String;

    goto/16 :goto_5

    .line 193
    :cond_11
    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->D()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    .line 205
    :cond_12
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 209
    :cond_13
    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    :cond_14
    move-object v1, v4

    goto/16 :goto_4

    :cond_15
    move-object v1, v2

    goto/16 :goto_2

    :cond_16
    move-object v1, v4

    goto :goto_7

    :cond_17
    move-object v0, v4

    goto/16 :goto_1
.end method

.method public final a(Lflipboard/objs/HasCommentaryItem;)V
    .locals 0

    .prologue
    .line 316
    invoke-virtual {p0, p0}, Lflipboard/gui/SocialBarTablet;->a(Landroid/view/View;)V

    .line 317
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 332
    iget-boolean v0, p0, Lflipboard/gui/SocialBarTablet;->g:Z

    if-eq v0, p1, :cond_0

    .line 333
    iput-boolean p1, p0, Lflipboard/gui/SocialBarTablet;->g:Z

    .line 334
    invoke-virtual {p0}, Lflipboard/gui/SocialBarTablet;->invalidate()V

    .line 336
    :cond_0
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 353
    iget-boolean v0, p0, Lflipboard/gui/SocialBarTablet;->g:Z

    if-eqz v0, :cond_0

    .line 354
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 355
    iget-object v0, p0, Lflipboard/gui/SocialBarTablet;->f:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->getLeft()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lflipboard/gui/SocialBarTablet;->f:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBar;->getTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 356
    iget-object v0, p0, Lflipboard/gui/SocialBarTablet;->f:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0, p1}, Lflipboard/gui/actionbar/FLActionBar;->draw(Landroid/graphics/Canvas;)V

    .line 357
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 361
    :goto_0
    return-void

    .line 359
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public getShowOnlyActionBar()Z
    .locals 1

    .prologue
    .line 340
    iget-boolean v0, p0, Lflipboard/gui/SocialBarTablet;->g:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 311
    iget-object v1, p0, Lflipboard/gui/SocialBarTablet;->e:Lflipboard/service/Section;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v2, p0, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    sget-object v3, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->a:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    invoke-static {p1, v1, v0, v2, v3}, Lflipboard/util/SocialHelper;->a(Landroid/view/View;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/FeedItem;Lflipboard/objs/UsageEventV2$SocialCardNavFrom;)V

    .line 312
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lflipboard/gui/SocialBarTablet;->c:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lflipboard/gui/SocialBarTablet;->c:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedItem;->b(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 324
    :cond_0
    iget-object v0, p0, Lflipboard/gui/SocialBarTablet;->h:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_1

    .line 325
    iget-object v0, p0, Lflipboard/gui/SocialBarTablet;->h:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedItem;->b(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 327
    :cond_1
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 328
    return-void
.end method
