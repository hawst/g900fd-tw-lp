.class public Lflipboard/gui/SocialBarTablet$StatusContainer;
.super Landroid/widget/LinearLayout;
.source "SocialBarTablet.java"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 373
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 374
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 378
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 379
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 383
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 386
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 388
    const v1, 0x7f0a0177

    invoke-virtual {p0, v1}, Lflipboard/gui/SocialBarTablet$StatusContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/SocialBarTablet$StatusContainer;->a:Landroid/view/View;

    .line 389
    const v1, 0x7f0a0179

    invoke-virtual {p0, v1}, Lflipboard/gui/SocialBarTablet$StatusContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/SocialBarTablet$StatusContainer;->b:Landroid/view/View;

    .line 390
    const v1, 0x7f0a0171

    invoke-virtual {p0, v1}, Lflipboard/gui/SocialBarTablet$StatusContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/SocialBarTablet$StatusContainer;->c:Landroid/view/View;

    .line 391
    const v1, 0x7f0a004c

    invoke-virtual {p0, v1}, Lflipboard/gui/SocialBarTablet$StatusContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/SocialBarTablet$StatusContainer;->d:Landroid/view/View;

    .line 392
    const v1, 0x7f0a0170

    invoke-virtual {p0, v1}, Lflipboard/gui/SocialBarTablet$StatusContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/SocialBarTablet$StatusContainer;->e:Landroid/view/View;

    .line 394
    iget-object v1, p0, Lflipboard/gui/SocialBarTablet$StatusContainer;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 395
    iget-object v1, p0, Lflipboard/gui/SocialBarTablet$StatusContainer;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 396
    int-to-float v0, v0

    invoke-virtual {p0}, Lflipboard/gui/SocialBarTablet$StatusContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09011e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 399
    :cond_0
    iget-object v1, p0, Lflipboard/gui/SocialBarTablet$StatusContainer;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 400
    iget-object v1, p0, Lflipboard/gui/SocialBarTablet$StatusContainer;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 401
    iget-object v1, p0, Lflipboard/gui/SocialBarTablet$StatusContainer;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 403
    iget-object v1, p0, Lflipboard/gui/SocialBarTablet$StatusContainer;->a:Landroid/view/View;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v1, v0, p2}, Landroid/view/View;->measure(II)V

    .line 405
    return-void
.end method
