.class Lflipboard/gui/FLBitmapView$9;
.super Landroid/os/AsyncTask;
.source "FLBitmapView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/gui/FLBitmapView;


# direct methods
.method constructor <init>(Lflipboard/gui/FLBitmapView;)V
    .locals 0

    .prologue
    .line 1065
    iput-object p1, p0, Lflipboard/gui/FLBitmapView$9;->a:Lflipboard/gui/FLBitmapView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1070
    iget-object v5, p0, Lflipboard/gui/FLBitmapView$9;->a:Lflipboard/gui/FLBitmapView;

    iget-object v0, v5, Lflipboard/gui/FLBitmapView;->o:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v6

    iget-object v0, v5, Lflipboard/gui/FLBitmapView;->n:Lflipboard/io/Download;

    if-eqz v0, :cond_2

    iget-object v0, v5, Lflipboard/gui/FLBitmapView;->m:Landroid/graphics/Movie;

    if-nez v0, :cond_2

    sget-object v0, Lflipboard/gui/FLBitmapView;->d:Ljava/util/HashMap;

    iget-object v1, v5, Lflipboard/gui/FLBitmapView;->n:Lflipboard/io/Download;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/Reference;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Movie;

    :goto_0
    if-nez v0, :cond_1

    iget-object v0, v5, Lflipboard/gui/FLBitmapView;->n:Lflipboard/io/Download;

    iget-object v0, v0, Lflipboard/io/Download;->f:Lflipboard/io/Download$Data;

    :try_start_0
    invoke-virtual {v0}, Lflipboard/io/Download$Data;->d()[B

    move-result-object v1

    if-eqz v1, :cond_3

    array-length v0, v1

    :goto_1
    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Landroid/graphics/Movie;->decodeByteArray([BII)Landroid/graphics/Movie;

    move-result-object v2

    sget-object v7, Lflipboard/gui/FLBitmapView;->d:Ljava/util/HashMap;

    monitor-enter v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    sget-object v0, Lflipboard/gui/FLBitmapView;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/Download;

    sget-object v1, Lflipboard/gui/FLBitmapView;->d:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/Reference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {v8, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v7

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1

    :catch_0
    move-exception v0

    sget-object v0, Lflipboard/gui/FLBitmapView;->a:Lflipboard/util/Log;

    move-object v0, v3

    :cond_1
    :goto_3
    if-eqz v0, :cond_2

    iget-object v1, v5, Lflipboard/gui/FLBitmapView;->o:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-ne v1, v6, :cond_2

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/FLBitmapView$6;

    invoke-direct {v2, v5, v6, v0}, Lflipboard/gui/FLBitmapView$6;-><init>(Lflipboard/gui/FLBitmapView;ILandroid/graphics/Movie;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 1071
    :cond_2
    return-object v3

    :cond_3
    move v0, v4

    .line 1070
    goto :goto_1

    :cond_4
    :try_start_3
    invoke-virtual {v8}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/Download;

    sget-object v8, Lflipboard/gui/FLBitmapView;->d:Ljava/util/HashMap;

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_5
    sget-object v0, Lflipboard/gui/FLBitmapView;->d:Ljava/util/HashMap;

    iget-object v1, v5, Lflipboard/gui/FLBitmapView;->n:Lflipboard/io/Download;

    new-instance v8, Ljava/lang/ref/SoftReference;

    invoke-direct {v8, v2}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v0, v2

    goto :goto_3

    :catch_1
    move-exception v0

    sget-object v0, Lflipboard/gui/FLBitmapView;->a:Lflipboard/util/Log;

    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    invoke-virtual {v0, v4}, Lflipboard/io/BitmapManager;->c(I)V

    iget-object v0, v5, Lflipboard/gui/FLBitmapView;->n:Lflipboard/io/Download;

    iput-object v3, v5, Lflipboard/gui/FLBitmapView;->n:Lflipboard/io/Download;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/FLBitmapView$5;

    invoke-direct {v2, v5, v0}, Lflipboard/gui/FLBitmapView$5;-><init>(Lflipboard/gui/FLBitmapView;Lflipboard/io/Download;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    move-object v0, v3

    goto :goto_3

    :cond_6
    move-object v0, v3

    goto/16 :goto_0
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1077
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView$9;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1078
    iget-object v0, p0, Lflipboard/gui/FLBitmapView$9;->a:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0}, Lflipboard/gui/FLBitmapView;->invalidate()V

    .line 1079
    iget-object v0, p0, Lflipboard/gui/FLBitmapView$9;->a:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0}, Lflipboard/gui/FLBitmapView;->d()V

    .line 1080
    iget-object v0, p0, Lflipboard/gui/FLBitmapView$9;->a:Lflipboard/gui/FLBitmapView;

    invoke-static {v0}, Lflipboard/gui/FLBitmapView;->g(Lflipboard/gui/FLBitmapView;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1081
    sget-object v0, Lflipboard/gui/FLBitmapView;->a:Lflipboard/util/Log;

    const-string v1, "Done loading movie, but according to the boolean we were already done"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1084
    :cond_0
    return-void
.end method
