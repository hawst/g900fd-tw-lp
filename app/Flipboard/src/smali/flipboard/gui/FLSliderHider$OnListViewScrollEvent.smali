.class public Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;
.super Ljava/lang/Object;
.source "FLSliderHider.java"


# instance fields
.field private final a:I

.field private final b:Landroid/util/SparseIntArray;


# direct methods
.method public constructor <init>(Landroid/widget/AbsListView;II)V
    .locals 4

    .prologue
    .line 411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 412
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iput v0, p0, Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;->a:I

    .line 413
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0, p3}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;->b:Landroid/util/SparseIntArray;

    .line 414
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    .line 415
    iget-object v1, p0, Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;->b:Landroid/util/SparseIntArray;

    add-int v2, p2, v0

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 414
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 417
    :cond_0
    return-void
.end method

.method static synthetic a(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)I
    .locals 1

    .prologue
    .line 398
    iget v0, p0, Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;->a:I

    return v0
.end method

.method static synthetic b(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)Landroid/util/SparseIntArray;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;->b:Landroid/util/SparseIntArray;

    return-object v0
.end method
