.class public Lflipboard/gui/flipping/SinglePage;
.super Ljava/lang/Object;
.source "SinglePage.java"


# static fields
.field public static M:F

.field private static final N:[F

.field private static final O:[F

.field private static final P:Ljava/nio/FloatBuffer;

.field private static final Q:Ljava/nio/FloatBuffer;

.field protected static final h:[F

.field protected static final i:Ljava/nio/FloatBuffer;

.field protected static final j:[F

.field protected static final k:Ljava/nio/FloatBuffer;

.field protected static final l:Ljava/nio/FloatBuffer;

.field protected static final m:Ljava/nio/FloatBuffer;

.field public static final o:Lflipboard/util/Log;


# instance fields
.field public A:Z

.field public B:Z

.field public C:Z

.field public D:Z

.field E:I

.field F:Z

.field G:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

.field H:Z

.field protected I:J

.field protected J:F

.field K:F

.field L:F

.field private R:F

.field private S:J

.field protected a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

.field protected b:F

.field protected c:Lflipboard/gui/flipping/FlippingBitmap;

.field protected d:[I

.field protected final e:Lflipboard/gui/flipping/FlipTransitionBase;

.field protected f:I

.field protected g:I

.field n:Z

.field protected p:F

.field protected q:F

.field protected r:I

.field protected s:I

.field protected t:I

.field volatile u:Z

.field public v:I

.field public w:Z

.field public x:I

.field y:I

.field public z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x20

    const/16 v2, 0x10

    const/16 v1, 0xc

    .line 32
    new-array v0, v1, [F

    sput-object v0, Lflipboard/gui/flipping/SinglePage;->h:[F

    .line 34
    new-array v0, v1, [F

    sput-object v0, Lflipboard/gui/flipping/SinglePage;->j:[F

    .line 43
    const-string v0, "flipper"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/SinglePage;->o:Lflipboard/util/Log;

    .line 66
    new-array v0, v2, [F

    sput-object v0, Lflipboard/gui/flipping/SinglePage;->N:[F

    .line 67
    new-array v0, v2, [F

    sput-object v0, Lflipboard/gui/flipping/SinglePage;->O:[F

    .line 75
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lflipboard/gui/flipping/SinglePage;->N:[F

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 76
    sget-object v1, Lflipboard/gui/flipping/SinglePage;->N:[F

    sget-object v2, Lflipboard/gui/flipping/SinglePage;->O:[F

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v2, v0

    aput v3, v1, v0

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    :cond_0
    sget-object v0, Lflipboard/gui/flipping/SinglePage;->N:[F

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 79
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 80
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/SinglePage;->P:Ljava/nio/FloatBuffer;

    .line 82
    sget-object v0, Lflipboard/gui/flipping/SinglePage;->N:[F

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 83
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 84
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/SinglePage;->Q:Ljava/nio/FloatBuffer;

    .line 86
    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 87
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 88
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/SinglePage;->l:Ljava/nio/FloatBuffer;

    .line 90
    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 91
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 92
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/SinglePage;->m:Ljava/nio/FloatBuffer;

    .line 94
    sget-object v0, Lflipboard/gui/flipping/SinglePage;->h:[F

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 95
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 96
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/SinglePage;->i:Ljava/nio/FloatBuffer;

    .line 98
    sget-object v0, Lflipboard/gui/flipping/SinglePage;->j:[F

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 99
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 100
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/SinglePage;->k:Ljava/nio/FloatBuffer;

    .line 584
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 585
    const v0, 0x3dcccccd    # 0.1f

    sput v0, Lflipboard/gui/flipping/SinglePage;->M:F

    .line 589
    :goto_1
    return-void

    .line 587
    :cond_1
    const v0, 0x3e4ccccd    # 0.2f

    sput v0, Lflipboard/gui/flipping/SinglePage;->M:F

    goto :goto_1
.end method

.method public constructor <init>(Lflipboard/gui/flipping/FlipTransitionBase;Lflipboard/gui/flipping/OpenGLTransitionRenderer;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const v0, 0x40490fdb    # (float)Math.PI

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->b:F

    .line 27
    new-array v0, v2, [I

    iput-object v0, p0, Lflipboard/gui/flipping/SinglePage;->d:[I

    .line 53
    iput v1, p0, Lflipboard/gui/flipping/SinglePage;->v:I

    .line 55
    iput v1, p0, Lflipboard/gui/flipping/SinglePage;->x:I

    .line 56
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->y:I

    .line 61
    iput-boolean v2, p0, Lflipboard/gui/flipping/SinglePage;->B:Z

    .line 62
    iput-boolean v2, p0, Lflipboard/gui/flipping/SinglePage;->C:Z

    .line 63
    iput-boolean v1, p0, Lflipboard/gui/flipping/SinglePage;->D:Z

    .line 107
    iput-object p1, p0, Lflipboard/gui/flipping/SinglePage;->e:Lflipboard/gui/flipping/FlipTransitionBase;

    .line 108
    iput-object p2, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    .line 109
    return-void
.end method

.method private m()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/16 v6, 0x8

    const/4 v1, 0x1

    .line 125
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-boolean v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->p:Z

    if-nez v0, :cond_1

    .line 126
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->r:I

    invoke-static {v0}, Lflipboard/util/JavaUtil;->b(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->f:I

    .line 127
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->s:I

    invoke-static {v0}, Lflipboard/util/JavaUtil;->b(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->g:I

    .line 128
    iput-boolean v1, p0, Lflipboard/gui/flipping/SinglePage;->F:Z

    .line 145
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lflipboard/gui/flipping/SinglePage;->D:Z

    if-eqz v0, :cond_3

    .line 146
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->t:I

    int-to-float v0, v0

    iget-object v1, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v1, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->k:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    div-float v1, v0, v1

    .line 147
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->s:I

    int-to-float v0, v0

    iget-object v2, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v2, v2, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->k:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 156
    :goto_1
    iget-boolean v2, p0, Lflipboard/gui/flipping/SinglePage;->n:Z

    if-eqz v2, :cond_4

    .line 157
    new-array v3, v6, [F

    fill-array-data v3, :array_0

    .line 163
    new-array v2, v6, [F

    fill-array-data v2, :array_1

    .line 186
    :goto_2
    if-ge v4, v6, :cond_6

    .line 187
    rem-int/lit8 v5, v4, 0x2

    if-nez v5, :cond_5

    .line 189
    aget v5, v3, v4

    mul-float/2addr v5, v1

    aput v5, v3, v4

    .line 190
    aget v5, v2, v4

    mul-float/2addr v5, v1

    aput v5, v2, v4

    .line 186
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 130
    :cond_1
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->r:I

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->f:I

    .line 131
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->s:I

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->g:I

    .line 134
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->f:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    .line 135
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->f:I

    .line 136
    iput-boolean v1, p0, Lflipboard/gui/flipping/SinglePage;->F:Z

    .line 138
    :cond_2
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->g:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 139
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->g:I

    .line 140
    iput-boolean v1, p0, Lflipboard/gui/flipping/SinglePage;->F:Z

    goto :goto_0

    .line 149
    :cond_3
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->r:I

    int-to-float v0, v0

    iget v1, p0, Lflipboard/gui/flipping/SinglePage;->f:I

    int-to-float v1, v1

    div-float v1, v0, v1

    .line 150
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->s:I

    int-to-float v0, v0

    iget v2, p0, Lflipboard/gui/flipping/SinglePage;->g:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    goto :goto_1

    .line 170
    :cond_4
    new-array v3, v6, [F

    fill-array-data v3, :array_2

    .line 176
    new-array v2, v6, [F

    fill-array-data v2, :array_3

    goto :goto_2

    .line 193
    :cond_5
    aget v5, v3, v4

    mul-float/2addr v5, v0

    aput v5, v3, v4

    .line 194
    aget v5, v2, v4

    mul-float/2addr v5, v0

    aput v5, v2, v4

    goto :goto_3

    .line 201
    :cond_6
    invoke-virtual {p0}, Lflipboard/gui/flipping/SinglePage;->a()Ljava/nio/FloatBuffer;

    move-result-object v1

    .line 202
    monitor-enter v1

    .line 203
    :try_start_0
    invoke-virtual {v1, v3}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 204
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 205
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    invoke-virtual {p0}, Lflipboard/gui/flipping/SinglePage;->b()Ljava/nio/FloatBuffer;

    move-result-object v1

    .line 207
    monitor-enter v1

    .line 208
    :try_start_1
    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 209
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 210
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 205
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 210
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 157
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
        0x0
        0x3f000000    # 0.5f
    .end array-data

    .line 163
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 170
    :array_2
    .array-data 4
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data

    .line 176
    :array_3
    .array-data 4
        0x3f000000    # 0.5f
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private n()F
    .locals 1

    .prologue
    .line 290
    invoke-virtual {p0}, Lflipboard/gui/flipping/SinglePage;->j()Lflipboard/gui/flipping/SinglePage;

    move-result-object v0

    .line 291
    if-eqz v0, :cond_0

    .line 292
    iget v0, v0, Lflipboard/gui/flipping/SinglePage;->b:F

    .line 296
    :goto_0
    return v0

    .line 294
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a()Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lflipboard/gui/flipping/SinglePage;->D:Z

    if-eqz v0, :cond_0

    .line 216
    sget-object v0, Lflipboard/gui/flipping/TextPage;->W:Ljava/nio/FloatBuffer;

    .line 218
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lflipboard/gui/flipping/SinglePage;->l:Ljava/nio/FloatBuffer;

    goto :goto_0
.end method

.method public a(F)V
    .locals 2

    .prologue
    .line 263
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->x:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->x:I

    .line 264
    const v0, 0x40490fdb    # (float)Math.PI

    const/4 v1, 0x0

    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 265
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->b:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 266
    :goto_0
    iput v1, p0, Lflipboard/gui/flipping/SinglePage;->b:F

    .line 268
    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->h:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 270
    invoke-virtual {p0}, Lflipboard/gui/flipping/SinglePage;->i()Lflipboard/gui/flipping/SinglePage;

    move-result-object v0

    .line 271
    if-eqz v0, :cond_0

    .line 272
    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/SinglePage;->b(F)V

    .line 275
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/gui/flipping/SinglePage;->S:J

    .line 276
    return-void

    .line 265
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;ZFIZ)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const v3, 0x3bc49ba6    # 0.006f

    const/4 v2, 0x0

    .line 603
    int-to-float v0, p4

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->J:F

    .line 604
    iput-object p1, p0, Lflipboard/gui/flipping/SinglePage;->G:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    .line 605
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v3

    if-gez v0, :cond_3

    .line 607
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->b:F

    const v1, 0x3fc90fdb

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 609
    iput v3, p0, Lflipboard/gui/flipping/SinglePage;->R:F

    .line 622
    :goto_0
    if-eqz p2, :cond_0

    .line 623
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->G:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne v0, v1, :cond_6

    .line 624
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->R:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_5

    .line 625
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->e:Lflipboard/gui/flipping/FlipTransitionBase;

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionBase;->a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;)V

    .line 626
    iput-boolean v5, p0, Lflipboard/gui/flipping/SinglePage;->H:Z

    .line 638
    :goto_1
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->e:Lflipboard/gui/flipping/FlipTransitionBase;

    iget-boolean v1, p0, Lflipboard/gui/flipping/SinglePage;->H:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionBase;->b(Z)V

    .line 641
    :cond_0
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->R:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_8

    .line 642
    const v0, 0x40490fdb    # (float)Math.PI

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->K:F

    .line 646
    :goto_2
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->b:F

    iget v1, p0, Lflipboard/gui/flipping/SinglePage;->R:F

    add-float/2addr v0, v1

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->L:F

    .line 648
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/gui/flipping/SinglePage;->I:J

    .line 650
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->G:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne v0, v1, :cond_9

    .line 651
    invoke-virtual {p0}, Lflipboard/gui/flipping/SinglePage;->j()Lflipboard/gui/flipping/SinglePage;

    move-result-object v0

    .line 655
    :goto_3
    if-eqz v0, :cond_1

    iget-boolean v1, v0, Lflipboard/gui/flipping/SinglePage;->z:Z

    if-eqz v1, :cond_1

    .line 657
    if-eqz p5, :cond_a

    .line 658
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v0, v0, Lflipboard/gui/flipping/SinglePage;->I:J

    sget v4, Lflipboard/gui/flipping/SinglePage;->M:F

    int-to-float v5, p4

    mul-float/2addr v4, v5

    float-to-int v4, v4

    int-to-long v4, v4

    add-long/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 663
    :goto_4
    iput-wide v0, p0, Lflipboard/gui/flipping/SinglePage;->I:J

    .line 665
    :cond_1
    return-void

    .line 612
    :cond_2
    const v0, -0x443b645a    # -0.006f

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->R:F

    goto :goto_0

    .line 616
    :cond_3
    cmpl-float v0, p3, v2

    if-lez v0, :cond_4

    .line 617
    const v0, 0x3dcccccd    # 0.1f

    invoke-static {p3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->R:F

    goto :goto_0

    .line 619
    :cond_4
    const v0, -0x42333333    # -0.1f

    invoke-static {p3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->R:F

    goto :goto_0

    .line 628
    :cond_5
    iput-boolean v4, p0, Lflipboard/gui/flipping/SinglePage;->H:Z

    goto :goto_1

    .line 631
    :cond_6
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->R:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_7

    .line 632
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->e:Lflipboard/gui/flipping/FlipTransitionBase;

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->b:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionBase;->a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;)V

    .line 633
    iput-boolean v5, p0, Lflipboard/gui/flipping/SinglePage;->H:Z

    goto :goto_1

    .line 635
    :cond_7
    iput-boolean v4, p0, Lflipboard/gui/flipping/SinglePage;->H:Z

    goto :goto_1

    .line 644
    :cond_8
    iput v2, p0, Lflipboard/gui/flipping/SinglePage;->K:F

    goto :goto_2

    .line 653
    :cond_9
    invoke-virtual {p0}, Lflipboard/gui/flipping/SinglePage;->i()Lflipboard/gui/flipping/SinglePage;

    move-result-object v0

    goto :goto_3

    .line 660
    :cond_a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v0, v0, Lflipboard/gui/flipping/SinglePage;->I:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_4
.end method

.method public final a(Lflipboard/gui/flipping/FlippingBitmap;)V
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const-string v1, "setBitmap"

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V

    .line 235
    if-eqz p1, :cond_2

    .line 236
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lflipboard/gui/flipping/SinglePage;->B:Z

    .line 237
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/flipping/SinglePage;->C:Z

    .line 238
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-boolean v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->q:Z

    if-eqz v0, :cond_0

    .line 239
    sget-object v0, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    invoke-virtual {v0, p1}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Lflipboard/gui/flipping/FlippingBitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 251
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 252
    :goto_0
    return-void

    .line 243
    :cond_0
    :try_start_1
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->c:Lflipboard/gui/flipping/FlippingBitmap;

    if-eqz v0, :cond_1

    .line 244
    sget-object v0, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    iget-object v1, p0, Lflipboard/gui/flipping/SinglePage;->c:Lflipboard/gui/flipping/FlippingBitmap;

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Lflipboard/gui/flipping/FlippingBitmap;)V

    .line 246
    :cond_1
    iput-object p1, p0, Lflipboard/gui/flipping/SinglePage;->c:Lflipboard/gui/flipping/FlippingBitmap;

    .line 247
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->h:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->requestRender()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 251
    :cond_2
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 22

    .prologue
    .line 312
    sget-object v1, Lflipboard/gui/flipping/SinglePage;->h:[F

    if-nez v1, :cond_0

    .line 313
    sget-object v1, Lflipboard/gui/flipping/SinglePage;->o:Lflipboard/util/Log;

    const-string v2, "SinglePage got told to draw before it was ready"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 464
    :goto_0
    return-void

    .line 324
    :cond_0
    move-object/from16 v0, p0

    iget v1, v0, Lflipboard/gui/flipping/SinglePage;->v:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lflipboard/gui/flipping/SinglePage;->v:I

    .line 325
    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 328
    move-object/from16 v0, p0

    iget v1, v0, Lflipboard/gui/flipping/SinglePage;->q:F

    move-object/from16 v0, p0

    iget v2, v0, Lflipboard/gui/flipping/SinglePage;->p:F

    neg-float v2, v2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 330
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lflipboard/gui/flipping/SinglePage;->D:Z

    if-eqz v1, :cond_1

    .line 331
    const/16 v1, 0xde1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 332
    const/16 v1, 0xde1

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v2, v2, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->i:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 343
    :goto_1
    const v1, 0x40490fdb    # (float)Math.PI

    invoke-direct/range {p0 .. p0}, Lflipboard/gui/flipping/SinglePage;->n()F

    move-result v2

    sub-float v2, v1, v2

    .line 344
    const v1, 0x40490fdb    # (float)Math.PI

    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/flipping/SinglePage;->b:F

    sub-float v3, v1, v3

    .line 346
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lflipboard/gui/flipping/SinglePage;->n:Z

    if-eqz v1, :cond_3

    .line 347
    move-object/from16 v0, p0

    iget v1, v0, Lflipboard/gui/flipping/SinglePage;->s:I

    int-to-float v1, v1

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v1, v4

    .line 351
    :goto_2
    invoke-static {v2}, Landroid/util/FloatMath;->sin(F)F

    move-result v4

    mul-float/2addr v4, v1

    .line 352
    invoke-static {v2}, Landroid/util/FloatMath;->cos(F)F

    move-result v2

    neg-float v2, v2

    mul-float/2addr v2, v1

    .line 353
    invoke-static {v3}, Landroid/util/FloatMath;->sin(F)F

    move-result v5

    mul-float/2addr v5, v1

    .line 354
    invoke-static {v3}, Landroid/util/FloatMath;->cos(F)F

    move-result v3

    mul-float/2addr v1, v3

    .line 357
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lflipboard/gui/flipping/SinglePage;->n:Z

    if-eqz v3, :cond_4

    .line 358
    sget-object v3, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/4 v6, 0x7

    sget-object v7, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/16 v8, 0x8

    sget-object v9, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/16 v10, 0xa

    sget-object v11, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/16 v12, 0xb

    sget-object v13, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/4 v14, 0x1

    sget-object v15, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/16 v16, 0x2

    sget-object v17, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/16 v18, 0x4

    sget-object v19, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/16 v20, 0x5

    const/16 v21, 0x0

    aput v21, v19, v20

    aput v21, v17, v18

    aput v21, v15, v16

    aput v21, v13, v14

    aput v21, v11, v12

    aput v21, v9, v10

    aput v21, v7, v8

    aput v21, v3, v6

    .line 360
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/flipping/SinglePage;->r:I

    int-to-float v3, v3

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v3, v6

    .line 361
    sget-object v6, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/4 v7, 0x0

    sget-object v8, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/4 v9, 0x6

    aput v3, v8, v9

    aput v3, v6, v7

    .line 362
    sget-object v6, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/4 v7, 0x3

    sget-object v8, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/16 v9, 0x9

    neg-float v10, v3

    aput v10, v8, v9

    aput v10, v6, v7

    .line 363
    sget-object v6, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/4 v7, 0x0

    sget-object v8, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/4 v9, 0x6

    aput v3, v8, v9

    aput v3, v6, v7

    .line 364
    sget-object v6, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/4 v7, 0x3

    sget-object v8, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/16 v9, 0x9

    neg-float v3, v3

    aput v3, v8, v9

    aput v3, v6, v7

    .line 366
    sget-object v3, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/4 v6, 0x1

    aput v2, v3, v6

    sget-object v3, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/4 v6, 0x2

    aput v4, v3, v6

    .line 367
    sget-object v3, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/4 v6, 0x4

    aput v2, v3, v6

    sget-object v2, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/4 v3, 0x5

    aput v4, v2, v3

    .line 368
    sget-object v2, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/4 v3, 0x7

    neg-float v4, v1

    aput v4, v2, v3

    sget-object v2, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/16 v3, 0x8

    aput v5, v2, v3

    .line 369
    sget-object v2, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/16 v3, 0xa

    neg-float v1, v1

    aput v1, v2, v3

    sget-object v1, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/16 v2, 0xb

    aput v5, v1, v2

    .line 386
    :goto_3
    sget-object v1, Lflipboard/gui/flipping/SinglePage;->i:Ljava/nio/FloatBuffer;

    sget-object v2, Lflipboard/gui/flipping/SinglePage;->h:[F

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 387
    sget-object v1, Lflipboard/gui/flipping/SinglePage;->i:Ljava/nio/FloatBuffer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 389
    sget-object v1, Lflipboard/gui/flipping/SinglePage;->k:Ljava/nio/FloatBuffer;

    sget-object v2, Lflipboard/gui/flipping/SinglePage;->j:[F

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 390
    sget-object v1, Lflipboard/gui/flipping/SinglePage;->k:Ljava/nio/FloatBuffer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 392
    const/16 v1, 0xde1

    const/16 v2, 0x2802

    const v3, 0x46240400    # 10497.0f

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 393
    const/16 v1, 0xde1

    const/16 v2, 0x2803

    const v3, 0x46240400    # 10497.0f

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 396
    const v1, 0x8074

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    .line 397
    const v1, 0x8078

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    .line 398
    const v1, 0x8076

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    .line 400
    const/16 v1, 0x901

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glFrontFace(I)V

    .line 408
    const/high16 v2, 0x3f800000    # 1.0f

    .line 411
    const/high16 v1, 0x3f800000    # 1.0f

    .line 412
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/flipping/SinglePage;->b:F

    const v4, 0x3fc90fdb

    cmpg-float v3, v3, v4

    if-gez v3, :cond_5

    .line 413
    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/flipping/SinglePage;->b:F

    invoke-static {v3}, Landroid/util/FloatMath;->cos(F)F

    move-result v3

    const v4, 0x3f19999a    # 0.6f

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    .line 418
    :goto_4
    const/high16 v4, 0x3f800000    # 1.0f

    .line 419
    const/high16 v3, 0x3f800000    # 1.0f

    .line 420
    invoke-direct/range {p0 .. p0}, Lflipboard/gui/flipping/SinglePage;->n()F

    move-result v5

    .line 421
    const v6, 0x3fc90fdb

    cmpl-float v6, v5, v6

    if-lez v6, :cond_6

    .line 422
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v5}, Landroid/util/FloatMath;->cos(F)F

    move-result v5

    const v6, 0x3f19999a    # 0.6f

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    .line 428
    :goto_5
    sget-object v5, Lflipboard/gui/flipping/SinglePage;->N:[F

    const/4 v6, 0x0

    sget-object v7, Lflipboard/gui/flipping/SinglePage;->N:[F

    const/4 v8, 0x1

    sget-object v9, Lflipboard/gui/flipping/SinglePage;->N:[F

    const/4 v10, 0x2

    sget-object v11, Lflipboard/gui/flipping/SinglePage;->N:[F

    const/4 v12, 0x4

    sget-object v13, Lflipboard/gui/flipping/SinglePage;->N:[F

    const/4 v14, 0x5

    sget-object v15, Lflipboard/gui/flipping/SinglePage;->N:[F

    const/16 v16, 0x6

    aput v2, v15, v16

    aput v2, v13, v14

    aput v2, v11, v12

    aput v2, v9, v10

    aput v2, v7, v8

    aput v2, v5, v6

    .line 430
    sget-object v5, Lflipboard/gui/flipping/SinglePage;->N:[F

    const/16 v6, 0x8

    sget-object v7, Lflipboard/gui/flipping/SinglePage;->N:[F

    const/16 v8, 0x9

    sget-object v9, Lflipboard/gui/flipping/SinglePage;->N:[F

    const/16 v10, 0xa

    sget-object v11, Lflipboard/gui/flipping/SinglePage;->N:[F

    const/16 v12, 0xc

    sget-object v13, Lflipboard/gui/flipping/SinglePage;->N:[F

    const/16 v14, 0xd

    sget-object v15, Lflipboard/gui/flipping/SinglePage;->N:[F

    const/16 v16, 0xe

    mul-float/2addr v2, v3

    aput v2, v15, v16

    aput v2, v13, v14

    aput v2, v11, v12

    aput v2, v9, v10

    aput v2, v7, v8

    aput v2, v5, v6

    .line 432
    sget-object v2, Lflipboard/gui/flipping/SinglePage;->P:Ljava/nio/FloatBuffer;

    sget-object v3, Lflipboard/gui/flipping/SinglePage;->N:[F

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 433
    sget-object v2, Lflipboard/gui/flipping/SinglePage;->P:Ljava/nio/FloatBuffer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 434
    const/4 v2, 0x4

    const/16 v3, 0x1406

    const/4 v5, 0x0

    sget-object v6, Lflipboard/gui/flipping/SinglePage;->P:Ljava/nio/FloatBuffer;

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColorPointer(IIILjava/nio/Buffer;)V

    .line 436
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/flipping/SinglePage;->a()Ljava/nio/FloatBuffer;

    move-result-object v2

    monitor-enter v2

    .line 437
    const/4 v3, 0x3

    const/16 v5, 0x1406

    const/4 v6, 0x0

    :try_start_0
    sget-object v7, Lflipboard/gui/flipping/SinglePage;->i:Ljava/nio/FloatBuffer;

    move-object/from16 v0, p1

    invoke-interface {v0, v3, v5, v6, v7}, Ljavax/microedition/khronos/opengles/GL10;->glVertexPointer(IIILjava/nio/Buffer;)V

    .line 438
    const/4 v3, 0x2

    const/16 v5, 0x1406

    const/4 v6, 0x0

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/flipping/SinglePage;->a()Ljava/nio/FloatBuffer;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-interface {v0, v3, v5, v6, v7}, Ljavax/microedition/khronos/opengles/GL10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    .line 439
    const/4 v3, 0x5

    const/4 v5, 0x0

    sget-object v6, Lflipboard/gui/flipping/SinglePage;->h:[F

    array-length v6, v6

    div-int/lit8 v6, v6, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v3, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 440
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 444
    sget-object v2, Lflipboard/gui/flipping/SinglePage;->O:[F

    const/4 v3, 0x0

    sget-object v5, Lflipboard/gui/flipping/SinglePage;->O:[F

    const/4 v6, 0x1

    sget-object v7, Lflipboard/gui/flipping/SinglePage;->O:[F

    const/4 v8, 0x2

    sget-object v9, Lflipboard/gui/flipping/SinglePage;->O:[F

    const/4 v10, 0x4

    sget-object v11, Lflipboard/gui/flipping/SinglePage;->O:[F

    const/4 v12, 0x5

    sget-object v13, Lflipboard/gui/flipping/SinglePage;->O:[F

    const/4 v14, 0x6

    mul-float/2addr v1, v4

    aput v1, v13, v14

    aput v1, v11, v12

    aput v1, v9, v10

    aput v1, v7, v8

    aput v1, v5, v6

    aput v1, v2, v3

    .line 446
    sget-object v1, Lflipboard/gui/flipping/SinglePage;->O:[F

    const/16 v2, 0x8

    sget-object v3, Lflipboard/gui/flipping/SinglePage;->O:[F

    const/16 v5, 0x9

    sget-object v6, Lflipboard/gui/flipping/SinglePage;->O:[F

    const/16 v7, 0xa

    sget-object v8, Lflipboard/gui/flipping/SinglePage;->O:[F

    const/16 v9, 0xc

    sget-object v10, Lflipboard/gui/flipping/SinglePage;->O:[F

    const/16 v11, 0xd

    sget-object v12, Lflipboard/gui/flipping/SinglePage;->O:[F

    const/16 v13, 0xe

    aput v4, v12, v13

    aput v4, v10, v11

    aput v4, v8, v9

    aput v4, v6, v7

    aput v4, v3, v5

    aput v4, v1, v2

    .line 447
    sget-object v1, Lflipboard/gui/flipping/SinglePage;->Q:Ljava/nio/FloatBuffer;

    sget-object v2, Lflipboard/gui/flipping/SinglePage;->O:[F

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 448
    sget-object v1, Lflipboard/gui/flipping/SinglePage;->Q:Ljava/nio/FloatBuffer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 449
    const/4 v1, 0x4

    const/16 v2, 0x1406

    const/4 v3, 0x0

    sget-object v4, Lflipboard/gui/flipping/SinglePage;->Q:Ljava/nio/FloatBuffer;

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColorPointer(IIILjava/nio/Buffer;)V

    .line 451
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/flipping/SinglePage;->b()Ljava/nio/FloatBuffer;

    move-result-object v2

    monitor-enter v2

    .line 452
    const/4 v1, 0x3

    const/16 v3, 0x1406

    const/4 v4, 0x0

    :try_start_1
    sget-object v5, Lflipboard/gui/flipping/SinglePage;->k:Ljava/nio/FloatBuffer;

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glVertexPointer(IIILjava/nio/Buffer;)V

    .line 453
    const/4 v1, 0x2

    const/16 v3, 0x1406

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/flipping/SinglePage;->b()Ljava/nio/FloatBuffer;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    .line 454
    const/4 v1, 0x5

    const/4 v3, 0x0

    sget-object v4, Lflipboard/gui/flipping/SinglePage;->j:[F

    array-length v4, v4

    div-int/lit8 v4, v4, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 455
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 458
    const v1, 0x8074

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    .line 459
    const v1, 0x8078

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    .line 460
    const v1, 0x8076

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    .line 463
    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_0

    .line 333
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/flipping/SinglePage;->d:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    if-eqz v1, :cond_2

    .line 334
    const/16 v1, 0xde1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 336
    const/16 v1, 0xde1

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/SinglePage;->d:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    goto/16 :goto_1

    .line 339
    :cond_2
    const/16 v1, 0xde1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto/16 :goto_1

    .line 349
    :cond_3
    move-object/from16 v0, p0

    iget v1, v0, Lflipboard/gui/flipping/SinglePage;->r:I

    int-to-float v1, v1

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v1, v4

    goto/16 :goto_2

    .line 371
    :cond_4
    sget-object v3, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/4 v6, 0x6

    sget-object v7, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/16 v8, 0x8

    sget-object v9, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/16 v10, 0x9

    sget-object v11, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/16 v12, 0xb

    sget-object v13, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/4 v14, 0x0

    sget-object v15, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/16 v16, 0x2

    sget-object v17, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/16 v18, 0x3

    sget-object v19, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/16 v20, 0x5

    const/16 v21, 0x0

    aput v21, v19, v20

    aput v21, v17, v18

    aput v21, v15, v16

    aput v21, v13, v14

    aput v21, v11, v12

    aput v21, v9, v10

    aput v21, v7, v8

    aput v21, v3, v6

    .line 374
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/flipping/SinglePage;->s:I

    int-to-float v3, v3

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v3, v6

    .line 375
    sget-object v6, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/4 v7, 0x1

    sget-object v8, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/4 v9, 0x7

    aput v3, v8, v9

    aput v3, v6, v7

    .line 376
    sget-object v6, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/4 v7, 0x4

    sget-object v8, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/16 v9, 0xa

    neg-float v10, v3

    aput v10, v8, v9

    aput v10, v6, v7

    .line 377
    sget-object v6, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/4 v7, 0x1

    sget-object v8, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/4 v9, 0x7

    aput v3, v8, v9

    aput v3, v6, v7

    .line 378
    sget-object v6, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/4 v7, 0x4

    sget-object v8, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/16 v9, 0xa

    neg-float v3, v3

    aput v3, v8, v9

    aput v3, v6, v7

    .line 380
    sget-object v3, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/4 v6, 0x0

    neg-float v7, v2

    aput v7, v3, v6

    sget-object v3, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/4 v6, 0x2

    aput v4, v3, v6

    .line 381
    sget-object v3, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/4 v6, 0x3

    neg-float v2, v2

    aput v2, v3, v6

    sget-object v2, Lflipboard/gui/flipping/SinglePage;->h:[F

    const/4 v3, 0x5

    aput v4, v2, v3

    .line 382
    sget-object v2, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/4 v3, 0x6

    aput v1, v2, v3

    sget-object v2, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/16 v3, 0x8

    aput v5, v2, v3

    .line 383
    sget-object v2, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/16 v3, 0x9

    aput v1, v2, v3

    sget-object v1, Lflipboard/gui/flipping/SinglePage;->j:[F

    const/16 v2, 0xb

    aput v5, v1, v2

    goto/16 :goto_3

    .line 415
    :cond_5
    const/high16 v1, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/flipping/SinglePage;->b:F

    invoke-static {v3}, Landroid/util/FloatMath;->sin(F)F

    move-result v3

    const v4, 0x3d99999a    # 0.075f

    mul-float/2addr v3, v4

    sub-float/2addr v1, v3

    goto/16 :goto_4

    .line 424
    :cond_6
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v5}, Landroid/util/FloatMath;->sin(F)F

    move-result v5

    const v6, 0x3d99999a    # 0.075f

    mul-float/2addr v5, v6

    sub-float/2addr v3, v5

    goto/16 :goto_5

    .line 440
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 455
    :catchall_1
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public final a(ZIIIIFF)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 113
    iput-boolean p1, p0, Lflipboard/gui/flipping/SinglePage;->n:Z

    .line 114
    iput p2, p0, Lflipboard/gui/flipping/SinglePage;->r:I

    .line 115
    iput p3, p0, Lflipboard/gui/flipping/SinglePage;->s:I

    .line 116
    iput p4, p0, Lflipboard/gui/flipping/SinglePage;->t:I

    .line 118
    neg-int v0, p5

    int-to-float v0, v0

    div-float/2addr v0, v2

    int-to-float v1, p3

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    add-float/2addr v0, p7

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->p:F

    .line 119
    neg-int v0, p4

    int-to-float v0, v0

    div-float/2addr v0, v2

    int-to-float v1, p2

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    add-float/2addr v0, p6

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->q:F

    .line 120
    invoke-direct {p0}, Lflipboard/gui/flipping/SinglePage;->m()V

    .line 121
    return-void
.end method

.method b()Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 224
    iget-boolean v0, p0, Lflipboard/gui/flipping/SinglePage;->D:Z

    if-eqz v0, :cond_0

    .line 225
    sget-object v0, Lflipboard/gui/flipping/TextPage;->X:Ljava/nio/FloatBuffer;

    .line 227
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lflipboard/gui/flipping/SinglePage;->m:Ljava/nio/FloatBuffer;

    goto :goto_0
.end method

.method protected b(F)V
    .locals 0

    .prologue
    .line 280
    return-void
.end method

.method public final b(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 14

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 468
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 469
    invoke-direct {p0}, Lflipboard/gui/flipping/SinglePage;->m()V

    .line 470
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const-string v2, "load gl texture"

    invoke-virtual {v0, v2}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V

    .line 473
    :try_start_0
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->c:Lflipboard/gui/flipping/FlippingBitmap;

    if-eqz v0, :cond_a

    .line 474
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->g:I

    if-eqz v0, :cond_0

    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->f:I

    if-nez v0, :cond_2

    .line 475
    :cond_0
    sget-object v0, Lflipboard/gui/flipping/SinglePage;->o:Lflipboard/util/Log;

    const-string v1, "Size is 0 for page %s while trying to load a bitmap"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lflipboard/gui/flipping/SinglePage;->E:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 517
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 529
    :cond_1
    :goto_0
    return-void

    .line 478
    :cond_2
    :try_start_1
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->d:[I

    const/4 v2, 0x0

    aget v0, v0, v2

    if-nez v0, :cond_9

    .line 479
    iget-object v3, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget v4, p0, Lflipboard/gui/flipping/SinglePage;->f:I

    iget v5, p0, Lflipboard/gui/flipping/SinglePage;->g:I

    iget-object v0, v3, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_1
    add-int/lit8 v2, v0, -0x1

    if-ltz v2, :cond_5

    iget-object v0, v3, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->v:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer$Texture;

    iget v6, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer$Texture;->b:I

    if-ne v6, v4, :cond_4

    iget v6, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer$Texture;->c:I

    if-ne v6, v5, :cond_4

    iget-object v3, v3, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->v:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    sget-object v2, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a:Lflipboard/util/Log;

    iget-boolean v2, v2, Lflipboard/util/Log;->f:Z

    if-eqz v2, :cond_3

    sget-object v2, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer$Texture;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    :cond_3
    iget v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer$Texture;->a:I

    .line 480
    :goto_2
    if-eqz v0, :cond_9

    .line 481
    iget-object v1, p0, Lflipboard/gui/flipping/SinglePage;->d:[I

    const/4 v2, 0x0

    aput v0, v1, v2

    .line 485
    :goto_3
    iget-boolean v0, p0, Lflipboard/gui/flipping/SinglePage;->F:Z

    if-eqz v0, :cond_7

    .line 486
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->d:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    if-nez v0, :cond_6

    .line 487
    const/4 v0, 0x1

    iget-object v1, p0, Lflipboard/gui/flipping/SinglePage;->d:[I

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    .line 488
    const/16 v0, 0xde1

    iget-object v1, p0, Lflipboard/gui/flipping/SinglePage;->d:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 491
    const/16 v0, 0xde1

    const/16 v1, 0x2801

    const v2, 0x46180400    # 9729.0f

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 494
    const/16 v1, 0xde1

    const/4 v2, 0x0

    const/16 v3, 0x1908

    iget v4, p0, Lflipboard/gui/flipping/SinglePage;->f:I

    iget v5, p0, Lflipboard/gui/flipping/SinglePage;->g:I

    const/4 v6, 0x0

    const/16 v7, 0x1908

    const/16 v8, 0x1401

    const/4 v9, 0x0

    move-object v0, p1

    invoke-interface/range {v0 .. v9}, Ljavax/microedition/khronos/opengles/GL10;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    .line 499
    :goto_4
    const/16 v0, 0xde1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/gui/flipping/SinglePage;->c:Lflipboard/gui/flipping/FlippingBitmap;

    iget-object v4, v4, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/GLUtils;->texSubImage2D(IIIILandroid/graphics/Bitmap;)V

    .line 510
    :goto_5
    const/16 v0, 0xde1

    const/16 v1, 0x2801

    const v2, 0x46180400    # 9729.0f

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 511
    const/16 v0, 0xde1

    const/16 v1, 0x2800

    const v2, 0x46180400    # 9729.0f

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 513
    sget-object v0, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    iget-object v1, p0, Lflipboard/gui/flipping/SinglePage;->c:Lflipboard/gui/flipping/FlippingBitmap;

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Lflipboard/gui/flipping/FlippingBitmap;)V

    .line 514
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/flipping/SinglePage;->c:Lflipboard/gui/flipping/FlippingBitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v10

    .line 517
    :goto_6
    iget-object v1, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 522
    if-nez v0, :cond_1

    .line 523
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 524
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->t:I

    mul-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-interface {p1, v0, v11, v11}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 525
    invoke-virtual {p0, p1}, Lflipboard/gui/flipping/SinglePage;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 526
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 527
    sget-object v0, Lflipboard/gui/flipping/SinglePage;->o:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "creating texture took "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v12

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 479
    goto/16 :goto_1

    :cond_5
    move v0, v1

    goto/16 :goto_2

    .line 496
    :cond_6
    const/16 v0, 0xde1

    :try_start_2
    iget-object v1, p0, Lflipboard/gui/flipping/SinglePage;->d:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 517
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0

    .line 501
    :cond_7
    :try_start_3
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->d:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    if-nez v0, :cond_8

    .line 502
    const/4 v0, 0x1

    iget-object v1, p0, Lflipboard/gui/flipping/SinglePage;->d:[I

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    .line 503
    const/16 v0, 0xde1

    iget-object v1, p0, Lflipboard/gui/flipping/SinglePage;->d:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 504
    const/16 v0, 0xde1

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/gui/flipping/SinglePage;->c:Lflipboard/gui/flipping/FlippingBitmap;

    iget-object v2, v2, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    goto/16 :goto_5

    .line 506
    :cond_8
    const/16 v0, 0xde1

    iget-object v1, p0, Lflipboard/gui/flipping/SinglePage;->d:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 507
    const/16 v0, 0xde1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/gui/flipping/SinglePage;->c:Lflipboard/gui/flipping/FlippingBitmap;

    iget-object v4, v4, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/GLUtils;->texSubImage2D(IIIILandroid/graphics/Bitmap;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_5

    :cond_9
    move v10, v1

    goto/16 :goto_3

    :cond_a
    move v0, v1

    goto/16 :goto_6
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 284
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->b:F

    return v0
.end method

.method c(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 542
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->d:[I

    aput v1, v0, v1

    .line 543
    invoke-virtual {p0}, Lflipboard/gui/flipping/SinglePage;->g()V

    .line 544
    iput-boolean v1, p0, Lflipboard/gui/flipping/SinglePage;->A:Z

    .line 545
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/flipping/SinglePage;->B:Z

    .line 546
    return-void
.end method

.method cancel()V
    .locals 1

    .prologue
    .line 743
    const v0, 0x40490fdb    # (float)Math.PI

    iput v0, p0, Lflipboard/gui/flipping/SinglePage;->b:F

    .line 744
    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 301
    iget-object v1, p0, Lflipboard/gui/flipping/SinglePage;->d:[I

    aget v1, v1, v0

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->c:Lflipboard/gui/flipping/FlippingBitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final f()Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const-wide v8, 0x40091fef1645a1cbL    # 3.1405927410125734

    const-wide v6, 0x3f50624dd2f1a9fcL    # 0.001

    const/4 v1, 0x0

    .line 533
    invoke-direct {p0}, Lflipboard/gui/flipping/SinglePage;->n()F

    move-result v2

    iget v3, p0, Lflipboard/gui/flipping/SinglePage;->b:F

    float-to-double v4, v3

    cmpl-double v3, v4, v8

    if-gez v3, :cond_0

    iget v3, p0, Lflipboard/gui/flipping/SinglePage;->b:F

    float-to-double v4, v3

    cmpg-double v3, v4, v6

    if-gtz v3, :cond_2

    :cond_0
    float-to-double v4, v2

    cmpl-double v3, v4, v8

    if-gez v3, :cond_1

    float-to-double v2, v2

    cmpg-double v2, v2, v6

    if-gtz v2, :cond_2

    :cond_1
    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    iget-object v2, p0, Lflipboard/gui/flipping/SinglePage;->d:[I

    aget v2, v2, v1

    if-eqz v2, :cond_3

    :goto_1
    return v0

    :cond_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method g()V
    .locals 2

    .prologue
    .line 550
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const-string v1, "release bitmap"

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V

    .line 552
    :try_start_0
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->c:Lflipboard/gui/flipping/FlippingBitmap;

    if-eqz v0, :cond_0

    .line 553
    sget-object v0, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    iget-object v1, p0, Lflipboard/gui/flipping/SinglePage;->c:Lflipboard/gui/flipping/FlippingBitmap;

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Lflipboard/gui/flipping/FlippingBitmap;)V

    .line 554
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/flipping/SinglePage;->c:Lflipboard/gui/flipping/FlippingBitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 557
    :cond_0
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 558
    return-void

    .line 557
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0
.end method

.method public final h()F
    .locals 4

    .prologue
    .line 679
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lflipboard/gui/flipping/SinglePage;->I:J

    sub-long/2addr v0, v2

    long-to-float v0, v0

    iget v1, p0, Lflipboard/gui/flipping/SinglePage;->J:F

    div-float/2addr v0, v1

    const v1, 0x3a83126f    # 0.001f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2}, Lflipboard/util/JavaUtil;->a(FFF)F

    move-result v0

    return v0
.end method

.method public i()Lflipboard/gui/flipping/SinglePage;
    .locals 2

    .prologue
    .line 733
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget v1, p0, Lflipboard/gui/flipping/SinglePage;->E:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v0

    return-object v0
.end method

.method public j()Lflipboard/gui/flipping/SinglePage;
    .locals 2

    .prologue
    .line 738
    iget-object v0, p0, Lflipboard/gui/flipping/SinglePage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget v1, p0, Lflipboard/gui/flipping/SinglePage;->E:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v0

    return-object v0
.end method

.method public final k()F
    .locals 1

    .prologue
    .line 748
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->r:I

    int-to-float v0, v0

    return v0
.end method

.method public final l()F
    .locals 1

    .prologue
    .line 753
    iget v0, p0, Lflipboard/gui/flipping/SinglePage;->s:I

    int-to-float v0, v0

    return v0
.end method
