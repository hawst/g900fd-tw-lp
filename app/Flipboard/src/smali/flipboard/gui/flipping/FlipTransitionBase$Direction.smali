.class public final enum Lflipboard/gui/flipping/FlipTransitionBase$Direction;
.super Ljava/lang/Enum;
.source "FlipTransitionBase.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/flipping/FlipTransitionBase$Direction;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

.field public static final enum b:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

.field private static final synthetic c:[Lflipboard/gui/flipping/FlipTransitionBase$Direction;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 692
    new-instance v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    const-string v1, "NEXT"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/flipping/FlipTransitionBase$Direction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    new-instance v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    const-string v1, "PREVIOUS"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/flipping/FlipTransitionBase$Direction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->b:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->b:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    aput-object v1, v0, v3

    sput-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->c:[Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 692
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/flipping/FlipTransitionBase$Direction;
    .locals 1

    .prologue
    .line 692
    const-class v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/flipping/FlipTransitionBase$Direction;
    .locals 1

    .prologue
    .line 692
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->c:[Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    invoke-virtual {v0}, [Lflipboard/gui/flipping/FlipTransitionBase$Direction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    return-object v0
.end method
