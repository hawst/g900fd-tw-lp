.class public Lflipboard/gui/flipping/OpenGLTransitionRenderer;
.super Ljava/lang/Object;
.source "OpenGLTransitionRenderer.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# static fields
.field public static final a:Lflipboard/util/Log;

.field public static b:F


# instance fields
.field private final A:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final C:Landroid/view/animation/Interpolator;

.field private final D:Landroid/view/animation/Interpolator;

.field private final E:Ljava/util/concurrent/Semaphore;

.field private F:F

.field private G:Lflipboard/gui/flipping/SinglePage;

.field private H:Ljava/util/concurrent/atomic/AtomicInteger;

.field private I:Ljava/lang/Thread;

.field private J:J

.field private K:J

.field private L:Ljava/lang/String;

.field final c:F

.field final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/gui/flipping/SinglePage;",
            ">;"
        }
    .end annotation
.end field

.field final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/gui/flipping/SinglePage;",
            ">;"
        }
    .end annotation
.end field

.field f:Lflipboard/gui/flipping/TextPageRefresh;

.field g:Lflipboard/gui/flipping/TextPageLoadMore;

.field public final h:Landroid/opengl/GLSurfaceView;

.field i:[I

.field j:Landroid/graphics/Rect;

.field k:Landroid/graphics/Rect;

.field l:Landroid/graphics/PointF;

.field protected m:F

.field final n:Lflipboard/gui/flipping/FlipTransitionBase;

.field protected o:Lflipboard/gui/flipping/SinglePage;

.field public p:Z

.field q:Z

.field r:Z

.field s:Z

.field public t:Lflipboard/util/Log;

.field u:I

.field v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/flipping/OpenGLTransitionRenderer$Texture;",
            ">;"
        }
    .end annotation
.end field

.field private final w:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/gui/flipping/SinglePage;",
            ">;"
        }
    .end annotation
.end field

.field private final x:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lflipboard/gui/flipping/SinglePage;",
            ">;"
        }
    .end annotation
.end field

.field private final y:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/gui/flipping/TileFlip;",
            ">;"
        }
    .end annotation
.end field

.field private final z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/flipping/SinglePage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-string v0, "flipper"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a:Lflipboard/util/Log;

    .line 49
    const v0, 0x3e20d97c

    sput v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->b:F

    return-void
.end method

.method public constructor <init>(Lflipboard/gui/flipping/FlipTransitionBase;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const v0, 0x3fc90fdb

    iput v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->c:F

    .line 63
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->i:[I

    .line 84
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v3}, Ljava/util/concurrent/Semaphore;-><init>(IZ)V

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->E:Ljava/util/concurrent/Semaphore;

    .line 88
    const-string v0, "flashing"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->t:Lflipboard/util/Log;

    .line 155
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->H:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 444
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->v:Ljava/util/List;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->d:Ljava/util/ArrayList;

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->e:Ljava/util/ArrayList;

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->w:Ljava/util/ArrayList;

    .line 98
    new-instance v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer$1;

    invoke-direct {v0, p0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer$1;-><init>(Lflipboard/gui/flipping/OpenGLTransitionRenderer;)V

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->x:Ljava/util/Comparator;

    .line 124
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->z:Ljava/util/List;

    .line 125
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    .line 126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->A:Ljava/util/List;

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->B:Ljava/util/List;

    .line 128
    iget-object v0, p1, Lflipboard/gui/flipping/FlipTransitionBase;->d:Landroid/opengl/GLSurfaceView;

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->h:Landroid/opengl/GLSurfaceView;

    .line 129
    iput-object p1, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->n:Lflipboard/gui/flipping/FlipTransitionBase;

    .line 130
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const v1, 0x3fcccccd    # 1.6f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->C:Landroid/view/animation/Interpolator;

    .line 136
    :goto_0
    new-instance v0, Lflipboard/gui/flipping/TileBounceInterpolator;

    invoke-direct {v0}, Lflipboard/gui/flipping/TileBounceInterpolator;-><init>()V

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->D:Landroid/view/animation/Interpolator;

    .line 138
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->h:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02005e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 139
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-direct {v1, v3, v3, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->k:Landroid/graphics/Rect;

    .line 142
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->h:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 143
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-direct {v1, v3, v3, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->j:Landroid/graphics/Rect;

    .line 144
    return-void

    .line 133
    :cond_0
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const v1, 0x3f19999a    # 0.6f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->C:Landroid/view/animation/Interpolator;

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Lflipboard/gui/flipping/SinglePage;
    .locals 2

    .prologue
    .line 781
    const/4 v0, 0x0

    .line 782
    if-ltz p1, :cond_1

    iget-object v1, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_1

    .line 783
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/SinglePage;

    .line 790
    :cond_0
    :goto_0
    return-object v0

    .line 784
    :cond_1
    const/4 v1, -0x1

    if-ne p1, v1, :cond_2

    .line 785
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->f:Lflipboard/gui/flipping/TextPageRefresh;

    goto :goto_0

    .line 786
    :cond_2
    iget-object v1, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 787
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->g:Lflipboard/gui/flipping/TextPageLoadMore;

    goto :goto_0
.end method

.method public final a()V
    .locals 8

    .prologue
    .line 202
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 203
    iget-object v1, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->I:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    .line 204
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "wrong thread!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_0
    iget-object v1, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->H:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    if-nez v1, :cond_2

    .line 209
    sget-object v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a:Lflipboard/util/Log;

    iget-boolean v1, v1, Lflipboard/util/Log;->f:Z

    if-eqz v1, :cond_1

    .line 210
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 211
    iget-wide v4, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->J:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x14

    cmp-long v1, v4, v6

    if-lez v1, :cond_1

    .line 212
    sget-object v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a:Lflipboard/util/Log;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v4

    const/4 v0, 0x1

    iget-object v4, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->L:Ljava/lang/String;

    aput-object v4, v1, v0

    const/4 v0, 0x2

    iget-wide v4, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->K:J

    iget-wide v6, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->J:J

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v0

    const/4 v0, 0x3

    iget-wide v4, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->K:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    .line 215
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->I:Ljava/lang/Thread;

    .line 216
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->E:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 220
    :cond_2
    return-void
.end method

.method public final a(Lflipboard/gui/flipping/SinglePage;)V
    .locals 1

    .prologue
    .line 290
    const-string v0, "setReorderingTile"

    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V

    .line 292
    :try_start_0
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->o:Lflipboard/gui/flipping/SinglePage;

    if-eqz v0, :cond_0

    .line 293
    invoke-virtual {p0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->b()V

    .line 295
    :cond_0
    iput-object p1, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->o:Lflipboard/gui/flipping/SinglePage;

    .line 296
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->h:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->requestRender()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298
    invoke-virtual {p0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 299
    return-void

    .line 298
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0
.end method

.method public final a(Lflipboard/gui/flipping/TileFlip;)V
    .locals 1

    .prologue
    .line 271
    const-string v0, "add tile"

    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V

    .line 281
    :try_start_0
    invoke-static {}, Lflipboard/gui/flipping/FlipUtil;->a()V

    .line 282
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 284
    invoke-virtual {p0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 285
    return-void

    .line 284
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 846
    iget-object v1, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->A:Ljava/util/List;

    monitor-enter v1

    .line 847
    :try_start_0
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->A:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 848
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 161
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 162
    iget-object v1, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->I:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    .line 163
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->H:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 175
    :goto_0
    return-void

    .line 167
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 168
    iget-object v1, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->E:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 169
    iput-wide v2, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->J:J

    .line 170
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->K:J

    .line 171
    iput-object p1, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->L:Ljava/lang/String;

    .line 172
    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->I:Ljava/lang/Thread;

    .line 173
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->H:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    goto :goto_0
.end method

.method final a(Ljavax/microedition/khronos/opengles/GL10;Lflipboard/gui/flipping/SinglePage;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 453
    invoke-virtual {p2}, Lflipboard/gui/flipping/SinglePage;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 454
    iget-boolean v0, p2, Lflipboard/gui/flipping/SinglePage;->D:Z

    if-eqz v0, :cond_0

    .line 455
    invoke-virtual {p2, p1}, Lflipboard/gui/flipping/SinglePage;->c(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 477
    :cond_0
    :goto_0
    return-void

    .line 460
    :cond_1
    invoke-virtual {p2}, Lflipboard/gui/flipping/SinglePage;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p2, Lflipboard/gui/flipping/SinglePage;->d:[I

    aget v0, v0, v5

    .line 464
    iget v1, p2, Lflipboard/gui/flipping/SinglePage;->f:I

    .line 465
    iget v2, p2, Lflipboard/gui/flipping/SinglePage;->g:I

    .line 468
    if-eqz p1, :cond_2

    if-eqz p3, :cond_4

    iget-object v3, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->v:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iget v4, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->u:I

    if-ge v3, v4, :cond_4

    .line 469
    :cond_2
    iget-object v3, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->v:Ljava/util/List;

    new-instance v4, Lflipboard/gui/flipping/OpenGLTransitionRenderer$Texture;

    invoke-direct {v4, v0, v1, v2}, Lflipboard/gui/flipping/OpenGLTransitionRenderer$Texture;-><init>(III)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 470
    sget-object v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a:Lflipboard/util/Log;

    iget-boolean v1, v1, Lflipboard/util/Log;->f:Z

    if-eqz v1, :cond_3

    .line 471
    sget-object v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a:Lflipboard/util/Log;

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v5

    .line 476
    :cond_3
    :goto_1
    invoke-virtual {p2, p1}, Lflipboard/gui/flipping/SinglePage;->c(Ljavax/microedition/khronos/opengles/GL10;)V

    goto :goto_0

    .line 474
    :cond_4
    iget-object v0, p2, Lflipboard/gui/flipping/SinglePage;->d:[I

    invoke-interface {p1, v6, v0, v5}, Ljavax/microedition/khronos/opengles/GL10;->glDeleteTextures(I[II)V

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 304
    const-string v0, "removeReorderingTile"

    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V

    .line 306
    :try_start_0
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->o:Lflipboard/gui/flipping/SinglePage;

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->z:Ljava/util/List;

    iget-object v1, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->o:Lflipboard/gui/flipping/SinglePage;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 308
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->o:Lflipboard/gui/flipping/SinglePage;

    .line 309
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->h:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->requestRender()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 313
    return-void

    .line 312
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 179
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    .line 180
    iget-object v2, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->I:Ljava/lang/Thread;

    if-ne v1, v2, :cond_0

    .line 181
    iget-object v1, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->H:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 197
    :goto_0
    return v0

    .line 185
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 187
    :try_start_0
    iget-object v4, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->E:Ljava/util/concurrent/Semaphore;

    const-wide/16 v6, 0x14

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v6, v7, v5}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 188
    iput-wide v2, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->J:J

    .line 189
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->K:J

    .line 190
    iput-object p1, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->L:Ljava/lang/String;

    .line 191
    iput-object v1, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->I:Ljava/lang/Thread;

    .line 192
    iget-object v1, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->H:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 197
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 853
    iput-boolean v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->q:Z

    .line 854
    const-string v0, "noBitmaps"

    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V

    .line 856
    :try_start_0
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 857
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v0}, Lflipboard/gui/flipping/FlipUtil;->a(I)V

    .line 858
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 860
    :cond_0
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/SinglePage;

    .line 861
    invoke-virtual {v0}, Lflipboard/gui/flipping/SinglePage;->g()V

    .line 862
    iget-object v0, v0, Lflipboard/gui/flipping/SinglePage;->d:[I

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 866
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0

    .line 864
    :cond_1
    :try_start_1
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    const/4 v0, 0x1

    new-array v1, v0, [I

    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer$Texture;

    const/4 v3, 0x0

    iget v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer$Texture;->a:I

    aput v0, v1, v3

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 866
    :cond_3
    invoke-virtual {p0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 867
    return-void
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 17

    .prologue
    .line 514
    const-string v2, "onDrawFrame"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V

    .line 515
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->s:Z

    .line 518
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v6, 0x1

    .line 521
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_1
    add-int/lit8 v3, v2, -0x1

    if-ltz v3, :cond_3

    .line 522
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/gui/flipping/SinglePage;

    .line 525
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->r:Z

    if-eqz v4, :cond_1

    .line 526
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2, v4}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljavax/microedition/khronos/opengles/GL10;Lflipboard/gui/flipping/SinglePage;Z)V

    move v2, v3

    .line 527
    goto :goto_1

    .line 518
    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    .line 532
    :cond_1
    iget-boolean v4, v2, Lflipboard/gui/flipping/SinglePage;->A:Z

    if-eqz v4, :cond_2

    .line 533
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2, v4}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljavax/microedition/khronos/opengles/GL10;Lflipboard/gui/flipping/SinglePage;Z)V

    :cond_2
    move v2, v3

    .line 535
    goto :goto_1

    .line 538
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->z:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 539
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->z:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    :goto_2
    add-int/lit8 v3, v2, -0x1

    if-ltz v3, :cond_4

    .line 540
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->z:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/gui/flipping/SinglePage;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2, v4}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljavax/microedition/khronos/opengles/GL10;Lflipboard/gui/flipping/SinglePage;Z)V

    move v2, v3

    goto :goto_2

    .line 542
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->z:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 546
    :cond_5
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glClearColor(FFFF)V

    .line 548
    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 549
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->m:F

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 552
    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v4, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->F:F

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 553
    const/4 v4, 0x0

    .line 555
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 556
    const/4 v3, 0x0

    .line 557
    const/4 v2, 0x0

    move v7, v2

    move-object v8, v3

    move v3, v4

    :goto_3
    if-ge v7, v10, :cond_13

    .line 558
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/gui/flipping/SinglePage;

    .line 562
    invoke-virtual {v2}, Lflipboard/gui/flipping/SinglePage;->e()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 563
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lflipboard/gui/flipping/SinglePage;->b(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 567
    :cond_6
    const/4 v4, 0x0

    .line 568
    iget-boolean v5, v2, Lflipboard/gui/flipping/SinglePage;->z:Z

    if-eqz v5, :cond_36

    iget-boolean v5, v2, Lflipboard/gui/flipping/SinglePage;->u:Z

    if-nez v5, :cond_36

    .line 569
    const/4 v5, 0x1

    .line 570
    invoke-virtual {v2}, Lflipboard/gui/flipping/SinglePage;->h()F

    move-result v4

    .line 571
    move-object/from16 v0, p0

    iget-object v6, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->C:Landroid/view/animation/Interpolator;

    invoke-interface {v6, v4}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v6

    .line 572
    iget v11, v2, Lflipboard/gui/flipping/SinglePage;->L:F

    iget v12, v2, Lflipboard/gui/flipping/SinglePage;->K:F

    iget v13, v2, Lflipboard/gui/flipping/SinglePage;->L:F

    sub-float/2addr v12, v13

    mul-float/2addr v6, v12

    add-float/2addr v6, v11

    .line 573
    const v11, 0x3a83126f    # 0.001f

    cmpg-float v11, v6, v11

    if-gez v11, :cond_e

    iget-object v11, v2, Lflipboard/gui/flipping/SinglePage;->G:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    sget-object v12, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-eq v11, v12, :cond_7

    iget-boolean v11, v2, Lflipboard/gui/flipping/SinglePage;->H:Z

    if-nez v11, :cond_e

    .line 574
    :cond_7
    const/4 v6, 0x0

    .line 578
    :cond_8
    :goto_4
    invoke-virtual {v2, v6}, Lflipboard/gui/flipping/SinglePage;->a(F)V

    .line 580
    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v6, v4, v6

    if-nez v6, :cond_b

    if-eqz v9, :cond_9

    iget-boolean v6, v2, Lflipboard/gui/flipping/SinglePage;->H:Z

    if-nez v6, :cond_b

    .line 581
    :cond_9
    const/4 v6, 0x0

    iput-boolean v6, v2, Lflipboard/gui/flipping/SinglePage;->z:Z

    .line 582
    iget-boolean v6, v2, Lflipboard/gui/flipping/SinglePage;->H:Z

    if-eqz v6, :cond_a

    .line 583
    move-object/from16 v0, p0

    iget-object v6, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->n:Lflipboard/gui/flipping/FlipTransitionBase;

    iget-object v11, v2, Lflipboard/gui/flipping/SinglePage;->G:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    invoke-virtual {v6, v11}, Lflipboard/gui/flipping/FlipTransitionBase;->b(Lflipboard/gui/flipping/FlipTransitionBase$Direction;)V

    .line 585
    :cond_a
    move-object/from16 v0, p0

    iget-object v6, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->n:Lflipboard/gui/flipping/FlipTransitionBase;

    invoke-virtual {v6, v2}, Lflipboard/gui/flipping/FlipTransitionBase;->a(Lflipboard/gui/flipping/SinglePage;)V

    :cond_b
    move v6, v4

    move v4, v5

    .line 590
    :goto_5
    invoke-virtual {v2}, Lflipboard/gui/flipping/SinglePage;->c()F

    move-result v11

    if-nez v8, :cond_10

    const/4 v5, 0x0

    :goto_6
    sub-float v5, v11, v5

    sget v11, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->b:F

    cmpg-float v5, v5, v11

    if-gez v5, :cond_11

    const/4 v5, 0x1

    .line 591
    :goto_7
    if-nez v5, :cond_d

    iget-boolean v5, v2, Lflipboard/gui/flipping/SinglePage;->z:Z

    if-nez v5, :cond_c

    if-eqz v8, :cond_d

    iget-boolean v5, v8, Lflipboard/gui/flipping/SinglePage;->z:Z

    if-eqz v5, :cond_d

    .line 592
    :cond_c
    const/4 v5, 0x0

    cmpl-float v5, v6, v5

    if-lez v5, :cond_12

    const/high16 v5, 0x3f800000    # 1.0f

    cmpg-float v5, v6, v5

    if-gez v5, :cond_12

    const/4 v5, 0x1

    :goto_8
    or-int/2addr v4, v5

    .line 593
    move-object/from16 v0, p0

    iget-object v5, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->w:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 594
    add-int/lit8 v3, v3, 0x1

    .line 557
    :cond_d
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    move-object v8, v2

    move v6, v4

    goto/16 :goto_3

    .line 575
    :cond_e
    float-to-double v12, v6

    const-wide v14, 0x40091fef0a882d18L    # 3.1405926535422957

    cmpl-double v11, v12, v14

    if-ltz v11, :cond_8

    iget-object v11, v2, Lflipboard/gui/flipping/SinglePage;->G:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    sget-object v12, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->b:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-eq v11, v12, :cond_f

    iget-boolean v11, v2, Lflipboard/gui/flipping/SinglePage;->H:Z

    if-nez v11, :cond_8

    .line 576
    :cond_f
    const v6, 0x40490fdb    # (float)Math.PI

    goto :goto_4

    .line 590
    :cond_10
    invoke-virtual {v8}, Lflipboard/gui/flipping/SinglePage;->c()F

    move-result v5

    goto :goto_6

    :cond_11
    const/4 v5, 0x0

    goto :goto_7

    .line 592
    :cond_12
    const/4 v5, 0x0

    goto :goto_8

    .line 599
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->t:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    .line 601
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->w:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_14

    if-eqz v9, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->G:Lflipboard/gui/flipping/SinglePage;

    if-eqz v2, :cond_15

    .line 603
    :cond_14
    const/16 v2, 0x100

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glClear(I)V

    .line 608
    :goto_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->w:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_18

    .line 609
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->w:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->x:Ljava/util/Comparator;

    invoke-static {v2, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 610
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->w:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 611
    const/4 v2, 0x0

    move v4, v2

    :goto_a
    if-ge v4, v5, :cond_16

    .line 612
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->w:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/gui/flipping/SinglePage;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lflipboard/gui/flipping/SinglePage;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 611
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_a

    .line 606
    :cond_15
    const/16 v2, 0x4100

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glClear(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_9

    .line 775
    :catchall_0
    move-exception v2

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v2

    .line 614
    :cond_16
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->w:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->w:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/gui/flipping/SinglePage;

    move-object/from16 v0, p0

    iput-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->G:Lflipboard/gui/flipping/SinglePage;

    .line 615
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->w:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 626
    :cond_17
    :goto_b
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->r:Z

    .line 629
    if-lez v3, :cond_1b

    .line 630
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_c
    add-int/lit8 v3, v2, -0x1

    if-ltz v3, :cond_1a

    .line 631
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/gui/flipping/SinglePage;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2, v4}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljavax/microedition/khronos/opengles/GL10;Lflipboard/gui/flipping/SinglePage;Z)V

    move v2, v3

    goto :goto_c

    .line 616
    :cond_18
    if-eqz v9, :cond_17

    .line 617
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->G:Lflipboard/gui/flipping/SinglePage;

    if-eqz v2, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->G:Lflipboard/gui/flipping/SinglePage;

    iget-boolean v2, v2, Lflipboard/gui/flipping/SinglePage;->C:Z

    if-nez v2, :cond_19

    .line 618
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->G:Lflipboard/gui/flipping/SinglePage;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lflipboard/gui/flipping/SinglePage;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 619
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->t:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->G:Lflipboard/gui/flipping/SinglePage;

    iget v5, v5, Lflipboard/gui/flipping/SinglePage;->E:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    goto :goto_b

    .line 621
    :cond_19
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->t:Lflipboard/util/Log;

    const-string v4, "Pages were not being drawn anymore and we don\'t have backup: FLASH :("

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v4, v5}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_b

    .line 633
    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 634
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v4, Lflipboard/gui/flipping/OpenGLTransitionRenderer$2;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v2}, Lflipboard/gui/flipping/OpenGLTransitionRenderer$2;-><init>(Lflipboard/gui/flipping/OpenGLTransitionRenderer;I)V

    invoke-virtual {v3, v4}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 641
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 646
    :cond_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_d
    add-int/lit8 v5, v2, -0x2

    if-ltz v5, :cond_1f

    .line 647
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/gui/flipping/TileFlip;

    .line 648
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    add-int/lit8 v4, v5, 0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflipboard/gui/flipping/TileFlip;

    .line 650
    if-eqz v2, :cond_1c

    if-nez v3, :cond_24

    .line 654
    :cond_1c
    sget-object v4, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v5, "One of the tiles is null while moving, in %s, out %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    const/4 v2, 0x1

    aput-object v3, v7, v2

    invoke-static {v5, v7}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v4, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 655
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_e
    add-int/lit8 v3, v2, -0x1

    if-ltz v3, :cond_1e

    .line 656
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/gui/flipping/TileFlip;

    .line 657
    if-eqz v2, :cond_1d

    .line 658
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2, v4}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljavax/microedition/khronos/opengles/GL10;Lflipboard/gui/flipping/SinglePage;Z)V

    :cond_1d
    move v2, v3

    .line 660
    goto :goto_e

    .line 661
    :cond_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 662
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v4, Lflipboard/gui/flipping/OpenGLTransitionRenderer$3;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v2}, Lflipboard/gui/flipping/OpenGLTransitionRenderer$3;-><init>(Lflipboard/gui/flipping/OpenGLTransitionRenderer;I)V

    invoke-virtual {v3, v4}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 669
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 699
    :cond_1f
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_20

    .line 700
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->t:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 702
    :cond_20
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_f
    add-int/lit8 v4, v2, -0x2

    if-ltz v4, :cond_2a

    .line 703
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/gui/flipping/TileFlip;

    .line 704
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    add-int/lit8 v5, v4, 0x1

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflipboard/gui/flipping/TileFlip;

    .line 707
    invoke-virtual {v2}, Lflipboard/gui/flipping/TileFlip;->e()Z

    move-result v5

    if-eqz v5, :cond_21

    .line 708
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lflipboard/gui/flipping/TileFlip;->b(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 710
    :cond_21
    invoke-virtual {v3}, Lflipboard/gui/flipping/TileFlip;->e()Z

    move-result v5

    if-eqz v5, :cond_22

    .line 711
    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lflipboard/gui/flipping/TileFlip;->b(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 716
    :cond_22
    iget v5, v2, Lflipboard/gui/flipping/TileFlip;->b:F

    const v7, 0x3fc90fdb

    cmpg-float v5, v5, v7

    if-gez v5, :cond_28

    .line 717
    invoke-virtual {v2}, Lflipboard/gui/flipping/TileFlip;->c()F

    move-result v5

    sget v7, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->b:F

    cmpl-float v5, v5, v7

    if-lez v5, :cond_23

    .line 718
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lflipboard/gui/flipping/TileFlip;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 720
    :cond_23
    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lflipboard/gui/flipping/TileFlip;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    move v2, v4

    goto :goto_f

    .line 674
    :cond_24
    invoke-virtual {v2}, Lflipboard/gui/flipping/TileFlip;->h()F

    move-result v4

    .line 675
    move-object/from16 v0, p0

    iget-object v7, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->D:Landroid/view/animation/Interpolator;

    invoke-interface {v7, v4}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v7

    .line 678
    const/high16 v8, 0x3f800000    # 1.0f

    cmpg-float v4, v4, v8

    if-ltz v4, :cond_25

    iget-object v4, v2, Lflipboard/gui/flipping/TileFlip;->Q:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_26

    iget-boolean v4, v2, Lflipboard/gui/flipping/TileFlip;->B:Z

    if-nez v4, :cond_26

    :cond_25
    const/4 v4, 0x1

    .line 679
    :goto_10
    if-eqz v4, :cond_27

    iget-object v4, v2, Lflipboard/gui/flipping/TileFlip;->P:Lflipboard/gui/flipping/FlippingContainer;

    if-eqz v4, :cond_27

    iget-object v4, v2, Lflipboard/gui/flipping/TileFlip;->P:Lflipboard/gui/flipping/FlippingContainer;

    iget-boolean v4, v4, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    if-eqz v4, :cond_27

    .line 680
    const v3, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v3, v7

    invoke-virtual {v2, v3}, Lflipboard/gui/flipping/TileFlip;->a(F)V

    move v2, v5

    goto/16 :goto_d

    .line 678
    :cond_26
    const/4 v4, 0x0

    goto :goto_10

    .line 683
    :cond_27
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2, v4}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljavax/microedition/khronos/opengles/GL10;Lflipboard/gui/flipping/SinglePage;Z)V

    .line 684
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3, v2}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljavax/microedition/khronos/opengles/GL10;Lflipboard/gui/flipping/SinglePage;Z)V

    .line 685
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    add-int/lit8 v3, v5, 0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 686
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->y:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 687
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v3, Lflipboard/gui/flipping/OpenGLTransitionRenderer$4;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer$4;-><init>(Lflipboard/gui/flipping/OpenGLTransitionRenderer;)V

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    move v2, v5

    .line 696
    goto/16 :goto_d

    .line 722
    :cond_28
    invoke-virtual {v2}, Lflipboard/gui/flipping/TileFlip;->c()F

    move-result v5

    float-to-double v8, v5

    const-wide v10, 0x400921fb54442d18L    # Math.PI

    sget v5, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->b:F

    float-to-double v12, v5

    sub-double/2addr v10, v12

    cmpg-double v5, v8, v10

    if-gez v5, :cond_29

    .line 723
    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lflipboard/gui/flipping/TileFlip;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 725
    :cond_29
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lflipboard/gui/flipping/TileFlip;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    move v2, v4

    .line 727
    goto/16 :goto_f

    .line 729
    :cond_2a
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->o:Lflipboard/gui/flipping/SinglePage;

    if-eqz v2, :cond_2c

    .line 730
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->o:Lflipboard/gui/flipping/SinglePage;

    invoke-virtual {v2}, Lflipboard/gui/flipping/SinglePage;->e()Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 731
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->o:Lflipboard/gui/flipping/SinglePage;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lflipboard/gui/flipping/SinglePage;->b(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 733
    :cond_2b
    const/16 v2, 0xb71

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    .line 734
    const/16 v2, 0xbe2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 735
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->o:Lflipboard/gui/flipping/SinglePage;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lflipboard/gui/flipping/SinglePage;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 736
    const/16 v2, 0xbe2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    .line 737
    const/16 v2, 0xb71

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 741
    :cond_2c
    if-eqz v6, :cond_2d

    .line 742
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->h:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v2}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 745
    :cond_2d
    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glGetError()I

    move-result v2

    .line 746
    if-eqz v2, :cond_2e

    .line 747
    const/16 v3, 0x505

    if-ne v2, v3, :cond_2f

    .line 748
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "OpenGL is out of memory, that\'s bad"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 762
    :cond_2e
    :goto_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->A:Ljava/util/List;

    monitor-enter v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 764
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->B:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_12
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_34

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    .line 765
    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v5, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_12

    .line 773
    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v3

    throw v2

    .line 749
    :cond_2f
    const/16 v3, 0x502

    if-ne v2, v3, :cond_30

    .line 750
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "OpenGL says invalid operation"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_11

    .line 751
    :cond_30
    const/16 v3, 0x501

    if-ne v2, v3, :cond_31

    .line 752
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "OpenGL says invalid value"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_11

    .line 753
    :cond_31
    const/16 v3, 0x500

    if-ne v2, v3, :cond_32

    .line 754
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "OpenGL says invalid enum"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_11

    .line 755
    :cond_32
    const/16 v3, 0x503

    if-ne v2, v3, :cond_33

    .line 756
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "OpenGL says stack overflow"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_11

    .line 757
    :cond_33
    const/16 v3, 0x504

    if-ne v2, v3, :cond_2e

    .line 758
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "OpenGL says stack underflow"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_11

    .line 767
    :cond_34
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->B:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 768
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->A:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_35

    .line 769
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->B:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->A:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 770
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->A:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 771
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->h:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v2}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 773
    :cond_35
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 775
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 776
    return-void

    :cond_36
    move/from16 v16, v4

    move v4, v6

    move/from16 v6, v16

    goto/16 :goto_5
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const v5, 0x3dcccccd    # 0.1f

    const/4 v0, 0x0

    .line 401
    invoke-interface {p1, v0, v0, v0, v0}, Ljavax/microedition/khronos/opengles/GL10;->glClearColor(FFFF)V

    .line 403
    invoke-interface {p1, v1, v1, p2, p3}, Ljavax/microedition/khronos/opengles/GL10;->glViewport(IIII)V

    .line 405
    const/16 v0, 0x1701

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 408
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 410
    int-to-float v0, p3

    const/high16 v1, -0x40000000    # -2.0f

    div-float/2addr v0, v1

    const v1, 0x3ec90fda

    invoke-static {v1}, Landroid/util/FloatMath;->sin(F)F

    move-result v1

    div-float/2addr v0, v1

    const v1, 0x3f96cbe4

    invoke-static {v1}, Landroid/util/FloatMath;->sin(F)F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->m:F

    .line 416
    const/high16 v0, 0x42340000    # 45.0f

    int-to-float v1, p2

    int-to-float v2, p3

    div-float/2addr v1, v2

    iget v2, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->m:F

    neg-float v2, v2

    int-to-float v3, p3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    sub-float/2addr v2, v5

    iget v3, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->m:F

    neg-float v3, v3

    add-float/2addr v3, v5

    invoke-static {p1, v0, v1, v2, v3}, Landroid/opengl/GLU;->gluPerspective(Ljavax/microedition/khronos/opengles/GL10;FFFF)V

    .line 418
    const/16 v0, 0x1700

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 421
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 422
    const/16 v0, 0xb44

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 423
    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 13

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v11, 0x1

    const/16 v1, 0xde1

    const/4 v2, 0x0

    .line 318
    const/16 v0, 0x1f03

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "GL_OES_texture_npot"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->p:Z

    .line 319
    sget-object v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "GL_RENDERER = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x1f01

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    sget-object v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "GL_VENDOR = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x1f00

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    sget-object v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "GL_VERSION = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x1f02

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 322
    sget-object v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "GL_EXTENSIONS = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x1f03

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    invoke-interface {p1, v1}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 324
    const/16 v0, 0x1d01

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glShadeModel(I)V

    .line 325
    invoke-interface {p1, v4, v4, v4, v4}, Ljavax/microedition/khronos/opengles/GL10;->glClearColor(FFFF)V

    .line 326
    invoke-interface {p1, v7}, Ljavax/microedition/khronos/opengles/GL10;->glClearDepthf(F)V

    .line 328
    const/16 v0, 0xb71

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 329
    const/16 v0, 0x303

    invoke-interface {p1, v11, v0}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 330
    const/16 v0, 0x203

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDepthFunc(I)V

    .line 332
    const/16 v0, 0xc50

    const/16 v3, 0x1102

    invoke-interface {p1, v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glHint(II)V

    .line 335
    const/4 v0, 0x2

    iget-object v3, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->i:[I

    invoke-interface {p1, v0, v3, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDeleteTextures(I[II)V

    .line 338
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->i:[I

    invoke-interface {p1, v11, v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    .line 339
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->i:[I

    aget v0, v0, v2

    invoke-interface {p1, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 342
    const/16 v0, 0x2801

    const v3, 0x46180400    # 9729.0f

    invoke-interface {p1, v1, v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 345
    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    const v3, 0x7f02005e

    invoke-virtual {v0, v3}, Lflipboard/io/BitmapManager;->a(I)Lflipboard/io/BitmapManager$Handle;

    move-result-object v0

    .line 346
    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->b()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 347
    invoke-static {v1, v2, v3, v2}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 348
    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->d()V

    .line 349
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->i:[I

    aget v0, v0, v2

    invoke-interface {p1, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 352
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->i:[I

    invoke-interface {p1, v11, v0, v11}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    .line 353
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->i:[I

    aget v0, v0, v11

    invoke-interface {p1, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 356
    const/16 v0, 0x2801

    const v3, 0x46180400    # 9729.0f

    invoke-interface {p1, v1, v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 359
    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    const v3, 0x7f0201d4

    invoke-virtual {v0, v3}, Lflipboard/io/BitmapManager;->a(I)Lflipboard/io/BitmapManager$Handle;

    move-result-object v3

    .line 360
    invoke-virtual {v3}, Lflipboard/io/BitmapManager$Handle;->i()I

    .line 361
    invoke-virtual {v3}, Lflipboard/io/BitmapManager$Handle;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 363
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v4

    if-nez v4, :cond_3

    .line 367
    :try_start_0
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 372
    :goto_0
    invoke-virtual {v3}, Lflipboard/io/BitmapManager$Handle;->d()V

    move v10, v11

    move-object v12, v0

    .line 376
    :goto_1
    iget-boolean v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->p:Z

    if-eqz v0, :cond_1

    .line 377
    invoke-static {v1, v2, v12, v2}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 378
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v7, v7}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->l:Landroid/graphics/PointF;

    .line 386
    :goto_2
    iget-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->i:[I

    aget v0, v0, v11

    invoke-interface {p1, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 387
    if-eqz v10, :cond_0

    .line 388
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->recycle()V

    .line 391
    :cond_0
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 392
    const v0, 0x3ecccccd    # 0.4f

    iput v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->F:F

    .line 396
    :goto_3
    return-void

    .line 369
    :catch_0
    move-exception v4

    sget-object v4, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    mul-int/2addr v5, v6

    invoke-virtual {v4, v5}, Lflipboard/io/BitmapManager;->c(I)V

    .line 370
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v4, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 380
    :cond_1
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-static {v0}, Lflipboard/util/JavaUtil;->b(I)I

    move-result v4

    .line 381
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-static {v0}, Lflipboard/util/JavaUtil;->b(I)I

    move-result v5

    .line 382
    const/16 v3, 0x1908

    const/16 v7, 0x1908

    const/16 v8, 0x1401

    const/4 v9, 0x0

    move-object v0, p1

    move v6, v2

    invoke-interface/range {v0 .. v9}, Ljavax/microedition/khronos/opengles/GL10;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    .line 383
    invoke-static {v1, v2, v2, v2, v12}, Landroid/opengl/GLUtils;->texSubImage2D(IIIILandroid/graphics/Bitmap;)V

    .line 384
    new-instance v0, Landroid/graphics/PointF;

    iget-object v2, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->j:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    int-to-float v3, v4

    div-float/2addr v2, v3

    iget-object v3, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->j:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v5

    div-float/2addr v3, v4

    invoke-direct {v0, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->l:Landroid/graphics/PointF;

    goto :goto_2

    .line 394
    :cond_2
    const v0, 0x3ee66666    # 0.45f

    iput v0, p0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->F:F

    goto :goto_3

    :cond_3
    move v10, v2

    move-object v12, v0

    goto :goto_1
.end method
