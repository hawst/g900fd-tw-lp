.class public Lflipboard/gui/flipping/FlippingContainer;
.super Landroid/view/ViewGroup;
.source "FlippingContainer.java"


# static fields
.field public static j:Lflipboard/util/Log;


# instance fields
.field public a:Z

.field final b:Landroid/view/View;

.field public c:I

.field public d:Z

.field public e:Z

.field f:J

.field g:J

.field final h:F

.field i:Z

.field public k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "flashing"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/FlippingContainer;->j:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 36
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 20
    iput-boolean v1, p0, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    .line 23
    iput-boolean v1, p0, Lflipboard/gui/flipping/FlippingContainer;->d:Z

    .line 24
    iput-boolean v1, p0, Lflipboard/gui/flipping/FlippingContainer;->e:Z

    .line 37
    iput-object p1, p0, Lflipboard/gui/flipping/FlippingContainer;->b:Landroid/view/View;

    .line 38
    invoke-virtual {p0, p1}, Lflipboard/gui/flipping/FlippingContainer;->addView(Landroid/view/View;)V

    .line 39
    iput p2, p0, Lflipboard/gui/flipping/FlippingContainer;->c:I

    .line 40
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/FlippingContainer;->setWillNotDraw(Z)V

    .line 41
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlippingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lflipboard/gui/flipping/FlippingContainer;->h:F

    .line 42
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 95
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlippingContainer;->e:Z

    if-nez v0, :cond_1

    move v0, v1

    .line 96
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lflipboard/gui/flipping/FlippingContainer;->f:J

    iput-wide v2, p0, Lflipboard/gui/flipping/FlippingContainer;->g:J

    .line 97
    iput-boolean v1, p0, Lflipboard/gui/flipping/FlippingContainer;->d:Z

    iput-boolean v1, p0, Lflipboard/gui/flipping/FlippingContainer;->e:Z

    .line 98
    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlippingContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlipTransitionViews;

    .line 100
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->s()V

    .line 102
    :cond_0
    return-void

    .line 95
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 182
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    if-eqz v0, :cond_0

    .line 183
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 185
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    if-eqz v0, :cond_0

    .line 150
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 152
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 51
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 52
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    iput-boolean v0, p0, Lflipboard/gui/flipping/FlippingContainer;->i:Z

    .line 53
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    if-eqz v0, :cond_0

    .line 54
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 56
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 57
    return-void
.end method

.method public getChild()Landroid/view/View;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lflipboard/gui/flipping/FlippingContainer;->b:Landroid/view/View;

    return-object v0
.end method

.method public invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 68
    iget-boolean v1, p0, Lflipboard/gui/flipping/FlippingContainer;->d:Z

    if-nez v1, :cond_3

    move v1, v2

    .line 69
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 71
    iput-boolean v2, p0, Lflipboard/gui/flipping/FlippingContainer;->d:Z

    .line 72
    iput-wide v4, p0, Lflipboard/gui/flipping/FlippingContainer;->f:J

    .line 73
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    iget v6, p0, Lflipboard/gui/flipping/FlippingContainer;->h:F

    div-float/2addr v3, v6

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v3, v6

    iget v6, p0, Lflipboard/gui/flipping/FlippingContainer;->h:F

    div-float/2addr v3, v6

    const/high16 v6, 0x447a0000    # 1000.0f

    cmpl-float v3, v3, v6

    if-lez v3, :cond_0

    move v0, v2

    .line 74
    :cond_0
    if-eqz v0, :cond_1

    .line 75
    iput-wide v4, p0, Lflipboard/gui/flipping/FlippingContainer;->g:J

    .line 76
    iput-boolean v2, p0, Lflipboard/gui/flipping/FlippingContainer;->e:Z

    .line 79
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlippingContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlipTransitionViews;

    .line 80
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;

    move-result-object v3

    .line 82
    if-eqz v1, :cond_2

    .line 83
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->s()V

    .line 84
    iget-object v0, v0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget v1, p0, Lflipboard/gui/flipping/FlippingContainer;->c:I

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v0

    .line 85
    if-eqz v0, :cond_2

    .line 86
    iput-boolean v2, v0, Lflipboard/gui/flipping/SinglePage;->C:Z

    .line 90
    :cond_2
    return-object v3

    :cond_3
    move v1, v0

    .line 68
    goto :goto_0
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    return v0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 172
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    if-eqz v0, :cond_0

    .line 173
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 175
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 116
    sub-int v2, p4, p2

    .line 117
    sub-int v3, p5, p3

    move v0, v1

    .line 118
    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlippingContainer;->getChildCount()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 119
    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/FlippingContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v1, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 118
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 121
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 107
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlippingContainer;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 108
    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/FlippingContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Landroid/view/View;->measure(II)V

    .line 107
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 110
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 111
    return-void
.end method

.method public setVisible(Z)V
    .locals 3

    .prologue
    .line 125
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 126
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Wrong thread, setVisible must be called on the UI thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :cond_0
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    if-eq v0, p1, :cond_2

    .line 130
    sget-object v0, Lflipboard/gui/flipping/FlippingContainer;->j:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lflipboard/gui/flipping/FlippingContainer;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 131
    iput-boolean p1, p0, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    .line 133
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlippingContainer;->getChild()Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;

    if-eqz v0, :cond_1

    .line 134
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlippingContainer;->getChild()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/FlipmagDetailViewTablet;

    if-eqz p1, :cond_1

    iget-object v1, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    const-string v1, "javascript:FLArticleViewBecameVisibile()"

    invoke-virtual {v0, v1}, Lflipboard/gui/FLWebView;->loadUrl(Ljava/lang/String;)V

    .line 137
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlippingContainer;->invalidate()V

    .line 139
    :cond_2
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .prologue
    const-wide/16 v10, 0x64

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 158
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 159
    const-string v3, "FlippingContainer[%d: small=%s/%s, large=%s/%s]"

    const/4 v0, 0x5

    new-array v6, v0, [Ljava/lang/Object;

    iget v0, p0, Lflipboard/gui/flipping/FlippingContainer;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v2

    iget-boolean v0, p0, Lflipboard/gui/flipping/FlippingContainer;->d:Z

    .line 160
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v6, v1

    const/4 v7, 0x2

    iget-boolean v0, p0, Lflipboard/gui/flipping/FlippingContainer;->d:Z

    if-eqz v0, :cond_0

    iget-wide v8, p0, Lflipboard/gui/flipping/FlippingContainer;->f:J

    sub-long v8, v4, v8

    cmp-long v0, v8, v10

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v6, v7

    const/4 v0, 0x3

    iget-boolean v7, p0, Lflipboard/gui/flipping/FlippingContainer;->e:Z

    .line 161
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v0, 0x4

    iget-boolean v7, p0, Lflipboard/gui/flipping/FlippingContainer;->e:Z

    if-eqz v7, :cond_1

    iget-wide v8, p0, Lflipboard/gui/flipping/FlippingContainer;->g:J

    sub-long/2addr v4, v8

    cmp-long v4, v4, v10

    if-lez v4, :cond_1

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v6, v0

    .line 159
    invoke-static {v3, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 160
    goto :goto_0

    :cond_1
    move v1, v2

    .line 161
    goto :goto_1
.end method
