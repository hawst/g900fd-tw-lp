.class public Lflipboard/gui/flipping/FlipTransitionViews;
.super Lflipboard/gui/flipping/FlipTransitionBase;
.source "FlipTransitionViews.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
.implements Lflipboard/activities/FlipboardActivity$OnBackPressedListener;


# static fields
.field private static final Q:Landroid/animation/TimeInterpolator;


# instance fields
.field protected final G:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/gui/flipping/FlippingContainer;",
            ">;"
        }
    .end annotation
.end field

.field final H:Landroid/widget/FrameLayout$LayoutParams;

.field final I:Ljava/util/concurrent/atomic/AtomicInteger;

.field final J:Ljava/lang/Runnable;

.field private K:Z

.field private final L:I

.field private M:Lflipboard/util/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observable",
            "<",
            "Ljava/lang/Object;",
            "Lflipboard/gui/flipping/FlipTransitionViews$Message;",
            "Lflipboard/gui/flipping/FlipTransitionBase$Direction;",
            ">;"
        }
    .end annotation
.end field

.field private final N:Ljava/lang/String;

.field private O:J

.field private P:Z

.field private final R:Landroid/view/ScaleGestureDetector;

.field private S:F

.field private T:F

.field private U:Lcom/amazon/motiongestures/GestureManager;

.field private V:Lcom/amazon/motiongestures/GestureListener;

.field private W:Lcom/amazon/motiongestures/GestureListener;

.field private final a:Z

.field private aa:Landroid/view/ViewTreeObserver$OnPreDrawListener;

.field private ab:Ljava/lang/Runnable;

.field private ac:I

.field private ad:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

.field private final ae:Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 63
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lflipboard/gui/flipping/FlipTransitionViews;->Q:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 107
    invoke-direct {p0, p1}, Lflipboard/gui/flipping/FlipTransitionBase;-><init>(Landroid/content/Context;)V

    .line 51
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    iput-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->a:Z

    .line 54
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->H:Landroid/widget/FrameLayout$LayoutParams;

    .line 58
    new-instance v0, Lflipboard/util/Observable;

    invoke-direct {v0}, Lflipboard/util/Observable;-><init>()V

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->M:Lflipboard/util/Observable;

    .line 59
    const-string v0, "fake"

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->N:Ljava/lang/String;

    .line 65
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->S:F

    .line 70
    new-instance v0, Lflipboard/gui/flipping/FlipTransitionViews$1;

    invoke-direct {v0, p0}, Lflipboard/gui/flipping/FlipTransitionViews$1;-><init>(Lflipboard/gui/flipping/FlipTransitionViews;)V

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->V:Lcom/amazon/motiongestures/GestureListener;

    .line 76
    new-instance v0, Lflipboard/gui/flipping/FlipTransitionViews$2;

    invoke-direct {v0, p0}, Lflipboard/gui/flipping/FlipTransitionViews$2;-><init>(Lflipboard/gui/flipping/FlipTransitionViews;)V

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->W:Lcom/amazon/motiongestures/GestureListener;

    .line 524
    iput v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->ac:I

    .line 636
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->I:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 637
    new-instance v0, Lflipboard/gui/flipping/FlipTransitionViews$8;

    invoke-direct {v0, p0}, Lflipboard/gui/flipping/FlipTransitionViews$8;-><init>(Lflipboard/gui/flipping/FlipTransitionViews;)V

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->J:Ljava/lang/Runnable;

    .line 722
    new-instance v0, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;-><init>(Lflipboard/gui/flipping/FlipTransitionViews;B)V

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->ae:Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    .line 109
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getDesiredNumberOfTextures()I

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->L:I

    .line 110
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->a:Z

    if-eqz v0, :cond_0

    .line 111
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-direct {v0, p1, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->R:Landroid/view/ScaleGestureDetector;

    .line 115
    :goto_0
    invoke-virtual {p0, v2}, Lflipboard/gui/flipping/FlipTransitionViews;->setChildrenDrawingOrderEnabled(Z)V

    .line 117
    check-cast p1, Landroid/app/Activity;

    invoke-static {p1}, Lflipboard/service/FlipboardManager;->a(Landroid/app/Activity;)Lcom/amazon/motiongestures/GestureManager;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->U:Lcom/amazon/motiongestures/GestureManager;

    .line 119
    new-instance v0, Lflipboard/gui/flipping/FlipTransitionViews$3;

    invoke-direct {v0, p0}, Lflipboard/gui/flipping/FlipTransitionViews$3;-><init>(Lflipboard/gui/flipping/FlipTransitionViews;)V

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->ab:Ljava/lang/Runnable;

    .line 130
    return-void

    .line 113
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->R:Landroid/view/ScaleGestureDetector;

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/gui/flipping/FlipTransitionViews;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->ab:Ljava/lang/Runnable;

    return-object v0
.end method

.method private a(ILflipboard/gui/flipping/FlipTransitionViews$TextureStats;)V
    .locals 10

    .prologue
    .line 725
    if-ltz p1, :cond_0

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 799
    :cond_0
    :goto_0
    return-void

    .line 729
    :cond_1
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0, p1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v4

    .line 730
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    .line 731
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    if-le p1, v1, :cond_2

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    sub-int v1, p1, v1

    .line 732
    :goto_1
    mul-int/lit8 v2, v1, 0x1e

    add-int/lit8 v3, v2, 0x64

    .line 733
    mul-int/lit8 v2, v1, 0x1e

    add-int/lit16 v2, v2, 0xfa

    .line 734
    iget-boolean v5, v0, Lflipboard/gui/flipping/FlippingContainer;->d:Z

    if-eqz v5, :cond_3

    iget-wide v6, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->a:J

    iget-wide v8, v0, Lflipboard/gui/flipping/FlippingContainer;->f:J

    sub-long/2addr v6, v8

    int-to-long v8, v2

    cmp-long v2, v6, v8

    if-gez v2, :cond_3

    const/4 v2, 0x1

    .line 735
    :goto_2
    iget-boolean v5, v0, Lflipboard/gui/flipping/FlippingContainer;->e:Z

    if-eqz v5, :cond_4

    iget-wide v6, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->a:J

    iget-wide v8, v0, Lflipboard/gui/flipping/FlippingContainer;->g:J

    sub-long/2addr v6, v8

    int-to-long v8, v3

    cmp-long v3, v6, v8

    if-gez v3, :cond_4

    const/4 v3, 0x1

    .line 737
    :goto_3
    const/4 v5, 0x0

    iput-boolean v5, v4, Lflipboard/gui/flipping/SinglePage;->A:Z

    .line 738
    iget-boolean v5, v4, Lflipboard/gui/flipping/SinglePage;->B:Z

    if-nez v5, :cond_7

    .line 743
    iget-boolean v1, v0, Lflipboard/gui/flipping/FlippingContainer;->d:Z

    if-eqz v1, :cond_0

    .line 747
    if-eqz v2, :cond_5

    .line 748
    iget v0, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->f:I

    .line 749
    if-eqz v3, :cond_0

    .line 750
    iget v0, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->g:I

    goto :goto_0

    .line 731
    :cond_2
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    sub-int/2addr v1, p1

    goto :goto_1

    .line 734
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 735
    :cond_4
    const/4 v3, 0x0

    goto :goto_3

    .line 755
    :cond_5
    iget-boolean v1, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->c:Z

    if-eqz v1, :cond_6

    .line 756
    iget v0, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->h:I

    goto :goto_0

    .line 759
    :cond_6
    iget v1, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->e:I

    .line 760
    sget-object v1, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-boolean v3, v0, Lflipboard/gui/flipping/FlippingContainer;->e:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-boolean v3, v0, Lflipboard/gui/flipping/FlippingContainer;->d:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 783
    :goto_4
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "FlipTransitionViews:checkPageTexture"

    invoke-static {v1}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 784
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-nez v1, :cond_d

    invoke-static {}, Lflipboard/gui/flipping/FlipUtil;->d()I

    move-result v1

    if-nez v1, :cond_d

    .line 785
    sget-object v1, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    invoke-virtual {v1, v0}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Landroid/view/View;)Lflipboard/gui/flipping/FlippingBitmap;

    move-result-object v2

    .line 786
    if-eqz v2, :cond_c

    .line 787
    sget-object v1, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-boolean v1, v4, Lflipboard/gui/flipping/SinglePage;->B:Z

    if-eqz v1, :cond_b

    const-string v1, "created"

    :goto_5
    aput-object v1, v3, v5

    const/4 v1, 0x1

    iget v5, v4, Lflipboard/gui/flipping/SinglePage;->E:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v1

    const/4 v1, 0x2

    iget v5, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v1

    .line 788
    invoke-virtual {v4, v2}, Lflipboard/gui/flipping/SinglePage;->a(Lflipboard/gui/flipping/FlippingBitmap;)V

    .line 789
    const/4 v1, 0x0

    iput-boolean v1, v0, Lflipboard/gui/flipping/FlippingContainer;->e:Z

    iput-boolean v1, v0, Lflipboard/gui/flipping/FlippingContainer;->d:Z

    .line 796
    :goto_6
    iget-boolean v0, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->c:Z

    if-nez v0, :cond_0

    .line 797
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_e

    const/4 v0, 0x1

    :goto_7
    iput-boolean v0, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->c:Z

    goto/16 :goto_0

    .line 761
    :cond_7
    if-lez v1, :cond_9

    if-nez v3, :cond_8

    iget-boolean v1, v0, Lflipboard/gui/flipping/FlippingContainer;->e:Z

    if-nez v1, :cond_9

    if-eqz v2, :cond_9

    .line 766
    :cond_8
    iget v0, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->h:I

    goto/16 :goto_0

    .line 772
    :cond_9
    iget-boolean v1, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->c:Z

    if-eqz v1, :cond_a

    .line 773
    iget v0, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->h:I

    goto/16 :goto_0

    .line 776
    :cond_a
    iget v1, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->d:I

    goto :goto_4

    .line 787
    :cond_b
    const-string v1, "updated"

    goto :goto_5

    .line 791
    :cond_c
    iget v0, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->h:I

    goto :goto_6

    .line 794
    :cond_d
    iget v0, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->h:I

    goto :goto_6

    .line 797
    :cond_e
    const/4 v0, 0x0

    goto :goto_7
.end method

.method static synthetic a(Lflipboard/gui/flipping/FlipTransitionViews;IZ)V
    .locals 0

    .prologue
    .line 43
    invoke-super {p0, p1, p2}, Lflipboard/gui/flipping/FlipTransitionBase;->a(IZ)V

    return-void
.end method

.method private static a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 970
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 974
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    const-string v1, "View actions must be performed on the UI thread (%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 977
    :cond_0
    return-void
.end method

.method static synthetic b(Lflipboard/gui/flipping/FlipTransitionViews;)Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->K:Z

    return v0
.end method

.method static synthetic c(Lflipboard/gui/flipping/FlipTransitionViews;)Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->P:Z

    return v0
.end method

.method public static e(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 1230
    move-object v0, p0

    :goto_0
    instance-of v1, v0, Lflipboard/gui/flipping/FlippingContainer;

    if-eqz v1, :cond_0

    .line 1231
    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    iget-boolean v0, v0, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    .line 1237
    :goto_1
    return v0

    .line 1233
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1234
    if-nez v0, :cond_1

    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_2

    .line 1235
    :cond_1
    check-cast v0, Landroid/view/View;

    goto :goto_0

    .line 1237
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private f(I)V
    .locals 10
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const-wide/16 v8, 0x2bc

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    .line 1381
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "FlipTransitionViews:enabledZoomedOut"

    invoke-static {v2}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 1382
    iput-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->P:Z

    .line 1383
    iput v7, p0, Lflipboard/gui/flipping/FlipTransitionViews;->S:F

    .line 1384
    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/2addr v2, p1

    iget-object v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-gt v2, v3, :cond_0

    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/2addr v2, p1

    if-gez v2, :cond_1

    :cond_0
    move p1, v0

    .line 1389
    :cond_1
    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int v4, v2, p1

    move v3, v0

    .line 1390
    :goto_0
    const/16 v0, 0x9

    if-ge v3, v0, :cond_6

    .line 1392
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/2addr v0, v3

    add-int/lit8 v2, v0, -0x4

    .line 1393
    if-ltz v2, :cond_2

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt v2, v0, :cond_2

    .line 1394
    if-ge v2, v4, :cond_3

    move v0, v1

    .line 1397
    :goto_1
    iget-object v5, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v5, v2}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v5

    invoke-virtual {v5, v0}, Lflipboard/gui/flipping/SinglePage;->a(F)V

    .line 1398
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    .line 1399
    if-eq v2, v4, :cond_5

    .line 1400
    const/4 v5, 0x1

    sub-int v6, v2, v4

    invoke-static {v0, v5, v6}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;ZI)V

    .line 1401
    if-ge v2, v4, :cond_4

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getHeight()I

    move-result v2

    neg-int v2, v2

    .line 1402
    :goto_2
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlippingContainer;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/view/ViewPropertyAnimator;->x(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    int-to-float v2, v2

    invoke-virtual {v5, v2}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    sget-object v5, Lflipboard/gui/flipping/FlipTransitionViews;->Q:Landroid/animation/TimeInterpolator;

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 1403
    new-instance v5, Lflipboard/gui/flipping/FlipTransitionViews$13;

    invoke-direct {v5, p0, v0}, Lflipboard/gui/flipping/FlipTransitionViews$13;-><init>(Lflipboard/gui/flipping/FlipTransitionViews;Lflipboard/gui/flipping/FlippingContainer;)V

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 1391
    :cond_2
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1394
    :cond_3
    const v0, 0x40490fdb    # (float)Math.PI

    goto :goto_1

    .line 1401
    :cond_4
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getHeight()I

    move-result v2

    goto :goto_2

    .line 1413
    :cond_5
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlippingContainer;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->x(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v2, Lflipboard/gui/flipping/FlipTransitionViews;->Q:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    goto :goto_3

    .line 1416
    :cond_6
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/flipping/FlipTransitionViews$14;

    invoke-direct {v1, p0}, Lflipboard/gui/flipping/FlipTransitionViews$14;-><init>(Lflipboard/gui/flipping/FlipTransitionViews;)V

    invoke-virtual {v0, v8, v9, v1}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    .line 1424
    iput v4, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    .line 1425
    return-void
.end method

.method private u()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1356
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FlipTransitionViews:enabledZoomedOut"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 1357
    iput-boolean v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->P:Z

    .line 1358
    const v0, 0x7f080033

    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->setBackgroundResource(I)V

    .line 1361
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/16 v0, 0x9

    if-ge v1, v0, :cond_1

    .line 1363
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x4

    .line 1364
    if-ltz v0, :cond_0

    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-gt v0, v2, :cond_0

    .line 1365
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    .line 1368
    invoke-virtual {v0, v3}, Lflipboard/gui/flipping/FlippingContainer;->setVisible(Z)V

    .line 1370
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlippingContainer;->clearAnimation()V

    .line 1362
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1372
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 356
    if-gez v0, :cond_0

    invoke-super {p0, p1}, Lflipboard/gui/flipping/FlipTransitionBase;->a(Landroid/view/View;)I

    move-result v0

    .line 358
    :goto_0
    return v0

    .line 356
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v1

    sub-int/2addr v0, v1

    .line 358
    goto :goto_0
.end method

.method public final a(FI)Lflipboard/gui/flipping/FlipTransitionViews$HopTask;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    .line 1257
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->F:J

    .line 1258
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1259
    iget-wide v4, p0, Lflipboard/gui/flipping/FlipTransitionViews;->O:J

    sub-long v4, v2, v4

    int-to-long v6, p2

    cmp-long v0, v4, v6

    if-gez v0, :cond_1

    .line 1308
    :cond_0
    :goto_0
    return-object v1

    .line 1263
    :cond_1
    iput-wide v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->O:J

    .line 1264
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    .line 1266
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v2, v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v2

    iget-boolean v2, v2, Lflipboard/gui/flipping/SinglePage;->B:Z

    if-eqz v2, :cond_2

    .line 1267
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v2, v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v2

    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->a(I)Lflipboard/gui/flipping/FlippingBitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflipboard/gui/flipping/SinglePage;->a(Lflipboard/gui/flipping/FlippingBitmap;)V

    .line 1269
    :cond_2
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v2

    iget-boolean v2, v2, Lflipboard/gui/flipping/SinglePage;->B:Z

    if-eqz v2, :cond_3

    .line 1270
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->a(I)Lflipboard/gui/flipping/FlippingBitmap;

    move-result-object v0

    invoke-virtual {v2, v0}, Lflipboard/gui/flipping/SinglePage;->a(Lflipboard/gui/flipping/FlippingBitmap;)V

    .line 1274
    :cond_3
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    invoke-virtual {p0, v0, v8, v8}, Lflipboard/gui/flipping/FlipTransitionViews;->a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;FF)Lflipboard/gui/flipping/SinglePage;

    move-result-object v0

    .line 1275
    if-eqz v0, :cond_0

    .line 1276
    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/gui/flipping/SinglePage;->u:Z

    .line 1277
    new-instance v1, Lflipboard/gui/flipping/FlipTransitionViews$11;

    invoke-direct {v1, p0, p1, p2, v0}, Lflipboard/gui/flipping/FlipTransitionViews$11;-><init>(Lflipboard/gui/flipping/FlipTransitionViews;FILflipboard/gui/flipping/SinglePage;)V

    .line 1306
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xa

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    goto :goto_0
.end method

.method public final a(I)Lflipboard/gui/flipping/FlippingBitmap;
    .locals 2

    .prologue
    .line 206
    if-ltz p1, :cond_0

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 207
    :cond_0
    const/4 v0, 0x0

    .line 209
    :goto_0
    return-object v0

    :cond_1
    sget-object v1, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Landroid/view/View;)Lflipboard/gui/flipping/FlippingBitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public final declared-synchronized a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 265
    monitor-enter p0

    :try_start_0
    const-string v2, "addFlippableView"

    invoke-static {v2}, Lflipboard/gui/flipping/FlipTransitionViews;->a(Ljava/lang/String;)V

    .line 266
    if-ltz p1, :cond_0

    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    if-gt p1, v2, :cond_0

    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 269
    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    .line 272
    :cond_0
    if-ltz p1, :cond_1

    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le p1, v2, :cond_e

    .line 273
    :cond_1
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 276
    :goto_0
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v8, v2, :cond_2

    .line 280
    :goto_1
    new-instance v9, Lflipboard/gui/flipping/FlippingContainer;

    invoke-direct {v9, p2, v8}, Lflipboard/gui/flipping/FlippingContainer;-><init>(Landroid/view/View;I)V

    .line 281
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    invoke-virtual {v9, v2}, Lflipboard/gui/flipping/FlippingContainer;->setVisible(Z)V

    .line 284
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v2, v8, v9}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 285
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->H:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0, v9, v8, v2}, Lflipboard/gui/flipping/FlipTransitionViews;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 287
    if-nez v0, :cond_3

    .line 289
    add-int/lit8 v0, v8, 0x1

    move v2, v0

    :goto_2
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 290
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    .line 291
    iput v2, v0, Lflipboard/gui/flipping/FlippingContainer;->c:I

    .line 289
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    move v0, v1

    .line 276
    goto :goto_1

    .line 296
    :cond_3
    invoke-static {p0}, Lflipboard/util/AndroidUtil;->d(Landroid/view/View;)Z

    move-result v2

    .line 297
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    if-gt v8, v0, :cond_4

    .line 298
    :goto_3
    if-gt v1, v8, :cond_5

    .line 299
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    sub-int v3, v1, v3

    invoke-static {v0, v2, v3}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;ZI)V

    .line 298
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v1, v8

    .line 302
    :goto_4
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 303
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    sub-int v3, v1, v3

    invoke-static {v0, v2, v3}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;ZI)V

    .line 302
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 307
    :cond_5
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_8

    .line 308
    new-instance v0, Lflipboard/gui/flipping/TextPageRefresh;

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-direct {v0, p0, v1}, Lflipboard/gui/flipping/TextPageRefresh;-><init>(Lflipboard/gui/flipping/FlipTransitionBase;Lflipboard/gui/flipping/OpenGLTransitionRenderer;)V

    .line 309
    iget-boolean v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->x:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/TextPageRefresh;->a(Z)V

    .line 310
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/TextPageRefresh;->a(F)V

    .line 311
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const-string v2, "setStartBookEndPage"

    invoke-virtual {v1, v2}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v2, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->f:Lflipboard/gui/flipping/TextPageRefresh;

    if-eqz v2, :cond_6

    iget-object v2, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->d:Ljava/util/ArrayList;

    iget-object v3, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->f:Lflipboard/gui/flipping/TextPageRefresh;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_6
    iput-object v0, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->f:Lflipboard/gui/flipping/TextPageRefresh;

    const/4 v2, -0x1

    iput v2, v0, Lflipboard/gui/flipping/TextPageRefresh;->E:I

    iget-object v2, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->d:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    const/4 v2, 0x1

    iput-boolean v2, v0, Lflipboard/gui/flipping/TextPageRefresh;->D:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    invoke-virtual {v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 313
    new-instance v0, Lflipboard/gui/flipping/TextPageLoadMore;

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-direct {v0, p0, v1}, Lflipboard/gui/flipping/TextPageLoadMore;-><init>(Lflipboard/gui/flipping/FlipTransitionBase;Lflipboard/gui/flipping/OpenGLTransitionRenderer;)V

    .line 314
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/TextPageLoadMore;->a(Z)V

    .line 315
    const v1, 0x40490fdb    # (float)Math.PI

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/TextPageLoadMore;->a(F)V

    .line 316
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const-string v2, "setEndBookEndPage"

    invoke-virtual {v1, v2}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-object v2, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->g:Lflipboard/gui/flipping/TextPageLoadMore;

    if-eqz v2, :cond_7

    iget-object v2, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->d:Ljava/util/ArrayList;

    iget-object v3, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->g:Lflipboard/gui/flipping/TextPageLoadMore;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_7
    iput-object v0, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->g:Lflipboard/gui/flipping/TextPageLoadMore;

    iget-object v2, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x1

    iput-boolean v2, v0, Lflipboard/gui/flipping/TextPageLoadMore;->D:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :try_start_4
    invoke-virtual {v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 319
    :cond_8
    new-instance v0, Lflipboard/gui/flipping/SinglePage;

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-direct {v0, p0, v1}, Lflipboard/gui/flipping/SinglePage;-><init>(Lflipboard/gui/flipping/FlipTransitionBase;Lflipboard/gui/flipping/OpenGLTransitionRenderer;)V

    .line 320
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    if-ge v8, v1, :cond_b

    .line 321
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/SinglePage;->a(F)V

    .line 325
    :goto_5
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getWidth()I

    move-result v1

    if-lez v1, :cond_9

    .line 326
    iget-boolean v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->w:Z

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getHeight()I

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lflipboard/gui/flipping/SinglePage;->a(ZIIIIFF)V

    .line 328
    :cond_9
    iget-object v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const-string v1, "addPage"

    invoke-virtual {v3, v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    iget-object v1, v3, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->f:Lflipboard/gui/flipping/TextPageRefresh;

    if-eqz v1, :cond_a

    iget-object v1, v3, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->g:Lflipboard/gui/flipping/TextPageLoadMore;

    if-nez v1, :cond_c

    :cond_a
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Set start and end bookend pages before adding regular pages"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_6
    invoke-virtual {v3}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 265
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 311
    :catchall_2
    move-exception v0

    :try_start_7
    invoke-virtual {v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0

    .line 316
    :catchall_3
    move-exception v0

    invoke-virtual {v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0

    .line 323
    :cond_b
    const v1, 0x40490fdb    # (float)Math.PI

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/SinglePage;->a(F)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_5

    .line 328
    :cond_c
    add-int/lit8 v1, v8, 0x1

    move v2, v1

    :goto_6
    :try_start_8
    iget-object v1, v3, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_d

    iget-object v1, v3, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/flipping/SinglePage;

    iget v4, v1, Lflipboard/gui/flipping/SinglePage;->E:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v1, Lflipboard/gui/flipping/SinglePage;->E:I

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_6

    :cond_d
    iput v8, v0, Lflipboard/gui/flipping/SinglePage;->E:I

    iget-object v1, v3, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v8, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v1, v3, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->d:Ljava/util/ArrayList;

    add-int/lit8 v2, v8, 0x1

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    invoke-virtual {v3}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 330
    monitor-exit p0

    return-object v9

    :cond_e
    move v8, p1

    goto/16 :goto_0
.end method

.method protected final a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;FFZ)Lflipboard/gui/flipping/SinglePage;
    .locals 11

    .prologue
    .line 1105
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_0

    .line 1106
    const/4 v0, 0x0

    .line 1218
    :goto_0
    return-object v0

    .line 1108
    :cond_0
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1110
    const/4 v0, 0x0

    goto :goto_0

    .line 1112
    :cond_1
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    .line 1114
    const/4 v2, 0x0

    .line 1115
    iget-object v1, v0, Lflipboard/gui/flipping/FlippingContainer;->b:Landroid/view/View;

    instance-of v1, v1, Lflipboard/gui/item/FlipmagDetailViewTablet;

    if-eqz v1, :cond_7

    .line 1116
    iget-object v1, v0, Lflipboard/gui/flipping/FlippingContainer;->b:Landroid/view/View;

    check-cast v1, Lflipboard/gui/item/FlipmagDetailViewTablet;

    .line 1117
    invoke-virtual {v1}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getCurrentViewIndex()I

    move-result v3

    .line 1120
    sget-object v4, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne p1, v4, :cond_3

    invoke-virtual {v1}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getNumberOfPages()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v3, v4, :cond_3

    .line 1121
    add-int/lit8 v3, v3, 0x1

    .line 1122
    const/4 v4, 0x1

    move v5, v3

    .line 1130
    :goto_1
    if-eqz v4, :cond_b

    .line 1132
    sget-object v3, Lflipboard/gui/flipping/FlipTransitionViews$15;->a:[I

    invoke-virtual {p1}, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->ordinal()I

    move-result v6

    aget v3, v3, v6

    packed-switch v3, :pswitch_data_0

    .line 1156
    iget v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/lit8 v3, v3, -0x1

    move v10, v3

    move v3, v2

    move v2, v10

    .line 1158
    :goto_2
    iget-object v6, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v2, v6, :cond_2

    .line 1159
    iget-object v6, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v6, v2}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v6

    .line 1160
    sget-object v7, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    invoke-virtual {v7}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a()Lflipboard/gui/flipping/FlippingBitmap;

    move-result-object v7

    .line 1161
    new-instance v8, Landroid/graphics/Canvas;

    iget-object v9, v7, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    invoke-direct {v8, v9}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1162
    invoke-virtual {v1, v8, v5}, Lflipboard/gui/item/FlipmagDetailViewTablet;->a(Landroid/graphics/Canvas;I)V

    .line 1163
    invoke-virtual {v6, v7}, Lflipboard/gui/flipping/SinglePage;->a(Lflipboard/gui/flipping/FlippingBitmap;)V

    .line 1164
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/flipping/FlippingContainer;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lflipboard/gui/flipping/FlippingContainer;->e:Z

    .line 1165
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/flipping/FlippingContainer;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lflipboard/gui/flipping/FlippingContainer;->d:Z

    :cond_2
    :goto_3
    move v1, v4

    .line 1171
    :goto_4
    if-nez v1, :cond_9

    .line 1173
    sget-object v1, Lflipboard/gui/flipping/FlipTransitionViews$15;->a:[I

    invoke-virtual {p1}, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 1182
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/lit8 v1, v1, -0x1

    .line 1184
    :goto_5
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v2, v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v2

    .line 1185
    iget-object v0, v0, Lflipboard/gui/flipping/FlippingContainer;->b:Landroid/view/View;

    instance-of v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    if-ltz v1, :cond_9

    .line 1186
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    .line 1188
    iget-object v1, v0, Lflipboard/gui/flipping/FlippingContainer;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    const-string v4, "fake"

    if-ne v1, v4, :cond_8

    .line 1189
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    .line 1190
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->performHapticFeedback(I)Z

    .line 1191
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1123
    :cond_3
    sget-object v4, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->b:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne p1, v4, :cond_4

    if-lez v3, :cond_4

    .line 1124
    add-int/lit8 v3, v3, -0x1

    .line 1125
    const/4 v4, 0x1

    move v5, v3

    goto/16 :goto_1

    .line 1127
    :cond_4
    const/4 v4, 0x0

    .line 1128
    const/4 v3, 0x0

    move v5, v3

    goto/16 :goto_1

    .line 1134
    :pswitch_0
    iget v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/lit8 v3, v3, 0x1

    .line 1135
    iget-object v6, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ne v3, v6, :cond_5

    .line 1136
    new-instance v2, Landroid/view/View;

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v2, v6}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1137
    const-string v6, "fake"

    invoke-virtual {v2, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1138
    const/4 v6, -0x1

    invoke-virtual {p0, v6, v2}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    .line 1139
    const/4 v2, 0x1

    .line 1141
    :cond_5
    iget-object v6, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget v7, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lflipboard/gui/flipping/SinglePage;->a(F)V

    move v10, v3

    move v3, v2

    move v2, v10

    .line 1142
    goto/16 :goto_2

    .line 1145
    :pswitch_1
    iget v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/lit8 v6, v3, -0x1

    .line 1146
    const/4 v3, 0x0

    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1147
    if-eq v6, v3, :cond_6

    .line 1148
    new-instance v2, Landroid/view/View;

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v2, v6}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1149
    const-string v6, "fake"

    invoke-virtual {v2, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1150
    const/4 v6, 0x0

    invoke-virtual {p0, v6, v2}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    .line 1151
    const/4 v2, 0x1

    .line 1153
    :cond_6
    iget-object v6, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget v7, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    invoke-virtual {v6, v7}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v6

    const v7, 0x40490fdb    # (float)Math.PI

    invoke-virtual {v6, v7}, Lflipboard/gui/flipping/SinglePage;->a(F)V

    move v10, v3

    move v3, v2

    move v2, v10

    .line 1154
    goto/16 :goto_2

    .line 1169
    :cond_7
    const/4 v1, 0x0

    move v3, v2

    goto/16 :goto_4

    .line 1175
    :pswitch_2
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/lit8 v1, v1, 0x1

    .line 1176
    goto/16 :goto_5

    .line 1179
    :pswitch_3
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/lit8 v1, v1, -0x1

    .line 1180
    goto/16 :goto_5

    .line 1193
    :cond_8
    sget-object v1, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    invoke-virtual {v1, v0}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Landroid/view/View;)Lflipboard/gui/flipping/FlippingBitmap;

    move-result-object v1

    .line 1194
    invoke-virtual {v2, v1}, Lflipboard/gui/flipping/SinglePage;->a(Lflipboard/gui/flipping/FlippingBitmap;)V

    .line 1195
    const/4 v1, 0x0

    iput-boolean v1, v0, Lflipboard/gui/flipping/FlippingContainer;->e:Z

    .line 1196
    const/4 v1, 0x0

    iput-boolean v1, v0, Lflipboard/gui/flipping/FlippingContainer;->d:Z

    .line 1199
    :cond_9
    invoke-super {p0, p1, p2, p3, p4}, Lflipboard/gui/flipping/FlipTransitionBase;->a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;FFZ)Lflipboard/gui/flipping/SinglePage;

    move-result-object v0

    .line 1200
    if-eqz v3, :cond_a

    .line 1201
    new-instance v1, Lflipboard/gui/flipping/FlipTransitionViews$10;

    invoke-direct {v1, p0}, Lflipboard/gui/flipping/FlipTransitionViews$10;-><init>(Lflipboard/gui/flipping/FlipTransitionViews;)V

    invoke-virtual {p0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->a(Lflipboard/util/Observer;)V

    .line 1217
    :cond_a
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->M:Lflipboard/util/Observable;

    sget-object v2, Lflipboard/gui/flipping/FlipTransitionViews$Message;->a:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Observable;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_b
    move v3, v2

    goto/16 :goto_3

    .line 1132
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 1173
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x32

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 807
    sget-object v2, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->D:Z

    if-eqz v2, :cond_1

    .line 917
    :cond_0
    :goto_0
    return-void

    .line 811
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 812
    iget-object v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->ae:Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;

    .line 813
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->I:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v6

    .line 815
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iput-wide v8, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->a:J

    iget-wide v8, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->a:J

    const-wide/16 v10, 0xc

    add-long/2addr v8, v10

    iput-wide v8, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->b:J

    iput v1, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->i:I

    iput v1, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->h:I

    iput v1, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->g:I

    iput v1, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->f:I

    iput v1, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->e:I

    iput v1, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->d:I

    iput-boolean v1, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->c:Z

    .line 816
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 820
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->m()Z

    move-result v2

    if-nez v2, :cond_0

    .line 830
    iget v8, p0, Lflipboard/gui/flipping/FlipTransitionViews;->ac:I

    .line 831
    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    sub-int/2addr v2, v8

    add-int/lit8 v2, v2, -0x1

    :goto_1
    if-ltz v2, :cond_3

    .line 833
    iget-object v9, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v9, v2}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v9

    .line 834
    iget-boolean v10, v9, Lflipboard/gui/flipping/SinglePage;->B:Z

    if-nez v10, :cond_2

    iget-boolean v10, v9, Lflipboard/gui/flipping/SinglePage;->A:Z

    if-nez v10, :cond_2

    .line 835
    iput-boolean v0, v9, Lflipboard/gui/flipping/SinglePage;->A:Z

    .line 836
    sget-object v9, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    new-array v9, v0, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v1

    .line 837
    iget v9, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->i:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->i:I

    .line 831
    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 842
    :cond_3
    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->L:I

    sub-int v9, v2, v8

    .line 843
    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/2addr v2, v9

    add-int/lit8 v2, v2, 0x1

    :goto_2
    if-ge v2, v7, :cond_5

    .line 845
    iget-object v10, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v10, v2}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v10

    .line 846
    iget-boolean v11, v10, Lflipboard/gui/flipping/SinglePage;->B:Z

    if-nez v11, :cond_4

    iget-boolean v11, v10, Lflipboard/gui/flipping/SinglePage;->A:Z

    if-nez v11, :cond_4

    .line 847
    iput-boolean v0, v10, Lflipboard/gui/flipping/SinglePage;->A:Z

    .line 848
    iget v10, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->i:I

    add-int/lit8 v10, v10, 0x1

    iput v10, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->i:I

    .line 849
    sget-object v10, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    new-array v10, v0, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v1

    .line 843
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 858
    :cond_5
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    if-gtz v2, :cond_0

    .line 864
    sget-object v2, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    iget-object v2, v2, Lflipboard/gui/flipping/ViewScreenshotCreator;->a:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const-string v7, "checkTextures"

    invoke-virtual {v2, v7}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 865
    :cond_6
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->J:Ljava/lang/Runnable;

    invoke-virtual {v0, v12, v13, v1}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    goto/16 :goto_0

    :cond_7
    move v2, v1

    .line 864
    goto :goto_3

    .line 872
    :cond_8
    :try_start_0
    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    invoke-direct {p0, v2, v3}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILflipboard/gui/flipping/FlipTransitionViews$TextureStats;)V

    .line 878
    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v7

    move v2, v0

    .line 879
    :goto_4
    if-gt v2, v7, :cond_b

    .line 880
    if-gt v2, v9, :cond_9

    .line 881
    iget v10, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/2addr v10, v2

    invoke-direct {p0, v10, v3}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILflipboard/gui/flipping/FlipTransitionViews$TextureStats;)V

    .line 883
    :cond_9
    if-gt v2, v8, :cond_a

    .line 884
    iget v10, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    sub-int/2addr v10, v2

    invoke-direct {p0, v10, v3}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILflipboard/gui/flipping/FlipTransitionViews$TextureStats;)V

    .line 879
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 888
    :cond_b
    sget-object v2, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    iget-boolean v2, v2, Lflipboard/util/Log;->f:Z

    if-eqz v2, :cond_c

    .line 889
    iget v2, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->d:I

    iget v7, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->e:I

    add-int/2addr v2, v7

    iget v7, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->h:I

    sub-int/2addr v2, v7

    if-lez v2, :cond_c

    .line 890
    sget-object v2, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->d:I

    iget v9, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->e:I

    add-int/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v2, v7

    const/4 v7, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v4, v8, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v7

    const/4 v4, 0x2

    iget-object v5, p0, Lflipboard/gui/flipping/FlipTransitionViews;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x3

    iget v5, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x4

    iget v5, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->e:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x5

    iget v5, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->h:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x6

    iget v5, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->f:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x7

    iget v5, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->g:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    const/16 v4, 0x8

    iget v5, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->i:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    .line 912
    :cond_c
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    if-nez v2, :cond_e

    iget v2, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->h:I

    iget v4, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->f:I

    add-int/2addr v2, v4

    iget v3, v3, Lflipboard/gui/flipping/FlipTransitionViews$TextureStats;->g:I

    add-int/2addr v2, v3

    if-lez v2, :cond_f

    :goto_5
    if-nez v0, :cond_d

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->I:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v6, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-nez v0, :cond_e

    .line 913
    :cond_d
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-wide/16 v2, 0x32

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->J:Ljava/lang/Runnable;

    invoke-virtual {v0, v2, v3, v1}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 916
    :cond_e
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    goto/16 :goto_0

    :cond_f
    move v0, v1

    .line 912
    goto :goto_5

    .line 916
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0
.end method

.method public final a(IZ)V
    .locals 2

    .prologue
    .line 221
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/flipping/FlipTransitionViews$4;

    invoke-direct {v1, p0, p1, p2}, Lflipboard/gui/flipping/FlipTransitionViews$4;-><init>(Lflipboard/gui/flipping/FlipTransitionViews;IZ)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 243
    return-void
.end method

.method public final a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 451
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne p1, v0, :cond_0

    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    :cond_0
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->b:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne p1, v0, :cond_2

    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    if-nez v0, :cond_2

    .line 452
    :cond_1
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    const-string v1, "flipWillComplete was called when we were already at the beginning or end. Ignoring"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 510
    :goto_0
    return-void

    .line 455
    :cond_2
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lflipboard/gui/flipping/FlippingContainer;

    .line 456
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const-string v1, "flipWillComplete"

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V

    .line 459
    iget-object v0, v5, Lflipboard/gui/flipping/FlippingContainer;->b:Landroid/view/View;

    instance-of v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;

    if-eqz v0, :cond_6

    .line 460
    iget-object v0, v5, Lflipboard/gui/flipping/FlippingContainer;->b:Landroid/view/View;

    check-cast v0, Lflipboard/gui/item/FlipmagDetailViewTablet;

    .line 461
    invoke-virtual {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getCurrentViewIndex()I

    move-result v1

    .line 462
    sget-object v2, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne p1, v2, :cond_4

    invoke-virtual {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getNumberOfPages()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_4

    .line 463
    add-int/lit8 v4, v1, 0x1

    move v2, v7

    .line 478
    :goto_1
    if-nez v2, :cond_3

    .line 479
    :try_start_0
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne p1, v0, :cond_7

    move v0, v7

    :goto_2
    add-int/2addr v0, v1

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 482
    :cond_3
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 484
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lflipboard/gui/flipping/FlippingContainer;

    .line 486
    if-eqz v2, :cond_8

    .line 487
    iget-object v0, v5, Lflipboard/gui/flipping/FlippingContainer;->b:Landroid/view/View;

    check-cast v0, Lflipboard/gui/item/FlipmagDetailViewTablet;

    .line 488
    invoke-virtual {v0, v4}, Lflipboard/gui/item/FlipmagDetailViewTablet;->setNextViewIndex(I)V

    move-object v3, v0

    .line 492
    :goto_3
    iget-object v8, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    new-instance v0, Lflipboard/gui/flipping/FlipTransitionViews$5;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lflipboard/gui/flipping/FlipTransitionViews$5;-><init>(Lflipboard/gui/flipping/FlipTransitionViews;ZLflipboard/gui/item/FlipmagDetailViewTablet;ILflipboard/gui/flipping/FlippingContainer;Lflipboard/gui/flipping/FlippingContainer;)V

    invoke-virtual {v8, v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/Runnable;)V

    .line 507
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->d:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 508
    iput-boolean v7, p0, Lflipboard/gui/flipping/FlipTransitionViews;->v:Z

    .line 509
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->M:Lflipboard/util/Observable;

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionViews$Message;->c:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    invoke-virtual {v0, v1, p1}, Lflipboard/util/Observable;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 465
    :cond_4
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->b:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne p1, v0, :cond_5

    if-lez v1, :cond_5

    .line 466
    add-int/lit8 v4, v1, -0x1

    move v2, v7

    .line 467
    goto :goto_1

    :cond_5
    move v2, v4

    .line 472
    goto :goto_1

    :cond_6
    move v2, v4

    .line 474
    goto :goto_1

    .line 479
    :cond_7
    const/4 v0, -0x1

    goto :goto_2

    .line 482
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0

    .line 490
    :cond_8
    const/4 v3, 0x0

    goto :goto_3
.end method

.method public final a(Lflipboard/util/Observer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/util/Observer",
            "<",
            "Ljava/lang/Object;",
            "Lflipboard/gui/flipping/FlipTransitionViews$Message;",
            "Lflipboard/gui/flipping/FlipTransitionBase$Direction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->M:Lflipboard/util/Observable;

    invoke-virtual {v0, p1}, Lflipboard/util/Observable;->b(Lflipboard/util/Observer;)V

    .line 99
    return-void
.end method

.method protected final a(Lflipboard/gui/flipping/SinglePage;I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 187
    if-ltz p2, :cond_0

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v0

    if-lt p2, v0, :cond_1

    :cond_0
    move v0, v2

    .line 200
    :goto_0
    return v0

    .line 190
    :cond_1
    iget-boolean v0, p1, Lflipboard/gui/flipping/SinglePage;->B:Z

    if-eqz v0, :cond_2

    move v0, v1

    .line 191
    goto :goto_0

    .line 194
    :cond_2
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    .line 195
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->r()Z

    move-result v3

    if-nez v3, :cond_3

    iget-boolean v3, v0, Lflipboard/gui/flipping/FlippingContainer;->k:Z

    if-eqz v3, :cond_6

    .line 196
    :cond_3
    if-ltz p2, :cond_6

    iget-object v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge p2, v3, :cond_6

    .line 197
    iget-boolean v3, v0, Lflipboard/gui/flipping/FlippingContainer;->e:Z

    if-nez v3, :cond_4

    iget-boolean v0, v0, Lflipboard/gui/flipping/FlippingContainer;->k:Z

    if-eqz v0, :cond_5

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0

    :cond_6
    move v0, v2

    .line 200
    goto :goto_0
.end method

.method protected final b(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 926
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    .line 927
    iput-boolean v2, v0, Lflipboard/gui/flipping/FlippingContainer;->e:Z

    iput-boolean v2, v0, Lflipboard/gui/flipping/FlippingContainer;->d:Z

    .line 928
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    .line 929
    invoke-super {p0, p1}, Lflipboard/gui/flipping/FlipTransitionBase;->b(I)V

    .line 930
    return-void
.end method

.method public b(Lflipboard/gui/flipping/FlipTransitionBase$Direction;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 531
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->y:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->y:I

    .line 532
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->z:I

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->z:I

    .line 535
    invoke-static {p0}, Lflipboard/util/AndroidUtil;->d(Landroid/view/View;)Z

    move-result v0

    .line 536
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 537
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/flipping/FlipTransitionViews$6;

    invoke-direct {v2, p0, v0}, Lflipboard/gui/flipping/FlipTransitionViews$6;-><init>(Lflipboard/gui/flipping/FlipTransitionViews;Z)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 546
    :cond_0
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->ad:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne v0, p1, :cond_2

    .line 547
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne p1, v0, :cond_1

    .line 548
    const/4 v0, 0x2

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->ac:I

    add-int/lit8 v1, v1, -0x3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->ac:I

    .line 565
    :goto_0
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->M:Lflipboard/util/Observable;

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionViews$Message;->b:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    invoke-virtual {v0, v1, p1}, Lflipboard/util/Observable;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 566
    return-void

    .line 550
    :cond_1
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->L:I

    add-int/lit8 v0, v0, -0x2

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->ac:I

    add-int/lit8 v1, v1, 0x3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->ac:I

    goto :goto_0

    .line 555
    :cond_2
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews$15;->a:[I

    invoke-virtual {p1}, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 563
    :goto_1
    iput-object p1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->ad:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    goto :goto_0

    .line 557
    :pswitch_0
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->L:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->ac:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->ac:I

    goto :goto_1

    .line 560
    :pswitch_1
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->ac:I

    add-int/lit8 v0, v0, -0x1

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->ac:I

    goto :goto_1

    .line 555
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Lflipboard/util/Observer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/util/Observer",
            "<",
            "Ljava/lang/Object;",
            "Lflipboard/gui/flipping/FlipTransitionViews$Message;",
            "Lflipboard/gui/flipping/FlipTransitionBase$Direction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->M:Lflipboard/util/Observable;

    invoke-virtual {v0, p1}, Lflipboard/util/Observable;->c(Lflipboard/util/Observer;)V

    .line 103
    return-void
.end method

.method public final declared-synchronized c(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 380
    monitor-enter p0

    :try_start_0
    const-string v0, "removeFlippableView"

    invoke-static {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->a(Ljava/lang/String;)V

    .line 381
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    .line 383
    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    if-ne p1, v2, :cond_6

    .line 386
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/lit8 v3, v3, 0x2

    if-le v2, v3, :cond_4

    .line 387
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/flipping/FlippingContainer;

    .line 395
    :cond_0
    :goto_0
    if-eqz v1, :cond_5

    .line 396
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lflipboard/gui/flipping/FlippingContainer;->setVisible(Z)V

    .line 405
    :cond_1
    :goto_1
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 406
    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->removeView(Landroid/view/View;)V

    .line 407
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlippingContainer;->removeAllViews()V

    .line 408
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const-string v0, "removePage"

    invoke-virtual {v2, v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, v2, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/SinglePage;

    iget-boolean v1, v0, Lflipboard/gui/flipping/SinglePage;->z:Z

    if-eqz v1, :cond_3

    iget-boolean v1, v0, Lflipboard/gui/flipping/SinglePage;->H:Z

    if-eqz v1, :cond_2

    iget-object v1, v2, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->n:Lflipboard/gui/flipping/FlipTransitionBase;

    iget-object v3, v0, Lflipboard/gui/flipping/SinglePage;->G:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    invoke-virtual {v1, v3}, Lflipboard/gui/flipping/FlipTransitionBase;->b(Lflipboard/gui/flipping/FlipTransitionBase$Direction;)V

    :cond_2
    iget-object v1, v2, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->n:Lflipboard/gui/flipping/FlipTransitionBase;

    invoke-virtual {v1, v0}, Lflipboard/gui/flipping/FlipTransitionBase;->a(Lflipboard/gui/flipping/SinglePage;)V

    :cond_3
    const/4 v1, 0x0

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v0, v3}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljavax/microedition/khronos/opengles/GL10;Lflipboard/gui/flipping/SinglePage;Z)V

    iget-object v0, v2, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->d:Ljava/util/ArrayList;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, p1, 0x1

    move v1, v0

    :goto_2
    iget-object v0, v2, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    iget-object v0, v2, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/SinglePage;

    iget v3, v0, Lflipboard/gui/flipping/SinglePage;->E:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v0, Lflipboard/gui/flipping/SinglePage;->E:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 389
    :cond_4
    :try_start_2
    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    if-lez v2, :cond_0

    .line 391
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    .line 392
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/flipping/FlippingContainer;

    .line 393
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    invoke-virtual {v2, v3}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v2

    const v3, 0x40490fdb    # (float)Math.PI

    invoke-virtual {v2, v3}, Lflipboard/gui/flipping/SinglePage;->a(F)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 380
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 398
    :cond_5
    :try_start_3
    sget-object v1, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    const-string v2, "We\'re removing the only flippable view, that could be bad"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 400
    :cond_6
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    if-ge p1, v1, :cond_1

    .line 402
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    goto/16 :goto_1

    .line 408
    :cond_7
    invoke-virtual {v2}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    move v1, p1

    .line 409
    :goto_3
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 410
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    .line 411
    iput v1, v0, Lflipboard/gui/flipping/FlippingContainer;->c:I

    .line 409
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 408
    :catchall_1
    move-exception v0

    invoke-virtual {v2}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0

    .line 414
    :cond_8
    invoke-static {p0}, Lflipboard/util/AndroidUtil;->d(Landroid/view/View;)Z

    move-result v1

    .line 415
    :goto_4
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_9

    .line 416
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    sub-int v2, p1, v2

    invoke-static {v0, v1, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;ZI)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 415
    add-int/lit8 p1, p1, 0x1

    goto :goto_4

    .line 418
    :cond_9
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized c(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 370
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    if-nez v2, :cond_1

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    iget-object v0, v0, Lflipboard/gui/flipping/FlippingContainer;->b:Landroid/view/View;

    if-ne v0, p1, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 371
    :cond_1
    if-ltz v1, :cond_2

    .line 372
    invoke-virtual {p0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->c(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 376
    :goto_1
    monitor-exit p0

    return-void

    .line 374
    :cond_2
    :try_start_1
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    const-string v1, "Can\'t find view %s in Flipping container while trying to remove it"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 370
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1428
    iget-boolean v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->P:Z

    if-eqz v1, :cond_0

    .line 1429
    invoke-direct {p0, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->f(I)V

    .line 1430
    const/4 v0, 0x1

    .line 1432
    :cond_0
    return v0
.end method

.method public final d(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 570
    if-ltz p1, :cond_0

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 571
    :cond_0
    const/4 v0, 0x0

    .line 573
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlippingContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public d()V
    .locals 3

    .prologue
    .line 514
    invoke-super {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->d()V

    .line 515
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->M:Lflipboard/util/Observable;

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionViews$Message;->d:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Observable;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 516
    return-void
.end method

.method public final d(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 434
    const-string v0, "swapFlippableView"

    invoke-static {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->a(Ljava/lang/String;)V

    .line 435
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 436
    new-instance v1, Lflipboard/gui/flipping/FlippingContainer;

    invoke-direct {v1, p1, v4}, Lflipboard/gui/flipping/FlippingContainer;-><init>(Landroid/view/View;I)V

    .line 437
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    .line 438
    iget-boolean v2, v0, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    invoke-virtual {v1, v2}, Lflipboard/gui/flipping/FlippingContainer;->setVisible(Z)V

    .line 439
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 440
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v2, v4, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 441
    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->removeView(Landroid/view/View;)V

    .line 442
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlippingContainer;->removeAllViews()V

    .line 443
    invoke-virtual {p0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->addView(Landroid/view/View;)V

    .line 447
    :goto_0
    return-void

    .line 445
    :cond_0
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    const-string v1, "Invalid index (%s) to swap for child (%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/high16 v4, 0x40400000    # 3.0f

    const/4 v0, 0x1

    .line 1438
    const/4 v1, 0x0

    .line 1439
    iget-boolean v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->a:Z

    if-eqz v2, :cond_2

    .line 1440
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->R:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1441
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->R:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1455
    :goto_0
    if-nez v0, :cond_0

    .line 1456
    invoke-super {p0, p1}, Lflipboard/gui/flipping/FlipTransitionBase;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1458
    :cond_0
    return v0

    .line 1443
    :cond_1
    iget-boolean v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->P:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->A:Z

    if-nez v2, :cond_2

    .line 1444
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 1445
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 1446
    mul-float/2addr v1, v4

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v1, v3

    float-to-int v1, v1

    .line 1447
    mul-float/2addr v2, v4

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-int v2, v2

    .line 1448
    mul-int/lit8 v2, v2, 0x3

    add-int/2addr v1, v2

    .line 1449
    add-int/lit8 v1, v1, -0x4

    invoke-direct {p0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->f(I)V

    .line 1451
    iput-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->A:Z

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final e(I)V
    .locals 4

    .prologue
    .line 660
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->I:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    if-nez v0, :cond_0

    .line 661
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    int-to-long v2, p1

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->J:Ljava/lang/Runnable;

    invoke-virtual {v0, v2, v3, v1}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    .line 663
    :cond_0
    return-void
.end method

.method protected getChildDrawingOrder(II)I
    .locals 1

    .prologue
    .line 1031
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/2addr v0, p2

    rem-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic getCurrentView()Landroid/view/View;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentView()Lflipboard/gui/flipping/FlippingContainer;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentView()Lflipboard/gui/flipping/FlippingContainer;
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    return-object v0
.end method

.method public getDesiredNumberOfTextures()I
    .locals 4

    .prologue
    .line 135
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    check-cast v0, Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->f()J

    move-result-wide v0

    .line 136
    const-wide/32 v2, 0x20000000

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 137
    const/4 v0, 0x6

    .line 139
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public getFlippableViews()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/flipping/FlippingContainer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected bridge synthetic getNextView()Landroid/view/View;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getNextView()Lflipboard/gui/flipping/FlippingContainer;

    move-result-object v0

    return-object v0
.end method

.method protected getNextView()Lflipboard/gui/flipping/FlippingContainer;
    .locals 2

    .prologue
    .line 155
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    .line 156
    const/4 v0, 0x0

    .line 158
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    goto :goto_0
.end method

.method public getNumberOfPages()I
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method protected bridge synthetic getPreviousView()Landroid/view/View;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getPreviousView()Lflipboard/gui/flipping/FlippingContainer;

    move-result-object v0

    return-object v0
.end method

.method protected getPreviousView()Lflipboard/gui/flipping/FlippingContainer;
    .locals 2

    .prologue
    .line 163
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    if-nez v0, :cond_0

    .line 164
    const/4 v0, 0x0

    .line 166
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    goto :goto_0
.end method

.method protected final l()V
    .locals 3

    .prologue
    .line 520
    invoke-super {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->l()V

    .line 521
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->M:Lflipboard/util/Observable;

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionViews$Message;->e:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Observable;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 522
    return-void
.end method

.method public final n()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1529
    invoke-super {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->n()V

    .line 1531
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    .line 1532
    sget-object v4, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v5, 0x1

    iget-boolean v6, v0, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object v0, v4, v5

    .line 1533
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1534
    goto :goto_0

    .line 1535
    :cond_0
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 601
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->K:Z

    .line 602
    invoke-super {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->onAttachedToWindow()V

    .line 603
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->f()V

    .line 605
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->U:Lcom/amazon/motiongestures/GestureManager;

    if-eqz v0, :cond_0

    .line 606
    invoke-static {}, Lcom/amazon/motiongestures/Gesture;->a()Lcom/amazon/motiongestures/Gesture;

    .line 607
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->U:Lcom/amazon/motiongestures/GestureManager;

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->V:Lcom/amazon/motiongestures/GestureListener;

    invoke-static {}, Lcom/amazon/motiongestures/GestureManager;->b()V

    .line 608
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->U:Lcom/amazon/motiongestures/GestureManager;

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->W:Lcom/amazon/motiongestures/GestureListener;

    invoke-static {}, Lcom/amazon/motiongestures/GestureManager;->b()V

    .line 611
    :cond_0
    new-instance v0, Lflipboard/gui/flipping/FlipTransitionViews$7;

    invoke-direct {v0, p0}, Lflipboard/gui/flipping/FlipTransitionViews$7;-><init>(Lflipboard/gui/flipping/FlipTransitionViews;)V

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->aa:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    .line 619
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->aa:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 621
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 584
    iput-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->K:Z

    .line 585
    invoke-super {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->onDetachedFromWindow()V

    .line 586
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const-string v2, "clearTextures"

    invoke-virtual {v1, v2}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V

    :goto_0
    :try_start_0
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1, v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, v1, Lflipboard/gui/flipping/SinglePage;->A:Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->h:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->requestRender()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 588
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->U:Lcom/amazon/motiongestures/GestureManager;

    if-eqz v0, :cond_1

    .line 589
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->U:Lcom/amazon/motiongestures/GestureManager;

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->V:Lcom/amazon/motiongestures/GestureListener;

    invoke-static {}, Lcom/amazon/motiongestures/GestureManager;->c()V

    .line 590
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->U:Lcom/amazon/motiongestures/GestureManager;

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->W:Lcom/amazon/motiongestures/GestureListener;

    invoke-static {}, Lcom/amazon/motiongestures/GestureManager;->c()V

    .line 592
    :cond_1
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->aa:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    if-eqz v0, :cond_2

    .line 593
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->aa:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 594
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->aa:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    .line 596
    :cond_2
    return-void

    .line 586
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0
.end method

.method public onLayout(ZIIII)V
    .locals 13

    .prologue
    .line 1002
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 1004
    invoke-super/range {p0 .. p5}, Lflipboard/gui/flipping/FlipTransitionBase;->onLayout(ZIIII)V

    .line 1005
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const-string v1, "onLayout"

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V

    .line 1007
    :try_start_0
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1008
    sub-int v9, p4, p2

    .line 1009
    sub-int v12, p5, p3

    .line 1012
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v8, v0, -0x1

    if-ltz v8, :cond_0

    .line 1013
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v9, v12}, Lflipboard/gui/flipping/FlippingContainer;->layout(IIII)V

    .line 1014
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0, v8}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v0

    iget-boolean v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->w:Z

    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->g:I

    iget v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->f:I

    iget v4, p0, Lflipboard/gui/flipping/FlipTransitionViews;->g:I

    iget v5, p0, Lflipboard/gui/flipping/FlipTransitionViews;->f:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lflipboard/gui/flipping/SinglePage;->a(ZIIIIFF)V

    move v0, v8

    goto :goto_0

    .line 1016
    :cond_0
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->f:Lflipboard/gui/flipping/TextPageRefresh;

    iget-boolean v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->w:Z

    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->g:I

    iget v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->f:I

    iget v4, p0, Lflipboard/gui/flipping/FlipTransitionViews;->g:I

    iget v5, p0, Lflipboard/gui/flipping/FlipTransitionViews;->f:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lflipboard/gui/flipping/SinglePage;->a(ZIIIIFF)V

    .line 1017
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->g:Lflipboard/gui/flipping/TextPageLoadMore;

    iget-boolean v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->w:Z

    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->g:I

    iget v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->f:I

    iget v4, p0, Lflipboard/gui/flipping/FlipTransitionViews;->g:I

    iget v5, p0, Lflipboard/gui/flipping/FlipTransitionViews;->f:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lflipboard/gui/flipping/TextPageLoadMore;->a(ZIIIIFF)V

    .line 1019
    :cond_1
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_2

    .line 1020
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v10

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1023
    :cond_2
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 1024
    return-void

    .line 1023
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0
.end method

.method public onMeasure(II)V
    .locals 6

    .prologue
    .line 981
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 984
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->setMeasuredDimension(II)V

    .line 987
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->d:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->getMeasuredWidth()I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->d:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->getMeasuredHeight()I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 988
    :cond_0
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->d:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0, p1, p2}, Landroid/opengl/GLSurfaceView;->measure(II)V

    .line 992
    :cond_1
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 993
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    goto :goto_0

    .line 995
    :cond_2
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_3

    .line 996
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    .line 998
    :cond_3
    return-void
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 13

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    const/high16 v3, 0x3f800000    # 1.0f

    const v11, 0x3f2eeeef

    const/4 v2, 0x0

    const v10, 0x3ea22222

    .line 1462
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    sub-float v0, v3, v0

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->T:F

    .line 1463
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->T:F

    float-to-double v0, v0

    const-wide v4, 0x3ff23d70a3d70a3dL    # 1.14

    mul-double/2addr v0, v4

    double-to-float v0, v0

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->T:F

    .line 1464
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->S:F

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->T:F

    sub-float/2addr v0, v1

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->S:F

    .line 1465
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->S:F

    invoke-static {v0, v2, v3}, Lflipboard/util/JavaUtil;->a(FFF)F

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->S:F

    .line 1466
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x3

    int-to-float v4, v0

    .line 1468
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x3

    int-to-float v5, v0

    .line 1469
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    const/16 v0, 0x9

    if-ge v3, v0, :cond_4

    .line 1471
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/2addr v0, v3

    add-int/lit8 v1, v0, -0x4

    .line 1472
    if-ltz v1, :cond_0

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt v1, v0, :cond_0

    .line 1473
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    .line 1476
    iget v6, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    if-ne v1, v6, :cond_1

    .line 1478
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->S:F

    mul-float/2addr v1, v11

    add-float/2addr v1, v10

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlippingContainer;->setScaleX(F)V

    .line 1479
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->S:F

    mul-float/2addr v1, v11

    add-float/2addr v1, v10

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlippingContainer;->setScaleY(F)V

    .line 1487
    :goto_1
    const/4 v1, 0x4

    if-ge v3, v1, :cond_2

    .line 1488
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    .line 1495
    :goto_2
    div-int/lit8 v6, v3, 0x3

    .line 1496
    rem-int/lit8 v7, v3, 0x3

    .line 1497
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getWidth()I

    move-result v8

    int-to-float v8, v8

    const/high16 v9, 0x40400000    # 3.0f

    div-float/2addr v8, v9

    int-to-float v7, v7

    mul-float/2addr v7, v8

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    sub-float/2addr v7, v8

    div-float v8, v4, v12

    add-float/2addr v7, v8

    .line 1499
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getHeight()I

    move-result v8

    int-to-float v8, v8

    const/high16 v9, 0x40400000    # 3.0f

    div-float/2addr v8, v9

    int-to-float v6, v6

    mul-float/2addr v6, v8

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getHeight()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    sub-float/2addr v6, v8

    div-float v8, v5, v12

    add-float/2addr v6, v8

    .line 1500
    sub-float v8, v7, v7

    .line 1501
    sub-float/2addr v1, v6

    .line 1502
    iget v9, p0, Lflipboard/gui/flipping/FlipTransitionViews;->S:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    invoke-virtual {v0, v7}, Lflipboard/gui/flipping/FlippingContainer;->setX(F)V

    .line 1503
    iget v7, p0, Lflipboard/gui/flipping/FlipTransitionViews;->S:F

    mul-float/2addr v1, v7

    add-float/2addr v1, v6

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlippingContainer;->setY(F)V

    .line 1470
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1481
    :cond_1
    invoke-virtual {v0, v10}, Lflipboard/gui/flipping/FlippingContainer;->setScaleX(F)V

    .line 1482
    invoke-virtual {v0, v10}, Lflipboard/gui/flipping/FlippingContainer;->setScaleY(F)V

    goto :goto_1

    .line 1489
    :cond_2
    const/4 v1, 0x4

    if-ne v3, v1, :cond_3

    move v1, v2

    .line 1490
    goto :goto_2

    .line 1492
    :cond_3
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getHeight()I

    move-result v1

    int-to-float v1, v1

    goto :goto_2

    .line 1505
    :cond_4
    const/4 v0, 0x1

    return v0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1509
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->P:Z

    if-nez v0, :cond_0

    .line 1510
    invoke-direct {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->u()V

    .line 1511
    const-wide/16 v0, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/4 v4, 0x3

    const/4 v7, 0x0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-super {p0, v0}, Lflipboard/gui/flipping/FlipTransitionBase;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1513
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/high16 v10, 0x40400000    # 3.0f

    const/high16 v9, 0x40000000    # 2.0f

    const v8, 0x3ea22222

    .line 1517
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->T:F

    float-to-double v2, v1

    const-wide v4, 0x3f847ae147ae147bL    # 0.01

    cmpl-double v1, v2, v4

    if-gez v1, :cond_1

    .line 1518
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->T:F

    float-to-double v2, v1

    const-wide v4, -0x407b851eb851eb85L    # -0.01

    cmpg-double v1, v2, v4

    if-lez v1, :cond_0

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->S:F

    float-to-double v2, v1

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_1

    .line 1520
    :cond_0
    invoke-direct {p0, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->f(I)V

    .line 1524
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->A:Z

    .line 1525
    return-void

    .line 1522
    :cond_1
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "FlipTransitionViews:enabledZoomedOut"

    invoke-static {v1}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    iget-boolean v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->P:Z

    if-nez v1, :cond_2

    invoke-direct {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->u()V

    :cond_2
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x3

    int-to-float v2, v1

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x3

    int-to-float v3, v1

    move v1, v0

    :goto_1
    const/16 v0, 0x9

    if-ge v1, v0, :cond_4

    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x4

    if-ltz v0, :cond_3

    iget-object v4, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-gt v0, v4, :cond_3

    iget-object v4, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    div-int/lit8 v4, v1, 0x3

    rem-int/lit8 v5, v1, 0x3

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v10

    int-to-float v5, v5

    mul-float/2addr v5, v6

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    sub-float/2addr v5, v6

    div-float v6, v2, v9

    add-float/2addr v5, v6

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v10

    int-to-float v4, v4

    mul-float/2addr v4, v6

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    sub-float/2addr v4, v6

    div-float v6, v3, v9

    add-float/2addr v4, v6

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlippingContainer;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/view/ViewPropertyAnimator;->x(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    sget-object v5, Lflipboard/gui/flipping/FlipTransitionViews;->Q:Landroid/animation/TimeInterpolator;

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    const-wide/16 v6, 0x2bc

    invoke-virtual {v4, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    const/4 v5, 0x4

    if-eq v1, v5, :cond_3

    new-instance v5, Lflipboard/gui/flipping/FlipTransitionViews$12;

    invoke-direct {v5, p0, v0}, Lflipboard/gui/flipping/FlipTransitionViews$12;-><init>(Lflipboard/gui/flipping/FlipTransitionViews;Lflipboard/gui/flipping/FlippingContainer;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0, p0}, Lflipboard/activities/FlipboardActivity;->a(Lflipboard/activities/FlipboardActivity$OnBackPressedListener;)V

    goto/16 :goto_0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x1

    return v0
.end method

.method public final s()V
    .locals 4

    .prologue
    .line 650
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->D:Z

    if-nez v0, :cond_0

    .line 651
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->e:J

    sub-long/2addr v0, v2

    .line 652
    const-wide/16 v2, 0x15e

    sub-long v0, v2, v0

    .line 653
    const-wide/16 v2, 0x64

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    .line 654
    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->e(I)V

    .line 656
    :cond_0
    return-void
.end method

.method public declared-synchronized setCurrentViewIndex(I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 934
    monitor-enter p0

    :try_start_0
    const-string v0, "setCurrentViewIndex"

    invoke-static {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->a(Ljava/lang/String;)V

    .line 936
    const/4 v0, 0x0

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v0, v1}, Lflipboard/util/JavaUtil;->a(III)I

    move-result v1

    .line 937
    if-eq v1, p1, :cond_0

    .line 938
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews;->b:Lflipboard/util/Log;

    const-string v3, "Invalid index set in SetCurrentViewIndex (%s). Valid range = 0 - %s. Clamped to %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 940
    :cond_0
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    if-eq v1, v0, :cond_4

    .line 941
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lflipboard/gui/flipping/FlippingContainer;->setVisible(Z)V

    .line 942
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    if-ltz v0, :cond_1

    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    iget-object v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 943
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    iget v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lflipboard/gui/flipping/FlippingContainer;->setVisible(Z)V

    .line 945
    :cond_1
    iput v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    move v0, v2

    .line 949
    :goto_0
    if-ge v0, v1, :cond_2

    .line 950
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v2, v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lflipboard/gui/flipping/SinglePage;->a(F)V

    .line 949
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 953
    :goto_1
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 954
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1, v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v1

    const v2, 0x40490fdb    # (float)Math.PI

    invoke-virtual {v1, v2}, Lflipboard/gui/flipping/SinglePage;->a(F)V

    .line 953
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 956
    :cond_3
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->t()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 958
    :cond_4
    monitor-exit p0

    return-void

    .line 934
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final t()V
    .locals 4

    .prologue
    .line 962
    invoke-static {p0}, Lflipboard/util/AndroidUtil;->d(Landroid/view/View;)Z

    move-result v2

    .line 963
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_0

    .line 964
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;->G:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget v3, p0, Lflipboard/gui/flipping/FlipTransitionViews;->h:I

    sub-int v3, v1, v3

    invoke-static {v0, v2, v3}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;ZI)V

    move v0, v1

    goto :goto_0

    .line 966
    :cond_0
    return-void
.end method
