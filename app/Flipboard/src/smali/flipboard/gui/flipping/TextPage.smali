.class public abstract Lflipboard/gui/flipping/TextPage;
.super Lflipboard/gui/flipping/SinglePage;
.source "TextPage.java"


# static fields
.field protected static final W:Ljava/nio/FloatBuffer;

.field protected static final X:Ljava/nio/FloatBuffer;

.field private static final ab:Ljava/nio/FloatBuffer;

.field private static final ac:Ljava/nio/FloatBuffer;

.field private static final ad:Ljava/nio/FloatBuffer;

.field private static final ae:Ljava/nio/FloatBuffer;


# instance fields
.field N:F

.field O:F

.field P:F

.field Q:F

.field protected R:Z

.field S:J

.field T:F

.field U:F

.field final V:F

.field private Y:[I

.field private Z:Z

.field private aa:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x30

    const/16 v2, 0x20

    .line 50
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 51
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 52
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/TextPage;->W:Ljava/nio/FloatBuffer;

    .line 54
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 55
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 56
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/TextPage;->X:Ljava/nio/FloatBuffer;

    .line 58
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 59
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 60
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/TextPage;->ac:Ljava/nio/FloatBuffer;

    .line 62
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 63
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 64
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/TextPage;->ae:Ljava/nio/FloatBuffer;

    .line 66
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 67
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 68
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/TextPage;->ad:Ljava/nio/FloatBuffer;

    .line 70
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 71
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 72
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/TextPage;->ab:Ljava/nio/FloatBuffer;

    .line 73
    return-void
.end method

.method public constructor <init>(Lflipboard/gui/flipping/FlipTransitionBase;Lflipboard/gui/flipping/OpenGLTransitionRenderer;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 78
    invoke-direct {p0, p1, p2}, Lflipboard/gui/flipping/SinglePage;-><init>(Lflipboard/gui/flipping/FlipTransitionBase;Lflipboard/gui/flipping/OpenGLTransitionRenderer;)V

    .line 29
    iput v0, p0, Lflipboard/gui/flipping/TextPage;->P:F

    .line 30
    iput v0, p0, Lflipboard/gui/flipping/TextPage;->Q:F

    .line 31
    new-array v0, v1, [I

    iput-object v0, p0, Lflipboard/gui/flipping/TextPage;->Y:[I

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/flipping/TextPage;->Z:Z

    .line 33
    iput-boolean v1, p0, Lflipboard/gui/flipping/TextPage;->R:Z

    .line 79
    iput-boolean v1, p0, Lflipboard/gui/flipping/TextPage;->D:Z

    .line 80
    iget-object v0, p2, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->h:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lflipboard/gui/flipping/TextPage;->V:F

    .line 82
    return-void
.end method


# virtual methods
.method final a()Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 299
    sget-object v0, Lflipboard/gui/flipping/TextPage;->W:Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 91
    iput-object p1, p0, Lflipboard/gui/flipping/TextPage;->aa:Ljava/lang/String;

    .line 92
    monitor-enter p0

    .line 93
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lflipboard/gui/flipping/TextPage;->Z:Z

    .line 94
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 14

    .prologue
    .line 100
    invoke-super {p0, p1}, Lflipboard/gui/flipping/SinglePage;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 101
    iget-boolean v0, p0, Lflipboard/gui/flipping/TextPage;->R:Z

    if-eqz v0, :cond_5

    .line 102
    iget v0, p0, Lflipboard/gui/flipping/TextPage;->N:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lflipboard/gui/flipping/TextPage;->m()V

    :cond_0
    iget v0, p0, Lflipboard/gui/flipping/TextPage;->Q:F

    iget v1, p0, Lflipboard/gui/flipping/TextPage;->P:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget v2, p0, Lflipboard/gui/flipping/TextPage;->P:F

    iget v3, p0, Lflipboard/gui/flipping/TextPage;->Q:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_2

    const v2, 0x3f4ccccd    # 0.8f

    iget-wide v4, p0, Lflipboard/gui/flipping/TextPage;->S:J

    sub-long v4, v0, v4

    long-to-float v3, v4

    mul-float/2addr v2, v3

    iget v3, p0, Lflipboard/gui/flipping/TextPage;->P:F

    iget v4, p0, Lflipboard/gui/flipping/TextPage;->Q:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_6

    iget v3, p0, Lflipboard/gui/flipping/TextPage;->P:F

    add-float/2addr v2, v3

    iput v2, p0, Lflipboard/gui/flipping/TextPage;->P:F

    iget v2, p0, Lflipboard/gui/flipping/TextPage;->P:F

    iget v3, p0, Lflipboard/gui/flipping/TextPage;->Q:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    iget v2, p0, Lflipboard/gui/flipping/TextPage;->Q:F

    iput v2, p0, Lflipboard/gui/flipping/TextPage;->P:F

    :cond_1
    :goto_0
    iget-object v2, p0, Lflipboard/gui/flipping/TextPage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v2, v2, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->h:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v2}, Landroid/opengl/GLSurfaceView;->requestRender()V

    :cond_2
    iput-wide v0, p0, Lflipboard/gui/flipping/TextPage;->S:J

    :cond_3
    const/16 v0, 0xb71

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    const/16 v0, 0xbe2

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget v0, p0, Lflipboard/gui/flipping/TextPage;->T:F

    iget v1, p0, Lflipboard/gui/flipping/TextPage;->U:F

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget v0, p0, Lflipboard/gui/flipping/TextPage;->P:F

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {p1, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const/16 v0, 0xde1

    iget-object v1, p0, Lflipboard/gui/flipping/TextPage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v1, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->i:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    const/16 v0, 0xde1

    const/16 v1, 0x2802

    const v2, 0x46240400    # 10497.0f

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    const/16 v0, 0xde1

    const/16 v1, 0x2803

    const v2, 0x46240400    # 10497.0f

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    const v0, 0x8074

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    const v0, 0x8078

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    const/16 v0, 0x901

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glFrontFace(I)V

    const/4 v0, 0x3

    const/16 v1, 0x1406

    const/4 v2, 0x0

    sget-object v3, Lflipboard/gui/flipping/TextPage;->ac:Ljava/nio/FloatBuffer;

    invoke-interface {p1, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glVertexPointer(IIILjava/nio/Buffer;)V

    const/4 v0, 0x2

    const/16 v1, 0x1406

    const/4 v2, 0x0

    sget-object v3, Lflipboard/gui/flipping/TextPage;->ae:Ljava/nio/FloatBuffer;

    invoke-interface {p1, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lflipboard/gui/flipping/TextPage;->Z:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    iget-object v1, p0, Lflipboard/gui/flipping/TextPage;->Y:[I

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDeleteTextures(I[II)V

    new-instance v12, Lflipboard/gui/FLTextView;

    iget-object v0, p0, Lflipboard/gui/flipping/TextPage;->e:Lflipboard/gui/flipping/FlipTransitionBase;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionBase;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v12, v0}, Lflipboard/gui/FLTextView;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x1

    invoke-virtual {v12, v0}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->w:Landroid/graphics/Typeface;

    invoke-virtual {v12, v0}, Lflipboard/gui/FLTextView;->setTypeface(Landroid/graphics/Typeface;)V

    const/4 v0, 0x1

    const/16 v1, 0x12

    invoke-virtual {v12, v0, v1}, Lflipboard/gui/FLTextView;->a(II)V

    iget-object v0, p0, Lflipboard/gui/flipping/TextPage;->aa:Ljava/lang/String;

    invoke-virtual {v12, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lflipboard/gui/flipping/TextPage;->t:I

    int-to-float v0, v0

    iget v1, p0, Lflipboard/gui/flipping/TextPage;->N:F

    const/high16 v2, 0x40400000    # 3.0f

    iget v3, p0, Lflipboard/gui/flipping/TextPage;->V:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v0, v0

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v12, v0, v1}, Lflipboard/gui/FLTextView;->measure(II)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v12}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v12}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v12, v0, v1, v2, v3}, Lflipboard/gui/FLTextView;->layout(IIII)V

    invoke-virtual {v12}, Lflipboard/gui/FLTextView;->getWidth()I

    move-result v0

    if-lez v0, :cond_4

    invoke-virtual {v12}, Lflipboard/gui/FLTextView;->getHeight()I

    move-result v0

    if-lez v0, :cond_4

    sget-object v0, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    invoke-virtual {v0, v12}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Landroid/view/View;)Lflipboard/gui/flipping/FlippingBitmap;

    move-result-object v13

    const/16 v0, 0xb44

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    const/4 v0, 0x1

    iget-object v1, p0, Lflipboard/gui/flipping/TextPage;->Y:[I

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    const/16 v0, 0xde1

    iget-object v1, p0, Lflipboard/gui/flipping/TextPage;->Y:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    const/16 v0, 0xde1

    const/16 v1, 0x2801

    const v2, 0x46180400    # 9729.0f

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v0, 0x3f800000    # 1.0f

    iget-boolean v2, p0, Lflipboard/gui/flipping/TextPage;->F:Z

    if-eqz v2, :cond_7

    iget-object v0, v13, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-static {v0}, Lflipboard/util/JavaUtil;->b(I)I

    move-result v4

    iget-object v0, v13, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-static {v0}, Lflipboard/util/JavaUtil;->b(I)I

    move-result v5

    invoke-virtual {v12}, Lflipboard/gui/FLTextView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, v4

    div-float v11, v0, v1

    invoke-virtual {v12}, Lflipboard/gui/FLTextView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, v5

    div-float v10, v0, v1

    const/16 v1, 0xde1

    const/4 v2, 0x0

    const/16 v3, 0x1908

    const/4 v6, 0x0

    const/16 v7, 0x1908

    const/16 v8, 0x1401

    const/4 v9, 0x0

    move-object v0, p1

    invoke-interface/range {v0 .. v9}, Ljavax/microedition/khronos/opengles/GL10;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    const/16 v0, 0xde1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, v13, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/GLUtils;->texSubImage2D(IIIILandroid/graphics/Bitmap;)V

    move v0, v10

    move v1, v11

    :goto_1
    const/16 v2, 0xde1

    iget-object v3, p0, Lflipboard/gui/flipping/TextPage;->Y:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-interface {p1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    invoke-virtual {v12}, Lflipboard/gui/FLTextView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v12}, Lflipboard/gui/FLTextView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v3, v4

    float-to-int v4, v4

    const/16 v5, 0xc

    new-array v5, v5, [F

    const/4 v6, 0x0

    aput v2, v5, v6

    const/4 v6, 0x1

    int-to-float v7, v4

    aput v7, v5, v6

    const/4 v6, 0x2

    const/4 v7, 0x0

    aput v7, v5, v6

    const/4 v6, 0x3

    const/4 v7, 0x0

    aput v7, v5, v6

    const/4 v6, 0x4

    int-to-float v7, v4

    aput v7, v5, v6

    const/4 v6, 0x5

    const/4 v7, 0x0

    aput v7, v5, v6

    const/4 v6, 0x6

    aput v2, v5, v6

    const/4 v2, 0x7

    int-to-float v6, v4

    sub-float/2addr v6, v3

    aput v6, v5, v2

    const/16 v2, 0x8

    const/4 v6, 0x0

    aput v6, v5, v2

    const/16 v2, 0x9

    const/4 v6, 0x0

    aput v6, v5, v2

    const/16 v2, 0xa

    int-to-float v4, v4

    sub-float v3, v4, v3

    aput v3, v5, v2

    const/16 v2, 0xb

    const/4 v3, 0x0

    aput v3, v5, v2

    sget-object v2, Lflipboard/gui/flipping/TextPage;->ab:Ljava/nio/FloatBuffer;

    invoke-virtual {v2, v5}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    sget-object v2, Lflipboard/gui/flipping/TextPage;->ab:Ljava/nio/FloatBuffer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    const/16 v2, 0x8

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v1, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput v4, v2, v3

    const/4 v3, 0x2

    const/4 v4, 0x0

    aput v4, v2, v3

    const/4 v3, 0x3

    const/4 v4, 0x0

    aput v4, v2, v3

    const/4 v3, 0x4

    aput v1, v2, v3

    const/4 v1, 0x5

    aput v0, v2, v1

    const/4 v1, 0x6

    const/4 v3, 0x0

    aput v3, v2, v1

    const/4 v1, 0x7

    aput v0, v2, v1

    sget-object v0, Lflipboard/gui/flipping/TextPage;->ad:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    sget-object v0, Lflipboard/gui/flipping/TextPage;->ad:Ljava/nio/FloatBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    sget-object v0, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    invoke-virtual {v0, v13}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Lflipboard/gui/flipping/FlippingBitmap;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/flipping/TextPage;->Z:Z

    :cond_4
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget v0, p0, Lflipboard/gui/flipping/TextPage;->N:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget v1, p0, Lflipboard/gui/flipping/TextPage;->V:F

    add-float/2addr v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/16 v0, 0xde1

    iget-object v1, p0, Lflipboard/gui/flipping/TextPage;->Y:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    const/4 v0, 0x3

    const/16 v1, 0x1406

    const/4 v2, 0x0

    sget-object v3, Lflipboard/gui/flipping/TextPage;->ab:Ljava/nio/FloatBuffer;

    invoke-interface {p1, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glVertexPointer(IIILjava/nio/Buffer;)V

    const/4 v0, 0x2

    const/16 v1, 0x1406

    const/4 v2, 0x0

    sget-object v3, Lflipboard/gui/flipping/TextPage;->ad:Ljava/nio/FloatBuffer;

    invoke-interface {p1, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    const v0, 0x8074

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    const v0, 0x8078

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    const/16 v0, 0xbe2

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    const/16 v0, 0xb71

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 104
    :cond_5
    return-void

    .line 102
    :cond_6
    iget v3, p0, Lflipboard/gui/flipping/TextPage;->P:F

    sub-float v2, v3, v2

    iput v2, p0, Lflipboard/gui/flipping/TextPage;->P:F

    iget v2, p0, Lflipboard/gui/flipping/TextPage;->P:F

    iget v3, p0, Lflipboard/gui/flipping/TextPage;->Q:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    iget v2, p0, Lflipboard/gui/flipping/TextPage;->Q:F

    iput v2, p0, Lflipboard/gui/flipping/TextPage;->P:F

    goto/16 :goto_0

    :cond_7
    const/16 v2, 0xde1

    const/4 v3, 0x0

    :try_start_1
    iget-object v4, v13, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 86
    iput-boolean p1, p0, Lflipboard/gui/flipping/TextPage;->R:Z

    .line 87
    return-void
.end method

.method final b()Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 305
    sget-object v0, Lflipboard/gui/flipping/TextPage;->X:Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method final c(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 285
    invoke-super {p0, p1}, Lflipboard/gui/flipping/SinglePage;->c(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 286
    const/4 v0, 0x1

    iget-object v1, p0, Lflipboard/gui/flipping/TextPage;->Y:[I

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDeleteTextures(I[II)V

    .line 287
    iget-object v0, p0, Lflipboard/gui/flipping/TextPage;->Y:[I

    aput v2, v0, v2

    .line 288
    return-void
.end method

.method final g()V
    .locals 1

    .prologue
    .line 292
    invoke-super {p0}, Lflipboard/gui/flipping/SinglePage;->g()V

    .line 293
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/flipping/TextPage;->Z:Z

    .line 294
    return-void
.end method

.method protected m()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v3, -0x40000000    # -2.0f

    const/4 v4, 0x0

    .line 108
    iget-object v0, p0, Lflipboard/gui/flipping/TextPage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->j:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lflipboard/gui/flipping/TextPage;->N:F

    .line 109
    iget-object v0, p0, Lflipboard/gui/flipping/TextPage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->j:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lflipboard/gui/flipping/TextPage;->O:F

    .line 110
    const/16 v0, 0xc

    new-array v0, v0, [F

    iget v1, p0, Lflipboard/gui/flipping/TextPage;->N:F

    div-float/2addr v1, v5

    aput v1, v0, v6

    iget v1, p0, Lflipboard/gui/flipping/TextPage;->O:F

    div-float/2addr v1, v5

    aput v1, v0, v7

    const/4 v1, 0x2

    aput v4, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lflipboard/gui/flipping/TextPage;->N:F

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lflipboard/gui/flipping/TextPage;->O:F

    div-float/2addr v2, v5

    aput v2, v0, v1

    const/4 v1, 0x5

    aput v4, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lflipboard/gui/flipping/TextPage;->N:F

    div-float/2addr v2, v5

    aput v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lflipboard/gui/flipping/TextPage;->O:F

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/16 v1, 0x8

    aput v4, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lflipboard/gui/flipping/TextPage;->N:F

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/16 v1, 0xa

    iget v2, p0, Lflipboard/gui/flipping/TextPage;->O:F

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/16 v1, 0xb

    aput v4, v0, v1

    .line 117
    sget-object v1, Lflipboard/gui/flipping/TextPage;->ac:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 118
    sget-object v0, Lflipboard/gui/flipping/TextPage;->ac:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v6}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 120
    iget-object v0, p0, Lflipboard/gui/flipping/TextPage;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->l:Landroid/graphics/PointF;

    .line 122
    const/16 v1, 0x8

    new-array v1, v1, [F

    iget v2, v0, Landroid/graphics/PointF;->x:F

    aput v2, v1, v6

    aput v4, v1, v7

    const/4 v2, 0x2

    aput v4, v1, v2

    const/4 v2, 0x3

    aput v4, v1, v2

    const/4 v2, 0x4

    iget v3, v0, Landroid/graphics/PointF;->x:F

    aput v3, v1, v2

    const/4 v2, 0x5

    iget v3, v0, Landroid/graphics/PointF;->y:F

    aput v3, v1, v2

    const/4 v2, 0x6

    aput v4, v1, v2

    const/4 v2, 0x7

    iget v0, v0, Landroid/graphics/PointF;->y:F

    aput v0, v1, v2

    .line 131
    sget-object v0, Lflipboard/gui/flipping/TextPage;->ae:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 132
    sget-object v0, Lflipboard/gui/flipping/TextPage;->ae:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v6}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 133
    return-void
.end method
