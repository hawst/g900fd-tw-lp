.class public Lflipboard/gui/flipping/TileBounceInterpolator;
.super Ljava/lang/Object;
.source "TileBounceInterpolator.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field private final a:F

.field private final b:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/gui/flipping/TileBounceInterpolator;-><init>(B)V

    .line 21
    return-void
.end method

.method private constructor <init>(B)V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const v0, 0x3f333333    # 0.7f

    iput v0, p0, Lflipboard/gui/flipping/TileBounceInterpolator;->a:F

    .line 26
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lflipboard/gui/flipping/TileBounceInterpolator;->a:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lflipboard/gui/flipping/TileBounceInterpolator;->b:F

    .line 27
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 32
    float-to-double v0, p1

    const-wide v2, 0x3fe6666666666666L    # 0.7

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 33
    iget v0, p0, Lflipboard/gui/flipping/TileBounceInterpolator;->a:F

    div-float v0, p1, v0

    .line 34
    mul-float/2addr v0, v0

    .line 38
    :goto_0
    return v0

    .line 36
    :cond_0
    iget v0, p0, Lflipboard/gui/flipping/TileBounceInterpolator;->b:F

    sub-float v0, v6, v0

    float-to-double v0, v0

    sub-float v2, p1, v6

    iget v3, p0, Lflipboard/gui/flipping/TileBounceInterpolator;->b:F

    add-float/2addr v2, v3

    float-to-double v2, v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    iget v4, p0, Lflipboard/gui/flipping/TileBounceInterpolator;->b:F

    div-float v4, v6, v4

    float-to-double v4, v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-float v0, v0

    goto :goto_0
.end method
