.class public Lflipboard/gui/flipping/ViewScreenshotCreator;
.super Ljava/lang/Object;
.source "ViewScreenshotCreator.java"


# static fields
.field public static b:Lflipboard/gui/flipping/ViewScreenshotCreator;

.field private static e:Lflipboard/util/Log;

.field private static f:Lflipboard/util/Log;


# instance fields
.field final a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:I

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "Screenshot creator"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/ViewScreenshotCreator;->e:Lflipboard/util/Log;

    .line 31
    const-string v0, "opengl"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/ViewScreenshotCreator;->f:Lflipboard/util/Log;

    return-void
.end method

.method private constructor <init>(II)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput p1, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->c:I

    .line 44
    iput p2, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->d:I

    .line 47
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->a:Ljava/util/LinkedList;

    .line 48
    invoke-direct {p0}, Lflipboard/gui/flipping/ViewScreenshotCreator;->b()V

    .line 50
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->g:I

    .line 51
    return-void
.end method

.method public static a(II)V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lflipboard/gui/flipping/ViewScreenshotCreator;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/flipping/ViewScreenshotCreator;-><init>(II)V

    sput-object v0, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    .line 38
    return-void
.end method

.method public static a(Landroid/graphics/Bitmap;Ljava/io/File;)V
    .locals 3

    .prologue
    .line 208
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 210
    :try_start_1
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x50

    invoke-virtual {p0, v0, v2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 217
    :goto_0
    return-void

    .line 212
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 216
    :catch_0
    move-exception v0

    .line 215
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private b()V
    .locals 7

    .prologue
    .line 76
    iget v0, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->c:I

    if-lez v0, :cond_1

    iget v0, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->d:I

    if-lez v0, :cond_1

    .line 77
    iget-object v1, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->a:Ljava/util/LinkedList;

    monitor-enter v1

    .line 78
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    .line 79
    :try_start_0
    iget-object v2, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->a:Ljava/util/LinkedList;

    sget-object v3, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    iget v4, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->c:I

    iget v5, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->d:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v3, v4, v5, v6}, Lflipboard/io/BitmapManager;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    :cond_1
    return-void

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()Lflipboard/gui/flipping/FlippingBitmap;
    .locals 5

    .prologue
    .line 194
    iget-object v1, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->a:Ljava/util/LinkedList;

    monitor-enter v1

    .line 195
    :try_start_0
    iget-object v0, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 196
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    .line 198
    :goto_0
    if-nez v0, :cond_0

    .line 199
    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    iget v2, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->c:I

    iget v3, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->d:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v2, v3, v4}, Lflipboard/io/BitmapManager;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 201
    :cond_0
    new-instance v2, Lflipboard/gui/flipping/FlippingBitmap;

    invoke-direct {v2, v0, v1}, Lflipboard/gui/flipping/FlippingBitmap;-><init>(Landroid/graphics/Bitmap;Z)V

    return-object v2

    .line 196
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 197
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)Lflipboard/gui/flipping/FlippingBitmap;
    .locals 10

    .prologue
    const/high16 v3, 0x42200000    # 40.0f

    const/high16 v1, 0x40000000    # 2.0f

    const/16 v9, 0x8

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 96
    .line 97
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    if-nez v0, :cond_1

    .line 98
    :cond_0
    sget-object v0, Lflipboard/gui/flipping/ViewScreenshotCreator;->e:Lflipboard/util/Log;

    const-string v4, "Creating a screenshot from a view width no height or width. Shouldn\'t happen. Doing a layout myself to fix it"

    new-array v6, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v4, v6}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    iget v0, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->c:I

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget v4, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->d:I

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {p1, v0, v4}, Landroid/view/View;->measure(II)V

    .line 100
    iget v0, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->c:I

    iget v4, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->d:I

    invoke-virtual {p1, v5, v5, v0, v4}, Landroid/view/View;->layout(IIII)V

    .line 102
    :cond_1
    const/4 v0, 0x0

    .line 103
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 105
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    iget v7, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->d:I

    if-ne v4, v7, :cond_4

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    iget v7, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->c:I

    if-ne v4, v7, :cond_4

    .line 106
    iget-object v4, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->a:Ljava/util/LinkedList;

    monitor-enter v4

    .line 107
    :try_start_0
    iget-object v0, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 108
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    sget-object v4, Lflipboard/gui/flipping/ViewScreenshotCreator;->e:Lflipboard/util/Log;

    new-array v4, v2, [Ljava/lang/Object;

    iget-object v7, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->a:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    .line 110
    if-nez v0, :cond_2

    .line 111
    sget-object v0, Lflipboard/gui/flipping/ViewScreenshotCreator;->e:Lflipboard/util/Log;

    .line 112
    const/4 v0, 0x0

    .line 187
    :goto_0
    return-object v0

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 114
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-nez v4, :cond_3

    .line 115
    iget v4, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->g:I

    invoke-virtual {v0, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 119
    :cond_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 123
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v4, "Bitmap from bitmap pool is recycled when trying to use it, pool size: %d\nTry to recover by allocating a new bitmap"

    new-array v7, v2, [Ljava/lang/Object;

    iget-object v8, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->a:Ljava/util/LinkedList;

    invoke-virtual {v8}, Ljava/util/LinkedList;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-static {v4, v7}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-array v7, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v4, v7}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 124
    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    iget v4, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->c:I

    iget v7, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->d:I

    invoke-virtual {v0, v4, v7, v6}, Lflipboard/io/BitmapManager;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 131
    :cond_4
    if-nez v0, :cond_c

    .line 136
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    .line 137
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 138
    instance-of v0, p1, Lflipboard/gui/toc/TileContainer;

    if-eqz v0, :cond_10

    .line 139
    sget-object v0, Lflipboard/gui/toc/TOCView;->a:Lflipboard/util/Log;

    move-object v0, p1

    .line 142
    check-cast v0, Lflipboard/gui/toc/TileContainer;

    invoke-virtual {v0}, Lflipboard/gui/toc/TileContainer;->getDeleteImage()Landroid/view/View;

    move-result-object v7

    move-object v0, p1

    .line 143
    check-cast v0, Lflipboard/gui/toc/TileContainer;

    invoke-virtual {v0}, Lflipboard/gui/toc/TileContainer;->getScale()F

    .line 144
    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int/2addr v4, v0

    .line 145
    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, v2

    move v2, v4

    .line 147
    :goto_1
    sget-object v4, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    invoke-virtual {v4, v2, v0, v6}, Lflipboard/io/BitmapManager;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v6, v0

    move v7, v5

    .line 158
    :goto_2
    if-eqz v6, :cond_b

    .line 159
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 162
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v4

    or-int/2addr v2, v4

    if-eqz v2, :cond_5

    .line 163
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 166
    :cond_5
    instance-of v2, p1, Lflipboard/gui/flipping/FlippingContainer;

    if-eqz v2, :cond_e

    .line 167
    check-cast p1, Lflipboard/gui/flipping/FlippingContainer;

    .line 168
    iget-object v2, p1, Lflipboard/gui/flipping/FlippingContainer;->b:Landroid/view/View;

    .line 169
    instance-of v4, v2, Lflipboard/gui/section/SectionPage;

    if-eqz v4, :cond_d

    .line 170
    check-cast v2, Lflipboard/gui/section/SectionPage;

    .line 171
    invoke-virtual {v2, v0}, Lflipboard/gui/section/SectionPage;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v2}, Lflipboard/gui/section/SectionPage;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Lflipboard/gui/flipping/FlippingContainer;

    iget-object v5, v2, Lflipboard/gui/section/SectionPage;->t:Lflipboard/gui/section/SectionScrubber;

    if-eqz v5, :cond_a

    iget-object v5, v2, Lflipboard/gui/section/SectionPage;->u:Landroid/view/ViewGroup;

    if-nez v5, :cond_a

    iget-object v2, v2, Lflipboard/gui/section/SectionPage;->t:Lflipboard/gui/section/SectionScrubber;

    iget v4, v4, Lflipboard/gui/flipping/FlippingContainer;->c:I

    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v5, "SectionScrubber:drawForPage"

    invoke-static {v5}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    iget-object v5, v2, Lflipboard/gui/section/SectionScrubber;->e:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    iget-object v5, v2, Lflipboard/gui/section/SectionScrubber;->e:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v5

    int-to-float v5, v5

    iget-object v8, v2, Lflipboard/gui/section/SectionScrubber;->e:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v0, v5, v8}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v5, v2, Lflipboard/gui/section/SectionScrubber;->e:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    :cond_6
    iget-object v5, v2, Lflipboard/gui/section/SectionScrubber;->a:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v5

    if-eq v5, v9, :cond_7

    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    iget-object v5, v2, Lflipboard/gui/section/SectionScrubber;->a:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getLeft()I

    move-result v5

    int-to-float v5, v5

    iget-object v8, v2, Lflipboard/gui/section/SectionScrubber;->a:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v8}, Lflipboard/gui/FLLabelTextView;->getTop()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v0, v5, v8}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v5, v2, Lflipboard/gui/section/SectionScrubber;->a:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5, v0}, Lflipboard/gui/FLLabelTextView;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    :cond_7
    iget-object v5, v2, Lflipboard/gui/section/SectionScrubber;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v5

    if-eq v5, v9, :cond_8

    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    iget-object v5, v2, Lflipboard/gui/section/SectionScrubber;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getLeft()I

    move-result v5

    int-to-float v5, v5

    iget-object v8, v2, Lflipboard/gui/section/SectionScrubber;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v8}, Lflipboard/gui/FLLabelTextView;->getTop()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v0, v5, v8}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v5, v2, Lflipboard/gui/section/SectionScrubber;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5, v0}, Lflipboard/gui/FLLabelTextView;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    :cond_8
    iget-object v5, v2, Lflipboard/gui/section/SectionScrubber;->d:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-eq v5, v9, :cond_9

    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    iget-object v5, v2, Lflipboard/gui/section/SectionScrubber;->d:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v5

    int-to-float v5, v5

    iget-object v8, v2, Lflipboard/gui/section/SectionScrubber;->d:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v0, v5, v8}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v5, v2, Lflipboard/gui/section/SectionScrubber;->d:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    :cond_9
    iget v5, v2, Lflipboard/gui/section/SectionScrubber;->g:I

    invoke-virtual {v2, v4}, Lflipboard/gui/section/SectionScrubber;->setPosition$2563266(I)V

    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    iget-object v4, v2, Lflipboard/gui/section/SectionScrubber;->c:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getLeft()I

    move-result v4

    int-to-float v4, v4

    iget-object v8, v2, Lflipboard/gui/section/SectionScrubber;->c:Landroid/widget/SeekBar;

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getTop()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v0, v4, v8}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v4, v2, Lflipboard/gui/section/SectionScrubber;->c:Landroid/widget/SeekBar;

    invoke-virtual {v4, v0}, Landroid/widget/SeekBar;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v2, v5}, Lflipboard/gui/section/SectionScrubber;->setPosition$2563266(I)V

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    .line 181
    :cond_a
    :goto_3
    sget-object v2, Lflipboard/gui/flipping/ViewScreenshotCreator;->f:Lflipboard/util/Log;

    iget-boolean v2, v2, Lflipboard/util/Log;->f:Z

    if-eqz v2, :cond_b

    .line 182
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 183
    const/high16 v2, 0x25ff0000

    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setColor(I)V

    move v2, v1

    move v4, v3

    .line 184
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 187
    :cond_b
    new-instance v0, Lflipboard/gui/flipping/FlippingBitmap;

    invoke-direct {v0, v6, v7}, Lflipboard/gui/flipping/FlippingBitmap;-><init>(Landroid/graphics/Bitmap;Z)V

    goto/16 :goto_0

    :cond_c
    move-object v6, v0

    move v7, v2

    .line 156
    goto/16 :goto_2

    .line 173
    :cond_d
    invoke-virtual {v2, v0}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    goto :goto_3

    .line 175
    :cond_e
    instance-of v2, p1, Lflipboard/gui/toc/TileContainer;

    if-eqz v2, :cond_f

    .line 177
    check-cast p1, Lflipboard/gui/toc/TileContainer;

    invoke-virtual {p1, v0}, Lflipboard/gui/toc/TileContainer;->a(Landroid/graphics/Canvas;)V

    goto :goto_3

    .line 179
    :cond_f
    invoke-virtual {p1, v0}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    goto :goto_3

    :cond_10
    move v0, v2

    move v2, v4

    goto/16 :goto_1
.end method

.method public final a(Lflipboard/gui/flipping/FlippingBitmap;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 221
    iget-object v0, p1, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Don\'t add recycled bitmaps"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :cond_0
    iget-boolean v0, p1, Lflipboard/gui/flipping/FlippingBitmap;->b:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_2

    .line 225
    iget-boolean v0, p1, Lflipboard/gui/flipping/FlippingBitmap;->b:Z

    if-nez v0, :cond_1

    .line 226
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Bitmap wants to be reused, but there are enough bitmaps in the pool already. This is not supposed to happen, deallocating the bitmap."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 228
    :cond_1
    iget-object v1, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->a:Ljava/util/LinkedList;

    monitor-enter v1

    .line 229
    :try_start_0
    iget-object v0, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->a:Ljava/util/LinkedList;

    iget-object v2, p1, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 230
    sget-object v0, Lflipboard/gui/flipping/ViewScreenshotCreator;->e:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->a:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    .line 231
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 233
    :cond_2
    return-void

    .line 231
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(II)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 58
    iget v2, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->c:I

    if-ne v2, p1, :cond_0

    iget v2, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->d:I

    if-eq v2, p2, :cond_2

    .line 59
    :cond_0
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "updateSize %dx%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    iput p1, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->c:I

    .line 61
    iput p2, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->d:I

    .line 62
    iget-object v2, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->a:Ljava/util/LinkedList;

    monitor-enter v2

    .line 63
    :goto_0
    :try_start_0
    iget-object v0, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 64
    iget-object v0, p0, Lflipboard/gui/flipping/ViewScreenshotCreator;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 66
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lflipboard/gui/flipping/ViewScreenshotCreator;->b()V

    .line 67
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 70
    :cond_2
    return v0
.end method
