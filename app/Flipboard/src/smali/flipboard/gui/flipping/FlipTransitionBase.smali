.class public abstract Lflipboard/gui/flipping/FlipTransitionBase;
.super Lflipboard/gui/ContainerView;
.source "FlipTransitionBase.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# static fields
.field public static final b:Lflipboard/util/Log;


# instance fields
.field protected A:Z

.field protected B:Ljava/util/concurrent/atomic/AtomicInteger;

.field protected C:I

.field protected D:Z

.field public E:Lflipboard/util/Log;

.field protected F:J

.field private G:F

.field private final a:I

.field public c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

.field protected d:Landroid/opengl/GLSurfaceView;

.field protected e:J

.field public f:I

.field public g:I

.field public h:I

.field protected i:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

.field protected j:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

.field protected k:J

.field protected l:Z

.field protected m:F

.field protected n:F

.field public o:F

.field public p:F

.field protected q:F

.field protected r:F

.field protected s:F

.field public final t:I

.field protected u:Z

.field protected v:Z

.field public w:Z

.field public x:Z

.field public y:I

.field public z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-string v0, "flipper"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/FlipTransitionBase;->b:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/16 v1, 0x8

    const/4 v7, 0x1

    .line 104
    invoke-direct {p0, p1}, Lflipboard/gui/ContainerView;-><init>(Landroid/content/Context;)V

    .line 63
    iput-boolean v7, p0, Lflipboard/gui/flipping/FlipTransitionBase;->v:Z

    .line 67
    iput-boolean v7, p0, Lflipboard/gui/flipping/FlipTransitionBase;->x:Z

    .line 75
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 79
    const-string v0, "flashing"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->E:Lflipboard/util/Log;

    .line 105
    invoke-static {}, Lflipboard/gui/flipping/FlipUtil;->c()V

    .line 107
    new-instance v0, Lflipboard/gui/flipping/FlipTransitionBase$1;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/flipping/FlipTransitionBase$1;-><init>(Lflipboard/gui/flipping/FlipTransitionBase;Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->d:Landroid/opengl/GLSurfaceView;

    .line 115
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->d:Landroid/opengl/GLSurfaceView;

    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/FlipTransitionBase;->addView(Landroid/view/View;)V

    .line 116
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->d:Landroid/opengl/GLSurfaceView;

    const/16 v5, 0x10

    move v2, v1

    move v3, v1

    move v4, v1

    invoke-virtual/range {v0 .. v6}, Landroid/opengl/GLSurfaceView;->setEGLConfigChooser(IIIIII)V

    .line 117
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->d:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, -0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 118
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->d:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0, v7}, Landroid/opengl/GLSurfaceView;->setZOrderOnTop(Z)V

    .line 120
    new-instance v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-direct {v0, p0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;-><init>(Lflipboard/gui/flipping/FlipTransitionBase;)V

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    .line 121
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->d:Landroid/opengl/GLSurfaceView;

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0, v1}, Landroid/opengl/GLSurfaceView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 122
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->getDesiredNumberOfTextures()I

    move-result v1

    iput v1, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->u:I

    .line 123
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->d:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0, v6}, Landroid/opengl/GLSurfaceView;->setRenderMode(I)V

    .line 125
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->t:I

    .line 128
    iput v6, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    .line 129
    iput v7, p0, Lflipboard/gui/flipping/FlipTransitionBase;->z:I

    .line 130
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 131
    const/16 v0, 0x226

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->a:I

    .line 136
    :goto_0
    check-cast p1, Lflipboard/activities/FlipboardActivity;

    .line 137
    iget-object v0, p1, Lflipboard/activities/FlipboardActivity;->X:Lflipboard/gui/flipping/FlipTransitionBase;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lflipboard/activities/FlipboardActivity;->X:Lflipboard/gui/flipping/FlipTransitionBase;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionBase;->e()V

    :cond_0
    iput-object p0, p1, Lflipboard/activities/FlipboardActivity;->X:Lflipboard/gui/flipping/FlipTransitionBase;

    .line 138
    return-void

    .line 133
    :cond_1
    const/16 v0, 0x15e

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->a:I

    goto :goto_0
.end method

.method private static a(ILflipboard/gui/flipping/SinglePage;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 885
    invoke-virtual {p1}, Lflipboard/gui/flipping/SinglePage;->j()Lflipboard/gui/flipping/SinglePage;

    move-result-object v0

    .line 886
    invoke-virtual {p1}, Lflipboard/gui/flipping/SinglePage;->i()Lflipboard/gui/flipping/SinglePage;

    move-result-object v1

    .line 887
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/16 v2, 0xc

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    iget v4, p1, Lflipboard/gui/flipping/SinglePage;->E:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p1}, Lflipboard/gui/flipping/SinglePage;->c()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-boolean v4, p1, Lflipboard/gui/flipping/SinglePage;->z:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-boolean v4, p1, Lflipboard/gui/flipping/SinglePage;->u:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-boolean v4, p1, Lflipboard/gui/flipping/SinglePage;->D:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    iget-boolean v4, p1, Lflipboard/gui/flipping/SinglePage;->B:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    iget-object v4, p1, Lflipboard/gui/flipping/SinglePage;->d:[I

    aget v4, v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    if-eqz v0, :cond_0

    iget v0, v0, Lflipboard/gui/flipping/SinglePage;->E:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    aput-object v0, v2, v3

    const/16 v3, 0x9

    if-eqz v1, :cond_1

    iget v0, v1, Lflipboard/gui/flipping/SinglePage;->E:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_1
    aput-object v0, v2, v3

    const/16 v0, 0xa

    invoke-virtual {p1}, Lflipboard/gui/flipping/SinglePage;->k()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v2, v0

    const/16 v0, 0xb

    invoke-virtual {p1}, Lflipboard/gui/flipping/SinglePage;->l()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v2, v0

    .line 888
    return-void

    .line 887
    :cond_0
    const-string v0, "none"

    goto :goto_0

    :cond_1
    const-string v0, "none"

    goto :goto_1
.end method

.method private a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;ZZ)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 702
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const-string v1, "startAutoFlip"

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V

    .line 703
    invoke-virtual {p0, p1, v2, v2, v6}, Lflipboard/gui/flipping/FlipTransitionBase;->a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;FFZ)Lflipboard/gui/flipping/SinglePage;

    move-result-object v0

    .line 704
    if-nez v0, :cond_0

    .line 705
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase;->b:Lflipboard/util/Log;

    const-string v1, "Cancelling auto flip, startFlip returned null. Did we auto-flip when a flip was being touched?"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 723
    :goto_0
    return-void

    .line 708
    :cond_0
    iput-boolean v3, v0, Lflipboard/gui/flipping/SinglePage;->u:Z

    .line 709
    sget-object v1, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne p1, v1, :cond_1

    .line 710
    const v1, -0x443b645a    # -0.006f

    iput v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->s:F

    .line 711
    const v1, 0x40490e38

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/SinglePage;->a(F)V

    .line 717
    :goto_1
    iput-boolean v6, p0, Lflipboard/gui/flipping/FlipTransitionBase;->l:Z

    .line 718
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lflipboard/gui/flipping/FlipTransitionBase;->k:J

    .line 719
    iget v3, p0, Lflipboard/gui/flipping/FlipTransitionBase;->s:F

    iget v4, p0, Lflipboard/gui/flipping/FlipTransitionBase;->a:I

    move-object v1, p1

    move v2, p3

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/flipping/SinglePage;->a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;ZFIZ)V

    .line 720
    iput-boolean v6, v0, Lflipboard/gui/flipping/SinglePage;->H:Z

    .line 721
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 722
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->d:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    goto :goto_0

    .line 713
    :cond_1
    const v1, 0x3bc49ba6    # 0.006f

    iput v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->s:F

    .line 714
    invoke-virtual {v0, v2}, Lflipboard/gui/flipping/SinglePage;->a(F)V

    goto :goto_1
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 171
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->F:J

    .line 173
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->A:Z

    if-eqz v0, :cond_2

    .line 174
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eq v0, v3, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 175
    :cond_0
    iput-boolean v2, p0, Lflipboard/gui/flipping/FlipTransitionBase;->A:Z

    :cond_1
    move v0, v2

    .line 246
    :goto_0
    return v0

    .line 180
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 182
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget v4, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    invoke-virtual {v1, v4}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v1

    .line 183
    if-eqz v1, :cond_3

    sget-object v1, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    if-nez v1, :cond_4

    :cond_3
    move v0, v2

    .line 185
    goto :goto_0

    .line 188
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-le v1, v3, :cond_5

    move v0, v2

    .line 189
    goto :goto_0

    .line 192
    :cond_5
    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    move v0, v2

    .line 246
    goto :goto_0

    .line 194
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->m:F

    .line 195
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->n:F

    .line 244
    :cond_6
    invoke-direct {p0, p1}, Lflipboard/gui/flipping/FlipTransitionBase;->c(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->G:F

    goto :goto_1

    .line 200
    :pswitch_2
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->b()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_7

    move v0, v2

    .line 201
    goto :goto_0

    .line 206
    :cond_7
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->w:Z

    if-eqz v0, :cond_8

    .line 207
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->m:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    sub-float v1, v0, v1

    .line 208
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->n:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float/2addr v0, v4

    .line 214
    :goto_2
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 215
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->getCancelDistance()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v4, v0, v4

    if-lez v4, :cond_9

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v0, v0, v4

    if-lez v0, :cond_9

    .line 216
    iput-boolean v3, p0, Lflipboard/gui/flipping/FlipTransitionBase;->A:Z

    move v0, v2

    .line 217
    goto :goto_0

    .line 210
    :cond_8
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->n:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float v1, v0, v1

    .line 211
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->m:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v0, v4

    goto :goto_2

    .line 221
    :cond_9
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->t:I

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-lez v0, :cond_c

    .line 222
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->getNextView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_a

    .line 224
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->i:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    .line 227
    :cond_a
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->n:F

    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionBase;->m:F

    invoke-virtual {p0, v0, v1, v2}, Lflipboard/gui/flipping/FlipTransitionBase;->a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;FF)Lflipboard/gui/flipping/SinglePage;

    move-result-object v0

    .line 228
    if-eqz v0, :cond_b

    .line 229
    invoke-direct {p0, p1}, Lflipboard/gui/flipping/FlipTransitionBase;->b(Landroid/view/MotionEvent;)V

    :cond_b
    move v0, v3

    .line 231
    goto/16 :goto_0

    .line 232
    :cond_c
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->t:I

    neg-int v0, v0

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_6

    .line 233
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->getPreviousView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_d

    .line 235
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    iput-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->i:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    .line 238
    :cond_d
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->b:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->n:F

    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionBase;->m:F

    invoke-virtual {p0, v0, v1, v2}, Lflipboard/gui/flipping/FlipTransitionBase;->a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;FF)Lflipboard/gui/flipping/SinglePage;

    move-result-object v0

    .line 239
    if-eqz v0, :cond_e

    .line 240
    invoke-direct {p0, p1}, Lflipboard/gui/flipping/FlipTransitionBase;->b(Landroid/view/MotionEvent;)V

    :cond_e
    move v0, v3

    .line 242
    goto/16 :goto_0

    .line 192
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private b(Landroid/view/MotionEvent;)V
    .locals 12

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const/high16 v10, 0x3f000000    # 0.5f

    const/high16 v7, -0x41000000    # -0.5f

    const/4 v6, 0x0

    const/4 v2, 0x1

    .line 377
    const/4 v1, 0x0

    .line 378
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const-string v3, "onTouchEvent"

    invoke-virtual {v0, v3}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V

    .line 384
    :try_start_0
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->j:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    sget-object v3, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne v0, v3, :cond_1

    .line 385
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget v3, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    invoke-virtual {v0, v3}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 390
    :goto_0
    iget-object v3, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v3}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 393
    if-nez v0, :cond_2

    .line 394
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase;->b:Lflipboard/util/Log;

    const-string v1, "No valid page in onTouchEvent, this is probably a bug."

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 471
    :cond_0
    :goto_1
    return-void

    .line 387
    :cond_1
    :try_start_1
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget v3, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 390
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0

    .line 398
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-eq v3, v4, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-ne v2, v3, :cond_13

    .line 399
    :cond_3
    iput-boolean v2, v0, Lflipboard/gui/flipping/SinglePage;->u:Z

    .line 400
    invoke-direct {p0, p1}, Lflipboard/gui/flipping/FlipTransitionBase;->c(Landroid/view/MotionEvent;)F

    move-result v4

    .line 401
    iget v3, p0, Lflipboard/gui/flipping/FlipTransitionBase;->G:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_b

    move v3, v2

    .line 402
    :goto_2
    if-nez v3, :cond_12

    .line 403
    iput v4, p0, Lflipboard/gui/flipping/FlipTransitionBase;->G:F

    .line 404
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->o:F

    sub-float/2addr v1, v4

    iget v4, p0, Lflipboard/gui/flipping/FlipTransitionBase;->p:F

    add-float/2addr v1, v4

    .line 406
    iget-object v4, p0, Lflipboard/gui/flipping/FlipTransitionBase;->j:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    sget-object v5, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne v4, v5, :cond_c

    .line 407
    iget v4, p0, Lflipboard/gui/flipping/FlipTransitionBase;->o:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    neg-float v4, v4

    add-float/2addr v1, v4

    .line 412
    :goto_3
    iget v4, p0, Lflipboard/gui/flipping/FlipTransitionBase;->o:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    div-float/2addr v1, v4

    const/high16 v4, -0x40800000    # -1.0f

    invoke-static {v1, v4, v8}, Lflipboard/util/JavaUtil;->a(FFF)F

    move-result v1

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->acos(D)D

    move-result-wide v4

    double-to-float v1, v4

    move v11, v3

    move v3, v1

    move v1, v11

    .line 416
    :goto_4
    if-nez v1, :cond_8

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-eq v1, v4, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v2, v1, :cond_8

    .line 420
    :cond_4
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->r:F

    sub-float v1, v3, v1

    iget v4, p0, Lflipboard/gui/flipping/FlipTransitionBase;->s:F

    const/high16 v5, 0x41200000    # 10.0f

    div-float/2addr v4, v5

    add-float/2addr v1, v4

    iput v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->s:F

    .line 423
    iget-boolean v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->u:Z

    if-eqz v1, :cond_5

    .line 425
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->j:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    sget-object v4, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->b:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne v1, v4, :cond_d

    .line 426
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->q:F

    const v4, 0x3f8ccccd    # 1.1f

    mul-float/2addr v1, v4

    add-float/2addr v1, v8

    .line 430
    :goto_5
    iget v4, p0, Lflipboard/gui/flipping/FlipTransitionBase;->s:F

    div-float v1, v4, v1

    iput v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->s:F

    .line 434
    :cond_5
    iget-boolean v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->u:Z

    if-nez v1, :cond_e

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->s:F

    cmpl-float v1, v1, v10

    if-gtz v1, :cond_6

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->s:F

    cmpg-float v1, v1, v7

    if-gez v1, :cond_e

    :cond_6
    move v1, v2

    :goto_6
    iput-boolean v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->A:Z

    .line 435
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->s:F

    invoke-static {v1, v7, v10}, Lflipboard/util/JavaUtil;->a(FFF)F

    move-result v1

    iput v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->s:F

    .line 437
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->q:F

    iget v4, p0, Lflipboard/gui/flipping/FlipTransitionBase;->s:F

    add-float/2addr v1, v4

    iput v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->q:F

    .line 438
    iget-boolean v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->u:Z

    if-eqz v1, :cond_7

    .line 439
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->j:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    sget-object v4, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne v1, v4, :cond_f

    .line 440
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->q:F

    float-to-double v4, v1

    const-wide v8, 0x3ffbecde5da115a9L    # 1.7453292519943295

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    double-to-float v1, v4

    iput v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->q:F

    .line 446
    :cond_7
    :goto_7
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->q:F

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/SinglePage;->a(F)V

    .line 447
    iput v3, p0, Lflipboard/gui/flipping/FlipTransitionBase;->r:F

    .line 449
    :cond_8
    iget-boolean v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->A:Z

    if-nez v1, :cond_9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-eq v1, v2, :cond_9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_0

    .line 450
    :cond_9
    iget-boolean v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->u:Z

    if-eqz v1, :cond_a

    .line 451
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->j:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    sget-object v3, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne v1, v3, :cond_10

    .line 452
    const v1, 0x3dcccccd    # 0.1f

    iput v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->s:F

    .line 453
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v1, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->g:Lflipboard/gui/flipping/TextPageLoadMore;

    iget-boolean v1, v1, Lflipboard/gui/flipping/TextPageLoadMore;->R:Z

    if-eqz v1, :cond_a

    .line 454
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->q:F

    sget v3, Lflipboard/gui/flipping/TextPageLoadMore;->Y:F

    cmpg-float v1, v1, v3

    if-gez v1, :cond_a

    .line 455
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->l()V

    .line 467
    :cond_a
    :goto_8
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->j:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    iget v3, p0, Lflipboard/gui/flipping/FlipTransitionBase;->s:F

    sget-object v4, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v4}, Lflipboard/app/FlipboardApplication;->n()Z

    move-result v4

    if-eqz v4, :cond_11

    const/16 v4, 0x177

    :goto_9
    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/flipping/SinglePage;->a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;ZFIZ)V

    .line 468
    iput-boolean v6, v0, Lflipboard/gui/flipping/SinglePage;->u:Z

    .line 469
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->d:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    goto/16 :goto_1

    :cond_b
    move v3, v6

    .line 401
    goto/16 :goto_2

    .line 409
    :cond_c
    iget v4, p0, Lflipboard/gui/flipping/FlipTransitionBase;->o:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    add-float/2addr v1, v4

    goto/16 :goto_3

    .line 428
    :cond_d
    const-wide v4, 0x4011d2b0a18be59aL    # 4.455751918948772

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->q:F

    float-to-double v8, v1

    sub-double/2addr v4, v8

    double-to-float v1, v4

    goto/16 :goto_5

    :cond_e
    move v1, v6

    .line 434
    goto/16 :goto_6

    .line 442
    :cond_f
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->q:F

    float-to-double v4, v1

    const-wide v8, 0x3ff6d91306c99d5bL    # 1.427996660722633

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    double-to-float v1, v4

    iput v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->q:F

    goto :goto_7

    .line 459
    :cond_10
    const v1, -0x42333333    # -0.1f

    iput v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->s:F

    .line 460
    iget-boolean v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->x:Z

    if-eqz v1, :cond_a

    .line 461
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->q:F

    sget v3, Lflipboard/gui/flipping/TextPageRefresh;->Y:F

    cmpl-float v1, v1, v3

    if-lez v1, :cond_a

    .line 462
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->k()V

    goto :goto_8

    .line 467
    :cond_11
    const/16 v4, 0x12c

    goto :goto_9

    :cond_12
    move v11, v3

    move v3, v1

    move v1, v11

    goto/16 :goto_4

    :cond_13
    move v3, v1

    move v1, v6

    goto/16 :goto_4
.end method

.method private c(Landroid/view/MotionEvent;)F
    .locals 2

    .prologue
    .line 739
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->w:Z

    if-eqz v0, :cond_0

    .line 740
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->f:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 741
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 747
    :goto_0
    sub-float v0, v1, v0

    return v0

    .line 743
    :cond_0
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->g:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 744
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    goto :goto_0
.end method

.method private c(I)Lflipboard/gui/flipping/SinglePage;
    .locals 2

    .prologue
    .line 642
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0, p1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v0

    .line 643
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lflipboard/gui/flipping/SinglePage;->D:Z

    if-nez v1, :cond_0

    invoke-virtual {p0, v0, p1}, Lflipboard/gui/flipping/FlipTransitionBase;->a(Lflipboard/gui/flipping/SinglePage;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 644
    invoke-virtual {p0, p1}, Lflipboard/gui/flipping/FlipTransitionBase;->a(I)Lflipboard/gui/flipping/FlippingBitmap;

    move-result-object v1

    .line 645
    if-eqz v1, :cond_0

    .line 646
    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/SinglePage;->a(Lflipboard/gui/flipping/FlippingBitmap;)V

    .line 647
    invoke-virtual {p0, p1}, Lflipboard/gui/flipping/FlipTransitionBase;->b(I)V

    .line 650
    :cond_0
    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 833
    sget-object v1, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    if-eqz v1, :cond_1

    .line 834
    sget-object v0, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->g:I

    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionBase;->f:I

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/flipping/ViewScreenshotCreator;->b(II)Z

    move-result v0

    .line 839
    :goto_0
    if-eqz v0, :cond_0

    .line 841
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const-string v1, "setSizeChanged"

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->r:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 843
    :cond_0
    return-void

    .line 836
    :cond_1
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->g:I

    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionBase;->f:I

    invoke-static {v1, v2}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(II)V

    goto :goto_0

    .line 841
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v1
.end method

.method private r()V
    .locals 3

    .prologue
    .line 847
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 848
    const/4 v0, -0x1

    :goto_0
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v1, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 849
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1, v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v1

    .line 850
    iget-boolean v2, p0, Lflipboard/gui/flipping/FlipTransitionBase;->w:Z

    iput-boolean v2, v1, Lflipboard/gui/flipping/SinglePage;->n:Z

    .line 848
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 853
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract a(I)Lflipboard/gui/flipping/FlippingBitmap;
.end method

.method protected final a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;FF)Lflipboard/gui/flipping/SinglePage;
    .locals 1

    .prologue
    .line 263
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lflipboard/gui/flipping/FlipTransitionBase;->a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;FFZ)Lflipboard/gui/flipping/SinglePage;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;FFZ)Lflipboard/gui/flipping/SinglePage;
    .locals 8

    .prologue
    const v6, 0x40490fdb    # (float)Math.PI

    const/4 v7, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 267
    invoke-static {p0}, Lflipboard/gui/flipping/FlipUtil;->a(Lflipboard/gui/flipping/FlipTransitionBase;)V

    .line 268
    invoke-static {}, Lflipboard/util/HappyUser;->b()V

    .line 269
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->E:Lflipboard/util/Log;

    new-array v0, v1, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 273
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase$5;->a:[I

    invoke-virtual {p1}, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 294
    const/4 v0, -0x1

    move v3, v0

    .line 297
    :goto_0
    iget-boolean v4, p0, Lflipboard/gui/flipping/FlipTransitionBase;->u:Z

    if-eqz v4, :cond_0

    .line 298
    invoke-virtual {p0, v1}, Lflipboard/gui/flipping/FlipTransitionBase;->performHapticFeedback(I)Z

    .line 303
    :cond_0
    if-nez p4, :cond_3

    .line 304
    invoke-direct {p0, v3}, Lflipboard/gui/flipping/FlipTransitionBase;->c(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v3

    .line 305
    invoke-direct {p0, v0}, Lflipboard/gui/flipping/FlipTransitionBase;->c(I)Lflipboard/gui/flipping/SinglePage;

    .line 309
    :goto_1
    if-nez v3, :cond_4

    .line 311
    const/4 v0, 0x0

    .line 357
    :goto_2
    return-object v0

    .line 275
    :pswitch_0
    iget v4, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    .line 276
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    add-int/lit8 v3, v0, 0x1

    .line 277
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->getNumberOfPages()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne v0, v5, :cond_1

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->u:Z

    .line 278
    iput v6, p0, Lflipboard/gui/flipping/FlipTransitionBase;->r:F

    .line 279
    iput v6, p0, Lflipboard/gui/flipping/FlipTransitionBase;->q:F

    .line 280
    iput v7, p0, Lflipboard/gui/flipping/FlipTransitionBase;->s:F

    move v0, v3

    move v3, v4

    .line 281
    goto :goto_0

    :cond_1
    move v0, v2

    .line 277
    goto :goto_3

    .line 284
    :pswitch_1
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    add-int/lit8 v3, v0, -0x1

    .line 285
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    if-nez v0, :cond_2

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->u:Z

    .line 286
    iput v7, p0, Lflipboard/gui/flipping/FlipTransitionBase;->r:F

    .line 287
    iput v7, p0, Lflipboard/gui/flipping/FlipTransitionBase;->q:F

    .line 288
    iput v7, p0, Lflipboard/gui/flipping/FlipTransitionBase;->s:F

    .line 289
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    goto :goto_0

    :cond_2
    move v0, v2

    .line 285
    goto :goto_4

    .line 307
    :cond_3
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0, v3}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v3

    goto :goto_1

    .line 315
    :cond_4
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iput-boolean v1, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->s:Z

    .line 318
    iget v4, p0, Lflipboard/gui/flipping/FlipTransitionBase;->C:I

    .line 319
    iput-boolean v2, p0, Lflipboard/gui/flipping/FlipTransitionBase;->v:Z

    .line 326
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->w:Z

    if-eqz v0, :cond_7

    .line 327
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->f:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 331
    :goto_5
    sub-float v5, p3, v0

    neg-float v6, v0

    invoke-static {v5, v6, v0}, Lflipboard/util/JavaUtil;->a(FFF)F

    move-result v5

    .line 334
    cmpl-float v6, v5, v7

    if-ltz v6, :cond_8

    .line 335
    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v0, v6

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->o:F

    .line 339
    :goto_6
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->o:F

    sub-float v0, v5, v0

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->p:F

    .line 340
    iput-object p1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->j:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    .line 341
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lflipboard/gui/flipping/FlipTransitionBase;->k:J

    .line 343
    iget-boolean v0, v3, Lflipboard/gui/flipping/SinglePage;->z:Z

    if-nez v0, :cond_5

    .line 344
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 345
    iput-boolean v1, v3, Lflipboard/gui/flipping/SinglePage;->z:Z

    .line 347
    :cond_5
    iput-boolean v1, v3, Lflipboard/gui/flipping/SinglePage;->u:Z

    .line 348
    iput v2, v3, Lflipboard/gui/flipping/SinglePage;->v:I

    .line 349
    iput v4, v3, Lflipboard/gui/flipping/SinglePage;->y:I

    .line 350
    iput-boolean v2, v3, Lflipboard/gui/flipping/SinglePage;->w:Z

    .line 351
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->q:F

    invoke-virtual {v3, v0}, Lflipboard/gui/flipping/SinglePage;->a(F)V

    .line 354
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/model/ConfigSetting;->LogStartFlipTime:Z

    if-eqz v0, :cond_6

    .line 355
    const-string v0, "start_flip_%s_time"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lflipboard/gui/flipping/FlipTransitionBase;->F:J

    sub-long/2addr v4, v6

    invoke-static {v0, v4, v5}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;J)V

    :cond_6
    move-object v0, v3

    .line 357
    goto/16 :goto_2

    .line 330
    :cond_7
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->g:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    move p3, p2

    goto :goto_5

    .line 337
    :cond_8
    const/high16 v6, -0x40000000    # -2.0f

    div-float/2addr v0, v6

    invoke-static {v5, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->o:F

    goto :goto_6

    .line 273
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public abstract a()V
.end method

.method public a(IZ)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 517
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    if-nez v0, :cond_1

    .line 543
    :cond_0
    return-void

    .line 521
    :cond_1
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->a:I

    rsub-int v0, v0, 0x3e8

    int-to-float v0, v0

    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionBase;->a:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    sget v2, Lflipboard/gui/flipping/SinglePage;->M:F

    div-float/2addr v0, v2

    float-to-int v4, v0

    .line 523
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v5

    move v3, v1

    move v2, v1

    .line 525
    :goto_0
    if-ge v3, v5, :cond_0

    .line 526
    if-lt v3, v4, :cond_2

    add-int/lit8 v0, v5, -0x2

    if-lt v3, v0, :cond_5

    :cond_2
    const/4 v0, 0x3

    if-ge v2, v0, :cond_5

    const/4 v0, 0x1

    .line 527
    :goto_1
    iget-object v6, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget v7, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    invoke-virtual {v6, v7}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v6

    .line 528
    if-eqz v6, :cond_3

    iget-boolean v6, v6, Lflipboard/gui/flipping/SinglePage;->B:Z

    if-eqz v6, :cond_3

    .line 529
    add-int/lit8 v2, v2, 0x1

    .line 531
    :cond_3
    iget v6, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    if-le v6, p1, :cond_6

    .line 532
    sget-object v6, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->b:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    invoke-direct {p0, v6, v0, p2}, Lflipboard/gui/flipping/FlipTransitionBase;->a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;ZZ)V

    .line 533
    if-nez p2, :cond_4

    .line 534
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    .line 525
    :cond_4
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_5
    move v0, v1

    .line 526
    goto :goto_1

    .line 537
    :cond_6
    sget-object v6, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    invoke-direct {p0, v6, v0, p2}, Lflipboard/gui/flipping/FlipTransitionBase;->a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;ZZ)V

    .line 538
    if-nez p2, :cond_4

    .line 539
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    goto :goto_2
.end method

.method public abstract a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;)V
.end method

.method public final a(Lflipboard/gui/flipping/SinglePage;)V
    .locals 2

    .prologue
    .line 576
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/flipping/FlipTransitionBase$2;

    invoke-direct {v1, p0}, Lflipboard/gui/flipping/FlipTransitionBase$2;-><init>(Lflipboard/gui/flipping/FlipTransitionBase;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 585
    iget-boolean v0, p1, Lflipboard/gui/flipping/SinglePage;->H:Z

    if-nez v0, :cond_0

    .line 587
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->s:Z

    .line 592
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->f()V

    .line 593
    return-void
.end method

.method protected a(Lflipboard/gui/flipping/SinglePage;I)Z
    .locals 1

    .prologue
    .line 482
    if-ltz p2, :cond_0

    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->getNumberOfPages()I

    move-result v0

    if-ge p2, v0, :cond_0

    iget-boolean v0, p1, Lflipboard/gui/flipping/SinglePage;->B:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(I)V
    .locals 0

    .prologue
    .line 686
    return-void
.end method

.method public abstract b(Lflipboard/gui/flipping/FlipTransitionBase$Direction;)V
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 731
    iput-boolean p1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->l:Z

    .line 732
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->v:Z

    .line 733
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x1

    return v0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 558
    invoke-static {p0}, Lflipboard/gui/flipping/FlipUtil;->b(Lflipboard/gui/flipping/FlipTransitionBase;)V

    .line 559
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    .line 926
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->D:Z

    .line 927
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->d:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->onPause()V

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const-string v1, "onPause"

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->s:Z

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->c()V

    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v1, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1, v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v1

    const/4 v2, 0x0

    iput-boolean v2, v1, Lflipboard/gui/flipping/SinglePage;->u:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0
.end method

.method public final f()V
    .locals 6

    .prologue
    .line 600
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lflipboard/gui/flipping/FlipTransitionBase;->e:J

    const-wide/16 v4, 0x15e

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 609
    :goto_0
    return-void

    .line 603
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/flipping/FlipTransitionBase$3;

    invoke-direct {v1, p0}, Lflipboard/gui/flipping/FlipTransitionBase$3;-><init>(Lflipboard/gui/flipping/FlipTransitionBase;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 618
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->getPreviousView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 619
    invoke-direct {p0, v1}, Lflipboard/gui/flipping/FlipTransitionBase;->c(I)Lflipboard/gui/flipping/SinglePage;

    .line 620
    invoke-virtual {p0, v1, v1}, Lflipboard/gui/flipping/FlipTransitionBase;->a(IZ)V

    .line 622
    :cond_0
    return-void
.end method

.method public gatherTransparentRegion(Landroid/graphics/Region;)Z
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x1

    return v0
.end method

.method public getCancelDistance()I
    .locals 1

    .prologue
    .line 475
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->t:I

    mul-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public abstract getCurrentView()Landroid/view/View;
.end method

.method public getCurrentViewIndex()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    return v0
.end method

.method public getDesiredNumberOfTextures()I
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x4

    return v0
.end method

.method public getLastFlipTime()J
    .locals 2

    .prologue
    .line 763
    iget-wide v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->k:J

    return-wide v0
.end method

.method protected abstract getNextView()Landroid/view/View;
.end method

.method public abstract getNumberOfPages()I
.end method

.method protected abstract getPreviousView()Landroid/view/View;
.end method

.method public getRenderer()Lflipboard/gui/flipping/OpenGLTransitionRenderer;
    .locals 1

    .prologue
    .line 857
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    return-object v0
.end method

.method public getRunningFlips()I
    .locals 1

    .prologue
    .line 865
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public getShareScreenshot()Lflipboard/gui/flipping/FlippingBitmap;
    .locals 2

    .prologue
    .line 689
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final h()I
    .locals 2

    .prologue
    .line 631
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    .line 632
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->getNextView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 633
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->getNumberOfPages()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 634
    invoke-direct {p0, v0}, Lflipboard/gui/flipping/FlipTransitionBase;->c(I)Lflipboard/gui/flipping/SinglePage;

    .line 635
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/flipping/FlipTransitionBase;->a(IZ)V

    .line 637
    :cond_0
    return v0
.end method

.method public final i()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 658
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->getNextView()Landroid/view/View;

    move-result-object v0

    .line 659
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->j:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    sget-object v2, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne v1, v2, :cond_1

    :cond_0
    if-eqz v0, :cond_1

    .line 660
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    invoke-direct {p0, v0}, Lflipboard/gui/flipping/FlipTransitionBase;->c(I)Lflipboard/gui/flipping/SinglePage;

    .line 661
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lflipboard/gui/flipping/FlipTransitionBase;->c(I)Lflipboard/gui/flipping/SinglePage;

    .line 662
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0, v3}, Lflipboard/gui/flipping/FlipTransitionBase;->a(IZ)V

    .line 666
    :goto_0
    return-void

    .line 664
    :cond_1
    invoke-virtual {p0, v3}, Lflipboard/gui/flipping/FlipTransitionBase;->performHapticFeedback(I)Z

    goto :goto_0
.end method

.method public final j()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 673
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->getPreviousView()Landroid/view/View;

    move-result-object v0

    .line 674
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->j:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    sget-object v2, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->b:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne v1, v2, :cond_1

    :cond_0
    if-eqz v0, :cond_1

    .line 675
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    invoke-direct {p0, v0}, Lflipboard/gui/flipping/FlipTransitionBase;->c(I)Lflipboard/gui/flipping/SinglePage;

    .line 676
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lflipboard/gui/flipping/FlipTransitionBase;->c(I)Lflipboard/gui/flipping/SinglePage;

    .line 677
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->h:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0, v3}, Lflipboard/gui/flipping/FlipTransitionBase;->a(IZ)V

    .line 681
    :goto_0
    return-void

    .line 679
    :cond_1
    invoke-virtual {p0, v3}, Lflipboard/gui/flipping/FlipTransitionBase;->performHapticFeedback(I)Z

    goto :goto_0
.end method

.method public k()V
    .locals 0

    .prologue
    .line 772
    return-void
.end method

.method protected l()V
    .locals 0

    .prologue
    .line 780
    return-void
.end method

.method protected final m()Z
    .locals 2

    .prologue
    .line 870
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()V
    .locals 2

    .prologue
    .line 875
    const/4 v0, -0x1

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v1, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->f:Lflipboard/gui/flipping/TextPageRefresh;

    invoke-static {v0, v1}, Lflipboard/gui/flipping/FlipTransitionBase;->a(ILflipboard/gui/flipping/SinglePage;)V

    .line 876
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Renderer noBitmaps: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-boolean v1, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->q:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 877
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->getNumberOfPages()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 878
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1, v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/flipping/FlipTransitionBase;->a(ILflipboard/gui/flipping/SinglePage;)V

    .line 877
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 880
    :cond_0
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v1, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->g:Lflipboard/gui/flipping/TextPageLoadMore;

    invoke-static {v0, v1}, Lflipboard/gui/flipping/FlipTransitionBase;->a(ILflipboard/gui/flipping/SinglePage;)V

    .line 881
    return-void
.end method

.method public o()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 903
    iput-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->D:Z

    .line 904
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lflipboard/gui/flipping/FlipTransitionBase;->e:J

    .line 905
    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->g:I

    if-lez v1, :cond_0

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->f:I

    if-lez v1, :cond_0

    .line 908
    invoke-direct {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->c()V

    .line 910
    :cond_0
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->d:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v1}, Landroid/opengl/GLSurfaceView;->onResume()V

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const-string v2, "onResume"

    invoke-virtual {v1, v2}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iput-boolean v0, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->q:Z

    :goto_0
    :try_start_0
    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v1, v1, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1, v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(I)Lflipboard/gui/flipping/SinglePage;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, v1, Lflipboard/gui/flipping/SinglePage;->B:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    .line 911
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const/16 v1, 0x15e

    new-instance v2, Lflipboard/gui/flipping/FlipTransitionBase$4;

    invoke-direct {v2, p0}, Lflipboard/gui/flipping/FlipTransitionBase$4;-><init>(Lflipboard/gui/flipping/FlipTransitionBase;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(ILjava/lang/Runnable;)V

    .line 917
    return-void

    .line 910
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    throw v0
.end method

.method public onAttachedToWindow()V
    .locals 1

    .prologue
    .line 785
    invoke-super {p0}, Lflipboard/gui/ContainerView;->onAttachedToWindow()V

    .line 786
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    .line 787
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 788
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->n()Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->w:Z

    .line 789
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 794
    invoke-super {p0}, Lflipboard/gui/ContainerView;->onDetachedFromWindow()V

    .line 795
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    .line 796
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 797
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lflipboard/gui/flipping/FlipTransitionBase;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 810
    if-eqz p1, :cond_0

    .line 811
    sub-int v0, p4, p2

    .line 812
    sub-int v1, p5, p3

    .line 814
    iget-object v2, p0, Lflipboard/gui/flipping/FlipTransitionBase;->d:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v2, v3, v3, v0, v1}, Landroid/opengl/GLSurfaceView;->layout(IIII)V

    .line 816
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v2}, Lflipboard/app/FlipboardApplication;->n()Z

    .line 817
    invoke-direct {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->r()V

    .line 819
    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->g:I

    .line 820
    iput v1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->f:I

    .line 824
    :cond_0
    invoke-direct {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->c()V

    .line 825
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 801
    const-string v0, "flip_orientation"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 802
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->n()Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->w:Z

    .line 803
    invoke-direct {p0}, Lflipboard/gui/flipping/FlipTransitionBase;->r()V

    .line 805
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->A:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->v:Z

    if-nez v0, :cond_1

    .line 366
    invoke-direct {p0, p1}, Lflipboard/gui/flipping/FlipTransitionBase;->b(Landroid/view/MotionEvent;)V

    .line 372
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 368
    :cond_1
    invoke-direct {p0, p1}, Lflipboard/gui/flipping/FlipTransitionBase;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->A:Z

    if-eqz v0, :cond_0

    .line 369
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 921
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->D:Z

    return v0
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 969
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionBase;->d:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->onPause()V

    .line 970
    return-void
.end method

.method public setShouldHaveFlipToRefresh(Z)V
    .locals 0

    .prologue
    .line 897
    iput-boolean p1, p0, Lflipboard/gui/flipping/FlipTransitionBase;->x:Z

    .line 898
    return-void
.end method
