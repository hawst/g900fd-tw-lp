.class public final enum Lflipboard/gui/flipping/FlipUtil$FlippingMessages;
.super Ljava/lang/Enum;
.source "FlipUtil.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/flipping/FlipUtil$FlippingMessages;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

.field public static final enum b:Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

.field private static final synthetic c:[Lflipboard/gui/flipping/FlipUtil$FlippingMessages;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 15
    new-instance v0, Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

    const-string v1, "flipStarted"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/flipping/FlipUtil$FlippingMessages;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/flipping/FlipUtil$FlippingMessages;->a:Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

    new-instance v0, Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

    const-string v1, "flipsIdle"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/flipping/FlipUtil$FlippingMessages;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/flipping/FlipUtil$FlippingMessages;->b:Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

    sget-object v1, Lflipboard/gui/flipping/FlipUtil$FlippingMessages;->a:Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/flipping/FlipUtil$FlippingMessages;->b:Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

    aput-object v1, v0, v3

    sput-object v0, Lflipboard/gui/flipping/FlipUtil$FlippingMessages;->c:[Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/flipping/FlipUtil$FlippingMessages;
    .locals 1

    .prologue
    .line 15
    const-class v0, Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/flipping/FlipUtil$FlippingMessages;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lflipboard/gui/flipping/FlipUtil$FlippingMessages;->c:[Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

    invoke-virtual {v0}, [Lflipboard/gui/flipping/FlipUtil$FlippingMessages;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

    return-object v0
.end method
