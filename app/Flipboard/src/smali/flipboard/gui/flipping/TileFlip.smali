.class public Lflipboard/gui/flipping/TileFlip;
.super Lflipboard/gui/flipping/SinglePage;
.source "TileFlip.java"


# static fields
.field protected static final R:Ljava/nio/FloatBuffer;

.field protected static final S:Ljava/nio/FloatBuffer;


# instance fields
.field public N:Lflipboard/gui/flipping/SinglePage;

.field public O:Lflipboard/gui/flipping/SinglePage;

.field final P:Lflipboard/gui/flipping/FlippingContainer;

.field public Q:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x20

    .line 22
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 23
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 24
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/TileFlip;->R:Ljava/nio/FloatBuffer;

    .line 26
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 27
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 28
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lflipboard/gui/flipping/TileFlip;->S:Ljava/nio/FloatBuffer;

    .line 29
    return-void
.end method

.method public constructor <init>(Lflipboard/gui/flipping/FlipTransitionBase;Lflipboard/gui/flipping/OpenGLTransitionRenderer;Lflipboard/gui/flipping/FlippingContainer;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lflipboard/gui/flipping/SinglePage;-><init>(Lflipboard/gui/flipping/FlipTransitionBase;Lflipboard/gui/flipping/OpenGLTransitionRenderer;)V

    .line 34
    iput-object p3, p0, Lflipboard/gui/flipping/TileFlip;->P:Lflipboard/gui/flipping/FlippingContainer;

    .line 35
    return-void
.end method


# virtual methods
.method final a()Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lflipboard/gui/flipping/TileFlip;->R:Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method final b()Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lflipboard/gui/flipping/TileFlip;->S:Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method public final i()Lflipboard/gui/flipping/SinglePage;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lflipboard/gui/flipping/TileFlip;->N:Lflipboard/gui/flipping/SinglePage;

    return-object v0
.end method

.method public final j()Lflipboard/gui/flipping/SinglePage;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lflipboard/gui/flipping/TileFlip;->O:Lflipboard/gui/flipping/SinglePage;

    return-object v0
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 61
    const/high16 v0, 0x44480000    # 800.0f

    iput v0, p0, Lflipboard/gui/flipping/TileFlip;->J:F

    .line 62
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/gui/flipping/TileFlip;->I:J

    .line 63
    return-void
.end method
