.class public Lflipboard/gui/flipping/TextPageRefresh;
.super Lflipboard/gui/flipping/TextPage;
.source "TextPageRefresh.java"


# static fields
.field public static Y:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lflipboard/gui/flipping/TextPageRefresh;->Y:F

    return-void
.end method

.method public constructor <init>(Lflipboard/gui/flipping/FlipTransitionBase;Lflipboard/gui/flipping/OpenGLTransitionRenderer;)V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lflipboard/gui/flipping/TextPage;-><init>(Lflipboard/gui/flipping/FlipTransitionBase;Lflipboard/gui/flipping/OpenGLTransitionRenderer;)V

    .line 16
    invoke-virtual {p1}, Lflipboard/gui/flipping/FlipTransitionBase;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d0131

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/TextPageRefresh;->a(Ljava/lang/String;)V

    .line 17
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 3

    .prologue
    const/high16 v2, 0x43340000    # 180.0f

    const/4 v1, 0x0

    .line 32
    invoke-super {p0, p1}, Lflipboard/gui/flipping/TextPage;->a(F)V

    .line 33
    sget v0, Lflipboard/gui/flipping/TextPageRefresh;->Y:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 34
    iget v0, p0, Lflipboard/gui/flipping/TextPageRefresh;->Q:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_0

    .line 35
    iput v2, p0, Lflipboard/gui/flipping/TextPageRefresh;->Q:F

    .line 36
    iget-object v0, p0, Lflipboard/gui/flipping/TextPageRefresh;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->h:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 37
    const v1, 0x7f0d0269

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/TextPageRefresh;->a(Ljava/lang/String;)V

    .line 38
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/gui/flipping/TextPageRefresh;->S:J

    .line 48
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    iget v0, p0, Lflipboard/gui/flipping/TextPageRefresh;->Q:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 42
    iput v1, p0, Lflipboard/gui/flipping/TextPageRefresh;->Q:F

    .line 43
    iget-object v0, p0, Lflipboard/gui/flipping/TextPageRefresh;->a:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->h:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 44
    const v1, 0x7f0d0131

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/flipping/TextPageRefresh;->a(Ljava/lang/String;)V

    .line 45
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/gui/flipping/TextPageRefresh;->S:J

    goto :goto_0
.end method

.method protected final m()V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 22
    invoke-super {p0}, Lflipboard/gui/flipping/TextPage;->m()V

    .line 25
    iget v0, p0, Lflipboard/gui/flipping/TextPageRefresh;->s:I

    int-to-float v0, v0

    div-float/2addr v0, v2

    iget v1, p0, Lflipboard/gui/flipping/TextPageRefresh;->V:F

    sub-float/2addr v0, v1

    iget v1, p0, Lflipboard/gui/flipping/TextPageRefresh;->O:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lflipboard/gui/flipping/TextPageRefresh;->U:F

    .line 26
    iget v0, p0, Lflipboard/gui/flipping/TextPageRefresh;->t:I

    div-int/lit8 v0, v0, -0x2

    int-to-float v0, v0

    iget v1, p0, Lflipboard/gui/flipping/TextPageRefresh;->V:F

    add-float/2addr v0, v1

    iget v1, p0, Lflipboard/gui/flipping/TextPageRefresh;->N:F

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lflipboard/gui/flipping/TextPageRefresh;->T:F

    .line 27
    return-void
.end method
