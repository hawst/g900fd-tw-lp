.class public final enum Lflipboard/gui/flipping/FlipTransitionViews$Message;
.super Ljava/lang/Enum;
.source "FlipTransitionViews.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/flipping/FlipTransitionViews$Message;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/flipping/FlipTransitionViews$Message;

.field public static final enum b:Lflipboard/gui/flipping/FlipTransitionViews$Message;

.field public static final enum c:Lflipboard/gui/flipping/FlipTransitionViews$Message;

.field public static final enum d:Lflipboard/gui/flipping/FlipTransitionViews$Message;

.field public static final enum e:Lflipboard/gui/flipping/FlipTransitionViews$Message;

.field private static final synthetic f:[Lflipboard/gui/flipping/FlipTransitionViews$Message;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 89
    new-instance v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;

    const-string v1, "FLIP_STARTED"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/flipping/FlipTransitionViews$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;->a:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    .line 90
    new-instance v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;

    const-string v1, "FLIP_FINISHED"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/flipping/FlipTransitionViews$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;->b:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    .line 91
    new-instance v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;

    const-string v1, "FLIP_WILL_COMPLETE"

    invoke-direct {v0, v1, v4}, Lflipboard/gui/flipping/FlipTransitionViews$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;->c:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    .line 92
    new-instance v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;

    const-string v1, "FLIPS_IDLE"

    invoke-direct {v0, v1, v5}, Lflipboard/gui/flipping/FlipTransitionViews$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;->d:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    .line 93
    new-instance v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;

    const-string v1, "FLIP_NEXT_TO_LOAD_MORE"

    invoke-direct {v0, v1, v6}, Lflipboard/gui/flipping/FlipTransitionViews$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;->e:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    .line 87
    const/4 v0, 0x5

    new-array v0, v0, [Lflipboard/gui/flipping/FlipTransitionViews$Message;

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionViews$Message;->a:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionViews$Message;->b:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionViews$Message;->c:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionViews$Message;->d:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionViews$Message;->e:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    aput-object v1, v0, v6

    sput-object v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;->f:[Lflipboard/gui/flipping/FlipTransitionViews$Message;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/flipping/FlipTransitionViews$Message;
    .locals 1

    .prologue
    .line 87
    const-class v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/flipping/FlipTransitionViews$Message;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;->f:[Lflipboard/gui/flipping/FlipTransitionViews$Message;

    invoke-virtual {v0}, [Lflipboard/gui/flipping/FlipTransitionViews$Message;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/flipping/FlipTransitionViews$Message;

    return-object v0
.end method
