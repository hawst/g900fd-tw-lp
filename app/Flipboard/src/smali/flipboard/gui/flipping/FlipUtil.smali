.class public Lflipboard/gui/flipping/FlipUtil;
.super Ljava/lang/Object;
.source "FlipUtil.java"


# static fields
.field private static a:Lflipboard/util/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observable",
            "<",
            "Ljava/lang/Object;",
            "Lflipboard/gui/flipping/FlipUtil$FlippingMessages;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lflipboard/util/Observable;

    invoke-direct {v0}, Lflipboard/util/Observable;-><init>()V

    sput-object v0, Lflipboard/gui/flipping/FlipUtil;->a:Lflipboard/util/Observable;

    return-void
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FlipboardManager:animationStarted"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 49
    sget v0, Lflipboard/gui/flipping/FlipUtil;->b:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lflipboard/gui/flipping/FlipUtil;->b:I

    .line 50
    return-void
.end method

.method public static a(I)V
    .locals 2

    .prologue
    .line 63
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FlipboardManager:animationsEnded"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 64
    sget v0, Lflipboard/gui/flipping/FlipUtil;->b:I

    sub-int/2addr v0, p0

    .line 65
    sput v0, Lflipboard/gui/flipping/FlipUtil;->b:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    sput v0, Lflipboard/gui/flipping/FlipUtil;->b:I

    .line 66
    return-void
.end method

.method public static a(Lflipboard/gui/flipping/FlipTransitionBase;)V
    .locals 2

    .prologue
    .line 35
    sget-object v0, Lflipboard/gui/flipping/FlipUtil;->a:Lflipboard/util/Observable;

    sget-object v1, Lflipboard/gui/flipping/FlipUtil$FlippingMessages;->a:Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

    invoke-virtual {v0, v1, p0}, Lflipboard/util/Observable;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 36
    return-void
.end method

.method public static a(Lflipboard/util/Observer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/util/Observer",
            "<",
            "Ljava/lang/Object;",
            "Lflipboard/gui/flipping/FlipUtil$FlippingMessages;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    sget-object v0, Lflipboard/gui/flipping/FlipUtil;->a:Lflipboard/util/Observable;

    invoke-virtual {v0, p0}, Lflipboard/util/Observable;->b(Lflipboard/util/Observer;)V

    .line 26
    return-void
.end method

.method public static b()V
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FlipboardManager:animationEnded"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 58
    const/4 v0, 0x1

    invoke-static {v0}, Lflipboard/gui/flipping/FlipUtil;->a(I)V

    .line 59
    return-void
.end method

.method public static b(Lflipboard/gui/flipping/FlipTransitionBase;)V
    .locals 2

    .prologue
    .line 40
    sget-object v0, Lflipboard/gui/flipping/FlipUtil;->a:Lflipboard/util/Observable;

    sget-object v1, Lflipboard/gui/flipping/FlipUtil$FlippingMessages;->b:Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

    invoke-virtual {v0, v1, p0}, Lflipboard/util/Observable;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 41
    return-void
.end method

.method public static b(Lflipboard/util/Observer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/util/Observer",
            "<",
            "Ljava/lang/Object;",
            "Lflipboard/gui/flipping/FlipUtil$FlippingMessages;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    sget-object v0, Lflipboard/gui/flipping/FlipUtil;->a:Lflipboard/util/Observable;

    invoke-virtual {v0, p0}, Lflipboard/util/Observable;->c(Lflipboard/util/Observer;)V

    .line 31
    return-void
.end method

.method public static c()V
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FlipboardManager:clearRunningAnimations"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 71
    const/4 v0, 0x0

    sput v0, Lflipboard/gui/flipping/FlipUtil;->b:I

    .line 72
    return-void
.end method

.method public static d()I
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FlipboardManager:getRunningAnimations"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 79
    sget v0, Lflipboard/gui/flipping/FlipUtil;->b:I

    return v0
.end method
