.class Lflipboard/gui/flipping/FlipTransitionViews$11;
.super Lflipboard/gui/flipping/FlipTransitionViews$HopTask;
.source "FlipTransitionViews.java"


# instance fields
.field final synthetic a:F

.field final synthetic b:I

.field final synthetic c:Lflipboard/gui/flipping/SinglePage;

.field final synthetic d:Lflipboard/gui/flipping/FlipTransitionViews;

.field private g:F

.field private final h:F

.field private final i:F

.field private j:J


# direct methods
.method constructor <init>(Lflipboard/gui/flipping/FlipTransitionViews;FILflipboard/gui/flipping/SinglePage;)V
    .locals 4

    .prologue
    .line 1278
    iput-object p1, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->d:Lflipboard/gui/flipping/FlipTransitionViews;

    iput p2, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->a:F

    iput p3, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->b:I

    iput-object p4, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->c:Lflipboard/gui/flipping/SinglePage;

    invoke-direct {p0, p1}, Lflipboard/gui/flipping/FlipTransitionViews$HopTask;-><init>(Lflipboard/gui/flipping/FlipTransitionViews;)V

    .line 1280
    const/high16 v0, 0x40800000    # 4.0f

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->a:F

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    const-wide/high16 v2, -0x4000000000000000L    # -2.0

    div-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->h:F

    .line 1281
    iget v0, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->h:F

    const/high16 v1, -0x40000000    # -2.0f

    mul-float/2addr v0, v1

    iget v1, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->b:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->i:F

    .line 1282
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->j:J

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 1286
    iget-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->e:Z

    if-eqz v0, :cond_1

    .line 1287
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews$11;->cancel()Z

    .line 1304
    :cond_0
    :goto_0
    return-void

    .line 1289
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->j:J

    sub-long/2addr v0, v2

    .line 1290
    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->h:F

    long-to-float v3, v0

    iget v4, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->i:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->g:F

    .line 1291
    const-wide v2, 0x400921fb54442d18L    # Math.PI

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    iget v6, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->g:F

    float-to-double v6, v6

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    mul-double/2addr v4, v6

    iget v6, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->a:F

    float-to-double v6, v6

    add-double/2addr v4, v6

    const-wide v6, 0x404ca5dc20000000L    # 57.295780181884766

    div-double/2addr v4, v6

    sub-double/2addr v2, v4

    double-to-float v2, v2

    .line 1292
    iget-object v3, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->c:Lflipboard/gui/flipping/SinglePage;

    invoke-virtual {v3, v2}, Lflipboard/gui/flipping/SinglePage;->a(F)V

    .line 1293
    iget v2, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->b:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->e:Z

    if-nez v0, :cond_0

    .line 1294
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews$11;->cancel()Z

    .line 1297
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->d:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0, v10}, Lflipboard/gui/flipping/FlipTransitionViews;->b(Z)V

    .line 1298
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->c:Lflipboard/gui/flipping/SinglePage;

    const v1, 0x40490fdb    # (float)Math.PI

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/SinglePage;->a(F)V

    .line 1299
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->c:Lflipboard/gui/flipping/SinglePage;

    iput-boolean v10, v0, Lflipboard/gui/flipping/SinglePage;->u:Z

    .line 1300
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->c:Lflipboard/gui/flipping/SinglePage;

    iput-boolean v10, v0, Lflipboard/gui/flipping/SinglePage;->z:Z

    .line 1301
    iget-object v0, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->d:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v1, p0, Lflipboard/gui/flipping/FlipTransitionViews$11;->c:Lflipboard/gui/flipping/SinglePage;

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->a(Lflipboard/gui/flipping/SinglePage;)V

    goto :goto_0
.end method
