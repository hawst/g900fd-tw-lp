.class public Lflipboard/gui/ServicePromptView;
.super Landroid/widget/LinearLayout;
.source "ServicePromptView.java"


# instance fields
.field a:Lflipboard/gui/FLButton;

.field b:Landroid/widget/TextView;

.field c:Landroid/widget/TextView;

.field d:Lflipboard/gui/FLTextIntf;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 220
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 221
    invoke-virtual {p1, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 222
    if-ltz v1, :cond_0

    .line 224
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 225
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lflipboard/gui/ServicePromptView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080089

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v1

    const/16 v4, 0x21

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    move-object p1, v0

    .line 230
    :cond_0
    return-object p1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)Z
    .locals 11

    .prologue
    const v9, 0x7f0a02ff

    const/4 v3, 0x1

    const/16 v10, 0x8

    const/4 v4, 0x0

    .line 66
    new-instance v0, Lflipboard/gui/ServicePromptView$1;

    invoke-direct {v0, p0}, Lflipboard/gui/ServicePromptView$1;-><init>(Lflipboard/gui/ServicePromptView;)V

    invoke-virtual {p0, v0}, Lflipboard/gui/ServicePromptView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    if-eqz p1, :cond_20

    .line 73
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, p1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v5

    .line 74
    iget-boolean v0, v5, Lflipboard/objs/ConfigService;->bO:Z

    if-eqz v0, :cond_20

    iget-boolean v0, v5, Lflipboard/objs/ConfigService;->bx:Z

    if-eqz v0, :cond_20

    .line 75
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v6

    .line 77
    sget-boolean v0, Lflipboard/service/FlipboardManager;->t:Z

    if-eqz v0, :cond_2

    if-eqz v6, :cond_0

    iget-object v0, v6, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-boolean v0, v0, Lflipboard/objs/UserService;->r:Z

    if-eqz v0, :cond_2

    :cond_0
    move v2, v3

    .line 78
    :goto_0
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v7

    .line 79
    const v0, 0x7f0a02fd

    invoke-virtual {p0, v0}, Lflipboard/gui/ServicePromptView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 80
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Lflipboard/service/Account;->h()Z

    move-result v1

    if-nez v1, :cond_1f

    .line 81
    :cond_1
    invoke-virtual {p0, v4}, Lflipboard/gui/ServicePromptView;->setVisibility(I)V

    .line 83
    iget-object v1, p0, Lflipboard/gui/ServicePromptView;->b:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 84
    iget-object v1, p0, Lflipboard/gui/ServicePromptView;->c:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 85
    invoke-virtual {p0, v9}, Lflipboard/gui/ServicePromptView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 86
    invoke-virtual {v5, v2}, Lflipboard/objs/ConfigService;->a(Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v2}, Lflipboard/objs/ConfigService;->b(Z)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v8, v9}, Lflipboard/gui/ServicePromptView;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    if-eqz v6, :cond_14

    .line 88
    invoke-virtual {v6}, Lflipboard/service/Account;->j()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lflipboard/gui/ServicePromptView;->d:Lflipboard/gui/FLTextIntf;

    iget-object v8, v6, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v8, v8, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    invoke-interface {v0, v8}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, v6, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->m:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    invoke-virtual {v6}, Lflipboard/service/Account;->g()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 95
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v2, :cond_3

    iget-object v0, v5, Lflipboard/objs/ConfigService;->aF:Ljava/lang/String;

    :goto_1
    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v2, :cond_4

    iget-object v0, v5, Lflipboard/objs/ConfigService;->aG:Ljava/lang/String;

    :goto_2
    invoke-virtual {v6, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lflipboard/gui/ServicePromptView;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 96
    if-eqz v7, :cond_6

    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v2, :cond_5

    iget-object v0, v5, Lflipboard/objs/ConfigService;->aS:Ljava/lang/String;

    :goto_3
    invoke-virtual {v6, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 97
    :goto_4
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_8

    .line 98
    iget-object v2, p0, Lflipboard/gui/ServicePromptView;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 99
    iget-object v2, p0, Lflipboard/gui/ServicePromptView;->c:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 132
    :goto_5
    if-eqz v0, :cond_13

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_13

    .line 133
    iget-object v1, p0, Lflipboard/gui/ServicePromptView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 134
    iget-object v1, p0, Lflipboard/gui/ServicePromptView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    :goto_6
    return v3

    :cond_2
    move v2, v4

    .line 77
    goto/16 :goto_0

    .line 95
    :cond_3
    iget-object v0, v5, Lflipboard/objs/ConfigService;->aV:Ljava/lang/String;

    goto :goto_1

    :cond_4
    iget-object v0, v5, Lflipboard/objs/ConfigService;->aW:Ljava/lang/String;

    goto :goto_2

    .line 96
    :cond_5
    iget-object v0, v5, Lflipboard/objs/ConfigService;->bj:Ljava/lang/String;

    goto :goto_3

    :cond_6
    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v2, :cond_7

    iget-object v0, v5, Lflipboard/objs/ConfigService;->aR:Ljava/lang/String;

    :goto_7
    invoke-virtual {v6, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_7
    iget-object v0, v5, Lflipboard/objs/ConfigService;->bi:Ljava/lang/String;

    goto :goto_7

    .line 101
    :cond_8
    iget-object v0, p0, Lflipboard/gui/ServicePromptView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    move-object v0, v1

    .line 103
    goto :goto_5

    .line 105
    :cond_9
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v2, :cond_a

    iget-object v0, v5, Lflipboard/objs/ConfigService;->aH:Ljava/lang/String;

    :goto_8
    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v2, :cond_b

    iget-object v0, v5, Lflipboard/objs/ConfigService;->aI:Ljava/lang/String;

    :goto_9
    invoke-virtual {v6, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lflipboard/gui/ServicePromptView;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 106
    if-eqz v7, :cond_d

    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v2, :cond_c

    iget-object v0, v5, Lflipboard/objs/ConfigService;->aQ:Ljava/lang/String;

    :goto_a
    invoke-virtual {v6, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 107
    :goto_b
    if-eqz v0, :cond_f

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_f

    .line 108
    iget-object v6, p0, Lflipboard/gui/ServicePromptView;->c:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 109
    iget-object v6, p0, Lflipboard/gui/ServicePromptView;->c:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    :goto_c
    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v2, :cond_10

    iget-object v0, v5, Lflipboard/objs/ConfigService;->aK:Ljava/lang/String;

    :goto_d
    invoke-virtual {v6, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 116
    if-eqz v2, :cond_11

    iget-object v0, v5, Lflipboard/objs/ConfigService;->aM:Ljava/lang/String;

    .line 117
    :goto_e
    if-eqz v6, :cond_12

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_12

    if-eqz v0, :cond_12

    .line 118
    iget-object v2, p0, Lflipboard/gui/ServicePromptView;->a:Lflipboard/gui/FLButton;

    invoke-virtual {v2, v4}, Lflipboard/gui/FLButton;->setVisibility(I)V

    .line 119
    iget-object v2, p0, Lflipboard/gui/ServicePromptView;->a:Lflipboard/gui/FLButton;

    invoke-virtual {v2, v6}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v2, p0, Lflipboard/gui/ServicePromptView;->a:Lflipboard/gui/FLButton;

    new-instance v5, Lflipboard/gui/ServicePromptView$2;

    invoke-direct {v5, p0, v0}, Lflipboard/gui/ServicePromptView$2;-><init>(Lflipboard/gui/ServicePromptView;Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, v1

    goto/16 :goto_5

    .line 105
    :cond_a
    iget-object v0, v5, Lflipboard/objs/ConfigService;->aX:Ljava/lang/String;

    goto :goto_8

    :cond_b
    iget-object v0, v5, Lflipboard/objs/ConfigService;->aY:Ljava/lang/String;

    goto :goto_9

    .line 106
    :cond_c
    iget-object v0, v5, Lflipboard/objs/ConfigService;->bh:Ljava/lang/String;

    goto :goto_a

    :cond_d
    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v2, :cond_e

    iget-object v0, v5, Lflipboard/objs/ConfigService;->aP:Ljava/lang/String;

    :goto_f
    invoke-virtual {v6, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_b

    :cond_e
    iget-object v0, v5, Lflipboard/objs/ConfigService;->bg:Ljava/lang/String;

    goto :goto_f

    .line 111
    :cond_f
    iget-object v0, p0, Lflipboard/gui/ServicePromptView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_c

    .line 115
    :cond_10
    iget-object v0, v5, Lflipboard/objs/ConfigService;->ba:Ljava/lang/String;

    goto :goto_d

    .line 116
    :cond_11
    iget-object v0, v5, Lflipboard/objs/ConfigService;->bc:Ljava/lang/String;

    goto :goto_e

    .line 128
    :cond_12
    iget-object v0, p0, Lflipboard/gui/ServicePromptView;->a:Lflipboard/gui/FLButton;

    invoke-virtual {v0, v10}, Lflipboard/gui/FLButton;->setVisibility(I)V

    move-object v0, v1

    goto/16 :goto_5

    .line 136
    :cond_13
    iget-object v0, p0, Lflipboard/gui/ServicePromptView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 140
    :cond_14
    iget-object v6, p0, Lflipboard/gui/ServicePromptView;->b:Landroid/widget/TextView;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 143
    invoke-virtual {v5}, Lflipboard/objs/ConfigService;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lflipboard/gui/ServicePromptView;->d:Lflipboard/gui/FLTextIntf;

    invoke-interface {v0, v10}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 145
    invoke-virtual {v5, v2}, Lflipboard/objs/ConfigService;->a(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v2}, Lflipboard/objs/ConfigService;->b(Z)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v0, v6}, Lflipboard/gui/ServicePromptView;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v2, :cond_17

    iget-object v0, v5, Lflipboard/objs/ConfigService;->aJ:Ljava/lang/String;

    :goto_10
    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 148
    if-eqz v2, :cond_18

    iget-object v0, v5, Lflipboard/objs/ConfigService;->aL:Ljava/lang/String;

    .line 150
    :goto_11
    if-eqz v1, :cond_19

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_19

    if-eqz v0, :cond_19

    .line 151
    iget-object v6, p0, Lflipboard/gui/ServicePromptView;->a:Lflipboard/gui/FLButton;

    invoke-virtual {v6, v4}, Lflipboard/gui/FLButton;->setVisibility(I)V

    .line 152
    iget-object v6, p0, Lflipboard/gui/ServicePromptView;->a:Lflipboard/gui/FLButton;

    invoke-virtual {v6, v1}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 153
    iget-object v1, p0, Lflipboard/gui/ServicePromptView;->a:Lflipboard/gui/FLButton;

    new-instance v6, Lflipboard/gui/ServicePromptView$3;

    invoke-direct {v6, p0, v0}, Lflipboard/gui/ServicePromptView$3;-><init>(Lflipboard/gui/ServicePromptView;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    :goto_12
    sget-boolean v0, Lflipboard/service/FlipboardManager;->q:Z

    if-eqz v0, :cond_1a

    .line 180
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    .line 181
    invoke-virtual {p0}, Lflipboard/gui/ServicePromptView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0235

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 182
    new-array v2, v3, [Ljava/lang/Object;

    iget-object v0, v0, Lflipboard/model/ConfigSetting;->NYTSubscribeLinkKindle:Ljava/lang/String;

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 189
    :cond_15
    :goto_13
    if-eqz v0, :cond_1e

    .line 190
    iget-object v1, p0, Lflipboard/gui/ServicePromptView;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_16

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    :cond_16
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v0, p0, Lflipboard/gui/ServicePromptView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 147
    :cond_17
    iget-object v0, v5, Lflipboard/objs/ConfigService;->aZ:Ljava/lang/String;

    goto :goto_10

    .line 148
    :cond_18
    iget-object v0, v5, Lflipboard/objs/ConfigService;->bb:Ljava/lang/String;

    goto :goto_11

    .line 175
    :cond_19
    iget-object v0, p0, Lflipboard/gui/ServicePromptView;->a:Lflipboard/gui/FLButton;

    invoke-virtual {v0, v10}, Lflipboard/gui/FLButton;->setVisibility(I)V

    goto :goto_12

    .line 184
    :cond_1a
    if-eqz v7, :cond_1c

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v2, :cond_1b

    iget-object v0, v5, Lflipboard/objs/ConfigService;->aO:Ljava/lang/String;

    :goto_14
    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 185
    :goto_15
    if-nez v0, :cond_15

    .line 186
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v5, Lflipboard/objs/ConfigService;->bd:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_13

    .line 184
    :cond_1b
    iget-object v0, v5, Lflipboard/objs/ConfigService;->bf:Ljava/lang/String;

    goto :goto_14

    :cond_1c
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v2, :cond_1d

    iget-object v0, v5, Lflipboard/objs/ConfigService;->aN:Ljava/lang/String;

    :goto_16
    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_15

    :cond_1d
    iget-object v0, v5, Lflipboard/objs/ConfigService;->be:Ljava/lang/String;

    goto :goto_16

    .line 193
    :cond_1e
    iget-object v0, p0, Lflipboard/gui/ServicePromptView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 199
    :cond_1f
    if-eqz p2, :cond_20

    .line 202
    iget-object v1, p0, Lflipboard/gui/ServicePromptView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 203
    iget-object v1, p0, Lflipboard/gui/ServicePromptView;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 204
    iget-object v1, p0, Lflipboard/gui/ServicePromptView;->a:Lflipboard/gui/FLButton;

    invoke-virtual {v1, v10}, Lflipboard/gui/FLButton;->setVisibility(I)V

    .line 206
    invoke-virtual {p0, v4}, Lflipboard/gui/ServicePromptView;->setVisibility(I)V

    .line 207
    iget-object v1, p0, Lflipboard/gui/ServicePromptView;->d:Lflipboard/gui/FLTextIntf;

    invoke-interface {v1, v4}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 208
    iget-object v1, p0, Lflipboard/gui/ServicePromptView;->d:Lflipboard/gui/FLTextIntf;

    iget-object v2, v6, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 209
    invoke-virtual {v6}, Lflipboard/service/Account;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 210
    invoke-virtual {p0, v9}, Lflipboard/gui/ServicePromptView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iget-object v1, v6, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    :cond_20
    move v3, v4

    .line 215
    goto/16 :goto_6
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 50
    const v0, 0x7f0a0300

    invoke-virtual {p0, v0}, Lflipboard/gui/ServicePromptView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    iput-object v0, p0, Lflipboard/gui/ServicePromptView;->a:Lflipboard/gui/FLButton;

    .line 51
    const v0, 0x7f0a0301

    invoke-virtual {p0, v0}, Lflipboard/gui/ServicePromptView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflipboard/gui/ServicePromptView;->b:Landroid/widget/TextView;

    .line 52
    const v0, 0x7f0a0302

    invoke-virtual {p0, v0}, Lflipboard/gui/ServicePromptView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflipboard/gui/ServicePromptView;->c:Landroid/widget/TextView;

    .line 53
    const v0, 0x7f0a02fe

    invoke-virtual {p0, v0}, Lflipboard/gui/ServicePromptView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/ServicePromptView;->d:Lflipboard/gui/FLTextIntf;

    .line 55
    return-void
.end method
