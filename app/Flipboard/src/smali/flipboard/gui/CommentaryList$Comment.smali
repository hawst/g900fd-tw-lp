.class Lflipboard/gui/CommentaryList$Comment;
.super Lflipboard/gui/CommentaryList$AuthorDetails;
.source "CommentaryList.java"


# instance fields
.field g:Ljava/lang/String;

.field h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedSectionLink;",
            ">;"
        }
    .end annotation
.end field

.field i:J

.field final j:Ljava/lang/String;

.field k:Lflipboard/objs/CommentaryResult$Item$Commentary;

.field final l:Z


# direct methods
.method constructor <init>(Lflipboard/objs/CommentaryResult$Item$Commentary;)V
    .locals 4

    .prologue
    .line 251
    invoke-direct {p0}, Lflipboard/gui/CommentaryList$AuthorDetails;-><init>()V

    .line 252
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->a:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->a:Ljava/lang/String;

    .line 253
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->c:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->b:Ljava/lang/String;

    .line 254
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->b:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->c:Ljava/lang/String;

    .line 255
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->g:Lflipboard/objs/Image;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->g:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->d:Ljava/lang/String;

    .line 256
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->j:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->g:Ljava/lang/String;

    .line 257
    iget-wide v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->k:J

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lflipboard/gui/CommentaryList$Comment;->i:J

    .line 258
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->h:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->j:Ljava/lang/String;

    .line 259
    iput-object p1, p0, Lflipboard/gui/CommentaryList$Comment;->k:Lflipboard/objs/CommentaryResult$Item$Commentary;

    .line 261
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->o:Ljava/util/List;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->h:Ljava/util/List;

    .line 262
    iget-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->h:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 263
    iget-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 264
    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    const-string v3, "author"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 265
    iput-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->f:Lflipboard/objs/FeedSectionLink;

    .line 270
    :cond_1
    iget-boolean v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->p:Z

    iput-boolean v0, p0, Lflipboard/gui/CommentaryList$Comment;->l:Z

    .line 272
    invoke-virtual {p0}, Lflipboard/gui/CommentaryList$Comment;->a()V

    .line 273
    return-void

    .line 255
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lflipboard/objs/FeedItem;)V
    .locals 4

    .prologue
    .line 276
    invoke-direct {p0}, Lflipboard/gui/CommentaryList$AuthorDetails;-><init>()V

    .line 277
    iget-object v0, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->a:Ljava/lang/String;

    .line 278
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->b:Ljava/lang/String;

    .line 279
    iget-object v0, p1, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->c:Ljava/lang/String;

    .line 280
    iget-object v0, p1, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->d:Ljava/lang/String;

    .line 281
    iget-object v0, p1, Lflipboard/objs/FeedItem;->z:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->g:Ljava/lang/String;

    .line 282
    iget-wide v0, p1, Lflipboard/objs/FeedItem;->Z:J

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lflipboard/gui/CommentaryList$Comment;->i:J

    .line 283
    iget-object v0, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->j:Ljava/lang/String;

    .line 285
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->h:Ljava/util/List;

    .line 286
    iget-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->h:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 287
    iget-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 288
    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    const-string v3, "author"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 289
    iput-object v0, p0, Lflipboard/gui/CommentaryList$Comment;->f:Lflipboard/objs/FeedSectionLink;

    .line 296
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/CommentaryList$Comment;->l:Z

    .line 298
    invoke-virtual {p0}, Lflipboard/gui/CommentaryList$Comment;->a()V

    .line 299
    return-void

    .line 280
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
