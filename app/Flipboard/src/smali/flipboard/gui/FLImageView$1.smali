.class Lflipboard/gui/FLImageView$1;
.super Lflipboard/io/Download$Observer;
.source "FLImageView.java"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lflipboard/gui/FLImageView;


# direct methods
.method constructor <init>(Lflipboard/gui/FLImageView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    iput-object p2, p0, Lflipboard/gui/FLImageView$1;->a:Ljava/lang/String;

    invoke-direct {p0}, Lflipboard/io/Download$Observer;-><init>()V

    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 432
    iget-object v0, p0, Lflipboard/gui/FLImageView$1;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 433
    sget-object v0, Lflipboard/gui/FLImageView;->d:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    invoke-static {v1}, Lflipboard/gui/FLImageView;->g(Lflipboard/gui/FLImageView;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x1

    iget-object v2, p0, Lflipboard/gui/FLImageView$1;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 434
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/FLImageView$1$2;

    invoke-direct {v1, p0}, Lflipboard/gui/FLImageView$1$2;-><init>(Lflipboard/gui/FLImageView$1;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 444
    :cond_0
    :goto_0
    return-void

    .line 442
    :cond_1
    iget-object v1, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    iget-object v0, v1, Lflipboard/gui/FLImageView;->a:Lflipboard/gui/FLImageView$Listener;

    if-eqz v0, :cond_2

    iget-object v0, v1, Lflipboard/gui/FLImageView;->a:Lflipboard/gui/FLImageView$Listener;

    invoke-interface {v0}, Lflipboard/gui/FLImageView$Listener;->a()V

    :cond_2
    iget-boolean v0, v1, Lflipboard/gui/FLImageView;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d00ca

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    :goto_1
    iput-boolean v4, v1, Lflipboard/gui/FLImageView;->b:Z

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0218

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 360
    check-cast p1, Lflipboard/io/Download;

    check-cast p2, Lflipboard/io/Download$Status;

    check-cast p3, Lflipboard/io/Download$Data;

    iget-object v2, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    iget-object v3, v2, Lflipboard/gui/FLImageView;->a:Lflipboard/gui/FLImageView$Listener;

    if-eqz v3, :cond_0

    iget-object v2, v2, Lflipboard/gui/FLImageView;->a:Lflipboard/gui/FLImageView$Listener;

    invoke-interface {v2, p2}, Lflipboard/gui/FLImageView$Listener;->a(Lflipboard/io/Download$Status;)V

    :cond_0
    sget-object v2, Lflipboard/gui/FLImageView$6;->a:[I

    invoke-virtual {p2}, Lflipboard/io/Download$Status;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    iget-object v2, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {p2}, Lflipboard/io/Download$Status;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0, v1}, Lflipboard/gui/FLImageView;->a(Lflipboard/gui/FLImageView;Ljava/lang/String;ZZ)V

    :cond_1
    :goto_0
    return-void

    :pswitch_0
    iget-object v2, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    const-string v3, "timeout"

    invoke-static {v2, v3, v1, v0}, Lflipboard/gui/FLImageView;->a(Lflipboard/gui/FLImageView;Ljava/lang/String;ZZ)V

    invoke-direct {p0}, Lflipboard/gui/FLImageView$1;->a()V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    const-string v3, "paused"

    invoke-static {v2, v3, v1, v0}, Lflipboard/gui/FLImageView;->a(Lflipboard/gui/FLImageView;Ljava/lang/String;ZZ)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    const-string v3, "failed"

    invoke-static {v2, v3, v1, v0}, Lflipboard/gui/FLImageView;->a(Lflipboard/gui/FLImageView;Ljava/lang/String;ZZ)V

    invoke-direct {p0}, Lflipboard/gui/FLImageView$1;->a()V

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p1, p0}, Lflipboard/io/Download;->b(Lflipboard/io/Download$Observer;)V

    iget-object v3, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    invoke-static {v3}, Lflipboard/gui/FLImageView;->a(Lflipboard/gui/FLImageView;)Lflipboard/io/Download$Observer;

    move-result-object v3

    if-eq v3, p0, :cond_2

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    :try_start_1
    iget-object v3, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    invoke-static {v3}, Lflipboard/gui/FLImageView;->b(Lflipboard/gui/FLImageView;)Lflipboard/io/Download$Observer;

    iget-object v3, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    invoke-static {v3}, Lflipboard/gui/FLImageView;->c(Lflipboard/gui/FLImageView;)Lflipboard/io/Download;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p3}, Lflipboard/io/Download$Data;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    invoke-static {v3}, Lflipboard/gui/FLImageView;->d(Lflipboard/gui/FLImageView;)Z

    move-result v3

    if-eqz v3, :cond_3

    if-eqz v2, :cond_3

    const-string v3, "image/gif"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v0, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    const-string v2, "possibly animated gif"

    invoke-static {v0, v6, v2, v1}, Lflipboard/gui/FLImageView;->a(Lflipboard/gui/FLImageView;Lflipboard/io/BitmapManager$Handle;Ljava/lang/String;Z)V

    iget-object v0, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    iget-object v0, v0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget-object v1, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getOriginalSize()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lflipboard/gui/FLBitmapView;->a(Lflipboard/io/Download;Landroid/graphics/Point;)V

    goto :goto_0

    :cond_3
    if-eqz v2, :cond_4

    const-string v3, "image/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    sget-object v3, Lflipboard/gui/FLImageView;->d:Lflipboard/util/Log;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v1

    aput-object p3, v3, v0

    iget-object v1, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "bad ctype: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v6, v2, v0}, Lflipboard/gui/FLImageView;->a(Lflipboard/gui/FLImageView;Lflipboard/io/BitmapManager$Handle;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p3}, Lflipboard/io/Download$Data;->a()J

    move-result-wide v2

    const-wide/32 v4, 0xa00000

    cmp-long v4, v2, v4

    if-lez v4, :cond_5

    iget-object v1, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "too big: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v6, v2, v0}, Lflipboard/gui/FLImageView;->a(Lflipboard/gui/FLImageView;Lflipboard/io/BitmapManager$Handle;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_5
    iget-object v2, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    sget-object v3, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    iget-object v4, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    iget-object v4, v4, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v4}, Lflipboard/gui/FLBitmapView;->getScaling()Z

    move-result v4

    if-nez v4, :cond_6

    :goto_1
    invoke-virtual {v3, p1, v0}, Lflipboard/io/BitmapManager;->a(Lflipboard/io/Download;Z)Lflipboard/io/BitmapManager$Handle;

    move-result-object v0

    const-string v3, "ready"

    invoke-static {v2, v0, v3, v1}, Lflipboard/gui/FLImageView;->a(Lflipboard/gui/FLImageView;Lflipboard/io/BitmapManager$Handle;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_1

    :pswitch_4
    iget-object v1, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    const-string v2, "not found"

    invoke-static {v1, v6, v2, v0}, Lflipboard/gui/FLImageView;->a(Lflipboard/gui/FLImageView;Lflipboard/io/BitmapManager$Handle;Ljava/lang/String;Z)V

    invoke-direct {p0}, Lflipboard/gui/FLImageView$1;->a()V

    goto/16 :goto_0

    :pswitch_5
    iget-object v2, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    invoke-static {v2}, Lflipboard/gui/FLImageView;->e(Lflipboard/gui/FLImageView;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    const-string v3, "loading"

    invoke-static {v2, v3, v0, v1}, Lflipboard/gui/FLImageView;->a(Lflipboard/gui/FLImageView;Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p0, Lflipboard/gui/FLImageView$1;->b:Lflipboard/gui/FLImageView;

    invoke-static {v0}, Lflipboard/gui/FLImageView;->f(Lflipboard/gui/FLImageView;)Lflipboard/gui/FLBusyView;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v0, p1, Lflipboard/io/Download;->i:F

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/FLImageView$1$1;

    invoke-direct {v2, p0, v0}, Lflipboard/gui/FLImageView$1$1;-><init>(Lflipboard/gui/FLImageView$1;F)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
