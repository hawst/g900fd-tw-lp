.class Lflipboard/gui/SocialCardMainItem$1;
.super Ljava/lang/Object;
.source "SocialCardMainItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/SocialCardMainItem;


# direct methods
.method constructor <init>(Lflipboard/gui/SocialCardMainItem;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lflipboard/gui/SocialCardMainItem$1;->a:Lflipboard/gui/SocialCardMainItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    .line 62
    iget-object v0, p0, Lflipboard/gui/SocialCardMainItem$1;->a:Lflipboard/gui/SocialCardMainItem;

    iget-object v0, v0, Lflipboard/gui/SocialCardMainItem;->b:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lflipboard/gui/SocialCardMainItem$1;->a:Lflipboard/gui/SocialCardMainItem;

    iget-object v1, p0, Lflipboard/gui/SocialCardMainItem$1;->a:Lflipboard/gui/SocialCardMainItem;

    iget-object v1, v1, Lflipboard/gui/SocialCardMainItem;->b:Lflipboard/objs/FeedItem;

    new-instance v2, Lflipboard/service/Section;

    invoke-direct {v2, v1}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedItem;)V

    invoke-virtual {v0}, Lflipboard/gui/SocialCardMainItem;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "source"

    const-string v6, "authorPopover"

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "originSectionIdentifier"

    iget-object v0, v0, Lflipboard/gui/SocialCardMainItem;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v4, Lflipboard/activities/DetailActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "sid"

    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "extra_current_item"

    iget-object v1, v1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v4, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v2}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    :cond_0
    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 65
    :cond_1
    return-void
.end method
