.class public Lflipboard/gui/SectionTitleView;
.super Landroid/view/ViewGroup;
.source "SectionTitleView.java"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private final d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/SectionTitleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    invoke-virtual {p0}, Lflipboard/gui/SectionTitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090061

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/SectionTitleView;->d:I

    .line 39
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 45
    const v0, 0x7f0a016c

    invoke-virtual {p0, v0}, Lflipboard/gui/SectionTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    .line 46
    const v0, 0x7f0a02e1

    invoke-virtual {p0, v0}, Lflipboard/gui/SectionTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/SectionTitleView;->b:Landroid/view/View;

    .line 47
    const v0, 0x7f0a02e2

    invoke-virtual {p0, v0}, Lflipboard/gui/SectionTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/SectionTitleView;->c:Landroid/view/View;

    .line 48
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 98
    invoke-virtual {p0}, Lflipboard/gui/SectionTitleView;->getPaddingLeft()I

    move-result v1

    .line 99
    sub-int v0, p4, p2

    invoke-virtual {p0}, Lflipboard/gui/SectionTitleView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    .line 100
    sub-int v2, p5, p3

    .line 101
    iget-object v3, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    .line 103
    iget-object v3, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 104
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 105
    iget-object v3, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    iget-object v4, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    sub-int v4, v0, v4

    iget-object v5, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v3, v4, v2, v0, v5}, Landroid/view/View;->layout(IIII)V

    .line 106
    iget-object v2, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    .line 124
    :goto_0
    iget-object v2, p0, Lflipboard/gui/SectionTitleView;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    .line 125
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 126
    iget-object v1, p0, Lflipboard/gui/SectionTitleView;->c:Landroid/view/View;

    iget-object v2, p0, Lflipboard/gui/SectionTitleView;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int v2, v0, v2

    iget-object v3, p0, Lflipboard/gui/SectionTitleView;->c:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v3, p5, v3

    invoke-virtual {v1, v2, v3, v0, p5}, Landroid/view/View;->layout(IIII)V

    .line 131
    :cond_0
    :goto_1
    return-void

    .line 108
    :cond_1
    iget-object v3, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    iget-object v4, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    iget-object v5, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v3, v1, v2, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 109
    iget-object v2, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    .line 111
    goto :goto_0

    .line 113
    :cond_2
    iget-object v3, p0, Lflipboard/gui/SectionTitleView;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 114
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 115
    iget-object v3, p0, Lflipboard/gui/SectionTitleView;->b:Landroid/view/View;

    iget-object v4, p0, Lflipboard/gui/SectionTitleView;->b:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    sub-int v4, v0, v4

    iget-object v5, p0, Lflipboard/gui/SectionTitleView;->b:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v3, v4, v2, v0, v5}, Landroid/view/View;->layout(IIII)V

    .line 116
    iget-object v2, p0, Lflipboard/gui/SectionTitleView;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    goto :goto_0

    .line 118
    :cond_3
    iget-object v3, p0, Lflipboard/gui/SectionTitleView;->b:Landroid/view/View;

    iget-object v4, p0, Lflipboard/gui/SectionTitleView;->b:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    iget-object v5, p0, Lflipboard/gui/SectionTitleView;->b:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v3, v1, v2, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 119
    iget-object v2, p0, Lflipboard/gui/SectionTitleView;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    goto/16 :goto_0

    .line 128
    :cond_4
    iget-object v0, p0, Lflipboard/gui/SectionTitleView;->c:Landroid/view/View;

    iget-object v2, p0, Lflipboard/gui/SectionTitleView;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    sub-int v2, p5, v2

    iget-object v3, p0, Lflipboard/gui/SectionTitleView;->c:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {v0, v1, v2, v3, p5}, Landroid/view/View;->layout(IIII)V

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 13

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    const/high16 v11, -0x80000000

    const/4 v10, 0x0

    .line 54
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 55
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 56
    iget-object v0, p0, Lflipboard/gui/SectionTitleView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v3, 0x8

    if-eq v0, v3, :cond_0

    .line 57
    iget-object v0, p0, Lflipboard/gui/SectionTitleView;->c:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 59
    :cond_0
    iget-object v0, p0, Lflipboard/gui/SectionTitleView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/SectionTitleView;->getPaddingLeft()I

    move-result v3

    add-int/2addr v0, v3

    invoke-virtual {p0}, Lflipboard/gui/SectionTitleView;->getPaddingRight()I

    move-result v3

    add-int v4, v0, v3

    .line 60
    iget-object v0, p0, Lflipboard/gui/SectionTitleView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 61
    sub-int v5, v1, v4

    .line 62
    iget-object v0, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 64
    iget-object v0, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    invoke-static {v10, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v0, v6, p2}, Landroid/view/View;->measure(II)V

    .line 66
    iget-object v0, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    if-le v0, v5, :cond_1

    .line 68
    iget-object v0, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 69
    invoke-interface {v0}, Lflipboard/gui/FLTextIntf;->getTextSize()F

    move-result v6

    .line 72
    float-to-double v6, v6

    int-to-double v8, v5

    mul-double/2addr v6, v8

    iget-object v8, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    int-to-double v8, v8

    div-double/2addr v6, v8

    double-to-int v6, v6

    .line 73
    iget v7, p0, Lflipboard/gui/SectionTitleView;->d:I

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-interface {v0, v10, v6}, Lflipboard/gui/FLTextIntf;->a(II)V

    .line 76
    iget-object v0, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    invoke-static {v5, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v0, v5, p2}, Landroid/view/View;->measure(II)V

    .line 78
    :cond_1
    iget-object v0, p0, Lflipboard/gui/SectionTitleView;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v4

    .line 86
    :goto_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    if-ne v4, v12, :cond_2

    move v0, v1

    .line 89
    :cond_2
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    if-ne v1, v12, :cond_4

    move v1, v2

    .line 92
    :goto_1
    invoke-virtual {p0, v0, v1}, Lflipboard/gui/SectionTitleView;->setMeasuredDimension(II)V

    .line 93
    return-void

    .line 82
    :cond_3
    iget-object v0, p0, Lflipboard/gui/SectionTitleView;->b:Landroid/view/View;

    invoke-static {v5, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v0, v5, p2}, Landroid/view/View;->measure(II)V

    .line 83
    iget-object v0, p0, Lflipboard/gui/SectionTitleView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v4

    goto :goto_0

    :cond_4
    move v1, v3

    goto :goto_1
.end method
