.class Lflipboard/gui/FLStaticTextView$FixedSegment;
.super Lflipboard/gui/FLStaticTextView$Segment;
.source "FLStaticTextView.java"


# instance fields
.field final a:Ljava/lang/String;

.field final synthetic b:Lflipboard/gui/FLStaticTextView;


# direct methods
.method constructor <init>(Lflipboard/gui/FLStaticTextView;Ljava/lang/String;FFF)V
    .locals 7

    .prologue
    .line 437
    iput-object p1, p0, Lflipboard/gui/FLStaticTextView$FixedSegment;->b:Lflipboard/gui/FLStaticTextView;

    .line 438
    const/4 v2, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lflipboard/gui/FLStaticTextView$Segment;-><init>(Lflipboard/gui/FLStaticTextView;IIFFF)V

    .line 439
    iput-object p2, p0, Lflipboard/gui/FLStaticTextView$FixedSegment;->a:Ljava/lang/String;

    .line 440
    return-void
.end method


# virtual methods
.method final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView$FixedSegment;->a:Ljava/lang/String;

    return-object v0
.end method

.method final a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 443
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView$FixedSegment;->a:Ljava/lang/String;

    iget v1, p0, Lflipboard/gui/FLStaticTextView$FixedSegment;->e:F

    iget v2, p0, Lflipboard/gui/FLStaticTextView$FixedSegment;->f:F

    iget-object v3, p0, Lflipboard/gui/FLStaticTextView$FixedSegment;->b:Lflipboard/gui/FLStaticTextView;

    invoke-static {v3}, Lflipboard/gui/FLStaticTextView;->c(Lflipboard/gui/FLStaticTextView;)Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 444
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 451
    const-string v0, "%f,%f,%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lflipboard/gui/FLStaticTextView$FixedSegment;->e:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lflipboard/gui/FLStaticTextView$FixedSegment;->f:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lflipboard/gui/FLStaticTextView$FixedSegment;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
