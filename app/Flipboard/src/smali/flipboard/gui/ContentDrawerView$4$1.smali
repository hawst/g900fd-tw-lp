.class Lflipboard/gui/ContentDrawerView$4$1;
.super Ljava/lang/Object;
.source "ContentDrawerView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/objs/SectionListResult;

.field final synthetic b:Lflipboard/gui/ContentDrawerView$4;


# direct methods
.method constructor <init>(Lflipboard/gui/ContentDrawerView$4;Lflipboard/objs/SectionListResult;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lflipboard/gui/ContentDrawerView$4$1;->b:Lflipboard/gui/ContentDrawerView$4;

    iput-object p2, p0, Lflipboard/gui/ContentDrawerView$4$1;->a:Lflipboard/objs/SectionListResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 240
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView$4$1;->a:Lflipboard/objs/SectionListResult;

    invoke-virtual {v0}, Lflipboard/objs/SectionListResult;->a()Ljava/util/List;

    move-result-object v0

    .line 241
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 242
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    .line 244
    instance-of v3, v0, Lflipboard/objs/SectionListItem;

    if-eqz v3, :cond_0

    .line 246
    new-instance v3, Lflipboard/objs/ConfigSection;

    invoke-direct {v3}, Lflipboard/objs/ConfigSection;-><init>()V

    .line 250
    check-cast v0, Lflipboard/objs/SectionListItem;

    iget-object v4, v0, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    iput-object v4, v3, Lflipboard/objs/ConfigSection;->p:Ljava/lang/Object;

    iget-object v4, v0, Lflipboard/objs/SectionListItem;->bP:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/ConfigSection;->bP:Ljava/lang/String;

    iget-object v4, v0, Lflipboard/objs/SectionListItem;->t:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/ConfigSection;->t:Ljava/lang/String;

    iget-object v4, v0, Lflipboard/objs/SectionListItem;->bQ:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/ConfigSection;->bQ:Ljava/lang/String;

    iget-object v4, v0, Lflipboard/objs/SectionListItem;->j:Lflipboard/objs/ConfigBrick;

    iput-object v4, v3, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget-object v0, v0, Lflipboard/objs/SectionListItem;->k:Lflipboard/objs/Author;

    iput-object v0, v3, Lflipboard/objs/ConfigSection;->l:Lflipboard/objs/Author;

    .line 251
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 253
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 255
    sget-object v0, Lflipboard/gui/ContentDrawerView;->a:Lflipboard/util/Log;

    .line 256
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView$4$1;->b:Lflipboard/gui/ContentDrawerView$4;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView$4;->a:Lflipboard/gui/ContentDrawerView;

    invoke-virtual {v0}, Lflipboard/gui/ContentDrawerView;->c()V

    .line 260
    :goto_1
    return-void

    .line 258
    :cond_2
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView$4$1;->b:Lflipboard/gui/ContentDrawerView$4;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView$4;->a:Lflipboard/gui/ContentDrawerView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-static {v1}, Lflipboard/service/ContentDrawerHandler;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(Ljava/util/List;)V

    goto :goto_1
.end method
