.class abstract Lflipboard/gui/FLSliderHider$AbsSlider;
.super Lflipboard/gui/FLSliderHider$Slider;
.source "FLSliderHider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lflipboard/gui/FLSliderHider$Slider;"
    }
.end annotation


# static fields
.field static final a:Landroid/view/animation/Interpolator;


# instance fields
.field b:Landroid/view/View;

.field final c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field final d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field e:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field f:Landroid/view/animation/Interpolator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 245
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lflipboard/gui/FLSliderHider$AbsSlider;->a:Landroid/view/animation/Interpolator;

    return-void
.end method

.method private constructor <init>(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;Lflipboard/gui/FLSliderHider$ScrollTrackingType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TT;TT;",
            "Lflipboard/gui/FLSliderHider$ScrollTrackingType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 255
    invoke-direct {p0, p4}, Lflipboard/gui/FLSliderHider$Slider;-><init>(Lflipboard/gui/FLSliderHider$ScrollTrackingType;)V

    .line 256
    iput-object p1, p0, Lflipboard/gui/FLSliderHider$AbsSlider;->b:Landroid/view/View;

    .line 257
    iput-object p2, p0, Lflipboard/gui/FLSliderHider$AbsSlider;->c:Ljava/lang/Object;

    .line 258
    iput-object p3, p0, Lflipboard/gui/FLSliderHider$AbsSlider;->d:Ljava/lang/Object;

    .line 259
    iput-object p2, p0, Lflipboard/gui/FLSliderHider$AbsSlider;->e:Ljava/lang/Object;

    .line 260
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;Lflipboard/gui/FLSliderHider$ScrollTrackingType;B)V
    .locals 0

    .prologue
    .line 243
    invoke-direct {p0, p1, p2, p3, p4}, Lflipboard/gui/FLSliderHider$AbsSlider;-><init>(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;Lflipboard/gui/FLSliderHider$ScrollTrackingType;)V

    return-void
.end method
