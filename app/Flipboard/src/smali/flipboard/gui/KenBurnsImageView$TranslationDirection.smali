.class final enum Lflipboard/gui/KenBurnsImageView$TranslationDirection;
.super Ljava/lang/Enum;
.source "KenBurnsImageView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/KenBurnsImageView$TranslationDirection;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/KenBurnsImageView$TranslationDirection;

.field public static final enum b:Lflipboard/gui/KenBurnsImageView$TranslationDirection;

.field private static final synthetic c:[Lflipboard/gui/KenBurnsImageView$TranslationDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 98
    new-instance v0, Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/KenBurnsImageView$TranslationDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/KenBurnsImageView$TranslationDirection;->a:Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    new-instance v0, Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    const-string v1, "OPPOSITE"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/KenBurnsImageView$TranslationDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/KenBurnsImageView$TranslationDirection;->b:Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    sget-object v1, Lflipboard/gui/KenBurnsImageView$TranslationDirection;->a:Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/KenBurnsImageView$TranslationDirection;->b:Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    aput-object v1, v0, v3

    sput-object v0, Lflipboard/gui/KenBurnsImageView$TranslationDirection;->c:[Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/KenBurnsImageView$TranslationDirection;
    .locals 1

    .prologue
    .line 98
    const-class v0, Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/KenBurnsImageView$TranslationDirection;
    .locals 1

    .prologue
    .line 98
    sget-object v0, Lflipboard/gui/KenBurnsImageView$TranslationDirection;->c:[Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    invoke-virtual {v0}, [Lflipboard/gui/KenBurnsImageView$TranslationDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    return-object v0
.end method
