.class public Lflipboard/gui/FLSliderHider$TranslateSlider;
.super Lflipboard/gui/FLSliderHider$AbsSlider;
.source "FLSliderHider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/gui/FLSliderHider$AbsSlider",
        "<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/View;FLflipboard/gui/FLSliderHider$ScrollTrackingType;)V
    .locals 6

    .prologue
    .line 302
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lflipboard/gui/FLSliderHider$AbsSlider;-><init>(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;Lflipboard/gui/FLSliderHider$ScrollTrackingType;B)V

    .line 303
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 4

    .prologue
    .line 307
    iget-object v0, p0, Lflipboard/gui/FLSliderHider$AbsSlider;->f:Landroid/view/animation/Interpolator;

    if-nez v0, :cond_0

    sget-object v0, Lflipboard/gui/FLSliderHider$AbsSlider;->a:Landroid/view/animation/Interpolator;

    :goto_0
    invoke-interface {v0, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    .line 308
    iget-object v0, p0, Lflipboard/gui/FLSliderHider$TranslateSlider;->c:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget-object v0, p0, Lflipboard/gui/FLSliderHider$TranslateSlider;->d:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    iget-object v0, p0, Lflipboard/gui/FLSliderHider$TranslateSlider;->c:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sub-float v0, v3, v0

    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    .line 309
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/FLSliderHider$TranslateSlider;->e:Ljava/lang/Object;

    .line 310
    iget-object v1, p0, Lflipboard/gui/FLSliderHider$TranslateSlider;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 311
    return-void

    .line 307
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLSliderHider$AbsSlider;->f:Landroid/view/animation/Interpolator;

    goto :goto_0
.end method
