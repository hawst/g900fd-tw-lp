.class Lflipboard/gui/FLImageButton$1;
.super Ljava/lang/Object;
.source "FLImageButton.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/view/ViewParent;

.field final synthetic b:Landroid/widget/ImageButton;

.field final synthetic c:I

.field final synthetic d:Lflipboard/gui/FLImageButton;


# direct methods
.method constructor <init>(Lflipboard/gui/FLImageButton;Landroid/view/ViewParent;Landroid/widget/ImageButton;I)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lflipboard/gui/FLImageButton$1;->d:Lflipboard/gui/FLImageButton;

    iput-object p2, p0, Lflipboard/gui/FLImageButton$1;->a:Landroid/view/ViewParent;

    iput-object p3, p0, Lflipboard/gui/FLImageButton$1;->b:Landroid/widget/ImageButton;

    iput p4, p0, Lflipboard/gui/FLImageButton$1;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, Lflipboard/gui/FLImageButton$1;->a:Landroid/view/ViewParent;

    iget-object v1, p0, Lflipboard/gui/FLImageButton$1;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 54
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 55
    iget-object v1, p0, Lflipboard/gui/FLImageButton$1;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->getHitRect(Landroid/graphics/Rect;)V

    .line 56
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lflipboard/gui/FLImageButton$1;->c:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 57
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lflipboard/gui/FLImageButton$1;->c:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 58
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lflipboard/gui/FLImageButton$1;->c:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 59
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Lflipboard/gui/FLImageButton$1;->c:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 60
    new-instance v1, Landroid/view/TouchDelegate;

    iget-object v2, p0, Lflipboard/gui/FLImageButton$1;->b:Landroid/widget/ImageButton;

    invoke-direct {v1, v0, v2}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    .line 61
    iget-object v0, p0, Lflipboard/gui/FLImageButton$1;->a:Landroid/view/ViewParent;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 63
    :cond_0
    return-void
.end method
