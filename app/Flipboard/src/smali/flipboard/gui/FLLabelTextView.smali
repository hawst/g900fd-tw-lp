.class public Lflipboard/gui/FLLabelTextView;
.super Landroid/view/View;
.source "FLLabelTextView.java"

# interfaces
.implements Lflipboard/gui/FLTextIntf;
.implements Lflipboard/gui/FLViewIntf;


# static fields
.field static final a:Lflipboard/util/Log;

.field private static final b:Lflipboard/util/Log;


# instance fields
.field private final c:Landroid/text/TextPaint;

.field private d:I

.field private final e:F

.field private f:Ljava/lang/CharSequence;

.field private g:F

.field private h:I

.field private i:F

.field private j:F

.field private k:I

.field private l:Z

.field private m:I

.field private n:[C

.field private o:I

.field private p:I

.field private q:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-string v0, "text"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/FLLabelTextView;->b:Lflipboard/util/Log;

    .line 238
    const-string v0, "measure"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/FLLabelTextView;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/FLLabelTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 60
    const v0, 0x7f0e0007

    invoke-direct {p0, p1, p2, v0}, Lflipboard/gui/FLLabelTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 17
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 64
    invoke-direct/range {p0 .. p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lflipboard/gui/FLLabelTextView;->l:Z

    .line 66
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    .line 71
    new-instance v3, Landroid/text/TextPaint;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/text/TextPaint;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    .line 72
    const/16 v11, 0xe

    .line 73
    const/high16 v10, -0x1000000

    .line 74
    const/4 v9, 0x0

    .line 75
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 76
    const/4 v5, 0x0

    .line 77
    const-string v4, ""

    .line 78
    const/16 v3, 0x33

    .line 80
    move-object/from16 v0, p0

    iget-object v12, v0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    invoke-static {v12}, Lflipboard/util/AndroidUtil;->a(Landroid/graphics/Paint;)V

    .line 82
    const/4 v12, 0x2

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v12, v13}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    .line 87
    sget-object v12, Lflipboard/app/R$styleable;->FLLabelTextView:[I

    const v13, 0x7f0e0007

    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v2, v0, v12, v1, v13}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v12

    .line 88
    invoke-virtual {v12}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v13

    .line 89
    const/4 v2, 0x0

    move/from16 v16, v2

    move v2, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move v10, v11

    move/from16 v11, v16

    :goto_0
    if-ge v11, v13, :cond_0

    .line 90
    invoke-virtual {v12, v11}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v14

    .line 92
    packed-switch v14, :pswitch_data_0

    .line 89
    :goto_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 94
    :pswitch_0
    invoke-virtual {v12, v14}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_1

    .line 98
    :pswitch_1
    invoke-virtual {v12, v14, v10}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v10

    goto :goto_1

    .line 102
    :pswitch_2
    const/16 v4, 0x9

    invoke-virtual {v12, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 106
    :pswitch_3
    const/4 v2, -0x1

    invoke-virtual {v12, v14, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    goto :goto_1

    .line 110
    :pswitch_4
    const/4 v9, 0x0

    invoke-virtual {v12, v14, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v9

    goto :goto_1

    .line 114
    :pswitch_5
    const/4 v8, 0x0

    invoke-virtual {v12, v14, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v8

    goto :goto_1

    .line 117
    :pswitch_6
    const/4 v7, 0x0

    invoke-virtual {v12, v14, v7}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v7

    goto :goto_1

    .line 120
    :pswitch_7
    const/4 v6, 0x0

    invoke-virtual {v12, v14, v6}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v6

    goto :goto_1

    .line 123
    :pswitch_8
    const/4 v5, 0x0

    invoke-virtual {v12, v14, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v5

    goto :goto_1

    .line 126
    :pswitch_9
    const/4 v15, 0x0

    invoke-virtual {v12, v14, v15}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v14

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lflipboard/gui/FLLabelTextView;->l:Z

    goto :goto_1

    .line 130
    :cond_0
    invoke-virtual {v12}, Landroid/content/res/TypedArray;->recycle()V

    .line 135
    if-nez v4, :cond_2

    .line 136
    const-string v4, "http://schemas.android.com/apk/res/android"

    const-string v11, "textStyle"

    move-object/from16 v0, p2

    invoke-interface {v0, v4, v11}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 137
    if-eqz v4, :cond_4

    const-string v11, "0x1"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    const-string v11, "0x3"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 138
    :cond_1
    const-string v4, "bold"

    .line 147
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iput v10, v0, Lflipboard/gui/FLLabelTextView;->d:I

    .line 148
    move-object/from16 v0, p0

    iput v2, v0, Lflipboard/gui/FLLabelTextView;->k:I

    .line 150
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    sget-object v11, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v11, v4}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 151
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    int-to-float v4, v10

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 152
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lflipboard/gui/FLLabelTextView;->setTextColor(I)V

    .line 154
    if-eqz v8, :cond_3

    .line 155
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    invoke-virtual {v2, v5, v7, v6, v8}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 158
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v2

    .line 160
    iget v4, v2, Landroid/graphics/Paint$FontMetrics;->bottom:F

    iget v2, v2, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float v2, v4, v2

    move-object/from16 v0, p0

    iput v2, v0, Lflipboard/gui/FLLabelTextView;->e:F

    .line 162
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    return-void

    .line 140
    :cond_4
    const-string v4, "normal"

    goto :goto_2

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_2
    .end packed-switch
.end method

.method private a()V
    .locals 2

    .prologue
    .line 227
    iget v0, p0, Lflipboard/gui/FLLabelTextView;->g:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 228
    iget-object v0, p0, Lflipboard/gui/FLLabelTextView;->f:Ljava/lang/CharSequence;

    iget-object v1, p0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    invoke-static {v0, v1}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v0

    iput v0, p0, Lflipboard/gui/FLLabelTextView;->g:F

    .line 229
    iget v0, p0, Lflipboard/gui/FLLabelTextView;->o:I

    iput v0, p0, Lflipboard/gui/FLLabelTextView;->h:I

    .line 230
    iget v0, p0, Lflipboard/gui/FLLabelTextView;->g:F

    iput v0, p0, Lflipboard/gui/FLLabelTextView;->i:F

    .line 232
    :cond_0
    return-void
.end method

.method private b(II)V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 271
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 272
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 273
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 274
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 276
    sget-object v4, Lflipboard/gui/FLLabelTextView;->b:Lflipboard/util/Log;

    iget-boolean v4, v4, Lflipboard/util/Log;->f:Z

    if-eqz v4, :cond_0

    .line 277
    sget-object v4, Lflipboard/gui/FLLabelTextView;->b:Lflipboard/util/Log;

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-static {v2}, Lflipboard/util/AndroidUtil;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v11

    const/4 v5, 0x4

    invoke-static {v3}, Lflipboard/util/AndroidUtil;->a(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x5

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x41

    invoke-static {v6, v7}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 280
    :cond_0
    invoke-direct {p0}, Lflipboard/gui/FLLabelTextView;->a()V

    .line 282
    sparse-switch v2, :sswitch_data_0

    .line 292
    :goto_0
    :sswitch_0
    sparse-switch v3, :sswitch_data_1

    .line 302
    :goto_1
    :sswitch_1
    sget-object v2, Lflipboard/gui/FLLabelTextView;->b:Lflipboard/util/Log;

    new-array v2, v11, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v10

    .line 303
    invoke-virtual {p0, v1, v0}, Lflipboard/gui/FLLabelTextView;->setMeasuredDimension(II)V

    .line 304
    return-void

    .line 286
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingLeft()I

    move-result v2

    iget v4, p0, Lflipboard/gui/FLLabelTextView;->g:F

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    add-int/2addr v2, v4

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingRight()I

    move-result v4

    add-int/2addr v2, v4

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_0

    .line 289
    :sswitch_3
    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingTop()I

    move-result v1

    iget v2, p0, Lflipboard/gui/FLLabelTextView;->g:F

    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v2, v4

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    .line 296
    :sswitch_4
    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingTop()I

    move-result v2

    iget v3, p0, Lflipboard/gui/FLLabelTextView;->e:F

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_1

    .line 299
    :sswitch_5
    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingTop()I

    move-result v0

    iget v2, p0, Lflipboard/gui/FLLabelTextView;->e:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_1

    .line 282
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_2
        0x0 -> :sswitch_3
        0x40000000 -> :sswitch_0
    .end sparse-switch

    .line 292
    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_4
        0x0 -> :sswitch_5
        0x40000000 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 217
    iget-object v0, p0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    int-to-float v1, p2

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {p1, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 218
    iput p2, p0, Lflipboard/gui/FLLabelTextView;->d:I

    .line 219
    return-void
.end method

.method public final a(ZI)V
    .locals 0

    .prologue
    .line 441
    return-void
.end method

.method public getText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lflipboard/gui/FLLabelTextView;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTextColor()I
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lflipboard/gui/FLLabelTextView;->m:I

    return v0
.end method

.method public getTextSize()F
    .locals 1

    .prologue
    .line 222
    iget v0, p0, Lflipboard/gui/FLLabelTextView;->d:I

    int-to-float v0, v0

    return v0
.end method

.method protected declared-synchronized onDetachedFromWindow()V
    .locals 6

    .prologue
    const/4 v1, 0x2

    .line 263
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lflipboard/gui/FLLabelTextView;->p:I

    if-le v0, v1, :cond_0

    .line 264
    sget-object v0, Lflipboard/gui/FLLabelTextView;->a:Lflipboard/util/Log;

    const-string v1, "measured %d times, %fms: 0%x, \"%s\""

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lflipboard/gui/FLLabelTextView;->p:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lflipboard/gui/FLLabelTextView;->q:J

    long-to-float v4, v4

    const v5, 0x49742400    # 1000000.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x41

    invoke-static {v4, v5}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 266
    :cond_0
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    monitor-exit p0

    return-void

    .line 263
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 314
    sget-object v0, Lflipboard/gui/FLLabelTextView;->b:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_0

    .line 315
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 316
    const v0, 0x2505e020

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 317
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 318
    const v0, 0x2505f040

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 319
    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingLeft()I

    move-result v0

    int-to-float v1, v0

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingTop()I

    move-result v0

    int-to-float v2, v0

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v0, v3

    int-to-float v3, v0

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v0, v4

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 322
    :cond_0
    invoke-direct {p0}, Lflipboard/gui/FLLabelTextView;->a()V

    .line 324
    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    sub-int v2, v0, v1

    .line 325
    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v1, v3

    sub-int v3, v0, v1

    .line 327
    iget v0, p0, Lflipboard/gui/FLLabelTextView;->i:F

    int-to-float v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 328
    iget-boolean v0, p0, Lflipboard/gui/FLLabelTextView;->l:Z

    if-eqz v0, :cond_2

    .line 433
    :cond_1
    :goto_0
    return-void

    .line 332
    :cond_2
    iget v0, p0, Lflipboard/gui/FLLabelTextView;->j:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    .line 333
    const/4 v0, 0x1

    new-array v0, v0, [F

    .line 334
    iget-object v1, p0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    const-string v4, "\u2026"

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {v1, v4, v5, v6, v0}, Landroid/text/TextPaint;->getTextWidths(Ljava/lang/String;II[F)I

    .line 335
    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, p0, Lflipboard/gui/FLLabelTextView;->j:F

    .line 338
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/FLLabelTextView;->i:F

    .line 339
    invoke-static {}, Lflipboard/util/AndroidUtil;->j()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 340
    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lflipboard/gui/FLLabelTextView;->o:I

    if-ge v0, v1, :cond_4

    .line 341
    iget-object v1, p0, Lflipboard/gui/FLLabelTextView;->f:Ljava/lang/CharSequence;

    const/4 v4, 0x0

    invoke-interface {v1, v4, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v4, p0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v1

    .line 342
    iget v4, p0, Lflipboard/gui/FLLabelTextView;->j:F

    add-float/2addr v4, v1

    int-to-float v5, v2

    cmpl-float v4, v4, v5

    if-lez v4, :cond_7

    .line 343
    iget v1, p0, Lflipboard/gui/FLLabelTextView;->i:F

    iget v4, p0, Lflipboard/gui/FLLabelTextView;->j:F

    add-float/2addr v1, v4

    iput v1, p0, Lflipboard/gui/FLLabelTextView;->i:F

    .line 344
    iput v0, p0, Lflipboard/gui/FLLabelTextView;->h:I

    .line 365
    :cond_4
    :goto_2
    int-to-float v0, v2

    iget v1, p0, Lflipboard/gui/FLLabelTextView;->j:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    int-to-float v0, v3

    iget v1, p0, Lflipboard/gui/FLLabelTextView;->e:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    .line 370
    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingLeft()I

    move-result v0

    int-to-float v1, v0

    .line 371
    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getPaddingTop()I

    move-result v0

    int-to-float v4, v0

    .line 373
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v5, "ar"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    .line 376
    const/4 v0, 0x0

    .line 377
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x10

    if-lt v6, v7, :cond_5

    .line 378
    iget v6, p0, Lflipboard/gui/FLLabelTextView;->k:I

    const v7, 0x800007

    and-int/2addr v6, v7

    packed-switch v6, :pswitch_data_0

    :cond_5
    :pswitch_0
    move v8, v0

    move v0, v1

    move v1, v8

    .line 394
    :goto_3
    if-nez v1, :cond_6

    .line 395
    iget v1, p0, Lflipboard/gui/FLLabelTextView;->k:I

    and-int/lit8 v1, v1, 0x7

    packed-switch v1, :pswitch_data_1

    .line 402
    :pswitch_1
    int-to-float v1, v2

    iget v2, p0, Lflipboard/gui/FLLabelTextView;->i:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 408
    :cond_6
    :goto_4
    :pswitch_2
    iget v1, p0, Lflipboard/gui/FLLabelTextView;->k:I

    and-int/lit8 v1, v1, 0x70

    sparse-switch v1, :sswitch_data_0

    .line 416
    int-to-float v1, v3

    iget v2, p0, Lflipboard/gui/FLLabelTextView;->e:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget-object v2, p0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float/2addr v1, v2

    add-float/2addr v1, v4

    .line 419
    :goto_5
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 422
    iget-object v0, p0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    iget v1, p0, Lflipboard/gui/FLLabelTextView;->m:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 423
    sget-boolean v0, Lflipboard/service/FlipboardManager;->r:Z

    if-eqz v0, :cond_a

    .line 424
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gui/FLLabelTextView;->n:[C

    const/4 v2, 0x0

    iget v3, p0, Lflipboard/gui/FLLabelTextView;->h:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 430
    :goto_6
    iget v0, p0, Lflipboard/gui/FLLabelTextView;->h:I

    iget-object v1, p0, Lflipboard/gui/FLLabelTextView;->f:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 431
    const-string v0, "\u2026"

    iget v1, p0, Lflipboard/gui/FLLabelTextView;->i:F

    iget v2, p0, Lflipboard/gui/FLLabelTextView;->j:F

    sub-float/2addr v1, v2

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 347
    :cond_7
    iput v1, p0, Lflipboard/gui/FLLabelTextView;->i:F

    .line 340
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 350
    :cond_8
    iget v0, p0, Lflipboard/gui/FLLabelTextView;->o:I

    new-array v1, v0, [F

    .line 351
    iget-object v0, p0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    iget-object v4, p0, Lflipboard/gui/FLLabelTextView;->n:[C

    const/4 v5, 0x0

    iget v6, p0, Lflipboard/gui/FLLabelTextView;->o:I

    invoke-virtual {v0, v4, v5, v6, v1}, Landroid/text/TextPaint;->getTextWidths([CII[F)I

    .line 352
    const/4 v0, 0x0

    :goto_7
    iget v4, p0, Lflipboard/gui/FLLabelTextView;->o:I

    if-ge v0, v4, :cond_4

    .line 353
    aget v4, v1, v0

    .line 354
    iget v5, p0, Lflipboard/gui/FLLabelTextView;->i:F

    add-float/2addr v5, v4

    iget v6, p0, Lflipboard/gui/FLLabelTextView;->j:F

    add-float/2addr v5, v6

    int-to-float v6, v2

    cmpl-float v5, v5, v6

    if-lez v5, :cond_9

    .line 355
    iget v1, p0, Lflipboard/gui/FLLabelTextView;->i:F

    iget v4, p0, Lflipboard/gui/FLLabelTextView;->j:F

    add-float/2addr v1, v4

    iput v1, p0, Lflipboard/gui/FLLabelTextView;->i:F

    .line 356
    iput v0, p0, Lflipboard/gui/FLLabelTextView;->h:I

    goto/16 :goto_2

    .line 359
    :cond_9
    iget v5, p0, Lflipboard/gui/FLLabelTextView;->i:F

    add-float/2addr v4, v5

    iput v4, p0, Lflipboard/gui/FLLabelTextView;->i:F

    .line 352
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 380
    :pswitch_3
    const/4 v0, 0x1

    .line 381
    if-eqz v5, :cond_5

    .line 382
    int-to-float v5, v2

    iget v6, p0, Lflipboard/gui/FLLabelTextView;->i:F

    sub-float/2addr v5, v6

    add-float/2addr v1, v5

    move v8, v0

    move v0, v1

    move v1, v8

    goto/16 :goto_3

    .line 386
    :pswitch_4
    const/4 v0, 0x1

    .line 387
    if-nez v5, :cond_5

    .line 388
    int-to-float v5, v2

    iget v6, p0, Lflipboard/gui/FLLabelTextView;->i:F

    sub-float/2addr v5, v6

    add-float/2addr v1, v5

    move v8, v0

    move v0, v1

    move v1, v8

    goto/16 :goto_3

    .line 399
    :pswitch_5
    int-to-float v1, v2

    iget v2, p0, Lflipboard/gui/FLLabelTextView;->i:F

    sub-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 400
    goto/16 :goto_4

    .line 410
    :sswitch_0
    iget-object v1, p0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float v1, v4, v1

    .line 411
    goto/16 :goto_5

    .line 413
    :sswitch_1
    int-to-float v1, v3

    iget-object v2, p0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Paint$FontMetrics;->bottom:F

    sub-float/2addr v1, v2

    add-float/2addr v1, v4

    .line 414
    goto/16 :goto_5

    .line 426
    :cond_a
    iget-object v1, p0, Lflipboard/gui/FLLabelTextView;->n:[C

    const/4 v2, 0x0

    iget v3, p0, Lflipboard/gui/FLLabelTextView;->h:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, p0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText([CIIFFLandroid/graphics/Paint;)V

    goto/16 :goto_6

    .line 378
    :pswitch_data_0
    .packed-switch 0x800003
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 395
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
        :pswitch_1
        :pswitch_5
    .end packed-switch

    .line 408
    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x50 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    .line 244
    sget-object v0, Lflipboard/gui/FLLabelTextView;->a:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_0

    .line 252
    iget v0, p0, Lflipboard/gui/FLLabelTextView;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/gui/FLLabelTextView;->p:I

    .line 253
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 254
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLLabelTextView;->b(II)V

    .line 255
    iget-wide v2, p0, Lflipboard/gui/FLLabelTextView;->q:J

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long v0, v4, v0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lflipboard/gui/FLLabelTextView;->q:J

    .line 259
    :goto_0
    return-void

    .line 257
    :cond_0
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLLabelTextView;->b(II)V

    goto :goto_0
.end method

.method public setGravity(I)V
    .locals 0

    .prologue
    .line 450
    iput p1, p0, Lflipboard/gui/FLLabelTextView;->k:I

    .line 451
    return-void
.end method

.method public setShadowLayer(FFFI)V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lflipboard/gui/FLLabelTextView;->c:Landroid/text/TextPaint;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 205
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    .line 171
    if-nez p1, :cond_0

    .line 172
    const-string p1, ""

    .line 176
    :cond_0
    invoke-virtual {p0, p1}, Lflipboard/gui/FLLabelTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 178
    iput-object p1, p0, Lflipboard/gui/FLLabelTextView;->f:Ljava/lang/CharSequence;

    .line 179
    iget-object v0, p0, Lflipboard/gui/FLLabelTextView;->n:[C

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/FLLabelTextView;->n:[C

    array-length v0, v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 180
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    new-array v0, v0, [C

    iput-object v0, p0, Lflipboard/gui/FLLabelTextView;->n:[C

    .line 182
    :cond_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/FLLabelTextView;->n:[C

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p1, v0, v1, v2, v3}, Lflipboard/gui/FLStaticTextView;->a(Ljava/lang/CharSequence;I[CIZ)I

    move-result v0

    iput v0, p0, Lflipboard/gui/FLLabelTextView;->o:I

    .line 183
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lflipboard/gui/FLLabelTextView;->g:F

    .line 186
    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->invalidate()V

    .line 189
    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 190
    if-eqz v0, :cond_3

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    .line 191
    :cond_3
    invoke-virtual {p0}, Lflipboard/gui/FLLabelTextView;->requestLayout()V

    .line 193
    :cond_4
    return-void
.end method

.method public setTextColor(I)V
    .locals 0

    .prologue
    .line 196
    iput p1, p0, Lflipboard/gui/FLLabelTextView;->m:I

    .line 197
    return-void
.end method

.method public setTextSize(I)V
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lflipboard/gui/FLLabelTextView;->a(II)V

    .line 214
    return-void
.end method
