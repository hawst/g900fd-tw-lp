.class public Lflipboard/gui/FLVideoView;
.super Landroid/widget/VideoView;
.source "FLVideoView.java"


# instance fields
.field a:I

.field b:I

.field public c:Lflipboard/gui/FLVideoView$TouchEventCallback;

.field public d:Lflipboard/gui/FLVideoView$DurationCallback;

.field private e:I

.field private final f:Landroid/os/Handler;

.field private g:J

.field private h:J

.field private final i:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lflipboard/gui/FLVideoView$TouchEventCallback;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lflipboard/gui/FLVideoView;-><init>(Landroid/content/Context;Lflipboard/gui/FLVideoView$TouchEventCallback;Lflipboard/gui/FLVideoView$DurationCallback;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lflipboard/gui/FLVideoView$TouchEventCallback;Lflipboard/gui/FLVideoView$DurationCallback;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;)V

    .line 18
    const/16 v0, 0x1e0

    iput v0, p0, Lflipboard/gui/FLVideoView;->a:I

    .line 19
    const/16 v0, 0x168

    iput v0, p0, Lflipboard/gui/FLVideoView;->b:I

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/gui/FLVideoView;->e:I

    .line 23
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLVideoView;->f:Landroid/os/Handler;

    .line 137
    new-instance v0, Lflipboard/gui/FLVideoView$2;

    invoke-direct {v0, p0}, Lflipboard/gui/FLVideoView$2;-><init>(Lflipboard/gui/FLVideoView;)V

    iput-object v0, p0, Lflipboard/gui/FLVideoView;->i:Ljava/lang/Runnable;

    .line 33
    iput-object p2, p0, Lflipboard/gui/FLVideoView;->c:Lflipboard/gui/FLVideoView$TouchEventCallback;

    .line 34
    iput-object p3, p0, Lflipboard/gui/FLVideoView;->d:Lflipboard/gui/FLVideoView$DurationCallback;

    .line 35
    return-void
.end method

.method static synthetic a(Lflipboard/gui/FLVideoView;J)J
    .locals 1

    .prologue
    .line 17
    iput-wide p1, p0, Lflipboard/gui/FLVideoView;->h:J

    return-wide p1
.end method

.method private a()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 98
    iget-wide v0, p0, Lflipboard/gui/FLVideoView;->h:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    .line 99
    iget-wide v0, p0, Lflipboard/gui/FLVideoView;->g:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lflipboard/gui/FLVideoView;->h:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lflipboard/gui/FLVideoView;->g:J

    .line 103
    :cond_0
    iput-wide v6, p0, Lflipboard/gui/FLVideoView;->h:J

    .line 104
    return-void
.end method

.method static synthetic a(Lflipboard/gui/FLVideoView;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lflipboard/gui/FLVideoView;->a()V

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 148
    invoke-virtual {p0}, Lflipboard/gui/FLVideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLVideoView;->d:Lflipboard/gui/FLVideoView$DurationCallback;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lflipboard/gui/FLVideoView;->d:Lflipboard/gui/FLVideoView$DurationCallback;

    invoke-virtual {p0}, Lflipboard/gui/FLVideoView;->getCurrentPosition()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    iget v2, p0, Lflipboard/gui/FLVideoView;->e:I

    div-int/2addr v1, v2

    invoke-interface {v0, v1}, Lflipboard/gui/FLVideoView$DurationCallback;->a(I)V

    .line 153
    iget-object v0, p0, Lflipboard/gui/FLVideoView;->f:Landroid/os/Handler;

    iget-object v1, p0, Lflipboard/gui/FLVideoView;->i:Ljava/lang/Runnable;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 155
    :cond_0
    return-void
.end method

.method static synthetic b(Lflipboard/gui/FLVideoView;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lflipboard/gui/FLVideoView;->b()V

    return-void
.end method


# virtual methods
.method public final a(III)V
    .locals 4

    .prologue
    .line 38
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    const/4 v0, 0x2

    if-ne p3, v0, :cond_3

    .line 42
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget v0, v0, Lflipboard/app/FlipboardApplication;->d:I

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget v1, v1, Lflipboard/app/FlipboardApplication;->e:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 43
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget v1, v1, Lflipboard/app/FlipboardApplication;->d:I

    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget v2, v2, Lflipboard/app/FlipboardApplication;->e:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 44
    mul-int v2, p1, v0

    mul-int v3, p2, v1

    if-le v2, v3, :cond_2

    .line 45
    iput v1, p0, Lflipboard/gui/FLVideoView;->a:I

    .line 46
    mul-int v0, p2, v1

    div-int/2addr v0, p1

    iput v0, p0, Lflipboard/gui/FLVideoView;->b:I

    .line 59
    :goto_1
    invoke-virtual {p0}, Lflipboard/gui/FLVideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget v1, p0, Lflipboard/gui/FLVideoView;->a:I

    iget v2, p0, Lflipboard/gui/FLVideoView;->b:I

    invoke-interface {v0, v1, v2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 61
    invoke-virtual {p0}, Lflipboard/gui/FLVideoView;->requestLayout()V

    .line 62
    invoke-virtual {p0}, Lflipboard/gui/FLVideoView;->invalidate()V

    goto :goto_0

    .line 49
    :cond_2
    mul-int v1, v0, p1

    div-int/2addr v1, p2

    iput v1, p0, Lflipboard/gui/FLVideoView;->a:I

    .line 50
    iput v0, p0, Lflipboard/gui/FLVideoView;->b:I

    goto :goto_1

    .line 53
    :cond_3
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget v0, v0, Lflipboard/app/FlipboardApplication;->d:I

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget v1, v1, Lflipboard/app/FlipboardApplication;->e:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 54
    iput v0, p0, Lflipboard/gui/FLVideoView;->a:I

    .line 55
    mul-int/2addr v0, p2

    div-int/2addr v0, p1

    iput v0, p0, Lflipboard/gui/FLVideoView;->b:I

    goto :goto_1
.end method

.method public getTotalWatchedTime()J
    .locals 4

    .prologue
    .line 107
    invoke-direct {p0}, Lflipboard/gui/FLVideoView;->a()V

    .line 108
    iget v0, p0, Lflipboard/gui/FLVideoView;->e:I

    int-to-long v0, v0

    iget-wide v2, p0, Lflipboard/gui/FLVideoView;->g:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 73
    iget v0, p0, Lflipboard/gui/FLVideoView;->a:I

    iget v1, p0, Lflipboard/gui/FLVideoView;->b:I

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/FLVideoView;->setMeasuredDimension(II)V

    .line 74
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lflipboard/gui/FLVideoView;->c:Lflipboard/gui/FLVideoView$TouchEventCallback;

    invoke-interface {v0}, Lflipboard/gui/FLVideoView$TouchEventCallback;->a()V

    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public pause()V
    .locals 0

    .prologue
    .line 90
    invoke-super {p0}, Landroid/widget/VideoView;->pause()V

    .line 91
    invoke-direct {p0}, Lflipboard/gui/FLVideoView;->a()V

    .line 92
    return-void
.end method

.method public setMediaController(Landroid/widget/MediaController;)V
    .locals 4

    .prologue
    .line 113
    invoke-super {p0, p1}, Landroid/widget/VideoView;->setMediaController(Landroid/widget/MediaController;)V

    .line 117
    invoke-virtual {p0}, Lflipboard/gui/FLVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "mediacontroller_progress"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 118
    invoke-virtual {p1, v0}, Landroid/widget/MediaController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    .line 119
    new-instance v1, Lflipboard/gui/FLVideoView$1;

    invoke-direct {v1, p0}, Lflipboard/gui/FLVideoView$1;-><init>(Lflipboard/gui/FLVideoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 135
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 78
    invoke-virtual {p0}, Lflipboard/gui/FLVideoView;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    invoke-super {p0}, Landroid/widget/VideoView;->start()V

    .line 80
    iget-object v0, p0, Lflipboard/gui/FLVideoView;->d:Lflipboard/gui/FLVideoView$DurationCallback;

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p0}, Lflipboard/gui/FLVideoView;->getDuration()I

    move-result v0

    iput v0, p0, Lflipboard/gui/FLVideoView;->e:I

    .line 82
    invoke-direct {p0}, Lflipboard/gui/FLVideoView;->b()V

    .line 83
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/gui/FLVideoView;->h:J

    .line 86
    :cond_0
    return-void
.end method
