.class public Lflipboard/gui/FLToggleImageButton;
.super Landroid/widget/ImageButton;
.source "FLToggleImageButton.java"

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field private static final b:[I


# instance fields
.field a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lflipboard/gui/FLToggleImageButton;->b:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 69
    invoke-super {p0}, Landroid/widget/ImageButton;->drawableStateChanged()V

    .line 70
    invoke-virtual {p0}, Lflipboard/gui/FLToggleImageButton;->invalidate()V

    .line 71
    return-void
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lflipboard/gui/FLToggleImageButton;->a:Z

    return v0
.end method

.method public onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 60
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/widget/ImageButton;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 61
    invoke-virtual {p0}, Lflipboard/gui/FLToggleImageButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    sget-object v1, Lflipboard/gui/FLToggleImageButton;->b:[I

    invoke-static {v0, v1}, Lflipboard/gui/FLToggleImageButton;->mergeDrawableStates([I[I)[I

    .line 64
    :cond_0
    return-object v0
.end method

.method public setChecked(Z)V
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lflipboard/gui/FLToggleImageButton;->a:Z

    .line 55
    invoke-virtual {p0}, Lflipboard/gui/FLToggleImageButton;->refreshDrawableState()V

    .line 56
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 77
    invoke-virtual {p0}, Lflipboard/gui/FLToggleImageButton;->refreshDrawableState()V

    .line 78
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lflipboard/gui/FLToggleImageButton;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lflipboard/gui/FLToggleImageButton;->setChecked(Z)V

    .line 47
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
