.class Lflipboard/gui/FLBitmapView$4;
.super Ljava/lang/Object;
.source "FLBitmapView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/gui/FLBitmapView;


# direct methods
.method constructor <init>(Lflipboard/gui/FLBitmapView;)V
    .locals 0

    .prologue
    .line 677
    iput-object p1, p0, Lflipboard/gui/FLBitmapView$4;->a:Lflipboard/gui/FLBitmapView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 680
    sget-object v0, Lflipboard/gui/FLBitmapView;->a:Lflipboard/util/Log;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lflipboard/gui/FLBitmapView$4;->a:Lflipboard/gui/FLBitmapView;

    iget v2, v2, Lflipboard/gui/FLBitmapView;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lflipboard/gui/FLBitmapView$4;->a:Lflipboard/gui/FLBitmapView;

    invoke-static {v2}, Lflipboard/gui/FLBitmapView;->a(Lflipboard/gui/FLBitmapView;)Lflipboard/io/BitmapManager$Handle;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lflipboard/gui/FLBitmapView$4;->a:Lflipboard/gui/FLBitmapView;

    invoke-static {v2}, Lflipboard/gui/FLBitmapView;->b(Lflipboard/gui/FLBitmapView;)Lflipboard/io/BitmapManager$Handle;

    move-result-object v2

    aput-object v2, v0, v1

    .line 681
    iget-object v0, p0, Lflipboard/gui/FLBitmapView$4;->a:Lflipboard/gui/FLBitmapView;

    invoke-static {v0}, Lflipboard/gui/FLBitmapView;->c(Lflipboard/gui/FLBitmapView;)Lflipboard/io/BitmapManager$Handle;

    move-result-object v0

    .line 682
    if-eqz v0, :cond_0

    .line 683
    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->b()Landroid/graphics/Bitmap;

    .line 684
    iget-object v1, p0, Lflipboard/gui/FLBitmapView$4;->a:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v1}, Lflipboard/gui/FLBitmapView;->postInvalidate()V

    .line 685
    iget-object v1, p0, Lflipboard/gui/FLBitmapView$4;->a:Lflipboard/gui/FLBitmapView;

    invoke-static {v1, v0}, Lflipboard/gui/FLBitmapView;->a(Lflipboard/gui/FLBitmapView;Lflipboard/io/BitmapManager$Handle;)V

    .line 687
    :cond_0
    return-void
.end method
