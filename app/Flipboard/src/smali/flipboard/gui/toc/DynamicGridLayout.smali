.class public Lflipboard/gui/toc/DynamicGridLayout;
.super Lflipboard/gui/ContainerView;
.source "DynamicGridLayout.java"


# static fields
.field public static final a:Lflipboard/util/Log;

.field private static final l:Landroid/graphics/Rect;


# instance fields
.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field h:I

.field final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field j:Landroid/graphics/Rect;

.field k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-string v0, "test"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/toc/DynamicGridLayout;->a:Lflipboard/util/Log;

    .line 32
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lflipboard/gui/toc/DynamicGridLayout;->l:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    const/4 v1, 0x0

    .line 44
    invoke-direct {p0, p1}, Lflipboard/gui/ContainerView;-><init>(Landroid/content/Context;)V

    .line 34
    const/4 v0, 0x3

    iput v0, p0, Lflipboard/gui/toc/DynamicGridLayout;->b:I

    const/4 v0, 0x2

    iput v0, p0, Lflipboard/gui/toc/DynamicGridLayout;->c:I

    .line 35
    iput v2, p0, Lflipboard/gui/toc/DynamicGridLayout;->d:I

    iput v2, p0, Lflipboard/gui/toc/DynamicGridLayout;->e:I

    .line 37
    iput v1, p0, Lflipboard/gui/toc/DynamicGridLayout;->h:I

    .line 38
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lflipboard/gui/toc/DynamicGridLayout;->i:Ljava/util/Set;

    .line 45
    invoke-virtual {p0, v1}, Lflipboard/gui/toc/DynamicGridLayout;->setClipChildren(Z)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/16 v3, 0x1e

    const/4 v2, 0x5

    const/4 v1, 0x0

    .line 49
    invoke-direct {p0, p1, p2}, Lflipboard/gui/ContainerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    const/4 v0, 0x3

    iput v0, p0, Lflipboard/gui/toc/DynamicGridLayout;->b:I

    const/4 v0, 0x2

    iput v0, p0, Lflipboard/gui/toc/DynamicGridLayout;->c:I

    .line 35
    iput v2, p0, Lflipboard/gui/toc/DynamicGridLayout;->d:I

    iput v2, p0, Lflipboard/gui/toc/DynamicGridLayout;->e:I

    .line 37
    iput v1, p0, Lflipboard/gui/toc/DynamicGridLayout;->h:I

    .line 38
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lflipboard/gui/toc/DynamicGridLayout;->i:Ljava/util/Set;

    .line 50
    invoke-virtual {p0, v1}, Lflipboard/gui/toc/DynamicGridLayout;->setClipChildren(Z)V

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201de

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 53
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 54
    new-instance v2, Landroid/graphics/Rect;

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1e

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1e

    invoke-direct {v2, v1, v0, v3, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v2}, Lflipboard/gui/toc/DynamicGridLayout;->setItemOverhang(Landroid/graphics/Rect;)V

    .line 55
    return-void
.end method

.method protected static a()I
    .locals 1

    .prologue
    .line 103
    const/16 v0, 0x15e

    return v0
.end method

.method static synthetic a(Lflipboard/gui/toc/DynamicGridLayout;ILandroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 29
    iget-boolean v0, p0, Lflipboard/gui/toc/DynamicGridLayout;->k:Z

    if-eqz v0, :cond_0

    add-int/lit8 p1, p1, 0x2

    :cond_0
    iget v0, p0, Lflipboard/gui/toc/DynamicGridLayout;->c:I

    rem-int v0, p1, v0

    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout;->c:I

    div-int v1, p1, v1

    if-nez p2, :cond_1

    new-instance p2, Landroid/graphics/Rect;

    invoke-direct {p2}, Landroid/graphics/Rect;-><init>()V

    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/toc/DynamicGridLayout;->getPaddingLeft()I

    move-result v2

    iget v3, p0, Lflipboard/gui/toc/DynamicGridLayout;->f:I

    iget v4, p0, Lflipboard/gui/toc/DynamicGridLayout;->d:I

    add-int/2addr v3, v4

    mul-int/2addr v0, v3

    add-int/2addr v0, v2

    iput v0, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lflipboard/gui/toc/DynamicGridLayout;->getPaddingTop()I

    move-result v0

    iget v2, p0, Lflipboard/gui/toc/DynamicGridLayout;->g:I

    iget v3, p0, Lflipboard/gui/toc/DynamicGridLayout;->e:I

    add-int/2addr v2, v3

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->top:I

    iget v0, p2, Landroid/graphics/Rect;->left:I

    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout;->f:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->right:I

    iget v0, p2, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout;->g:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->bottom:I

    return-object p2
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    .prologue
    .line 79
    iput p1, p0, Lflipboard/gui/toc/DynamicGridLayout;->b:I

    .line 80
    iput p2, p0, Lflipboard/gui/toc/DynamicGridLayout;->c:I

    .line 81
    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 723
    invoke-virtual {p0}, Lflipboard/gui/toc/DynamicGridLayout;->getChildCount()I

    move-result v1

    .line 724
    if-gez p2, :cond_1

    move v0, v1

    .line 727
    :goto_0
    if-ne v0, v1, :cond_0

    if-lez v1, :cond_0

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lflipboard/gui/toc/DynamicGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Lflipboard/gui/toc/TileContainer$MoreTileContainer;

    if-eqz v1, :cond_0

    .line 728
    add-int/lit8 v0, v0, -0x1

    .line 730
    :cond_0
    invoke-super {p0, p1, v0}, Lflipboard/gui/ContainerView;->addView(Landroid/view/View;I)V

    .line 731
    return-void

    :cond_1
    move v0, p2

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 11

    .prologue
    .line 634
    invoke-virtual {p0}, Lflipboard/gui/toc/DynamicGridLayout;->getChildCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 678
    :cond_0
    return-void

    .line 638
    :cond_1
    iget v6, p0, Lflipboard/gui/toc/DynamicGridLayout;->f:I

    .line 639
    iget v7, p0, Lflipboard/gui/toc/DynamicGridLayout;->g:I

    .line 641
    const/4 v2, 0x0

    .line 643
    invoke-virtual {p0}, Lflipboard/gui/toc/DynamicGridLayout;->getPaddingTop()I

    move-result v1

    .line 645
    const/4 v0, 0x0

    .line 646
    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/toc/DynamicGridLayout;->getPaddingLeft()I

    move-result v4

    .line 647
    const/4 v3, 0x0

    .line 649
    iget-boolean v5, p0, Lflipboard/gui/toc/DynamicGridLayout;->k:Z

    if-eqz v5, :cond_2

    if-nez v0, :cond_2

    sget-object v5, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v5, v5, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v5, :cond_2

    .line 650
    iget v3, p0, Lflipboard/gui/toc/DynamicGridLayout;->d:I

    add-int/2addr v3, v6

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v4, v3

    .line 651
    const/4 v3, 0x2

    .line 654
    :cond_2
    :goto_1
    iget v5, p0, Lflipboard/gui/toc/DynamicGridLayout;->c:I

    if-ge v3, v5, :cond_4

    .line 655
    invoke-virtual {p0}, Lflipboard/gui/toc/DynamicGridLayout;->getChildCount()I

    move-result v5

    if-ge v2, v5, :cond_5

    .line 657
    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p0, v2}, Lflipboard/gui/toc/DynamicGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 660
    iget-object v8, p0, Lflipboard/gui/toc/DynamicGridLayout;->i:Ljava/util/Set;

    invoke-interface {v8, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-static {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->e(Landroid/view/View;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 661
    new-instance v8, Landroid/graphics/Rect;

    add-int v9, v4, v6

    add-int v10, v1, v7

    invoke-direct {v8, v4, v1, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    const/16 v9, 0x15e

    iget-object v10, p0, Lflipboard/gui/toc/DynamicGridLayout;->j:Landroid/graphics/Rect;

    invoke-static {v2, v8, v9, v10}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;Landroid/graphics/Rect;ILandroid/graphics/Rect;)V

    .line 665
    :goto_2
    iget v2, p0, Lflipboard/gui/toc/DynamicGridLayout;->d:I

    add-int/2addr v2, v6

    add-int/2addr v4, v2

    .line 654
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v5

    goto :goto_1

    .line 663
    :cond_3
    add-int v8, v4, v6

    add-int v9, v1, v7

    invoke-virtual {v2, v4, v1, v8, v9}, Landroid/view/View;->layout(IIII)V

    goto :goto_2

    .line 667
    :cond_4
    iget v3, p0, Lflipboard/gui/toc/DynamicGridLayout;->e:I

    add-int/2addr v3, v7

    add-int/2addr v1, v3

    .line 645
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 671
    :cond_5
    iget v0, p0, Lflipboard/gui/toc/DynamicGridLayout;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/gui/toc/DynamicGridLayout;->h:I

    .line 674
    iget-object v0, p0, Lflipboard/gui/toc/DynamicGridLayout;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 675
    invoke-virtual {p0}, Lflipboard/gui/toc/DynamicGridLayout;->getChildCount()I

    move-result v0

    :goto_3
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_0

    .line 676
    iget-object v1, p0, Lflipboard/gui/toc/DynamicGridLayout;->i:Ljava/util/Set;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/DynamicGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 617
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 618
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 620
    invoke-virtual {p0}, Lflipboard/gui/toc/DynamicGridLayout;->getPaddingLeft()I

    move-result v0

    sub-int v0, v1, v0

    invoke-virtual {p0}, Lflipboard/gui/toc/DynamicGridLayout;->getPaddingRight()I

    move-result v3

    sub-int/2addr v0, v3

    iget v3, p0, Lflipboard/gui/toc/DynamicGridLayout;->d:I

    iget v4, p0, Lflipboard/gui/toc/DynamicGridLayout;->c:I

    add-int/lit8 v4, v4, -0x2

    mul-int/2addr v3, v4

    sub-int/2addr v0, v3

    iget v3, p0, Lflipboard/gui/toc/DynamicGridLayout;->c:I

    div-int/2addr v0, v3

    iput v0, p0, Lflipboard/gui/toc/DynamicGridLayout;->f:I

    .line 621
    invoke-virtual {p0}, Lflipboard/gui/toc/DynamicGridLayout;->getPaddingTop()I

    move-result v0

    sub-int v0, v2, v0

    invoke-virtual {p0}, Lflipboard/gui/toc/DynamicGridLayout;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v0, v3

    iget v3, p0, Lflipboard/gui/toc/DynamicGridLayout;->e:I

    iget v4, p0, Lflipboard/gui/toc/DynamicGridLayout;->b:I

    add-int/lit8 v4, v4, -0x1

    mul-int/2addr v3, v4

    sub-int/2addr v0, v3

    iget v3, p0, Lflipboard/gui/toc/DynamicGridLayout;->b:I

    div-int/2addr v0, v3

    iput v0, p0, Lflipboard/gui/toc/DynamicGridLayout;->g:I

    .line 623
    iget v0, p0, Lflipboard/gui/toc/DynamicGridLayout;->f:I

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 624
    iget v0, p0, Lflipboard/gui/toc/DynamicGridLayout;->g:I

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 625
    invoke-virtual {p0}, Lflipboard/gui/toc/DynamicGridLayout;->getChildCount()I

    move-result v0

    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_0

    .line 626
    invoke-virtual {p0, v0}, Lflipboard/gui/toc/DynamicGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Landroid/view/View;->measure(II)V

    goto :goto_0

    .line 629
    :cond_0
    invoke-virtual {p0, v1, v2}, Lflipboard/gui/toc/DynamicGridLayout;->setMeasuredDimension(II)V

    .line 630
    return-void
.end method

.method public setGridOrder(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 610
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 611
    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    goto :goto_0

    .line 613
    :cond_0
    return-void
.end method

.method public setIsCoverStoryPage(Z)V
    .locals 0

    .prologue
    .line 91
    iput-boolean p1, p0, Lflipboard/gui/toc/DynamicGridLayout;->k:Z

    .line 92
    return-void
.end method

.method public setItemOverhang(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 96
    if-nez p1, :cond_0

    .line 97
    sget-object v0, Lflipboard/gui/toc/DynamicGridLayout;->l:Landroid/graphics/Rect;

    iput-object v0, p0, Lflipboard/gui/toc/DynamicGridLayout;->j:Landroid/graphics/Rect;

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_0
    iput-object p1, p0, Lflipboard/gui/toc/DynamicGridLayout;->j:Landroid/graphics/Rect;

    goto :goto_0
.end method
