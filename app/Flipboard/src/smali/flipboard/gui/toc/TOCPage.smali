.class public Lflipboard/gui/toc/TOCPage;
.super Lflipboard/gui/FLRelativeLayout;
.source "TOCPage.java"

# interfaces
.implements Lflipboard/gui/FLViewIntf;
.implements Lflipboard/samsung/spen/OnSamsungViewListener;


# static fields
.field static d:Lflipboard/util/Log;

.field static e:Lflipboard/util/Log;


# instance fields
.field private a:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/User;",
            "Lflipboard/service/User$Message;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/io/NetworkManager;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lflipboard/gui/FLTextView;

.field final f:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lflipboard/service/Section;",
            ">;"
        }
    .end annotation
.end field

.field protected g:I

.field protected h:I

.field protected i:Lflipboard/gui/toc/DynamicGridLayout;

.field protected j:Lflipboard/gui/FLTextIntf;

.field k:Landroid/view/View;

.field l:Landroid/view/View;

.field protected m:I

.field protected o:Lflipboard/gui/toc/TOCView;

.field protected p:Lflipboard/gui/toc/TileContainer$MoreTileContainer;

.field protected q:Lflipboard/samsung/spen/TOCSectionPreview;

.field protected final r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/samsung/spen/TOCSectionPreview;",
            ">;"
        }
    .end annotation
.end field

.field protected s:Lflipboard/gui/toc/TOCSearchBoxContainer;

.field protected t:Landroid/widget/ImageView;

.field protected u:Z

.field private v:Lflipboard/gui/FLLabelTextView;

.field private w:Lflipboard/gui/FLImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-string v0, "tileflips"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/toc/TOCPage;->d:Lflipboard/util/Log;

    .line 62
    const-string v0, "sectionpreview"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/toc/TOCPage;->e:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/toc/TOCPage;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/toc/TOCPage;->u:Z

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/toc/TOCPage;->r:Ljava/util/List;

    .line 100
    return-void
.end method

.method private a(Lflipboard/samsung/spen/TOCSectionPreview;)F
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v0, 0x3f000000    # 0.5f

    .line 407
    invoke-virtual {p1}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lflipboard/util/AndroidUtil;->c(Landroid/view/View;)[I

    move-result-object v1

    .line 408
    invoke-virtual {p1}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {p1}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p1}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    .line 409
    aget v3, v1, v5

    int-to-float v3, v3

    invoke-virtual {p1}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1

    .line 410
    invoke-virtual {p1}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, v2

    div-float/2addr v0, v1

    .line 414
    :cond_0
    :goto_0
    return v0

    .line 411
    :cond_1
    aget v1, v1, v5

    invoke-virtual {p1}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v1, v3

    int-to-float v1, v1

    iget-object v3, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v3}, Lflipboard/gui/toc/DynamicGridLayout;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3f666666    # 0.9f

    mul-float/2addr v3, v4

    cmpl-float v1, v1, v3

    if-lez v1, :cond_0

    .line 412
    invoke-virtual {p1}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingRight()I

    move-result v0

    sub-int v0, v2, v0

    int-to-float v0, v0

    int-to-float v1, v2

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/gui/toc/TOCPage;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 55
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v4, 0x3f800000    # 1.0f

    move v3, v1

    move v5, v1

    move v6, v2

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->t:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private b(Lflipboard/samsung/spen/TOCSectionPreview;)F
    .locals 4

    .prologue
    .line 420
    invoke-virtual {p1}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->c(Landroid/view/View;)[I

    move-result-object v0

    .line 421
    invoke-virtual {p1}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    mul-int/lit8 v1, v1, 0x3

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p1}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p1}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    .line 422
    const/4 v2, 0x1

    aget v0, v0, v2

    invoke-virtual {p1}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    int-to-float v0, v0

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getHeight()I

    move-result v2

    int-to-float v2, v2

    const v3, 0x3f59999a    # 0.85f

    mul-float/2addr v2, v3

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 423
    invoke-virtual {p1}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingBottom()I

    move-result v0

    sub-int v0, v1, v0

    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 427
    :goto_0
    return v0

    .line 424
    :cond_0
    invoke-virtual {p1}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p1}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 425
    invoke-virtual {p1}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0

    .line 427
    :cond_1
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 661
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    const v1, 0x7f0a0237

    invoke-virtual {p0, v1}, Lflipboard/gui/toc/TOCPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "tocAccountButton"

    invoke-virtual {v0, v1, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 662
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->t:Landroid/widget/ImageView;

    const-string v2, "tocRibbonButton"

    invoke-virtual {v0, v1, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 663
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    const v1, 0x7f0a0364

    invoke-virtual {p0, v1}, Lflipboard/gui/toc/TOCPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "tocRefreshButton"

    invoke-virtual {v0, v1, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 664
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    const-string v2, "CGSearch"

    invoke-virtual {v0, v1, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 666
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 667
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->getMenu()Lflipboard/gui/actionbar/FLActionBarMenu;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 668
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->getMenu()Lflipboard/gui/actionbar/FLActionBarMenu;

    move-result-object v0

    const v1, 0x7f0a03b6

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 669
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 670
    sget-object v1, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    const-string v2, "tocSettingsButton"

    invoke-virtual {v1, v0, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 675
    :cond_0
    iget-object v5, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    move v2, v3

    :goto_0
    invoke-virtual {v5}, Lflipboard/gui/toc/DynamicGridLayout;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_4

    invoke-virtual {v5, v2}, Lflipboard/gui/toc/DynamicGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v0, v1, Lflipboard/gui/toc/TileContainer;

    if-eqz v0, :cond_3

    move-object v0, v1

    check-cast v0, Lflipboard/gui/toc/TileContainer;

    invoke-virtual {v0}, Lflipboard/gui/toc/TileContainer;->getSection()Lflipboard/service/Section;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_1

    sget-object v6, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    const-string v7, "tocTile"

    invoke-virtual {v6, v1, v7}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    sget-object v6, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    const-string v7, "%s-%s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const-string v9, "tocTile"

    aput-object v9, v8, v3

    const/4 v9, 0x1

    aput-object v0, v8, v9

    invoke-static {v7, v8}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v1, v0}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move-object v0, v4

    goto :goto_1

    :cond_3
    instance-of v0, v1, Lflipboard/gui/toc/TileContainer$MoreTileContainer;

    if-eqz v0, :cond_5

    const-string v0, "kFLAddSectionIdentifier"

    goto :goto_1

    .line 676
    :cond_4
    return-void

    :cond_5
    move-object v0, v4

    goto :goto_1
.end method

.method protected final a(II)V
    .locals 5

    .prologue
    .line 538
    iput p1, p0, Lflipboard/gui/toc/TOCPage;->g:I

    .line 539
    iput p2, p0, Lflipboard/gui/toc/TOCPage;->h:I

    .line 540
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->j:Lflipboard/gui/FLTextIntf;

    if-eqz v0, :cond_0

    .line 541
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->j:Lflipboard/gui/FLTextIntf;

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0328

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 543
    :cond_0
    return-void
.end method

.method protected final a(Landroid/view/View;)V
    .locals 10

    .prologue
    .line 305
    sget-object v0, Lflipboard/gui/toc/TOCPage;->e:Lflipboard/util/Log;

    .line 307
    instance-of v0, p1, Lflipboard/gui/toc/TileContainer;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 308
    check-cast v0, Lflipboard/gui/toc/TileContainer;

    .line 310
    iget-object v1, v0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->m()Ljava/util/List;

    move-result-object v4

    .line 311
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 365
    :cond_0
    :goto_0
    return-void

    .line 314
    :cond_1
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    if-nez v1, :cond_d

    .line 315
    instance-of v1, p1, Lflipboard/gui/toc/CoverStoryTileContainer;

    if-eqz v1, :cond_8

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v1, :cond_8

    .line 317
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f030145

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;

    iput-object v1, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    move-object v1, p1

    .line 318
    check-cast v1, Lflipboard/gui/toc/CoverStoryTileContainer;

    .line 319
    const v2, 0x7f0a035b

    invoke-virtual {v1, v2}, Lflipboard/gui/toc/CoverStoryTileContainer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 320
    iget-object v3, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    const v5, 0x7f0a035b

    invoke-virtual {v3, v5}, Lflipboard/samsung/spen/TOCSectionPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 321
    const v2, 0x7f0a035c

    invoke-virtual {v1, v2}, Lflipboard/gui/toc/CoverStoryTileContainer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLLabelTextView;

    .line 322
    iget-object v3, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    const v5, 0x7f0a035c

    invoke-virtual {v3, v5}, Lflipboard/samsung/spen/TOCSectionPreview;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v3, v2}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 323
    const v2, 0x7f0a035d

    invoke-virtual {v1, v2}, Lflipboard/gui/toc/CoverStoryTileContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLStaticTextView;

    .line 324
    iget-object v2, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    const v3, 0x7f0a035d

    invoke-virtual {v2, v3}, Lflipboard/samsung/spen/TOCSectionPreview;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLStaticTextView;

    .line 325
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->w:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLStaticTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 326
    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 330
    :goto_1
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    iget-object v2, p0, Lflipboard/gui/toc/TOCPage;->o:Lflipboard/gui/toc/TOCView;

    iput-object p1, v1, Lflipboard/samsung/spen/TOCSectionPreview;->d:Landroid/view/View;

    iput-object v2, v1, Lflipboard/samsung/spen/TOCSectionPreview;->i:Lflipboard/gui/toc/TOCView;

    .line 331
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    iget-object v2, v0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->t()Z

    move-result v2

    invoke-virtual {v1, v2}, Lflipboard/samsung/spen/TOCSectionPreview;->setIsCoverStories(Z)V

    .line 333
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    invoke-virtual {p0, v1}, Lflipboard/gui/toc/TOCPage;->addView(Landroid/view/View;)V

    .line 334
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->r:Ljava/util/List;

    iget-object v2, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 335
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    invoke-virtual {v1, p0}, Lflipboard/samsung/spen/TOCSectionPreview;->setSPenHoverListener(Lflipboard/samsung/spen/OnSamsungViewListener;)V

    .line 336
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    iget-object v0, v0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v1, v0}, Lflipboard/samsung/spen/TOCSectionPreview;->setSection(Lflipboard/service/Section;)V

    .line 337
    iget-object v5, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    iget-object v0, v5, Lflipboard/samsung/spen/TOCSectionPreview;->e:Lflipboard/service/Section;

    iget-object v1, v0, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->i()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, v5, Lflipboard/samsung/spen/TOCSectionPreview;->d:Landroid/view/View;

    instance-of v0, v0, Lflipboard/gui/toc/TileContainer;

    if-eqz v0, :cond_2

    iget-object v0, v5, Lflipboard/samsung/spen/TOCSectionPreview;->d:Landroid/view/View;

    check-cast v0, Lflipboard/gui/toc/TileContainer;

    invoke-virtual {v0}, Lflipboard/gui/toc/TileContainer;->getCurrentTile()Lflipboard/gui/toc/TileView;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/toc/TileView;->getGradientIndex()I

    move-result v0

    iget-object v2, v5, Lflipboard/samsung/spen/TOCSectionPreview;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/samsung/spen/TOCSectionPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget-object v6, Lflipboard/gui/toc/TileView;->o:[I

    aget v0, v6, v0

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v0}, Lflipboard/gui/FLImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    :goto_2
    if-eqz v1, :cond_3

    iget-object v0, v5, Lflipboard/samsung/spen/TOCSectionPreview;->e:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, v5, Lflipboard/samsung/spen/TOCSectionPreview;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v5}, Lflipboard/samsung/spen/TOCSectionPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d00b7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_3
    iget-boolean v0, v5, Lflipboard/samsung/spen/TOCSectionPreview;->f:Z

    if-eqz v0, :cond_4

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v0, :cond_4

    iget-object v0, v5, Lflipboard/samsung/spen/TOCSectionPreview;->b:Lflipboard/gui/FLStaticTextView;

    const/4 v1, 0x0

    invoke-virtual {v5}, Lflipboard/samsung/spen/TOCSectionPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09004a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLStaticTextView;->a(II)V

    :cond_4
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    new-instance v2, Lflipboard/samsung/spen/TOCSectionPreview$SectionPreviewItem;

    invoke-direct {v2, v5, v0}, Lflipboard/samsung/spen/TOCSectionPreview$SectionPreviewItem;-><init>(Lflipboard/samsung/spen/TOCSectionPreview;Lflipboard/objs/FeedItem;)V

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x3

    if-lt v0, v2, :cond_5

    :cond_6
    iget-object v0, v5, Lflipboard/samsung/spen/TOCSectionPreview;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    const/4 v0, 0x0

    move v4, v0

    :goto_4
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_c

    invoke-virtual {v5}, Lflipboard/samsung/spen/TOCSectionPreview;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030146

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    const v0, 0x7f0a00c0

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lflipboard/gui/FLTextIntf;

    const v0, 0x7f0a0373

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    const v1, 0x7f0a0374

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    const v2, 0x7f0a0372

    invoke-virtual {v7, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-lez v4, :cond_7

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_7
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/samsung/spen/TOCSectionPreview$SectionPreviewItem;

    iget-object v8, v2, Lflipboard/samsung/spen/TOCSectionPreview$SectionPreviewItem;->a:Ljava/lang/String;

    invoke-interface {v3, v8}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v2, Lflipboard/samsung/spen/TOCSectionPreview$SectionPreviewItem;->b:Ljava/lang/String;

    if-eqz v3, :cond_b

    iget-object v3, v2, Lflipboard/samsung/spen/TOCSectionPreview$SectionPreviewItem;->b:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_b

    iget-object v3, v2, Lflipboard/samsung/spen/TOCSectionPreview$SectionPreviewItem;->b:Ljava/lang/String;

    invoke-interface {v0, v3}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    :goto_5
    invoke-virtual {v5}, Lflipboard/samsung/spen/TOCSectionPreview;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-wide v2, v2, Lflipboard/samsung/spen/TOCSectionPreview$SectionPreviewItem;->c:J

    const-wide/16 v8, 0x3e8

    mul-long/2addr v2, v8

    invoke-static {v0, v2, v3}, Lflipboard/util/JavaUtil;->d(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v5, Lflipboard/samsung/spen/TOCSectionPreview;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_4

    .line 328
    :cond_8
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f030144

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/samsung/spen/TOCSectionPreview;

    iput-object v1, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    goto/16 :goto_1

    .line 337
    :cond_9
    iget-object v0, v5, Lflipboard/samsung/spen/TOCSectionPreview;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    goto/16 :goto_2

    :cond_a
    iget-object v0, v5, Lflipboard/samsung/spen/TOCSectionPreview;->b:Lflipboard/gui/FLStaticTextView;

    iget-object v1, v5, Lflipboard/samsung/spen/TOCSectionPreview;->e:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_b
    const/16 v3, 0x8

    invoke-interface {v0, v3}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    goto :goto_5

    .line 340
    :cond_c
    instance-of v0, p1, Lflipboard/gui/toc/CoverStoryTileContainer;

    if-eqz v0, :cond_e

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v0, :cond_e

    .line 342
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Lflipboard/samsung/spen/TOCSectionPreview;->a(I)V

    .line 354
    :cond_d
    :goto_6
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-wide/16 v2, 0x3e8

    new-instance v1, Lflipboard/gui/toc/TOCPage$3;

    invoke-direct {v1, p0}, Lflipboard/gui/toc/TOCPage$3;-><init>(Lflipboard/gui/toc/TOCPage;)V

    invoke-virtual {v0, v2, v3, v1}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    goto/16 :goto_0

    .line 344
    :cond_e
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    const v1, 0x3f2aaaab

    const/high16 v2, 0x3f800000    # 1.0f

    const v3, 0x3f2aaaab

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    iget-object v6, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    invoke-direct {p0, v6}, Lflipboard/gui/toc/TOCPage;->a(Lflipboard/samsung/spen/TOCSectionPreview;)F

    move-result v6

    const/4 v7, 0x1

    iget-object v8, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    invoke-direct {p0, v8}, Lflipboard/gui/toc/TOCPage;->b(Lflipboard/samsung/spen/TOCSectionPreview;)F

    move-result v8

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 346
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 347
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    .line 348
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    invoke-virtual {v1, v0}, Lflipboard/samsung/spen/TOCSectionPreview;->startAnimation(Landroid/view/animation/Animation;)V

    .line 349
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Lflipboard/samsung/spen/TOCSectionPreview;->a(I)V

    goto :goto_6
.end method

.method public final a(Lflipboard/service/User;)V
    .locals 6

    .prologue
    const v5, 0x7f02005a

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 481
    invoke-virtual {p1}, Lflipboard/service/User;->b()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->w:Lflipboard/gui/FLImageView;

    if-eqz v0, :cond_4

    .line 482
    const-string v0, "flipboard"

    invoke-virtual {p1, v0}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 483
    invoke-virtual {v0}, Lflipboard/service/Account;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 484
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->w:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v5}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    .line 489
    :goto_0
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->c:Lflipboard/gui/FLTextView;

    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 490
    invoke-virtual {p1}, Lflipboard/service/User;->q()Ljava/util/List;

    move-result-object v0

    .line 492
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 493
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 495
    if-ne v1, v3, :cond_2

    .line 496
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0210

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 500
    :goto_1
    iget-object v2, p0, Lflipboard/gui/toc/TOCPage;->v:Lflipboard/gui/FLLabelTextView;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v0, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 501
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->v:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0, v4}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 525
    :cond_0
    :goto_2
    return-void

    .line 486
    :cond_1
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->w:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/service/Account;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    goto :goto_0

    .line 498
    :cond_2
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0211

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 503
    :cond_3
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->v:Lflipboard/gui/FLLabelTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    goto :goto_2

    .line 506
    :cond_4
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 508
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->c:Lflipboard/gui/FLTextView;

    if-eqz v1, :cond_5

    .line 509
    const v1, 0x7f0d0114

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 511
    iget-object v2, p0, Lflipboard/gui/toc/TOCPage;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v2, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 514
    :cond_5
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->v:Lflipboard/gui/FLLabelTextView;

    if-eqz v1, :cond_6

    .line 515
    const v1, 0x7f0d0139

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 517
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->v:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 518
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->v:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0, v4}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 521
    :cond_6
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->w:Lflipboard/gui/FLImageView;

    if-eqz v0, :cond_0

    .line 522
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->w:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    goto :goto_2
.end method

.method a(Ljava/util/List;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lflipboard/service/Section;",
            "Lflipboard/gui/toc/TileContainer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 560
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "TOCPage:setSections"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 561
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 564
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 565
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/toc/TileContainer;

    .line 566
    if-nez v1, :cond_1

    .line 567
    new-instance v1, Lflipboard/gui/toc/TileContainer;

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lflipboard/gui/toc/TileContainer;-><init>(Landroid/content/Context;Lflipboard/service/Section;)V

    iget-object v2, p0, Lflipboard/gui/toc/TOCPage;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v2}, Lflipboard/gui/toc/TOCView;->v()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lflipboard/gui/toc/DynamicGridLayout$State;->b:Lflipboard/gui/toc/DynamicGridLayout$State;

    invoke-virtual {v1, v2, v5}, Lflipboard/gui/toc/TileContainer;->a(Lflipboard/gui/toc/DynamicGridLayout$State;Z)V

    :cond_0
    invoke-virtual {v1, p0}, Lflipboard/gui/toc/TileContainer;->setSPenHoverListener(Lflipboard/samsung/spen/OnSamsungViewListener;)V

    iget-object v2, p0, Lflipboard/gui/toc/TOCPage;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 568
    iget-object v2, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v2, v1}, Lflipboard/gui/toc/DynamicGridLayout;->addView(Landroid/view/View;)V

    .line 580
    :goto_1
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 583
    invoke-virtual {v0, v5}, Lflipboard/service/Section;->b(Z)V

    goto :goto_0

    .line 569
    :cond_1
    iget-object v2, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-static {v2, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 571
    invoke-virtual {v1}, Lflipboard/gui/toc/TileContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v2, v1}, Lflipboard/gui/toc/DynamicGridLayout;->removeView(Landroid/view/View;)V

    .line 572
    iget-object v2, v1, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-interface {p2, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 573
    iget-object v2, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v2, v1}, Lflipboard/gui/toc/DynamicGridLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 576
    :cond_2
    iget-object v2, v1, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-interface {p2, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 585
    :cond_3
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v0, v3}, Lflipboard/gui/toc/DynamicGridLayout;->setGridOrder(Ljava/util/List;)V

    .line 586
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v0}, Lflipboard/gui/toc/DynamicGridLayout;->clearDisappearingChildren()V

    .line 587
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->p:Lflipboard/gui/toc/TileContainer$MoreTileContainer;

    if-eqz v0, :cond_4

    .line 589
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->p:Lflipboard/gui/toc/TileContainer$MoreTileContainer;

    invoke-virtual {v0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->bringToFront()V

    .line 591
    :cond_4
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v0}, Lflipboard/gui/toc/DynamicGridLayout;->requestLayout()V

    .line 593
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 594
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addAll(Ljava/util/Collection;)Z

    .line 595
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 139
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Updating the no-internet view on page "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lflipboard/gui/toc/TOCPage;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-static {}, Lflipboard/service/FlipboardManager;->g()V

    .line 140
    if-eqz p1, :cond_1

    .line 141
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->k:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 144
    :cond_0
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->l:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 151
    :goto_0
    return-void

    .line 146
    :cond_1
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->k:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 147
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->k:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 149
    :cond_2
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(ZI)V
    .locals 3

    .prologue
    .line 687
    if-eqz p1, :cond_1

    if-nez p2, :cond_1

    .line 689
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/TOCActivity;

    .line 690
    iget-object v1, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "light_box"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "light_box"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    .line 691
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->a()V

    .line 698
    :cond_0
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v0, :cond_1

    .line 702
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "TOCPage:onPageOffsetChange"

    new-instance v2, Lflipboard/gui/toc/TOCPage$6;

    invoke-direct {v2, p0}, Lflipboard/gui/toc/TOCPage$6;-><init>(Lflipboard/gui/toc/TOCPage;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 720
    :cond_1
    return-void

    .line 690
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/16 v9, 0x12c

    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3f2aaaab

    const/4 v5, 0x1

    .line 370
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    if-eqz v0, :cond_2

    .line 371
    sget-object v0, Lflipboard/gui/toc/TOCPage;->e:Lflipboard/util/Log;

    .line 374
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v0

    .line 375
    instance-of v0, v0, Lflipboard/gui/toc/CoverStoryTileContainer;

    if-eqz v0, :cond_1

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v0, :cond_1

    .line 377
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    invoke-virtual {v0, v9}, Lflipboard/samsung/spen/TOCSectionPreview;->b(I)V

    .line 387
    :goto_0
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    .line 388
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-wide/16 v2, 0x12c

    new-instance v4, Lflipboard/gui/toc/TOCPage$4;

    invoke-direct {v4, p0, v0}, Lflipboard/gui/toc/TOCPage$4;-><init>(Lflipboard/gui/toc/TOCPage;Lflipboard/samsung/spen/TOCSectionPreview;)V

    invoke-virtual {v1, v2, v3, v4}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    .line 398
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    iget-object v1, v0, Lflipboard/samsung/spen/TOCSectionPreview;->h:Lflipboard/samsung/spen/OnSamsungViewListener;

    if-ne v1, p0, :cond_0

    iput-object v10, v0, Lflipboard/samsung/spen/TOCSectionPreview;->h:Lflipboard/samsung/spen/OnSamsungViewListener;

    .line 399
    :cond_0
    iput-object v10, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    .line 403
    :goto_1
    return-void

    .line 379
    :cond_1
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    iget-object v3, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    invoke-direct {p0, v3}, Lflipboard/gui/toc/TOCPage;->a(Lflipboard/samsung/spen/TOCSectionPreview;)F

    move-result v6

    iget-object v3, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    invoke-direct {p0, v3}, Lflipboard/gui/toc/TOCPage;->b(Lflipboard/samsung/spen/TOCSectionPreview;)F

    move-result v8

    move v3, v1

    move v4, v2

    move v7, v5

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 381
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 382
    invoke-virtual {v0, v5}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 383
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    invoke-virtual {v1, v0}, Lflipboard/samsung/spen/TOCSectionPreview;->startAnimation(Landroid/view/animation/Animation;)V

    .line 384
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    invoke-virtual {v0, v9}, Lflipboard/samsung/spen/TOCSectionPreview;->b(I)V

    goto :goto_0

    .line 401
    :cond_2
    sget-object v0, Lflipboard/gui/toc/TOCPage;->e:Lflipboard/util/Log;

    goto :goto_1
.end method

.method public final b(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 433
    sget-object v0, Lflipboard/gui/toc/TOCPage;->e:Lflipboard/util/Log;

    .line 437
    instance-of v0, p1, Lflipboard/gui/toc/TileContainer;

    if-eqz v0, :cond_0

    .line 439
    sget-object v0, Lflipboard/gui/toc/TOCPage;->e:Lflipboard/util/Log;

    .line 441
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->b()V

    .line 443
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-wide/16 v2, 0x1f4

    new-instance v1, Lflipboard/gui/toc/TOCPage$5;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/toc/TOCPage$5;-><init>(Lflipboard/gui/toc/TOCPage;Landroid/view/View;)V

    invoke-virtual {v0, v2, v3, v1}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    .line 459
    :cond_0
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 463
    instance-of v0, p1, Lflipboard/samsung/spen/TOCSectionPreview;

    if-eqz v0, :cond_0

    .line 464
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    if-eqz v0, :cond_0

    .line 465
    sget-object v0, Lflipboard/gui/toc/TOCPage;->e:Lflipboard/util/Log;

    .line 466
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->b()V

    .line 469
    :cond_0
    return-void
.end method

.method protected getLatestSectionPreview()Landroid/view/View;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->q:Lflipboard/samsung/spen/TOCSectionPreview;

    return-object v0
.end method

.method public getPageNumber()I
    .locals 1

    .prologue
    .line 528
    iget v0, p0, Lflipboard/gui/toc/TOCPage;->g:I

    return v0
.end method

.method public getSections()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;"
        }
    .end annotation

    .prologue
    .line 546
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 104
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onAttachedToWindow()V

    .line 105
    new-instance v0, Lflipboard/gui/toc/TOCPage$1;

    invoke-direct {v0, p0}, Lflipboard/gui/toc/TOCPage$1;-><init>(Lflipboard/gui/toc/TOCPage;)V

    iput-object v0, p0, Lflipboard/gui/toc/TOCPage;->b:Lflipboard/util/Observer;

    .line 118
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->b:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/io/NetworkManager;->a(Lflipboard/util/Observer;)V

    .line 119
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCPage;->a(Z)V

    .line 121
    new-instance v0, Lflipboard/gui/toc/TOCPage$2;

    invoke-direct {v0, p0}, Lflipboard/gui/toc/TOCPage$2;-><init>(Lflipboard/gui/toc/TOCPage;)V

    iput-object v0, p0, Lflipboard/gui/toc/TOCPage;->a:Lflipboard/util/Observer;

    .line 134
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->a:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Lflipboard/util/Observer;)V

    .line 135
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 155
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onDetachedFromWindow()V

    .line 157
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->b:Lflipboard/util/Observer;

    if-eqz v0, :cond_0

    .line 158
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->b:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/io/NetworkManager;->b(Lflipboard/util/Observer;)V

    .line 159
    iput-object v2, p0, Lflipboard/gui/toc/TOCPage;->b:Lflipboard/util/Observer;

    .line 162
    :cond_0
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->a:Lflipboard/util/Observer;

    if-eqz v0, :cond_1

    .line 163
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->a:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->c(Lflipboard/util/Observer;)V

    .line 164
    iput-object v2, p0, Lflipboard/gui/toc/TOCPage;->a:Lflipboard/util/Observer;

    .line 167
    :cond_1
    return-void
.end method

.method protected onFinishInflate()V
    .locals 8

    .prologue
    const/4 v1, 0x3

    const v7, 0x7f090132

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 189
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    if-nez v0, :cond_1

    .line 190
    const v0, 0x7f0a0363

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/DynamicGridLayout;

    iput-object v0, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    .line 191
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_5

    .line 192
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_4

    .line 193
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b000f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/toc/DynamicGridLayout;->a(II)V

    .line 197
    :goto_0
    const v0, 0x7f0a0355

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCSearchBoxContainer;

    iput-object v0, p0, Lflipboard/gui/toc/TOCPage;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    .line 198
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    invoke-virtual {v0, v5}, Lflipboard/gui/toc/TOCSearchBoxContainer;->setClickable(Z)V

    .line 200
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    new-instance v1, Lflipboard/gui/toc/TOCPage$SearchBoxOnClickListener;

    invoke-direct {v1, p0}, Lflipboard/gui/toc/TOCPage$SearchBoxOnClickListener;-><init>(Lflipboard/gui/toc/TOCPage;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/TOCSearchBoxContainer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    :cond_0
    :goto_1
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_7

    .line 211
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 212
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v1, v4, v0, v4, v4}, Lflipboard/gui/toc/DynamicGridLayout;->setPadding(IIII)V

    .line 220
    :cond_1
    :goto_2
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 221
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    iput v0, v1, Lflipboard/gui/toc/DynamicGridLayout;->d:I

    iput v0, v1, Lflipboard/gui/toc/DynamicGridLayout;->e:I

    .line 223
    const v0, 0x7f0a0365

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, p0, Lflipboard/gui/toc/TOCPage;->j:Lflipboard/gui/FLTextIntf;

    .line 225
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->j:Lflipboard/gui/FLTextIntf;

    if-eqz v0, :cond_2

    .line 226
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->j:Lflipboard/gui/FLTextIntf;

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0328

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    iget v3, p0, Lflipboard/gui/toc/TOCPage;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, Lflipboard/gui/toc/TOCPage;->h:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 229
    :cond_2
    const v0, 0x7f0a0364

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/toc/TOCPage;->k:Landroid/view/View;

    .line 230
    const v0, 0x7f0a0352

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/toc/TOCPage;->l:Landroid/view/View;

    .line 231
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCPage;->a(Z)V

    .line 233
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 234
    const v1, 0x7f0a004c

    invoke-virtual {p0, v1}, Lflipboard/gui/toc/TOCPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/actionbar/FLActionBar;

    .line 236
    if-eqz v1, :cond_3

    .line 237
    new-instance v2, Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-direct {v2, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;-><init>(Landroid/content/Context;)V

    .line 238
    invoke-virtual {v0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v3, 0x7f0f000a

    invoke-virtual {v0, v3, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 239
    const v0, 0x7f0a03b6

    invoke-virtual {v2, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    invoke-virtual {v0, v6}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 240
    const v0, 0x7f0a03b7

    invoke-virtual {v2, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    invoke-virtual {v0, v4}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 242
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 243
    const/high16 v3, 0x7f0f0000

    invoke-virtual {v0, v3, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 244
    const v0, 0x7f0a0398

    invoke-virtual {v2, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    invoke-virtual {v0, v4}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 245
    const v0, 0x7f0a0399

    invoke-virtual {v2, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    invoke-virtual {v0, v4}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 246
    const v0, 0x7f0a0399

    invoke-virtual {v2, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    sget-boolean v3, Lflipboard/service/FlipboardManager;->o:Z

    iput-boolean v3, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    .line 247
    invoke-virtual {v1, v2}, Lflipboard/gui/actionbar/FLActionBar;->setMenu(Lflipboard/gui/actionbar/FLActionBarMenu;)V

    .line 248
    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->d:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    invoke-virtual {v1, v0}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    .line 250
    :cond_3
    const v0, 0x7f0a00fe

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/toc/TOCPage;->c:Lflipboard/gui/FLTextView;

    .line 251
    const v0, 0x7f0a0358

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/toc/TOCPage;->v:Lflipboard/gui/FLLabelTextView;

    .line 252
    const v0, 0x7f0a0237

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/toc/TOCPage;->w:Lflipboard/gui/FLImageView;

    .line 253
    const v0, 0x7f0a0354

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflipboard/gui/toc/TOCPage;->t:Landroid/widget/ImageView;

    .line 254
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCPage;->a(Lflipboard/service/User;)V

    .line 256
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onFinishInflate()V

    .line 257
    return-void

    .line 195
    :cond_4
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/toc/DynamicGridLayout;->a(II)V

    goto/16 :goto_0

    .line 203
    :cond_5
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_6

    .line 204
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v0, v1, v6}, Lflipboard/gui/toc/DynamicGridLayout;->a(II)V

    goto/16 :goto_1

    .line 206
    :cond_6
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v0, v6, v1}, Lflipboard/gui/toc/DynamicGridLayout;->a(II)V

    goto/16 :goto_1

    .line 214
    :cond_7
    const-wide/high16 v0, 0x3ff8000000000000L    # 1.5

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 215
    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v1, v4, v0, v4, v4}, Lflipboard/gui/toc/DynamicGridLayout;->setPadding(IIII)V

    goto/16 :goto_2
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 269
    invoke-super/range {p0 .. p5}, Lflipboard/gui/FLRelativeLayout;->onLayout(ZIIII)V

    .line 270
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/samsung/spen/TOCSectionPreview;

    .line 272
    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lflipboard/util/AndroidUtil;->c(Landroid/view/View;)[I

    move-result-object v2

    .line 273
    aget v1, v2, v8

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v1, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v1, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v1, v4

    .line 274
    aget v4, v2, v8

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v5

    add-int/2addr v4, v5

    .line 276
    iget-object v5, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v5}, Lflipboard/gui/toc/DynamicGridLayout;->getTop()I

    move-result v5

    add-int/2addr v5, p3

    if-ge v4, v5, :cond_0

    .line 277
    aget v1, v2, v8

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v4

    sub-int/2addr v1, v4

    .line 284
    :goto_1
    aget v4, v2, v7

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v5

    add-int/2addr v4, v5

    .line 285
    aget v5, v2, v7

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    .line 287
    iget-object v6, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v6}, Lflipboard/gui/toc/DynamicGridLayout;->getLeft()I

    move-result v6

    if-ge v4, v6, :cond_2

    .line 288
    aget v2, v2, v7

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v2, v4

    .line 295
    :goto_2
    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v0, v2, v1, v4, v5}, Lflipboard/samsung/spen/TOCSectionPreview;->layout(IIII)V

    goto/16 :goto_0

    .line 278
    :cond_0
    iget-object v4, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v4}, Lflipboard/gui/toc/DynamicGridLayout;->getBottom()I

    move-result v4

    add-int/2addr v4, p3

    if-le v1, v4, :cond_1

    .line 279
    aget v1, v2, v8

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v1, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v1, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingBottom()I

    move-result v4

    add-int/2addr v1, v4

    goto :goto_1

    .line 281
    :cond_1
    aget v1, v2, v8

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v1, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v1, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v4

    sub-int/2addr v1, v4

    goto/16 :goto_1

    .line 289
    :cond_2
    iget-object v4, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v4}, Lflipboard/gui/toc/DynamicGridLayout;->getRight()I

    move-result v4

    if-le v5, v4, :cond_3

    .line 290
    aget v2, v2, v7

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v2, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v2, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingRight()I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_2

    .line 292
    :cond_3
    aget v2, v2, v7

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v2, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v2, v4

    goto/16 :goto_2

    .line 297
    :cond_4
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 261
    invoke-super {p0, p1, p2}, Lflipboard/gui/FLRelativeLayout;->onMeasure(II)V

    .line 262
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/samsung/spen/TOCSectionPreview;

    .line 263
    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    mul-int/lit8 v3, v3, 0x3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lflipboard/samsung/spen/TOCSectionPreview;->measure(II)V

    goto :goto_0

    .line 265
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 170
    if-eqz p1, :cond_1

    .line 171
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->t:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->t:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 174
    :cond_0
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    if-eqz v0, :cond_1

    .line 175
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/TOCSearchBoxContainer;->setVisibility(I)V

    .line 178
    :cond_1
    return-void
.end method

.method public setLast(Z)V
    .locals 2

    .prologue
    .line 637
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "TOCPage:setLast"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 638
    iput-boolean p1, p0, Lflipboard/gui/toc/TOCPage;->u:Z

    .line 639
    if-eqz p1, :cond_1

    .line 640
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->p:Lflipboard/gui/toc/TileContainer$MoreTileContainer;

    if-nez v0, :cond_0

    .line 641
    new-instance v0, Lflipboard/gui/toc/TileContainer$MoreTileContainer;

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCPage;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lflipboard/gui/toc/TileContainer$MoreTileContainer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/toc/TOCPage;->p:Lflipboard/gui/toc/TileContainer$MoreTileContainer;

    .line 642
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->p:Lflipboard/gui/toc/TileContainer$MoreTileContainer;

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/DynamicGridLayout;->addView(Landroid/view/View;)V

    .line 650
    :cond_0
    :goto_0
    return-void

    .line 645
    :cond_1
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->p:Lflipboard/gui/toc/TileContainer$MoreTileContainer;

    if-eqz v0, :cond_0

    .line 646
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    iget-object v1, p0, Lflipboard/gui/toc/TOCPage;->p:Lflipboard/gui/toc/TileContainer$MoreTileContainer;

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/DynamicGridLayout;->removeView(Landroid/view/View;)V

    .line 647
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/toc/TOCPage;->p:Lflipboard/gui/toc/TileContainer$MoreTileContainer;

    goto :goto_0
.end method

.method public setMaxTiles(I)V
    .locals 0

    .prologue
    .line 623
    iput p1, p0, Lflipboard/gui/toc/TOCPage;->m:I

    .line 624
    return-void
.end method

.method public setMoreTileCount(I)V
    .locals 1

    .prologue
    .line 654
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->p:Lflipboard/gui/toc/TileContainer$MoreTileContainer;

    if-eqz v0, :cond_0

    .line 655
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage;->p:Lflipboard/gui/toc/TileContainer$MoreTileContainer;

    invoke-virtual {v0, p1}, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->setCount(I)V

    .line 657
    :cond_0
    return-void
.end method

.method protected setPaginator(I)V
    .locals 1

    .prologue
    .line 533
    iget v0, p0, Lflipboard/gui/toc/TOCPage;->g:I

    invoke-virtual {p0, v0, p1}, Lflipboard/gui/toc/TOCPage;->a(II)V

    .line 534
    return-void
.end method

.method public setTocView(Lflipboard/gui/toc/TOCView;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lflipboard/gui/toc/TOCPage;->o:Lflipboard/gui/toc/TOCView;

    .line 183
    return-void
.end method
