.class public Lflipboard/gui/toc/DynamicGridLayout$DragManager;
.super Ljava/lang/Object;
.source "DynamicGridLayout.java"


# instance fields
.field a:I

.field b:I

.field c:Landroid/view/View;

.field d:Landroid/view/View;

.field e:I

.field f:I

.field g:I

.field h:I

.field i:I

.field j:I

.field k:I

.field l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field m:Landroid/view/ViewGroup;

.field n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/toc/DynamicGridLayout;",
            ">;"
        }
    .end annotation
.end field

.field o:I

.field p:Lflipboard/gui/toc/DynamicGridLayout$State;

.field q:I


# direct methods
.method constructor <init>(Landroid/view/ViewGroup;Ljava/util/List;ILflipboard/gui/toc/DynamicGridLayout$State;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/toc/DynamicGridLayout;",
            ">;I",
            "Lflipboard/gui/toc/DynamicGridLayout$State;",
            ")V"
        }
    .end annotation

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    iput-object p1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->m:Landroid/view/ViewGroup;

    .line 126
    iput-object p2, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->n:Ljava/util/List;

    .line 127
    iput p3, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->o:I

    .line 128
    if-nez p4, :cond_0

    sget-object p4, Lflipboard/gui/toc/DynamicGridLayout$State;->a:Lflipboard/gui/toc/DynamicGridLayout$State;

    :cond_0
    iput-object p4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->p:Lflipboard/gui/toc/DynamicGridLayout$State;

    .line 129
    return-void
.end method

.method private a(Landroid/graphics/Rect;)V
    .locals 6

    .prologue
    .line 288
    iget-object v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->d:Landroid/view/View;

    .line 289
    iget-object v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->n:Ljava/util/List;

    iget v2, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->o:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/DynamicGridLayout;

    iget-object v0, v0, Lflipboard/gui/toc/DynamicGridLayout;->j:Landroid/graphics/Rect;

    .line 291
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    iget v4, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v4

    iget v5, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v5

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    invoke-virtual {p1, v2, v3, v4, v0}, Landroid/graphics/Rect;->union(IIII)V

    .line 292
    return-void
.end method

.method static synthetic a(Lflipboard/gui/toc/DynamicGridLayout$DragManager;Lflipboard/gui/toc/DynamicGridLayout$State;Lflipboard/gui/toc/DynamicGridLayout$State;)V
    .locals 0

    .prologue
    .line 108
    invoke-virtual {p0, p1, p2}, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a(Lflipboard/gui/toc/DynamicGridLayout$State;Lflipboard/gui/toc/DynamicGridLayout$State;)V

    return-void
.end method


# virtual methods
.method final a(II)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 299
    iget-object v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->n:Ljava/util/List;

    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->o:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/DynamicGridLayout;

    .line 300
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 301
    invoke-direct {p0, v1}, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a(Landroid/graphics/Rect;)V

    .line 304
    iget-object v2, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    iget v3, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->e:I

    add-int/2addr v3, p1

    iget v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->f:I

    add-int/2addr v4, p2

    invoke-virtual {v2, p1, p2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 307
    iget-object v2, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->d:Landroid/view/View;

    iget v3, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->i:I

    add-int/2addr v3, p1

    iget v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->j:I

    add-int/2addr v4, p2

    iget v5, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->i:I

    add-int/2addr v5, p1

    iget v6, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->e:I

    add-int/2addr v5, v6

    iget v6, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->j:I

    add-int/2addr v6, p2

    iget v7, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->f:I

    add-int/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 309
    invoke-direct {p0, v1}, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a(Landroid/graphics/Rect;)V

    .line 310
    iget-object v2, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->m:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->invalidate(Landroid/graphics/Rect;)V

    .line 314
    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->e:I

    div-int/lit8 v1, v1, 0x2

    add-int v3, p1, v1

    .line 315
    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->f:I

    div-int/lit8 v1, v1, 0x2

    add-int v4, p2, v1

    .line 317
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 318
    iget-object v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    add-int/lit8 v2, v1, -0x1

    if-ltz v2, :cond_3

    .line 319
    iget-object v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->l:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 320
    instance-of v6, v1, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;

    if-eqz v6, :cond_4

    .line 321
    invoke-static {v0, v2, v5}, Lflipboard/gui/toc/DynamicGridLayout;->a(Lflipboard/gui/toc/DynamicGridLayout;ILandroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 325
    invoke-virtual {v5, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 326
    iget v3, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->k:I

    if-eq v2, v3, :cond_3

    check-cast v1, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;

    invoke-interface {v1}, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 327
    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->k:I

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 328
    sget-object v1, Lflipboard/gui/toc/DynamicGridLayout;->a:Lflipboard/util/Log;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->k:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v9

    .line 329
    iget-object v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->l:Ljava/util/List;

    iget v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->k:I

    invoke-interface {v1, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    iget-object v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    if-eq v1, v4, :cond_0

    .line 330
    sget-object v1, Lflipboard/gui/toc/DynamicGridLayout;->a:Lflipboard/util/Log;

    new-array v1, v9, [Ljava/lang/Object;

    iget v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->k:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v8

    .line 332
    :cond_0
    iput v2, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->k:I

    .line 333
    iget-object v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->l:Ljava/util/List;

    iget v2, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->k:I

    iget-object v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    invoke-interface {v1, v2, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 335
    iget-object v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    :goto_1
    add-int/lit8 v2, v1, -0x1

    if-lt v2, v3, :cond_3

    .line 336
    iget-object v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->l:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 337
    iget-object v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    if-eq v1, v4, :cond_1

    .line 338
    const/4 v4, 0x0

    invoke-static {v0, v2, v4}, Lflipboard/gui/toc/DynamicGridLayout;->a(Lflipboard/gui/toc/DynamicGridLayout;ILandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-static {}, Lflipboard/gui/toc/DynamicGridLayout;->a()I

    move-result v5

    iget-object v6, v0, Lflipboard/gui/toc/DynamicGridLayout;->j:Landroid/graphics/Rect;

    invoke-static {v1, v4, v5, v6}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;Landroid/graphics/Rect;ILandroid/graphics/Rect;)V

    :cond_1
    move v1, v2

    .line 340
    goto :goto_1

    :cond_2
    move v1, v2

    .line 344
    goto/16 :goto_0

    .line 345
    :cond_3
    return-void

    :cond_4
    move v1, v2

    goto/16 :goto_0
.end method

.method final a(Lflipboard/gui/toc/DynamicGridLayout$State;Lflipboard/gui/toc/DynamicGridLayout$State;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 528
    iget-object v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/DynamicGridLayout;

    .line 529
    invoke-virtual {v0}, Lflipboard/gui/toc/DynamicGridLayout;->getChildCount()I

    move-result v1

    :goto_0
    add-int/lit8 v3, v1, -0x1

    if-ltz v3, :cond_0

    .line 530
    invoke-virtual {v0, v3}, Lflipboard/gui/toc/DynamicGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 531
    instance-of v1, v2, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;

    if-eqz v1, :cond_2

    move-object v1, v2

    .line 532
    check-cast v1, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;

    .line 533
    iget-object v5, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    if-ne v2, v5, :cond_1

    .line 534
    invoke-interface {v1, p1, v6}, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;->a(Lflipboard/gui/toc/DynamicGridLayout$State;Z)V

    move v1, v3

    goto :goto_0

    .line 536
    :cond_1
    invoke-interface {v1, p2, v6}, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;->a(Lflipboard/gui/toc/DynamicGridLayout$State;Z)V

    :cond_2
    move v1, v3

    .line 539
    goto :goto_0

    .line 541
    :cond_3
    return-void
.end method

.method final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 548
    new-instance v0, Lflipboard/gui/toc/DynamicGridLayout$DragManager$7;

    invoke-direct {v0, p0}, Lflipboard/gui/toc/DynamicGridLayout$DragManager$7;-><init>(Lflipboard/gui/toc/DynamicGridLayout$DragManager;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 557
    return-void
.end method

.method final a(Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 415
    iget v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->o:I

    if-gez v0, :cond_0

    .line 416
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Trying to access page -1, skipping"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 459
    :goto_0
    return-void

    .line 419
    :cond_0
    iget-object v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->n:Ljava/util/List;

    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->o:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/DynamicGridLayout;

    .line 420
    invoke-virtual {v0}, Lflipboard/gui/toc/DynamicGridLayout;->getChildCount()I

    move-result v3

    .line 424
    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a:I

    iget v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->i:I

    add-int/2addr v1, v4

    iput v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a:I

    .line 425
    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->b:I

    iget v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->j:I

    add-int/2addr v1, v4

    iput v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->b:I

    .line 428
    iput v2, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->j:I

    iput v2, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->i:I

    move-object v1, v0

    .line 429
    :goto_1
    iget-object v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->m:Landroid/view/ViewGroup;

    if-eq v1, v4, :cond_1

    .line 430
    iget v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->i:I

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->i:I

    .line 431
    iget v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->j:I

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->j:I

    .line 432
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    goto :goto_1

    .line 435
    :cond_1
    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a:I

    iget v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->i:I

    sub-int/2addr v1, v4

    iput v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a:I

    .line 436
    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->b:I

    iget v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->j:I

    sub-int/2addr v1, v4

    iput v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->b:I

    .line 439
    iget v1, v0, Lflipboard/gui/toc/DynamicGridLayout;->h:I

    iput v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->q:I

    .line 441
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->l:Ljava/util/List;

    move v1, v2

    .line 444
    :goto_2
    if-ge v1, v3, :cond_4

    .line 445
    invoke-virtual {v0, v1}, Lflipboard/gui/toc/DynamicGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 446
    iget-object v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->l:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 448
    iget-object v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    if-ne v2, v4, :cond_3

    .line 449
    if-eqz p1, :cond_2

    .line 450
    iget v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a:I

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->g:I

    .line 451
    iget v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->b:I

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int v2, v4, v2

    iput v2, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->h:I

    .line 453
    :cond_2
    iput v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->k:I

    .line 444
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 458
    :cond_4
    iget-object v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->l:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a(Ljava/util/List;)V

    goto/16 :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 243
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 254
    :cond_0
    :goto_0
    return v7

    .line 246
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->b:I

    iget v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a:I

    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->i:I

    sub-int/2addr v0, v1

    iput v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a:I

    iget v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->b:I

    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->j:I

    sub-int/2addr v0, v1

    iput v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->b:I

    iget-object v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->q:I

    iget-object v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->n:Ljava/util/List;

    iget v2, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->o:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/DynamicGridLayout;

    iget v0, v0, Lflipboard/gui/toc/DynamicGridLayout;->h:I

    if-eq v1, v0, :cond_1

    invoke-virtual {p0, v7}, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a(Z)V

    :cond_1
    iget v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a:I

    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->g:I

    sub-int/2addr v0, v1

    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->b:I

    iget v2, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->h:I

    sub-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a(II)V

    goto :goto_0

    .line 250
    :pswitch_1
    iget-object v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->n:Ljava/util/List;

    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->o:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/DynamicGridLayout;

    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->q:I

    iget v2, v0, Lflipboard/gui/toc/DynamicGridLayout;->h:I

    if-eq v1, v2, :cond_2

    invoke-virtual {p0, v7}, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a(Z)V

    :cond_2
    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->k:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lflipboard/gui/toc/DynamicGridLayout;->a(Lflipboard/gui/toc/DynamicGridLayout;ILandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v2

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3, v2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iget v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->i:I

    iget v4, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->j:I

    invoke-virtual {v3, v1, v4}, Landroid/graphics/Rect;->offset(II)V

    iget-object v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->d:Landroid/view/View;

    instance-of v1, v1, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->d:Landroid/view/View;

    check-cast v1, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;

    sget-object v4, Lflipboard/gui/toc/DynamicGridLayout$State;->b:Lflipboard/gui/toc/DynamicGridLayout$State;

    const/4 v5, 0x1

    invoke-interface {v1, v4, v5}, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;->a(Lflipboard/gui/toc/DynamicGridLayout$State;Z)V

    :cond_3
    iget-object v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    iget v4, v2, Landroid/graphics/Rect;->left:I

    iget v5, v2, Landroid/graphics/Rect;->top:I

    iget v6, v2, Landroid/graphics/Rect;->right:I

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v4, v5, v6, v2}, Landroid/view/View;->layout(IIII)V

    iget-object v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    check-cast v1, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;

    sget-object v2, Lflipboard/gui/toc/DynamicGridLayout$State;->b:Lflipboard/gui/toc/DynamicGridLayout$State;

    invoke-interface {v1, v2, v7}, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;->a(Lflipboard/gui/toc/DynamicGridLayout$State;Z)V

    sget-object v1, Lflipboard/gui/toc/DynamicGridLayout$State;->b:Lflipboard/gui/toc/DynamicGridLayout$State;

    iput-object v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->p:Lflipboard/gui/toc/DynamicGridLayout$State;

    iget-object v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->d:Landroid/view/View;

    invoke-static {}, Lflipboard/gui/toc/DynamicGridLayout;->a()I

    move-result v2

    iget-object v4, v0, Lflipboard/gui/toc/DynamicGridLayout;->j:Landroid/graphics/Rect;

    new-instance v5, Lflipboard/gui/toc/DynamicGridLayout$DragManager$6;

    invoke-direct {v5, p0}, Lflipboard/gui/toc/DynamicGridLayout$DragManager$6;-><init>(Lflipboard/gui/toc/DynamicGridLayout$DragManager;)V

    invoke-static {v1, v3, v2, v4, v5}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;Landroid/graphics/Rect;ILandroid/graphics/Rect;Ljava/lang/Runnable;)V

    iget-object v1, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->l:Ljava/util/List;

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/DynamicGridLayout;->setGridOrder(Ljava/util/List;)V

    iget-object v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->m:Landroid/view/ViewGroup;

    instance-of v0, v0, Lflipboard/gui/toc/DynamicGridLayout$DragContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->m:Landroid/view/ViewGroup;

    check-cast v0, Lflipboard/gui/toc/DynamicGridLayout$DragContainer;

    invoke-interface {v0}, Lflipboard/gui/toc/DynamicGridLayout$DragContainer;->v_()V

    goto/16 :goto_0

    .line 243
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
