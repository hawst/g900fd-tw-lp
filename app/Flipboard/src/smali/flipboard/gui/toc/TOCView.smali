.class public Lflipboard/gui/toc/TOCView;
.super Lflipboard/gui/flipping/FlipTransitionViews;
.source "TOCView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lflipboard/gui/toc/DynamicGridLayout$DragContainer;


# static fields
.field public static final a:Lflipboard/util/Log;


# instance fields
.field K:I

.field L:I

.field public final M:Lflipboard/gui/toc/CoverPage;

.field final N:Landroid/view/ViewGroup$LayoutParams;

.field O:Landroid/content/Context;

.field P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

.field Q:Lflipboard/gui/toc/DynamicGridLayout$State;

.field final R:I

.field S:I

.field private T:Z

.field private final U:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;"
        }
    .end annotation
.end field

.field private final V:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/toc/TOCPage;",
            ">;"
        }
    .end annotation
.end field

.field private W:Lflipboard/gui/flipping/SinglePage;

.field private final aa:I

.field private ab:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FlipboardManager;",
            "Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private ac:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-string v0, "toc"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/toc/TOCView;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x6

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 69
    invoke-direct {p0, p1}, Lflipboard/gui/flipping/FlipTransitionViews;-><init>(Landroid/content/Context;)V

    .line 519
    const/high16 v0, 0x41f00000    # 30.0f

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v0, v3}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v0

    iput v0, p0, Lflipboard/gui/toc/TOCView;->R:I

    .line 636
    const/high16 v0, 0x40c00000    # 6.0f

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v0, v3}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v0

    iput v0, p0, Lflipboard/gui/toc/TOCView;->S:I

    .line 70
    iput-object p1, p0, Lflipboard/gui/toc/TOCView;->O:Landroid/content/Context;

    .line 71
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenHeightDp:I

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->screenWidthDp:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/16 v3, 0x210

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lflipboard/gui/toc/TOCView;->T:Z

    .line 72
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_1

    .line 74
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b000e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b000f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    mul-int/2addr v0, v3

    iput v0, p0, Lflipboard/gui/toc/TOCView;->K:I

    .line 75
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b000c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b000d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    mul-int/2addr v0, v3

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lflipboard/gui/toc/TOCView;->L:I

    .line 77
    iput-boolean v2, p0, Lflipboard/gui/toc/TOCView;->x:Z

    .line 78
    const v0, 0x7f030138

    invoke-static {p1, v0, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/CoverPage;

    iput-object v0, p0, Lflipboard/gui/toc/TOCView;->M:Lflipboard/gui/toc/CoverPage;

    .line 79
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->M:Lflipboard/gui/toc/CoverPage;

    invoke-virtual {v0}, Lflipboard/gui/toc/CoverPage;->a()V

    .line 80
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->M:Lflipboard/gui/toc/CoverPage;

    new-instance v3, Lflipboard/gui/toc/TOCView$1;

    invoke-direct {v3, p0}, Lflipboard/gui/toc/TOCView$1;-><init>(Lflipboard/gui/toc/TOCView;)V

    invoke-virtual {v0, v3}, Lflipboard/gui/toc/CoverPage;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->M:Lflipboard/gui/toc/CoverPage;

    invoke-virtual {p0, v5, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    .line 98
    :goto_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    .line 99
    const-string v3, "limit_toc_pages"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    .line 100
    iput v5, p0, Lflipboard/gui/toc/TOCView;->aa:I

    .line 107
    :goto_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/toc/TOCView;->U:Ljava/util/List;

    .line 108
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    .line 109
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lflipboard/gui/toc/TOCView;->N:Landroid/view/ViewGroup$LayoutParams;

    .line 111
    invoke-virtual {p0, p0}, Lflipboard/gui/toc/TOCView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 112
    invoke-virtual {p0, p0}, Lflipboard/gui/toc/TOCView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    invoke-virtual {p0, v2}, Lflipboard/gui/toc/TOCView;->setClipChildren(Z)V

    .line 116
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/toc/TOCView$2;

    invoke-direct {v1, p0}, Lflipboard/gui/toc/TOCView$2;-><init>(Lflipboard/gui/toc/TOCView;)V

    iput-object v1, p0, Lflipboard/gui/toc/TOCView;->ab:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/util/Observer;)V

    .line 147
    invoke-direct {p0}, Lflipboard/gui/toc/TOCView;->x()V

    .line 148
    return-void

    :cond_0
    move v0, v2

    .line 71
    goto/16 :goto_0

    .line 88
    :cond_1
    iput-object v7, p0, Lflipboard/gui/toc/TOCView;->M:Lflipboard/gui/toc/CoverPage;

    .line 89
    iput v6, p0, Lflipboard/gui/toc/TOCView;->K:I

    .line 90
    iget-boolean v0, p0, Lflipboard/gui/toc/TOCView;->T:Z

    if-eqz v0, :cond_2

    .line 91
    const/4 v0, 0x5

    iput v0, p0, Lflipboard/gui/toc/TOCView;->L:I

    goto :goto_1

    .line 93
    :cond_2
    const/4 v0, 0x3

    iput v0, p0, Lflipboard/gui/toc/TOCView;->L:I

    goto :goto_1

    .line 101
    :cond_3
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_4

    .line 102
    const/4 v0, 0x4

    iput v0, p0, Lflipboard/gui/toc/TOCView;->aa:I

    goto :goto_2

    .line 104
    :cond_4
    iput v6, p0, Lflipboard/gui/toc/TOCView;->aa:I

    goto :goto_2
.end method

.method static synthetic a(Lflipboard/gui/toc/TOCView;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lflipboard/gui/toc/TOCView;->x()V

    return-void
.end method

.method static synthetic b(Lflipboard/gui/toc/TOCView;)Ljava/util/List;
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lflipboard/gui/toc/TOCView;->getOrderedSections()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lflipboard/gui/toc/TOCView;)Lflipboard/gui/toc/DynamicGridLayout$DragManager;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    return-object v0
.end method

.method static synthetic d(Lflipboard/gui/toc/TOCView;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lflipboard/gui/toc/TOCView;->h:I

    return v0
.end method

.method static synthetic e(Lflipboard/gui/toc/TOCView;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lflipboard/gui/toc/TOCView;->h:I

    return v0
.end method

.method private f(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    .line 631
    sget-object v0, Lflipboard/gui/toc/TOCView;->a:Lflipboard/util/Log;

    .line 632
    invoke-static {p1}, Lflipboard/util/AndroidUtil;->c(Landroid/view/View;)[I

    move-result-object v7

    move-object v0, p1

    check-cast v0, Lflipboard/gui/toc/TileContainer;

    invoke-virtual {v0}, Lflipboard/gui/toc/TileContainer;->getDeleteImage()Landroid/view/View;

    move-result-object v1

    move-object v0, p1

    check-cast v0, Lflipboard/gui/toc/TileContainer;

    invoke-virtual {v0}, Lflipboard/gui/toc/TileContainer;->getScale()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v8

    add-float/2addr v2, v3

    mul-float/2addr v2, v0

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v8

    add-float/2addr v3, v4

    mul-float/2addr v3, v0

    float-to-int v3, v3

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v8

    mul-float/2addr v4, v0

    float-to-int v4, v4

    iget v5, p0, Lflipboard/gui/toc/TOCView;->S:I

    add-int v6, v4, v5

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v8

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iget v4, p0, Lflipboard/gui/toc/TOCView;->S:I

    add-int v8, v1, v4

    sget-object v1, Lflipboard/gui/toc/TOCView;->a:Lflipboard/util/Log;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v9

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v10

    const/4 v4, 0x2

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v1, v4

    new-instance v0, Lflipboard/gui/flipping/SinglePage;

    iget-object v1, p0, Lflipboard/gui/toc/TOCView;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-direct {v0, p0, v1}, Lflipboard/gui/flipping/SinglePage;-><init>(Lflipboard/gui/flipping/FlipTransitionBase;Lflipboard/gui/flipping/OpenGLTransitionRenderer;)V

    iget-boolean v1, p0, Lflipboard/gui/toc/TOCView;->w:Z

    iget v4, p0, Lflipboard/gui/toc/TOCView;->g:I

    iget v5, p0, Lflipboard/gui/toc/TOCView;->f:I

    aget v9, v7, v9

    sub-int v6, v9, v6

    int-to-float v6, v6

    aget v7, v7, v10

    sub-int/2addr v7, v8

    int-to-float v7, v7

    invoke-virtual/range {v0 .. v7}, Lflipboard/gui/flipping/SinglePage;->a(ZIIIIFF)V

    sget-object v1, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    invoke-virtual {v1, p1}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Landroid/view/View;)Lflipboard/gui/flipping/FlippingBitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/SinglePage;->a(Lflipboard/gui/flipping/FlippingBitmap;)V

    iput-object v0, p0, Lflipboard/gui/toc/TOCView;->W:Lflipboard/gui/flipping/SinglePage;

    .line 633
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v1, p0, Lflipboard/gui/toc/TOCView;->W:Lflipboard/gui/flipping/SinglePage;

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Lflipboard/gui/flipping/SinglePage;)V

    .line 634
    return-void
.end method

.method private getOrderedSections()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 497
    iget-object v4, p0, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    :goto_0
    iget-object v0, v4, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, v4, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v0}, Lflipboard/gui/toc/DynamicGridLayout;->getChildCount()I

    move-result v7

    move v3, v2

    :goto_1
    if-ge v3, v7, :cond_1

    invoke-virtual {v0, v3}, Lflipboard/gui/toc/DynamicGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    instance-of v9, v8, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;

    if-eqz v9, :cond_0

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v4, v6}, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a(Ljava/util/List;)V

    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 498
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 501
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 502
    instance-of v3, v0, Lflipboard/gui/toc/TileContainer;

    if-eqz v3, :cond_3

    .line 503
    check-cast v0, Lflipboard/gui/toc/TileContainer;

    iget-object v0, v0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 506
    :cond_4
    return-object v1
.end method

.method private x()V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCPage;

    .line 162
    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCView;->c(Landroid/view/View;)V

    goto :goto_0

    .line 164
    :cond_0
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 165
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->u()V

    .line 166
    return-void
.end method

.method private y()Lflipboard/gui/toc/TOCPage;
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v0, 0x0

    .line 172
    iget-object v1, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 174
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_1

    .line 175
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f03013b

    invoke-static {v1, v2, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCPage;

    .line 179
    :goto_0
    invoke-virtual {v0, p0}, Lflipboard/gui/toc/TOCPage;->setTocView(Lflipboard/gui/toc/TOCView;)V

    .line 182
    iget v1, p0, Lflipboard/gui/toc/TOCView;->L:I

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/TOCPage;->setMaxTiles(I)V

    .line 198
    :goto_1
    iget-object v1, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    invoke-virtual {p0, v3, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    .line 203
    :cond_0
    return-object v0

    .line 177
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f03013a

    invoke-static {v1, v2, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCCoverPage;

    .line 178
    iget-boolean v1, p0, Lflipboard/gui/toc/TOCView;->T:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/TOCCoverPage;->setUse5UpToc(Z)V

    goto :goto_0

    .line 183
    :cond_2
    iget-object v1, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget v2, p0, Lflipboard/gui/toc/TOCView;->aa:I

    if-eq v2, v3, :cond_3

    iget v2, p0, Lflipboard/gui/toc/TOCView;->aa:I

    if-lt v1, v2, :cond_3

    const/4 v1, 0x1

    :goto_2
    if-nez v1, :cond_0

    .line 188
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_4

    .line 189
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f030142

    invoke-static {v1, v2, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCPage;

    .line 193
    :goto_3
    invoke-virtual {v0, p0}, Lflipboard/gui/toc/TOCPage;->setTocView(Lflipboard/gui/toc/TOCView;)V

    .line 194
    iget v1, p0, Lflipboard/gui/toc/TOCView;->K:I

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/TOCPage;->setMaxTiles(I)V

    goto :goto_1

    .line 183
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 191
    :cond_4
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f03013f

    invoke-static {v1, v2, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCPage;

    goto :goto_3
.end method


# virtual methods
.method final a(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 511
    const-class v0, Lflipboard/gui/toc/TileContainer;

    new-instance v1, Lflipboard/gui/toc/TOCView$5;

    invoke-direct {v1, p0}, Lflipboard/gui/toc/TOCView$5;-><init>(Lflipboard/gui/toc/TOCView;)V

    invoke-static {p1, v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/ViewGroup;Ljava/lang/Class;Lflipboard/util/Callback;)V

    .line 517
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 348
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 351
    :cond_1
    if-nez p1, :cond_2

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 355
    :cond_2
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-boolean v0, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_0

    .line 361
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 362
    iget v0, p0, Lflipboard/gui/toc/TOCView;->h:I

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    iget v0, p0, Lflipboard/gui/toc/TOCView;->h:I

    add-int/lit8 v0, v0, 0x1

    if-gt v1, v0, :cond_4

    .line 363
    invoke-virtual {p0, v1}, Lflipboard/gui/toc/TOCView;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 364
    if-eqz v0, :cond_3

    .line 365
    const-class v3, Lflipboard/gui/toc/TileContainer;

    new-instance v4, Lflipboard/gui/toc/TOCView$3;

    invoke-direct {v4, p0, v2}, Lflipboard/gui/toc/TOCView$3;-><init>(Lflipboard/gui/toc/TOCView;Ljava/util/List;)V

    invoke-static {v0, v3, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/ViewGroup;Ljava/lang/Class;Lflipboard/util/Callback;)V

    .line 362
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 377
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 378
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v0

    .line 379
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p1, v3}, Lflipboard/service/Flap;->a(Lflipboard/service/User;ZZ)Lflipboard/service/Flap$UpdateRequest;

    move-result-object v1

    .line 380
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 381
    invoke-virtual {v1, v0, v5, v5}, Lflipboard/service/Flap$UpdateRequest;->a(Lflipboard/service/Section;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto :goto_2

    .line 383
    :cond_5
    invoke-virtual {v1}, Lflipboard/service/Flap$UpdateRequest;->c()V

    goto :goto_0
.end method

.method public final b(Lflipboard/gui/flipping/FlipTransitionBase$Direction;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 330
    invoke-super {p0, p1}, Lflipboard/gui/flipping/FlipTransitionViews;->b(Lflipboard/gui/flipping/FlipTransitionBase$Direction;)V

    .line 333
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->b()V

    iput-object v1, p0, Lflipboard/gui/toc/TOCView;->W:Lflipboard/gui/flipping/SinglePage;

    .line 336
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCView;->a(Z)V

    .line 339
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->ai:Landroid/os/Bundle;

    .line 340
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 420
    const/4 v0, 0x0

    return v0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 393
    invoke-super {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->d()V

    .line 394
    iget v0, p0, Lflipboard/gui/toc/TOCView;->h:I

    .line 395
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_0

    .line 396
    add-int/lit8 v0, v0, -0x1

    .line 398
    :cond_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 399
    iget-object v1, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCView;->a(Landroid/view/ViewGroup;)V

    .line 401
    :cond_1
    return-void
.end method

.method public getPages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/toc/TOCPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 669
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    return-object v0
.end method

.method protected final k()V
    .locals 2

    .prologue
    .line 413
    invoke-super {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->k()V

    .line 414
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/TOCActivity;

    invoke-virtual {v0}, Lflipboard/activities/TOCActivity;->j()V

    .line 415
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v1, "nanoTOCPullToRefresh"

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    .line 416
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 425
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 431
    :cond_0
    :goto_0
    return-void

    .line 428
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 429
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->w()V

    goto :goto_0
.end method

.method protected declared-synchronized onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 605
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->onDetachedFromWindow()V

    .line 606
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/toc/TOCView;->ab:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Lflipboard/util/Observer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 607
    monitor-exit p0

    return-void

    .line 605
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    const-wide/16 v12, 0x7d0

    const-wide/16 v10, 0x64

    const/4 v8, 0x4

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 526
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    .line 527
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 528
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCPage;

    .line 529
    iget-object v0, v0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 531
    :cond_0
    iget v0, p0, Lflipboard/gui/toc/TOCView;->h:I

    .line 532
    iget-object v2, p0, Lflipboard/gui/toc/TOCView;->M:Lflipboard/gui/toc/CoverPage;

    if-eqz v2, :cond_1

    .line 533
    add-int/lit8 v0, v0, -0x1

    .line 535
    :cond_1
    new-instance v2, Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    iget-object v3, p0, Lflipboard/gui/toc/TOCView;->Q:Lflipboard/gui/toc/DynamicGridLayout$State;

    invoke-direct {v2, p0, v1, v0, v3}, Lflipboard/gui/toc/DynamicGridLayout$DragManager;-><init>(Landroid/view/ViewGroup;Ljava/util/List;ILflipboard/gui/toc/DynamicGridLayout$State;)V

    iput-object v2, p0, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    .line 541
    :cond_2
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    invoke-virtual {v0}, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 542
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 543
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->n()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v1, p0, Lflipboard/gui/toc/TOCView;->R:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    move v0, v4

    :goto_1
    if-eqz v0, :cond_8

    iget-wide v0, p0, Lflipboard/gui/toc/TOCView;->ac:J

    sub-long v0, v2, v0

    cmp-long v0, v0, v12

    if-lez v0, :cond_8

    iget v0, p0, Lflipboard/gui/toc/TOCView;->h:I

    if-lez v0, :cond_8

    move v0, v4

    :goto_2
    if-eqz v0, :cond_a

    .line 544
    iput-wide v2, p0, Lflipboard/gui/toc/TOCView;->ac:J

    .line 545
    iget-object v3, p0, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "DynamicGridLayout:previousPage"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    iget v0, v3, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->o:I

    if-gtz v0, :cond_9

    move v4, v5

    :goto_3
    if-eqz v4, :cond_3

    .line 547
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    iget-object v0, v0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->d:Landroid/view/View;

    invoke-direct {p0, v0}, Lflipboard/gui/toc/TOCView;->f(Landroid/view/View;)V

    .line 548
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->j()V

    .line 561
    :cond_3
    :goto_4
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    invoke-virtual {v0, p1}, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a(Landroid/view/MotionEvent;)Z

    .line 563
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    iget-object v0, v0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->p:Lflipboard/gui/toc/DynamicGridLayout$State;

    iput-object v0, p0, Lflipboard/gui/toc/TOCView;->Q:Lflipboard/gui/toc/DynamicGridLayout$State;

    .line 567
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    invoke-virtual {v0}, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 568
    invoke-super {p0, p1}, Lflipboard/gui/flipping/FlipTransitionViews;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    .line 571
    :cond_4
    return v5

    :cond_5
    move v0, v5

    .line 543
    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p0, Lflipboard/gui/toc/TOCView;->R:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_7

    move v0, v4

    goto :goto_1

    :cond_7
    move v0, v5

    goto :goto_1

    :cond_8
    move v0, v5

    goto :goto_2

    .line 545
    :cond_9
    iget-object v0, v3, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->n:Ljava/util/List;

    iget v1, v3, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->o:I

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/DynamicGridLayout;

    iget-object v1, v3, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->n:Ljava/util/List;

    iget v2, v3, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->o:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v0}, Lflipboard/gui/toc/DynamicGridLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Lflipboard/gui/toc/DynamicGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    iget v2, v0, Lflipboard/gui/toc/DynamicGridLayout;->h:I

    iput v2, v3, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->q:I

    iget-object v2, v3, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    invoke-virtual {v1, v2}, Lflipboard/gui/toc/DynamicGridLayout;->removeView(Landroid/view/View;)V

    iget-object v2, v3, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    check-cast v2, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;

    invoke-interface {v2, v4}, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;->b(Z)Landroid/view/View;

    move-result-object v2

    iput-object v2, v3, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    iget-object v2, v3, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v3, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    invoke-virtual {v0, v2, v5}, Lflipboard/gui/toc/DynamicGridLayout;->addView(Landroid/view/View;I)V

    invoke-virtual {v0, v6}, Lflipboard/gui/toc/DynamicGridLayout;->removeView(Landroid/view/View;)V

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v7, Lflipboard/gui/toc/DynamicGridLayout$DragManager$1;

    invoke-direct {v7, v3, v1, v6, v0}, Lflipboard/gui/toc/DynamicGridLayout$DragManager$1;-><init>(Lflipboard/gui/toc/DynamicGridLayout$DragManager;Lflipboard/gui/toc/DynamicGridLayout;Landroid/view/View;Lflipboard/gui/toc/DynamicGridLayout;)V

    invoke-virtual {v2, v10, v11, v7}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    iget v0, v3, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->o:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v3, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->o:I

    goto/16 :goto_3

    .line 550
    :cond_a
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->n()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getHeight()I

    move-result v1

    iget v6, p0, Lflipboard/gui/toc/TOCView;->R:I

    sub-int/2addr v1, v6

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_b

    move v0, v4

    :goto_5
    if-eqz v0, :cond_e

    iget-wide v0, p0, Lflipboard/gui/toc/TOCView;->ac:J

    sub-long v0, v2, v0

    cmp-long v0, v0, v12

    if-lez v0, :cond_e

    iget v0, p0, Lflipboard/gui/toc/TOCView;->h:I

    iget-object v1, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_e

    move v0, v4

    :goto_6
    if-eqz v0, :cond_3

    .line 551
    iput-wide v2, p0, Lflipboard/gui/toc/TOCView;->ac:J

    .line 552
    iget-object v7, p0, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "DynamicGridLayout:nextPage"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    iget v0, v7, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->o:I

    iget-object v1, v7, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_f

    move v4, v5

    :goto_7
    if-eqz v4, :cond_3

    .line 554
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    iget-object v0, v0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->d:Landroid/view/View;

    invoke-direct {p0, v0}, Lflipboard/gui/toc/TOCView;->f(Landroid/view/View;)V

    .line 555
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->i()V

    goto/16 :goto_4

    :cond_b
    move v0, v5

    .line 550
    goto :goto_5

    :cond_c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getWidth()I

    move-result v1

    iget v6, p0, Lflipboard/gui/toc/TOCView;->R:I

    sub-int/2addr v1, v6

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_d

    move v0, v4

    goto :goto_5

    :cond_d
    move v0, v5

    goto :goto_5

    :cond_e
    move v0, v5

    goto :goto_6

    .line 552
    :cond_f
    iget-object v0, v7, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->n:Ljava/util/List;

    iget v1, v7, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->o:I

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/DynamicGridLayout;

    iget-object v1, v7, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->n:Ljava/util/List;

    iget v2, v7, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->o:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v0, v5}, Lflipboard/gui/toc/DynamicGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-nez v3, :cond_10

    const/4 v2, 0x0

    move-object v6, v2

    :goto_8
    if-nez v6, :cond_11

    move v4, v5

    goto :goto_7

    :cond_10
    move-object v2, v3

    check-cast v2, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;

    invoke-interface {v2, v4}, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;->b(Z)Landroid/view/View;

    move-result-object v2

    move-object v6, v2

    goto :goto_8

    :cond_11
    iget v2, v0, Lflipboard/gui/toc/DynamicGridLayout;->h:I

    iput v2, v7, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->q:I

    iget-object v2, v7, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    invoke-virtual {v1, v2}, Lflipboard/gui/toc/DynamicGridLayout;->removeView(Landroid/view/View;)V

    iget-object v2, v7, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    check-cast v2, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;

    invoke-interface {v2, v4}, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;->b(Z)Landroid/view/View;

    move-result-object v2

    iput-object v2, v7, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    iget-object v2, v7, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v7, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Lflipboard/gui/toc/DynamicGridLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v0, v3}, Lflipboard/gui/toc/DynamicGridLayout;->removeView(Landroid/view/View;)V

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v3, Lflipboard/gui/toc/DynamicGridLayout$DragManager$2;

    invoke-direct {v3, v7, v1, v6, v0}, Lflipboard/gui/toc/DynamicGridLayout$DragManager$2;-><init>(Lflipboard/gui/toc/DynamicGridLayout$DragManager;Lflipboard/gui/toc/DynamicGridLayout;Landroid/view/View;Lflipboard/gui/toc/DynamicGridLayout;)V

    invoke-virtual {v2, v10, v11, v3}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    iget v0, v7, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v7, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->o:I

    goto/16 :goto_7
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 435
    const/4 v0, 0x0

    return v0
.end method

.method protected onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 612
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 613
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 615
    invoke-virtual {p0, v1, v2}, Lflipboard/gui/toc/TOCView;->setMeasuredDimension(II)V

    .line 616
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getChildCount()I

    move-result v0

    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_1

    .line 617
    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 619
    instance-of v4, v3, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;

    if-eqz v4, :cond_0

    .line 620
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    goto :goto_0

    .line 622
    :cond_0
    invoke-virtual {v3, p1, p2}, Landroid/view/View;->measure(II)V

    goto :goto_0

    .line 626
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(IILandroid/content/Context;)V

    .line 627
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    invoke-virtual {v0}, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406
    const/4 v0, 0x0

    .line 408
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lflipboard/gui/flipping/FlipTransitionViews;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final u()V
    .locals 13

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 211
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "TOCView:applySections"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 212
    iget-object v8, p0, Lflipboard/gui/toc/TOCView;->U:Ljava/util/List;

    .line 213
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 214
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v10

    .line 217
    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 218
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 220
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 221
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "first section is not cover stories: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 225
    :cond_0
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCPage;

    .line 226
    iget-object v1, v0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v1}, Lflipboard/gui/toc/DynamicGridLayout;->getChildCount()I

    move-result v1

    :goto_0
    add-int/lit8 v3, v1, -0x1

    if-ltz v3, :cond_1

    .line 227
    iget-object v1, v0, Lflipboard/gui/toc/TOCPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v1, v3}, Lflipboard/gui/toc/DynamicGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 228
    instance-of v6, v1, Lflipboard/gui/toc/TileContainer;

    if-eqz v6, :cond_2

    .line 229
    check-cast v1, Lflipboard/gui/toc/TileContainer;

    .line 230
    iget-object v6, v1, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-interface {v9, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    move v1, v3

    .line 232
    goto :goto_0

    .line 237
    :cond_3
    iget v0, p0, Lflipboard/gui/toc/TOCView;->aa:I

    if-gez v0, :cond_4

    .line 238
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    .line 242
    :goto_1
    iget v1, p0, Lflipboard/gui/toc/TOCView;->L:I

    if-ge v0, v1, :cond_5

    move v1, v2

    .line 244
    :goto_2
    iget v3, p0, Lflipboard/gui/toc/TOCView;->aa:I

    if-lez v3, :cond_10

    iget v3, p0, Lflipboard/gui/toc/TOCView;->aa:I

    if-lt v1, v3, :cond_10

    .line 245
    iget v1, p0, Lflipboard/gui/toc/TOCView;->aa:I

    add-int/lit8 v1, v1, -0x1

    move v3, v1

    .line 247
    :goto_3
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    sub-int v0, v1, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 251
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v12

    move v5, v2

    move v7, v2

    .line 253
    :goto_4
    if-gt v7, v3, :cond_f

    .line 254
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v7, v0, :cond_6

    invoke-direct {p0}, Lflipboard/gui/toc/TOCView;->y()Lflipboard/gui/toc/TOCPage;

    move-result-object v0

    .line 255
    :goto_5
    add-int/lit8 v1, v7, 0x1

    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v0, v1, v6}, Lflipboard/gui/toc/TOCPage;->a(II)V

    .line 258
    if-ne v7, v3, :cond_7

    move v1, v4

    .line 261
    :goto_6
    iget v6, v0, Lflipboard/gui/toc/TOCPage;->m:I

    if-eqz v1, :cond_8

    move v1, v4

    :goto_7
    sub-int v1, v6, v1

    .line 262
    add-int/2addr v1, v5

    invoke-static {v1, v12}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 263
    invoke-interface {v8, v5, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    .line 264
    if-ne v7, v3, :cond_9

    move v1, v4

    :goto_8
    invoke-virtual {v0, v1}, Lflipboard/gui/toc/TOCPage;->setLast(Z)V

    .line 265
    invoke-virtual {v0, v5, v9}, Lflipboard/gui/toc/TOCPage;->a(Ljava/util/List;Ljava/util/Map;)V

    .line 266
    invoke-virtual {v0, v11}, Lflipboard/gui/toc/TOCPage;->setMoreTileCount(I)V

    .line 253
    add-int/lit8 v0, v7, 0x1

    move v5, v6

    move v7, v0

    goto :goto_4

    .line 240
    :cond_4
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lflipboard/gui/toc/TOCView;->L:I

    iget v3, p0, Lflipboard/gui/toc/TOCView;->K:I

    iget v5, p0, Lflipboard/gui/toc/TOCView;->aa:I

    add-int/lit8 v5, v5, -0x1

    mul-int/2addr v3, v5

    add-int/2addr v1, v3

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_1

    .line 242
    :cond_5
    iget v1, p0, Lflipboard/gui/toc/TOCView;->L:I

    sub-int v1, v0, v1

    iget v3, p0, Lflipboard/gui/toc/TOCView;->K:I

    div-int/2addr v1, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 254
    :cond_6
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCPage;

    goto :goto_5

    :cond_7
    move v1, v2

    .line 258
    goto :goto_6

    :cond_8
    move v1, v2

    .line 261
    goto :goto_7

    :cond_9
    move v1, v2

    .line 264
    goto :goto_8

    .line 271
    :goto_9
    if-ge v0, v12, :cond_a

    .line 272
    add-int/lit8 v1, v0, 0x1

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 273
    invoke-virtual {v0, v4}, Lflipboard/service/Section;->b(Z)V

    move v0, v1

    .line 274
    goto :goto_9

    .line 277
    :cond_a
    invoke-interface {v9}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TileContainer;

    .line 278
    invoke-virtual {v0}, Lflipboard/gui/toc/TileContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_a

    .line 281
    :cond_b
    iget v0, p0, Lflipboard/gui/toc/TOCView;->L:I

    if-ge v12, v0, :cond_c

    .line 283
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCPage;

    invoke-virtual {v0, v4}, Lflipboard/gui/toc/TOCPage;->setLast(Z)V

    .line 285
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v0, :cond_c

    .line 288
    invoke-direct {p0}, Lflipboard/gui/toc/TOCView;->y()Lflipboard/gui/toc/TOCPage;

    move-result-object v0

    .line 289
    iget-object v1, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 290
    invoke-virtual {v0, v4}, Lflipboard/gui/toc/TOCPage;->setLast(Z)V

    .line 297
    :cond_c
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_b
    add-int/lit8 v1, v0, -0x1

    add-int/lit8 v0, v3, 0x1

    if-lt v1, v0, :cond_d

    .line 298
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCPage;

    .line 299
    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCView;->c(Landroid/view/View;)V

    .line 300
    iget-object v4, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move v0, v1

    .line 301
    goto :goto_b

    .line 306
    :cond_d
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v10, :cond_e

    .line 307
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCPage;

    .line 308
    iget-object v3, p0, Lflipboard/gui/toc/TOCView;->V:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v0, v3}, Lflipboard/gui/toc/TOCPage;->setPaginator(I)V

    goto :goto_c

    .line 316
    :cond_e
    invoke-virtual {p0, v2}, Lflipboard/gui/toc/TOCView;->a(Z)V

    .line 317
    return-void

    :cond_f
    move v0, v5

    goto/16 :goto_9

    :cond_10
    move v3, v1

    goto/16 :goto_3
.end method

.method public final v()Z
    .locals 2

    .prologue
    .line 450
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->Q:Lflipboard/gui/toc/DynamicGridLayout$State;

    .line 451
    :goto_0
    if-eqz v0, :cond_0

    .line 452
    sget-object v1, Lflipboard/gui/toc/TOCView$6;->b:[I

    invoke-virtual {v0}, Lflipboard/gui/toc/DynamicGridLayout$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 458
    :cond_0
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 450
    :cond_1
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    iget-object v0, v0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->p:Lflipboard/gui/toc/DynamicGridLayout$State;

    goto :goto_0

    .line 455
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_1

    .line 452
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final v_()V
    .locals 3

    .prologue
    .line 488
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-direct {p0}, Lflipboard/gui/toc/TOCView;->getOrderedSections()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/service/User;->a(Ljava/util/List;Z)V

    .line 489
    return-void
.end method

.method public final w()V
    .locals 3

    .prologue
    .line 463
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    if-eqz v0, :cond_0

    .line 464
    iget-object v0, p0, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    sget-object v1, Lflipboard/gui/toc/DynamicGridLayout$1;->a:[I

    iget-object v2, v0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->p:Lflipboard/gui/toc/DynamicGridLayout$State;

    invoke-virtual {v2}, Lflipboard/gui/toc/DynamicGridLayout$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    .line 465
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const/16 v1, 0x15e

    new-instance v2, Lflipboard/gui/toc/TOCView$4;

    invoke-direct {v2, p0}, Lflipboard/gui/toc/TOCView$4;-><init>(Lflipboard/gui/toc/TOCView;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(ILjava/lang/Runnable;)V

    .line 480
    :cond_0
    return-void

    .line 464
    :pswitch_0
    sget-object v1, Lflipboard/gui/toc/DynamicGridLayout$State;->a:Lflipboard/gui/toc/DynamicGridLayout$State;

    sget-object v2, Lflipboard/gui/toc/DynamicGridLayout$State;->a:Lflipboard/gui/toc/DynamicGridLayout$State;

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a(Lflipboard/gui/toc/DynamicGridLayout$State;Lflipboard/gui/toc/DynamicGridLayout$State;)V

    sget-object v1, Lflipboard/gui/toc/DynamicGridLayout$State;->a:Lflipboard/gui/toc/DynamicGridLayout$State;

    iput-object v1, v0, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->p:Lflipboard/gui/toc/DynamicGridLayout$State;

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
