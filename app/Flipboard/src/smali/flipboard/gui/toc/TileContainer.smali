.class public Lflipboard/gui/toc/TileContainer;
.super Lflipboard/gui/ContainerView;
.source "TileContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lflipboard/gui/FLViewIntf;
.implements Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;
.implements Lflipboard/util/FLAnimation$Scaleable;
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/gui/ContainerView;",
        "Landroid/view/View$OnClickListener;",
        "Lflipboard/gui/FLViewIntf;",
        "Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;",
        "Lflipboard/util/FLAnimation$Scaleable;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/Section;",
        "Lflipboard/service/Section$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static l:Lflipboard/util/Log;

.field private static m:Lflipboard/util/Log;

.field private static t:Landroid/view/View$OnLongClickListener;


# instance fields
.field protected a:Lflipboard/service/Section;

.field protected b:Landroid/widget/ImageView;

.field protected c:Lflipboard/gui/toc/TileView;

.field protected d:Lflipboard/gui/toc/TileView;

.field protected e:Z

.field protected f:Lflipboard/objs/FeedItem;

.field protected g:Lflipboard/gui/FLBusyView;

.field h:Lflipboard/gui/toc/TOCView;

.field i:Z

.field j:I

.field k:Landroid/view/MotionEvent;

.field private n:Lflipboard/gui/toc/DynamicGridLayout$State;

.field private o:I

.field private p:I

.field private q:F

.field private r:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private s:Lflipboard/samsung/spen/OnSamsungViewListener;

.field private u:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lflipboard/gui/toc/TOCPage;->d:Lflipboard/util/Log;

    sput-object v0, Lflipboard/gui/toc/TileContainer;->l:Lflipboard/util/Log;

    .line 58
    sget-object v0, Lflipboard/gui/toc/TOCPage;->e:Lflipboard/util/Log;

    sput-object v0, Lflipboard/gui/toc/TileContainer;->m:Lflipboard/util/Log;

    .line 80
    new-instance v0, Lflipboard/gui/toc/TileContainer$1;

    invoke-direct {v0}, Lflipboard/gui/toc/TileContainer$1;-><init>()V

    sput-object v0, Lflipboard/gui/toc/TileContainer;->t:Landroid/view/View$OnLongClickListener;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lflipboard/gui/toc/TileContainer;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 122
    invoke-direct {p0, p1}, Lflipboard/gui/ContainerView;-><init>(Landroid/content/Context;)V

    .line 67
    sget-object v0, Lflipboard/gui/toc/DynamicGridLayout$State;->a:Lflipboard/gui/toc/DynamicGridLayout$State;

    iput-object v0, p0, Lflipboard/gui/toc/TileContainer;->n:Lflipboard/gui/toc/DynamicGridLayout$State;

    .line 68
    iput v1, p0, Lflipboard/gui/toc/TileContainer;->o:I

    .line 69
    iput v1, p0, Lflipboard/gui/toc/TileContainer;->p:I

    .line 70
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lflipboard/gui/toc/TileContainer;->q:F

    .line 71
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lflipboard/gui/toc/TileContainer;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 74
    const/high16 v0, -0x80000000

    iput v0, p0, Lflipboard/gui/toc/TileContainer;->j:I

    .line 125
    iget-object v0, p2, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    iput-object v0, p0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    .line 126
    iget-object v0, p2, Lflipboard/gui/toc/TileContainer;->c:Lflipboard/gui/toc/TileView;

    iget v0, v0, Lflipboard/gui/toc/TileView;->i:I

    iput v0, p0, Lflipboard/gui/toc/TileContainer;->o:I

    .line 127
    iget-object v0, p2, Lflipboard/gui/toc/TileContainer;->c:Lflipboard/gui/toc/TileView;

    iget v0, v0, Lflipboard/gui/toc/TileView;->j:I

    iput v0, p0, Lflipboard/gui/toc/TileContainer;->p:I

    .line 128
    iget-object v0, p2, Lflipboard/gui/toc/TileContainer;->c:Lflipboard/gui/toc/TileView;

    iget-object v0, v0, Lflipboard/gui/toc/TileView;->m:Lflipboard/objs/FeedItem;

    iput-object v0, p0, Lflipboard/gui/toc/TileContainer;->f:Lflipboard/objs/FeedItem;

    .line 131
    invoke-direct {p0}, Lflipboard/gui/toc/TileContainer;->h()V

    .line 134
    iget-object v0, p2, Lflipboard/gui/toc/TileContainer;->n:Lflipboard/gui/toc/DynamicGridLayout$State;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/toc/TileContainer;->a(Lflipboard/gui/toc/DynamicGridLayout$State;Z)V

    .line 135
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lflipboard/service/Section;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 107
    invoke-direct {p0, p1}, Lflipboard/gui/ContainerView;-><init>(Landroid/content/Context;)V

    .line 67
    sget-object v0, Lflipboard/gui/toc/DynamicGridLayout$State;->a:Lflipboard/gui/toc/DynamicGridLayout$State;

    iput-object v0, p0, Lflipboard/gui/toc/TileContainer;->n:Lflipboard/gui/toc/DynamicGridLayout$State;

    .line 68
    iput v1, p0, Lflipboard/gui/toc/TileContainer;->o:I

    .line 69
    iput v1, p0, Lflipboard/gui/toc/TileContainer;->p:I

    .line 70
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lflipboard/gui/toc/TileContainer;->q:F

    .line 71
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lflipboard/gui/toc/TileContainer;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 74
    const/high16 v0, -0x80000000

    iput v0, p0, Lflipboard/gui/toc/TileContainer;->j:I

    .line 110
    iput-object p2, p0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    .line 113
    invoke-direct {p0}, Lflipboard/gui/toc/TileContainer;->h()V

    .line 114
    return-void
.end method

.method static synthetic a(Lflipboard/gui/toc/TileContainer;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lflipboard/gui/toc/TileContainer;->i()V

    return-void
.end method

.method static synthetic b(Lflipboard/gui/toc/TileContainer;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lflipboard/gui/toc/TileContainer;->g()V

    return-void
.end method

.method static synthetic c(Lflipboard/gui/toc/TileContainer;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lflipboard/gui/toc/TileContainer;->j()V

    return-void
.end method

.method static synthetic d(Lflipboard/gui/toc/TileContainer;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lflipboard/gui/toc/TileContainer;->k()V

    return-void
.end method

.method static synthetic f()Lflipboard/service/User;
    .locals 1

    .prologue
    .line 55
    invoke-static {}, Lflipboard/gui/toc/TileContainer;->getUser()Lflipboard/service/User;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->g:Lflipboard/gui/FLBusyView;

    invoke-virtual {v0}, Lflipboard/gui/FLBusyView;->getVisibility()I

    move-result v1

    .line 273
    iget v0, p0, Lflipboard/gui/toc/TileContainer;->j:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->d:Lflipboard/gui/toc/TileView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    .line 274
    :goto_0
    if-eq v1, v0, :cond_1

    .line 275
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->g:Lflipboard/gui/FLBusyView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLBusyView;->setVisibility(I)V

    .line 277
    :cond_1
    return-void

    .line 273
    :cond_2
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private static getUser()Lflipboard/service/User;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    return-object v0
.end method

.method private h()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 282
    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileContainer;->setClipChildren(Z)V

    .line 285
    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileContainer;->setWillNotDraw(Z)V

    .line 288
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020229

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileContainer;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 291
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->f:Lflipboard/objs/FeedItem;

    if-nez v0, :cond_0

    .line 292
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getCurrentTOCItem()V

    .line 296
    :cond_0
    new-instance v0, Lflipboard/gui/FLBusyView;

    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lflipboard/gui/FLBusyView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/toc/TileContainer;->g:Lflipboard/gui/FLBusyView;

    .line 297
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->g:Lflipboard/gui/FLBusyView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBusyView;->setVisibility(I)V

    .line 300
    invoke-direct {p0}, Lflipboard/gui/toc/TileContainer;->l()V

    .line 302
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 304
    sget-object v0, Lflipboard/gui/toc/TileContainer;->t:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileContainer;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 308
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    .line 309
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090087

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 310
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    iget-object v2, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v3

    invoke-virtual {v1, v2, v3, v0, v0}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 311
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    const v1, 0x7f0201de

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 312
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 313
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 314
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    new-instance v1, Lflipboard/gui/toc/TileContainer$2;

    invoke-direct {v1, p0}, Lflipboard/gui/toc/TileContainer$2;-><init>(Lflipboard/gui/toc/TileContainer;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 356
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileContainer;->addView(Landroid/view/View;)V

    .line 361
    :cond_1
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->g:Lflipboard/gui/FLBusyView;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileContainer;->addView(Landroid/view/View;)V

    .line 363
    invoke-virtual {p0, p0}, Lflipboard/gui/toc/TileContainer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 364
    return-void
.end method

.method private i()V
    .locals 4

    .prologue
    .line 411
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 412
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 413
    const-string v2, "source"

    const-string v3, "toc"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    const-string v2, "pageIndex"

    iget-object v3, p0, Lflipboard/gui/toc/TileContainer;->h:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v3}, Lflipboard/gui/toc/TOCView;->getCurrentViewIndex()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 415
    iget-object v2, p0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v2, v0, v1}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 417
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->s:Lflipboard/samsung/spen/OnSamsungViewListener;

    if-eqz v0, :cond_0

    .line 418
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->s:Lflipboard/samsung/spen/OnSamsungViewListener;

    .line 420
    :cond_0
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 552
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->f:Lflipboard/objs/FeedItem;

    .line 555
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getCurrentTOCItem()V

    .line 558
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->f:Lflipboard/objs/FeedItem;

    if-nez v1, :cond_1

    .line 560
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->c:Lflipboard/gui/toc/TileView;

    invoke-virtual {v0}, Lflipboard/gui/toc/TileView;->b()V

    .line 571
    :cond_0
    :goto_0
    return-void

    .line 565
    :cond_1
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->f:Lflipboard/objs/FeedItem;

    if-eq v0, v1, :cond_0

    .line 570
    invoke-direct {p0}, Lflipboard/gui/toc/TileContainer;->l()V

    goto :goto_0
.end method

.method private k()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 707
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->c()Lflipboard/gui/toc/TOCPage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->e(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    if-nez v0, :cond_6

    .line 750
    :cond_1
    :goto_1
    return-void

    .line 707
    :cond_2
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->d()Lflipboard/gui/toc/TOCView;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->v()Z

    move-result v3

    if-eqz v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->getRunningFlips()I

    move-result v0

    if-lez v0, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0

    .line 710
    :cond_6
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 711
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->c:Lflipboard/gui/toc/TileView;

    .line 712
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->d:Lflipboard/gui/toc/TileView;

    .line 714
    if-eqz v1, :cond_1

    .line 716
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v3, Lflipboard/gui/toc/TileContainer$8;

    invoke-direct {v3, p0, v1, v0}, Lflipboard/gui/toc/TileContainer$8;-><init>(Lflipboard/gui/toc/TileContainer;Lflipboard/gui/toc/TileView;Lflipboard/gui/toc/TileView;)V

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method private l()V
    .locals 2

    .prologue
    .line 767
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "TileContainer:setNextTile"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 769
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->c:Lflipboard/gui/toc/TileView;

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileContainer;->a(Z)Lflipboard/gui/toc/TileView;

    move-result-object v0

    .line 770
    const/4 v1, -0x1

    iput v1, p0, Lflipboard/gui/toc/TileContainer;->o:I

    .line 773
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/TileView;->setVisibility(I)V

    .line 774
    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileContainer;->addView(Landroid/view/View;)V

    .line 777
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->g:Lflipboard/gui/FLBusyView;

    if-eqz v1, :cond_0

    .line 778
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->g:Lflipboard/gui/FLBusyView;

    invoke-virtual {v1}, Lflipboard/gui/FLBusyView;->bringToFront()V

    .line 781
    :cond_0
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 782
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->bringToFront()V

    .line 787
    :cond_1
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->d:Lflipboard/gui/toc/TileView;

    if-eqz v1, :cond_2

    .line 788
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->d:Lflipboard/gui/toc/TileView;

    invoke-virtual {p0, v1}, Lflipboard/gui/toc/TileContainer;->removeView(Landroid/view/View;)V

    .line 790
    :cond_2
    iput-object v0, p0, Lflipboard/gui/toc/TileContainer;->d:Lflipboard/gui/toc/TileView;

    .line 791
    invoke-direct {p0}, Lflipboard/gui/toc/TileContainer;->g()V

    .line 795
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->c:Lflipboard/gui/toc/TileView;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->c:Lflipboard/gui/toc/TileView;

    iget-object v1, v1, Lflipboard/gui/toc/TileView;->m:Lflipboard/objs/FeedItem;

    if-nez v1, :cond_4

    .line 796
    :cond_3
    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileContainer;->a(Lflipboard/gui/toc/TileView;)V

    .line 798
    :cond_4
    return-void

    .line 769
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Z)Lflipboard/gui/toc/TileView;
    .locals 3

    .prologue
    .line 757
    new-instance v0, Lflipboard/gui/toc/TileView;

    iget v1, p0, Lflipboard/gui/toc/TileContainer;->o:I

    iget v2, p0, Lflipboard/gui/toc/TileContainer;->p:I

    invoke-direct {v0, p0, v1, v2, p1}, Lflipboard/gui/toc/TileView;-><init>(Lflipboard/gui/toc/TileContainer;IIZ)V

    return-object v0
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 209
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/toc/TileContainer;->u:Z

    .line 211
    :try_start_0
    iget v0, p0, Lflipboard/gui/toc/TileContainer;->q:F

    iget v1, p0, Lflipboard/gui/toc/TileContainer;->q:F

    iget-object v2, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget-object v3, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 212
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lflipboard/gui/toc/TileContainer;->q:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 213
    invoke-virtual {p0, p1}, Lflipboard/gui/toc/TileContainer;->draw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 215
    iput-boolean v4, p0, Lflipboard/gui/toc/TileContainer;->u:Z

    .line 216
    return-void

    .line 215
    :catchall_0
    move-exception v0

    iput-boolean v4, p0, Lflipboard/gui/toc/TileContainer;->u:Z

    throw v0
.end method

.method public final a(Lflipboard/gui/toc/DynamicGridLayout$State;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 245
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->n:Lflipboard/gui/toc/DynamicGridLayout$State;

    if-ne v0, p1, :cond_0

    .line 268
    :goto_0
    return-void

    .line 248
    :cond_0
    iput-object p1, p0, Lflipboard/gui/toc/TileContainer;->n:Lflipboard/gui/toc/DynamicGridLayout$State;

    .line 250
    if-eqz p2, :cond_1

    const/16 v0, 0x15e

    .line 251
    :goto_1
    iput-object p1, p0, Lflipboard/gui/toc/TileContainer;->n:Lflipboard/gui/toc/DynamicGridLayout$State;

    .line 252
    sget-object v2, Lflipboard/gui/toc/TileContainer$9;->a:[I

    invoke-virtual {p1}, Lflipboard/gui/toc/DynamicGridLayout$State;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 265
    new-instance v1, Lflipboard/util/FLAnimation$Scale;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, p0, v2, v0}, Lflipboard/util/FLAnimation$Scale;-><init>(Lflipboard/util/FLAnimation$Scaleable;FI)V

    .line 266
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 250
    goto :goto_1

    .line 254
    :pswitch_0
    iget-object v2, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 255
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->bringToFront()V

    .line 257
    new-instance v1, Lflipboard/util/FLAnimation$Scale;

    const v2, 0x3f7851ec    # 0.97f

    invoke-direct {v1, p0, v2, v0}, Lflipboard/util/FLAnimation$Scale;-><init>(Lflipboard/util/FLAnimation$Scaleable;FI)V

    goto :goto_0

    .line 260
    :pswitch_1
    iget-object v2, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 261
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->bringToFront()V

    .line 262
    new-instance v1, Lflipboard/util/FLAnimation$Scale;

    const v2, 0x3f8ccccd    # 1.1f

    invoke-direct {v1, p0, v2, v0}, Lflipboard/util/FLAnimation$Scale;-><init>(Lflipboard/util/FLAnimation$Scaleable;FI)V

    goto :goto_0

    .line 252
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lflipboard/gui/toc/TileView;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 618
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->d:Lflipboard/gui/toc/TileView;

    if-ne p1, v0, :cond_0

    .line 621
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->c()Lflipboard/gui/toc/TOCPage;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->c()Lflipboard/gui/toc/TOCPage;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCPage;->getLatestSectionPreview()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 625
    :goto_0
    iget-object v3, p0, Lflipboard/gui/toc/TileContainer;->c:Lflipboard/gui/toc/TileView;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lflipboard/gui/toc/TileContainer;->c:Lflipboard/gui/toc/TileView;

    iget-object v3, v3, Lflipboard/gui/toc/TileView;->m:Lflipboard/objs/FeedItem;

    if-eqz v3, :cond_1

    if-nez v0, :cond_1

    .line 626
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627
    invoke-direct {p0}, Lflipboard/gui/toc/TileContainer;->k()V

    .line 647
    :cond_0
    :goto_1
    return-void

    .line 631
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/toc/TileContainer$6;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/toc/TileContainer$6;-><init>(Lflipboard/gui/toc/TileContainer;Lflipboard/gui/toc/TileView;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public a(Lflipboard/service/Section;Lflipboard/service/Section$Message;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 511
    sget-object v0, Lflipboard/gui/toc/TileContainer$9;->b:[I

    invoke-virtual {p2}, Lflipboard/service/Section$Message;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 540
    :goto_0
    return-void

    .line 513
    :pswitch_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/toc/TileContainer$3;

    invoke-direct {v1, p0}, Lflipboard/gui/toc/TileContainer$3;-><init>(Lflipboard/gui/toc/TileContainer;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 523
    :pswitch_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/toc/TileContainer$4;

    invoke-direct {v1, p0}, Lflipboard/gui/toc/TileContainer$4;-><init>(Lflipboard/gui/toc/TileContainer;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 532
    :pswitch_2
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/toc/TileContainer$5;

    invoke-direct {v1, p0}, Lflipboard/gui/toc/TileContainer$5;-><init>(Lflipboard/gui/toc/TileContainer;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 511
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 55
    check-cast p1, Lflipboard/service/Section;

    check-cast p2, Lflipboard/service/Section$Message;

    invoke-virtual {p0, p1, p2, p3}, Lflipboard/gui/toc/TileContainer;->a(Lflipboard/service/Section;Lflipboard/service/Section$Message;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(ZI)V
    .locals 0

    .prologue
    .line 597
    if-eqz p1, :cond_0

    :goto_0
    iput p2, p0, Lflipboard/gui/toc/TileContainer;->j:I

    .line 598
    invoke-direct {p0}, Lflipboard/gui/toc/TileContainer;->g()V

    .line 599
    return-void

    .line 597
    :cond_0
    const/high16 p2, -0x80000000

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Z)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 227
    new-instance v0, Lflipboard/gui/toc/TileContainer;

    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lflipboard/gui/toc/TileContainer;-><init>(Landroid/content/Context;Lflipboard/gui/toc/TileContainer;)V

    .line 228
    if-nez p1, :cond_0

    .line 229
    invoke-virtual {v0, v2}, Lflipboard/gui/toc/TileContainer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 230
    iget-object v1, v0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    :cond_0
    return-object v0
.end method

.method protected b()Z
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x1

    return v0
.end method

.method protected final c()Lflipboard/gui/toc/TOCPage;
    .locals 1

    .prologue
    .line 147
    const-class v0, Lflipboard/gui/toc/TOCPage;

    invoke-static {p0, v0}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCPage;

    return-object v0
.end method

.method protected final d()Lflipboard/gui/toc/TOCView;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->h:Lflipboard/gui/toc/TOCView;

    if-nez v0, :cond_0

    .line 155
    const-class v0, Lflipboard/gui/toc/TOCView;

    invoke-static {p0, v0}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCView;

    iput-object v0, p0, Lflipboard/gui/toc/TileContainer;->h:Lflipboard/gui/toc/TOCView;

    .line 157
    :cond_0
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->h:Lflipboard/gui/toc/TOCView;

    return-object v0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 382
    iput-object p1, p0, Lflipboard/gui/toc/TileContainer;->k:Landroid/view/MotionEvent;

    .line 383
    invoke-super {p0, p1}, Lflipboard/gui/ContainerView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method final e()V
    .locals 4

    .prologue
    .line 683
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    .line 684
    if-eqz v0, :cond_1

    .line 685
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->d()Lflipboard/gui/toc/TOCView;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/gui/toc/TOCView;->v()Z

    move-result v1

    if-nez v1, :cond_1

    .line 686
    sget-object v1, Lflipboard/gui/toc/TileContainer;->l:Lflipboard/util/Log;

    iget-boolean v1, v1, Lflipboard/util/Log;->f:Z

    if-eqz v1, :cond_0

    .line 687
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->c()Lflipboard/gui/toc/TOCPage;

    move-result-object v1

    .line 688
    sget-object v2, Lflipboard/gui/toc/TileContainer;->l:Lflipboard/util/Log;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget v3, v1, Lflipboard/gui/toc/TOCPage;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    invoke-static {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->e(Landroid/view/View;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v2, v0

    .line 690
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const/16 v1, 0x15e

    new-instance v2, Lflipboard/gui/toc/TileContainer$7;

    invoke-direct {v2, p0}, Lflipboard/gui/toc/TileContainer$7;-><init>(Lflipboard/gui/toc/TileContainer;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(ILjava/lang/Runnable;)V

    .line 698
    :cond_1
    return-void
.end method

.method getCurrentTOCItem()V
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    iput-object v0, p0, Lflipboard/gui/toc/TileContainer;->f:Lflipboard/objs/FeedItem;

    .line 373
    return-void
.end method

.method public getCurrentTile()Lflipboard/gui/toc/TileView;
    .locals 1

    .prologue
    .line 943
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->c:Lflipboard/gui/toc/TileView;

    return-object v0
.end method

.method public getDeleteImage()Landroid/view/View;
    .locals 1

    .prologue
    .line 805
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lflipboard/gui/toc/TileContainer;->q:F

    return v0
.end method

.method public getSection()Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 575
    invoke-super {p0}, Lflipboard/gui/ContainerView;->onAttachedToWindow()V

    .line 576
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getPageOffset()I

    move-result v0

    iput v0, p0, Lflipboard/gui/toc/TileContainer;->j:I

    .line 579
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->c()Lflipboard/gui/toc/TOCPage;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lflipboard/gui/toc/TileContainer;->i:Z

    .line 581
    iget-boolean v0, p0, Lflipboard/gui/toc/TileContainer;->i:Z

    if-nez v0, :cond_0

    .line 583
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v0, p0}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    .line 586
    invoke-direct {p0}, Lflipboard/gui/toc/TileContainer;->j()V

    .line 589
    invoke-direct {p0}, Lflipboard/gui/toc/TileContainer;->g()V

    .line 592
    :cond_0
    iput-boolean v1, p0, Lflipboard/gui/toc/TileContainer;->e:Z

    .line 593
    return-void

    .line 579
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 387
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->d()Lflipboard/gui/toc/TOCView;

    move-result-object v0

    .line 388
    if-nez v0, :cond_1

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 393
    :cond_1
    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->v()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 394
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->k:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 395
    iget-object v2, p0, Lflipboard/gui/toc/TileContainer;->k:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    .line 396
    iget-object v3, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    if-ge v1, v3, :cond_2

    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 397
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->performClick()Z

    goto :goto_0

    .line 399
    :cond_2
    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->w()V

    goto :goto_0

    .line 404
    :cond_3
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 407
    invoke-direct {p0}, Lflipboard/gui/toc/TileContainer;->i()V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 603
    invoke-super {p0}, Lflipboard/gui/ContainerView;->onDetachedFromWindow()V

    .line 604
    iget-boolean v0, p0, Lflipboard/gui/toc/TileContainer;->i:Z

    if-nez v0, :cond_0

    .line 605
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v0, p0}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    .line 607
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/toc/TileContainer;->e:Z

    .line 608
    const/high16 v0, -0x80000000

    iput v0, p0, Lflipboard/gui/toc/TileContainer;->j:I

    .line 609
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 196
    iget v0, p0, Lflipboard/gui/toc/TileContainer;->q:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/gui/toc/TileContainer;->u:Z

    if-nez v0, :cond_0

    .line 197
    iget v0, p0, Lflipboard/gui/toc/TileContainer;->q:F

    iget v1, p0, Lflipboard/gui/toc/TileContainer;->q:F

    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 199
    :cond_0
    invoke-super {p0, p1}, Lflipboard/gui/ContainerView;->onDraw(Landroid/graphics/Canvas;)V

    .line 200
    return-void
.end method

.method public onHoverChanged(Z)V
    .locals 2

    .prologue
    .line 478
    sget-object v0, Lflipboard/gui/toc/TileContainer;->m:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " Tile Container send hover Changed event at Section - "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 480
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getCurrentTile()Lflipboard/gui/toc/TileView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 482
    if-eqz p1, :cond_1

    .line 483
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->s:Lflipboard/samsung/spen/OnSamsungViewListener;

    if-eqz v0, :cond_0

    .line 484
    sget-object v0, Lflipboard/gui/toc/TileContainer;->m:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " Tile Container send hover ENTER event at Section - "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 485
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->s:Lflipboard/samsung/spen/OnSamsungViewListener;

    invoke-interface {v0, p0}, Lflipboard/samsung/spen/OnSamsungViewListener;->b(Landroid/view/View;)V

    .line 494
    :cond_0
    :goto_0
    return-void

    .line 488
    :cond_1
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->s:Lflipboard/samsung/spen/OnSamsungViewListener;

    if-eqz v0, :cond_0

    .line 489
    sget-object v0, Lflipboard/gui/toc/TileContainer;->m:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " Tile Container send hover exit event at Section "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 490
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->s:Lflipboard/samsung/spen/OnSamsungViewListener;

    invoke-interface {v0, p0}, Lflipboard/samsung/spen/OnSamsungViewListener;->c(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const v4, 0x3cf5c280    # 0.029999971f

    const/4 v3, 0x0

    .line 449
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->c:Lflipboard/gui/toc/TileView;

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->c:Lflipboard/gui/toc/TileView;

    sub-int v1, p4, p2

    sub-int v2, p5, p3

    invoke-virtual {v0, v3, v3, v1, v2}, Lflipboard/gui/toc/TileView;->layout(IIII)V

    .line 452
    :cond_0
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->d:Lflipboard/gui/toc/TileView;

    if-eqz v0, :cond_1

    .line 453
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->d:Lflipboard/gui/toc/TileView;

    sub-int v1, p4, p2

    sub-int v2, p5, p3

    invoke-virtual {v0, v3, v3, v1, v2}, Lflipboard/gui/toc/TileView;->layout(IIII)V

    .line 455
    :cond_1
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 456
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    .line 457
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v1

    .line 458
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v4

    div-float/2addr v2, v5

    float-to-int v2, v2

    .line 459
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getMeasuredHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v4

    div-float/2addr v3, v5

    float-to-int v3, v3

    .line 460
    neg-int v2, v2

    .line 461
    neg-int v3, v3

    .line 462
    iget-object v4, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    add-int/2addr v0, v2

    add-int/2addr v1, v3

    invoke-virtual {v4, v2, v3, v0, v1}, Landroid/widget/ImageView;->layout(IIII)V

    .line 465
    :cond_2
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->g:Lflipboard/gui/FLBusyView;

    if-eqz v0, :cond_3

    .line 466
    sub-int v0, p4, p2

    .line 467
    sub-int v1, p5, p3

    .line 468
    iget-object v2, p0, Lflipboard/gui/toc/TileContainer;->g:Lflipboard/gui/FLBusyView;

    invoke-virtual {v2}, Lflipboard/gui/FLBusyView;->getMeasuredWidth()I

    move-result v2

    .line 469
    iget-object v3, p0, Lflipboard/gui/toc/TileContainer;->g:Lflipboard/gui/FLBusyView;

    invoke-virtual {v3}, Lflipboard/gui/FLBusyView;->getMeasuredHeight()I

    move-result v3

    .line 470
    add-int/lit8 v4, v2, 0xa

    sub-int/2addr v0, v4

    .line 471
    add-int/lit8 v4, v3, 0xa

    sub-int/2addr v1, v4

    .line 472
    iget-object v4, p0, Lflipboard/gui/toc/TileContainer;->g:Lflipboard/gui/FLBusyView;

    add-int/2addr v2, v0

    add-int/2addr v3, v1

    invoke-virtual {v4, v0, v1, v2, v3}, Lflipboard/gui/FLBusyView;->layout(IIII)V

    .line 474
    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 425
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->c:Lflipboard/gui/toc/TileView;

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->c:Lflipboard/gui/toc/TileView;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/toc/TileView;->measure(II)V

    .line 428
    :cond_0
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->d:Lflipboard/gui/toc/TileView;

    if-eqz v0, :cond_1

    .line 429
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->d:Lflipboard/gui/toc/TileView;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/toc/TileView;->measure(II)V

    .line 431
    :cond_1
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 432
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 433
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090087

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 434
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    add-int/2addr v2, v1

    .line 435
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    add-int/2addr v0, v1

    .line 436
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/widget/ImageView;->measure(II)V

    .line 439
    :cond_2
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer;->g:Lflipboard/gui/FLBusyView;

    if-eqz v0, :cond_3

    .line 440
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090137

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 441
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->g:Lflipboard/gui/FLBusyView;

    invoke-virtual {v1, v0, v0}, Lflipboard/gui/FLBusyView;->measure(II)V

    .line 444
    :cond_3
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/toc/TileContainer;->setMeasuredDimension(II)V

    .line 445
    return-void
.end method

.method public setSPenHoverListener(Lflipboard/samsung/spen/OnSamsungViewListener;)V
    .locals 0

    .prologue
    .line 497
    iput-object p1, p0, Lflipboard/gui/toc/TileContainer;->s:Lflipboard/samsung/spen/OnSamsungViewListener;

    .line 498
    return-void
.end method

.method public setScale(F)V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    .line 171
    iput p1, p0, Lflipboard/gui/toc/TileContainer;->q:F

    .line 172
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lflipboard/gui/toc/TileContainer;->n:Lflipboard/gui/toc/DynamicGridLayout$State;

    sget-object v2, Lflipboard/gui/toc/DynamicGridLayout$State;->c:Lflipboard/gui/toc/DynamicGridLayout$State;

    if-ne v1, v2, :cond_0

    instance-of v1, v0, Lflipboard/gui/toc/TOCView;

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getLeft()I

    move-result v3

    div-int/lit8 v4, v1, 0x2

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->getTop()I

    move-result v4

    div-int/lit8 v5, v2, 0x2

    add-int/2addr v4, v5

    int-to-float v1, v1

    iget v5, p0, Lflipboard/gui/toc/TileContainer;->q:F

    mul-float/2addr v1, v5

    float-to-double v6, v1

    add-double/2addr v6, v8

    double-to-int v1, v6

    int-to-float v2, v2

    iget v5, p0, Lflipboard/gui/toc/TileContainer;->q:F

    mul-float/2addr v2, v5

    float-to-double v6, v2

    add-double/2addr v6, v8

    double-to-int v2, v6

    div-int/lit8 v5, v1, 0x2

    sub-int v5, v3, v5

    iget-object v6, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getWidth()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v6, v2, 0x2

    sub-int v6, v4, v6

    iget-object v7, p0, Lflipboard/gui/toc/TileContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getHeight()I

    move-result v7

    sub-int/2addr v6, v7

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v4

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/view/View;->invalidate(IIII)V

    .line 173
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer;->invalidate()V

    .line 174
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 222
    const-string v0, "%s[%s: state=%s, scale=%s, pendingFlip=%s]"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v3}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lflipboard/gui/toc/TileContainer;->n:Lflipboard/gui/toc/DynamicGridLayout$State;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lflipboard/gui/toc/TileContainer;->q:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lflipboard/gui/toc/TileContainer;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
