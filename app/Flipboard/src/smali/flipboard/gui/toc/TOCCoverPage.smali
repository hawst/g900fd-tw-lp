.class public Lflipboard/gui/toc/TOCCoverPage;
.super Lflipboard/gui/toc/TOCPage;
.source "TOCCoverPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Lflipboard/gui/FLStaticTextView;

.field private b:Z

.field private c:Landroid/widget/RelativeLayout;

.field private v:Lflipboard/gui/FLImageView;

.field private w:Landroid/view/View;

.field private x:Lflipboard/service/Section;

.field private y:Lflipboard/gui/toc/CoverStoryTileContainer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lflipboard/gui/toc/TOCPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 6

    .prologue
    .line 262
    invoke-super {p0}, Lflipboard/gui/toc/TOCPage;->a()V

    .line 263
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->y:Lflipboard/gui/toc/CoverStoryTileContainer;

    if-eqz v0, :cond_0

    .line 265
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverPage;->y:Lflipboard/gui/toc/CoverStoryTileContainer;

    const-string v2, "tocTile"

    invoke-virtual {v0, v1, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 266
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverPage;->y:Lflipboard/gui/toc/CoverStoryTileContainer;

    const-string v2, "%s-%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "tocTile"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "auth/flipboard/coverstories"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 268
    :cond_0
    return-void
.end method

.method final a(Ljava/util/List;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lflipboard/service/Section;",
            "Lflipboard/gui/toc/TileContainer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 114
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 115
    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v1

    if-nez v1, :cond_0

    .line 116
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Not cover stories: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 120
    :cond_0
    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverPage;->y:Lflipboard/gui/toc/CoverStoryTileContainer;

    if-nez v1, :cond_1

    .line 122
    new-instance v1, Lflipboard/gui/toc/CoverStoryTileContainer;

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverPage;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lflipboard/gui/toc/CoverStoryTileContainer;-><init>(Landroid/content/Context;Lflipboard/service/Section;)V

    .line 123
    iput-object v1, p0, Lflipboard/gui/toc/TOCCoverPage;->y:Lflipboard/gui/toc/CoverStoryTileContainer;

    .line 124
    invoke-virtual {v1, p0}, Lflipboard/gui/toc/CoverStoryTileContainer;->setSPenHoverListener(Lflipboard/samsung/spen/OnSamsungViewListener;)V

    .line 125
    iget-object v2, p0, Lflipboard/gui/toc/TOCCoverPage;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 126
    iput-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->x:Lflipboard/service/Section;

    .line 127
    new-instance v1, Lflipboard/gui/toc/TOCCoverPage$1;

    invoke-direct {v1, p0}, Lflipboard/gui/toc/TOCCoverPage$1;-><init>(Lflipboard/gui/toc/TOCCoverPage;)V

    .line 143
    new-array v2, v4, [Lflipboard/service/Section;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 147
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p1, v4, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-super {p0, v1, p2}, Lflipboard/gui/toc/TOCPage;->a(Ljava/util/List;Ljava/util/Map;)V

    .line 148
    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverPage;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v3, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(ILjava/lang/Object;)V

    .line 149
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 272
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    :cond_0
    :goto_0
    return-void

    .line 275
    :cond_1
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->v:Lflipboard/gui/FLImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 277
    const-string v1, "source"

    const-string v2, "toc"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const-string v1, "pageIndex"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 279
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverPage;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/toc/TOCCoverPage;->x:Lflipboard/service/Section;

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverPage;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 5

    .prologue
    const v4, 0x7f090132

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    const v0, 0x7f0a034f

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCCoverPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->c:Landroid/widget/RelativeLayout;

    .line 49
    const v0, 0x7f0a0350

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCCoverPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/DynamicGridLayout;

    iput-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    .line 50
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_0

    .line 51
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v0, v1, v3}, Lflipboard/gui/toc/DynamicGridLayout;->a(II)V

    .line 52
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v2, v1, v2, v2}, Lflipboard/gui/toc/DynamicGridLayout;->setPadding(IIII)V

    .line 57
    :goto_0
    const v0, 0x7f0a0352

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCCoverPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->w:Landroid/view/View;

    .line 58
    invoke-super {p0}, Lflipboard/gui/toc/TOCPage;->onFinishInflate()V

    .line 59
    return-void

    .line 54
    :cond_0
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v0, v3, v1}, Lflipboard/gui/toc/DynamicGridLayout;->a(II)V

    .line 55
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1, v2, v2, v2}, Lflipboard/gui/toc/DynamicGridLayout;->setPadding(IIII)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 205
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getMeasuredHeight()I

    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->y:Lflipboard/gui/toc/CoverStoryTileContainer;

    invoke-virtual {v0}, Lflipboard/gui/toc/TileContainer;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Lflipboard/gui/toc/TileContainer;->getMeasuredHeight()I

    move-result v2

    add-int/lit8 v1, v1, 0x0

    add-int/lit8 v2, v2, 0x0

    invoke-virtual {v0, v7, v7, v1, v2}, Lflipboard/gui/toc/TileContainer;->layout(IIII)V

    .line 207
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v8, :cond_0

    move v0, p2

    .line 212
    :goto_0
    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverPage;->getHeight()I

    move-result v2

    iget-object v3, p0, Lflipboard/gui/toc/TOCCoverPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v3}, Lflipboard/gui/toc/DynamicGridLayout;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v1, v0, v2, p4, p5}, Lflipboard/gui/toc/DynamicGridLayout;->layout(IIII)V

    .line 214
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->c:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverPage;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {v0, p2, p3, p4, v1}, Landroid/widget/RelativeLayout;->layout(IIII)V

    .line 217
    add-int/lit8 v0, p2, 0x0

    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverPage;->w:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    sub-int v1, p5, v1

    .line 218
    iget-object v2, p0, Lflipboard/gui/toc/TOCCoverPage;->w:Landroid/view/View;

    iget-object v3, p0, Lflipboard/gui/toc/TOCCoverPage;->w:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lflipboard/gui/toc/TOCCoverPage;->w:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 220
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/samsung/spen/TOCSectionPreview;

    .line 222
    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lflipboard/util/AndroidUtil;->c(Landroid/view/View;)[I

    move-result-object v2

    .line 223
    aget v1, v2, v8

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v1, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v1, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v1, v4

    .line 224
    aget v4, v2, v8

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v5

    add-int/2addr v4, v5

    .line 226
    iget-object v5, p0, Lflipboard/gui/toc/TOCCoverPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v5}, Lflipboard/gui/toc/DynamicGridLayout;->getBottom()I

    move-result v5

    add-int/2addr v5, p3

    if-le v1, v5, :cond_1

    .line 227
    aget v1, v2, v8

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v1, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v1, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingBottom()I

    move-result v4

    add-int/2addr v1, v4

    .line 234
    :goto_2
    aget v4, v2, v7

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v5

    add-int/2addr v4, v5

    .line 235
    aget v5, v2, v7

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    .line 237
    iget-object v6, p0, Lflipboard/gui/toc/TOCCoverPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v6}, Lflipboard/gui/toc/DynamicGridLayout;->getLeft()I

    move-result v6

    if-ge v4, v6, :cond_3

    .line 238
    aget v2, v2, v7

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v2, v4

    .line 245
    :goto_3
    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v0, v2, v1, v4, v5}, Lflipboard/samsung/spen/TOCSectionPreview;->layout(IIII)V

    goto/16 :goto_1

    .line 210
    :cond_0
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, p2

    goto/16 :goto_0

    .line 228
    :cond_1
    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v1}, Lflipboard/gui/toc/DynamicGridLayout;->getTop()I

    move-result v1

    add-int/2addr v1, p3

    if-ge v4, v1, :cond_2

    .line 229
    aget v1, v2, v8

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v4

    sub-int/2addr v1, v4

    goto :goto_2

    .line 231
    :cond_2
    aget v1, v2, v8

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v1, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v1, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v4

    sub-int/2addr v1, v4

    goto/16 :goto_2

    .line 239
    :cond_3
    iget-object v4, p0, Lflipboard/gui/toc/TOCCoverPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v4}, Lflipboard/gui/toc/DynamicGridLayout;->getRight()I

    move-result v4

    if-le v5, v4, :cond_4

    .line 240
    aget v2, v2, v7

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v2, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v2, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingRight()I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_3

    .line 242
    :cond_4
    aget v2, v2, v7

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v2, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v2, v4

    goto/16 :goto_3

    .line 247
    :cond_5
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/high16 v7, -0x80000000

    const/high16 v6, 0x40000000    # 2.0f

    .line 63
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/toc/TOCCoverPage;->setMeasuredDimension(II)V

    .line 65
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 66
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 68
    iget-boolean v0, p0, Lflipboard/gui/toc/TOCCoverPage;->b:Z

    if-eqz v0, :cond_1

    .line 70
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    .line 75
    :goto_0
    if-le v3, v2, :cond_2

    .line 76
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v0, v4

    double-to-int v0, v0

    .line 77
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    sub-int/2addr v1, v0

    .line 79
    iget-object v4, p0, Lflipboard/gui/toc/TOCCoverPage;->c:Landroid/widget/RelativeLayout;

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v4, p1, v0}, Landroid/widget/RelativeLayout;->measure(II)V

    .line 80
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getMeasuredWidth()I

    move-result v0

    iget-object v4, p0, Lflipboard/gui/toc/TOCCoverPage;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getMeasuredHeight()I

    move-result v4

    iget-object v5, p0, Lflipboard/gui/toc/TOCCoverPage;->c:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lflipboard/gui/toc/TOCCoverPage;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v5

    if-lez v5, :cond_0

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget-object v5, p0, Lflipboard/gui/toc/TOCCoverPage;->y:Lflipboard/gui/toc/CoverStoryTileContainer;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lflipboard/gui/toc/TOCCoverPage;->y:Lflipboard/gui/toc/CoverStoryTileContainer;

    invoke-virtual {v5, v0, v4}, Lflipboard/gui/toc/CoverStoryTileContainer;->measure(II)V

    .line 81
    :cond_0
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lflipboard/gui/toc/DynamicGridLayout;->measure(II)V

    .line 88
    :goto_1
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->w:Landroid/view/View;

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 89
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/samsung/spen/TOCSectionPreview;

    .line 90
    iget-boolean v2, v0, Lflipboard/samsung/spen/TOCSectionPreview;->f:Z

    if-eqz v2, :cond_3

    .line 92
    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lflipboard/samsung/spen/TOCSectionPreview;->measure(II)V

    goto :goto_2

    .line 72
    :cond_1
    const-wide v0, 0x3fe6666666666666L    # 0.7

    goto/16 :goto_0

    .line 83
    :cond_2
    int-to-double v4, v2

    mul-double/2addr v0, v4

    double-to-int v0, v0

    .line 84
    sub-int v1, v2, v0

    .line 85
    iget-object v4, p0, Lflipboard/gui/toc/TOCCoverPage;->c:Landroid/widget/RelativeLayout;

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v4, v0, p2}, Landroid/widget/RelativeLayout;->measure(II)V

    .line 86
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lflipboard/gui/toc/DynamicGridLayout;->measure(II)V

    goto :goto_1

    .line 94
    :cond_3
    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    mul-int/lit8 v3, v3, 0x3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lflipboard/samsung/spen/TOCSectionPreview;->measure(II)V

    goto/16 :goto_2

    .line 97
    :cond_4
    return-void
.end method

.method protected setCoverStoryProvenanceView(Lflipboard/gui/FLStaticTextView;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lflipboard/gui/toc/TOCCoverPage;->a:Lflipboard/gui/FLStaticTextView;

    .line 188
    return-void
.end method

.method setProvenance(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 160
    new-instance v0, Lflipboard/gui/SocialFormatter;

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverPage;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lflipboard/gui/SocialFormatter;-><init>(Landroid/content/Context;)V

    .line 162
    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverPage;->a:Lflipboard/gui/FLStaticTextView;

    if-eqz v1, :cond_0

    .line 163
    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverPage;->y:Lflipboard/gui/toc/CoverStoryTileContainer;

    invoke-virtual {v1}, Lflipboard/gui/toc/CoverStoryTileContainer;->getCoverStoryProvenanceView()Lflipboard/gui/FLStaticTextView;

    move-result-object v1

    .line 164
    if-eqz v1, :cond_0

    .line 165
    invoke-virtual {p0, v1}, Lflipboard/gui/toc/TOCCoverPage;->setCoverStoryProvenanceView(Lflipboard/gui/FLStaticTextView;)V

    .line 168
    :cond_0
    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverPage;->a:Lflipboard/gui/FLStaticTextView;

    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    .line 170
    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverPage;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverPage;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getPaddingLeft()I

    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverPage;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getPaddingRight()I

    .line 171
    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverPage;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMaxLines()I

    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverPage;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getPaint()Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Lflipboard/gui/SocialFormatter;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 173
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/toc/TOCCoverPage$2;

    invoke-direct {v2, p0, v0}, Lflipboard/gui/toc/TOCCoverPage$2;-><init>(Lflipboard/gui/toc/TOCCoverPage;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 183
    :cond_1
    return-void
.end method

.method protected setUse5UpToc(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 192
    iput-boolean p1, p0, Lflipboard/gui/toc/TOCCoverPage;->b:Z

    .line 194
    if-eqz p1, :cond_0

    move v0, v1

    .line 200
    :goto_0
    iget-object v2, p0, Lflipboard/gui/toc/TOCCoverPage;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/toc/DynamicGridLayout;->a(II)V

    .line 201
    return-void

    .line 198
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
