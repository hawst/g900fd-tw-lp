.class public Lflipboard/gui/toc/TOCSearchBoxContainer;
.super Lflipboard/gui/FLRelativeLayout;
.source "TOCSearchBoxContainer.java"


# instance fields
.field a:Landroid/widget/ImageView;

.field b:Landroid/widget/ImageView;

.field c:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/toc/TOCSearchBoxContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lflipboard/gui/toc/TOCSearchBoxContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 30
    const v1, 0x7f030141

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 31
    return-void
.end method


# virtual methods
.method public getRealWidth()I
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lflipboard/gui/toc/TOCSearchBoxContainer;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/toc/TOCSearchBoxContainer;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lflipboard/gui/toc/TOCSearchBoxContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onFinishInflate()V

    .line 46
    const v0, 0x7f0a0368

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCSearchBoxContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflipboard/gui/toc/TOCSearchBoxContainer;->a:Landroid/widget/ImageView;

    .line 47
    const v0, 0x7f0a0369

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCSearchBoxContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflipboard/gui/toc/TOCSearchBoxContainer;->b:Landroid/widget/ImageView;

    .line 48
    const v0, 0x7f0a0367

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCSearchBoxContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflipboard/gui/toc/TOCSearchBoxContainer;->c:Landroid/widget/ImageView;

    .line 49
    return-void
.end method
