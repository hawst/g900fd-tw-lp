.class public Lflipboard/gui/toc/TOCCoverStoryPageTablet;
.super Lflipboard/gui/toc/TOCPage;
.source "TOCCoverStoryPageTablet.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private A:Landroid/view/View;

.field private B:Landroid/view/View;

.field private C:Landroid/view/View;

.field private D:Landroid/view/View;

.field private E:I

.field private F:I

.field private G:Ljava/lang/String;

.field a:Lflipboard/gui/FLStaticTextView;

.field b:I

.field c:I

.field private v:Landroid/widget/RelativeLayout;

.field private w:Lflipboard/gui/FLImageView;

.field private x:Lflipboard/service/Section;

.field private y:Lflipboard/gui/toc/CoverStoryTileContainerTablet;

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x3

    .line 54
    invoke-direct {p0, p1, p2}, Lflipboard/gui/toc/TOCPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    iput v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->b:I

    .line 41
    iput v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->c:I

    .line 55
    return-void
.end method

.method static synthetic a(Lflipboard/gui/toc/TOCCoverStoryPageTablet;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->G:Ljava/lang/String;

    return-object v0
.end method

.method private setProvenance(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 116
    new-instance v0, Lflipboard/gui/SocialFormatter;

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lflipboard/gui/SocialFormatter;-><init>(Landroid/content/Context;)V

    .line 118
    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->a:Lflipboard/gui/FLStaticTextView;

    if-nez v1, :cond_0

    .line 119
    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->y:Lflipboard/gui/toc/CoverStoryTileContainerTablet;

    invoke-virtual {v1}, Lflipboard/gui/toc/CoverStoryTileContainerTablet;->getCoverStoryProvenanceView()Lflipboard/gui/FLStaticTextView;

    move-result-object v1

    .line 120
    if-eqz v1, :cond_0

    .line 121
    invoke-virtual {p0, v1}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->setCoverStoryProvenanceView(Lflipboard/gui/FLStaticTextView;)V

    .line 124
    :cond_0
    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->a:Lflipboard/gui/FLStaticTextView;

    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    .line 126
    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getPaddingLeft()I

    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getPaddingRight()I

    .line 127
    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMaxLines()I

    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getPaint()Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Lflipboard/gui/SocialFormatter;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->G:Ljava/lang/String;

    .line 129
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/toc/TOCCoverStoryPageTablet$1;

    invoke-direct {v1, p0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet$1;-><init>(Lflipboard/gui/toc/TOCCoverStoryPageTablet;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 139
    :cond_1
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 6

    .prologue
    .line 269
    invoke-super {p0}, Lflipboard/gui/toc/TOCPage;->a()V

    .line 270
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->y:Lflipboard/gui/toc/CoverStoryTileContainerTablet;

    if-eqz v0, :cond_0

    .line 272
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->y:Lflipboard/gui/toc/CoverStoryTileContainerTablet;

    const-string v2, "tocTile"

    invoke-virtual {v0, v1, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 273
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->y:Lflipboard/gui/toc/CoverStoryTileContainerTablet;

    const-string v2, "%s-%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "tocTile"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "auth/flipboard/coverstories"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 275
    :cond_0
    return-void
.end method

.method protected final a(Lflipboard/service/Section;)V
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p1}, Lflipboard/service/Section;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p1, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->setProvenance(Ljava/util/List;)V

    .line 112
    :cond_0
    return-void
.end method

.method final a(Ljava/util/List;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lflipboard/service/Section;",
            "Lflipboard/gui/toc/TileContainer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 85
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 86
    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v1

    if-nez v1, :cond_0

    .line 87
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Not cover stories: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 91
    :cond_0
    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->y:Lflipboard/gui/toc/CoverStoryTileContainerTablet;

    if-nez v1, :cond_1

    .line 93
    new-instance v1, Lflipboard/gui/toc/CoverStoryTileContainerTablet;

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lflipboard/gui/toc/CoverStoryTileContainerTablet;-><init>(Landroid/content/Context;Lflipboard/service/Section;)V

    .line 94
    iput-object v1, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->y:Lflipboard/gui/toc/CoverStoryTileContainerTablet;

    .line 95
    invoke-virtual {v1, p0}, Lflipboard/gui/toc/CoverStoryTileContainerTablet;->setSPenHoverListener(Lflipboard/samsung/spen/OnSamsungViewListener;)V

    .line 96
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2, v0}, Lflipboard/service/User;->a(Lflipboard/service/Section;)V

    .line 97
    iget-object v2, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->v:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 98
    iput-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->x:Lflipboard/service/Section;

    .line 99
    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->a(Lflipboard/service/Section;)V

    .line 103
    :cond_1
    const/4 v1, 0x1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {p1, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-super {p0, v1, p2}, Lflipboard/gui/toc/TOCPage;->a(Ljava/util/List;Ljava/util/Map;)V

    .line 104
    iget-object v1, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v3, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(ILjava/lang/Object;)V

    .line 105
    return-void
.end method

.method public getProvenanceText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->G:Ljava/lang/String;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 279
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 282
    :cond_1
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->w:Lflipboard/gui/FLImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 284
    const-string v1, "source"

    const-string v2, "toc"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const-string v1, "pageIndex"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 286
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->x:Lflipboard/service/Section;

    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 59
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->z:Z

    .line 60
    const v0, 0x7f0a034f

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->v:Landroid/widget/RelativeLayout;

    .line 61
    const v0, 0x7f0a0350

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/DynamicGridLayout;

    iput-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->i:Lflipboard/gui/toc/DynamicGridLayout;

    .line 62
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b000c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->b:I

    .line 63
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b000d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->c:I

    .line 64
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_1

    .line 65
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->i:Lflipboard/gui/toc/DynamicGridLayout;

    iget v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->c:I

    iget v4, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->b:I

    invoke-virtual {v0, v3, v4}, Lflipboard/gui/toc/DynamicGridLayout;->a(II)V

    .line 69
    :goto_1
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/DynamicGridLayout;->setIsCoverStoryPage(Z)V

    .line 70
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->A:Landroid/view/View;

    .line 71
    const v0, 0x7f0a0354

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->B:Landroid/view/View;

    .line 72
    const v0, 0x7f0a0355

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCSearchBoxContainer;

    iput-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    .line 73
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    invoke-virtual {v0, v2}, Lflipboard/gui/toc/TOCSearchBoxContainer;->setClipChildren(Z)V

    .line 74
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    invoke-virtual {v0, v2}, Lflipboard/gui/toc/TOCSearchBoxContainer;->setClipToPadding(Z)V

    .line 75
    invoke-virtual {p0, v2}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->setClipChildren(Z)V

    .line 76
    invoke-virtual {p0, v2}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->setClipToPadding(Z)V

    .line 77
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    new-instance v1, Lflipboard/gui/toc/TOCPage$SearchBoxOnClickListener;

    invoke-direct {v1, p0}, Lflipboard/gui/toc/TOCPage$SearchBoxOnClickListener;-><init>(Lflipboard/gui/toc/TOCPage;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/TOCSearchBoxContainer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    const v0, 0x7f0a0356

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->C:Landroid/view/View;

    .line 79
    const v0, 0x7f0a0359

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->D:Landroid/view/View;

    .line 80
    invoke-super {p0}, Lflipboard/gui/toc/TOCPage;->onFinishInflate()V

    .line 81
    return-void

    :cond_0
    move v0, v2

    .line 59
    goto/16 :goto_0

    .line 67
    :cond_1
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->i:Lflipboard/gui/toc/DynamicGridLayout;

    iget v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->b:I

    iget v4, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->c:I

    invoke-virtual {v0, v3, v4}, Lflipboard/gui/toc/DynamicGridLayout;->a(II)V

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 210
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09007d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 211
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09007b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 213
    iget-boolean v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->z:Z

    if-eqz v3, :cond_0

    .line 214
    iget v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->E:I

    iget-object v4, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->D:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v3, v4

    .line 215
    div-int/lit8 v3, v3, 0x2

    sub-int v3, p4, v3

    iget-object v4, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->D:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v3, v4

    .line 216
    iget-object v4, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->A:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int v4, p5, v4

    iget-object v5, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->D:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v4, v5

    .line 217
    iget-object v5, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->D:Landroid/view/View;

    sub-int v6, v3, v2

    iget-object v7, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->D:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v3

    iget-object v8, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->D:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v4

    invoke-virtual {v5, v6, v4, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 219
    iget-object v5, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->C:Landroid/view/View;

    sub-int v2, v3, v2

    iget-object v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->C:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v4, v3

    sub-int v0, v3, v0

    invoke-virtual {v5, v2, v0, p4, v4}, Landroid/view/View;->layout(IIII)V

    move v0, v1

    .line 230
    :goto_0
    iget-object v2, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->i:Lflipboard/gui/toc/DynamicGridLayout;

    iget-object v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v3}, Lflipboard/gui/toc/DynamicGridLayout;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v4}, Lflipboard/gui/toc/DynamicGridLayout;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2, v1, v0, v3, v4}, Lflipboard/gui/toc/DynamicGridLayout;->layout(IIII)V

    .line 233
    iget-object v2, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->v:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->v:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->v:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/widget/RelativeLayout;->layout(IIII)V

    .line 235
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->A:Landroid/view/View;

    iget-object v2, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->A:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    sub-int v2, p5, v2

    iget-object v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->A:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, p5}, Landroid/view/View;->layout(IIII)V

    .line 236
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->B:Landroid/view/View;

    iget-object v2, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->B:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int v2, p4, v2

    iget-object v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->B:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, p3

    invoke-virtual {v0, v2, p3, p4, v3}, Landroid/view/View;->layout(IIII)V

    .line 237
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    iget-object v2, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->B:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int v2, p4, v2

    iget-object v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    invoke-virtual {v3}, Lflipboard/gui/toc/TOCSearchBoxContainer;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    invoke-virtual {v3}, Lflipboard/gui/toc/TOCSearchBoxContainer;->getPaddingTop()I

    move-result v3

    add-int/2addr v3, p3

    iget-object v4, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->B:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    sub-int v4, p4, v4

    iget-object v5, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    invoke-virtual {v5}, Lflipboard/gui/toc/TOCSearchBoxContainer;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    invoke-virtual {v5}, Lflipboard/gui/toc/TOCSearchBoxContainer;->getPaddingTop()I

    move-result v5

    add-int/2addr v5, p3

    iget-object v6, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    invoke-virtual {v6}, Lflipboard/gui/toc/TOCSearchBoxContainer;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v0, v2, v3, v4, v5}, Lflipboard/gui/toc/TOCSearchBoxContainer;->layout(IIII)V

    .line 238
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/samsung/spen/TOCSectionPreview;

    .line 240
    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Lflipboard/util/AndroidUtil;->c(Landroid/view/View;)[I

    move-result-object v3

    .line 241
    aget v2, v3, v9

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v2, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v2, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v2, v5

    .line 242
    aget v5, v3, v9

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v6

    add-int/2addr v5, v6

    .line 244
    iget-object v6, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v6}, Lflipboard/gui/toc/DynamicGridLayout;->getTop()I

    move-result v6

    add-int/2addr v6, p3

    if-ge v5, v6, :cond_1

    .line 245
    aget v2, v3, v9

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v5

    sub-int/2addr v2, v5

    .line 252
    :goto_2
    aget v5, v3, v1

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v6

    add-int/2addr v5, v6

    .line 253
    aget v6, v3, v1

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    .line 255
    iget-object v7, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v7}, Lflipboard/gui/toc/DynamicGridLayout;->getLeft()I

    move-result v7

    if-ge v5, v7, :cond_3

    .line 256
    aget v3, v3, v1

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v3, v5

    .line 263
    :goto_3
    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v3

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v2

    invoke-virtual {v0, v3, v2, v5, v6}, Lflipboard/samsung/spen/TOCSectionPreview;->layout(IIII)V

    goto/16 :goto_1

    .line 222
    :cond_0
    iget-object v2, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->D:Landroid/view/View;

    iget-object v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->D:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->D:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2, v0, v0, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 226
    iget v2, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->F:I

    .line 227
    iget-object v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->C:Landroid/view/View;

    iget-object v4, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->C:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v2, v4

    iget-object v5, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->C:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v3, v0, v4, v5, v2}, Landroid/view/View;->layout(IIII)V

    .line 228
    iget v2, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->F:I

    add-int/2addr v0, v2

    goto/16 :goto_0

    .line 246
    :cond_1
    iget-object v5, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v5}, Lflipboard/gui/toc/DynamicGridLayout;->getBottom()I

    move-result v5

    add-int/2addr v5, p3

    if-le v2, v5, :cond_2

    .line 247
    aget v2, v3, v9

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v2, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v2, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingBottom()I

    move-result v5

    add-int/2addr v2, v5

    goto/16 :goto_2

    .line 249
    :cond_2
    aget v2, v3, v9

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v2, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredHeight()I

    move-result v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingBottom()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v2, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v5

    sub-int/2addr v2, v5

    goto/16 :goto_2

    .line 257
    :cond_3
    iget-object v5, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-virtual {v5}, Lflipboard/gui/toc/DynamicGridLayout;->getRight()I

    move-result v5

    if-le v6, v5, :cond_4

    .line 258
    aget v3, v3, v1

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v3, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredWidth()I

    move-result v5

    sub-int/2addr v3, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingRight()I

    move-result v5

    add-int/2addr v3, v5

    goto/16 :goto_3

    .line 260
    :cond_4
    aget v3, v3, v1

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v3, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getMeasuredWidth()I

    move-result v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v3, v5

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v3, v5

    goto/16 :goto_3

    .line 265
    :cond_5
    return-void
.end method

.method protected onMeasure(II)V
    .locals 13

    .prologue
    const/high16 v12, -0x80000000

    const/high16 v11, 0x40000000    # 2.0f

    const/4 v10, 0x0

    .line 153
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->setMeasuredDimension(II)V

    .line 155
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090132

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 156
    invoke-virtual {p0}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 157
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 158
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 162
    iget-object v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->B:Landroid/view/View;

    invoke-static {v2, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v1, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v3, v5, v6}, Landroid/view/View;->measure(II)V

    .line 163
    iget-object v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    invoke-static {v2, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v1, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v3, v5, v6}, Lflipboard/gui/toc/TOCSearchBoxContainer;->measure(II)V

    .line 165
    iget-object v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->B:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget-object v5, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    invoke-virtual {v5}, Lflipboard/gui/toc/TOCSearchBoxContainer;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v3, v5

    int-to-double v6, v3

    const-wide v8, 0x3ff199999999999aL    # 1.1

    mul-double/2addr v6, v8

    double-to-int v3, v6

    .line 166
    iget-boolean v5, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->z:Z

    if-eqz v5, :cond_1

    .line 167
    const-wide/high16 v6, 0x3fd0000000000000L    # 0.25

    int-to-double v8, v2

    mul-double/2addr v6, v8

    double-to-int v0, v6

    iput v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->E:I

    .line 168
    iget v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->E:I

    if-ge v0, v3, :cond_0

    .line 169
    iput v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->E:I

    .line 171
    :cond_0
    iput v1, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->F:I

    .line 173
    iget v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->E:I

    sub-int v0, v2, v0

    move v2, v0

    move v0, v1

    .line 179
    :goto_0
    iget-object v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->A:Landroid/view/View;

    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v1, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v3, v5, v6}, Landroid/view/View;->measure(II)V

    .line 183
    iget-object v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->A:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v5, v0, v3

    .line 188
    iget-boolean v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->z:Z

    if-eqz v0, :cond_2

    .line 189
    iget v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->c:I

    add-int/lit8 v0, v0, -0x1

    mul-int/2addr v0, v4

    sub-int v0, v5, v0

    iget v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->c:I

    div-int v3, v0, v3

    .line 190
    iget v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->b:I

    add-int/lit8 v0, v0, -0x1

    mul-int/2addr v0, v4

    sub-int v0, v2, v0

    iget v6, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->b:I

    div-int/2addr v0, v6

    .line 196
    :goto_1
    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v4

    .line 197
    iget-object v4, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->v:Landroid/widget/RelativeLayout;

    invoke-static {v0, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v3, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v4, v0, v3}, Landroid/widget/RelativeLayout;->measure(II)V

    .line 198
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->i:Lflipboard/gui/toc/DynamicGridLayout;

    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v5, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lflipboard/gui/toc/DynamicGridLayout;->measure(II)V

    .line 200
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->C:Landroid/view/View;

    iget v2, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->E:I

    invoke-static {v2, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v1, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->measure(II)V

    .line 201
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->D:Landroid/view/View;

    iget v2, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->E:I

    invoke-static {v2, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v1, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/view/View;->measure(II)V

    .line 202
    iget-object v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/samsung/spen/TOCSectionPreview;

    .line 203
    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingLeft()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getAnchor()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    mul-int/lit8 v3, v3, 0x3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v0}, Lflipboard/samsung/spen/TOCSectionPreview;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v3, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lflipboard/samsung/spen/TOCSectionPreview;->measure(II)V

    goto :goto_2

    .line 175
    :cond_1
    const-wide v6, 0x3fc999999999999aL    # 0.2

    int-to-double v8, v1

    mul-double/2addr v6, v8

    double-to-int v3, v6

    iput v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->F:I

    .line 176
    iput v2, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->E:I

    .line 177
    iget v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->F:I

    sub-int v3, v1, v3

    .line 178
    sub-int v0, v3, v0

    goto/16 :goto_0

    .line 192
    :cond_2
    iget v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->b:I

    add-int/lit8 v0, v0, -0x1

    mul-int/2addr v0, v4

    sub-int v0, v5, v0

    iget v3, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->b:I

    div-int v3, v0, v3

    .line 193
    iget v0, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->c:I

    add-int/lit8 v0, v0, -0x1

    mul-int/2addr v0, v4

    sub-int v0, v2, v0

    iget v6, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->c:I

    div-int/2addr v0, v6

    goto/16 :goto_1

    .line 206
    :cond_3
    return-void
.end method

.method protected setCoverStoryProvenanceView(Lflipboard/gui/FLStaticTextView;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->a:Lflipboard/gui/FLStaticTextView;

    .line 149
    return-void
.end method
