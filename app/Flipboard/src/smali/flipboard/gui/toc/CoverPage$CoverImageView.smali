.class Lflipboard/gui/toc/CoverPage$CoverImageView;
.super Lflipboard/gui/FLImageView;
.source "CoverPage.java"


# instance fields
.field f:Lflipboard/objs/FeedItem;

.field final synthetic g:Lflipboard/gui/toc/CoverPage;


# direct methods
.method constructor <init>(Lflipboard/gui/toc/CoverPage;Landroid/content/Context;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 366
    iput-object p1, p0, Lflipboard/gui/toc/CoverPage$CoverImageView;->g:Lflipboard/gui/toc/CoverPage;

    .line 367
    invoke-direct {p0, p2}, Lflipboard/gui/FLImageView;-><init>(Landroid/content/Context;)V

    .line 368
    sget-object v0, Lflipboard/gui/FLImageView$Align;->b:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/CoverPage$CoverImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 369
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/CoverPage$CoverImageView;->setScaling(Z)V

    .line 370
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/toc/CoverPage$CoverImageView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 371
    return-void
.end method


# virtual methods
.method protected final a(Lflipboard/io/BitmapManager$Handle;)V
    .locals 23
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 384
    new-instance v22, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x1

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 387
    sget-object v2, Lflipboard/gui/toc/CoverPage;->a:Lflipboard/util/Log;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/gui/toc/CoverPage$CoverImageView;->g:Lflipboard/gui/toc/CoverPage;

    iget v4, v4, Lflipboard/gui/toc/CoverPage;->r:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 388
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v2, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 389
    new-instance v3, Lflipboard/gui/toc/CoverPage$CoverImageView$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lflipboard/gui/toc/CoverPage$CoverImageView$1;-><init>(Lflipboard/gui/toc/CoverPage$CoverImageView;)V

    invoke-virtual {v2, v3}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 412
    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 414
    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 422
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/toc/CoverPage$CoverImageView;->f:Lflipboard/objs/FeedItem;

    if-eqz v2, :cond_3

    .line 423
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/toc/CoverPage$CoverImageView;->f:Lflipboard/objs/FeedItem;

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->K()I

    move-result v2

    int-to-float v2, v2

    .line 424
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/toc/CoverPage$CoverImageView;->f:Lflipboard/objs/FeedItem;

    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->L()I

    move-result v3

    int-to-float v3, v3

    move v4, v3

    move v3, v2

    .line 429
    :goto_0
    const/4 v2, 0x0

    .line 430
    const/4 v5, 0x0

    cmpl-float v5, v3, v5

    if-lez v5, :cond_0

    const/4 v5, 0x0

    cmpl-float v5, v4, v5

    if-lez v5, :cond_0

    .line 431
    div-float v2, v3, v4

    .line 433
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/toc/CoverPage$CoverImageView;->getMeasuredWidth()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/toc/CoverPage$CoverImageView;->getMeasuredHeight()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    .line 434
    sub-float v6, v2, v5

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    .line 438
    cmpl-float v7, v5, v2

    if-lez v7, :cond_4

    .line 439
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/toc/CoverPage$CoverImageView;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float v3, v7, v3

    mul-float/2addr v3, v4

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/toc/CoverPage$CoverImageView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 444
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/gui/toc/CoverPage$CoverImageView;->g:Lflipboard/gui/toc/CoverPage;

    invoke-static {v4}, Lflipboard/gui/toc/CoverPage;->b(Lflipboard/gui/toc/CoverPage;)Lflipboard/gui/toc/CoverPage$AnimationType;

    move-result-object v4

    sget-object v7, Lflipboard/gui/toc/CoverPage$AnimationType;->a:Lflipboard/gui/toc/CoverPage$AnimationType;

    if-eq v4, v7, :cond_8

    float-to-double v8, v6

    const-wide v10, 0x3fc999999999999aL    # 0.2

    cmpg-double v4, v8, v10

    if-ltz v4, :cond_1

    const/high16 v4, 0x3f800000    # 1.0f

    cmpg-float v4, v6, v4

    if-gez v4, :cond_8

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v6

    const-wide v8, 0x3fc999999999999aL    # 0.2

    cmpl-double v4, v6, v8

    if-lez v4, :cond_8

    .line 446
    :cond_1
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v2}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 447
    const-wide/16 v4, 0x1388

    move-wide/from16 v20, v4

    .line 451
    :goto_2
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x3fd0000000000000L    # 0.25

    add-double/2addr v4, v6

    double-to-float v5, v4

    .line 452
    const/high16 v8, 0x3f000000    # 0.5f

    .line 453
    const/high16 v10, 0x3f000000    # 0.5f

    .line 454
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/toc/CoverPage$CoverImageView;->f:Lflipboard/objs/FeedItem;

    if-eqz v2, :cond_2

    .line 455
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/toc/CoverPage$CoverImageView;->f:Lflipboard/objs/FeedItem;

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lflipboard/objs/Image;->e()Landroid/graphics/PointF;

    move-result-object v2

    .line 456
    :goto_3
    if-eqz v2, :cond_2

    .line 457
    iget v8, v2, Landroid/graphics/PointF;->x:F

    .line 458
    iget v10, v2, Landroid/graphics/PointF;->y:F

    .line 461
    :cond_2
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v6

    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    cmpl-double v2, v6, v12

    if-lez v2, :cond_7

    .line 463
    new-instance v2, Landroid/view/animation/ScaleAnimation;

    add-float v4, v5, v3

    add-float v6, v5, v3

    const/4 v7, 0x1

    const/4 v9, 0x1

    move v5, v3

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 467
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/toc/CoverPage$CoverImageView;->g:Lflipboard/gui/toc/CoverPage;

    sget-object v4, Lflipboard/gui/toc/CoverPage$AnimationType;->a:Lflipboard/gui/toc/CoverPage$AnimationType;

    invoke-static {v3, v4}, Lflipboard/gui/toc/CoverPage;->a(Lflipboard/gui/toc/CoverPage;Lflipboard/gui/toc/CoverPage$AnimationType;)Lflipboard/gui/toc/CoverPage$AnimationType;

    .line 504
    :goto_5
    move-wide/from16 v0, v20

    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 505
    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 506
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    .line 507
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 508
    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 510
    const/4 v2, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 511
    const/4 v2, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->setFillBefore(Z)V

    .line 512
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/CoverPage$CoverImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 513
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lflipboard/gui/toc/CoverPage$CoverImageView;->setVisibility(I)V

    .line 516
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v3, Lflipboard/gui/toc/CoverPage$CoverImageView$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lflipboard/gui/toc/CoverPage$CoverImageView$2;-><init>(Lflipboard/gui/toc/CoverPage$CoverImageView;)V

    const-wide/16 v4, 0x3e8

    add-long v4, v4, v20

    const-wide/16 v6, 0xc8

    sub-long/2addr v4, v6

    invoke-virtual {v2, v3, v4, v5}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;J)V

    .line 524
    return-void

    .line 426
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v2

    int-to-float v2, v2

    .line 427
    invoke-virtual/range {p1 .. p1}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v3

    int-to-float v3, v3

    move v4, v3

    move v3, v2

    goto/16 :goto_0

    .line 441
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/toc/CoverPage$CoverImageView;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float v4, v7, v4

    mul-float/2addr v3, v4

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/toc/CoverPage$CoverImageView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    goto/16 :goto_1

    .line 449
    :cond_5
    const-wide/16 v4, 0x1b58

    move-wide/from16 v20, v4

    goto/16 :goto_2

    .line 455
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 465
    :cond_7
    new-instance v11, Landroid/view/animation/ScaleAnimation;

    add-float v12, v5, v3

    add-float v14, v5, v3

    const/16 v16, 0x1

    const/16 v18, 0x1

    move v13, v3

    move v15, v3

    move/from16 v17, v8

    move/from16 v19, v10

    invoke-direct/range {v11 .. v19}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object v2, v11

    goto/16 :goto_4

    .line 470
    :cond_8
    sget-object v4, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v4}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 471
    const-wide/16 v20, 0x157c

    .line 476
    :goto_6
    cmpl-float v2, v2, v5

    if-lez v2, :cond_b

    .line 478
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/toc/CoverPage$CoverImageView;->g:Lflipboard/gui/toc/CoverPage;

    invoke-static {v2}, Lflipboard/gui/toc/CoverPage;->c(Lflipboard/gui/toc/CoverPage;)Lflipboard/gui/toc/CoverPage$TranslationDirection;

    move-result-object v2

    sget-object v4, Lflipboard/gui/toc/CoverPage$TranslationDirection;->b:Lflipboard/gui/toc/CoverPage$TranslationDirection;

    if-ne v2, v4, :cond_a

    .line 479
    new-instance v4, Landroid/view/animation/TranslateAnimation;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v8, v3, v2

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-direct/range {v4 .. v12}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 480
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/toc/CoverPage$CoverImageView;->g:Lflipboard/gui/toc/CoverPage;

    sget-object v5, Lflipboard/gui/toc/CoverPage$TranslationDirection;->a:Lflipboard/gui/toc/CoverPage$TranslationDirection;

    invoke-static {v2, v5}, Lflipboard/gui/toc/CoverPage;->a(Lflipboard/gui/toc/CoverPage;Lflipboard/gui/toc/CoverPage$TranslationDirection;)Lflipboard/gui/toc/CoverPage$TranslationDirection;

    move-object v11, v4

    .line 485
    :goto_7
    new-instance v2, Landroid/view/animation/ScaleAnimation;

    const/4 v7, 0x1

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x1

    const/high16 v10, 0x3f000000    # 0.5f

    move v4, v3

    move v5, v3

    move v6, v3

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 486
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/toc/CoverPage$CoverImageView;->g:Lflipboard/gui/toc/CoverPage;

    sget-object v4, Lflipboard/gui/toc/CoverPage$AnimationType;->b:Lflipboard/gui/toc/CoverPage$AnimationType;

    invoke-static {v3, v4}, Lflipboard/gui/toc/CoverPage;->a(Lflipboard/gui/toc/CoverPage;Lflipboard/gui/toc/CoverPage$AnimationType;)Lflipboard/gui/toc/CoverPage$AnimationType;

    .line 498
    :goto_8
    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v11, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 499
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    .line 500
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 501
    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    move-object v2, v11

    goto/16 :goto_5

    .line 473
    :cond_9
    const-wide/16 v20, 0x2328

    goto :goto_6

    .line 482
    :cond_a
    new-instance v4, Landroid/view/animation/TranslateAnimation;

    const/4 v5, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v6, v3, v2

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-direct/range {v4 .. v12}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 483
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/toc/CoverPage$CoverImageView;->g:Lflipboard/gui/toc/CoverPage;

    sget-object v5, Lflipboard/gui/toc/CoverPage$TranslationDirection;->b:Lflipboard/gui/toc/CoverPage$TranslationDirection;

    invoke-static {v2, v5}, Lflipboard/gui/toc/CoverPage;->a(Lflipboard/gui/toc/CoverPage;Lflipboard/gui/toc/CoverPage$TranslationDirection;)Lflipboard/gui/toc/CoverPage$TranslationDirection;

    move-object v11, v4

    goto :goto_7

    .line 488
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/toc/CoverPage$CoverImageView;->g:Lflipboard/gui/toc/CoverPage;

    invoke-static {v2}, Lflipboard/gui/toc/CoverPage;->c(Lflipboard/gui/toc/CoverPage;)Lflipboard/gui/toc/CoverPage$TranslationDirection;

    move-result-object v2

    sget-object v4, Lflipboard/gui/toc/CoverPage$TranslationDirection;->b:Lflipboard/gui/toc/CoverPage$TranslationDirection;

    if-ne v2, v4, :cond_c

    .line 489
    new-instance v4, Landroid/view/animation/TranslateAnimation;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v12, v3, v2

    invoke-direct/range {v4 .. v12}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 490
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/toc/CoverPage$CoverImageView;->g:Lflipboard/gui/toc/CoverPage;

    sget-object v5, Lflipboard/gui/toc/CoverPage$TranslationDirection;->a:Lflipboard/gui/toc/CoverPage$TranslationDirection;

    invoke-static {v2, v5}, Lflipboard/gui/toc/CoverPage;->a(Lflipboard/gui/toc/CoverPage;Lflipboard/gui/toc/CoverPage$TranslationDirection;)Lflipboard/gui/toc/CoverPage$TranslationDirection;

    move-object v11, v4

    .line 495
    :goto_9
    new-instance v2, Landroid/view/animation/ScaleAnimation;

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    const/4 v9, 0x1

    const/high16 v10, 0x3f800000    # 1.0f

    move v4, v3

    move v5, v3

    move v6, v3

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 496
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/toc/CoverPage$CoverImageView;->g:Lflipboard/gui/toc/CoverPage;

    sget-object v4, Lflipboard/gui/toc/CoverPage$AnimationType;->c:Lflipboard/gui/toc/CoverPage$AnimationType;

    invoke-static {v3, v4}, Lflipboard/gui/toc/CoverPage;->a(Lflipboard/gui/toc/CoverPage;Lflipboard/gui/toc/CoverPage$AnimationType;)Lflipboard/gui/toc/CoverPage$AnimationType;

    goto :goto_8

    .line 492
    :cond_c
    new-instance v4, Landroid/view/animation/TranslateAnimation;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v10, v3, v2

    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-direct/range {v4 .. v12}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 493
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/toc/CoverPage$CoverImageView;->g:Lflipboard/gui/toc/CoverPage;

    sget-object v5, Lflipboard/gui/toc/CoverPage$TranslationDirection;->b:Lflipboard/gui/toc/CoverPage$TranslationDirection;

    invoke-static {v2, v5}, Lflipboard/gui/toc/CoverPage;->a(Lflipboard/gui/toc/CoverPage;Lflipboard/gui/toc/CoverPage$TranslationDirection;)Lflipboard/gui/toc/CoverPage$TranslationDirection;

    move-object v11, v4

    goto :goto_9
.end method

.method public setImage(Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 375
    sget-object v0, Lflipboard/gui/toc/CoverPage$CoverImageView;->d:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 376
    iput-object p1, p0, Lflipboard/gui/toc/CoverPage$CoverImageView;->f:Lflipboard/objs/FeedItem;

    .line 377
    invoke-super {p0, p1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 378
    return-void
.end method
