.class public Lflipboard/gui/toc/CoverStoryTileTabletView;
.super Lflipboard/gui/toc/TileView;
.source "CoverStoryTileTabletView.java"


# instance fields
.field protected a:Lflipboard/gui/FLStaticTextView;

.field protected b:Landroid/view/View;

.field private q:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Lflipboard/gui/toc/TileContainer;Z)V
    .locals 6

    .prologue
    const/4 v2, -0x1

    .line 23
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lflipboard/gui/toc/TileView;-><init>(Lflipboard/gui/toc/TileContainer;IIZZ)V

    .line 24
    return-void
.end method


# virtual methods
.method protected final a(Z)V
    .locals 3

    .prologue
    .line 28
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "CoverStoryTileTablet:initView"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 29
    invoke-super {p0, p1}, Lflipboard/gui/toc/TileView;->a(Z)V

    .line 31
    invoke-virtual {p0}, Lflipboard/gui/toc/CoverStoryTileTabletView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 34
    const v1, 0x7f03013d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lflipboard/gui/toc/CoverStoryTileTabletView;->q:Landroid/view/ViewGroup;

    .line 35
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileTabletView;->q:Landroid/view/ViewGroup;

    const v1, 0x7f0a035d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/toc/CoverStoryTileTabletView;->a:Lflipboard/gui/FLStaticTextView;

    .line 36
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileTabletView;->q:Landroid/view/ViewGroup;

    const v1, 0x7f0a035e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/toc/CoverStoryTileTabletView;->b:Landroid/view/View;

    .line 37
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileTabletView;->a:Lflipboard/gui/FLStaticTextView;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->w:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 38
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileTabletView;->q:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/CoverStoryTileTabletView;->addView(Landroid/view/View;)V

    .line 41
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileTabletView;->e:Lflipboard/gui/FLStaticTextView;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileTabletView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lflipboard/gui/toc/CoverStoryTileTabletView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 44
    :cond_0
    return-void
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 48
    invoke-super {p0}, Lflipboard/gui/toc/TileView;->b()V

    .line 50
    new-instance v0, Lflipboard/gui/SocialFormatter;

    invoke-virtual {p0}, Lflipboard/gui/toc/CoverStoryTileTabletView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lflipboard/gui/SocialFormatter;-><init>(Landroid/content/Context;)V

    .line 51
    iget-object v1, p0, Lflipboard/gui/toc/CoverStoryTileTabletView;->p:Lflipboard/gui/toc/TileContainer;

    iget-object v1, v1, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-virtual {v0, v1}, Lflipboard/gui/SocialFormatter;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lflipboard/gui/toc/CoverStoryTileTabletView;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileTabletView;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 59
    return-void
.end method

.method public getCoverStoryProvenanceView()Lflipboard/gui/FLStaticTextView;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileTabletView;->a:Lflipboard/gui/FLStaticTextView;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 63
    invoke-super/range {p0 .. p5}, Lflipboard/gui/toc/TileView;->onLayout(ZIIII)V

    .line 64
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileTabletView;->q:Landroid/view/ViewGroup;

    invoke-virtual {v0, p2, p3, p4, p5}, Landroid/view/ViewGroup;->layout(IIII)V

    .line 65
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0, p1, p2}, Lflipboard/gui/toc/TileView;->onMeasure(II)V

    .line 70
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileTabletView;->q:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->measure(II)V

    .line 71
    return-void
.end method
