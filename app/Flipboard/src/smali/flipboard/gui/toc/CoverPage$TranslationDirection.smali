.class final enum Lflipboard/gui/toc/CoverPage$TranslationDirection;
.super Ljava/lang/Enum;
.source "CoverPage.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/toc/CoverPage$TranslationDirection;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/toc/CoverPage$TranslationDirection;

.field public static final enum b:Lflipboard/gui/toc/CoverPage$TranslationDirection;

.field private static final synthetic c:[Lflipboard/gui/toc/CoverPage$TranslationDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 356
    new-instance v0, Lflipboard/gui/toc/CoverPage$TranslationDirection;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/toc/CoverPage$TranslationDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/toc/CoverPage$TranslationDirection;->a:Lflipboard/gui/toc/CoverPage$TranslationDirection;

    new-instance v0, Lflipboard/gui/toc/CoverPage$TranslationDirection;

    const-string v1, "OPPOSITE"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/toc/CoverPage$TranslationDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/toc/CoverPage$TranslationDirection;->b:Lflipboard/gui/toc/CoverPage$TranslationDirection;

    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/gui/toc/CoverPage$TranslationDirection;

    sget-object v1, Lflipboard/gui/toc/CoverPage$TranslationDirection;->a:Lflipboard/gui/toc/CoverPage$TranslationDirection;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/toc/CoverPage$TranslationDirection;->b:Lflipboard/gui/toc/CoverPage$TranslationDirection;

    aput-object v1, v0, v3

    sput-object v0, Lflipboard/gui/toc/CoverPage$TranslationDirection;->c:[Lflipboard/gui/toc/CoverPage$TranslationDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 356
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/toc/CoverPage$TranslationDirection;
    .locals 1

    .prologue
    .line 356
    const-class v0, Lflipboard/gui/toc/CoverPage$TranslationDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/CoverPage$TranslationDirection;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/toc/CoverPage$TranslationDirection;
    .locals 1

    .prologue
    .line 356
    sget-object v0, Lflipboard/gui/toc/CoverPage$TranslationDirection;->c:[Lflipboard/gui/toc/CoverPage$TranslationDirection;

    invoke-virtual {v0}, [Lflipboard/gui/toc/CoverPage$TranslationDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/toc/CoverPage$TranslationDirection;

    return-object v0
.end method
