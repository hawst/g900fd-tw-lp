.class Lflipboard/gui/toc/TOCPage$1;
.super Ljava/lang/Object;
.source "TOCPage.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/io/NetworkManager;",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/gui/toc/TOCPage;


# direct methods
.method constructor <init>(Lflipboard/gui/toc/TOCPage;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lflipboard/gui/toc/TOCPage$1;->a:Lflipboard/gui/toc/TOCPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 105
    check-cast p2, Ljava/lang/Boolean;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Notify connection on TOC page, connected: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-static {}, Lflipboard/service/FlipboardManager;->g()V

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/toc/TOCPage$1$1;

    invoke-direct {v1, p0, p2}, Lflipboard/gui/toc/TOCPage$1$1;-><init>(Lflipboard/gui/toc/TOCPage$1;Ljava/lang/Boolean;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    return-void
.end method
