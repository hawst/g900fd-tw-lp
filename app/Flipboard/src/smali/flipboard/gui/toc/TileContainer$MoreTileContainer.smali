.class public Lflipboard/gui/toc/TileContainer$MoreTileContainer;
.super Landroid/widget/FrameLayout;
.source "TileContainer.java"

# interfaces
.implements Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;
.implements Lflipboard/util/FLAnimation$Scaleable;


# instance fields
.field a:Lflipboard/gui/toc/DynamicGridLayout$State;

.field private b:Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;

.field private c:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 819
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 814
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->c:F

    .line 815
    sget-object v0, Lflipboard/gui/toc/DynamicGridLayout$State;->a:Lflipboard/gui/toc/DynamicGridLayout$State;

    iput-object v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->a:Lflipboard/gui/toc/DynamicGridLayout$State;

    .line 820
    new-instance v0, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;-><init>(Lflipboard/gui/toc/TileContainer$MoreTileContainer;Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->b:Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;

    .line 821
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->b:Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->addView(Landroid/view/View;)V

    .line 822
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->setWillNotDraw(Z)V

    .line 823
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/gui/toc/DynamicGridLayout$State;Z)V
    .locals 4

    .prologue
    const/16 v0, 0x15e

    const/4 v1, 0x0

    .line 840
    iget-object v2, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->a:Lflipboard/gui/toc/DynamicGridLayout$State;

    if-eq v2, p1, :cond_0

    .line 841
    iput-object p1, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->a:Lflipboard/gui/toc/DynamicGridLayout$State;

    .line 842
    sget-object v2, Lflipboard/gui/toc/TileContainer$9;->a:[I

    invoke-virtual {p1}, Lflipboard/gui/toc/DynamicGridLayout$State;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 847
    new-instance v2, Lflipboard/util/FLAnimation$Scale;

    const/high16 v3, 0x3f800000    # 1.0f

    if-eqz p2, :cond_2

    :goto_0
    invoke-direct {v2, p0, v3, v0}, Lflipboard/util/FLAnimation$Scale;-><init>(Lflipboard/util/FLAnimation$Scaleable;FI)V

    .line 850
    :cond_0
    :goto_1
    return-void

    .line 844
    :pswitch_0
    new-instance v2, Lflipboard/util/FLAnimation$Scale;

    const v3, 0x3f7851ec    # 0.97f

    if-eqz p2, :cond_1

    :goto_2
    invoke-direct {v2, p0, v3, v0}, Lflipboard/util/FLAnimation$Scale;-><init>(Lflipboard/util/FLAnimation$Scaleable;FI)V

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_2

    :cond_2
    move v0, v1

    .line 847
    goto :goto_0

    .line 842
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 868
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Z)Landroid/view/View;
    .locals 1

    .prologue
    .line 867
    const/4 v0, 0x0

    return-object v0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 830
    iget v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->c:F

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 857
    iget v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->c:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 858
    iget v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->c:F

    iget v1, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->c:F

    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 860
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 861
    return-void
.end method

.method public setCount(I)V
    .locals 1

    .prologue
    .line 865
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->b:Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;

    invoke-virtual {v0, p1}, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->setCount(I)V

    .line 866
    return-void
.end method

.method public setScale(F)V
    .locals 0

    .prologue
    .line 834
    iput p1, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->c:F

    .line 835
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer;->invalidate()V

    .line 836
    return-void
.end method
