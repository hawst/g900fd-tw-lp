.class Lflipboard/gui/toc/TOCPage$6;
.super Ljava/lang/Object;
.source "TOCPage.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/gui/toc/TOCPage;


# direct methods
.method constructor <init>(Lflipboard/gui/toc/TOCPage;)V
    .locals 0

    .prologue
    .line 702
    iput-object p1, p0, Lflipboard/gui/toc/TOCPage$6;->a:Lflipboard/gui/toc/TOCPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 706
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage$6;->a:Lflipboard/gui/toc/TOCPage;

    iget-object v0, v0, Lflipboard/gui/toc/TOCPage;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->getPages()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCPage;

    .line 707
    invoke-virtual {v0}, Lflipboard/gui/toc/TOCPage;->getPageNumber()I

    move-result v2

    iget-object v3, p0, Lflipboard/gui/toc/TOCPage$6;->a:Lflipboard/gui/toc/TOCPage;

    iget v3, v3, Lflipboard/gui/toc/TOCPage;->g:I

    if-eq v2, v3, :cond_0

    .line 708
    invoke-virtual {v0}, Lflipboard/gui/toc/TOCPage;->getSections()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 709
    invoke-virtual {v0}, Lflipboard/service/Section;->n()V

    goto :goto_0

    .line 713
    :cond_1
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage$6;->a:Lflipboard/gui/toc/TOCPage;

    iget-object v0, v0, Lflipboard/gui/toc/TOCPage;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 714
    invoke-virtual {v0}, Lflipboard/service/Section;->m()Ljava/util/List;

    goto :goto_1

    .line 716
    :cond_2
    return-void
.end method
