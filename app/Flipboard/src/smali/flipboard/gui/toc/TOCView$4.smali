.class Lflipboard/gui/toc/TOCView$4;
.super Ljava/lang/Object;
.source "TOCView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/gui/toc/TOCView;


# direct methods
.method constructor <init>(Lflipboard/gui/toc/TOCView;)V
    .locals 0

    .prologue
    .line 465
    iput-object p1, p0, Lflipboard/gui/toc/TOCView$4;->a:Lflipboard/gui/toc/TOCView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 468
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/gui/toc/TOCView$4;->a:Lflipboard/gui/toc/TOCView;

    invoke-static {v1}, Lflipboard/gui/toc/TOCView;->b(Lflipboard/gui/toc/TOCView;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/service/User;->a(Ljava/util/List;Z)V

    .line 469
    iget-object v0, p0, Lflipboard/gui/toc/TOCView$4;->a:Lflipboard/gui/toc/TOCView;

    iget-object v1, p0, Lflipboard/gui/toc/TOCView$4;->a:Lflipboard/gui/toc/TOCView;

    invoke-static {v1}, Lflipboard/gui/toc/TOCView;->c(Lflipboard/gui/toc/TOCView;)Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    move-result-object v1

    iget-object v1, v1, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->p:Lflipboard/gui/toc/DynamicGridLayout$State;

    iput-object v1, v0, Lflipboard/gui/toc/TOCView;->Q:Lflipboard/gui/toc/DynamicGridLayout$State;

    .line 473
    iget-object v0, p0, Lflipboard/gui/toc/TOCView$4;->a:Lflipboard/gui/toc/TOCView;

    invoke-static {v0}, Lflipboard/gui/toc/TOCView;->d(Lflipboard/gui/toc/TOCView;)I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/toc/TOCView$4;->a:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v1}, Lflipboard/gui/toc/TOCView;->getFlippableViews()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 474
    iget-object v1, p0, Lflipboard/gui/toc/TOCView$4;->a:Lflipboard/gui/toc/TOCView;

    iget-object v0, p0, Lflipboard/gui/toc/TOCView$4;->a:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->getFlippableViews()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lflipboard/gui/toc/TOCView$4;->a:Lflipboard/gui/toc/TOCView;

    invoke-static {v2}, Lflipboard/gui/toc/TOCView;->e(Lflipboard/gui/toc/TOCView;)I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Lflipboard/gui/toc/TOCView;->a(Landroid/view/ViewGroup;)V

    .line 476
    :cond_0
    return-void
.end method
