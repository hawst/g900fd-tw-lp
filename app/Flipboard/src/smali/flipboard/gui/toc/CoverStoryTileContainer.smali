.class public Lflipboard/gui/toc/CoverStoryTileContainer;
.super Lflipboard/gui/toc/TileContainer;
.source "CoverStoryTileContainer.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lflipboard/service/Section;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lflipboard/gui/toc/TileContainer;-><init>(Landroid/content/Context;Lflipboard/service/Section;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected final a(Z)Lflipboard/gui/toc/TileView;
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lflipboard/gui/toc/CoverStoryTileView;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/toc/CoverStoryTileView;-><init>(Lflipboard/gui/toc/TileContainer;Z)V

    return-object v0
.end method

.method public final a(Lflipboard/service/Section;Lflipboard/service/Section$Message;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 32
    invoke-super {p0, p1, p2, p3}, Lflipboard/gui/toc/TileContainer;->a(Lflipboard/service/Section;Lflipboard/service/Section$Message;Ljava/lang/Object;)V

    .line 33
    sget-object v0, Lflipboard/gui/toc/CoverStoryTileContainer$1;->a:[I

    invoke-virtual {p2}, Lflipboard/service/Section$Message;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 45
    :cond_0
    :goto_0
    return-void

    .line 35
    :pswitch_0
    invoke-virtual {p1}, Lflipboard/service/Section;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    check-cast p3, Lflipboard/json/FLObject;

    .line 37
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileContainer;->c:Lflipboard/gui/toc/TileView;

    check-cast v0, Lflipboard/gui/toc/CoverStoryTileView;

    .line 39
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_0

    const-string v1, "changes"

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v2}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, v0, Lflipboard/gui/toc/CoverStoryTileView;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/JavaUtil;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/toc/CoverStoryTileContainer;->c()Lflipboard/gui/toc/TOCPage;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCCoverStoryPageTablet;

    invoke-virtual {v0, p1}, Lflipboard/gui/toc/TOCCoverStoryPageTablet;->a(Lflipboard/service/Section;)V

    goto :goto_0

    .line 33
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lflipboard/service/Section;

    check-cast p2, Lflipboard/service/Section$Message;

    invoke-virtual {p0, p1, p2, p3}, Lflipboard/gui/toc/CoverStoryTileContainer;->a(Lflipboard/service/Section;Lflipboard/service/Section$Message;Ljava/lang/Object;)V

    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return v0
.end method

.method protected final b()Z
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    return v0
.end method

.method public getCoverStoryProvenanceView()Lflipboard/gui/FLStaticTextView;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileContainer;->c:Lflipboard/gui/toc/TileView;

    check-cast v0, Lflipboard/gui/toc/CoverStoryTileView;

    invoke-virtual {v0}, Lflipboard/gui/toc/CoverStoryTileView;->getCoverStoryProvenanceView()Lflipboard/gui/FLStaticTextView;

    move-result-object v0

    return-object v0
.end method
