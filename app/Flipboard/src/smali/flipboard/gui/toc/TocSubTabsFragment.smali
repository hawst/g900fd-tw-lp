.class public Lflipboard/gui/toc/TocSubTabsFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "TocSubTabsFragment.java"


# instance fields
.field a:Landroid/support/v4/view/ViewPager;

.field b:Lflipboard/gui/tabs/SlidingTabLayout;

.field c:Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 36
    const v0, 0x7f030148

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 37
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 38
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {}, Lflipboard/service/User;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 39
    new-instance v1, Lflipboard/gui/toc/TocTabNoTopicsPagerAdapter;

    invoke-virtual {p0}, Lflipboard/gui/toc/TocSubTabsFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-direct {v1, v2}, Lflipboard/gui/toc/TocTabNoTopicsPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    iput-object v1, p0, Lflipboard/gui/toc/TocSubTabsFragment;->c:Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;

    .line 43
    :goto_0
    iget-object v1, p0, Lflipboard/gui/toc/TocSubTabsFragment;->a:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lflipboard/gui/toc/TocSubTabsFragment;->c:Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 44
    iget-object v1, p0, Lflipboard/gui/toc/TocSubTabsFragment;->b:Lflipboard/gui/tabs/SlidingTabLayout;

    iget-object v2, p0, Lflipboard/gui/toc/TocSubTabsFragment;->a:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lflipboard/gui/toc/TocSubTabsFragment;->c:Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;

    invoke-virtual {v1, v5, v2, v3}, Lflipboard/gui/tabs/SlidingTabLayout;->a(ILandroid/support/v4/view/ViewPager;Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;)V

    .line 45
    iget-object v1, p0, Lflipboard/gui/toc/TocSubTabsFragment;->b:Lflipboard/gui/tabs/SlidingTabLayout;

    new-array v2, v5, [I

    aput v4, v2, v4

    invoke-virtual {v1, v2}, Lflipboard/gui/tabs/SlidingTabLayout;->setSelectedIndicatorColors([I)V

    .line 46
    return-object v0

    .line 41
    :cond_0
    new-instance v1, Lflipboard/gui/toc/TocTabPagerAdapter;

    invoke-virtual {p0}, Lflipboard/gui/toc/TocSubTabsFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-direct {v1, v2}, Lflipboard/gui/toc/TocTabPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    iput-object v1, p0, Lflipboard/gui/toc/TocSubTabsFragment;->c:Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;

    goto :goto_0
.end method
