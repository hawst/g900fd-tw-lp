.class Lflipboard/gui/toc/TOCView$2;
.super Ljava/lang/Object;
.source "TOCView.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/FlipboardManager;",
        "Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/gui/toc/TOCView;


# direct methods
.method constructor <init>(Lflipboard/gui/toc/TOCView;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lflipboard/gui/toc/TOCView$2;->a:Lflipboard/gui/toc/TOCView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 116
    check-cast p2, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    sget-object v0, Lflipboard/gui/toc/TOCView$6;->a:[I

    invoke-virtual {p2}, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/toc/TOCView$2$1;

    invoke-direct {v1, p0}, Lflipboard/gui/toc/TOCView$2$1;-><init>(Lflipboard/gui/toc/TOCView$2;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    :pswitch_1
    sget-object v0, Lflipboard/gui/toc/TOCView;->a:Lflipboard/util/Log;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/toc/TOCView$2$2;

    invoke-direct {v1, p0}, Lflipboard/gui/toc/TOCView$2$2;-><init>(Lflipboard/gui/toc/TOCView$2;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lflipboard/gui/toc/TOCView$2;->a:Lflipboard/gui/toc/TOCView;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, p3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/TOCView;->a(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
