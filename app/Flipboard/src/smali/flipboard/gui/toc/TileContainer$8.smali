.class Lflipboard/gui/toc/TileContainer$8;
.super Ljava/lang/Object;
.source "TileContainer.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/gui/toc/TileView;

.field final synthetic b:Lflipboard/gui/toc/TileView;

.field final synthetic c:Lflipboard/gui/toc/TileContainer;


# direct methods
.method constructor <init>(Lflipboard/gui/toc/TileContainer;Lflipboard/gui/toc/TileView;Lflipboard/gui/toc/TileView;)V
    .locals 0

    .prologue
    .line 716
    iput-object p1, p0, Lflipboard/gui/toc/TileContainer$8;->c:Lflipboard/gui/toc/TileContainer;

    iput-object p2, p0, Lflipboard/gui/toc/TileContainer$8;->a:Lflipboard/gui/toc/TileView;

    iput-object p3, p0, Lflipboard/gui/toc/TileContainer$8;->b:Lflipboard/gui/toc/TileView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 720
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$8;->c:Lflipboard/gui/toc/TileContainer;

    iget-object v1, p0, Lflipboard/gui/toc/TileContainer$8;->a:Lflipboard/gui/toc/TileView;

    iput-object v1, v0, Lflipboard/gui/toc/TileContainer;->c:Lflipboard/gui/toc/TileView;

    .line 721
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$8;->c:Lflipboard/gui/toc/TileContainer;

    const/4 v1, 0x0

    iput-object v1, v0, Lflipboard/gui/toc/TileContainer;->d:Lflipboard/gui/toc/TileView;

    .line 722
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$8;->c:Lflipboard/gui/toc/TileContainer;

    invoke-static {v0}, Lflipboard/gui/toc/TileContainer;->b(Lflipboard/gui/toc/TileContainer;)V

    .line 725
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$8;->c:Lflipboard/gui/toc/TileContainer;

    invoke-virtual {v0}, Lflipboard/gui/toc/TileContainer;->d()Lflipboard/gui/toc/TOCView;

    move-result-object v12

    .line 726
    if-nez v12, :cond_1

    .line 746
    :cond_0
    :goto_0
    return-void

    .line 730
    :cond_1
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$8;->c:Lflipboard/gui/toc/TileContainer;

    invoke-virtual {v0}, Lflipboard/gui/toc/TileContainer;->c()Lflipboard/gui/toc/TOCPage;

    move-result-object v0

    .line 731
    if-eqz v0, :cond_0

    .line 735
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$8;->c:Lflipboard/gui/toc/TileContainer;

    invoke-virtual {v0}, Lflipboard/gui/toc/TileContainer;->c()Lflipboard/gui/toc/TOCPage;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCPage;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lflipboard/gui/flipping/FlippingContainer;

    .line 736
    iget-object v13, p0, Lflipboard/gui/toc/TileContainer$8;->a:Lflipboard/gui/toc/TileView;

    iget-object v9, p0, Lflipboard/gui/toc/TileContainer$8;->b:Lflipboard/gui/toc/TileView;

    new-instance v14, Lflipboard/gui/toc/TileContainer$8$1;

    invoke-direct {v14, p0}, Lflipboard/gui/toc/TileContainer$8$1;-><init>(Lflipboard/gui/toc/TileContainer$8;)V

    if-eqz v8, :cond_4

    iget-boolean v0, v8, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    if-eqz v0, :cond_4

    if-eqz v13, :cond_4

    if-eqz v9, :cond_4

    iget-object v0, v12, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    const-string v1, "updateTileWithFlip"

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/String;)V

    invoke-virtual {v13}, Lflipboard/gui/toc/TileView;->getWidth()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {v13}, Lflipboard/gui/toc/TileView;->getHeight()I

    move-result v0

    if-gtz v0, :cond_3

    :cond_2
    invoke-virtual {v9}, Lflipboard/gui/toc/TileView;->getWidth()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v9}, Lflipboard/gui/toc/TileView;->getHeight()I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v13, v0, v1}, Lflipboard/gui/toc/TileView;->measure(II)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v13}, Lflipboard/gui/toc/TileView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v13}, Lflipboard/gui/toc/TileView;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v13, v0, v1, v2, v3}, Lflipboard/gui/toc/TileView;->layout(IIII)V

    :cond_3
    invoke-static {v9}, Lflipboard/util/AndroidUtil;->c(Landroid/view/View;)[I

    move-result-object v11

    invoke-virtual {v9}, Lflipboard/gui/toc/TileView;->getWidth()I

    move-result v2

    invoke-virtual {v9}, Lflipboard/gui/toc/TileView;->getHeight()I

    move-result v3

    new-instance v0, Lflipboard/gui/flipping/TileFlip;

    iget-object v1, v12, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-direct {v0, v12, v1, v8}, Lflipboard/gui/flipping/TileFlip;-><init>(Lflipboard/gui/flipping/FlipTransitionBase;Lflipboard/gui/flipping/OpenGLTransitionRenderer;Lflipboard/gui/flipping/FlippingContainer;)V

    const/4 v1, 0x1

    iget v4, v12, Lflipboard/gui/flipping/FlipTransitionViews;->g:I

    iget v5, v12, Lflipboard/gui/flipping/FlipTransitionViews;->f:I

    const/4 v6, 0x0

    aget v6, v11, v6

    int-to-float v6, v6

    const/4 v7, 0x1

    aget v7, v11, v7

    int-to-float v7, v7

    invoke-virtual/range {v0 .. v7}, Lflipboard/gui/flipping/TileFlip;->a(ZIIIIFF)V

    sget-object v1, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    invoke-virtual {v1, v9}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Landroid/view/View;)Lflipboard/gui/flipping/FlippingBitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/TileFlip;->a(Lflipboard/gui/flipping/FlippingBitmap;)V

    new-instance v4, Lflipboard/gui/flipping/TileFlip;

    iget-object v1, v12, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-direct {v4, v12, v1, v8}, Lflipboard/gui/flipping/TileFlip;-><init>(Lflipboard/gui/flipping/FlipTransitionBase;Lflipboard/gui/flipping/OpenGLTransitionRenderer;Lflipboard/gui/flipping/FlippingContainer;)V

    const/4 v5, 0x1

    iget v8, v12, Lflipboard/gui/flipping/FlipTransitionViews;->g:I

    iget v9, v12, Lflipboard/gui/flipping/FlipTransitionViews;->f:I

    const/4 v1, 0x0

    aget v1, v11, v1

    int-to-float v10, v1

    const/4 v1, 0x1

    aget v1, v11, v1

    int-to-float v11, v1

    move v6, v2

    move v7, v3

    invoke-virtual/range {v4 .. v11}, Lflipboard/gui/flipping/TileFlip;->a(ZIIIIFF)V

    sget-object v1, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    invoke-virtual {v1, v13}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Landroid/view/View;)Lflipboard/gui/flipping/FlippingBitmap;

    move-result-object v1

    invoke-virtual {v4, v1}, Lflipboard/gui/flipping/TileFlip;->a(Lflipboard/gui/flipping/FlippingBitmap;)V

    const/4 v1, 0x0

    iput-boolean v1, v13, Lflipboard/gui/toc/TileView;->n:Z

    invoke-virtual {v4}, Lflipboard/gui/flipping/TileFlip;->m()V

    iput-object v0, v4, Lflipboard/gui/flipping/TileFlip;->N:Lflipboard/gui/flipping/SinglePage;

    iget-object v1, v12, Lflipboard/gui/flipping/FlipTransitionViews;->E:Lflipboard/util/Log;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v4, Lflipboard/gui/flipping/TileFlip;->Q:Ljava/lang/Boolean;

    iput-object v4, v0, Lflipboard/gui/flipping/TileFlip;->O:Lflipboard/gui/flipping/SinglePage;

    invoke-virtual {v13}, Lflipboard/gui/toc/TileView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lflipboard/gui/flipping/FlipTransitionViews$9;

    invoke-direct {v2, v12, v13, v4}, Lflipboard/gui/flipping/FlipTransitionViews$9;-><init>(Lflipboard/gui/flipping/FlipTransitionViews;Lflipboard/gui/toc/TileView;Lflipboard/gui/flipping/TileFlip;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    iget-object v1, v12, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1, v4}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Lflipboard/gui/flipping/TileFlip;)V

    iget-object v1, v12, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v1, v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Lflipboard/gui/flipping/TileFlip;)V

    iget-object v0, v12, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0, v14}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Lflipboard/gui/flipping/TileFlip;->a(F)V

    iget-object v0, v12, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    iget-object v0, v0, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->h:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    iget-object v0, v12, Lflipboard/gui/flipping/FlipTransitionViews;->c:Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    invoke-virtual {v0}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a()V

    goto/16 :goto_0

    :cond_4
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, v14}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto/16 :goto_0
.end method
