.class public Lflipboard/gui/toc/TocTabPagerAdapter;
.super Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;
.source "TocTabPagerAdapter.java"


# instance fields
.field private b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lflipboard/activities/FlipboardFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentManager;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 30
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lflipboard/gui/toc/TocTabPagerAdapter;->b:Landroid/util/SparseArray;

    .line 34
    return-void
.end method


# virtual methods
.method public final synthetic a(I)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 22
    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to create fragment for invalid position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-instance v0, Lflipboard/gui/personal/TocGridFragment;

    invoke-direct {v0}, Lflipboard/gui/personal/TocGridFragment;-><init>()V

    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lflipboard/gui/personal/TocDrawerListFragment$Filter;->b:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    invoke-static {v0}, Lflipboard/gui/personal/TocDrawerListFragment;->a(Lflipboard/gui/personal/TocDrawerListFragment$Filter;)Lflipboard/gui/personal/TocDrawerListFragment;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    sget-object v0, Lflipboard/gui/personal/TocDrawerListFragment$Filter;->c:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    invoke-static {v0}, Lflipboard/gui/personal/TocDrawerListFragment;->a(Lflipboard/gui/personal/TocDrawerListFragment$Filter;)Lflipboard/gui/personal/TocDrawerListFragment;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    new-instance v0, Lflipboard/gui/personal/SocialNetworksFragment;

    invoke-direct {v0}, Lflipboard/gui/personal/SocialNetworksFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string v2, "key_social_networks_is_settings"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardFragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 95
    invoke-super {p0, p1, p2}, Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardFragment;

    .line 96
    iget-object v1, p0, Lflipboard/gui/toc/TocTabPagerAdapter;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 97
    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lflipboard/gui/toc/TocTabPagerAdapter;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 103
    invoke-super {p0, p1, p2, p3}, Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 104
    return-void
.end method

.method public final synthetic b(I)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 22
    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Try to get title for invalid position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d002c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0329

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d032c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d035a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x4

    return v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method
