.class Lflipboard/gui/toc/TOCPage$SearchBoxOnClickListener;
.super Ljava/lang/Object;
.source "TOCPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/toc/TOCPage;


# direct methods
.method constructor <init>(Lflipboard/gui/toc/TOCPage;)V
    .locals 0

    .prologue
    .line 722
    iput-object p1, p0, Lflipboard/gui/toc/TOCPage$SearchBoxOnClickListener;->a:Lflipboard/gui/toc/TOCPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    .line 727
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage$SearchBoxOnClickListener;->a:Lflipboard/gui/toc/TOCPage;

    invoke-static {v0}, Lflipboard/gui/toc/TOCPage;->a(Lflipboard/gui/toc/TOCPage;)V

    .line 729
    iget-object v0, p0, Lflipboard/gui/toc/TOCPage$SearchBoxOnClickListener;->a:Lflipboard/gui/toc/TOCPage;

    iget-object v9, v0, Lflipboard/gui/toc/TOCPage;->s:Lflipboard/gui/toc/TOCSearchBoxContainer;

    iget-object v0, p0, Lflipboard/gui/toc/TOCPage$SearchBoxOnClickListener;->a:Lflipboard/gui/toc/TOCPage;

    iget-object v0, v0, Lflipboard/gui/toc/TOCPage;->t:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v10

    new-instance v11, Lflipboard/gui/toc/TOCPage$SearchBoxOnClickListener$1;

    invoke-direct {v11, p0}, Lflipboard/gui/toc/TOCPage$SearchBoxOnClickListener$1;-><init>(Lflipboard/gui/toc/TOCPage$SearchBoxOnClickListener;)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    int-to-float v4, v10

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    iget-object v1, v9, Lflipboard/gui/toc/TOCSearchBoxContainer;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, v9, Lflipboard/gui/toc/TOCSearchBoxContainer;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v10

    int-to-float v0, v0

    iget-object v1, v9, Lflipboard/gui/toc/TOCSearchBoxContainer;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v2, v0, v1

    new-instance v0, Landroid/view/animation/ScaleAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, v9, Lflipboard/gui/toc/TOCSearchBoxContainer;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v0, v11}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 744
    return-void
.end method
