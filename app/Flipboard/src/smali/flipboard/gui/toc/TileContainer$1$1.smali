.class Lflipboard/gui/toc/TileContainer$1$1;
.super Ljava/lang/Object;
.source "TileContainer.java"

# interfaces
.implements Lflipboard/util/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Callback",
        "<",
        "Lflipboard/gui/toc/TOCView;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lflipboard/gui/toc/TileContainer$1;


# direct methods
.method constructor <init>(Lflipboard/gui/toc/TileContainer$1;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lflipboard/gui/toc/TileContainer$1$1;->b:Lflipboard/gui/toc/TileContainer$1;

    iput-object p2, p0, Lflipboard/gui/toc/TileContainer$1$1;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 88
    check-cast p1, Lflipboard/gui/toc/TOCView;

    invoke-virtual {p1}, Lflipboard/gui/toc/TOCView;->getRunningFlips()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lflipboard/gui/toc/TileContainer$1$1;->a:Landroid/view/View;

    iget-object v0, p1, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    if-eqz v0, :cond_0

    iget-object v2, p1, Lflipboard/gui/toc/TOCView;->P:Lflipboard/gui/toc/DynamicGridLayout$DragManager;

    iput-object v1, v2, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->c:Landroid/view/View;

    move-object v0, v1

    check-cast v0, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Lflipboard/gui/toc/DynamicGridLayout$DragCandidate;->b(Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->d:Landroid/view/View;

    iget-object v0, v2, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->d:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    const-class v3, Lflipboard/gui/FLImageView;

    new-instance v4, Lflipboard/gui/toc/DynamicGridLayout$DragManager$3;

    invoke-direct {v4, v2}, Lflipboard/gui/toc/DynamicGridLayout$DragManager$3;-><init>(Lflipboard/gui/toc/DynamicGridLayout$DragManager;)V

    invoke-static {v0, v3, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/ViewGroup;Ljava/lang/Class;Lflipboard/util/Callback;)V

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iput v0, v2, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->e:I

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, v2, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->f:I

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->a(Z)V

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    iget-object v4, v2, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->d:Landroid/view/View;

    iget v5, v2, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->i:I

    add-int/2addr v0, v5

    iget v5, v2, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->j:I

    add-int/2addr v3, v5

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v5

    iget v6, v2, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->i:I

    add-int/2addr v5, v6

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v6

    iget v7, v2, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->j:I

    add-int/2addr v6, v7

    invoke-virtual {v4, v0, v3, v5, v6}, Landroid/view/View;->layout(IIII)V

    iget-object v0, v2, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->d:Landroid/view/View;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v2, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->m:Landroid/view/ViewGroup;

    iget-object v3, v2, Lflipboard/gui/toc/DynamicGridLayout$DragManager;->d:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-wide/16 v4, 0x14

    new-instance v3, Lflipboard/gui/toc/DynamicGridLayout$DragManager$4;

    invoke-direct {v3, v2}, Lflipboard/gui/toc/DynamicGridLayout$DragManager$4;-><init>(Lflipboard/gui/toc/DynamicGridLayout$DragManager;)V

    invoke-virtual {v0, v4, v5, v3}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-wide/16 v4, 0x64

    new-instance v3, Lflipboard/gui/toc/DynamicGridLayout$DragManager$5;

    invoke-direct {v3, v2, v1}, Lflipboard/gui/toc/DynamicGridLayout$DragManager$5;-><init>(Lflipboard/gui/toc/DynamicGridLayout$DragManager;Landroid/view/View;)V

    invoke-virtual {v0, v4, v5, v3}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    :cond_0
    return-void
.end method
