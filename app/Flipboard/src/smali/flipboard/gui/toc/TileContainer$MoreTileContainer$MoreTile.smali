.class public Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;
.super Lflipboard/gui/FLRelativeLayout;
.source "TileContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field protected a:Lflipboard/gui/FLStaticTextView;

.field final synthetic b:Lflipboard/gui/toc/TileContainer$MoreTileContainer;

.field private c:I


# direct methods
.method public constructor <init>(Lflipboard/gui/toc/TileContainer$MoreTileContainer;Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v2, -0x2

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 878
    iput-object p1, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->b:Lflipboard/gui/toc/TileContainer$MoreTileContainer;

    .line 879
    invoke-direct {p0, p2}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 881
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 882
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080058

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->setBackgroundColor(I)V

    .line 883
    new-instance v0, Lflipboard/gui/FLStaticTextView;

    const/4 v1, 0x0

    invoke-direct {v0, p2, v1}, Lflipboard/gui/FLStaticTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->a:Lflipboard/gui/FLStaticTextView;

    .line 884
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 885
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 886
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 887
    iget-object v1, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 889
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090138

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v2, p2}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v4, v1}, Lflipboard/gui/FLStaticTextView;->a(II)V

    .line 890
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080059

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    .line 892
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->a:Lflipboard/gui/FLStaticTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setId(I)V

    .line 893
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->a:Lflipboard/gui/FLStaticTextView;

    const/16 v1, 0xa

    invoke-virtual {v0, v4, v4, v1, v4}, Lflipboard/gui/FLStaticTextView;->setPadding(IIII)V

    .line 894
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->addView(Landroid/view/View;)V

    .line 896
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 897
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 898
    const v1, 0x7f02021e

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 900
    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->addView(Landroid/view/View;)V

    .line 902
    invoke-virtual {p0, p0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 903
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 913
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 927
    :goto_0
    return-void

    .line 916
    :cond_0
    const-class v0, Lflipboard/gui/toc/TOCView;

    invoke-static {p0, v0}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCView;

    .line 917
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->v()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 918
    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->w()V

    goto :goto_0

    .line 922
    :cond_1
    iget v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->c:I

    if-lez v0, :cond_2

    .line 923
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget v1, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->c:I

    invoke-static {v0, v1}, Lflipboard/activities/ContentDrawerTabletActivity;->a(Landroid/app/Activity;I)V

    goto :goto_0

    .line 925
    :cond_2
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflipboard/activities/ContentDrawerPhoneActivity;->a(Landroid/app/Activity;I)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    .line 931
    iget v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->c:I

    if-lez v0, :cond_0

    .line 932
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0325

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 936
    :goto_0
    invoke-super {p0, p1, p2}, Lflipboard/gui/FLRelativeLayout;->onMeasure(II)V

    .line 937
    return-void

    .line 934
    :cond_0
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d002a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method setCount(I)V
    .locals 0

    .prologue
    .line 907
    iput p1, p0, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->c:I

    .line 908
    invoke-virtual {p0}, Lflipboard/gui/toc/TileContainer$MoreTileContainer$MoreTile;->requestLayout()V

    .line 909
    return-void
.end method
