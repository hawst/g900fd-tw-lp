.class public final enum Lflipboard/gui/toc/DynamicGridLayout$State;
.super Ljava/lang/Enum;
.source "DynamicGridLayout.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/toc/DynamicGridLayout$State;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/toc/DynamicGridLayout$State;

.field public static final enum b:Lflipboard/gui/toc/DynamicGridLayout$State;

.field public static final enum c:Lflipboard/gui/toc/DynamicGridLayout$State;

.field private static final synthetic d:[Lflipboard/gui/toc/DynamicGridLayout$State;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 70
    new-instance v0, Lflipboard/gui/toc/DynamicGridLayout$State;

    const-string v1, "normal"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/toc/DynamicGridLayout$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/toc/DynamicGridLayout$State;->a:Lflipboard/gui/toc/DynamicGridLayout$State;

    new-instance v0, Lflipboard/gui/toc/DynamicGridLayout$State;

    const-string v1, "editing"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/toc/DynamicGridLayout$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/toc/DynamicGridLayout$State;->b:Lflipboard/gui/toc/DynamicGridLayout$State;

    new-instance v0, Lflipboard/gui/toc/DynamicGridLayout$State;

    const-string v1, "dragging"

    invoke-direct {v0, v1, v4}, Lflipboard/gui/toc/DynamicGridLayout$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/toc/DynamicGridLayout$State;->c:Lflipboard/gui/toc/DynamicGridLayout$State;

    const/4 v0, 0x3

    new-array v0, v0, [Lflipboard/gui/toc/DynamicGridLayout$State;

    sget-object v1, Lflipboard/gui/toc/DynamicGridLayout$State;->a:Lflipboard/gui/toc/DynamicGridLayout$State;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/toc/DynamicGridLayout$State;->b:Lflipboard/gui/toc/DynamicGridLayout$State;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/gui/toc/DynamicGridLayout$State;->c:Lflipboard/gui/toc/DynamicGridLayout$State;

    aput-object v1, v0, v4

    sput-object v0, Lflipboard/gui/toc/DynamicGridLayout$State;->d:[Lflipboard/gui/toc/DynamicGridLayout$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/toc/DynamicGridLayout$State;
    .locals 1

    .prologue
    .line 70
    const-class v0, Lflipboard/gui/toc/DynamicGridLayout$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/DynamicGridLayout$State;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/toc/DynamicGridLayout$State;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lflipboard/gui/toc/DynamicGridLayout$State;->d:[Lflipboard/gui/toc/DynamicGridLayout$State;

    invoke-virtual {v0}, [Lflipboard/gui/toc/DynamicGridLayout$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/toc/DynamicGridLayout$State;

    return-object v0
.end method
