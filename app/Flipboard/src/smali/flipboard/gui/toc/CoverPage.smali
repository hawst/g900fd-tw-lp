.class public Lflipboard/gui/toc/CoverPage;
.super Lflipboard/gui/FLRelativeLayout;
.source "CoverPage.java"

# interfaces
.implements Lflipboard/gui/FLViewIntf;
.implements Lflipboard/gui/item/TabletItem;
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/gui/FLRelativeLayout;",
        "Lflipboard/gui/FLViewIntf;",
        "Lflipboard/gui/item/TabletItem;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/Section;",
        "Lflipboard/service/Section$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Lflipboard/util/Log;

.field static final b:[I


# instance fields
.field public c:Lflipboard/service/Section;

.field d:Landroid/view/View;

.field e:Landroid/view/ViewGroup;

.field f:Lflipboard/gui/FLStaticTextView;

.field g:Lflipboard/gui/FLImageView;

.field h:Lflipboard/gui/FLTextIntf;

.field i:Lflipboard/gui/FLTextIntf;

.field j:Lflipboard/gui/FLTextIntf;

.field k:Lflipboard/gui/FLTextIntf;

.field public l:I

.field public final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/io/Download;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lflipboard/objs/FeedItem;

.field q:Ljava/util/TimerTask;

.field r:I

.field s:I

.field public t:Z

.field u:Z

.field private v:Landroid/widget/ImageView;

.field private w:Lflipboard/objs/FeedItem;

.field private x:Lflipboard/gui/toc/CoverPage$AnimationType;

.field private y:Lflipboard/gui/toc/CoverPage$TranslationDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-string v0, "cover"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/toc/CoverPage;->a:Lflipboard/util/Log;

    .line 50
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lflipboard/gui/toc/CoverPage;->b:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0200cd
        0x7f0200cf
        0x7f0200d3
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/toc/CoverPage;->m:Ljava/util/List;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/toc/CoverPage;->o:Ljava/util/List;

    .line 72
    const/high16 v0, -0x80000000

    iput v0, p0, Lflipboard/gui/toc/CoverPage;->s:I

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/toc/CoverPage;->m:Ljava/util/List;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/toc/CoverPage;->o:Ljava/util/List;

    .line 72
    const/high16 v0, -0x80000000

    iput v0, p0, Lflipboard/gui/toc/CoverPage;->s:I

    .line 85
    return-void
.end method

.method static synthetic a(Lflipboard/gui/toc/CoverPage;Lflipboard/gui/toc/CoverPage$AnimationType;)Lflipboard/gui/toc/CoverPage$AnimationType;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lflipboard/gui/toc/CoverPage;->x:Lflipboard/gui/toc/CoverPage$AnimationType;

    return-object p1
.end method

.method static synthetic a(Lflipboard/gui/toc/CoverPage;Lflipboard/gui/toc/CoverPage$TranslationDirection;)Lflipboard/gui/toc/CoverPage$TranslationDirection;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lflipboard/gui/toc/CoverPage;->y:Lflipboard/gui/toc/CoverPage$TranslationDirection;

    return-object p1
.end method

.method static synthetic a(Lflipboard/gui/toc/CoverPage;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lflipboard/gui/toc/CoverPage;->c()V

    return-void
.end method

.method private a(Lflipboard/objs/FeedItem;Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/objs/FeedItem;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v3, 0x15e

    const/4 v1, 0x0

    .line 222
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->O()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->P()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223
    :cond_0
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 224
    invoke-direct {p0, v0, p2}, Lflipboard/gui/toc/CoverPage;->a(Lflipboard/objs/FeedItem;Ljava/util/Set;)V

    goto :goto_0

    .line 227
    :cond_1
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->an:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 228
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget v0, v0, Lflipboard/app/FlipboardApplication;->d:I

    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget v2, v2, Lflipboard/app/FlipboardApplication;->e:I

    invoke-virtual {p1, v0, v2}, Lflipboard/objs/FeedItem;->a(II)Ljava/lang/String;

    move-result-object v0

    .line 232
    if-eqz v0, :cond_2

    .line 233
    iget-object v2, p0, Lflipboard/gui/toc/CoverPage;->o:Ljava/util/List;

    sget-object v3, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    invoke-virtual {v3, v0, v1, v1}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;ZZ)Lflipboard/io/Download;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    :cond_2
    iget-object v0, p1, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v0, :cond_c

    iget-object v0, p1, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v0

    .line 236
    :goto_2
    if-eqz v0, :cond_3

    .line 237
    iget-object v2, p0, Lflipboard/gui/toc/CoverPage;->o:Ljava/util/List;

    sget-object v3, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    invoke-virtual {v3, v0, v1, v1}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;ZZ)Lflipboard/io/Download;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    :cond_3
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 241
    iget-object v1, v0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 242
    iget-object v0, v0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 245
    :cond_4
    return-void

    .line 227
    :cond_5
    iget v0, p1, Lflipboard/objs/FeedItem;->ak:I

    if-lez v0, :cond_6

    move v0, v1

    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->g()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    goto :goto_1

    :cond_7
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->K()I

    move-result v2

    if-lt v2, v3, :cond_8

    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->L()I

    move-result v2

    if-ge v2, v3, :cond_9

    :cond_8
    move v0, v1

    goto :goto_1

    :cond_9
    if-nez v0, :cond_a

    iget-object v0, p1, Lflipboard/objs/FeedItem;->x:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p1, Lflipboard/objs/FeedItem;->x:Ljava/lang/String;

    const-string v2, "graphics"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v1

    goto :goto_1

    :cond_a
    invoke-virtual {v0}, Lflipboard/objs/Image;->d()Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v1

    goto/16 :goto_1

    :cond_b
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 235
    :cond_c
    const/4 v0, 0x0

    goto :goto_2
.end method

.method static synthetic b(Lflipboard/gui/toc/CoverPage;)Lflipboard/gui/toc/CoverPage$AnimationType;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->x:Lflipboard/gui/toc/CoverPage$AnimationType;

    return-object v0
.end method

.method static synthetic c(Lflipboard/gui/toc/CoverPage;)Lflipboard/gui/toc/CoverPage$TranslationDirection;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->y:Lflipboard/gui/toc/CoverPage$TranslationDirection;

    return-object v0
.end method

.method private c()V
    .locals 7

    .prologue
    const/16 v6, 0xa

    const/4 v5, 0x4

    const/4 v2, 0x0

    .line 175
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/Download;

    .line 176
    invoke-virtual {v0}, Lflipboard/io/Download;->b()V

    goto :goto_0

    .line 178
    :cond_0
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 179
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 182
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 184
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->c:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->m()Ljava/util/List;

    .line 185
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->c:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 186
    invoke-direct {p0, v0, v1}, Lflipboard/gui/toc/CoverPage;->a(Lflipboard/objs/FeedItem;Ljava/util/Set;)V

    goto :goto_1

    .line 189
    :cond_1
    iput v2, p0, Lflipboard/gui/toc/CoverPage;->l:I

    .line 190
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->m:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 193
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 194
    invoke-static {v3}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 196
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->k:Lflipboard/gui/FLTextIntf;

    if-eqz v0, :cond_5

    .line 197
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move v1, v2

    .line 198
    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 199
    if-lez v1, :cond_2

    .line 200
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 202
    :cond_2
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 204
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v5, :cond_4

    .line 205
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 206
    invoke-virtual {p0}, Lflipboard/gui/toc/CoverPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00b4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    :cond_4
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->k:Lflipboard/gui/FLTextIntf;

    invoke-interface {v0, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 212
    :cond_5
    sget-object v0, Lflipboard/gui/toc/CoverPage;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/gui/toc/CoverPage;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    .line 213
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/toc/CoverPage;->t:Z

    .line 90
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->h:Lflipboard/service/Section;

    iput-object v0, p0, Lflipboard/gui/toc/CoverPage;->c:Lflipboard/service/Section;

    .line 91
    const v0, 0x7f0a029a

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/CoverPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lflipboard/gui/toc/CoverPage;->e:Landroid/view/ViewGroup;

    .line 92
    const v0, 0x7f0a029c

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/CoverPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/toc/CoverPage;->d:Landroid/view/View;

    .line 93
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/CoverPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/toc/CoverPage;->f:Lflipboard/gui/FLStaticTextView;

    .line 94
    const v0, 0x7f0a029e

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/CoverPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/toc/CoverPage;->g:Lflipboard/gui/FLImageView;

    .line 95
    const v0, 0x7f0a0274

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/CoverPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, p0, Lflipboard/gui/toc/CoverPage;->h:Lflipboard/gui/FLTextIntf;

    .line 96
    const v0, 0x7f0a02a0

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/CoverPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, p0, Lflipboard/gui/toc/CoverPage;->i:Lflipboard/gui/FLTextIntf;

    .line 97
    const v0, 0x7f0a029f

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/CoverPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, p0, Lflipboard/gui/toc/CoverPage;->j:Lflipboard/gui/FLTextIntf;

    .line 98
    const v0, 0x7f0a034d

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/CoverPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, p0, Lflipboard/gui/toc/CoverPage;->k:Lflipboard/gui/FLTextIntf;

    .line 99
    const v0, 0x7f0a0066

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/CoverPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflipboard/gui/toc/CoverPage;->v:Landroid/widget/ImageView;

    .line 101
    sget-boolean v0, Lflipboard/service/FlipboardManager;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->v:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->v:Landroid/widget/ImageView;

    const v1, 0x7f0200e8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 104
    :cond_0
    return-void
.end method

.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 346
    iput-object p2, p0, Lflipboard/gui/toc/CoverPage;->w:Lflipboard/objs/FeedItem;

    .line 347
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 47
    check-cast p2, Lflipboard/service/Section$Message;

    sget-object v0, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    if-ne p2, v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/toc/CoverPage$1;

    invoke-direct {v1, p0}, Lflipboard/gui/toc/CoverPage$1;-><init>(Lflipboard/gui/toc/CoverPage;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public final a(ZI)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 133
    sget-object v0, Lflipboard/gui/toc/CoverPage;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    iget-boolean v2, p0, Lflipboard/gui/toc/CoverPage;->t:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 134
    if-eqz p1, :cond_1

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-gt v0, v3, :cond_1

    .line 135
    iput p2, p0, Lflipboard/gui/toc/CoverPage;->s:I

    .line 136
    iget-boolean v0, p0, Lflipboard/gui/toc/CoverPage;->t:Z

    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {p0}, Lflipboard/gui/toc/CoverPage;->b()V

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    const/high16 v0, -0x80000000

    iput v0, p0, Lflipboard/gui/toc/CoverPage;->s:I

    .line 141
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->q:Ljava/util/TimerTask;

    if-eqz v0, :cond_2

    .line 142
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->q:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/toc/CoverPage;->q:Ljava/util/TimerTask;

    .line 145
    :cond_2
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    :goto_1
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_3

    .line 146
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    move v0, v1

    goto :goto_1

    .line 148
    :cond_3
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 149
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 151
    :cond_4
    iput-boolean v3, p0, Lflipboard/gui/toc/CoverPage;->t:Z

    goto :goto_0
.end method

.method final b()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 249
    sget-object v0, Lflipboard/gui/toc/CoverPage;->a:Lflipboard/util/Log;

    new-array v0, v6, [Ljava/lang/Object;

    iget v1, p0, Lflipboard/gui/toc/CoverPage;->r:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    iget v1, p0, Lflipboard/gui/toc/CoverPage;->s:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lflipboard/gui/toc/CoverPage;->t:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v5

    .line 250
    iget v0, p0, Lflipboard/gui/toc/CoverPage;->s:I

    if-eqz v0, :cond_0

    .line 251
    iput-boolean v2, p0, Lflipboard/gui/toc/CoverPage;->t:Z

    .line 306
    :goto_0
    return-void

    .line 255
    :cond_0
    iput-boolean v4, p0, Lflipboard/gui/toc/CoverPage;->t:Z

    .line 256
    iget v0, p0, Lflipboard/gui/toc/CoverPage;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/gui/toc/CoverPage;->r:I

    .line 258
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 259
    iget v0, p0, Lflipboard/gui/toc/CoverPage;->l:I

    add-int/lit8 v0, v0, 0x1

    sget-object v1, Lflipboard/gui/toc/CoverPage;->b:[I

    array-length v1, v1

    rem-int/2addr v0, v1

    iput v0, p0, Lflipboard/gui/toc/CoverPage;->l:I

    .line 260
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/toc/CoverPage;->p:Lflipboard/objs/FeedItem;

    .line 262
    new-instance v0, Lflipboard/gui/toc/CoverPage$CoverImageView;

    invoke-virtual {p0}, Lflipboard/gui/toc/CoverPage;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lflipboard/gui/toc/CoverPage$CoverImageView;-><init>(Lflipboard/gui/toc/CoverPage;Landroid/content/Context;)V

    .line 263
    invoke-static {v0, v3}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 265
    sget-object v1, Lflipboard/gui/toc/CoverPage;->b:[I

    iget v2, p0, Lflipboard/gui/toc/CoverPage;->l:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/CoverPage$CoverImageView;->setBitmap(I)V

    .line 266
    iget-object v1, p0, Lflipboard/gui/toc/CoverPage;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 268
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->f:Lflipboard/gui/FLStaticTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 269
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->d:Landroid/view/View;

    invoke-static {v0, v3}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 305
    :goto_1
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->p:Lflipboard/objs/FeedItem;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/CoverPage;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 271
    :cond_1
    iget v0, p0, Lflipboard/gui/toc/CoverPage;->l:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lflipboard/gui/toc/CoverPage;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, Lflipboard/gui/toc/CoverPage;->l:I

    .line 272
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->m:Ljava/util/List;

    iget v1, p0, Lflipboard/gui/toc/CoverPage;->l:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iput-object v0, p0, Lflipboard/gui/toc/CoverPage;->p:Lflipboard/objs/FeedItem;

    .line 273
    sget-object v0, Lflipboard/gui/toc/CoverPage;->a:Lflipboard/util/Log;

    new-array v0, v6, [Ljava/lang/Object;

    iget v1, p0, Lflipboard/gui/toc/CoverPage;->l:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    iget v1, p0, Lflipboard/gui/toc/CoverPage;->r:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    iget-object v1, p0, Lflipboard/gui/toc/CoverPage;->p:Lflipboard/objs/FeedItem;

    aput-object v1, v0, v5

    .line 276
    new-instance v0, Lflipboard/gui/toc/CoverPage$CoverImageView;

    invoke-virtual {p0}, Lflipboard/gui/toc/CoverPage;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lflipboard/gui/toc/CoverPage$CoverImageView;-><init>(Lflipboard/gui/toc/CoverPage;Landroid/content/Context;)V

    .line 277
    invoke-virtual {v0, v2}, Lflipboard/gui/toc/CoverPage$CoverImageView;->setRecycle(Z)V

    .line 278
    invoke-static {v0, v3}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 279
    iget-object v1, p0, Lflipboard/gui/toc/CoverPage;->p:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/CoverPage$CoverImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 280
    iget-object v1, p0, Lflipboard/gui/toc/CoverPage;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 283
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->p:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->I()Ljava/lang/String;

    move-result-object v0

    .line 284
    if-eqz v0, :cond_3

    .line 285
    iget-object v1, p0, Lflipboard/gui/toc/CoverPage;->f:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 290
    :goto_2
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 293
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->p:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 294
    iget-object v1, v0, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v1, :cond_2

    .line 295
    iget-object v1, p0, Lflipboard/gui/toc/CoverPage;->g:Lflipboard/gui/FLImageView;

    iget-object v2, v0, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v2}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 297
    :cond_2
    iget-object v1, p0, Lflipboard/gui/toc/CoverPage;->h:Lflipboard/gui/FLTextIntf;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    invoke-interface {v1, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 298
    iget-object v1, p0, Lflipboard/gui/toc/CoverPage;->i:Lflipboard/gui/FLTextIntf;

    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->p:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bf:Lflipboard/objs/FeedItem$Note;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->p:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bf:Lflipboard/objs/FeedItem$Note;

    iget-object v0, v0, Lflipboard/objs/FeedItem$Note;->a:Ljava/lang/String;

    :goto_3
    invoke-interface {v1, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 300
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->p:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    .line 301
    if-nez v0, :cond_5

    const/4 v0, 0x0

    .line 302
    :goto_4
    iget-object v1, p0, Lflipboard/gui/toc/CoverPage;->j:Lflipboard/gui/FLTextIntf;

    if-nez v0, :cond_6

    const-string v0, ""

    :goto_5
    invoke-interface {v1, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 303
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->d:Landroid/view/View;

    invoke-static {v0, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_1

    .line 287
    :cond_3
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->f:Lflipboard/gui/FLStaticTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 298
    :cond_4
    const-string v0, ""

    goto :goto_3

    .line 301
    :cond_5
    invoke-static {v0}, Lflipboard/util/HttpUtil;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 302
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "/ "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method

.method public getCurrent()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->p:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->w:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method public getSection()Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->c:Lflipboard/service/Section;

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->c:Lflipboard/service/Section;

    invoke-virtual {v0, p0}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    .line 128
    invoke-direct {p0}, Lflipboard/gui/toc/CoverPage;->c()V

    .line 129
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 157
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onDetachedFromWindow()V

    .line 158
    iget-object v0, p0, Lflipboard/gui/toc/CoverPage;->c:Lflipboard/service/Section;

    invoke-virtual {v0, p0}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    .line 159
    return-void
.end method

.method public setNeverShare(Z)V
    .locals 0

    .prologue
    .line 110
    iput-boolean p1, p0, Lflipboard/gui/toc/CoverPage;->u:Z

    .line 111
    return-void
.end method
