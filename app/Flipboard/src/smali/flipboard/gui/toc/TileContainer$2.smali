.class Lflipboard/gui/toc/TileContainer$2;
.super Ljava/lang/Object;
.source "TileContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/toc/TileContainer;


# direct methods
.method constructor <init>(Lflipboard/gui/toc/TileContainer;)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Lflipboard/gui/toc/TileContainer$2;->a:Lflipboard/gui/toc/TileContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 317
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 354
    :cond_0
    :goto_0
    return-void

    .line 320
    :cond_1
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$2;->a:Lflipboard/gui/toc/TileContainer;

    invoke-virtual {v0}, Lflipboard/gui/toc/TileContainer;->d()Lflipboard/gui/toc/TOCView;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->v()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 321
    invoke-static {}, Lflipboard/gui/toc/TileContainer;->f()Lflipboard/service/User;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/toc/TileContainer$2;->a:Lflipboard/gui/toc/TileContainer;

    iget-object v1, v1, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->d(Lflipboard/service/Section;)Z

    .line 324
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/gui/toc/TileContainer$2;->a:Lflipboard/gui/toc/TileContainer;

    iget-object v1, v1, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 325
    if-eqz v0, :cond_0

    .line 327
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialog$Builder;

    iget-object v2, p0, Lflipboard/gui/toc/TileContainer$2;->a:Lflipboard/gui/toc/TileContainer;

    invoke-virtual {v2}, Lflipboard/gui/toc/TileContainer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lflipboard/gui/dialog/FLAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lflipboard/gui/toc/TileContainer$2;->a:Lflipboard/gui/toc/TileContainer;

    .line 328
    invoke-virtual {v2}, Lflipboard/gui/toc/TileContainer;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0322

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/toc/TileContainer$2;->a:Lflipboard/gui/toc/TileContainer;

    .line 329
    invoke-virtual {v2}, Lflipboard/gui/toc/TileContainer;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0321

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 330
    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    .line 329
    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 332
    const v0, 0x7f0d031f

    new-instance v2, Lflipboard/gui/toc/TileContainer$2$1;

    invoke-direct {v2, p0}, Lflipboard/gui/toc/TileContainer$2$1;-><init>(Lflipboard/gui/toc/TileContainer$2;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 340
    const v0, 0x7f0d0320

    new-instance v2, Lflipboard/gui/toc/TileContainer$2$2;

    invoke-direct {v2, p0}, Lflipboard/gui/toc/TileContainer$2$2;-><init>(Lflipboard/gui/toc/TileContainer$2;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 348
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$2;->a:Lflipboard/gui/toc/TileContainer;

    invoke-virtual {v0}, Lflipboard/gui/toc/TileContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->a(Landroid/app/AlertDialog$Builder;)Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 352
    :cond_2
    iget-object v0, p0, Lflipboard/gui/toc/TileContainer$2;->a:Lflipboard/gui/toc/TileContainer;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    invoke-static {v0}, Lflipboard/gui/toc/TileContainer;->a(Lflipboard/gui/toc/TileContainer;)V

    goto/16 :goto_0
.end method
