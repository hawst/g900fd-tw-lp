.class public Lflipboard/gui/toc/CoverStoryTileView;
.super Lflipboard/gui/toc/TileView;
.source "CoverStoryTileView.java"


# instance fields
.field protected a:Lflipboard/gui/FLStaticTextView;

.field private b:Landroid/view/ViewGroup;

.field private q:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lflipboard/gui/toc/TileContainer;Z)V
    .locals 6

    .prologue
    const/4 v2, -0x1

    .line 33
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lflipboard/gui/toc/TileView;-><init>(Lflipboard/gui/toc/TileContainer;IIZZ)V

    .line 34
    return-void
.end method

.method static synthetic a(Lflipboard/gui/toc/CoverStoryTileView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileView;->q:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public final a(Lflipboard/io/BitmapManager$Handle;)V
    .locals 6

    .prologue
    .line 104
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileView;->q:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 105
    invoke-virtual {p1}, Lflipboard/io/BitmapManager$Handle;->m()F

    move-result v0

    .line 106
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/toc/CoverStoryTileView$1;

    invoke-direct {v2, p0, v0}, Lflipboard/gui/toc/CoverStoryTileView$1;-><init>(Lflipboard/gui/toc/CoverStoryTileView;F)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 120
    :cond_0
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->m:Z

    if-nez v0, :cond_1

    .line 121
    const-string v0, "toc_load_since_app_launch"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-wide v4, v1, Lflipboard/app/FlipboardApplication;->l:J

    sub-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;J)V

    .line 122
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/app/FlipboardApplication;->m:Z

    .line 124
    :cond_1
    invoke-super {p0, p1}, Lflipboard/gui/toc/TileView;->a(Lflipboard/io/BitmapManager$Handle;)V

    .line 125
    return-void
.end method

.method protected final a(Z)V
    .locals 6

    .prologue
    const/high16 v5, 0x41a00000    # 20.0f

    .line 38
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "CoverStoryTileView:initView"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 39
    invoke-super {p0, p1}, Lflipboard/gui/toc/TileView;->a(Z)V

    .line 41
    invoke-virtual {p0}, Lflipboard/gui/toc/CoverStoryTileView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 44
    const v0, 0x7f03013c

    const/4 v1, 0x0

    invoke-static {v2, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lflipboard/gui/toc/CoverStoryTileView;->b:Landroid/view/ViewGroup;

    .line 45
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileView;->b:Landroid/view/ViewGroup;

    const v1, 0x7f0a035d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/toc/CoverStoryTileView;->a:Lflipboard/gui/FLStaticTextView;

    .line 46
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileView;->a:Lflipboard/gui/FLStaticTextView;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->w:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 47
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileView;->b:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/CoverStoryTileView;->addView(Landroid/view/View;)V

    .line 49
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileView;->b:Landroid/view/ViewGroup;

    const v1, 0x7f0a035c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    .line 51
    iget-object v1, p0, Lflipboard/gui/toc/CoverStoryTileView;->b:Landroid/view/ViewGroup;

    const v3, 0x7f0a035b

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lflipboard/gui/toc/CoverStoryTileView;->q:Landroid/widget/ImageView;

    .line 53
    iget-object v1, p0, Lflipboard/gui/toc/CoverStoryTileView;->e:Lflipboard/gui/FLStaticTextView;

    if-eqz v1, :cond_0

    .line 54
    iget-object v1, p0, Lflipboard/gui/toc/CoverStoryTileView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v3, p0, Lflipboard/gui/toc/CoverStoryTileView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 55
    iget-object v1, p0, Lflipboard/gui/toc/CoverStoryTileView;->b:Landroid/view/ViewGroup;

    iget-object v3, p0, Lflipboard/gui/toc/CoverStoryTileView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 57
    iget-object v1, p0, Lflipboard/gui/toc/CoverStoryTileView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 58
    const/4 v3, 0x3

    iget-object v4, p0, Lflipboard/gui/toc/CoverStoryTileView;->q:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getId()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 59
    const/4 v3, 0x2

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getId()I

    move-result v0

    invoke-virtual {v1, v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 60
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 62
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileView;->e:Lflipboard/gui/FLStaticTextView;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lflipboard/gui/toc/CoverStoryTileView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090136

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lflipboard/gui/FLStaticTextView;->a(II)V

    .line 65
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileView;->e:Lflipboard/gui/FLStaticTextView;

    const/high16 v1, 0x41400000    # 12.0f

    .line 66
    invoke-static {v1, v2}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v1

    .line 67
    invoke-static {v5, v2}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v3

    .line 68
    invoke-static {v5, v2}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    .line 69
    invoke-static {v5, v2}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v2

    .line 65
    invoke-virtual {v0, v1, v3, v4, v2}, Lflipboard/gui/FLStaticTextView;->setPadding(IIII)V

    .line 74
    :cond_0
    return-void
.end method

.method protected final b()V
    .locals 3

    .prologue
    .line 78
    invoke-super {p0}, Lflipboard/gui/toc/TileView;->b()V

    .line 80
    new-instance v0, Lflipboard/gui/SocialFormatter;

    invoke-virtual {p0}, Lflipboard/gui/toc/CoverStoryTileView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lflipboard/gui/SocialFormatter;-><init>(Landroid/content/Context;)V

    .line 81
    iget-object v1, p0, Lflipboard/gui/toc/CoverStoryTileView;->a:Lflipboard/gui/FLStaticTextView;

    iget-object v2, p0, Lflipboard/gui/toc/CoverStoryTileView;->p:Lflipboard/gui/toc/TileContainer;

    iget-object v2, v2, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-virtual {v0, v2}, Lflipboard/gui/SocialFormatter;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    return-void
.end method

.method public getCoverStoryProvenanceView()Lflipboard/gui/FLStaticTextView;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileView;->a:Lflipboard/gui/FLStaticTextView;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 86
    invoke-super/range {p0 .. p5}, Lflipboard/gui/toc/TileView;->onLayout(ZIIII)V

    .line 87
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileView;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, p2, p3, p4, p5}, Landroid/view/ViewGroup;->layout(IIII)V

    .line 88
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    .prologue
    .line 92
    invoke-super {p0, p1, p2}, Lflipboard/gui/toc/TileView;->onMeasure(II)V

    .line 93
    iget-object v0, p0, Lflipboard/gui/toc/CoverStoryTileView;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->measure(II)V

    .line 94
    return-void
.end method
