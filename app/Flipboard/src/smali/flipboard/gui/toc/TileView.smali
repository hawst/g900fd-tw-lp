.class public Lflipboard/gui/toc/TileView;
.super Landroid/view/ViewGroup;
.source "TileView.java"

# interfaces
.implements Lflipboard/gui/FLImageView$Listener;
.implements Lflipboard/gui/FLViewIntf;


# static fields
.field private static b:I

.field public static final o:[I

.field private static final q:[I


# instance fields
.field private a:Z

.field protected c:Lflipboard/gui/FLStaticTextView;

.field protected d:Lflipboard/gui/FLStaticTextView;

.field protected e:Lflipboard/gui/FLStaticTextView;

.field protected f:Lflipboard/gui/FLImageView;

.field protected g:Landroid/view/View;

.field protected h:Landroid/view/View;

.field protected i:I

.field protected j:I

.field protected k:I

.field protected l:Z

.field m:Lflipboard/objs/FeedItem;

.field public n:Z

.field p:Lflipboard/gui/toc/TileContainer;

.field private r:I

.field private s:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lflipboard/gui/toc/TileView;->o:[I

    .line 52
    const/4 v0, 0x0

    sput v0, Lflipboard/gui/toc/TileView;->b:I

    .line 65
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lflipboard/gui/toc/TileView;->q:[I

    return-void

    .line 45
    :array_0
    .array-data 4
        0x7f020221
        0x7f020222
        0x7f020223
        0x7f020224
        0x7f020225
    .end array-data

    .line 65
    :array_1
    .array-data 4
        0x7f08002f
        0x7f080030
        0x7f080031
    .end array-data
.end method

.method constructor <init>(Lflipboard/gui/toc/TileContainer;IIZ)V
    .locals 6

    .prologue
    .line 80
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lflipboard/gui/toc/TileView;-><init>(Lflipboard/gui/toc/TileContainer;IIZZ)V

    .line 81
    return-void
.end method

.method constructor <init>(Lflipboard/gui/toc/TileContainer;IIZZ)V
    .locals 2

    .prologue
    .line 84
    invoke-virtual {p1}, Lflipboard/gui/toc/TileContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/toc/TileView;->a:Z

    .line 85
    iput p2, p0, Lflipboard/gui/toc/TileView;->i:I

    .line 86
    if-gez p3, :cond_0

    invoke-static {}, Lflipboard/gui/toc/TileView;->getRandomGray()I

    move-result p3

    :cond_0
    iput p3, p0, Lflipboard/gui/toc/TileView;->j:I

    .line 87
    iput-boolean p4, p0, Lflipboard/gui/toc/TileView;->l:Z

    .line 88
    iput-object p1, p0, Lflipboard/gui/toc/TileView;->p:Lflipboard/gui/toc/TileContainer;

    .line 89
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileView;->setWillNotDraw(Z)V

    .line 90
    invoke-virtual {p0, p5}, Lflipboard/gui/toc/TileView;->a(Z)V

    .line 93
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    if-eqz v0, :cond_1

    .line 94
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p1, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->b()V

    .line 100
    return-void
.end method

.method private static b(ZI)Z
    .locals 1

    .prologue
    .line 513
    if-eqz p0, :cond_0

    const/4 v0, -0x2

    if-lt p1, v0, :cond_0

    const/4 v0, 0x2

    if-le p1, v0, :cond_1

    :cond_0
    if-nez p1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getNextGradientIndex()I
    .locals 3

    .prologue
    .line 56
    sget v0, Lflipboard/gui/toc/TileView;->b:I

    .line 57
    sget v1, Lflipboard/gui/toc/TileView;->b:I

    add-int/lit8 v1, v1, 0x1

    .line 58
    sput v1, Lflipboard/gui/toc/TileView;->b:I

    sget-object v2, Lflipboard/gui/toc/TileView;->o:[I

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 59
    const/4 v1, 0x0

    sput v1, Lflipboard/gui/toc/TileView;->b:I

    .line 62
    :cond_0
    return v0
.end method

.method private static getRandomGray()I
    .locals 6

    .prologue
    .line 72
    sget-object v0, Lflipboard/gui/toc/TileView;->q:[I

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    sget-object v1, Lflipboard/gui/toc/TileView;->q:[I

    array-length v1, v1

    int-to-double v4, v1

    mul-double/2addr v2, v4

    double-to-int v1, v2

    aget v0, v0, v1

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 456
    return-void
.end method

.method public a(Lflipboard/io/BitmapManager$Handle;)V
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->p:Lflipboard/gui/toc/TileContainer;

    invoke-virtual {v0, p0}, Lflipboard/gui/toc/TileContainer;->a(Lflipboard/gui/toc/TileView;)V

    .line 452
    return-void
.end method

.method public final a(Lflipboard/io/Download$Status;)V
    .locals 2

    .prologue
    .line 460
    sget-object v0, Lflipboard/gui/toc/TileView$3;->a:[I

    invoke-virtual {p1}, Lflipboard/io/Download$Status;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 472
    :goto_0
    return-void

    .line 464
    :pswitch_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/toc/TileView$2;

    invoke-direct {v1, p0}, Lflipboard/gui/toc/TileView$2;-><init>(Lflipboard/gui/toc/TileView;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 460
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Z)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v4, 0x14

    const/16 v3, 0xc

    const/4 v10, -0x1

    .line 105
    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 107
    new-instance v0, Lflipboard/gui/FLImageView;

    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v0, v5}, Lflipboard/gui/FLImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    .line 108
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setBusyBottomRight(Z)V

    .line 109
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLImageView;->setAllowPreloadOnUIThread(Z)V

    .line 110
    iget-object v5, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    if-nez p1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v5, v0}, Lflipboard/gui/FLImageView;->setFade(Z)V

    .line 111
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    iget v5, p0, Lflipboard/gui/toc/TileView;->j:I

    invoke-virtual {v0, v5}, Lflipboard/gui/FLImageView;->setBackgroundResource(I)V

    .line 112
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileView;->addView(Landroid/view/View;)V

    .line 114
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, p0}, Lflipboard/gui/FLImageView;->setListener(Lflipboard/gui/FLImageView$Listener;)V

    .line 116
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, v6}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    .line 117
    iget-object v5, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    iget-boolean v0, p0, Lflipboard/gui/toc/TileView;->l:Z

    if-eqz v0, :cond_3

    const v0, 0x7f02022d

    :goto_1
    invoke-virtual {v5, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 120
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    iget-object v5, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLImageView;->addView(Landroid/view/View;)V

    .line 127
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->p:Lflipboard/gui/toc/TileContainer;

    invoke-virtual {v0}, Lflipboard/gui/toc/TileContainer;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    new-instance v0, Lflipboard/gui/FLStaticTextView;

    const/4 v5, 0x0

    invoke-direct {v0, v6, v5}, Lflipboard/gui/FLStaticTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    .line 129
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v5, v5, Lflipboard/service/FlipboardManager;->w:Landroid/graphics/Typeface;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLStaticTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 130
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    const/4 v7, -0x2

    invoke-direct {v5, v10, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v5}, Lflipboard/gui/FLStaticTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 131
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f090138

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v0, v2, v5}, Lflipboard/gui/FLStaticTextView;->a(II)V

    .line 132
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v10}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    .line 133
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Lflipboard/gui/FLStaticTextView;->setMaxLines(I)V

    .line 135
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 138
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setRtlAlignment(Z)V

    move v0, v3

    move v5, v4

    .line 144
    :goto_2
    iget-object v7, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    int-to-float v5, v5

    .line 145
    invoke-static {v5, v6}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v5

    const/high16 v8, 0x41000000    # 8.0f

    .line 146
    invoke-static {v8, v6}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v8

    int-to-float v0, v0

    .line 147
    invoke-static {v0, v6}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v0

    const/4 v9, 0x0

    .line 148
    invoke-static {v9, v6}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v9

    .line 144
    invoke-virtual {v7, v5, v8, v0, v9}, Lflipboard/gui/FLStaticTextView;->setPadding(IIII)V

    .line 151
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileView;->addView(Landroid/view/View;)V

    .line 154
    :cond_0
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->p:Lflipboard/gui/toc/TileContainer;

    iget-object v0, v0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->M:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 155
    new-instance v0, Lflipboard/gui/FLStaticTextView;

    const/4 v5, 0x0

    invoke-direct {v0, v6, v5}, Lflipboard/gui/FLStaticTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    .line 156
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v5, v5, Lflipboard/service/FlipboardManager;->w:Landroid/graphics/Typeface;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLStaticTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 157
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    const/4 v7, -0x2

    invoke-direct {v5, v10, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v5}, Lflipboard/gui/FLStaticTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 158
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f090136

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v0, v2, v5}, Lflipboard/gui/FLStaticTextView;->a(II)V

    .line 159
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0b0005

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    invoke-virtual {v0, v5}, Lflipboard/gui/FLStaticTextView;->setMaxLines(I)V

    .line 160
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v10}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    .line 163
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 166
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setRtlAlignment(Z)V

    move v0, v3

    move v5, v4

    .line 171
    :goto_3
    iget-object v7, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    int-to-float v5, v5

    .line 172
    invoke-static {v5, v6}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v5

    const/4 v8, 0x0

    .line 173
    invoke-static {v8, v6}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v8

    int-to-float v0, v0

    .line 174
    invoke-static {v0, v6}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v0

    const/4 v9, 0x0

    .line 175
    invoke-static {v9, v6}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v9

    .line 171
    invoke-virtual {v7, v5, v8, v0, v9}, Lflipboard/gui/FLStaticTextView;->setPadding(IIII)V

    .line 178
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileView;->addView(Landroid/view/View;)V

    .line 181
    :cond_1
    new-instance v0, Lflipboard/gui/FLStaticTextView;

    const/4 v5, 0x0

    invoke-direct {v0, v6, v5}, Lflipboard/gui/FLStaticTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    .line 182
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    const/4 v7, -0x2

    invoke-direct {v5, v10, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v5}, Lflipboard/gui/FLStaticTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 183
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f090136

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v0, v2, v5}, Lflipboard/gui/FLStaticTextView;->a(II)V

    .line 184
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b0005

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLStaticTextView;->setMaxLines(I)V

    .line 185
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v10}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    .line 188
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 191
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setRtlAlignment(Z)V

    .line 196
    :goto_4
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    int-to-float v1, v4

    .line 197
    invoke-static {v1, v6}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v1

    const/high16 v2, 0x41000000    # 8.0f

    .line 198
    invoke-static {v2, v6}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v2

    int-to-float v3, v3

    .line 199
    invoke-static {v3, v6}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v3

    const/high16 v4, 0x41000000    # 8.0f

    .line 200
    invoke-static {v4, v6}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v4

    .line 196
    invoke-virtual {v0, v1, v2, v3, v4}, Lflipboard/gui/FLStaticTextView;->setPadding(IIII)V

    .line 203
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileView;->addView(Landroid/view/View;)V

    .line 205
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, v6}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/toc/TileView;->g:Landroid/view/View;

    .line 206
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->g:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v10, v10}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 207
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->g:Landroid/view/View;

    const v1, 0x7f02021f

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 208
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->g:Landroid/view/View;

    invoke-virtual {p0, v0}, Lflipboard/gui/toc/TileView;->addView(Landroid/view/View;)V

    .line 209
    return-void

    :cond_2
    move v0, v2

    .line 110
    goto/16 :goto_0

    .line 117
    :cond_3
    const v0, 0x7f02005f

    goto/16 :goto_1

    :cond_4
    move v0, v4

    move v5, v3

    .line 141
    goto/16 :goto_2

    :cond_5
    move v0, v4

    move v5, v3

    .line 169
    goto/16 :goto_3

    :cond_6
    move v11, v4

    move v4, v3

    move v3, v11

    .line 194
    goto :goto_4
.end method

.method public final a(ZI)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 475
    iput p2, p0, Lflipboard/gui/toc/TileView;->r:I

    .line 476
    iput-boolean p1, p0, Lflipboard/gui/toc/TileView;->s:Z

    .line 477
    invoke-static {p1, p2}, Lflipboard/gui/toc/TileView;->b(ZI)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 478
    iget-boolean v0, p0, Lflipboard/gui/toc/TileView;->a:Z

    if-eqz v0, :cond_0

    .line 479
    iput-boolean v4, p0, Lflipboard/gui/toc/TileView;->a:Z

    .line 480
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->m:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/toc/TileView;->m:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/toc/TileView;->m:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 490
    :cond_0
    :goto_0
    return-void

    .line 485
    :cond_1
    iget-boolean v0, p0, Lflipboard/gui/toc/TileView;->a:Z

    if-nez v0, :cond_0

    .line 486
    iput-boolean v5, p0, Lflipboard/gui/toc/TileView;->a:Z

    .line 487
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "TileView:doClear"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    iget-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setListener(Lflipboard/gui/FLImageView$Listener;)V

    iget-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {p0, v1}, Lflipboard/gui/toc/TileView;->removeView(Landroid/view/View;)V

    new-instance v1, Lflipboard/gui/FLImageView;

    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    invoke-direct {v1, v2, v3}, Lflipboard/gui/FLImageView;-><init>(Landroid/content/Context;Lflipboard/gui/FLImageView;)V

    iput-object v1, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v4}, Lflipboard/gui/FLImageView;->setBusyBottomRight(Z)V

    iget-object v1, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v5}, Lflipboard/gui/FLImageView;->setFade(Z)V

    iget-object v1, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, p0}, Lflipboard/gui/FLImageView;->setListener(Lflipboard/gui/FLImageView$Listener;)V

    iget-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {p0, v0, v4}, Lflipboard/gui/toc/TileView;->addView(Landroid/view/View;I)V

    goto :goto_0
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->p:Lflipboard/gui/toc/TileContainer;

    iget-object v0, v0, Lflipboard/gui/toc/TileContainer;->f:Lflipboard/objs/FeedItem;

    iput-object v0, p0, Lflipboard/gui/toc/TileView;->m:Lflipboard/objs/FeedItem;

    .line 217
    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->c()V

    .line 218
    return-void
.end method

.method protected final c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 235
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->m:Lflipboard/objs/FeedItem;

    if-nez v0, :cond_4

    const/4 v0, 0x0

    .line 236
    :goto_0
    iget-boolean v1, p0, Lflipboard/gui/toc/TileView;->s:Z

    iget v2, p0, Lflipboard/gui/toc/TileView;->r:I

    invoke-static {v1, v2}, Lflipboard/gui/toc/TileView;->b(ZI)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 237
    iget-object v1, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    iget-object v2, p0, Lflipboard/gui/toc/TileView;->m:Lflipboard/objs/FeedItem;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 239
    :cond_0
    if-eqz v0, :cond_5

    .line 240
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p0, Lflipboard/gui/toc/TileView;->p:Lflipboard/gui/toc/TileContainer;

    iget-object v1, v1, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0324

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lflipboard/gui/toc/TileView;->p:Lflipboard/gui/toc/TileContainer;

    iget-object v3, v3, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    iget-object v3, v3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->M:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    :cond_3
    :goto_1
    return-void

    .line 235
    :cond_4
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->m:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 242
    :cond_5
    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->d()V

    goto :goto_1
.end method

.method protected final d()V
    .locals 4

    .prologue
    .line 273
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 276
    :cond_0
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->p:Lflipboard/gui/toc/TileContainer;

    iget-object v0, v0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-boolean v0, v0, Lflipboard/service/Section$Meta;->b:Z

    if-eqz v0, :cond_4

    .line 277
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/toc/TileView;->p:Lflipboard/gui/toc/TileContainer;

    iget-object v1, v1, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    .line 278
    iget-object v0, v1, Lflipboard/objs/ConfigService;->I:Ljava/lang/String;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "#"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v1, Lflipboard/objs/ConfigService;->I:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    :goto_0
    iput v0, p0, Lflipboard/gui/toc/TileView;->k:I

    .line 279
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    iget v2, p0, Lflipboard/gui/toc/TileView;->k:I

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setBackgroundColor(I)V

    .line 280
    invoke-virtual {v1}, Lflipboard/objs/ConfigService;->h()Ljava/lang/String;

    move-result-object v0

    .line 281
    if-eqz v0, :cond_3

    .line 282
    iget-object v1, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 288
    :goto_1
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    if-eqz v0, :cond_1

    .line 289
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p0, Lflipboard/gui/toc/TileView;->p:Lflipboard/gui/toc/TileContainer;

    iget-object v1, v1, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 347
    :cond_1
    :goto_2
    return-void

    .line 278
    :cond_2
    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08005b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0

    .line 284
    :cond_3
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d032b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 291
    :cond_4
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->m:Lflipboard/objs/FeedItem;

    if-nez v0, :cond_9

    .line 293
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    iget v1, p0, Lflipboard/gui/toc/TileView;->j:I

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBackgroundResource(I)V

    .line 294
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    const v1, 0x7f08005c

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setTextColorResource(I)V

    .line 296
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    if-eqz v0, :cond_5

    .line 297
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p0, Lflipboard/gui/toc/TileView;->p:Lflipboard/gui/toc/TileContainer;

    iget-object v1, v1, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 301
    :cond_5
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->p:Lflipboard/gui/toc/TileContainer;

    iget-object v0, v0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lflipboard/gui/toc/TileView;->p:Lflipboard/gui/toc/TileContainer;

    iget-object v0, v0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->h()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 302
    :cond_6
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 304
    :cond_7
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->p:Lflipboard/gui/toc/TileContainer;

    iget-object v0, v0, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->d:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 305
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p0, Lflipboard/gui/toc/TileView;->p:Lflipboard/gui/toc/TileContainer;

    iget-object v1, v1, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v1, v1, Lflipboard/service/Section$Meta;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 307
    :cond_8
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    const v1, 0x7f0d0327

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(I)V

    goto :goto_2

    .line 316
    :cond_9
    iget v0, p0, Lflipboard/gui/toc/TileView;->i:I

    if-gez v0, :cond_a

    .line 317
    invoke-static {}, Lflipboard/gui/toc/TileView;->getNextGradientIndex()I

    move-result v0

    iput v0, p0, Lflipboard/gui/toc/TileView;->i:I

    .line 319
    :cond_a
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    sget-object v1, Lflipboard/gui/toc/TileView;->o:[I

    iget v2, p0, Lflipboard/gui/toc/TileView;->i:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBackgroundResource(I)V

    .line 321
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    if-eqz v0, :cond_b

    .line 322
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p0, Lflipboard/gui/toc/TileView;->p:Lflipboard/gui/toc/TileContainer;

    iget-object v1, v1, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 326
    :cond_b
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->m:Lflipboard/objs/FeedItem;

    invoke-static {v0}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v0

    .line 327
    if-eqz v0, :cond_d

    .line 328
    invoke-static {v0}, Lflipboard/util/JavaUtil;->e(Ljava/lang/String;)Ljava/lang/String;

    .line 329
    iget-object v1, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 330
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    .line 335
    :goto_3
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    if-eqz v0, :cond_c

    .line 336
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p0, Lflipboard/gui/toc/TileView;->p:Lflipboard/gui/toc/TileContainer;

    iget-object v1, v1, Lflipboard/gui/toc/TileContainer;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 340
    :cond_c
    new-instance v0, Lflipboard/gui/toc/TileView$1;

    invoke-direct {v0, p0}, Lflipboard/gui/toc/TileView$1;-><init>(Lflipboard/gui/toc/TileView;)V

    const-wide/16 v2, 0x64

    invoke-virtual {p0, v0, v2, v3}, Lflipboard/gui/toc/TileView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_2

    .line 332
    :cond_d
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    goto :goto_3
.end method

.method public getGradientIndex()I
    .locals 1

    .prologue
    .line 222
    iget v0, p0, Lflipboard/gui/toc/TileView;->i:I

    if-gez v0, :cond_0

    .line 223
    invoke-static {}, Lflipboard/gui/toc/TileView;->getNextGradientIndex()I

    move-result v0

    iput v0, p0, Lflipboard/gui/toc/TileView;->i:I

    .line 225
    :cond_0
    iget v0, p0, Lflipboard/gui/toc/TileView;->i:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 358
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 359
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/toc/TileView;->n:Z

    .line 360
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 397
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    sub-int v1, p4, p2

    sub-int v3, p5, p3

    invoke-virtual {v0, v2, v2, v1, v3}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 398
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 399
    iget-boolean v0, p0, Lflipboard/gui/toc/TileView;->l:Z

    if-eqz v0, :cond_4

    .line 400
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    iget-object v1, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v3, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v0, v2, v2, v1, v3}, Landroid/view/View;->layout(IIII)V

    .line 409
    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    if-eqz v0, :cond_8

    .line 410
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 411
    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getWidth()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v0, v1

    .line 412
    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getWidth()I

    move-result v0

    .line 417
    :goto_1
    iget-object v3, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v4, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v3, v1, v2, v0, v4}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 418
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 421
    :goto_2
    iget-object v1, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    if-eqz v1, :cond_1

    .line 422
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 423
    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getWidth()I

    move-result v1

    iget-object v3, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v3

    sub-int v3, v1, v3

    .line 424
    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getWidth()I

    move-result v1

    .line 429
    :goto_3
    iget-object v4, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    iget-object v5, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v4, v3, v0, v1, v5}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 433
    :cond_1
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    if-eqz v0, :cond_2

    instance-of v0, p0, Lflipboard/gui/toc/CoverStoryTileView;

    if-nez v0, :cond_2

    .line 434
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 435
    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getWidth()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v0, v1

    .line 436
    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getWidth()I

    move-result v0

    .line 441
    :goto_4
    iget-object v3, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    iget-object v4, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v4

    sub-int v4, p5, v4

    invoke-virtual {v3, v1, v4, v0, p5}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 444
    :cond_2
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->g:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 445
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->g:Landroid/view/View;

    sub-int v1, p4, p2

    sub-int v3, p5, p3

    invoke-virtual {v0, v2, v2, v1, v3}, Landroid/view/View;->layout(IIII)V

    .line 447
    :cond_3
    return-void

    .line 402
    :cond_4
    sub-int v0, p5, p3

    div-int/lit8 v0, v0, 0x2

    .line 403
    iget-object v1, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    iget-object v3, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_0

    .line 414
    :cond_5
    invoke-virtual {p0}, Lflipboard/gui/toc/TileView;->getPaddingLeft()I

    move-result v1

    .line 415
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v1

    goto/16 :goto_1

    .line 427
    :cond_6
    iget-object v1, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v1

    move v3, v2

    goto :goto_3

    .line 439
    :cond_7
    iget-object v0, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v0

    move v1, v2

    goto :goto_4

    :cond_8
    move v0, v2

    goto/16 :goto_2
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    .line 371
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 372
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 374
    iget-object v2, p0, Lflipboard/gui/toc/TileView;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v2, p1, p2}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 376
    iget-object v2, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 377
    iget-object v2, p0, Lflipboard/gui/toc/TileView;->h:Landroid/view/View;

    div-int/lit8 v3, v1, 0x2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, p1, v3}, Landroid/view/View;->measure(II)V

    .line 380
    :cond_0
    iget-object v2, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    if-eqz v2, :cond_1

    .line 381
    iget-object v2, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 384
    :cond_1
    iget-object v2, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    if-eqz v2, :cond_2

    .line 385
    iget-object v2, p0, Lflipboard/gui/toc/TileView;->d:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 389
    :cond_2
    iget-object v2, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    if-eqz v2, :cond_3

    .line 390
    iget-object v2, p0, Lflipboard/gui/toc/TileView;->e:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/toc/TileView;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v1, v4

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 392
    :cond_3
    invoke-virtual {p0, v0, v1}, Lflipboard/gui/toc/TileView;->setMeasuredDimension(II)V

    .line 393
    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    .prologue
    .line 365
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 366
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/toc/TileView;->n:Z

    .line 367
    return-void
.end method
