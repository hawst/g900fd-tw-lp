.class public Lflipboard/gui/PagerIndicatorStrip;
.super Landroid/view/View;
.source "PagerIndicatorStrip.java"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:Landroid/graphics/Paint;

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 22
    iput v0, p0, Lflipboard/gui/PagerIndicatorStrip;->e:I

    .line 23
    iput v0, p0, Lflipboard/gui/PagerIndicatorStrip;->f:I

    .line 27
    const/high16 v0, 0x41600000    # 14.0f

    invoke-static {v0, p1}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v0

    iput v0, p0, Lflipboard/gui/PagerIndicatorStrip;->a:I

    .line 28
    const/high16 v0, 0x41000000    # 8.0f

    invoke-static {v0, p1}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v0

    iput v0, p0, Lflipboard/gui/PagerIndicatorStrip;->b:I

    .line 29
    const/high16 v0, 0x40400000    # 3.0f

    invoke-static {v0, p1}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v0

    iput v0, p0, Lflipboard/gui/PagerIndicatorStrip;->c:I

    .line 30
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/PagerIndicatorStrip;->d:Landroid/graphics/Paint;

    .line 31
    iget-object v0, p0, Lflipboard/gui/PagerIndicatorStrip;->d:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 32
    iget-object v0, p0, Lflipboard/gui/PagerIndicatorStrip;->d:Landroid/graphics/Paint;

    const v1, -0x7f000001

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 33
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 65
    iget v0, p0, Lflipboard/gui/PagerIndicatorStrip;->a:I

    iget v1, p0, Lflipboard/gui/PagerIndicatorStrip;->c:I

    add-int v3, v0, v1

    .line 66
    iget v1, p0, Lflipboard/gui/PagerIndicatorStrip;->c:I

    .line 67
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lflipboard/gui/PagerIndicatorStrip;->e:I

    if-ge v0, v2, :cond_1

    .line 68
    iget-object v4, p0, Lflipboard/gui/PagerIndicatorStrip;->d:Landroid/graphics/Paint;

    iget v2, p0, Lflipboard/gui/PagerIndicatorStrip;->f:I

    if-ne v2, v0, :cond_0

    const/4 v2, -0x1

    :goto_1
    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 69
    int-to-float v2, v1

    int-to-float v4, v3

    iget v5, p0, Lflipboard/gui/PagerIndicatorStrip;->c:I

    int-to-float v5, v5

    iget-object v6, p0, Lflipboard/gui/PagerIndicatorStrip;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4, v5, v6}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 70
    iget v2, p0, Lflipboard/gui/PagerIndicatorStrip;->c:I

    iget v4, p0, Lflipboard/gui/PagerIndicatorStrip;->b:I

    add-int/2addr v2, v4

    iget v4, p0, Lflipboard/gui/PagerIndicatorStrip;->c:I

    add-int/2addr v2, v4

    add-int/2addr v1, v2

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 68
    :cond_0
    const v2, -0x7f000001

    goto :goto_1

    .line 72
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 52
    iget v0, p0, Lflipboard/gui/PagerIndicatorStrip;->e:I

    if-nez v0, :cond_0

    .line 53
    invoke-virtual {p0, v1, v1}, Lflipboard/gui/PagerIndicatorStrip;->setMeasuredDimension(II)V

    .line 61
    :goto_0
    return-void

    .line 55
    :cond_0
    iget v0, p0, Lflipboard/gui/PagerIndicatorStrip;->c:I

    mul-int/lit8 v0, v0, 0x2

    .line 56
    iget v1, p0, Lflipboard/gui/PagerIndicatorStrip;->e:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lflipboard/gui/PagerIndicatorStrip;->b:I

    mul-int/2addr v1, v2

    .line 57
    iget v2, p0, Lflipboard/gui/PagerIndicatorStrip;->e:I

    mul-int/2addr v2, v0

    add-int/2addr v1, v2

    .line 58
    iget v2, p0, Lflipboard/gui/PagerIndicatorStrip;->a:I

    add-int/2addr v0, v2

    iget v2, p0, Lflipboard/gui/PagerIndicatorStrip;->a:I

    add-int/2addr v0, v2

    .line 59
    invoke-virtual {p0, v1, v0}, Lflipboard/gui/PagerIndicatorStrip;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public setIndicatorCount(I)V
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lflipboard/gui/PagerIndicatorStrip;->e:I

    if-eq v0, p1, :cond_0

    .line 37
    iput p1, p0, Lflipboard/gui/PagerIndicatorStrip;->e:I

    .line 38
    invoke-virtual {p0}, Lflipboard/gui/PagerIndicatorStrip;->requestLayout()V

    .line 39
    invoke-virtual {p0}, Lflipboard/gui/PagerIndicatorStrip;->invalidate()V

    .line 41
    :cond_0
    return-void
.end method

.method public setSelectedIndex(I)V
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lflipboard/gui/PagerIndicatorStrip;->f:I

    if-eq v0, p1, :cond_0

    .line 45
    iput p1, p0, Lflipboard/gui/PagerIndicatorStrip;->f:I

    .line 46
    invoke-virtual {p0}, Lflipboard/gui/PagerIndicatorStrip;->invalidate()V

    .line 48
    :cond_0
    return-void
.end method
