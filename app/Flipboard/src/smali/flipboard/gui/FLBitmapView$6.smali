.class Lflipboard/gui/FLBitmapView$6;
.super Ljava/lang/Object;
.source "FLBitmapView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Landroid/graphics/Movie;

.field final synthetic c:Lflipboard/gui/FLBitmapView;


# direct methods
.method constructor <init>(Lflipboard/gui/FLBitmapView;ILandroid/graphics/Movie;)V
    .locals 0

    .prologue
    .line 753
    iput-object p1, p0, Lflipboard/gui/FLBitmapView$6;->c:Lflipboard/gui/FLBitmapView;

    iput p2, p0, Lflipboard/gui/FLBitmapView$6;->a:I

    iput-object p3, p0, Lflipboard/gui/FLBitmapView$6;->b:Landroid/graphics/Movie;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 757
    iget-object v0, p0, Lflipboard/gui/FLBitmapView$6;->c:Lflipboard/gui/FLBitmapView;

    invoke-static {v0}, Lflipboard/gui/FLBitmapView;->d(Lflipboard/gui/FLBitmapView;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    iget v1, p0, Lflipboard/gui/FLBitmapView$6;->a:I

    if-ne v0, v1, :cond_0

    .line 758
    iget-object v0, p0, Lflipboard/gui/FLBitmapView$6;->b:Landroid/graphics/Movie;

    invoke-virtual {v0}, Landroid/graphics/Movie;->duration()I

    move-result v0

    if-lez v0, :cond_1

    .line 759
    iget-object v0, p0, Lflipboard/gui/FLBitmapView$6;->c:Lflipboard/gui/FLBitmapView;

    iget-object v1, p0, Lflipboard/gui/FLBitmapView$6;->b:Landroid/graphics/Movie;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBitmapView;->setMovie(Landroid/graphics/Movie;)V

    .line 766
    :cond_0
    :goto_0
    return-void

    .line 760
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLBitmapView$6;->c:Lflipboard/gui/FLBitmapView;

    invoke-static {v0}, Lflipboard/gui/FLBitmapView;->e(Lflipboard/gui/FLBitmapView;)Lflipboard/io/Download;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 762
    iget-object v0, p0, Lflipboard/gui/FLBitmapView$6;->c:Lflipboard/gui/FLBitmapView;

    sget-object v1, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    iget-object v2, p0, Lflipboard/gui/FLBitmapView$6;->c:Lflipboard/gui/FLBitmapView;

    invoke-static {v2}, Lflipboard/gui/FLBitmapView;->e(Lflipboard/gui/FLBitmapView;)Lflipboard/io/Download;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lflipboard/io/BitmapManager;->a(Lflipboard/io/Download;Z)Lflipboard/io/BitmapManager$Handle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBitmapView;->setBitmap(Lflipboard/io/BitmapManager$Handle;)V

    .line 763
    iget-object v0, p0, Lflipboard/gui/FLBitmapView$6;->c:Lflipboard/gui/FLBitmapView;

    invoke-static {v0}, Lflipboard/gui/FLBitmapView;->f(Lflipboard/gui/FLBitmapView;)Lflipboard/io/Download;

    goto :goto_0
.end method
