.class public Lflipboard/gui/FLSearchBar;
.super Lflipboard/gui/FLEditText;
.source "FLSearchBar.java"


# static fields
.field public static final b:Lflipboard/util/Log;


# instance fields
.field private c:Lflipboard/service/FLSearchManager;

.field private d:Lflipboard/service/Flap$SearchObserver;

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/objs/SearchResultCategory;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lflipboard/gui/FLSearchBar$SearchResultObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-string v0, "flsearchview"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/FLSearchBar;->b:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lflipboard/gui/FLEditText;-><init>(Landroid/content/Context;)V

    .line 35
    invoke-direct {p0}, Lflipboard/gui/FLSearchBar;->b()V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method static synthetic a(Lflipboard/gui/FLSearchBar;)Lflipboard/gui/FLSearchBar$SearchResultObserver;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lflipboard/gui/FLSearchBar;->f:Lflipboard/gui/FLSearchBar$SearchResultObserver;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 47
    new-instance v0, Lflipboard/service/FLSearchManager;

    invoke-virtual {p0}, Lflipboard/gui/FLSearchBar;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lflipboard/service/FLSearchManager;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lflipboard/gui/FLSearchBar;->c:Lflipboard/service/FLSearchManager;

    .line 48
    new-instance v0, Lflipboard/gui/FLSearchBar$1;

    invoke-direct {v0, p0}, Lflipboard/gui/FLSearchBar$1;-><init>(Lflipboard/gui/FLSearchBar;)V

    iput-object v0, p0, Lflipboard/gui/FLSearchBar;->d:Lflipboard/service/Flap$SearchObserver;

    .line 76
    return-void
.end method


# virtual methods
.method final varargs a(Lflipboard/gui/FLSearchBar$Input;[Ljava/lang/Object;)V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 91
    iget-object v0, p0, Lflipboard/gui/FLSearchBar;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLSearchBar;->e:Ljava/util/ArrayList;

    .line 95
    :cond_0
    sget-object v0, Lflipboard/gui/FLSearchBar$2;->a:[I

    invoke-virtual {p1}, Lflipboard/gui/FLSearchBar$Input;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 132
    :cond_1
    :goto_0
    return-void

    .line 98
    :pswitch_0
    invoke-virtual {p0}, Lflipboard/gui/FLSearchBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 99
    invoke-virtual {p0}, Lflipboard/gui/FLSearchBar;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0

    .line 103
    :pswitch_1
    invoke-virtual {p0}, Lflipboard/gui/FLSearchBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 104
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 106
    iget-object v0, p0, Lflipboard/gui/FLSearchBar;->c:Lflipboard/service/FLSearchManager;

    invoke-static {v1}, Lflipboard/service/FLSearchManager;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lflipboard/gui/FLSearchBar;->c:Lflipboard/service/FLSearchManager;

    sget-object v2, Lflipboard/service/Flap$SearchType;->d:Lflipboard/service/Flap$SearchType;

    iget-object v3, p0, Lflipboard/gui/FLSearchBar;->d:Lflipboard/service/Flap$SearchObserver;

    const-string v4, "topic"

    const/4 v5, -0x1

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/FLSearchManager;->a(Ljava/lang/String;Lflipboard/service/Flap$SearchType;Lflipboard/service/Flap$SearchObserver;Ljava/lang/String;I)V

    goto :goto_0

    .line 111
    :cond_2
    iget-object v0, p0, Lflipboard/gui/FLSearchBar;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 112
    iget-object v0, p0, Lflipboard/gui/FLSearchBar;->f:Lflipboard/gui/FLSearchBar$SearchResultObserver;

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Lflipboard/gui/FLSearchBar;->f:Lflipboard/gui/FLSearchBar$SearchResultObserver;

    iget-object v1, p0, Lflipboard/gui/FLSearchBar;->e:Ljava/util/ArrayList;

    invoke-interface {v0, v1, v4, v2, v3}, Lflipboard/gui/FLSearchBar$SearchResultObserver;->a(Ljava/util/List;ZJ)V

    goto :goto_0

    .line 119
    :pswitch_2
    const/4 v0, 0x0

    aget-object v0, p2, v0

    iget-object v1, p0, Lflipboard/gui/FLSearchBar;->d:Lflipboard/service/Flap$SearchObserver;

    if-ne v0, v1, :cond_1

    .line 121
    aget-object v0, p2, v4

    invoke-virtual {p0}, Lflipboard/gui/FLSearchBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    aget-object v0, p2, v5

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lflipboard/gui/FLSearchBar;->e:Ljava/util/ArrayList;

    .line 124
    array-length v0, p2

    const/4 v1, 0x3

    if-le v0, v1, :cond_3

    .line 125
    array-length v0, p2

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p2, v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 127
    :goto_1
    iget-object v2, p0, Lflipboard/gui/FLSearchBar;->f:Lflipboard/gui/FLSearchBar$SearchResultObserver;

    iget-object v3, p0, Lflipboard/gui/FLSearchBar;->e:Ljava/util/ArrayList;

    aget-object v4, p2, v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    invoke-interface {v2, v3, v4, v0, v1}, Lflipboard/gui/FLSearchBar$SearchResultObserver;->a(Ljava/util/List;ZJ)V

    goto/16 :goto_0

    :cond_3
    move-wide v0, v2

    goto :goto_1

    .line 95
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 80
    invoke-super {p0}, Lflipboard/gui/FLEditText;->onFinishInflate()V

    .line 81
    invoke-direct {p0}, Lflipboard/gui/FLSearchBar;->b()V

    .line 82
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0, p1, p2, p3, p4}, Lflipboard/gui/FLEditText;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 87
    sget-object v0, Lflipboard/gui/FLSearchBar$Input;->b:Lflipboard/gui/FLSearchBar$Input;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/FLSearchBar;->a(Lflipboard/gui/FLSearchBar$Input;[Ljava/lang/Object;)V

    .line 88
    return-void
.end method

.method public setSearchResultObserver(Lflipboard/gui/FLSearchBar$SearchResultObserver;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lflipboard/gui/FLSearchBar;->f:Lflipboard/gui/FLSearchBar$SearchResultObserver;

    .line 136
    return-void
.end method
