.class public Lflipboard/gui/FLDynamicGridView;
.super Lorg/askerov/dynamicgrid/DynamicGridView;
.source "FLDynamicGridView.java"


# instance fields
.field public a:Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;

.field private d:Lflipboard/gui/FLDynamicGridView$ViewAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lorg/askerov/dynamicgrid/DynamicGridView;-><init>(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lorg/askerov/dynamicgrid/DynamicGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lorg/askerov/dynamicgrid/DynamicGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method static synthetic a(Lflipboard/gui/FLDynamicGridView;)Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView;->a:Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;

    return-object v0
.end method

.method static synthetic b(Lflipboard/gui/FLDynamicGridView;)Lflipboard/gui/FLDynamicGridView$ViewAdapter;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView;->d:Lflipboard/gui/FLDynamicGridView$ViewAdapter;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 40
    invoke-super {p0, p1}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Landroid/content/Context;)V

    .line 42
    const v0, 0x7f020229

    invoke-virtual {p0, v0}, Lflipboard/gui/FLDynamicGridView;->setSelector(I)V

    .line 43
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflipboard/gui/FLDynamicGridView;->setDrawSelectorOnTop(Z)V

    .line 45
    invoke-virtual {p0}, Lflipboard/gui/FLDynamicGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09004e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 46
    invoke-virtual {p0, v0}, Lflipboard/gui/FLDynamicGridView;->setHorizontalSpacing(I)V

    .line 47
    invoke-virtual {p0, v0}, Lflipboard/gui/FLDynamicGridView;->setVerticalSpacing(I)V

    .line 49
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lflipboard/gui/FLDynamicGridView;->setNumColumns(I)V

    .line 50
    new-instance v0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;-><init>(Lflipboard/gui/FLDynamicGridView;Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/FLDynamicGridView;->a:Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;

    .line 51
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView;->a:Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;

    invoke-virtual {p0, v0}, Lflipboard/gui/FLDynamicGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 52
    new-instance v0, Lflipboard/gui/FLDynamicGridView$1;

    invoke-direct {v0, p0}, Lflipboard/gui/FLDynamicGridView$1;-><init>(Lflipboard/gui/FLDynamicGridView;)V

    invoke-virtual {p0, v0}, Lflipboard/gui/FLDynamicGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 58
    return-void
.end method

.method public getColumnWidthSafe()I
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 74
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 75
    invoke-virtual {p0}, Lflipboard/gui/FLDynamicGridView;->getColumnWidth()I

    move-result v0

    .line 81
    :goto_0
    return v0

    .line 79
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/FLDynamicGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09004e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 80
    mul-int/lit8 v0, v0, 0x2

    .line 81
    invoke-virtual {p0}, Lflipboard/gui/FLDynamicGridView;->getMeasuredWidth()I

    move-result v1

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x3

    goto :goto_0
.end method

.method public setHeaderView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView;->a:Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;

    invoke-static {v0, p1}, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->a(Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;Landroid/view/View;)V

    .line 66
    return-void
.end method

.method public setItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView;->a:Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->a(Ljava/util/List;)V

    .line 70
    return-void
.end method

.method public setViewAdapter(Lflipboard/gui/FLDynamicGridView$ViewAdapter;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lflipboard/gui/FLDynamicGridView;->d:Lflipboard/gui/FLDynamicGridView$ViewAdapter;

    .line 62
    return-void
.end method
