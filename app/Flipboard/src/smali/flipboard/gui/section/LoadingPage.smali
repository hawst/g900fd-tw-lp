.class public Lflipboard/gui/section/LoadingPage;
.super Lflipboard/gui/section/SectionPage;
.source "LoadingPage.java"


# instance fields
.field private x:Lflipboard/gui/NoContentView;

.field private y:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lflipboard/service/Section;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 26
    new-instance v1, Lflipboard/gui/section/Group;

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-object v0, v0, Lflipboard/app/FlipboardApplication;->k:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SectionPageTemplate;

    new-instance v2, Lflipboard/objs/FeedItem;

    invoke-direct {v2}, Lflipboard/objs/FeedItem;-><init>()V

    invoke-direct {v1, v4, v0, v2, v3}, Lflipboard/gui/section/Group;-><init>(Lflipboard/service/Section;Lflipboard/objs/SectionPageTemplate;Lflipboard/objs/FeedItem;Z)V

    invoke-direct {p0, p1, v1, p2, p3}, Lflipboard/gui/section/SectionPage;-><init>(Landroid/content/Context;Lflipboard/gui/section/Group;Lflipboard/service/Section;Z)V

    .line 27
    invoke-virtual {p0}, Lflipboard/gui/section/LoadingPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/LoadingPage;->setBackgroundColor(I)V

    .line 30
    const v0, 0x7f0300de

    invoke-static {p1, v0, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/NoContentView;

    iput-object v0, p0, Lflipboard/gui/section/LoadingPage;->x:Lflipboard/gui/NoContentView;

    .line 31
    iget-object v0, p0, Lflipboard/gui/section/LoadingPage;->x:Lflipboard/gui/NoContentView;

    invoke-virtual {v0, p2}, Lflipboard/gui/NoContentView;->setSection(Lflipboard/service/Section;)V

    .line 32
    iget-object v0, p0, Lflipboard/gui/section/LoadingPage;->x:Lflipboard/gui/NoContentView;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/LoadingPage;->a(Landroid/view/View;)V

    .line 34
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/LoadingPage;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 35
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/LoadingPage;)Lflipboard/gui/NoContentView;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lflipboard/gui/section/LoadingPage;->x:Lflipboard/gui/NoContentView;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 66
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/LoadingPage$2;

    invoke-direct {v1, p0}, Lflipboard/gui/section/LoadingPage$2;-><init>(Lflipboard/gui/section/LoadingPage;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 72
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 86
    return-void
.end method
