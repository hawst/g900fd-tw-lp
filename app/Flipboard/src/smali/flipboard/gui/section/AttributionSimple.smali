.class public Lflipboard/gui/section/AttributionSimple;
.super Lflipboard/gui/section/AttributionBase;
.source "AttributionSimple.java"


# instance fields
.field private s:Lflipboard/gui/FLTextView;

.field private t:Lflipboard/gui/FLLabelTextView;

.field private u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private v:I

.field private w:I

.field private x:I

.field private y:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lflipboard/gui/section/AttributionBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/section/AttributionSimple;->u:Ljava/util/List;

    .line 49
    const v0, 0x7f030022

    invoke-static {p1, v0, p0}, Lflipboard/gui/section/AttributionSimple;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f0a007d

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionSimple;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionSimple;->s:Lflipboard/gui/FLTextView;

    iget-object v0, p0, Lflipboard/gui/section/AttributionSimple;->u:Ljava/util/List;

    iget-object v1, p0, Lflipboard/gui/section/AttributionSimple;->s:Lflipboard/gui/FLTextView;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const v0, 0x7f0a007f

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionSimple;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionSimple;->t:Lflipboard/gui/FLLabelTextView;

    iget-object v0, p0, Lflipboard/gui/section/AttributionSimple;->u:Ljava/util/List;

    iget-object v1, p0, Lflipboard/gui/section/AttributionSimple;->t:Lflipboard/gui/FLLabelTextView;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lflipboard/gui/section/AttributionSimple;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080050

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lflipboard/gui/section/AttributionSimple;->v:I

    const v1, 0x7f080051

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lflipboard/gui/section/AttributionSimple;->w:I

    const v1, 0x7f080084

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lflipboard/gui/section/AttributionSimple;->x:I

    const v1, 0x7f080085

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/AttributionSimple;->y:I

    .line 50
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 6

    .prologue
    .line 102
    invoke-virtual {p0, p2}, Lflipboard/gui/section/AttributionSimple;->setTag(Ljava/lang/Object;)V

    .line 104
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 106
    new-instance v1, Lflipboard/gui/section/AttributionSimple$1;

    invoke-direct {v1, p0, v0, p1}, Lflipboard/gui/section/AttributionSimple$1;-><init>(Lflipboard/gui/section/AttributionSimple;Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V

    invoke-virtual {p0, v1}, Lflipboard/gui/section/AttributionSimple;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->B()Lflipboard/objs/FeedSectionLink;

    move-result-object v1

    .line 113
    if-ne p2, v0, :cond_0

    if-eqz v1, :cond_0

    .line 115
    iget-object v0, p0, Lflipboard/gui/section/AttributionSimple;->s:Lflipboard/gui/FLTextView;

    iget-object v1, v1, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    :goto_0
    iget-boolean v0, p2, Lflipboard/objs/FeedItem;->bk:Z

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lflipboard/gui/section/AttributionSimple;->t:Lflipboard/gui/FLLabelTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 127
    :goto_1
    return-void

    .line 117
    :cond_0
    iget-object v1, p0, Lflipboard/gui/section/AttributionSimple;->s:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 123
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionSimple;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-wide v2, p2, Lflipboard/objs/FeedItem;->Z:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lflipboard/util/JavaUtil;->e(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 124
    iget-object v1, p0, Lflipboard/gui/section/AttributionSimple;->t:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Lflipboard/gui/section/AttributionSimple;->t:Lflipboard/gui/FLLabelTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 97
    iget-object v0, p0, Lflipboard/gui/section/AttributionSimple;->u:Ljava/util/List;

    invoke-virtual {p0, v1, v1, v0}, Lflipboard/gui/section/AttributionSimple;->a(IILjava/util/List;)I

    .line 98
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/high16 v7, -0x80000000

    const/4 v1, 0x0

    .line 75
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 76
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 81
    iget-object v0, p0, Lflipboard/gui/section/AttributionSimple;->t:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_1

    .line 82
    iget-object v0, p0, Lflipboard/gui/section/AttributionSimple;->t:Lflipboard/gui/FLLabelTextView;

    add-int/lit8 v2, v3, 0x0

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v4, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v0, v2, v5}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 83
    iget v0, p0, Lflipboard/gui/section/AttributionSimple;->f:I

    iget-object v2, p0, Lflipboard/gui/section/AttributionSimple;->t:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v2, v0, 0x0

    .line 84
    iget-object v0, p0, Lflipboard/gui/section/AttributionSimple;->t:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 87
    :goto_0
    iget-object v5, p0, Lflipboard/gui/section/AttributionSimple;->s:Lflipboard/gui/FLTextView;

    sub-int v6, v3, v2

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v4, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v5, v6, v1}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 88
    iget-object v1, p0, Lflipboard/gui/section/AttributionSimple;->s:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v2

    .line 89
    iget-object v2, p0, Lflipboard/gui/section/AttributionSimple;->s:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 91
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v4, 0x40000000    # 2.0f

    if-ne v0, v4, :cond_0

    move v0, v3

    .line 92
    :goto_1
    invoke-virtual {p0, v0, v2}, Lflipboard/gui/section/AttributionSimple;->setMeasuredDimension(II)V

    .line 93
    return-void

    :cond_0
    move v0, v1

    .line 91
    goto :goto_1

    :cond_1
    move v0, v1

    move v2, v1

    goto :goto_0
.end method

.method public setColor(Z)V
    .locals 2

    .prologue
    .line 69
    iget-object v1, p0, Lflipboard/gui/section/AttributionSimple;->s:Lflipboard/gui/FLTextView;

    if-eqz p1, :cond_0

    iget v0, p0, Lflipboard/gui/section/AttributionSimple;->v:I

    :goto_0
    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    .line 70
    iget-object v1, p0, Lflipboard/gui/section/AttributionSimple;->t:Lflipboard/gui/FLLabelTextView;

    if-eqz p1, :cond_1

    iget v0, p0, Lflipboard/gui/section/AttributionSimple;->x:I

    :goto_1
    invoke-virtual {v1, v0}, Lflipboard/gui/FLLabelTextView;->setTextColor(I)V

    .line 71
    return-void

    .line 69
    :cond_0
    iget v0, p0, Lflipboard/gui/section/AttributionSimple;->w:I

    goto :goto_0

    .line 70
    :cond_1
    iget v0, p0, Lflipboard/gui/section/AttributionSimple;->y:I

    goto :goto_1
.end method

.method public setInverted(Z)V
    .locals 0

    .prologue
    .line 131
    return-void
.end method
