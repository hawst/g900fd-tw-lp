.class Lflipboard/gui/section/AttributionTablet$2;
.super Ljava/lang/Object;
.source "AttributionTablet.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/objs/FeedSectionLink;

.field final synthetic b:Lflipboard/service/Section;

.field final synthetic c:Landroid/content/Context;

.field final synthetic d:Lflipboard/gui/section/AttributionTablet;


# direct methods
.method constructor <init>(Lflipboard/gui/section/AttributionTablet;Lflipboard/objs/FeedSectionLink;Lflipboard/service/Section;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 362
    iput-object p1, p0, Lflipboard/gui/section/AttributionTablet$2;->d:Lflipboard/gui/section/AttributionTablet;

    iput-object p2, p0, Lflipboard/gui/section/AttributionTablet$2;->a:Lflipboard/objs/FeedSectionLink;

    iput-object p3, p0, Lflipboard/gui/section/AttributionTablet$2;->b:Lflipboard/service/Section;

    iput-object p4, p0, Lflipboard/gui/section/AttributionTablet$2;->c:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 366
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v1, "topicButtonTapped"

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    .line 367
    new-instance v0, Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet$2;->a:Lflipboard/objs/FeedSectionLink;

    invoke-direct {v0, v1}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    .line 368
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 369
    const-string v2, "source"

    sget-object v3, Lflipboard/objs/UsageEventV2$SectionNavFrom;->l:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    invoke-virtual {v3}, Lflipboard/objs/UsageEventV2$SectionNavFrom;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const-string v2, "originSectionIdentifier"

    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet$2;->b:Lflipboard/service/Section;

    iget-object v3, v3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v3, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v2

    if-nez v2, :cond_0

    .line 372
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2, v0}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 374
    :cond_0
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet$2;->c:Landroid/content/Context;

    invoke-virtual {v0, v2, v1}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 375
    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet$2;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 376
    return-void
.end method
