.class Lflipboard/gui/section/MagazineLikesDialog$1$1;
.super Ljava/lang/Object;
.source "MagazineLikesDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lflipboard/gui/section/MagazineLikesDialog$1;


# direct methods
.method constructor <init>(Lflipboard/gui/section/MagazineLikesDialog$1;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lflipboard/gui/section/MagazineLikesDialog$1$1;->b:Lflipboard/gui/section/MagazineLikesDialog$1;

    iput-object p2, p0, Lflipboard/gui/section/MagazineLikesDialog$1$1;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 78
    iget-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog$1$1;->b:Lflipboard/gui/section/MagazineLikesDialog$1;

    iget-object v0, v0, Lflipboard/gui/section/MagazineLikesDialog$1;->a:Lflipboard/gui/section/MagazineLikesDialog;

    invoke-static {v0}, Lflipboard/gui/section/MagazineLikesDialog;->a(Lflipboard/gui/section/MagazineLikesDialog;)Z

    .line 80
    iget-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog$1$1;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item;

    .line 83
    iget-object v2, p0, Lflipboard/gui/section/MagazineLikesDialog$1$1;->b:Lflipboard/gui/section/MagazineLikesDialog$1;

    iget-object v2, v2, Lflipboard/gui/section/MagazineLikesDialog$1;->a:Lflipboard/gui/section/MagazineLikesDialog;

    invoke-static {v2}, Lflipboard/gui/section/MagazineLikesDialog;->b(Lflipboard/gui/section/MagazineLikesDialog;)Lflipboard/objs/FeedItem;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/objs/CommentaryResult$Item;->a:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/gui/section/MagazineLikesDialog$1$1;->b:Lflipboard/gui/section/MagazineLikesDialog$1;

    iget-object v3, v3, Lflipboard/gui/section/MagazineLikesDialog$1;->a:Lflipboard/gui/section/MagazineLikesDialog;

    invoke-static {v3}, Lflipboard/gui/section/MagazineLikesDialog;->b(Lflipboard/gui/section/MagazineLikesDialog;)Lflipboard/objs/FeedItem;

    move-result-object v3

    iget-object v3, v3, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 86
    iget-object v1, v0, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 87
    iget-object v1, p0, Lflipboard/gui/section/MagazineLikesDialog$1$1;->b:Lflipboard/gui/section/MagazineLikesDialog$1;

    iget-object v1, v1, Lflipboard/gui/section/MagazineLikesDialog$1;->a:Lflipboard/gui/section/MagazineLikesDialog;

    iget-object v1, v1, Lflipboard/gui/section/MagazineLikesDialog;->l:Ljava/util/ArrayList;

    iget-object v2, v0, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 91
    :cond_1
    iget-object v1, p0, Lflipboard/gui/section/MagazineLikesDialog$1$1;->b:Lflipboard/gui/section/MagazineLikesDialog$1;

    iget-object v1, v1, Lflipboard/gui/section/MagazineLikesDialog$1;->a:Lflipboard/gui/section/MagazineLikesDialog;

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item;->u:Ljava/lang/String;

    invoke-static {v1, v0}, Lflipboard/gui/section/MagazineLikesDialog;->a(Lflipboard/gui/section/MagazineLikesDialog;Ljava/lang/String;)Ljava/lang/String;

    .line 92
    iget-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog$1$1;->b:Lflipboard/gui/section/MagazineLikesDialog$1;

    iget-object v0, v0, Lflipboard/gui/section/MagazineLikesDialog$1;->a:Lflipboard/gui/section/MagazineLikesDialog;

    invoke-static {v0}, Lflipboard/gui/section/MagazineLikesDialog;->c(Lflipboard/gui/section/MagazineLikesDialog;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 94
    iget-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog$1$1;->b:Lflipboard/gui/section/MagazineLikesDialog$1;

    iget-object v0, v0, Lflipboard/gui/section/MagazineLikesDialog$1;->a:Lflipboard/gui/section/MagazineLikesDialog;

    invoke-static {v0}, Lflipboard/gui/section/MagazineLikesDialog;->d(Lflipboard/gui/section/MagazineLikesDialog;)Z

    .line 97
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog$1$1;->b:Lflipboard/gui/section/MagazineLikesDialog$1;

    iget-object v0, v0, Lflipboard/gui/section/MagazineLikesDialog$1;->a:Lflipboard/gui/section/MagazineLikesDialog;

    invoke-static {v0}, Lflipboard/gui/section/MagazineLikesDialog;->e(Lflipboard/gui/section/MagazineLikesDialog;)Lflipboard/gui/section/MagazineLikesDialog$Adapter;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->notifyDataSetChanged()V

    .line 101
    :cond_3
    return-void
.end method
