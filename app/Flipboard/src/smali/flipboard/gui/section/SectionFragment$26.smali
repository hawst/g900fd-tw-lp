.class public Lflipboard/gui/section/SectionFragment$26;
.super Ljava/lang/Object;
.source "SectionFragment.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/gui/section/SectionFragment;


# direct methods
.method public constructor <init>(Lflipboard/gui/section/SectionFragment;)V
    .locals 0

    .prologue
    .line 3544
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$26;->a:Lflipboard/gui/section/SectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 3548
    sget-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    new-array v0, v5, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 3550
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$26;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v3, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$26;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    iget-object v4, v3, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    const/4 v1, 0x0

    iput-object v1, v0, Lflipboard/objs/FeedItem;->N:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->d()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v3, v0}, Lflipboard/service/Section;->b(Lflipboard/objs/FeedItem;)V

    move v1, v2

    :goto_0
    iget-object v0, v3, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, v3, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v4}, Lflipboard/objs/FeedItem;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v3, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v0, v3, Lflipboard/service/Section;->u:Ljava/util/List;

    iget-object v4, v3, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    invoke-interface {v0, v1, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    sget-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    new-array v0, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    :cond_0
    invoke-virtual {v3, v5}, Lflipboard/service/Section;->e(Z)V

    invoke-virtual {v3}, Lflipboard/service/Section;->u()V

    sget-object v0, Lflipboard/service/Section$Message;->f:Lflipboard/service/Section$Message;

    iget-object v1, v3, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    invoke-virtual {v3, v0, v1}, Lflipboard/service/Section;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3552
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$26;->a:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lflipboard/activities/FeedActivity;

    if-eqz v0, :cond_1

    .line 3553
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$26;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/SectionFragment$26$1;

    invoke-direct {v1, p0}, Lflipboard/gui/section/SectionFragment$26$1;-><init>(Lflipboard/gui/section/SectionFragment$26;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 3561
    :cond_1
    return-void

    .line 3550
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 3566
    sget-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 3567
    return-void
.end method
