.class Lflipboard/gui/section/SectionFragment$12;
.super Ljava/lang/Object;
.source "SectionFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:J

.field final synthetic b:Lflipboard/gui/section/SectionFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment;J)V
    .locals 0

    .prologue
    .line 1507
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$12;->b:Lflipboard/gui/section/SectionFragment;

    iput-wide p2, p0, Lflipboard/gui/section/SectionFragment$12;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 6

    .prologue
    .line 1510
    const-string v0, "section_open"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lflipboard/gui/section/SectionFragment$12;->a:J

    sub-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;J)V

    .line 1511
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$12;->b:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1512
    const/4 v0, 0x1

    return v0
.end method
