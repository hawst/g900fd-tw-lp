.class public Lflipboard/gui/section/SectionTabletView;
.super Lflipboard/gui/flipping/FlipTransitionViews;
.source "SectionTabletView.java"


# instance fields
.field a:Lflipboard/gui/section/SectionFragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lflipboard/gui/section/SectionFragment;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lflipboard/gui/flipping/FlipTransitionViews;-><init>(Landroid/content/Context;)V

    .line 30
    iput-object p2, p0, Lflipboard/gui/section/SectionTabletView;->a:Lflipboard/gui/section/SectionFragment;

    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/SectionTabletView;->x:Z

    .line 32
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/item/TabletItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    instance-of v0, p0, Lflipboard/gui/item/TabletItem;

    if-eqz v0, :cond_1

    .line 40
    check-cast p0, Lflipboard/gui/item/TabletItem;

    .line 41
    invoke-interface {p0}, Lflipboard/gui/item/TabletItem;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 42
    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_0
    return-void

    .line 46
    :cond_1
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 47
    check-cast p0, Landroid/view/ViewGroup;

    .line 48
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 49
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2, p1}, Lflipboard/gui/section/SectionTabletView;->a(Landroid/view/View;Ljava/util/List;)V

    .line 48
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method final a(IILjava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/item/TabletItem;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/item/TabletItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p0}, Lflipboard/gui/section/SectionTabletView;->getFlippableViews()Ljava/util/List;

    move-result-object v2

    .line 56
    const/4 v0, 0x0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 57
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    move v1, v0

    .line 58
    :goto_0
    if-gt v1, v3, :cond_0

    .line 59
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p3}, Lflipboard/gui/section/SectionTabletView;->a(Landroid/view/View;Ljava/util/List;)V

    .line 58
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 61
    :cond_0
    return-object p3
.end method

.method protected final d()V
    .locals 0

    .prologue
    .line 106
    invoke-super {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->d()V

    .line 107
    invoke-virtual {p0}, Lflipboard/gui/section/SectionTabletView;->u()V

    .line 108
    return-void
.end method

.method protected final k()V
    .locals 2

    .prologue
    .line 140
    invoke-super {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->k()V

    .line 141
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->l()Lflipboard/service/FlipboardManager$RootScreenStyle;

    move-result-object v0

    .line 142
    sget-object v1, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v0, v1, :cond_0

    .line 143
    iget-object v0, p0, Lflipboard/gui/section/SectionTabletView;->a:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->l()V

    .line 150
    :goto_0
    return-void

    .line 145
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/section/SectionTabletView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/SectionTabletActivity;

    .line 146
    iget-object v0, v0, Lflipboard/activities/SectionTabletActivity;->o:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->l()V

    goto :goto_0
.end method

.method public final o()V
    .locals 0

    .prologue
    .line 100
    invoke-super {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->o()V

    .line 101
    invoke-virtual {p0}, Lflipboard/gui/section/SectionTabletView;->u()V

    .line 102
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    .line 154
    invoke-super/range {p0 .. p5}, Lflipboard/gui/flipping/FlipTransitionViews;->onLayout(ZIIII)V

    .line 155
    sub-int v0, p4, p2

    .line 156
    sub-int v1, p5, p3

    .line 157
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {p0}, Lflipboard/gui/section/SectionTabletView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, Lflipboard/service/FlipboardManager;->a(IILandroid/content/Context;)V

    .line 158
    return-void
.end method

.method protected final r()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return v0
.end method

.method public final u()V
    .locals 3

    .prologue
    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 70
    iget v1, p0, Lflipboard/gui/section/SectionTabletView;->h:I

    iget v2, p0, Lflipboard/gui/section/SectionTabletView;->h:I

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {p0, v1, v2, v0}, Lflipboard/gui/section/SectionTabletView;->a(IILjava/util/List;)Ljava/util/List;

    .line 71
    iget v1, p0, Lflipboard/gui/section/SectionTabletView;->h:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lflipboard/gui/section/SectionTabletView;->h:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v1, v2, v0}, Lflipboard/gui/section/SectionTabletView;->a(IILjava/util/List;)Ljava/util/List;

    .line 74
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 96
    :goto_0
    return-void

    .line 81
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 82
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/TabletItem;

    .line 84
    invoke-interface {v0}, Lflipboard/gui/item/TabletItem;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 87
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 95
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/util/List;)V

    goto :goto_0
.end method
