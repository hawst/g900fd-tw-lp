.class Lflipboard/gui/section/ContributorsDialog$Adapter;
.super Landroid/widget/BaseAdapter;
.source "ContributorsDialog.java"


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/CommentaryResult$Item$Commentary;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lflipboard/gui/section/ContributorsDialog;

.field private c:Landroid/view/View$OnClickListener;

.field private d:Landroid/view/View$OnClickListener;

.field private final e:Lflipboard/objs/CommentaryResult$Item$Commentary;


# direct methods
.method public constructor <init>(Lflipboard/gui/section/ContributorsDialog;)V
    .locals 3

    .prologue
    .line 106
    iput-object p1, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->b:Lflipboard/gui/section/ContributorsDialog;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1}, Lflipboard/gui/section/ContributorsDialog;->a(Lflipboard/gui/section/ContributorsDialog;)Lflipboard/service/Section;

    move-result-object v1

    iget-object v1, v1, Lflipboard/service/Section;->G:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->a:Ljava/util/List;

    .line 108
    new-instance v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    invoke-direct {v0}, Lflipboard/objs/CommentaryResult$Item$Commentary;-><init>()V

    .line 109
    const-string v1, "header"

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    .line 110
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v2, 0x7f0d0203

    invoke-virtual {v1, v2}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->c:Ljava/lang/String;

    .line 111
    iget-object v1, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    new-instance v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    invoke-direct {v0}, Lflipboard/objs/CommentaryResult$Item$Commentary;-><init>()V

    .line 114
    const-string v1, "owner"

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    .line 115
    invoke-static {p1}, Lflipboard/gui/section/ContributorsDialog;->a(Lflipboard/gui/section/ContributorsDialog;)Lflipboard/service/Section;

    move-result-object v1

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->M:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->c:Ljava/lang/String;

    .line 116
    invoke-static {p1}, Lflipboard/gui/section/ContributorsDialog;->a(Lflipboard/gui/section/ContributorsDialog;)Lflipboard/service/Section;

    move-result-object v1

    iget-object v1, v1, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    .line 117
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 118
    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v2

    iget-object v2, v2, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    iput-object v2, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->g:Lflipboard/objs/Image;

    .line 119
    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v1

    iget-object v1, v1, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->b:Ljava/lang/String;

    .line 121
    :cond_0
    iget-object v1, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    new-instance v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    invoke-direct {v0}, Lflipboard/objs/CommentaryResult$Item$Commentary;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->e:Lflipboard/objs/CommentaryResult$Item$Commentary;

    .line 124
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->e:Lflipboard/objs/CommentaryResult$Item$Commentary;

    const-string v1, "header"

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    .line 125
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->e:Lflipboard/objs/CommentaryResult$Item$Commentary;

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v2, 0x7f0d01f3

    invoke-virtual {v1, v2}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->c:Ljava/lang/String;

    .line 126
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->a:Ljava/util/List;

    iget-object v1, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->e:Lflipboard/objs/CommentaryResult$Item$Commentary;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->a:Ljava/util/List;

    invoke-static {p1}, Lflipboard/gui/section/ContributorsDialog;->a(Lflipboard/gui/section/ContributorsDialog;)Lflipboard/service/Section;

    move-result-object v1

    iget-object v1, v1, Lflipboard/service/Section;->G:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 129
    new-instance v0, Lflipboard/gui/section/ContributorsDialog$Adapter$1;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/section/ContributorsDialog$Adapter$1;-><init>(Lflipboard/gui/section/ContributorsDialog$Adapter;Lflipboard/gui/section/ContributorsDialog;)V

    iput-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->c:Landroid/view/View$OnClickListener;

    .line 177
    new-instance v0, Lflipboard/gui/section/ContributorsDialog$Adapter$2;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/section/ContributorsDialog$Adapter$2;-><init>(Lflipboard/gui/section/ContributorsDialog$Adapter;Lflipboard/gui/section/ContributorsDialog;)V

    iput-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->d:Landroid/view/View$OnClickListener;

    .line 198
    return-void
.end method

.method private a(I)Lflipboard/objs/CommentaryResult$Item$Commentary;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lflipboard/gui/section/ContributorsDialog$Adapter;->a(I)Lflipboard/objs/CommentaryResult$Item$Commentary;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 213
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 225
    invoke-direct {p0, p1}, Lflipboard/gui/section/ContributorsDialog$Adapter;->a(I)Lflipboard/objs/CommentaryResult$Item$Commentary;

    move-result-object v0

    .line 226
    iget-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    const-string v1, "header"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 238
    invoke-direct {p0, p1}, Lflipboard/gui/section/ContributorsDialog$Adapter;->a(I)Lflipboard/objs/CommentaryResult$Item$Commentary;

    move-result-object v3

    .line 241
    if-nez p2, :cond_4

    .line 242
    new-instance v2, Lflipboard/gui/section/ContributorsDialog$Holder;

    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->b:Lflipboard/gui/section/ContributorsDialog;

    invoke-direct {v2, v0, v6}, Lflipboard/gui/section/ContributorsDialog$Holder;-><init>(Lflipboard/gui/section/ContributorsDialog;B)V

    .line 243
    invoke-virtual {p0, p1}, Lflipboard/gui/section/ContributorsDialog$Adapter;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_3

    .line 245
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03004d

    invoke-static {v0, v1, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 246
    const v0, 0x7f0a0125

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lflipboard/gui/section/ContributorsDialog$Holder;->d:Landroid/view/View;

    .line 247
    const v0, 0x7f0a00c0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, v2, Lflipboard/gui/section/ContributorsDialog$Holder;->a:Lflipboard/gui/FLTextIntf;

    .line 248
    const v0, 0x7f0a00c3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, v2, Lflipboard/gui/section/ContributorsDialog$Holder;->b:Lflipboard/gui/FLTextIntf;

    .line 249
    const v0, 0x7f0a00be

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, v2, Lflipboard/gui/section/ContributorsDialog$Holder;->c:Lflipboard/gui/FLImageView;

    .line 250
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0900b7

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 251
    iget-object v4, v2, Lflipboard/gui/section/ContributorsDialog$Holder;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 252
    iput v0, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 253
    iput v0, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 254
    iget-object v0, v2, Lflipboard/gui/section/ContributorsDialog$Holder;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v6}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 255
    iget-object v0, v2, Lflipboard/gui/section/ContributorsDialog$Holder;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02005a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflipboard/gui/FLImageView;->setPlaceholder(Landroid/graphics/drawable/Drawable;)V

    .line 257
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 258
    iget-object v0, v2, Lflipboard/gui/section/ContributorsDialog$Holder;->d:Landroid/view/View;

    iget-object v4, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    const v0, 0x7f0a012a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    move-object v0, v1

    .line 261
    check-cast v0, Landroid/view/ViewGroup;

    .line 262
    const v4, 0x7f0a0127

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 268
    :goto_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v2

    .line 273
    :goto_1
    iput-object v3, v0, Lflipboard/gui/section/ContributorsDialog$Holder;->e:Lflipboard/objs/CommentaryResult$Item$Commentary;

    .line 274
    iget-object v2, v0, Lflipboard/gui/section/ContributorsDialog$Holder;->a:Lflipboard/gui/FLTextIntf;

    iget-object v4, v3, Lflipboard/objs/CommentaryResult$Item$Commentary;->c:Ljava/lang/String;

    invoke-interface {v2, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 275
    iget-object v2, v0, Lflipboard/gui/section/ContributorsDialog$Holder;->b:Lflipboard/gui/FLTextIntf;

    if-eqz v2, :cond_0

    .line 276
    iget-object v2, v0, Lflipboard/gui/section/ContributorsDialog$Holder;->b:Lflipboard/gui/FLTextIntf;

    iget-object v4, v3, Lflipboard/objs/CommentaryResult$Item$Commentary;->b:Ljava/lang/String;

    invoke-interface {v2, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 278
    :cond_0
    iget-object v2, v0, Lflipboard/gui/section/ContributorsDialog$Holder;->c:Lflipboard/gui/FLImageView;

    if-eqz v2, :cond_1

    .line 279
    iget-object v2, v0, Lflipboard/gui/section/ContributorsDialog$Holder;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->a()V

    .line 280
    iget-object v2, v0, Lflipboard/gui/section/ContributorsDialog$Holder;->c:Lflipboard/gui/FLImageView;

    iget-object v4, v3, Lflipboard/objs/CommentaryResult$Item$Commentary;->g:Lflipboard/objs/Image;

    invoke-virtual {v2, v4}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 283
    :cond_1
    iget-object v2, v0, Lflipboard/gui/section/ContributorsDialog$Holder;->d:Landroid/view/View;

    if-eqz v2, :cond_2

    .line 284
    iget-object v2, v0, Lflipboard/gui/section/ContributorsDialog$Holder;->d:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 285
    const/4 v2, 0x1

    if-eq p1, v2, :cond_5

    iget-object v2, p0, Lflipboard/gui/section/ContributorsDialog$Adapter;->b:Lflipboard/gui/section/ContributorsDialog;

    invoke-static {v2}, Lflipboard/gui/section/ContributorsDialog;->a(Lflipboard/gui/section/ContributorsDialog;)Lflipboard/service/Section;

    move-result-object v2

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2, v3}, Lflipboard/service/Section;->a(Lflipboard/service/User;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 286
    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Holder;->d:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 291
    :cond_2
    :goto_2
    return-object v1

    .line 265
    :cond_3
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03004f

    invoke-static {v0, v1, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 266
    const v0, 0x7f0a004f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, v2, Lflipboard/gui/section/ContributorsDialog$Holder;->a:Lflipboard/gui/FLTextIntf;

    goto :goto_0

    .line 270
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/ContributorsDialog$Holder;

    move-object v1, p2

    goto :goto_1

    .line 288
    :cond_5
    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Holder;->d:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    .line 232
    invoke-direct {p0, p1}, Lflipboard/gui/section/ContributorsDialog$Adapter;->a(I)Lflipboard/objs/CommentaryResult$Item$Commentary;

    move-result-object v0

    .line 233
    iget-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    const-string v1, "header"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
