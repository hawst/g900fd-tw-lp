.class public Lflipboard/gui/section/MagazineLikesDialog;
.super Lflipboard/gui/dialog/FLDialogFragment;
.source "MagazineLikesDialog.java"


# instance fields
.field public j:Lflipboard/objs/FeedItem;

.field public k:Lflipboard/gui/section/MagazineLikesDialog$Adapter;

.field public l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/objs/CommentaryResult$Item$Commentary;",
            ">;"
        }
    .end annotation
.end field

.field public m:Z

.field public n:Ljava/lang/String;

.field public o:Z

.field private p:Lflipboard/service/Flap$CommentaryObserver;

.field private q:Landroid/widget/AbsListView$OnScrollListener;

.field private r:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogFragment;-><init>()V

    .line 72
    new-instance v0, Lflipboard/gui/section/MagazineLikesDialog$1;

    invoke-direct {v0, p0}, Lflipboard/gui/section/MagazineLikesDialog$1;-><init>(Lflipboard/gui/section/MagazineLikesDialog;)V

    iput-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog;->p:Lflipboard/service/Flap$CommentaryObserver;

    .line 118
    new-instance v0, Lflipboard/gui/section/MagazineLikesDialog$2;

    invoke-direct {v0, p0}, Lflipboard/gui/section/MagazineLikesDialog$2;-><init>(Lflipboard/gui/section/MagazineLikesDialog;)V

    iput-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog;->q:Landroid/widget/AbsListView$OnScrollListener;

    .line 133
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/MagazineLikesDialog;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lflipboard/gui/section/MagazineLikesDialog;->n:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lflipboard/gui/section/MagazineLikesDialog;)Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/section/MagazineLikesDialog;->o:Z

    return v0
.end method

.method static synthetic b(Lflipboard/gui/section/MagazineLikesDialog;)Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog;->j:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method static synthetic c(Lflipboard/gui/section/MagazineLikesDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lflipboard/gui/section/MagazineLikesDialog;)Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/section/MagazineLikesDialog;->m:Z

    return v0
.end method

.method static synthetic e(Lflipboard/gui/section/MagazineLikesDialog;)Lflipboard/gui/section/MagazineLikesDialog$Adapter;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog;->k:Lflipboard/gui/section/MagazineLikesDialog$Adapter;

    return-object v0
.end method

.method static synthetic f(Lflipboard/gui/section/MagazineLikesDialog;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lflipboard/gui/section/MagazineLikesDialog;->m:Z

    return v0
.end method

.method static synthetic g(Lflipboard/gui/section/MagazineLikesDialog;)Landroid/view/View;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog;->r:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final e()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 138
    iget-boolean v0, p0, Lflipboard/gui/section/MagazineLikesDialog;->m:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/gui/section/MagazineLikesDialog;->o:Z

    if-nez v0, :cond_0

    .line 139
    iput-boolean v5, p0, Lflipboard/gui/section/MagazineLikesDialog;->o:Z

    .line 140
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/section/MagazineLikesDialog;->j:Lflipboard/objs/FeedItem;

    iget-object v2, p0, Lflipboard/gui/section/MagazineLikesDialog;->n:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/gui/section/MagazineLikesDialog;->p:Lflipboard/service/Flap$CommentaryObserver;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v4, v5, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/util/List;ZLjava/lang/String;Lflipboard/service/Flap$CommentaryObserver;)V

    .line 142
    :cond_0
    return-void
.end method

.method public final l_()Z
    .locals 1

    .prologue
    .line 338
    invoke-virtual {p0}, Lflipboard/gui/section/MagazineLikesDialog;->a()V

    .line 339
    const/4 v0, 0x1

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 147
    invoke-super {p0, p1}, Lflipboard/gui/dialog/FLDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 149
    const v0, 0x7f0e0005

    invoke-virtual {p0, v0}, Lflipboard/gui/section/MagazineLikesDialog;->a(I)V

    .line 150
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 155
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030057

    invoke-static {v0, v1, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 156
    const v0, 0x7f0a004c

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 157
    invoke-virtual {v0, v6, v3}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    .line 158
    invoke-virtual {p0}, Lflipboard/gui/section/MagazineLikesDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lflipboard/activities/FlipboardActivity;

    .line 159
    if-eqz v1, :cond_0

    .line 160
    invoke-virtual {v0, v1, p0}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/gui/dialog/FLDialogFragment;)V

    .line 162
    :cond_0
    const v1, 0x7f0a0143

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 165
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f030050

    invoke-static {v1, v2, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/section/MagazineLikesDialog;->r:Landroid/view/View;

    .line 166
    iget-object v1, p0, Lflipboard/gui/section/MagazineLikesDialog;->r:Landroid/view/View;

    const v2, 0x7f0a012d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 169
    iget-object v1, p0, Lflipboard/gui/section/MagazineLikesDialog;->j:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    if-eqz v1, :cond_2

    .line 170
    iget-object v1, p0, Lflipboard/gui/section/MagazineLikesDialog;->j:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    iget v1, v1, Lflipboard/objs/CommentaryResult$Item;->c:I

    move v2, v1

    .line 172
    :goto_0
    if-ne v2, v6, :cond_1

    .line 175
    invoke-virtual {p0}, Lflipboard/gui/section/MagazineLikesDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0d01e2

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 179
    :goto_1
    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v3

    invoke-static {v1, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 180
    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 182
    const v0, 0x7f0a0144

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 183
    new-instance v1, Lflipboard/gui/section/MagazineLikesDialog$Adapter;

    invoke-direct {v1, p0, v3}, Lflipboard/gui/section/MagazineLikesDialog$Adapter;-><init>(Lflipboard/gui/section/MagazineLikesDialog;B)V

    iput-object v1, p0, Lflipboard/gui/section/MagazineLikesDialog;->k:Lflipboard/gui/section/MagazineLikesDialog$Adapter;

    .line 184
    iget-object v1, p0, Lflipboard/gui/section/MagazineLikesDialog;->k:Lflipboard/gui/section/MagazineLikesDialog$Adapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 185
    iget-object v1, p0, Lflipboard/gui/section/MagazineLikesDialog;->q:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 187
    return-object v4

    .line 177
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/section/MagazineLikesDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0d01e1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 192
    invoke-super {p0}, Lflipboard/gui/dialog/FLDialogFragment;->onDestroyView()V

    .line 193
    iput-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog;->p:Lflipboard/service/Flap$CommentaryObserver;

    .line 194
    iput-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog;->q:Landroid/widget/AbsListView$OnScrollListener;

    .line 195
    return-void
.end method
