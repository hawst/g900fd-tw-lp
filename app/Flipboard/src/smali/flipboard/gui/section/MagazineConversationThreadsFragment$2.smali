.class Lflipboard/gui/section/MagazineConversationThreadsFragment$2;
.super Ljava/lang/Object;
.source "MagazineConversationThreadsFragment.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/section/MagazineConversationThreadsFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/MagazineConversationThreadsFragment;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment$2;->a:Lflipboard/gui/section/MagazineConversationThreadsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const v4, 0x7f04001b

    .line 135
    new-instance v0, Lflipboard/gui/section/MagazineConversationComposeFragment;

    invoke-direct {v0}, Lflipboard/gui/section/MagazineConversationComposeFragment;-><init>()V

    .line 136
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 137
    const-string v2, "feedItemId"

    iget-object v3, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment$2;->a:Lflipboard/gui/section/MagazineConversationThreadsFragment;

    invoke-static {v3}, Lflipboard/gui/section/MagazineConversationThreadsFragment;->c(Lflipboard/gui/section/MagazineConversationThreadsFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-virtual {v0, v1}, Lflipboard/gui/section/MagazineConversationComposeFragment;->setArguments(Landroid/os/Bundle;)V

    .line 139
    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment$2;->a:Lflipboard/gui/section/MagazineConversationThreadsFragment;

    invoke-static {v1}, Lflipboard/gui/section/MagazineConversationThreadsFragment;->d(Lflipboard/gui/section/MagazineConversationThreadsFragment;)Lflipboard/activities/GenericFragmentActivity;

    move-result-object v1

    iget-object v1, v1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 140
    const v2, 0x7f040019

    const v3, 0x7f040011

    invoke-virtual {v1, v2, v4, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->a(IIII)Landroid/support/v4/app/FragmentTransaction;

    .line 141
    const v2, 0x7f0a004e

    const-string v3, "magazine_conversation_compose"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 142
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->a(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 143
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 144
    const/4 v0, 0x1

    return v0
.end method
