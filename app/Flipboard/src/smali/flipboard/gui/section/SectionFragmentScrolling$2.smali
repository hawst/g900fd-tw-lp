.class Lflipboard/gui/section/SectionFragmentScrolling$2;
.super Ljava/lang/Object;
.source "SectionFragmentScrolling.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/section/SectionFragmentScrolling;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragmentScrolling;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 150
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 151
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    .line 152
    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-virtual {v2}, Lflipboard/gui/section/SectionFragmentScrolling;->getResources()Landroid/content/res/Resources;

    .line 153
    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v2}, Lflipboard/gui/section/SectionFragmentScrolling;->a(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/service/Section;

    move-result-object v2

    sget-object v3, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->a:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    invoke-static {v2, v1, v3}, Lflipboard/gui/FlipFragment;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UsageEventV2$FlipItemNavFrom;)Lflipboard/gui/FlipFragment;

    move-result-object v1

    .line 154
    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-virtual {v2}, Lflipboard/gui/section/SectionFragmentScrolling;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const v3, 0x7f0a02f0

    invoke-virtual {v2, v3, v1}, Landroid/support/v4/app/FragmentTransaction;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 155
    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragmentScrolling;->b(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/ViewWithDrawerLayout;

    move-result-object v1

    new-instance v2, Lflipboard/gui/section/SectionFragmentScrolling$2$1;

    invoke-direct {v2, p0, v0}, Lflipboard/gui/section/SectionFragmentScrolling$2$1;-><init>(Lflipboard/gui/section/SectionFragmentScrolling$2;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Lflipboard/gui/ViewWithDrawerLayout;->setAnimationListener(Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;)V

    .line 211
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->b(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/ViewWithDrawerLayout;

    move-result-object v0

    iget-boolean v1, v0, Lflipboard/gui/ViewWithDrawerLayout;->a:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/gui/ViewWithDrawerLayout;->a:Z

    invoke-virtual {v0}, Lflipboard/gui/ViewWithDrawerLayout;->a()V

    .line 213
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->f(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/activities/FlipboardActivity;

    move-result-object v0

    new-instance v1, Lflipboard/gui/section/SectionFragmentScrolling$2$2;

    invoke-direct {v1, p0}, Lflipboard/gui/section/SectionFragmentScrolling$2$2;-><init>(Lflipboard/gui/section/SectionFragmentScrolling$2;)V

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->a(Lflipboard/activities/FlipboardActivity$OnBackPressedListener;)V

    .line 220
    return-void
.end method
