.class Lflipboard/gui/section/SectionFragment$14;
.super Ljava/lang/Object;
.source "SectionFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/gui/section/SectionFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment;)V
    .locals 0

    .prologue
    .line 1969
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$14;->a:Lflipboard/gui/section/SectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1972
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$14;->a:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->e()Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v0

    .line 1973
    if-eqz v0, :cond_1

    .line 1974
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$14;->a:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Lflipboard/gui/section/SectionFragment;->a(I)V

    .line 1975
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$14;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragment;->b(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/SectionScrubber;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1976
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$14;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragment;->b(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/SectionScrubber;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$14;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v2}, Lflipboard/gui/section/SectionFragment;->l(Lflipboard/gui/section/SectionFragment;)I

    move-result v2

    invoke-virtual {v1, v2}, Lflipboard/gui/section/SectionScrubber;->setPosition$2563266(I)V

    .line 1977
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$14;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragment;->l(Lflipboard/gui/section/SectionFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Lflipboard/gui/section/SectionAdPage;

    if-eqz v1, :cond_2

    .line 1978
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$14;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->b(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/SectionScrubber;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionScrubber;->setVisibility(I)V

    .line 1991
    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$14;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->j(Lflipboard/gui/section/SectionFragment;)Lflipboard/service/FLAdManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1992
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$14;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->l(Lflipboard/gui/section/SectionFragment;)I

    move-result v0

    .line 1993
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$14;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    const-string v2, "SectionFragment:notify:FLIP_WILL_COMPLETE"

    new-instance v3, Lflipboard/gui/section/SectionFragment$14$1;

    invoke-direct {v3, p0, v0}, Lflipboard/gui/section/SectionFragment$14$1;-><init>(Lflipboard/gui/section/SectionFragment$14;I)V

    invoke-virtual {v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 2001
    :cond_1
    return-void

    .line 1979
    :cond_2
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Lflipboard/gui/section/SectionPage;

    if-eqz v1, :cond_4

    .line 1980
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/SectionPage;

    .line 1981
    iget-object v0, v0, Lflipboard/gui/section/SectionPage;->u:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 1982
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$14;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->b(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/SectionScrubber;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionScrubber;->setVisibility(I)V

    goto :goto_0

    .line 1984
    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$14;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->b(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/SectionScrubber;

    move-result-object v0

    invoke-virtual {v0, v3}, Lflipboard/gui/section/SectionScrubber;->setVisibility(I)V

    goto :goto_0

    .line 1987
    :cond_4
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$14;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->b(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/SectionScrubber;

    move-result-object v0

    invoke-virtual {v0, v3}, Lflipboard/gui/section/SectionScrubber;->setVisibility(I)V

    goto :goto_0
.end method
