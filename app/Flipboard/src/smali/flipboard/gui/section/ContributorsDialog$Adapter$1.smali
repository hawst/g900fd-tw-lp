.class Lflipboard/gui/section/ContributorsDialog$Adapter$1;
.super Ljava/lang/Object;
.source "ContributorsDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/section/ContributorsDialog;

.field final synthetic b:Lflipboard/gui/section/ContributorsDialog$Adapter;


# direct methods
.method constructor <init>(Lflipboard/gui/section/ContributorsDialog$Adapter;Lflipboard/gui/section/ContributorsDialog;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$1;->b:Lflipboard/gui/section/ContributorsDialog$Adapter;

    iput-object p2, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$1;->a:Lflipboard/gui/section/ContributorsDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 133
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    .line 134
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 135
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v3, 0x7f0d00c4

    invoke-virtual {v2, v3}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->c:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 136
    const v2, 0x7f0d026a

    invoke-virtual {v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 137
    const v2, 0x7f0d004a

    invoke-virtual {v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 138
    new-instance v2, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;

    invoke-direct {v2, p0, v0}, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;-><init>(Lflipboard/gui/section/ContributorsDialog$Adapter$1;Lflipboard/objs/CommentaryResult$Item$Commentary;)V

    iput-object v2, v1, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 173
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$1;->b:Lflipboard/gui/section/ContributorsDialog$Adapter;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter;->b:Lflipboard/gui/section/ContributorsDialog;

    invoke-virtual {v0}, Lflipboard/gui/section/ContributorsDialog;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "remove_contributor_confirm"

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 174
    return-void
.end method
