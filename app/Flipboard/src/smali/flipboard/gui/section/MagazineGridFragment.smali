.class public Lflipboard/gui/section/MagazineGridFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "MagazineGridFragment.java"


# instance fields
.field private a:Lflipboard/service/Section;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lflipboard/gui/section/MagazineGridFragment;
    .locals 3

    .prologue
    .line 24
    new-instance v0, Lflipboard/gui/section/MagazineGridFragment;

    invoke-direct {v0}, Lflipboard/gui/section/MagazineGridFragment;-><init>()V

    .line 25
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 26
    const-string v2, "extra_section_id"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    invoke-virtual {v0, v1}, Lflipboard/gui/section/MagazineGridFragment;->setArguments(Landroid/os/Bundle;)V

    .line 28
    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 33
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-virtual {p0}, Lflipboard/gui/section/MagazineGridFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {p0}, Lflipboard/gui/section/MagazineGridFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_section_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 36
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->e(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/MagazineGridFragment;->a:Lflipboard/service/Section;

    .line 38
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 42
    new-instance v1, Lflipboard/gui/FLDynamicGridView;

    invoke-virtual {p0}, Lflipboard/gui/section/MagazineGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v1, v0}, Lflipboard/gui/FLDynamicGridView;-><init>(Landroid/content/Context;)V

    .line 43
    new-instance v2, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-direct {v2, v0, v1}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;-><init>(Lflipboard/activities/FlipboardActivity;Lflipboard/gui/FLDynamicGridView;)V

    .line 45
    iget-object v0, p0, Lflipboard/gui/section/MagazineGridFragment;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    .line 46
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 48
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    .line 49
    if-eqz v0, :cond_0

    iget-object v3, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 50
    iget-object v0, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->b:Z

    iget-object v2, v2, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->a:Lflipboard/gui/FLDynamicGridView;

    invoke-virtual {v2, v0}, Lflipboard/gui/FLDynamicGridView;->setItems(Ljava/util/List;)V

    .line 54
    :cond_0
    return-object v1
.end method
