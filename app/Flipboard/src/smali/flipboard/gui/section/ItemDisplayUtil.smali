.class public Lflipboard/gui/section/ItemDisplayUtil;
.super Ljava/lang/Object;
.source "ItemDisplayUtil.java"


# direct methods
.method public static a(ILflipboard/objs/Image;)I
    .locals 2

    .prologue
    .line 335
    int-to-float v0, p0

    invoke-virtual {p1}, Lflipboard/objs/Image;->h()F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static a(Lflipboard/objs/Image;)I
    .locals 1

    .prologue
    .line 342
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v0

    invoke-static {v0, p0}, Lflipboard/gui/section/ItemDisplayUtil;->a(ILflipboard/objs/Image;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 32
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 355
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-nez v2, :cond_1

    .line 357
    :cond_0
    const/4 v2, 0x0

    .line 373
    :goto_0
    return-object v2

    .line 360
    :cond_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-le v2, v3, :cond_2

    .line 368
    const v2, 0x3dcccccd    # 0.1f

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    const v3, 0x3dcccccd    # 0.1f

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static/range {p0 .. p0}, Landroid/renderscript/RenderScript;->create(Landroid/content/Context;)Landroid/renderscript/RenderScript;

    move-result-object v3

    sget-object v4, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_NONE:Landroid/renderscript/Allocation$MipmapControl;

    const/4 v5, 0x1

    invoke-static {v3, v2, v4, v5}, Landroid/renderscript/Allocation;->createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;Landroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    move-result-object v4

    invoke-virtual {v4}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/renderscript/Allocation;->createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;)Landroid/renderscript/Allocation;

    move-result-object v5

    invoke-static {v3}, Landroid/renderscript/Element;->U8_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/renderscript/ScriptIntrinsicBlur;->create(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)Landroid/renderscript/ScriptIntrinsicBlur;

    move-result-object v3

    const/high16 v6, 0x41c80000    # 25.0f

    invoke-virtual {v3, v6}, Landroid/renderscript/ScriptIntrinsicBlur;->setRadius(F)V

    invoke-virtual {v3, v4}, Landroid/renderscript/ScriptIntrinsicBlur;->setInput(Landroid/renderscript/Allocation;)V

    invoke-virtual {v3, v5}, Landroid/renderscript/ScriptIntrinsicBlur;->forEach(Landroid/renderscript/Allocation;)V

    invoke-virtual {v5, v2}, Landroid/renderscript/Allocation;->copyTo(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 370
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    const v3, 0x3dcccccd    # 0.1f

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    const v4, 0x3dcccccd    # 0.1f

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    float-to-int v4, v4

    const/4 v5, 0x1

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    mul-int v3, v5, v9

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v8, v5

    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    add-int/lit8 v21, v5, -0x1

    add-int/lit8 v22, v9, -0x1

    mul-int v4, v5, v9

    new-array v0, v4, [I

    move-object/from16 v23, v0

    new-array v0, v4, [I

    move-object/from16 v24, v0

    new-array v0, v4, [I

    move-object/from16 v25, v0

    invoke-static {v5, v9}, Ljava/lang/Math;->max(II)I

    move-result v4

    new-array v0, v4, [I

    move-object/from16 v26, v0

    const v4, 0x2a400

    new-array v0, v4, [I

    move-object/from16 v27, v0

    const/4 v4, 0x0

    :goto_1
    const v6, 0x2a400

    if-ge v4, v6, :cond_3

    div-int/lit16 v6, v4, 0x2a4

    aput v6, v27, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    const/4 v6, 0x0

    const/16 v4, 0x33

    const/4 v7, 0x3

    filled-new-array {v4, v7}, [I

    move-result-object v4

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v7, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[I

    const/4 v7, 0x0

    move/from16 v19, v6

    move v13, v6

    move/from16 v20, v7

    :goto_2
    move/from16 v0, v20

    if-ge v0, v9, :cond_8

    const/4 v6, 0x0

    const/16 v7, -0x19

    move v8, v6

    move v10, v6

    move v11, v6

    move v12, v6

    move v14, v7

    move/from16 v16, v6

    move/from16 v17, v6

    move/from16 v18, v6

    move v7, v6

    :goto_3
    const/16 v15, 0x19

    if-gt v14, v15, :cond_5

    const/4 v15, 0x0

    invoke-static {v14, v15}, Ljava/lang/Math;->max(II)I

    move-result v15

    move/from16 v0, v21

    invoke-static {v0, v15}, Ljava/lang/Math;->min(II)I

    move-result v15

    add-int/2addr v15, v13

    aget v15, v3, v15

    add-int/lit8 v28, v14, 0x19

    aget-object v28, v4, v28

    const/16 v29, 0x0

    const/high16 v30, 0xff0000

    and-int v30, v30, v15

    shr-int/lit8 v30, v30, 0x10

    aput v30, v28, v29

    const/16 v29, 0x1

    const v30, 0xff00

    and-int v30, v30, v15

    shr-int/lit8 v30, v30, 0x8

    aput v30, v28, v29

    const/16 v29, 0x2

    and-int/lit16 v15, v15, 0xff

    aput v15, v28, v29

    invoke-static {v14}, Ljava/lang/Math;->abs(I)I

    move-result v15

    rsub-int/lit8 v15, v15, 0x1a

    const/16 v29, 0x0

    aget v29, v28, v29

    mul-int v29, v29, v15

    add-int v18, v18, v29

    const/16 v29, 0x1

    aget v29, v28, v29

    mul-int v29, v29, v15

    add-int v17, v17, v29

    const/16 v29, 0x2

    aget v29, v28, v29

    mul-int v15, v15, v29

    add-int v16, v16, v15

    if-lez v14, :cond_4

    const/4 v15, 0x0

    aget v15, v28, v15

    add-int/2addr v8, v15

    const/4 v15, 0x1

    aget v15, v28, v15

    add-int/2addr v7, v15

    const/4 v15, 0x2

    aget v15, v28, v15

    add-int/2addr v6, v15

    :goto_4
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    :cond_4
    const/4 v15, 0x0

    aget v15, v28, v15

    add-int/2addr v12, v15

    const/4 v15, 0x1

    aget v15, v28, v15

    add-int/2addr v11, v15

    const/4 v15, 0x2

    aget v15, v28, v15

    add-int/2addr v10, v15

    goto :goto_4

    :cond_5
    const/16 v14, 0x19

    const/4 v15, 0x0

    :goto_5
    if-ge v15, v5, :cond_7

    aget v28, v27, v18

    aput v28, v23, v13

    aget v28, v27, v17

    aput v28, v24, v13

    aget v28, v27, v16

    aput v28, v25, v13

    sub-int v18, v18, v12

    sub-int v17, v17, v11

    sub-int v16, v16, v10

    add-int/lit8 v28, v14, -0x19

    add-int/lit8 v28, v28, 0x33

    rem-int/lit8 v28, v28, 0x33

    aget-object v28, v4, v28

    const/16 v29, 0x0

    aget v29, v28, v29

    sub-int v12, v12, v29

    const/16 v29, 0x1

    aget v29, v28, v29

    sub-int v11, v11, v29

    const/16 v29, 0x2

    aget v29, v28, v29

    sub-int v10, v10, v29

    if-nez v20, :cond_6

    add-int/lit8 v29, v15, 0x19

    add-int/lit8 v29, v29, 0x1

    move/from16 v0, v29

    move/from16 v1, v21

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v29

    aput v29, v26, v15

    :cond_6
    aget v29, v26, v15

    add-int v29, v29, v19

    aget v29, v3, v29

    const/16 v30, 0x0

    const/high16 v31, 0xff0000

    and-int v31, v31, v29

    shr-int/lit8 v31, v31, 0x10

    aput v31, v28, v30

    const/16 v30, 0x1

    const v31, 0xff00

    and-int v31, v31, v29

    shr-int/lit8 v31, v31, 0x8

    aput v31, v28, v30

    const/16 v30, 0x2

    move/from16 v0, v29

    and-int/lit16 v0, v0, 0xff

    move/from16 v29, v0

    aput v29, v28, v30

    const/16 v29, 0x0

    aget v29, v28, v29

    add-int v8, v8, v29

    const/16 v29, 0x1

    aget v29, v28, v29

    add-int v7, v7, v29

    const/16 v29, 0x2

    aget v28, v28, v29

    add-int v6, v6, v28

    add-int v18, v18, v8

    add-int v17, v17, v7

    add-int v16, v16, v6

    add-int/lit8 v14, v14, 0x1

    rem-int/lit8 v14, v14, 0x33

    rem-int/lit8 v28, v14, 0x33

    aget-object v28, v4, v28

    const/16 v29, 0x0

    aget v29, v28, v29

    add-int v12, v12, v29

    const/16 v29, 0x1

    aget v29, v28, v29

    add-int v11, v11, v29

    const/16 v29, 0x2

    aget v29, v28, v29

    add-int v10, v10, v29

    const/16 v29, 0x0

    aget v29, v28, v29

    sub-int v8, v8, v29

    const/16 v29, 0x1

    aget v29, v28, v29

    sub-int v7, v7, v29

    const/16 v29, 0x2

    aget v28, v28, v29

    sub-int v6, v6, v28

    add-int/lit8 v13, v13, 0x1

    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_5

    :cond_7
    add-int v6, v19, v5

    add-int/lit8 v7, v20, 0x1

    move/from16 v19, v6

    move/from16 v20, v7

    goto/16 :goto_2

    :cond_8
    const/4 v14, 0x0

    :goto_6
    if-ge v14, v5, :cond_e

    const/4 v7, 0x0

    mul-int/lit8 v8, v5, -0x19

    const/16 v6, -0x19

    move v10, v7

    move v11, v7

    move v12, v7

    move v13, v7

    move v15, v6

    move/from16 v16, v7

    move/from16 v17, v7

    move/from16 v18, v7

    move v6, v8

    move v8, v7

    :goto_7
    const/16 v19, 0x19

    move/from16 v0, v19

    if-gt v15, v0, :cond_b

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v19

    add-int v19, v19, v14

    add-int/lit8 v20, v15, 0x19

    aget-object v20, v4, v20

    const/16 v21, 0x0

    aget v28, v23, v19

    aput v28, v20, v21

    const/16 v21, 0x1

    aget v28, v24, v19

    aput v28, v20, v21

    const/16 v21, 0x2

    aget v28, v25, v19

    aput v28, v20, v21

    invoke-static {v15}, Ljava/lang/Math;->abs(I)I

    move-result v21

    rsub-int/lit8 v21, v21, 0x1a

    aget v28, v23, v19

    mul-int v28, v28, v21

    add-int v18, v18, v28

    aget v28, v24, v19

    mul-int v28, v28, v21

    add-int v17, v17, v28

    aget v19, v25, v19

    mul-int v19, v19, v21

    add-int v16, v16, v19

    if-lez v15, :cond_a

    const/16 v19, 0x0

    aget v19, v20, v19

    add-int v10, v10, v19

    const/16 v19, 0x1

    aget v19, v20, v19

    add-int v8, v8, v19

    const/16 v19, 0x2

    aget v19, v20, v19

    add-int v7, v7, v19

    :goto_8
    move/from16 v0, v22

    if-ge v15, v0, :cond_9

    add-int/2addr v6, v5

    :cond_9
    add-int/lit8 v15, v15, 0x1

    goto :goto_7

    :cond_a
    const/16 v19, 0x0

    aget v19, v20, v19

    add-int v13, v13, v19

    const/16 v19, 0x1

    aget v19, v20, v19

    add-int v12, v12, v19

    const/16 v19, 0x2

    aget v19, v20, v19

    add-int v11, v11, v19

    goto :goto_8

    :cond_b
    const/16 v6, 0x19

    const/4 v15, 0x0

    move/from16 v19, v18

    move/from16 v18, v17

    move/from16 v17, v16

    move/from16 v16, v15

    move v15, v6

    move v6, v7

    move v7, v8

    move v8, v10

    move v10, v11

    move v11, v12

    move v12, v13

    move v13, v14

    :goto_9
    move/from16 v0, v16

    if-ge v0, v9, :cond_d

    const/high16 v20, -0x1000000

    aget v21, v3, v13

    and-int v20, v20, v21

    aget v21, v27, v19

    shl-int/lit8 v21, v21, 0x10

    or-int v20, v20, v21

    aget v21, v27, v18

    shl-int/lit8 v21, v21, 0x8

    or-int v20, v20, v21

    aget v21, v27, v17

    or-int v20, v20, v21

    aput v20, v3, v13

    sub-int v19, v19, v12

    sub-int v18, v18, v11

    sub-int v17, v17, v10

    add-int/lit8 v20, v15, -0x19

    add-int/lit8 v20, v20, 0x33

    rem-int/lit8 v20, v20, 0x33

    aget-object v20, v4, v20

    const/16 v21, 0x0

    aget v21, v20, v21

    sub-int v12, v12, v21

    const/16 v21, 0x1

    aget v21, v20, v21

    sub-int v11, v11, v21

    const/16 v21, 0x2

    aget v21, v20, v21

    sub-int v10, v10, v21

    if-nez v14, :cond_c

    add-int/lit8 v21, v16, 0x1a

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->min(II)I

    move-result v21

    mul-int v21, v21, v5

    aput v21, v26, v16

    :cond_c
    aget v21, v26, v16

    add-int v21, v21, v14

    const/16 v28, 0x0

    aget v29, v23, v21

    aput v29, v20, v28

    const/16 v28, 0x1

    aget v29, v24, v21

    aput v29, v20, v28

    const/16 v28, 0x2

    aget v21, v25, v21

    aput v21, v20, v28

    const/16 v21, 0x0

    aget v21, v20, v21

    add-int v8, v8, v21

    const/16 v21, 0x1

    aget v21, v20, v21

    add-int v7, v7, v21

    const/16 v21, 0x2

    aget v20, v20, v21

    add-int v6, v6, v20

    add-int v19, v19, v8

    add-int v18, v18, v7

    add-int v17, v17, v6

    add-int/lit8 v15, v15, 0x1

    rem-int/lit8 v15, v15, 0x33

    aget-object v20, v4, v15

    const/16 v21, 0x0

    aget v21, v20, v21

    add-int v12, v12, v21

    const/16 v21, 0x1

    aget v21, v20, v21

    add-int v11, v11, v21

    const/16 v21, 0x2

    aget v21, v20, v21

    add-int v10, v10, v21

    const/16 v21, 0x0

    aget v21, v20, v21

    sub-int v8, v8, v21

    const/16 v21, 0x1

    aget v21, v20, v21

    sub-int v7, v7, v21

    const/16 v21, 0x2

    aget v20, v20, v21

    sub-int v6, v6, v20

    add-int/2addr v13, v5

    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_9

    :cond_d
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_6

    :cond_e
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v8, v5

    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    goto/16 :goto_0
.end method

.method public static a(Lflipboard/objs/FeedItem;Z)Landroid/text/SpannableString;
    .locals 13

    .prologue
    const v1, 0x7f080074

    const v2, 0x7f080037

    const/16 v12, 0x21

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 253
    invoke-static {p0}, Lflipboard/gui/section/ItemDisplayUtil;->d(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v10

    .line 254
    invoke-static {p0}, Lflipboard/gui/section/ItemDisplayUtil;->c(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v11

    .line 255
    iget-object v6, p0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    .line 256
    if-eqz v10, :cond_3

    move v9, v8

    .line 257
    :goto_0
    if-eqz v11, :cond_4

    move v4, v8

    .line 258
    :goto_1
    if-eqz v6, :cond_5

    move v3, v8

    .line 259
    :goto_2
    if-eqz v4, :cond_6

    if-eqz v3, :cond_6

    move v0, v8

    .line 261
    :goto_3
    if-eqz v0, :cond_b

    invoke-virtual {v6, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    move v6, v5

    move v7, v5

    .line 267
    :goto_4
    if-eqz v9, :cond_9

    .line 268
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v10}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 269
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 271
    if-eqz v4, :cond_a

    .line 272
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v4

    .line 273
    new-instance v11, Landroid/text/style/ForegroundColorSpan;

    if-eqz p1, :cond_7

    move v0, v1

    :goto_5
    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v11, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3, v11, v5, v4, v12}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 274
    new-instance v0, Landroid/text/style/StyleSpan;

    invoke-direct {v0, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v3, v0, v5, v4, v12}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    move v0, v4

    .line 278
    :goto_6
    if-eqz v7, :cond_2

    .line 279
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v4

    .line 280
    if-eqz v6, :cond_0

    .line 282
    add-int/lit8 v0, v0, 0x1

    .line 284
    :cond_0
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    if-eqz p1, :cond_8

    if-eqz v6, :cond_1

    const v1, 0x7f080075

    :cond_1
    :goto_7
    invoke-virtual {v9, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v5, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3, v5, v0, v4, v12}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 285
    if-nez v6, :cond_2

    .line 286
    new-instance v1, Landroid/text/style/StyleSpan;

    invoke-direct {v1, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v3, v1, v0, v4, v12}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    :cond_2
    move-object v0, v3

    .line 292
    :goto_8
    return-object v0

    :cond_3
    move v9, v5

    .line 256
    goto :goto_0

    :cond_4
    move v4, v5

    .line 257
    goto :goto_1

    :cond_5
    move v3, v5

    .line 258
    goto :goto_2

    :cond_6
    move v0, v5

    .line 259
    goto :goto_3

    :cond_7
    move v0, v2

    .line 273
    goto :goto_5

    :cond_8
    move v1, v2

    .line 284
    goto :goto_7

    .line 292
    :cond_9
    const/4 v0, 0x0

    goto :goto_8

    :cond_a
    move v0, v5

    goto :goto_6

    :cond_b
    move v6, v0

    move v7, v3

    goto :goto_4
.end method

.method public static a(Lflipboard/objs/FeedItem;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 58
    iget-object v0, p0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    .line 72
    :cond_0
    :goto_0
    return-object v0

    .line 61
    :cond_1
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_2

    .line 63
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->D()Ljava/lang/String;

    move-result-object v0

    .line 64
    invoke-static {v0}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    :cond_2
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 70
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 72
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 151
    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    .line 152
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v8, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 153
    if-gtz v0, :cond_0

    .line 154
    const/4 v0, 0x0

    .line 171
    :goto_0
    return-object v0

    .line 155
    :cond_0
    if-ne v0, v6, :cond_1

    .line 156
    const v0, 0x7f0d0324

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 158
    :cond_1
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 159
    add-int/lit8 v5, v0, -0x1

    move v1, v2

    .line 160
    :goto_1
    if-ge v1, v5, :cond_3

    .line 161
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 162
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 163
    add-int/lit8 v0, v1, 0x1

    if-ge v0, v5, :cond_2

    .line 164
    const-string v0, ", "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 160
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 167
    :cond_3
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, v8, :cond_4

    .line 168
    const v0, 0x7f0d0292

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 170
    :cond_4
    const-string v0, ", "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 171
    const v0, 0x7f0d0291

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/service/Section;",
            "Lflipboard/objs/FeedItem;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 112
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 113
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->ck:Z

    if-nez v0, :cond_2

    if-eqz p0, :cond_2

    iget-object v0, p0, Lflipboard/service/Section;->G:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 114
    iget-object v0, p0, Lflipboard/service/Section;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    .line 115
    iget-object v3, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->c:Ljava/lang/String;

    .line 116
    const-string v4, "contributor"

    iget-object v5, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v3}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 117
    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v4, v4, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v4, v4, Lflipboard/service/User;->d:Ljava/lang/String;

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->e:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    invoke-interface {v1, v6, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 121
    :cond_1
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 127
    :cond_2
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    .line 128
    invoke-static {v0}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 129
    invoke-interface {v1, v6, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 131
    :cond_3
    return-object v1
.end method

.method public static b(Lflipboard/objs/FeedItem;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 81
    const/4 v0, 0x0

    .line 82
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->J()Lflipboard/objs/Image;

    move-result-object v1

    .line 83
    if-eqz v1, :cond_0

    .line 84
    iget-object v0, v1, Lflipboard/objs/Image;->j:Ljava/lang/String;

    .line 86
    :cond_0
    if-nez v0, :cond_1

    iget-object v1, p0, Lflipboard/objs/FeedItem;->bF:Lflipboard/objs/FeedItem;

    if-eqz v1, :cond_1

    .line 87
    iget-object v0, p0, Lflipboard/objs/FeedItem;->bF:Lflipboard/objs/FeedItem;

    invoke-static {v0}, Lflipboard/gui/section/ItemDisplayUtil;->c(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v0

    .line 89
    :cond_1
    invoke-static {v0}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 90
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0294

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 91
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 93
    :cond_2
    return-object v0
.end method

.method public static c(Lflipboard/objs/FeedItem;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 184
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 185
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 186
    iget-object v1, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    const-string v2, "author"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lflipboard/objs/FeedSectionLink;->h:Ljava/lang/String;

    const-string v2, "user"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    const-string v2, "cdn.flipboard.com"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 187
    iget-object v0, v0, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    .line 214
    :cond_0
    :goto_0
    return-object v0

    .line 191
    :cond_1
    iget-object v0, p0, Lflipboard/objs/FeedItem;->av:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/objs/FeedItem;->av:Ljava/lang/String;

    const-string v1, "cdn.flipboard.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 192
    iget-object v0, p0, Lflipboard/objs/FeedItem;->av:Ljava/lang/String;

    goto :goto_0

    .line 195
    :cond_2
    iget-object v0, p0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 197
    iget-object v0, p0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/HttpUtil;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199
    if-eqz v0, :cond_4

    .line 201
    const-string v1, "www."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 202
    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 208
    :cond_3
    :goto_1
    const-string v1, "cdn.flipboard.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 203
    :cond_5
    const-string v1, "api."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 204
    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 205
    :cond_6
    const-string v1, "m."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 206
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static d(Lflipboard/objs/FeedItem;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 223
    invoke-static {p0}, Lflipboard/gui/section/ItemDisplayUtil;->c(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v1

    .line 224
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    .line 225
    if-eqz v1, :cond_2

    move v5, v4

    .line 226
    :goto_0
    if-eqz v0, :cond_3

    move v2, v4

    .line 228
    :goto_1
    if-eqz v5, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v2, v3

    .line 233
    :cond_0
    if-eqz v2, :cond_4

    .line 234
    if-eqz v5, :cond_1

    .line 235
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v2}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0d0258

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 238
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v3

    aput-object v0, v5, v4

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 244
    :cond_1
    :goto_2
    return-object v0

    :cond_2
    move v5, v3

    .line 225
    goto :goto_0

    :cond_3
    move v2, v3

    .line 226
    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 241
    goto :goto_2
.end method

.method public static e(Lflipboard/objs/FeedItem;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 301
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v1

    .line 302
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    .line 303
    invoke-static {p0}, Lflipboard/gui/section/ItemDisplayUtil;->f(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v2

    .line 304
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 306
    :cond_0
    const/4 v0, 0x0

    .line 308
    :cond_1
    if-eqz v0, :cond_3

    .line 309
    if-eqz v1, :cond_2

    .line 310
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " \u2022 "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 315
    :cond_2
    :goto_0
    return-object v0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public static f(Lflipboard/objs/FeedItem;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 321
    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "status"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v0

    .line 328
    :goto_0
    return-object v0

    .line 323
    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    iget-object v0, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 324
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    iget-object v0, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    goto :goto_0

    .line 326
    :cond_1
    invoke-static {p0}, Lflipboard/gui/section/ItemDisplayUtil;->c(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
