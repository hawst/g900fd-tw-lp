.class public Lflipboard/gui/section/SectionFragmentScrolling$$ViewInjector;
.super Ljava/lang/Object;
.source "SectionFragmentScrolling$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/SectionFragmentScrolling;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a028c

    const-string v1, "field \'swipeRefreshLayout\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/swiperefresh/SwipeRefreshLayout;

    iput-object v0, p1, Lflipboard/gui/section/SectionFragmentScrolling;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    .line 12
    const v0, 0x7f0a02f2

    const-string v1, "field \'viewPager\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/PullToRefreshVerticalViewPager;

    iput-object v0, p1, Lflipboard/gui/section/SectionFragmentScrolling;->b:Lflipboard/gui/PullToRefreshVerticalViewPager;

    .line 14
    return-void
.end method

.method public static reset(Lflipboard/gui/section/SectionFragmentScrolling;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    .line 18
    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->b:Lflipboard/gui/PullToRefreshVerticalViewPager;

    .line 19
    return-void
.end method
