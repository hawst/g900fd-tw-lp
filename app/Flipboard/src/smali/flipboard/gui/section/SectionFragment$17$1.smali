.class Lflipboard/gui/section/SectionFragment$17$1;
.super Ljava/lang/Object;
.source "SectionFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Ljava/util/LinkedList;

.field final synthetic c:Lflipboard/gui/section/SectionFragment$17;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment$17;Ljava/util/List;Ljava/util/LinkedList;)V
    .locals 0

    .prologue
    .line 2425
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iput-object p2, p0, Lflipboard/gui/section/SectionFragment$17$1;->a:Ljava/util/List;

    iput-object p3, p0, Lflipboard/gui/section/SectionFragment$17$1;->b:Ljava/util/LinkedList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lflipboard/gui/flipping/FlipTransitionViews;ILflipboard/gui/section/SectionPage;)V
    .locals 2

    .prologue
    .line 2567
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 2568
    invoke-virtual {p3}, Lflipboard/gui/section/SectionPage;->g()V

    .line 2570
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->b(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/SectionScrubber;

    move-result-object v0

    invoke-virtual {p3, v0}, Lflipboard/gui/section/SectionPage;->setScrubber(Lflipboard/gui/section/SectionScrubber;)V

    .line 2571
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-boolean v0, v0, Lflipboard/gui/section/SectionFragment;->C:Z

    invoke-virtual {p3, v0}, Lflipboard/gui/section/SectionPage;->setIsOpenedFromThirdParty(Z)V

    .line 2572
    invoke-virtual {p3}, Lflipboard/gui/section/SectionPage;->f()V

    .line 2573
    invoke-virtual {p3}, Lflipboard/gui/section/SectionPage;->h()V

    .line 2574
    invoke-virtual {p1, p2, p3}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    move-result-object v0

    .line 2575
    iget-object v1, p3, Lflipboard/gui/section/SectionPage;->w:Lflipboard/gui/section/Group;

    invoke-virtual {v1}, Lflipboard/gui/section/Group;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2576
    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/gui/flipping/FlippingContainer;->k:Z

    .line 2578
    :cond_1
    return-void
.end method

.method private a(Lflipboard/gui/section/Group;)V
    .locals 4

    .prologue
    .line 2548
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    const-string v0, "SectionFragment:removeItemsForGroup"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 2549
    iget-object v0, p1, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v0, v0, Lflipboard/gui/section/GroupFranchiseMeta;->a:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_2

    .line 2550
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->h:Ljava/util/List;

    iget-object v1, p1, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v1, v1, Lflipboard/gui/section/GroupFranchiseMeta;->a:Lflipboard/objs/FeedItem;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2560
    :cond_0
    iget-object v0, p1, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    if-eqz v0, :cond_1

    .line 2561
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->m:Lflipboard/gui/section/ListSingleThreadWrapper;

    iget-object v1, p1, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->remove(Ljava/lang/Object;)Z

    .line 2563
    :cond_1
    return-void

    .line 2552
    :cond_2
    iget-object v0, p1, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 2553
    const-string v2, "list"

    iget-object v3, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2554
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment$17;->h:Ljava/util/List;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 2556
    :cond_3
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment$17;->h:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2428
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-boolean v0, v0, Lflipboard/gui/section/SectionFragment$17;->d:Z

    if-eqz v0, :cond_1

    move v0, v2

    .line 2429
    :goto_0
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    const-string v3, "synthetic-client-profile-page"

    invoke-static {v1, v3}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/gui/section/SectionFragment;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-boolean v1, v1, Lflipboard/gui/section/SectionFragment$17;->d:Z

    if-eqz v1, :cond_13

    .line 2430
    const/4 v0, 0x1

    move v1, v0

    .line 2432
    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->e()Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v4

    .line 2433
    if-eqz v4, :cond_12

    .line 2435
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 2437
    invoke-virtual {v4}, Lflipboard/gui/flipping/FlipTransitionViews;->getRunningFlips()I

    move-result v0

    if-lez v0, :cond_5

    .line 2438
    invoke-virtual {v4}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    invoke-virtual {v4}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v0, v3, :cond_3

    .line 2439
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/SectionPage;

    .line 2440
    invoke-direct {p0, v4, v1, v0}, Lflipboard/gui/section/SectionFragment$17$1;->a(Lflipboard/gui/flipping/FlipTransitionViews;ILflipboard/gui/section/SectionPage;)V

    .line 2441
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    .line 2442
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v2, v1, v0}, Lflipboard/gui/section/SectionFragment;->a(ILflipboard/gui/section/Group;)V

    .line 2443
    invoke-direct {p0, v0}, Lflipboard/gui/section/SectionFragment$17$1;->a(Lflipboard/gui/section/Group;)V

    .line 2444
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionFragment;->c(I)V

    .line 2446
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-boolean v0, v0, Lflipboard/gui/section/SectionFragment$17;->d:Z

    if-eqz v0, :cond_0

    .line 2447
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->p(Lflipboard/gui/section/SectionFragment;)I

    .line 2448
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragment;->l(Lflipboard/gui/section/SectionFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionFragment;->d(I)V

    .line 2452
    :cond_0
    invoke-virtual {v4}, Lflipboard/gui/flipping/FlipTransitionViews;->getFlippableViews()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    .line 2453
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlippingContainer;->a()V

    goto :goto_2

    .line 2428
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v0

    goto/16 :goto_0

    .line 2455
    :cond_2
    const/16 v0, 0xc8

    invoke-virtual {v4, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->e(I)V

    .line 2457
    :cond_3
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3, p0}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    .line 2544
    :cond_4
    :goto_3
    return-void

    .line 2465
    :cond_5
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->q(Lflipboard/gui/section/SectionFragment;)V

    .line 2468
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v1

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    .line 2469
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17$1;->b:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/section/SectionPage;

    .line 2470
    invoke-direct {p0, v4, v3, v1}, Lflipboard/gui/section/SectionFragment$17$1;->a(Lflipboard/gui/flipping/FlipTransitionViews;ILflipboard/gui/section/SectionPage;)V

    .line 2471
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v1, v3, v0}, Lflipboard/gui/section/SectionFragment;->a(ILflipboard/gui/section/Group;)V

    .line 2472
    invoke-direct {p0, v0}, Lflipboard/gui/section/SectionFragment$17$1;->a(Lflipboard/gui/section/Group;)V

    .line 2473
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    .line 2474
    goto :goto_4

    .line 2476
    :cond_6
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionFragment;->c(I)V

    .line 2479
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-boolean v0, v0, Lflipboard/gui/section/SectionFragment$17;->d:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->C()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->l(Lflipboard/gui/section/SectionFragment;)I

    move-result v0

    if-gt v3, v0, :cond_8

    .line 2480
    :cond_7
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragment;->l(Lflipboard/gui/section/SectionFragment;)I

    move-result v1

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$17$1;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v1, v3

    invoke-static {v0, v1}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/gui/section/SectionFragment;I)I

    .line 2481
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragment;->l(Lflipboard/gui/section/SectionFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionFragment;->d(I)V

    .line 2485
    :cond_8
    invoke-virtual {v4}, Lflipboard/gui/flipping/FlipTransitionViews;->getFlippableViews()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    .line 2486
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlippingContainer;->a()V

    goto :goto_5

    .line 2488
    :cond_9
    invoke-virtual {v4, v2}, Lflipboard/gui/flipping/FlipTransitionViews;->e(I)V

    .line 2490
    :cond_a
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->n(Lflipboard/gui/section/SectionFragment;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_b

    .line 2491
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Oops: just done updating, but according to the boolean we weren\'t updating at all."

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2493
    :cond_b
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    const-string v1, "adding pages"

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionFragment;->b(Ljava/lang/String;)V

    .line 2494
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-boolean v0, v0, Lflipboard/gui/section/SectionFragment$17;->f:Z

    if-eqz v0, :cond_c

    .line 2495
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-boolean v0, v0, Lflipboard/gui/section/SectionFragment$17;->g:Z

    if-eqz v0, :cond_10

    .line 2496
    invoke-virtual {v4}, Lflipboard/gui/flipping/FlipTransitionViews;->g()V

    .line 2500
    :goto_6
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0, v2}, Lflipboard/gui/section/SectionFragment;->a(I)V

    .line 2505
    :cond_c
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragment;->j(Lflipboard/gui/section/SectionFragment;)Lflipboard/service/FLAdManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/service/FLAdManager;)V

    .line 2507
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->c()V

    .line 2509
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "* SectionFragment.addPages ungroupedItems size"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v3}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", prependingItems.size "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment;->g:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v3}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", groupedItems.size "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v3}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " precedingGroups.size "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment$17;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2511
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-boolean v0, v0, Lflipboard/service/Section;->F:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->r(Lflipboard/gui/section/SectionFragment;)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->k()Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 2512
    :cond_d
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "* SectionFragment.addPages endPage = true, actionRefresh "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-boolean v3, v3, Lflipboard/service/Section;->F:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", hasNewContentAtStart "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v3}, Lflipboard/gui/section/SectionFragment;->r(Lflipboard/gui/section/SectionFragment;)Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", section.isEOF() "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v3}, Lflipboard/service/Section;->k()Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", ungroupedItems.isEmpty() "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v3}, Lflipboard/gui/section/ListSingleThreadWrapper;->isEmpty()Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2513
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getFlippableViews()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 2514
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getFlippableViews()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getFlippableViews()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlippingContainer;->getChild()Landroid/view/View;

    move-result-object v0

    .line 2516
    if-eqz v0, :cond_e

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->s(Lflipboard/gui/section/SectionFragment;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 2517
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->t(Lflipboard/gui/section/SectionFragment;)Z

    .line 2518
    new-instance v0, Lflipboard/objs/FeedItem;

    invoke-direct {v0}, Lflipboard/objs/FeedItem;-><init>()V

    .line 2519
    const-string v1, "synthetic-action-refresh"

    iput-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 2520
    const-string v1, "synthetic-action-refresh"

    iput-object v1, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    .line 2521
    new-instance v1, Lflipboard/gui/section/Group;

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-static {}, Lflipboard/gui/section/SectionFragment;->k()Lflipboard/objs/SectionPageTemplate;

    move-result-object v4

    invoke-direct {v1, v3, v4, v0, v2}, Lflipboard/gui/section/Group;-><init>(Lflipboard/service/Section;Lflipboard/objs/SectionPageTemplate;Lflipboard/objs/FeedItem;Z)V

    .line 2522
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/gui/section/Group;)V

    .line 2523
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v2}, Lflipboard/gui/flipping/FlipTransitionViews;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/gui/section/SectionFragment;Landroid/content/Context;Lflipboard/gui/section/Group;)Lflipboard/gui/section/SectionPage;

    move-result-object v0

    .line 2524
    invoke-virtual {v0}, Lflipboard/gui/section/SectionPage;->f()V

    .line 2525
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragment;->b(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/SectionScrubber;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionPage;->setScrubber(Lflipboard/gui/section/SectionScrubber;)V

    .line 2526
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    .line 2533
    :cond_e
    :goto_7
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->c(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/LoadingPage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2534
    const-string v0, "profile"

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2535
    :cond_f
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->c(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/LoadingPage;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/section/LoadingPage;->a()V

    .line 2536
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "SectionFragment.addPages runOnUIThread() loadingPage.setNoContent() was called\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    goto/16 :goto_3

    .line 2498
    :cond_10
    invoke-virtual {v4, v2}, Lflipboard/gui/flipping/FlipTransitionViews;->setCurrentViewIndex(I)V

    goto/16 :goto_6

    .line 2530
    :cond_11
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "* SectionFragment.addPages endPage = false, actionRefresh "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-boolean v2, v2, Lflipboard/service/Section;->F:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", hasNewContentAtStart "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v2}, Lflipboard/gui/section/SectionFragment;->r(Lflipboard/gui/section/SectionFragment;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", section.isEOF() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->k()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ungroupedItems.isEmpty() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v2}, Lflipboard/gui/section/ListSingleThreadWrapper;->isEmpty()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    goto/16 :goto_7

    .line 2540
    :cond_12
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 2541
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "Flipper was null when trying to add newly created pages"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2542
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17$1;->c:Lflipboard/gui/section/SectionFragment$17;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->n(Lflipboard/gui/section/SectionFragment;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto/16 :goto_3

    :cond_13
    move v1, v0

    goto/16 :goto_1
.end method
