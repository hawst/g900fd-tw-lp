.class Lflipboard/gui/section/SectionFragment$11;
.super Ljava/lang/Object;
.source "SectionFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:I

.field final synthetic c:I

.field final synthetic d:Lflipboard/gui/section/SectionFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment;Ljava/util/List;II)V
    .locals 0

    .prologue
    .line 1441
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$11;->d:Lflipboard/gui/section/SectionFragment;

    iput-object p2, p0, Lflipboard/gui/section/SectionFragment$11;->a:Ljava/util/List;

    iput p3, p0, Lflipboard/gui/section/SectionFragment$11;->b:I

    iput p4, p0, Lflipboard/gui/section/SectionFragment$11;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 1444
    new-instance v7, Ljava/util/ArrayList;

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$11;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1445
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$11;->d:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 1446
    if-nez v1, :cond_0

    .line 1501
    :goto_0
    return-void

    .line 1451
    :cond_0
    const/4 v0, 0x0

    move v6, v0

    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$11;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_3

    .line 1452
    iget v0, p0, Lflipboard/gui/section/SectionFragment$11;->b:I

    if-eq v6, v0, :cond_1

    .line 1453
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$11;->a:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/gui/section/Group;

    .line 1454
    instance-of v0, v2, Lflipboard/gui/section/Group$AdGroup;

    if-eqz v0, :cond_2

    move-object v0, v2

    .line 1455
    check-cast v0, Lflipboard/gui/section/Group$AdGroup;

    iget-object v8, v0, Lflipboard/gui/section/Group$AdGroup;->l:Lflipboard/service/FLAdManager$AdAsset;

    .line 1456
    iget-object v0, v8, Lflipboard/service/FLAdManager$AdAsset;->b:Lflipboard/objs/Ad$Asset;

    if-eqz v0, :cond_1

    .line 1457
    new-instance v0, Lflipboard/gui/section/SectionAdPage;

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$11;->d:Lflipboard/gui/section/SectionFragment;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v4, v8, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget-object v5, p0, Lflipboard/gui/section/SectionFragment$11;->d:Lflipboard/gui/section/SectionFragment;

    iget-boolean v5, v5, Lflipboard/gui/section/SectionFragment;->D:Z

    invoke-direct/range {v0 .. v5}, Lflipboard/gui/section/SectionAdPage;-><init>(Landroid/content/Context;Lflipboard/gui/section/Group;Lflipboard/service/Section;Lflipboard/objs/Ad;Z)V

    .line 1458
    const v2, 0x7f03009b

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/item/AdItem;

    .line 1459
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/section/SectionFragment$11;->d:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v4}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lflipboard/util/AndroidUtil;->a(Landroid/app/Activity;)I

    move-result v4

    invoke-virtual {v2, v8, v3, v4}, Lflipboard/gui/item/AdItem;->a(Lflipboard/service/FLAdManager$AdAsset;II)V

    .line 1460
    invoke-virtual {v0, v2}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    .line 1461
    const/4 v2, 0x1

    iput-boolean v2, v0, Lflipboard/gui/section/SectionPage;->p:Z

    .line 1462
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1451
    :cond_1
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 1465
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$11;->d:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/section/SectionFragment;->a(Landroid/content/Context;Lflipboard/gui/section/Group;)Lflipboard/gui/section/SectionPage;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1470
    :cond_3
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/SectionFragment$11$1;

    invoke-direct {v1, p0, v7}, Lflipboard/gui/section/SectionFragment$11$1;-><init>(Lflipboard/gui/section/SectionFragment$11;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
