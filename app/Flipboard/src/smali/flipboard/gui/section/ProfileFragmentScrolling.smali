.class public Lflipboard/gui/section/ProfileFragmentScrolling;
.super Lflipboard/activities/FlipboardFragment;
.source "ProfileFragmentScrolling.java"

# interfaces
.implements Lflipboard/gui/section/scrolling/component/MetricBarComponent$OnMetricClickListener;


# instance fields
.field protected a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

.field private b:Landroid/widget/FrameLayout;

.field private c:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

.field private d:Lflipboard/gui/section/ProfileFragmentScrolling$SectionsAndAccountObserver;

.field private e:Lflipboard/gui/section/ProfileFragmentScrolling$UserObserver;

.field private f:Lflipboard/gui/section/ProfileFragmentScrolling$AccountLoginViewClickHandler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 42
    new-instance v0, Lflipboard/gui/section/ProfileFragmentScrolling$SectionsAndAccountObserver;

    invoke-direct {v0, p0, v1}, Lflipboard/gui/section/ProfileFragmentScrolling$SectionsAndAccountObserver;-><init>(Lflipboard/gui/section/ProfileFragmentScrolling;B)V

    iput-object v0, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->d:Lflipboard/gui/section/ProfileFragmentScrolling$SectionsAndAccountObserver;

    .line 43
    new-instance v0, Lflipboard/gui/section/ProfileFragmentScrolling$UserObserver;

    invoke-direct {v0, p0, v1}, Lflipboard/gui/section/ProfileFragmentScrolling$UserObserver;-><init>(Lflipboard/gui/section/ProfileFragmentScrolling;B)V

    iput-object v0, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->e:Lflipboard/gui/section/ProfileFragmentScrolling$UserObserver;

    .line 190
    return-void
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 66
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v4, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 67
    const-string v0, "flipboard"

    invoke-virtual {v4, v0}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v5

    .line 68
    if-eqz v5, :cond_1

    const/4 v0, 0x1

    .line 71
    :goto_0
    if-eqz v0, :cond_2

    .line 72
    const v0, 0x7f030135

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 73
    const v0, 0x7f030137

    invoke-virtual {p1, v0, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 74
    invoke-static {p0, v6}, Lbutterknife/ButterKnife;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 76
    const v0, 0x7f0a0349

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLDynamicGridView;

    .line 77
    new-instance v7, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lflipboard/activities/FlipboardActivity;

    invoke-direct {v7, v1, v0}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;-><init>(Lflipboard/activities/FlipboardActivity;Lflipboard/gui/FLDynamicGridView;)V

    iput-object v7, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->c:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    .line 78
    iget-object v0, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->c:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    iget-object v0, v0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->a:Lflipboard/gui/FLDynamicGridView;

    invoke-virtual {v0, v6}, Lflipboard/gui/FLDynamicGridView;->setHeaderView(Landroid/view/View;)V

    .line 79
    iput-object v8, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->f:Lflipboard/gui/section/ProfileFragmentScrolling$AccountLoginViewClickHandler;

    move-object v0, v2

    .line 87
    :goto_1
    iget-object v1, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    invoke-virtual {v1, p0}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->setOnMetricClickListener(Lflipboard/gui/section/scrolling/component/MetricBarComponent$OnMetricClickListener;)V

    .line 88
    iget-object v1, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    invoke-virtual {v1, v3}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->setShowActionBar(Z)V

    .line 89
    iget-object v1, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iget-object v2, v4, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-virtual {v1, v5, v2}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a(Lflipboard/service/Account;Ljava/lang/String;)V

    .line 91
    iget-object v1, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->c:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    if-eqz v1, :cond_0

    .line 92
    iget-object v1, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->c:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-virtual {v4}, Lflipboard/service/User;->q()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->a(Ljava/util/List;)V

    .line 95
    :cond_0
    return-object v0

    :cond_1
    move v0, v3

    .line 68
    goto :goto_0

    .line 81
    :cond_2
    const v0, 0x7f030136

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 82
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 83
    new-instance v1, Lflipboard/gui/section/ProfileFragmentScrolling$AccountLoginViewClickHandler;

    invoke-direct {v1, p0}, Lflipboard/gui/section/ProfileFragmentScrolling$AccountLoginViewClickHandler;-><init>(Lflipboard/gui/section/ProfileFragmentScrolling;)V

    iput-object v1, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->f:Lflipboard/gui/section/ProfileFragmentScrolling$AccountLoginViewClickHandler;

    .line 84
    iget-object v1, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->f:Lflipboard/gui/section/ProfileFragmentScrolling$AccountLoginViewClickHandler;

    invoke-static {v1, v0}, Lbutterknife/ButterKnife;->a(Ljava/lang/Object;Landroid/view/View;)V

    goto :goto_1
.end method

.method static synthetic a(Lflipboard/gui/section/ProfileFragmentScrolling;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lflipboard/gui/section/ProfileFragmentScrolling;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lflipboard/gui/section/ProfileFragmentScrolling;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->b:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public static a()Lflipboard/gui/section/ProfileFragmentScrolling;
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lflipboard/gui/section/ProfileFragmentScrolling;

    invoke-direct {v0}, Lflipboard/gui/section/ProfileFragmentScrolling;-><init>()V

    return-object v0
.end method

.method static synthetic b(Lflipboard/gui/section/ProfileFragmentScrolling;)V
    .locals 2

    .prologue
    .line 33
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/ProfileFragmentScrolling$1;

    invoke-direct {v1, p0}, Lflipboard/gui/section/ProfileFragmentScrolling$1;-><init>(Lflipboard/gui/section/ProfileFragmentScrolling;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic c(Lflipboard/gui/section/ProfileFragmentScrolling;)Lflipboard/gui/section/scrolling/component/MagazineGridComponent;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->c:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 121
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 122
    invoke-virtual {v2}, Lflipboard/service/User;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 123
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 137
    :cond_1
    :goto_1
    return-void

    .line 123
    :sswitch_0
    const-string v3, "articles"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v3, "magazineCount"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v3, "follower"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 125
    :pswitch_0
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-static {v0}, Lflipboard/util/ActivityUtil;->a(Lflipboard/activities/FlipboardActivity;)V

    goto :goto_1

    .line 128
    :pswitch_1
    iget-object v0, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->c:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    if-eqz v0, :cond_1

    .line 129
    iget-object v0, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->c:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    iget-object v2, v0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->a:Lflipboard/gui/FLDynamicGridView;

    iget-object v0, v2, Lflipboard/gui/FLDynamicGridView;->a:Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;

    invoke-static {v0}, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->a(Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v2, v0, v1}, Lflipboard/gui/FLDynamicGridView;->smoothScrollToPositionFromTop(II)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x3

    goto :goto_2

    .line 133
    :pswitch_2
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-object v1, v2, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lflipboard/util/ActivityUtil;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 123
    :sswitch_data_0
    .sparse-switch
        -0x60eb2e25 -> :sswitch_1
        -0x493f2dc3 -> :sswitch_0
        0x11fd201e -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 53
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->d:Lflipboard/gui/section/ProfileFragmentScrolling$SectionsAndAccountObserver;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/util/Observer;)V

    .line 54
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->e:Lflipboard/gui/section/ProfileFragmentScrolling$UserObserver;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Lflipboard/util/Observer;)V

    .line 55
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lflipboard/gui/section/ProfileFragmentScrolling;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->b:Landroid/widget/FrameLayout;

    .line 61
    iget-object v0, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->b:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->b:Landroid/widget/FrameLayout;

    invoke-direct {p0, p1, v1}, Lflipboard/gui/section/ProfileFragmentScrolling;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 62
    iget-object v0, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->b:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 114
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onDestroy()V

    .line 115
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->d:Lflipboard/gui/section/ProfileFragmentScrolling$SectionsAndAccountObserver;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Lflipboard/util/Observer;)V

    .line 116
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->e:Lflipboard/gui/section/ProfileFragmentScrolling$UserObserver;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->c(Lflipboard/util/Observer;)V

    .line 117
    return-void
.end method
