.class public Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;
.super Landroid/widget/BaseAdapter;
.source "MagazineConversationThreadCommentsAdapter.java"


# instance fields
.field public a:Z

.field private b:Lflipboard/activities/GenericFragmentActivity;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/CommentaryResult$Item$Commentary;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lflipboard/activities/GenericFragmentActivity;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/activities/GenericFragmentActivity;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/CommentaryResult$Item$Commentary;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->a:Z

    .line 43
    iput-object p1, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->b:Lflipboard/activities/GenericFragmentActivity;

    .line 44
    iput-object p2, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->c:Ljava/util/List;

    .line 45
    iput-object p3, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->d:Ljava/lang/String;

    .line 46
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;)Lflipboard/activities/GenericFragmentActivity;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->b:Lflipboard/activities/GenericFragmentActivity;

    return-object v0
.end method

.method private a(I)Lflipboard/objs/CommentaryResult$Item$Commentary;
    .locals 2

    .prologue
    .line 64
    const/4 v0, 0x0

    .line 65
    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->c:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 66
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    .line 68
    :cond_0
    return-object v0
.end method

.method static synthetic a(Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;Lflipboard/objs/CommentaryResult$Item$Commentary;)V
    .locals 4

    .prologue
    .line 34
    new-instance v0, Lflipboard/objs/FeedItem;

    invoke-direct {v0}, Lflipboard/objs/FeedItem;-><init>()V

    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->d:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedItem;->ci:Ljava/lang/String;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->h:Ljava/lang/String;

    new-instance v3, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$2;

    invoke-direct {v3, p0, p1}, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$2;-><init>(Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;Lflipboard/objs/CommentaryResult$Item$Commentary;)V

    invoke-virtual {v1, v0, v2, v3}, Lflipboard/service/FlipboardManager;->a(Lflipboard/objs/FeedItem;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V

    return-void
.end method

.method static synthetic b(Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;)Ljava/util/List;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->c:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 50
    const/4 v0, 0x0

    .line 51
    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->c:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 52
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 54
    :cond_0
    iget-boolean v1, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->a:Z

    if-eqz v1, :cond_1

    .line 57
    add-int/lit8 v0, v0, 0x1

    .line 59
    :cond_1
    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->a(I)Lflipboard/objs/CommentaryResult$Item$Commentary;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 73
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 93
    const/4 v0, 0x1

    .line 94
    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->c:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 96
    const/4 v0, 0x0

    .line 98
    :cond_0
    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/16 v4, 0x8

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 103
    invoke-virtual {p0, p1}, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_3

    .line 106
    invoke-direct {p0, p1}, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->a(I)Lflipboard/objs/CommentaryResult$Item$Commentary;

    move-result-object v2

    .line 109
    if-nez p2, :cond_2

    .line 110
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300d3

    invoke-static {v0, v1, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 112
    new-instance v1, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$Holder;

    invoke-direct {v1, p0, v8}, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$Holder;-><init>(Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;B)V

    .line 113
    const v0, 0x7f0a0266

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$Holder;->a:Lflipboard/gui/FLImageView;

    .line 114
    const v0, 0x7f0a004f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$Holder;->b:Lflipboard/gui/FLTextIntf;

    .line 115
    const v0, 0x7f0a0268

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$Holder;->c:Lflipboard/gui/FLTextIntf;

    .line 116
    const v0, 0x7f0a0270

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$Holder;->d:Lflipboard/gui/FLTextIntf;

    .line 117
    const v0, 0x7f0a0267

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$Holder;->e:Lflipboard/gui/FLTextIntf;

    .line 118
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 124
    :goto_0
    if-eqz v2, :cond_1

    .line 125
    iput p1, v1, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$Holder;->f:I

    .line 126
    iget-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$Holder;->b:Lflipboard/gui/FLTextIntf;

    iget-object v3, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->c:Ljava/lang/String;

    invoke-interface {v0, v3}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$Holder;->a:Lflipboard/gui/FLImageView;

    iget-object v3, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->g:Lflipboard/objs/Image;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 128
    iget-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$Holder;->c:Lflipboard/gui/FLTextIntf;

    iget-object v3, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->j:Ljava/lang/String;

    invoke-interface {v0, v3}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$Holder;->c:Lflipboard/gui/FLTextIntf;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 131
    iget-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$Holder;->d:Lflipboard/gui/FLTextIntf;

    invoke-interface {v0, v4}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 133
    iget-wide v4, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->k:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    .line 134
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->b:Lflipboard/activities/GenericFragmentActivity;

    invoke-static {v0, v4, v5}, Lflipboard/util/JavaUtil;->b(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    .line 136
    iget-object v3, v1, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$Holder;->e:Lflipboard/gui/FLTextIntf;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " ago"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v3, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->j:Ljava/lang/String;

    .line 139
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 141
    iget-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$Holder;->c:Lflipboard/gui/FLTextIntf;

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->o:Ljava/util/List;

    invoke-static {v0, v3, v1, v4}, Lflipboard/util/SocialHelper;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/List;Landroid/os/Bundle;)V

    .line 143
    new-instance v1, Lflipboard/gui/actionbar/FLActionBarMenu;

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->b:Lflipboard/activities/GenericFragmentActivity;

    invoke-direct {v1, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0a026f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0, v8, v8}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    sget-object v3, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->d:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    invoke-virtual {v0, v3}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    iget-object v3, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->b:Lflipboard/activities/GenericFragmentActivity;

    invoke-virtual {v3}, Lflipboard/activities/GenericFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09007b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->b:Lflipboard/activities/GenericFragmentActivity;

    invoke-virtual {v4}, Lflipboard/activities/GenericFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09007d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v0, v3, v4, v3, v3}, Lflipboard/gui/actionbar/FLActionBar;->a(IIII)V

    iget-object v3, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->b:Lflipboard/activities/GenericFragmentActivity;

    invoke-virtual {v0, v3, v9}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/gui/dialog/FLDialogFragment;)V

    iget-boolean v3, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->p:Z

    if-eqz v3, :cond_0

    invoke-virtual {v0, v8}, Lflipboard/gui/actionbar/FLActionBar;->setVisibility(I)V

    const v3, 0x7f0d0019

    invoke-virtual {v1, v3}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v3

    new-instance v4, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$1;

    invoke-direct {v4, p0, v2}, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$1;-><init>(Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;Lflipboard/objs/CommentaryResult$Item$Commentary;)V

    iput-object v4, v3, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    const/4 v2, 0x1

    iput-boolean v2, v3, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    invoke-virtual {v3, v8}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    :cond_0
    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->setMenu(Lflipboard/gui/actionbar/FLActionBarMenu;)V

    .line 151
    :cond_1
    :goto_1
    return-object p2

    .line 120
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$Holder;

    .line 121
    iget-object v1, v0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter$Holder;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->a()V

    move-object v1, v0

    goto/16 :goto_0

    .line 145
    :cond_3
    if-nez p2, :cond_1

    .line 146
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030050

    invoke-static {v0, v1, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 147
    const v0, 0x7f0a012d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 148
    const-string v0, "loading"

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 78
    .line 79
    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->c:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 80
    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    const/4 v0, 0x1

    .line 82
    :cond_0
    return v0
.end method
