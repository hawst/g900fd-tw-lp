.class public Lflipboard/gui/section/SectionScrubber;
.super Landroid/view/ViewGroup;
.source "SectionScrubber.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field public a:Lflipboard/gui/FLLabelTextView;

.field public b:Lflipboard/gui/FLLabelTextView;

.field public c:Landroid/widget/SeekBar;

.field public d:Landroid/view/View;

.field public e:Landroid/view/View;

.field f:I

.field public g:I

.field h:Landroid/graphics/Paint;

.field private i:Lflipboard/gui/FLBusyView;

.field private j:Lflipboard/gui/section/SectionScrubber$ScrubberListener;

.field private k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 38
    const/4 v0, 0x5

    iput v0, p0, Lflipboard/gui/section/SectionScrubber;->f:I

    .line 42
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/SectionScrubber;->h:Landroid/graphics/Paint;

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    const/4 v0, 0x5

    iput v0, p0, Lflipboard/gui/section/SectionScrubber;->f:I

    .line 42
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/SectionScrubber;->h:Landroid/graphics/Paint;

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    const/4 v0, 0x5

    iput v0, p0, Lflipboard/gui/section/SectionScrubber;->f:I

    .line 42
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/SectionScrubber;->h:Landroid/graphics/Paint;

    .line 57
    return-void
.end method

.method private static a(Landroid/view/View;II)[I
    .locals 4

    .prologue
    .line 170
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int v0, p1, v0

    div-int/lit8 v0, v0, 0x2

    sub-int v0, p2, v0

    .line 171
    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v0, v3

    aput v3, v1, v2

    const/4 v2, 0x1

    aput v0, v1, v2

    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 75
    iget-object v0, p0, Lflipboard/gui/section/SectionScrubber;->a:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 76
    iget-object v0, p0, Lflipboard/gui/section/SectionScrubber;->a:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 77
    iget-object v0, p0, Lflipboard/gui/section/SectionScrubber;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 79
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 305
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->a:Lflipboard/service/audio/FLAudioManager;

    if-eqz v0, :cond_0

    .line 306
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    sget-object v1, Lflipboard/service/audio/FLAudioManager;->a:Lflipboard/service/audio/FLAudioManager;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    sget-object v2, Lflipboard/service/audio/FLAudioManager;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v2}, Lflipboard/service/audio/FLAudioManager;->i()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 308
    :cond_0
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->a:Lflipboard/service/audio/FLAudioManager;

    if-eqz v0, :cond_1

    sget-object v0, Lflipboard/service/audio/FLAudioManager;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 309
    iget-object v0, p0, Lflipboard/gui/section/SectionScrubber;->e:Landroid/view/View;

    invoke-static {v0, v3}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 313
    :goto_0
    return-void

    .line 311
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionScrubber;->e:Landroid/view/View;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public getMeasuredChildHeight()I
    .locals 1

    .prologue
    .line 283
    iget v0, p0, Lflipboard/gui/section/SectionScrubber;->k:I

    return v0
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 63
    const v0, 0x7f0a02f4

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionScrubber;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/section/SectionScrubber;->a:Lflipboard/gui/FLLabelTextView;

    .line 64
    const v0, 0x7f0a02f6

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionScrubber;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/section/SectionScrubber;->b:Lflipboard/gui/FLLabelTextView;

    .line 65
    const v0, 0x7f0a02f5

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionScrubber;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lflipboard/gui/section/SectionScrubber;->c:Landroid/widget/SeekBar;

    .line 66
    iget-object v0, p0, Lflipboard/gui/section/SectionScrubber;->c:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 67
    iget-object v0, p0, Lflipboard/gui/section/SectionScrubber;->c:Landroid/widget/SeekBar;

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 68
    const v0, 0x7f0a02f7

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionScrubber;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLBusyView;

    iput-object v0, p0, Lflipboard/gui/section/SectionScrubber;->i:Lflipboard/gui/FLBusyView;

    .line 69
    const v0, 0x7f0a02f8

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionScrubber;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/SectionScrubber;->d:Landroid/view/View;

    .line 70
    const v0, 0x7f0a02f3

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionScrubber;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/SectionScrubber;->e:Landroid/view/View;

    .line 71
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    .line 129
    sub-int v2, p4, p2

    .line 130
    sub-int v3, p5, p3

    .line 131
    const/4 v1, 0x0

    invoke-virtual {p0}, Lflipboard/gui/section/SectionScrubber;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionScrubber;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 132
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionScrubber;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iget-object v4, p0, Lflipboard/gui/section/SectionScrubber;->i:Lflipboard/gui/FLBusyView;

    invoke-virtual {v4}, Lflipboard/gui/FLBusyView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v0, v4

    .line 135
    iget-object v4, p0, Lflipboard/gui/section/SectionScrubber;->e:Landroid/view/View;

    invoke-static {v4, v1, v3}, Lflipboard/gui/section/SectionScrubber;->a(Landroid/view/View;II)[I

    move-result-object v4

    .line 136
    iget-object v5, p0, Lflipboard/gui/section/SectionScrubber;->e:Landroid/view/View;

    const/4 v6, 0x0

    aget v6, v4, v6

    iget-object v7, p0, Lflipboard/gui/section/SectionScrubber;->e:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, p2

    const/4 v8, 0x1

    aget v4, v4, v8

    invoke-virtual {v5, p2, v6, v7, v4}, Landroid/view/View;->layout(IIII)V

    .line 138
    mul-int/lit8 v4, v0, 0x2

    sub-int/2addr v2, v4

    .line 139
    iget-object v4, p0, Lflipboard/gui/section/SectionScrubber;->a:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v4

    iget-object v5, p0, Lflipboard/gui/section/SectionScrubber;->c:Landroid/widget/SeekBar;

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lflipboard/gui/section/SectionScrubber;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {p0}, Lflipboard/gui/section/SectionScrubber;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09007b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    .line 141
    add-int/2addr v0, p2

    sub-int/2addr v2, v4

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 142
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->a:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v2

    const/16 v4, 0x8

    if-eq v2, v4, :cond_1

    .line 143
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->a:Lflipboard/gui/FLLabelTextView;

    invoke-static {v2, v1, v3}, Lflipboard/gui/section/SectionScrubber;->a(Landroid/view/View;II)[I

    move-result-object v2

    .line 144
    iget-object v4, p0, Lflipboard/gui/section/SectionScrubber;->a:Lflipboard/gui/FLLabelTextView;

    const/4 v5, 0x0

    aget v5, v2, v5

    iget-object v6, p0, Lflipboard/gui/section/SectionScrubber;->a:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v0

    const/4 v7, 0x1

    aget v2, v2, v7

    invoke-virtual {v4, v0, v5, v6, v2}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 147
    :cond_1
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->a:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Lflipboard/gui/section/SectionScrubber;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09007b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 148
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->c:Landroid/widget/SeekBar;

    invoke-static {v2, v1, v3}, Lflipboard/gui/section/SectionScrubber;->a(Landroid/view/View;II)[I

    move-result-object v2

    .line 149
    iget-object v4, p0, Lflipboard/gui/section/SectionScrubber;->c:Landroid/widget/SeekBar;

    const/4 v5, 0x0

    aget v5, v2, v5

    iget-object v6, p0, Lflipboard/gui/section/SectionScrubber;->c:Landroid/widget/SeekBar;

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v0

    const/4 v7, 0x1

    aget v2, v2, v7

    invoke-virtual {v4, v0, v5, v6, v2}, Landroid/widget/SeekBar;->layout(IIII)V

    .line 151
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->c:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Lflipboard/gui/section/SectionScrubber;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09007b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 152
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v2

    const/16 v4, 0x8

    if-eq v2, v4, :cond_2

    .line 153
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->b:Lflipboard/gui/FLLabelTextView;

    invoke-static {v2, v1, v3}, Lflipboard/gui/section/SectionScrubber;->a(Landroid/view/View;II)[I

    move-result-object v2

    .line 154
    iget-object v4, p0, Lflipboard/gui/section/SectionScrubber;->b:Lflipboard/gui/FLLabelTextView;

    const/4 v5, 0x0

    aget v5, v2, v5

    iget-object v6, p0, Lflipboard/gui/section/SectionScrubber;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v0

    const/4 v7, 0x1

    aget v2, v2, v7

    invoke-virtual {v4, v0, v5, v6, v2}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 157
    :cond_2
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Lflipboard/gui/section/SectionScrubber;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09007d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 158
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->i:Lflipboard/gui/FLBusyView;

    invoke-static {v2, v1, v3}, Lflipboard/gui/section/SectionScrubber;->a(Landroid/view/View;II)[I

    move-result-object v2

    .line 159
    iget-object v4, p0, Lflipboard/gui/section/SectionScrubber;->i:Lflipboard/gui/FLBusyView;

    const/4 v5, 0x0

    aget v5, v2, v5

    iget-object v6, p0, Lflipboard/gui/section/SectionScrubber;->i:Lflipboard/gui/FLBusyView;

    invoke-virtual {v6}, Lflipboard/gui/FLBusyView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v0

    const/4 v7, 0x1

    aget v2, v2, v7

    invoke-virtual {v4, v0, v5, v6, v2}, Lflipboard/gui/FLBusyView;->layout(IIII)V

    .line 161
    iget-object v0, p0, Lflipboard/gui/section/SectionScrubber;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_3

    .line 163
    iget-object v0, p0, Lflipboard/gui/section/SectionScrubber;->d:Landroid/view/View;

    invoke-static {v0, v1, v3}, Lflipboard/gui/section/SectionScrubber;->a(Landroid/view/View;II)[I

    move-result-object v0

    .line 164
    iget-object v1, p0, Lflipboard/gui/section/SectionScrubber;->d:Landroid/view/View;

    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int v2, p4, v2

    const/4 v3, 0x0

    aget v3, v0, v3

    const/4 v4, 0x1

    aget v0, v0, v4

    invoke-virtual {v1, v2, v3, p4, v0}, Landroid/view/View;->layout(IIII)V

    .line 166
    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 84
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 85
    iput v1, p0, Lflipboard/gui/section/SectionScrubber;->k:I

    .line 87
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 88
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->a:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v2

    if-eq v2, v3, :cond_0

    .line 89
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->a:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2, v1, v1}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 90
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2, v1, v1}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 92
    :cond_0
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eq v2, v3, :cond_1

    .line 93
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->d:Landroid/view/View;

    invoke-virtual {v2, v1, v1}, Landroid/view/View;->measure(II)V

    .line 95
    :cond_1
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 96
    iget-object v3, p0, Lflipboard/gui/section/SectionScrubber;->i:Lflipboard/gui/FLBusyView;

    invoke-virtual {v3, v2, v2}, Lflipboard/gui/FLBusyView;->measure(II)V

    .line 97
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lflipboard/gui/section/SectionScrubber;->i:Lflipboard/gui/FLBusyView;

    invoke-virtual {v3}, Lflipboard/gui/FLBusyView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v2, v3

    .line 98
    iget-object v3, p0, Lflipboard/gui/section/SectionScrubber;->e:Landroid/view/View;

    invoke-virtual {v3, v1, v1}, Landroid/view/View;->measure(II)V

    .line 100
    iget-object v1, p0, Lflipboard/gui/section/SectionScrubber;->i:Lflipboard/gui/FLBusyView;

    invoke-virtual {v1}, Lflipboard/gui/FLBusyView;->getMeasuredHeight()I

    move-result v1

    iget-object v3, p0, Lflipboard/gui/section/SectionScrubber;->d:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lflipboard/gui/section/SectionScrubber;->k:I

    .line 101
    iget v1, p0, Lflipboard/gui/section/SectionScrubber;->k:I

    iget-object v3, p0, Lflipboard/gui/section/SectionScrubber;->e:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lflipboard/gui/section/SectionScrubber;->k:I

    .line 102
    iget v1, p0, Lflipboard/gui/section/SectionScrubber;->k:I

    iget-object v3, p0, Lflipboard/gui/section/SectionScrubber;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lflipboard/gui/section/SectionScrubber;->k:I

    .line 104
    invoke-virtual {p0}, Lflipboard/gui/section/SectionScrubber;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f09007b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v2

    .line 105
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->a:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v2

    sub-int v2, v0, v2

    iget-object v3, p0, Lflipboard/gui/section/SectionScrubber;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    mul-int/lit8 v1, v1, 0x2

    sub-int v1, v2, v1

    invoke-virtual {p0}, Lflipboard/gui/section/SectionScrubber;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09007d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 106
    iget-object v2, p0, Lflipboard/gui/section/SectionScrubber;->c:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 107
    iget v3, p0, Lflipboard/gui/section/SectionScrubber;->k:I

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, p0, Lflipboard/gui/section/SectionScrubber;->k:I

    .line 109
    invoke-virtual {p0}, Lflipboard/gui/section/SectionScrubber;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0201eb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v3

    .line 110
    iget v4, p0, Lflipboard/gui/section/SectionScrubber;->f:I

    mul-int/2addr v3, v4

    .line 111
    iget-object v4, p0, Lflipboard/gui/section/SectionScrubber;->c:Landroid/widget/SeekBar;

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v4, v1, v2}, Landroid/widget/SeekBar;->measure(II)V

    .line 112
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/section/SectionScrubber;->setMeasuredDimension(II)V

    .line 113
    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4

    .prologue
    .line 271
    if-eqz p3, :cond_0

    .line 272
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iget v2, p0, Lflipboard/gui/section/SectionScrubber;->f:I

    add-int/lit8 v2, v2, -0x1

    int-to-double v2, v2

    mul-double/2addr v0, v2

    const-wide v2, 0x40c3880000000000L    # 10000.0

    div-double/2addr v0, v2

    int-to-double v2, p2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-double v0, v0

    .line 273
    double-to-int v0, v0

    .line 274
    iget-object v1, p0, Lflipboard/gui/section/SectionScrubber;->j:Lflipboard/gui/section/SectionScrubber$ScrubberListener;

    if-eqz v1, :cond_0

    iget v1, p0, Lflipboard/gui/section/SectionScrubber;->g:I

    if-eq v0, v1, :cond_0

    .line 275
    iput v0, p0, Lflipboard/gui/section/SectionScrubber;->g:I

    .line 276
    iget-object v0, p0, Lflipboard/gui/section/SectionScrubber;->j:Lflipboard/gui/section/SectionScrubber$ScrubberListener;

    iget v1, p0, Lflipboard/gui/section/SectionScrubber;->g:I

    invoke-interface {v0, v1}, Lflipboard/gui/section/SectionScrubber$ScrubberListener;->b(I)V

    .line 279
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 289
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lflipboard/gui/section/SectionScrubber;->j:Lflipboard/gui/section/SectionScrubber$ScrubberListener;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lflipboard/gui/section/SectionScrubber;->j:Lflipboard/gui/section/SectionScrubber$ScrubberListener;

    invoke-interface {v0}, Lflipboard/gui/section/SectionScrubber$ScrubberListener;->f()V

    .line 296
    :cond_0
    return-void
.end method

.method public setLoading(Z)V
    .locals 2

    .prologue
    .line 206
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "SectionScrubber:setLoading"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 207
    if-eqz p1, :cond_0

    .line 208
    iget-object v0, p0, Lflipboard/gui/section/SectionScrubber;->i:Lflipboard/gui/FLBusyView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBusyView;->setVisibility(I)V

    .line 212
    :goto_0
    return-void

    .line 210
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionScrubber;->i:Lflipboard/gui/FLBusyView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBusyView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setNumberOfPages(I)V
    .locals 1

    .prologue
    .line 182
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "SectionScrubber:setNumberOfPages"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 183
    iget v0, p0, Lflipboard/gui/section/SectionScrubber;->f:I

    if-eq p1, v0, :cond_0

    .line 184
    iput p1, p0, Lflipboard/gui/section/SectionScrubber;->f:I

    .line 185
    iget-object v0, p0, Lflipboard/gui/section/SectionScrubber;->c:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->requestLayout()V

    .line 186
    iget v0, p0, Lflipboard/gui/section/SectionScrubber;->g:I

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionScrubber;->setPosition$2563266(I)V

    .line 188
    :cond_0
    return-void
.end method

.method public final setPosition$2563266(I)V
    .locals 2

    .prologue
    .line 197
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "SectionScrubber:setPosition"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 198
    const/high16 v0, 0x3f800000    # 1.0f

    int-to-float v1, p1

    mul-float/2addr v0, v1

    iget v1, p0, Lflipboard/gui/section/SectionScrubber;->f:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lflipboard/gui/section/SectionScrubber;->c:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    .line 200
    iget-object v1, p0, Lflipboard/gui/section/SectionScrubber;->c:Landroid/widget/SeekBar;

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 201
    iput p1, p0, Lflipboard/gui/section/SectionScrubber;->g:I

    .line 202
    return-void
.end method

.method public setScrubberListener(Lflipboard/gui/section/SectionScrubber$ScrubberListener;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lflipboard/gui/section/SectionScrubber;->j:Lflipboard/gui/section/SectionScrubber$ScrubberListener;

    .line 301
    return-void
.end method
