.class public Lflipboard/gui/section/UserListFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "UserListFragment.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/activities/FlipboardFragment;",
        "Lflipboard/service/Flap$JSONResultObserver;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/User;",
        "Lflipboard/service/User$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lflipboard/gui/section/UserListFragment$UserListAdapter;

.field final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/objs/FeedSectionLink;",
            ">;"
        }
    .end annotation
.end field

.field c:Z

.field d:Landroid/content/ContentResolver;

.field e:Landroid/database/Cursor;

.field private f:Ljava/lang/String;

.field private g:Lflipboard/service/Flap$FollowListType;

.field private h:Ljava/lang/String;

.field private i:Lflipboard/objs/UsageEventV2$FollowFrom;

.field private m:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;

.field private n:Landroid/widget/AbsListView$OnScrollListener;

.field private o:Landroid/widget/AdapterView$OnItemClickListener;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/TextView;

.field private r:Ljava/lang/String;

.field private s:Z

.field private t:Z

.field private u:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/UserListFragment;->b:Ljava/util/ArrayList;

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/UserListFragment;->c:Z

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/section/UserListFragment;->u:Z

    .line 469
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/UserListFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lflipboard/gui/section/UserListFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lflipboard/gui/section/UserListFragment;->r:Ljava/lang/String;

    return-object p1
.end method

.method public static a(Lflipboard/service/Flap$FollowListType;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 211
    const/4 v0, 0x0

    .line 212
    sget-object v1, Lflipboard/service/Flap$FollowListType;->a:Lflipboard/service/Flap$FollowListType;

    if-ne p0, v1, :cond_0

    .line 213
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v1, 0x7f0d0157

    invoke-virtual {v0, v1}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 221
    :goto_0
    return-object v0

    .line 214
    :cond_0
    sget-object v1, Lflipboard/service/Flap$FollowListType;->b:Lflipboard/service/Flap$FollowListType;

    if-ne p0, v1, :cond_1

    .line 215
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v1, 0x7f0d0159

    invoke-virtual {v0, v1}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 216
    :cond_1
    sget-object v1, Lflipboard/service/Flap$FollowListType;->c:Lflipboard/service/Flap$FollowListType;

    if-ne p0, v1, :cond_2

    .line 217
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v1, 0x7f0d00ee

    invoke-virtual {v0, v1}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 219
    :cond_2
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v2, "Missing UserListFragment title for type %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lflipboard/service/Flap$FollowListType;->name()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/gui/section/UserListFragment;Lflipboard/gui/section/UserListFragment$Holder;Lflipboard/objs/FeedSectionLink;)V
    .locals 2

    .prologue
    .line 56
    iput-object p2, p1, Lflipboard/gui/section/UserListFragment$Holder;->a:Lflipboard/objs/FeedSectionLink;

    iget-object v0, p1, Lflipboard/gui/section/UserListFragment$Holder;->b:Lflipboard/gui/FLTextIntf;

    iget-object v1, p2, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lflipboard/gui/section/UserListFragment$Holder;->c:Lflipboard/gui/FLTextIntf;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    iget-object v0, p1, Lflipboard/gui/section/UserListFragment$Holder;->c:Lflipboard/gui/FLTextIntf;

    iget-object v1, p2, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p1, Lflipboard/gui/section/UserListFragment$Holder;->f:Lflipboard/gui/FLImageView;

    iget-object v1, p2, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    iget-object v0, p1, Lflipboard/gui/section/UserListFragment$Holder;->e:Lflipboard/gui/FollowButton;

    invoke-virtual {v0, p2}, Lflipboard/gui/FollowButton;->setSectionLink(Lflipboard/objs/FeedSectionLink;)V

    iget-object v0, p1, Lflipboard/gui/section/UserListFragment$Holder;->e:Lflipboard/gui/FollowButton;

    iget-object v1, p0, Lflipboard/gui/section/UserListFragment;->i:Lflipboard/objs/UsageEventV2$FollowFrom;

    invoke-virtual {v0, v1}, Lflipboard/gui/FollowButton;->setFrom(Lflipboard/objs/UsageEventV2$FollowFrom;)V

    return-void

    :cond_0
    iget-object v0, p1, Lflipboard/gui/section/UserListFragment$Holder;->c:Lflipboard/gui/FLTextIntf;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedSectionLink;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 746
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->H:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->j:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 747
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->o:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 748
    if-eqz p1, :cond_0

    .line 749
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->B:Lflipboard/objs/UsageEventV2$CommonEventData;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 750
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->F:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 754
    :goto_0
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 755
    iput-boolean v3, p0, Lflipboard/gui/section/UserListFragment;->u:Z

    .line 756
    return-void

    .line 752
    :cond_0
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->B:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/gui/section/UserListFragment;Z)Z
    .locals 0

    .prologue
    .line 56
    iput-boolean p1, p0, Lflipboard/gui/section/UserListFragment;->c:Z

    return p1
.end method

.method static synthetic b(Lflipboard/gui/section/UserListFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 523
    iget-boolean v0, p0, Lflipboard/gui/section/UserListFragment;->t:Z

    if-eqz v0, :cond_2

    .line 524
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 525
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->p:Landroid/widget/TextView;

    const v1, 0x7f0d00eb

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 529
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 602
    :cond_0
    :goto_1
    return-void

    .line 527
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->p:Landroid/widget/TextView;

    const v1, 0x7f0d00ea

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 531
    :cond_2
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/gui/section/UserListFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 532
    iget-object v3, p0, Lflipboard/gui/section/UserListFragment;->g:Lflipboard/service/Flap$FollowListType;

    sget-object v4, Lflipboard/service/Flap$FollowListType;->c:Lflipboard/service/Flap$FollowListType;

    if-ne v3, v4, :cond_7

    .line 533
    if-eqz v0, :cond_0

    .line 534
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "flipboard"

    iget-object v3, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 535
    :goto_2
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v4, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lflipboard/service/User;->c(Ljava/lang/String;)Z

    move-result v3

    .line 536
    if-eqz v0, :cond_4

    if-nez v3, :cond_4

    move v3, v1

    .line 537
    :goto_3
    if-eqz v3, :cond_5

    .line 539
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    .line 540
    iget-object v3, p0, Lflipboard/gui/section/UserListFragment;->p:Landroid/widget/TextView;

    const v4, 0x7f0d014f

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-virtual {p0, v4, v1}, Lflipboard/gui/section/UserListFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 541
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 542
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->q:Landroid/widget/TextView;

    const v1, 0x7f0d01f1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 543
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->q:Landroid/widget/TextView;

    new-instance v1, Lflipboard/gui/section/UserListFragment$4;

    invoke-direct {v1, p0}, Lflipboard/gui/section/UserListFragment$4;-><init>(Lflipboard/gui/section/UserListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_3
    move v0, v2

    .line 534
    goto :goto_2

    :cond_4
    move v3, v2

    .line 536
    goto :goto_3

    .line 552
    :cond_5
    if-nez v0, :cond_6

    .line 554
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->p:Landroid/widget/TextView;

    const v1, 0x7f0d0145

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 555
    invoke-direct {p0}, Lflipboard/gui/section/UserListFragment;->c()V

    goto/16 :goto_1

    .line 558
    :cond_6
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    .line 559
    iget-object v3, p0, Lflipboard/gui/section/UserListFragment;->p:Landroid/widget/TextView;

    sget-object v4, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v5, 0x7f0d0146

    invoke-virtual {v4, v5}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v4, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 560
    invoke-direct {p0}, Lflipboard/gui/section/UserListFragment;->c()V

    goto/16 :goto_1

    .line 565
    :cond_7
    iget-object v1, p0, Lflipboard/gui/section/UserListFragment;->g:Lflipboard/service/Flap$FollowListType;

    sget-object v3, Lflipboard/service/Flap$FollowListType;->d:Lflipboard/service/Flap$FollowListType;

    if-ne v1, v3, :cond_8

    .line 566
    if-eqz v0, :cond_0

    .line 567
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->p:Landroid/widget/TextView;

    const v1, 0x7f0d0147

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 568
    invoke-direct {p0}, Lflipboard/gui/section/UserListFragment;->c()V

    goto/16 :goto_1

    .line 572
    :cond_8
    iget-object v1, p0, Lflipboard/gui/section/UserListFragment;->g:Lflipboard/service/Flap$FollowListType;

    sget-object v3, Lflipboard/service/Flap$FollowListType;->b:Lflipboard/service/Flap$FollowListType;

    if-ne v1, v3, :cond_a

    .line 573
    if-eqz v0, :cond_9

    .line 574
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->p:Landroid/widget/TextView;

    const v1, 0x7f0d0151

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 575
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 576
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->q:Landroid/widget/TextView;

    const v1, 0x7f0d00ef

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 577
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->q:Landroid/widget/TextView;

    new-instance v1, Lflipboard/gui/section/UserListFragment$5;

    invoke-direct {v1, p0}, Lflipboard/gui/section/UserListFragment$5;-><init>(Lflipboard/gui/section/UserListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 589
    :cond_9
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->p:Landroid/widget/TextView;

    const v1, 0x7f0d0154

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 590
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 592
    :cond_a
    iget-object v1, p0, Lflipboard/gui/section/UserListFragment;->g:Lflipboard/service/Flap$FollowListType;

    sget-object v2, Lflipboard/service/Flap$FollowListType;->a:Lflipboard/service/Flap$FollowListType;

    if-ne v1, v2, :cond_0

    .line 593
    if-eqz v0, :cond_b

    .line 594
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->p:Landroid/widget/TextView;

    const v1, 0x7f0d0150

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 595
    invoke-direct {p0}, Lflipboard/gui/section/UserListFragment;->c()V

    goto/16 :goto_1

    .line 597
    :cond_b
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->p:Landroid/widget/TextView;

    const v1, 0x7f0d0153

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 598
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method private c()V
    .locals 2

    .prologue
    .line 605
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->q:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 606
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->q:Landroid/widget/TextView;

    const v1, 0x7f0d014e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 607
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->q:Landroid/widget/TextView;

    new-instance v1, Lflipboard/gui/section/UserListFragment$6;

    invoke-direct {v1, p0}, Lflipboard/gui/section/UserListFragment$6;-><init>(Lflipboard/gui/section/UserListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 614
    return-void
.end method

.method static synthetic c(Lflipboard/gui/section/UserListFragment;)Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lflipboard/gui/section/UserListFragment;->c:Z

    return v0
.end method

.method static synthetic d(Lflipboard/gui/section/UserListFragment;)Lflipboard/service/Flap$FollowListType;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->g:Lflipboard/service/Flap$FollowListType;

    return-object v0
.end method

.method static synthetic e(Lflipboard/gui/section/UserListFragment;)Lflipboard/activities/FlipboardActivity;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    return-object v0
.end method

.method static synthetic f(Lflipboard/gui/section/UserListFragment;)Z
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/section/UserListFragment;->s:Z

    return v0
.end method

.method static synthetic g(Lflipboard/gui/section/UserListFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->r:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lflipboard/gui/section/UserListFragment;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lflipboard/gui/section/UserListFragment;->b()V

    return-void
.end method

.method static synthetic i(Lflipboard/gui/section/UserListFragment;)Z
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/UserListFragment;->t:Z

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 226
    iget-boolean v0, p0, Lflipboard/gui/section/UserListFragment;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/gui/section/UserListFragment;->s:Z

    if-nez v0, :cond_0

    .line 227
    iput-boolean v4, p0, Lflipboard/gui/section/UserListFragment;->s:Z

    .line 228
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->g:Lflipboard/service/Flap$FollowListType;

    sget-object v1, Lflipboard/service/Flap$FollowListType;->d:Lflipboard/service/Flap$FollowListType;

    if-ne v0, v1, :cond_1

    .line 229
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "email_query"

    new-instance v2, Lflipboard/gui/section/UserListFragment$3;

    invoke-direct {v2, p0}, Lflipboard/gui/section/UserListFragment$3;-><init>(Lflipboard/gui/section/UserListFragment;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 243
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 244
    :cond_2
    :goto_1
    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/section/UserListFragment;->f:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/gui/section/UserListFragment;->r:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/gui/section/UserListFragment;->g:Lflipboard/service/Flap$FollowListType;

    iget-object v5, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    new-instance v0, Lflipboard/service/Flap$FollowListRequest;

    sget-object v7, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v7, v7, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-direct {v0, v6, v7}, Lflipboard/service/Flap$FollowListRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    move-object v6, p0

    invoke-static/range {v0 .. v6}, Lflipboard/service/Flap$FollowListRequest;->a(Lflipboard/service/Flap$FollowListRequest;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$FollowListType;ZLjava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V

    goto :goto_0

    .line 243
    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public final a(Lflipboard/json/FLObject;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 621
    .line 623
    :try_start_0
    new-instance v1, Lflipboard/json/JSONParser;

    invoke-virtual {p1}, Lflipboard/json/FLObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lflipboard/json/JSONParser;->z()Lflipboard/objs/UserListResult;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 629
    :goto_0
    if-eqz v1, :cond_0

    .line 630
    iget-object v0, v1, Lflipboard/objs/UserListResult;->a:Ljava/util/List;

    .line 635
    :cond_0
    iget-boolean v2, p0, Lflipboard/gui/section/UserListFragment;->u:Z

    if-eqz v2, :cond_1

    .line 636
    invoke-direct {p0, v0}, Lflipboard/gui/section/UserListFragment;->a(Ljava/util/List;)V

    .line 639
    :cond_1
    iget-object v1, v1, Lflipboard/objs/UserListResult;->i:Ljava/lang/String;

    .line 640
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v3, Lflipboard/gui/section/UserListFragment$7;

    invoke-direct {v3, p0, v0, v1}, Lflipboard/gui/section/UserListFragment$7;-><init>(Lflipboard/gui/section/UserListFragment;Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    .line 665
    return-void

    .line 625
    :catch_0
    move-exception v1

    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v2, "Unable to parse UserListResult %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v0

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 56
    check-cast p2, Lflipboard/service/User$Message;

    sget-object v0, Lflipboard/service/User$Message;->g:Lflipboard/service/User$Message;

    invoke-virtual {p2, v0}, Lflipboard/service/User$Message;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p3, :cond_1

    check-cast p3, Lflipboard/service/Account;

    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    iget-object v1, p3, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/UserListFragment$9;

    invoke-direct {v1, p0}, Lflipboard/gui/section/UserListFragment$9;-><init>(Lflipboard/gui/section/UserListFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lflipboard/service/User$Message;->l:Lflipboard/service/User$Message;

    invoke-virtual {p2, v0}, Lflipboard/service/User$Message;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p3, Lflipboard/service/Section;

    if-eqz p3, :cond_0

    move v1, v2

    move v3, v2

    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    iget-object v4, v0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    iget-object v5, p3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v5, v5, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p3}, Lflipboard/service/Section;->s()Z

    move-result v3

    iput-boolean v3, v0, Lflipboard/objs/FeedSectionLink;->o:Z

    const/4 v3, 0x1

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    if-nez v3, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->g:Lflipboard/service/Flap$FollowListType;

    sget-object v1, Lflipboard/service/Flap$FollowListType;->b:Lflipboard/service/Flap$FollowListType;

    if-ne v0, v1, :cond_0

    invoke-virtual {p3}, Lflipboard/service/Section;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->f:Ljava/lang/String;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lflipboard/objs/FeedSectionLink;

    const-string v1, "user"

    invoke-direct {v0, p3, v1}, Lflipboard/objs/FeedSectionLink;-><init>(Lflipboard/service/Section;Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/gui/section/UserListFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->a:Lflipboard/gui/section/UserListFragment$UserListAdapter;

    invoke-virtual {v0}, Lflipboard/gui/section/UserListFragment$UserListAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 668
    iget-boolean v0, p0, Lflipboard/gui/section/UserListFragment;->u:Z

    if-eqz v0, :cond_0

    .line 669
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/gui/section/UserListFragment;->a(Ljava/util/List;)V

    .line 671
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/UserListFragment$8;

    invoke-direct {v1, p0}, Lflipboard/gui/section/UserListFragment$8;-><init>(Lflipboard/gui/section/UserListFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    .line 684
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 735
    if-nez p1, :cond_0

    .line 736
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/UserListFragment;->u:Z

    .line 738
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FlipboardFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 739
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 96
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FlipboardFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 98
    invoke-virtual {p0}, Lflipboard/gui/section/UserListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/UserListFragment;->f:Ljava/lang/String;

    .line 99
    invoke-virtual {p0}, Lflipboard/gui/section/UserListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "listType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/service/Flap$FollowListType;->valueOf(Ljava/lang/String;)Lflipboard/service/Flap$FollowListType;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/UserListFragment;->g:Lflipboard/service/Flap$FollowListType;

    .line 100
    invoke-virtual {p0}, Lflipboard/gui/section/UserListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "serviceId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    .line 102
    sget-object v0, Lflipboard/gui/section/UserListFragment$10;->a:[I

    iget-object v1, p0, Lflipboard/gui/section/UserListFragment;->g:Lflipboard/service/Flap$FollowListType;

    invoke-virtual {v1}, Lflipboard/service/Flap$FollowListType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 117
    :goto_0
    const v0, 0x7f03014e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 118
    const v1, 0x7f0a0386

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;

    iput-object v1, p0, Lflipboard/gui/section/UserListFragment;->m:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;

    .line 120
    iget-object v1, p0, Lflipboard/gui/section/UserListFragment;->m:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;

    invoke-virtual {v1, v4}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->setDrawingListUnderStickyHeader(Z)V

    .line 121
    const v1, 0x7f0a0387

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 122
    const v2, 0x7f0a0388

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lflipboard/gui/section/UserListFragment;->p:Landroid/widget/TextView;

    .line 123
    iget-object v2, p0, Lflipboard/gui/section/UserListFragment;->p:Landroid/widget/TextView;

    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v6, "regular"

    invoke-virtual {v5, v6}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 124
    const v2, 0x7f0a0389

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lflipboard/gui/section/UserListFragment;->q:Landroid/widget/TextView;

    .line 125
    iget-object v2, p0, Lflipboard/gui/section/UserListFragment;->m:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;

    invoke-virtual {v2, v1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->setEmptyView(Landroid/view/View;)V

    .line 127
    iget-object v1, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    invoke-static {v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v3

    .line 128
    :goto_1
    iget-object v2, p0, Lflipboard/gui/section/UserListFragment;->g:Lflipboard/service/Flap$FollowListType;

    sget-object v5, Lflipboard/service/Flap$FollowListType;->c:Lflipboard/service/Flap$FollowListType;

    if-ne v2, v5, :cond_0

    .line 129
    if-eqz v1, :cond_3

    const-string v2, "flipboard"

    iget-object v5, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v3

    .line 130
    :goto_2
    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v5, v5, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v6, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lflipboard/service/User;->c(Ljava/lang/String;)Z

    move-result v5

    .line 131
    if-eqz v2, :cond_4

    if-nez v5, :cond_4

    move v2, v3

    .line 132
    :goto_3
    if-eqz v2, :cond_0

    .line 133
    iput-boolean v4, p0, Lflipboard/gui/section/UserListFragment;->c:Z

    .line 137
    :cond_0
    if-eqz v1, :cond_1

    .line 138
    iget-object v1, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    sget-object v2, Lflipboard/service/Flap$FollowListType;->d:Lflipboard/service/Flap$FollowListType;

    invoke-virtual {v2}, Lflipboard/service/Flap$FollowListType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 141
    sget-object v1, Lflipboard/objs/UsageEventV2$FollowFrom;->j:Lflipboard/objs/UsageEventV2$FollowFrom;

    iput-object v1, p0, Lflipboard/gui/section/UserListFragment;->i:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 163
    :cond_1
    :goto_4
    new-instance v1, Lflipboard/gui/section/UserListFragment$1;

    invoke-direct {v1, p0}, Lflipboard/gui/section/UserListFragment$1;-><init>(Lflipboard/gui/section/UserListFragment;)V

    iput-object v1, p0, Lflipboard/gui/section/UserListFragment;->n:Landroid/widget/AbsListView$OnScrollListener;

    .line 177
    new-instance v1, Lflipboard/gui/section/UserListFragment$2;

    invoke-direct {v1, p0}, Lflipboard/gui/section/UserListFragment$2;-><init>(Lflipboard/gui/section/UserListFragment;)V

    iput-object v1, p0, Lflipboard/gui/section/UserListFragment;->o:Landroid/widget/AdapterView$OnItemClickListener;

    .line 189
    new-instance v1, Lflipboard/gui/section/UserListFragment$UserListAdapter;

    invoke-direct {v1, p0, v4}, Lflipboard/gui/section/UserListFragment$UserListAdapter;-><init>(Lflipboard/gui/section/UserListFragment;B)V

    iput-object v1, p0, Lflipboard/gui/section/UserListFragment;->a:Lflipboard/gui/section/UserListFragment$UserListAdapter;

    .line 191
    iget-object v1, p0, Lflipboard/gui/section/UserListFragment;->m:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;

    iget-object v2, p0, Lflipboard/gui/section/UserListFragment;->a:Lflipboard/gui/section/UserListFragment$UserListAdapter;

    invoke-virtual {v1, v2}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->setAdapter(Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;)V

    .line 192
    iget-object v1, p0, Lflipboard/gui/section/UserListFragment;->m:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;

    iget-object v2, p0, Lflipboard/gui/section/UserListFragment;->n:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v1, v2}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 193
    iget-object v1, p0, Lflipboard/gui/section/UserListFragment;->m:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;

    iget-object v2, p0, Lflipboard/gui/section/UserListFragment;->o:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 195
    invoke-direct {p0}, Lflipboard/gui/section/UserListFragment;->b()V

    .line 197
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, p0}, Lflipboard/service/User;->b(Lflipboard/util/Observer;)V

    .line 199
    return-object v0

    .line 104
    :pswitch_0
    sget-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->i:Lflipboard/objs/UsageEventV2$FollowFrom;

    iput-object v0, p0, Lflipboard/gui/section/UserListFragment;->i:Lflipboard/objs/UsageEventV2$FollowFrom;

    goto/16 :goto_0

    .line 107
    :pswitch_1
    sget-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->h:Lflipboard/objs/UsageEventV2$FollowFrom;

    iput-object v0, p0, Lflipboard/gui/section/UserListFragment;->i:Lflipboard/objs/UsageEventV2$FollowFrom;

    goto/16 :goto_0

    .line 110
    :pswitch_2
    sget-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->e:Lflipboard/objs/UsageEventV2$FollowFrom;

    iput-object v0, p0, Lflipboard/gui/section/UserListFragment;->i:Lflipboard/objs/UsageEventV2$FollowFrom;

    goto/16 :goto_0

    .line 113
    :pswitch_3
    sget-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->j:Lflipboard/objs/UsageEventV2$FollowFrom;

    iput-object v0, p0, Lflipboard/gui/section/UserListFragment;->i:Lflipboard/objs/UsageEventV2$FollowFrom;

    goto/16 :goto_0

    :cond_2
    move v1, v4

    .line 127
    goto/16 :goto_1

    :cond_3
    move v2, v4

    .line 129
    goto :goto_2

    :cond_4
    move v2, v4

    .line 131
    goto :goto_3

    .line 143
    :cond_5
    iget-object v2, p0, Lflipboard/gui/section/UserListFragment;->h:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_6
    move v3, v1

    :goto_5
    packed-switch v3, :pswitch_data_1

    .line 157
    sget-object v1, Lflipboard/objs/UsageEventV2$FollowFrom;->e:Lflipboard/objs/UsageEventV2$FollowFrom;

    iput-object v1, p0, Lflipboard/gui/section/UserListFragment;->i:Lflipboard/objs/UsageEventV2$FollowFrom;

    goto :goto_4

    .line 143
    :sswitch_0
    const-string v3, "flipboard"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    move v3, v4

    goto :goto_5

    :sswitch_1
    const-string v5, "facebook"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    goto :goto_5

    :sswitch_2
    const-string v3, "twitter"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v3, 0x2

    goto :goto_5

    :sswitch_3
    const-string v3, "googleplus"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v3, 0x3

    goto :goto_5

    .line 145
    :pswitch_4
    sget-object v1, Lflipboard/objs/UsageEventV2$FollowFrom;->k:Lflipboard/objs/UsageEventV2$FollowFrom;

    iput-object v1, p0, Lflipboard/gui/section/UserListFragment;->i:Lflipboard/objs/UsageEventV2$FollowFrom;

    goto/16 :goto_4

    .line 148
    :pswitch_5
    sget-object v1, Lflipboard/objs/UsageEventV2$FollowFrom;->l:Lflipboard/objs/UsageEventV2$FollowFrom;

    iput-object v1, p0, Lflipboard/gui/section/UserListFragment;->i:Lflipboard/objs/UsageEventV2$FollowFrom;

    goto/16 :goto_4

    .line 151
    :pswitch_6
    sget-object v1, Lflipboard/objs/UsageEventV2$FollowFrom;->m:Lflipboard/objs/UsageEventV2$FollowFrom;

    iput-object v1, p0, Lflipboard/gui/section/UserListFragment;->i:Lflipboard/objs/UsageEventV2$FollowFrom;

    goto/16 :goto_4

    .line 154
    :pswitch_7
    sget-object v1, Lflipboard/objs/UsageEventV2$FollowFrom;->n:Lflipboard/objs/UsageEventV2$FollowFrom;

    iput-object v1, p0, Lflipboard/gui/section/UserListFragment;->i:Lflipboard/objs/UsageEventV2$FollowFrom;

    goto/16 :goto_4

    .line 102
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 143
    :sswitch_data_0
    .sparse-switch
        -0x5b73d8ad -> :sswitch_3
        -0x369e558d -> :sswitch_2
        0x1da19ac6 -> :sswitch_1
        0x5d692a99 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 203
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onDestroy()V

    .line 204
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p0}, Lflipboard/service/User;->c(Lflipboard/util/Observer;)V

    .line 205
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 208
    :cond_0
    return-void
.end method
