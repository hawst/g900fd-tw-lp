.class public Lflipboard/gui/section/item/PostItemTablet;
.super Ljava/lang/Object;
.source "PostItemTablet.java"

# interfaces
.implements Lflipboard/gui/section/item/PostItem$ItemLayout;


# instance fields
.field private A:I

.field a:Lflipboard/objs/FeedItem;

.field b:Lflipboard/service/Section;

.field c:Lflipboard/gui/FLStaticTextView;

.field d:Lflipboard/gui/FLStaticTextView;

.field e:Lflipboard/gui/FLImageViewGroup;

.field f:Lflipboard/gui/FLLabelTextView;

.field g:Lflipboard/gui/FLTextView;

.field h:Lflipboard/gui/FLButton;

.field i:Lflipboard/gui/section/AttributionBase;

.field j:Landroid/view/View;

.field k:Landroid/view/View;

.field l:Lflipboard/gui/FLLabelTextView;

.field m:Lflipboard/gui/section/item/PostItemTablet$Layout;

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Landroid/view/ViewGroup$LayoutParams;

.field private t:Landroid/graphics/drawable/Drawable;

.field private u:Z

.field private v:Lflipboard/gui/FLCameleonImageView;

.field private w:Z

.field private final x:Landroid/content/Context;

.field private final y:Landroid/view/ViewGroup;

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-boolean v1, p0, Lflipboard/gui/section/item/PostItemTablet;->o:Z

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->r:Z

    .line 69
    iput-boolean v1, p0, Lflipboard/gui/section/item/PostItemTablet;->z:Z

    .line 78
    iput-object p1, p0, Lflipboard/gui/section/item/PostItemTablet;->x:Landroid/content/Context;

    .line 79
    iput-object p2, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    .line 80
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    .line 81
    return-void
.end method

.method private static a(IIILandroid/view/View;)V
    .locals 5

    .prologue
    .line 643
    sub-int v0, p1, p0

    invoke-virtual {p3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 644
    invoke-virtual {p3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int v1, p2, v1

    div-int/lit8 v1, v1, 0x2

    .line 645
    add-int/lit8 v2, v1, 0x0

    add-int v3, p0, v0

    invoke-virtual {p3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v1, v4

    add-int/2addr v0, p0

    invoke-virtual {p3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v0, v4

    invoke-virtual {p3, v2, v3, v1, v0}, Landroid/view/View;->layout(IIII)V

    .line 646
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/item/PostItemTablet;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->w:Z

    return v0
.end method

.method static synthetic b(Lflipboard/gui/section/item/PostItemTablet;)Lflipboard/gui/FLCameleonImageView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    const v1, 0x7f0a004f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    .line 87
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    const v1, 0x7f0a020f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->d:Lflipboard/gui/FLStaticTextView;

    .line 88
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    const v1, 0x7f0a0040

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageViewGroup;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    .line 89
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0}, Lflipboard/gui/FLImageViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->s:Landroid/view/ViewGroup$LayoutParams;

    .line 90
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0}, Lflipboard/gui/FLImageViewGroup;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->t:Landroid/graphics/drawable/Drawable;

    .line 91
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    const v1, 0x7f0a0205

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->g:Lflipboard/gui/FLTextView;

    .line 92
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    const v1, 0x7f0a0210

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->h:Lflipboard/gui/FLButton;

    .line 93
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    const v1, 0x7f0a0065

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->j:Landroid/view/View;

    .line 94
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    const v1, 0x7f0a0212

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->k:Landroid/view/View;

    .line 95
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    const v1, 0x7f0a0213

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->l:Lflipboard/gui/FLLabelTextView;

    .line 96
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    const v1, 0x7f0a0211

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    .line 97
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->x:Landroid/content/Context;

    const v1, 0x7f030156

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionBase;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    .line 98
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 99
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 195
    return-void
.end method

.method public final a(II)V
    .locals 14

    .prologue
    .line 263
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    .line 264
    if-eqz v1, :cond_14

    iget v0, v1, Lflipboard/objs/Image;->f:I

    move v7, v0

    .line 265
    :goto_0
    if-eqz v1, :cond_15

    iget v0, v1, Lflipboard/objs/Image;->g:I

    move v1, v0

    .line 266
    :goto_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    .line 267
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    .line 268
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v0

    sub-int v0, v8, v0

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v2

    sub-int v5, v0, v2

    .line 269
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v0

    sub-int v0, v9, v0

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v2

    sub-int v4, v0, v2

    .line 270
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->x:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v2, v0, Landroid/util/DisplayMetrics;->density:F

    .line 274
    const/4 v0, 0x0

    cmpl-float v0, v2, v0

    if-lez v0, :cond_16

    .line 275
    int-to-float v0, v5

    div-float/2addr v0, v2

    .line 276
    int-to-float v3, v4

    div-float v2, v3, v2

    .line 281
    :goto_2
    iget-boolean v3, p0, Lflipboard/gui/section/item/PostItemTablet;->n:Z

    if-eqz v3, :cond_17

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lflipboard/gui/section/AttributionBase;->setInverted(Z)V

    sget-object v3, Lflipboard/gui/section/item/PostItemTablet$Layout;->e:Lflipboard/gui/section/item/PostItemTablet$Layout;

    :goto_3
    iput-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    .line 283
    invoke-static {}, Lflipboard/util/AndroidUtil;->c()I

    move-result v3

    if-lt v8, v3, :cond_0

    invoke-static {}, Lflipboard/util/AndroidUtil;->b()I

    move-result v3

    if-lt v9, v3, :cond_0

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v6, Lflipboard/gui/section/item/PostItemTablet$Layout;->e:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-ne v3, v6, :cond_0

    .line 284
    const/4 v3, 0x1

    iput-boolean v3, p0, Lflipboard/gui/section/item/PostItemTablet;->z:Z

    .line 285
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lflipboard/gui/section/AttributionBase;->setInverted(Z)V

    .line 288
    :cond_0
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->a:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v3, :cond_23

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->a:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    .line 289
    :goto_4
    const/high16 v6, 0x43960000    # 300.0f

    cmpg-float v6, v0, v6

    if-ltz v6, :cond_1

    const v6, 0x43878000    # 271.0f

    cmpg-float v6, v2, v6

    if-ltz v6, :cond_1

    const/16 v6, 0x64

    if-le v3, v6, :cond_24

    .line 290
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->x:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0900f3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lflipboard/gui/FLStaticTextView;->a(II)V

    .line 298
    :goto_5
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->l:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_37

    .line 299
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->l:Lflipboard/gui/FLLabelTextView;

    const/high16 v2, -0x80000000

    invoke-static {v5, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/high16 v3, -0x80000000

    invoke-static {v4, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 300
    const/4 v0, 0x0

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->l:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v4, v2

    iget v3, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    sub-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 303
    :goto_6
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_4

    .line 305
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v3, Lflipboard/gui/section/item/PostItemTablet$Layout;->b:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v3, Lflipboard/gui/section/item/PostItemTablet$Layout;->a:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v3, Lflipboard/gui/section/item/PostItemTablet$Layout;->c:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-ne v2, v3, :cond_3

    .line 306
    :cond_2
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900ae

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 307
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v3, v2, v2, v2, v2}, Lflipboard/gui/FLTextView;->setPadding(IIII)V

    .line 309
    :cond_3
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->g:Lflipboard/gui/FLTextView;

    const/4 v3, 0x0

    const/4 v6, 0x0

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/4 v6, 0x0

    const/4 v8, 0x0

    invoke-static {v6, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v2, v3, v6}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 312
    :cond_4
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->h:Lflipboard/gui/FLButton;

    invoke-virtual {v2}, Lflipboard/gui/FLButton;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_5

    .line 313
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->h:Lflipboard/gui/FLButton;

    const/4 v3, 0x0

    const/4 v6, 0x0

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/4 v6, 0x0

    const/4 v8, 0x0

    invoke-static {v6, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v2, v3, v6}, Lflipboard/gui/FLButton;->measure(II)V

    .line 314
    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->h:Lflipboard/gui/FLButton;

    invoke-virtual {v3}, Lflipboard/gui/FLButton;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v0, v3

    iget v3, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    sub-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 317
    :cond_5
    iget-boolean v2, p0, Lflipboard/gui/section/item/PostItemTablet;->u:Z

    if-eqz v2, :cond_26

    const/high16 v2, -0x80000000

    move v6, v2

    .line 318
    :goto_7
    if-lez v7, :cond_36

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v3, Lflipboard/gui/section/item/PostItemTablet$Layout;->d:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-ne v2, v3, :cond_36

    .line 320
    iget-boolean v2, p0, Lflipboard/gui/section/item/PostItemTablet;->q:Z

    if-eqz v2, :cond_6

    .line 322
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    const/high16 v3, -0x80000000

    invoke-static {v5, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    div-int/lit8 v8, v0, 0x2

    const/high16 v10, -0x80000000

    invoke-static {v8, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v2, v3, v8}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 323
    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 327
    :cond_6
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    mul-int/lit8 v8, v4, 0x3

    div-int/lit8 v8, v8, 0x5

    const/high16 v10, -0x80000000

    invoke-static {v8, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v2, v3, v8}, Lflipboard/gui/FLImageViewGroup;->measure(II)V

    .line 329
    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v3}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v0, v3

    iget v3, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    sub-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v2, v0

    .line 333
    :goto_8
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v3, Lflipboard/gui/section/item/PostItemTablet$Layout;->c:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-ne v0, v3, :cond_35

    .line 334
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->q:Z

    if-eqz v0, :cond_7

    .line 335
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    const/high16 v3, -0x80000000

    invoke-static {v5, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    div-int/lit8 v8, v2, 0x2

    const/high16 v10, -0x80000000

    invoke-static {v8, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v0, v3, v8}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 337
    :cond_7
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    div-int/lit8 v3, v5, 0x2

    const/high16 v8, -0x80000000

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget-object v8, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v8}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v8

    sub-int/2addr v4, v8

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Lflipboard/gui/FLImageViewGroup;->measure(II)V

    .line 338
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->k:Landroid/view/View;

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v3}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v4}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v4

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/view/View;->measure(II)V

    .line 339
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->q:Z

    if-eqz v0, :cond_8

    .line 340
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v3}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v3

    const/high16 v4, -0x80000000

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v4

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 342
    :cond_8
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v0

    iget v3, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    add-int/2addr v0, v3

    sub-int v0, v5, v0

    .line 346
    :goto_9
    iget-boolean v3, p0, Lflipboard/gui/section/item/PostItemTablet;->z:Z

    if-eqz v3, :cond_34

    .line 347
    div-int/lit8 v0, v5, 0x2

    move v3, v0

    .line 351
    :goto_a
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->w:Z

    if-eqz v0, :cond_33

    .line 352
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    const v4, 0x7f0a0214

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLCameleonImageView;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    .line 353
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lflipboard/gui/FLCameleonImageView;->setVisibility(I)V

    .line 354
    const/4 v0, 0x0

    .line 355
    iget-boolean v4, p0, Lflipboard/gui/section/item/PostItemTablet;->z:Z

    if-nez v4, :cond_a

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v8, Lflipboard/gui/section/item/PostItemTablet$Layout;->e:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-eq v4, v8, :cond_9

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v8, Lflipboard/gui/section/item/PostItemTablet$Layout;->c:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-ne v4, v8, :cond_a

    .line 356
    :cond_9
    const/4 v0, 0x1

    .line 358
    :cond_a
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    const v8, 0x7f020001

    invoke-virtual {v4, v8}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    .line 359
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    const/4 v8, 0x0

    invoke-static {v4, v0, v8}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLCameleonImageView;ZZ)V

    .line 361
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    if-eqz v0, :cond_33

    .line 362
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    const/high16 v4, -0x80000000

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/high16 v8, -0x80000000

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v0, v4, v8}, Lflipboard/gui/FLCameleonImageView;->measure(II)V

    .line 363
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLCameleonImageView;->getMeasuredWidth()I

    move-result v0

    iget v4, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    add-int/2addr v0, v4

    sub-int v0, v3, v0

    .line 364
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v8, Lflipboard/gui/section/item/PostItemTablet$Layout;->e:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-ne v4, v8, :cond_32

    .line 365
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLCameleonImageView;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v3, v4

    move v13, v0

    move v0, v3

    move v3, v13

    .line 370
    :goto_b
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v8, Lflipboard/gui/section/item/PostItemTablet$Layout;->e:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-ne v4, v8, :cond_29

    .line 371
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    div-int/lit8 v4, v2, 0x5

    .line 372
    const/4 v2, 0x0

    .line 373
    iget-boolean v8, p0, Lflipboard/gui/section/item/PostItemTablet;->q:Z

    if-eqz v8, :cond_b

    .line 375
    iget-boolean v8, p0, Lflipboard/gui/section/item/PostItemTablet;->z:Z

    if-eqz v8, :cond_27

    .line 377
    div-int/lit8 v8, v5, 0x2

    iget v10, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    sub-int/2addr v8, v10

    .line 378
    iget-object v10, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    const/high16 v11, -0x80000000

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    const/high16 v11, -0x80000000

    invoke-static {v4, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v10, v8, v11}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 386
    :cond_b
    :goto_c
    iget-object v8, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v10

    const/high16 v11, 0x40000000    # 2.0f

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    sub-int/2addr v4, v2

    const/high16 v11, -0x80000000

    invoke-static {v4, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v8, v10, v4}, Lflipboard/gui/FLImageViewGroup;->measure(II)V

    .line 387
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v4}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v2

    .line 388
    iget-boolean v2, p0, Lflipboard/gui/section/item/PostItemTablet;->z:Z

    if-eqz v2, :cond_28

    .line 389
    sub-int v2, v9, v4

    .line 394
    :goto_d
    iget-object v8, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    invoke-virtual {v8}, Lflipboard/gui/section/AttributionBase;->getVisibility()I

    move-result v8

    if-nez v8, :cond_c

    .line 395
    iget-object v8, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    const/high16 v10, 0x40000000    # 2.0f

    invoke-static {v0, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    const/high16 v10, -0x80000000

    invoke-static {v2, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v0, v10}, Lflipboard/gui/section/AttributionBase;->measure(II)V

    .line 396
    const/4 v0, 0x0

    iget-object v8, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    invoke-virtual {v8}, Lflipboard/gui/section/AttributionBase;->getMeasuredHeight()I

    move-result v8

    sub-int/2addr v2, v8

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 398
    :cond_c
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_d

    .line 399
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    const/high16 v8, -0x80000000

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/high16 v8, -0x80000000

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v0, v3, v8}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 400
    const/4 v0, 0x0

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    .line 403
    :cond_d
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->z:Z

    if-nez v0, :cond_e

    .line 404
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionBase;->getMeasuredHeight()I

    move-result v0

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    iget v2, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    mul-int/lit8 v2, v2, 0x6

    add-int/2addr v0, v2

    .line 405
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->j:Landroid/view/View;

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v3}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v3

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/view/View;->measure(II)V

    .line 409
    :cond_e
    sub-int v0, v9, v4

    iget v2, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    sub-int v2, v0, v2

    .line 420
    :cond_f
    :goto_e
    const/4 v0, 0x0

    iget v3, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    sub-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 422
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v2, Lflipboard/gui/section/item/PostItemTablet$Layout;->b:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-ne v0, v2, :cond_2e

    .line 423
    div-int/lit8 v0, v5, 0x2

    .line 424
    mul-int v2, v0, v1

    div-int/2addr v2, v7

    .line 425
    if-le v2, v3, :cond_2c

    .line 426
    mul-int v0, v3, v7

    div-int v1, v0, v1

    .line 427
    const/16 v0, 0x78

    if-le v1, v0, :cond_2b

    .line 428
    const/4 v0, 0x0

    .line 429
    iget-boolean v2, p0, Lflipboard/gui/section/item/PostItemTablet;->q:Z

    if-eqz v2, :cond_10

    .line 430
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    const/high16 v2, -0x80000000

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/high16 v4, -0x80000000

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v0, v2, v4}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 431
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 433
    :cond_10
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    sub-int v0, v3, v0

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v2, v1, v0}, Lflipboard/gui/FLImageViewGroup;->measure(II)V

    .line 444
    :cond_11
    :goto_f
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_13

    .line 445
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v0

    iget v1, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    add-int/2addr v0, v1

    .line 446
    iget-boolean v1, p0, Lflipboard/gui/section/item/PostItemTablet;->q:Z

    if-eqz v1, :cond_12

    .line 447
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 449
    :cond_12
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->d:Lflipboard/gui/FLStaticTextView;

    sget-object v2, Lflipboard/gui/FLStaticTextView$BlockType;->a:Lflipboard/gui/FLStaticTextView$BlockType;

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v4}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v4

    sub-int v4, v5, v4

    iget v6, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    sub-int/2addr v4, v6

    invoke-virtual {v1, v2, v4, v0}, Lflipboard/gui/FLStaticTextView;->a(Lflipboard/gui/FLStaticTextView$BlockType;II)V

    .line 450
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->d:Lflipboard/gui/FLStaticTextView;

    const/high16 v1, -0x80000000

    invoke-static {v5, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    const/high16 v2, -0x80000000

    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 451
    const/4 v0, 0x0

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v3, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    .line 468
    :cond_13
    :goto_10
    return-void

    .line 264
    :cond_14
    const/4 v0, 0x0

    move v7, v0

    goto/16 :goto_0

    .line 265
    :cond_15
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_1

    .line 278
    :cond_16
    int-to-float v0, v5

    .line 279
    int-to-float v2, v4

    goto/16 :goto_2

    .line 281
    :cond_17
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v3}, Lflipboard/gui/FLImageViewGroup;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_18

    sget-object v3, Lflipboard/gui/section/item/PostItemTablet$Layout;->a:Lflipboard/gui/section/item/PostItemTablet$Layout;

    goto/16 :goto_3

    :cond_18
    if-eqz v5, :cond_19

    if-nez v4, :cond_1a

    :cond_19
    sget-object v3, Lflipboard/gui/section/item/PostItemTablet$Layout;->a:Lflipboard/gui/section/item/PostItemTablet$Layout;

    goto/16 :goto_3

    :cond_1a
    int-to-float v3, v5

    int-to-float v6, v4

    div-float v10, v3, v6

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v11

    if-eqz v11, :cond_1c

    iget v3, v11, Lflipboard/objs/Image;->f:I

    move v6, v3

    :goto_11
    if-eqz v11, :cond_1d

    iget v3, v11, Lflipboard/objs/Image;->g:I

    :goto_12
    if-eqz v6, :cond_1b

    if-nez v3, :cond_1e

    :cond_1b
    sget-object v3, Lflipboard/gui/section/item/PostItemTablet$Layout;->a:Lflipboard/gui/section/item/PostItemTablet$Layout;

    goto/16 :goto_3

    :cond_1c
    const/4 v3, 0x0

    move v6, v3

    goto :goto_11

    :cond_1d
    const/4 v3, 0x0

    goto :goto_12

    :cond_1e
    int-to-float v11, v3

    int-to-float v12, v6

    div-float/2addr v11, v12

    int-to-float v12, v5

    mul-float/2addr v11, v12

    int-to-float v12, v4

    sub-float v11, v12, v11

    iget-object v12, p0, Lflipboard/gui/section/item/PostItemTablet;->x:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v12

    iget v12, v12, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v11, v12

    float-to-int v11, v11

    const/16 v12, 0xf0

    if-le v11, v12, :cond_21

    if-le v6, v3, :cond_21

    div-int/lit8 v11, v5, 0x3

    if-le v6, v11, :cond_21

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->a:Lflipboard/objs/FeedItem;

    iget-boolean v3, v3, Lflipboard/objs/FeedItem;->ab:Z

    if-eqz v3, :cond_20

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->a:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->ac:Ljava/lang/String;

    if-eqz v3, :cond_1f

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->l:Lflipboard/gui/FLLabelTextView;

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->a:Lflipboard/objs/FeedItem;

    iget-object v6, v6, Lflipboard/objs/FeedItem;->ac:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1f
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->l:Lflipboard/gui/FLLabelTextView;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    :cond_20
    sget-object v3, Lflipboard/gui/section/item/PostItemTablet$Layout;->d:Lflipboard/gui/section/item/PostItemTablet$Layout;

    goto/16 :goto_3

    :cond_21
    int-to-float v6, v6

    int-to-float v3, v3

    div-float v3, v6, v3

    const/4 v6, 0x0

    cmpl-float v6, v3, v6

    if-lez v6, :cond_22

    div-float v3, v10, v3

    const/high16 v6, 0x40000000    # 2.0f

    cmpl-float v3, v3, v6

    if-lez v3, :cond_22

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->k:Landroid/view/View;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    sget-object v3, Lflipboard/gui/section/item/PostItemTablet$Layout;->c:Lflipboard/gui/section/item/PostItemTablet$Layout;

    goto/16 :goto_3

    :cond_22
    sget-object v3, Lflipboard/gui/section/item/PostItemTablet$Layout;->b:Lflipboard/gui/section/item/PostItemTablet$Layout;

    goto/16 :goto_3

    .line 288
    :cond_23
    const/4 v3, 0x0

    goto/16 :goto_4

    .line 291
    :cond_24
    const/high16 v6, 0x43fa0000    # 500.0f

    cmpl-float v0, v0, v6

    if-lez v0, :cond_25

    const v0, 0x43878000    # 271.0f

    cmpl-float v0, v2, v0

    if-lez v0, :cond_25

    const/16 v0, 0x3c

    if-ge v3, v0, :cond_25

    .line 292
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->x:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0900f1

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lflipboard/gui/FLStaticTextView;->a(II)V

    goto/16 :goto_5

    .line 294
    :cond_25
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->x:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0900f2

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lflipboard/gui/FLStaticTextView;->a(II)V

    goto/16 :goto_5

    .line 317
    :cond_26
    const/high16 v2, 0x40000000    # 2.0f

    move v6, v2

    goto/16 :goto_7

    .line 380
    :cond_27
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    const/high16 v8, -0x80000000

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    const/high16 v10, -0x80000000

    invoke-static {v4, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v2, v8, v10}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 381
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v2

    iget v8, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    add-int/2addr v2, v8

    add-int/lit8 v2, v2, 0x0

    goto/16 :goto_c

    .line 391
    :cond_28
    const/4 v2, 0x0

    iget-object v8, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v8

    sub-int v8, v4, v8

    iget v10, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    sub-int/2addr v8, v10

    invoke-static {v2, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto/16 :goto_d

    .line 411
    :cond_29
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemTablet;->a:Lflipboard/objs/FeedItem;

    iget-boolean v4, v4, Lflipboard/objs/FeedItem;->bN:Z

    if-nez v4, :cond_2a

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    if-eqz v4, :cond_2a

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    invoke-virtual {v4}, Lflipboard/gui/section/AttributionBase;->getVisibility()I

    move-result v4

    const/16 v8, 0x8

    if-eq v4, v8, :cond_2a

    .line 412
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    const/high16 v8, -0x80000000

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v4, v0, v8}, Lflipboard/gui/section/AttributionBase;->measure(II)V

    .line 413
    const/4 v0, 0x0

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    invoke-virtual {v4}, Lflipboard/gui/section/AttributionBase;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v2, v4

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 415
    :cond_2a
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_f

    .line 416
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    const/high16 v4, -0x80000000

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/high16 v4, -0x80000000

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 417
    const/4 v0, 0x0

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto/16 :goto_e

    .line 435
    :cond_2b
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    const/4 v1, 0x0

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    const/4 v2, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLImageViewGroup;->measure(II)V

    goto/16 :goto_f

    .line 438
    :cond_2c
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->q:Z

    if-eqz v0, :cond_2d

    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getHeight()I

    move-result v0

    sub-int v0, v2, v0

    :goto_13
    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v1, v4, v0}, Lflipboard/gui/FLImageViewGroup;->measure(II)V

    .line 439
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->q:Z

    if-eqz v0, :cond_11

    .line 440
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v1

    const/high16 v4, -0x80000000

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    const/high16 v4, -0x80000000

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    goto/16 :goto_f

    :cond_2d
    move v0, v2

    .line 438
    goto :goto_13

    .line 455
    :cond_2e
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v1, Lflipboard/gui/section/item/PostItemTablet$Layout;->c:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-ne v0, v1, :cond_30

    .line 456
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    add-int/2addr v0, v1

    sub-int/2addr v5, v0

    .line 464
    :cond_2f
    :goto_14
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_13

    .line 465
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->d:Lflipboard/gui/FLStaticTextView;

    const/high16 v1, -0x80000000

    invoke-static {v5, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    const/high16 v2, -0x80000000

    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    goto/16 :goto_10

    .line 457
    :cond_30
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->z:Z

    if-eqz v0, :cond_2f

    .line 459
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->q:Z

    if-eqz v0, :cond_31

    .line 460
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v3, v0

    .line 462
    :goto_15
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    add-int/2addr v1, v2

    sub-int/2addr v5, v1

    move v3, v0

    goto :goto_14

    :cond_31
    move v0, v3

    goto :goto_15

    :cond_32
    move v13, v0

    move v0, v3

    move v3, v13

    goto/16 :goto_b

    :cond_33
    move v0, v3

    goto/16 :goto_b

    :cond_34
    move v3, v0

    goto/16 :goto_a

    :cond_35
    move v0, v5

    goto/16 :goto_9

    :cond_36
    move v2, v0

    goto/16 :goto_8

    :cond_37
    move v0, v4

    goto/16 :goto_6
.end method

.method public final a(IIII)V
    .locals 12

    .prologue
    .line 473
    sub-int v4, p3, p1

    .line 474
    sub-int v5, p4, p2

    .line 475
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v3

    .line 476
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v0

    .line 478
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->l:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 479
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->l:Lflipboard/gui/FLLabelTextView;

    iget v2, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    sub-int v2, v3, v2

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->l:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v3

    iget v7, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    sub-int/2addr v6, v7

    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->l:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v0

    invoke-virtual {v1, v2, v0, v6, v7}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 480
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->l:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 481
    iget v1, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    add-int/2addr v0, v1

    .line 485
    :cond_0
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->a:Lflipboard/objs/FeedItem;

    iget-boolean v1, v1, Lflipboard/objs/FeedItem;->bN:Z

    if-eqz v1, :cond_1

    .line 486
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lflipboard/gui/section/AttributionBase;->setVisibility(I)V

    .line 489
    :cond_1
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v2, Lflipboard/gui/section/item/PostItemTablet$Layout;->d:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-ne v1, v2, :cond_4

    .line 491
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v2}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v3

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v6}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v1, v3, v0, v2, v6}, Lflipboard/gui/FLImageViewGroup;->layout(IIII)V

    .line 494
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_2

    .line 495
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->g:Lflipboard/gui/FLTextView;

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v3

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v1, v3, v0, v2, v6}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 498
    :cond_2
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 499
    iget-boolean v1, p0, Lflipboard/gui/section/item/PostItemTablet;->q:Z

    if-eqz v1, :cond_3

    .line 500
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v3

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    .line 501
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v1

    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v0

    invoke-virtual {v2, v1, v0, v6, v7}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 502
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 505
    :cond_3
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->h:Lflipboard/gui/FLButton;

    invoke-virtual {v1}, Lflipboard/gui/FLButton;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_e

    .line 506
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->h:Lflipboard/gui/FLButton;

    invoke-static {v0, v5, v4, v1}, Lflipboard/gui/section/item/PostItemTablet;->a(IIILandroid/view/View;)V

    .line 512
    :cond_4
    :goto_0
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v2, Lflipboard/gui/section/item/PostItemTablet$Layout;->e:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-ne v1, v2, :cond_10

    .line 513
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v6}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v6

    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v7}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {v0, v1, v2, v6, v7}, Lflipboard/gui/FLImageViewGroup;->layout(IIII)V

    .line 515
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    invoke-virtual {v1}, Lflipboard/gui/section/AttributionBase;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 516
    iget-boolean v1, p0, Lflipboard/gui/section/item/PostItemTablet;->z:Z

    if-nez v1, :cond_5

    .line 517
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v0

    .line 518
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 519
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->j:Landroid/view/View;

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 520
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->j:Landroid/view/View;

    const/4 v6, 0x0

    sub-int v1, v0, v1

    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v7}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v7

    invoke-virtual {v2, v6, v1, v7, v0}, Landroid/view/View;->layout(IIII)V

    .line 521
    iget v1, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    sub-int/2addr v0, v1

    .line 525
    :cond_5
    iget-boolean v1, p0, Lflipboard/gui/section/item/PostItemTablet;->w:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    if-eqz v1, :cond_7

    .line 526
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v1

    sub-int v1, v4, v1

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLCameleonImageView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    .line 527
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v0, v2

    .line 528
    iget-boolean v6, p0, Lflipboard/gui/section/item/PostItemTablet;->z:Z

    if-eqz v6, :cond_6

    .line 529
    div-int/lit8 v6, v1, 0x2

    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v7

    add-int/2addr v6, v7

    iget v7, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    add-int/2addr v6, v7

    sub-int/2addr v1, v6

    .line 531
    :cond_6
    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v7}, Lflipboard/gui/FLCameleonImageView;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v1

    iget-object v8, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v8}, Lflipboard/gui/FLCameleonImageView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v2

    invoke-virtual {v6, v1, v2, v7, v8}, Lflipboard/gui/FLCameleonImageView;->layout(IIII)V

    .line 534
    :cond_7
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    invoke-virtual {v1}, Lflipboard/gui/section/AttributionBase;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v0, v1

    .line 535
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    invoke-virtual {v2}, Lflipboard/gui/section/AttributionBase;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v3

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    invoke-virtual {v6}, Lflipboard/gui/section/AttributionBase;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v1

    invoke-virtual {v0, v3, v1, v2, v6}, Lflipboard/gui/section/AttributionBase;->layout(IIII)V

    .line 537
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->q:Z

    if-eqz v0, :cond_8

    .line 538
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v0

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    iget v2, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    sub-int/2addr v0, v2

    .line 540
    iget-boolean v2, p0, Lflipboard/gui/section/item/PostItemTablet;->z:Z

    if-eqz v2, :cond_18

    .line 542
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v2}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 543
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v0

    iget v6, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    add-int/2addr v0, v6

    .line 546
    :goto_1
    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v2

    iget-object v8, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v8}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v0

    invoke-virtual {v6, v2, v0, v7, v8}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 547
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->z:Z

    if-nez v0, :cond_8

    .line 548
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v1, v0

    .line 552
    :cond_8
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_9

    .line 553
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v1, v0

    .line 554
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v3

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v1, v3, v0, v2, v6}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 555
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->z:Z

    if-eqz v0, :cond_f

    .line 556
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    .line 562
    :cond_9
    :goto_2
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v0

    .line 585
    :goto_3
    iget v1, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    add-int/2addr v1, v0

    .line 587
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v2, Lflipboard/gui/section/item/PostItemTablet$Layout;->b:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-ne v0, v2, :cond_14

    .line 588
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v0

    sub-int v0, v4, v0

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v2}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v2

    sub-int v2, v0, v2

    .line 589
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v1

    .line 590
    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v7}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v2

    invoke-virtual {v6, v2, v1, v7, v0}, Lflipboard/gui/FLImageViewGroup;->layout(IIII)V

    .line 592
    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v6}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v2

    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v7

    sub-int/2addr v6, v7

    .line 593
    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    iget-object v8, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v8}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v1

    iget-object v9, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v9}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v9, v6

    iget-object v10, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v10}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v10

    add-int/2addr v10, v1

    iget-object v11, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v11}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v11

    add-int/2addr v10, v11

    invoke-virtual {v7, v6, v8, v9, v10}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 595
    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-eq v6, v7, :cond_a

    .line 596
    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->g:Lflipboard/gui/FLTextView;

    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v2

    iget-object v8, p0, Lflipboard/gui/section/item/PostItemTablet;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v8}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v1

    invoke-virtual {v6, v2, v1, v7, v8}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 613
    :cond_a
    :goto_4
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_c

    .line 614
    iget-boolean v2, p0, Lflipboard/gui/section/item/PostItemTablet;->z:Z

    if-eqz v2, :cond_16

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v6, Lflipboard/gui/section/item/PostItemTablet$Layout;->e:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-ne v2, v6, :cond_16

    .line 615
    div-int/lit8 v2, v4, 0x2

    add-int/2addr v2, v3

    .line 616
    iget-boolean v3, p0, Lflipboard/gui/section/item/PostItemTablet;->q:Z

    if-eqz v3, :cond_b

    .line 617
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v3

    iget v6, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    add-int/2addr v3, v6

    add-int/2addr v1, v3

    .line 620
    :cond_b
    :goto_5
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->d:Lflipboard/gui/FLStaticTextView;

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v2

    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v1

    invoke-virtual {v3, v2, v1, v6, v7}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 621
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    .line 624
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 627
    :cond_c
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->h:Lflipboard/gui/FLButton;

    invoke-virtual {v0}, Lflipboard/gui/FLButton;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_d

    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v2, Lflipboard/gui/section/item/PostItemTablet$Layout;->d:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-eq v0, v2, :cond_d

    .line 628
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->h:Lflipboard/gui/FLButton;

    invoke-static {v1, v5, v4, v0}, Lflipboard/gui/section/item/PostItemTablet;->a(IIILandroid/view/View;)V

    .line 630
    :cond_d
    return-void

    .line 508
    :cond_e
    iget v1, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    add-int/2addr v0, v1

    goto/16 :goto_0

    .line 558
    :cond_f
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800aa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    goto/16 :goto_2

    .line 565
    :cond_10
    iget-boolean v1, p0, Lflipboard/gui/section/item/PostItemTablet;->w:Z

    if-eqz v1, :cond_11

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    if-eqz v1, :cond_11

    .line 566
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v2, Lflipboard/gui/section/item/PostItemTablet$Layout;->c:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-ne v1, v2, :cond_13

    .line 567
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v1

    sub-int v1, v4, v1

    iget v2, p0, Lflipboard/gui/section/item/PostItemTablet;->A:I

    sub-int/2addr v1, v2

    .line 569
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v6}, Lflipboard/gui/FLCameleonImageView;->getMeasuredWidth()I

    move-result v6

    sub-int v6, v1, v6

    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v7}, Lflipboard/gui/FLCameleonImageView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v0

    invoke-virtual {v2, v6, v0, v1, v7}, Lflipboard/gui/FLCameleonImageView;->layout(IIII)V

    .line 576
    :cond_11
    :goto_6
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_12

    .line 577
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v3

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v1, v3, v0, v2, v6}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 578
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 580
    :cond_12
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    invoke-virtual {v2}, Lflipboard/gui/section/AttributionBase;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v3

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    invoke-virtual {v6}, Lflipboard/gui/section/AttributionBase;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v1, v3, v0, v2, v6}, Lflipboard/gui/section/AttributionBase;->layout(IIII)V

    .line 581
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    invoke-virtual {v1}, Lflipboard/gui/section/AttributionBase;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 582
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->j:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 571
    :cond_13
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v1

    sub-int v1, v4, v1

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLCameleonImageView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    .line 572
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v6}, Lflipboard/gui/FLCameleonImageView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v1

    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->v:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v7}, Lflipboard/gui/FLCameleonImageView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v0

    invoke-virtual {v2, v1, v0, v6, v7}, Lflipboard/gui/FLCameleonImageView;->layout(IIII)V

    goto :goto_6

    .line 599
    :cond_14
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->m:Lflipboard/gui/section/item/PostItemTablet$Layout;

    sget-object v2, Lflipboard/gui/section/item/PostItemTablet$Layout;->c:Lflipboard/gui/section/item/PostItemTablet$Layout;

    if-ne v0, v2, :cond_17

    .line 600
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v2

    .line 601
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v0

    sub-int v6, v4, v0

    .line 602
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v2

    .line 603
    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    iget-object v8, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v8}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v8

    sub-int v8, v6, v8

    invoke-virtual {v7, v8, v2, v6, v0}, Lflipboard/gui/FLImageViewGroup;->layout(IIII)V

    .line 605
    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_15

    .line 606
    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->g:Lflipboard/gui/FLTextView;

    iget-object v8, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v8}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v8

    sub-int v8, v6, v8

    iget-object v9, p0, Lflipboard/gui/section/item/PostItemTablet;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v9}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v2

    invoke-virtual {v7, v8, v2, v6, v9}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 609
    :cond_15
    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->k:Landroid/view/View;

    iget-object v8, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v8}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v8

    sub-int v8, v6, v8

    invoke-virtual {v7, v8, v2, v6, v0}, Landroid/view/View;->layout(IIII)V

    .line 610
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v7

    sub-int/2addr v6, v7

    iget-object v7, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v0

    invoke-virtual {v2, v6, v0, p3, v7}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    goto/16 :goto_4

    :cond_16
    move v2, v3

    goto/16 :goto_5

    :cond_17
    move v0, v1

    goto/16 :goto_4

    :cond_18
    move v2, v0

    move v0, v1

    goto/16 :goto_1
.end method

.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 7

    .prologue
    const/4 v5, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/16 v6, 0x8

    .line 118
    iput-object p2, p0, Lflipboard/gui/section/item/PostItemTablet;->a:Lflipboard/objs/FeedItem;

    .line 119
    iput-object p1, p0, Lflipboard/gui/section/item/PostItemTablet;->b:Lflipboard/service/Section;

    .line 121
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->I()Ljava/lang/String;

    move-result-object v0

    .line 122
    if-eqz v0, :cond_4

    .line 123
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    :goto_0
    iget-boolean v0, p2, Lflipboard/objs/FeedItem;->aj:Z

    if-eqz v0, :cond_5

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p2, Lflipboard/objs/FeedItem;->bN:Z

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->w:Z

    .line 132
    iget-boolean v0, p2, Lflipboard/objs/FeedItem;->bN:Z

    if-eqz v0, :cond_6

    iget-object v4, p2, Lflipboard/objs/FeedItem;->bR:Ljava/lang/String;

    .line 133
    :goto_2
    if-nez v4, :cond_0

    .line 134
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->G()Ljava/lang/String;

    move-result-object v4

    .line 136
    :cond_0
    if-eqz v4, :cond_8

    .line 137
    const/4 v3, 0x0

    .line 138
    invoke-static {p2}, Lflipboard/gui/section/ItemDisplayUtil;->c(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v0

    .line 139
    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lflipboard/service/Section;->v()Z

    move-result v5

    if-nez v5, :cond_c

    .line 142
    :goto_3
    iget-object v3, p2, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    .line 143
    if-eqz v3, :cond_1

    .line 144
    if-nez v0, :cond_7

    move-object v0, v3

    .line 146
    :cond_1
    :goto_4
    if-eqz v0, :cond_b

    .line 147
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " \u2022 "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 149
    :goto_5
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemTablet;->d:Lflipboard/gui/FLStaticTextView;

    iget-object v4, p2, Lflipboard/objs/FeedItem;->Y:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Lflipboard/gui/FLStaticTextView;->a(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 154
    :goto_6
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->i()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 155
    iput-boolean v1, p0, Lflipboard/gui/section/item/PostItemTablet;->p:Z

    .line 157
    iget-object v0, p2, Lflipboard/objs/FeedItem;->bq:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 158
    iput-boolean v1, p0, Lflipboard/gui/section/item/PostItemTablet;->q:Z

    .line 159
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->bq:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->f:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 163
    :cond_2
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->l()Ljava/util/List;

    move-result-object v0

    .line 164
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 165
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageViewGroup;->setImages(Ljava/util/List;)V

    .line 171
    :cond_3
    :goto_7
    iget-boolean v0, p2, Lflipboard/objs/FeedItem;->bN:Z

    if-eqz v0, :cond_a

    .line 172
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->h:Lflipboard/gui/FLButton;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->bQ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 173
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->d:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->bR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    invoke-virtual {v0, v6}, Lflipboard/gui/section/AttributionBase;->setVisibility(I)V

    .line 180
    :goto_8
    return-void

    .line 125
    :cond_4
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 129
    goto/16 :goto_1

    .line 132
    :cond_6
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2

    .line 144
    :cond_7
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " / "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 151
    :cond_8
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    goto :goto_6

    .line 168
    :cond_9
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0, v6}, Lflipboard/gui/FLImageViewGroup;->setVisibility(I)V

    goto :goto_7

    .line 176
    :cond_a
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->i:Lflipboard/gui/section/AttributionBase;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/AttributionBase;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 177
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v6}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->h:Lflipboard/gui/FLButton;

    invoke-virtual {v0, v6}, Lflipboard/gui/FLButton;->setVisibility(I)V

    goto :goto_8

    :cond_b
    move-object v0, v4

    goto/16 :goto_5

    :cond_c
    move-object v0, v3

    goto/16 :goto_3
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 103
    iput-boolean p1, p0, Lflipboard/gui/section/item/PostItemTablet;->n:Z

    .line 104
    return-void
.end method

.method public final a(ZI)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 650
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "PostItemTablet:onPageOffsetChange"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 651
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->p:Z

    if-eqz v0, :cond_0

    .line 652
    invoke-static {p1, p2}, Lflipboard/util/AndroidUtil;->a(ZI)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 653
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->r:Z

    if-eqz v0, :cond_0

    .line 654
    iput-boolean v3, p0, Lflipboard/gui/section/item/PostItemTablet;->r:Z

    .line 655
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0}, Lflipboard/gui/FLImageViewGroup;->a()V

    .line 668
    :cond_0
    :goto_0
    return-void

    .line 658
    :cond_1
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->r:Z

    if-nez v0, :cond_0

    .line 659
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/item/PostItemTablet;->r:Z

    .line 660
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 661
    new-instance v0, Lflipboard/gui/FLImageViewGroup;

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->x:Landroid/content/Context;

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLImageViewGroup;-><init>(Landroid/content/Context;Lflipboard/gui/FLImageViewGroup;)V

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    .line 662
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 664
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->y:Landroid/view/ViewGroup;

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemTablet;->e:Lflipboard/gui/FLImageViewGroup;

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemTablet;->s:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1, v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 673
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/item/PostItemTablet$1;

    invoke-direct {v1, p0}, Lflipboard/gui/section/item/PostItemTablet$1;-><init>(Lflipboard/gui/section/item/PostItemTablet;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;J)V

    .line 685
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 113
    iput-boolean p1, p0, Lflipboard/gui/section/item/PostItemTablet;->o:Z

    .line 114
    return-void
.end method

.method public final c()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->a:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lflipboard/gui/section/item/PostItemPhone$Layout;
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Lflipboard/gui/FLButton;
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemTablet;->h:Lflipboard/gui/FLButton;

    return-object v0
.end method
