.class Lflipboard/gui/section/item/ProfileBioItem$2;
.super Ljava/lang/Object;
.source "ProfileBioItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/section/item/ProfileBioItem;


# direct methods
.method constructor <init>(Lflipboard/gui/section/item/ProfileBioItem;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lflipboard/gui/section/item/ProfileBioItem$2;->a:Lflipboard/gui/section/item/ProfileBioItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 224
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem$2;->a:Lflipboard/gui/section/item/ProfileBioItem;

    invoke-static {v0}, Lflipboard/gui/section/item/ProfileBioItem;->a(Lflipboard/gui/section/item/ProfileBioItem;)Lflipboard/service/Section;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->i:Lflipboard/objs/FeedSectionLink;

    iget-object v0, v0, Lflipboard/objs/FeedSectionLink;->k:Ljava/lang/String;

    .line 225
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem$2;->a:Lflipboard/gui/section/item/ProfileBioItem;

    invoke-virtual {v0}, Lflipboard/gui/section/item/ProfileBioItem;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lflipboard/activities/UpdateAccountActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v2, 0x1e43

    invoke-virtual {v0, v1, v2}, Lflipboard/activities/FlipboardActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 237
    :goto_0
    return-void

    .line 228
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem$2;->a:Lflipboard/gui/section/item/ProfileBioItem;

    invoke-static {v0}, Lflipboard/gui/section/item/ProfileBioItem;->a(Lflipboard/gui/section/item/ProfileBioItem;)Lflipboard/service/Section;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/service/Section;->s()Z

    move-result v0

    if-nez v0, :cond_1

    .line 229
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/gui/section/item/ProfileBioItem$2;->a:Lflipboard/gui/section/item/ProfileBioItem;

    invoke-static {v1}, Lflipboard/gui/section/item/ProfileBioItem;->a(Lflipboard/gui/section/item/ProfileBioItem;)Lflipboard/service/Section;

    move-result-object v1

    const-string v2, "layoutView_userProfile"

    invoke-virtual {v0, v1, v3, v2}, Lflipboard/service/User;->a(Lflipboard/service/Section;ZLjava/lang/String;)Lflipboard/service/Section;

    .line 230
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem$2;->a:Lflipboard/gui/section/item/ProfileBioItem;

    invoke-virtual {v0}, Lflipboard/gui/section/item/ProfileBioItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 231
    iget-object v1, p0, Lflipboard/gui/section/item/ProfileBioItem$2;->a:Lflipboard/gui/section/item/ProfileBioItem;

    iget-object v1, v1, Lflipboard/gui/section/item/ProfileBioItem;->e:Lflipboard/gui/FLButton;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 233
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/gui/section/item/ProfileBioItem$2;->a:Lflipboard/gui/section/item/ProfileBioItem;

    invoke-static {v1}, Lflipboard/gui/section/item/ProfileBioItem;->a(Lflipboard/gui/section/item/ProfileBioItem;)Lflipboard/service/Section;

    move-result-object v1

    const-string v2, "layoutView_userProfile"

    invoke-virtual {v0, v1, v3, v2}, Lflipboard/service/User;->b(Lflipboard/service/Section;ZLjava/lang/String;)Z

    .line 234
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem$2;->a:Lflipboard/gui/section/item/ProfileBioItem;

    iget-object v0, v0, Lflipboard/gui/section/item/ProfileBioItem;->e:Lflipboard/gui/FLButton;

    iget-object v1, p0, Lflipboard/gui/section/item/ProfileBioItem$2;->a:Lflipboard/gui/section/item/ProfileBioItem;

    invoke-virtual {v1}, Lflipboard/gui/section/item/ProfileBioItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d02fe

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
