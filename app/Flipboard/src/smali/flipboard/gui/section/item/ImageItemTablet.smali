.class public Lflipboard/gui/section/item/ImageItemTablet;
.super Landroid/view/ViewGroup;
.source "ImageItemTablet.java"

# interfaces
.implements Lflipboard/gui/FLViewIntf;
.implements Lflipboard/gui/item/TabletItem;


# instance fields
.field a:Lflipboard/objs/FeedItem;

.field b:Lflipboard/gui/FLImageView;

.field c:Lflipboard/gui/FLStaticTextView;

.field d:Landroid/widget/ImageButton;

.field e:Lflipboard/gui/section/Attribution;

.field private f:Landroid/view/ViewGroup$LayoutParams;

.field private g:Z

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Landroid/view/View;

.field private j:Z

.field private k:I

.field private l:Z

.field private final m:I

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Lflipboard/service/Section;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    const v0, 0x7fffffff

    iput v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->k:I

    .line 50
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->m:I

    .line 51
    return-void
.end method

.method private getImageAlign()Lflipboard/gui/FLImageView$Align;
    .locals 1

    .prologue
    .line 169
    sget-object v0, Lflipboard/gui/FLImageView$Align;->a:Lflipboard/gui/FLImageView$Align;

    return-object v0
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 3

    .prologue
    .line 68
    iput-object p2, p0, Lflipboard/gui/section/item/ImageItemTablet;->a:Lflipboard/objs/FeedItem;

    .line 69
    iput-object p1, p0, Lflipboard/gui/section/item/ImageItemTablet;->q:Lflipboard/service/Section;

    .line 70
    invoke-virtual {p0, p2}, Lflipboard/gui/section/item/ImageItemTablet;->setTag(Ljava/lang/Object;)V

    .line 71
    iget-object v0, p2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    new-instance v0, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->d:Landroid/widget/ImageButton;

    .line 73
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->d:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 74
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->d:Landroid/widget/ImageButton;

    const v1, 0x7f02004f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 75
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->d:Landroid/widget/ImageButton;

    new-instance v1, Lflipboard/gui/section/item/ImageItemTablet$1;

    invoke-direct {v1, p0}, Lflipboard/gui/section/item/ImageItemTablet$1;-><init>(Lflipboard/gui/section/item/ImageItemTablet;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->d:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ImageItemTablet;->addView(Landroid/view/View;)V

    .line 84
    :cond_0
    iget-boolean v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->j:Z

    iget v1, p0, Lflipboard/gui/section/item/ImageItemTablet;->k:I

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(ZI)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 85
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, p2}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 94
    :cond_1
    :goto_0
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->I()Ljava/lang/String;

    move-result-object v0

    .line 95
    if-eqz v0, :cond_3

    .line 96
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->e:Lflipboard/gui/section/Attribution;

    invoke-interface {v0, p1, p2}, Lflipboard/gui/section/Attribution;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 103
    return-void

    .line 87
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->g:Z

    .line 88
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_1

    .line 90
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    iget v2, v0, Lflipboard/objs/Image;->f:I

    iget v0, v0, Lflipboard/objs/Image;->g:I

    invoke-virtual {v1, v2, v0}, Lflipboard/gui/FLImageView;->a(II)V

    goto :goto_0

    .line 97
    :cond_3
    invoke-static {p2}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 98
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 100
    :cond_4
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(ZI)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 132
    iput-boolean p1, p0, Lflipboard/gui/section/item/ImageItemTablet;->j:Z

    .line 133
    iput p2, p0, Lflipboard/gui/section/item/ImageItemTablet;->k:I

    .line 134
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "ImageItemTablet:onPageOffsetChange"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 135
    invoke-static {p1, p2}, Lflipboard/util/AndroidUtil;->a(ZI)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    iget-boolean v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->g:Z

    if-eqz v0, :cond_0

    .line 137
    iput-boolean v3, p0, Lflipboard/gui/section/item/ImageItemTablet;->g:Z

    .line 138
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemTablet;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    iget-boolean v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->g:Z

    if-nez v0, :cond_0

    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->g:Z

    .line 143
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ImageItemTablet;->removeView(Landroid/view/View;)V

    .line 144
    new-instance v0, Lflipboard/gui/FLImageView;

    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLImageView;-><init>(Landroid/content/Context;Lflipboard/gui/FLImageView;)V

    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    .line 145
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemTablet;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 146
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemTablet;->f:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, v0, v3, v1}, Lflipboard/gui/section/item/ImageItemTablet;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->a:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 56
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 57
    const v0, 0x7f0a0040

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ImageItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    .line 58
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->f:Landroid/view/ViewGroup$LayoutParams;

    .line 59
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->h:Landroid/graphics/drawable/Drawable;

    .line 60
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ImageItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    .line 61
    const v0, 0x7f0a008b

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ImageItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Attribution;

    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->e:Lflipboard/gui/section/Attribution;

    .line 62
    const v0, 0x7f0a0065

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ImageItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->i:Landroid/view/View;

    .line 63
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 207
    sub-int v0, p5, p3

    .line 208
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemTablet;->getPaddingLeft()I

    move-result v1

    .line 209
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemTablet;->getPaddingTop()I

    move-result v2

    .line 210
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemTablet;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v0, v3

    .line 211
    iget-object v3, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    iget-object v4, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    iget-object v5, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v3, v1, v2, v4, v5}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 213
    iget v2, p0, Lflipboard/gui/section/item/ImageItemTablet;->m:I

    add-int/2addr v1, v2

    .line 214
    iget v2, p0, Lflipboard/gui/section/item/ImageItemTablet;->m:I

    sub-int/2addr v0, v2

    .line 215
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemTablet;->e:Lflipboard/gui/section/Attribution;

    iget-object v3, p0, Lflipboard/gui/section/item/ImageItemTablet;->e:Lflipboard/gui/section/Attribution;

    invoke-interface {v3}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v0, v3

    iget-object v4, p0, Lflipboard/gui/section/item/ImageItemTablet;->e:Lflipboard/gui/section/Attribution;

    invoke-interface {v4}, Lflipboard/gui/section/Attribution;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    invoke-interface {v2, v1, v3, v4, v0}, Lflipboard/gui/section/Attribution;->layout(IIII)V

    .line 217
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 218
    iget v2, p0, Lflipboard/gui/section/item/ImageItemTablet;->m:I

    add-int/2addr v1, v2

    .line 219
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemTablet;->e:Lflipboard/gui/section/Attribution;

    invoke-interface {v2}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v0, v2

    .line 220
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v3, p0, Lflipboard/gui/section/item/ImageItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v0, v3

    iget-object v4, p0, Lflipboard/gui/section/item/ImageItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v1, v3, v4, v0}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 221
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getBottom()I

    move-result v0

    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getTop()I

    move-result v2

    if-lt v0, v2, :cond_3

    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0800aa

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    .line 224
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 225
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->i:Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getBottom()I

    move-result v2

    iget-object v3, p0, Lflipboard/gui/section/item/ImageItemTablet;->i:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lflipboard/gui/section/item/ImageItemTablet;->i:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 228
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->d:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 229
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemTablet;->d:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 230
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getTop()I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemTablet;->d:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getMeasuredHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 231
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemTablet;->d:Landroid/widget/ImageButton;

    iget-object v3, p0, Lflipboard/gui/section/item/ImageItemTablet;->d:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lflipboard/gui/section/item/ImageItemTablet;->d:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/widget/ImageButton;->layout(IIII)V

    .line 233
    :cond_2
    return-void

    .line 221
    :cond_3
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080008

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    const/4 v7, 0x1

    const/high16 v11, 0x40000000    # 2.0f

    const/high16 v10, -0x80000000

    .line 174
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 175
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 177
    int-to-float v0, v1

    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v3

    float-to-int v0, v0

    .line 178
    const/16 v3, 0x12c

    if-ge v0, v3, :cond_0

    iget-boolean v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->p:Z

    if-nez v0, :cond_0

    .line 179
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->e:Lflipboard/gui/section/Attribution;

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ImageItemTablet;->removeView(Landroid/view/View;)V

    .line 180
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f030023

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Attribution;

    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->e:Lflipboard/gui/section/Attribution;

    .line 181
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->e:Lflipboard/gui/section/Attribution;

    const v3, 0x7f0a008b

    invoke-interface {v0, v3}, Lflipboard/gui/section/Attribution;->setId(I)V

    .line 182
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->e:Lflipboard/gui/section/Attribution;

    iget-object v3, p0, Lflipboard/gui/section/item/ImageItemTablet;->q:Lflipboard/service/Section;

    iget-object v4, p0, Lflipboard/gui/section/item/ImageItemTablet;->a:Lflipboard/objs/FeedItem;

    invoke-interface {v0, v3, v4}, Lflipboard/gui/section/Attribution;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 183
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemTablet;->e:Lflipboard/gui/section/Attribution;

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ImageItemTablet;->addView(Landroid/view/View;)V

    .line 184
    iput-boolean v7, p0, Lflipboard/gui/section/item/ImageItemTablet;->p:Z

    .line 186
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemTablet;->getPaddingLeft()I

    move-result v0

    sub-int v0, v1, v0

    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemTablet;->getPaddingRight()I

    move-result v3

    sub-int/2addr v0, v3

    .line 187
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemTablet;->getPaddingTop()I

    move-result v3

    sub-int v3, v2, v3

    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemTablet;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    .line 188
    iget-object v4, p0, Lflipboard/gui/section/item/ImageItemTablet;->e:Lflipboard/gui/section/Attribution;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lflipboard/gui/section/Attribution;->setDrawTopDivider(Z)V

    .line 189
    iget v4, p0, Lflipboard/gui/section/item/ImageItemTablet;->m:I

    mul-int/lit8 v4, v4, 0x2

    sub-int v4, v0, v4

    .line 190
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemTablet;->getPaddingBottom()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    sub-int v5, v3, v5

    .line 192
    iget-object v6, p0, Lflipboard/gui/section/item/ImageItemTablet;->e:Lflipboard/gui/section/Attribution;

    invoke-interface {v6, v7}, Lflipboard/gui/section/Attribution;->setInverted(Z)V

    .line 193
    iget-object v6, p0, Lflipboard/gui/section/item/ImageItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-static {v0, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-static {v3, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 194
    iget-object v6, p0, Lflipboard/gui/section/item/ImageItemTablet;->e:Lflipboard/gui/section/Attribution;

    iget v7, p0, Lflipboard/gui/section/item/ImageItemTablet;->m:I

    mul-int/lit8 v7, v7, 0x2

    sub-int v7, v0, v7

    invoke-static {v7, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    iget v8, p0, Lflipboard/gui/section/item/ImageItemTablet;->m:I

    mul-int/lit8 v8, v8, 0x2

    sub-int v8, v3, v8

    invoke-static {v8, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-interface {v6, v7, v8}, Lflipboard/gui/section/Attribution;->measure(II)V

    .line 195
    iget-object v6, p0, Lflipboard/gui/section/item/ImageItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v6

    if-nez v6, :cond_1

    .line 196
    iget-object v6, p0, Lflipboard/gui/section/item/ImageItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-static {v4, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v5, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v6, v4, v5}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 197
    iget-object v4, p0, Lflipboard/gui/section/item/ImageItemTablet;->i:Landroid/view/View;

    iget-object v5, p0, Lflipboard/gui/section/item/ImageItemTablet;->e:Lflipboard/gui/section/Attribution;

    invoke-interface {v5}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v5

    iget-object v6, p0, Lflipboard/gui/section/item/ImageItemTablet;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    int-to-double v6, v5

    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v6, v8

    double-to-int v5, v6

    invoke-static {v5, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v4, p1, v5}, Landroid/view/View;->measure(II)V

    .line 199
    :cond_1
    iget-object v4, p0, Lflipboard/gui/section/item/ImageItemTablet;->d:Landroid/widget/ImageButton;

    if-eqz v4, :cond_2

    .line 200
    iget-object v4, p0, Lflipboard/gui/section/item/ImageItemTablet;->d:Landroid/widget/ImageButton;

    invoke-static {v0, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v3, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v4, v0, v3}, Landroid/widget/ImageButton;->measure(II)V

    .line 202
    :cond_2
    invoke-virtual {p0, v1, v2}, Lflipboard/gui/section/item/ImageItemTablet;->setMeasuredDimension(II)V

    .line 203
    return-void
.end method

.method public setInverted(Z)V
    .locals 0

    .prologue
    .line 112
    iput-boolean p1, p0, Lflipboard/gui/section/item/ImageItemTablet;->l:Z

    .line 113
    return-void
.end method

.method public setIsFullBleed(Z)V
    .locals 0

    .prologue
    .line 122
    iput-boolean p1, p0, Lflipboard/gui/section/item/ImageItemTablet;->o:Z

    .line 123
    return-void
.end method

.method public setIsOneUp(Z)V
    .locals 0

    .prologue
    .line 117
    iput-boolean p1, p0, Lflipboard/gui/section/item/ImageItemTablet;->n:Z

    .line 118
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 153
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    return-void
.end method
