.class Lflipboard/gui/section/item/GenericSuggestedFollowItem$1$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "GenericSuggestedFollowItem.java"


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;


# direct methods
.method constructor <init>(Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1$1;->b:Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;

    iput-object p2, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1$1;->a:Landroid/view/View;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    .line 223
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationStart(Landroid/animation/Animator;)V

    .line 225
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1$1;->b:Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;

    iget-object v0, v0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;->a:Lflipboard/gui/section/item/GenericSuggestedFollowItem;

    iget-object v0, v0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1$1;->b:Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;

    iget-object v0, v0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;->a:Lflipboard/gui/section/item/GenericSuggestedFollowItem;

    invoke-static {v0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a(Lflipboard/gui/section/item/GenericSuggestedFollowItem;)I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1$1;->b:Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;

    iget-object v1, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;->a:Lflipboard/gui/section/item/GenericSuggestedFollowItem;

    iget-object v1, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 226
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1$1;->b:Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;

    iget-object v0, v0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;->a:Lflipboard/gui/section/item/GenericSuggestedFollowItem;

    iget-object v1, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1$1;->b:Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;

    iget-object v1, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;->a:Lflipboard/gui/section/item/GenericSuggestedFollowItem;

    invoke-static {v1}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->b(Lflipboard/gui/section/item/GenericSuggestedFollowItem;)I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1$1;->a:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a(ILandroid/view/View;)Landroid/view/View;

    .line 234
    :goto_0
    return-void

    .line 228
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1$1;->b:Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;

    iget-object v0, v0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;->a:Lflipboard/gui/section/item/GenericSuggestedFollowItem;

    iget-object v0, v0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->b:Lflipboard/gui/swipe/SwipeableLinearLayout;

    iget-object v1, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1$1;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Lflipboard/gui/swipe/SwipeableLinearLayout;->removeView(Landroid/view/View;)V

    .line 229
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1$1;->a:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 230
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1$1;->b:Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;

    iget-object v0, v0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;->a:Lflipboard/gui/section/item/GenericSuggestedFollowItem;

    iget-object v0, v0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->b:Lflipboard/gui/swipe/SwipeableLinearLayout;

    iget-object v1, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1$1;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Lflipboard/gui/swipe/SwipeableLinearLayout;->addView(Landroid/view/View;)V

    .line 231
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1$1;->b:Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;

    iget-object v0, v0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;->a:Lflipboard/gui/section/item/GenericSuggestedFollowItem;

    invoke-static {v0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->c(Lflipboard/gui/section/item/GenericSuggestedFollowItem;)I

    .line 232
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1$1;->b:Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;

    iget-object v0, v0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;->a:Lflipboard/gui/section/item/GenericSuggestedFollowItem;

    invoke-virtual {v0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->invalidate()V

    goto :goto_0
.end method
