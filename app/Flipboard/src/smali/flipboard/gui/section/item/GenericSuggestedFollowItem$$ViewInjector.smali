.class public Lflipboard/gui/section/item/GenericSuggestedFollowItem$$ViewInjector;
.super Ljava/lang/Object;
.source "GenericSuggestedFollowItem$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/item/GenericSuggestedFollowItem;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a0344

    const-string v1, "field \'suggestedUserList\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/swipe/SwipeableLinearLayout;

    iput-object v0, p1, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->b:Lflipboard/gui/swipe/SwipeableLinearLayout;

    .line 12
    const v0, 0x7f0a0342

    const-string v1, "field \'headerTitle\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->c:Lflipboard/gui/FLTextView;

    .line 14
    const v0, 0x7f0a028e

    const-string v1, "field \'backgroundImage\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p1, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->d:Lflipboard/gui/FLImageView;

    .line 16
    const v0, 0x7f0a0341

    const-string v1, "field \'rowContainer\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p1, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->e:Landroid/widget/LinearLayout;

    .line 18
    return-void
.end method

.method public static reset(Lflipboard/gui/section/item/GenericSuggestedFollowItem;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    iput-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->b:Lflipboard/gui/swipe/SwipeableLinearLayout;

    .line 22
    iput-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->c:Lflipboard/gui/FLTextView;

    .line 23
    iput-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->d:Lflipboard/gui/FLImageView;

    .line 24
    iput-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->e:Landroid/widget/LinearLayout;

    .line 25
    return-void
.end method
