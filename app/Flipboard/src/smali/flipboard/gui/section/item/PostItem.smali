.class public Lflipboard/gui/section/item/PostItem;
.super Landroid/view/ViewGroup;
.source "PostItem.java"

# interfaces
.implements Lflipboard/gui/FLViewIntf;
.implements Lflipboard/gui/item/TabletItem;


# instance fields
.field public a:Lflipboard/gui/section/item/PostItem$ItemLayout;

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/PostItem;->setClipToPadding(Z)V

    .line 29
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Lflipboard/gui/section/item/PostItemTablet;

    invoke-direct {v0, p1, p0}, Lflipboard/gui/section/item/PostItemTablet;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    :goto_0
    iput-object v0, p0, Lflipboard/gui/section/item/PostItem;->a:Lflipboard/gui/section/item/PostItem$ItemLayout;

    .line 30
    return-void

    .line 29
    :cond_0
    new-instance v0, Lflipboard/gui/section/item/PostItemPhone;

    invoke-direct {v0, p1, p0}, Lflipboard/gui/section/item/PostItemPhone;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lflipboard/gui/section/item/PostItem;->a:Lflipboard/gui/section/item/PostItem$ItemLayout;

    invoke-interface {v0, p1, p2}, Lflipboard/gui/section/item/PostItem$ItemLayout;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 52
    return-void
.end method

.method public final a(ZI)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lflipboard/gui/section/item/PostItem;->a:Lflipboard/gui/section/item/PostItem$ItemLayout;

    invoke-interface {v0, p1, p2}, Lflipboard/gui/section/item/PostItem$ItemLayout;->a(ZI)V

    .line 76
    return-void
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lflipboard/gui/section/item/PostItem;->a:Lflipboard/gui/section/item/PostItem$ItemLayout;

    invoke-interface {v0}, Lflipboard/gui/section/item/PostItem$ItemLayout;->c()Lflipboard/objs/FeedItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemLayout()Lflipboard/gui/section/item/PostItem$ItemLayout;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lflipboard/gui/section/item/PostItem;->a:Lflipboard/gui/section/item/PostItem$ItemLayout;

    return-object v0
.end method

.method public getSponsoredActionButton()Lflipboard/gui/FLButton;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lflipboard/gui/section/item/PostItem;->a:Lflipboard/gui/section/item/PostItem$ItemLayout;

    invoke-interface {v0}, Lflipboard/gui/section/item/PostItem$ItemLayout;->f()Lflipboard/gui/FLButton;

    move-result-object v0

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 35
    invoke-virtual {p0}, Lflipboard/gui/section/item/PostItem;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 36
    iget-object v0, p0, Lflipboard/gui/section/item/PostItem;->a:Lflipboard/gui/section/item/PostItem$ItemLayout;

    invoke-interface {v0}, Lflipboard/gui/section/item/PostItem$ItemLayout;->a()V

    .line 37
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lflipboard/gui/section/item/PostItem;->a:Lflipboard/gui/section/item/PostItem$ItemLayout;

    invoke-interface {v0, p2, p3, p4, p5}, Lflipboard/gui/section/item/PostItem$ItemLayout;->a(IIII)V

    .line 71
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 61
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItem;->b:Z

    if-eqz v0, :cond_0

    .line 62
    const/high16 v0, 0x43fa0000    # 500.0f

    invoke-virtual {p0}, Lflipboard/gui/section/item/PostItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 64
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/section/item/PostItem;->setMeasuredDimension(II)V

    .line 65
    iget-object v0, p0, Lflipboard/gui/section/item/PostItem;->a:Lflipboard/gui/section/item/PostItem$ItemLayout;

    invoke-interface {v0, p1, p2}, Lflipboard/gui/section/item/PostItem$ItemLayout;->a(II)V

    .line 66
    return-void
.end method

.method public setCanFullBleed(Z)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lflipboard/gui/section/item/PostItem;->a:Lflipboard/gui/section/item/PostItem$ItemLayout;

    invoke-interface {v0, p1}, Lflipboard/gui/section/item/PostItem$ItemLayout;->c(Z)V

    .line 93
    return-void
.end method

.method public setIsFullBleed(Z)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lflipboard/gui/section/item/PostItem;->a:Lflipboard/gui/section/item/PostItem$ItemLayout;

    invoke-interface {v0, p1}, Lflipboard/gui/section/item/PostItem$ItemLayout;->a(Z)V

    .line 42
    return-void
.end method

.method public setIsGalleryPost(Z)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lflipboard/gui/section/item/PostItem;->a:Lflipboard/gui/section/item/PostItem$ItemLayout;

    invoke-interface {v0, p1}, Lflipboard/gui/section/item/PostItem$ItemLayout;->b(Z)V

    .line 47
    return-void
.end method
