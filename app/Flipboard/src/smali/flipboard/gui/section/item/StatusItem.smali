.class public Lflipboard/gui/section/item/StatusItem;
.super Landroid/view/ViewGroup;
.source "StatusItem.java"

# interfaces
.implements Lflipboard/gui/item/TabletItem;


# instance fields
.field a:Lflipboard/objs/FeedItem;

.field b:Lflipboard/gui/FLStaticTextView;

.field private final c:I

.field private d:Lflipboard/gui/section/Attribution;

.field private e:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    invoke-virtual {p0}, Lflipboard/gui/section/item/StatusItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/item/StatusItem;->c:I

    .line 40
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 115
    iput-object p2, p0, Lflipboard/gui/section/item/StatusItem;->a:Lflipboard/objs/FeedItem;

    .line 116
    iget-object v0, p2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "status"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "StatusItemView got a FeedItem that is not a status: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    :goto_0
    return-void

    .line 121
    :cond_0
    invoke-virtual {p0, p2}, Lflipboard/gui/section/item/StatusItem;->setTag(Ljava/lang/Object;)V

    .line 123
    iget-object v0, p2, Lflipboard/objs/FeedItem;->aJ:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lflipboard/objs/FeedItem;->aJ:Lflipboard/objs/FeedItem;

    .line 124
    :goto_1
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->D()Ljava/lang/String;

    move-result-object v1

    .line 125
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 126
    iget-object v0, p0, Lflipboard/gui/section/item/StatusItem;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    :cond_1
    :goto_2
    iget-object v0, p0, Lflipboard/gui/section/item/StatusItem;->d:Lflipboard/gui/section/Attribution;

    invoke-interface {v0, p1, p2}, Lflipboard/gui/section/Attribution;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    goto :goto_0

    :cond_2
    move-object v0, p2

    .line 123
    goto :goto_1

    .line 127
    :cond_3
    iget-object v1, v0, Lflipboard/objs/FeedItem;->aB:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lflipboard/objs/FeedItem;->aB:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 128
    iget-object v0, v0, Lflipboard/objs/FeedItem;->aB:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 129
    iget-object v1, p0, Lflipboard/gui/section/item/StatusItem;->b:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->b(Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lflipboard/gui/section/item/StatusItem;->a:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 46
    invoke-virtual {p0}, Lflipboard/gui/section/item/StatusItem;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 47
    const v0, 0x7f0a0234

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/StatusItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/StatusItem;->e:Landroid/view/View;

    .line 48
    const v0, 0x7f0a0202

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/StatusItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/StatusItem;->b:Lflipboard/gui/FLStaticTextView;

    .line 49
    const v0, 0x7f0a008b

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/StatusItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Attribution;

    iput-object v0, p0, Lflipboard/gui/section/item/StatusItem;->d:Lflipboard/gui/section/Attribution;

    .line 50
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 101
    sub-int v0, p5, p3

    .line 102
    iget-object v1, p0, Lflipboard/gui/section/item/StatusItem;->d:Lflipboard/gui/section/Attribution;

    invoke-interface {v1}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v1

    iget v2, p0, Lflipboard/gui/section/item/StatusItem;->c:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lflipboard/gui/section/item/StatusItem;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v2

    iget-object v3, p0, Lflipboard/gui/section/item/StatusItem;->e:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 103
    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 104
    invoke-virtual {p0}, Lflipboard/gui/section/item/StatusItem;->getPaddingLeft()I

    move-result v1

    .line 106
    iget-object v2, p0, Lflipboard/gui/section/item/StatusItem;->e:Landroid/view/View;

    iget-object v3, p0, Lflipboard/gui/section/item/StatusItem;->e:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lflipboard/gui/section/item/StatusItem;->e:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 107
    iget-object v2, p0, Lflipboard/gui/section/item/StatusItem;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v1

    .line 108
    iget-object v3, p0, Lflipboard/gui/section/item/StatusItem;->b:Lflipboard/gui/FLStaticTextView;

    iget-object v4, p0, Lflipboard/gui/section/item/StatusItem;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v2

    iget-object v5, p0, Lflipboard/gui/section/item/StatusItem;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v3, v2, v0, v4, v5}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 109
    iget-object v2, p0, Lflipboard/gui/section/item/StatusItem;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v2

    iget-object v3, p0, Lflipboard/gui/section/item/StatusItem;->e:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v3, p0, Lflipboard/gui/section/item/StatusItem;->c:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 110
    iget-object v2, p0, Lflipboard/gui/section/item/StatusItem;->d:Lflipboard/gui/section/Attribution;

    iget-object v3, p0, Lflipboard/gui/section/item/StatusItem;->d:Lflipboard/gui/section/Attribution;

    invoke-interface {v3}, Lflipboard/gui/section/Attribution;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lflipboard/gui/section/item/StatusItem;->d:Lflipboard/gui/section/Attribution;

    invoke-interface {v4}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-interface {v2, v1, v0, v3, v4}, Lflipboard/gui/section/Attribution;->layout(IIII)V

    .line 111
    return-void
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    .line 55
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 57
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/section/item/StatusItem;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lflipboard/gui/section/item/StatusItem;->getPaddingRight()I

    move-result v1

    sub-int v2, v0, v1

    .line 58
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/section/item/StatusItem;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lflipboard/gui/section/item/StatusItem;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 60
    iget-object v1, p0, Lflipboard/gui/section/item/StatusItem;->e:Landroid/view/View;

    const/high16 v3, -0x80000000

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/high16 v4, -0x80000000

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/view/View;->measure(II)V

    .line 61
    iget-object v1, p0, Lflipboard/gui/section/item/StatusItem;->d:Lflipboard/gui/section/Attribution;

    const/high16 v3, -0x80000000

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/high16 v4, -0x80000000

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-interface {v1, v3, v4}, Lflipboard/gui/section/Attribution;->measure(II)V

    .line 62
    iget-object v1, p0, Lflipboard/gui/section/item/StatusItem;->d:Lflipboard/gui/section/Attribution;

    invoke-interface {v1}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lflipboard/gui/section/item/StatusItem;->c:I

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 64
    invoke-virtual {p0}, Lflipboard/gui/section/item/StatusItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    .line 65
    int-to-float v0, v2

    div-float v5, v0, v4

    .line 66
    int-to-float v0, v3

    div-float/2addr v0, v4

    .line 67
    mul-float v6, v5, v0

    .line 68
    iget-object v0, p0, Lflipboard/gui/section/item/StatusItem;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->D()Ljava/lang/String;

    move-result-object v7

    .line 69
    const/4 v1, 0x0

    .line 70
    const/4 v0, 0x0

    .line 71
    if-eqz v7, :cond_0

    .line 72
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900f4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    float-to-int v0, v0

    .line 73
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v8, 0x7f0900f5

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    float-to-int v8, v1

    .line 74
    int-to-float v1, v0

    div-float v1, v5, v1

    float-to-int v1, v1

    .line 75
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    int-to-float v9, v9

    int-to-float v1, v1

    div-float v1, v9, v1

    float-to-double v10, v1

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v1, v10

    .line 76
    invoke-virtual {p0}, Lflipboard/gui/section/item/StatusItem;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    .line 77
    int-to-float v1, v1

    mul-float/2addr v1, v5

    int-to-float v0, v0

    mul-float/2addr v0, v1

    iget v1, v9, Landroid/util/DisplayMetrics;->scaledDensity:F

    mul-float/2addr v0, v1

    div-float/2addr v0, v4

    float-to-int v1, v0

    .line 78
    int-to-float v0, v8

    div-float v0, v5, v0

    float-to-int v0, v0

    .line 79
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    int-to-float v7, v7

    int-to-float v0, v0

    div-float v0, v7, v0

    float-to-double v10, v0

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v0, v10

    .line 80
    int-to-float v0, v0

    mul-float/2addr v0, v5

    int-to-float v5, v8

    mul-float/2addr v0, v5

    iget v5, v9, Landroid/util/DisplayMetrics;->scaledDensity:F

    mul-float/2addr v0, v5

    div-float/2addr v0, v4

    float-to-int v0, v0

    .line 82
    :cond_0
    int-to-float v1, v1

    cmpl-float v1, v6, v1

    if-ltz v1, :cond_1

    const v1, 0x481c4000    # 160000.0f

    cmpl-float v1, v6, v1

    if-lez v1, :cond_1

    .line 83
    invoke-virtual {p0}, Lflipboard/gui/section/item/StatusItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900f4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/StatusItem;->setTextSizes(I)V

    .line 89
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/item/StatusItem;->b:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p0, Lflipboard/gui/section/item/StatusItem;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v2, v1

    const/high16 v2, -0x80000000

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    const/high16 v2, -0x80000000

    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 91
    return-void

    .line 84
    :cond_1
    int-to-float v0, v0

    cmpl-float v0, v6, v0

    if-ltz v0, :cond_2

    .line 85
    invoke-virtual {p0}, Lflipboard/gui/section/item/StatusItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900f5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/StatusItem;->setTextSizes(I)V

    goto :goto_0

    .line 87
    :cond_2
    invoke-virtual {p0}, Lflipboard/gui/section/item/StatusItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/StatusItem;->setTextSizes(I)V

    goto :goto_0
.end method

.method setTextSizes(I)V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lflipboard/gui/section/item/StatusItem;->b:Lflipboard/gui/FLStaticTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lflipboard/gui/FLStaticTextView;->a(II)V

    .line 96
    return-void
.end method
