.class Lflipboard/gui/section/item/GenericSuggestedFollowItem$3;
.super Ljava/lang/Object;
.source "GenericSuggestedFollowItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/section/item/GenericSuggestedFollowItem;


# direct methods
.method constructor <init>(Lflipboard/gui/section/item/GenericSuggestedFollowItem;)V
    .locals 0

    .prologue
    .line 303
    iput-object p1, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$3;->a:Lflipboard/gui/section/item/GenericSuggestedFollowItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 306
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;

    .line 307
    new-instance v1, Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->a:Lflipboard/objs/FeedSectionLink;

    invoke-direct {v1, v0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    .line 308
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$3;->a:Lflipboard/gui/section/item/GenericSuggestedFollowItem;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a(Lflipboard/gui/section/item/GenericSuggestedFollowItem;Ljava/lang/String;)V

    .line 309
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 310
    if-eqz v0, :cond_0

    .line 311
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 312
    const-string v3, "source"

    sget-object v4, Lflipboard/objs/UsageEventV2$SectionNavFrom;->k:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    invoke-virtual {v4}, Lflipboard/objs/UsageEventV2$SectionNavFrom;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    invoke-static {v1, v0, v2}, Lflipboard/util/SocialHelper;->a(Lflipboard/service/Section;Landroid/content/Context;Landroid/os/Bundle;)V

    .line 315
    :cond_0
    return-void
.end method
