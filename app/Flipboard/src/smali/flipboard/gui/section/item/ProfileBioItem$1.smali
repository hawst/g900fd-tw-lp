.class Lflipboard/gui/section/item/ProfileBioItem$1;
.super Ljava/lang/Object;
.source "ProfileBioItem.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/FlipboardManager;",
        "Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/gui/section/item/ProfileBioItem;


# direct methods
.method constructor <init>(Lflipboard/gui/section/item/ProfileBioItem;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lflipboard/gui/section/item/ProfileBioItem$1;->a:Lflipboard/gui/section/item/ProfileBioItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 73
    check-cast p2, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    sget-object v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->e:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem$1;->a:Lflipboard/gui/section/item/ProfileBioItem;

    invoke-static {v0}, Lflipboard/gui/section/item/ProfileBioItem;->a(Lflipboard/gui/section/item/ProfileBioItem;)Lflipboard/service/Section;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lflipboard/service/Section$Meta;->i:Lflipboard/objs/FeedSectionLink;

    if-eqz v0, :cond_1

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v2, "flipboard"

    invoke-virtual {v1, v2}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    iget-object v2, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    iput-object v2, v0, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    iget-object v2, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    iput-object v2, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    iget-object v2, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->s:Ljava/lang/String;

    iput-object v2, v0, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    iget-object v2, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v2}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    if-eqz v2, :cond_2

    iget-object v0, v0, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    iget-object v1, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v1}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem$1;->a:Lflipboard/gui/section/item/ProfileBioItem;

    invoke-static {v0}, Lflipboard/gui/section/item/ProfileBioItem;->a(Lflipboard/gui/section/item/ProfileBioItem;)Lflipboard/service/Section;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/service/Section;->w()V

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/item/ProfileBioItem$1$1;

    invoke-direct {v1, p0}, Lflipboard/gui/section/item/ProfileBioItem$1$1;-><init>(Lflipboard/gui/section/item/ProfileBioItem$1;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    :cond_1
    return-void

    :cond_2
    new-instance v2, Lflipboard/objs/Image;

    invoke-direct {v2}, Lflipboard/objs/Image;-><init>()V

    iget-object v1, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v1}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lflipboard/objs/Image;->a:Ljava/lang/String;

    iput-object v2, v0, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    goto :goto_0
.end method
