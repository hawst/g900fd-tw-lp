.class public Lflipboard/gui/section/item/PostItemPhone;
.super Ljava/lang/Object;
.source "PostItemPhone.java"

# interfaces
.implements Lflipboard/gui/section/item/PostItem$ItemLayout;


# instance fields
.field private A:Z

.field private B:Lflipboard/gui/section/scrolling/ItemActionBar;

.field a:Lflipboard/objs/FeedItem;

.field b:Lflipboard/service/Section;

.field c:Lflipboard/gui/FLStaticTextView;

.field public d:Lflipboard/gui/FLStaticTextView;

.field public e:Lflipboard/gui/FLImageViewGroup;

.field f:Lflipboard/gui/FLTextView;

.field g:Lflipboard/gui/FLButton;

.field h:Lflipboard/gui/FLLabelTextView;

.field i:Lflipboard/gui/section/Attribution;

.field j:Lflipboard/gui/section/Attribution;

.field k:Landroid/view/View;

.field l:Landroid/view/View;

.field public m:Lflipboard/gui/section/item/PostItemPhone$Layout;

.field public final n:Landroid/view/ViewGroup;

.field public o:Z

.field public p:Lflipboard/gui/section/AttributionPublisher;

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Landroid/view/ViewGroup$LayoutParams;

.field private w:Landroid/graphics/drawable/Drawable;

.field private final x:Lflipboard/activities/FlipboardActivity;

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-boolean v1, p0, Lflipboard/gui/section/item/PostItemPhone;->r:Z

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/item/PostItemPhone;->u:Z

    .line 76
    iput v1, p0, Lflipboard/gui/section/item/PostItemPhone;->y:I

    move-object v0, p1

    .line 90
    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->x:Lflipboard/activities/FlipboardActivity;

    .line 91
    iput-object p2, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    .line 92
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    .line 93
    return-void
.end method

.method public static a(IILflipboard/objs/FeedItem;ZZZ)Lflipboard/gui/section/item/PostItemPhone$Layout;
    .locals 14

    .prologue
    .line 816
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    .line 817
    int-to-float v0, p0

    div-float/2addr v0, v4

    float-to-int v5, v0

    .line 818
    int-to-float v0, p1

    div-float/2addr v0, v4

    float-to-int v6, v0

    .line 820
    if-eqz p5, :cond_0

    .line 821
    sget-object v0, Lflipboard/gui/section/item/PostItemPhone$Layout;->d:Lflipboard/gui/section/item/PostItemPhone$Layout;

    .line 873
    :goto_0
    return-object v0

    .line 825
    :cond_0
    if-eqz p3, :cond_1

    .line 826
    sget-object v0, Lflipboard/gui/section/item/PostItemPhone$Layout;->e:Lflipboard/gui/section/item/PostItemPhone$Layout;

    goto :goto_0

    .line 828
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lflipboard/objs/FeedItem;->i()Z

    move-result v0

    if-nez v0, :cond_2

    .line 829
    sget-object v0, Lflipboard/gui/section/item/PostItemPhone$Layout;->a:Lflipboard/gui/section/item/PostItemPhone$Layout;

    goto :goto_0

    .line 831
    :cond_2
    if-eqz p0, :cond_3

    if-eqz p1, :cond_3

    mul-int v0, v5, v6

    const v1, 0x8ca0

    if-ge v0, v1, :cond_4

    .line 832
    :cond_3
    sget-object v0, Lflipboard/gui/section/item/PostItemPhone$Layout;->a:Lflipboard/gui/section/item/PostItemPhone$Layout;

    goto :goto_0

    .line 835
    :cond_4
    invoke-virtual/range {p2 .. p2}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v7

    .line 836
    if-eqz v7, :cond_6

    iget v0, v7, Lflipboard/objs/Image;->f:I

    move v3, v0

    .line 837
    :goto_1
    if-eqz v7, :cond_7

    iget v0, v7, Lflipboard/objs/Image;->g:I

    move v2, v0

    .line 839
    :goto_2
    if-eqz v3, :cond_5

    if-nez v2, :cond_8

    .line 840
    :cond_5
    sget-object v0, Lflipboard/gui/section/item/PostItemPhone$Layout;->a:Lflipboard/gui/section/item/PostItemPhone$Layout;

    goto :goto_0

    .line 836
    :cond_6
    const/4 v0, 0x0

    move v3, v0

    goto :goto_1

    .line 837
    :cond_7
    const/4 v0, 0x0

    move v2, v0

    goto :goto_2

    .line 843
    :cond_8
    int-to-float v0, v2

    int-to-float v1, v3

    div-float/2addr v0, v1

    int-to-float v1, p0

    mul-float v8, v0, v1

    .line 844
    div-float v9, v8, v4

    .line 845
    int-to-double v0, v3

    int-to-double v10, v5

    const-wide v12, 0x3ff4cccccccccccdL    # 1.3

    div-double/2addr v10, v12

    cmpl-double v0, v0, v10

    if-lez v0, :cond_9

    const/4 v0, 0x1

    move v1, v0

    .line 846
    :goto_3
    if-eqz p4, :cond_a

    int-to-float v0, v6

    div-float v0, v9, v0

    float-to-double v10, v0

    const-wide v12, 0x3fe999999999999aL    # 0.8

    cmpl-double v0, v10, v12

    if-lez v0, :cond_a

    if-eqz v1, :cond_a

    invoke-virtual {v7}, Lflipboard/objs/Image;->b()Z

    move-result v0

    if-nez v0, :cond_a

    .line 848
    sget-object v0, Lflipboard/gui/section/item/PostItemPhone$Layout;->e:Lflipboard/gui/section/item/PostItemPhone$Layout;

    goto :goto_0

    .line 845
    :cond_9
    const/4 v0, 0x0

    move v1, v0

    goto :goto_3

    .line 850
    :cond_a
    int-to-float v0, p1

    sub-float/2addr v0, v8

    div-float/2addr v0, v4

    float-to-int v4, v0

    .line 852
    const/4 v0, 0x0

    .line 853
    invoke-virtual/range {p2 .. p2}, Lflipboard/objs/FeedItem;->I()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_c

    .line 854
    invoke-virtual/range {p2 .. p2}, Lflipboard/objs/FeedItem;->I()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x3

    if-le v7, v8, :cond_b

    .line 855
    const/16 v0, 0x28

    .line 857
    :cond_b
    invoke-virtual/range {p2 .. p2}, Lflipboard/objs/FeedItem;->I()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0x28

    if-le v7, v8, :cond_c

    .line 858
    add-int/lit8 v0, v0, 0x28

    .line 862
    :cond_c
    if-le v4, v0, :cond_d

    if-le v3, v2, :cond_d

    if-eqz v1, :cond_d

    .line 863
    sget-object v0, Lflipboard/gui/section/item/PostItemPhone$Layout;->d:Lflipboard/gui/section/item/PostItemPhone$Layout;

    goto/16 :goto_0

    .line 866
    :cond_d
    int-to-float v0, v3

    int-to-float v1, v2

    div-float/2addr v0, v1

    .line 868
    int-to-float v1, v6

    mul-float/2addr v0, v1

    .line 870
    const/16 v1, 0xb4

    if-lt v6, v1, :cond_e

    int-to-float v1, v5

    div-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_f

    .line 871
    :cond_e
    sget-object v0, Lflipboard/gui/section/item/PostItemPhone$Layout;->c:Lflipboard/gui/section/item/PostItemPhone$Layout;

    goto/16 :goto_0

    .line 873
    :cond_f
    sget-object v0, Lflipboard/gui/section/item/PostItemPhone$Layout;->b:Lflipboard/gui/section/item/PostItemPhone$Layout;

    goto/16 :goto_0
.end method

.method static synthetic a(Lflipboard/gui/section/item/PostItemPhone;)V
    .locals 4

    .prologue
    .line 50
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->a:Lflipboard/objs/FeedItem;

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->b:Lflipboard/service/Section;

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->x:Lflipboard/activities/FlipboardActivity;

    sget-object v3, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->b:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    invoke-static {v0, v1, v2, v3}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/UsageEventV2$SocialCardNavFrom;)V

    return-void
.end method

.method static synthetic b(Lflipboard/gui/section/item/PostItemPhone;)Lflipboard/gui/section/scrolling/ItemActionBar;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    return-object v0
.end method

.method private g()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 239
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->k:Landroid/view/View;

    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v5, Lflipboard/gui/section/item/PostItemPhone$Layout;->e:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v0, v5, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 240
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v5, Lflipboard/gui/section/item/PostItemPhone$Layout;->e:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v0, v5, :cond_4

    move v0, v2

    :goto_1
    invoke-virtual {v4, v0}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    invoke-interface {v0}, Lflipboard/gui/section/Attribution;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 244
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v5, Lflipboard/gui/section/item/PostItemPhone$Layout;->e:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v0, v5, :cond_5

    move v0, v3

    :goto_2
    invoke-interface {v4, v0}, Lflipboard/gui/section/Attribution;->setInverted(Z)V

    .line 246
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    invoke-interface {v0}, Lflipboard/gui/section/Attribution;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 247
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v5, Lflipboard/gui/section/item/PostItemPhone$Layout;->e:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v0, v5, :cond_6

    move v0, v3

    :goto_3
    invoke-interface {v4, v0}, Lflipboard/gui/section/Attribution;->setInverted(Z)V

    .line 248
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v5, Lflipboard/gui/section/item/PostItemPhone$Layout;->e:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v4, v5, :cond_7

    :goto_4
    invoke-virtual {v0, v3}, Lflipboard/gui/section/scrolling/ItemActionBar;->setInverted(Z)V

    .line 250
    :cond_1
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v4, Lflipboard/gui/section/item/PostItemPhone$Layout;->e:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v0, v4, :cond_8

    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0800aa

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_5
    invoke-virtual {v3, v0}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    .line 251
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v3, Lflipboard/gui/section/item/PostItemPhone$Layout;->b:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-eq v0, v3, :cond_9

    .line 252
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1, v1}, Lflipboard/gui/FLStaticTextView;->a(Lflipboard/gui/FLStaticTextView$BlockType;II)V

    .line 256
    :cond_2
    :goto_6
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v4, Lflipboard/gui/section/item/PostItemPhone$Layout;->a:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v0, v4, :cond_a

    move v0, v2

    :goto_7
    invoke-virtual {v3, v0}, Lflipboard/gui/FLImageViewGroup;->setVisibility(I)V

    .line 258
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->a:Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->bN:Z

    if-eqz v0, :cond_b

    move v0, v1

    :goto_8
    invoke-virtual {v3, v0}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 259
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->a:Lflipboard/objs/FeedItem;

    iget-boolean v3, v3, Lflipboard/objs/FeedItem;->bN:Z

    if-eqz v3, :cond_c

    :goto_9
    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setVisibility(I)V

    .line 260
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 261
    return-void

    :cond_3
    move v0, v2

    .line 239
    goto/16 :goto_0

    :cond_4
    move v0, v1

    .line 240
    goto/16 :goto_1

    :cond_5
    move v0, v1

    .line 244
    goto :goto_2

    :cond_6
    move v0, v1

    .line 247
    goto :goto_3

    :cond_7
    move v3, v1

    .line 248
    goto :goto_4

    .line 250
    :cond_8
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f080008

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_5

    .line 253
    :cond_9
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v3, Lflipboard/gui/section/item/PostItemPhone$Layout;->c:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-eq v0, v3, :cond_2

    .line 254
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    goto :goto_6

    :cond_a
    move v0, v1

    .line 256
    goto :goto_7

    :cond_b
    move v0, v2

    .line 258
    goto :goto_8

    :cond_c
    move v1, v2

    .line 259
    goto :goto_9
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 97
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    const v1, 0x7f0a004f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    .line 98
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    const v1, 0x7f0a020f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    .line 99
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    const v1, 0x7f0a0040

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageViewGroup;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    .line 100
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    const v1, 0x7f0a0205

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    .line 101
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    const v1, 0x7f0a0210

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    .line 102
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0}, Lflipboard/gui/FLImageViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->v:Landroid/view/ViewGroup$LayoutParams;

    .line 103
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0}, Lflipboard/gui/FLImageViewGroup;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->w:Landroid/graphics/drawable/Drawable;

    .line 104
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    const v1, 0x7f0a0065

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->k:Landroid/view/View;

    .line 105
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    const v1, 0x7f0a0212

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->l:Landroid/view/View;

    .line 106
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    const v1, 0x7f0a0211

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    .line 107
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->x:Lflipboard/activities/FlipboardActivity;

    const v1, 0x7f030021

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionServiceInfo;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    .line 108
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->x:Lflipboard/activities/FlipboardActivity;

    const v1, 0x7f030023

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionSmall;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    .line 109
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 110
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 112
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->x:Lflipboard/activities/FlipboardActivity;

    const v1, 0x7f03009a

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/ItemActionBar;

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    .line 113
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 114
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 115
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 224
    iput p1, p0, Lflipboard/gui/section/item/PostItemPhone;->y:I

    .line 225
    return-void
.end method

.method public final a(II)V
    .locals 14

    .prologue
    .line 274
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    .line 275
    if-eqz v1, :cond_1a

    iget v0, v1, Lflipboard/objs/Image;->f:I

    move v10, v0

    .line 276
    :goto_0
    if-eqz v1, :cond_1b

    iget v0, v1, Lflipboard/objs/Image;->g:I

    move v6, v0

    .line 277
    :goto_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v11

    .line 278
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    .line 279
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v0

    sub-int v0, v11, v0

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 280
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v1

    sub-int v1, v9, v1

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v2

    sub-int v12, v1, v2

    .line 281
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->x:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v1}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v13, v1, Landroid/util/DisplayMetrics;->density:F

    .line 284
    const/4 v1, 0x0

    cmpl-float v1, v13, v1

    if-lez v1, :cond_1c

    .line 285
    int-to-float v1, v0

    div-float v2, v1, v13

    .line 286
    int-to-float v1, v12

    div-float/2addr v1, v13

    move v7, v1

    move v8, v2

    .line 292
    :goto_2
    int-to-float v1, v9

    div-float/2addr v1, v13

    float-to-int v1, v1

    .line 294
    sget v2, Lflipboard/gui/section/AttributionServiceInfo;->s:I

    if-le v1, v2, :cond_1d

    const/high16 v1, 0x43960000    # 300.0f

    cmpl-float v1, v8, v1

    if-lez v1, :cond_1d

    .line 295
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lflipboard/gui/section/Attribution;->setVisibility(I)V

    .line 296
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lflipboard/gui/section/scrolling/ItemActionBar;->setVisibility(I)V

    .line 297
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    const/16 v2, 0x8

    invoke-interface {v1, v2}, Lflipboard/gui/section/Attribution;->setVisibility(I)V

    .line 298
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v11, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/high16 v3, -0x80000000

    invoke-static {v12, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-interface {v1, v2, v3}, Lflipboard/gui/section/Attribution;->measure(II)V

    .line 299
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v11, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/high16 v3, -0x80000000

    invoke-static {v12, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lflipboard/gui/section/scrolling/ItemActionBar;->measure(II)V

    .line 306
    :goto_3
    const/high16 v1, 0x430c0000    # 140.0f

    cmpg-float v1, v7, v1

    if-gez v1, :cond_0

    .line 308
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lflipboard/gui/section/AttributionPublisher;->setVisibility(I)V

    .line 310
    :cond_0
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v1}, Lflipboard/gui/section/AttributionPublisher;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_3c

    .line 311
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/high16 v3, -0x80000000

    invoke-static {v12, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lflipboard/gui/section/AttributionPublisher;->measure(II)V

    .line 312
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v1}, Lflipboard/gui/section/AttributionPublisher;->getMeasuredHeight()I

    move-result v1

    iget v2, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    add-int/2addr v1, v2

    sub-int v1, v9, v1

    .line 315
    :goto_4
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_3

    .line 317
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v3, Lflipboard/gui/section/item/PostItemPhone$Layout;->b:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v3, Lflipboard/gui/section/item/PostItemPhone$Layout;->a:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v3, Lflipboard/gui/section/item/PostItemPhone$Layout;->c:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v2, v3, :cond_2

    .line 318
    :cond_1
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900ae

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 319
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {v3, v2, v2, v2, v2}, Lflipboard/gui/FLTextView;->setPadding(IIII)V

    .line 321
    :cond_2
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 324
    :cond_3
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    invoke-virtual {v2}, Lflipboard/gui/FLButton;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_1e

    .line 325
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lflipboard/gui/FLButton;->measure(II)V

    .line 326
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    invoke-virtual {v2}, Lflipboard/gui/FLButton;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 339
    :cond_4
    :goto_5
    iget-boolean v2, p0, Lflipboard/gui/section/item/PostItemPhone;->o:Z

    if-nez v2, :cond_5

    .line 340
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->a:Lflipboard/objs/FeedItem;

    iget-boolean v3, p0, Lflipboard/gui/section/item/PostItemPhone;->q:Z

    iget-boolean v4, p0, Lflipboard/gui/section/item/PostItemPhone;->A:Z

    iget-boolean v5, p0, Lflipboard/gui/section/item/PostItemPhone;->r:Z

    invoke-static/range {v0 .. v5}, Lflipboard/gui/section/item/PostItemPhone;->a(IILflipboard/objs/FeedItem;ZZZ)Lflipboard/gui/section/item/PostItemPhone$Layout;

    move-result-object v2

    .line 341
    invoke-virtual {p0, v2}, Lflipboard/gui/section/item/PostItemPhone;->a(Lflipboard/gui/section/item/PostItemPhone$Layout;)V

    .line 342
    invoke-direct {p0}, Lflipboard/gui/section/item/PostItemPhone;->g()V

    .line 345
    :cond_5
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v3, Lflipboard/gui/section/item/PostItemPhone$Layout;->e:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v2, v3, :cond_9

    .line 346
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->k:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 347
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    invoke-interface {v2}, Lflipboard/gui/section/Attribution;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_6

    .line 348
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lflipboard/gui/section/Attribution;->setInverted(Z)V

    .line 350
    :cond_6
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    invoke-interface {v2}, Lflipboard/gui/section/Attribution;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_7

    .line 351
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lflipboard/gui/section/Attribution;->setInverted(Z)V

    .line 353
    :cond_7
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v2}, Lflipboard/gui/section/scrolling/ItemActionBar;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_8

    .line 354
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lflipboard/gui/section/scrolling/ItemActionBar;->setInverted(Z)V

    .line 356
    :cond_8
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0800aa

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    .line 359
    :cond_9
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v3, Lflipboard/gui/section/item/PostItemPhone$Layout;->a:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-eq v2, v3, :cond_a

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v3, Lflipboard/gui/section/item/PostItemPhone$Layout;->b:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v2, v3, :cond_b

    .line 360
    :cond_a
    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v3

    sub-int/2addr v1, v3

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 363
    :cond_b
    if-lez v10, :cond_e

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v3, Lflipboard/gui/section/item/PostItemPhone$Layout;->d:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v2, v3, :cond_e

    .line 364
    iget-boolean v2, p0, Lflipboard/gui/section/item/PostItemPhone;->t:Z

    if-eqz v2, :cond_c

    .line 366
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    const/high16 v3, -0x80000000

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    div-int/lit8 v4, v1, 0x2

    const/high16 v5, -0x80000000

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 367
    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v1, v3

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 371
    :cond_c
    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v2, v13

    float-to-int v2, v2

    .line 372
    iget-boolean v3, p0, Lflipboard/gui/section/item/PostItemPhone;->r:Z

    if-eqz v3, :cond_21

    sub-int v2, v1, v2

    .line 373
    :goto_6
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v11, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/high16 v5, -0x80000000

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v3, v4, v2}, Lflipboard/gui/FLImageViewGroup;->measure(II)V

    .line 375
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v3}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v3

    if-le v2, v3, :cond_d

    .line 376
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v3}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v3

    const/high16 v4, -0x80000000

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 379
    :cond_d
    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v3}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v1, v3

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 382
    :cond_e
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->a:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v2, :cond_22

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->a:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    .line 384
    :goto_7
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v4, Lflipboard/gui/section/item/PostItemPhone$Layout;->d:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v3, v4, :cond_3b

    .line 385
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v3}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v13

    sub-float v3, v7, v3

    .line 389
    :goto_8
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v5, Lflipboard/gui/section/item/PostItemPhone$Layout;->a:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-eq v4, v5, :cond_23

    const/4 v4, 0x1

    .line 390
    :goto_9
    if-eqz v4, :cond_24

    const/high16 v4, 0x43960000    # 300.0f

    cmpg-float v4, v8, v4

    if-ltz v4, :cond_f

    const/high16 v4, 0x43020000    # 130.0f

    cmpg-float v4, v3, v4

    if-gez v4, :cond_24

    .line 391
    :cond_f
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    const/4 v4, 0x0

    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->x:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v5}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0900f0

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lflipboard/gui/FLStaticTextView;->a(II)V

    .line 401
    :goto_a
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v4, Lflipboard/gui/section/item/PostItemPhone$Layout;->e:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v2, v4, :cond_29

    .line 402
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v11, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v9, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lflipboard/gui/FLImageViewGroup;->measure(II)V

    .line 403
    const/4 v2, 0x0

    .line 404
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    invoke-interface {v4}, Lflipboard/gui/section/Attribution;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_10

    .line 405
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    invoke-interface {v2}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    .line 406
    iget-boolean v4, p0, Lflipboard/gui/section/item/PostItemPhone;->q:Z

    if-eqz v4, :cond_10

    .line 407
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09000b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v2, v4

    .line 410
    :cond_10
    if-lez v2, :cond_28

    .line 411
    iget v4, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    mul-int/lit8 v4, v4, 0x6

    add-int/2addr v2, v4

    .line 412
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->l:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 413
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->l:Landroid/view/View;

    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v5}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v5

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v4, v5, v2}, Landroid/view/View;->measure(II)V

    .line 417
    :goto_b
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_11

    .line 418
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    const/high16 v4, -0x80000000

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/high16 v5, -0x80000000

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 419
    const/4 v2, 0x0

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v1, v4

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 421
    :cond_11
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v2

    iget v4, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    mul-int/lit8 v4, v4, 0x6

    add-int/2addr v2, v4

    .line 424
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    invoke-interface {v4}, Lflipboard/gui/section/Attribution;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_12

    .line 425
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    invoke-interface {v4}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v2, v4

    .line 427
    :cond_12
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v4}, Lflipboard/gui/section/scrolling/ItemActionBar;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_13

    .line 428
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v4}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v2, v4

    .line 431
    :cond_13
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->k:Landroid/view/View;

    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v5}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v5

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v4, v5, v2}, Landroid/view/View;->measure(II)V

    .line 438
    :cond_14
    :goto_c
    const/4 v2, 0x0

    iget v4, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    sub-int/2addr v1, v4

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 440
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v2, Lflipboard/gui/section/item/PostItemPhone$Layout;->b:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v1, v2, :cond_2f

    .line 441
    div-int/lit8 v2, v0, 0x2

    .line 442
    mul-int v1, v2, v6

    div-int v3, v1, v10

    .line 443
    const/4 v1, 0x0

    iget v5, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    sub-int/2addr v4, v5

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 444
    if-le v3, v4, :cond_2b

    .line 445
    mul-int v1, v4, v10

    div-int/2addr v1, v6

    .line 447
    iget v2, p0, Lflipboard/gui/section/item/PostItemPhone;->y:I

    if-eqz v2, :cond_3a

    .line 448
    iget v1, p0, Lflipboard/gui/section/item/PostItemPhone;->y:I

    move v2, v1

    .line 450
    :goto_d
    const/16 v1, 0x64

    if-le v2, v1, :cond_2a

    .line 451
    const/4 v1, 0x0

    .line 452
    iget-boolean v3, p0, Lflipboard/gui/section/item/PostItemPhone;->t:Z

    if-eqz v3, :cond_15

    .line 453
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    const/high16 v3, -0x80000000

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/high16 v5, -0x80000000

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v1, v3, v5}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 454
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    .line 456
    :cond_15
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    sub-int v1, v4, v1

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v3, v2, v1}, Lflipboard/gui/FLImageViewGroup;->measure(II)V

    .line 457
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v1

    iput v1, p0, Lflipboard/gui/section/item/PostItemPhone;->y:I

    .line 481
    :cond_16
    :goto_e
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_18

    .line 482
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v1

    iget v2, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    add-int/2addr v1, v2

    .line 483
    iget-boolean v2, p0, Lflipboard/gui/section/item/PostItemPhone;->t:Z

    if-eqz v2, :cond_17

    .line 484
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    .line 486
    :cond_17
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    sget-object v3, Lflipboard/gui/FLStaticTextView$BlockType;->a:Lflipboard/gui/FLStaticTextView$BlockType;

    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v5}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v5

    sub-int v5, v0, v5

    iget v6, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    sub-int/2addr v5, v6

    invoke-virtual {v2, v3, v5, v1}, Lflipboard/gui/FLStaticTextView;->a(Lflipboard/gui/FLStaticTextView$BlockType;II)V

    .line 487
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    const/high16 v2, -0x80000000

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    const/high16 v2, -0x80000000

    invoke-static {v4, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 488
    const/4 v0, 0x0

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v4, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    .line 490
    :cond_18
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v1, Lflipboard/gui/section/item/PostItemPhone$Layout;->b:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v0, v1, :cond_19

    .line 491
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionPublisher;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_19

    .line 493
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v11, v1

    iget v2, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    mul-int/lit8 v2, v2, 0x3

    sub-int/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v2}, Lflipboard/gui/section/AttributionPublisher;->getMeasuredHeight()I

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/section/AttributionPublisher;->measure(II)V

    .line 547
    :cond_19
    :goto_f
    return-void

    .line 275
    :cond_1a
    const/4 v0, 0x0

    move v10, v0

    goto/16 :goto_0

    .line 276
    :cond_1b
    const/4 v0, 0x0

    move v6, v0

    goto/16 :goto_1

    .line 288
    :cond_1c
    int-to-float v2, v0

    .line 289
    int-to-float v1, v12

    move v7, v1

    move v8, v2

    goto/16 :goto_2

    .line 301
    :cond_1d
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    const/16 v2, 0x8

    invoke-interface {v1, v2}, Lflipboard/gui/section/Attribution;->setVisibility(I)V

    .line 302
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lflipboard/gui/section/scrolling/ItemActionBar;->setVisibility(I)V

    .line 303
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lflipboard/gui/section/Attribution;->setVisibility(I)V

    .line 304
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v11, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/high16 v3, -0x80000000

    invoke-static {v12, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-interface {v1, v2, v3}, Lflipboard/gui/section/Attribution;->measure(II)V

    goto/16 :goto_3

    .line 328
    :cond_1e
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    invoke-interface {v2}, Lflipboard/gui/section/Attribution;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_1f

    .line 329
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    invoke-interface {v2}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 331
    :cond_1f
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    invoke-interface {v2}, Lflipboard/gui/section/Attribution;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_20

    .line 332
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    invoke-interface {v2}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 334
    :cond_20
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v2}, Lflipboard/gui/section/scrolling/ItemActionBar;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_4

    .line 335
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v2}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    goto/16 :goto_5

    :cond_21
    move v2, v1

    .line 372
    goto/16 :goto_6

    .line 382
    :cond_22
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 389
    :cond_23
    const/4 v4, 0x0

    goto/16 :goto_9

    .line 392
    :cond_24
    const/high16 v4, 0x43960000    # 300.0f

    cmpg-float v4, v8, v4

    if-ltz v4, :cond_25

    const/high16 v4, 0x437a0000    # 250.0f

    cmpg-float v4, v7, v4

    if-ltz v4, :cond_25

    const/16 v4, 0x64

    if-le v2, v4, :cond_26

    .line 393
    :cond_25
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    const/4 v4, 0x0

    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->x:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v5}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0900f3

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lflipboard/gui/FLStaticTextView;->a(II)V

    goto/16 :goto_a

    .line 394
    :cond_26
    const/high16 v4, 0x43fa0000    # 500.0f

    cmpl-float v4, v8, v4

    if-lez v4, :cond_27

    const/high16 v4, 0x437a0000    # 250.0f

    cmpl-float v4, v7, v4

    if-lez v4, :cond_27

    const/16 v4, 0x3c

    if-ge v2, v4, :cond_27

    .line 395
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    const/4 v4, 0x0

    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->x:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v5}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0900f1

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lflipboard/gui/FLStaticTextView;->a(II)V

    goto/16 :goto_a

    .line 397
    :cond_27
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    const/4 v4, 0x0

    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->x:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v5}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0900f2

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lflipboard/gui/FLStaticTextView;->a(II)V

    goto/16 :goto_a

    .line 415
    :cond_28
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->l:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_b

    .line 433
    :cond_29
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_14

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v4, Lflipboard/gui/section/item/PostItemPhone$Layout;->c:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-eq v2, v4, :cond_14

    .line 434
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    const/high16 v4, -0x80000000

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/high16 v5, -0x80000000

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 435
    const/4 v2, 0x0

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v1, v4

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto/16 :goto_c

    .line 459
    :cond_2a
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    const/4 v2, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/4 v3, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lflipboard/gui/FLImageViewGroup;->measure(II)V

    goto/16 :goto_e

    .line 462
    :cond_2b
    iget v1, p0, Lflipboard/gui/section/item/PostItemPhone;->y:I

    if-eqz v1, :cond_2d

    .line 465
    iget v2, p0, Lflipboard/gui/section/item/PostItemPhone;->y:I

    .line 466
    const/high16 v1, 0x40000000    # 2.0f

    .line 470
    :goto_10
    iget-boolean v5, p0, Lflipboard/gui/section/item/PostItemPhone;->t:Z

    if-eqz v5, :cond_2c

    .line 471
    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    const/high16 v6, -0x80000000

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    const/high16 v7, -0x80000000

    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 473
    :cond_2c
    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget-boolean v1, p0, Lflipboard/gui/section/item/PostItemPhone;->t:Z

    if-eqz v1, :cond_2e

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v3, v1

    :goto_11
    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v5, v2, v1}, Lflipboard/gui/FLImageViewGroup;->measure(II)V

    .line 474
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v1

    iput v1, p0, Lflipboard/gui/section/item/PostItemPhone;->y:I

    .line 476
    iget-boolean v1, p0, Lflipboard/gui/section/item/PostItemPhone;->t:Z

    if-eqz v1, :cond_16

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v2}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v2

    if-le v1, v2, :cond_16

    .line 477
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v2}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v2

    const/high16 v5, -0x80000000

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/high16 v5, -0x80000000

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    goto/16 :goto_e

    .line 468
    :cond_2d
    const/high16 v1, -0x80000000

    goto :goto_10

    :cond_2e
    move v1, v3

    .line 473
    goto :goto_11

    .line 497
    :cond_2f
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v2, Lflipboard/gui/section/item/PostItemPhone$Layout;->c:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v1, v2, :cond_39

    .line 498
    iget v1, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    sub-int v1, v12, v1

    .line 499
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    invoke-interface {v2}, Lflipboard/gui/section/Attribution;->getVisibility()I

    move-result v2

    const/16 v5, 0x8

    if-eq v2, v5, :cond_30

    .line 500
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    invoke-interface {v2}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 502
    :cond_30
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    invoke-interface {v2}, Lflipboard/gui/section/Attribution;->getVisibility()I

    move-result v2

    const/16 v5, 0x8

    if-eq v2, v5, :cond_31

    .line 503
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    invoke-interface {v2}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 505
    :cond_31
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v2}, Lflipboard/gui/section/scrolling/ItemActionBar;->getVisibility()I

    move-result v2

    const/16 v5, 0x8

    if-eq v2, v5, :cond_32

    .line 506
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v2}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 508
    :cond_32
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v2

    .line 509
    iget v5, v2, Lflipboard/objs/Image;->f:I

    mul-int/2addr v5, v1

    iget v2, v2, Lflipboard/objs/Image;->g:I

    div-int v2, v5, v2

    .line 510
    mul-int/lit8 v5, v0, 0x2

    div-int/lit8 v5, v5, 0x5

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 511
    iget v2, p0, Lflipboard/gui/section/item/PostItemPhone;->y:I

    if-eqz v2, :cond_36

    .line 514
    iget v5, p0, Lflipboard/gui/section/item/PostItemPhone;->y:I

    .line 515
    const/high16 v2, 0x40000000    # 2.0f

    .line 519
    :goto_12
    const/16 v6, 0x64

    if-le v5, v6, :cond_37

    .line 520
    iget-object v6, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-static {v5, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v6, v2, v1}, Lflipboard/gui/FLImageViewGroup;->measure(II)V

    .line 524
    :goto_13
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v1

    iput v1, p0, Lflipboard/gui/section/item/PostItemPhone;->y:I

    .line 525
    iget v1, p0, Lflipboard/gui/section/item/PostItemPhone;->y:I

    int-to-float v1, v1

    sub-float v1, v8, v1

    iget v2, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    .line 527
    const/16 v2, 0x12c

    if-lt v1, v2, :cond_33

    const/high16 v1, 0x43020000    # 130.0f

    cmpg-float v1, v3, v1

    if-gez v1, :cond_34

    .line 528
    :cond_33
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->x:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v3}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0900f0

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lflipboard/gui/FLStaticTextView;->a(II)V

    .line 531
    :cond_34
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v1

    iget v2, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    add-int/2addr v1, v2

    sub-int v1, v0, v1

    .line 532
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_38

    .line 533
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    const/high16 v2, -0x80000000

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/high16 v3, -0x80000000

    invoke-static {v4, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 534
    const/4 v0, 0x0

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v4, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 537
    :goto_14
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v2}, Lflipboard/gui/section/AttributionPublisher;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_35

    .line 539
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    iget v3, p0, Lflipboard/gui/section/item/PostItemPhone;->y:I

    sub-int v3, v11, v3

    iget v4, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    mul-int/lit8 v4, v4, 0x3

    sub-int/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v4}, Lflipboard/gui/section/AttributionPublisher;->getMeasuredHeight()I

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lflipboard/gui/section/AttributionPublisher;->measure(II)V

    .line 542
    :cond_35
    :goto_15
    const/4 v2, 0x0

    iget v3, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    sub-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 543
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_19

    .line 544
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    const/high16 v3, -0x80000000

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    const/high16 v3, -0x80000000

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v2, v1, v0}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    goto/16 :goto_f

    .line 517
    :cond_36
    const/high16 v2, -0x80000000

    goto/16 :goto_12

    .line 522
    :cond_37
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    const/4 v5, 0x0

    invoke-static {v5, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/4 v5, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v1, v2, v5}, Lflipboard/gui/FLImageViewGroup;->measure(II)V

    goto/16 :goto_13

    :cond_38
    move v0, v4

    goto :goto_14

    :cond_39
    move v1, v0

    move v0, v4

    goto :goto_15

    :cond_3a
    move v2, v1

    goto/16 :goto_d

    :cond_3b
    move v3, v7

    goto/16 :goto_8

    :cond_3c
    move v1, v9

    goto/16 :goto_4
.end method

.method public final a(IIII)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/16 v10, 0x8

    .line 551
    sub-int v5, p3, p1

    .line 552
    sub-int v1, p4, p2

    .line 553
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v7

    .line 554
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v4

    .line 558
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    invoke-interface {v0}, Lflipboard/gui/section/Attribution;->getVisibility()I

    move-result v0

    if-eq v0, v10, :cond_23

    .line 560
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    invoke-interface {v0}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v1, v0

    .line 561
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    invoke-interface {v6}, Lflipboard/gui/section/Attribution;->getMeasuredWidth()I

    move-result v6

    invoke-interface {v2, v3, v0, v6, v1}, Lflipboard/gui/section/Attribution;->layout(IIII)V

    .line 563
    :goto_0
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    invoke-interface {v2}, Lflipboard/gui/section/Attribution;->getVisibility()I

    move-result v2

    if-eq v2, v10, :cond_0

    .line 565
    iget-boolean v2, p0, Lflipboard/gui/section/item/PostItemPhone;->q:Z

    if-eqz v2, :cond_22

    .line 567
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f09000b

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    .line 569
    :goto_1
    iget-object v6, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    iget-object v8, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    invoke-interface {v8}, Lflipboard/gui/section/Attribution;->getMeasuredWidth()I

    move-result v8

    iget-object v9, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    invoke-interface {v9}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v2

    invoke-interface {v6, v3, v2, v8, v9}, Lflipboard/gui/section/Attribution;->layout(IIII)V

    .line 571
    :cond_0
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v2}, Lflipboard/gui/section/scrolling/ItemActionBar;->getVisibility()I

    move-result v2

    if-eq v2, v10, :cond_1

    .line 573
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v2}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v0, v2

    .line 574
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v6}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredWidth()I

    move-result v6

    invoke-virtual {v2, v3, v0, v6, v1}, Lflipboard/gui/section/scrolling/ItemActionBar;->layout(IIII)V

    .line 577
    :cond_1
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v6, Lflipboard/gui/section/item/PostItemPhone$Layout;->d:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v2, v6, :cond_8

    .line 581
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    invoke-interface {v0}, Lflipboard/gui/section/Attribution;->getVisibility()I

    move-result v0

    if-eq v0, v10, :cond_21

    .line 582
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    invoke-interface {v0}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 584
    :goto_2
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v4}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v6}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v2, v3, v0, v4, v6}, Lflipboard/gui/FLImageViewGroup;->layout(IIII)V

    .line 586
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v2

    if-eq v2, v10, :cond_2

    .line 587
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v2, v3, v0, v4, v6}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 590
    :cond_2
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v2}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 591
    iget-boolean v2, p0, Lflipboard/gui/section/item/PostItemPhone;->t:Z

    if-eqz v2, :cond_3

    .line 592
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v2}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    .line 593
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v2

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v3, v2, v0, v4, v6}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 594
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 596
    :cond_3
    iget v2, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    add-int/2addr v0, v2

    .line 598
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_4

    .line 599
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v7

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2, v7, v0, v3, v4}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 600
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v2

    iget v3, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 603
    :cond_4
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v2}, Lflipboard/gui/section/AttributionPublisher;->getVisibility()I

    move-result v2

    if-eq v2, v10, :cond_5

    .line 604
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v3}, Lflipboard/gui/section/AttributionPublisher;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v7

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v4}, Lflipboard/gui/section/AttributionPublisher;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2, v7, v0, v3, v4}, Lflipboard/gui/section/AttributionPublisher;->layout(IIII)V

    .line 605
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v2}, Lflipboard/gui/section/AttributionPublisher;->getMeasuredHeight()I

    move-result v2

    iget v3, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 608
    :cond_5
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_6

    .line 609
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v2

    .line 610
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    add-int/2addr v2, v7

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v3, v7, v0, v2, v4}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 611
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 614
    :cond_6
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    invoke-virtual {v2}, Lflipboard/gui/FLButton;->getVisibility()I

    move-result v2

    if-eq v2, v10, :cond_7

    .line 615
    sub-int v2, v1, v0

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    invoke-virtual {v3}, Lflipboard/gui/FLButton;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 616
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    invoke-virtual {v3}, Lflipboard/gui/FLButton;->getMeasuredWidth()I

    move-result v3

    sub-int v3, v5, v3

    div-int/lit8 v3, v3, 0x2

    .line 617
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    add-int/2addr v0, v2

    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    invoke-virtual {v5}, Lflipboard/gui/FLButton;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v3

    sub-int/2addr v1, v2

    invoke-virtual {v4, v3, v0, v5, v1}, Lflipboard/gui/FLButton;->layout(IIII)V

    .line 759
    :cond_7
    :goto_3
    return-void

    .line 618
    :cond_8
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v6, Lflipboard/gui/section/item/PostItemPhone$Layout;->e:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v2, v6, :cond_d

    .line 622
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v2}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v2

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v4}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v1, v3, v3, v2, v4}, Lflipboard/gui/FLImageViewGroup;->layout(IIII)V

    .line 623
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->l:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v10, :cond_9

    .line 624
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->l:Landroid/view/View;

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->l:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v1, v3, v3, v2, v4}, Landroid/view/View;->layout(IIII)V

    .line 626
    :cond_9
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v1

    .line 628
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->k:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 629
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->k:Landroid/view/View;

    sub-int v2, v1, v2

    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v5}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v5

    invoke-virtual {v4, v3, v2, v5, v1}, Landroid/view/View;->layout(IIII)V

    .line 631
    iget v2, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    sub-int/2addr v1, v2

    .line 633
    iget-boolean v2, p0, Lflipboard/gui/section/item/PostItemPhone;->t:Z

    if-eqz v2, :cond_a

    .line 634
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v2}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v7

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    .line 635
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v2

    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v3, v2, v1, v4, v5}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 636
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 639
    :cond_a
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_b

    .line 640
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    invoke-interface {v2}, Lflipboard/gui/section/Attribution;->getVisibility()I

    move-result v2

    if-eq v2, v10, :cond_c

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    invoke-interface {v2}, Lflipboard/gui/section/Attribution;->a()Z

    move-result v2

    if-nez v2, :cond_c

    .line 641
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v1, v0

    .line 645
    :goto_4
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v7

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {v1, v7, v0, v2, v3}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 648
    :cond_b
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    goto/16 :goto_3

    .line 643
    :cond_c
    iget v1, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_4

    .line 650
    :cond_d
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v2, Lflipboard/gui/section/item/PostItemPhone$Layout;->b:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-eq v0, v2, :cond_e

    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v2, Lflipboard/gui/section/item/PostItemPhone$Layout;->a:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v0, v2, :cond_19

    .line 653
    :cond_e
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    invoke-interface {v0}, Lflipboard/gui/section/Attribution;->getVisibility()I

    move-result v0

    if-eq v0, v10, :cond_f

    .line 654
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    invoke-interface {v0}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v1, v0

    .line 656
    :cond_f
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/ItemActionBar;->getVisibility()I

    move-result v0

    if-eq v0, v10, :cond_10

    .line 657
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v1, v0

    .line 660
    :cond_10
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    invoke-virtual {v0}, Lflipboard/gui/FLButton;->getVisibility()I

    move-result v0

    if-eq v0, v10, :cond_11

    .line 661
    iget v0, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    sub-int v0, v1, v0

    .line 662
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    invoke-virtual {v1}, Lflipboard/gui/FLButton;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v5, v1

    div-int/lit8 v1, v1, 0x2

    .line 663
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    invoke-virtual {v2}, Lflipboard/gui/FLButton;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v0, v2

    .line 664
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    invoke-virtual {v4}, Lflipboard/gui/FLButton;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v3, v1, v2, v4, v0}, Lflipboard/gui/FLButton;->layout(IIII)V

    .line 667
    iget v0, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    sub-int v1, v2, v0

    .line 671
    :cond_11
    iget v0, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    sub-int v2, v1, v0

    .line 672
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v2, v0

    .line 673
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v7

    invoke-virtual {v1, v7, v0, v3, v2}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 675
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v1}, Lflipboard/gui/section/AttributionPublisher;->getVisibility()I

    move-result v1

    if-eq v1, v10, :cond_12

    .line 676
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v1}, Lflipboard/gui/section/AttributionPublisher;->getMeasuredHeight()I

    move-result v1

    iget v3, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    add-int/2addr v1, v3

    sub-int/2addr v0, v1

    .line 677
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v3}, Lflipboard/gui/section/AttributionPublisher;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v7

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v4}, Lflipboard/gui/section/AttributionPublisher;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v1, v7, v0, v3, v4}, Lflipboard/gui/section/AttributionPublisher;->layout(IIII)V

    .line 681
    :cond_12
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v1

    sub-int v4, v5, v1

    .line 684
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v1

    sub-int v5, v4, v1

    .line 686
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v1

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v1, v3

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v3

    if-le v1, v3, :cond_18

    .line 687
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v2, v1

    .line 688
    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v3

    if-eq v3, v10, :cond_17

    .line 689
    iget v3, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v3, v6

    sub-int v3, v1, v3

    .line 690
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v3

    .line 698
    :goto_5
    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 699
    iget-object v6, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v8, Lflipboard/gui/section/item/PostItemPhone$Layout;->a:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v6, v8, :cond_13

    iget-object v6, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v6

    if-lez v6, :cond_14

    .line 700
    :cond_13
    iget v6, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    sub-int/2addr v0, v6

    .line 703
    :cond_14
    iget-object v6, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v6

    if-eq v6, v10, :cond_15

    .line 704
    iget v6, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    add-int/2addr v6, v1

    .line 705
    iget-object v8, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    iget-object v9, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v9}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v9, v5

    invoke-virtual {v8, v5, v6, v9, v2}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 708
    :cond_15
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v2, v5, v3, v4, v1}, Lflipboard/gui/FLImageViewGroup;->layout(IIII)V

    .line 711
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v1

    if-eq v1, v10, :cond_16

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v2, Lflipboard/gui/section/item/PostItemPhone$Layout;->a:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-eq v1, v2, :cond_16

    .line 712
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v5

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v3

    invoke-virtual {v1, v5, v3, v2, v4}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 715
    :cond_16
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v0, v2

    iget-object v3, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v7

    invoke-virtual {v1, v7, v2, v3, v0}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    goto/16 :goto_3

    :cond_17
    move v3, v1

    move v1, v2

    .line 692
    goto :goto_5

    .line 696
    :cond_18
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v0

    move v3, v0

    goto :goto_5

    .line 716
    :cond_19
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v2, Lflipboard/gui/section/item/PostItemPhone$Layout;->c:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v0, v2, :cond_7

    .line 717
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    invoke-interface {v0}, Lflipboard/gui/section/Attribution;->getVisibility()I

    move-result v0

    if-eq v0, v10, :cond_20

    .line 718
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    invoke-interface {v0}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v4

    .line 722
    :goto_6
    iget v2, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    sub-int v2, p3, v2

    .line 723
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v4}, Lflipboard/gui/FLImageViewGroup;->getMeasuredWidth()I

    move-result v4

    sub-int v6, v2, v4

    .line 724
    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v4}, Lflipboard/gui/FLImageViewGroup;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 728
    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v5, v6, v0, v2, v4}, Lflipboard/gui/FLImageViewGroup;->layout(IIII)V

    .line 731
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v7

    iget-object v8, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v8}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v0

    invoke-virtual {v2, v7, v0, v5, v8}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 732
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v2

    iget v5, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    add-int/2addr v2, v5

    add-int/2addr v2, v0

    .line 733
    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v5}, Lflipboard/gui/section/AttributionPublisher;->getVisibility()I

    move-result v5

    if-eq v5, v10, :cond_1a

    .line 734
    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    iget-object v8, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v8}, Lflipboard/gui/section/AttributionPublisher;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v7

    iget-object v9, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v9}, Lflipboard/gui/section/AttributionPublisher;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v2

    invoke-virtual {v5, v7, v2, v8, v9}, Lflipboard/gui/section/AttributionPublisher;->layout(IIII)V

    .line 735
    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v5}, Lflipboard/gui/section/AttributionPublisher;->getMeasuredHeight()I

    move-result v5

    iget v8, p0, Lflipboard/gui/section/item/PostItemPhone;->z:I

    add-int/2addr v5, v8

    add-int/2addr v2, v5

    .line 738
    :cond_1a
    iget-object v5, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v2

    .line 739
    iget-object v8, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    iget-object v9, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v9}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v9, v7

    invoke-virtual {v8, v7, v2, v9, v5}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 741
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v2

    if-eq v2, v10, :cond_1b

    .line 743
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    iget-object v7, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v6

    iget-object v8, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {v8}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v0

    invoke-virtual {v2, v6, v0, v7, v8}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 746
    :cond_1b
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    invoke-virtual {v0}, Lflipboard/gui/FLButton;->getVisibility()I

    move-result v0

    if-eq v0, v10, :cond_7

    .line 748
    if-le v5, v4, :cond_1c

    const/4 v3, 0x1

    .line 749
    :cond_1c
    if-eqz v3, :cond_1e

    move v0, v4

    .line 750
    :goto_7
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    invoke-virtual {v2}, Lflipboard/gui/FLButton;->getMeasuredHeight()I

    move-result v2

    .line 751
    sub-int/2addr v1, v0

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 752
    add-int/2addr v0, v1

    .line 753
    if-eqz v3, :cond_1d

    move p1, v6

    .line 754
    :cond_1d
    if-eqz v3, :cond_1f

    .line 755
    :goto_8
    sub-int v1, p3, p1

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    invoke-virtual {v2}, Lflipboard/gui/FLButton;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 756
    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    add-int v3, p1, v1

    add-int/2addr v1, p1

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    invoke-virtual {v4}, Lflipboard/gui/FLButton;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v1, v4

    iget-object v4, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    invoke-virtual {v4}, Lflipboard/gui/FLButton;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2, v3, v0, v1, v4}, Lflipboard/gui/FLButton;->layout(IIII)V

    goto/16 :goto_3

    :cond_1e
    move v0, v5

    .line 749
    goto :goto_7

    :cond_1f
    move p3, v6

    .line 754
    goto :goto_8

    :cond_20
    move v0, v4

    goto/16 :goto_6

    :cond_21
    move v0, v3

    goto/16 :goto_2

    :cond_22
    move v2, v3

    goto/16 :goto_1

    :cond_23
    move v0, v1

    goto/16 :goto_0
.end method

.method public final a(Lflipboard/gui/section/item/PostItemPhone$Layout;)V
    .locals 1

    .prologue
    .line 233
    iput-object p1, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    .line 234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/item/PostItemPhone;->o:Z

    .line 235
    invoke-direct {p0}, Lflipboard/gui/section/item/PostItemPhone;->g()V

    .line 236
    return-void
.end method

.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/16 v3, 0x8

    .line 136
    iput-object p2, p0, Lflipboard/gui/section/item/PostItemPhone;->a:Lflipboard/objs/FeedItem;

    .line 137
    iput-object p1, p0, Lflipboard/gui/section/item/PostItemPhone;->b:Lflipboard/service/Section;

    .line 139
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->I()Ljava/lang/String;

    move-result-object v0

    .line 140
    if-eqz v0, :cond_3

    .line 141
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    :goto_0
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    .line 149
    iget-boolean v0, p2, Lflipboard/objs/FeedItem;->bN:Z

    if-eqz v0, :cond_4

    iget-object v0, p2, Lflipboard/objs/FeedItem;->bR:Ljava/lang/String;

    .line 155
    :goto_1
    if-nez v0, :cond_0

    .line 156
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->G()Ljava/lang/String;

    move-result-object v0

    .line 158
    :cond_0
    if-eqz v0, :cond_5

    .line 159
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v0

    .line 160
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    iget-object v2, p2, Lflipboard/objs/FeedItem;->Y:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/FLStaticTextView;->a(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 165
    :goto_2
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->i()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 166
    iput-boolean v4, p0, Lflipboard/gui/section/item/PostItemPhone;->s:Z

    .line 168
    iget-object v0, p2, Lflipboard/objs/FeedItem;->bq:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 169
    iput-boolean v4, p0, Lflipboard/gui/section/item/PostItemPhone;->t:Z

    .line 170
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->bq:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 175
    :cond_1
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemPhone;->r:Z

    if-eqz v0, :cond_6

    .line 176
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->l()Ljava/util/List;

    move-result-object v0

    .line 181
    :goto_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 182
    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageViewGroup;->setImages(Ljava/util/List;)V

    .line 188
    :cond_2
    :goto_4
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p2}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    iget v1, p2, Lflipboard/objs/FeedItem;->E:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v0, v4

    .line 190
    iget-boolean v0, p2, Lflipboard/objs/FeedItem;->bN:Z

    if-eqz v0, :cond_8

    .line 191
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->bQ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 192
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->bR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    invoke-interface {v0, v3}, Lflipboard/gui/section/Attribution;->setVisibility(I)V

    .line 194
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    invoke-interface {v0, v3}, Lflipboard/gui/section/Attribution;->setVisibility(I)V

    .line 209
    :goto_5
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->p:Lflipboard/gui/section/AttributionPublisher;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/AttributionPublisher;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 210
    return-void

    .line 143
    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 149
    :cond_4
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 162
    :cond_5
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    goto :goto_2

    .line 179
    :cond_6
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_3

    .line 185
    :cond_7
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLImageViewGroup;->setVisibility(I)V

    goto :goto_4

    .line 196
    :cond_8
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    invoke-interface {v0, p1, p2}, Lflipboard/gui/section/Attribution;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 197
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->j:Lflipboard/gui/section/Attribution;

    invoke-interface {v0, p1, p2}, Lflipboard/gui/section/Attribution;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 198
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->B:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/scrolling/ItemActionBar;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 200
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->i:Lflipboard/gui/section/Attribution;

    new-instance v1, Lflipboard/gui/section/item/PostItemPhone$1;

    invoke-direct {v1, p0}, Lflipboard/gui/section/item/PostItemPhone$1;-><init>(Lflipboard/gui/section/item/PostItemPhone;)V

    invoke-interface {v0, v1}, Lflipboard/gui/section/Attribution;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 207
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLButton;->setVisibility(I)V

    goto :goto_5
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 120
    iput-boolean p1, p0, Lflipboard/gui/section/item/PostItemPhone;->q:Z

    .line 121
    return-void
.end method

.method public final a(ZI)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 764
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "PostItemTablet:onPageOffsetChange"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 765
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemPhone;->s:Z

    if-eqz v0, :cond_0

    .line 766
    invoke-static {p1, p2}, Lflipboard/util/AndroidUtil;->a(ZI)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 767
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemPhone;->u:Z

    if-eqz v0, :cond_0

    .line 768
    iput-boolean v3, p0, Lflipboard/gui/section/item/PostItemPhone;->u:Z

    .line 769
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0}, Lflipboard/gui/FLImageViewGroup;->a()V

    .line 782
    :cond_0
    :goto_0
    return-void

    .line 772
    :cond_1
    iget-boolean v0, p0, Lflipboard/gui/section/item/PostItemPhone;->u:Z

    if-nez v0, :cond_0

    .line 773
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/item/PostItemPhone;->u:Z

    .line 774
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 775
    new-instance v0, Lflipboard/gui/FLImageViewGroup;

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->x:Lflipboard/activities/FlipboardActivity;

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLImageViewGroup;-><init>(Landroid/content/Context;Lflipboard/gui/FLImageViewGroup;)V

    iput-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    .line 776
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 778
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    iget-object v1, p0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    iget-object v2, p0, Lflipboard/gui/section/item/PostItemPhone;->v:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1, v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 786
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/item/PostItemPhone$2;

    invoke-direct {v1, p0}, Lflipboard/gui/section/item/PostItemPhone$2;-><init>(Lflipboard/gui/section/item/PostItemPhone;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;J)V

    .line 799
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 130
    iput-boolean p1, p0, Lflipboard/gui/section/item/PostItemPhone;->r:Z

    .line 131
    return-void
.end method

.method public final c()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->a:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 125
    iput-boolean p1, p0, Lflipboard/gui/section/item/PostItemPhone;->A:Z

    .line 126
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 219
    iget v0, p0, Lflipboard/gui/section/item/PostItemPhone;->y:I

    return v0
.end method

.method public final e()Lflipboard/gui/section/item/PostItemPhone$Layout;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    return-object v0
.end method

.method public final f()Lflipboard/gui/FLButton;
    .locals 1

    .prologue
    .line 878
    iget-object v0, p0, Lflipboard/gui/section/item/PostItemPhone;->g:Lflipboard/gui/FLButton;

    return-object v0
.end method
