.class public Lflipboard/gui/section/item/AlbumItem;
.super Landroid/view/ViewGroup;
.source "AlbumItem.java"

# interfaces
.implements Lflipboard/gui/FLViewIntf;
.implements Lflipboard/gui/item/TabletItem;


# instance fields
.field a:Lflipboard/service/Section;

.field b:Lflipboard/objs/FeedItem;

.field c:Lflipboard/gui/section/Attribution;

.field final d:I

.field final e:I

.field private f:Lflipboard/gui/section/Group;

.field private g:Lflipboard/gui/FLStaticTextView;

.field private h:Lflipboard/gui/FLButton;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Lflipboard/gui/FLStaticTextView;

.field private l:Z

.field private m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/gui/FLImageView;",
            ">;"
        }
    .end annotation
.end field

.field private n:Z

.field private o:I

.field private p:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/item/AlbumItem;->l:Z

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/section/item/AlbumItem;->n:Z

    .line 48
    const/high16 v0, -0x80000000

    iput v0, p0, Lflipboard/gui/section/item/AlbumItem;->o:I

    .line 54
    invoke-virtual {p0}, Lflipboard/gui/section/item/AlbumItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    .line 55
    invoke-virtual {p0}, Lflipboard/gui/section/item/AlbumItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/item/AlbumItem;->e:I

    .line 56
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 305
    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->f:Lflipboard/gui/section/Group;

    if-nez v1, :cond_1

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 309
    :cond_1
    iget-boolean v1, p0, Lflipboard/gui/section/item/AlbumItem;->n:Z

    iget v2, p0, Lflipboard/gui/section/item/AlbumItem;->o:I

    invoke-static {v1, v2}, Lflipboard/util/AndroidUtil;->a(ZI)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 310
    iget-boolean v1, p0, Lflipboard/gui/section/item/AlbumItem;->l:Z

    if-eqz v1, :cond_0

    .line 311
    iput-boolean v0, p0, Lflipboard/gui/section/item/AlbumItem;->l:Z

    move v2, v0

    .line 312
    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->f:Lflipboard/gui/section/Group;

    iget-object v0, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 313
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->f:Lflipboard/gui/section/Group;

    iget-object v0, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 314
    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    .line 315
    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 316
    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setTag(Ljava/lang/Object;)V

    .line 312
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 320
    :cond_2
    iget-boolean v0, p0, Lflipboard/gui/section/item/AlbumItem;->l:Z

    if-nez v0, :cond_0

    .line 321
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/item/AlbumItem;->l:Z

    .line 322
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 323
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 324
    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/AlbumItem;->removeView(Landroid/view/View;)V

    .line 325
    invoke-virtual {p0}, Lflipboard/gui/section/item/AlbumItem;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f03009d

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lflipboard/gui/section/item/AlbumItem;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 326
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 327
    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/AlbumItem;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 329
    :cond_3
    iput-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->m:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method private a(III)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 277
    if-ge p2, p3, :cond_1

    const/4 v0, 0x1

    move v2, v0

    :goto_0
    move v3, v1

    .line 278
    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 279
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 280
    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->f:Lflipboard/gui/section/Group;

    iget-object v1, v1, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v1, v2}, Lflipboard/objs/SectionPageTemplate;->a(Z)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/SectionPageTemplate$Area;

    .line 283
    invoke-virtual {v1, v2}, Lflipboard/objs/SectionPageTemplate$Area;->c(Z)F

    move-result v4

    int-to-float v5, p2

    mul-float/2addr v4, v5

    float-to-int v4, v4

    .line 284
    invoke-virtual {v1, v2}, Lflipboard/objs/SectionPageTemplate$Area;->c(Z)F

    move-result v5

    cmpl-float v5, v5, v7

    if-lez v5, :cond_0

    .line 285
    iget v5, p0, Lflipboard/gui/section/item/AlbumItem;->e:I

    add-int/2addr v4, v5

    .line 287
    :cond_0
    invoke-virtual {v1, v2}, Lflipboard/objs/SectionPageTemplate$Area;->d(Z)F

    move-result v5

    int-to-float v6, p3

    mul-float/2addr v5, v6

    float-to-int v5, v5

    .line 288
    invoke-virtual {v1, v2}, Lflipboard/objs/SectionPageTemplate$Area;->d(Z)F

    move-result v1

    cmpl-float v1, v1, v7

    if-lez v1, :cond_3

    .line 289
    iget v1, p0, Lflipboard/gui/section/item/AlbumItem;->e:I

    add-int/2addr v1, v5

    .line 291
    :goto_2
    add-int/2addr v1, p1

    .line 292
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v4

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v1

    invoke-virtual {v0, v4, v1, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 278
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_1
    move v2, v1

    .line 277
    goto :goto_0

    .line 294
    :cond_2
    return-void

    :cond_3
    move v1, v5

    goto :goto_2
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 84
    iput-object p1, p0, Lflipboard/gui/section/item/AlbumItem;->a:Lflipboard/service/Section;

    .line 85
    iput-object p2, p0, Lflipboard/gui/section/item/AlbumItem;->b:Lflipboard/objs/FeedItem;

    .line 87
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->i:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 88
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->j:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 89
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->k:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    invoke-virtual {p0, p2}, Lflipboard/gui/section/item/AlbumItem;->setTag(Ljava/lang/Object;)V

    .line 91
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->c:Lflipboard/gui/section/Attribution;

    invoke-interface {v0, p1, p2}, Lflipboard/gui/section/Attribution;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 93
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->C()Lflipboard/objs/FeedSectionLink;

    move-result-object v0

    .line 94
    if-nez v0, :cond_1

    .line 95
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v1

    .line 96
    iget-object v0, v1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v2, "status"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p2, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 98
    if-eqz v0, :cond_0

    iget-object v3, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-nez v3, :cond_0

    .line 99
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    goto :goto_0

    .line 104
    :cond_1
    iget v0, p2, Lflipboard/objs/FeedItem;->aw:I

    iget-object v1, p2, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 105
    if-lez v0, :cond_4

    .line 106
    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->h:Lflipboard/gui/FLButton;

    invoke-virtual {p0}, Lflipboard/gui/section/item/AlbumItem;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0215

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->h:Lflipboard/gui/FLButton;

    invoke-virtual {v0, p2}, Lflipboard/gui/FLButton;->setTag(Ljava/lang/Object;)V

    .line 112
    :goto_1
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_3

    .line 113
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v1

    .line 114
    const/4 v0, 0x0

    .line 115
    iget-object v2, v1, Lflipboard/objs/FeedItem;->av:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 116
    iget-object v0, v1, Lflipboard/objs/FeedItem;->av:Ljava/lang/String;

    .line 123
    :cond_2
    :goto_2
    if-eqz v0, :cond_3

    .line 124
    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->g:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1, v4}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    .line 125
    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->g:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    :cond_3
    return-void

    .line 109
    :cond_4
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->h:Lflipboard/gui/FLButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setVisibility(I)V

    goto :goto_1

    .line 117
    :cond_5
    iget-object v2, v1, Lflipboard/objs/FeedItem;->ar:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 118
    iget-object v0, v1, Lflipboard/objs/FeedItem;->ar:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/HttpUtil;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 119
    :cond_6
    iget-object v2, v1, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 120
    iget-object v0, v1, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/HttpUtil;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public final a(ZI)V
    .locals 1

    .prologue
    .line 298
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "AlbumItemTablet:onPageOffsetChange"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 299
    iput-boolean p1, p0, Lflipboard/gui/section/item/AlbumItem;->n:Z

    .line 300
    iput p2, p0, Lflipboard/gui/section/item/AlbumItem;->o:I

    .line 301
    invoke-direct {p0}, Lflipboard/gui/section/item/AlbumItem;->a()V

    .line 302
    return-void
.end method

.method public getAlbumTemplate()Lflipboard/objs/SectionPageTemplate;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->f:Lflipboard/gui/section/Group;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->f:Lflipboard/gui/section/Group;

    iget-object v0, v0, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->b:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 60
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 61
    invoke-virtual {p0}, Lflipboard/gui/section/item/AlbumItem;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 62
    invoke-virtual {p0, v1, v1, v1, v1}, Lflipboard/gui/section/item/AlbumItem;->setPadding(IIII)V

    .line 63
    const v0, 0x7f0a008b

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/AlbumItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Attribution;

    iput-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->c:Lflipboard/gui/section/Attribution;

    .line 64
    const v0, 0x7f0a0045

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/AlbumItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    iput-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->h:Lflipboard/gui/FLButton;

    .line 65
    const v0, 0x7f0a01f3

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/AlbumItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->i:Landroid/view/View;

    .line 66
    const v0, 0x7f0a0076

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/AlbumItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->j:Landroid/view/View;

    .line 67
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/AlbumItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->k:Lflipboard/gui/FLStaticTextView;

    .line 68
    const v0, 0x7f0a01f2

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/AlbumItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->g:Lflipboard/gui/FLStaticTextView;

    .line 70
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 72
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 73
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->h:Lflipboard/gui/FLButton;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLButton;->setVisibility(I)V

    .line 74
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->g:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    .line 78
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 13

    .prologue
    .line 220
    sub-int v2, p4, p2

    .line 221
    sub-int v3, p5, p3

    .line 222
    iget v0, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    .line 223
    iget v1, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    .line 225
    sget-object v4, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v4, v4, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v4, :cond_2

    .line 226
    iget-object v4, p0, Lflipboard/gui/section/item/AlbumItem;->k:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    .line 227
    iget-object v5, p0, Lflipboard/gui/section/item/AlbumItem;->k:Lflipboard/gui/FLStaticTextView;

    iget-object v6, p0, Lflipboard/gui/section/item/AlbumItem;->k:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v5, v0, v1, v6, v4}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 228
    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->g:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v1

    add-int v5, v4, v1

    .line 229
    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->g:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v1

    const/16 v6, 0x8

    if-eq v1, v6, :cond_0

    .line 230
    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->g:Lflipboard/gui/FLStaticTextView;

    iget-object v6, p0, Lflipboard/gui/section/item/AlbumItem;->g:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v1, v0, v4, v6, v5}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 233
    :cond_0
    iget v0, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    sub-int v1, v2, v0

    .line 234
    iget v4, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    .line 235
    iget v0, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    const/4 v6, 0x3

    new-array v6, v6, [I

    const/4 v7, 0x0

    iget-object v8, p0, Lflipboard/gui/section/item/AlbumItem;->i:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    aput v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lflipboard/gui/section/item/AlbumItem;->j:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    aput v8, v6, v7

    const/4 v7, 0x2

    iget-object v8, p0, Lflipboard/gui/section/item/AlbumItem;->h:Lflipboard/gui/FLButton;

    invoke-virtual {v8}, Lflipboard/gui/FLButton;->getMeasuredHeight()I

    move-result v8

    aput v8, v6, v7

    invoke-static {v6}, Lflipboard/util/JavaUtil;->a([I)I

    move-result v6

    add-int/2addr v6, v0

    .line 236
    const/4 v0, 0x3

    new-array v7, v0, [Landroid/view/View;

    const/4 v0, 0x0

    iget-object v8, p0, Lflipboard/gui/section/item/AlbumItem;->i:Landroid/view/View;

    aput-object v8, v7, v0

    const/4 v0, 0x1

    iget-object v8, p0, Lflipboard/gui/section/item/AlbumItem;->j:Landroid/view/View;

    aput-object v8, v7, v0

    const/4 v0, 0x2

    iget-object v8, p0, Lflipboard/gui/section/item/AlbumItem;->h:Lflipboard/gui/FLButton;

    aput-object v8, v7, v0

    const/4 v0, 0x0

    :goto_0
    const/4 v8, 0x3

    if-ge v0, v8, :cond_1

    aget-object v8, v7, v0

    .line 237
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    sub-int v9, v1, v9

    .line 238
    div-int/lit8 v10, v6, 0x2

    add-int/2addr v10, v4

    .line 239
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    sub-int v11, v10, v11

    .line 240
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    add-int/2addr v10, v12

    .line 241
    invoke-virtual {v8, v9, v11, v1, v10}, Landroid/view/View;->layout(IIII)V

    .line 244
    iget v1, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    sub-int v1, v9, v1

    .line 236
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 247
    :cond_1
    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 248
    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->c:Lflipboard/gui/section/Attribution;

    invoke-interface {v1}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v0

    .line 249
    iget-object v4, p0, Lflipboard/gui/section/item/AlbumItem;->c:Lflipboard/gui/section/Attribution;

    const/4 v5, 0x0

    invoke-interface {v4, v5, v0, v2, v1}, Lflipboard/gui/section/Attribution;->layout(IIII)V

    .line 251
    iget v0, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    add-int/2addr v0, v1

    .line 253
    sub-int v1, v3, v0

    .line 254
    invoke-direct {p0, v0, v2, v1}, Lflipboard/gui/section/item/AlbumItem;->a(III)V

    .line 266
    :goto_1
    return-void

    .line 256
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->c:Lflipboard/gui/section/Attribution;

    invoke-interface {v0}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v3, v0

    .line 258
    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->c:Lflipboard/gui/section/Attribution;

    const/4 v4, 0x0

    invoke-interface {v1, v4, v0, v2, v3}, Lflipboard/gui/section/Attribution;->layout(IIII)V

    .line 260
    iget v1, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    sub-int/2addr v0, v1

    .line 261
    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->k:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v0, v1

    .line 262
    iget-object v3, p0, Lflipboard/gui/section/item/AlbumItem;->k:Lflipboard/gui/FLStaticTextView;

    iget v4, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    iget v5, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    iget-object v6, p0, Lflipboard/gui/section/item/AlbumItem;->k:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v3, v4, v1, v5, v0}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 264
    const/4 v0, 0x0

    iget v3, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    sub-int/2addr v1, v3

    invoke-direct {p0, v0, v2, v1}, Lflipboard/gui/section/item/AlbumItem;->a(III)V

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 11

    .prologue
    .line 138
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 139
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 140
    invoke-virtual {p0, v6, v0}, Lflipboard/gui/section/item/AlbumItem;->setMeasuredDimension(II)V

    .line 143
    const/high16 v1, -0x80000000

    invoke-static {v6, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 144
    const/high16 v2, -0x80000000

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 145
    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v3, v3, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v3, :cond_0

    .line 146
    iget-object v3, p0, Lflipboard/gui/section/item/AlbumItem;->i:Landroid/view/View;

    invoke-virtual {v3, v1, v2}, Landroid/view/View;->measure(II)V

    .line 147
    iget-object v3, p0, Lflipboard/gui/section/item/AlbumItem;->j:Landroid/view/View;

    invoke-virtual {v3, v1, v2}, Landroid/view/View;->measure(II)V

    .line 148
    iget-object v3, p0, Lflipboard/gui/section/item/AlbumItem;->h:Lflipboard/gui/FLButton;

    invoke-virtual {v3, v1, v1}, Lflipboard/gui/FLButton;->measure(II)V

    .line 151
    :cond_0
    iget v1, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    sub-int v1, v6, v1

    iget-object v3, p0, Lflipboard/gui/section/item/AlbumItem;->i:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v1, v3

    iget v3, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    sub-int/2addr v1, v3

    iget-object v3, p0, Lflipboard/gui/section/item/AlbumItem;->j:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v1, v3

    iget v3, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    sub-int/2addr v1, v3

    iget-object v3, p0, Lflipboard/gui/section/item/AlbumItem;->h:Lflipboard/gui/FLButton;

    invoke-virtual {v3}, Lflipboard/gui/FLButton;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v1, v3

    iget v3, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    sub-int/2addr v1, v3

    .line 152
    iget-object v3, p0, Lflipboard/gui/section/item/AlbumItem;->k:Lflipboard/gui/FLStaticTextView;

    const/high16 v4, -0x80000000

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v3, v4, v2}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 153
    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v3, v3, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v3, :cond_1

    .line 154
    iget-object v3, p0, Lflipboard/gui/section/item/AlbumItem;->g:Lflipboard/gui/FLStaticTextView;

    const/high16 v4, -0x80000000

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v3, v1, v2}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 157
    :cond_1
    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->c:Lflipboard/gui/section/Attribution;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v6, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-interface {v1, v3, v2}, Lflipboard/gui/section/Attribution;->measure(II)V

    .line 159
    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->k:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/section/item/AlbumItem;->g:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    .line 161
    const/4 v2, 0x3

    new-array v2, v2, [I

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/gui/section/item/AlbumItem;->i:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    aput v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lflipboard/gui/section/item/AlbumItem;->j:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    aput v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lflipboard/gui/section/item/AlbumItem;->h:Lflipboard/gui/FLButton;

    invoke-virtual {v4}, Lflipboard/gui/FLButton;->getMeasuredHeight()I

    move-result v4

    aput v4, v2, v3

    invoke-static {v2}, Lflipboard/util/JavaUtil;->a([I)I

    move-result v2

    .line 162
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->c:Lflipboard/gui/section/Attribution;

    invoke-interface {v1}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    sub-int/2addr v0, v1

    .line 164
    iget v1, p0, Lflipboard/gui/section/item/AlbumItem;->d:I

    sub-int v7, v0, v1

    .line 165
    if-ge v6, v7, :cond_4

    const/4 v5, 0x1

    .line 168
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->f:Lflipboard/gui/section/Group;

    if-nez v0, :cond_7

    .line 169
    const/high16 v0, -0x80000000

    .line 170
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-object v1, v1, Lflipboard/app/FlipboardApplication;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v8, v0

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/SectionPageTemplate;

    .line 171
    iget-object v0, v2, Lflipboard/objs/SectionPageTemplate;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_3

    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_2

    :cond_3
    iget-boolean v0, v2, Lflipboard/objs/SectionPageTemplate;->i:Z

    if-nez v0, :cond_2

    .line 172
    new-instance v0, Lflipboard/gui/section/Group;

    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->a:Lflipboard/service/Section;

    iget-object v3, p0, Lflipboard/gui/section/item/AlbumItem;->b:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    const/4 v4, 0x0

    invoke-direct/range {v0 .. v7}, Lflipboard/gui/section/Group;-><init>(Lflipboard/service/Section;Lflipboard/objs/SectionPageTemplate;Ljava/util/List;Lflipboard/objs/SidebarGroup;ZII)V

    .line 175
    iget v1, v0, Lflipboard/gui/section/Group;->g:I

    if-le v1, v8, :cond_b

    .line 176
    iput-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->f:Lflipboard/gui/section/Group;

    .line 177
    iget v0, v0, Lflipboard/gui/section/Group;->g:I

    :goto_2
    move v8, v0

    .line 179
    goto :goto_1

    .line 165
    :cond_4
    const/4 v5, 0x0

    goto :goto_0

    .line 180
    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->f:Lflipboard/gui/section/Group;

    iget-object v1, v1, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->m:Ljava/util/ArrayList;

    .line 181
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->f:Lflipboard/gui/section/Group;

    iget-object v0, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 182
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_7

    .line 183
    invoke-virtual {p0}, Lflipboard/gui/section/item/AlbumItem;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f03009d

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lflipboard/gui/section/item/AlbumItem;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 184
    iget-object v3, p0, Lflipboard/gui/section/item/AlbumItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    iget-object v3, p0, Lflipboard/gui/section/item/AlbumItem;->p:Landroid/view/View$OnClickListener;

    if-eqz v3, :cond_6

    .line 186
    iget-object v3, p0, Lflipboard/gui/section/item/AlbumItem;->p:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    :cond_6
    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/AlbumItem;->addView(Landroid/view/View;)V

    .line 182
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 193
    :cond_7
    const/4 v0, 0x0

    move v2, v0

    :goto_4
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 194
    iget-object v0, p0, Lflipboard/gui/section/item/AlbumItem;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 195
    iget-object v1, p0, Lflipboard/gui/section/item/AlbumItem;->f:Lflipboard/gui/section/Group;

    iget-object v1, v1, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v1, v5}, Lflipboard/objs/SectionPageTemplate;->a(Z)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/SectionPageTemplate$Area;

    .line 196
    invoke-virtual {v1, v5}, Lflipboard/objs/SectionPageTemplate$Area;->a(Z)F

    move-result v3

    int-to-float v4, v6

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 197
    invoke-virtual {v1, v5}, Lflipboard/objs/SectionPageTemplate$Area;->b(Z)F

    move-result v4

    int-to-float v8, v7

    mul-float/2addr v4, v8

    float-to-int v4, v4

    .line 199
    invoke-virtual {v1, v5}, Lflipboard/objs/SectionPageTemplate$Area;->c(Z)F

    move-result v8

    int-to-float v9, v6

    mul-float/2addr v8, v9

    float-to-int v8, v8

    .line 200
    invoke-virtual {v1, v5}, Lflipboard/objs/SectionPageTemplate$Area;->d(Z)F

    move-result v9

    int-to-float v10, v7

    mul-float/2addr v9, v10

    float-to-int v9, v9

    .line 203
    sub-int v8, v6, v8

    invoke-static {v8, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 204
    sub-int v8, v7, v9

    invoke-static {v8, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 207
    invoke-virtual {v1, v5}, Lflipboard/objs/SectionPageTemplate$Area;->c(Z)F

    move-result v8

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-lez v8, :cond_8

    .line 208
    iget v8, p0, Lflipboard/gui/section/item/AlbumItem;->e:I

    sub-int/2addr v3, v8

    .line 210
    :cond_8
    invoke-virtual {v1, v5}, Lflipboard/objs/SectionPageTemplate$Area;->d(Z)F

    move-result v1

    const/4 v8, 0x0

    cmpl-float v1, v1, v8

    if-lez v1, :cond_a

    .line 211
    iget v1, p0, Lflipboard/gui/section/item/AlbumItem;->e:I

    sub-int v1, v4, v1

    .line 213
    :goto_5
    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/view/View;->measure(II)V

    .line 193
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 215
    :cond_9
    invoke-direct {p0}, Lflipboard/gui/section/item/AlbumItem;->a()V

    .line 216
    return-void

    :cond_a
    move v1, v4

    goto :goto_5

    :cond_b
    move v0, v8

    goto/16 :goto_2
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 132
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    iput-object p1, p0, Lflipboard/gui/section/item/AlbumItem;->p:Landroid/view/View$OnClickListener;

    .line 134
    return-void
.end method
