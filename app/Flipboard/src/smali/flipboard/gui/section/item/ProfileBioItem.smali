.class public Lflipboard/gui/section/item/ProfileBioItem;
.super Landroid/view/ViewGroup;
.source "ProfileBioItem.java"

# interfaces
.implements Lflipboard/gui/item/TabletItem;


# instance fields
.field a:Lflipboard/gui/FLImageView;

.field b:Lflipboard/gui/FLImageView;

.field c:Lflipboard/gui/FLLabelTextView;

.field d:Lflipboard/gui/FLTextView;

.field e:Lflipboard/gui/FLButton;

.field private f:Landroid/view/View;

.field private g:Lflipboard/service/Section;

.field private h:Lflipboard/objs/FeedItem;

.field private i:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FlipboardManager;",
            "Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/item/ProfileBioItem;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem;->g:Lflipboard/service/Section;

    return-object v0
.end method

.method static synthetic b(Lflipboard/gui/section/item/ProfileBioItem;)Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem;->h:Lflipboard/objs/FeedItem;

    return-object v0
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 177
    iput-object p1, p0, Lflipboard/gui/section/item/ProfileBioItem;->g:Lflipboard/service/Section;

    .line 178
    iput-object p2, p0, Lflipboard/gui/section/item/ProfileBioItem;->h:Lflipboard/objs/FeedItem;

    .line 180
    iget-object v0, p1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    .line 181
    if-eqz v0, :cond_2

    .line 182
    iget-object v1, v0, Lflipboard/service/Section$Meta;->j:Lflipboard/objs/Image;

    if-eqz v1, :cond_3

    .line 183
    iget-object v1, p0, Lflipboard/gui/section/item/ProfileBioItem;->a:Lflipboard/gui/FLImageView;

    iget-object v2, v0, Lflipboard/service/Section$Meta;->j:Lflipboard/objs/Image;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 196
    :goto_0
    iget-object v2, v0, Lflipboard/service/Section$Meta;->i:Lflipboard/objs/FeedSectionLink;

    .line 197
    if-eqz v2, :cond_2

    .line 198
    iget-object v0, v2, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    if-eqz v0, :cond_5

    .line 199
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem;->b:Lflipboard/gui/FLImageView;

    iget-object v1, v2, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 203
    :goto_1
    iget-object v0, v2, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem;->c:Lflipboard/gui/FLLabelTextView;

    iget-object v1, v2, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 206
    :cond_0
    iget-object v0, v2, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, v2, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    .line 207
    :goto_2
    iget-object v1, v2, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, v2, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    .line 208
    :goto_3
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 209
    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileBioItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0049

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 211
    :cond_1
    iget-object v3, p0, Lflipboard/gui/section/item/ProfileBioItem;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileBioItem;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0342

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    aput-object v1, v5, v7

    invoke-static {v4, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    iget-object v1, v2, Lflipboard/objs/FeedSectionLink;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 214
    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileBioItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 220
    :goto_4
    iget-object v1, p0, Lflipboard/gui/section/item/ProfileBioItem;->e:Lflipboard/gui/FLButton;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 221
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem;->e:Lflipboard/gui/FLButton;

    new-instance v1, Lflipboard/gui/section/item/ProfileBioItem$2;

    invoke-direct {v1, p0}, Lflipboard/gui/section/item/ProfileBioItem$2;-><init>(Lflipboard/gui/section/item/ProfileBioItem;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    :cond_2
    return-void

    .line 186
    :cond_3
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v1

    invoke-static {}, Lflipboard/util/AndroidUtil;->d()I

    move-result v2

    mul-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x4

    div-int/lit8 v1, v1, 0xa

    .line 187
    iget-object v2, p1, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-static {v2, v1}, Lflipboard/util/AndroidUtil;->a(Ljava/util/List;I)Lflipboard/objs/FeedItem;

    move-result-object v1

    .line 188
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 189
    iget-object v2, p0, Lflipboard/gui/section/item/ProfileBioItem;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v2, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 190
    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/Section$Meta;->j:Lflipboard/objs/Image;

    .line 191
    invoke-virtual {p1}, Lflipboard/service/Section;->w()V

    goto/16 :goto_0

    .line 193
    :cond_4
    iget-object v1, p0, Lflipboard/gui/section/item/ProfileBioItem;->f:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 201
    :cond_5
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem;->b:Lflipboard/gui/FLImageView;

    const v1, 0x7f02005a

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    goto/16 :goto_1

    .line 206
    :cond_6
    const-string v0, ""

    goto/16 :goto_2

    .line 207
    :cond_7
    const-string v1, ""

    goto/16 :goto_3

    .line 215
    :cond_8
    invoke-virtual {p1}, Lflipboard/service/Section;->s()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 216
    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileBioItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 218
    :cond_9
    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileBioItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d02fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem;->h:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lflipboard/gui/section/item/ProfileBioItem$1;

    invoke-direct {v0, p0}, Lflipboard/gui/section/item/ProfileBioItem$1;-><init>(Lflipboard/gui/section/item/ProfileBioItem;)V

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem;->i:Lflipboard/util/Observer;

    .line 105
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/section/item/ProfileBioItem;->i:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/util/Observer;)V

    .line 106
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 107
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 111
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/section/item/ProfileBioItem;->i:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Lflipboard/util/Observer;)V

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem;->i:Lflipboard/util/Observer;

    .line 113
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 114
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 57
    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileBioItem;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 58
    const v0, 0x7f0a006b

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ProfileBioItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem;->b:Lflipboard/gui/FLImageView;

    .line 59
    const v0, 0x7f0a02a5

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ProfileBioItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem;->a:Lflipboard/gui/FLImageView;

    .line 60
    const v0, 0x7f0a02a7

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ProfileBioItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem;->c:Lflipboard/gui/FLLabelTextView;

    .line 61
    const v0, 0x7f0a02a8

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ProfileBioItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem;->d:Lflipboard/gui/FLTextView;

    .line 62
    const v0, 0x7f0a0260

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ProfileBioItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem;->e:Lflipboard/gui/FLButton;

    .line 63
    const v0, 0x7f0a02a6

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ProfileBioItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileBioItem;->f:Landroid/view/View;

    .line 64
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 150
    sub-int v0, p4, p2

    .line 151
    sub-int v1, p5, p3

    .line 152
    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileBioItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09007b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 155
    mul-int/lit8 v3, v2, 0x2

    .line 157
    iget-object v4, p0, Lflipboard/gui/section/item/ProfileBioItem;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v4, v9, v9, v0, v1}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 159
    sub-int v4, v0, v3

    .line 160
    sub-int v5, v1, v3

    .line 161
    iget-object v6, p0, Lflipboard/gui/section/item/ProfileBioItem;->e:Lflipboard/gui/FLButton;

    iget-object v7, p0, Lflipboard/gui/section/item/ProfileBioItem;->e:Lflipboard/gui/FLButton;

    invoke-virtual {v7}, Lflipboard/gui/FLButton;->getMeasuredWidth()I

    move-result v7

    sub-int v7, v4, v7

    iget-object v8, p0, Lflipboard/gui/section/item/ProfileBioItem;->e:Lflipboard/gui/FLButton;

    invoke-virtual {v8}, Lflipboard/gui/FLButton;->getMeasuredHeight()I

    move-result v8

    sub-int v8, v5, v8

    invoke-virtual {v6, v7, v8, v4, v5}, Lflipboard/gui/FLButton;->layout(IIII)V

    .line 163
    add-int/lit8 v3, v3, 0x0

    .line 164
    iget-object v4, p0, Lflipboard/gui/section/item/ProfileBioItem;->d:Lflipboard/gui/FLTextView;

    iget-object v6, p0, Lflipboard/gui/section/item/ProfileBioItem;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v6

    sub-int v6, v5, v6

    iget-object v7, p0, Lflipboard/gui/section/item/ProfileBioItem;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v3

    invoke-virtual {v4, v3, v6, v7, v5}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 166
    iget-object v4, p0, Lflipboard/gui/section/item/ProfileBioItem;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v5, v4

    sub-int/2addr v4, v2

    .line 167
    iget-object v5, p0, Lflipboard/gui/section/item/ProfileBioItem;->c:Lflipboard/gui/FLLabelTextView;

    iget-object v6, p0, Lflipboard/gui/section/item/ProfileBioItem;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v6

    sub-int v6, v4, v6

    iget-object v7, p0, Lflipboard/gui/section/item/ProfileBioItem;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v3

    invoke-virtual {v5, v3, v6, v7, v4}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 169
    iget-object v5, p0, Lflipboard/gui/section/item/ProfileBioItem;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v4, v5

    sub-int v2, v4, v2

    .line 170
    iget-object v4, p0, Lflipboard/gui/section/item/ProfileBioItem;->b:Lflipboard/gui/FLImageView;

    iget-object v5, p0, Lflipboard/gui/section/item/ProfileBioItem;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v2, v5

    iget-object v6, p0, Lflipboard/gui/section/item/ProfileBioItem;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v6}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v3

    invoke-virtual {v4, v3, v5, v6, v2}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 172
    iget-object v2, p0, Lflipboard/gui/section/item/ProfileBioItem;->f:Landroid/view/View;

    iget-object v3, p0, Lflipboard/gui/section/item/ProfileBioItem;->f:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v1, v3

    invoke-virtual {v2, v9, v3, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 173
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    .line 118
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 119
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 120
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 121
    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileBioItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09007b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 122
    mul-int/lit8 v3, v2, 0x2

    .line 123
    mul-int/lit8 v4, v3, 0x2

    sub-int v4, v0, v4

    .line 124
    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v1, v3

    .line 126
    iget-object v5, p0, Lflipboard/gui/section/item/ProfileBioItem;->a:Lflipboard/gui/FLImageView;

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 128
    iget-object v5, p0, Lflipboard/gui/section/item/ProfileBioItem;->e:Lflipboard/gui/FLButton;

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lflipboard/gui/FLButton;->measure(II)V

    .line 130
    iget-object v5, p0, Lflipboard/gui/section/item/ProfileBioItem;->e:Lflipboard/gui/FLButton;

    invoke-virtual {v5}, Lflipboard/gui/FLButton;->getMeasuredWidth()I

    move-result v5

    sub-int/2addr v4, v5

    sub-int/2addr v4, v2

    .line 131
    iget-object v5, p0, Lflipboard/gui/section/item/ProfileBioItem;->d:Lflipboard/gui/FLTextView;

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 133
    iget-object v5, p0, Lflipboard/gui/section/item/ProfileBioItem;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v3, v5

    sub-int/2addr v3, v2

    .line 134
    iget-object v5, p0, Lflipboard/gui/section/item/ProfileBioItem;->c:Lflipboard/gui/FLLabelTextView;

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, v4, v6}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 136
    iget-object v4, p0, Lflipboard/gui/section/item/ProfileBioItem;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v3, v4

    sub-int v2, v3, v2

    .line 137
    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileBioItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900a3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 138
    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 139
    iget-object v4, p0, Lflipboard/gui/section/item/ProfileBioItem;->b:Lflipboard/gui/FLImageView;

    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v4, v5, v3}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 142
    iget-object v3, p0, Lflipboard/gui/section/item/ProfileBioItem;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v3}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 143
    sub-int v2, v1, v2

    .line 144
    iget-object v3, p0, Lflipboard/gui/section/item/ProfileBioItem;->f:Landroid/view/View;

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v3, v4, v2}, Landroid/view/View;->measure(II)V

    .line 145
    invoke-virtual {p0, v0, v1}, Lflipboard/gui/section/item/ProfileBioItem;->setMeasuredDimension(II)V

    .line 146
    return-void
.end method
