.class public Lflipboard/gui/section/item/ImageItemPhone;
.super Landroid/view/ViewGroup;
.source "ImageItemPhone.java"

# interfaces
.implements Lflipboard/gui/FLViewIntf;
.implements Lflipboard/gui/item/TabletItem;


# instance fields
.field a:Lflipboard/objs/FeedItem;

.field b:Lflipboard/gui/FLImageView;

.field c:Lflipboard/gui/FLStaticTextView;

.field d:Landroid/widget/ImageButton;

.field e:Lflipboard/gui/section/AttributionServiceInfo;

.field f:Landroid/view/View;

.field g:Landroid/view/View;

.field h:Lflipboard/gui/section/scrolling/ItemActionBar;

.field i:Lflipboard/gui/section/AttributionSocial;

.field private j:Lflipboard/gui/section/AttributionSmall;

.field private k:Landroid/view/ViewGroup$LayoutParams;

.field private l:Z

.field private m:Landroid/graphics/drawable/Drawable;

.field private n:Z

.field private o:I

.field private p:Z

.field private final q:I

.field private r:Z

.field private s:Z

.field private t:Lflipboard/service/Section;

.field private u:I

.field private v:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    const v0, 0x7fffffff

    iput v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->o:I

    .line 79
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->q:I

    .line 80
    return-void
.end method

.method public static a(Lflipboard/objs/FeedItem;FF)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 202
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    .line 203
    if-eqz v1, :cond_0

    .line 204
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v2}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 205
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, p1

    div-float/2addr v3, v2

    .line 206
    invoke-static {}, Lflipboard/util/AndroidUtil;->d()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p2

    div-float v2, v4, v2

    .line 207
    div-float v2, v3, v2

    .line 208
    invoke-virtual {v1}, Lflipboard/objs/Image;->h()F

    move-result v1

    .line 209
    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x3ecccccd    # 0.4f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    const/4 v0, 0x1

    .line 211
    :cond_0
    return v0
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 100
    iput-object p2, p0, Lflipboard/gui/section/item/ImageItemPhone;->a:Lflipboard/objs/FeedItem;

    .line 101
    iput-object p1, p0, Lflipboard/gui/section/item/ImageItemPhone;->t:Lflipboard/service/Section;

    .line 102
    invoke-virtual {p0, p2}, Lflipboard/gui/section/item/ImageItemPhone;->setTag(Ljava/lang/Object;)V

    .line 103
    iget-object v2, p2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v3, "video"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 104
    new-instance v2, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->d:Landroid/widget/ImageButton;

    .line 105
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->d:Landroid/widget/ImageButton;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 106
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->d:Landroid/widget/ImageButton;

    const v3, 0x7f02004f

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 107
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->d:Landroid/widget/ImageButton;

    new-instance v3, Lflipboard/gui/section/item/ImageItemPhone$1;

    invoke-direct {v3, p0}, Lflipboard/gui/section/item/ImageItemPhone$1;-><init>(Lflipboard/gui/section/item/ImageItemPhone;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->d:Landroid/widget/ImageButton;

    invoke-virtual {p0, v2}, Lflipboard/gui/section/item/ImageItemPhone;->addView(Landroid/view/View;)V

    .line 116
    :cond_0
    iget-boolean v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->n:Z

    iget v3, p0, Lflipboard/gui/section/item/ImageItemPhone;->o:I

    invoke-static {v2, v3}, Lflipboard/util/AndroidUtil;->a(ZI)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 117
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v2, p2}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 126
    :cond_1
    :goto_0
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v2

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->D()Ljava/lang/String;

    move-result-object v2

    .line 127
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->I()Ljava/lang/String;

    move-result-object v3

    .line 128
    if-eqz v2, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 129
    :goto_1
    if-nez v0, :cond_5

    .line 130
    if-eqz v3, :cond_4

    .line 132
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    :goto_2
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->e:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/AttributionServiceInfo;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 139
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->j:Lflipboard/gui/section/AttributionSmall;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/AttributionSmall;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 140
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->h:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/scrolling/ItemActionBar;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 141
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->ab()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 142
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->i:Lflipboard/gui/section/AttributionSocial;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/AttributionSocial;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 143
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->i:Lflipboard/gui/section/AttributionSocial;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionSocial;->setVisibility(I)V

    .line 147
    :goto_3
    return-void

    .line 119
    :cond_2
    iput-boolean v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->l:Z

    .line 120
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v2

    .line 121
    if-eqz v2, :cond_1

    .line 122
    iget-object v3, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    iget v4, v2, Lflipboard/objs/Image;->f:I

    iget v2, v2, Lflipboard/objs/Image;->g:I

    invoke-virtual {v3, v4, v2}, Lflipboard/gui/FLImageView;->a(II)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 128
    goto :goto_1

    .line 133
    :cond_4
    invoke-static {p2}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 134
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 136
    :cond_5
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    goto :goto_2

    .line 145
    :cond_6
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->i:Lflipboard/gui/section/AttributionSocial;

    invoke-virtual {v0, v5}, Lflipboard/gui/section/AttributionSocial;->setVisibility(I)V

    goto :goto_3
.end method

.method public final a(ZI)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 176
    iput-boolean p1, p0, Lflipboard/gui/section/item/ImageItemPhone;->n:Z

    .line 177
    iput p2, p0, Lflipboard/gui/section/item/ImageItemPhone;->o:I

    .line 178
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "ImageItemTablet:onPageOffsetChange"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 179
    invoke-static {p1, p2}, Lflipboard/util/AndroidUtil;->a(ZI)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    iget-boolean v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->l:Z

    if-eqz v0, :cond_0

    .line 181
    iput-boolean v3, p0, Lflipboard/gui/section/item/ImageItemPhone;->l:Z

    .line 182
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    iget-boolean v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->l:Z

    if-nez v0, :cond_0

    .line 186
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->l:Z

    .line 187
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ImageItemPhone;->removeView(Landroid/view/View;)V

    .line 188
    new-instance v0, Lflipboard/gui/FLImageView;

    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLImageView;-><init>(Landroid/content/Context;Lflipboard/gui/FLImageView;)V

    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    .line 189
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 190
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->k:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, v0, v3, v1}, Lflipboard/gui/section/item/ImageItemPhone;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->a:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 84
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 85
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 86
    const v0, 0x7f0a0040

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ImageItemPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    .line 87
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->k:Landroid/view/ViewGroup$LayoutParams;

    .line 88
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->m:Landroid/graphics/drawable/Drawable;

    .line 89
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ImageItemPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    .line 90
    const v0, 0x7f0a008b

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ImageItemPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionServiceInfo;

    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->e:Lflipboard/gui/section/AttributionServiceInfo;

    .line 91
    const v0, 0x7f0a01fa

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ImageItemPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionSmall;

    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->j:Lflipboard/gui/section/AttributionSmall;

    .line 92
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->u:I

    .line 94
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 95
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 320
    sub-int v0, p5, p3

    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getPaddingBottom()I

    move-result v1

    sub-int v2, v0, v1

    .line 321
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getPaddingLeft()I

    move-result v3

    .line 322
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getPaddingTop()I

    move-result v1

    .line 324
    iget-boolean v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->s:Z

    if-eqz v0, :cond_9

    .line 325
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f09000b

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 327
    :goto_0
    iget-object v4, p0, Lflipboard/gui/section/item/ImageItemPhone;->e:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v4}, Lflipboard/gui/section/AttributionServiceInfo;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 328
    iget-object v5, p0, Lflipboard/gui/section/item/ImageItemPhone;->e:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v5}, Lflipboard/gui/section/AttributionServiceInfo;->getVisibility()I

    move-result v5

    if-eq v5, v8, :cond_0

    .line 329
    iget-object v5, p0, Lflipboard/gui/section/item/ImageItemPhone;->e:Lflipboard/gui/section/AttributionServiceInfo;

    iget-object v6, p0, Lflipboard/gui/section/item/ImageItemPhone;->e:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v6}, Lflipboard/gui/section/AttributionServiceInfo;->getMeasuredWidth()I

    move-result v6

    invoke-virtual {v5, v7, v0, v6, v4}, Lflipboard/gui/section/AttributionServiceInfo;->layout(IIII)V

    .line 332
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->j:Lflipboard/gui/section/AttributionSmall;

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionSmall;->getVisibility()I

    move-result v0

    if-eq v0, v8, :cond_8

    .line 333
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->j:Lflipboard/gui/section/AttributionSmall;

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionSmall;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v2, v0

    .line 335
    :goto_1
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->h:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v2}, Lflipboard/gui/section/scrolling/ItemActionBar;->getVisibility()I

    move-result v2

    if-eq v2, v8, :cond_1

    .line 336
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->h:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v2}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v0, v2

    .line 337
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->h:Lflipboard/gui/section/scrolling/ItemActionBar;

    iget-object v5, p0, Lflipboard/gui/section/item/ImageItemPhone;->h:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v5}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredWidth()I

    move-result v5

    iget-object v6, p0, Lflipboard/gui/section/item/ImageItemPhone;->h:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v6}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v2, v7, v0, v5, v6}, Lflipboard/gui/section/scrolling/ItemActionBar;->layout(IIII)V

    .line 339
    :cond_1
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->i:Lflipboard/gui/section/AttributionSocial;

    invoke-virtual {v2}, Lflipboard/gui/section/AttributionSocial;->getVisibility()I

    move-result v2

    if-eq v2, v8, :cond_2

    .line 340
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->i:Lflipboard/gui/section/AttributionSocial;

    invoke-virtual {v2}, Lflipboard/gui/section/AttributionSocial;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v0, v2

    .line 341
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->i:Lflipboard/gui/section/AttributionSocial;

    iget-object v5, p0, Lflipboard/gui/section/item/ImageItemPhone;->i:Lflipboard/gui/section/AttributionSocial;

    invoke-virtual {v5}, Lflipboard/gui/section/AttributionSocial;->getMeasuredWidth()I

    move-result v5

    iget-object v6, p0, Lflipboard/gui/section/item/ImageItemPhone;->i:Lflipboard/gui/section/AttributionSocial;

    invoke-virtual {v6}, Lflipboard/gui/section/AttributionSocial;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v2, v7, v0, v5, v6}, Lflipboard/gui/section/AttributionSocial;->layout(IIII)V

    .line 343
    :cond_2
    iget-boolean v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->v:Z

    if-nez v2, :cond_6

    .line 344
    sub-int v1, v0, v4

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v4

    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 345
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    iget-object v4, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v3

    iget-object v5, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v2, v3, v1, v4, v5}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 351
    :goto_2
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    .line 352
    iget v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->q:I

    .line 353
    iget v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->u:I

    sub-int/2addr v0, v2

    .line 354
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v3, p0, Lflipboard/gui/section/item/ImageItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v0, v3

    iget-object v4, p0, Lflipboard/gui/section/item/ImageItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v1, v3, v4, v0}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 355
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getBottom()I

    move-result v0

    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getTop()I

    move-result v2

    if-lt v0, v2, :cond_7

    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0800aa

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_3
    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    .line 358
    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 359
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->g:Landroid/view/View;

    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getBottom()I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->g:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->g:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v3}, Lflipboard/gui/FLImageView;->getBottom()I

    move-result v3

    invoke-virtual {v0, v7, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 362
    :cond_4
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->d:Landroid/widget/ImageButton;

    if-eqz v0, :cond_5

    .line 363
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->d:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 364
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getTop()I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->d:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getMeasuredHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 365
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->d:Landroid/widget/ImageButton;

    iget-object v3, p0, Lflipboard/gui/section/item/ImageItemPhone;->d:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lflipboard/gui/section/item/ImageItemPhone;->d:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/widget/ImageButton;->layout(IIII)V

    .line 367
    :cond_5
    return-void

    .line 347
    :cond_6
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    iget-object v4, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v3

    iget-object v5, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v2, v3, v1, v4, v5}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 348
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->f:Landroid/view/View;

    iget-object v4, p0, Lflipboard/gui/section/item/ImageItemPhone;->f:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v3

    iget-object v5, p0, Lflipboard/gui/section/item/ImageItemPhone;->f:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_2

    .line 355
    :cond_7
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080008

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto/16 :goto_3

    :cond_8
    move v0, v2

    goto/16 :goto_1

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    .line 217
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 218
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 220
    int-to-float v0, v3

    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 221
    const/16 v1, 0x12c

    if-ge v0, v1, :cond_10

    .line 222
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->e:Lflipboard/gui/section/AttributionServiceInfo;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionServiceInfo;->setVisibility(I)V

    .line 223
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->h:Lflipboard/gui/section/scrolling/ItemActionBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/section/scrolling/ItemActionBar;->setVisibility(I)V

    .line 224
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->j:Lflipboard/gui/section/AttributionSmall;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionSmall;->setVisibility(I)V

    .line 230
    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getPaddingLeft()I

    move-result v0

    sub-int v0, v3, v0

    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getPaddingRight()I

    move-result v1

    sub-int v5, v0, v1

    .line 231
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getPaddingTop()I

    move-result v0

    sub-int v0, v4, v0

    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getPaddingBottom()I

    move-result v1

    sub-int v6, v0, v1

    .line 232
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->e:Lflipboard/gui/section/AttributionServiceInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionServiceInfo;->setDrawTopDivider(Z)V

    .line 233
    iget v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->q:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v7, v5, v0

    .line 234
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getPaddingBottom()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    sub-int v2, v6, v0

    .line 236
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->e:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionServiceInfo;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 237
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->e:Lflipboard/gui/section/AttributionServiceInfo;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v3, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    const/high16 v8, -0x80000000

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v0, v1, v8}, Lflipboard/gui/section/AttributionServiceInfo;->measure(II)V

    .line 239
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->j:Lflipboard/gui/section/AttributionSmall;

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionSmall;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    .line 240
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->j:Lflipboard/gui/section/AttributionSmall;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v3, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    const/high16 v8, -0x80000000

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v0, v1, v8}, Lflipboard/gui/section/AttributionSmall;->measure(II)V

    .line 242
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->h:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/ItemActionBar;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    .line 243
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->h:Lflipboard/gui/section/scrolling/ItemActionBar;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v3, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    const/high16 v8, -0x80000000

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v0, v1, v8}, Lflipboard/gui/section/scrolling/ItemActionBar;->measure(II)V

    .line 245
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->i:Lflipboard/gui/section/AttributionSocial;

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionSocial;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    .line 246
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->i:Lflipboard/gui/section/AttributionSocial;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v3, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    const/high16 v8, -0x80000000

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v0, v1, v8}, Lflipboard/gui/section/AttributionSocial;->measure(II)V

    .line 249
    :cond_3
    int-to-float v0, v3

    int-to-float v1, v4

    div-float/2addr v0, v1

    .line 250
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    .line 251
    if-eqz v1, :cond_13

    .line 252
    iget v8, v1, Lflipboard/objs/Image;->f:I

    int-to-float v8, v8

    iget v1, v1, Lflipboard/objs/Image;->g:I

    int-to-float v1, v1

    div-float v1, v8, v1

    .line 255
    sub-float v0, v1, v0

    const v1, 0x3ecccccd    # 0.4f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_11

    const/4 v0, 0x1

    .line 256
    :goto_1
    iget-boolean v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->s:Z

    if-nez v1, :cond_4

    if-eqz v0, :cond_12

    :cond_4
    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->v:Z

    .line 260
    :goto_3
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->e:Lflipboard/gui/section/AttributionServiceInfo;

    iget-boolean v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->v:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->p:Z

    if-eqz v0, :cond_14

    :cond_5
    const/4 v0, 0x1

    :goto_4
    invoke-virtual {v1, v0}, Lflipboard/gui/section/AttributionServiceInfo;->setInverted(Z)V

    .line 261
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->j:Lflipboard/gui/section/AttributionSmall;

    iget-boolean v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->v:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->p:Z

    if-eqz v0, :cond_15

    :cond_6
    const/4 v0, 0x1

    :goto_5
    invoke-virtual {v1, v0}, Lflipboard/gui/section/AttributionSmall;->setInverted(Z)V

    .line 262
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->h:Lflipboard/gui/section/scrolling/ItemActionBar;

    iget-boolean v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->v:Z

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->p:Z

    if-eqz v0, :cond_16

    :cond_7
    const/4 v0, 0x1

    :goto_6
    invoke-virtual {v1, v0}, Lflipboard/gui/section/scrolling/ItemActionBar;->setInverted(Z)V

    .line 263
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->i:Lflipboard/gui/section/AttributionSocial;

    iget-boolean v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->v:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->p:Z

    if-eqz v0, :cond_17

    :cond_8
    const/4 v0, 0x1

    :goto_7
    invoke-virtual {v1, v0}, Lflipboard/gui/section/AttributionSocial;->setInverted(Z)V

    .line 264
    const/4 v0, 0x0

    .line 265
    iget-boolean v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->v:Z

    if-eqz v1, :cond_18

    .line 267
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v6, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v1, v8, v9}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 268
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->g:Landroid/view/View;

    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 269
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->j:Lflipboard/gui/section/AttributionSmall;

    invoke-virtual {v1}, Lflipboard/gui/section/AttributionSmall;->getVisibility()I

    move-result v1

    const/16 v8, 0x8

    if-eq v1, v8, :cond_9

    .line 270
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->j:Lflipboard/gui/section/AttributionSmall;

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionSmall;->getMeasuredHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 272
    :cond_9
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->h:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v1}, Lflipboard/gui/section/scrolling/ItemActionBar;->getVisibility()I

    move-result v1

    const/16 v8, 0x8

    if-eq v1, v8, :cond_a

    .line 273
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->h:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v1}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 275
    :cond_a
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->i:Lflipboard/gui/section/AttributionSocial;

    invoke-virtual {v1}, Lflipboard/gui/section/AttributionSocial;->getVisibility()I

    move-result v1

    const/16 v8, 0x8

    if-eq v1, v8, :cond_b

    .line 276
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->i:Lflipboard/gui/section/AttributionSocial;

    invoke-virtual {v1}, Lflipboard/gui/section/AttributionSocial;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 279
    :cond_b
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->e:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v1}, Lflipboard/gui/section/AttributionServiceInfo;->getVisibility()I

    move-result v1

    const/16 v8, 0x8

    if-eq v1, v8, :cond_1d

    .line 280
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->f:Landroid/view/View;

    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 281
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->e:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v1}, Lflipboard/gui/section/AttributionServiceInfo;->getMeasuredHeight()I

    move-result v1

    int-to-double v8, v1

    const-wide/high16 v10, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v8, v10

    double-to-int v1, v8

    .line 282
    iget-boolean v8, p0, Lflipboard/gui/section/item/ImageItemPhone;->s:Z

    if-eqz v8, :cond_c

    .line 283
    invoke-virtual {p0}, Lflipboard/gui/section/item/ImageItemPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09000b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    add-int/2addr v1, v8

    .line 285
    :cond_c
    iget-object v8, p0, Lflipboard/gui/section/item/ImageItemPhone;->f:Landroid/view/View;

    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v8, p1, v1}, Landroid/view/View;->measure(II)V

    move v1, v2

    .line 305
    :goto_8
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_d

    .line 306
    iget-object v2, p0, Lflipboard/gui/section/item/ImageItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    const/high16 v8, -0x80000000

    invoke-static {v7, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    const/high16 v8, -0x80000000

    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v2, v7, v1}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 307
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 309
    :cond_d
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_e

    .line 310
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->g:Landroid/view/View;

    int-to-double v8, v0

    const-wide/high16 v10, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v8, v10

    double-to-int v0, v8

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v1, p1, v0}, Landroid/view/View;->measure(II)V

    .line 312
    :cond_e
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->d:Landroid/widget/ImageButton;

    if-eqz v0, :cond_f

    .line 313
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->d:Landroid/widget/ImageButton;

    const/high16 v1, -0x80000000

    invoke-static {v5, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    const/high16 v2, -0x80000000

    invoke-static {v6, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageButton;->measure(II)V

    .line 315
    :cond_f
    invoke-virtual {p0, v3, v4}, Lflipboard/gui/section/item/ImageItemPhone;->setMeasuredDimension(II)V

    .line 316
    return-void

    .line 226
    :cond_10
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->e:Lflipboard/gui/section/AttributionServiceInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionServiceInfo;->setVisibility(I)V

    .line 227
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->h:Lflipboard/gui/section/scrolling/ItemActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/section/scrolling/ItemActionBar;->setVisibility(I)V

    .line 228
    iget-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->j:Lflipboard/gui/section/AttributionSmall;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionSmall;->setVisibility(I)V

    goto/16 :goto_0

    .line 255
    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 256
    :cond_12
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 258
    :cond_13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->v:Z

    goto/16 :goto_3

    .line 260
    :cond_14
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 261
    :cond_15
    const/4 v0, 0x0

    goto/16 :goto_5

    .line 262
    :cond_16
    const/4 v0, 0x0

    goto/16 :goto_6

    .line 263
    :cond_17
    const/4 v0, 0x0

    goto/16 :goto_7

    .line 289
    :cond_18
    const/4 v1, 0x0

    .line 290
    iget-object v8, p0, Lflipboard/gui/section/item/ImageItemPhone;->e:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v8}, Lflipboard/gui/section/AttributionServiceInfo;->getVisibility()I

    move-result v8

    const/16 v9, 0x8

    if-eq v8, v9, :cond_19

    .line 291
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->e:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v1}, Lflipboard/gui/section/AttributionServiceInfo;->getMeasuredHeight()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    .line 293
    :cond_19
    iget-object v8, p0, Lflipboard/gui/section/item/ImageItemPhone;->j:Lflipboard/gui/section/AttributionSmall;

    invoke-virtual {v8}, Lflipboard/gui/section/AttributionSmall;->getVisibility()I

    move-result v8

    const/16 v9, 0x8

    if-eq v8, v9, :cond_1a

    .line 294
    iget-object v8, p0, Lflipboard/gui/section/item/ImageItemPhone;->j:Lflipboard/gui/section/AttributionSmall;

    invoke-virtual {v8}, Lflipboard/gui/section/AttributionSmall;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v1, v8

    .line 296
    :cond_1a
    iget-object v8, p0, Lflipboard/gui/section/item/ImageItemPhone;->h:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v8}, Lflipboard/gui/section/scrolling/ItemActionBar;->getVisibility()I

    move-result v8

    const/16 v9, 0x8

    if-eq v8, v9, :cond_1b

    .line 297
    iget-object v8, p0, Lflipboard/gui/section/item/ImageItemPhone;->h:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v8}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v1, v8

    .line 299
    :cond_1b
    iget-object v8, p0, Lflipboard/gui/section/item/ImageItemPhone;->i:Lflipboard/gui/section/AttributionSocial;

    invoke-virtual {v8}, Lflipboard/gui/section/AttributionSocial;->getVisibility()I

    move-result v8

    const/16 v9, 0x8

    if-eq v8, v9, :cond_1c

    .line 300
    iget-object v8, p0, Lflipboard/gui/section/item/ImageItemPhone;->i:Lflipboard/gui/section/AttributionSocial;

    invoke-virtual {v8}, Lflipboard/gui/section/AttributionSocial;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v1, v8

    .line 302
    :cond_1c
    iget-object v8, p0, Lflipboard/gui/section/item/ImageItemPhone;->b:Lflipboard/gui/FLImageView;

    sub-int v1, v6, v1

    const/high16 v9, -0x80000000

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v8, p1, v1}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 303
    iget-object v1, p0, Lflipboard/gui/section/item/ImageItemPhone;->e:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v1}, Lflipboard/gui/section/AttributionServiceInfo;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v2, v1

    goto/16 :goto_8

    :cond_1d
    move v1, v2

    goto/16 :goto_8
.end method

.method public setInverted(Z)V
    .locals 0

    .prologue
    .line 156
    iput-boolean p1, p0, Lflipboard/gui/section/item/ImageItemPhone;->p:Z

    .line 157
    return-void
.end method

.method public setIsFullBleed(Z)V
    .locals 0

    .prologue
    .line 166
    iput-boolean p1, p0, Lflipboard/gui/section/item/ImageItemPhone;->s:Z

    .line 167
    return-void
.end method

.method public setIsOneUp(Z)V
    .locals 0

    .prologue
    .line 161
    iput-boolean p1, p0, Lflipboard/gui/section/item/ImageItemPhone;->r:Z

    .line 162
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 197
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    return-void
.end method
