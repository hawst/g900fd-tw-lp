.class public Lflipboard/gui/section/item/ProfileSummaryItem;
.super Landroid/view/ViewGroup;
.source "ProfileSummaryItem.java"

# interfaces
.implements Lflipboard/gui/item/TabletItem;


# instance fields
.field a:Lflipboard/service/Section;

.field private b:Landroid/widget/LinearLayout;

.field private c:Lflipboard/gui/FLTextView;

.field private d:Lflipboard/gui/FLTextView;

.field private e:Lflipboard/gui/FLTextView;

.field private f:Lflipboard/gui/FLTextView;

.field private g:Lflipboard/objs/FeedItem;

.field private h:Lflipboard/gui/MagazineThumbView;

.field private i:Lflipboard/gui/MagazineThumbView;

.field private j:Lflipboard/gui/MagazineThumbView;

.field private k:Lflipboard/gui/MagazineThumbView;

.field private l:Landroid/widget/LinearLayout;

.field private m:Landroid/graphics/Paint;

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    return-void
.end method

.method private a(Lflipboard/objs/SidebarGroup;I)I
    .locals 7

    .prologue
    const/4 v3, 0x1

    .line 159
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v1, p2

    .line 160
    :goto_0
    if-ge p2, v4, :cond_5

    .line 161
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 162
    if-eqz v0, :cond_6

    .line 163
    iget-object v2, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v3

    .line 164
    :goto_1
    if-nez p2, :cond_2

    .line 165
    iget-object v5, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->h:Lflipboard/gui/MagazineThumbView;

    iget-object v6, v0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    invoke-virtual {v5, v6, v2}, Lflipboard/gui/MagazineThumbView;->a(Lflipboard/objs/Image;Z)V

    .line 166
    iget-object v2, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->h:Lflipboard/gui/MagazineThumbView;

    iget-object v5, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lflipboard/gui/MagazineThumbView;->setTitle(Ljava/lang/String;)V

    .line 167
    iget-object v2, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->h:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v2, v0}, Lflipboard/gui/MagazineThumbView;->setTag(Ljava/lang/Object;)V

    .line 168
    add-int/lit8 v0, v1, 0x1

    .line 160
    :goto_2
    add-int/lit8 p2, p2, 0x1

    move v1, v0

    goto :goto_0

    .line 163
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 169
    :cond_2
    if-ne p2, v3, :cond_3

    .line 170
    iget-object v5, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->i:Lflipboard/gui/MagazineThumbView;

    iget-object v6, v0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    invoke-virtual {v5, v6, v2}, Lflipboard/gui/MagazineThumbView;->a(Lflipboard/objs/Image;Z)V

    .line 171
    iget-object v2, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->i:Lflipboard/gui/MagazineThumbView;

    iget-object v5, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lflipboard/gui/MagazineThumbView;->setTitle(Ljava/lang/String;)V

    .line 172
    iget-object v2, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->i:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v2, v0}, Lflipboard/gui/MagazineThumbView;->setTag(Ljava/lang/Object;)V

    .line 173
    add-int/lit8 v0, v1, 0x1

    goto :goto_2

    .line 174
    :cond_3
    const/4 v5, 0x2

    if-ne p2, v5, :cond_4

    .line 175
    iget-object v5, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->j:Lflipboard/gui/MagazineThumbView;

    iget-object v6, v0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    invoke-virtual {v5, v6, v2}, Lflipboard/gui/MagazineThumbView;->a(Lflipboard/objs/Image;Z)V

    .line 176
    iget-object v2, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->j:Lflipboard/gui/MagazineThumbView;

    iget-object v5, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lflipboard/gui/MagazineThumbView;->setTitle(Ljava/lang/String;)V

    .line 177
    iget-object v2, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->j:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v2, v0}, Lflipboard/gui/MagazineThumbView;->setTag(Ljava/lang/Object;)V

    .line 178
    add-int/lit8 v0, v1, 0x1

    goto :goto_2

    .line 179
    :cond_4
    const/4 v5, 0x3

    if-ne p2, v5, :cond_6

    iget-boolean v5, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->n:Z

    if-eqz v5, :cond_6

    .line 180
    iget-object v5, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->k:Lflipboard/gui/MagazineThumbView;

    iget-object v6, v0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    invoke-virtual {v5, v6, v2}, Lflipboard/gui/MagazineThumbView;->a(Lflipboard/objs/Image;Z)V

    .line 181
    iget-object v2, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->k:Lflipboard/gui/MagazineThumbView;

    iget-object v5, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lflipboard/gui/MagazineThumbView;->setTitle(Ljava/lang/String;)V

    .line 182
    iget-object v2, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->k:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v2, v0}, Lflipboard/gui/MagazineThumbView;->setTag(Ljava/lang/Object;)V

    .line 183
    add-int/lit8 v0, v1, 0x1

    goto :goto_2

    .line 187
    :cond_5
    return v1

    :cond_6
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 9

    .prologue
    .line 120
    iput-object p1, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->a:Lflipboard/service/Section;

    .line 121
    iput-object p2, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->g:Lflipboard/objs/FeedItem;

    .line 123
    iget-object v0, p1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v1, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    .line 124
    if-eqz v1, :cond_6

    .line 125
    const/4 v0, 0x0

    .line 126
    const/4 v2, 0x0

    .line 127
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    .line 128
    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    const-string v4, "magazines"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 129
    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->f:Ljava/util/List;

    if-eqz v1, :cond_3

    .line 130
    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/SidebarGroup$Metrics;

    .line 131
    iget-object v4, v1, Lflipboard/objs/SidebarGroup$Metrics;->b:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, v1, Lflipboard/objs/SidebarGroup$Metrics;->b:Ljava/lang/String;

    .line 132
    :goto_2
    iget-object v7, v1, Lflipboard/objs/SidebarGroup$Metrics;->a:Ljava/lang/String;

    if-eqz v7, :cond_2

    iget-object v7, v1, Lflipboard/objs/SidebarGroup$Metrics;->a:Ljava/lang/String;

    const-string v8, "magazineCount"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 133
    iget-object v7, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->e:Lflipboard/gui/FLTextView;

    invoke-virtual {v7, v4}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v4, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->f:Lflipboard/gui/FLTextView;

    iget-object v1, v1, Lflipboard/objs/SidebarGroup$Metrics;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 131
    :cond_1
    const-string v4, "0"

    goto :goto_2

    .line 135
    :cond_2
    iget-object v7, v1, Lflipboard/objs/SidebarGroup$Metrics;->a:Ljava/lang/String;

    if-eqz v7, :cond_0

    iget-object v7, v1, Lflipboard/objs/SidebarGroup$Metrics;->a:Ljava/lang/String;

    const-string v8, "readers"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 136
    iget-object v7, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v7, v4}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v4, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->d:Lflipboard/gui/FLTextView;

    iget-object v1, v1, Lflipboard/objs/SidebarGroup$Metrics;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 141
    :cond_3
    invoke-direct {p0, v0, v3}, Lflipboard/gui/section/item/ProfileSummaryItem;->a(Lflipboard/objs/SidebarGroup;I)I

    move-result v0

    move v3, v0

    goto :goto_0

    .line 142
    :cond_4
    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    const-string v4, "contributor"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    :goto_3
    move-object v2, v0

    .line 146
    goto/16 :goto_0

    .line 147
    :cond_5
    if-eqz v2, :cond_6

    .line 148
    invoke-direct {p0, v2, v3}, Lflipboard/gui/section/item/ProfileSummaryItem;->a(Lflipboard/objs/SidebarGroup;I)I

    .line 151
    :cond_6
    return-void

    :cond_7
    move-object v0, v2

    goto :goto_3
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->g:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 256
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 257
    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileSummaryItem;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 258
    iget-object v1, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getBottom()I

    move-result v4

    .line 259
    iget-object v1, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getTop()I

    move-result v2

    .line 261
    int-to-float v1, v0

    int-to-float v2, v2

    int-to-float v3, v0

    int-to-float v4, v4

    iget-object v5, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->m:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 262
    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 61
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 62
    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileSummaryItem;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 63
    const v0, 0x7f0a02b1

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ProfileSummaryItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->b:Landroid/widget/LinearLayout;

    .line 64
    const v0, 0x7f0a02ac

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ProfileSummaryItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->c:Lflipboard/gui/FLTextView;

    .line 65
    const v0, 0x7f0a02ad

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ProfileSummaryItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->d:Lflipboard/gui/FLTextView;

    .line 66
    const v0, 0x7f0a02af

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ProfileSummaryItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->e:Lflipboard/gui/FLTextView;

    .line 67
    const v0, 0x7f0a02b0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ProfileSummaryItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->f:Lflipboard/gui/FLTextView;

    .line 68
    const v0, 0x7f0a02ae

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ProfileSummaryItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->l:Landroid/widget/LinearLayout;

    .line 69
    const v0, 0x7f0a02b2

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ProfileSummaryItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/MagazineThumbView;

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->h:Lflipboard/gui/MagazineThumbView;

    .line 70
    const v0, 0x7f0a02b3

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ProfileSummaryItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/MagazineThumbView;

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->i:Lflipboard/gui/MagazineThumbView;

    .line 71
    const v0, 0x7f0a02b4

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ProfileSummaryItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/MagazineThumbView;

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->j:Lflipboard/gui/MagazineThumbView;

    .line 72
    const v0, 0x7f0a02b8

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/ProfileSummaryItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/MagazineThumbView;

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->k:Lflipboard/gui/MagazineThumbView;

    .line 74
    new-instance v2, Lflipboard/gui/section/item/ProfileSummaryItem$1;

    invoke-direct {v2, p0}, Lflipboard/gui/section/item/ProfileSummaryItem$1;-><init>(Lflipboard/gui/section/item/ProfileSummaryItem;)V

    .line 83
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->h:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v0, v4}, Lflipboard/gui/MagazineThumbView;->setImage(Lflipboard/objs/Image;)V

    .line 84
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->h:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v0, v2}, Lflipboard/gui/MagazineThumbView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->i:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v0, v4}, Lflipboard/gui/MagazineThumbView;->setImage(Lflipboard/objs/Image;)V

    .line 87
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->i:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v0, v2}, Lflipboard/gui/MagazineThumbView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->j:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v0, v4}, Lflipboard/gui/MagazineThumbView;->setImage(Lflipboard/objs/Image;)V

    .line 90
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->j:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v0, v2}, Lflipboard/gui/MagazineThumbView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    invoke-static {}, Lflipboard/util/AndroidUtil;->a()Lflipboard/objs/SectionPageTemplate;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/SectionPageTemplate;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->n:Z

    .line 93
    iget-boolean v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->n:Z

    if-eqz v0, :cond_1

    .line 94
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->k:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v0, v4}, Lflipboard/gui/MagazineThumbView;->setImage(Lflipboard/objs/Image;)V

    .line 95
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->k:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v0, v2}, Lflipboard/gui/MagazineThumbView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->l:Landroid/widget/LinearLayout;

    new-instance v2, Lflipboard/gui/section/item/ProfileSummaryItem$2;

    invoke-direct {v2, p0}, Lflipboard/gui/section/item/ProfileSummaryItem$2;-><init>(Lflipboard/gui/section/item/ProfileSummaryItem;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->m:Landroid/graphics/Paint;

    .line 112
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->m:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileSummaryItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080052

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 113
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->m:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 114
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->m:Landroid/graphics/Paint;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileSummaryItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 115
    invoke-virtual {p0, v1}, Lflipboard/gui/section/item/ProfileSummaryItem;->setWillNotDraw(Z)V

    .line 116
    return-void

    :cond_0
    move v0, v1

    .line 92
    goto :goto_0

    .line 97
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->k:Lflipboard/gui/MagazineThumbView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lflipboard/gui/MagazineThumbView;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->b:Landroid/widget/LinearLayout;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    .line 230
    sub-int v2, p4, p2

    .line 231
    sub-int v3, p5, p3

    .line 232
    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileSummaryItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 234
    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileSummaryItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 235
    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileSummaryItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f090082

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 236
    iget-boolean v5, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->n:Z

    if-eqz v5, :cond_0

    .line 238
    :goto_0
    iget-object v1, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->b:Landroid/widget/LinearLayout;

    add-int/lit8 v5, v0, 0x0

    iget-object v6, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v6

    sub-int v6, v3, v6

    add-int/lit8 v0, v0, 0x0

    iget-object v7, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v0, v7

    invoke-virtual {v1, v5, v6, v0, v3}, Landroid/widget/LinearLayout;->layout(IIII)V

    .line 240
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v3, v0

    .line 241
    div-int/lit8 v1, v0, 0x2

    iget-object v3, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v1, v3

    .line 242
    iget-object v3, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->l:Landroid/widget/LinearLayout;

    add-int/lit8 v5, v4, 0x0

    iget-object v6, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v6

    sub-int v6, v1, v6

    add-int/lit8 v4, v4, 0x0

    iget-object v7, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v4, v7

    invoke-virtual {v3, v5, v6, v4, v1}, Landroid/widget/LinearLayout;->layout(IIII)V

    .line 244
    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v1

    iget-object v3, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 245
    mul-int/lit8 v1, v2, 0x3

    div-int/lit8 v1, v1, 0x4

    .line 246
    iget-object v2, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v1, v2

    .line 247
    iget-object v3, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->d:Lflipboard/gui/FLTextView;

    iget-object v4, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v0, v4

    iget-object v5, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v3, v2, v4, v5, v0}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 249
    iget-object v2, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v0, v2

    .line 250
    iget-object v2, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 251
    iget-object v2, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->c:Lflipboard/gui/FLTextView;

    iget-object v3, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v0, v3

    iget-object v4, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v1, v3, v4, v0}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 252
    return-void

    :cond_0
    move v0, v1

    .line 236
    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    .line 197
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 198
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 199
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 200
    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileSummaryItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 201
    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileSummaryItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 202
    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileSummaryItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f090082

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 203
    iget-boolean v5, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->n:Z

    if-eqz v5, :cond_0

    .line 204
    :goto_0
    mul-int/lit8 v1, v4, 0x2

    .line 205
    mul-int/lit8 v5, v0, 0x2

    sub-int v5, v2, v5

    .line 206
    iget-boolean v6, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->n:Z

    if-eqz v6, :cond_1

    .line 210
    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v2, v0

    mul-int/lit8 v6, v4, 0x3

    sub-int/2addr v0, v6

    div-int/lit8 v0, v0, 0x4

    .line 214
    :goto_1
    iget-object v6, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->b:Landroid/widget/LinearLayout;

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v6, v7, v0}, Landroid/widget/LinearLayout;->measure(II)V

    .line 216
    iget-object v0, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v3, v0

    sub-int/2addr v0, v4

    .line 217
    div-int/lit8 v4, v5, 0x2

    .line 218
    iget-object v5, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->d:Lflipboard/gui/FLTextView;

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 219
    iget-object v5, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->c:Lflipboard/gui/FLTextView;

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    iget-object v7, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v7

    sub-int/2addr v0, v7

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v5, v6, v0}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 221
    const/4 v0, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lflipboard/gui/section/item/ProfileSummaryItem;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v0, v5, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 222
    iget-object v5, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v5

    iget-object v6, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v5

    .line 223
    iget-object v5, p0, Lflipboard/gui/section/item/ProfileSummaryItem;->l:Landroid/widget/LinearLayout;

    sub-int v0, v4, v0

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/widget/LinearLayout;->measure(II)V

    .line 225
    invoke-virtual {p0, v2, v3}, Lflipboard/gui/section/item/ProfileSummaryItem;->setMeasuredDimension(II)V

    .line 226
    return-void

    :cond_0
    move v0, v1

    .line 203
    goto/16 :goto_0

    .line 212
    :cond_1
    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v2, v0

    mul-int/lit8 v6, v4, 0x2

    sub-int/2addr v0, v6

    div-int/lit8 v0, v0, 0x3

    goto :goto_1
.end method
