.class public Lflipboard/gui/section/item/GenericSuggestedFollowItem;
.super Landroid/widget/FrameLayout;
.source "GenericSuggestedFollowItem.java"

# interfaces
.implements Lflipboard/gui/item/PageboxView;
.implements Lflipboard/gui/section/scrolling/PhoneItemView;
.implements Lflipboard/service/Flap$JSONResultObserver;
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/FrameLayout;",
        "Lflipboard/gui/item/PageboxView;",
        "Lflipboard/gui/section/scrolling/PhoneItemView;",
        "Lflipboard/service/Flap$JSONResultObserver;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/User;",
        "Lflipboard/service/User$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/objs/FeedSectionLink;",
            ">;"
        }
    .end annotation
.end field

.field protected b:Lflipboard/gui/swipe/SwipeableLinearLayout;

.field protected c:Lflipboard/gui/FLTextView;

.field protected d:Lflipboard/gui/FLImageView;

.field protected e:Landroid/widget/LinearLayout;

.field private f:Landroid/graphics/Paint;

.field private g:I

.field private h:I

.field private i:Lflipboard/gui/swipe/SwipeDismissTouchListener$DismissCallback;

.field private j:Lflipboard/objs/FeedItem;

.field private k:Lflipboard/service/Section;

.field private l:I

.field private m:Lflipboard/objs/SidebarGroup$RenderHints;

.field private n:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a:Ljava/util/ArrayList;

    .line 68
    const/4 v0, 0x5

    iput v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->g:I

    .line 69
    iget v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->g:I

    iput v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->h:I

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a:Ljava/util/ArrayList;

    .line 68
    const/4 v0, 0x5

    iput v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->g:I

    .line 69
    iget v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->g:I

    iput v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->h:I

    .line 84
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/item/GenericSuggestedFollowItem;)I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->h:I

    return v0
.end method

.method private a()V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 155
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    if-eqz v0, :cond_4

    .line 156
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->d:Lflipboard/gui/FLImageView;

    iget-object v2, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 157
    invoke-virtual {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08009b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 161
    :goto_0
    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->setBackgroundColor(I)V

    .line 164
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->k:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    invoke-virtual {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f090122

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 166
    iget-object v2, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1, v0, v1, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 170
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 171
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->b:Lflipboard/gui/swipe/SwipeableLinearLayout;

    invoke-virtual {v0}, Lflipboard/gui/swipe/SwipeableLinearLayout;->removeAllViews()V

    .line 173
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 174
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    .line 175
    iget-object v0, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 176
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 178
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 179
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->c:Lflipboard/gui/FLTextView;

    iget-object v3, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    :goto_1
    invoke-virtual {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/app/Activity;)I

    move-result v0

    .line 186
    invoke-virtual {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09000b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    .line 187
    const-string v4, "topic"

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->q:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 189
    if-eqz v0, :cond_6

    .line 190
    invoke-virtual {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f090090

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 194
    :goto_2
    div-int v0, v3, v0

    iput v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->g:I

    .line 196
    iget v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->g:I

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-le v0, v3, :cond_1

    .line 197
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->g:I

    .line 200
    :cond_1
    iget v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->g:I

    iput v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->h:I

    .line 202
    :goto_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 203
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 204
    new-instance v3, Lflipboard/objs/FeedSectionLink;

    invoke-direct {v3, v0}, Lflipboard/objs/FeedSectionLink;-><init>(Lflipboard/objs/FeedItem;)V

    .line 205
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 207
    iget v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->g:I

    if-ge v1, v0, :cond_3

    .line 208
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a(ILandroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 209
    new-instance v3, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;

    invoke-direct {v3, p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem$1;-><init>(Lflipboard/gui/section/item/GenericSuggestedFollowItem;)V

    iput-object v3, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->i:Lflipboard/gui/swipe/SwipeDismissTouchListener$DismissCallback;

    .line 240
    iget-object v3, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->b:Lflipboard/gui/swipe/SwipeableLinearLayout;

    iget-object v4, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->i:Lflipboard/gui/swipe/SwipeDismissTouchListener$DismissCallback;

    if-eqz v4, :cond_2

    new-instance v5, Lflipboard/gui/swipe/SwipeDismissTouchListener;

    invoke-direct {v5, v0, v4}, Lflipboard/gui/swipe/SwipeDismissTouchListener;-><init>(Landroid/view/View;Lflipboard/gui/swipe/SwipeDismissTouchListener$DismissCallback;)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_2
    invoke-virtual {v3, v0}, Lflipboard/gui/swipe/SwipeableLinearLayout;->addView(Landroid/view/View;)V

    .line 202
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 159
    :cond_4
    invoke-virtual {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0800aa

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto/16 :goto_0

    .line 181
    :cond_5
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 192
    :cond_6
    invoke-virtual {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f090091

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_2

    .line 245
    :cond_7
    invoke-virtual {p0, v3}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->setVisibility(I)V

    .line 250
    :cond_8
    :goto_4
    return-void

    .line 248
    :cond_9
    invoke-virtual {p0, v3}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->setVisibility(I)V

    goto :goto_4
.end method

.method static synthetic a(Lflipboard/gui/section/item/GenericSuggestedFollowItem;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 52
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->F:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->a:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->k:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->s:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->m:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v2, v2, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->w:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->v:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->m:Lflipboard/objs/SidebarGroup$RenderHints;

    iget v2, v2, Lflipboard/objs/SidebarGroup$RenderHints;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->o:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v0, v1, p1}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    return-void
.end method

.method static synthetic b(Lflipboard/gui/section/item/GenericSuggestedFollowItem;)I
    .locals 2

    .prologue
    .line 52
    iget v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->h:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->h:I

    return v0
.end method

.method static synthetic c(Lflipboard/gui/section/item/GenericSuggestedFollowItem;)I
    .locals 2

    .prologue
    .line 52
    iget v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->l:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->l:I

    return v0
.end method

.method static synthetic d(Lflipboard/gui/section/item/GenericSuggestedFollowItem;)Lflipboard/gui/swipe/SwipeDismissTouchListener$DismissCallback;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->i:Lflipboard/gui/swipe/SwipeDismissTouchListener$DismissCallback;

    return-object v0
.end method


# virtual methods
.method public final a(ILandroid/view/View;)Landroid/view/View;
    .locals 7

    .prologue
    const v6, 0x7f0a0266

    const/16 v3, 0x10

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 255
    if-nez p2, :cond_1

    .line 257
    invoke-virtual {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030151

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 258
    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 259
    invoke-virtual {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02005a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setPlaceholder(Landroid/graphics/drawable/Drawable;)V

    .line 260
    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 261
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 263
    new-instance v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;

    invoke-direct {v1, v4}, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;-><init>(B)V

    .line 264
    const v0, 0x7f0a004f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->b:Lflipboard/gui/FLTextView;

    .line 265
    const v0, 0x7f0a0268

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->c:Lflipboard/gui/FLTextView;

    .line 266
    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->f:Lflipboard/gui/FLImageView;

    .line 267
    const v0, 0x7f0a038e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->d:Landroid/view/ViewGroup;

    .line 268
    const v0, 0x7f0a01a3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FollowButton;

    iput-object v0, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->e:Lflipboard/gui/FollowButton;

    .line 269
    iget-object v0, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->e:Lflipboard/gui/FollowButton;

    invoke-virtual {v0}, Lflipboard/gui/FollowButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 270
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 272
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 279
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 280
    iput-object v0, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->a:Lflipboard/objs/FeedSectionLink;

    invoke-virtual {v0}, Lflipboard/objs/FeedSectionLink;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->b:Lflipboard/gui/FLTextView;

    iget-object v3, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    invoke-static {v2}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v2, v4}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    iget-object v2, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->c:Lflipboard/gui/FLTextView;

    iget-object v3, v0, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->c:Lflipboard/gui/FLTextView;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lflipboard/gui/FLTextView;->setMaxLines(I)V

    iget-object v2, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->c:Lflipboard/gui/FLTextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    :goto_1
    iget-object v2, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v2, v4}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    iget-object v2, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->f:Lflipboard/gui/FLImageView;

    iget-object v3, v0, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    :cond_0
    :goto_2
    iget-object v2, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->e:Lflipboard/gui/FollowButton;

    invoke-virtual {v2, v0}, Lflipboard/gui/FollowButton;->setSectionLink(Lflipboard/objs/FeedSectionLink;)V

    iget-object v0, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->e:Lflipboard/gui/FollowButton;

    sget-object v2, Lflipboard/objs/UsageEventV2$FollowFrom;->a:Lflipboard/objs/UsageEventV2$FollowFrom;

    invoke-virtual {v0, v2}, Lflipboard/gui/FollowButton;->setFrom(Lflipboard/objs/UsageEventV2$FollowFrom;)V

    .line 283
    iget-object v0, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->e:Lflipboard/gui/FollowButton;

    new-instance v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$2;

    invoke-direct {v1, p0, p2}, Lflipboard/gui/section/item/GenericSuggestedFollowItem$2;-><init>(Lflipboard/gui/section/item/GenericSuggestedFollowItem;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FollowButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 303
    new-instance v0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$3;

    invoke-direct {v0, p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem$3;-><init>(Lflipboard/gui/section/item/GenericSuggestedFollowItem;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 318
    return-object p2

    .line 274
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;

    .line 275
    iget-object v1, v0, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->a()V

    move-object v1, v0

    goto :goto_0

    .line 280
    :cond_2
    iget-object v2, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v2, v5}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lflipboard/objs/FeedSectionLink;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->b:Lflipboard/gui/FLTextView;

    iget-object v3, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v2, v5}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    iget-object v2, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v2, v5}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    iget-object v2, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0800aa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iget-object v3, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v3, v2}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    iget-object v3, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->b:Lflipboard/gui/FLTextView;

    const/high16 v4, 0x41500000    # 13.0f

    invoke-virtual {v3, v4}, Lflipboard/gui/FLTextView;->setTextSize(F)V

    iget-object v3, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v3, v2}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    iget-object v2, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->b:Lflipboard/gui/FLTextView;

    const v3, 0x7f020232

    invoke-virtual {v2, v3}, Lflipboard/gui/FLTextView;->setBackgroundResource(I)V

    iget-object v2, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->e:Lflipboard/gui/FollowButton;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lflipboard/gui/FollowButton;->setInverted(Z)V

    goto/16 :goto_2

    :cond_4
    iget-object v2, v1, Lflipboard/gui/section/item/GenericSuggestedFollowItem$Holder;->b:Lflipboard/gui/FLTextView;

    const v3, 0x7f020230

    invoke-virtual {v2, v3}, Lflipboard/gui/FLTextView;->setBackgroundResource(I)V

    goto/16 :goto_2
.end method

.method public final a(Lflipboard/json/FLObject;)V
    .locals 0

    .prologue
    .line 371
    return-void
.end method

.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 3

    .prologue
    .line 137
    iput-object p2, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    .line 138
    iput-object p1, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->k:Lflipboard/service/Section;

    .line 140
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "enable_scrolling"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 141
    if-eqz v0, :cond_0

    .line 142
    invoke-direct {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a()V

    .line 144
    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 52
    check-cast p2, Lflipboard/service/User$Message;

    sget-object v0, Lflipboard/service/User$Message;->l:Lflipboard/service/User$Message;

    invoke-virtual {p2, v0}, Lflipboard/service/User$Message;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    check-cast p3, Lflipboard/service/Section;

    if-eqz p3, :cond_2

    move v1, v2

    move v3, v2

    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    iget-object v4, v0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    iget-object v5, p3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v5, v5, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p3}, Lflipboard/service/Section;->s()Z

    move-result v3

    iput-boolean v3, v0, Lflipboard/objs/FeedSectionLink;->o:Z

    const/4 v3, 0x1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    if-nez v3, :cond_2

    invoke-virtual {p3}, Lflipboard/service/Section;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lflipboard/objs/FeedSectionLink;

    const-string v1, "user"

    invoke-direct {v0, p3, v1}, Lflipboard/objs/FeedSectionLink;-><init>(Lflipboard/service/Section;Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->b:Lflipboard/gui/swipe/SwipeableLinearLayout;

    invoke-virtual {v0}, Lflipboard/gui/swipe/SwipeableLinearLayout;->invalidate()V

    :cond_2
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 376
    return-void
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 403
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 404
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p0}, Lflipboard/service/User;->b(Lflipboard/util/Observer;)V

    .line 405
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 409
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 410
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p0}, Lflipboard/service/User;->c(Lflipboard/util/Observer;)V

    .line 411
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    .line 99
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 100
    invoke-virtual {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->getHeight()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->b:Lflipboard/gui/swipe/SwipeableLinearLayout;

    invoke-virtual {v1}, Lflipboard/gui/swipe/SwipeableLinearLayout;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v0, v1

    .line 101
    invoke-virtual {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09007b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 102
    const/4 v0, 0x0

    move v6, v0

    move v0, v1

    :goto_0
    iget v1, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->g:I

    iget v2, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->l:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    if-ge v6, v1, :cond_0

    .line 103
    invoke-virtual {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->getWidth()I

    move-result v3

    .line 104
    iget-object v1, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->b:Lflipboard/gui/swipe/SwipeableLinearLayout;

    invoke-virtual {v1, v6}, Lflipboard/gui/swipe/SwipeableLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int v7, v0, v1

    .line 105
    int-to-float v1, v8

    invoke-virtual {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->getPaddingBottom()I

    move-result v0

    sub-int v0, v7, v0

    int-to-float v2, v0

    sub-int v0, v3, v8

    int-to-float v3, v0

    invoke-virtual {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->getPaddingBottom()I

    move-result v0

    sub-int v0, v7, v0

    int-to-float v4, v0

    iget-object v5, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->f:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 102
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v0, v7

    goto :goto_0

    .line 107
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 88
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 89
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 90
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->f:Landroid/graphics/Paint;

    .line 91
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->f:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080072

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 92
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 93
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->f:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 94
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->setWillNotDraw(Z)V

    .line 95
    return-void
.end method

.method public setListRowCount(I)V
    .locals 2

    .prologue
    .line 110
    iput p1, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->g:I

    .line 111
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->b:Lflipboard/gui/swipe/SwipeableLinearLayout;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->b:Lflipboard/gui/swipe/SwipeableLinearLayout;

    invoke-virtual {v0}, Lflipboard/gui/swipe/SwipeableLinearLayout;->getChildCount()I

    move-result v0

    .line 113
    if-le v0, p1, :cond_0

    .line 114
    iget-object v1, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->b:Lflipboard/gui/swipe/SwipeableLinearLayout;

    sub-int/2addr v0, p1

    invoke-virtual {v1, p1, v0}, Lflipboard/gui/swipe/SwipeableLinearLayout;->removeViews(II)V

    .line 115
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->b:Lflipboard/gui/swipe/SwipeableLinearLayout;

    invoke-virtual {v0}, Lflipboard/gui/swipe/SwipeableLinearLayout;->invalidate()V

    .line 118
    :cond_0
    return-void
.end method

.method public setMaxHeight(I)V
    .locals 0

    .prologue
    .line 416
    return-void
.end method

.method public setPagebox(Lflipboard/objs/SidebarGroup;)V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->j:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->n:Ljava/lang/String;

    .line 123
    new-instance v0, Lflipboard/objs/FeedItem;

    invoke-direct {v0}, Lflipboard/objs/FeedItem;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    .line 124
    invoke-virtual {p1}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->m:Lflipboard/objs/SidebarGroup$RenderHints;

    .line 125
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    iget-object v1, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->m:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v1, v1, Lflipboard/objs/SidebarGroup$RenderHints;->i:Lflipboard/objs/Image;

    iput-object v1, v0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    .line 126
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    iget-object v1, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->m:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v1, v1, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 127
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    iget-object v1, p1, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    .line 128
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    iget-object v1, p1, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedItem;->c:Ljava/lang/String;

    .line 129
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    iget-object v1, p1, Lflipboard/objs/SidebarGroup;->c:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    .line 130
    iget-object v0, p0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->j:Lflipboard/objs/FeedItem;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    .line 132
    invoke-direct {p0}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a()V

    .line 133
    return-void
.end method
