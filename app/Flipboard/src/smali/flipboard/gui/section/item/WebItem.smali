.class public Lflipboard/gui/section/item/WebItem;
.super Landroid/view/ViewGroup;
.source "WebItem.java"

# interfaces
.implements Lflipboard/gui/FLViewIntf;
.implements Lflipboard/gui/item/TabletItem;


# instance fields
.field private a:Lflipboard/gui/FLWebView;

.field private b:Lflipboard/objs/FeedItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/section/item/WebItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/item/WebItem;)Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lflipboard/gui/section/item/WebItem;->b:Lflipboard/objs/FeedItem;

    return-object v0
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 58
    iput-object p2, p0, Lflipboard/gui/section/item/WebItem;->b:Lflipboard/objs/FeedItem;

    .line 59
    return-void
.end method

.method public final a(ZI)V
    .locals 3

    .prologue
    .line 68
    invoke-static {p1, p2}, Lflipboard/util/AndroidUtil;->a(ZI)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lflipboard/gui/section/item/WebItem;->a:Lflipboard/gui/FLWebView;

    if-nez v0, :cond_0

    .line 70
    invoke-virtual {p0}, Lflipboard/gui/section/item/WebItem;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lflipboard/gui/section/item/WebItem$1;

    invoke-direct {v1, p0, v0}, Lflipboard/gui/section/item/WebItem$1;-><init>(Lflipboard/gui/section/item/WebItem;Landroid/content/Context;)V

    new-instance v2, Lflipboard/util/FLWebViewClient;

    invoke-direct {v2, v0}, Lflipboard/util/FLWebViewClient;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lflipboard/gui/FLWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iput-object v1, p0, Lflipboard/gui/section/item/WebItem;->a:Lflipboard/gui/FLWebView;

    .line 71
    iget-object v0, p0, Lflipboard/gui/section/item/WebItem;->a:Lflipboard/gui/FLWebView;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/WebItem;->addView(Landroid/view/View;)V

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/item/WebItem;->a:Lflipboard/gui/FLWebView;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lflipboard/gui/section/item/WebItem;->a:Lflipboard/gui/FLWebView;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/item/WebItem;->removeView(Landroid/view/View;)V

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/section/item/WebItem;->a:Lflipboard/gui/FLWebView;

    goto :goto_0
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lflipboard/gui/section/item/WebItem;->b:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 95
    invoke-virtual {p0}, Lflipboard/gui/section/item/WebItem;->getPaddingLeft()I

    move-result v0

    .line 96
    invoke-virtual {p0}, Lflipboard/gui/section/item/WebItem;->getPaddingTop()I

    move-result v1

    .line 97
    iget-object v2, p0, Lflipboard/gui/section/item/WebItem;->a:Lflipboard/gui/FLWebView;

    if-eqz v2, :cond_0

    .line 98
    iget-object v2, p0, Lflipboard/gui/section/item/WebItem;->a:Lflipboard/gui/FLWebView;

    iget-object v3, p0, Lflipboard/gui/section/item/WebItem;->a:Lflipboard/gui/FLWebView;

    invoke-virtual {v3}, Lflipboard/gui/FLWebView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lflipboard/gui/section/item/WebItem;->a:Lflipboard/gui/FLWebView;

    invoke-virtual {v4}, Lflipboard/gui/FLWebView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Lflipboard/gui/FLWebView;->layout(IIII)V

    .line 100
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 83
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 84
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 85
    iget-object v2, p0, Lflipboard/gui/section/item/WebItem;->a:Lflipboard/gui/FLWebView;

    if-eqz v2, :cond_0

    .line 86
    invoke-virtual {p0}, Lflipboard/gui/section/item/WebItem;->getPaddingLeft()I

    move-result v2

    sub-int v2, v0, v2

    invoke-virtual {p0}, Lflipboard/gui/section/item/WebItem;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 87
    invoke-virtual {p0}, Lflipboard/gui/section/item/WebItem;->getPaddingTop()I

    move-result v3

    sub-int v3, v1, v3

    invoke-virtual {p0}, Lflipboard/gui/section/item/WebItem;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 88
    iget-object v4, p0, Lflipboard/gui/section/item/WebItem;->a:Lflipboard/gui/FLWebView;

    invoke-virtual {v4, v2, v3}, Lflipboard/gui/FLWebView;->measure(II)V

    .line 90
    :cond_0
    invoke-virtual {p0, v0, v1}, Lflipboard/gui/section/item/WebItem;->setMeasuredDimension(II)V

    .line 91
    return-void
.end method
