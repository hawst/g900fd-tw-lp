.class public Lflipboard/gui/section/item/ImageItemPhone$$ViewInjector;
.super Ljava/lang/Object;
.source "ImageItemPhone$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/item/ImageItemPhone;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a01f9

    const-string v1, "field \'topGradient\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    iput-object v0, p1, Lflipboard/gui/section/item/ImageItemPhone;->f:Landroid/view/View;

    .line 12
    const v0, 0x7f0a01f8

    const-string v1, "field \'bottomGradient\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    iput-object v0, p1, Lflipboard/gui/section/item/ImageItemPhone;->g:Landroid/view/View;

    .line 14
    const v0, 0x7f0a0094

    const-string v1, "field \'itemActionBar\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/section/scrolling/ItemActionBar;

    iput-object v0, p1, Lflipboard/gui/section/item/ImageItemPhone;->h:Lflipboard/gui/section/scrolling/ItemActionBar;

    .line 16
    const v0, 0x7f0a01fb

    const-string v1, "field \'attributionSocial\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    check-cast v0, Lflipboard/gui/section/AttributionSocial;

    iput-object v0, p1, Lflipboard/gui/section/item/ImageItemPhone;->i:Lflipboard/gui/section/AttributionSocial;

    .line 18
    return-void
.end method

.method public static reset(Lflipboard/gui/section/item/ImageItemPhone;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->f:Landroid/view/View;

    .line 22
    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->g:Landroid/view/View;

    .line 23
    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->h:Lflipboard/gui/section/scrolling/ItemActionBar;

    .line 24
    iput-object v0, p0, Lflipboard/gui/section/item/ImageItemPhone;->i:Lflipboard/gui/section/AttributionSocial;

    .line 25
    return-void
.end method
