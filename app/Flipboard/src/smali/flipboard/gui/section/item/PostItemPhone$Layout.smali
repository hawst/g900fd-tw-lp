.class public final enum Lflipboard/gui/section/item/PostItemPhone$Layout;
.super Ljava/lang/Enum;
.source "PostItemPhone.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/section/item/PostItemPhone$Layout;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/section/item/PostItemPhone$Layout;

.field public static final enum b:Lflipboard/gui/section/item/PostItemPhone$Layout;

.field public static final enum c:Lflipboard/gui/section/item/PostItemPhone$Layout;

.field public static final enum d:Lflipboard/gui/section/item/PostItemPhone$Layout;

.field public static final enum e:Lflipboard/gui/section/item/PostItemPhone$Layout;

.field private static final synthetic f:[Lflipboard/gui/section/item/PostItemPhone$Layout;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 85
    new-instance v0, Lflipboard/gui/section/item/PostItemPhone$Layout;

    const-string v1, "NO_IMAGE"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/section/item/PostItemPhone$Layout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/section/item/PostItemPhone$Layout;->a:Lflipboard/gui/section/item/PostItemPhone$Layout;

    new-instance v0, Lflipboard/gui/section/item/PostItemPhone$Layout;

    const-string v1, "IMAGE_RIGHT"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/section/item/PostItemPhone$Layout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/section/item/PostItemPhone$Layout;->b:Lflipboard/gui/section/item/PostItemPhone$Layout;

    new-instance v0, Lflipboard/gui/section/item/PostItemPhone$Layout;

    const-string v1, "IMAGE_RIGHT_FULL_HEIGHT"

    invoke-direct {v0, v1, v4}, Lflipboard/gui/section/item/PostItemPhone$Layout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/section/item/PostItemPhone$Layout;->c:Lflipboard/gui/section/item/PostItemPhone$Layout;

    new-instance v0, Lflipboard/gui/section/item/PostItemPhone$Layout;

    const-string v1, "IMAGE_TOP"

    invoke-direct {v0, v1, v5}, Lflipboard/gui/section/item/PostItemPhone$Layout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/section/item/PostItemPhone$Layout;->d:Lflipboard/gui/section/item/PostItemPhone$Layout;

    new-instance v0, Lflipboard/gui/section/item/PostItemPhone$Layout;

    const-string v1, "FULL_BLEED"

    invoke-direct {v0, v1, v6}, Lflipboard/gui/section/item/PostItemPhone$Layout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/section/item/PostItemPhone$Layout;->e:Lflipboard/gui/section/item/PostItemPhone$Layout;

    .line 84
    const/4 v0, 0x5

    new-array v0, v0, [Lflipboard/gui/section/item/PostItemPhone$Layout;

    sget-object v1, Lflipboard/gui/section/item/PostItemPhone$Layout;->a:Lflipboard/gui/section/item/PostItemPhone$Layout;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/section/item/PostItemPhone$Layout;->b:Lflipboard/gui/section/item/PostItemPhone$Layout;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/gui/section/item/PostItemPhone$Layout;->c:Lflipboard/gui/section/item/PostItemPhone$Layout;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/gui/section/item/PostItemPhone$Layout;->d:Lflipboard/gui/section/item/PostItemPhone$Layout;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/gui/section/item/PostItemPhone$Layout;->e:Lflipboard/gui/section/item/PostItemPhone$Layout;

    aput-object v1, v0, v6

    sput-object v0, Lflipboard/gui/section/item/PostItemPhone$Layout;->f:[Lflipboard/gui/section/item/PostItemPhone$Layout;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/section/item/PostItemPhone$Layout;
    .locals 1

    .prologue
    .line 84
    const-class v0, Lflipboard/gui/section/item/PostItemPhone$Layout;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/item/PostItemPhone$Layout;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/section/item/PostItemPhone$Layout;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lflipboard/gui/section/item/PostItemPhone$Layout;->f:[Lflipboard/gui/section/item/PostItemPhone$Layout;

    invoke-virtual {v0}, [Lflipboard/gui/section/item/PostItemPhone$Layout;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/section/item/PostItemPhone$Layout;

    return-object v0
.end method
