.class public Lflipboard/gui/section/ListSingleThreadWrapper;
.super Ljava/lang/Object;
.source "ListSingleThreadWrapper.java"

# interfaces
.implements Ljava/util/List;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/List",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->b:Ljava/lang/ref/WeakReference;

    .line 23
    iput-object p1, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    .line 24
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 27
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    .line 31
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v1, v0, :cond_1

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Called "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from wrong thread. Current thread = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". Main looper thread = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 33
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v1, :cond_0

    .line 34
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 36
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unwanted.checkThread_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":"

    const-string v3, "_"

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    .line 37
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40
    :cond_1
    return-void
.end method


# virtual methods
.method public add(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .prologue
    .line 48
    const-string v0, "ListSingleThread:add int object"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 50
    return-void
.end method

.method public add(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 53
    const-string v0, "ListSingleThread:add object"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public addAll(ILjava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .prologue
    .line 58
    const-string v0, "ListSingleThread:addAll"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .prologue
    .line 63
    const-string v0, "ListSingleThread:addAll"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 68
    const-string v0, "ListSingleThread:clear"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 70
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 73
    const-string v0, "ListSingleThread:contains"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 78
    const-string v0, "ListSingleThread:containsAll"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 83
    const-string v0, "ListSingleThread:get"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 88
    const-string v0, "ListSingleThread:indexOf"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 93
    const-string v0, "ListSingleThread:isEmpty"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 43
    const-string v0, "ListSingleThread:iterator"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 98
    const-string v0, "ListSingleThread:lastIndexOf"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 103
    const-string v0, "ListSingleThread:ListIterator"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 108
    const-string v0, "ListSingleThread:ListIterator"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public remove(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 113
    const-string v0, "ListSingleThread:remove int"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 118
    const-string v0, "ListSingleThread:remove object"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 123
    const-string v0, "ListSingleThread:removeAll"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 128
    const-string v0, "ListSingleThread:retainAll"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)TE;"
        }
    .end annotation

    .prologue
    .line 133
    const-string v0, "ListSingleThread:set"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 138
    const-string v0, "ListSingleThread:size"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public subList(II)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 143
    const-string v0, "ListSingleThread:subList"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 144
    new-instance v0, Lflipboard/gui/section/ListSingleThreadWrapper;

    iget-object v1, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v1, p1, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lflipboard/gui/section/ListSingleThreadWrapper;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 148
    const-string v0, "ListSingleThread:toArray"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 153
    const-string v0, "ListSingleThread:toArray"

    invoke-direct {p0, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->a(Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lflipboard/gui/section/ListSingleThreadWrapper;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
