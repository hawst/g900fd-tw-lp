.class public Lflipboard/gui/section/AttributionSmall$$ViewInjector;
.super Ljava/lang/Object;
.source "AttributionSmall$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/AttributionSmall;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a007d

    const-string v1, "field \'titleTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/AttributionSmall;->a:Lflipboard/gui/FLTextView;

    .line 12
    const v0, 0x7f0a007e

    const-string v1, "field \'serviceIconImageView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p1, Lflipboard/gui/section/AttributionSmall;->b:Lflipboard/gui/FLImageView;

    .line 14
    const v0, 0x7f0a0006

    const-string v1, "field \'flipButton\' and method \'flipIntoMagazine\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 15
    check-cast v0, Lflipboard/gui/FLCameleonImageView;

    iput-object v0, p1, Lflipboard/gui/section/AttributionSmall;->c:Lflipboard/gui/FLCameleonImageView;

    .line 16
    new-instance v0, Lflipboard/gui/section/AttributionSmall$$ViewInjector$1;

    invoke-direct {v0, p1}, Lflipboard/gui/section/AttributionSmall$$ViewInjector$1;-><init>(Lflipboard/gui/section/AttributionSmall;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 24
    return-void
.end method

.method public static reset(Lflipboard/gui/section/AttributionSmall;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    iput-object v0, p0, Lflipboard/gui/section/AttributionSmall;->a:Lflipboard/gui/FLTextView;

    .line 28
    iput-object v0, p0, Lflipboard/gui/section/AttributionSmall;->b:Lflipboard/gui/FLImageView;

    .line 29
    iput-object v0, p0, Lflipboard/gui/section/AttributionSmall;->c:Lflipboard/gui/FLCameleonImageView;

    .line 30
    return-void
.end method
