.class public Lflipboard/gui/section/MagazineLikesDialog$Adapter;
.super Landroid/widget/BaseAdapter;
.source "MagazineLikesDialog.java"


# instance fields
.field final synthetic a:Lflipboard/gui/section/MagazineLikesDialog;


# direct methods
.method private constructor <init>(Lflipboard/gui/section/MagazineLikesDialog;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->a:Lflipboard/gui/section/MagazineLikesDialog;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflipboard/gui/section/MagazineLikesDialog;B)V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0, p1}, Lflipboard/gui/section/MagazineLikesDialog$Adapter;-><init>(Lflipboard/gui/section/MagazineLikesDialog;)V

    return-void
.end method

.method private a(I)Lflipboard/objs/CommentaryResult$Item$Commentary;
    .locals 2

    .prologue
    .line 214
    const/4 v0, 0x0

    .line 215
    iget-object v1, p0, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->a:Lflipboard/gui/section/MagazineLikesDialog;

    iget-object v1, v1, Lflipboard/gui/section/MagazineLikesDialog;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 216
    iget-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->a:Lflipboard/gui/section/MagazineLikesDialog;

    iget-object v0, v0, Lflipboard/gui/section/MagazineLikesDialog;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    .line 218
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 201
    const/4 v0, 0x0

    .line 202
    iget-object v1, p0, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->a:Lflipboard/gui/section/MagazineLikesDialog;

    iget-object v1, v1, Lflipboard/gui/section/MagazineLikesDialog;->l:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 203
    iget-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->a:Lflipboard/gui/section/MagazineLikesDialog;

    iget-object v0, v0, Lflipboard/gui/section/MagazineLikesDialog;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 205
    :cond_0
    iget-object v1, p0, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->a:Lflipboard/gui/section/MagazineLikesDialog;

    invoke-static {v1}, Lflipboard/gui/section/MagazineLikesDialog;->f(Lflipboard/gui/section/MagazineLikesDialog;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 207
    add-int/lit8 v0, v0, 0x1

    .line 209
    :cond_1
    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 197
    invoke-direct {p0, p1}, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->a(I)Lflipboard/objs/CommentaryResult$Item$Commentary;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 223
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->a:Lflipboard/gui/section/MagazineLikesDialog;

    iget-object v0, v0, Lflipboard/gui/section/MagazineLikesDialog;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 237
    const/4 v0, 0x0

    .line 241
    :goto_0
    return v0

    .line 239
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 256
    invoke-direct {p0, p1}, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->a(I)Lflipboard/objs/CommentaryResult$Item$Commentary;

    move-result-object v3

    .line 259
    if-nez p2, :cond_3

    .line 260
    new-instance v2, Lflipboard/gui/section/MagazineLikesDialog$Holder;

    iget-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->a:Lflipboard/gui/section/MagazineLikesDialog;

    invoke-direct {v2, v0, v5}, Lflipboard/gui/section/MagazineLikesDialog$Holder;-><init>(Lflipboard/gui/section/MagazineLikesDialog;B)V

    .line 261
    invoke-virtual {p0, p1}, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_2

    .line 263
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03004d

    const/4 v4, 0x0

    invoke-static {v0, v1, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 264
    const v0, 0x7f0a00c0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, v2, Lflipboard/gui/section/MagazineLikesDialog$Holder;->a:Lflipboard/gui/FLTextIntf;

    .line 265
    const v0, 0x7f0a00c3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, v2, Lflipboard/gui/section/MagazineLikesDialog$Holder;->b:Lflipboard/gui/FLTextIntf;

    .line 266
    const v0, 0x7f0a00be

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, v2, Lflipboard/gui/section/MagazineLikesDialog$Holder;->c:Lflipboard/gui/FLImageView;

    .line 267
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0900b7

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 268
    iget-object v4, v2, Lflipboard/gui/section/MagazineLikesDialog$Holder;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 269
    iput v0, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 270
    iput v0, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 271
    iget-object v0, v2, Lflipboard/gui/section/MagazineLikesDialog$Holder;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 272
    iget-object v0, v2, Lflipboard/gui/section/MagazineLikesDialog$Holder;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02005a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflipboard/gui/FLImageView;->setPlaceholder(Landroid/graphics/drawable/Drawable;)V

    .line 273
    const v0, 0x7f0a012a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    move-object v0, v1

    .line 274
    check-cast v0, Landroid/view/ViewGroup;

    .line 275
    const v4, 0x7f0a0127

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 277
    new-instance v0, Lflipboard/gui/section/MagazineLikesDialog$Adapter$1;

    invoke-direct {v0, p0, v3}, Lflipboard/gui/section/MagazineLikesDialog$Adapter$1;-><init>(Lflipboard/gui/section/MagazineLikesDialog$Adapter;Lflipboard/objs/CommentaryResult$Item$Commentary;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 308
    :goto_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v2

    .line 313
    :goto_1
    if-eqz v3, :cond_1

    .line 314
    iget-object v2, v0, Lflipboard/gui/section/MagazineLikesDialog$Holder;->a:Lflipboard/gui/FLTextIntf;

    iget-object v4, v3, Lflipboard/objs/CommentaryResult$Item$Commentary;->c:Ljava/lang/String;

    invoke-interface {v2, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 315
    iget-object v2, v0, Lflipboard/gui/section/MagazineLikesDialog$Holder;->b:Lflipboard/gui/FLTextIntf;

    if-eqz v2, :cond_0

    .line 316
    iget-object v2, v0, Lflipboard/gui/section/MagazineLikesDialog$Holder;->b:Lflipboard/gui/FLTextIntf;

    iget-object v4, v3, Lflipboard/objs/CommentaryResult$Item$Commentary;->b:Ljava/lang/String;

    invoke-interface {v2, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 318
    :cond_0
    iget-object v2, v0, Lflipboard/gui/section/MagazineLikesDialog$Holder;->c:Lflipboard/gui/FLImageView;

    if-eqz v2, :cond_1

    .line 319
    iget-object v2, v0, Lflipboard/gui/section/MagazineLikesDialog$Holder;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->a()V

    .line 320
    iget-object v0, v0, Lflipboard/gui/section/MagazineLikesDialog$Holder;->c:Lflipboard/gui/FLImageView;

    iget-object v2, v3, Lflipboard/objs/CommentaryResult$Item$Commentary;->g:Lflipboard/objs/Image;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 324
    :cond_1
    return-object v1

    .line 306
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->a:Lflipboard/gui/section/MagazineLikesDialog;

    invoke-static {v0}, Lflipboard/gui/section/MagazineLikesDialog;->g(Lflipboard/gui/section/MagazineLikesDialog;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 310
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/MagazineLikesDialog$Holder;

    move-object v1, p2

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    .line 247
    const/4 v0, 0x0

    .line 248
    iget-object v1, p0, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->a:Lflipboard/gui/section/MagazineLikesDialog;

    iget-object v1, v1, Lflipboard/gui/section/MagazineLikesDialog;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 249
    const/4 v0, 0x1

    .line 251
    :cond_0
    return v0
.end method
