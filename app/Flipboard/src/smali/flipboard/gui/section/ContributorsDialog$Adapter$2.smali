.class Lflipboard/gui/section/ContributorsDialog$Adapter$2;
.super Ljava/lang/Object;
.source "ContributorsDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/section/ContributorsDialog;

.field final synthetic b:Lflipboard/gui/section/ContributorsDialog$Adapter;


# direct methods
.method constructor <init>(Lflipboard/gui/section/ContributorsDialog$Adapter;Lflipboard/gui/section/ContributorsDialog;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$2;->b:Lflipboard/gui/section/ContributorsDialog$Adapter;

    iput-object p2, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$2;->a:Lflipboard/gui/section/ContributorsDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 181
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/ContributorsDialog$Holder;

    iget-object v1, v0, Lflipboard/gui/section/ContributorsDialog$Holder;->e:Lflipboard/objs/CommentaryResult$Item$Commentary;

    .line 183
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 184
    const-string v0, "source"

    const-string v3, "contributor_list"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string v0, "originSectionIdentifier"

    iget-object v3, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$2;->b:Lflipboard/gui/section/ContributorsDialog$Adapter;

    iget-object v3, v3, Lflipboard/gui/section/ContributorsDialog$Adapter;->b:Lflipboard/gui/section/ContributorsDialog;

    invoke-static {v3}, Lflipboard/gui/section/ContributorsDialog;->a(Lflipboard/gui/section/ContributorsDialog;)Lflipboard/service/Section;

    move-result-object v3

    iget-object v3, v3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const/4 v0, 0x0

    .line 190
    iget-object v3, v1, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    const-string v4, "owner"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$2;->b:Lflipboard/gui/section/ContributorsDialog$Adapter;

    iget-object v3, v3, Lflipboard/gui/section/ContributorsDialog$Adapter;->b:Lflipboard/gui/section/ContributorsDialog;

    invoke-static {v3}, Lflipboard/gui/section/ContributorsDialog;->a(Lflipboard/gui/section/ContributorsDialog;)Lflipboard/service/Section;

    move-result-object v3

    iget-object v3, v3, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$2;->b:Lflipboard/gui/section/ContributorsDialog$Adapter;

    iget-object v3, v3, Lflipboard/gui/section/ContributorsDialog$Adapter;->b:Lflipboard/gui/section/ContributorsDialog;

    invoke-static {v3}, Lflipboard/gui/section/ContributorsDialog;->a(Lflipboard/gui/section/ContributorsDialog;)Lflipboard/service/Section;

    move-result-object v3

    iget-object v3, v3, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v3, v3, Lflipboard/service/Section$Meta;->i:Lflipboard/objs/FeedSectionLink;

    if-eqz v3, :cond_1

    .line 191
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$2;->b:Lflipboard/gui/section/ContributorsDialog$Adapter;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter;->b:Lflipboard/gui/section/ContributorsDialog;

    invoke-static {v0}, Lflipboard/gui/section/ContributorsDialog;->a(Lflipboard/gui/section/ContributorsDialog;)Lflipboard/service/Section;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->i:Lflipboard/objs/FeedSectionLink;

    .line 195
    :cond_0
    :goto_0
    new-instance v1, Lflipboard/service/Section;

    invoke-direct {v1, v0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$2;->b:Lflipboard/gui/section/ContributorsDialog$Adapter;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter;->b:Lflipboard/gui/section/ContributorsDialog;

    invoke-virtual {v0}, Lflipboard/gui/section/ContributorsDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v1, v0, v2}, Lflipboard/util/SocialHelper;->a(Lflipboard/service/Section;Landroid/content/Context;Landroid/os/Bundle;)V

    .line 196
    return-void

    .line 192
    :cond_1
    iget-object v3, v1, Lflipboard/objs/CommentaryResult$Item$Commentary;->o:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lflipboard/objs/CommentaryResult$Item$Commentary;->o:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 193
    iget-object v0, v1, Lflipboard/objs/CommentaryResult$Item$Commentary;->o:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    goto :goto_0
.end method
