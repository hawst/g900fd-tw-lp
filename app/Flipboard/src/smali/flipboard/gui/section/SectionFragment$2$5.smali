.class Lflipboard/gui/section/SectionFragment$2$5;
.super Ljava/lang/Object;
.source "SectionFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/Object;

.field final synthetic b:Lflipboard/gui/section/SectionFragment$2;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment$2;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 531
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$2$5;->b:Lflipboard/gui/section/SectionFragment$2;

    iput-object p2, p0, Lflipboard/gui/section/SectionFragment$2$5;->a:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 534
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2$5;->a:Ljava/lang/Object;

    check-cast v0, Lflipboard/objs/Invite;

    .line 535
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$2$5;->b:Lflipboard/gui/section/SectionFragment$2;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v1}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lflipboard/activities/FlipboardActivity;

    .line 536
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v2, :cond_0

    .line 537
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$2$5;->b:Lflipboard/gui/section/SectionFragment$2;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 538
    invoke-virtual {v2}, Lflipboard/service/User;->c()Z

    move-result v3

    if-nez v3, :cond_1

    .line 540
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "show_firstlaunch_smartlink_message"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 541
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lflipboard/activities/FirstRunActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 542
    const/high16 v2, 0x20000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 543
    const-string v2, "extra_show_invite_dialog"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 544
    invoke-virtual {v1, v0}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    .line 545
    invoke-virtual {v1}, Lflipboard/activities/FlipboardActivity;->finish()V

    .line 575
    :cond_0
    :goto_0
    return-void

    .line 546
    :cond_1
    invoke-virtual {v2}, Lflipboard/service/User;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 548
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 549
    const v3, 0x7f0d000e

    invoke-virtual {v2, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 550
    const v3, 0x7f0d000d

    invoke-virtual {v2, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 551
    const v3, 0x7f0d023f

    invoke-virtual {v2, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 552
    const v3, 0x7f0d004a

    invoke-virtual {v2, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 553
    new-instance v3, Lflipboard/gui/section/SectionFragment$2$5$1;

    invoke-direct {v3, p0, v1, v0}, Lflipboard/gui/section/SectionFragment$2$5$1;-><init>(Lflipboard/gui/section/SectionFragment$2$5;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/Invite;)V

    iput-object v3, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 564
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2$5;->b:Lflipboard/gui/section/SectionFragment$2;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    new-instance v3, Lflipboard/gui/section/SectionFragment$2$5$2;

    invoke-direct {v3, p0, v2, v1}, Lflipboard/gui/section/SectionFragment$2$5$2;-><init>(Lflipboard/gui/section/SectionFragment$2$5;Lflipboard/gui/dialog/FLAlertDialogFragment;Lflipboard/activities/FlipboardActivity;)V

    invoke-virtual {v0, v3}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 572
    :cond_2
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$2$5;->b:Lflipboard/gui/section/SectionFragment$2;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v1, v0}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/objs/Invite;)V

    goto :goto_0
.end method
