.class Lflipboard/gui/section/SuggestedUsersFragment$1;
.super Lflipboard/gui/personal/TabStripStatic$ImageFragmentPagerAdapter;
.source "SuggestedUsersFragment.java"


# instance fields
.field final synthetic b:Lflipboard/gui/section/SuggestedUsersFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SuggestedUsersFragment;Landroid/support/v4/app/FragmentManager;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lflipboard/gui/section/SuggestedUsersFragment$1;->b:Lflipboard/gui/section/SuggestedUsersFragment;

    invoke-direct {p0, p2}, Lflipboard/gui/personal/TabStripStatic$ImageFragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    return-void
.end method


# virtual methods
.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 5

    .prologue
    .line 62
    iget-object v0, p0, Lflipboard/gui/section/SuggestedUsersFragment$1;->b:Lflipboard/gui/section/SuggestedUsersFragment;

    invoke-static {v0}, Lflipboard/gui/section/SuggestedUsersFragment;->a(Lflipboard/gui/section/SuggestedUsersFragment;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 64
    if-nez v0, :cond_0

    .line 66
    new-instance v1, Lflipboard/gui/section/UserListFragment;

    invoke-direct {v1}, Lflipboard/gui/section/UserListFragment;-><init>()V

    .line 67
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 69
    const-string v0, "uid"

    iget-object v3, p0, Lflipboard/gui/section/SuggestedUsersFragment$1;->b:Lflipboard/gui/section/SuggestedUsersFragment;

    invoke-virtual {v3}, Lflipboard/gui/section/SuggestedUsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "uid"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lflipboard/gui/section/SuggestedUsersFragment$1;->b:Lflipboard/gui/section/SuggestedUsersFragment;

    invoke-static {v0}, Lflipboard/gui/section/SuggestedUsersFragment;->b(Lflipboard/gui/section/SuggestedUsersFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 72
    sget-object v3, Lflipboard/service/Flap$FollowListType;->d:Lflipboard/service/Flap$FollowListType;

    invoke-virtual {v3}, Lflipboard/service/Flap$FollowListType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 74
    const-string v0, "listType"

    sget-object v3, Lflipboard/service/Flap$FollowListType;->d:Lflipboard/service/Flap$FollowListType;

    invoke-virtual {v3}, Lflipboard/service/Flap$FollowListType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :goto_0
    invoke-virtual {v1, v2}, Lflipboard/gui/section/UserListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 86
    iget-object v0, p0, Lflipboard/gui/section/SuggestedUsersFragment$1;->b:Lflipboard/gui/section/SuggestedUsersFragment;

    invoke-static {v0}, Lflipboard/gui/section/SuggestedUsersFragment;->a(Lflipboard/gui/section/SuggestedUsersFragment;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move-object v0, v1

    .line 89
    :cond_0
    return-object v0

    .line 77
    :cond_1
    const-string v3, "listType"

    sget-object v4, Lflipboard/service/Flap$FollowListType;->c:Lflipboard/service/Flap$FollowListType;

    invoke-virtual {v4}, Lflipboard/service/Flap$FollowListType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v3, "serviceId"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lflipboard/gui/section/SuggestedUsersFragment$1;->b:Lflipboard/gui/section/SuggestedUsersFragment;

    invoke-static {v0}, Lflipboard/gui/section/SuggestedUsersFragment;->b(Lflipboard/gui/section/SuggestedUsersFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 3

    .prologue
    .line 104
    const/4 v1, 0x0

    .line 105
    iget-object v0, p0, Lflipboard/gui/section/SuggestedUsersFragment$1;->b:Lflipboard/gui/section/SuggestedUsersFragment;

    invoke-static {v0}, Lflipboard/gui/section/SuggestedUsersFragment;->b(Lflipboard/gui/section/SuggestedUsersFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 106
    const-string v2, "flipboard"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 107
    const v0, 0x7f0200ef

    .line 117
    :goto_0
    return v0

    .line 108
    :cond_0
    const-string v2, "facebook"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 109
    const v0, 0x7f0200ee

    goto :goto_0

    .line 110
    :cond_1
    const-string v2, "twitter"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 111
    const v0, 0x7f0200f1

    goto :goto_0

    .line 112
    :cond_2
    sget-object v2, Lflipboard/service/Flap$FollowListType;->d:Lflipboard/service/Flap$FollowListType;

    invoke-virtual {v2}, Lflipboard/service/Flap$FollowListType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 113
    const v0, 0x7f0200ed

    goto :goto_0

    .line 114
    :cond_3
    const-string v2, "googleplus"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 115
    const v0, 0x7f0200f0

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method
