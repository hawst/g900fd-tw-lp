.class public Lflipboard/gui/section/GroupFranchiseMeta;
.super Ljava/lang/Object;
.source "GroupFranchiseMeta.java"


# instance fields
.field final a:Lflipboard/objs/FeedItem;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:I

.field public f:I

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lflipboard/objs/FeedItem;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput v0, p0, Lflipboard/gui/section/GroupFranchiseMeta;->e:I

    .line 24
    iput v0, p0, Lflipboard/gui/section/GroupFranchiseMeta;->f:I

    .line 37
    iput-object p1, p0, Lflipboard/gui/section/GroupFranchiseMeta;->a:Lflipboard/objs/FeedItem;

    .line 38
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lflipboard/gui/section/GroupFranchiseMeta;
    .locals 3

    .prologue
    .line 59
    const/4 v0, 0x0

    .line 60
    const-string v1, "franchiseGroupItemSectionId"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 61
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2, v1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v1

    .line 62
    if-eqz v1, :cond_0

    .line 63
    const-string v2, "franchiseGroupItemId"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 64
    invoke-virtual {v1, v2}, Lflipboard/service/Section;->a(Ljava/lang/String;)Lflipboard/objs/FeedItem;

    move-result-object v1

    .line 65
    if-eqz v1, :cond_0

    .line 66
    new-instance v0, Lflipboard/gui/section/GroupFranchiseMeta;

    invoke-direct {v0, v1}, Lflipboard/gui/section/GroupFranchiseMeta;-><init>(Lflipboard/objs/FeedItem;)V

    .line 67
    const-string v1, "pageInFranchise"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lflipboard/gui/section/GroupFranchiseMeta;->e:I

    .line 68
    const-string v1, "totalPagesInFranchise"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lflipboard/gui/section/GroupFranchiseMeta;->f:I

    .line 69
    const-string v1, "remoteid"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/gui/section/GroupFranchiseMeta;->c:Ljava/lang/String;

    .line 70
    const-string v1, "title"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/gui/section/GroupFranchiseMeta;->b:Ljava/lang/String;

    .line 71
    const-string v1, "footerTitle"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/gui/section/GroupFranchiseMeta;->g:Ljava/lang/String;

    .line 74
    :cond_0
    return-object v0
.end method
