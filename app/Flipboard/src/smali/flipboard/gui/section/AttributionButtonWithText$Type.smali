.class public final enum Lflipboard/gui/section/AttributionButtonWithText$Type;
.super Ljava/lang/Enum;
.source "AttributionButtonWithText.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/section/AttributionButtonWithText$Type;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/section/AttributionButtonWithText$Type;

.field public static final enum b:Lflipboard/gui/section/AttributionButtonWithText$Type;

.field public static final enum c:Lflipboard/gui/section/AttributionButtonWithText$Type;

.field public static final enum d:Lflipboard/gui/section/AttributionButtonWithText$Type;

.field private static final synthetic e:[Lflipboard/gui/section/AttributionButtonWithText$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lflipboard/gui/section/AttributionButtonWithText$Type;

    const-string v1, "comment"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/section/AttributionButtonWithText$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/section/AttributionButtonWithText$Type;->a:Lflipboard/gui/section/AttributionButtonWithText$Type;

    new-instance v0, Lflipboard/gui/section/AttributionButtonWithText$Type;

    const-string v1, "like"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/section/AttributionButtonWithText$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/section/AttributionButtonWithText$Type;->b:Lflipboard/gui/section/AttributionButtonWithText$Type;

    new-instance v0, Lflipboard/gui/section/AttributionButtonWithText$Type;

    const-string v1, "share"

    invoke-direct {v0, v1, v4}, Lflipboard/gui/section/AttributionButtonWithText$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/section/AttributionButtonWithText$Type;->c:Lflipboard/gui/section/AttributionButtonWithText$Type;

    new-instance v0, Lflipboard/gui/section/AttributionButtonWithText$Type;

    const-string v1, "flip"

    invoke-direct {v0, v1, v5}, Lflipboard/gui/section/AttributionButtonWithText$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/section/AttributionButtonWithText$Type;->d:Lflipboard/gui/section/AttributionButtonWithText$Type;

    const/4 v0, 0x4

    new-array v0, v0, [Lflipboard/gui/section/AttributionButtonWithText$Type;

    sget-object v1, Lflipboard/gui/section/AttributionButtonWithText$Type;->a:Lflipboard/gui/section/AttributionButtonWithText$Type;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/section/AttributionButtonWithText$Type;->b:Lflipboard/gui/section/AttributionButtonWithText$Type;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/gui/section/AttributionButtonWithText$Type;->c:Lflipboard/gui/section/AttributionButtonWithText$Type;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/gui/section/AttributionButtonWithText$Type;->d:Lflipboard/gui/section/AttributionButtonWithText$Type;

    aput-object v1, v0, v5

    sput-object v0, Lflipboard/gui/section/AttributionButtonWithText$Type;->e:[Lflipboard/gui/section/AttributionButtonWithText$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/section/AttributionButtonWithText$Type;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lflipboard/gui/section/AttributionButtonWithText$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText$Type;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/section/AttributionButtonWithText$Type;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lflipboard/gui/section/AttributionButtonWithText$Type;->e:[Lflipboard/gui/section/AttributionButtonWithText$Type;

    invoke-virtual {v0}, [Lflipboard/gui/section/AttributionButtonWithText$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/section/AttributionButtonWithText$Type;

    return-object v0
.end method
