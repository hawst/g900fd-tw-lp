.class Lflipboard/gui/section/MagazineConversationThreadsAdapter$ConversationThreadClickListener;
.super Ljava/lang/Object;
.source "MagazineConversationThreadsAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/section/MagazineConversationThreadsAdapter;


# direct methods
.method private constructor <init>(Lflipboard/gui/section/MagazineConversationThreadsAdapter;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter$ConversationThreadClickListener;->a:Lflipboard/gui/section/MagazineConversationThreadsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflipboard/gui/section/MagazineConversationThreadsAdapter;B)V
    .locals 0

    .prologue
    .line 250
    invoke-direct {p0, p1}, Lflipboard/gui/section/MagazineConversationThreadsAdapter$ConversationThreadClickListener;-><init>(Lflipboard/gui/section/MagazineConversationThreadsAdapter;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 254
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 255
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;

    iget v0, v0, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;->f:I

    .line 256
    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter$ConversationThreadClickListener;->a:Lflipboard/gui/section/MagazineConversationThreadsAdapter;

    invoke-static {v1}, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->b(Lflipboard/gui/section/MagazineConversationThreadsAdapter;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    .line 257
    new-instance v1, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-direct {v1}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;-><init>()V

    .line 258
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 259
    const-string v3, "feedItemId"

    iget-object v4, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->h:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const-string v3, "magazine_id"

    iget-object v4, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter$ConversationThreadClickListener;->a:Lflipboard/gui/section/MagazineConversationThreadsAdapter;

    invoke-static {v4}, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->c(Lflipboard/gui/section/MagazineConversationThreadsAdapter;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const-string v3, "magazine_conversation_thread_description"

    iget-object v4, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->j:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const-string v3, "magazine_conversation_thread_creator_username"

    iget-object v4, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const-string v3, "magazine_conversation_thread_creation_time"

    iget-wide v4, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->k:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const-string v3, "magazine_conversation_thread_creator_image_url"

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->g:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    invoke-virtual {v1, v2}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->setArguments(Landroid/os/Bundle;)V

    .line 266
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter$ConversationThreadClickListener;->a:Lflipboard/gui/section/MagazineConversationThreadsAdapter;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->a(Lflipboard/gui/section/MagazineConversationThreadsAdapter;)Lflipboard/activities/GenericFragmentActivity;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 267
    const v2, 0x7f0a004e

    const-string v3, "magazine_conversation_thread_detail"

    invoke-virtual {v0, v2, v1, v3}, Landroid/support/v4/app/FragmentTransaction;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 268
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->a(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 269
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 271
    :cond_0
    return-void
.end method
