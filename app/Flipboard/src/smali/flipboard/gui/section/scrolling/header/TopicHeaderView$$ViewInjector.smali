.class public Lflipboard/gui/section/scrolling/header/TopicHeaderView$$ViewInjector;
.super Ljava/lang/Object;
.source "TopicHeaderView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/scrolling/header/TopicHeaderView;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a004c

    const-string v1, "field \'actionBar\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    .line 12
    const v0, 0x7f0a0089

    const-string v1, "field \'topicTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->b:Lflipboard/gui/FLTextView;

    .line 14
    const v0, 0x7f0a02e6

    const-string v1, "field \'followerCountTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c:Lflipboard/gui/FLLabelTextView;

    .line 16
    const v0, 0x7f0a02e7

    const-string v1, "field \'bottomLineView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->d:Landroid/view/View;

    .line 18
    return-void
.end method

.method public static reset(Lflipboard/gui/section/scrolling/header/TopicHeaderView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    .line 22
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->b:Lflipboard/gui/FLTextView;

    .line 23
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c:Lflipboard/gui/FLLabelTextView;

    .line 24
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->d:Landroid/view/View;

    .line 25
    return-void
.end method
