.class public Lflipboard/gui/section/scrolling/SectionLinkItemView$$ViewInjector;
.super Ljava/lang/Object;
.source "SectionLinkItemView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/scrolling/SectionLinkItemView;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a02e8

    const-string v1, "field \'image\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/SectionLinkItemView;->a:Lflipboard/gui/FLImageView;

    .line 12
    const v0, 0x7f0a02ea

    const-string v1, "field \'name\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/SectionLinkItemView;->b:Lflipboard/gui/FLTextView;

    .line 14
    const v0, 0x7f0a02eb

    const-string v1, "field \'author\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/SectionLinkItemView;->c:Lflipboard/gui/FLTextView;

    .line 16
    const v0, 0x7f0a02ec

    const-string v1, "field \'description\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/SectionLinkItemView;->d:Lflipboard/gui/FLTextView;

    .line 18
    const v0, 0x7f0a01a3

    const-string v1, "field \'followButton\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    check-cast v0, Lflipboard/gui/FollowButton;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/SectionLinkItemView;->e:Lflipboard/gui/FollowButton;

    .line 20
    const v0, 0x7f0a0094

    const-string v1, "field \'itemActionBar\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    check-cast v0, Lflipboard/gui/section/scrolling/ItemActionBar;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/SectionLinkItemView;->f:Lflipboard/gui/section/scrolling/ItemActionBar;

    .line 22
    const v0, 0x7f0a008c

    const-string v1, "field \'attributionService\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    check-cast v0, Lflipboard/gui/section/AttributionServiceInfo;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/SectionLinkItemView;->g:Lflipboard/gui/section/AttributionServiceInfo;

    .line 24
    const v0, 0x7f0a02e9

    const-string v1, "field \'contentContainer\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 25
    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/SectionLinkItemView;->h:Landroid/widget/FrameLayout;

    .line 26
    return-void
.end method

.method public static reset(Lflipboard/gui/section/scrolling/SectionLinkItemView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    iput-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->a:Lflipboard/gui/FLImageView;

    .line 30
    iput-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->b:Lflipboard/gui/FLTextView;

    .line 31
    iput-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->c:Lflipboard/gui/FLTextView;

    .line 32
    iput-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->d:Lflipboard/gui/FLTextView;

    .line 33
    iput-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->e:Lflipboard/gui/FollowButton;

    .line 34
    iput-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->f:Lflipboard/gui/section/scrolling/ItemActionBar;

    .line 35
    iput-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->g:Lflipboard/gui/section/AttributionServiceInfo;

    .line 36
    iput-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->h:Landroid/widget/FrameLayout;

    .line 37
    return-void
.end method
