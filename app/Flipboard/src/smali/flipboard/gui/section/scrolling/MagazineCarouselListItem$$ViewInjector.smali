.class public Lflipboard/gui/section/scrolling/MagazineCarouselListItem$$ViewInjector;
.super Ljava/lang/Object;
.source "MagazineCarouselListItem$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/scrolling/MagazineCarouselListItem;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a025e

    const-string v1, "field \'imageView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->a:Lflipboard/gui/FLImageView;

    .line 12
    const v0, 0x7f0a0143

    const-string v1, "field \'titleView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->b:Lflipboard/gui/FLStaticTextView;

    .line 14
    const v0, 0x7f0a025f

    const-string v1, "field \'subtitleView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->c:Lflipboard/gui/FLStaticTextView;

    .line 16
    const v0, 0x7f0a0260

    const-string v1, "field \'followView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->d:Lflipboard/gui/FLLabelTextView;

    .line 18
    return-void
.end method

.method public static reset(Lflipboard/gui/section/scrolling/MagazineCarouselListItem;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    iput-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->a:Lflipboard/gui/FLImageView;

    .line 22
    iput-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->b:Lflipboard/gui/FLStaticTextView;

    .line 23
    iput-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->c:Lflipboard/gui/FLStaticTextView;

    .line 24
    iput-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->d:Lflipboard/gui/FLLabelTextView;

    .line 25
    return-void
.end method
