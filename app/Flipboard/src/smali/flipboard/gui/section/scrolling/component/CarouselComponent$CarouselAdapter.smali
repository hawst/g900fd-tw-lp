.class public Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;
.super Lflipboard/gui/CarouselView$CarouselAdapter;
.source "CarouselComponent.java"


# instance fields
.field final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field final synthetic c:Lflipboard/gui/section/scrolling/component/CarouselComponent;

.field private d:Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lflipboard/gui/section/scrolling/component/CarouselComponent;Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 85
    iput-object p1, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;->c:Lflipboard/gui/section/scrolling/component/CarouselComponent;

    invoke-direct {p0}, Lflipboard/gui/CarouselView$CarouselAdapter;-><init>()V

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;->b:Ljava/util/List;

    .line 86
    iput-object p2, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;->d:Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter;

    .line 87
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;->d:Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter;

    invoke-virtual {p0, p1}, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2, p3}, Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter;->a(Ljava/lang/Object;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;->d:Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter;

    invoke-virtual {p0, p1}, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;->a(I)Ljava/lang/Object;

    invoke-interface {v0, p1}, Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter;->a(I)I

    move-result v0

    return v0
.end method
