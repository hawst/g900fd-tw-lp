.class public Lflipboard/gui/section/scrolling/header/ProfileCover$MagazineCarouselTileHolder$$ViewInjector;
.super Ljava/lang/Object;
.source "ProfileCover$MagazineCarouselTileHolder$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/scrolling/header/ProfileCover$MagazineCarouselTileHolder;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a004f

    const-string v1, "field \'titleTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileCover$MagazineCarouselTileHolder;->a:Landroid/widget/TextView;

    .line 12
    const v0, 0x7f0a0261

    const-string v1, "field \'descriptionTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileCover$MagazineCarouselTileHolder;->b:Landroid/widget/TextView;

    .line 14
    return-void
.end method

.method public static reset(Lflipboard/gui/section/scrolling/header/ProfileCover$MagazineCarouselTileHolder;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover$MagazineCarouselTileHolder;->a:Landroid/widget/TextView;

    .line 18
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover$MagazineCarouselTileHolder;->b:Landroid/widget/TextView;

    .line 19
    return-void
.end method
