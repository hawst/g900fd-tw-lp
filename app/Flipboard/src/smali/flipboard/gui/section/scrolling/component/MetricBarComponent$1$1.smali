.class Lflipboard/gui/section/scrolling/component/MetricBarComponent$1$1;
.super Ljava/lang/Object;
.source "MetricBarComponent.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lflipboard/gui/section/scrolling/component/MetricBarComponent$1;


# direct methods
.method constructor <init>(Lflipboard/gui/section/scrolling/component/MetricBarComponent$1;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lflipboard/gui/section/scrolling/component/MetricBarComponent$1$1;->b:Lflipboard/gui/section/scrolling/component/MetricBarComponent$1;

    iput-object p2, p0, Lflipboard/gui/section/scrolling/component/MetricBarComponent$1$1;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 38
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MetricBarComponent$1$1;->b:Lflipboard/gui/section/scrolling/component/MetricBarComponent$1;

    iget-object v0, v0, Lflipboard/gui/section/scrolling/component/MetricBarComponent$1;->a:Lflipboard/gui/section/scrolling/component/MetricBarComponent;

    invoke-static {v0}, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->a(Lflipboard/gui/section/scrolling/component/MetricBarComponent;)Lflipboard/gui/MetricBar;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/MetricBar;->a()V

    .line 39
    const/4 v0, 0x3

    iget-object v1, p0, Lflipboard/gui/section/scrolling/component/MetricBarComponent$1$1;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 40
    const/4 v0, 0x0

    .line 42
    iget-object v1, p0, Lflipboard/gui/section/scrolling/component/MetricBarComponent$1$1;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;

    .line 43
    iget-object v4, p0, Lflipboard/gui/section/scrolling/component/MetricBarComponent$1$1;->b:Lflipboard/gui/section/scrolling/component/MetricBarComponent$1;

    iget-object v4, v4, Lflipboard/gui/section/scrolling/component/MetricBarComponent$1;->a:Lflipboard/gui/section/scrolling/component/MetricBarComponent;

    invoke-static {v4}, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->b(Lflipboard/gui/section/scrolling/component/MetricBarComponent;)Lflipboard/gui/section/scrolling/component/MetricBarComponent$ArticleCountMetricListener;

    move-result-object v4

    if-eqz v4, :cond_0

    const-string v4, "articles"

    iget-object v5, v0, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 45
    iget-object v4, p0, Lflipboard/gui/section/scrolling/component/MetricBarComponent$1$1;->b:Lflipboard/gui/section/scrolling/component/MetricBarComponent$1;

    iget-object v4, v4, Lflipboard/gui/section/scrolling/component/MetricBarComponent$1;->a:Lflipboard/gui/section/scrolling/component/MetricBarComponent;

    invoke-static {v4}, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->b(Lflipboard/gui/section/scrolling/component/MetricBarComponent;)Lflipboard/gui/section/scrolling/component/MetricBarComponent$ArticleCountMetricListener;

    move-result-object v4

    iget-object v5, v0, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;->c:Ljava/lang/String;

    iget-object v5, v0, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;->a:Ljava/lang/String;

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;->b:Ljava/lang/String;

    invoke-interface {v4, v5, v0}, Lflipboard/gui/section/scrolling/component/MetricBarComponent$ArticleCountMetricListener;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 46
    :cond_0
    if-ge v1, v2, :cond_2

    .line 47
    iget-object v4, p0, Lflipboard/gui/section/scrolling/component/MetricBarComponent$1$1;->b:Lflipboard/gui/section/scrolling/component/MetricBarComponent$1;

    iget-object v4, v4, Lflipboard/gui/section/scrolling/component/MetricBarComponent$1;->a:Lflipboard/gui/section/scrolling/component/MetricBarComponent;

    iget-object v5, v0, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;->c:Ljava/lang/String;

    iget-object v6, v0, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;->a:Ljava/lang/String;

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;->b:Ljava/lang/String;

    invoke-static {v4, v5, v6, v0}, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->a(Lflipboard/gui/section/scrolling/component/MetricBarComponent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method
