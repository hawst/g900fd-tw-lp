.class public Lflipboard/gui/section/scrolling/BannerView;
.super Landroid/widget/LinearLayout;
.source "BannerView.java"

# interfaces
.implements Lflipboard/gui/section/scrolling/PhoneItemView;


# instance fields
.field a:Lflipboard/gui/FLStaticTextView;

.field b:Lflipboard/gui/FLTextView;

.field c:Lflipboard/gui/FLImageView;

.field d:Lflipboard/gui/section/scrolling/ItemProfileBar;

.field e:Lflipboard/gui/section/scrolling/ItemActionBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 63
    iget-object v0, p0, Lflipboard/gui/section/scrolling/BannerView;->a:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v0, p0, Lflipboard/gui/section/scrolling/BannerView;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImageBestFit(Lflipboard/objs/Image;)V

    .line 65
    invoke-static {p2, v2}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/FeedItem;Z)Landroid/text/SpannableString;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_0

    .line 67
    iget-object v1, p0, Lflipboard/gui/section/scrolling/BannerView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lflipboard/gui/section/scrolling/BannerView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 72
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/BannerView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/scrolling/ItemProfileBar;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 73
    iget-object v0, p0, Lflipboard/gui/section/scrolling/BannerView;->e:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/scrolling/ItemActionBar;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 74
    return-void

    .line 70
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/BannerView;->b:Lflipboard/gui/FLTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 56
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 57
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 58
    iget-object v0, p0, Lflipboard/gui/section/scrolling/BannerView;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/BannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090020

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setMaxHeight(I)V

    .line 59
    return-void
.end method

.method public setMaxHeight(I)V
    .locals 0

    .prologue
    .line 79
    return-void
.end method
