.class public Lflipboard/gui/section/scrolling/header/ProfileHeaderView$$ViewInjector;
.super Ljava/lang/Object;
.source "ProfileHeaderView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/scrolling/header/ProfileHeaderView;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a004c

    const-string v1, "field \'actionBar\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    .line 12
    const v0, 0x7f0a0266

    const-string v1, "field \'avatarImageView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    .line 14
    const v0, 0x7f0a02e3

    const-string v1, "field \'titleTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    .line 16
    const v0, 0x7f0a02e4

    const-string v1, "field \'subtitleTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->d:Lflipboard/gui/FLTextView;

    .line 18
    const v0, 0x7f0a02e5

    const-string v1, "field \'metricBar\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    check-cast v0, Lflipboard/gui/MetricBar;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->e:Lflipboard/gui/MetricBar;

    .line 20
    return-void
.end method

.method public static reset(Lflipboard/gui/section/scrolling/header/ProfileHeaderView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    .line 24
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    .line 25
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    .line 26
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->d:Lflipboard/gui/FLTextView;

    .line 27
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->e:Lflipboard/gui/MetricBar;

    .line 28
    return-void
.end method
