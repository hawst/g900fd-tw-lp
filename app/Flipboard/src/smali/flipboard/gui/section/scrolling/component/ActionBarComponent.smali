.class public Lflipboard/gui/section/scrolling/component/ActionBarComponent;
.super Ljava/lang/Object;
.source "ActionBarComponent.java"

# interfaces
.implements Lflipboard/gui/FLSliderHider$SliderView;


# instance fields
.field public final a:Lflipboard/gui/FollowButton;

.field public b:Z

.field private final c:Lflipboard/gui/actionbar/FLActionBar;

.field private final d:Lflipboard/gui/FLSliderHider;


# direct methods
.method public constructor <init>(Landroid/view/View;Lflipboard/gui/actionbar/FLActionBar;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lflipboard/gui/FLSliderHider;

    invoke-direct {v0}, Lflipboard/gui/FLSliderHider;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->d:Lflipboard/gui/FLSliderHider;

    .line 21
    iput-boolean v5, p0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->b:Z

    .line 24
    iput-object p2, p0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->c:Lflipboard/gui/actionbar/FLActionBar;

    .line 27
    invoke-virtual {p2}, Lflipboard/gui/actionbar/FLActionBar;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 28
    new-instance v1, Lflipboard/gui/FollowButton;

    invoke-direct {v1, v0}, Lflipboard/gui/FollowButton;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->a:Lflipboard/gui/FollowButton;

    .line 29
    iget-object v1, p0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->a:Lflipboard/gui/FollowButton;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x2

    const/4 v4, -0x1

    invoke-direct {v2, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Lflipboard/gui/FollowButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 30
    new-instance v1, Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-direct {v1, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;-><init>(Landroid/content/Context;)V

    .line 31
    const-string v0, "follow_button"

    invoke-virtual {v1, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(Ljava/lang/CharSequence;)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 32
    iget-object v2, p0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->a:Lflipboard/gui/FollowButton;

    iput-object v2, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->m:Landroid/view/View;

    .line 33
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 34
    invoke-virtual {p2, v1}, Lflipboard/gui/actionbar/FLActionBar;->setMenu(Lflipboard/gui/actionbar/FLActionBarMenu;)V

    .line 35
    const/4 v0, 0x0

    invoke-virtual {p2, v5, v0}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    .line 36
    invoke-virtual {p2}, Lflipboard/gui/actionbar/FLActionBar;->d()V

    .line 39
    new-instance v0, Lflipboard/gui/section/scrolling/component/ActionBarComponent$1;

    invoke-direct {v0, p0, p2, p1}, Lflipboard/gui/section/scrolling/component/ActionBarComponent$1;-><init>(Lflipboard/gui/section/scrolling/component/ActionBarComponent;Lflipboard/gui/actionbar/FLActionBar;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Lflipboard/gui/actionbar/FLActionBar;->post(Ljava/lang/Runnable;)Z

    .line 47
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/scrolling/component/ActionBarComponent;)Lflipboard/gui/FLSliderHider;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->d:Lflipboard/gui/FLSliderHider;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 67
    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->b:Z

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->d:Lflipboard/gui/FLSliderHider;

    int-to-float v1, p1

    iput v1, v0, Lflipboard/gui/FLSliderHider;->a:F

    .line 70
    :cond_0
    return-void
.end method

.method public final a(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)V
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->b:Z

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->d:Lflipboard/gui/FLSliderHider;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLSliderHider;->a(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)V

    .line 77
    :cond_0
    return-void
.end method

.method public final a(Lflipboard/objs/FeedSectionLink;)V
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->b:Z

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->a:Lflipboard/gui/FollowButton;

    invoke-virtual {v0, p1}, Lflipboard/gui/FollowButton;->setSectionLink(Lflipboard/objs/FeedSectionLink;)V

    .line 58
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 50
    iput-boolean p1, p0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->b:Z

    .line 51
    iget-object v1, p0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->c:Lflipboard/gui/actionbar/FLActionBar;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lflipboard/gui/actionbar/FLActionBar;->setVisibility(I)V

    .line 52
    return-void

    .line 51
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
