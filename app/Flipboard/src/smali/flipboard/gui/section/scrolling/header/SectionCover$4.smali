.class Lflipboard/gui/section/scrolling/header/SectionCover$4;
.super Ljava/lang/Object;
.source "SectionCover.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/service/Section;

.field final synthetic b:Lflipboard/gui/section/scrolling/header/SectionCover;


# direct methods
.method constructor <init>(Lflipboard/gui/section/scrolling/header/SectionCover;Lflipboard/service/Section;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lflipboard/gui/section/scrolling/header/SectionCover$4;->b:Lflipboard/gui/section/scrolling/header/SectionCover;

    iput-object p2, p0, Lflipboard/gui/section/scrolling/header/SectionCover$4;->a:Lflipboard/service/Section;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 177
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover$4;->b:Lflipboard/gui/section/scrolling/header/SectionCover;

    invoke-static {v0}, Lflipboard/gui/section/scrolling/header/SectionCover;->b(Lflipboard/gui/section/scrolling/header/SectionCover;)I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 178
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover$4;->b:Lflipboard/gui/section/scrolling/header/SectionCover;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/SectionCover$4;->a:Lflipboard/service/Section;

    iget-object v2, v1, Lflipboard/service/Section;->G:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lflipboard/service/Section;->G:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lflipboard/gui/section/ContributorsDialog;

    invoke-direct {v2}, Lflipboard/gui/section/ContributorsDialog;-><init>()V

    iput-object v1, v2, Lflipboard/gui/section/ContributorsDialog;->j:Lflipboard/service/Section;

    iput-object v0, v2, Lflipboard/gui/section/ContributorsDialog;->k:Lflipboard/gui/section/ContributorsDialog$ContributorChangedListener;

    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/header/SectionCover;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    const-string v1, "edit_contributors"

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/section/ContributorsDialog;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover$4;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover$4;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->i:Lflipboard/objs/FeedSectionLink;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover$4;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->i:Lflipboard/objs/FeedSectionLink;

    .line 182
    new-instance v1, Lflipboard/service/Section;

    invoke-direct {v1, v0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    .line 183
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover$4;->b:Lflipboard/gui/section/scrolling/header/SectionCover;

    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/header/SectionCover;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    const-string v2, "mag_cover"

    invoke-static {v0, v1, v2}, Lflipboard/util/ActivityUtil;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Ljava/lang/String;)V

    goto :goto_0
.end method
