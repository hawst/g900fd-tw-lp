.class public Lflipboard/gui/section/scrolling/component/MagazineGridComponent;
.super Ljava/lang/Object;
.source "MagazineGridComponent.java"


# instance fields
.field public final a:Lflipboard/gui/FLDynamicGridView;

.field public b:Z

.field private final c:Lflipboard/activities/FlipboardActivity;


# direct methods
.method public constructor <init>(Lflipboard/activities/FlipboardActivity;Lflipboard/gui/FLDynamicGridView;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->b:Z

    .line 35
    iput-object p1, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->c:Lflipboard/activities/FlipboardActivity;

    .line 36
    iput-object p2, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->a:Lflipboard/gui/FLDynamicGridView;

    .line 37
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->a:Lflipboard/gui/FLDynamicGridView;

    new-instance v1, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;

    invoke-direct {v1, p0}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;-><init>(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLDynamicGridView;->setViewAdapter(Lflipboard/gui/FLDynamicGridView$ViewAdapter;)V

    .line 38
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)Lflipboard/activities/FlipboardActivity;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->c:Lflipboard/activities/FlipboardActivity;

    return-object v0
.end method

.method static synthetic a(Lflipboard/objs/Magazine;Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 25
    iget-boolean v0, p0, Lflipboard/objs/Magazine;->n:Z

    if-eqz v0, :cond_1

    iget-object v0, p1, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->a:Lflipboard/gui/FLImageView;

    iget v1, p0, Lflipboard/objs/Magazine;->o:I

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBackgroundResource(I)V

    :goto_0
    iget-object v0, p1, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    const-string v2, "private"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p1, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->c:Landroid/widget/ImageView;

    const v1, 0x7f020180

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p1, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p1, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->d:Landroid/widget/TextView;

    const v1, 0x7f0d0204

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p1, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p1, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->a:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/objs/Magazine;->t:Lflipboard/objs/Image;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lflipboard/objs/Magazine;->i:Lflipboard/objs/Author;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/objs/Magazine;->i:Lflipboard/objs/Author;

    iget-object v1, v1, Lflipboard/objs/Author;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->c:Landroid/widget/ImageView;

    const v1, 0x7f0200cc

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p1, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p1, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->d:Landroid/widget/TextView;

    const v1, 0x7f0d0202

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p1, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic b(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)Landroid/widget/AbsListView$LayoutParams;
    .locals 3

    .prologue
    .line 25
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->a:Lflipboard/gui/FLDynamicGridView;

    invoke-virtual {v0}, Lflipboard/gui/FLDynamicGridView;->getColumnWidthSafe()I

    move-result v0

    int-to-float v1, v0

    const v2, 0x3f4ccccd    # 0.8f

    div-float/2addr v1, v2

    float-to-int v1, v1

    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v2, v0, v1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    return-object v2
.end method

.method static synthetic c(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->b:Z

    return v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Magazine;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 41
    iput-boolean v5, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->b:Z

    .line 43
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 46
    new-instance v1, Lflipboard/objs/Magazine;

    iget-object v2, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->c:Lflipboard/activities/FlipboardActivity;

    const v3, 0x7f0d0246

    invoke-virtual {v2, v3}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "public"

    const v4, 0x7f0200d6

    invoke-direct {v1, v2, v3, v4, v6}, Lflipboard/objs/Magazine;-><init>(Ljava/lang/String;Ljava/lang/String;ILflipboard/objs/Link;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    new-instance v1, Lflipboard/objs/Magazine;

    iget-object v2, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->c:Lflipboard/activities/FlipboardActivity;

    const v3, 0x7f0d0261

    invoke-virtual {v2, v3}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "private"

    const v4, 0x7f0200d7

    invoke-direct {v1, v2, v3, v4, v6}, Lflipboard/objs/Magazine;-><init>(Ljava/lang/String;Ljava/lang/String;ILflipboard/objs/Link;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    :goto_0
    new-instance v1, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$CreateNewMagazineTile;

    invoke-direct {v1, p0, v5}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$CreateNewMagazineTile;-><init>(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;B)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    iget-object v1, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->a:Lflipboard/gui/FLDynamicGridView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLDynamicGridView;->setItems(Ljava/util/List;)V

    .line 54
    return-void

    .line 49
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 50
    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
