.class public Lflipboard/gui/section/scrolling/StatusView;
.super Landroid/widget/LinearLayout;
.source "StatusView.java"

# interfaces
.implements Lflipboard/gui/section/scrolling/PhoneItemView;


# instance fields
.field a:Lflipboard/gui/section/scrolling/ItemProfileBar;

.field b:Lflipboard/gui/section/scrolling/ItemActionBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lflipboard/gui/section/scrolling/StatusView;->a:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/scrolling/ItemProfileBar;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 45
    iget-object v0, p0, Lflipboard/gui/section/scrolling/StatusView;->a:Lflipboard/gui/section/scrolling/ItemProfileBar;

    iget-object v0, v0, Lflipboard/gui/section/scrolling/ItemProfileBar;->h:Lflipboard/gui/FLTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setMaxLines(I)V

    .line 46
    iget-object v0, p0, Lflipboard/gui/section/scrolling/StatusView;->b:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/scrolling/ItemActionBar;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 47
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 38
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 39
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 40
    return-void
.end method

.method public setMaxHeight(I)V
    .locals 0

    .prologue
    .line 52
    return-void
.end method
