.class final Lflipboard/gui/section/scrolling/ItemActionBar$$ViewInjector$4;
.super Ljava/lang/Object;
.source "ItemActionBar$$ViewInjector.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/section/scrolling/ItemActionBar;


# direct methods
.method constructor <init>(Lflipboard/gui/section/scrolling/ItemActionBar;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lflipboard/gui/section/scrolling/ItemActionBar$$ViewInjector$4;->a:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 47
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar$$ViewInjector$4;->a:Lflipboard/gui/section/scrolling/ItemActionBar;

    sget-object v1, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v2, "topicButtonTapped"

    invoke-virtual {v1, v2}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    new-instance v1, Lflipboard/service/Section;

    iget-object v2, v0, Lflipboard/gui/section/scrolling/ItemActionBar;->h:Lflipboard/objs/FeedSectionLink;

    invoke-direct {v1, v2}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "source"

    sget-object v4, Lflipboard/objs/UsageEventV2$SectionNavFrom;->l:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    invoke-virtual {v4}, Lflipboard/objs/UsageEventV2$SectionNavFrom;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "originSectionIdentifier"

    iget-object v4, v0, Lflipboard/gui/section/scrolling/ItemActionBar;->a:Lflipboard/service/Section;

    iget-object v4, v4, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v4, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v3

    if-nez v3, :cond_0

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v3, v1}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    :cond_0
    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/ItemActionBar;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v1, v0, v2}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    .line 48
    return-void
.end method
