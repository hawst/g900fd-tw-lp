.class public Lflipboard/gui/section/scrolling/FullBleedView;
.super Landroid/widget/FrameLayout;
.source "FullBleedView.java"

# interfaces
.implements Lflipboard/gui/section/scrolling/PhoneItemView;


# instance fields
.field a:Lflipboard/gui/FLStaticTextView;

.field b:Lflipboard/gui/FLTextView;

.field c:Lflipboard/gui/FLImageView;

.field d:Lflipboard/gui/section/scrolling/ItemProfileBar;

.field e:Lflipboard/gui/section/scrolling/ItemActionBar;

.field f:Landroid/view/View;

.field g:Landroid/view/View;

.field public h:I

.field public i:I

.field public j:Lflipboard/objs/Image;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 73
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 85
    iget-object v0, p2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    .line 97
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->a:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->j:Lflipboard/objs/Image;

    .line 99
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->c:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/FullBleedView;->j:Lflipboard/objs/Image;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImageBestFit(Lflipboard/objs/Image;)V

    .line 100
    invoke-static {p2, v4}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/FeedItem;Z)Landroid/text/SpannableString;

    move-result-object v0

    .line 101
    if-eqz v0, :cond_2

    .line 102
    iget-object v1, p0, Lflipboard/gui/section/scrolling/FullBleedView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 108
    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-virtual {v0, v4}, Lflipboard/gui/section/scrolling/ItemProfileBar;->setInverted(Z)V

    .line 109
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/scrolling/ItemProfileBar;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 110
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_3

    .line 111
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 116
    :goto_2
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->e:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v0, v4}, Lflipboard/gui/section/scrolling/ItemActionBar;->setInverted(Z)V

    .line 117
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->e:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/scrolling/ItemActionBar;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 118
    return-void

    .line 88
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    .line 89
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->R()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->a:Lflipboard/gui/FLStaticTextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setMaxLines(I)V

    .line 94
    :goto_3
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->a:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 92
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->a:Lflipboard/gui/FLStaticTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setMaxLines(I)V

    goto :goto_3

    .line 105
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    goto :goto_1

    .line 113
    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 77
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 78
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 79
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/FullBleedView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->h:I

    .line 80
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 161
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/FullBleedView;->getPaddingLeft()I

    move-result v0

    .line 162
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/FullBleedView;->getPaddingTop()I

    move-result v1

    .line 163
    iget-object v2, p0, Lflipboard/gui/section/scrolling/FullBleedView;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v0

    .line 164
    iget-object v3, p0, Lflipboard/gui/section/scrolling/FullBleedView;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v3}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v1

    .line 166
    iget-object v4, p0, Lflipboard/gui/section/scrolling/FullBleedView;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v4, v0, v1, v2, v3}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 167
    iget-object v4, p0, Lflipboard/gui/section/scrolling/FullBleedView;->f:Landroid/view/View;

    iget-object v5, p0, Lflipboard/gui/section/scrolling/FullBleedView;->f:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v3, v5

    invoke-virtual {v4, v0, v5, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 168
    iget-object v4, p0, Lflipboard/gui/section/scrolling/FullBleedView;->g:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 169
    iget-object v4, p0, Lflipboard/gui/section/scrolling/FullBleedView;->g:Landroid/view/View;

    iget-object v5, p0, Lflipboard/gui/section/scrolling/FullBleedView;->g:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    invoke-virtual {v4, v0, v1, v2, v5}, Landroid/view/View;->layout(IIII)V

    .line 172
    :cond_0
    iget v2, p0, Lflipboard/gui/section/scrolling/FullBleedView;->h:I

    add-int/2addr v1, v2

    .line 173
    iget v2, p0, Lflipboard/gui/section/scrolling/FullBleedView;->h:I

    sub-int v2, v3, v2

    .line 175
    iget-object v3, p0, Lflipboard/gui/section/scrolling/FullBleedView;->e:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v3}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v2, v3

    .line 176
    iget-object v4, p0, Lflipboard/gui/section/scrolling/FullBleedView;->e:Lflipboard/gui/section/scrolling/ItemActionBar;

    iget-object v5, p0, Lflipboard/gui/section/scrolling/FullBleedView;->e:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v5}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredWidth()I

    move-result v5

    invoke-virtual {v4, v0, v3, v5, v2}, Lflipboard/gui/section/scrolling/ItemActionBar;->layout(IIII)V

    .line 177
    iget v2, p0, Lflipboard/gui/section/scrolling/FullBleedView;->h:I

    add-int/2addr v0, v2

    .line 179
    iget-object v2, p0, Lflipboard/gui/section/scrolling/FullBleedView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    iget-object v4, p0, Lflipboard/gui/section/scrolling/FullBleedView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-virtual {v4}, Lflipboard/gui/section/scrolling/ItemProfileBar;->getMeasuredWidth()I

    move-result v4

    iget-object v5, p0, Lflipboard/gui/section/scrolling/FullBleedView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-virtual {v5}, Lflipboard/gui/section/scrolling/ItemProfileBar;->getMeasuredHeight()I

    move-result v5

    iget v6, p0, Lflipboard/gui/section/scrolling/FullBleedView;->h:I

    add-int/2addr v5, v6

    invoke-virtual {v2, v0, v1, v4, v5}, Lflipboard/gui/section/scrolling/ItemProfileBar;->layout(IIII)V

    .line 182
    iget-object v1, p0, Lflipboard/gui/section/scrolling/FullBleedView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v3, v1

    .line 184
    iget-object v2, p0, Lflipboard/gui/section/scrolling/FullBleedView;->b:Lflipboard/gui/FLTextView;

    iget-object v4, p0, Lflipboard/gui/section/scrolling/FullBleedView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2, v0, v1, v4, v3}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 187
    iget-object v2, p0, Lflipboard/gui/section/scrolling/FullBleedView;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v1, v2

    .line 189
    iget-object v3, p0, Lflipboard/gui/section/scrolling/FullBleedView;->a:Lflipboard/gui/FLStaticTextView;

    iget-object v4, p0, Lflipboard/gui/section/scrolling/FullBleedView;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v3, v0, v2, v4, v1}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 190
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10

    .prologue
    const/high16 v9, -0x80000000

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 122
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 123
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    .line 124
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/FullBleedView;->getPaddingLeft()I

    move-result v0

    sub-int v0, v2, v0

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/FullBleedView;->getPaddingRight()I

    move-result v3

    sub-int v3, v0, v3

    .line 125
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/FullBleedView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/FullBleedView;->getPaddingBottom()I

    move-result v4

    add-int/2addr v4, v0

    .line 127
    iget v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->i:I

    if-nez v0, :cond_0

    .line 128
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/FullBleedView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/app/Activity;)I

    move-result v0

    sub-int/2addr v0, v4

    iput v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->i:I

    .line 131
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->j:Lflipboard/objs/Image;

    invoke-static {v3, v0}, Lflipboard/gui/section/ItemDisplayUtil;->a(ILflipboard/objs/Image;)I

    move-result v0

    .line 132
    int-to-float v4, v0

    iget v5, p0, Lflipboard/gui/section/scrolling/FullBleedView;->i:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    .line 133
    iget v5, p0, Lflipboard/gui/section/scrolling/FullBleedView;->i:I

    if-gt v0, v5, :cond_2

    float-to-double v4, v4

    const-wide v6, 0x3fe6666666666666L    # 0.7

    cmpl-double v0, v4, v6

    if-ltz v0, :cond_2

    const/4 v0, 0x1

    .line 135
    :goto_0
    if-eqz v0, :cond_3

    .line 136
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->c:Lflipboard/gui/FLImageView;

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget v5, p0, Lflipboard/gui/section/scrolling/FullBleedView;->i:I

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 142
    :goto_1
    iget v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->h:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v3, v0

    .line 144
    iget-object v3, p0, Lflipboard/gui/section/scrolling/FullBleedView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lflipboard/gui/section/scrolling/ItemProfileBar;->measure(II)V

    .line 145
    iget-object v3, p0, Lflipboard/gui/section/scrolling/FullBleedView;->g:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_1

    .line 146
    iget-object v3, p0, Lflipboard/gui/section/scrolling/FullBleedView;->g:Landroid/view/View;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget-object v5, p0, Lflipboard/gui/section/scrolling/FullBleedView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-virtual {v5}, Lflipboard/gui/section/scrolling/ItemProfileBar;->getMeasuredHeight()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    .line 150
    :cond_1
    iget-object v3, p0, Lflipboard/gui/section/scrolling/FullBleedView;->a:Lflipboard/gui/FLStaticTextView;

    mul-int/lit8 v4, v0, 0x3

    div-int/lit8 v4, v4, 0x5

    invoke-static {v4, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 151
    iget-object v3, p0, Lflipboard/gui/section/scrolling/FullBleedView;->b:Lflipboard/gui/FLTextView;

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 153
    iget-object v3, p0, Lflipboard/gui/section/scrolling/FullBleedView;->e:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v3, v4, v1}, Lflipboard/gui/section/scrolling/ItemActionBar;->measure(II)V

    .line 154
    iget-object v1, p0, Lflipboard/gui/section/scrolling/FullBleedView;->f:Landroid/view/View;

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget-object v3, p0, Lflipboard/gui/section/scrolling/FullBleedView;->e:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v3}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredHeight()I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/section/scrolling/FullBleedView;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v4

    iget-object v5, p0, Lflipboard/gui/section/scrolling/FullBleedView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v0, v3}, Landroid/view/View;->measure(II)V

    .line 156
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lflipboard/gui/section/scrolling/FullBleedView;->setMeasuredDimension(II)V

    .line 157
    return-void

    :cond_2
    move v0, v1

    .line 133
    goto/16 :goto_0

    .line 138
    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->c:Lflipboard/gui/FLImageView;

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget v5, p0, Lflipboard/gui/section/scrolling/FullBleedView;->i:I

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Lflipboard/gui/FLImageView;->measure(II)V

    goto/16 :goto_1
.end method

.method public setMaxHeight(I)V
    .locals 0

    .prologue
    .line 194
    iput p1, p0, Lflipboard/gui/section/scrolling/FullBleedView;->i:I

    .line 195
    return-void
.end method
