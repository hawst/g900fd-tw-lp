.class Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;
.super Lflipboard/gui/FLSliderHider$Slider;
.source "ProfileHeaderView.java"


# instance fields
.field final synthetic a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I


# direct methods
.method private constructor <init>(Lflipboard/gui/section/scrolling/header/ProfileHeaderView;)V
    .locals 3

    .prologue
    .line 203
    iput-object p1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    .line 204
    sget-object v0, Lflipboard/gui/FLSliderHider$ScrollTrackingType;->b:Lflipboard/gui/FLSliderHider$ScrollTrackingType;

    invoke-direct {p0, v0}, Lflipboard/gui/FLSliderHider$Slider;-><init>(Lflipboard/gui/FLSliderHider$ScrollTrackingType;)V

    .line 205
    invoke-virtual {p1}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->b:I

    .line 206
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->c:I

    .line 207
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->d:I

    .line 208
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBar;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBar;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->e:I

    .line 209
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getTop()I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->f:I

    .line 210
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->getPaddingTop()I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->g:I

    .line 212
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    iget-object v1, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setPivotX(F)V

    .line 213
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setPivotY(F)V

    .line 214
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setPivotX(F)V

    .line 215
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getTop()I

    move-result v1

    iget-object v2, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setPivotY(F)V

    .line 216
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->d:Lflipboard/gui/FLTextView;

    iget-object v1, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setPivotX(F)V

    .line 217
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->d:Lflipboard/gui/FLTextView;

    iget-object v1, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getTop()I

    move-result v1

    iget-object v2, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setPivotY(F)V

    .line 218
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->e:Lflipboard/gui/MetricBar;

    iget-object v1, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->e:Lflipboard/gui/MetricBar;

    invoke-virtual {v1}, Lflipboard/gui/MetricBar;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lflipboard/gui/MetricBar;->setPivotX(F)V

    .line 219
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->e:Lflipboard/gui/MetricBar;

    iget-object v1, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->e:Lflipboard/gui/MetricBar;

    invoke-virtual {v1}, Lflipboard/gui/MetricBar;->getTop()I

    move-result v1

    iget-object v2, p1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lflipboard/gui/MetricBar;->setPivotY(F)V

    .line 220
    return-void
.end method

.method synthetic constructor <init>(Lflipboard/gui/section/scrolling/header/ProfileHeaderView;B)V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0, p1}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;-><init>(Lflipboard/gui/section/scrolling/header/ProfileHeaderView;)V

    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 224
    iget v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->d:I

    if-eqz v0, :cond_0

    .line 225
    iget v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->e:I

    int-to-float v0, v0

    mul-float/2addr v0, v3

    iget v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->d:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 226
    sub-float v1, v3, v0

    sub-float v2, v3, p1

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 227
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iget-object v1, v1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setScaleX(F)V

    .line 228
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iget-object v1, v1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setScaleY(F)V

    .line 229
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iget-object v1, v1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setScaleX(F)V

    .line 230
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iget-object v1, v1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setScaleY(F)V

    .line 231
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iget-object v1, v1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setScaleX(F)V

    .line 232
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iget-object v1, v1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setScaleY(F)V

    .line 233
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iget-object v1, v1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->e:Lflipboard/gui/MetricBar;

    invoke-virtual {v1, v0}, Lflipboard/gui/MetricBar;->setScaleX(F)V

    .line 234
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iget-object v1, v1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->e:Lflipboard/gui/MetricBar;

    invoke-virtual {v1, v0}, Lflipboard/gui/MetricBar;->setScaleY(F)V

    .line 237
    :cond_0
    iget v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->f:I

    iget v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->g:I

    sub-int/2addr v0, v1

    neg-int v0, v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    .line 238
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iget-object v1, v1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setTranslationY(F)V

    .line 239
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iget-object v1, v1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setTranslationY(F)V

    .line 240
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iget-object v1, v1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    sub-float v2, v3, p1

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setAlpha(F)V

    .line 241
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iget-object v1, v1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setTranslationY(F)V

    .line 242
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iget-object v1, v1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->d:Lflipboard/gui/FLTextView;

    sub-float v2, v3, p1

    invoke-virtual {v1, v2}, Lflipboard/gui/FLTextView;->setAlpha(F)V

    .line 243
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iget-object v1, v1, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->e:Lflipboard/gui/MetricBar;

    invoke-virtual {v1, v0}, Lflipboard/gui/MetricBar;->setTranslationY(F)V

    .line 244
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iget-object v0, v0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->e:Lflipboard/gui/MetricBar;

    sub-float v1, v3, p1

    invoke-virtual {v0, v1}, Lflipboard/gui/MetricBar;->setAlpha(F)V

    .line 245
    iget v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->b:I

    int-to-float v0, v0

    iget v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->b:I

    iget v2, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->c:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 246
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    invoke-static {v1}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c(Lflipboard/gui/section/scrolling/header/ProfileHeaderView;)Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    invoke-virtual {v2}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v1, v4, v4, v2, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 247
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$ProfileSlider;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    invoke-static {v1}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c(Lflipboard/gui/section/scrolling/header/ProfileHeaderView;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->setClipRect(Landroid/graphics/Rect;)V

    .line 248
    return-void
.end method
