.class public Lflipboard/gui/section/scrolling/AudioView$$ViewInjector;
.super Ljava/lang/Object;
.source "AudioView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/scrolling/AudioView;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a0091

    const-string v1, "field \'titleView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/AudioView;->a:Lflipboard/gui/FLStaticTextView;

    .line 12
    const v0, 0x7f0a0092

    const-string v1, "field \'durationView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/AudioView;->b:Lflipboard/gui/FLTextView;

    .line 14
    const v0, 0x7f0a0093

    const-string v1, "field \'subtitleView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/AudioView;->c:Lflipboard/gui/FLTextView;

    .line 16
    const v0, 0x7f0a008e

    const-string v1, "field \'imageView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/AudioView;->d:Lflipboard/gui/FLImageView;

    .line 18
    const v0, 0x7f0a008c

    const-string v1, "field \'attributionService\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    check-cast v0, Lflipboard/gui/section/AttributionServiceInfo;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/AudioView;->e:Lflipboard/gui/section/AttributionServiceInfo;

    .line 20
    const v0, 0x7f0a0094

    const-string v1, "field \'itemActionBar\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    check-cast v0, Lflipboard/gui/section/scrolling/ItemActionBar;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/AudioView;->f:Lflipboard/gui/section/scrolling/ItemActionBar;

    .line 22
    const v0, 0x7f0a008f

    const-string v1, "field \'playOverlay\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/AudioView;->g:Landroid/widget/ImageView;

    .line 24
    const v0, 0x7f0a0090

    const-string v1, "field \'loadingIndicator\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 25
    check-cast v0, Lflipboard/gui/FLBusyView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/AudioView;->h:Lflipboard/gui/FLBusyView;

    .line 26
    const v0, 0x7f0a008d

    const-string v1, "field \'contentContainer\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 27
    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/AudioView;->i:Landroid/widget/LinearLayout;

    .line 28
    return-void
.end method

.method public static reset(Lflipboard/gui/section/scrolling/AudioView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->a:Lflipboard/gui/FLStaticTextView;

    .line 32
    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->b:Lflipboard/gui/FLTextView;

    .line 33
    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->c:Lflipboard/gui/FLTextView;

    .line 34
    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->d:Lflipboard/gui/FLImageView;

    .line 35
    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->e:Lflipboard/gui/section/AttributionServiceInfo;

    .line 36
    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->f:Lflipboard/gui/section/scrolling/ItemActionBar;

    .line 37
    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->g:Landroid/widget/ImageView;

    .line 38
    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->h:Lflipboard/gui/FLBusyView;

    .line 39
    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->i:Landroid/widget/LinearLayout;

    .line 40
    return-void
.end method
