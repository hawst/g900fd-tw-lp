.class Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;
.super Lflipboard/gui/FLSliderHider$Slider;
.source "TopicHeaderView.java"


# instance fields
.field final synthetic a:Lflipboard/gui/section/scrolling/header/TopicHeaderView;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I


# direct methods
.method private constructor <init>(Lflipboard/gui/section/scrolling/header/TopicHeaderView;)V
    .locals 3

    .prologue
    .line 178
    iput-object p1, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->a:Lflipboard/gui/section/scrolling/header/TopicHeaderView;

    .line 179
    sget-object v0, Lflipboard/gui/FLSliderHider$ScrollTrackingType;->b:Lflipboard/gui/FLSliderHider$ScrollTrackingType;

    invoke-direct {p0, v0}, Lflipboard/gui/FLSliderHider$Slider;-><init>(Lflipboard/gui/FLSliderHider$ScrollTrackingType;)V

    .line 180
    invoke-virtual {p1}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->b:I

    .line 181
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->c:I

    .line 182
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->d:I

    .line 183
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBar;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBar;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->e:I

    .line 184
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getTop()I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->f:I

    .line 185
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->getPaddingTop()I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->g:I

    .line 187
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->b:Lflipboard/gui/FLTextView;

    iget-object v1, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setPivotX(F)V

    .line 188
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->b:Lflipboard/gui/FLTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setPivotY(F)V

    .line 189
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c:Lflipboard/gui/FLLabelTextView;

    iget-object v1, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setPivotX(F)V

    .line 190
    iget-object v0, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c:Lflipboard/gui/FLLabelTextView;

    iget-object v1, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLLabelTextView;->getTop()I

    move-result v1

    iget-object v2, p1, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setPivotY(F)V

    .line 191
    return-void
.end method

.method synthetic constructor <init>(Lflipboard/gui/section/scrolling/header/TopicHeaderView;B)V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0, p1}, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;-><init>(Lflipboard/gui/section/scrolling/header/TopicHeaderView;)V

    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 195
    iget v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->d:I

    if-eqz v0, :cond_0

    .line 196
    iget v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->e:I

    int-to-float v0, v0

    mul-float/2addr v0, v4

    iget v2, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->d:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 197
    sub-float v2, v4, v0

    sub-float v3, v4, p1

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    .line 198
    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->a:Lflipboard/gui/section/scrolling/header/TopicHeaderView;

    iget-object v2, v2, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v2, v0}, Lflipboard/gui/FLTextView;->setScaleX(F)V

    .line 199
    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->a:Lflipboard/gui/section/scrolling/header/TopicHeaderView;

    iget-object v2, v2, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v2, v0}, Lflipboard/gui/FLTextView;->setScaleY(F)V

    .line 200
    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->a:Lflipboard/gui/section/scrolling/header/TopicHeaderView;

    iget-object v2, v2, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2, v0}, Lflipboard/gui/FLLabelTextView;->setScaleX(F)V

    .line 201
    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->a:Lflipboard/gui/section/scrolling/header/TopicHeaderView;

    iget-object v2, v2, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2, v0}, Lflipboard/gui/FLLabelTextView;->setScaleY(F)V

    .line 204
    :cond_0
    iget v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->f:I

    iget v2, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->g:I

    sub-int/2addr v0, v2

    neg-int v0, v0

    int-to-float v0, v0

    mul-float v2, v0, p1

    .line 205
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->a:Lflipboard/gui/section/scrolling/header/TopicHeaderView;

    iget-object v0, v0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setTranslationY(F)V

    .line 206
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->a:Lflipboard/gui/section/scrolling/header/TopicHeaderView;

    iget-object v0, v0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    cmpl-float v0, p1, v4

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 207
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->a:Lflipboard/gui/section/scrolling/header/TopicHeaderView;

    iget-object v0, v0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLLabelTextView;->setTranslationY(F)V

    .line 208
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->a:Lflipboard/gui/section/scrolling/header/TopicHeaderView;

    iget-object v0, v0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c:Lflipboard/gui/FLLabelTextView;

    sub-float v2, v4, p1

    invoke-virtual {v0, v2}, Lflipboard/gui/FLLabelTextView;->setAlpha(F)V

    .line 209
    iget v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->b:I

    int-to-float v0, v0

    iget v2, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->b:I

    iget v3, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->c:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v2, p1

    sub-float/2addr v0, v2

    float-to-int v0, v0

    .line 210
    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->a:Lflipboard/gui/section/scrolling/header/TopicHeaderView;

    invoke-static {v2}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c(Lflipboard/gui/section/scrolling/header/TopicHeaderView;)Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->a:Lflipboard/gui/section/scrolling/header/TopicHeaderView;

    invoke-virtual {v3}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {v2, v1, v1, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 211
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->a:Lflipboard/gui/section/scrolling/header/TopicHeaderView;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$TopicSlider;->a:Lflipboard/gui/section/scrolling/header/TopicHeaderView;

    invoke-static {v1}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c(Lflipboard/gui/section/scrolling/header/TopicHeaderView;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->setClipRect(Landroid/graphics/Rect;)V

    .line 212
    return-void

    .line 206
    :cond_1
    const/16 v0, 0xff

    goto :goto_0
.end method
