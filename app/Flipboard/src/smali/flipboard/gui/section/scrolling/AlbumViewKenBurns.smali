.class public Lflipboard/gui/section/scrolling/AlbumViewKenBurns;
.super Lflipboard/gui/FLRelativeLayout;
.source "AlbumViewKenBurns.java"

# interfaces
.implements Lflipboard/gui/section/scrolling/PhoneItemView;


# instance fields
.field a:Lflipboard/gui/KenBurnsImageView;

.field b:Lflipboard/gui/FLTextView;

.field c:Landroid/widget/TextView;

.field d:Lflipboard/gui/section/scrolling/ItemActionBar;

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    invoke-static {}, Lflipboard/util/AndroidUtil;->d()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->b(Landroid/app/Activity;)I

    move-result v0

    sub-int v0, v1, v0

    iput v0, p0, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->e:I

    .line 58
    return-void
.end method

.method public static a(Lflipboard/objs/FeedItem;)Z
    .locals 2

    .prologue
    .line 95
    invoke-static {p0}, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->b(Lflipboard/objs/FeedItem;)Ljava/util/List;

    move-result-object v0

    .line 96
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lflipboard/objs/FeedItem;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/objs/FeedItem;",
            ")",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Image;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v5, 0x2bc

    .line 76
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->P()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    .line 77
    :goto_0
    if-nez v0, :cond_1

    .line 78
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    .line 90
    :goto_1
    return-object v0

    .line 76
    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    goto :goto_0

    .line 80
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 81
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 82
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v3

    .line 83
    if-eqz v3, :cond_2

    .line 84
    iget v0, v3, Lflipboard/objs/Image;->f:I

    if-lt v0, v5, :cond_3

    iget v0, v3, Lflipboard/objs/Image;->g:I

    if-lt v0, v5, :cond_3

    const/4 v0, 0x1

    .line 85
    :goto_3
    invoke-virtual {v3}, Lflipboard/objs/Image;->b()Z

    move-result v4

    if-nez v4, :cond_2

    if-eqz v0, :cond_2

    .line 86
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 84
    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    :cond_4
    move-object v0, v1

    .line 90
    goto :goto_1
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->a:Lflipboard/gui/KenBurnsImageView;

    invoke-static {p2}, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->b(Lflipboard/objs/FeedItem;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/KenBurnsImageView;->setImages(Ljava/util/List;)V

    .line 70
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->b:Lflipboard/gui/FLTextView;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->c:Landroid/widget/TextView;

    invoke-static {p2}, Lflipboard/gui/section/ItemDisplayUtil;->d(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->d:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/scrolling/ItemActionBar;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 73
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onFinishInflate()V

    .line 63
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 64
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->d:Lflipboard/gui/section/scrolling/ItemActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/section/scrolling/ItemActionBar;->setInverted(Z)V

    .line 65
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 101
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    .line 102
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    .line 103
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    .line 104
    iget v0, p0, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->e:I

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 105
    invoke-super {p0, p1, v0}, Lflipboard/gui/FLRelativeLayout;->onMeasure(II)V

    .line 106
    return-void
.end method

.method public setMaxHeight(I)V
    .locals 0

    .prologue
    .line 110
    iput p1, p0, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->e:I

    .line 111
    return-void
.end method
