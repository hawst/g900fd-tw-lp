.class public Lflipboard/gui/section/scrolling/ItemProfileBar$$ViewInjector;
.super Ljava/lang/Object;
.source "ItemProfileBar$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/scrolling/ItemProfileBar;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const v5, 0x7f0a021f

    const v4, 0x7f0a021e

    const v3, 0x7f0a021d

    .line 10
    const v0, 0x7f0a0218

    const-string v1, "field \'title\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/ItemProfileBar;->f:Lflipboard/gui/FLStaticTextView;

    .line 12
    const v0, 0x7f0a021a

    const-string v1, "field \'timeSpan\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/ItemProfileBar;->g:Lflipboard/gui/FLLabelTextView;

    .line 14
    const v0, 0x7f0a021b

    const-string v1, "field \'statusBody\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/ItemProfileBar;->h:Lflipboard/gui/FLTextView;

    .line 16
    const v0, 0x7f0a0217

    const-string v1, "field \'avatar\' and method \'onAvatarClick\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 17
    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/ItemProfileBar;->i:Lflipboard/gui/FLImageView;

    .line 18
    new-instance v0, Lflipboard/gui/section/scrolling/ItemProfileBar$$ViewInjector$1;

    invoke-direct {v0, p1}, Lflipboard/gui/section/scrolling/ItemProfileBar$$ViewInjector$1;-><init>(Lflipboard/gui/section/scrolling/ItemProfileBar;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    const v0, 0x7f0a0219

    const-string v1, "field \'serviceIcon\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 27
    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/ItemProfileBar;->j:Lflipboard/gui/FLImageView;

    .line 28
    const-string v0, "method \'onSocialLikeClick\'"

    invoke-virtual {p0, p2, v3, v0}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 29
    new-instance v1, Lflipboard/gui/section/scrolling/ItemProfileBar$$ViewInjector$2;

    invoke-direct {v1, p1}, Lflipboard/gui/section/scrolling/ItemProfileBar$$ViewInjector$2;-><init>(Lflipboard/gui/section/scrolling/ItemProfileBar;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    const-string v0, "method \'onSocialCommentClick\'"

    invoke-virtual {p0, p2, v4, v0}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 38
    new-instance v1, Lflipboard/gui/section/scrolling/ItemProfileBar$$ViewInjector$3;

    invoke-direct {v1, p1}, Lflipboard/gui/section/scrolling/ItemProfileBar$$ViewInjector$3;-><init>(Lflipboard/gui/section/scrolling/ItemProfileBar;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    const-string v0, "method \'onSocialShareClick\'"

    invoke-virtual {p0, p2, v5, v0}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 47
    new-instance v1, Lflipboard/gui/section/scrolling/ItemProfileBar$$ViewInjector$4;

    invoke-direct {v1, p1}, Lflipboard/gui/section/scrolling/ItemProfileBar$$ViewInjector$4;-><init>(Lflipboard/gui/section/scrolling/ItemProfileBar;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    const/4 v0, 0x3

    new-array v1, v0, [Lflipboard/gui/section/AttributionButtonWithText;

    const/4 v2, 0x0

    const-string v0, "socialButtons"

    .line 56
    invoke-virtual {p0, p2, v3, v0}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    const-string v0, "socialButtons"

    .line 57
    invoke-virtual {p0, p2, v4, v0}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    aput-object v0, v1, v2

    const/4 v2, 0x2

    const-string v0, "socialButtons"

    .line 58
    invoke-virtual {p0, p2, v5, v0}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    aput-object v0, v1, v2

    .line 55
    invoke-static {v1}, Lbutterknife/ButterKnife$Finder;->a([Landroid/view/View;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p1, Lflipboard/gui/section/scrolling/ItemProfileBar;->k:Ljava/util/List;

    .line 59
    return-void
.end method

.method public static reset(Lflipboard/gui/section/scrolling/ItemProfileBar;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 62
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->f:Lflipboard/gui/FLStaticTextView;

    .line 63
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->g:Lflipboard/gui/FLLabelTextView;

    .line 64
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->h:Lflipboard/gui/FLTextView;

    .line 65
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->i:Lflipboard/gui/FLImageView;

    .line 66
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->j:Lflipboard/gui/FLImageView;

    .line 67
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->k:Ljava/util/List;

    .line 68
    return-void
.end method
