.class public Lflipboard/gui/section/scrolling/AudioView;
.super Landroid/widget/FrameLayout;
.source "AudioView.java"

# interfaces
.implements Lflipboard/gui/item/TabletItem;
.implements Lflipboard/gui/section/scrolling/PhoneItemView;
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/FrameLayout;",
        "Lflipboard/gui/item/TabletItem;",
        "Lflipboard/gui/section/scrolling/PhoneItemView;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/audio/FLAudioManager;",
        "Lflipboard/service/audio/FLAudioManager$AudioMessage;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static j:Lflipboard/util/Log;


# instance fields
.field public a:Lflipboard/gui/FLStaticTextView;

.field public b:Lflipboard/gui/FLTextView;

.field public c:Lflipboard/gui/FLTextView;

.field public d:Lflipboard/gui/FLImageView;

.field public e:Lflipboard/gui/section/AttributionServiceInfo;

.field public f:Lflipboard/gui/section/scrolling/ItemActionBar;

.field public g:Landroid/widget/ImageView;

.field public h:Lflipboard/gui/FLBusyView;

.field public i:Landroid/widget/LinearLayout;

.field public final k:Lflipboard/service/FlipboardManager;

.field public l:Z

.field public m:I

.field private n:Lflipboard/service/audio/FLAudioManager;

.field private o:Lflipboard/service/audio/FLAudioManager$AudioState;

.field private p:Lflipboard/objs/FeedItem;

.field private q:Lflipboard/service/Section;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->b:Lflipboard/util/Log;

    sput-object v0, Lflipboard/gui/section/scrolling/AudioView;->j:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 88
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->k:Lflipboard/service/FlipboardManager;

    .line 90
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->b:Lflipboard/service/audio/FLAudioManager$AudioState;

    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->o:Lflipboard/service/audio/FLAudioManager$AudioState;

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/section/scrolling/AudioView;->l:Z

    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 88
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->k:Lflipboard/service/FlipboardManager;

    .line 90
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->b:Lflipboard/service/audio/FLAudioManager$AudioState;

    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->o:Lflipboard/service/audio/FLAudioManager$AudioState;

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/section/scrolling/AudioView;->l:Z

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 88
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->k:Lflipboard/service/FlipboardManager;

    .line 90
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->b:Lflipboard/service/audio/FLAudioManager$AudioState;

    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->o:Lflipboard/service/audio/FLAudioManager$AudioState;

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/section/scrolling/AudioView;->l:Z

    .line 106
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/scrolling/AudioView;)Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->p:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method static synthetic a(Lflipboard/gui/section/scrolling/AudioView;Lflipboard/service/audio/FLAudioManager$AudioState;)Lflipboard/service/audio/FLAudioManager$AudioState;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lflipboard/gui/section/scrolling/AudioView;->o:Lflipboard/service/audio/FLAudioManager$AudioState;

    return-object p1
.end method

.method static synthetic b(Lflipboard/gui/section/scrolling/AudioView;)Lflipboard/service/audio/FLAudioManager;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->n:Lflipboard/service/audio/FLAudioManager;

    return-object v0
.end method

.method static synthetic c(Lflipboard/gui/section/scrolling/AudioView;)Lflipboard/service/audio/FLAudioManager$AudioState;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->o:Lflipboard/service/audio/FLAudioManager$AudioState;

    return-object v0
.end method

.method static synthetic d(Lflipboard/gui/section/scrolling/AudioView;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->q:Lflipboard/service/Section;

    return-object v0
.end method

.method static synthetic e(Lflipboard/gui/section/scrolling/AudioView;)V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->h:Lflipboard/gui/FLBusyView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBusyView;->setVisibility(I)V

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->g:Landroid/widget/ImageView;

    const v1, 0x7f02004a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->a:Lflipboard/service/audio/FLAudioManager$AudioState;

    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->o:Lflipboard/service/audio/FLAudioManager$AudioState;

    return-void

    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->g:Landroid/widget/ImageView;

    const v1, 0x7f02004b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method static synthetic f(Lflipboard/gui/section/scrolling/AudioView;)V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->o:Lflipboard/service/audio/FLAudioManager$AudioState;

    sget-object v1, Lflipboard/service/audio/FLAudioManager$AudioState;->b:Lflipboard/service/audio/FLAudioManager$AudioState;

    if-eq v0, v1, :cond_0

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->g:Landroid/widget/ImageView;

    const v1, 0x7f02004e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->h:Lflipboard/gui/FLBusyView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBusyView;->setVisibility(I)V

    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->b:Lflipboard/service/audio/FLAudioManager$AudioState;

    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->o:Lflipboard/service/audio/FLAudioManager$AudioState;

    return-void

    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->g:Landroid/widget/ImageView;

    const v1, 0x7f02004f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method static synthetic g(Lflipboard/gui/section/scrolling/AudioView;)V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->h:Lflipboard/gui/FLBusyView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBusyView;->setVisibility(I)V

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->g:Landroid/widget/ImageView;

    const v1, 0x7f020044

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->c:Lflipboard/service/audio/FLAudioManager$AudioState;

    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->o:Lflipboard/service/audio/FLAudioManager$AudioState;

    return-void

    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->g:Landroid/widget/ImageView;

    const v1, 0x7f020045

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 117
    iput-object p1, p0, Lflipboard/gui/section/scrolling/AudioView;->q:Lflipboard/service/Section;

    .line 118
    iput-object p2, p0, Lflipboard/gui/section/scrolling/AudioView;->p:Lflipboard/objs/FeedItem;

    .line 119
    iget-object v2, p0, Lflipboard/gui/section/scrolling/AudioView;->a:Lflipboard/gui/FLStaticTextView;

    iget-object v3, p2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/AudioView;->getContext()Landroid/content/Context;

    iget v2, p2, Lflipboard/objs/FeedItem;->bz:F

    float-to-int v2, v2

    int-to-long v2, v2

    invoke-static {v2, v3}, Lflipboard/util/JavaUtil;->a(J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 122
    const-string v3, "00:00"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 123
    iget-object v2, p0, Lflipboard/gui/section/scrolling/AudioView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/AudioView;->getContext()Landroid/content/Context;

    iget v3, p2, Lflipboard/objs/FeedItem;->bz:F

    float-to-int v3, v3

    int-to-long v4, v3

    invoke-static {v4, v5}, Lflipboard/util/JavaUtil;->a(J)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v2, p0, Lflipboard/gui/section/scrolling/AudioView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v2, v1}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 129
    :goto_0
    iget-object v2, p0, Lflipboard/gui/section/scrolling/AudioView;->d:Lflipboard/gui/FLImageView;

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflipboard/gui/FLImageView;->setImageBestFit(Lflipboard/objs/Image;)V

    .line 130
    iget-object v2, p0, Lflipboard/gui/section/scrolling/AudioView;->d:Lflipboard/gui/FLImageView;

    new-instance v3, Lflipboard/gui/section/scrolling/AudioView$1;

    invoke-direct {v3, p0}, Lflipboard/gui/section/scrolling/AudioView$1;-><init>(Lflipboard/gui/section/scrolling/AudioView;)V

    invoke-virtual {v2, v3}, Lflipboard/gui/FLImageView;->setListener(Lflipboard/gui/FLImageView$Listener;)V

    .line 147
    invoke-static {p2}, Lflipboard/gui/section/ItemDisplayUtil;->c(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v2

    .line 148
    if-eqz v2, :cond_2

    .line 149
    iget-object v3, p0, Lflipboard/gui/section/scrolling/AudioView;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v3, v2}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    iget-object v2, p0, Lflipboard/gui/section/scrolling/AudioView;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v2, v1}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 155
    :goto_1
    iget-object v2, p0, Lflipboard/gui/section/scrolling/AudioView;->e:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v2, p1, p2}, Lflipboard/gui/section/AttributionServiceInfo;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 156
    iget-object v2, p0, Lflipboard/gui/section/scrolling/AudioView;->e:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v2, v0}, Lflipboard/gui/section/AttributionServiceInfo;->setInverted(Z)V

    .line 157
    iget-object v2, p0, Lflipboard/gui/section/scrolling/AudioView;->f:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v2, v0}, Lflipboard/gui/section/scrolling/ItemActionBar;->setInverted(Z)V

    .line 158
    iget-object v2, p0, Lflipboard/gui/section/scrolling/AudioView;->f:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v2, p1, p2}, Lflipboard/gui/section/scrolling/ItemActionBar;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 160
    iget-object v2, p0, Lflipboard/gui/section/scrolling/AudioView;->k:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->z()Lflipboard/service/audio/FLAudioManager;

    move-result-object v2

    .line 161
    invoke-virtual {v2}, Lflipboard/service/audio/FLAudioManager;->f()Lflipboard/objs/FeedItem;

    move-result-object v3

    invoke-virtual {p2, v3}, Lflipboard/objs/FeedItem;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lflipboard/service/audio/FLAudioManager;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 162
    :goto_2
    iget-object v1, p0, Lflipboard/gui/section/scrolling/AudioView;->g:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    const v0, 0x7f02004b

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 164
    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/AudioView;->l:Z

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->g:Landroid/widget/ImageView;

    new-instance v1, Lflipboard/gui/section/scrolling/AudioView$3;

    invoke-direct {v1, p0}, Lflipboard/gui/section/scrolling/AudioView$3;-><init>(Lflipboard/gui/section/scrolling/AudioView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    :cond_0
    return-void

    .line 126
    :cond_1
    iget-object v2, p0, Lflipboard/gui/section/scrolling/AudioView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v2, v6}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    goto :goto_0

    .line 152
    :cond_2
    iget-object v2, p0, Lflipboard/gui/section/scrolling/AudioView;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v2, v6}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    move v0, v1

    .line 161
    goto :goto_2

    .line 162
    :cond_4
    const v0, 0x7f02004f

    goto :goto_3
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 54
    check-cast p1, Lflipboard/service/audio/FLAudioManager;

    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->k:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/scrolling/AudioView$2;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/section/scrolling/AudioView$2;-><init>(Lflipboard/gui/section/scrolling/AudioView;Lflipboard/service/audio/FLAudioManager;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->p:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 176
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 177
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->k:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->z()Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/util/Observer;)V

    .line 178
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 182
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 183
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->k:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->z()Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lflipboard/service/audio/FLAudioManager;->c(Lflipboard/util/Observer;)V

    .line 184
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 110
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 111
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 112
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->k:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->z()Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->n:Lflipboard/service/audio/FLAudioManager;

    .line 113
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    .line 338
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 340
    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/AudioView;->l:Z

    if-nez v0, :cond_2

    .line 341
    iget v0, p0, Lflipboard/gui/section/scrolling/AudioView;->m:I

    if-nez v0, :cond_0

    .line 342
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/AudioView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/app/Activity;)I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/AudioView;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/AudioView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lflipboard/gui/section/scrolling/AudioView;->m:I

    .line 345
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView;->e:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionServiceInfo;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/section/scrolling/AudioView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lflipboard/gui/section/scrolling/AudioView;->f:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v1}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 346
    int-to-float v1, v0

    iget v2, p0, Lflipboard/gui/section/scrolling/AudioView;->m:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 347
    iget v2, p0, Lflipboard/gui/section/scrolling/AudioView;->m:I

    if-gt v0, v2, :cond_3

    float-to-double v2, v1

    const-wide v4, 0x3fe6666666666666L    # 0.7

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_3

    const/4 v1, 0x1

    .line 348
    :goto_0
    if-eqz v1, :cond_1

    .line 349
    iget v0, p0, Lflipboard/gui/section/scrolling/AudioView;->m:I

    .line 351
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/AudioView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lflipboard/gui/section/scrolling/AudioView;->setMeasuredDimension(II)V

    .line 353
    :cond_2
    return-void

    .line 347
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMaxHeight(I)V
    .locals 0

    .prologue
    .line 205
    return-void
.end method
