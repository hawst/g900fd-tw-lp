.class public Lflipboard/gui/section/scrolling/ScrollItemWrapper;
.super Landroid/widget/FrameLayout;
.source "ScrollItemWrapper.java"


# instance fields
.field public a:Landroid/view/View;

.field public b:I

.field public c:I

.field private d:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 53
    invoke-direct {p0, p1}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->setScreenHeight(Landroid/content/Context;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    invoke-direct {p0, p1}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->setScreenHeight(Landroid/content/Context;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    invoke-direct {p0, p1}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->setScreenHeight(Landroid/content/Context;)V

    .line 64
    return-void
.end method

.method private setScreenHeight(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 90
    check-cast p1, Landroid/app/Activity;

    invoke-static {p1}, Lflipboard/util/AndroidUtil;->a(Landroid/app/Activity;)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->b:I

    .line 91
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 102
    iget v0, p0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->b:I

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/2addr v0, p1

    .line 104
    iget v1, p0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->c:I

    if-nez v1, :cond_0

    .line 106
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 108
    :cond_0
    invoke-virtual {p0, v0}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->setMaxHeight(I)V

    .line 109
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 133
    iput p2, p0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->c:I

    .line 134
    packed-switch p2, :pswitch_data_0

    .line 144
    :goto_0
    return-void

    .line 136
    :pswitch_0
    div-int/lit8 v0, p1, 0x2

    div-int/lit8 v1, p1, 0x2

    invoke-virtual {p0, p1, v0, p1, v1}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->setPadding(IIII)V

    goto :goto_0

    .line 139
    :pswitch_1
    invoke-virtual {p0, v2, v2, v2, v2}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->setPadding(IIII)V

    goto :goto_0

    .line 142
    :pswitch_2
    const v0, 0x7f080072

    invoke-virtual {p0, v0}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->setBackgroundResource(I)V

    .line 143
    div-int/lit8 v0, p1, 0x2

    div-int/lit8 v1, p1, 0x2

    invoke-virtual {p0, v2, v0, v2, v1}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->setPadding(IIII)V

    goto :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getWrappedItem()Landroid/view/View;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->a:Landroid/view/View;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 68
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 69
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 71
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->a:Landroid/view/View;

    instance-of v0, v0, Lflipboard/gui/section/scrolling/PhoneItemView;

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "wrapped item must implement PhoneItemView!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_0
    invoke-virtual {p0, v3}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->a(I)V

    .line 77
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "enable_scrolling_item_position"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 78
    if-eqz v0, :cond_1

    .line 79
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->d:Landroid/widget/TextView;

    .line 80
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->d:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 81
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->d:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 82
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080099

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 83
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800aa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 84
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->d:Landroid/widget/TextView;

    const/high16 v1, 0x42800000    # 64.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 85
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->d:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->addView(Landroid/view/View;)V

    .line 87
    :cond_1
    return-void
.end method

.method public setItemPosition(I)V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->d:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    :cond_0
    return-void
.end method

.method public setMaxHeight(I)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->a:Landroid/view/View;

    check-cast v0, Lflipboard/gui/section/scrolling/PhoneItemView;

    invoke-interface {v0, p1}, Lflipboard/gui/section/scrolling/PhoneItemView;->setMaxHeight(I)V

    .line 118
    return-void
.end method

.method public setPaddingTop(I)V
    .locals 3

    .prologue
    .line 154
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->getPaddingRight()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p0, v0, p1, v1, v2}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->setPadding(IIII)V

    .line 155
    return-void
.end method
