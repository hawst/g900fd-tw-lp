.class Lflipboard/gui/section/scrolling/AudioView$3;
.super Ljava/lang/Object;
.source "AudioView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/section/scrolling/AudioView;


# direct methods
.method constructor <init>(Lflipboard/gui/section/scrolling/AudioView;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lflipboard/gui/section/scrolling/AudioView$3;->a:Lflipboard/gui/section/scrolling/AudioView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 242
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView$3;->a:Lflipboard/gui/section/scrolling/AudioView;

    invoke-static {v0}, Lflipboard/gui/section/scrolling/AudioView;->b(Lflipboard/gui/section/scrolling/AudioView;)Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    if-nez v0, :cond_1

    .line 271
    :cond_0
    :goto_0
    return-void

    .line 248
    :cond_1
    sget-object v0, Lflipboard/gui/section/scrolling/AudioView$5;->a:[I

    iget-object v1, p0, Lflipboard/gui/section/scrolling/AudioView$3;->a:Lflipboard/gui/section/scrolling/AudioView;

    invoke-static {v1}, Lflipboard/gui/section/scrolling/AudioView;->c(Lflipboard/gui/section/scrolling/AudioView;)Lflipboard/service/audio/FLAudioManager$AudioState;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/service/audio/FLAudioManager$AudioState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 260
    const-string v0, "timeline_pause"

    .line 262
    iget-object v1, p0, Lflipboard/gui/section/scrolling/AudioView$3;->a:Lflipboard/gui/section/scrolling/AudioView;

    invoke-static {v1}, Lflipboard/gui/section/scrolling/AudioView;->b(Lflipboard/gui/section/scrolling/AudioView;)Lflipboard/service/audio/FLAudioManager;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/section/scrolling/AudioView$3;->a:Lflipboard/gui/section/scrolling/AudioView;

    invoke-static {v2}, Lflipboard/gui/section/scrolling/AudioView;->a(Lflipboard/gui/section/scrolling/AudioView;)Lflipboard/objs/FeedItem;

    move-result-object v2

    iget-object v3, p0, Lflipboard/gui/section/scrolling/AudioView$3;->a:Lflipboard/gui/section/scrolling/AudioView;

    invoke-static {v3}, Lflipboard/gui/section/scrolling/AudioView;->d(Lflipboard/gui/section/scrolling/AudioView;)Lflipboard/service/Section;

    move-result-object v3

    const-string v4, "playFromTimeline"

    invoke-virtual {v1, v2, v3, v4}, Lflipboard/service/audio/FLAudioManager;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Ljava/lang/String;)V

    .line 265
    :goto_1
    sget-object v1, Lflipboard/gui/section/scrolling/AudioView;->j:Lflipboard/util/Log;

    .line 266
    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->c(Ljava/lang/String;)V

    .line 268
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView$3;->a:Lflipboard/gui/section/scrolling/AudioView;

    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/AudioView;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lflipboard/activities/SectionTabletActivity;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView$3;->a:Lflipboard/gui/section/scrolling/AudioView;

    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/AudioView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/SectionTabletActivity;

    invoke-virtual {v0}, Lflipboard/activities/SectionTabletActivity;->j()V

    goto :goto_0

    .line 250
    :pswitch_0
    const-string v0, "timeline_buffering"

    .line 252
    iget-object v1, p0, Lflipboard/gui/section/scrolling/AudioView$3;->a:Lflipboard/gui/section/scrolling/AudioView;

    invoke-static {v1}, Lflipboard/gui/section/scrolling/AudioView;->b(Lflipboard/gui/section/scrolling/AudioView;)Lflipboard/service/audio/FLAudioManager;

    move-result-object v1

    const-string v2, "bufferingFromTimeline"

    invoke-virtual {v1, v2}, Lflipboard/service/audio/FLAudioManager;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 255
    :pswitch_1
    const-string v0, "timeline_play"

    .line 257
    iget-object v1, p0, Lflipboard/gui/section/scrolling/AudioView$3;->a:Lflipboard/gui/section/scrolling/AudioView;

    invoke-static {v1}, Lflipboard/gui/section/scrolling/AudioView;->b(Lflipboard/gui/section/scrolling/AudioView;)Lflipboard/service/audio/FLAudioManager;

    move-result-object v1

    const-string v2, "pauseFromTimeline"

    invoke-virtual {v1, v2}, Lflipboard/service/audio/FLAudioManager;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 248
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
