.class public Lflipboard/gui/section/scrolling/header/SectionCover$$ViewInjector;
.super Ljava/lang/Object;
.source "SectionCover$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/scrolling/header/SectionCover;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a02d6

    const-string v1, "field \'backgroundImageView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/SectionCover;->a:Lflipboard/gui/FLImageView;

    .line 12
    const v0, 0x7f0a02d7

    const-string v1, "field \'titleTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/SectionCover;->b:Lflipboard/gui/FLTextView;

    .line 14
    const v0, 0x7f0a02d9

    const-string v1, "field \'subtitleTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/SectionCover;->c:Lflipboard/gui/FLTextView;

    .line 16
    const v0, 0x7f0a02d8

    const-string v1, "field \'authorTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/SectionCover;->d:Lflipboard/gui/FLTextView;

    .line 18
    const v0, 0x7f0a02da

    const-string v1, "field \'infoTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/SectionCover;->e:Lflipboard/gui/FLTextView;

    .line 20
    const v0, 0x7f0a02db

    const-string v1, "field \'metricOverflowTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/SectionCover;->f:Lflipboard/gui/FLTextView;

    .line 22
    const v0, 0x7f0a02dc

    const-string v1, "field \'metricBar\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    check-cast v0, Lflipboard/gui/MetricBar;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/SectionCover;->g:Lflipboard/gui/MetricBar;

    .line 24
    return-void
.end method

.method public static reset(Lflipboard/gui/section/scrolling/header/SectionCover;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->a:Lflipboard/gui/FLImageView;

    .line 28
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->b:Lflipboard/gui/FLTextView;

    .line 29
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->c:Lflipboard/gui/FLTextView;

    .line 30
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->d:Lflipboard/gui/FLTextView;

    .line 31
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->e:Lflipboard/gui/FLTextView;

    .line 32
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->f:Lflipboard/gui/FLTextView;

    .line 33
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->g:Lflipboard/gui/MetricBar;

    .line 34
    return-void
.end method
