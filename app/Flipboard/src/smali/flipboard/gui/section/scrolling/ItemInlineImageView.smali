.class public Lflipboard/gui/section/scrolling/ItemInlineImageView;
.super Landroid/view/ViewGroup;
.source "ItemInlineImageView.java"

# interfaces
.implements Lflipboard/gui/section/scrolling/PhoneItemView;


# instance fields
.field a:Lflipboard/gui/FLStaticTextView;

.field b:Lflipboard/gui/FLStaticTextView;

.field c:Lflipboard/gui/FLStaticTextView;

.field d:Lflipboard/gui/section/scrolling/ItemProfileBar;

.field e:Lflipboard/gui/FLImageView;

.field f:Lflipboard/gui/section/scrolling/ItemActionBar;

.field g:Lflipboard/objs/FeedItem;

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 64
    iput-object p2, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->g:Lflipboard/objs/FeedItem;

    .line 65
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->I()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    invoke-static {p2, v2}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/FeedItem;Z)Landroid/text/SpannableString;

    move-result-object v0

    .line 67
    if-eqz v0, :cond_0

    .line 68
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    .line 73
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->e:Lflipboard/gui/FLImageView;

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImageBestFit(Lflipboard/objs/Image;)V

    .line 75
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/scrolling/ItemProfileBar;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 76
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->f:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/scrolling/ItemActionBar;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 77
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->b:Lflipboard/gui/FLStaticTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 58
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 59
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemInlineImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->h:I

    .line 60
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemInlineImageView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemInlineImageView;->getPaddingTop()I

    move-result v3

    .line 84
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 85
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->getMeasuredHeight()I

    move-result v0

    .line 86
    iget-object v4, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    iget-object v5, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-virtual {v5}, Lflipboard/gui/section/scrolling/ItemProfileBar;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v2

    add-int v6, v3, v0

    invoke-virtual {v4, v2, v3, v5, v6}, Lflipboard/gui/section/scrolling/ItemProfileBar;->layout(IIII)V

    .line 89
    :goto_0
    add-int/2addr v0, v3

    .line 90
    iget-object v3, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->a:Lflipboard/gui/FLStaticTextView;

    iget-object v4, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v2

    iget-object v5, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v3, v2, v0, v4, v5}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 92
    iget-object v3, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v0, v3

    .line 94
    iget-object v3, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 95
    iget-object v3, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->b:Lflipboard/gui/FLStaticTextView;

    iget-object v4, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v2

    iget-object v5, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v3, v2, v0, v4, v5}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 98
    :cond_0
    iget-object v3, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    .line 99
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->e:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v1

    sub-int v1, p4, v1

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemInlineImageView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v1, v3

    .line 100
    iget-object v3, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->e:Lflipboard/gui/FLImageView;

    iget-object v4, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->e:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    iget-object v5, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->e:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v3, v1, v0, v4, v5}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 102
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v3, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v2

    iget-object v4, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v1, v2, v0, v3, v4}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 105
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->h:I

    add-int/2addr v0, v1

    .line 106
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemInlineImageView;->getPaddingLeft()I

    move-result v1

    sub-int v1, v2, v1

    .line 107
    iget-object v2, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->f:Lflipboard/gui/section/scrolling/ItemActionBar;

    iget-object v3, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->f:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v3}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->f:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v4}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2, v1, v0, v3, v4}, Lflipboard/gui/section/scrolling/ItemActionBar;->layout(IIII)V

    .line 108
    return-void

    :cond_2
    move v0, v1

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 10

    .prologue
    const/high16 v9, -0x80000000

    const/4 v8, 0x0

    .line 113
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 114
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 115
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemInlineImageView;->getPaddingLeft()I

    move-result v1

    sub-int v1, v2, v1

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemInlineImageView;->getPaddingRight()I

    move-result v3

    sub-int v3, v1, v3

    .line 116
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemInlineImageView;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemInlineImageView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 118
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v1, v4, v5}, Lflipboard/gui/section/scrolling/ItemProfileBar;->measure(II)V

    .line 119
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-virtual {v1}, Lflipboard/gui/section/scrolling/ItemProfileBar;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 120
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-virtual {v1}, Lflipboard/gui/section/scrolling/ItemProfileBar;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    :cond_0
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->a:Lflipboard/gui/FLStaticTextView;

    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v1, v4, v5}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 124
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v1

    iget-object v4, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getPaddingBottom()I

    move-result v4

    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 126
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 127
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->b:Lflipboard/gui/FLStaticTextView;

    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v1, v4, v5}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 128
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v1

    iget-object v4, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getPaddingBottom()I

    move-result v4

    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 131
    :cond_1
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->g:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    .line 132
    if-eqz v1, :cond_2

    .line 133
    mul-int/lit8 v4, v3, 0x2

    div-int/lit8 v4, v4, 0x5

    .line 135
    iget-object v5, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLStaticTextView;->getMaxHeight()F

    move-result v5

    float-to-int v5, v5

    if-lez v5, :cond_3

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMaxHeight()F

    move-result v1

    float-to-int v1, v1

    .line 137
    :goto_0
    iget-object v5, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->e:Lflipboard/gui/FLImageView;

    invoke-static {v4, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, v4, v6}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 139
    iget-object v4, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->c:Lflipboard/gui/FLStaticTextView;

    sget-object v5, Lflipboard/gui/FLStaticTextView$BlockType;->a:Lflipboard/gui/FLStaticTextView$BlockType;

    iget-object v6, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->e:Lflipboard/gui/FLImageView;

    invoke-virtual {v6}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v6

    sub-int v6, v3, v6

    iget-object v7, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->e:Lflipboard/gui/FLImageView;

    invoke-virtual {v7}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {v4, v5, v6, v7}, Lflipboard/gui/FLStaticTextView;->a(Lflipboard/gui/FLStaticTextView$BlockType;II)V

    .line 140
    iget-object v4, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->c:Lflipboard/gui/FLStaticTextView;

    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v4, v3, v5}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 142
    iget-object v3, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v3

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 144
    :cond_2
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->f:Lflipboard/gui/section/scrolling/ItemActionBar;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lflipboard/gui/section/scrolling/ItemActionBar;->measure(II)V

    .line 145
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->f:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v1}, Lflipboard/gui/section/scrolling/ItemActionBar;->getMeasuredHeight()I

    move-result v1

    iget v2, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->h:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 146
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemInlineImageView;->getPaddingTop()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemInlineImageView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0, v1, v0}, Lflipboard/gui/section/scrolling/ItemInlineImageView;->setMeasuredDimension(II)V

    .line 148
    return-void

    .line 135
    :cond_3
    iget v5, v1, Lflipboard/objs/Image;->g:I

    mul-int/2addr v5, v4

    iget v1, v1, Lflipboard/objs/Image;->f:I

    div-int v1, v5, v1

    goto :goto_0
.end method

.method public setMaxHeight(I)V
    .locals 0

    .prologue
    .line 153
    return-void
.end method
