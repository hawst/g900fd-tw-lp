.class public Lflipboard/gui/section/scrolling/header/ProfileCover;
.super Lflipboard/gui/FLViewGroup;
.source "ProfileCover.java"

# interfaces
.implements Lflipboard/gui/item/TabletItem;
.implements Lflipboard/gui/section/scrolling/component/CarouselComponent$OnPageSelectedListener;
.implements Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter;
.implements Lflipboard/gui/section/scrolling/component/MetricBarComponent$OnMetricClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/gui/FLViewGroup;",
        "Lflipboard/gui/item/TabletItem;",
        "Lflipboard/gui/section/scrolling/component/CarouselComponent$OnPageSelectedListener",
        "<",
        "Lflipboard/objs/FeedItem;",
        ">;",
        "Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter",
        "<",
        "Lflipboard/objs/FeedItem;",
        ">;",
        "Lflipboard/gui/section/scrolling/component/MetricBarComponent$OnMetricClickListener;"
    }
.end annotation


# instance fields
.field protected a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

.field protected b:Lflipboard/gui/FLImageView;

.field protected c:Lflipboard/gui/CarouselView;

.field protected d:Landroid/widget/TextView;

.field private e:Lflipboard/gui/section/scrolling/component/CarouselComponent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/gui/section/scrolling/component/CarouselComponent",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lflipboard/service/Section;

.field private g:Lflipboard/objs/FeedItem;

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/scrolling/header/ProfileCover;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->f:Lflipboard/service/Section;

    return-object v0
.end method

.method private static b(I)I
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x4

    if-ge p0, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(I)I
    .locals 1

    .prologue
    .line 31
    invoke-static {p1}, Lflipboard/gui/section/scrolling/header/ProfileCover;->b(I)I

    move-result v0

    return v0
.end method

.method public final synthetic a(Ljava/lang/Object;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 31
    check-cast p1, Lflipboard/objs/FeedItem;

    invoke-static {p2}, Lflipboard/gui/section/scrolling/header/ProfileCover;->b(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    if-eqz p3, :cond_2

    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/header/ProfileCover$MagazineCarouselTileHolder;

    :goto_0
    iput-object p1, v0, Lflipboard/gui/section/scrolling/header/ProfileCover$MagazineCarouselTileHolder;->c:Lflipboard/objs/FeedItem;

    iget-object v1, v0, Lflipboard/gui/section/scrolling/header/ProfileCover$MagazineCarouselTileHolder;->a:Landroid/widget/TextView;

    iget-object v2, p1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v0, Lflipboard/gui/section/scrolling/header/ProfileCover$MagazineCarouselTileHolder;->b:Landroid/widget/TextView;

    iget-object v1, p1, Lflipboard/objs/FeedItem;->by:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-object p3

    :pswitch_0
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/header/ProfileCover$MagazineCarouselTileEndCardHolder;

    :goto_2
    iget v1, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->h:I

    add-int/lit8 v1, v1, -0x4

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->j:Ljava/lang/String;

    :goto_3
    iget-object v0, v0, Lflipboard/gui/section/scrolling/header/ProfileCover$MagazineCarouselTileEndCardHolder;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_0
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300cf

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    new-instance v0, Lflipboard/gui/section/scrolling/header/ProfileCover$MagazineCarouselTileEndCardHolder;

    invoke-direct {v0, p0, p3}, Lflipboard/gui/section/scrolling/header/ProfileCover$MagazineCarouselTileEndCardHolder;-><init>(Lflipboard/gui/section/scrolling/header/ProfileCover;Landroid/view/View;)V

    invoke-virtual {p3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_2

    :cond_1
    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->i:Ljava/lang/String;

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_2
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300ce

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    new-instance v0, Lflipboard/gui/section/scrolling/header/ProfileCover$MagazineCarouselTileHolder;

    invoke-direct {v0, p0, p3}, Lflipboard/gui/section/scrolling/header/ProfileCover$MagazineCarouselTileHolder;-><init>(Lflipboard/gui/section/scrolling/header/ProfileCover;Landroid/view/View;)V

    invoke-virtual {p3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final synthetic a(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 31
    check-cast p2, Lflipboard/objs/FeedItem;

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setImageBestFit(Lflipboard/objs/Image;)V

    return-void
.end method

.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x0

    .line 129
    iput-object p1, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->f:Lflipboard/service/Section;

    .line 130
    iput-object p2, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->g:Lflipboard/objs/FeedItem;

    .line 131
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->setItem(Lflipboard/service/Section;)V

    .line 132
    const/4 v0, 0x0

    .line 133
    if-eqz p1, :cond_1

    iget-object v1, p1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v1, v1, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v1, v1, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 134
    iget-object v0, p1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    .line 138
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 139
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v4}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 140
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->c:Lflipboard/gui/CarouselView;

    invoke-virtual {v1, v4}, Lflipboard/gui/CarouselView;->setVisibility(I)V

    .line 141
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 142
    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iput v1, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->h:I

    .line 146
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->e:Lflipboard/gui/section/scrolling/component/CarouselComponent;

    iget-object v0, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    const/4 v2, 0x5

    iget v3, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->h:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-interface {v0, v4, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/gui/section/scrolling/component/CarouselComponent;->a(Ljava/util/List;)V

    .line 162
    :goto_1
    return-void

    .line 135
    :cond_1
    if-eqz p2, :cond_0

    iget-object v1, p2, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p2, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    iget-object v0, p2, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    goto :goto_0

    .line 149
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->c:Lflipboard/gui/CarouselView;

    invoke-virtual {v0, v2}, Lflipboard/gui/CarouselView;->setVisibility(I)V

    .line 151
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 153
    iget-object v0, p1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->i:Lflipboard/objs/FeedSectionLink;

    .line 154
    if-eqz v0, :cond_3

    iget-object v1, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-static {v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 155
    iget-object v0, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    .line 160
    :goto_2
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/ProfileCover;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d024e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 158
    :cond_3
    const-string v0, "User"

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 115
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 125
    :cond_1
    :goto_1
    return-void

    .line 115
    :sswitch_0
    const-string v1, "magazineCount"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "follower"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 117
    :pswitch_0
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/ProfileCover;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->f:Lflipboard/service/Section;

    invoke-static {v0, v1}, Lflipboard/util/ActivityUtil;->a(Landroid/content/Context;Lflipboard/service/Section;)V

    goto :goto_1

    .line 120
    :pswitch_1
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->f:Lflipboard/service/Section;

    if-eqz v0, :cond_1

    .line 121
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/ProfileCover;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->f:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->J:Ljava/lang/String;

    invoke-static {v0, v1}, Lflipboard/util/ActivityUtil;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 115
    :sswitch_data_0
    .sparse-switch
        -0x60eb2e25 -> :sswitch_0
        0x11fd201e -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->g:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 70
    invoke-super {p0}, Lflipboard/gui/FLViewGroup;->onFinishInflate()V

    .line 71
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 72
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    invoke-virtual {v0, p0}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->setOnMetricClickListener(Lflipboard/gui/section/scrolling/component/MetricBarComponent$OnMetricClickListener;)V

    .line 73
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->setShowActionBar(Z)V

    .line 74
    new-instance v0, Lflipboard/gui/section/scrolling/component/CarouselComponent;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->c:Lflipboard/gui/CarouselView;

    invoke-direct {v0, v1, p0}, Lflipboard/gui/section/scrolling/component/CarouselComponent;-><init>(Lflipboard/gui/CarouselView;Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter;)V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->e:Lflipboard/gui/section/scrolling/component/CarouselComponent;

    .line 75
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->e:Lflipboard/gui/section/scrolling/component/CarouselComponent;

    iput-object p0, v0, Lflipboard/gui/section/scrolling/component/CarouselComponent;->c:Lflipboard/gui/section/scrolling/component/CarouselComponent$OnPageSelectedListener;

    .line 76
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/ProfileCover;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0214

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->i:Ljava/lang/String;

    .line 77
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/ProfileCover;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0242

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->j:Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->e:Lflipboard/gui/section/scrolling/component/CarouselComponent;

    new-instance v1, Lflipboard/gui/PaginatedPagerTransformer;

    invoke-direct {v1}, Lflipboard/gui/PaginatedPagerTransformer;-><init>()V

    iget-object v0, v0, Lflipboard/gui/section/scrolling/component/CarouselComponent;->a:Lflipboard/gui/CarouselView;

    invoke-virtual {v0, v1}, Lflipboard/gui/CarouselView;->setPageTransformer$382b7817(Landroid/support/v4/view/ViewPager$PageTransformer;)V

    .line 79
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 101
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/ProfileCover;->getPaddingLeft()I

    move-result v0

    .line 102
    sub-int v1, p4, p2

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/ProfileCover;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 103
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/ProfileCover;->getPaddingTop()I

    move-result v2

    .line 104
    iget-object v3, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    invoke-static {v3, v2, v0, v1, v5}, Lflipboard/gui/section/scrolling/header/ProfileCover;->a(Landroid/view/View;IIII)I

    move-result v3

    add-int/2addr v2, v3

    .line 105
    iget-object v3, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->d:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_0

    .line 106
    iget-object v3, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->b:Lflipboard/gui/FLImageView;

    invoke-static {v3, v2, v0, v1, v5}, Lflipboard/gui/section/scrolling/header/ProfileCover;->a(Landroid/view/View;IIII)I

    .line 107
    iget-object v3, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->c:Lflipboard/gui/CarouselView;

    invoke-static {v3, v2, v0, v1, v5}, Lflipboard/gui/section/scrolling/header/ProfileCover;->a(Landroid/view/View;IIII)I

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v3, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->d:Landroid/widget/TextView;

    invoke-static {v3, v2, v0, v1, v5}, Lflipboard/gui/section/scrolling/header/ProfileCover;->a(Landroid/view/View;IIII)I

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 84
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/ProfileCover;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 86
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/ProfileCover;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/ProfileCover;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 87
    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 88
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/ProfileCover;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/ProfileCover;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    invoke-virtual {v2}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 89
    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 90
    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->d:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_0

    .line 91
    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 92
    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->c:Lflipboard/gui/CarouselView;

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/CarouselView;->measure(II)V

    .line 96
    :goto_0
    invoke-super {p0, p1, p2}, Lflipboard/gui/FLViewGroup;->onMeasure(II)V

    .line 97
    return-void

    .line 94
    :cond_0
    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v0, v1}, Landroid/widget/TextView;->measure(II)V

    goto :goto_0
.end method
