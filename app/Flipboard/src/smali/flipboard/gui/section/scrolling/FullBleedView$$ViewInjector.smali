.class public Lflipboard/gui/section/scrolling/FullBleedView$$ViewInjector;
.super Ljava/lang/Object;
.source "FullBleedView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/scrolling/FullBleedView;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a0091

    const-string v1, "field \'titleView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/FullBleedView;->a:Lflipboard/gui/FLStaticTextView;

    .line 12
    const v0, 0x7f0a0093

    const-string v1, "field \'subtitleView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/FullBleedView;->b:Lflipboard/gui/FLTextView;

    .line 14
    const v0, 0x7f0a008e

    const-string v1, "field \'imageView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/FullBleedView;->c:Lflipboard/gui/FLImageView;

    .line 16
    const v0, 0x7f0a020c

    const-string v1, "field \'profileBar\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    check-cast v0, Lflipboard/gui/section/scrolling/ItemProfileBar;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/FullBleedView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    .line 18
    const v0, 0x7f0a0094

    const-string v1, "field \'actionBar\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    check-cast v0, Lflipboard/gui/section/scrolling/ItemActionBar;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/FullBleedView;->e:Lflipboard/gui/section/scrolling/ItemActionBar;

    .line 20
    const v0, 0x7f0a01f8

    const-string v1, "field \'gradientBottom\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    iput-object v0, p1, Lflipboard/gui/section/scrolling/FullBleedView;->f:Landroid/view/View;

    .line 22
    const v0, 0x7f0a01f9

    const-string v1, "field \'gradientTop\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    iput-object v0, p1, Lflipboard/gui/section/scrolling/FullBleedView;->g:Landroid/view/View;

    .line 24
    return-void
.end method

.method public static reset(Lflipboard/gui/section/scrolling/FullBleedView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    iput-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->a:Lflipboard/gui/FLStaticTextView;

    .line 28
    iput-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->b:Lflipboard/gui/FLTextView;

    .line 29
    iput-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->c:Lflipboard/gui/FLImageView;

    .line 30
    iput-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    .line 31
    iput-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->e:Lflipboard/gui/section/scrolling/ItemActionBar;

    .line 32
    iput-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->f:Landroid/view/View;

    .line 33
    iput-object v0, p0, Lflipboard/gui/section/scrolling/FullBleedView;->g:Landroid/view/View;

    .line 34
    return-void
.end method
