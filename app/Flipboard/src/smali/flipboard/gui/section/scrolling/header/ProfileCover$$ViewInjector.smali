.class public Lflipboard/gui/section/scrolling/header/ProfileCover$$ViewInjector;
.super Ljava/lang/Object;
.source "ProfileCover$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/scrolling/header/ProfileCover;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a02a9

    const-string v1, "field \'profileHeaderView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileCover;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    .line 12
    const v0, 0x7f0a02aa

    const-string v1, "field \'magazineCoverImageView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileCover;->b:Lflipboard/gui/FLImageView;

    .line 14
    const v0, 0x7f0a02ab

    const-string v1, "field \'magazineCarouselView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/CarouselView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileCover;->c:Lflipboard/gui/CarouselView;

    .line 16
    const v0, 0x7f0a028d

    const-string v1, "field \'emptyStateTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/header/ProfileCover;->d:Landroid/widget/TextView;

    .line 18
    return-void
.end method

.method public static reset(Lflipboard/gui/section/scrolling/header/ProfileCover;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    .line 22
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->b:Lflipboard/gui/FLImageView;

    .line 23
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->c:Lflipboard/gui/CarouselView;

    .line 24
    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileCover;->d:Landroid/widget/TextView;

    .line 25
    return-void
.end method
