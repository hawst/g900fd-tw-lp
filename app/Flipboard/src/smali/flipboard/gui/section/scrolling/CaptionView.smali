.class public Lflipboard/gui/section/scrolling/CaptionView;
.super Landroid/widget/LinearLayout;
.source "CaptionView.java"

# interfaces
.implements Lflipboard/gui/section/scrolling/PhoneItemView;


# instance fields
.field a:Lflipboard/gui/FLStaticTextView;

.field b:Lflipboard/gui/FLTextView;

.field c:Lflipboard/gui/FLImageView;

.field d:Lflipboard/gui/section/scrolling/ItemProfileBar;

.field e:Lflipboard/gui/section/scrolling/ItemActionBar;

.field public f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 91
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/CaptionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/CaptionView;->f:Z

    if-eqz v0, :cond_0

    const v0, 0x7f08009b

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/scrolling/CaptionView;->setBackgroundColor(I)V

    .line 92
    return-void

    .line 91
    :cond_0
    const v0, 0x7f080004

    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 95
    iget-object v1, p0, Lflipboard/gui/section/scrolling/CaptionView;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/CaptionView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/CaptionView;->f:Z

    if-eqz v0, :cond_0

    const v0, 0x7f080051

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    .line 96
    return-void

    .line 95
    :cond_0
    const v0, 0x7f080090

    goto :goto_0
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 64
    iget-object v0, p0, Lflipboard/gui/section/scrolling/CaptionView;->a:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v0, p0, Lflipboard/gui/section/scrolling/CaptionView;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImageBestFit(Lflipboard/objs/Image;)V

    .line 66
    invoke-static {p2, v2}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/FeedItem;Z)Landroid/text/SpannableString;

    move-result-object v0

    .line 67
    if-eqz v0, :cond_0

    .line 68
    iget-object v1, p0, Lflipboard/gui/section/scrolling/CaptionView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lflipboard/gui/section/scrolling/CaptionView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 73
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/CaptionView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    iget-boolean v1, p0, Lflipboard/gui/section/scrolling/CaptionView;->f:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/section/scrolling/ItemProfileBar;->setInverted(Z)V

    .line 74
    iget-object v0, p0, Lflipboard/gui/section/scrolling/CaptionView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/scrolling/ItemProfileBar;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 75
    iget-object v0, p0, Lflipboard/gui/section/scrolling/CaptionView;->e:Lflipboard/gui/section/scrolling/ItemActionBar;

    iget-boolean v1, p0, Lflipboard/gui/section/scrolling/CaptionView;->f:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/section/scrolling/ItemActionBar;->setInverted(Z)V

    .line 76
    iget-object v0, p0, Lflipboard/gui/section/scrolling/CaptionView;->e:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/scrolling/ItemActionBar;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 78
    invoke-direct {p0}, Lflipboard/gui/section/scrolling/CaptionView;->b()V

    .line 79
    invoke-direct {p0}, Lflipboard/gui/section/scrolling/CaptionView;->a()V

    .line 80
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/CaptionView;->b:Lflipboard/gui/FLTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 58
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 59
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 60
    return-void
.end method

.method public setInverted(Z)V
    .locals 1

    .prologue
    .line 83
    iput-boolean p1, p0, Lflipboard/gui/section/scrolling/CaptionView;->f:Z

    .line 84
    iget-object v0, p0, Lflipboard/gui/section/scrolling/CaptionView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/scrolling/ItemProfileBar;->setInverted(Z)V

    .line 85
    iget-object v0, p0, Lflipboard/gui/section/scrolling/CaptionView;->e:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/scrolling/ItemActionBar;->setInverted(Z)V

    .line 86
    invoke-direct {p0}, Lflipboard/gui/section/scrolling/CaptionView;->b()V

    .line 87
    invoke-direct {p0}, Lflipboard/gui/section/scrolling/CaptionView;->a()V

    .line 88
    return-void
.end method

.method public setMaxHeight(I)V
    .locals 0

    .prologue
    .line 101
    return-void
.end method
