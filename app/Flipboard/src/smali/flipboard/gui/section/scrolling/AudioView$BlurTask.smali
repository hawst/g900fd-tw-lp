.class Lflipboard/gui/section/scrolling/AudioView$BlurTask;
.super Landroid/os/AsyncTask;
.source "AudioView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lflipboard/io/BitmapManager$Handle;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/gui/section/scrolling/AudioView;


# direct methods
.method private constructor <init>(Lflipboard/gui/section/scrolling/AudioView;)V
    .locals 0

    .prologue
    .line 358
    iput-object p1, p0, Lflipboard/gui/section/scrolling/AudioView$BlurTask;->a:Lflipboard/gui/section/scrolling/AudioView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflipboard/gui/section/scrolling/AudioView;B)V
    .locals 0

    .prologue
    .line 358
    invoke-direct {p0, p1}, Lflipboard/gui/section/scrolling/AudioView$BlurTask;-><init>(Lflipboard/gui/section/scrolling/AudioView;)V

    return-void
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 358
    check-cast p1, [Lflipboard/io/BitmapManager$Handle;

    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView$BlurTask;->a:Lflipboard/gui/section/scrolling/AudioView;

    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/AudioView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Lflipboard/io/BitmapManager$Handle;->b()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/section/ItemDisplayUtil;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 358
    check-cast p1, Landroid/graphics/Bitmap;

    if-eqz p1, :cond_0

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/AudioView$BlurTask;->a:Lflipboard/gui/section/scrolling/AudioView;

    invoke-virtual {v1}, Lflipboard/gui/section/scrolling/AudioView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lflipboard/gui/section/scrolling/AudioView$BlurTask;->a:Lflipboard/gui/section/scrolling/AudioView;

    invoke-virtual {v1}, Lflipboard/gui/section/scrolling/AudioView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080048

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DARKEN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v1, p0, Lflipboard/gui/section/scrolling/AudioView$BlurTask;->a:Lflipboard/gui/section/scrolling/AudioView;

    invoke-virtual {v1, v0}, Lflipboard/gui/section/scrolling/AudioView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AudioView$BlurTask;->a:Lflipboard/gui/section/scrolling/AudioView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lflipboard/gui/section/scrolling/AudioView;->setBackgroundColor(I)V

    goto :goto_0
.end method
