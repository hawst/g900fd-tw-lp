.class public Lflipboard/gui/section/scrolling/header/SectionCover;
.super Lflipboard/gui/FLViewGroup;
.source "SectionCover.java"

# interfaces
.implements Lflipboard/gui/item/TabletItem;
.implements Lflipboard/gui/section/ContributorsDialog$ContributorChangedListener;


# instance fields
.field protected a:Lflipboard/gui/FLImageView;

.field protected b:Lflipboard/gui/FLTextView;

.field protected c:Lflipboard/gui/FLTextView;

.field protected d:Lflipboard/gui/FLTextView;

.field protected e:Lflipboard/gui/FLTextView;

.field protected f:Lflipboard/gui/FLTextView;

.field protected g:Lflipboard/gui/MetricBar;

.field private h:Lflipboard/objs/FeedItem;

.field private i:I

.field private j:Lflipboard/gui/section/scrolling/component/MetricBarComponent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;)V

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->i:I

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->i:I

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->i:I

    .line 82
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/scrolling/header/SectionCover;)Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->h:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method static synthetic a(Lflipboard/gui/FLTextIntf;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 42
    invoke-static {p0, p1}, Lflipboard/gui/section/scrolling/header/SectionCover;->b(Lflipboard/gui/FLTextIntf;Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/scrolling/header/SectionCover;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 3

    .prologue
    .line 42
    invoke-static {p1, p2}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->d:Lflipboard/gui/FLTextView;

    invoke-static {v0}, Lflipboard/gui/section/ItemDisplayUtil;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lflipboard/gui/section/scrolling/header/SectionCover;->b(Lflipboard/gui/FLTextIntf;Ljava/lang/CharSequence;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->i:I

    return-void
.end method

.method static synthetic b(Lflipboard/gui/section/scrolling/header/SectionCover;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->i:I

    return v0
.end method

.method private static b(Lflipboard/gui/FLTextIntf;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 218
    invoke-static {p1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    const/16 v0, 0x8

    invoke-interface {p0, v0}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 224
    :goto_0
    return-void

    .line 221
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 222
    invoke-interface {p0, p1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setBackgroundImageView(Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 227
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->J()Lflipboard/objs/Image;

    move-result-object v0

    .line 228
    if-eqz v0, :cond_0

    .line 229
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setImageBestFit(Lflipboard/objs/Image;)V

    .line 250
    :goto_0
    return-void

    .line 231
    :cond_0
    iget-object v0, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lflipboard/gui/section/scrolling/header/SectionCover$6;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/section/scrolling/header/SectionCover$6;-><init>(Lflipboard/gui/section/scrolling/header/SectionCover;Lflipboard/objs/FeedItem;)V

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Ljava/util/List;Lflipboard/service/Flap$TypedResultObserver;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;)V
    .locals 1

    .prologue
    .line 207
    new-instance v0, Lflipboard/gui/section/scrolling/header/SectionCover$5;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/section/scrolling/header/SectionCover$5;-><init>(Lflipboard/gui/section/scrolling/header/SectionCover;Lflipboard/service/Section;)V

    invoke-virtual {p0, v0}, Lflipboard/gui/section/scrolling/header/SectionCover;->post(Ljava/lang/Runnable;)Z

    .line 215
    return-void
.end method

.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 123
    iput-object p2, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->h:Lflipboard/objs/FeedItem;

    .line 124
    invoke-virtual {p0, p1}, Lflipboard/gui/section/scrolling/header/SectionCover;->setItem(Lflipboard/service/Section;)V

    .line 125
    return-void
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->h:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0}, Lflipboard/gui/FLViewGroup;->onFinishInflate()V

    .line 87
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 88
    new-instance v0, Lflipboard/gui/section/scrolling/component/MetricBarComponent;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->g:Lflipboard/gui/MetricBar;

    invoke-direct {v0, v1}, Lflipboard/gui/section/scrolling/component/MetricBarComponent;-><init>(Lflipboard/gui/MetricBar;)V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->j:Lflipboard/gui/section/scrolling/component/MetricBarComponent;

    .line 89
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->a:Lflipboard/gui/FLImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setDisableLoadingView(Z)V

    .line 90
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 106
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, p2, p3, p4, p5}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 108
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/SectionCover;->getPaddingLeft()I

    move-result v0

    .line 109
    sub-int v1, p4, p2

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/SectionCover;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 110
    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->b:Lflipboard/gui/FLTextView;

    const/4 v3, 0x0

    invoke-static {v2, v3, v0, v1, v4}, Lflipboard/gui/section/scrolling/header/SectionCover;->a(Landroid/view/View;IIII)I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    .line 112
    iget-object v3, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->d:Lflipboard/gui/FLTextView;

    invoke-static {v3, v2, v0, v1, v4}, Lflipboard/gui/section/scrolling/header/SectionCover;->a(Landroid/view/View;IIII)I

    .line 114
    sub-int v2, p5, p3

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/SectionCover;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    .line 115
    iget-object v3, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->g:Lflipboard/gui/MetricBar;

    invoke-static {v3, v2, v0, v1}, Lflipboard/gui/section/scrolling/header/SectionCover;->a(Landroid/view/View;III)I

    move-result v3

    sub-int/2addr v2, v3

    .line 116
    iget-object v3, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->f:Lflipboard/gui/FLTextView;

    invoke-static {v3, v2, v0, v1}, Lflipboard/gui/section/scrolling/header/SectionCover;->a(Landroid/view/View;III)I

    move-result v3

    sub-int/2addr v2, v3

    .line 117
    iget-object v3, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->e:Lflipboard/gui/FLTextView;

    invoke-static {v3, v2, v0, v1}, Lflipboard/gui/section/scrolling/header/SectionCover;->a(Landroid/view/View;III)I

    move-result v3

    sub-int/2addr v2, v3

    .line 118
    iget-object v3, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->c:Lflipboard/gui/FLTextView;

    invoke-static {v3, v2, v0, v1}, Lflipboard/gui/section/scrolling/header/SectionCover;->a(Landroid/view/View;III)I

    .line 119
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 94
    invoke-super {p0, p1, p2}, Lflipboard/gui/FLViewGroup;->onMeasure(II)V

    .line 95
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->a:Lflipboard/gui/FLImageView;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/SectionCover;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 96
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->b:Lflipboard/gui/FLTextView;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/SectionCover;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 97
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->d:Lflipboard/gui/FLTextView;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/SectionCover;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 98
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->c:Lflipboard/gui/FLTextView;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/SectionCover;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 99
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->e:Lflipboard/gui/FLTextView;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/SectionCover;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 100
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->f:Lflipboard/gui/FLTextView;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/SectionCover;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 101
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->g:Lflipboard/gui/MetricBar;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/SectionCover;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 102
    return-void
.end method

.method public setItem(Lflipboard/service/Section;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 133
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->h:Lflipboard/objs/FeedItem;

    if-nez v0, :cond_0

    .line 134
    iget-object v0, p1, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->h:Lflipboard/objs/FeedItem;

    .line 136
    :cond_0
    invoke-virtual {p1}, Lflipboard/service/Section;->f()Z

    move-result v0

    if-nez v0, :cond_2

    .line 137
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/SectionCover;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020180

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getCurrentTextColor()I

    move-result v2

    invoke-static {v2}, Lflipboard/util/ColorFilterUtil;->b(I)Landroid/graphics/ColorFilter;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 139
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v0, v3, v3, v3}, Lflipboard/gui/FLTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 143
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->h:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->h:Lflipboard/objs/FeedItem;

    invoke-direct {p0, v0}, Lflipboard/gui/section/scrolling/header/SectionCover;->setBackgroundImageView(Lflipboard/objs/FeedItem;)V

    .line 145
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->j:Lflipboard/gui/section/scrolling/component/MetricBarComponent;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->h:Lflipboard/objs/FeedItem;

    new-instance v2, Lflipboard/gui/section/scrolling/header/SectionCover$1;

    invoke-direct {v2, p0}, Lflipboard/gui/section/scrolling/header/SectionCover$1;-><init>(Lflipboard/gui/section/scrolling/header/SectionCover;)V

    iput-object v2, v0, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->c:Lflipboard/gui/section/scrolling/component/MetricBarComponent$ArticleCountMetricListener;

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-object v0, v0, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->d:Lflipboard/service/Flap$CommentaryObserver;

    invoke-virtual {v2, v1, v0}, Lflipboard/service/FlipboardManager;->a(Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)V

    .line 151
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->e:Lflipboard/gui/FLTextView;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->h:Lflipboard/objs/FeedItem;

    invoke-static {v1}, Lflipboard/gui/section/ItemDisplayUtil;->b(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/section/scrolling/header/SectionCover;->b(Lflipboard/gui/FLTextIntf;Ljava/lang/CharSequence;)V

    .line 152
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->e:Lflipboard/gui/FLTextView;

    new-instance v1, Lflipboard/gui/section/scrolling/header/SectionCover$2;

    invoke-direct {v1, p0}, Lflipboard/gui/section/scrolling/header/SectionCover$2;-><init>(Lflipboard/gui/section/scrolling/header/SectionCover;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->b:Lflipboard/gui/FLTextView;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->h:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-static {v0, v1}, Lflipboard/gui/section/scrolling/header/SectionCover;->b(Lflipboard/gui/FLTextIntf;Ljava/lang/CharSequence;)V

    .line 162
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->c:Lflipboard/gui/FLTextView;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->h:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->by:Ljava/lang/String;

    invoke-static {v0, v1}, Lflipboard/gui/section/scrolling/header/SectionCover;->b(Lflipboard/gui/FLTextIntf;Ljava/lang/CharSequence;)V

    .line 164
    :cond_1
    new-instance v0, Lflipboard/gui/section/scrolling/header/SectionCover$3;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/section/scrolling/header/SectionCover$3;-><init>(Lflipboard/gui/section/scrolling/header/SectionCover;Lflipboard/service/Section;)V

    invoke-virtual {p1, v0}, Lflipboard/service/Section;->a(Lflipboard/service/Flap$JSONResultObserver;)V

    .line 174
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->d:Lflipboard/gui/FLTextView;

    new-instance v1, Lflipboard/gui/section/scrolling/header/SectionCover$4;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/section/scrolling/header/SectionCover$4;-><init>(Lflipboard/gui/section/scrolling/header/SectionCover;Lflipboard/service/Section;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    return-void

    .line 141
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/SectionCover;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v3, v3, v3, v3}, Lflipboard/gui/FLTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
