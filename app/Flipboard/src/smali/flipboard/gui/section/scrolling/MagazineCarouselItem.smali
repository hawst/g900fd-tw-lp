.class public Lflipboard/gui/section/scrolling/MagazineCarouselItem;
.super Landroid/widget/LinearLayout;
.source "MagazineCarouselItem.java"

# interfaces
.implements Lflipboard/gui/section/scrolling/PhoneItemView;


# instance fields
.field a:Lit/sephiroth/android/library/widget/HListView;

.field b:Lflipboard/gui/FLLabelTextView;

.field c:Lflipboard/gui/section/scrolling/MagazineCarouselItem$HorizontalListAdapter;

.field private d:Landroid/graphics/Paint;

.field private e:Lcom/squareup/otto/Bus;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 5

    .prologue
    .line 71
    if-eqz p2, :cond_1

    iget-object v0, p2, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 72
    iget-object v0, p2, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    .line 73
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 74
    instance-of v1, v2, Lflipboard/activities/FlipboardActivity;

    if-eqz v1, :cond_0

    move-object v1, v2

    .line 75
    check-cast v1, Lflipboard/activities/FlipboardActivity;

    iget-object v1, v1, Lflipboard/activities/FlipboardActivity;->aa:Lcom/squareup/otto/Bus;

    iput-object v1, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->e:Lcom/squareup/otto/Bus;

    .line 79
    :cond_0
    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    const/4 v3, 0x1

    iget-object v4, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v1, v3, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 80
    new-instance v3, Lflipboard/gui/section/scrolling/MagazineCarouselItem$HorizontalListAdapter;

    invoke-static {v1}, Lflipboard/util/AndroidUtil;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v3, p0, v2, v1}, Lflipboard/gui/section/scrolling/MagazineCarouselItem$HorizontalListAdapter;-><init>(Lflipboard/gui/section/scrolling/MagazineCarouselItem;Landroid/content/Context;Ljava/util/List;)V

    iput-object v3, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->c:Lflipboard/gui/section/scrolling/MagazineCarouselItem$HorizontalListAdapter;

    .line 81
    iget-object v1, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->a:Lit/sephiroth/android/library/widget/HListView;

    iget-object v2, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->c:Lflipboard/gui/section/scrolling/MagazineCarouselItem$HorizontalListAdapter;

    invoke-virtual {v1, v2}, Lit/sephiroth/android/library/widget/HListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 82
    iget-object v1, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->b:Lflipboard/gui/FLLabelTextView;

    iget-object v0, v0, Lflipboard/objs/SidebarGroup;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    :cond_1
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    .line 89
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 91
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->getWidth()I

    move-result v6

    .line 92
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 93
    iget-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getBottom()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLLabelTextView;->getTop()I

    move-result v1

    add-int/2addr v0, v1

    div-int/lit8 v8, v0, 0x2

    .line 94
    iget-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getLeft()I

    move-result v0

    sub-int/2addr v0, v7

    .line 95
    iget-object v1, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLLabelTextView;->getRight()I

    move-result v1

    add-int v9, v1, v7

    .line 97
    int-to-float v1, v7

    int-to-float v2, v8

    int-to-float v3, v0

    int-to-float v4, v8

    iget-object v5, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 98
    int-to-float v1, v9

    int-to-float v2, v8

    sub-int v0, v6, v7

    int-to-float v3, v0

    int-to-float v4, v8

    iget-object v5, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 100
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 59
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 60
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 61
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->d:Landroid/graphics/Paint;

    .line 62
    iget-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->d:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080052

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 63
    iget-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 64
    iget-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->d:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 65
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->setWillNotDraw(Z)V

    .line 66
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 104
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 105
    packed-switch v0, :pswitch_data_0

    .line 123
    :cond_0
    :goto_0
    :pswitch_0
    return v2

    .line 107
    :pswitch_1
    iget-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->e:Lcom/squareup/otto/Bus;

    if-eqz v0, :cond_0

    .line 108
    new-instance v0, Lflipboard/activities/FlipboardActivity$ToggleSwipeBackGestureEvent;

    invoke-direct {v0}, Lflipboard/activities/FlipboardActivity$ToggleSwipeBackGestureEvent;-><init>()V

    .line 109
    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/activities/FlipboardActivity$ToggleSwipeBackGestureEvent;->a:Z

    .line 110
    iget-object v1, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->e:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, v0}, Lcom/squareup/otto/Bus;->c(Ljava/lang/Object;)V

    goto :goto_0

    .line 116
    :pswitch_2
    iget-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->e:Lcom/squareup/otto/Bus;

    if-eqz v0, :cond_0

    .line 117
    new-instance v0, Lflipboard/activities/FlipboardActivity$ToggleSwipeBackGestureEvent;

    invoke-direct {v0}, Lflipboard/activities/FlipboardActivity$ToggleSwipeBackGestureEvent;-><init>()V

    .line 118
    iput-boolean v2, v0, Lflipboard/activities/FlipboardActivity$ToggleSwipeBackGestureEvent;->a:Z

    .line 119
    iget-object v1, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->e:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, v0}, Lcom/squareup/otto/Bus;->c(Ljava/lang/Object;)V

    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setMaxHeight(I)V
    .locals 0

    .prologue
    .line 179
    return-void
.end method
