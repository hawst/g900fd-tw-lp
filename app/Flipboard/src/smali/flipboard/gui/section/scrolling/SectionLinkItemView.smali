.class public Lflipboard/gui/section/scrolling/SectionLinkItemView;
.super Landroid/widget/FrameLayout;
.source "SectionLinkItemView.java"

# interfaces
.implements Lflipboard/gui/item/TabletItem;
.implements Lflipboard/gui/section/scrolling/PhoneItemView;


# instance fields
.field public a:Lflipboard/gui/FLImageView;

.field public b:Lflipboard/gui/FLTextView;

.field public c:Lflipboard/gui/FLTextView;

.field public d:Lflipboard/gui/FLTextView;

.field public e:Lflipboard/gui/FollowButton;

.field public f:Lflipboard/gui/section/scrolling/ItemActionBar;

.field public g:Lflipboard/gui/section/AttributionServiceInfo;

.field public h:Landroid/widget/FrameLayout;

.field private i:Lflipboard/objs/FeedItem;

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 75
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 85
    iput-object p2, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->i:Lflipboard/objs/FeedItem;

    .line 86
    iget-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->J()Lflipboard/objs/Image;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImageBestFit(Lflipboard/objs/Image;)V

    .line 88
    iget-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->g:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/AttributionServiceInfo;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 89
    iget-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->g:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v0, v4}, Lflipboard/gui/section/AttributionServiceInfo;->setInverted(Z)V

    .line 91
    iget-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->b:Lflipboard/gui/FLTextView;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v0, p2, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    .line 94
    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/SectionLinkItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0324

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p2, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 101
    :goto_0
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->C()Lflipboard/objs/FeedSectionLink;

    move-result-object v1

    .line 102
    iget-object v0, p2, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 103
    :goto_1
    if-eqz v0, :cond_2

    .line 104
    iget-object v2, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v2, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 110
    :goto_2
    iget-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->e:Lflipboard/gui/FollowButton;

    invoke-virtual {v0}, Lflipboard/gui/FollowButton;->a()V

    .line 111
    iget-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->e:Lflipboard/gui/FollowButton;

    invoke-virtual {v0, v4}, Lflipboard/gui/FollowButton;->setInverted(Z)V

    .line 112
    iget-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->e:Lflipboard/gui/FollowButton;

    invoke-virtual {v0, v1}, Lflipboard/gui/FollowButton;->setSectionLink(Lflipboard/objs/FeedSectionLink;)V

    .line 114
    new-instance v0, Lflipboard/gui/section/scrolling/SectionLinkItemView$1;

    invoke-direct {v0, p0, p2}, Lflipboard/gui/section/scrolling/SectionLinkItemView$1;-><init>(Lflipboard/gui/section/scrolling/SectionLinkItemView;Lflipboard/objs/FeedItem;)V

    invoke-virtual {p0, v0}, Lflipboard/gui/section/scrolling/SectionLinkItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->f:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v0, v4}, Lflipboard/gui/section/scrolling/ItemActionBar;->setInverted(Z)V

    .line 122
    iget-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->f:Lflipboard/gui/section/scrolling/ItemActionBar;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/scrolling/ItemActionBar;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 123
    return-void

    .line 98
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v6}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    goto :goto_0

    .line 102
    :cond_1
    iget-object v0, p2, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->j:Ljava/lang/String;

    goto :goto_1

    .line 107
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v6}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->i:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 79
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 80
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 81
    return-void
.end method

.method public setIsFullBleed(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 136
    iput-boolean p1, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->j:Z

    .line 138
    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->j:Z

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/SectionLinkItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09000b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 142
    :goto_0
    iget-object v2, p0, Lflipboard/gui/section/scrolling/SectionLinkItemView;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1, v0, v1, v1}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 143
    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public setMaxHeight(I)V
    .locals 0

    .prologue
    .line 133
    return-void
.end method
