.class Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;
.super Ljava/lang/Object;
.source "MagazineGridComponent.java"

# interfaces
.implements Lflipboard/gui/FLDynamicGridView$ViewAdapter;


# instance fields
.field final synthetic a:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;


# direct methods
.method constructor <init>(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;->a:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 76
    instance-of v0, p1, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$CreateNewMagazineTile;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 87
    invoke-virtual {p0, p1}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;->a(Ljava/lang/Object;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 99
    if-nez p2, :cond_1

    .line 100
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;->a:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-static {v0}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->a(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)Lflipboard/activities/FlipboardActivity;

    move-result-object v0

    const v1, 0x7f0300d6

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 101
    new-instance v0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;->a:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-direct {v0, v1}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;-><init>(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)V

    .line 102
    invoke-static {v0, p2}, Lbutterknife/ButterKnife;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 103
    iget-object v1, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;->a:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-static {v1}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->b(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)Landroid/widget/AbsListView$LayoutParams;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 104
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 112
    :goto_0
    iget-object v1, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;->a:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-static {v1}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->c(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 113
    check-cast p1, Lflipboard/objs/FeedItem;

    .line 114
    iget-object v1, v0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, p1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 115
    iget-object v0, v0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->b:Landroid/widget/TextView;

    iget-object v1, p1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    :cond_0
    :goto_1
    return-object p2

    .line 89
    :pswitch_0
    if-nez p2, :cond_0

    .line 90
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;->a:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-static {v0}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->a(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)Lflipboard/activities/FlipboardActivity;

    move-result-object v0

    const v1, 0x7f0300d7

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 91
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;->a:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-static {v0}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->b(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)Landroid/widget/AbsListView$LayoutParams;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 108
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;

    .line 109
    invoke-static {v0}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->a(Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;)V

    goto :goto_0

    .line 117
    :cond_2
    check-cast p1, Lflipboard/objs/Magazine;

    invoke-static {p1, v0}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->a(Lflipboard/objs/Magazine;Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;)V

    goto :goto_1

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 127
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;->a:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-static {v0}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->c(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    check-cast p1, Lflipboard/objs/FeedItem;

    .line 129
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;->a:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-static {v0}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->a(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)Lflipboard/activities/FlipboardActivity;

    move-result-object v0

    iget-object v1, p1, Lflipboard/objs/FeedItem;->d:Ljava/lang/String;

    iget-object v2, p1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    iget-object v4, p1, Lflipboard/objs/FeedItem;->n:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lflipboard/util/ActivityUtil;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :goto_0
    return-void

    .line 131
    :cond_0
    invoke-virtual {p0, p1}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;->a(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 132
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;->a:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-static {v1}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->a(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)Lflipboard/activities/FlipboardActivity;

    move-result-object v1

    const-class v2, Lflipboard/activities/GenericDialogFragmentActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 133
    const-string v1, "fragment_type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 134
    iget-object v1, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;->a:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-static {v1}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->a(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)Lflipboard/activities/FlipboardActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 136
    :cond_1
    check-cast p1, Lflipboard/objs/Magazine;

    .line 137
    iget-boolean v0, p1, Lflipboard/objs/Magazine;->n:Z

    if-eqz v0, :cond_2

    .line 138
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;->a:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-static {v0}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->a(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)Lflipboard/activities/FlipboardActivity;

    move-result-object v0

    iget-object v1, p1, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lflipboard/util/ActivityUtil;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 140
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileViewAdapter;->a:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-static {v0}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->a(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)Lflipboard/activities/FlipboardActivity;

    move-result-object v0

    iget-object v1, p1, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    iget-object v2, p1, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/Magazine;->r:Ljava/lang/String;

    iget-object v4, p1, Lflipboard/objs/Magazine;->s:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lflipboard/util/ActivityUtil;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
