.class Lflipboard/gui/section/scrolling/MagazineCarouselItem$HorizontalListAdapter;
.super Landroid/widget/BaseAdapter;
.source "MagazineCarouselItem.java"


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field b:Landroid/view/LayoutInflater;

.field final synthetic c:Lflipboard/gui/section/scrolling/MagazineCarouselItem;


# direct methods
.method public constructor <init>(Lflipboard/gui/section/scrolling/MagazineCarouselItem;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 131
    iput-object p1, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem$HorizontalListAdapter;->c:Lflipboard/gui/section/scrolling/MagazineCarouselItem;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 132
    iput-object p3, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem$HorizontalListAdapter;->a:Ljava/util/List;

    .line 133
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem$HorizontalListAdapter;->b:Landroid/view/LayoutInflater;

    .line 134
    return-void
.end method

.method private a(I)Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem$HorizontalListAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem$HorizontalListAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 126
    invoke-direct {p0, p1}, Lflipboard/gui/section/scrolling/MagazineCarouselItem$HorizontalListAdapter;->a(I)Lflipboard/objs/FeedItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 148
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 154
    if-nez p2, :cond_0

    .line 155
    iget-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselItem$HorizontalListAdapter;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f0300cd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 158
    :goto_0
    invoke-direct {p0, p1}, Lflipboard/gui/section/scrolling/MagazineCarouselItem$HorizontalListAdapter;->a(I)Lflipboard/objs/FeedItem;

    move-result-object v2

    move-object v0, v1

    .line 159
    check-cast v0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;

    invoke-virtual {v0, v2}, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->setItem(Lflipboard/objs/FeedItem;)V

    .line 161
    new-instance v0, Lflipboard/gui/section/scrolling/MagazineCarouselItem$HorizontalListAdapter$1;

    invoke-direct {v0, p0, v2}, Lflipboard/gui/section/scrolling/MagazineCarouselItem$HorizontalListAdapter$1;-><init>(Lflipboard/gui/section/scrolling/MagazineCarouselItem$HorizontalListAdapter;Lflipboard/objs/FeedItem;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    return-object v1

    :cond_0
    move-object v1, p2

    goto :goto_0
.end method
