.class public Lflipboard/gui/section/scrolling/ItemActionBar;
.super Lflipboard/gui/FLRelativeLayout;
.source "ItemActionBar.java"

# interfaces
.implements Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;


# instance fields
.field a:Lflipboard/service/Section;

.field b:Lflipboard/objs/FeedItem;

.field public c:Lflipboard/gui/FLCameleonImageView;

.field public d:Lflipboard/gui/FLCameleonImageView;

.field public e:Lflipboard/gui/FLCameleonImageView;

.field public f:Lflipboard/gui/FLTextView;

.field g:Lflipboard/gui/FLTextView;

.field h:Lflipboard/objs/FeedSectionLink;

.field private i:Z

.field private j:Lflipboard/objs/ConfigService;

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 82
    return-void
.end method

.method private a(III)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 183
    const/4 v0, 0x0

    .line 184
    if-lez p1, :cond_0

    .line 186
    if-ne p1, v1, :cond_1

    .line 187
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 191
    :goto_0
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 193
    :cond_0
    return-object v0

    .line 189
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Lflipboard/objs/HasCommentaryItem;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 150
    iget-object v1, p1, Lflipboard/objs/HasCommentaryItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    .line 151
    const/4 v0, 0x0

    .line 152
    if-eqz v1, :cond_3

    .line 153
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 154
    iget v3, v1, Lflipboard/objs/CommentaryResult$Item;->b:I

    const v4, 0x7f0d006d

    const v5, 0x7f0d006e

    invoke-direct {p0, v3, v4, v5}, Lflipboard/gui/section/scrolling/ItemActionBar;->a(III)Ljava/lang/String;

    move-result-object v3

    .line 155
    if-eqz v3, :cond_0

    .line 156
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    :cond_0
    iget v3, v1, Lflipboard/objs/CommentaryResult$Item;->c:I

    const v4, 0x7f0d01e2

    const v5, 0x7f0d01e1

    invoke-direct {p0, v3, v4, v5}, Lflipboard/gui/section/scrolling/ItemActionBar;->a(III)Ljava/lang/String;

    move-result-object v3

    .line 159
    if-eqz v3, :cond_1

    .line 160
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    :cond_1
    iget v1, v1, Lflipboard/objs/CommentaryResult$Item;->d:I

    const v3, 0x7f0d0264

    const v4, 0x7f0d0265

    invoke-direct {p0, v1, v3, v4}, Lflipboard/gui/section/scrolling/ItemActionBar;->a(III)Ljava/lang/String;

    move-result-object v1

    .line 163
    if-eqz v1, :cond_2

    .line 164
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 167
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d003d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 170
    :cond_3
    if-nez v0, :cond_4

    .line 171
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->g:Lflipboard/gui/FLTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 173
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemActionBar;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemActionBar;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemActionBar;->getPaddingRight()I

    move-result v3

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09007b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    invoke-virtual {p0, v1, v2, v3, v4}, Lflipboard/gui/section/scrolling/ItemActionBar;->setPadding(IIII)V

    .line 179
    :goto_0
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    return-void

    .line 175
    :cond_4
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v6}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 176
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemActionBar;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemActionBar;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemActionBar;->getPaddingRight()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v6}, Lflipboard/gui/section/scrolling/ItemActionBar;->setPadding(IIII)V

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 210
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->i:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0800aa

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    .line 211
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->f:Lflipboard/gui/FLTextView;

    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->i:Z

    if-eqz v0, :cond_1

    const v0, 0x7f020232

    :goto_1
    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setBackgroundResource(I)V

    .line 212
    return-void

    .line 210
    :cond_0
    const v0, 0x7f080097

    goto :goto_0

    .line 211
    :cond_1
    const v0, 0x7f020230

    goto :goto_1
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 215
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->j:Lflipboard/objs/ConfigService;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->d:Lflipboard/gui/FLCameleonImageView;

    const v1, 0x7f020003

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    .line 217
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->d:Lflipboard/gui/FLCameleonImageView;

    iget-boolean v1, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->i:Z

    iget-object v2, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->b:Lflipboard/objs/FeedItem;

    iget-boolean v2, v2, Lflipboard/objs/FeedItem;->af:Z

    invoke-static {v0, v1, v2}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLCameleonImageView;ZZ)V

    .line 218
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->e:Lflipboard/gui/FLCameleonImageView;

    const/high16 v1, 0x7f020000

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    .line 219
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->e:Lflipboard/gui/FLCameleonImageView;

    iget-boolean v1, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->i:Z

    invoke-static {v0, v1, v3}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLCameleonImageView;ZZ)V

    .line 220
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->c:Lflipboard/gui/FLCameleonImageView;

    const v1, 0x7f020001

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    .line 221
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->c:Lflipboard/gui/FLCameleonImageView;

    iget-boolean v1, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->i:Z

    invoke-static {v0, v1, v3}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLCameleonImageView;ZZ)V

    .line 223
    :cond_0
    return-void
.end method

.method public final a(Lflipboard/objs/HasCommentaryItem;)V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0, p1}, Lflipboard/gui/section/scrolling/ItemActionBar;->b(Lflipboard/objs/HasCommentaryItem;)V

    .line 147
    return-void
.end method

.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 100
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->ai()Ljava/lang/String;

    move-result-object v0

    .line 101
    if-nez v0, :cond_0

    .line 102
    const-string v0, "flipboard"

    .line 104
    :cond_0
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2, v0}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v2

    iput-object v2, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->j:Lflipboard/objs/ConfigService;

    .line 105
    iget-object v2, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->c:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v2, p2}, Lflipboard/gui/FLCameleonImageView;->setTag(Ljava/lang/Object;)V

    .line 108
    const-string v2, "flipboard"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 109
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->d:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->e:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setVisibility(I)V

    .line 115
    :goto_0
    iput-object p2, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->b:Lflipboard/objs/FeedItem;

    .line 116
    iput-object p1, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->a:Lflipboard/service/Section;

    .line 117
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemActionBar;->a()V

    .line 118
    invoke-direct {p0}, Lflipboard/gui/section/scrolling/ItemActionBar;->c()V

    .line 119
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->b:Lflipboard/objs/FeedItem;

    invoke-static {v0}, Lflipboard/util/TopicHelper;->a(Lflipboard/objs/FeedItem;)Lflipboard/objs/FeedSectionLink;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->h:Lflipboard/objs/FeedSectionLink;

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->h:Lflipboard/objs/FeedSectionLink;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->f:Lflipboard/gui/FLTextView;

    iget-object v2, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->h:Lflipboard/objs/FeedSectionLink;

    iget-object v2, v2, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 120
    :cond_1
    :goto_1
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    .line 121
    if-eqz v0, :cond_5

    const-string v2, "flipboard"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->k:Z

    .line 122
    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->k:Z

    if-eqz v0, :cond_2

    .line 123
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/gui/section/scrolling/ItemActionBar;->b(Lflipboard/objs/HasCommentaryItem;)V

    .line 125
    :cond_2
    return-void

    .line 112
    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->d:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLCameleonImageView;->setVisibility(I)V

    .line 113
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->e:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLCameleonImageView;->setVisibility(I)V

    goto :goto_0

    .line 119
    :cond_4
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->f:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    goto :goto_1

    :cond_5
    move v0, v1

    .line 121
    goto :goto_2
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 237
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v1, "commentButtonTapped"

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    .line 238
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemActionBar;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 239
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->b:Lflipboard/objs/FeedItem;

    iget-object v2, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->a:Lflipboard/service/Section;

    sget-object v3, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->b:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    invoke-static {v1, v2, v0, v3}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/UsageEventV2$SocialCardNavFrom;)V

    .line 240
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 129
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onAttachedToWindow()V

    .line 130
    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->k:Z

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedItem;->a(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 132
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->b:Lflipboard/objs/FeedItem;

    invoke-direct {p0, v0}, Lflipboard/gui/section/scrolling/ItemActionBar;->b(Lflipboard/objs/HasCommentaryItem;)V

    .line 134
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 138
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onDetachedFromWindow()V

    .line 139
    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->k:Z

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedItem;->b(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 142
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 86
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onFinishInflate()V

    .line 87
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 88
    return-void
.end method

.method public setInverted(Z)V
    .locals 3

    .prologue
    .line 91
    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->i:Z

    if-eq p1, v0, :cond_0

    .line 92
    iput-boolean p1, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->i:Z

    .line 93
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemActionBar;->a()V

    .line 94
    invoke-direct {p0}, Lflipboard/gui/section/scrolling/ItemActionBar;->c()V

    .line 95
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz p1, :cond_1

    const v0, 0x7f0800aa

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    .line 97
    :cond_0
    return-void

    .line 95
    :cond_1
    const v0, 0x7f080038

    goto :goto_0
.end method
