.class public Lflipboard/gui/section/scrolling/header/StandardHeaderView;
.super Lflipboard/gui/FLViewGroup;
.source "StandardHeaderView.java"

# interfaces
.implements Lflipboard/gui/section/scrolling/header/HeaderView;


# instance fields
.field protected a:Lflipboard/gui/actionbar/FLActionBar;

.field private b:Lflipboard/gui/section/scrolling/component/ActionBarComponent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/StandardHeaderView;->b:Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->a(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)V

    .line 77
    return-void
.end method

.method public getActionBar()Lflipboard/gui/actionbar/FLActionBar;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/StandardHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 71
    return-object p0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 38
    invoke-super {p0}, Lflipboard/gui/FLViewGroup;->onFinishInflate()V

    .line 39
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 40
    new-instance v0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/StandardHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-direct {v0, p0, v1}, Lflipboard/gui/section/scrolling/component/ActionBarComponent;-><init>(Landroid/view/View;Lflipboard/gui/actionbar/FLActionBar;)V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/StandardHeaderView;->b:Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    .line 41
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 51
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/StandardHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/StandardHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBar;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/StandardHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v2}, Lflipboard/gui/actionbar/FLActionBar;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Lflipboard/gui/actionbar/FLActionBar;->layout(IIII)V

    .line 52
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 45
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/StandardHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/StandardHeaderView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 46
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/StandardHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/StandardHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBar;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/section/scrolling/header/StandardHeaderView;->setMeasuredDimension(II)V

    .line 47
    return-void
.end method

.method public setItem(Lflipboard/service/Section;)V
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/StandardHeaderView;->b:Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    new-instance v1, Lflipboard/objs/FeedSectionLink;

    iget-object v2, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    invoke-direct {v1, p1, v2}, Lflipboard/objs/FeedSectionLink;-><init>(Lflipboard/service/Section;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->a(Lflipboard/objs/FeedSectionLink;)V

    .line 67
    return-void
.end method

.method public setShowActionBar(Z)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/StandardHeaderView;->b:Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->a(Z)V

    .line 57
    return-void
.end method
