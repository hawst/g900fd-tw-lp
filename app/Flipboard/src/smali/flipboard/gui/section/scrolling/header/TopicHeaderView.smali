.class public Lflipboard/gui/section/scrolling/header/TopicHeaderView;
.super Lflipboard/gui/FLViewGroup;
.source "TopicHeaderView.java"

# interfaces
.implements Lflipboard/gui/section/scrolling/header/HeaderView;


# instance fields
.field protected a:Lflipboard/gui/actionbar/FLActionBar;

.field protected b:Lflipboard/gui/FLTextView;

.field protected c:Lflipboard/gui/FLLabelTextView;

.field protected d:Landroid/view/View;

.field private final e:Lflipboard/gui/FLSliderHider;

.field private final f:Landroid/graphics/Rect;

.field private g:Lflipboard/gui/section/scrolling/component/ActionBarComponent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;)V

    .line 40
    new-instance v0, Lflipboard/gui/FLSliderHider;

    invoke-direct {v0}, Lflipboard/gui/FLSliderHider;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->e:Lflipboard/gui/FLSliderHider;

    .line 41
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->f:Landroid/graphics/Rect;

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    new-instance v0, Lflipboard/gui/FLSliderHider;

    invoke-direct {v0}, Lflipboard/gui/FLSliderHider;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->e:Lflipboard/gui/FLSliderHider;

    .line 41
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->f:Landroid/graphics/Rect;

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    new-instance v0, Lflipboard/gui/FLSliderHider;

    invoke-direct {v0}, Lflipboard/gui/FLSliderHider;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->e:Lflipboard/gui/FLSliderHider;

    .line 41
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->f:Landroid/graphics/Rect;

    .line 55
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/scrolling/header/TopicHeaderView;)Lflipboard/gui/section/scrolling/component/ActionBarComponent;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->g:Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    return-object v0
.end method

.method static synthetic b(Lflipboard/gui/section/scrolling/header/TopicHeaderView;)Lflipboard/gui/FLSliderHider;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->e:Lflipboard/gui/FLSliderHider;

    return-object v0
.end method

.method static synthetic c(Lflipboard/gui/section/scrolling/header/TopicHeaderView;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->f:Landroid/graphics/Rect;

    return-object v0
.end method


# virtual methods
.method public final a(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->g:Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->a(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)V

    .line 160
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->e:Lflipboard/gui/FLSliderHider;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLSliderHider;->a(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)V

    .line 161
    return-void
.end method

.method public getActionBar()Lflipboard/gui/actionbar/FLActionBar;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 144
    return-object p0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0}, Lflipboard/gui/FLViewGroup;->onFinishInflate()V

    .line 60
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 61
    new-instance v0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-direct {v0, p0, v1}, Lflipboard/gui/section/scrolling/component/ActionBarComponent;-><init>(Landroid/view/View;Lflipboard/gui/actionbar/FLActionBar;)V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->g:Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    .line 63
    new-instance v0, Lflipboard/gui/section/scrolling/header/TopicHeaderView$1;

    invoke-direct {v0, p0}, Lflipboard/gui/section/scrolling/header/TopicHeaderView$1;-><init>(Lflipboard/gui/section/scrolling/header/TopicHeaderView;)V

    invoke-virtual {p0, v0}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->post(Ljava/lang/Runnable;)Z

    .line 73
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 89
    sub-int v0, p5, p3

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 90
    const/4 v1, 0x4

    new-array v1, v1, [Landroid/view/View;

    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    aput-object v2, v1, v6

    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->b:Lflipboard/gui/FLTextView;

    aput-object v2, v1, v5

    const/4 v2, 0x2

    iget-object v3, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c:Lflipboard/gui/FLLabelTextView;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->d:Landroid/view/View;

    aput-object v3, v1, v2

    invoke-static {v1}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a([Landroid/view/View;)I

    move-result v1

    .line 91
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->getPaddingLeft()I

    move-result v2

    .line 92
    sub-int v3, p4, p2

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    .line 93
    sub-int/2addr v0, v1

    .line 94
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->getPaddingTop()I

    move-result v1

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    .line 96
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-static {v1, v0, v2, v3, v5}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a(Landroid/view/View;IIII)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->b:Lflipboard/gui/FLTextView;

    invoke-static {v1, v0, v2, v3, v5}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a(Landroid/view/View;IIII)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c:Lflipboard/gui/FLLabelTextView;

    invoke-static {v1, v0, v2, v3, v5}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a(Landroid/view/View;IIII)I

    .line 101
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->d:Landroid/view/View;

    sub-int v1, p5, p3

    sub-int v2, p4, p2

    invoke-static {v0, v1, v6, v2}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a(Landroid/view/View;III)I

    .line 102
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 77
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 78
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->b:Lflipboard/gui/FLTextView;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 79
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c:Lflipboard/gui/FLLabelTextView;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 80
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->d:Landroid/view/View;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 82
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->b:Lflipboard/gui/FLTextView;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c:Lflipboard/gui/FLLabelTextView;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->d:Landroid/view/View;

    aput-object v2, v0, v1

    invoke-static {v0}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->a([Landroid/view/View;)I

    move-result v0

    .line 83
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    .line 84
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    add-int/2addr v0, v1

    invoke-static {v0, p2}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->resolveSize(II)I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->setMeasuredDimension(II)V

    .line 85
    return-void
.end method

.method public setItem(Lflipboard/service/Section;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 106
    invoke-virtual {p1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v0

    .line 107
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    iget-object v0, p1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    iget-object v2, v0, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    const-string v3, "topic"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/objs/SidebarGroup;->f:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lflipboard/objs/SidebarGroup;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup$Metrics;

    if-eqz v0, :cond_1

    const-string v3, "follower"

    iget-object v4, v0, Lflipboard/objs/SidebarGroup$Metrics;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget v1, v0, Lflipboard/objs/SidebarGroup$Metrics;->c:I

    if-ne v1, v6, :cond_2

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0156

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 109
    :goto_0
    invoke-static {v0}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 110
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c:Lflipboard/gui/FLLabelTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 115
    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->g:Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    new-instance v1, Lflipboard/objs/FeedSectionLink;

    const-string v2, "topic"

    invoke-direct {v1, p1, v2}, Lflipboard/objs/FeedSectionLink;-><init>(Lflipboard/service/Section;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->a(Lflipboard/objs/FeedSectionLink;)V

    .line 116
    return-void

    .line 108
    :cond_2
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0155

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v0, v0, Lflipboard/objs/SidebarGroup$Metrics;->b:Ljava/lang/String;

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 112
    :cond_4
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1, v5}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 113
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public setShowActionBar(Z)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->g:Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->a(Z)V

    .line 150
    return-void
.end method
