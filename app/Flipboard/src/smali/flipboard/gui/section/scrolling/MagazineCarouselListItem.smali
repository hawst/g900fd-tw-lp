.class public Lflipboard/gui/section/scrolling/MagazineCarouselListItem;
.super Landroid/view/ViewGroup;
.source "MagazineCarouselListItem.java"


# instance fields
.field a:Lflipboard/gui/FLImageView;

.field b:Lflipboard/gui/FLStaticTextView;

.field c:Lflipboard/gui/FLStaticTextView;

.field d:Lflipboard/gui/FLLabelTextView;

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 47
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 48
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->e:I

    .line 49
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    .line 80
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 81
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    .line 82
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->getPaddingLeft()I

    move-result v2

    .line 83
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->getPaddingTop()I

    move-result v3

    .line 85
    iget-object v4, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->a:Lflipboard/gui/FLImageView;

    iget-object v5, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v5

    iget-object v6, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v6}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {v4, v2, v3, v5, v6}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 87
    iget-object v4, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v4

    sub-int v4, v0, v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v2

    .line 88
    iget v5, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->e:I

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v5, v3

    .line 89
    iget-object v6, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->b:Lflipboard/gui/FLStaticTextView;

    iget-object v7, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v4

    iget-object v8, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v8}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v5

    invoke-virtual {v6, v4, v5, v7, v8}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 91
    iget-object v4, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v4

    sub-int v4, v0, v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v2

    .line 92
    iget-object v6, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->e:I

    add-int/2addr v5, v6

    .line 93
    iget-object v6, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v7, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v4

    iget-object v8, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v8}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v5

    invoke-virtual {v6, v4, v5, v7, v8}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 95
    iget-object v4, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->d:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v0, v4

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    .line 96
    add-int/2addr v1, v3

    iget-object v2, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->d:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->e:I

    sub-int/2addr v1, v2

    .line 97
    iget-object v2, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->d:Lflipboard/gui/FLLabelTextView;

    iget-object v3, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->d:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->d:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 99
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v7, -0x80000000

    const/4 v6, 0x0

    .line 61
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 62
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 63
    iget v2, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->e:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    .line 64
    iget v3, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->e:I

    .line 66
    iget-object v3, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->a:Lflipboard/gui/FLImageView;

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 68
    iget-object v3, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->b:Lflipboard/gui/FLStaticTextView;

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 70
    iget-object v3, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->c:Lflipboard/gui/FLStaticTextView;

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 72
    iget-object v3, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->d:Lflipboard/gui/FLLabelTextView;

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v3, v2, v4}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 74
    invoke-virtual {p0, v0, v1}, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->setMeasuredDimension(II)V

    .line 75
    return-void
.end method

.method public setItem(Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->a()V

    .line 53
    iget-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 54
    iget-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->b:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v0, p0, Lflipboard/gui/section/scrolling/MagazineCarouselListItem;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    return-void
.end method
