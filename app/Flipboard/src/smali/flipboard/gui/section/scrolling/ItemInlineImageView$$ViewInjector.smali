.class public Lflipboard/gui/section/scrolling/ItemInlineImageView$$ViewInjector;
.super Ljava/lang/Object;
.source "ItemInlineImageView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/scrolling/ItemInlineImageView;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a0091

    const-string v1, "field \'titleView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/ItemInlineImageView;->a:Lflipboard/gui/FLStaticTextView;

    .line 12
    const v0, 0x7f0a0093

    const-string v1, "field \'subtitleView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/ItemInlineImageView;->b:Lflipboard/gui/FLStaticTextView;

    .line 14
    const v0, 0x7f0a020d

    const-string v1, "field \'excerptView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/ItemInlineImageView;->c:Lflipboard/gui/FLStaticTextView;

    .line 16
    const v0, 0x7f0a020c

    const-string v1, "field \'profileBar\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    check-cast v0, Lflipboard/gui/section/scrolling/ItemProfileBar;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/ItemInlineImageView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    .line 18
    const v0, 0x7f0a008e

    const-string v1, "field \'imageView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/ItemInlineImageView;->e:Lflipboard/gui/FLImageView;

    .line 20
    const v0, 0x7f0a0094

    const-string v1, "field \'itemActionBar\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    check-cast v0, Lflipboard/gui/section/scrolling/ItemActionBar;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/ItemInlineImageView;->f:Lflipboard/gui/section/scrolling/ItemActionBar;

    .line 22
    return-void
.end method

.method public static reset(Lflipboard/gui/section/scrolling/ItemInlineImageView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->a:Lflipboard/gui/FLStaticTextView;

    .line 26
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->b:Lflipboard/gui/FLStaticTextView;

    .line 27
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->c:Lflipboard/gui/FLStaticTextView;

    .line 28
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->d:Lflipboard/gui/section/scrolling/ItemProfileBar;

    .line 29
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->e:Lflipboard/gui/FLImageView;

    .line 30
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemInlineImageView;->f:Lflipboard/gui/section/scrolling/ItemActionBar;

    .line 31
    return-void
.end method
