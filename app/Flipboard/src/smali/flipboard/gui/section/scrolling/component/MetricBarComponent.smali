.class public Lflipboard/gui/section/scrolling/component/MetricBarComponent;
.super Ljava/lang/Object;
.source "MetricBarComponent.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final a:Lflipboard/gui/MetricBar;

.field public b:Lflipboard/gui/section/scrolling/component/MetricBarComponent$OnMetricClickListener;

.field public c:Lflipboard/gui/section/scrolling/component/MetricBarComponent$ArticleCountMetricListener;

.field public final d:Lflipboard/service/Flap$CommentaryObserver;


# direct methods
.method public constructor <init>(Lflipboard/gui/MetricBar;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Lflipboard/gui/section/scrolling/component/MetricBarComponent$1;

    invoke-direct {v0, p0}, Lflipboard/gui/section/scrolling/component/MetricBarComponent$1;-><init>(Lflipboard/gui/section/scrolling/component/MetricBarComponent;)V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->d:Lflipboard/service/Flap$CommentaryObserver;

    .line 62
    iput-object p1, p0, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->a:Lflipboard/gui/MetricBar;

    .line 63
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/scrolling/component/MetricBarComponent;)Lflipboard/gui/MetricBar;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->a:Lflipboard/gui/MetricBar;

    return-object v0
.end method

.method static synthetic a(Lflipboard/gui/section/scrolling/component/MetricBarComponent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0, p1, p2, p3}, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lflipboard/gui/section/scrolling/component/MetricBarComponent;)Lflipboard/gui/section/scrolling/component/MetricBarComponent$ArticleCountMetricListener;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->c:Lflipboard/gui/section/scrolling/component/MetricBarComponent$ArticleCountMetricListener;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 70
    iget-object v1, p0, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->a:Lflipboard/gui/MetricBar;

    invoke-virtual {p2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v1, Lflipboard/gui/MetricBar;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/MetricBar$MetricItemViewHolder;

    if-eqz v0, :cond_0

    invoke-static {v0, v2, p3}, Lflipboard/gui/MetricBar$MetricItemViewHolder;->a(Lflipboard/gui/MetricBar$MetricItemViewHolder;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-static {v0, p0}, Lflipboard/gui/MetricBar$MetricItemViewHolder;->a(Lflipboard/gui/MetricBar$MetricItemViewHolder;Landroid/view/View$OnClickListener;)V

    .line 71
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-virtual {v1}, Lflipboard/gui/MetricBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f0300da

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    new-instance v3, Lflipboard/gui/MetricBar$MetricItemViewHolder;

    const/4 v4, 0x0

    invoke-direct {v3, v1, p1, v0, v4}, Lflipboard/gui/MetricBar$MetricItemViewHolder;-><init>(Lflipboard/gui/MetricBar;Ljava/lang/String;Landroid/view/ViewGroup;B)V

    iget-object v4, v1, Lflipboard/gui/MetricBar;->f:Ljava/lang/String;

    iget-object v5, v1, Lflipboard/gui/MetricBar;->g:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lflipboard/gui/MetricBar$MetricItemViewHolder;->a(Lflipboard/gui/MetricBar$MetricItemViewHolder;Ljava/lang/String;Ljava/lang/String;)V

    iget v4, v1, Lflipboard/gui/MetricBar;->d:I

    iget v5, v1, Lflipboard/gui/MetricBar;->e:I

    invoke-static {v3, v4, v5}, Lflipboard/gui/MetricBar$MetricItemViewHolder;->b(Lflipboard/gui/MetricBar$MetricItemViewHolder;II)V

    iget v4, v1, Lflipboard/gui/MetricBar;->b:I

    iget v5, v1, Lflipboard/gui/MetricBar;->c:I

    invoke-static {v3, v4, v5}, Lflipboard/gui/MetricBar$MetricItemViewHolder;->a(Lflipboard/gui/MetricBar$MetricItemViewHolder;II)V

    invoke-static {v3, v2, p3}, Lflipboard/gui/MetricBar$MetricItemViewHolder;->a(Lflipboard/gui/MetricBar$MetricItemViewHolder;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-static {v3, p0}, Lflipboard/gui/MetricBar$MetricItemViewHolder;->a(Lflipboard/gui/MetricBar$MetricItemViewHolder;Landroid/view/View$OnClickListener;)V

    iget-object v2, v1, Lflipboard/gui/MetricBar;->a:Ljava/util/Map;

    invoke-interface {v2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1, v0}, Lflipboard/gui/MetricBar;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->b:Lflipboard/gui/section/scrolling/component/MetricBarComponent$OnMetricClickListener;

    if-eqz v0, :cond_0

    .line 80
    iget-object v1, p0, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->b:Lflipboard/gui/section/scrolling/component/MetricBarComponent$OnMetricClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Lflipboard/gui/section/scrolling/component/MetricBarComponent$OnMetricClickListener;->a(Ljava/lang/String;)V

    .line 82
    :cond_0
    return-void
.end method
