.class public Lflipboard/gui/section/scrolling/component/CarouselComponent;
.super Ljava/lang/Object;
.source "CarouselComponent.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/support/v4/view/ViewPager$OnPageChangeListener;"
    }
.end annotation


# instance fields
.field public final a:Lflipboard/gui/CarouselView;

.field public final b:Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/gui/section/scrolling/component/CarouselComponent",
            "<TT;>.CarouselAdapter;"
        }
    .end annotation
.end field

.field public c:Lflipboard/gui/section/scrolling/component/CarouselComponent$OnPageSelectedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/gui/section/scrolling/component/CarouselComponent$OnPageSelectedListener",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lflipboard/gui/CarouselView;Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/gui/CarouselView;",
            "Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent;->a:Lflipboard/gui/CarouselView;

    .line 20
    new-instance v0, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;

    invoke-direct {v0, p0, p2}, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;-><init>(Lflipboard/gui/section/scrolling/component/CarouselComponent;Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter;)V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent;->b:Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;

    .line 21
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent;->b:Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;

    invoke-virtual {p1, v0}, Lflipboard/gui/CarouselView;->setAdapter(Lflipboard/gui/CarouselView$CarouselAdapter;)V

    .line 22
    invoke-virtual {p1, p0}, Lflipboard/gui/CarouselView;->setOnPageChangedListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 23
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent;->c:Lflipboard/gui/section/scrolling/component/CarouselComponent$OnPageSelectedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent;->b:Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;

    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;->c()I

    move-result v0

    if-lez v0, :cond_0

    .line 51
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent;->b:Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent;->c:Lflipboard/gui/section/scrolling/component/CarouselComponent$OnPageSelectedListener;

    invoke-interface {v1, p1, v0}, Lflipboard/gui/section/scrolling/component/CarouselComponent$OnPageSelectedListener;->a(ILjava/lang/Object;)V

    .line 54
    :cond_0
    return-void
.end method

.method public final a(IFI)V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent;->c:Lflipboard/gui/section/scrolling/component/CarouselComponent$OnPageSelectedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent;->b:Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;

    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;->c()I

    move-result v0

    if-lez v0, :cond_0

    .line 43
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent;->b:Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;->a(I)Ljava/lang/Object;

    .line 44
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent;->c:Lflipboard/gui/section/scrolling/component/CarouselComponent$OnPageSelectedListener;

    .line 46
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/CarouselComponent;->b:Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;

    iget-object v1, v0, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, v0, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, v0, Landroid/support/v4/view/PagerAdapter;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v1}, Landroid/database/DataSetObservable;->notifyChanged()V

    iget-object v1, v0, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;->c:Lflipboard/gui/section/scrolling/component/CarouselComponent;

    iget-object v1, v1, Lflipboard/gui/section/scrolling/component/CarouselComponent;->a:Lflipboard/gui/CarouselView;

    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;->c()I

    move-result v0

    invoke-virtual {v1, v0}, Lflipboard/gui/CarouselView;->setIndicatorCount(I)V

    .line 29
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/scrolling/component/CarouselComponent;->a(I)V

    .line 30
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 58
    return-void
.end method
