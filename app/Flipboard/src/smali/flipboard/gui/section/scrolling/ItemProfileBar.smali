.class public Lflipboard/gui/section/scrolling/ItemProfileBar;
.super Landroid/view/ViewGroup;
.source "ItemProfileBar.java"

# interfaces
.implements Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;


# static fields
.field static final o:Lbutterknife/ButterKnife$Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbutterknife/ButterKnife$Action",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field static final p:Lbutterknife/ButterKnife$Setter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbutterknife/ButterKnife$Setter",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final q:Lbutterknife/ButterKnife$Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbutterknife/ButterKnife$Action",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final a:Lflipboard/activities/FlipboardActivity;

.field b:Lflipboard/service/Section;

.field c:Lflipboard/objs/FeedItem;

.field d:Lflipboard/objs/FeedSectionLink;

.field e:Landroid/os/Bundle;

.field f:Lflipboard/gui/FLStaticTextView;

.field g:Lflipboard/gui/FLLabelTextView;

.field h:Lflipboard/gui/FLTextView;

.field i:Lflipboard/gui/FLImageView;

.field j:Lflipboard/gui/FLImageView;

.field k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/section/AttributionButtonWithText;",
            ">;"
        }
    .end annotation
.end field

.field l:Lflipboard/gui/section/AttributionButtonWithText;

.field m:Lflipboard/gui/section/AttributionButtonWithText;

.field n:Lflipboard/gui/section/AttributionButtonWithText;

.field private r:Z

.field private s:Z

.field private final t:I

.field private final u:I

.field private v:Lflipboard/objs/FeedItem;

.field private w:Lflipboard/objs/ConfigService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lflipboard/gui/section/scrolling/ItemProfileBar$1;

    invoke-direct {v0}, Lflipboard/gui/section/scrolling/ItemProfileBar$1;-><init>()V

    sput-object v0, Lflipboard/gui/section/scrolling/ItemProfileBar;->o:Lbutterknife/ButterKnife$Action;

    .line 91
    new-instance v0, Lflipboard/gui/section/scrolling/ItemProfileBar$2;

    invoke-direct {v0}, Lflipboard/gui/section/scrolling/ItemProfileBar$2;-><init>()V

    sput-object v0, Lflipboard/gui/section/scrolling/ItemProfileBar;->p:Lbutterknife/ButterKnife$Setter;

    .line 99
    new-instance v0, Lflipboard/gui/section/scrolling/ItemProfileBar$3;

    invoke-direct {v0}, Lflipboard/gui/section/scrolling/ItemProfileBar$3;-><init>()V

    sput-object v0, Lflipboard/gui/section/scrolling/ItemProfileBar;->q:Lbutterknife/ButterKnife$Action;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 107
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lflipboard/gui/section/scrolling/ItemProfileBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lflipboard/gui/section/scrolling/ItemProfileBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 113
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 116
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 117
    check-cast p1, Lflipboard/activities/FlipboardActivity;

    iput-object p1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->a:Lflipboard/activities/FlipboardActivity;

    .line 118
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090110

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->t:I

    .line 119
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090094

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->u:I

    .line 121
    new-instance v0, Lflipboard/gui/section/scrolling/ItemProfileBar$4;

    invoke-direct {v0, p0}, Lflipboard/gui/section/scrolling/ItemProfileBar$4;-><init>(Lflipboard/gui/section/scrolling/ItemProfileBar;)V

    invoke-virtual {p0, v0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    return-void
.end method

.method private static a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 204
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 205
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Landroid/view/View;->measure(II)V

    .line 206
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/scrolling/ItemProfileBar;Lflipboard/objs/CommentaryResult$Item;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lflipboard/gui/section/scrolling/ItemProfileBar;->a(Lflipboard/objs/CommentaryResult$Item;)V

    return-void
.end method

.method private a(Lflipboard/objs/CommentaryResult$Item;)V
    .locals 3

    .prologue
    .line 440
    if-eqz p1, :cond_1

    .line 441
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->m:Lflipboard/gui/section/AttributionButtonWithText;

    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->b:I

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionButtonWithText;->setNumber(I)V

    .line 442
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->l:Lflipboard/gui/section/AttributionButtonWithText;

    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->c:I

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionButtonWithText;->setNumber(I)V

    .line 443
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->n:Lflipboard/gui/section/AttributionButtonWithText;

    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->d:I

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionButtonWithText;->setNumber(I)V

    .line 450
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->a()V

    .line 451
    return-void

    .line 445
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    .line 446
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lflipboard/gui/section/AttributionButtonWithText;->setNumber(I)V

    goto :goto_0
.end method

.method private c()V
    .locals 5

    .prologue
    const v2, 0x7f080073

    const v1, 0x7f0800aa

    .line 327
    iget-object v3, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->f:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->r:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    .line 328
    iget-object v3, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->r:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    .line 329
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->g:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-boolean v4, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->r:Z

    if-eqz v4, :cond_2

    :goto_2
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setTextColor(I)V

    .line 330
    return-void

    .line 327
    :cond_0
    const v0, 0x7f080008

    goto :goto_0

    :cond_1
    move v0, v2

    .line 328
    goto :goto_1

    :cond_2
    move v1, v2

    .line 329
    goto :goto_2
.end method


# virtual methods
.method final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 422
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->w:Lflipboard/objs/ConfigService;

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->l:Lflipboard/gui/section/AttributionButtonWithText;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->w:Lflipboard/objs/ConfigService;

    sget-object v2, Lflipboard/gui/section/AttributionButtonWithText$Type;->b:Lflipboard/gui/section/AttributionButtonWithText$Type;

    iget-object v3, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->c:Lflipboard/objs/FeedItem;

    iget-boolean v3, v3, Lflipboard/objs/FeedItem;->af:Z

    iget-boolean v4, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->r:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lflipboard/gui/section/AttributionButtonWithText;->a(Lflipboard/objs/ConfigService;Lflipboard/gui/section/AttributionButtonWithText$Type;ZZ)V

    .line 424
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->m:Lflipboard/gui/section/AttributionButtonWithText;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->w:Lflipboard/objs/ConfigService;

    sget-object v2, Lflipboard/gui/section/AttributionButtonWithText$Type;->a:Lflipboard/gui/section/AttributionButtonWithText$Type;

    iget-boolean v3, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->r:Z

    invoke-virtual {v0, v1, v2, v5, v3}, Lflipboard/gui/section/AttributionButtonWithText;->a(Lflipboard/objs/ConfigService;Lflipboard/gui/section/AttributionButtonWithText$Type;ZZ)V

    .line 425
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->n:Lflipboard/gui/section/AttributionButtonWithText;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->w:Lflipboard/objs/ConfigService;

    sget-object v2, Lflipboard/gui/section/AttributionButtonWithText$Type;->c:Lflipboard/gui/section/AttributionButtonWithText$Type;

    iget-boolean v3, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->r:Z

    invoke-virtual {v0, v1, v2, v5, v3}, Lflipboard/gui/section/AttributionButtonWithText;->a(Lflipboard/objs/ConfigService;Lflipboard/gui/section/AttributionButtonWithText$Type;ZZ)V

    .line 427
    :cond_0
    return-void
.end method

.method public final a(Lflipboard/objs/HasCommentaryItem;)V
    .locals 2

    .prologue
    .line 431
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/scrolling/ItemProfileBar$5;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/section/scrolling/ItemProfileBar$5;-><init>(Lflipboard/gui/section/scrolling/ItemProfileBar;Lflipboard/objs/HasCommentaryItem;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    .line 437
    return-void
.end method

.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v8, 0x8

    .line 279
    iput-object p1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->b:Lflipboard/service/Section;

    .line 280
    iput-object p2, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->c:Lflipboard/objs/FeedItem;

    .line 281
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    .line 282
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v3, "flipboard"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 283
    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->B()Lflipboard/objs/FeedSectionLink;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->d:Lflipboard/objs/FeedSectionLink;

    .line 284
    invoke-virtual {p0, p2}, Lflipboard/gui/section/scrolling/ItemProfileBar;->setTag(Ljava/lang/Object;)V

    .line 286
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    if-ne p2, v0, :cond_3

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->V()Z

    move-result v0

    if-nez v0, :cond_3

    .line 289
    invoke-virtual {p0, v8}, Lflipboard/gui/section/scrolling/ItemProfileBar;->setVisibility(I)V

    .line 313
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 282
    goto :goto_0

    .line 283
    :cond_2
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->B()Lflipboard/objs/FeedSectionLink;

    move-result-object v0

    goto :goto_1

    .line 292
    :cond_3
    invoke-virtual {p0, v2}, Lflipboard/gui/section/scrolling/ItemProfileBar;->setVisibility(I)V

    .line 296
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->e:Landroid/os/Bundle;

    .line 297
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->e:Landroid/os/Bundle;

    const-string v3, "source"

    const-string v4, "sectionLink"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->e:Landroid/os/Bundle;

    const-string v3, "originSectionIdentifier"

    iget-object v4, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->e:Landroid/os/Bundle;

    const-string v3, "linkType"

    const-string v4, "magazine"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->ai()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v3, "flipboard"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->c:Lflipboard/objs/FeedItem;

    iget-object v4, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    if-ne v3, v4, :cond_6

    :cond_4
    iput-boolean v2, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->s:Z

    const-string v0, "flipboard"

    :goto_3
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->w:Lflipboard/objs/ConfigService;

    .line 302
    invoke-direct {p0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->c()V

    .line 303
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->c:Lflipboard/objs/FeedItem;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    if-ne v0, v1, :cond_8

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->d:Lflipboard/objs/FeedSectionLink;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->f:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->d:Lflipboard/objs/FeedSectionLink;

    iget-object v1, v1, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->d:Lflipboard/objs/FeedSectionLink;

    iget-object v0, v0, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->i:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->d:Lflipboard/objs/FeedSectionLink;

    iget-object v1, v1, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    :goto_4
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v8}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 304
    :goto_5
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->D()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->s:Z

    if-nez v1, :cond_c

    if-eqz v0, :cond_c

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_c

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v1}, Lflipboard/activities/FlipboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    iget-object v4, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->e:Landroid/os/Bundle;

    invoke-static {v1, v0, v3, v4}, Lflipboard/util/FLTextUtil;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Landroid/os/Bundle;)Landroid/text/SpannableString;

    move-result-object v1

    if-nez v1, :cond_b

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    :goto_6
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 305
    :goto_7
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->c:Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->bk:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->g:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0, v8}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 306
    :goto_8
    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->s:Z

    if-eqz v0, :cond_12

    .line 307
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->k:Ljava/util/List;

    sget-object v1, Lflipboard/gui/section/scrolling/ItemProfileBar;->q:Lbutterknife/ButterKnife$Action;

    invoke-static {v0, v1}, Lbutterknife/ButterKnife;->a(Ljava/util/List;Lbutterknife/ButterKnife$Action;)V

    .line 308
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->ad:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->l:Lflipboard/gui/section/AttributionButtonWithText;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionButtonWithText;->setTag(Ljava/lang/Object;)V

    :goto_9
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->ag:Z

    if-eqz v0, :cond_f

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->m:Lflipboard/gui/section/AttributionButtonWithText;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->c:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionButtonWithText;->setTag(Ljava/lang/Object;)V

    :goto_a
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->w:Lflipboard/objs/ConfigService;

    invoke-virtual {v0, v1}, Lflipboard/objs/FeedItem;->b(Lflipboard/objs/ConfigService;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->n:Lflipboard/gui/section/AttributionButtonWithText;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->c:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionButtonWithText;->setTag(Ljava/lang/Object;)V

    :goto_b
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->ag:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->ad:Z

    if-eqz v0, :cond_11

    :cond_5
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    invoke-direct {p0, v0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->a(Lflipboard/objs/CommentaryResult$Item;)V

    goto/16 :goto_2

    .line 301
    :cond_6
    iput-boolean v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->s:Z

    goto/16 :goto_3

    .line 303
    :cond_7
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v8}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto/16 :goto_4

    :cond_8
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->f:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->i:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v1}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    :goto_c
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->w:Lflipboard/objs/ConfigService;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->w:Lflipboard/objs/ConfigService;

    iget-object v0, v0, Lflipboard/objs/ConfigService;->m:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->j:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->w:Lflipboard/objs/ConfigService;

    iget-object v1, v1, Lflipboard/objs/ConfigService;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_9
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->i:Lflipboard/gui/FLImageView;

    const v1, 0x7f02005a

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    goto :goto_c

    :cond_a
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v8}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 304
    :cond_b
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->h:Lflipboard/gui/FLTextView;

    invoke-static {v0}, Lflipboard/util/FLTextUtil;->a(Landroid/widget/TextView;)V

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->h:Lflipboard/gui/FLTextView;

    invoke-static {v0}, Lflipboard/util/FLTextUtil;->b(Landroid/widget/TextView;)V

    goto/16 :goto_6

    :cond_c
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v8}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    goto/16 :goto_7

    .line 305
    :cond_d
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->c:Lflipboard/objs/FeedItem;

    iget-wide v4, v1, Lflipboard/objs/FeedItem;->Z:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-static {v0, v4, v5}, Lflipboard/util/JavaUtil;->c(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->g:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->g:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    goto/16 :goto_8

    .line 308
    :cond_e
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->l:Lflipboard/gui/section/AttributionButtonWithText;

    invoke-virtual {v0, v8}, Lflipboard/gui/section/AttributionButtonWithText;->setVisibility(I)V

    goto/16 :goto_9

    :cond_f
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->m:Lflipboard/gui/section/AttributionButtonWithText;

    invoke-virtual {v0, v8}, Lflipboard/gui/section/AttributionButtonWithText;->setVisibility(I)V

    goto/16 :goto_a

    :cond_10
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->n:Lflipboard/gui/section/AttributionButtonWithText;

    invoke-virtual {v0, v8}, Lflipboard/gui/section/AttributionButtonWithText;->setVisibility(I)V

    goto/16 :goto_b

    :cond_11
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->a()V

    goto/16 :goto_2

    .line 310
    :cond_12
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->k:Ljava/util/List;

    sget-object v1, Lflipboard/gui/section/scrolling/ItemProfileBar;->o:Lbutterknife/ButterKnife$Action;

    invoke-static {v0, v1}, Lbutterknife/ButterKnife;->a(Ljava/util/List;Lbutterknife/ButterKnife$Action;)V

    goto/16 :goto_2
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 464
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->v:Lflipboard/objs/FeedItem;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->b:Lflipboard/service/Section;

    iget-object v2, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->a:Lflipboard/activities/FlipboardActivity;

    sget-object v3, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->b:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    invoke-static {v0, v1, v2, v3}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/UsageEventV2$SocialCardNavFrom;)V

    .line 465
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 142
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 143
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->c:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->c:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedItem;->a(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 146
    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 150
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 151
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->c:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->c:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedItem;->b(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 154
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 131
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 132
    invoke-static {p0, p0}, Lbutterknife/ButterKnife;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 135
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->k:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->l:Lflipboard/gui/section/AttributionButtonWithText;

    .line 136
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->k:Ljava/util/List;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->m:Lflipboard/gui/section/AttributionButtonWithText;

    .line 137
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->k:Ljava/util/List;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->n:Lflipboard/gui/section/AttributionButtonWithText;

    .line 138
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v3, 0x0

    .line 211
    sub-int v1, p4, p2

    .line 215
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 216
    iget-object v2, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    .line 217
    iget-object v4, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v4, v3, v3, v0, v2}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 219
    iget v2, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->t:I

    add-int v4, v0, v2

    .line 220
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->f:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v4

    .line 221
    iget-object v2, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->f:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    .line 222
    iget-object v5, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->f:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v5, v4, v3, v0, v2}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 224
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->g:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v1, v0

    .line 225
    iget-object v5, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->g:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v5

    if-eq v5, v9, :cond_0

    .line 226
    iget-object v5, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->g:Lflipboard/gui/FLLabelTextView;

    iget-object v6, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->g:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v0

    iget-object v7, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->g:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v7

    add-int/lit8 v7, v7, 0x0

    invoke-virtual {v5, v0, v3, v6, v7}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 229
    :cond_0
    iget-object v5, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->g:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v5

    if-eq v5, v9, :cond_1

    .line 230
    :goto_0
    iget v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->t:I

    sub-int/2addr v0, v1

    .line 231
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v0, v1

    .line 232
    iget-object v5, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->f:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v5

    iget-object v6, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v6}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    .line 233
    iget-object v6, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->j:Lflipboard/gui/FLImageView;

    add-int/lit8 v7, v5, 0x0

    add-int/lit8 v5, v5, 0x0

    iget-object v8, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v8}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v5, v8

    invoke-virtual {v6, v1, v7, v0, v5}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 241
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v0

    if-eq v0, v9, :cond_5

    .line 242
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v2

    .line 243
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->h:Lflipboard/gui/FLTextView;

    iget-object v5, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v4

    invoke-virtual {v1, v4, v2, v5, v0}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 246
    :goto_1
    iget v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->t:I

    add-int v5, v0, v1

    .line 247
    iget-object v2, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v3

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionButtonWithText;->getMeasuredHeight()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    goto :goto_2

    :cond_1
    move v0, v1

    .line 229
    goto :goto_0

    .line 247
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v4

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionButtonWithText;->getVisibility()I

    move-result v4

    if-eq v4, v9, :cond_4

    div-int/lit8 v4, v1, 0x2

    add-int/2addr v4, v5

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionButtonWithText;->getMeasuredHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v4, v6

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionButtonWithText;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v2

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionButtonWithText;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v4

    invoke-virtual {v0, v2, v4, v6, v7}, Lflipboard/gui/section/AttributionButtonWithText;->layout(IIII)V

    iget v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->u:I

    add-int/2addr v0, v6

    :goto_4
    move v2, v0

    goto :goto_3

    .line 248
    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/high16 v8, -0x80000000

    const/16 v7, 0x8

    const/4 v2, 0x0

    .line 158
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 159
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 161
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v0

    if-eq v0, v7, :cond_0

    .line 162
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->i:Lflipboard/gui/FLImageView;

    invoke-static {v0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->a(Landroid/view/View;)V

    .line 166
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v0

    if-eq v0, v7, :cond_1

    .line 167
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->j:Lflipboard/gui/FLImageView;

    invoke-static {v0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->a(Landroid/view/View;)V

    .line 172
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->g:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v0

    if-eq v0, v7, :cond_2

    .line 173
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->g:Lflipboard/gui/FLLabelTextView;

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v4, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v0, v1, v5}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 174
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->g:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v1

    .line 175
    iget v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->t:I

    mul-int/lit8 v0, v0, 0x3

    .line 181
    :goto_0
    iget-object v5, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v5

    sub-int v5, v3, v5

    iget-object v6, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v6}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v6

    sub-int/2addr v5, v6

    sub-int v1, v5, v1

    sub-int v0, v1, v0

    .line 182
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->f:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v4, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v1, v0, v5}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 183
    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->f:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v0

    .line 187
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v1

    if-eq v1, v7, :cond_3

    .line 188
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->h:Lflipboard/gui/FLTextView;

    iget-object v5, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v5

    sub-int v5, v3, v5

    iget v6, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->t:I

    sub-int/2addr v5, v6

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v4, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v1, v5, v4}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 189
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    move v1, v0

    .line 193
    :goto_1
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f090092

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 194
    iget-object v4, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->k:Ljava/util/List;

    sget-object v5, Lflipboard/gui/section/scrolling/ItemProfileBar;->p:Lbutterknife/ButterKnife$Setter;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v4, v5, v0}, Lbutterknife/ButterKnife;->a(Ljava/util/List;Lbutterknife/ButterKnife$Setter;Ljava/lang/Object;)V

    .line 195
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/Integer;

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionButtonWithText;->getMeasuredHeight()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v2

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->k:Ljava/util/List;

    .line 196
    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionButtonWithText;->getMeasuredHeight()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v9

    iget-object v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->k:Ljava/util/List;

    .line 197
    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionButtonWithText;->getMeasuredHeight()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v10

    .line 195
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v1

    .line 199
    iget-object v1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 200
    invoke-virtual {p0, v3, v0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->setMeasuredDimension(II)V

    .line 201
    return-void

    .line 178
    :cond_2
    iget v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->t:I

    mul-int/lit8 v0, v0, 0x2

    move v1, v2

    goto/16 :goto_0

    :cond_3
    move v1, v0

    goto :goto_1
.end method

.method public setInverted(Z)V
    .locals 1

    .prologue
    .line 271
    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->r:Z

    if-eq p1, v0, :cond_0

    .line 272
    iput-boolean p1, p0, Lflipboard/gui/section/scrolling/ItemProfileBar;->r:Z

    .line 273
    invoke-direct {p0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->c()V

    .line 274
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/ItemProfileBar;->a()V

    .line 276
    :cond_0
    return-void
.end method
