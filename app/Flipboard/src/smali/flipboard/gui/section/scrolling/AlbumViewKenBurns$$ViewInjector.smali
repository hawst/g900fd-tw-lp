.class public Lflipboard/gui/section/scrolling/AlbumViewKenBurns$$ViewInjector;
.super Ljava/lang/Object;
.source "AlbumViewKenBurns$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/scrolling/AlbumViewKenBurns;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a008e

    const-string v1, "field \'kenBurnsImageView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/KenBurnsImageView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->a:Lflipboard/gui/KenBurnsImageView;

    .line 12
    const v0, 0x7f0a0091

    const-string v1, "field \'titleTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->b:Lflipboard/gui/FLTextView;

    .line 14
    const v0, 0x7f0a0093

    const-string v1, "field \'subtitleTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->c:Landroid/widget/TextView;

    .line 16
    const v0, 0x7f0a0094

    const-string v1, "field \'itemActionBar\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    check-cast v0, Lflipboard/gui/section/scrolling/ItemActionBar;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->d:Lflipboard/gui/section/scrolling/ItemActionBar;

    .line 18
    return-void
.end method

.method public static reset(Lflipboard/gui/section/scrolling/AlbumViewKenBurns;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    iput-object v0, p0, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->a:Lflipboard/gui/KenBurnsImageView;

    .line 22
    iput-object v0, p0, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->b:Lflipboard/gui/FLTextView;

    .line 23
    iput-object v0, p0, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->c:Landroid/widget/TextView;

    .line 24
    iput-object v0, p0, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->d:Lflipboard/gui/section/scrolling/ItemActionBar;

    .line 25
    return-void
.end method
