.class public Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;
.super Ljava/lang/Object;
.source "MagazineGridComponent.java"


# instance fields
.field protected a:Lflipboard/gui/FLImageView;

.field protected b:Landroid/widget/TextView;

.field protected c:Landroid/widget/ImageView;

.field protected d:Landroid/widget/TextView;

.field final synthetic e:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;


# direct methods
.method protected constructor <init>(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->e:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 177
    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->a:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->e:Lflipboard/gui/section/scrolling/component/MagazineGridComponent;

    invoke-static {v1}, Lflipboard/gui/section/scrolling/component/MagazineGridComponent;->a(Lflipboard/gui/section/scrolling/component/MagazineGridComponent;)Lflipboard/activities/FlipboardActivity;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBackgroundColor(I)V

    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->a()V

    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->b:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->c:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/gui/section/scrolling/component/MagazineGridComponent$MagazineTileHolder;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method
