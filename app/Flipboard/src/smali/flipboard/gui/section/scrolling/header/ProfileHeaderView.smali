.class public Lflipboard/gui/section/scrolling/header/ProfileHeaderView;
.super Lflipboard/gui/FLViewGroup;
.source "ProfileHeaderView.java"

# interfaces
.implements Lflipboard/gui/section/scrolling/header/HeaderView;


# instance fields
.field protected a:Lflipboard/gui/actionbar/FLActionBar;

.field protected b:Lflipboard/gui/FLImageView;

.field protected c:Lflipboard/gui/FLTextView;

.field protected d:Lflipboard/gui/FLTextView;

.field protected e:Lflipboard/gui/MetricBar;

.field private final f:Lflipboard/gui/FLSliderHider;

.field private final g:Landroid/graphics/Rect;

.field private h:Lflipboard/gui/section/scrolling/component/ActionBarComponent;

.field private i:Lflipboard/gui/section/scrolling/component/MetricBarComponent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;)V

    .line 44
    new-instance v0, Lflipboard/gui/FLSliderHider;

    invoke-direct {v0}, Lflipboard/gui/FLSliderHider;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->f:Lflipboard/gui/FLSliderHider;

    .line 45
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->g:Landroid/graphics/Rect;

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    new-instance v0, Lflipboard/gui/FLSliderHider;

    invoke-direct {v0}, Lflipboard/gui/FLSliderHider;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->f:Lflipboard/gui/FLSliderHider;

    .line 45
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->g:Landroid/graphics/Rect;

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    new-instance v0, Lflipboard/gui/FLSliderHider;

    invoke-direct {v0}, Lflipboard/gui/FLSliderHider;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->f:Lflipboard/gui/FLSliderHider;

    .line 45
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->g:Landroid/graphics/Rect;

    .line 60
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/scrolling/header/ProfileHeaderView;)Lflipboard/gui/section/scrolling/component/ActionBarComponent;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->h:Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    return-object v0
.end method

.method private static a(Lflipboard/gui/FLTextIntf;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 174
    invoke-static {p1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    const/16 v0, 0x8

    invoke-interface {p0, v0}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 180
    :goto_0
    return-void

    .line 177
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 178
    invoke-interface {p0, p1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic b(Lflipboard/gui/section/scrolling/header/ProfileHeaderView;)Lflipboard/gui/FLSliderHider;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->f:Lflipboard/gui/FLSliderHider;

    return-object v0
.end method

.method static synthetic c(Lflipboard/gui/section/scrolling/header/ProfileHeaderView;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->g:Landroid/graphics/Rect;

    return-object v0
.end method


# virtual methods
.method public final a(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->h:Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->a(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)V

    .line 185
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->f:Lflipboard/gui/FLSliderHider;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLSliderHider;->a(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)V

    .line 186
    return-void
.end method

.method public final a(Lflipboard/service/Account;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 151
    .line 154
    if-eqz p1, :cond_1

    .line 155
    iget-object v0, p1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v0}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v2

    .line 156
    iget-object v0, p1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v0, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    .line 157
    iget-object v0, p1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->s:Ljava/lang/String;

    .line 161
    :goto_0
    if-eqz v2, :cond_0

    .line 162
    iget-object v3, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v3, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 166
    :goto_1
    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    invoke-static {v2, v1}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a(Lflipboard/gui/FLTextIntf;Ljava/lang/CharSequence;)V

    .line 167
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->d:Lflipboard/gui/FLTextView;

    invoke-static {v1, v0}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a(Lflipboard/gui/FLTextIntf;Ljava/lang/CharSequence;)V

    .line 170
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->i:Lflipboard/gui/section/scrolling/component/MetricBarComponent;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "flipboard-_posts_:m:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iget-object v0, v0, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->d:Lflipboard/service/Flap$CommentaryObserver;

    invoke-virtual {v1, v2, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)V

    .line 171
    return-void

    .line 164
    :cond_0
    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    const v3, 0x7f02005a

    invoke-virtual {v2, v3}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    goto :goto_1

    :cond_1
    move-object v1, v0

    move-object v2, v0

    goto :goto_0
.end method

.method public getActionBar()Lflipboard/gui/actionbar/FLActionBar;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 136
    return-object p0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0}, Lflipboard/gui/FLViewGroup;->onFinishInflate()V

    .line 65
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 66
    new-instance v0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-direct {v0, p0, v1}, Lflipboard/gui/section/scrolling/component/ActionBarComponent;-><init>(Landroid/view/View;Lflipboard/gui/actionbar/FLActionBar;)V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->h:Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    .line 67
    new-instance v0, Lflipboard/gui/section/scrolling/component/MetricBarComponent;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->e:Lflipboard/gui/MetricBar;

    invoke-direct {v0, v1}, Lflipboard/gui/section/scrolling/component/MetricBarComponent;-><init>(Lflipboard/gui/MetricBar;)V

    iput-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->i:Lflipboard/gui/section/scrolling/component/MetricBarComponent;

    .line 69
    new-instance v0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$1;

    invoke-direct {v0, p0}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView$1;-><init>(Lflipboard/gui/section/scrolling/header/ProfileHeaderView;)V

    invoke-virtual {p0, v0}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->post(Ljava/lang/Runnable;)Z

    .line 79
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 95
    sub-int v0, p5, p3

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 96
    const/4 v1, 0x4

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    aput-object v3, v1, v2

    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    aput-object v2, v1, v5

    const/4 v2, 0x2

    iget-object v3, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->d:Lflipboard/gui/FLTextView;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->e:Lflipboard/gui/MetricBar;

    aput-object v3, v1, v2

    invoke-static {v1}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a([Landroid/view/View;)I

    move-result v1

    .line 97
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->getPaddingLeft()I

    move-result v2

    .line 98
    sub-int v3, p4, p2

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    .line 99
    sub-int/2addr v0, v1

    .line 100
    div-int/lit8 v0, v0, 0x2

    .line 102
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    invoke-static {v1, v0, v2, v3, v5}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a(Landroid/view/View;IIII)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    invoke-static {v1, v0, v2, v3, v5}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a(Landroid/view/View;IIII)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->d:Lflipboard/gui/FLTextView;

    invoke-static {v1, v0, v2, v3, v5}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a(Landroid/view/View;IIII)I

    move-result v1

    add-int/2addr v0, v1

    .line 105
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->e:Lflipboard/gui/MetricBar;

    invoke-static {v1, v0, v2, v3, v5}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a(Landroid/view/View;IIII)I

    .line 107
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->getPaddingTop()I

    move-result v1

    invoke-static {v0, v1, v2, v3, v5}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a(Landroid/view/View;IIII)I

    .line 108
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 83
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 84
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 85
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 86
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->d:Lflipboard/gui/FLTextView;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 87
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->e:Lflipboard/gui/MetricBar;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 89
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->d:Lflipboard/gui/FLTextView;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->e:Lflipboard/gui/MetricBar;

    aput-object v2, v0, v1

    invoke-static {v0}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a([Landroid/view/View;)I

    move-result v0

    .line 90
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v0, p2}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->resolveSize(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->setMeasuredDimension(II)V

    .line 91
    return-void
.end method

.method public setItem(Lflipboard/service/Section;)V
    .locals 8

    .prologue
    .line 116
    iget-object v0, p1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    .line 117
    if-eqz v0, :cond_0

    .line 118
    iget-object v0, v0, Lflipboard/service/Section$Meta;->i:Lflipboard/objs/FeedSectionLink;

    .line 119
    if-eqz v0, :cond_0

    .line 120
    iget-object v1, v0, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    if-eqz v1, :cond_2

    .line 121
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 125
    :goto_0
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->c:Lflipboard/gui/FLTextView;

    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a(Lflipboard/gui/FLTextIntf;Ljava/lang/CharSequence;)V

    .line 126
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->d:Lflipboard/gui/FLTextView;

    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    invoke-static {v1, v2}, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->a(Lflipboard/gui/FLTextIntf;Ljava/lang/CharSequence;)V

    .line 127
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->h:Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    invoke-virtual {v1, v0}, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->a(Lflipboard/objs/FeedSectionLink;)V

    .line 128
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->h:Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    sget-object v1, Lflipboard/objs/UsageEventV2$FollowFrom;->b:Lflipboard/objs/UsageEventV2$FollowFrom;

    iget-boolean v2, v0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->b:Z

    if-eqz v2, :cond_0

    iget-object v0, v0, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->a:Lflipboard/gui/FollowButton;

    invoke-virtual {v0, v1}, Lflipboard/gui/FollowButton;->setFrom(Lflipboard/objs/UsageEventV2$FollowFrom;)V

    .line 131
    :cond_0
    iget-object v2, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->i:Lflipboard/gui/section/scrolling/component/MetricBarComponent;

    iget-object v0, v2, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->a:Lflipboard/gui/MetricBar;

    invoke-virtual {v0}, Lflipboard/gui/MetricBar;->a()V

    iget-object v0, p1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    const-string v4, "magazines"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->f:Ljava/util/List;

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    iget-object v4, v0, Lflipboard/objs/SidebarGroup;->f:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    const/4 v1, 0x0

    iget-object v0, v0, Lflipboard/objs/SidebarGroup;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup$Metrics;

    iget-object v6, v2, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->c:Lflipboard/gui/section/scrolling/component/MetricBarComponent$ArticleCountMetricListener;

    if-eqz v6, :cond_3

    const-string v6, "articles"

    iget-object v7, v0, Lflipboard/objs/SidebarGroup$Metrics;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, v2, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->c:Lflipboard/gui/section/scrolling/component/MetricBarComponent$ArticleCountMetricListener;

    iget-object v7, v0, Lflipboard/objs/SidebarGroup$Metrics;->a:Ljava/lang/String;

    iget-object v7, v0, Lflipboard/objs/SidebarGroup$Metrics;->d:Ljava/lang/String;

    iget-object v0, v0, Lflipboard/objs/SidebarGroup$Metrics;->b:Ljava/lang/String;

    invoke-interface {v6, v7, v0}, Lflipboard/gui/section/scrolling/component/MetricBarComponent$ArticleCountMetricListener;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 123
    :cond_2
    iget-object v1, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->b:Lflipboard/gui/FLImageView;

    const v2, 0x7f02005a

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    goto/16 :goto_0

    .line 131
    :cond_3
    if-ge v1, v4, :cond_5

    iget-object v6, v0, Lflipboard/objs/SidebarGroup$Metrics;->a:Ljava/lang/String;

    iget-object v7, v0, Lflipboard/objs/SidebarGroup$Metrics;->d:Ljava/lang/String;

    iget-object v0, v0, Lflipboard/objs/SidebarGroup$Metrics;->b:Ljava/lang/String;

    invoke-virtual {v2, v6, v7, v0}, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    goto :goto_1

    .line 132
    :cond_4
    return-void

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public setOnMetricClickListener(Lflipboard/gui/section/scrolling/component/MetricBarComponent$OnMetricClickListener;)V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->i:Lflipboard/gui/section/scrolling/component/MetricBarComponent;

    iput-object p1, v0, Lflipboard/gui/section/scrolling/component/MetricBarComponent;->b:Lflipboard/gui/section/scrolling/component/MetricBarComponent$OnMetricClickListener;

    .line 112
    return-void
.end method

.method public setShowActionBar(Z)V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;->h:Lflipboard/gui/section/scrolling/component/ActionBarComponent;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/scrolling/component/ActionBarComponent;->a(Z)V

    .line 142
    return-void
.end method
