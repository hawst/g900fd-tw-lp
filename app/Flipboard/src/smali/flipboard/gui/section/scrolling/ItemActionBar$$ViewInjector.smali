.class public Lflipboard/gui/section/scrolling/ItemActionBar$$ViewInjector;
.super Ljava/lang/Object;
.source "ItemActionBar$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/scrolling/ItemActionBar;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a01ec

    const-string v1, "field \'flipButton\' and method \'onFlipButtonClick\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 11
    check-cast v0, Lflipboard/gui/FLCameleonImageView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/ItemActionBar;->c:Lflipboard/gui/FLCameleonImageView;

    .line 12
    new-instance v0, Lflipboard/gui/section/scrolling/ItemActionBar$$ViewInjector$1;

    invoke-direct {v0, p1}, Lflipboard/gui/section/scrolling/ItemActionBar$$ViewInjector$1;-><init>(Lflipboard/gui/section/scrolling/ItemActionBar;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 20
    const v0, 0x7f0a01ed

    const-string v1, "field \'likeButton\' and method \'onSocialLikeClick\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 21
    check-cast v0, Lflipboard/gui/FLCameleonImageView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/ItemActionBar;->d:Lflipboard/gui/FLCameleonImageView;

    .line 22
    new-instance v0, Lflipboard/gui/section/scrolling/ItemActionBar$$ViewInjector$2;

    invoke-direct {v0, p1}, Lflipboard/gui/section/scrolling/ItemActionBar$$ViewInjector$2;-><init>(Lflipboard/gui/section/scrolling/ItemActionBar;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    const v0, 0x7f0a01ee

    const-string v1, "field \'commentButton\' and method \'onSocialCommentClick\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 31
    check-cast v0, Lflipboard/gui/FLCameleonImageView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/ItemActionBar;->e:Lflipboard/gui/FLCameleonImageView;

    .line 32
    new-instance v0, Lflipboard/gui/section/scrolling/ItemActionBar$$ViewInjector$3;

    invoke-direct {v0, p1}, Lflipboard/gui/section/scrolling/ItemActionBar$$ViewInjector$3;-><init>(Lflipboard/gui/section/scrolling/ItemActionBar;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    const v0, 0x7f0a01eb

    const-string v1, "field \'relatedTopic\' and method \'onTopicTagClick\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 41
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/ItemActionBar;->f:Lflipboard/gui/FLTextView;

    .line 42
    new-instance v0, Lflipboard/gui/section/scrolling/ItemActionBar$$ViewInjector$4;

    invoke-direct {v0, p1}, Lflipboard/gui/section/scrolling/ItemActionBar$$ViewInjector$4;-><init>(Lflipboard/gui/section/scrolling/ItemActionBar;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    const v0, 0x7f0a01ef

    const-string v1, "field \'activityText\' and method \'onSocialCommentClick\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 51
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/ItemActionBar;->g:Lflipboard/gui/FLTextView;

    .line 52
    new-instance v0, Lflipboard/gui/section/scrolling/ItemActionBar$$ViewInjector$5;

    invoke-direct {v0, p1}, Lflipboard/gui/section/scrolling/ItemActionBar$$ViewInjector$5;-><init>(Lflipboard/gui/section/scrolling/ItemActionBar;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    return-void
.end method

.method public static reset(Lflipboard/gui/section/scrolling/ItemActionBar;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 63
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->c:Lflipboard/gui/FLCameleonImageView;

    .line 64
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->d:Lflipboard/gui/FLCameleonImageView;

    .line 65
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->e:Lflipboard/gui/FLCameleonImageView;

    .line 66
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->f:Lflipboard/gui/FLTextView;

    .line 67
    iput-object v0, p0, Lflipboard/gui/section/scrolling/ItemActionBar;->g:Lflipboard/gui/FLTextView;

    .line 68
    return-void
.end method
