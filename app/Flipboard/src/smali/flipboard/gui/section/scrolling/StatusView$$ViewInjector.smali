.class public Lflipboard/gui/section/scrolling/StatusView$$ViewInjector;
.super Ljava/lang/Object;
.source "StatusView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/scrolling/StatusView;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a020c

    const-string v1, "field \'profileBar\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/section/scrolling/ItemProfileBar;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/StatusView;->a:Lflipboard/gui/section/scrolling/ItemProfileBar;

    .line 12
    const v0, 0x7f0a0094

    const-string v1, "field \'itemActionBar\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/section/scrolling/ItemActionBar;

    iput-object v0, p1, Lflipboard/gui/section/scrolling/StatusView;->b:Lflipboard/gui/section/scrolling/ItemActionBar;

    .line 14
    return-void
.end method

.method public static reset(Lflipboard/gui/section/scrolling/StatusView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lflipboard/gui/section/scrolling/StatusView;->a:Lflipboard/gui/section/scrolling/ItemProfileBar;

    .line 18
    iput-object v0, p0, Lflipboard/gui/section/scrolling/StatusView;->b:Lflipboard/gui/section/scrolling/ItemActionBar;

    .line 19
    return-void
.end method
