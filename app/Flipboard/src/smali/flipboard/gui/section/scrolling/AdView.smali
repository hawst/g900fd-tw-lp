.class public Lflipboard/gui/section/scrolling/AdView;
.super Landroid/widget/RelativeLayout;
.source "AdView.java"

# interfaces
.implements Lflipboard/gui/section/scrolling/PhoneItemView;


# static fields
.field private static final j:Landroid/graphics/Paint;


# instance fields
.field a:Lflipboard/gui/FLImageView;

.field b:Lflipboard/gui/item/AdButtonGroup;

.field c:Z

.field public d:Lflipboard/objs/Ad$Asset;

.field e:Lflipboard/objs/FeedItem;

.field public f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/graphics/RectF;",
            "Lflipboard/objs/Ad$HotSpot;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lflipboard/objs/Ad$ButtonInfo;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lflipboard/objs/Ad$VideoInfo;

.field private k:Lflipboard/service/Section;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 36
    sput-object v0, Lflipboard/gui/section/scrolling/AdView;->j:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 37
    sget-object v0, Lflipboard/gui/section/scrolling/AdView;->j:Landroid/graphics/Paint;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 38
    sget-object v0, Lflipboard/gui/section/scrolling/AdView;->j:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 39
    sget-object v0, Lflipboard/gui/section/scrolling/AdView;->j:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    invoke-virtual {p0, v0}, Lflipboard/gui/section/scrolling/AdView;->setClipToPadding(Z)V

    .line 61
    invoke-virtual {p0, v0}, Lflipboard/gui/section/scrolling/AdView;->setWillNotDraw(Z)V

    .line 62
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x3f000000    # 0.5f

    .line 97
    iput-object p2, p0, Lflipboard/gui/section/scrolling/AdView;->e:Lflipboard/objs/FeedItem;

    .line 98
    iput-object p1, p0, Lflipboard/gui/section/scrolling/AdView;->k:Lflipboard/service/Section;

    .line 99
    invoke-virtual {p0, p2}, Lflipboard/gui/section/scrolling/AdView;->setTag(Ljava/lang/Object;)V

    .line 100
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AdView;->d:Lflipboard/objs/Ad$Asset;

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AdView;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v4}, Lflipboard/gui/FLImageView;->setScaling(Z)V

    .line 102
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AdView;->a:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/AdView;->d:Lflipboard/objs/Ad$Asset;

    iget-object v1, v1, Lflipboard/objs/Ad$Asset;->a:Ljava/lang/String;

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v3, v3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLImageView;->a(Ljava/lang/String;Landroid/graphics/PointF;)V

    .line 103
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AdView;->f:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-static {}, Lflipboard/service/FLAdManager;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AdView;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 106
    iget-object v1, p0, Lflipboard/gui/section/scrolling/AdView;->a:Lflipboard/gui/FLImageView;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    sget-object v2, Lflipboard/gui/section/scrolling/AdView;->j:Landroid/graphics/Paint;

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/FLImageView;->a(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 107
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AdView;->g:Lflipboard/objs/Ad$ButtonInfo;

    if-eqz v0, :cond_2

    .line 111
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AdView;->b:Lflipboard/gui/item/AdButtonGroup;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/AdView;->g:Lflipboard/objs/Ad$ButtonInfo;

    iget-object v2, p0, Lflipboard/gui/section/scrolling/AdView;->h:Ljava/util/List;

    invoke-virtual {v0, p1, v1, v2}, Lflipboard/gui/item/AdButtonGroup;->a(Lflipboard/service/Section;Lflipboard/objs/Ad$ButtonInfo;Ljava/util/List;)V

    .line 112
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AdView;->b:Lflipboard/gui/item/AdButtonGroup;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/AdView;->i:Lflipboard/objs/Ad$VideoInfo;

    invoke-virtual {v0, v1}, Lflipboard/gui/item/AdButtonGroup;->setVideoInfo(Lflipboard/objs/Ad$VideoInfo;)V

    .line 113
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AdView;->b:Lflipboard/gui/item/AdButtonGroup;

    invoke-virtual {v0, v4}, Lflipboard/gui/item/AdButtonGroup;->setVisibility(I)V

    .line 118
    :cond_1
    :goto_0
    return-void

    .line 115
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AdView;->b:Lflipboard/gui/item/AdButtonGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/item/AdButtonGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 86
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 87
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 88
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/AdView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 89
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AdView;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/AdView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02021f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 90
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AdView;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLImageView;->setAllowPreloadOnUIThread(Z)V

    .line 91
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AdView;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v3, v3}, Lflipboard/gui/FLImageView;->a(ZI)V

    .line 92
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 127
    .line 128
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    move v0, v3

    .line 161
    :goto_1
    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v3, v2

    :cond_2
    return v3

    .line 130
    :pswitch_1
    iput-boolean v2, p0, Lflipboard/gui/section/scrolling/AdView;->c:Z

    move v0, v3

    .line 131
    goto :goto_1

    .line 133
    :pswitch_2
    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/AdView;->c:Z

    if-eqz v0, :cond_0

    .line 134
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-nez v0, :cond_7

    .line 135
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AdView;->f:Ljava/util/Map;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflipboard/gui/section/scrolling/AdView;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Ad$HotSpot;

    .line 136
    :goto_2
    if-eqz v0, :cond_7

    .line 137
    iget-boolean v1, v0, Lflipboard/objs/Ad$HotSpot;->i:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lflipboard/gui/section/scrolling/AdView;->i:Lflipboard/objs/Ad$VideoInfo;

    if-eqz v1, :cond_6

    .line 138
    iget-object v0, v0, Lflipboard/objs/Ad$HotSpot;->g:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/AdView;->h:Ljava/util/List;

    invoke-static {v0, v1}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 140
    iget-object v0, p0, Lflipboard/gui/section/scrolling/AdView;->i:Lflipboard/objs/Ad$VideoInfo;

    iget-object v0, v0, Lflipboard/objs/Ad$VideoInfo;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 141
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/AdView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/scrolling/AdView;->i:Lflipboard/objs/Ad$VideoInfo;

    invoke-static {v0, v1}, Lflipboard/util/VideoUtil;->a(Landroid/content/Context;Lflipboard/objs/Ad$VideoInfo;)V

    :cond_4
    move v0, v2

    .line 152
    :goto_3
    iput-boolean v3, p0, Lflipboard/gui/section/scrolling/AdView;->c:Z

    goto :goto_1

    .line 135
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 145
    :cond_6
    iget-object v1, v0, Lflipboard/objs/Ad$HotSpot;->f:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 146
    invoke-virtual {p0}, Lflipboard/gui/section/scrolling/AdView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lflipboard/gui/section/scrolling/AdView;->k:Lflipboard/service/Section;

    iget-object v5, v0, Lflipboard/objs/Ad$HotSpot;->f:Ljava/lang/String;

    invoke-static {v1, v4, v5}, Lflipboard/service/FLAdManager;->a(Landroid/content/Context;Lflipboard/service/Section;Ljava/lang/String;)V

    .line 147
    iget-object v0, v0, Lflipboard/objs/Ad$HotSpot;->g:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gui/section/scrolling/AdView;->h:Ljava/util/List;

    invoke-static {v0, v1}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;Ljava/util/List;)V

    move v0, v2

    .line 148
    goto :goto_3

    .line 156
    :pswitch_3
    iget-boolean v0, p0, Lflipboard/gui/section/scrolling/AdView;->c:Z

    if-eqz v0, :cond_0

    .line 157
    iput-boolean v3, p0, Lflipboard/gui/section/scrolling/AdView;->c:Z

    goto/16 :goto_0

    :cond_7
    move v0, v3

    goto :goto_3

    .line 128
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public setMaxHeight(I)V
    .locals 0

    .prologue
    .line 122
    return-void
.end method
