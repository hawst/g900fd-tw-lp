.class Lflipboard/gui/section/SectionFragment$11$1;
.super Ljava/lang/Object;
.source "SectionFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lflipboard/gui/section/SectionFragment$11;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment$11;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1471
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$11$1;->b:Lflipboard/gui/section/SectionFragment$11;

    iput-object p2, p0, Lflipboard/gui/section/SectionFragment$11$1;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1475
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$11$1;->b:Lflipboard/gui/section/SectionFragment$11;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$11;->d:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->e()Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v5

    .line 1476
    if-eqz v5, :cond_2

    move v1, v2

    move v0, v2

    .line 1478
    :goto_0
    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$11$1;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 1479
    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$11$1;->b:Lflipboard/gui/section/SectionFragment$11;

    iget v3, v3, Lflipboard/gui/section/SectionFragment$11;->c:I

    if-ne v1, v3, :cond_3

    move v3, v4

    .line 1482
    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$11$1;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/SectionPage;

    .line 1483
    if-eqz v0, :cond_0

    .line 1484
    instance-of v6, v0, Lflipboard/gui/section/SectionAdPage;

    if-nez v6, :cond_1

    .line 1485
    iget-object v6, p0, Lflipboard/gui/section/SectionFragment$11$1;->b:Lflipboard/gui/section/SectionFragment$11;

    iget-object v6, v6, Lflipboard/gui/section/SectionFragment$11;->d:Lflipboard/gui/section/SectionFragment;

    invoke-static {v6}, Lflipboard/gui/section/SectionFragment;->b(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/SectionScrubber;

    move-result-object v6

    invoke-virtual {v0, v6}, Lflipboard/gui/section/SectionPage;->setScrubber(Lflipboard/gui/section/SectionScrubber;)V

    .line 1489
    :goto_2
    iget-object v6, p0, Lflipboard/gui/section/SectionFragment$11$1;->b:Lflipboard/gui/section/SectionFragment$11;

    iget-object v6, v6, Lflipboard/gui/section/SectionFragment$11;->d:Lflipboard/gui/section/SectionFragment;

    iget-boolean v6, v6, Lflipboard/gui/section/SectionFragment;->C:Z

    invoke-virtual {v0, v6}, Lflipboard/gui/section/SectionPage;->setIsOpenedFromThirdParty(Z)V

    .line 1490
    invoke-virtual {v0}, Lflipboard/gui/section/SectionPage;->f()V

    .line 1491
    invoke-virtual {v0}, Lflipboard/gui/section/SectionPage;->h()V

    .line 1492
    add-int v6, v1, v3

    invoke-virtual {v5, v6, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    move-result-object v6

    .line 1493
    iget-object v0, v0, Lflipboard/gui/section/SectionPage;->w:Lflipboard/gui/section/Group;

    invoke-virtual {v0}, Lflipboard/gui/section/Group;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1494
    iput-boolean v4, v6, Lflipboard/gui/flipping/FlippingContainer;->k:Z

    .line 1478
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v3

    goto :goto_0

    .line 1487
    :cond_1
    sget-object v6, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    goto :goto_2

    .line 1499
    :cond_2
    return-void

    :cond_3
    move v3, v0

    goto :goto_1
.end method
