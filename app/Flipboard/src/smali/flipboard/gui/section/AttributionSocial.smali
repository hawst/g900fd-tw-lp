.class public Lflipboard/gui/section/AttributionSocial;
.super Lflipboard/gui/section/AttributionBase;
.source "AttributionSocial.java"


# instance fields
.field private s:Lflipboard/objs/FeedItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lflipboard/gui/section/AttributionBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    check-cast p1, Lflipboard/activities/FlipboardActivity;

    iput-object p1, p0, Lflipboard/gui/section/AttributionSocial;->a:Lflipboard/activities/FlipboardActivity;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/AttributionSocial;->n:Ljava/util/List;

    .line 49
    new-instance v0, Lflipboard/gui/section/AttributionSocial$1;

    invoke-direct {v0, p0}, Lflipboard/gui/section/AttributionSocial$1;-><init>(Lflipboard/gui/section/AttributionSocial;)V

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionSocial;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/AttributionSocial;)Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    return-object v0
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const v6, 0x7f030020

    const/4 v5, 0x0

    .line 70
    iput-object p2, p0, Lflipboard/gui/section/AttributionSocial;->d:Lflipboard/objs/FeedItem;

    .line 71
    iput-object p1, p0, Lflipboard/gui/section/AttributionSocial;->b:Lflipboard/service/Section;

    .line 72
    invoke-virtual {p0, p2}, Lflipboard/gui/section/AttributionSocial;->setTag(Ljava/lang/Object;)V

    .line 74
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/AttributionSocial;->e:Lflipboard/objs/FeedItem;

    .line 76
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    .line 77
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->ai()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    .line 79
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 80
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v0

    .line 81
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionSocial;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0241

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1}, Lflipboard/objs/ConfigService;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 82
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 83
    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v4, 0x22

    invoke-virtual {v3, v2, v5, v0, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 84
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v2, "googlereader"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    if-eqz v1, :cond_5

    iget-object v0, v1, Lflipboard/objs/ConfigService;->o:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 87
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->j:Lflipboard/gui/FLImageView;

    iget-object v2, v1, Lflipboard/objs/ConfigService;->o:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 92
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->ad:Z

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->a:Lflipboard/activities/FlipboardActivity;

    invoke-static {v0, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    iput-object v0, p0, Lflipboard/gui/section/AttributionSocial;->k:Lflipboard/gui/section/AttributionButtonWithText;

    .line 94
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->k:Lflipboard/gui/section/AttributionButtonWithText;

    const v2, 0x7f0a0003

    invoke-virtual {v0, v2}, Lflipboard/gui/section/AttributionButtonWithText;->setId(I)V

    .line 95
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->k:Lflipboard/gui/section/AttributionButtonWithText;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionSocial;->addView(Landroid/view/View;)V

    .line 96
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->n:Ljava/util/List;

    iget-object v2, p0, Lflipboard/gui/section/AttributionSocial;->k:Lflipboard/gui/section/AttributionButtonWithText;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->k:Lflipboard/gui/section/AttributionButtonWithText;

    iget-object v2, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v2}, Lflipboard/gui/section/AttributionButtonWithText;->setTag(Ljava/lang/Object;)V

    .line 98
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->k:Lflipboard/gui/section/AttributionButtonWithText;

    new-instance v2, Lflipboard/gui/section/AttributionSocial$2;

    invoke-direct {v2, p0, p1}, Lflipboard/gui/section/AttributionSocial$2;-><init>(Lflipboard/gui/section/AttributionSocial;Lflipboard/service/Section;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/section/AttributionButtonWithText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->ag:Z

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->a:Lflipboard/activities/FlipboardActivity;

    invoke-static {v0, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    iput-object v0, p0, Lflipboard/gui/section/AttributionSocial;->l:Lflipboard/gui/section/AttributionButtonWithText;

    .line 109
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->l:Lflipboard/gui/section/AttributionButtonWithText;

    const v2, 0x7f0a0001

    invoke-virtual {v0, v2}, Lflipboard/gui/section/AttributionButtonWithText;->setId(I)V

    .line 110
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->n:Ljava/util/List;

    iget-object v2, p0, Lflipboard/gui/section/AttributionSocial;->l:Lflipboard/gui/section/AttributionButtonWithText;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->l:Lflipboard/gui/section/AttributionButtonWithText;

    invoke-virtual {v0, p2}, Lflipboard/gui/section/AttributionButtonWithText;->setTag(Ljava/lang/Object;)V

    .line 112
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->l:Lflipboard/gui/section/AttributionButtonWithText;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionSocial;->addView(Landroid/view/View;)V

    .line 113
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->l:Lflipboard/gui/section/AttributionButtonWithText;

    new-instance v2, Lflipboard/gui/section/AttributionSocial$3;

    invoke-direct {v2, p0, p1}, Lflipboard/gui/section/AttributionSocial$3;-><init>(Lflipboard/gui/section/AttributionSocial;Lflipboard/service/Section;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/section/AttributionButtonWithText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/objs/FeedItem;->b(Lflipboard/objs/ConfigService;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 121
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->a:Lflipboard/activities/FlipboardActivity;

    invoke-static {v0, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    iput-object v0, p0, Lflipboard/gui/section/AttributionSocial;->m:Lflipboard/gui/section/AttributionButtonWithText;

    .line 122
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->m:Lflipboard/gui/section/AttributionButtonWithText;

    const v1, 0x7f0a0005

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionButtonWithText;->setId(I)V

    .line 123
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->m:Lflipboard/gui/section/AttributionButtonWithText;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionSocial;->addView(Landroid/view/View;)V

    .line 124
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->n:Ljava/util/List;

    iget-object v1, p0, Lflipboard/gui/section/AttributionSocial;->m:Lflipboard/gui/section/AttributionButtonWithText;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->m:Lflipboard/gui/section/AttributionButtonWithText;

    new-instance v1, Lflipboard/gui/section/AttributionSocial$4;

    invoke-direct {v1, p0, p1, p2}, Lflipboard/gui/section/AttributionSocial$4;-><init>(Lflipboard/gui/section/AttributionSocial;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionButtonWithText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->ag:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->ad:Z

    if-eqz v0, :cond_4

    .line 133
    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionSocial;->a(Lflipboard/objs/CommentaryResult$Item;)V

    .line 136
    :cond_4
    iput-boolean v5, p0, Lflipboard/gui/section/AttributionSocial;->r:Z

    .line 137
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionSocial;->c()V

    .line 138
    return-void

    .line 89
    :cond_5
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->j:Lflipboard/gui/FLImageView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 233
    iget-boolean v0, p0, Lflipboard/gui/section/AttributionSocial;->c:Z

    return v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 237
    return-void
.end method

.method protected final c()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 184
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    iget v0, v0, Lflipboard/objs/CommentaryResult$Item;->c:I

    if-lez v0, :cond_8

    move v0, v1

    .line 185
    :goto_0
    iget-object v3, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->ai()Ljava/lang/String;

    move-result-object v3

    .line 186
    if-nez v3, :cond_0

    .line 187
    const-string v3, "flipboard"

    .line 189
    :cond_0
    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v4, v3}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v3

    .line 190
    iget-boolean v4, p0, Lflipboard/gui/section/AttributionSocial;->r:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    iget-boolean v4, v4, Lflipboard/objs/FeedItem;->af:Z

    iget-boolean v5, p0, Lflipboard/gui/section/AttributionSocial;->o:Z

    if-ne v4, v5, :cond_1

    iget-boolean v4, p0, Lflipboard/gui/section/AttributionSocial;->p:Z

    if-ne v4, v0, :cond_1

    iget-boolean v4, p0, Lflipboard/gui/section/AttributionSocial;->q:Z

    iget-boolean v5, p0, Lflipboard/gui/section/AttributionSocial;->c:Z

    if-eq v4, v5, :cond_3

    .line 191
    :cond_1
    iget-object v4, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    iget-boolean v4, v4, Lflipboard/objs/FeedItem;->af:Z

    iput-boolean v4, p0, Lflipboard/gui/section/AttributionSocial;->o:Z

    .line 192
    iget-object v4, p0, Lflipboard/gui/section/AttributionSocial;->k:Lflipboard/gui/section/AttributionButtonWithText;

    if-eqz v4, :cond_2

    .line 193
    iget-object v4, p0, Lflipboard/gui/section/AttributionSocial;->k:Lflipboard/gui/section/AttributionButtonWithText;

    sget-object v5, Lflipboard/gui/section/AttributionButtonWithText$Type;->b:Lflipboard/gui/section/AttributionButtonWithText$Type;

    iget-boolean v6, p0, Lflipboard/gui/section/AttributionSocial;->o:Z

    iget-boolean v7, p0, Lflipboard/gui/section/AttributionSocial;->c:Z

    invoke-virtual {v4, v3, v5, v6, v7}, Lflipboard/gui/section/AttributionButtonWithText;->a(Lflipboard/objs/ConfigService;Lflipboard/gui/section/AttributionButtonWithText$Type;ZZ)V

    .line 195
    :cond_2
    iput-boolean v0, p0, Lflipboard/gui/section/AttributionSocial;->p:Z

    .line 197
    :cond_3
    iget-boolean v0, p0, Lflipboard/gui/section/AttributionSocial;->r:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lflipboard/gui/section/AttributionSocial;->q:Z

    iget-boolean v4, p0, Lflipboard/gui/section/AttributionSocial;->c:Z

    if-eq v0, v4, :cond_7

    .line 198
    :cond_4
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->l:Lflipboard/gui/section/AttributionButtonWithText;

    if-eqz v0, :cond_5

    .line 199
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->l:Lflipboard/gui/section/AttributionButtonWithText;

    sget-object v4, Lflipboard/gui/section/AttributionButtonWithText$Type;->a:Lflipboard/gui/section/AttributionButtonWithText$Type;

    iget-boolean v5, p0, Lflipboard/gui/section/AttributionSocial;->c:Z

    invoke-virtual {v0, v3, v4, v2, v5}, Lflipboard/gui/section/AttributionButtonWithText;->a(Lflipboard/objs/ConfigService;Lflipboard/gui/section/AttributionButtonWithText$Type;ZZ)V

    .line 201
    :cond_5
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->m:Lflipboard/gui/section/AttributionButtonWithText;

    if-eqz v0, :cond_6

    .line 202
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->m:Lflipboard/gui/section/AttributionButtonWithText;

    sget-object v4, Lflipboard/gui/section/AttributionButtonWithText$Type;->c:Lflipboard/gui/section/AttributionButtonWithText$Type;

    iget-boolean v5, p0, Lflipboard/gui/section/AttributionSocial;->c:Z

    invoke-virtual {v0, v3, v4, v2, v5}, Lflipboard/gui/section/AttributionButtonWithText;->a(Lflipboard/objs/ConfigService;Lflipboard/gui/section/AttributionButtonWithText$Type;ZZ)V

    .line 204
    :cond_6
    iget-object v2, p0, Lflipboard/gui/section/AttributionSocial;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/AttributionSocial;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-boolean v0, p0, Lflipboard/gui/section/AttributionSocial;->c:Z

    if-eqz v0, :cond_9

    const v0, 0x7f0800aa

    :goto_1
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    .line 206
    :cond_7
    iput-boolean v1, p0, Lflipboard/gui/section/AttributionSocial;->r:Z

    .line 207
    iget-boolean v0, p0, Lflipboard/gui/section/AttributionSocial;->c:Z

    iput-boolean v0, p0, Lflipboard/gui/section/AttributionSocial;->q:Z

    .line 208
    return-void

    :cond_8
    move v0, v2

    .line 184
    goto :goto_0

    .line 204
    :cond_9
    const v0, 0x7f08009b

    goto :goto_1
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Lflipboard/gui/section/AttributionBase;->onFinishInflate()V

    .line 61
    const v0, 0x7f0a007d

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionSocial;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionSocial;->h:Lflipboard/gui/FLTextView;

    .line 62
    const v0, 0x7f0a007e

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionSocial;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionSocial;->j:Lflipboard/gui/FLImageView;

    .line 64
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 65
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 212
    iget v2, p0, Lflipboard/gui/section/AttributionSocial;->g:I

    .line 215
    iget v1, p0, Lflipboard/gui/section/AttributionSocial;->f:I

    .line 216
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v0

    const/16 v3, 0x8

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 221
    :goto_0
    iget-object v3, p0, Lflipboard/gui/section/AttributionSocial;->j:Lflipboard/gui/FLImageView;

    iget-object v4, p0, Lflipboard/gui/section/AttributionSocial;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {v3, v1, v2, v0, v4}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 223
    iget v1, p0, Lflipboard/gui/section/AttributionSocial;->g:I

    add-int/2addr v0, v1

    .line 224
    iget-object v1, p0, Lflipboard/gui/section/AttributionSocial;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v0

    .line 225
    iget-object v3, p0, Lflipboard/gui/section/AttributionSocial;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v2

    .line 226
    iget-object v4, p0, Lflipboard/gui/section/AttributionSocial;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v4, v0, v2, v1, v3}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 228
    iget v1, p0, Lflipboard/gui/section/AttributionSocial;->f:I

    add-int/2addr v1, v3

    .line 229
    iget-object v2, p0, Lflipboard/gui/section/AttributionSocial;->n:Ljava/util/List;

    invoke-virtual {p0, v0, v1, v2}, Lflipboard/gui/section/AttributionSocial;->a(IILjava/util/List;)I

    .line 230
    return-void

    .line 219
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/16 v8, 0x8

    const/4 v2, 0x0

    .line 156
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 157
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 159
    iget-object v1, p0, Lflipboard/gui/section/AttributionSocial;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v4, v1

    iget v3, p0, Lflipboard/gui/section/AttributionSocial;->g:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    .line 161
    iget-object v3, p0, Lflipboard/gui/section/AttributionSocial;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v3

    if-eq v3, v8, :cond_4

    .line 162
    iget-object v3, p0, Lflipboard/gui/section/AttributionSocial;->h:Lflipboard/gui/FLTextView;

    const/high16 v5, -0x80000000

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v3, v1, v0}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 163
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v0

    iget v1, p0, Lflipboard/gui/section/AttributionSocial;->g:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 165
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v0

    if-eq v0, v8, :cond_0

    .line 166
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v0

    .line 167
    iget-object v3, p0, Lflipboard/gui/section/AttributionSocial;->j:Lflipboard/gui/FLImageView;

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v3, v5, v0}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 170
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionSocial;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f090092

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 172
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v2

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 173
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-eq v7, v8, :cond_3

    .line 174
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v0, v3, v7}, Landroid/view/View;->measure(II)V

    .line 175
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    :goto_2
    move v3, v0

    .line 177
    goto :goto_1

    .line 178
    :cond_1
    if-lez v1, :cond_2

    iget v2, p0, Lflipboard/gui/section/AttributionSocial;->g:I

    .line 179
    :cond_2
    add-int v0, v1, v3

    add-int/2addr v0, v2

    invoke-virtual {p0, v4, v0}, Lflipboard/gui/section/AttributionSocial;->setMeasuredDimension(II)V

    .line 180
    return-void

    :cond_3
    move v0, v3

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public setDrawTopDivider(Z)V
    .locals 0

    .prologue
    .line 143
    return-void
.end method

.method public setInverted(Z)V
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lflipboard/gui/section/AttributionSocial;->c:Z

    if-eq p1, v0, :cond_0

    .line 147
    iput-boolean p1, p0, Lflipboard/gui/section/AttributionSocial;->c:Z

    .line 148
    iget-object v0, p0, Lflipboard/gui/section/AttributionSocial;->s:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 149
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionSocial;->c()V

    .line 152
    :cond_0
    return-void
.end method
