.class Lflipboard/gui/section/SectionPage$2;
.super Ljava/lang/Object;
.source "SectionPage.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/Section;",
        "Lflipboard/service/Section$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/gui/FLTextIntf;

.field final synthetic b:Lflipboard/gui/section/SectionPage;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionPage;Lflipboard/gui/FLTextIntf;)V
    .locals 0

    .prologue
    .line 485
    iput-object p1, p0, Lflipboard/gui/section/SectionPage$2;->b:Lflipboard/gui/section/SectionPage;

    iput-object p2, p0, Lflipboard/gui/section/SectionPage$2;->a:Lflipboard/gui/FLTextIntf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 485
    check-cast p1, Lflipboard/service/Section;

    check-cast p2, Lflipboard/service/Section$Message;

    sget-object v0, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    if-ne p2, v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v3, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    iget-object v4, p0, Lflipboard/gui/section/SectionPage$2;->b:Lflipboard/gui/section/SectionPage;

    iget-object v4, v4, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    iget-object v4, v4, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v1

    :goto_1
    invoke-virtual {p1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v5, p0, Lflipboard/gui/section/SectionPage$2;->a:Lflipboard/gui/FLTextIntf;

    invoke-interface {v5}, Lflipboard/gui/FLTextIntf;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    :goto_2
    if-eqz v0, :cond_0

    invoke-virtual {p1, p0}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    iget-object v0, p0, Lflipboard/gui/section/SectionPage$2;->b:Lflipboard/gui/section/SectionPage;

    invoke-static {v0}, Lflipboard/gui/section/SectionPage;->a(Lflipboard/gui/section/SectionPage;)Lflipboard/util/Observer;

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionPage$2;->b:Lflipboard/gui/section/SectionPage;

    invoke-static {v0}, Lflipboard/gui/section/SectionPage;->b(Lflipboard/gui/section/SectionPage;)Lflipboard/service/FlipboardManager;

    move-result-object v0

    new-instance v1, Lflipboard/gui/section/SectionPage$2$1;

    invoke-direct {v1, p0, v4}, Lflipboard/gui/section/SectionPage$2$1;-><init>(Lflipboard/gui/section/SectionPage$2;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method
