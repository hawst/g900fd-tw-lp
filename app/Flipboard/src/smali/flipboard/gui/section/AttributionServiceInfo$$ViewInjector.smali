.class public Lflipboard/gui/section/AttributionServiceInfo$$ViewInjector;
.super Ljava/lang/Object;
.source "AttributionServiceInfo$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/AttributionServiceInfo;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a007a

    const-string v1, "field \'reasonTextField\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/AttributionServiceInfo;->u:Lflipboard/gui/FLTextView;

    .line 12
    const v0, 0x7f0a007b

    const-string v1, "field \'reasonDivider\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    iput-object v0, p1, Lflipboard/gui/section/AttributionServiceInfo;->v:Landroid/view/View;

    .line 14
    const v0, 0x7f0a0083

    const-string v1, "field \'reply\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/section/AttributionServiceInfo;->w:Lflipboard/gui/FLTextView;

    .line 16
    return-void
.end method

.method public static reset(Lflipboard/gui/section/AttributionServiceInfo;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19
    iput-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->u:Lflipboard/gui/FLTextView;

    .line 20
    iput-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->v:Landroid/view/View;

    .line 21
    iput-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->w:Lflipboard/gui/FLTextView;

    .line 22
    return-void
.end method
