.class Lflipboard/gui/section/MagazineConversationComposeFragment$4;
.super Ljava/lang/Object;
.source "MagazineConversationComposeFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lflipboard/gui/section/MagazineConversationComposeFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/MagazineConversationComposeFragment;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lflipboard/gui/section/MagazineConversationComposeFragment$4;->a:Lflipboard/gui/section/MagazineConversationComposeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 160
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 157
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    .line 164
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 165
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment$4;->a:Lflipboard/gui/section/MagazineConversationComposeFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationComposeFragment;->b(Lflipboard/gui/section/MagazineConversationComposeFragment;)Lflipboard/gui/FLButton;

    move-result-object v0

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080084

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setTextColor(I)V

    .line 166
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment$4;->a:Lflipboard/gui/section/MagazineConversationComposeFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationComposeFragment;->b(Lflipboard/gui/section/MagazineConversationComposeFragment;)Lflipboard/gui/FLButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setEnabled(Z)V

    .line 171
    :goto_0
    return-void

    .line 168
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment$4;->a:Lflipboard/gui/section/MagazineConversationComposeFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationComposeFragment;->b(Lflipboard/gui/section/MagazineConversationComposeFragment;)Lflipboard/gui/FLButton;

    move-result-object v0

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080082

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setTextColor(I)V

    .line 169
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment$4;->a:Lflipboard/gui/section/MagazineConversationComposeFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationComposeFragment;->b(Lflipboard/gui/section/MagazineConversationComposeFragment;)Lflipboard/gui/FLButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setEnabled(Z)V

    goto :goto_0
.end method
