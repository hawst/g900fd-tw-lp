.class Lflipboard/gui/section/SectionFragment$4;
.super Ljava/lang/Object;
.source "SectionFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lflipboard/gui/section/SectionFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment;Z)V
    .locals 0

    .prologue
    .line 668
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$4;->b:Lflipboard/gui/section/SectionFragment;

    iput-boolean p2, p0, Lflipboard/gui/section/SectionFragment$4;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 670
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 671
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$4;->b:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->e(Lflipboard/gui/section/SectionFragment;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 672
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$4;->b:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->e(Lflipboard/gui/section/SectionFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 674
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$4;->b:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->m()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 675
    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragment$4;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$4;->b:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 676
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 677
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$4;->b:Lflipboard/gui/section/SectionFragment;

    invoke-static {v2, v0}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/gui/section/SectionFragment;Lflipboard/objs/FeedItem;)V

    .line 678
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$4;->b:Lflipboard/gui/section/SectionFragment;

    invoke-static {v2}, Lflipboard/gui/section/SectionFragment;->g(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/Group;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$4;->b:Lflipboard/gui/section/SectionFragment;

    invoke-static {v2}, Lflipboard/gui/section/SectionFragment;->g(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/Group;

    move-result-object v2

    iget-object v2, v2, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 679
    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 682
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$4;->b:Lflipboard/gui/section/SectionFragment;

    iget-boolean v2, p0, Lflipboard/gui/section/SectionFragment$4;->a:Z

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/section/SectionFragment;->a(Ljava/util/List;Z)V

    .line 683
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$4;->b:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 684
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$4;->b:Lflipboard/gui/section/SectionFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Lflipboard/gui/section/SectionFragment;->a(ZZ)V

    .line 686
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$4;->b:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->a()V

    .line 687
    return-void
.end method
