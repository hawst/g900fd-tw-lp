.class Lflipboard/gui/section/SectionFragment$DetailActivityCommunicationsLink;
.super Ljava/lang/Object;
.source "SectionFragment.java"


# instance fields
.field final synthetic a:Lflipboard/gui/section/SectionFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment;)V
    .locals 0

    .prologue
    .line 293
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$DetailActivityCommunicationsLink;->a:Lflipboard/gui/section/SectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdateSnapshot(Lflipboard/gui/section/SectionFragment$DetailAcititySnapshotMessage;)V
    .locals 6
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 296
    iget-object v0, p1, Lflipboard/gui/section/SectionFragment$DetailAcititySnapshotMessage;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lflipboard/gui/section/SectionFragment$DetailAcititySnapshotMessage;->a:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$DetailActivityCommunicationsLink;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$DetailActivityCommunicationsLink;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v1, p1, Lflipboard/gui/section/SectionFragment$DetailAcititySnapshotMessage;->b:Ljava/lang/String;

    iget-object v2, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v2, :cond_3

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionFragment;->c(Ljava/lang/String;)I

    move-result v1

    sget-object v3, Lflipboard/activities/DetailActivity;->A:Landroid/graphics/Bitmap;

    if-nez v3, :cond_0

    iget-object v3, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v3}, Lflipboard/gui/flipping/FlipTransitionViews;->getWidth()I

    move-result v3

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getHeight()I

    move-result v0

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v0, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lflipboard/activities/DetailActivity;->A:Landroid/graphics/Bitmap;

    :cond_0
    new-instance v0, Landroid/graphics/Canvas;

    sget-object v3, Lflipboard/activities/DetailActivity;->A:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v2, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1, v0}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 299
    :cond_1
    :goto_0
    return-void

    .line 297
    :cond_2
    sput-object v5, Lflipboard/activities/DetailActivity;->A:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_3
    sput-object v5, Lflipboard/activities/DetailActivity;->A:Landroid/graphics/Bitmap;

    goto :goto_0
.end method
