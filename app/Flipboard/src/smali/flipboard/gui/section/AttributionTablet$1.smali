.class Lflipboard/gui/section/AttributionTablet$1;
.super Ljava/lang/Object;
.source "AttributionTablet.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/section/AttributionTablet;


# direct methods
.method constructor <init>(Lflipboard/gui/section/AttributionTablet;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lflipboard/gui/section/AttributionTablet$1;->a:Lflipboard/gui/section/AttributionTablet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 79
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet$1;->a:Lflipboard/gui/section/AttributionTablet;

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionTablet;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 80
    if-eqz v0, :cond_0

    .line 81
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 82
    check-cast v0, Ljava/lang/String;

    .line 83
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 84
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet$1;->a:Lflipboard/gui/section/AttributionTablet;

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    instance-of v1, v0, Lflipboard/objs/FeedItem;

    if-eqz v1, :cond_0

    .line 86
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_2

    .line 87
    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet$1;->a:Lflipboard/gui/section/AttributionTablet;

    iget-object v2, v1, Lflipboard/gui/section/AttributionTablet;->b:Lflipboard/service/Section;

    move-object v1, v0

    check-cast v1, Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v1

    check-cast v0, Lflipboard/objs/FeedItem;

    sget-object v3, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->b:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    invoke-static {p1, v2, v1, v0, v3}, Lflipboard/util/SocialHelper;->a(Landroid/view/View;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/FeedItem;Lflipboard/objs/UsageEventV2$SocialCardNavFrom;)V

    goto :goto_0

    .line 89
    :cond_2
    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet$1;->a:Lflipboard/gui/section/AttributionTablet;

    iget-object v2, v1, Lflipboard/gui/section/AttributionTablet;->b:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet$1;->a:Lflipboard/gui/section/AttributionTablet;

    invoke-virtual {v1}, Lflipboard/gui/section/AttributionTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lflipboard/activities/FlipboardActivity;

    sget-object v3, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->b:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    invoke-static {v0, v2, v1, v3}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/UsageEventV2$SocialCardNavFrom;)V

    goto :goto_0
.end method
