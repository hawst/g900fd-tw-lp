.class public Lflipboard/gui/section/AttributionTablet;
.super Lflipboard/gui/section/AttributionBase;
.source "AttributionTablet.java"

# interfaces
.implements Lflipboard/gui/item/TabletItem;


# instance fields
.field private A:Landroid/os/Bundle;

.field protected s:Lflipboard/objs/FeedItem;

.field protected t:Lflipboard/gui/FLLabelTextView;

.field protected u:Lflipboard/gui/section/AttributionButtonWithText;

.field private v:Lflipboard/gui/FLTextView;

.field private w:Lflipboard/gui/FLTextView;

.field private x:Landroid/widget/ImageView;

.field private y:Lflipboard/gui/FLLabelTextView;

.field private z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lflipboard/gui/section/AttributionBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    check-cast p1, Lflipboard/activities/FlipboardActivity;

    iput-object p1, p0, Lflipboard/gui/section/AttributionTablet;->a:Lflipboard/activities/FlipboardActivity;

    .line 61
    return-void
.end method


# virtual methods
.method protected final a(Lflipboard/objs/CommentaryResult$Item;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x0

    .line 199
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 200
    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet;->d:Lflipboard/objs/FeedItem;

    iget-boolean v1, v1, Lflipboard/objs/FeedItem;->bk:Z

    if-nez v1, :cond_0

    .line 201
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->d:Lflipboard/objs/FeedItem;

    iget-wide v4, v3, Lflipboard/objs/FeedItem;->Z:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-static {v1, v4, v5}, Lflipboard/util/JavaUtil;->d(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    invoke-static {v1, v3}, Lflipboard/gui/SocialFormatter;->a(Landroid/content/Context;Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v1

    .line 204
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 205
    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->d:Lflipboard/objs/FeedItem;

    iget-boolean v3, v3, Lflipboard/objs/FeedItem;->bk:Z

    if-nez v3, :cond_1

    .line 206
    const-string v3, " \u00b7 "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    :cond_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    :cond_2
    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->bf:Lflipboard/objs/FeedItem$Note;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->bf:Lflipboard/objs/FeedItem$Note;

    iget-object v1, v1, Lflipboard/objs/FeedItem$Note;->b:Ljava/util/List;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->bf:Lflipboard/objs/FeedItem$Note;

    iget-object v1, v1, Lflipboard/objs/FeedItem$Note;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 213
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bf:Lflipboard/objs/FeedItem$Note;

    iget-object v1, v0, Lflipboard/objs/FeedItem$Note;->a:Ljava/lang/String;

    .line 214
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bf:Lflipboard/objs/FeedItem$Note;

    iget-object v0, v0, Lflipboard/objs/FeedItem$Note;->b:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    move-object v9, v0

    move-object v0, v1

    move-object v1, v9

    .line 217
    :goto_0
    if-eqz v1, :cond_6

    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->b:Lflipboard/service/Section;

    invoke-virtual {v3, v1}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedSectionLink;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 218
    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->v:Lflipboard/gui/FLTextView;

    invoke-virtual {v3, v8}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 219
    if-nez v0, :cond_3

    .line 220
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0d01ce

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v1, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    aput-object v4, v3, v8

    invoke-static {v0, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 222
    :cond_3
    const-string v3, " \u00b7 "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->v:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->A:Landroid/os/Bundle;

    invoke-static {v0, v2, v1, v3}, Lflipboard/util/SocialHelper;->b(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/List;Landroid/os/Bundle;)V

    .line 230
    :cond_4
    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->u:Lflipboard/gui/section/AttributionButtonWithText;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 231
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->u:Lflipboard/gui/section/AttributionButtonWithText;

    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->d:I

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionButtonWithText;->setNumber(I)V

    .line 234
    :cond_5
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionTablet;->c()V

    .line 235
    return-void

    .line 225
    :cond_6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 226
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->v:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v8}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 227
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->v:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_7
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 101
    iput-object p2, p0, Lflipboard/gui/section/AttributionTablet;->d:Lflipboard/objs/FeedItem;

    .line 102
    iput-object p1, p0, Lflipboard/gui/section/AttributionTablet;->b:Lflipboard/service/Section;

    .line 103
    invoke-virtual {p0, p2}, Lflipboard/gui/section/AttributionTablet;->setTag(Ljava/lang/Object;)V

    .line 105
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    .line 106
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 107
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    iput-object v3, p0, Lflipboard/gui/section/AttributionTablet;->A:Landroid/os/Bundle;

    .line 108
    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->A:Landroid/os/Bundle;

    const-string v4, "source"

    const-string v5, "sectionLink"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->A:Landroid/os/Bundle;

    const-string v4, "originSectionIdentifier"

    iget-object v5, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v5, v5, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->A:Landroid/os/Bundle;

    const-string v4, "linkType"

    const-string v5, "magazine"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->B()Lflipboard/objs/FeedSectionLink;

    move-result-object v3

    .line 112
    iget-object v4, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    if-ne p2, v4, :cond_2

    if-eqz v3, :cond_2

    .line 114
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->h:Lflipboard/gui/FLTextView;

    iget-object v1, v3, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v0, v3, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 117
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->i:Lflipboard/gui/FLImageView;

    iget-object v1, v3, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 121
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v8}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 180
    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionTablet;->a(Lflipboard/objs/CommentaryResult$Item;)V

    .line 182
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->a:Lflipboard/activities/FlipboardActivity;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p2, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, Lflipboard/util/TopicHelper;->a(Lflipboard/objs/FeedItem;)Lflipboard/objs/FeedSectionLink;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->t:Lflipboard/gui/FLLabelTextView;

    iget-object v4, v1, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->t:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v3, v2}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->t:Lflipboard/gui/FLLabelTextView;

    new-instance v3, Lflipboard/gui/section/AttributionTablet$2;

    invoke-direct {v3, p0, v1, p1, v0}, Lflipboard/gui/section/AttributionTablet$2;-><init>(Lflipboard/gui/section/AttributionTablet;Lflipboard/objs/FeedSectionLink;Lflipboard/service/Section;Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Lflipboard/gui/FLLabelTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    :cond_0
    return-void

    .line 119
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v8}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto :goto_0

    .line 123
    :cond_2
    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    if-eq v0, v3, :cond_3

    .line 124
    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    iput-object v3, p0, Lflipboard/gui/section/AttributionTablet;->s:Lflipboard/objs/FeedItem;

    .line 125
    iput-object v0, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    .line 127
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->s:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v3, "flipboard"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 128
    if-eqz v0, :cond_6

    .line 129
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->w:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->s:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v0

    .line 131
    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->s:Lflipboard/objs/FeedItem;

    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->M()Lflipboard/objs/FeedSectionLink;

    move-result-object v3

    .line 132
    invoke-virtual {p1, v3}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedSectionLink;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 133
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0142

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 134
    iget-object v5, v3, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    .line 135
    new-array v6, v7, [Ljava/lang/Object;

    aput-object v0, v6, v2

    aput-object v5, v6, v1

    invoke-static {v4, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 136
    iget-object v4, p0, Lflipboard/gui/section/AttributionTablet;->w:Lflipboard/gui/FLTextView;

    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iget-object v5, p0, Lflipboard/gui/section/AttributionTablet;->A:Landroid/os/Bundle;

    invoke-static {v4, v0, v3, v5}, Lflipboard/util/SocialHelper;->b(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/List;Landroid/os/Bundle;)V

    .line 150
    :cond_3
    :goto_2
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 151
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->i:Lflipboard/gui/FLImageView;

    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v3}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 159
    :goto_3
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v3, "flipboard"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 160
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->M()Lflipboard/objs/FeedSectionLink;

    move-result-object v0

    .line 162
    if-eqz v0, :cond_a

    invoke-virtual {p1, v0}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedSectionLink;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 164
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0141

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {v5}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v5, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 165
    iget-object v4, p0, Lflipboard/gui/section/AttributionTablet;->h:Lflipboard/gui/FLTextView;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object v5, p0, Lflipboard/gui/section/AttributionTablet;->A:Landroid/os/Bundle;

    invoke-static {v4, v3, v0, v5}, Lflipboard/util/SocialHelper;->b(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/List;Landroid/os/Bundle;)V

    move v0, v1

    .line 169
    :goto_4
    if-nez v0, :cond_4

    .line 170
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->h:Lflipboard/gui/FLTextView;

    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    :cond_4
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    .line 174
    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v3, "googlereader"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    if-eqz v0, :cond_9

    iget-object v1, v0, Lflipboard/objs/ConfigService;->m:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 175
    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet;->j:Lflipboard/gui/FLImageView;

    iget-object v0, v0, Lflipboard/objs/ConfigService;->m:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 138
    :cond_5
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0140

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 139
    new-array v4, v1, [Ljava/lang/Object;

    aput-object v0, v4, v2

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 140
    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->w:Lflipboard/gui/FLTextView;

    invoke-virtual {v3, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 143
    :cond_6
    iget-object v0, p2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v3, "status"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    move-object v0, p2

    .line 144
    :goto_5
    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->y:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v3, v2}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 145
    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->y:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 147
    new-array v0, v7, [Landroid/view/View;

    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->x:Landroid/widget/ImageView;

    aput-object v3, v0, v2

    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->y:Lflipboard/gui/FLLabelTextView;

    aput-object v3, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/AttributionTablet;->z:Ljava/util/List;

    goto/16 :goto_2

    .line 143
    :cond_7
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    goto :goto_5

    .line 154
    :cond_8
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->i:Lflipboard/gui/FLImageView;

    const v3, 0x7f02005a

    invoke-virtual {v0, v3}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    goto/16 :goto_3

    .line 177
    :cond_9
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v8}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_a
    move v0, v2

    goto/16 :goto_4
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 352
    iget-boolean v0, p0, Lflipboard/gui/section/AttributionTablet;->c:Z

    return v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 385
    return-void
.end method

.method protected final c()V
    .locals 5

    .prologue
    const v1, 0x7f0800aa

    const v2, 0x7f08009b

    .line 240
    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/AttributionTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-boolean v0, p0, Lflipboard/gui/section/AttributionTablet;->c:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    .line 241
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->w:Lflipboard/gui/FLTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/AttributionTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-boolean v4, p0, Lflipboard/gui/section/AttributionTablet;->c:Z

    if-eqz v4, :cond_4

    :goto_1
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    .line 242
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->ai()Ljava/lang/String;

    move-result-object v0

    .line 243
    if-nez v0, :cond_0

    .line 244
    const-string v0, "flipboard"

    .line 246
    :cond_0
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    .line 247
    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet;->u:Lflipboard/gui/section/AttributionButtonWithText;

    if-eqz v1, :cond_1

    .line 248
    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet;->u:Lflipboard/gui/section/AttributionButtonWithText;

    sget-object v2, Lflipboard/gui/section/AttributionButtonWithText$Type;->d:Lflipboard/gui/section/AttributionButtonWithText$Type;

    const/4 v3, 0x0

    iget-boolean v4, p0, Lflipboard/gui/section/AttributionTablet;->c:Z

    invoke-virtual {v1, v0, v2, v3, v4}, Lflipboard/gui/section/AttributionButtonWithText;->a(Lflipboard/objs/ConfigService;Lflipboard/gui/section/AttributionButtonWithText$Type;ZZ)V

    .line 250
    :cond_1
    iget-boolean v0, p0, Lflipboard/gui/section/AttributionTablet;->c:Z

    if-eqz v0, :cond_2

    .line 251
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->t:Lflipboard/gui/FLLabelTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 253
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 240
    goto :goto_0

    :cond_4
    move v1, v2

    .line 241
    goto :goto_1
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->d:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0}, Lflipboard/gui/section/AttributionBase;->onFinishInflate()V

    .line 66
    const v0, 0x7f0a007c

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionTablet;->i:Lflipboard/gui/FLImageView;

    .line 67
    const v0, 0x7f0a007d

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionTablet;->h:Lflipboard/gui/FLTextView;

    .line 68
    const v0, 0x7f0a0080

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionTablet;->v:Lflipboard/gui/FLTextView;

    .line 69
    const v0, 0x7f0a0087

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionTablet;->w:Lflipboard/gui/FLTextView;

    .line 70
    const v0, 0x7f0a007e

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionTablet;->j:Lflipboard/gui/FLImageView;

    .line 71
    const v0, 0x7f0a0084

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionTablet;->x:Landroid/widget/ImageView;

    .line 72
    const v0, 0x7f0a0085

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionTablet;->y:Lflipboard/gui/FLLabelTextView;

    .line 74
    const v0, 0x7f0a0089

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionTablet;->t:Lflipboard/gui/FLLabelTextView;

    .line 76
    new-instance v0, Lflipboard/gui/section/AttributionTablet$1;

    invoke-direct {v0, p0}, Lflipboard/gui/section/AttributionTablet$1;-><init>(Lflipboard/gui/section/AttributionTablet;)V

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionTablet;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/16 v7, 0x8

    .line 305
    sub-int v4, p4, p2

    .line 306
    iget v0, p0, Lflipboard/gui/section/AttributionTablet;->f:I

    .line 307
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->w:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v2

    if-eq v2, v7, :cond_0

    .line 308
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->w:Lflipboard/gui/FLTextView;

    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->w:Lflipboard/gui/FLTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v3

    iget-object v5, p0, Lflipboard/gui/section/AttributionTablet;->w:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v2, v1, v0, v3, v5}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 309
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->w:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v2

    iget v3, p0, Lflipboard/gui/section/AttributionTablet;->f:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 313
    :cond_0
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v2

    if-eq v2, v7, :cond_4

    .line 314
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v2

    .line 315
    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->i:Lflipboard/gui/FLImageView;

    iget-object v5, p0, Lflipboard/gui/section/AttributionTablet;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v3, v1, v0, v2, v5}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 316
    iget v1, p0, Lflipboard/gui/section/AttributionTablet;->f:I

    add-int/2addr v2, v1

    .line 317
    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v1

    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->t:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    .line 319
    :goto_0
    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v3

    add-int v5, v2, v3

    .line 320
    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    .line 321
    iget-object v6, p0, Lflipboard/gui/section/AttributionTablet;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v6, v2, v0, v5, v3}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 323
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->t:Lflipboard/gui/FLLabelTextView;

    iget-object v5, p0, Lflipboard/gui/section/AttributionTablet;->t:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v5

    sub-int v5, v4, v5

    iget-object v6, p0, Lflipboard/gui/section/AttributionTablet;->t:Lflipboard/gui/FLLabelTextView;

    .line 324
    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v1

    .line 323
    invoke-virtual {v0, v5, v1, v4, v6}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 328
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->x:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eq v0, v7, :cond_3

    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->y:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v0

    if-eq v0, v7, :cond_3

    .line 329
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->z:Ljava/util/List;

    invoke-virtual {p0, v2, v3, v0}, Lflipboard/gui/section/AttributionTablet;->a(IILjava/util/List;)I

    move-result v0

    add-int/2addr v0, v3

    .line 334
    :goto_1
    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v1

    if-eq v1, v7, :cond_1

    .line 336
    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v2

    .line 337
    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/section/AttributionTablet;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    .line 338
    iget-object v4, p0, Lflipboard/gui/section/AttributionTablet;->j:Lflipboard/gui/FLImageView;

    add-int v5, v0, v3

    add-int/2addr v3, v0

    iget-object v6, p0, Lflipboard/gui/section/AttributionTablet;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v6}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v3, v6

    invoke-virtual {v4, v2, v5, v1, v3}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 339
    iget v2, p0, Lflipboard/gui/section/AttributionTablet;->f:I

    add-int/2addr v2, v1

    .line 343
    :cond_1
    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet;->v:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v1

    if-eq v1, v7, :cond_2

    .line 345
    iget-object v1, p0, Lflipboard/gui/section/AttributionTablet;->v:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v0

    .line 347
    iget-object v3, p0, Lflipboard/gui/section/AttributionTablet;->v:Lflipboard/gui/FLTextView;

    iget-object v4, p0, Lflipboard/gui/section/AttributionTablet;->v:Lflipboard/gui/FLTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {v3, v2, v0, v4, v1}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 349
    :cond_2
    return-void

    :cond_3
    move v0, v3

    goto :goto_1

    :cond_4
    move v2, v1

    move v1, v0

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/high16 v8, -0x80000000

    const/4 v1, 0x0

    const/16 v7, 0x8

    .line 257
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 258
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 259
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v0

    if-eq v0, v7, :cond_0

    .line 260
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->i:Lflipboard/gui/FLImageView;

    invoke-static {v0}, Lflipboard/gui/section/AttributionTablet;->a(Landroid/view/View;)V

    .line 263
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v3, v0

    iget v2, p0, Lflipboard/gui/section/AttributionTablet;->g:I

    sub-int/2addr v0, v2

    .line 265
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->t:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v2

    if-eq v2, v7, :cond_1

    .line 266
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->t:Lflipboard/gui/FLLabelTextView;

    div-int/lit8 v5, v0, 0x2

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 267
    invoke-static {v4, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 266
    invoke-virtual {v2, v5, v6}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 270
    :cond_1
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->t:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    .line 271
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v2

    if-eq v2, v7, :cond_2

    .line 272
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->h:Lflipboard/gui/FLTextView;

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v4, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v2, v5, v6}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 274
    :cond_2
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->x:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-eq v2, v7, :cond_3

    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->y:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v2

    if-eq v2, v7, :cond_3

    .line 275
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->x:Landroid/widget/ImageView;

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v4, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v2, v5, v6}, Landroid/widget/ImageView;->measure(II)V

    .line 276
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->y:Lflipboard/gui/FLLabelTextView;

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v4, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v2, v5, v6}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 278
    :cond_3
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v2

    if-eq v2, v7, :cond_4

    .line 279
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->j:Lflipboard/gui/FLImageView;

    invoke-static {v2}, Lflipboard/gui/section/AttributionTablet;->a(Landroid/view/View;)V

    .line 281
    :cond_4
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->v:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v2

    if-eq v2, v7, :cond_6

    .line 283
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v2

    if-eq v2, v7, :cond_5

    .line 284
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v2

    iget v5, p0, Lflipboard/gui/section/AttributionTablet;->f:I

    add-int/2addr v2, v5

    sub-int/2addr v0, v2

    .line 286
    :cond_5
    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->v:Lflipboard/gui/FLTextView;

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v4, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v2, v0, v5}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 288
    :cond_6
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v0

    iget-object v2, p0, Lflipboard/gui/section/AttributionTablet;->v:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, v0

    .line 289
    iget-object v0, p0, Lflipboard/gui/section/AttributionTablet;->y:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v0

    iget-object v5, p0, Lflipboard/gui/section/AttributionTablet;->x:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 290
    iget v0, p0, Lflipboard/gui/section/AttributionTablet;->f:I

    mul-int/lit8 v0, v0, 0x2

    .line 291
    iget-object v6, p0, Lflipboard/gui/section/AttributionTablet;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v6}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v2, v5

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 292
    iget-object v5, p0, Lflipboard/gui/section/AttributionTablet;->w:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v5

    if-eq v5, v7, :cond_7

    .line 293
    iget-object v5, p0, Lflipboard/gui/section/AttributionTablet;->w:Lflipboard/gui/FLTextView;

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v4, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v5, v6, v4}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 294
    iget-object v4, p0, Lflipboard/gui/section/AttributionTablet;->w:Lflipboard/gui/FLTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v2, v4

    .line 295
    iget v4, p0, Lflipboard/gui/section/AttributionTablet;->f:I

    add-int/2addr v0, v4

    .line 297
    :cond_7
    if-nez v2, :cond_8

    move v0, v1

    .line 300
    :cond_8
    add-int/2addr v0, v2

    invoke-virtual {p0, v3, v0}, Lflipboard/gui/section/AttributionTablet;->setMeasuredDimension(II)V

    .line 301
    return-void
.end method

.method public setInverted(Z)V
    .locals 1

    .prologue
    .line 191
    iget-boolean v0, p0, Lflipboard/gui/section/AttributionTablet;->c:Z

    if-eq p1, v0, :cond_0

    .line 192
    iput-boolean p1, p0, Lflipboard/gui/section/AttributionTablet;->c:Z

    .line 193
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionTablet;->c()V

    .line 195
    :cond_0
    return-void
.end method
