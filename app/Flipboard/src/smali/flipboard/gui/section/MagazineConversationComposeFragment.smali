.class public Lflipboard/gui/section/MagazineConversationComposeFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "MagazineConversationComposeFragment.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lflipboard/activities/GenericFragmentActivity;

.field private c:Lflipboard/gui/actionbar/FLActionBar;

.field private d:Lflipboard/gui/FLEditText;

.field private e:Lflipboard/gui/FLButton;

.field private f:Ljava/lang/String;

.field private g:Lflipboard/util/Log;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 41
    const-string v0, "New Conversation"

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->a:Ljava/lang/String;

    .line 49
    const-string v0, "magazine_conversation_thread_creation"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->g:Lflipboard/util/Log;

    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/MagazineConversationComposeFragment;)Lflipboard/activities/GenericFragmentActivity;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->b:Lflipboard/activities/GenericFragmentActivity;

    return-object v0
.end method

.method static synthetic b(Lflipboard/gui/section/MagazineConversationComposeFragment;)Lflipboard/gui/FLButton;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->e:Lflipboard/gui/FLButton;

    return-object v0
.end method

.method static synthetic c(Lflipboard/gui/section/MagazineConversationComposeFragment;)V
    .locals 6

    .prologue
    .line 36
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->b:Lflipboard/activities/GenericFragmentActivity;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->d(Landroid/app/Activity;)V

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v0

    sget-object v1, Lflipboard/objs/CommentaryResult$CommentType;->b:Lflipboard/objs/CommentaryResult$CommentType;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v3, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->f:Ljava/lang/String;

    iget-object v4, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->d:Lflipboard/gui/FLEditText;

    invoke-virtual {v4}, Lflipboard/gui/FLEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lflipboard/gui/section/MagazineConversationComposeFragment$3;

    invoke-direct {v5, p0}, Lflipboard/gui/section/MagazineConversationComposeFragment$3;-><init>(Lflipboard/gui/section/MagazineConversationComposeFragment;)V

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/Flap;->a(Lflipboard/objs/CommentaryResult$CommentType;Lflipboard/service/User;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/ServiceReloginObserver;)V

    return-void
.end method

.method static synthetic d(Lflipboard/gui/section/MagazineConversationComposeFragment;)Lflipboard/util/Log;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->g:Lflipboard/util/Log;

    return-object v0
.end method

.method static synthetic e(Lflipboard/gui/section/MagazineConversationComposeFragment;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->j:Z

    return v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 54
    const v0, 0x7f0300d0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 55
    invoke-virtual {p0}, Lflipboard/gui/section/MagazineConversationComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/GenericFragmentActivity;

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->b:Lflipboard/activities/GenericFragmentActivity;

    .line 58
    invoke-virtual {p0}, Lflipboard/gui/section/MagazineConversationComposeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "feedItemId"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->f:Ljava/lang/String;

    .line 60
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->b:Lflipboard/activities/GenericFragmentActivity;

    const v2, 0x7f0a004d

    invoke-virtual {v0, v2}, Lflipboard/activities/GenericFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 61
    const-string v2, "New Conversation"

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->b:Lflipboard/activities/GenericFragmentActivity;

    invoke-virtual {v0}, Lflipboard/activities/GenericFragmentActivity;->l()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->c:Lflipboard/gui/actionbar/FLActionBar;

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->c:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->e()V

    new-instance v0, Lflipboard/gui/actionbar/FLActionBarMenu;

    iget-object v2, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->b:Lflipboard/activities/GenericFragmentActivity;

    invoke-virtual {v2}, Lflipboard/activities/GenericFragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lflipboard/gui/actionbar/FLActionBarMenu;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->b:Lflipboard/activities/GenericFragmentActivity;

    invoke-virtual {v2}, Lflipboard/activities/GenericFragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    const v3, 0x7f0f0007

    invoke-virtual {v2, v3, v0}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v2, 0x7f0a03a6

    invoke-virtual {v0, v2}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v2

    iput-boolean v4, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    iput-boolean v4, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->s:Z

    iget-object v3, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->c:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v3, v0}, Lflipboard/gui/actionbar/FLActionBar;->setMenu(Lflipboard/gui/actionbar/FLActionBarMenu;)V

    invoke-virtual {v2}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->e:Lflipboard/gui/FLButton;

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->e:Lflipboard/gui/FLButton;

    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v3, 0x7f0d029c

    invoke-virtual {v2, v3}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->e:Lflipboard/gui/FLButton;

    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v2}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080084

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLButton;->setTextColor(I)V

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->e:Lflipboard/gui/FLButton;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lflipboard/gui/FLButton;->setEnabled(Z)V

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->e:Lflipboard/gui/FLButton;

    new-instance v2, Lflipboard/gui/section/MagazineConversationComposeFragment$2;

    invoke-direct {v2, p0}, Lflipboard/gui/section/MagazineConversationComposeFragment$2;-><init>(Lflipboard/gui/section/MagazineConversationComposeFragment;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->c:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->d()V

    .line 65
    const v0, 0x7f0a0265

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLEditText;

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->d:Lflipboard/gui/FLEditText;

    .line 66
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->d:Lflipboard/gui/FLEditText;

    invoke-virtual {v0}, Lflipboard/gui/FLEditText;->requestFocus()Z

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/section/MagazineConversationComposeFragment$1;

    invoke-direct {v2, p0}, Lflipboard/gui/section/MagazineConversationComposeFragment$1;-><init>(Lflipboard/gui/section/MagazineConversationComposeFragment;)V

    invoke-virtual {v0, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    .line 67
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment;->d:Lflipboard/gui/FLEditText;

    new-instance v2, Lflipboard/gui/section/MagazineConversationComposeFragment$4;

    invoke-direct {v2, p0}, Lflipboard/gui/section/MagazineConversationComposeFragment$4;-><init>(Lflipboard/gui/section/MagazineConversationComposeFragment;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/FLEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 68
    return-object v1
.end method
