.class public Lflipboard/gui/section/SectionFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "SectionFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lflipboard/gui/section/SectionScrubber$ScrubberListener;
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/activities/FlipboardFragment;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/view/View$OnLongClickListener;",
        "Lflipboard/gui/section/SectionScrubber$ScrubberListener;",
        "Lflipboard/util/Observer",
        "<",
        "Ljava/lang/Object;",
        "Lflipboard/gui/flipping/FlipTransitionViews$Message;",
        "Lflipboard/gui/flipping/FlipTransitionBase$Direction;",
        ">;"
    }
.end annotation


# static fields
.field public static G:Lcom/squareup/otto/Bus;

.field private static P:Lflipboard/objs/SectionPageTemplate;

.field public static final a:Lflipboard/util/Log;

.field private static final az:Landroid/os/Bundle;

.field public static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public A:Z

.field public B:Landroid/os/Bundle;

.field C:Z

.field public D:Z

.field public E:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field public F:Landroid/support/v4/widget/DrawerLayout;

.field private final H:Ljava/util/concurrent/ExecutorService;

.field private I:Landroid/view/View;

.field private J:Lflipboard/gui/section/LoadingPage;

.field private final K:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private L:Z

.field private final M:Ljava/lang/Object;

.field private final N:Ljava/lang/Object;

.field private volatile O:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FLAdManager;",
            "Lflipboard/service/FLAdManager$Message;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private Q:Z

.field private R:Z

.field private S:Z

.field private T:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/Section;",
            "Lflipboard/service/Section$Message;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private U:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/User;",
            "Lflipboard/service/User$Message;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private V:Lflipboard/objs/UsageEventV2$SectionNavFrom;

.field private W:Ljava/lang/String;

.field private X:Lflipboard/io/UsageEvent;

.field private Y:Lflipboard/objs/UsageEventV2;

.field private Z:J

.field private aA:Lflipboard/gui/section/SectionFragment$DetailActivityCommunicationsLink;

.field private aB:Lflipboard/gui/flipping/FlipTransitionViews$HopTask;

.field private aC:Z

.field private aa:J

.field private ab:J

.field private ac:I

.field private ad:I

.field private ae:I

.field private af:I

.field private ag:Ljava/util/concurrent/atomic/AtomicInteger;

.field private ah:I

.field private ai:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FLAdManager;",
            "Lflipboard/service/FLAdManager$Message;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private aj:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/service/FLAdManager;",
            ">;"
        }
    .end annotation
.end field

.field private ak:Lflipboard/objs/FeedItem;

.field private al:Lflipboard/util/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Callback",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final am:Z

.field private an:Z

.field private ao:I

.field private ap:Lflipboard/io/UsageEvent;

.field private aq:Lflipboard/gui/section/Group;

.field private ar:Z

.field private as:I

.field private at:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lflipboard/objs/FeedItem;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private au:Ljava/lang/String;

.field private av:Lflipboard/gui/section/Group;

.field private aw:Z

.field private ax:Lflipboard/gui/section/Group;

.field private ay:Z

.field public e:I

.field f:Lflipboard/gui/section/ListSingleThreadWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/gui/section/ListSingleThreadWrapper",
            "<",
            "Lflipboard/gui/section/Group;",
            ">;"
        }
    .end annotation
.end field

.field g:Lflipboard/gui/section/ListSingleThreadWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/gui/section/ListSingleThreadWrapper",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field h:Lflipboard/gui/section/ListSingleThreadWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/gui/section/ListSingleThreadWrapper",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field i:Lflipboard/gui/section/ListSingleThreadWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/gui/section/ListSingleThreadWrapper",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field m:Lflipboard/gui/section/ListSingleThreadWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/gui/section/ListSingleThreadWrapper",
            "<",
            "Lflipboard/objs/SidebarGroup;",
            ">;"
        }
    .end annotation
.end field

.field n:Lflipboard/gui/item/CoverSectionItemTablet;

.field public o:Lflipboard/service/Section;

.field public p:Z

.field public q:I

.field public r:Lflipboard/service/FlipboardManager;

.field public s:Lflipboard/gui/section/SectionScrubber;

.field public t:Lflipboard/gui/flipping/FlipTransitionViews;

.field public final u:Lflipboard/service/audio/FLAudioManager;

.field public v:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/audio/FLAudioManager;",
            "Lflipboard/service/audio/FLAudioManager$AudioMessage;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public w:Lflipboard/service/FLAdManager;

.field public x:Z

.field public y:Z

.field z:Lflipboard/objs/FeedItem;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 124
    const-string v0, "templates"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/section/SectionFragment;->a:Lflipboard/util/Log;

    .line 144
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "web"

    aput-object v1, v0, v4

    const-string v1, "post"

    aput-object v1, v0, v3

    const-string v1, "status"

    aput-object v1, v0, v5

    const-string v1, "image"

    aput-object v1, v0, v6

    const-string v1, "video"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "album"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "audio"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "list"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "section"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "sectioncover"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "pagebox"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "group"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "promotedaction"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "synthetic-client-app-cover"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "synthetic-client-profile-page"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "synthetic-client-profile-summary-item"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "synthetic-action-refresh"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lflipboard/gui/section/SectionFragment;->b:Ljava/util/List;

    .line 147
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "post"

    aput-object v1, v0, v4

    const-string v1, "status"

    aput-object v1, v0, v3

    const-string v1, "image"

    aput-object v1, v0, v5

    const-string v1, "video"

    aput-object v1, v0, v6

    const-string v1, "album"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "audio"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "section"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "sectionCover"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lflipboard/gui/section/SectionFragment;->c:Ljava/util/List;

    .line 148
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "pageboxList"

    aput-object v1, v0, v4

    const-string v1, "pageboxGrid"

    aput-object v1, v0, v3

    const-string v1, "pageboxSuggestedFollows"

    aput-object v1, v0, v5

    const-string v1, "recommendedTopics"

    aput-object v1, v0, v6

    const-string v1, "pageboxAddService"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "pageboxCreateAccount"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "pageboxFindFriends"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "pageboxCarousel"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lflipboard/gui/section/SectionFragment;->d:Ljava/util/List;

    .line 254
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 257
    sput-object v0, Lflipboard/gui/section/SectionFragment;->az:Landroid/os/Bundle;

    const-string v1, "source"

    sget-object v2, Lflipboard/objs/UsageEventV2$DetailItemNavFrom;->a:Lflipboard/objs/UsageEventV2$DetailItemNavFrom;

    invoke-virtual {v2}, Lflipboard/objs/UsageEventV2$DetailItemNavFrom;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    new-instance v0, Lcom/squareup/otto/Bus;

    invoke-direct {v0}, Lcom/squareup/otto/Bus;-><init>()V

    sput-object v0, Lflipboard/gui/section/SectionFragment;->G:Lcom/squareup/otto/Bus;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 303
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 179
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    .line 193
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->M:Ljava/lang/Object;

    .line 194
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->N:Ljava/lang/Object;

    .line 211
    iput v2, p0, Lflipboard/gui/section/SectionFragment;->ad:I

    .line 213
    iput v1, p0, Lflipboard/gui/section/SectionFragment;->af:I

    .line 214
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->ag:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 226
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    iput-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->am:Z

    .line 235
    iput-boolean v1, p0, Lflipboard/gui/section/SectionFragment;->ar:Z

    .line 238
    iput v1, p0, Lflipboard/gui/section/SectionFragment;->as:I

    .line 251
    iput-boolean v2, p0, Lflipboard/gui/section/SectionFragment;->aw:Z

    .line 271
    new-instance v0, Lflipboard/gui/section/SectionFragment$DetailActivityCommunicationsLink;

    invoke-direct {v0, p0}, Lflipboard/gui/section/SectionFragment$DetailActivityCommunicationsLink;-><init>(Lflipboard/gui/section/SectionFragment;)V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->aA:Lflipboard/gui/section/SectionFragment$DetailActivityCommunicationsLink;

    .line 2203
    iput-boolean v1, p0, Lflipboard/gui/section/SectionFragment;->aC:Z

    .line 304
    invoke-virtual {p0, v2}, Lflipboard/gui/section/SectionFragment;->setRetainInstance(Z)V

    .line 305
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->K:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 308
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->z()Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->u:Lflipboard/service/audio/FLAudioManager;

    .line 309
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->H:Ljava/util/concurrent/ExecutorService;

    .line 310
    return-void
.end method

.method static synthetic A(Lflipboard/gui/section/SectionFragment;)Z
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->y:Z

    return v0
.end method

.method static synthetic a(Lflipboard/gui/section/SectionFragment;I)I
    .locals 0

    .prologue
    .line 123
    iput p1, p0, Lflipboard/gui/section/SectionFragment;->q:I

    return p1
.end method

.method static synthetic a(Lflipboard/gui/section/SectionFragment;Landroid/content/Context;Lflipboard/gui/section/Group;)Lflipboard/gui/section/SectionPage;
    .locals 3

    .prologue
    .line 123
    new-instance v1, Lflipboard/gui/section/SectionPage;

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-boolean v2, p0, Lflipboard/gui/section/SectionFragment;->D:Z

    invoke-direct {v1, p1, p2, v0, v2}, Lflipboard/gui/section/SectionPage;-><init>(Landroid/content/Context;Lflipboard/gui/section/Group;Lflipboard/service/Section;Z)V

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ag:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1, v0}, Lflipboard/gui/section/SectionPage;->setItemDisplayedCounter(Ljava/util/concurrent/atomic/AtomicInteger;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lflipboard/gui/section/SectionPage;->a(Z)Z

    const/4 v0, 0x1

    iput-boolean v0, v1, Lflipboard/gui/section/SectionPage;->r:Z

    const v0, 0x7f030029

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    return-object v1
.end method

.method static synthetic a(Lflipboard/gui/section/SectionFragment;Lflipboard/gui/section/Group;)Lflipboard/gui/section/SectionPage;
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lflipboard/gui/section/SectionFragment;->c(Lflipboard/gui/section/Group;)Lflipboard/gui/section/SectionPage;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lflipboard/gui/section/SectionFragment;Lflipboard/objs/FeedItem;Ljava/util/List;)Ljava/util/List;
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0, p1, p2}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/objs/FeedItem;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(Lflipboard/objs/FeedItem;Ljava/util/List;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/objs/FeedItem;",
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/section/Group;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/section/Group;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 2275
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2276
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2277
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 2278
    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v4, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v4}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lflipboard/service/User;->a(Lflipboard/objs/FeedItem;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2279
    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2281
    :cond_0
    iput-object p1, v0, Lflipboard/objs/FeedItem;->co:Lflipboard/objs/FeedItem;

    goto :goto_0

    :cond_1
    move v7, v8

    .line 2286
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2287
    sget-object v3, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->g()I

    move-result v4

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->h()I

    move-result v5

    const/4 v6, 0x1

    move-object v0, p0

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lflipboard/gui/section/SectionFragment;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;IIZ)Lflipboard/gui/section/Group;

    move-result-object v2

    .line 2288
    if-eqz v2, :cond_5

    .line 2289
    new-instance v3, Lflipboard/gui/section/GroupFranchiseMeta;

    invoke-direct {v3, p1}, Lflipboard/gui/section/GroupFranchiseMeta;-><init>(Lflipboard/objs/FeedItem;)V

    .line 2290
    iget-object v0, p1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    iput-object v0, v3, Lflipboard/gui/section/GroupFranchiseMeta;->b:Ljava/lang/String;

    .line 2291
    iget-object v0, p1, Lflipboard/objs/FeedItem;->e:Ljava/lang/String;

    iput-object v0, v3, Lflipboard/gui/section/GroupFranchiseMeta;->d:Ljava/lang/String;

    .line 2292
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 2293
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 2294
    iget-object v4, v0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/gui/section/GroupFranchiseMeta;->c:Ljava/lang/String;

    .line 2295
    iget-object v0, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    iput-object v0, v3, Lflipboard/gui/section/GroupFranchiseMeta;->g:Ljava/lang/String;

    .line 2297
    :cond_2
    iput v7, v3, Lflipboard/gui/section/GroupFranchiseMeta;->e:I

    .line 2298
    add-int/lit8 v0, v7, 0x1

    .line 2299
    iput-object v3, v2, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    .line 2300
    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    move v7, v0

    .line 2302
    goto :goto_1

    .line 2303
    :cond_3
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    .line 2304
    iget-object v0, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    iput v2, v0, Lflipboard/gui/section/GroupFranchiseMeta;->f:I

    goto :goto_3

    .line 2306
    :cond_4
    return-object v9

    :cond_5
    move v0, v7

    goto :goto_2
.end method

.method private a(ILflipboard/objs/FeedItem;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 884
    sget-boolean v0, Lflipboard/util/DuplicateOccurrenceLog;->a:Z

    if-eqz v0, :cond_1

    .line 885
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 886
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " on page "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", sectionHasGap: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lflipboard/gui/section/SectionFragment;->A:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 887
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v1, "Adding ungrouped item that already exist"

    invoke-direct {v6, v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 888
    invoke-virtual {v6}, Ljava/lang/Exception;->fillInStackTrace()Ljava/lang/Throwable;

    .line 889
    const/4 v1, 0x0

    .line 890
    const/4 v2, 0x0

    .line 891
    new-instance v4, Ljava/util/ArrayList;

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v0, v0, Lflipboard/io/NetworkManager;->d:Ljava/util/List;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 892
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_0
    if-ltz v3, :cond_0

    if-nez v1, :cond_0

    .line 893
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/RequestLogEntry;

    .line 894
    iget-object v5, v0, Lflipboard/io/RequestLogEntry;->b:Ljava/lang/String;

    const-string v7, "updateFeed"

    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 895
    const/4 v1, 0x1

    .line 892
    :goto_1
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    move-object v2, v0

    goto :goto_0

    .line 899
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 900
    new-instance v5, Ljava/util/ArrayList;

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 901
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->i:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 902
    new-instance v0, Lflipboard/gui/section/SectionFragment$8;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lflipboard/gui/section/SectionFragment$8;-><init>(Lflipboard/gui/section/SectionFragment;Lflipboard/io/RequestLogEntry;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    invoke-static {v6, v0}, Lnet/hockeyapp/android/ExceptionHandler;->a(Ljava/lang/Throwable;Lnet/hockeyapp/android/CrashManagerListener;)V

    .line 942
    :cond_1
    return-void

    :cond_2
    move-object v0, v2

    goto :goto_1
.end method

.method private a(Landroid/content/Intent;Lflipboard/objs/FeedItem;Landroid/view/View;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/16 v3, 0x4e42

    const/4 v2, 0x0

    .line 1876
    invoke-static {p2}, Lflipboard/usage/UsageTracker;->a(Lflipboard/objs/FeedItem;)V

    .line 1877
    const-string v0, "sid"

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1878
    const-string v0, "extra_current_item"

    iget-object v1, p2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1879
    const-string v0, "extra_opened_from_section_fragment"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1883
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    if-eqz p3, :cond_1

    .line 1884
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1885
    invoke-virtual {p3, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1886
    invoke-virtual {p3}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-static {p3, v2, v2, v0, v1}, Landroid/app/ActivityOptions;->makeScaleUpAnimation(Landroid/view/View;IIII)Landroid/app/ActivityOptions;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 1887
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, p1, v3, v0}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 1894
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/gui/section/SectionFragment;->af:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/gui/section/SectionFragment;->af:I

    .line 1895
    return-void

    .line 1889
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, p1, v3}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1890
    iget-object v0, p2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "image"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1891
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f040001

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->overridePendingTransition(II)V

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/gui/section/SectionFragment;Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 123
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lflipboard/gui/section/SectionFragment;->b(Lflipboard/objs/FeedItem;)Lflipboard/gui/section/Group;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->ax:Lflipboard/gui/section/Group;

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ax:Lflipboard/gui/section/Group;

    if-nez v0, :cond_1

    sget-object v0, Lflipboard/gui/section/SectionFragment;->a:Lflipboard/util/Log;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "synthetic-client-profile-page"

    invoke-direct {p0, v0}, Lflipboard/gui/section/SectionFragment;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/SectionFragment$6;

    invoke-direct {v1, p0}, Lflipboard/gui/section/SectionFragment$6;-><init>(Lflipboard/gui/section/SectionFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/SectionFragment$7;

    invoke-direct {v1, p0}, Lflipboard/gui/section/SectionFragment$7;-><init>(Lflipboard/gui/section/SectionFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/gui/section/SectionFragment;Z)V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/SectionFragment$4;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/section/SectionFragment$4;-><init>(Lflipboard/gui/section/SectionFragment;Z)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Lflipboard/io/UsageEvent;)V
    .locals 2

    .prologue
    .line 1685
    const-string v0, "prominenceOverride"

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1686
    return-void
.end method

.method private a(Lflipboard/objs/FeedItem;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 843
    .line 845
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    move v3, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    .line 846
    add-int/lit8 v1, v1, 0x1

    .line 847
    iget-object v6, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 849
    const-string v0, "groupedItems"

    invoke-direct {p0, v1, p1, v0}, Lflipboard/gui/section/SectionFragment;->a(ILflipboard/objs/FeedItem;Ljava/lang/String;)V

    move v3, v4

    goto :goto_0

    .line 850
    :cond_0
    iget-object v6, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    if-eqz v6, :cond_a

    iget-object v6, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v6, v6, Lflipboard/gui/section/GroupFranchiseMeta;->a:Lflipboard/objs/FeedItem;

    if-eqz v6, :cond_a

    iget-object v0, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v0, v0, Lflipboard/gui/section/GroupFranchiseMeta;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, p1}, Lflipboard/objs/FeedItem;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 852
    const-string v0, "groupedItems-franchise-item"

    invoke-direct {p0, v1, p1, v0}, Lflipboard/gui/section/SectionFragment;->a(ILflipboard/objs/FeedItem;Ljava/lang/String;)V

    move v0, v4

    :goto_1
    move v3, v0

    .line 854
    goto :goto_0

    .line 855
    :cond_1
    if-nez v3, :cond_2

    .line 856
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/ListSingleThreadWrapper;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 857
    if-eqz v3, :cond_2

    .line 858
    const/4 v0, -0x1

    const-string v1, "ungroupedItems"

    invoke-direct {p0, v0, p1, v1}, Lflipboard/gui/section/SectionFragment;->a(ILflipboard/objs/FeedItem;Ljava/lang/String;)V

    .line 861
    :cond_2
    if-nez v3, :cond_4

    iget-object v0, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 862
    sget-object v0, Lflipboard/gui/section/SectionFragment;->b:Ljava/util/List;

    iget-object v1, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 863
    iget-object v0, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "status"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->D()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    iget-object v0, p1, Lflipboard/objs/FeedItem;->aB:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lflipboard/objs/FeedItem;->aB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 864
    :cond_3
    :goto_2
    if-eqz v4, :cond_7

    .line 865
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Got empty status update"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 866
    const-string v0, "unwanted.empty_status"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    .line 876
    :cond_4
    :goto_3
    if-eqz v3, :cond_5

    .line 877
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-nez v0, :cond_5

    invoke-static {}, Lflipboard/io/UsageEvent;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 878
    const-string v0, "unwanted.SectionFragment_duplicate_items_detected"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    .line 881
    :cond_5
    return-void

    :cond_6
    move v4, v2

    .line 863
    goto :goto_2

    .line 867
    :cond_7
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->R()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->T()Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    if-nez v0, :cond_9

    .line 868
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Got empty image"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 869
    const-string v0, "unwanted.empty_image"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    goto :goto_3

    .line 871
    :cond_9
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/ListSingleThreadWrapper;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_a
    move v0, v3

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/FeedItem;Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 1797
    iget-object v1, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v2, "status"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1798
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_1

    .line 1799
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    sget-object v2, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->c:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    invoke-static {p2, v1, v0, p1, v2}, Lflipboard/util/SocialHelper;->a(Landroid/view/View;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/FeedItem;Lflipboard/objs/UsageEventV2$SocialCardNavFrom;)V

    .line 1872
    :cond_0
    :goto_0
    return-void

    .line 1801
    :cond_1
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    sget-object v2, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->c:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    invoke-static {p1, v1, v0, v2}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/UsageEventV2$SocialCardNavFrom;)V

    goto :goto_0

    .line 1805
    :cond_2
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v2, "detail_to_detail_on_phone"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1806
    if-nez v1, :cond_3

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_a

    .line 1807
    :cond_3
    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-class v1, Lflipboard/activities/DetailActivity;

    invoke-direct {v4, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1808
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1810
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v3

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    .line 1811
    iget-object v0, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_4
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1812
    iget-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->S()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v5, "section"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v5, "status"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1813
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->P()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1818
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->e()Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v1

    .line 1819
    if-eqz v1, :cond_4

    .line 1820
    invoke-virtual {v1, v2}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/section/SectionPage;

    .line 1821
    if-eqz v1, :cond_4

    .line 1822
    iget-object v1, v1, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1823
    instance-of v5, v1, Lflipboard/gui/section/item/AlbumItem;

    if-eqz v5, :cond_4

    .line 1824
    check-cast v1, Lflipboard/gui/section/item/AlbumItem;

    .line 1825
    invoke-virtual {v1}, Lflipboard/gui/section/item/AlbumItem;->getAlbumTemplate()Lflipboard/objs/SectionPageTemplate;

    move-result-object v1

    .line 1826
    if-eqz v1, :cond_4

    iget-object v5, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v5, :cond_4

    .line 1827
    invoke-virtual {v1}, Lflipboard/objs/SectionPageTemplate;->a()I

    move-result v1

    iget-object v5, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v1, v5}, Ljava/lang/Math;->min(II)I

    move-result v9

    move v5, v3

    .line 1828
    :goto_3
    if-ge v5, v9, :cond_4

    .line 1829
    iget-object v1, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    .line 1830
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1828
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_3

    .line 1836
    :cond_5
    iget-object v1, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 1837
    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1838
    :cond_6
    iget-object v1, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-eqz v1, :cond_4

    .line 1839
    iget-object v0, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1840
    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1844
    :cond_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 1845
    goto/16 :goto_1

    .line 1846
    :cond_8
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 1847
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1848
    const-string v1, "extra_content_discovery_from_source"

    sget-object v2, Lflipboard/gui/section/SectionFragment;->az:Landroid/os/Bundle;

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1849
    const-string v1, "extra_item_ids"

    invoke-virtual {v4, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1850
    const-string v0, "sid"

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object v0, v4

    .line 1864
    :goto_5
    if-eqz v0, :cond_0

    .line 1865
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    if-eqz v1, :cond_9

    .line 1866
    const-string v1, "pages_since_last_ad"

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    iget v2, v2, Lflipboard/service/FLAdManager;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1868
    :cond_9
    invoke-direct {p0, v0, p1, p2}, Lflipboard/gui/section/SectionFragment;->a(Landroid/content/Intent;Lflipboard/objs/FeedItem;Landroid/view/View;)V

    goto/16 :goto_0

    .line 1852
    :cond_a
    iget-object v1, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v2, "video"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1853
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p1, v2, v0}, Lflipboard/util/VideoUtil;->a(Landroid/app/Activity;Lflipboard/objs/FeedItem;Ljava/lang/String;Z)V

    .line 1854
    iget v0, p0, Lflipboard/gui/section/SectionFragment;->af:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/gui/section/SectionFragment;->af:I

    .line 1855
    const/4 v0, 0x0

    goto :goto_5

    .line 1857
    :cond_b
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, p1}, Lflipboard/util/MeteringHelper;->b(Landroid/content/Context;Lflipboard/objs/FeedItem;)Lflipboard/util/MeteringHelper$AccessType;

    move-result-object v1

    .line 1858
    iget-object v2, p1, Lflipboard/objs/FeedItem;->W:Ljava/lang/String;

    if-eqz v2, :cond_c

    sget-object v2, Lflipboard/util/MeteringHelper$AccessType;->c:Lflipboard/util/MeteringHelper$AccessType;

    if-eq v1, v2, :cond_c

    move v3, v0

    .line 1859
    :cond_c
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v3, :cond_d

    const-class v0, Lflipboard/activities/DetailActivity;

    :goto_6
    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1860
    const-string v0, "extra_content_discovery_from_source"

    sget-object v2, Lflipboard/gui/section/SectionFragment;->az:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1861
    const-string v0, "sid"

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object v0, v1

    goto :goto_5

    .line 1859
    :cond_d
    const-class v0, Lflipboard/activities/DetailActivityStayOnRotation;

    goto :goto_6
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 2105
    if-eqz p1, :cond_1

    .line 2107
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->aj:Ljava/util/HashMap;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->aq:Lflipboard/gui/section/Group;

    iget-object v1, v1, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v1, v1, Lflipboard/gui/section/GroupFranchiseMeta;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/FLAdManager;

    .line 2108
    if-eqz v0, :cond_1

    .line 2109
    iget v1, p0, Lflipboard/gui/section/SectionFragment;->q:I

    .line 2110
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v2}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v2

    if-le v2, v1, :cond_0

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v2, :cond_0

    .line 2113
    invoke-virtual {v0, v1}, Lflipboard/service/FLAdManager;->a(I)V

    .line 2114
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FLAdManager;->a(ILflipboard/objs/Ad;)V

    .line 2116
    :cond_0
    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FLAdManager;->a(IZ)V

    .line 2120
    :cond_1
    const-string v0, "change_page"

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->a(Ljava/lang/String;)V

    .line 2121
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/SectionFragment;)Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->am:Z

    return v0
.end method

.method static synthetic a(Lflipboard/gui/section/SectionFragment;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lflipboard/gui/section/SectionFragment;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lflipboard/gui/section/SectionFragment;Ljava/util/List;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0, p1, p2}, Lflipboard/gui/section/SectionFragment;->a(Ljava/util/List;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/util/List;Ljava/lang/String;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SidebarGroup;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 2673
    const/4 v0, 0x0

    .line 2674
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ah:Z

    if-eqz v1, :cond_6

    .line 2675
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 2676
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 2677
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    .line 2678
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v0

    move-object v2, v0

    .line 2679
    :goto_1
    if-eqz v2, :cond_1

    iget-object v0, v2, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2680
    iget v0, v2, Lflipboard/objs/SidebarGroup$RenderHints;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2681
    const/4 v1, 0x1

    .line 2682
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Duplicate sidebarGroup type "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " at pageIndex: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v2, Lflipboard/objs/SidebarGroup$RenderHints;->c:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 2683
    const-string v0, "Duplicate sidebarGroup type "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v6, " at pageIndex: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget v6, v2, Lflipboard/objs/SidebarGroup$RenderHints;->c:I

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 2684
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 2686
    :cond_0
    iget v0, v2, Lflipboard/objs/SidebarGroup$RenderHints;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    move v0, v1

    move v1, v0

    .line 2688
    goto :goto_0

    .line 2678
    :cond_2
    const/4 v0, 0x0

    move-object v2, v0

    goto :goto_1

    .line 2690
    :cond_3
    if-eqz v1, :cond_5

    .line 2691
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    invoke-direct {p0, v0, p2}, Lflipboard/gui/section/SectionFragment;->a(Ljava/util/List;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2692
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Duplicate sidebarGroup type "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " in section: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2693
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2694
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 2696
    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Caught: Duplicate sidebarGroup in section"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    move-result-object v0

    new-instance v2, Lflipboard/gui/section/SectionFragment$18;

    invoke-direct {v2, p0, v4}, Lflipboard/gui/section/SectionFragment$18;-><init>(Lflipboard/gui/section/SectionFragment;Ljava/lang/StringBuffer;)V

    invoke-static {v0, v2}, Lnet/hockeyapp/android/ExceptionHandler;->a(Ljava/lang/Throwable;Lnet/hockeyapp/android/CrashManagerListener;)V

    .line 2705
    :cond_5
    :goto_2
    return v1

    :cond_6
    move v1, v0

    goto :goto_2
.end method

.method private b(Lflipboard/objs/FeedItem;)Lflipboard/gui/section/Group;
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 1191
    .line 1192
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 1193
    if-eqz v1, :cond_1

    .line 1194
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v2, "enable_tablet_profiles"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1195
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v2}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v1, :cond_2

    :cond_0
    move v1, v5

    .line 1197
    :goto_0
    if-eqz v1, :cond_1

    .line 1198
    new-instance v1, Lflipboard/objs/FeedItem;

    invoke-direct {v1}, Lflipboard/objs/FeedItem;-><init>()V

    .line 1199
    const-string v2, "synthetic-client-profile-page"

    iput-object v2, v1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 1201
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "synthetic-client-profile-page_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    .line 1202
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v2}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1203
    new-instance v4, Lflipboard/gui/section/Group;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-static {}, Lflipboard/gui/section/SectionFragment;->k()Lflipboard/objs/SectionPageTemplate;

    move-result-object v3

    invoke-direct {v4, v2, v3, v1, v0}, Lflipboard/gui/section/Group;-><init>(Lflipboard/service/Section;Lflipboard/objs/SectionPageTemplate;Lflipboard/objs/FeedItem;Z)V

    .line 1222
    :cond_1
    :goto_1
    return-object v4

    :cond_2
    move v1, v0

    .line 1195
    goto :goto_0

    .line 1205
    :cond_3
    new-instance v3, Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1206
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1207
    new-instance v1, Lflipboard/objs/FeedItem;

    invoke-direct {v1}, Lflipboard/objs/FeedItem;-><init>()V

    .line 1208
    const-string v2, "synthetic-client-profile-summary-item"

    iput-object v2, v1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 1209
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "synthetic-client-profile-summary-item_"

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    .line 1210
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1211
    if-eqz p1, :cond_5

    iget-object v1, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1212
    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1217
    :cond_4
    :goto_2
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v5, :cond_6

    .line 1218
    :goto_3
    new-instance v0, Lflipboard/gui/section/Group;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-static {}, Lflipboard/util/AndroidUtil;->a()Lflipboard/objs/SectionPageTemplate;

    move-result-object v2

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->g()I

    move-result v6

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->h()I

    move-result v7

    invoke-direct/range {v0 .. v7}, Lflipboard/gui/section/Group;-><init>(Lflipboard/service/Section;Lflipboard/objs/SectionPageTemplate;Ljava/util/List;Lflipboard/objs/SidebarGroup;ZII)V

    move-object v4, v0

    goto :goto_1

    .line 1213
    :cond_5
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1214
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    move v5, v0

    .line 1217
    goto :goto_3
.end method

.method static synthetic b(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/SectionScrubber;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    return-object v0
.end method

.method private b(Lflipboard/gui/section/Group;)V
    .locals 4

    .prologue
    .line 760
    iget-object v0, p1, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    iget-object v1, p1, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v1, v1, Lflipboard/gui/section/GroupFranchiseMeta;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 763
    :cond_0
    iget-object v0, p1, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 764
    const-string v2, "list"

    iget-object v3, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 765
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-virtual {v2, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->removeAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 767
    :cond_1
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v2, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 772
    :cond_2
    return-void
.end method

.method static synthetic c(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/LoadingPage;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->J:Lflipboard/gui/section/LoadingPage;

    return-object v0
.end method

.method private c(Lflipboard/gui/section/Group;)Lflipboard/gui/section/SectionPage;
    .locals 2

    .prologue
    .line 1527
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lflipboard/gui/section/SectionFragment;->a(Landroid/content/Context;Lflipboard/gui/section/Group;)Lflipboard/gui/section/SectionPage;

    move-result-object v0

    .line 1528
    iget-boolean v1, p1, Lflipboard/gui/section/Group;->e:Z

    if-eqz v1, :cond_1

    .line 1529
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1530
    invoke-virtual {v0}, Lflipboard/gui/section/SectionPage;->d()V

    .line 1532
    :cond_0
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionPage;->a(Lflipboard/service/Section;)V

    .line 1534
    :cond_1
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->y()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1535
    invoke-virtual {v0}, Lflipboard/gui/section/SectionPage;->g()V

    .line 1537
    :cond_2
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionPage;->setScrubber(Lflipboard/gui/section/SectionScrubber;)V

    .line 1538
    iget-boolean v1, p0, Lflipboard/gui/section/SectionFragment;->C:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionPage;->setIsOpenedFromThirdParty(Z)V

    .line 1539
    invoke-virtual {v0}, Lflipboard/gui/section/SectionPage;->f()V

    .line 1540
    invoke-virtual {v0}, Lflipboard/gui/section/SectionPage;->h()V

    .line 1541
    return-object v0
.end method

.method private d(Lflipboard/gui/section/Group;)V
    .locals 5

    .prologue
    .line 2125
    iget-object v0, p1, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    if-eqz v0, :cond_1

    .line 2126
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "franchise"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->ap:Lflipboard/io/UsageEvent;

    .line 2127
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment;->aq:Lflipboard/gui/section/Group;

    .line 2128
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ap:Lflipboard/io/UsageEvent;

    const-string v1, "action"

    const-string v2, "displayed"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2129
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ap:Lflipboard/io/UsageEvent;

    const-string v1, "sectionIdentifier"

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2130
    iget-object v0, p1, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget v0, v0, Lflipboard/gui/section/GroupFranchiseMeta;->f:I

    if-lez v0, :cond_0

    .line 2131
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ap:Lflipboard/io/UsageEvent;

    const-string v1, "numPages"

    iget-object v2, p1, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget v2, v2, Lflipboard/gui/section/GroupFranchiseMeta;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2133
    :cond_0
    iget-object v0, p1, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v1, v0, Lflipboard/gui/section/GroupFranchiseMeta;->a:Lflipboard/objs/FeedItem;

    .line 2134
    iget-object v0, v1, Lflipboard/objs/FeedItem;->cn:Lflipboard/json/FLObject;

    if-eqz v0, :cond_1

    .line 2135
    iget-object v0, v1, Lflipboard/objs/FeedItem;->cn:Lflipboard/json/FLObject;

    invoke-virtual {v0}, Lflipboard/json/FLObject;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2136
    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->ap:Lflipboard/io/UsageEvent;

    iget-object v4, v1, Lflipboard/objs/FeedItem;->cn:Lflipboard/json/FLObject;

    invoke-virtual {v4, v0}, Lflipboard/json/FLObject;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 2140
    :cond_1
    return-void
.end method

.method static synthetic d(Lflipboard/gui/section/SectionFragment;)V
    .locals 2

    .prologue
    .line 123
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/SectionFragment$5;

    invoke-direct {v1, p0}, Lflipboard/gui/section/SectionFragment$5;-><init>(Lflipboard/gui/section/SectionFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method private d(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 823
    .line 824
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 825
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0, v2}, Lflipboard/gui/section/ListSingleThreadWrapper;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    .line 826
    iget-object v1, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    .line 827
    :goto_0
    if-ge v3, v4, :cond_0

    if-nez v2, :cond_0

    .line 828
    iget-object v1, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    .line 829
    iget-object v1, v1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 830
    const/4 v1, 0x1

    .line 827
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_0

    .line 834
    :cond_0
    return v2

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method static synthetic e(Lflipboard/gui/section/SectionFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->E:Ljava/util/ArrayList;

    return-object v0
.end method

.method private f(I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 4153
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4205
    :cond_0
    :goto_0
    return-void

    .line 4158
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    if-nez v0, :cond_6

    .line 4159
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->M:Ljava/lang/Object;

    monitor-enter v2

    .line 4160
    :try_start_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    if-nez v0, :cond_5

    .line 4163
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;)Lflipboard/service/FLAdManager;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    .line 4164
    const/4 v0, 0x1

    .line 4166
    :goto_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4169
    :goto_2
    const/4 v2, 0x0

    .line 4170
    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->O:Lflipboard/util/Observer;

    if-nez v3, :cond_3

    .line 4171
    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->M:Ljava/lang/Object;

    monitor-enter v3

    .line 4172
    :try_start_1
    iget-object v4, p0, Lflipboard/gui/section/SectionFragment;->O:Lflipboard/util/Observer;

    if-nez v4, :cond_2

    .line 4175
    new-instance v2, Lflipboard/gui/section/SectionFragment$32;

    invoke-direct {v2, p0}, Lflipboard/gui/section/SectionFragment$32;-><init>(Lflipboard/gui/section/SectionFragment;)V

    .line 4193
    iput-object v2, p0, Lflipboard/gui/section/SectionFragment;->O:Lflipboard/util/Observer;

    .line 4195
    :cond_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 4198
    :cond_3
    if-eqz v2, :cond_4

    .line 4199
    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    invoke-virtual {v3, v2}, Lflipboard/service/FLAdManager;->b(Lflipboard/util/Observer;)V

    .line 4202
    :cond_4
    if-eqz v0, :cond_0

    .line 4203
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    invoke-virtual {v0, p1, v1}, Lflipboard/service/FLAdManager;->a(II)V

    goto :goto_0

    .line 4166
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 4195
    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method static synthetic f(Lflipboard/gui/section/SectionFragment;)V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragment;->r()V

    return-void
.end method

.method static synthetic g(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/Group;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ax:Lflipboard/gui/section/Group;

    return-object v0
.end method

.method static synthetic h(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/flipping/FlipTransitionViews$HopTask;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->aB:Lflipboard/gui/flipping/FlipTransitionViews$HopTask;

    return-object v0
.end method

.method static synthetic i(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/flipping/FlipTransitionViews$HopTask;
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->aB:Lflipboard/gui/flipping/FlipTransitionViews$HopTask;

    return-object v0
.end method

.method static synthetic j(Lflipboard/gui/section/SectionFragment;)Lflipboard/service/FLAdManager;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    return-object v0
.end method

.method static k()Lflipboard/objs/SectionPageTemplate;
    .locals 2

    .prologue
    .line 4323
    sget-object v0, Lflipboard/gui/section/SectionFragment;->P:Lflipboard/objs/SectionPageTemplate;

    if-nez v0, :cond_0

    .line 4324
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const-string v1, "Backup"

    invoke-virtual {v0, v1}, Lflipboard/app/FlipboardApplication;->a(Ljava/lang/String;)Lflipboard/objs/SectionPageTemplate;

    move-result-object v0

    sput-object v0, Lflipboard/gui/section/SectionFragment;->P:Lflipboard/objs/SectionPageTemplate;

    .line 4327
    :cond_0
    sget-object v0, Lflipboard/gui/section/SectionFragment;->P:Lflipboard/objs/SectionPageTemplate;

    return-object v0
.end method

.method static synthetic k(Lflipboard/gui/section/SectionFragment;)Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->R:Z

    return v0
.end method

.method static synthetic l(Lflipboard/gui/section/SectionFragment;)I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lflipboard/gui/section/SectionFragment;->q:I

    return v0
.end method

.method static synthetic m(Lflipboard/gui/section/SectionFragment;)V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragment;->t()V

    return-void
.end method

.method static synthetic n(Lflipboard/gui/section/SectionFragment;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->K:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic o(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/Group;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->av:Lflipboard/gui/section/Group;

    return-object v0
.end method

.method static synthetic p(Lflipboard/gui/section/SectionFragment;)I
    .locals 2

    .prologue
    .line 123
    iget v0, p0, Lflipboard/gui/section/SectionFragment;->q:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lflipboard/gui/section/SectionFragment;->q:I

    return v0
.end method

.method static synthetic q(Lflipboard/gui/section/SectionFragment;)V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragment;->x()V

    return-void
.end method

.method private r()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 693
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->C()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->Y:Lflipboard/objs/UsageEventV2;

    if-nez v0, :cond_4

    .line 694
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->n:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v3, Lflipboard/objs/UsageEventV2$EventCategory;->a:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v3}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->Y:Lflipboard/objs/UsageEventV2;

    .line 695
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->Y:Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v3, v3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 699
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    .line 700
    iget-object v3, v0, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    const-string v4, "magazine"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v0, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    const-string v4, "contributor"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 701
    :cond_1
    iget-object v0, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 702
    const/4 v0, 0x1

    .line 710
    :goto_0
    if-eqz v0, :cond_6

    .line 711
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->A()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 712
    const-string v0, "self_with_magazines"

    .line 723
    :goto_1
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->Y:Lflipboard/objs/UsageEventV2;

    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v1, v3, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 726
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 727
    const-string v1, "extra_content_discovery_from_source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 728
    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 729
    const/4 v1, 0x0

    .line 731
    invoke-static {}, Lflipboard/objs/UsageEventV2$SectionNavFrom;->values()[Lflipboard/objs/UsageEventV2$SectionNavFrom;

    move-result-object v4

    array-length v5, v4

    :goto_2
    if-ge v2, v5, :cond_b

    aget-object v0, v4, v2

    .line 732
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2$SectionNavFrom;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 738
    :goto_3
    if-nez v0, :cond_2

    .line 739
    const-string v1, "masthead"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 740
    sget-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->g:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    .line 748
    :cond_2
    :goto_4
    if-eqz v0, :cond_3

    .line 749
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->Y:Lflipboard/objs/UsageEventV2;

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v1, v2, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 752
    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->Y:Lflipboard/objs/UsageEventV2;

    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 754
    :cond_4
    return-void

    .line 714
    :cond_5
    const-string v0, "other_with_magazines"

    goto :goto_1

    .line 717
    :cond_6
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->A()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 718
    const-string v0, "self_without_magazines"

    goto :goto_1

    .line 720
    :cond_7
    const-string v0, "other_without_magazines"

    goto :goto_1

    .line 731
    :cond_8
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 741
    :cond_9
    const-string v1, "contentGuide"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 742
    sget-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->h:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    goto :goto_4

    .line 743
    :cond_a
    const-string v1, "sectionItem"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 744
    sget-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->i:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    goto :goto_4

    :cond_b
    move-object v0, v1

    goto :goto_3

    :cond_c
    move v0, v2

    goto :goto_0
.end method

.method static synthetic r(Lflipboard/gui/section/SectionFragment;)Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->ar:Z

    return v0
.end method

.method private s()V
    .locals 3

    .prologue
    .line 1791
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/SectionTabletActivity;

    .line 1792
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "reset"

    invoke-virtual {v0, v1, v2}, Lflipboard/activities/SectionTabletActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1793
    return-void
.end method

.method static synthetic s(Lflipboard/gui/section/SectionFragment;)Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->L:Z

    return v0
.end method

.method private t()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2164
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "SectionFragment:tryNukeOldPages"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 2165
    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->A:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lflipboard/gui/section/SectionFragment;->ao:I

    iget v1, p0, Lflipboard/gui/section/SectionFragment;->q:I

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getRunningFlips()I

    move-result v0

    if-nez v0, :cond_2

    .line 2166
    sget-object v0, Lflipboard/gui/section/SectionFragment;->a:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Section had a gap before index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lflipboard/gui/section/SectionFragment;->ao:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nuking "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v1

    iget v2, p0, Lflipboard/gui/section/SectionFragment;->ao:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pages"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2167
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragment;->x()V

    .line 2168
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v0

    iget v1, p0, Lflipboard/gui/section/SectionFragment;->ao:I

    if-le v0, v1, :cond_3

    .line 2169
    sget-object v0, Lflipboard/gui/section/SectionFragment;->a:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Removing page at index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2170
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v0

    if-lez v0, :cond_1

    .line 2171
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->c(I)V

    .line 2172
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    .line 2173
    iget-object v0, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 2174
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->at:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2179
    :cond_1
    const-string v0, "unwanted.fewer_flippable_views_than_groups_when_nuking"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    .line 2180
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragment;->s()V

    .line 2201
    :cond_2
    :goto_1
    return-void

    .line 2184
    :cond_3
    const-string v0, "nuking"

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->b(Ljava/lang/String;)V

    .line 2185
    sget-object v0, Lflipboard/gui/section/SectionFragment;->a:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Pages left after nuke: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2186
    iput-boolean v3, p0, Lflipboard/gui/section/SectionFragment;->A:Z

    .line 2187
    iput v3, p0, Lflipboard/gui/section/SectionFragment;->ao:I

    .line 2188
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2189
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->E:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    .line 2190
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->E:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2192
    :cond_4
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2193
    invoke-virtual {p0, v0, v3}, Lflipboard/gui/section/SectionFragment;->a(Ljava/util/List;Z)V

    .line 2194
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2195
    const/4 v0, 0x1

    invoke-virtual {p0, v3, v0}, Lflipboard/gui/section/SectionFragment;->a(ZZ)V

    goto :goto_1

    .line 2197
    :cond_5
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iput-boolean v3, v0, Lflipboard/service/Section;->i:Z

    .line 2198
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0, v3, v4, v4}, Lflipboard/service/Section;->a(ZLjava/lang/String;Landroid/os/Bundle;)Z

    goto :goto_1
.end method

.method static synthetic t(Lflipboard/gui/section/SectionFragment;)Z
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->L:Z

    return v0
.end method

.method static synthetic u(Lflipboard/gui/section/SectionFragment;)Lflipboard/activities/FlipboardActivity;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    return-object v0
.end method

.method private u()V
    .locals 2

    .prologue
    .line 4082
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    if-eqz v0, :cond_1

    .line 4083
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionScrubber;->setLoading(Z)V

    .line 4089
    :cond_0
    :goto_0
    return-void

    .line 4085
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->e()Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v0

    .line 4086
    if-eqz v0, :cond_0

    .line 4087
    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->an:Z

    if-nez v0, :cond_0

    .line 4088
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragment;->w()V

    goto :goto_0
.end method

.method static synthetic v(Lflipboard/gui/section/SectionFragment;)Lflipboard/service/audio/FLAudioManager;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->u:Lflipboard/service/audio/FLAudioManager;

    return-object v0
.end method

.method private v()V
    .locals 3

    .prologue
    .line 4297
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    const-string v0, "clearAdPagesAndReinitAds"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 4300
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->e()Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v1

    .line 4301
    if-eqz v1, :cond_2

    .line 4302
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_2

    .line 4303
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v2, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lflipboard/gui/section/Group$AdGroup;

    if-eqz v2, :cond_0

    .line 4304
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v2, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->remove(I)Ljava/lang/Object;

    .line 4306
    :cond_0
    invoke-virtual {v1, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, Lflipboard/gui/section/SectionAdPage;

    if-eqz v2, :cond_1

    .line 4307
    invoke-virtual {v1, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->c(I)V

    .line 4302
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 4312
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_3

    .line 4313
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    .line 4314
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lflipboard/service/FLAdManager;->a(II)V

    .line 4317
    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->e(I)V

    .line 4319
    :cond_3
    return-void
.end method

.method static synthetic w(Lflipboard/gui/section/SectionFragment;)Lflipboard/activities/FlipboardActivity;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    return-object v0
.end method

.method private w()V
    .locals 4

    .prologue
    .line 4460
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->an:Z

    if-nez v0, :cond_2

    .line 4461
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->J:Lflipboard/gui/section/LoadingPage;

    if-nez v0, :cond_0

    .line 4462
    new-instance v0, Lflipboard/gui/section/LoadingPage;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-boolean v3, p0, Lflipboard/gui/section/SectionFragment;->D:Z

    invoke-direct {v0, v1, v2, v3}, Lflipboard/gui/section/LoadingPage;-><init>(Landroid/content/Context;Lflipboard/service/Section;Z)V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->J:Lflipboard/gui/section/LoadingPage;

    .line 4463
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->J:Lflipboard/gui/section/LoadingPage;

    iget-boolean v1, p0, Lflipboard/gui/section/SectionFragment;->C:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/section/LoadingPage;->setIsOpenedFromThirdParty(Z)V

    .line 4464
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->J:Lflipboard/gui/section/LoadingPage;

    invoke-virtual {v0}, Lflipboard/gui/section/LoadingPage;->f()V

    .line 4466
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->J:Lflipboard/gui/section/LoadingPage;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/section/LoadingPage$1;

    invoke-direct {v2, v0}, Lflipboard/gui/section/LoadingPage$1;-><init>(Lflipboard/gui/section/LoadingPage;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 4467
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->J:Lflipboard/gui/section/LoadingPage;

    invoke-virtual {v0}, Lflipboard/gui/section/LoadingPage;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 4468
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->J:Lflipboard/gui/section/LoadingPage;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    .line 4470
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->an:Z

    .line 4472
    :cond_2
    return-void
.end method

.method private x()V
    .locals 2

    .prologue
    .line 4475
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_1

    .line 4476
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->J:Lflipboard/gui/section/LoadingPage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->J:Lflipboard/gui/section/LoadingPage;

    invoke-virtual {v0}, Lflipboard/gui/section/LoadingPage;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4477
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->J:Lflipboard/gui/section/LoadingPage;

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->c(Landroid/view/View;)V

    .line 4480
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    check-cast v0, Lflipboard/gui/section/SectionTabletView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionTabletView;->u()V

    .line 4481
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->an:Z

    .line 4483
    :cond_1
    return-void
.end method

.method static synthetic x(Lflipboard/gui/section/SectionFragment;)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 123
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ah:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v0

    move v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    instance-of v3, v0, Lflipboard/gui/section/Group$AdGroup;

    if-nez v3, :cond_3

    iget-object v0, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Duplicate: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") on page "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " and "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " while inserting page "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v3}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    sget-object v8, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Duplicate in section list: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, v1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    const/4 v3, 0x1

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lflipboard/activities/FlipboardActivity;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Duplicate item on page: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " and "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    move v1, v3

    :goto_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :cond_2
    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "Seen item id: "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " ("

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v7, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ") on page "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    :cond_4
    move v1, v0

    :cond_5
    if-nez v1, :cond_6

    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    const-string v2, "No duplicates detected"

    invoke-static {v0, v2}, Lflipboard/gui/FLToast;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    :cond_6
    return v1
.end method

.method private y()Lflipboard/gui/section/Group;
    .locals 5

    .prologue
    .line 4564
    new-instance v0, Lflipboard/objs/FeedItem;

    invoke-direct {v0}, Lflipboard/objs/FeedItem;-><init>()V

    .line 4565
    const-string v1, "synthetic-client-app-cover"

    iput-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 4566
    const-string v1, "synthetic-client-app-cover"

    iput-object v1, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    .line 4567
    new-instance v1, Lflipboard/gui/section/Group;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-static {}, Lflipboard/gui/section/SectionFragment;->k()Lflipboard/objs/SectionPageTemplate;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v0, v4}, Lflipboard/gui/section/Group;-><init>(Lflipboard/service/Section;Lflipboard/objs/SectionPageTemplate;Lflipboard/objs/FeedItem;Z)V

    .line 4568
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/model/ConfigSetting;->EnableCoverStoriesInterstitials:Z

    if-eqz v0, :cond_0

    .line 4569
    const/4 v0, 0x1

    iput-boolean v0, v1, Lflipboard/gui/section/Group;->f:Z

    .line 4571
    :cond_0
    return-object v1
.end method

.method static synthetic y(Lflipboard/gui/section/SectionFragment;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->aj:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic z(Lflipboard/gui/section/SectionFragment;)Z
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->x:Z

    return v0
.end method


# virtual methods
.method final a(Ljava/util/List;Ljava/util/List;Ljava/util/List;IIZ)Lflipboard/gui/section/Group;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/section/Group;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SidebarGroup;",
            ">;IIZ)",
            "Lflipboard/gui/section/Group;"
        }
    .end annotation

    .prologue
    .line 2944
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2945
    const/4 v2, 0x0

    .line 3030
    :cond_0
    :goto_0
    return-object v2

    .line 2947
    :cond_1
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v2}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    const/4 v7, 0x1

    .line 2948
    :goto_1
    sget-object v2, Lflipboard/activities/SettingsFragment;->f:Lflipboard/objs/SectionPageTemplate;

    if-eqz v2, :cond_4

    .line 2949
    sget-object v2, Lflipboard/activities/SettingsFragment;->f:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v2}, Lflipboard/objs/SectionPageTemplate;->a()I

    move-result v2

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    if-gt v2, v3, :cond_4

    .line 2950
    new-instance v2, Lflipboard/gui/section/Group;

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    sget-object v4, Lflipboard/activities/SettingsFragment;->f:Lflipboard/objs/SectionPageTemplate;

    const/4 v6, 0x0

    move-object/from16 v5, p1

    move/from16 v8, p4

    move/from16 v9, p5

    invoke-direct/range {v2 .. v9}, Lflipboard/gui/section/Group;-><init>(Lflipboard/service/Section;Lflipboard/objs/SectionPageTemplate;Ljava/util/List;Lflipboard/objs/SidebarGroup;ZII)V

    .line 2951
    iget-object v3, v2, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflipboard/objs/FeedItem;

    .line 2952
    const-string v5, "list"

    iget-object v6, v3, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2953
    iget-object v3, v3, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 2947
    :cond_2
    const/4 v7, 0x0

    goto :goto_1

    .line 2955
    :cond_3
    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2962
    :cond_4
    const/4 v9, 0x0

    .line 2963
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v2, v2, Lflipboard/objs/TOCSection;->E:Z

    if-eqz v2, :cond_8

    .line 2964
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v2, v2, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v2, :cond_5

    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const-string v3, "NYTMostEmailed"

    invoke-virtual {v2, v3}, Lflipboard/app/FlipboardApplication;->a(Ljava/lang/String;)Lflipboard/objs/SectionPageTemplate;

    move-result-object v4

    .line 2966
    :goto_3
    new-instance v2, Lflipboard/gui/section/Group;

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    const/4 v6, 0x0

    move-object/from16 v5, p1

    move/from16 v8, p4

    move/from16 v9, p5

    invoke-direct/range {v2 .. v9}, Lflipboard/gui/section/Group;-><init>(Lflipboard/service/Section;Lflipboard/objs/SectionPageTemplate;Ljava/util/List;Lflipboard/objs/SidebarGroup;ZII)V

    .line 2970
    const/4 v4, -0x1

    .line 2971
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move v5, v3

    :goto_4
    if-ltz v5, :cond_6

    const/4 v3, -0x1

    if-ne v4, v3, :cond_6

    .line 2972
    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflipboard/gui/section/Group;

    .line 2973
    iget v6, v3, Lflipboard/gui/section/Group;->i:I

    if-ltz v6, :cond_2c

    .line 2974
    iget v3, v3, Lflipboard/gui/section/Group;->i:I

    .line 2971
    :goto_5
    add-int/lit8 v4, v5, -0x1

    move v5, v4

    move v4, v3

    goto :goto_4

    .line 2964
    :cond_5
    invoke-static {}, Lflipboard/gui/section/SectionFragment;->k()Lflipboard/objs/SectionPageTemplate;

    move-result-object v4

    goto :goto_3

    .line 2977
    :cond_6
    add-int/lit8 v3, v4, 0x1

    iput v3, v2, Lflipboard/gui/section/Group;->i:I

    move-object v9, v2

    .line 3001
    :cond_7
    :goto_6
    if-nez v9, :cond_23

    if-eqz p6, :cond_23

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_23

    .line 3002
    new-instance v3, Lflipboard/gui/section/Group;

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-static {}, Lflipboard/gui/section/SectionFragment;->k()Lflipboard/objs/SectionPageTemplate;

    move-result-object v5

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/FeedItem;

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v2, v6}, Lflipboard/gui/section/Group;-><init>(Lflipboard/service/Section;Lflipboard/objs/SectionPageTemplate;Lflipboard/objs/FeedItem;Z)V

    move-object v2, v3

    goto/16 :goto_0

    .line 2980
    :cond_8
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/FeedItem;

    .line 2982
    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->P()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 2984
    iget-object v3, v2, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v3, :cond_9

    iget-object v3, v2, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 2985
    :cond_9
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2995
    :cond_a
    :goto_7
    if-nez v9, :cond_7

    .line 2996
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    const-string v3, "nytimes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    const-string v3, "ft"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    :cond_b
    const/4 v2, 0x1

    move v5, v2

    :goto_8
    if-eqz v5, :cond_13

    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v2, v2, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v2, :cond_13

    const/4 v2, 0x1

    move v3, v2

    :goto_9
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-object v2, v2, Lflipboard/app/FlipboardApplication;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_c
    :goto_a
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/SectionPageTemplate;

    iget-boolean v4, v2, Lflipboard/objs/SectionPageTemplate;->i:Z

    if-nez v4, :cond_c

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_f

    iget-boolean v4, v2, Lflipboard/objs/SectionPageTemplate;->l:Z

    if-eqz v4, :cond_d

    if-eqz v5, :cond_e

    :cond_d
    iget-boolean v4, v2, Lflipboard/objs/SectionPageTemplate;->m:Z

    if-eqz v4, :cond_14

    :cond_e
    const/4 v4, 0x1

    :goto_b
    if-eqz v4, :cond_c

    :cond_f
    if-eqz v3, :cond_10

    invoke-static {}, Lflipboard/gui/section/SectionFragment;->k()Lflipboard/objs/SectionPageTemplate;

    move-result-object v4

    if-eq v2, v4, :cond_c

    :cond_10
    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 2987
    :cond_11
    new-instance v9, Lflipboard/gui/section/Group;

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-static {}, Lflipboard/gui/section/SectionFragment;->k()Lflipboard/objs/SectionPageTemplate;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v9, v3, v4, v2, v5}, Lflipboard/gui/section/Group;-><init>(Lflipboard/service/Section;Lflipboard/objs/SectionPageTemplate;Lflipboard/objs/FeedItem;Z)V

    goto/16 :goto_7

    .line 2996
    :cond_12
    const/4 v2, 0x0

    move v5, v2

    goto :goto_8

    :cond_13
    const/4 v2, 0x0

    move v3, v2

    goto :goto_9

    :cond_14
    const/4 v4, 0x0

    goto :goto_b

    :cond_15
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v4

    const/4 v11, 0x0

    const/4 v6, 0x0

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    const/4 v3, 0x0

    :goto_c
    if-nez v6, :cond_17

    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_17

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/SidebarGroup;

    invoke-virtual {v2}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v9

    if-eqz v9, :cond_2b

    iget v10, v9, Lflipboard/objs/SidebarGroup$RenderHints;->c:I

    if-gt v10, v4, :cond_2b

    iget-boolean v10, v2, Lflipboard/objs/SidebarGroup;->b:Z

    if-eqz v10, :cond_2b

    sget-object v3, Lflipboard/objs/SidebarGroup$RenderHints;->a:Ljava/util/List;

    iget-object v9, v9, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    invoke-interface {v3, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v2}, Lflipboard/objs/SidebarGroup;->b()Z

    move-result v9

    if-nez v9, :cond_16

    if-eqz v3, :cond_2a

    :cond_16
    move-object v6, v2

    move v2, v3

    :goto_d
    move v3, v2

    goto :goto_c

    :cond_17
    if-eqz v6, :cond_19

    if-eqz v3, :cond_19

    new-instance v11, Lflipboard/gui/section/Group;

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-direct {v11, v2, v6}, Lflipboard/gui/section/Group;-><init>(Lflipboard/service/Section;Lflipboard/objs/SidebarGroup;)V

    :cond_18
    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-object v9, v11

    goto/16 :goto_6

    :cond_19
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v2, v2, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v2, :cond_1e

    const/4 v2, 0x5

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    move-object v10, v2

    :goto_e
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_1a
    :goto_f
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v4}, Lflipboard/objs/SectionPageTemplate;->a()I

    move-result v2

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    if-gt v2, v3, :cond_1a

    iget v2, v4, Lflipboard/objs/SectionPageTemplate;->h:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1b

    const/high16 v2, 0x3f800000    # 1.0f

    iget v3, v4, Lflipboard/objs/SectionPageTemplate;->h:F

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    sub-int v8, v3, v2

    const/4 v3, 0x0

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v5, v2

    :goto_10
    if-le v5, v8, :cond_29

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/gui/section/Group;

    iget-object v2, v2, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    if-ne v2, v4, :cond_1f

    const/4 v2, 0x1

    :goto_11
    if-nez v2, :cond_1a

    :cond_1b
    new-instance v2, Lflipboard/gui/section/Group;

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    move-object/from16 v5, p1

    move/from16 v8, p4

    move/from16 v9, p5

    invoke-direct/range {v2 .. v9}, Lflipboard/gui/section/Group;-><init>(Lflipboard/service/Section;Lflipboard/objs/SectionPageTemplate;Ljava/util/List;Lflipboard/objs/SidebarGroup;ZII)V

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move v9, v3

    :goto_12
    if-ltz v9, :cond_21

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflipboard/gui/section/Group;

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v5

    sub-int v13, v5, v9

    array-length v5, v10

    if-eq v13, v5, :cond_21

    iget-object v5, v4, Lflipboard/objs/SectionPageTemplate;->c:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_1c
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_20

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iget-object v8, v3, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    iget-object v8, v8, Lflipboard/objs/SectionPageTemplate;->c:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_1d
    :goto_13
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1c

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1d

    add-int/lit8 v8, v13, -0x1

    aget v8, v10, v8

    iget-object v0, v4, Lflipboard/objs/SectionPageTemplate;->c:Ljava/util/List;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v16

    div-int v8, v8, v16

    iget v0, v2, Lflipboard/gui/section/Group;->g:I

    move/from16 v16, v0

    sub-int v8, v16, v8

    iput v8, v2, Lflipboard/gui/section/Group;->g:I

    goto :goto_13

    :cond_1e
    const/4 v2, 0x4

    new-array v2, v2, [I

    fill-array-data v2, :array_1

    move-object v10, v2

    goto/16 :goto_e

    :cond_1f
    add-int/lit8 v2, v5, -0x1

    move v5, v2

    goto/16 :goto_10

    :cond_20
    add-int/lit8 v3, v9, -0x1

    move v9, v3

    goto :goto_12

    :cond_21
    if-eqz v11, :cond_22

    iget v3, v11, Lflipboard/gui/section/Group;->g:I

    iget v4, v2, Lflipboard/gui/section/Group;->g:I

    if-ge v3, v4, :cond_28

    :cond_22
    :goto_14
    move-object v11, v2

    goto/16 :goto_f

    .line 3005
    :cond_23
    if-eqz v9, :cond_25

    iget-object v2, v9, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    if-eqz v2, :cond_25

    .line 3006
    iget-object v2, v9, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_15
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_25

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/FeedItem;

    .line 3007
    const-string v4, "list"

    iget-object v5, v2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 3008
    iget-object v2, v2, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    goto :goto_15

    .line 3010
    :cond_24
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_15

    .line 3016
    :cond_25
    sget-object v2, Lflipboard/gui/section/SectionFragment;->a:Lflipboard/util/Log;

    iget-boolean v2, v2, Lflipboard/util/Log;->f:Z

    if-eqz v2, :cond_27

    if-eqz v9, :cond_27

    .line 3017
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v2

    .line 3018
    invoke-static {}, Lflipboard/util/AndroidUtil;->d()I

    move-result v3

    .line 3019
    iget-object v4, v9, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v4, v7}, Lflipboard/objs/SectionPageTemplate;->a(Z)Ljava/util/List;

    move-result-object v10

    .line 3020
    sget-object v4, Lflipboard/gui/section/SectionFragment;->a:Lflipboard/util/Log;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v9, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    iget-object v6, v6, Lflipboard/objs/SectionPageTemplate;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, v9, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    iget-object v6, v6, Lflipboard/objs/SectionPageTemplate;->b:Ljava/lang/String;

    aput-object v6, v4, v5

    .line 3021
    const/4 v4, 0x0

    move v8, v4

    :goto_16
    iget-object v4, v9, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v8, v4, :cond_26

    .line 3022
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 3023
    iget-object v4, v9, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lflipboard/objs/FeedItem;

    .line 3024
    invoke-interface {v10, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lflipboard/objs/SectionPageTemplate$Area;

    move-object/from16 v0, p0

    iget-object v5, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-static/range {v2 .. v7}, Lflipboard/gui/section/Group;->a(IILflipboard/objs/SectionPageTemplate$Area;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Z)I

    move-result v4

    .line 3025
    sget-object v5, Lflipboard/gui/section/SectionFragment;->a:Lflipboard/util/Log;

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual {v6}, Lflipboard/objs/FeedItem;->hashCode()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v5, v12

    const/4 v12, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v12

    const/4 v4, 0x2

    iget-object v12, v6, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    aput-object v12, v5, v4

    const/4 v4, 0x3

    iget-object v12, v6, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    aput-object v12, v5, v4

    const/4 v4, 0x4

    iget-object v6, v6, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    aput-object v6, v5, v4

    const/4 v4, 0x5

    aput-object v11, v5, v4

    .line 3021
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_16

    .line 3027
    :cond_26
    sget-object v2, Lflipboard/gui/section/SectionFragment;->a:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, v9, Lflipboard/gui/section/Group;->g:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    :cond_27
    move-object v2, v9

    goto/16 :goto_0

    :cond_28
    move-object v2, v11

    goto/16 :goto_14

    :cond_29
    move v2, v3

    goto/16 :goto_11

    :cond_2a
    move v2, v3

    goto/16 :goto_d

    :cond_2b
    move v2, v3

    goto/16 :goto_d

    :cond_2c
    move v3, v4

    goto/16 :goto_5

    .line 2996
    nop

    :array_0
    .array-data 4
        0x1f4
        0x64
        0x78
        0x190
        0xa
    .end array-data

    :array_1
    .array-data 4
        0x32
        0xf
        0x5
        0x5
    .end array-data
.end method

.method final a(Landroid/content/Context;Lflipboard/gui/section/Group;)Lflipboard/gui/section/SectionPage;
    .locals 13

    .prologue
    .line 3145
    new-instance v10, Lflipboard/gui/section/SectionPage;

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-boolean v1, p0, Lflipboard/gui/section/SectionFragment;->D:Z

    invoke-direct {v10, p1, p2, v0, v1}, Lflipboard/gui/section/SectionPage;-><init>(Landroid/content/Context;Lflipboard/gui/section/Group;Lflipboard/service/Section;Z)V

    .line 3146
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ag:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->setItemDisplayedCounter(Ljava/util/concurrent/atomic/AtomicInteger;)V

    .line 3148
    const/4 v0, 0x0

    .line 3150
    const-string v1, "image"

    invoke-virtual {p2, v1}, Lflipboard/gui/section/Group;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 3151
    const/4 v0, 0x1

    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->setIsImagePage(Z)V

    .line 3153
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    .line 3162
    :cond_0
    :goto_0
    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Z)Z

    move-result v11

    .line 3164
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_d

    const/4 v0, 0x1

    move v3, v0

    .line 3165
    :goto_1
    const/4 v0, 0x0

    move v4, v0

    :goto_2
    iget-object v0, p2, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_42

    .line 3166
    iget-object v0, p2, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 3167
    iget-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 3168
    sget-object v2, Lflipboard/gui/section/SectionFragment;->b:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_44

    .line 3169
    sget-object v1, Lflipboard/gui/section/SectionFragment;->a:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 3170
    const-string v1, "status"

    .line 3171
    iget-object v2, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-eqz v2, :cond_e

    iget-object v2, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_e

    .line 3172
    iget-object v0, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    move-object v5, v0

    .line 3179
    :goto_3
    iget-object v0, p2, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v0, v3}, Lflipboard/objs/SectionPageTemplate;->a(Z)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SectionPageTemplate$Area;

    .line 3180
    if-eqz v3, :cond_1

    iget-boolean v2, v0, Lflipboard/objs/SectionPageTemplate$Area;->f:Z

    if-nez v2, :cond_2

    :cond_1
    if-nez v3, :cond_f

    iget-boolean v2, v0, Lflipboard/objs/SectionPageTemplate$Area;->g:Z

    if-eqz v2, :cond_f

    :cond_2
    const/4 v2, 0x1

    .line 3181
    :goto_4
    iget-object v6, p2, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    if-eqz v6, :cond_10

    const/4 v6, 0x1

    .line 3182
    :goto_5
    iget-object v7, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v7}, Lflipboard/service/Section;->t()Z

    move-result v7

    if-eqz v7, :cond_11

    sget-object v7, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v7}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v7

    if-eqz v7, :cond_11

    const/4 v7, 0x1

    .line 3183
    :goto_6
    iget-object v8, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v8}, Lflipboard/service/Section;->y()Z

    move-result v8

    .line 3184
    if-eqz v2, :cond_12

    if-nez v6, :cond_12

    if-nez v7, :cond_12

    if-nez v8, :cond_12

    const/4 v2, 0x1

    move v7, v2

    .line 3186
    :goto_7
    invoke-virtual {v5}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_13

    invoke-virtual {v5}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_13

    const/4 v2, 0x1

    .line 3187
    :goto_8
    iget-object v8, v5, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v8, :cond_14

    iget-object v8, v5, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_14

    const/4 v8, 0x1

    .line 3188
    :goto_9
    const/4 v9, 0x0

    .line 3190
    iget-boolean v12, p2, Lflipboard/gui/section/Group;->f:Z

    if-eqz v12, :cond_18

    .line 3191
    const v0, 0x7f0300a3

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/InterstitialView;

    .line 3192
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iput-object v1, v0, Lflipboard/gui/item/InterstitialView;->e:Lflipboard/service/Section;

    .line 3193
    sget-object v2, Lflipboard/gui/item/InterstitialView$Layout;->b:Lflipboard/gui/item/InterstitialView$Layout;

    iget-object v1, v0, Lflipboard/gui/item/InterstitialView;->f:Lflipboard/gui/item/InterstitialView$Layout;

    if-ne v1, v2, :cond_3

    iget-boolean v1, v0, Lflipboard/gui/item/InterstitialView;->g:Z

    const/4 v6, 0x1

    if-eq v1, v6, :cond_4

    :cond_3
    iput-object v2, v0, Lflipboard/gui/item/InterstitialView;->f:Lflipboard/gui/item/InterstitialView$Layout;

    sget-object v1, Lflipboard/gui/item/InterstitialView$Layout;->a:Lflipboard/gui/item/InterstitialView$Layout;

    if-ne v2, v1, :cond_15

    const/4 v1, 0x1

    :goto_a
    invoke-virtual {v0, v1}, Lflipboard/gui/item/InterstitialView;->setWillNotDraw(Z)V

    iget v1, v0, Lflipboard/gui/item/InterstitialView;->c:I

    invoke-virtual {v0, v1}, Lflipboard/gui/item/InterstitialView;->setBackgroundColor(I)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/gui/item/InterstitialView;->g:Z

    iget-object v1, v0, Lflipboard/gui/item/InterstitialView;->a:Lflipboard/gui/FLTextView;

    iget v6, v0, Lflipboard/gui/item/InterstitialView;->d:I

    invoke-virtual {v1, v6}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    sget-object v1, Lflipboard/gui/item/InterstitialView$Layout;->c:Lflipboard/gui/item/InterstitialView$Layout;

    if-eq v2, v1, :cond_16

    const/4 v1, 0x1

    :goto_b
    iget-object v2, v0, Lflipboard/gui/item/InterstitialView;->b:Lflipboard/gui/section/AttributionSmall;

    invoke-virtual {v2, v1}, Lflipboard/gui/section/AttributionSmall;->setInverted(Z)V

    .line 3194
    :cond_4
    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    .line 3195
    const/4 v1, 0x1

    iput-boolean v1, v10, Lflipboard/gui/section/SectionPage;->m:Z

    .line 3196
    iget-object v1, v0, Lflipboard/gui/item/InterstitialView;->f:Lflipboard/gui/item/InterstitialView$Layout;

    sget-object v2, Lflipboard/gui/item/InterstitialView$Layout;->a:Lflipboard/gui/item/InterstitialView$Layout;

    if-eq v1, v2, :cond_5

    iget-boolean v1, v0, Lflipboard/gui/item/InterstitialView;->g:Z

    if-nez v1, :cond_17

    :cond_5
    const/4 v1, 0x1

    :goto_c
    invoke-virtual {v10, v1}, Lflipboard/gui/section/SectionPage;->a(Z)Z

    .line 3197
    const/4 v1, 0x1

    iput-boolean v1, v10, Lflipboard/gui/section/SectionPage;->p:Z

    .line 3383
    :cond_6
    :goto_d
    if-eqz v0, :cond_9

    .line 3384
    iget-boolean v1, p0, Lflipboard/gui/section/SectionFragment;->p:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v1, v5}, Lflipboard/service/Section;->c(Lflipboard/objs/FeedItem;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 3385
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 3386
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f030071

    invoke-static {v1, v2, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 3389
    :cond_7
    const v1, 0x7f0a0076

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 3390
    if-eqz v1, :cond_8

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 3391
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3394
    :cond_8
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3395
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 3165
    :cond_9
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_2

    .line 3154
    :cond_a
    const-string v1, "video"

    invoke-virtual {p2, v1}, Lflipboard/gui/section/Group;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 3155
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Lflipboard/gui/section/SectionPage;->setIsVideoPage(Z)V

    goto/16 :goto_0

    .line 3156
    :cond_b
    const-string v1, "section"

    invoke-virtual {p2, v1}, Lflipboard/gui/section/Group;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 3157
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Lflipboard/gui/section/SectionPage;->setIsSectionTilePage(Z)V

    goto/16 :goto_0

    .line 3158
    :cond_c
    const-string v1, "audio"

    invoke-virtual {p2, v1}, Lflipboard/gui/section/Group;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3159
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Lflipboard/gui/section/SectionPage;->setIsAudioPage(Z)V

    goto/16 :goto_0

    .line 3164
    :cond_d
    const/4 v0, 0x0

    move v3, v0

    goto/16 :goto_1

    .line 3174
    :cond_e
    sget-object v0, Lflipboard/gui/section/SectionFragment;->a:Lflipboard/util/Log;

    .line 3175
    const/4 v0, 0x0

    .line 3399
    :goto_e
    return-object v0

    .line 3180
    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 3181
    :cond_10
    const/4 v6, 0x0

    goto/16 :goto_5

    .line 3182
    :cond_11
    const/4 v7, 0x0

    goto/16 :goto_6

    .line 3184
    :cond_12
    const/4 v2, 0x0

    move v7, v2

    goto/16 :goto_7

    .line 3186
    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_8

    .line 3187
    :cond_14
    const/4 v8, 0x0

    goto/16 :goto_9

    .line 3193
    :cond_15
    const/4 v1, 0x0

    goto/16 :goto_a

    :cond_16
    const/4 v1, 0x0

    goto/16 :goto_b

    .line 3196
    :cond_17
    const/4 v1, 0x0

    goto/16 :goto_c

    .line 3199
    :cond_18
    const-string v12, "pagebox"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1b

    .line 3200
    iget-object v0, p2, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    invoke-virtual {v0}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v0

    .line 3201
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v1

    .line 3202
    iget-object v2, v0, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_19
    :goto_f
    packed-switch v0, :pswitch_data_0

    move-object v0, v9

    .line 3231
    :goto_10
    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    goto/16 :goto_d

    .line 3202
    :sswitch_0
    const-string v6, "pageboxList"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    const/4 v0, 0x0

    goto :goto_f

    :sswitch_1
    const-string v6, "pageboxGrid"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    const/4 v0, 0x1

    goto :goto_f

    :sswitch_2
    const-string v6, "pageboxSuggestedFollows"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    const/4 v0, 0x2

    goto :goto_f

    :sswitch_3
    const-string v6, "recommendedTopics"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    const/4 v0, 0x3

    goto :goto_f

    :sswitch_4
    const-string v6, "recommendedMagmakers"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    const/4 v0, 0x4

    goto :goto_f

    :sswitch_5
    const-string v6, "pageboxCreateAccount"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    const/4 v0, 0x5

    goto :goto_f

    :sswitch_6
    const-string v6, "pageboxAddService"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    const/4 v0, 0x6

    goto :goto_f

    :sswitch_7
    const-string v6, "pageboxFindFriends"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    const/4 v0, 0x7

    goto :goto_f

    :sswitch_8
    const-string v6, "pageboxCarousel"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    const/16 v0, 0x8

    goto :goto_f

    .line 3204
    :pswitch_0
    if-eqz v1, :cond_1a

    .line 3205
    const v0, 0x7f030132

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 3209
    :goto_11
    iput-boolean v1, v10, Lflipboard/gui/section/SectionPage;->p:Z

    goto :goto_10

    .line 3207
    :cond_1a
    const v0, 0x7f0300a8

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    goto :goto_11

    .line 3212
    :pswitch_1
    const v0, 0x7f0300a6

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    goto/16 :goto_10

    .line 3217
    :pswitch_2
    const v0, 0x7f030131

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    goto/16 :goto_10

    .line 3222
    :pswitch_3
    const v0, 0x7f030068

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    goto/16 :goto_10

    .line 3225
    :pswitch_4
    const v0, 0x7f0300aa

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/PageboxPaginatedCarousel;

    .line 3226
    const/4 v1, 0x1

    iput-boolean v1, v10, Lflipboard/gui/section/SectionPage;->p:Z

    .line 3227
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/item/PageboxPaginatedCarousel;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    goto/16 :goto_10

    .line 3232
    :cond_1b
    const-string v12, "status"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1c

    .line 3233
    const v0, 0x7f0300bd

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/item/StatusItem;

    .line 3234
    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    goto/16 :goto_d

    .line 3236
    :cond_1c
    const-string v12, "image"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1e

    const-string v12, "post"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1d

    if-nez v2, :cond_1d

    if-nez v8, :cond_1d

    invoke-virtual {v5}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v2

    if-nez v2, :cond_1e

    :cond_1d
    const-string v2, "video"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 3237
    :cond_1e
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_20

    .line 3238
    const v0, 0x7f0300a2

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/item/ImageItemTablet;

    .line 3239
    const v1, 0x7f0a008b

    invoke-virtual {v0, v1}, Lflipboard/gui/section/item/ImageItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 3240
    invoke-virtual {v0, v11}, Lflipboard/gui/section/item/ImageItemTablet;->setInverted(Z)V

    .line 3241
    iget-object v1, p2, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1f

    const/4 v1, 0x1

    :goto_12
    invoke-virtual {v0, v1}, Lflipboard/gui/section/item/ImageItemTablet;->setIsOneUp(Z)V

    .line 3242
    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    goto/16 :goto_d

    .line 3241
    :cond_1f
    const/4 v1, 0x0

    goto :goto_12

    .line 3245
    :cond_20
    const v1, 0x7f0300a0

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/section/item/ImageItemPhone;

    .line 3246
    const v2, 0x7f0a008b

    invoke-virtual {v1, v2}, Lflipboard/gui/section/item/ImageItemPhone;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 3247
    invoke-virtual {v1, v11}, Lflipboard/gui/section/item/ImageItemPhone;->setInverted(Z)V

    .line 3248
    iget-object v2, p2, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v6, 0x1

    if-ne v2, v6, :cond_22

    const/4 v2, 0x1

    :goto_13
    invoke-virtual {v1, v2}, Lflipboard/gui/section/item/ImageItemPhone;->setIsOneUp(Z)V

    .line 3249
    invoke-virtual {v0, v3}, Lflipboard/objs/SectionPageTemplate$Area;->a(Z)F

    move-result v2

    invoke-virtual {v0, v3}, Lflipboard/objs/SectionPageTemplate$Area;->b(Z)F

    move-result v0

    invoke-static {v5, v2, v0}, Lflipboard/gui/section/item/ImageItemPhone;->a(Lflipboard/objs/FeedItem;FF)Z

    move-result v0

    if-eqz v0, :cond_23

    if-eqz v7, :cond_23

    const/4 v0, 0x1

    .line 3250
    :goto_14
    invoke-virtual {v1, v0}, Lflipboard/gui/section/item/ImageItemPhone;->setIsFullBleed(Z)V

    .line 3251
    invoke-virtual {v10, v1}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    .line 3253
    if-eqz v0, :cond_21

    .line 3254
    const/4 v0, 0x1

    iput-boolean v0, v10, Lflipboard/gui/section/SectionPage;->p:Z

    :cond_21
    move-object v0, v1

    .line 3256
    goto/16 :goto_d

    .line 3248
    :cond_22
    const/4 v2, 0x0

    goto :goto_13

    .line 3249
    :cond_23
    const/4 v0, 0x0

    goto :goto_14

    .line 3257
    :cond_24
    const-string v2, "post"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_25

    const-string v2, "promotedAction"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 3258
    :cond_25
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v1, v1, Lflipboard/objs/TOCSection;->E:Z

    if-eqz v1, :cond_26

    .line 3260
    const v0, 0x7f0300b6

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/PostItemTabletEnumerated;

    .line 3261
    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    .line 3262
    add-int/lit8 v1, v4, 0x1

    iget v2, p2, Lflipboard/gui/section/Group;->i:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lflipboard/gui/item/PostItemTabletEnumerated;->setNumber(I)V

    goto/16 :goto_d

    .line 3265
    :cond_26
    const v1, 0x7f0300b5

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/section/item/PostItem;

    .line 3266
    const/4 v2, 0x0

    invoke-virtual {v5}, Lflipboard/objs/FeedItem;->i()Z

    move-result v6

    if-eqz v6, :cond_27

    invoke-virtual {v5}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v6

    invoke-virtual {v6}, Lflipboard/objs/Image;->c()Z

    move-result v6

    if-nez v6, :cond_27

    const/4 v2, 0x1

    :cond_27
    iget-object v6, v5, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    if-eqz v6, :cond_28

    iget-object v6, v5, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    const/4 v6, 0x0

    move v8, v6

    move v6, v2

    :goto_15
    if-ge v8, v9, :cond_29

    iget-object v2, v5, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/FeedItem;

    if-eqz v2, :cond_43

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->R()Z

    move-result v12

    if-eqz v12, :cond_43

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->i()Z

    move-result v12

    if-eqz v12, :cond_43

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v2

    invoke-virtual {v2}, Lflipboard/objs/Image;->c()Z

    move-result v2

    if-nez v2, :cond_43

    add-int/lit8 v2, v6, 0x1

    :goto_16
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    move v6, v2

    goto :goto_15

    :cond_28
    move v6, v2

    :cond_29
    const/16 v2, 0xc

    if-lt v6, v2, :cond_2d

    iget-object v2, p2, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    iget-object v2, v2, Lflipboard/objs/SectionPageTemplate;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v6, 0x1

    if-ne v2, v6, :cond_2d

    const/4 v2, 0x1

    .line 3267
    :goto_17
    invoke-virtual {v0, v3}, Lflipboard/objs/SectionPageTemplate$Area;->d(Z)F

    move-result v6

    const/4 v8, 0x0

    cmpl-float v6, v6, v8

    if-nez v6, :cond_2e

    invoke-virtual {v0, v3}, Lflipboard/objs/SectionPageTemplate$Area;->a(Z)F

    move-result v6

    const/high16 v8, 0x3f800000    # 1.0f

    cmpl-float v6, v6, v8

    if-nez v6, :cond_2e

    iget-object v6, p2, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v6}, Lflipboard/objs/SectionPageTemplate;->a()I

    move-result v6

    const/4 v8, 0x1

    if-eq v6, v8, :cond_2a

    iget-object v6, p2, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v6}, Lflipboard/objs/SectionPageTemplate;->a()I

    move-result v6

    const/4 v8, 0x3

    if-lt v6, v8, :cond_2e

    :cond_2a
    const/4 v6, 0x1

    .line 3268
    :goto_18
    invoke-virtual {v1, v6}, Lflipboard/gui/section/item/PostItem;->setCanFullBleed(Z)V

    .line 3269
    if-nez v2, :cond_2b

    if-eqz v7, :cond_2b

    invoke-virtual {v0, v3}, Lflipboard/objs/SectionPageTemplate$Area;->a(Z)F

    move-result v6

    invoke-virtual {v0, v3}, Lflipboard/objs/SectionPageTemplate$Area;->b(Z)F

    move-result v0

    invoke-virtual {v5, v6, v0}, Lflipboard/objs/FeedItem;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 3270
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lflipboard/gui/section/item/PostItem;->setIsFullBleed(Z)V

    .line 3271
    const/4 v0, 0x1

    iput-boolean v0, v10, Lflipboard/gui/section/SectionPage;->p:Z

    .line 3273
    :cond_2b
    invoke-virtual {v1, v2}, Lflipboard/gui/section/item/PostItem;->setIsGalleryPost(Z)V

    .line 3275
    invoke-virtual {v10, v1}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    .line 3277
    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 3278
    iget-boolean v0, v5, Lflipboard/objs/FeedItem;->bN:Z

    if-eqz v0, :cond_2c

    .line 3279
    invoke-virtual {v1}, Lflipboard/gui/section/item/PostItem;->getSponsoredActionButton()Lflipboard/gui/FLButton;

    move-result-object v0

    .line 3280
    invoke-virtual {v0, v5}, Lflipboard/gui/FLButton;->setTag(Ljava/lang/Object;)V

    .line 3281
    invoke-virtual {v0, p0}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2c
    move-object v0, v1

    .line 3283
    goto/16 :goto_d

    .line 3266
    :cond_2d
    const/4 v2, 0x0

    goto :goto_17

    .line 3267
    :cond_2e
    const/4 v6, 0x0

    goto :goto_18

    .line 3284
    :cond_2f
    const-string v0, "web"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 3285
    new-instance v0, Lflipboard/gui/section/item/WebItem;

    invoke-direct {v0, p1}, Lflipboard/gui/section/item/WebItem;-><init>(Landroid/content/Context;)V

    .line 3286
    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    .line 3287
    const/4 v1, 0x1

    iput-boolean v1, v10, Lflipboard/gui/section/SectionPage;->p:Z

    .line 3288
    invoke-virtual {v10}, Lflipboard/gui/section/SectionPage;->e()V

    .line 3290
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_d

    .line 3291
    :cond_30
    invoke-virtual {v5}, Lflipboard/objs/FeedItem;->P()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 3292
    iget-object v0, v5, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v0, :cond_41

    iget-object v0, v5, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_41

    .line 3293
    const v0, 0x7f03009e

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/item/AlbumItem;

    .line 3294
    const v1, 0x7f0a008b

    invoke-virtual {v0, v1}, Lflipboard/gui/section/item/AlbumItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 3295
    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    .line 3296
    if-nez v4, :cond_6

    iget-object v1, p2, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    .line 3297
    const/4 v1, 0x1

    iput-boolean v1, v10, Lflipboard/gui/section/SectionPage;->q:Z

    goto/16 :goto_d

    .line 3301
    :cond_31
    const-string v0, "audio"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 3303
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_33

    .line 3304
    const v0, 0x7f03009f

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/AudioItemTablet;

    .line 3305
    const v1, 0x7f0a008b

    invoke-virtual {v0, v1}, Lflipboard/gui/item/AudioItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 3315
    :cond_32
    :goto_19
    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    goto/16 :goto_d

    .line 3308
    :cond_33
    const v0, 0x7f030026

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/AudioView;

    .line 3309
    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/gui/section/scrolling/AudioView;->l:Z

    const/4 v1, 0x3

    const/4 v2, 0x0

    if-eqz v7, :cond_34

    sget-object v8, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v8}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v8

    if-eqz v8, :cond_34

    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/AudioView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v8, 0x7f09000b

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    :goto_1a
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v12, 0x0

    invoke-virtual {v0, v8, v2, v9, v12}, Lflipboard/gui/section/scrolling/AudioView;->setPadding(IIII)V

    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/AudioView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v8, 0x7f090019

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0}, Lflipboard/gui/section/scrolling/AudioView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09001a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iget-object v9, v0, Lflipboard/gui/section/scrolling/AudioView;->d:Lflipboard/gui/FLImageView;

    invoke-virtual {v9}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    if-eqz v6, :cond_35

    iput v8, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v8, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v1, v0, Lflipboard/gui/section/scrolling/AudioView;->e:Lflipboard/gui/section/AttributionServiceInfo;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lflipboard/gui/section/AttributionServiceInfo;->setStatusMaxLines(I)V

    :goto_1b
    iget-object v1, v0, Lflipboard/gui/section/scrolling/AudioView;->d:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v9}, Lflipboard/gui/FLImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3310
    if-eqz v7, :cond_32

    .line 3311
    const/4 v1, 0x1

    iput-boolean v1, v10, Lflipboard/gui/section/SectionPage;->p:Z

    goto :goto_19

    .line 3309
    :cond_34
    const/4 v1, 0x2

    iget-object v8, v0, Lflipboard/gui/section/scrolling/AudioView;->e:Lflipboard/gui/section/AttributionServiceInfo;

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Lflipboard/gui/section/AttributionServiceInfo;->setStatusMaxLines(I)V

    goto :goto_1a

    :cond_35
    iput v2, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v2, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v2, v0, Lflipboard/gui/section/scrolling/AudioView;->e:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v2, v1}, Lflipboard/gui/section/AttributionServiceInfo;->setStatusMaxLines(I)V

    goto :goto_1b

    .line 3317
    :cond_36
    const-string v0, "list"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 3318
    const v0, 0x7f0300a4

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/ListItemTablet;

    .line 3319
    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    goto/16 :goto_d

    .line 3321
    :cond_37
    const-string v0, "section"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 3322
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v0

    .line 3324
    if-eqz v0, :cond_38

    .line 3325
    const v0, 0x7f03010b

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/SectionLinkItemView;

    .line 3326
    invoke-virtual {v0, v7}, Lflipboard/gui/section/scrolling/SectionLinkItemView;->setIsFullBleed(Z)V

    .line 3327
    iput-boolean v7, v10, Lflipboard/gui/section/SectionPage;->p:Z

    .line 3333
    :goto_1c
    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    goto/16 :goto_d

    .line 3330
    :cond_38
    const v0, 0x7f0300bb

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 3331
    const/4 v1, 0x0

    iput-boolean v1, v10, Lflipboard/gui/section/SectionPage;->p:Z

    goto :goto_1c

    .line 3335
    :cond_39
    const-string v0, "sectionCover"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 3336
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->l()Lflipboard/service/FlipboardManager$RootScreenStyle;

    move-result-object v0

    sget-object v1, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v0, v1, :cond_3a

    .line 3337
    const v0, 0x7f030104

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/header/SectionCover;

    .line 3338
    const/4 v1, 0x1

    iput-boolean v1, v10, Lflipboard/gui/section/SectionPage;->m:Z

    .line 3339
    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    .line 3346
    :goto_1d
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Lflipboard/gui/section/SectionPage;->a(Z)Z

    .line 3347
    iput-boolean v7, v10, Lflipboard/gui/section/SectionPage;->p:Z

    .line 3348
    invoke-virtual {v10}, Lflipboard/gui/section/SectionPage;->e()V

    goto/16 :goto_d

    .line 3342
    :cond_3a
    const v0, 0x7f0300b8

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/CoverSectionItemTablet;

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->n:Lflipboard/gui/item/CoverSectionItemTablet;

    .line 3343
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->n:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    .line 3344
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->n:Lflipboard/gui/item/CoverSectionItemTablet;

    goto :goto_1d

    .line 3349
    :cond_3b
    const-string v0, "synthetic-client-app-cover"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 3350
    const v0, 0x7f0300e6

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/CoverPage;

    .line 3351
    invoke-virtual {v0}, Lflipboard/gui/toc/CoverPage;->a()V

    .line 3352
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/CoverPage;->setNeverShare(Z)V

    .line 3353
    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    .line 3354
    const/4 v1, 0x1

    iput-boolean v1, v10, Lflipboard/gui/section/SectionPage;->p:Z

    .line 3355
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Lflipboard/gui/section/SectionPage;->a(Z)Z

    .line 3356
    invoke-virtual {v10}, Lflipboard/gui/section/SectionPage;->e()V

    goto/16 :goto_d

    .line 3358
    :cond_3c
    const-string v0, "synthetic-client-profile-page"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 3359
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 3360
    const v0, 0x7f0300ed

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/header/ProfileCover;

    .line 3361
    const/4 v1, 0x1

    iput-boolean v1, v10, Lflipboard/gui/section/SectionPage;->m:Z

    .line 3362
    const/4 v1, 0x1

    iput-boolean v1, v10, Lflipboard/gui/section/SectionPage;->p:Z

    .line 3363
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Lflipboard/gui/section/SectionPage;->a(Z)Z

    .line 3364
    invoke-virtual {v10}, Lflipboard/gui/section/SectionPage;->e()V

    .line 3365
    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    move-object v0, v9

    .line 3366
    goto/16 :goto_d

    .line 3367
    :cond_3d
    const v0, 0x7f0300ec

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/item/ProfileBioItem;

    .line 3368
    const/4 v1, 0x0

    iput-boolean v1, v10, Lflipboard/gui/section/SectionPage;->p:Z

    .line 3369
    if-eqz v3, :cond_3e

    .line 3370
    const/4 v1, 0x1

    iput-boolean v1, v10, Lflipboard/gui/section/SectionPage;->p:Z

    .line 3371
    invoke-virtual {v10}, Lflipboard/gui/section/SectionPage;->e()V

    .line 3373
    :cond_3e
    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    move-object v0, v9

    .line 3374
    goto/16 :goto_d

    .line 3375
    :cond_3f
    const-string v0, "synthetic-client-profile-summary-item"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 3376
    const v0, 0x7f0300f0

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/item/ProfileSummaryItem;

    .line 3377
    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    move-object v0, v9

    .line 3378
    goto/16 :goto_d

    :cond_40
    const-string v0, "synthetic-action-refresh"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 3379
    const v0, 0x7f030029

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 3380
    invoke-virtual {v10, v0}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    .line 3381
    const/4 v0, 0x1

    iput-boolean v0, v10, Lflipboard/gui/section/SectionPage;->r:Z

    :cond_41
    move-object v0, v9

    goto/16 :goto_d

    :cond_42
    move-object v0, v10

    .line 3399
    goto/16 :goto_e

    :cond_43
    move v2, v6

    goto/16 :goto_16

    :cond_44
    move-object v5, v0

    goto/16 :goto_3

    .line 3202
    :sswitch_data_0
    .sparse-switch
        -0x620a2cc5 -> :sswitch_2
        -0x4c18dce0 -> :sswitch_7
        -0x32a239eb -> :sswitch_5
        -0x2b6b8b30 -> :sswitch_6
        0x4995335f -> :sswitch_3
        0x5455065c -> :sswitch_8
        0x5812a3a7 -> :sswitch_4
        0x5b8b0e42 -> :sswitch_1
        0x5b8d339a -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1227
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ak:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_1

    .line 1228
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->ak:Lflipboard/objs/FeedItem;

    invoke-static {v0, v3}, Lflipboard/util/MeteringHelper;->b(Landroid/content/Context;Lflipboard/objs/FeedItem;)Lflipboard/util/MeteringHelper$AccessType;

    move-result-object v0

    .line 1230
    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->ak:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->W:Ljava/lang/String;

    if-eqz v3, :cond_2

    sget-object v3, Lflipboard/util/MeteringHelper$AccessType;->c:Lflipboard/util/MeteringHelper$AccessType;

    if-eq v0, v3, :cond_2

    move v0, v1

    .line 1231
    :goto_0
    new-instance v3, Landroid/content/Intent;

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    if-eqz v0, :cond_3

    const-class v0, Lflipboard/activities/DetailActivity;

    :goto_1
    invoke-direct {v3, v4, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1232
    const-string v0, "extra_content_discovery_from_source"

    sget-object v4, Lflipboard/gui/section/SectionFragment;->az:Landroid/os/Bundle;

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1233
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    if-eqz v0, :cond_0

    .line 1234
    const-string v0, "pages_since_last_ad"

    iget-object v4, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    iget v4, v4, Lflipboard/service/FLAdManager;->d:I

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1236
    :cond_0
    const-string v0, "extra_item_ids"

    new-array v1, v1, [Ljava/lang/String;

    iget-object v4, p0, Lflipboard/gui/section/SectionFragment;->ak:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    aput-object v4, v1, v2

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1237
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ak:Lflipboard/objs/FeedItem;

    invoke-direct {p0, v3, v0, v5}, Lflipboard/gui/section/SectionFragment;->a(Landroid/content/Intent;Lflipboard/objs/FeedItem;Landroid/view/View;)V

    .line 1238
    iput-object v5, p0, Lflipboard/gui/section/SectionFragment;->ak:Lflipboard/objs/FeedItem;

    .line 1240
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 1230
    goto :goto_0

    .line 1231
    :cond_3
    const-class v0, Lflipboard/activities/DetailActivityStayOnRotation;

    goto :goto_1
.end method

.method final a(I)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2061
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "SectionFragment:setCurrentViewIndex"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 2062
    iget v0, p0, Lflipboard/gui/section/SectionFragment;->q:I

    .line 2063
    iput p1, p0, Lflipboard/gui/section/SectionFragment;->q:I

    .line 2064
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragment;->t()V

    .line 2066
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v1

    if-ge p1, v1, :cond_4

    .line 2067
    if-ge v0, p1, :cond_5

    move v1, v2

    .line 2068
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/ListSingleThreadWrapper;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    .line 2069
    iget-object v4, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    if-eqz v4, :cond_9

    .line 2070
    iget-object v4, p0, Lflipboard/gui/section/SectionFragment;->aq:Lflipboard/gui/section/Group;

    if-eqz v4, :cond_0

    .line 2071
    iget-object v4, p0, Lflipboard/gui/section/SectionFragment;->aq:Lflipboard/gui/section/Group;

    iget-object v5, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    if-eqz v5, :cond_7

    iget-object v5, v4, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    if-eqz v5, :cond_7

    iget-object v5, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v5, v5, Lflipboard/gui/section/GroupFranchiseMeta;->a:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v4, v4, Lflipboard/gui/section/GroupFranchiseMeta;->a:Lflipboard/objs/FeedItem;

    if-eqz v5, :cond_6

    if-eqz v4, :cond_6

    invoke-virtual {v5, v4}, Lflipboard/objs/FeedItem;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    move v4, v2

    .line 2072
    :goto_1
    if-nez v4, :cond_8

    .line 2074
    invoke-direct {p0, v1}, Lflipboard/gui/section/SectionFragment;->a(Z)V

    .line 2081
    :cond_0
    invoke-direct {p0, v0}, Lflipboard/gui/section/SectionFragment;->d(Lflipboard/gui/section/Group;)V

    iget-object v4, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->aj:Ljava/util/HashMap;

    iget-object v1, v4, Lflipboard/gui/section/GroupFranchiseMeta;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_c

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->N:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->aj:Ljava/util/HashMap;

    iget-object v5, v4, Lflipboard/gui/section/GroupFranchiseMeta;->d:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;Lflipboard/gui/section/GroupFranchiseMeta;)Lflipboard/service/FLAdManager;

    move-result-object v0

    iput p1, v0, Lflipboard/service/FLAdManager;->c:I

    invoke-virtual {v0, p1}, Lflipboard/service/FLAdManager;->a(I)V

    iget-object v5, p0, Lflipboard/gui/section/SectionFragment;->aj:Ljava/util/HashMap;

    iget-object v6, v4, Lflipboard/gui/section/GroupFranchiseMeta;->d:Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v2

    :goto_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v0

    :goto_3
    const/4 v0, 0x0

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->ai:Ljava/util/HashMap;

    iget-object v5, v4, Lflipboard/gui/section/GroupFranchiseMeta;->d:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_a

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->N:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget-object v5, p0, Lflipboard/gui/section/SectionFragment;->ai:Ljava/util/HashMap;

    iget-object v6, v4, Lflipboard/gui/section/GroupFranchiseMeta;->d:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_1

    new-instance v0, Lflipboard/gui/section/SectionFragment$31;

    invoke-direct {v0, p0, v4}, Lflipboard/gui/section/SectionFragment$31;-><init>(Lflipboard/gui/section/SectionFragment;Lflipboard/gui/section/GroupFranchiseMeta;)V

    iget-object v5, p0, Lflipboard/gui/section/SectionFragment;->ai:Ljava/util/HashMap;

    iget-object v6, v4, Lflipboard/gui/section/GroupFranchiseMeta;->d:Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v2, v0

    :goto_4
    if-eqz v2, :cond_2

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->aj:Ljava/util/HashMap;

    iget-object v5, v4, Lflipboard/gui/section/GroupFranchiseMeta;->d:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/FLAdManager;

    invoke-virtual {v0, v2}, Lflipboard/service/FLAdManager;->b(Lflipboard/util/Observer;)V

    :cond_2
    if-eqz v1, :cond_3

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->aj:Ljava/util/HashMap;

    iget-object v1, v4, Lflipboard/gui/section/GroupFranchiseMeta;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/FLAdManager;

    invoke-virtual {v0, v3, v3}, Lflipboard/service/FLAdManager;->a(II)V

    .line 2085
    :cond_3
    :goto_5
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->aj:Ljava/util/HashMap;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->aq:Lflipboard/gui/section/Group;

    iget-object v1, v1, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v1, v1, Lflipboard/gui/section/GroupFranchiseMeta;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/FLAdManager;

    .line 2086
    if-eqz v0, :cond_4

    .line 2087
    invoke-virtual {v0, p1}, Lflipboard/service/FLAdManager;->a(I)V

    .line 2097
    :cond_4
    :goto_6
    return-void

    :cond_5
    move v1, v3

    .line 2067
    goto/16 :goto_0

    :cond_6
    move v4, v3

    .line 2071
    goto/16 :goto_1

    :cond_7
    move v4, v3

    goto/16 :goto_1

    .line 2078
    :cond_8
    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->aq:Lflipboard/gui/section/Group;

    goto :goto_5

    .line 2081
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    .line 2091
    :cond_9
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->aq:Lflipboard/gui/section/Group;

    if-eqz v0, :cond_4

    .line 2093
    invoke-direct {p0, v1}, Lflipboard/gui/section/SectionFragment;->a(Z)V

    goto :goto_6

    :cond_a
    move-object v2, v0

    goto :goto_4

    :cond_b
    move v0, v3

    goto :goto_2

    :cond_c
    move v1, v3

    goto :goto_3
.end method

.method final a(ILflipboard/gui/section/Group;)V
    .locals 2

    .prologue
    .line 2605
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    const-string v0, "addGroup with index"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 2606
    iget v0, p0, Lflipboard/gui/section/SectionFragment;->q:I

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->aq:Lflipboard/gui/section/Group;

    if-nez v0, :cond_0

    .line 2607
    invoke-direct {p0, p2}, Lflipboard/gui/section/SectionFragment;->d(Lflipboard/gui/section/Group;)V

    .line 2609
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/ListSingleThreadWrapper;->add(ILjava/lang/Object;)V

    .line 2610
    sget-object v0, Lflipboard/gui/section/SectionFragment;->a:Lflipboard/util/Log;

    .line 2611
    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->A:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lflipboard/gui/section/SectionFragment;->ao:I

    if-gt p1, v0, :cond_1

    .line 2612
    iget v0, p0, Lflipboard/gui/section/SectionFragment;->ao:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/gui/section/SectionFragment;->ao:I

    .line 2613
    sget-object v0, Lflipboard/gui/section/SectionFragment;->a:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Gap moved, now at "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lflipboard/gui/section/SectionFragment;->ao:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2615
    :cond_1
    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->an:Z

    if-eqz v0, :cond_2

    .line 2616
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragment;->x()V

    .line 2618
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 2621
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    :goto_0
    invoke-direct {p0, v0}, Lflipboard/gui/section/SectionFragment;->f(I)V

    .line 2622
    invoke-virtual {p0, p1}, Lflipboard/gui/section/SectionFragment;->e(I)V

    .line 2625
    :cond_2
    return-void

    .line 2621
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILjava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/section/Group;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2225
    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->S:Z

    if-nez v0, :cond_0

    .line 2257
    :goto_0
    return-void

    .line 2229
    :cond_0
    if-ltz p1, :cond_1

    .line 2230
    sget-object v0, Lflipboard/gui/section/SectionFragment;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    .line 2233
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    .line 2234
    if-ltz p1, :cond_4

    if-eqz p2, :cond_4

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_4

    if-eqz v0, :cond_4

    .line 2235
    invoke-virtual {v0, p1}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v0

    .line 2236
    if-eqz v0, :cond_4

    .line 2237
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2238
    invoke-static {v0, v2}, Lflipboard/gui/section/SectionTabletView;->a(Landroid/view/View;Ljava/util/List;)V

    .line 2239
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/TabletItem;

    .line 2240
    invoke-interface {v0}, Lflipboard/gui/item/TabletItem;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 2241
    iget-boolean v3, v0, Lflipboard/objs/FeedItem;->aQ:Z

    if-eqz v3, :cond_2

    iget-boolean v3, v0, Lflipboard/objs/FeedItem;->be:Z

    if-nez v3, :cond_2

    .line 2242
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v4, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v3, v4, v0}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    goto :goto_1

    .line 2247
    :cond_3
    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    .line 2248
    iget-object v2, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    if-nez v2, :cond_5

    move-object v0, v1

    .line 2249
    :goto_2
    if-eqz v0, :cond_4

    iget-boolean v2, v0, Lflipboard/objs/FeedItem;->aQ:Z

    if-eqz v2, :cond_4

    iget-boolean v2, v0, Lflipboard/objs/FeedItem;->be:Z

    if-nez v2, :cond_4

    .line 2250
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v2, v3, v0}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 2256
    :cond_4
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0, v2, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    goto :goto_0

    .line 2248
    :cond_5
    iget-object v0, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v0, v0, Lflipboard/gui/section/GroupFranchiseMeta;->a:Lflipboard/objs/FeedItem;

    goto :goto_2
.end method

.method final a(Lflipboard/gui/section/Group;)V
    .locals 1

    .prologue
    .line 2601
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lflipboard/gui/section/SectionFragment;->a(ILflipboard/gui/section/Group;)V

    .line 2602
    return-void
.end method

.method final a(Lflipboard/objs/Invite;)V
    .locals 6

    .prologue
    .line 950
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 951
    new-instance v1, Lflipboard/io/UsageEvent;

    const-string v2, "event"

    invoke-direct {v1, v2}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 952
    const-string v2, "id"

    const-string v3, "didReceiveContributorInvite"

    invoke-virtual {v1, v2, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 953
    const-string v2, "sectionIdentifier"

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v3, v3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 954
    invoke-virtual {v1}, Lflipboard/io/UsageEvent;->a()V

    .line 955
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v2, p1, Lflipboard/objs/Invite;->g:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/Invite;->h:Ljava/lang/String;

    new-instance v4, Lflipboard/gui/section/SectionFragment$9;

    invoke-direct {v4, p0, v0, p1}, Lflipboard/gui/section/SectionFragment$9;-><init>(Lflipboard/gui/section/SectionFragment;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/Invite;)V

    new-instance v0, Lflipboard/service/Flap$AcceptContributorInviteRequest;

    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v5, v5, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-direct {v0, v1, v5}, Lflipboard/service/Flap$AcceptContributorInviteRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v0, v2, v3, v4}, Lflipboard/service/Flap$AcceptContributorInviteRequest;->a(Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$AcceptContributorInviteRequest;

    .line 987
    return-void
.end method

.method final a(Lflipboard/service/FLAdManager;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 4216
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    const-string v1, "tryCreateAndInsertAdPage"

    invoke-static {v1}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 4221
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v1

    move v2, v1

    .line 4222
    :goto_0
    if-eqz p1, :cond_0

    iget-object v1, p1, Lflipboard/service/FLAdManager;->b:Lflipboard/gui/section/GroupFranchiseMeta;

    if-nez v1, :cond_1

    .line 4223
    :cond_0
    invoke-direct {p0, v2}, Lflipboard/gui/section/SectionFragment;->f(I)V

    .line 4225
    :cond_1
    if-eqz p1, :cond_3

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v1, :cond_3

    .line 4226
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    .line 4227
    invoke-virtual {p1}, Lflipboard/service/FLAdManager;->b()Z

    move-result v1

    .line 4231
    :goto_1
    if-nez v1, :cond_4

    .line 4292
    :goto_2
    return-void

    :cond_2
    move v2, v6

    .line 4221
    goto :goto_0

    .line 4229
    :cond_3
    sget-object v1, Lflipboard/gui/section/SectionFragment;->a:Lflipboard/util/Log;

    const-string v3, "tryCreateAndInsertAdPage: unable to init ads %t"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v6

    goto :goto_1

    .line 4236
    :cond_4
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->K:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v6, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-nez v1, :cond_5

    .line 4237
    sget-object v0, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    goto :goto_2

    .line 4245
    :cond_5
    :try_start_0
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v1

    .line 4246
    invoke-virtual {p1, v2, v1}, Lflipboard/service/FLAdManager;->b(II)Lflipboard/service/FLAdManager$AdAsset;

    move-result-object v7

    .line 4247
    if-eqz v7, :cond_8

    iget-object v2, v7, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget v2, v2, Lflipboard/objs/Ad;->i:I

    if-gt v2, v1, :cond_8

    .line 4248
    iget-object v2, v7, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget v8, v2, Lflipboard/objs/Ad;->i:I

    .line 4249
    sget-object v2, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    const/4 v1, 0x2

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v3}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    .line 4253
    new-instance v1, Lflipboard/objs/FeedItem;

    invoke-direct {v1}, Lflipboard/objs/FeedItem;-><init>()V

    .line 4254
    const-string v2, "image"

    iput-object v2, v1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 4255
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    iput-object v2, v1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    .line 4257
    new-instance v2, Lflipboard/gui/section/Group$AdGroup;

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-static {}, Lflipboard/gui/section/SectionFragment;->k()Lflipboard/objs/SectionPageTemplate;

    move-result-object v4

    invoke-direct {v2, v3, v4, v1, v7}, Lflipboard/gui/section/Group$AdGroup;-><init>(Lflipboard/service/Section;Lflipboard/objs/SectionPageTemplate;Lflipboard/objs/FeedItem;Lflipboard/service/FLAdManager$AdAsset;)V

    .line 4258
    iget-object v1, p1, Lflipboard/service/FLAdManager;->b:Lflipboard/gui/section/GroupFranchiseMeta;

    if-eqz v1, :cond_6

    .line 4259
    iget-object v1, p1, Lflipboard/service/FLAdManager;->b:Lflipboard/gui/section/GroupFranchiseMeta;

    iput-object v1, v2, Lflipboard/gui/section/Group$AdGroup;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    .line 4263
    :cond_6
    iget-object v1, v7, Lflipboard/service/FLAdManager$AdAsset;->b:Lflipboard/objs/Ad$Asset;

    if-eqz v1, :cond_7

    .line 4264
    new-instance v0, Lflipboard/gui/section/SectionAdPage;

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v4, v7, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget-boolean v5, p0, Lflipboard/gui/section/SectionFragment;->D:Z

    invoke-direct/range {v0 .. v5}, Lflipboard/gui/section/SectionAdPage;-><init>(Landroid/content/Context;Lflipboard/gui/section/Group;Lflipboard/service/Section;Lflipboard/objs/Ad;Z)V

    .line 4265
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v3, 0x7f03009b

    const/4 v4, 0x0

    invoke-static {v1, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/item/AdItem;

    .line 4266
    invoke-static {}, Lflipboard/service/FLAdManager;->a()Landroid/graphics/Point;

    move-result-object v3

    .line 4267
    iget v4, v3, Landroid/graphics/Point;->x:I

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v7, v4, v3}, Lflipboard/gui/item/AdItem;->a(Lflipboard/service/FLAdManager$AdAsset;II)V

    .line 4268
    iget-object v3, v7, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget-object v3, v3, Lflipboard/objs/Ad;->k:Lflipboard/objs/Ad$VideoInfo;

    invoke-virtual {v1, v3}, Lflipboard/gui/item/AdItem;->setVideoInfo(Lflipboard/objs/Ad$VideoInfo;)V

    .line 4269
    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionAdPage;->a(Landroid/view/View;)V

    .line 4270
    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/gui/section/SectionAdPage;->p:Z

    .line 4277
    :cond_7
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    .line 4278
    if-eqz v1, :cond_8

    if-eqz v0, :cond_8

    .line 4281
    sget-object v3, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v2, Lflipboard/gui/section/Group$AdGroup;->l:Lflipboard/service/FLAdManager$AdAsset;

    iget-object v5, v5, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget v5, v5, Lflipboard/objs/Ad;->i:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getFlippableViews()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 4282
    invoke-virtual {p0, v8, v2}, Lflipboard/gui/section/SectionFragment;->a(ILflipboard/gui/section/Group;)V

    .line 4283
    invoke-virtual {v0}, Lflipboard/gui/section/SectionAdPage;->f()V

    .line 4284
    invoke-virtual {v0}, Lflipboard/gui/section/SectionAdPage;->h()V

    .line 4285
    sget-object v2, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getFlippableViews()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 4286
    invoke-virtual {v1, v8, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    .line 4287
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->c(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4291
    :cond_8
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->K:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->K:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    throw v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 123
    check-cast p2, Lflipboard/gui/flipping/FlipTransitionViews$Message;

    check-cast p3, Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;->b:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    if-ne p2, v0, :cond_1

    iget v0, p0, Lflipboard/gui/section/SectionFragment;->q:I

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->e()Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getRunningFlips()I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    new-instance v3, Lflipboard/gui/section/SectionFragment$13;

    invoke-direct {v3, p0, v1, v0, p3}, Lflipboard/gui/section/SectionFragment$13;-><init>(Lflipboard/gui/section/SectionFragment;IILflipboard/gui/flipping/FlipTransitionBase$Direction;)V

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v1, "layoutViewFlips"

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;->c:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/SectionFragment$14;

    invoke-direct {v1, p0}, Lflipboard/gui/section/SectionFragment$14;-><init>(Lflipboard/gui/section/SectionFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;->d:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    if-ne p2, v0, :cond_5

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/section/SectionFragment$15;

    invoke-direct {v2, p0}, Lflipboard/gui/section/SectionFragment$15;-><init>(Lflipboard/gui/section/SectionFragment;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    iget-boolean v1, p0, Lflipboard/gui/section/SectionFragment;->R:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget v0, v0, Lflipboard/model/ConfigSetting;->MarkLastPageReadDelay:F

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    if-nez v0, :cond_3

    const/16 v0, 0x1f4

    :cond_3
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    int-to-long v2, v0

    new-instance v0, Lflipboard/gui/section/SectionFragment$16;

    invoke-direct {v0, p0}, Lflipboard/gui/section/SectionFragment$16;-><init>(Lflipboard/gui/section/SectionFragment;)V

    invoke-virtual {v1, v2, v3, v0}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    :cond_4
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    invoke-virtual {v0}, Lflipboard/service/HintManager;->a()V

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/SectionPage;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->F:Landroid/support/v4/widget/DrawerLayout;

    invoke-static {v1}, Lflipboard/gui/personal/FLDrawerLayout;->b(Landroid/support/v4/widget/DrawerLayout;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lflipboard/gui/section/SectionPage;->c()V

    goto :goto_0

    :cond_5
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionViews$Message;->e:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, v2}, Lflipboard/service/Section;->a(ZLjava/lang/String;Landroid/os/Bundle;)Z

    invoke-direct {p0}, Lflipboard/gui/section/SectionFragment;->u()V

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->i()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2149
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ap:Lflipboard/io/UsageEvent;

    if-eqz v0, :cond_1

    .line 2150
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ap:Lflipboard/io/UsageEvent;

    const-string v1, "exitMethod"

    invoke-virtual {v0, v1, p1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2151
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ap:Lflipboard/io/UsageEvent;

    const-string v1, "exitSectionIdentifier"

    invoke-virtual {v0, v1, v6}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2152
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->aq:Lflipboard/gui/section/Group;

    iget-object v0, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget v0, v0, Lflipboard/gui/section/GroupFranchiseMeta;->e:I

    if-lez v0, :cond_0

    .line 2153
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ap:Lflipboard/io/UsageEvent;

    const-string v1, "pageInFranchise"

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->aq:Lflipboard/gui/section/Group;

    iget-object v2, v2, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget v2, v2, Lflipboard/gui/section/GroupFranchiseMeta;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2155
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ap:Lflipboard/io/UsageEvent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->ap:Lflipboard/io/UsageEvent;

    iget-wide v4, v1, Lflipboard/io/UsageEvent;->e:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lflipboard/io/UsageEvent;->g:J

    .line 2156
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ap:Lflipboard/io/UsageEvent;

    invoke-direct {p0, v0}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/io/UsageEvent;)V

    .line 2157
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ap:Lflipboard/io/UsageEvent;

    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 2158
    iput-object v6, p0, Lflipboard/gui/section/SectionFragment;->ap:Lflipboard/io/UsageEvent;

    .line 2159
    iput-object v6, p0, Lflipboard/gui/section/SectionFragment;->aq:Lflipboard/gui/section/Group;

    .line 2161
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 3823
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    check-cast v0, Lflipboard/gui/section/SectionTabletView;

    .line 3824
    if-eqz v0, :cond_1

    .line 3825
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    const v3, 0x7fffffff

    invoke-virtual {v0, v2, v3, v1}, Lflipboard/gui/section/SectionTabletView;->a(IILjava/util/List;)Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/item/TabletItem;

    invoke-interface {v1}, Lflipboard/gui/item/TabletItem;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v2

    iget-object v2, v2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {v1, v6}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f030071

    invoke-static {v2, v4, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    const v2, 0x7f0a019f

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLTextIntf;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionTabletView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Lflipboard/gui/section/SectionPage;

    invoke-virtual {v2, v1, v4}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;Landroid/view/View;)V

    goto :goto_0

    .line 3827
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->p:Z

    .line 3828
    return-void
.end method

.method final a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2779
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 2780
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 2781
    iget-boolean v3, v0, Lflipboard/objs/FeedItem;->ab:Z

    if-eqz v3, :cond_0

    .line 2782
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2786
    :cond_1
    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflipboard/objs/FeedItem;

    .line 2787
    iget-object v0, v1, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2788
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    :goto_1
    add-int/lit8 v2, v0, -0x1

    if-lez v0, :cond_2

    .line 2793
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 2795
    iget-boolean v4, v0, Lflipboard/objs/FeedItem;->ab:Z

    if-nez v4, :cond_3

    iget-object v4, v1, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    iget-object v5, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2796
    invoke-interface {p1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2799
    iget-object v4, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v4, v4, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v4}, Lflipboard/objs/FeedItem;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v0, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    iget-object v4, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v4, v4, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2801
    invoke-interface {p1, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2803
    invoke-interface {p1, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_3
    move v0, v2

    .line 2806
    goto :goto_1

    .line 2808
    :cond_4
    return-void
.end method

.method final a(Ljava/util/List;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const v9, 0x7f0d0224

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1009
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    const-string v0, "processNewItems"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 1010
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->n()V

    .line 1011
    iget-boolean v6, p0, Lflipboard/gui/section/SectionFragment;->y:Z

    .line 1012
    iget-boolean v7, p0, Lflipboard/gui/section/SectionFragment;->x:Z

    .line 1013
    iput-boolean v3, p0, Lflipboard/gui/section/SectionFragment;->x:Z

    .line 1014
    iput-boolean v3, p0, Lflipboard/gui/section/SectionFragment;->y:Z

    .line 1016
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 1017
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "SectionFragment.processNewItems() wasRefresh:"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1018
    if-nez p2, :cond_b

    .line 1021
    sget-boolean v0, Lflipboard/util/DuplicateOccurrenceLog;->a:Z

    if-nez v0, :cond_0

    invoke-static {}, Lflipboard/io/UsageEvent;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v2, v3

    .line 1022
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->i:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 1023
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->i:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0, v2}, Lflipboard/gui/section/ListSingleThreadWrapper;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1024
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    .line 1025
    if-eq v0, v1, :cond_1

    .line 1026
    invoke-static {}, Lflipboard/io/UsageEvent;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1027
    const-string v0, "unwanted.SectionFragment_mismatch_in_items_after_load_more"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    .line 1022
    :cond_1
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1029
    :cond_2
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v1}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 1030
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v5, "Mismatch in items after load more\n"

    invoke-virtual {v1, v5}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1031
    const/4 v1, -0x1

    const-string v5, "Mismatch in items after load more"

    invoke-direct {p0, v1, v0, v5}, Lflipboard/gui/section/SectionFragment;->a(ILflipboard/objs/FeedItem;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move v2, v3

    .line 1036
    :cond_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    .line 1037
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 1039
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v3

    :cond_5
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1040
    iget-object v5, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v5, v0}, Lflipboard/service/Section;->c(Lflipboard/objs/FeedItem;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-boolean v5, p0, Lflipboard/gui/section/SectionFragment;->Q:Z

    if-eqz v5, :cond_6

    iget-boolean v5, v0, Lflipboard/objs/FeedItem;->be:Z

    if-nez v5, :cond_5

    .line 1041
    :cond_6
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->N()Z

    move-result v5

    if-nez v5, :cond_7

    .line 1044
    iget-object v5, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v5, v5, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v6, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v6}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Lflipboard/service/User;->a(Lflipboard/objs/FeedItem;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 1046
    invoke-direct {p0, v0}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/objs/FeedItem;)V

    move v1, v4

    .line 1050
    :cond_7
    iget-boolean v5, v0, Lflipboard/objs/FeedItem;->cd:Z

    if-eqz v5, :cond_5

    .line 1051
    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->ak:Lflipboard/objs/FeedItem;

    goto :goto_2

    .line 1054
    :cond_8
    if-eqz v1, :cond_9

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1055
    invoke-virtual {p0, v3, v4}, Lflipboard/gui/section/SectionFragment;->a(ZZ)V

    .line 1147
    :cond_9
    :goto_3
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->i:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->clear()V

    .line 1148
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 1149
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "Syncing allItems and knownItems in processNewItems\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 1150
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->i:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/ListSingleThreadWrapper;->addAll(Ljava/util/Collection;)Z

    .line 1151
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->a(Ljava/util/List;)V

    .line 1152
    return-void

    .line 1058
    :cond_a
    if-eqz v6, :cond_9

    .line 1059
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_9

    .line 1060
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 1061
    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v9}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " oops"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    goto :goto_3

    :cond_b
    move v1, v3

    move v2, v4

    .line 1068
    :goto_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    if-eqz v2, :cond_c

    .line 1069
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1070
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->i:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v2, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 1068
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1076
    :cond_c
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->i:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v1

    if-ne v0, v1, :cond_f

    move v0, v4

    :goto_5
    and-int/2addr v0, v2

    .line 1077
    if-nez v0, :cond_15

    .line 1079
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 1080
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_19

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v1, :cond_19

    .line 1082
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1, v3}, Lflipboard/gui/section/ListSingleThreadWrapper;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/section/Group;

    iget-object v1, v1, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    .line 1083
    if-eqz v1, :cond_1c

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1c

    .line 1084
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v5, "synthetic-client-profile-page"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v5, v1

    .line 1087
    :goto_6
    iget v1, p0, Lflipboard/gui/section/SectionFragment;->q:I

    if-nez v1, :cond_16

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->D()Z

    move-result v1

    if-nez v1, :cond_d

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1, v3}, Lflipboard/gui/section/ListSingleThreadWrapper;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/section/Group;

    invoke-virtual {v1}, Lflipboard/gui/section/Group;->a()Z

    move-result v1

    if-nez v1, :cond_d

    if-eqz v5, :cond_16

    .line 1089
    :cond_d
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    .line 1090
    if-eqz v1, :cond_e

    iget-object v5, p0, Lflipboard/gui/section/SectionFragment;->z:Lflipboard/objs/FeedItem;

    invoke-virtual {v1, v5}, Lflipboard/objs/FeedItem;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 1091
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->i()V

    .line 1093
    :cond_e
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    iget-object v5, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v5}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Lflipboard/gui/section/ListSingleThreadWrapper;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1094
    :goto_7
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v1

    if-le v1, v4, :cond_10

    .line 1095
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1, v4}, Lflipboard/gui/flipping/FlipTransitionViews;->c(I)V

    goto :goto_7

    :cond_f
    move v0, v3

    .line 1076
    goto/16 :goto_5

    .line 1097
    :cond_10
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->clear()V

    .line 1098
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_11
    :goto_8
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    .line 1099
    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->N()Z

    move-result v8

    if-nez v8, :cond_11

    .line 1100
    invoke-direct {p0, v1}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/objs/FeedItem;)V

    goto :goto_8

    .line 1103
    :cond_12
    iget-boolean v0, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_13

    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->aw:Z

    if-eqz v0, :cond_13

    if-nez v2, :cond_13

    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    iget-boolean v0, v0, Lflipboard/service/HintManager;->d:Z

    .line 1130
    :cond_13
    :goto_9
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragment;->v()V

    .line 1131
    if-eqz v2, :cond_14

    iget v0, p0, Lflipboard/gui/section/SectionFragment;->q:I

    if-eqz v0, :cond_1a

    .line 1132
    :cond_14
    :goto_a
    invoke-virtual {p0, v6, v4}, Lflipboard/gui/section/SectionFragment;->a(ZZ)V

    .line 1135
    :cond_15
    if-eqz v2, :cond_9

    if-eqz v7, :cond_9

    .line 1136
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    .line 1137
    if-eqz v0, :cond_1b

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->z:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/objs/FeedItem;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 1138
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->i()V

    goto/16 :goto_3

    .line 1109
    :cond_16
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->D()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1110
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0, v3}, Lflipboard/gui/section/ListSingleThreadWrapper;->remove(I)Ljava/lang/Object;

    .line 1111
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0, v3}, Lflipboard/gui/flipping/FlipTransitionViews;->c(I)V

    .line 1113
    :cond_17
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->g:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/ListSingleThreadWrapper;->addAll(Ljava/util/Collection;)Z

    .line 1114
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->clear()V

    .line 1115
    if-nez v6, :cond_18

    .line 1116
    iput-boolean v4, p0, Lflipboard/gui/section/SectionFragment;->ar:Z

    .line 1119
    :cond_18
    iput v3, p0, Lflipboard/gui/section/SectionFragment;->ao:I

    .line 1120
    iput-boolean v4, p0, Lflipboard/gui/section/SectionFragment;->A:Z

    .line 1121
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iput-boolean v4, v0, Lflipboard/service/Section;->i:Z

    goto :goto_9

    .line 1124
    :cond_19
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->clear()V

    .line 1125
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1126
    invoke-direct {p0, v0}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/objs/FeedItem;)V

    goto :goto_b

    :cond_1a
    move v4, v3

    .line 1131
    goto :goto_a

    .line 1140
    :cond_1b
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 1141
    if-eqz v0, :cond_9

    .line 1142
    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v1

    invoke-virtual {v0, v9}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    goto/16 :goto_3

    :cond_1c
    move v5, v3

    goto/16 :goto_6
.end method

.method final a(ZZ)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2311
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    const-string v0, "SectionFragment:addPages"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 2312
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 2313
    if-nez v0, :cond_1

    .line 2583
    :cond_0
    :goto_0
    return-void

    .line 2316
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 2317
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v2, "SectionFragment.addPages called\n"

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2318
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v2, "\tnumber: "

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2319
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v2, "\tflipToStartWhenDone: "

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2320
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->K:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2321
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v2, "SectionFragment.addPages doingUpdate.compareAndSet(false, true) == true\n"

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2323
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->g:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2325
    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v2, "SectionFragment.addPages prepedingPages [sic] == "

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2326
    if-eqz v4, :cond_3

    .line 2327
    iget-object v8, p0, Lflipboard/gui/section/SectionFragment;->g:Lflipboard/gui/section/ListSingleThreadWrapper;

    .line 2328
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2334
    :goto_2
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2335
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v2, "SectionFragment.addPages itemsToPaginate is empty\n"

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2336
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->c()V

    .line 2337
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->K:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto/16 :goto_0

    :cond_2
    move v4, v1

    .line 2323
    goto :goto_1

    .line 2330
    :cond_3
    iget-object v8, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    .line 2331
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_2

    .line 2340
    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2341
    new-instance v5, Ljava/util/ArrayList;

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->m:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2342
    iget-object v9, p0, Lflipboard/gui/section/SectionFragment;->H:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lflipboard/gui/section/SectionFragment$17;

    move-object v1, p0

    move v6, p1

    move v7, p2

    invoke-direct/range {v0 .. v8}, Lflipboard/gui/section/SectionFragment$17;-><init>(Lflipboard/gui/section/SectionFragment;Ljava/util/ArrayList;Ljava/util/List;ZLjava/util/ArrayList;ZZLjava/util/List;)V

    invoke-interface {v9, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto/16 :goto_0
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 2829
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_0

    .line 2831
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0, p1}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lflipboard/gui/section/SectionAdPage;

    if-nez v0, :cond_0

    .line 2832
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0, p1}, Lflipboard/gui/flipping/FlipTransitionViews;->setCurrentViewIndex(I)V

    .line 2836
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    const-string v1, "SectionFragment:onScrubberPositionChanged"

    new-instance v2, Lflipboard/gui/section/SectionFragment$20;

    invoke-direct {v2, p0, p1}, Lflipboard/gui/section/SectionFragment$20;-><init>(Lflipboard/gui/section/SectionFragment;I)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 2844
    return-void
.end method

.method final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2206
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 2207
    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->aC:Z

    if-nez v0, :cond_0

    .line 2208
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ah:Z

    if-eqz v0, :cond_1

    .line 2209
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Size mismatch: Views are "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v2}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and Groups are "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v2}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " after "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 2210
    invoke-virtual {v0}, Ljava/lang/Exception;->fillInStackTrace()Ljava/lang/Throwable;

    .line 2211
    new-instance v1, Lflipboard/service/FlCrashListener;

    invoke-direct {v1}, Lflipboard/service/FlCrashListener;-><init>()V

    invoke-static {v0, v1}, Lnet/hockeyapp/android/ExceptionHandler;->a(Ljava/lang/Throwable;Lnet/hockeyapp/android/CrashManagerListener;)V

    .line 2218
    :cond_0
    :goto_0
    return-void

    .line 2213
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unwanted.different_number_of_views_than_groups_after_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    .line 2214
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->aC:Z

    goto :goto_0
.end method

.method final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2261
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    const-string v1, "SectionFragment:needMorePages"

    invoke-static {v1}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 2262
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v1, :cond_0

    .line 2263
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lflipboard/gui/section/SectionFragment;->q:I

    sub-int/2addr v1, v2

    const/4 v2, 0x5

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    .line 2265
    :cond_0
    return v0
.end method

.method final c(Ljava/lang/String;)I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 3779
    move v1, v2

    move v3, v2

    .line 3781
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    if-nez v1, :cond_4

    .line 3783
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0, v3}, Lflipboard/gui/section/ListSingleThreadWrapper;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    .line 3784
    iget-object v7, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    move v4, v2

    .line 3785
    :goto_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_3

    if-nez v1, :cond_3

    .line 3786
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 3787
    iget-object v5, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    .line 3788
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->P()Z

    move-result v6

    if-eqz v6, :cond_0

    move v5, v2

    .line 3790
    :goto_2
    iget-object v6, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_2

    if-nez v1, :cond_2

    .line 3791
    iget-object v1, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    .line 3792
    iget-object v1, v1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 3793
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v1, v6

    .line 3794
    goto :goto_2

    .line 3795
    :cond_0
    if-nez v5, :cond_1

    .line 3796
    iget-object v5, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-eqz v5, :cond_2

    iget-object v5, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    move v5, v2

    .line 3799
    :goto_3
    iget-object v6, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_2

    if-nez v1, :cond_2

    .line 3800
    iget-object v1, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    .line 3801
    iget-object v1, v1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 3802
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v1, v6

    .line 3803
    goto :goto_3

    .line 3806
    :cond_1
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 3808
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    .line 3809
    goto :goto_1

    .line 3810
    :cond_3
    if-nez v1, :cond_6

    .line 3811
    add-int/lit8 v0, v3, 0x1

    :goto_4
    move v3, v0

    .line 3813
    goto/16 :goto_0

    .line 3814
    :cond_4
    if-eqz v1, :cond_5

    .line 3817
    :goto_5
    return v3

    :cond_5
    const/4 v3, -0x1

    goto :goto_5

    :cond_6
    move v0, v3

    goto :goto_4
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2586
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v0

    const/16 v1, 0x32

    if-ge v0, v1, :cond_2

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->k()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2587
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "* SectionFragment.addPages runOnUIThread() ungroupedItems.size() < FETCH_MORE_ITEMS_LIMIT && !section.isEOF() == true\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2588
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2589
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lflipboard/service/Section;->a(ZLjava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2590
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "SectionFragment.addPages runOnUIThread() section.fetchMore(false) == true \n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2591
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragment;->u()V

    .line 2593
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "* SectionFragment.addPages runOnUIThread() section.fetchMore(false) == false \n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2598
    :cond_1
    :goto_0
    return-void

    .line 2596
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "* SectionFragment.addPages runOnUIThread() ungroupedItems.size() < FETCH_MORE_ITEMS_LIMIT && !section.isEOF() == false\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    goto :goto_0
.end method

.method final c(I)V
    .locals 1

    .prologue
    .line 4068
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    if-eqz v0, :cond_0

    .line 4069
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/SectionScrubber;->setNumberOfPages(I)V

    .line 4071
    :cond_0
    return-void
.end method

.method public final d()Lflipboard/gui/actionbar/FLActionBar;
    .locals 2

    .prologue
    .line 4332
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->e()Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v0

    .line 4333
    if-eqz v0, :cond_0

    .line 4334
    iget v1, p0, Lflipboard/gui/section/SectionFragment;->q:I

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 4335
    if-eqz v0, :cond_0

    .line 4336
    const v1, 0x7f0a004c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 4339
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final d(I)V
    .locals 1

    .prologue
    .line 4075
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    if-eqz v0, :cond_0

    .line 4076
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/SectionScrubber;->setPosition$2563266(I)V

    .line 4078
    :cond_0
    return-void
.end method

.method final e()Lflipboard/gui/flipping/FlipTransitionViews;
    .locals 2

    .prologue
    .line 2770
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2771
    if-eqz v0, :cond_0

    .line 2772
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlipTransitionViews;

    .line 2774
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final e(I)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 4491
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v0

    if-le v0, p1, :cond_9

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_9

    .line 4492
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0, p1}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v0

    .line 4493
    instance-of v1, v0, Lflipboard/gui/section/SectionAdPage;

    if-eqz v1, :cond_2

    check-cast v0, Lflipboard/gui/section/SectionAdPage;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionAdPage;->getAd()Lflipboard/objs/Ad;

    move-result-object v0

    move-object v2, v0

    .line 4494
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0, p1}, Lflipboard/gui/section/ListSingleThreadWrapper;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    .line 4496
    iget-object v1, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    if-eqz v1, :cond_3

    .line 4497
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->aj:Ljava/util/HashMap;

    iget-object v4, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v4, v4, Lflipboard/gui/section/GroupFranchiseMeta;->d:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/service/FLAdManager;

    .line 4498
    if-eqz v1, :cond_0

    .line 4499
    invoke-virtual {v1, p1, v2}, Lflipboard/service/FLAdManager;->a(ILflipboard/objs/Ad;)V

    .line 4507
    :cond_0
    :goto_1
    iget-object v1, v0, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    if-eqz v1, :cond_5

    .line 4508
    iget-object v1, v0, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    iget-object v1, v1, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    .line 4509
    if-eqz v1, :cond_4

    .line 4510
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    .line 4511
    iget-object v4, v1, Lflipboard/objs/FeedItem;->bP:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 4514
    iget-object v1, v1, Lflipboard/objs/FeedItem;->bP:Ljava/lang/String;

    sget-object v4, Lflipboard/service/FLAdManager$ImpressionEvent;->a:Lflipboard/service/FLAdManager$ImpressionEvent;

    invoke-static {v1, v4, v3}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;Lflipboard/service/FLAdManager$ImpressionEvent;Ljava/util/List;)V

    goto :goto_2

    :cond_2
    move-object v2, v3

    .line 4493
    goto :goto_0

    .line 4501
    :cond_3
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    if-eqz v1, :cond_0

    .line 4502
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    invoke-virtual {v1, p1, v2}, Lflipboard/service/FLAdManager;->a(ILflipboard/objs/Ad;)V

    goto :goto_1

    .line 4518
    :cond_4
    iget-object v1, v0, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    iget-object v1, v1, Lflipboard/objs/SidebarGroup;->i:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 4519
    iget-object v1, v0, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    iget-object v1, v1, Lflipboard/objs/SidebarGroup;->i:Ljava/lang/String;

    sget-object v2, Lflipboard/service/FLAdManager$ImpressionEvent;->a:Lflipboard/service/FLAdManager$ImpressionEvent;

    invoke-static {v1, v2, v3}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;Lflipboard/service/FLAdManager$ImpressionEvent;Ljava/util/List;)V

    .line 4524
    :cond_5
    iget-object v1, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    if-eqz v1, :cond_9

    .line 4525
    iget-object v0, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 4526
    iget-object v2, v0, Lflipboard/objs/FeedItem;->bP:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-boolean v2, v0, Lflipboard/objs/FeedItem;->bN:Z

    if-eqz v2, :cond_6

    .line 4529
    :cond_7
    iget-object v2, v0, Lflipboard/objs/FeedItem;->bP:Ljava/lang/String;

    sget-object v4, Lflipboard/service/FLAdManager$ImpressionEvent;->a:Lflipboard/service/FLAdManager$ImpressionEvent;

    iget-boolean v5, v0, Lflipboard/objs/FeedItem;->bN:Z

    if-eqz v5, :cond_8

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bS:Ljava/util/List;

    :goto_4
    invoke-static {v2, v4, v0}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;Lflipboard/service/FLAdManager$ImpressionEvent;Ljava/util/List;)V

    goto :goto_3

    :cond_8
    move-object v0, v3

    goto :goto_4

    .line 4534
    :cond_9
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2848
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_1

    .line 2849
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->d(I)V

    .line 2850
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->a(I)V

    .line 2851
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->t()V

    .line 2852
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v1

    add-int/lit8 v1, v1, -0x5

    if-lt v0, v1, :cond_0

    .line 2853
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/section/SectionFragment;->a(ZZ)V

    .line 2856
    :cond_0
    iget v0, p0, Lflipboard/gui/section/SectionFragment;->q:I

    .line 2857
    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->e(I)V

    .line 2859
    :cond_1
    return-void
.end method

.method final g()I
    .locals 2

    .prologue
    .line 3404
    const/4 v0, 0x0

    .line 3405
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v1, :cond_0

    .line 3406
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getWidth()I

    move-result v0

    .line 3408
    :cond_0
    if-eqz v0, :cond_1

    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v0

    goto :goto_0
.end method

.method final h()I
    .locals 2

    .prologue
    .line 3413
    const/4 v0, 0x0

    .line 3414
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v1, :cond_0

    .line 3415
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getHeight()I

    move-result v0

    .line 3417
    :cond_0
    if-eqz v0, :cond_1

    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lflipboard/util/AndroidUtil;->d()I

    move-result v0

    goto :goto_0
.end method

.method public final i()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 3839
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->z:Lflipboard/objs/FeedItem;

    .line 3840
    new-instance v0, Lflipboard/gui/section/Group;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-static {}, Lflipboard/gui/section/SectionFragment;->k()Lflipboard/objs/SectionPageTemplate;

    move-result-object v2

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->z:Lflipboard/objs/FeedItem;

    invoke-direct {v0, v1, v2, v3, v4}, Lflipboard/gui/section/Group;-><init>(Lflipboard/service/Section;Lflipboard/objs/SectionPageTemplate;Lflipboard/objs/FeedItem;Z)V

    .line 3841
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lflipboard/gui/section/SectionFragment;->a(Landroid/content/Context;Lflipboard/gui/section/Group;)Lflipboard/gui/section/SectionPage;

    move-result-object v1

    .line 3842
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v2}, Lflipboard/gui/section/ListSingleThreadWrapper;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 3843
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v2, v4}, Lflipboard/gui/section/ListSingleThreadWrapper;->remove(I)Ljava/lang/Object;

    .line 3845
    :cond_0
    invoke-virtual {p0, v4, v0}, Lflipboard/gui/section/SectionFragment;->a(ILflipboard/gui/section/Group;)V

    .line 3846
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3847
    invoke-virtual {v1}, Lflipboard/gui/section/SectionPage;->d()V

    .line 3849
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v1, v0}, Lflipboard/gui/section/SectionPage;->a(Lflipboard/service/Section;)V

    .line 3850
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v1, v0}, Lflipboard/gui/section/SectionPage;->setScrubber(Lflipboard/gui/section/SectionScrubber;)V

    .line 3851
    invoke-virtual {v1}, Lflipboard/gui/section/SectionPage;->f()V

    .line 3852
    invoke-virtual {v1}, Lflipboard/gui/section/SectionPage;->h()V

    .line 3853
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->d(Landroid/view/View;)V

    .line 3854
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 4061
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    if-eqz v0, :cond_0

    .line 4062
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionScrubber;->b()V

    .line 4064
    :cond_0
    return-void
.end method

.method public final l()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 4408
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 4409
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "SectionFragment.refreshSection() called\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 4410
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/User;)V

    .line 4411
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0, v2}, Lflipboard/service/Section;->d(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4412
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->m()V

    .line 4413
    iput-boolean v2, p0, Lflipboard/gui/section/SectionFragment;->x:Z

    .line 4414
    iput-boolean v2, p0, Lflipboard/gui/section/SectionFragment;->y:Z

    .line 4416
    :cond_0
    return-void
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 4419
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 4420
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 4421
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 4422
    new-instance v0, Lflipboard/gui/dialog/FLProgressDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLProgressDialogFragment;-><init>()V

    .line 4423
    const/4 v2, 0x1

    iput-boolean v2, v0, Lflipboard/gui/dialog/FLProgressDialogFragment;->j:Z

    .line 4424
    const v2, 0x7f0d01e5

    invoke-virtual {v0, v2}, Lflipboard/gui/dialog/FLProgressDialogFragment;->f(I)V

    .line 4425
    new-instance v2, Lflipboard/gui/section/SectionFragment$34;

    invoke-direct {v2, p0}, Lflipboard/gui/section/SectionFragment$34;-><init>(Lflipboard/gui/section/SectionFragment;)V

    iput-object v2, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 4436
    const-string v2, "feed_fetch_progress"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 4438
    :cond_0
    return-void
.end method

.method final n()V
    .locals 2

    .prologue
    .line 4450
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 4451
    if-eqz v0, :cond_0

    .line 4452
    const-string v1, "feed_fetch_progress"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lflipboard/gui/dialog/FLDialogFragment;

    .line 4453
    if-eqz v0, :cond_0

    .line 4454
    invoke-virtual {v0}, Lflipboard/gui/dialog/FLDialogFragment;->a()V

    .line 4457
    :cond_0
    return-void
.end method

.method public final o()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4537
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 4538
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v2}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v2

    invoke-virtual {v0, v2}, Lflipboard/gui/section/ListSingleThreadWrapper;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    iget-object v0, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v0, v1

    .line 4540
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v7, -0x1

    const/4 v5, 0x0

    .line 3653
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FlipboardFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 3654
    const/16 v0, 0x4e37

    if-ne p1, v0, :cond_6

    .line 3655
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->I:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 3656
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->I:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3658
    :cond_0
    if-ne p2, v7, :cond_3

    if-eqz p3, :cond_3

    .line 3659
    const-string v0, "sid"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3660
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3662
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    .line 3670
    if-nez v0, :cond_1

    .line 3671
    new-instance v0, Lflipboard/service/Section;

    const-string v2, "Title"

    const-string v3, "Service"

    const-string v4, "image"

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 3672
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 3674
    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 3675
    const-string v2, "source"

    const-string v3, "masthead"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3676
    const-string v2, "extra_section_group_id"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3677
    if-eqz v2, :cond_2

    .line 3678
    const-string v3, "sectionGroupIdentifier"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3680
    :cond_2
    const-string v2, "originSectionIdentifier"

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v3, v3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3681
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->startActivity(Landroid/content/Intent;)V

    .line 3745
    :cond_3
    :goto_0
    if-eqz p3, :cond_5

    .line 3746
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 3748
    if-eqz v1, :cond_d

    .line 3749
    const-string v0, "usage_intent_extra_flipcount"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 3750
    iget-wide v2, p0, Lflipboard/gui/section/SectionFragment;->ab:J

    const-string v4, "extra_result_active_time"

    const-wide/16 v8, 0x0

    invoke-virtual {v1, v4, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    add-long/2addr v2, v8

    iput-wide v2, p0, Lflipboard/gui/section/SectionFragment;->ab:J

    .line 3752
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    if-eqz v2, :cond_4

    .line 3753
    const-string v2, "pages_since_last_ad"

    invoke-virtual {v1, v2, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 3754
    if-ltz v2, :cond_4

    .line 3755
    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    iput v2, v3, Lflipboard/service/FLAdManager;->d:I

    .line 3760
    :cond_4
    :goto_1
    iget v2, p0, Lflipboard/gui/section/SectionFragment;->ad:I

    add-int/2addr v2, v0

    iput v2, p0, Lflipboard/gui/section/SectionFragment;->ad:I

    .line 3761
    sget-object v2, Lflipboard/gui/section/SectionFragment;->a:Lflipboard/util/Log;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v5

    iget v0, p0, Lflipboard/gui/section/SectionFragment;->ad:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v6

    const/4 v0, 0x2

    iget v3, p0, Lflipboard/gui/section/SectionFragment;->ac:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    .line 3764
    if-eqz v1, :cond_5

    const-string v0, "intent_extra_flag_inappropriate"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3765
    const-string v0, "intent_extra_flag_inappropriate_feed_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3766
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_5

    .line 3767
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v1, v0}, Lflipboard/service/Section;->a(Ljava/lang/String;)Lflipboard/objs/FeedItem;

    move-result-object v1

    .line 3768
    if-eqz v1, :cond_5

    .line 3769
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/SectionTabletActivity;

    .line 3770
    invoke-virtual {v0, v1}, Lflipboard/activities/SectionTabletActivity;->b(Lflipboard/objs/FeedItem;)V

    .line 3775
    :cond_5
    return-void

    .line 3685
    :cond_6
    const/16 v0, 0x4e42

    if-ne p1, v0, :cond_b

    .line 3686
    if-eqz p3, :cond_3

    .line 3687
    const-string v0, "extra_result_item_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3689
    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->c(Ljava/lang/String;)I

    move-result v1

    .line 3690
    if-ltz v1, :cond_a

    iget v0, p0, Lflipboard/gui/section/SectionFragment;->q:I

    if-eq v1, v0, :cond_a

    move v0, v6

    .line 3691
    :goto_2
    if-eqz v0, :cond_8

    .line 3692
    invoke-virtual {p0, v1}, Lflipboard/gui/section/SectionFragment;->a(I)V

    .line 3693
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->e()Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v2

    .line 3694
    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v3, :cond_7

    .line 3695
    invoke-virtual {v2, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->setCurrentViewIndex(I)V

    .line 3696
    invoke-virtual {p0, v1}, Lflipboard/gui/section/SectionFragment;->d(I)V

    .line 3698
    :cond_7
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    if-eqz v2, :cond_8

    .line 3699
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    invoke-virtual {v2, v1}, Lflipboard/service/FLAdManager;->a(I)V

    .line 3700
    invoke-virtual {p0, v1}, Lflipboard/gui/section/SectionFragment;->e(I)V

    .line 3707
    :cond_8
    const-string v2, "extra_result_is_flipmag"

    invoke-virtual {p3, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 3708
    if-nez v2, :cond_9

    if-eqz v0, :cond_3

    .line 3709
    :cond_9
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "SectionFragment:onAcitivtyResult:REQUEST_CODE_DETAIL_VIEW"

    new-instance v3, Lflipboard/gui/section/SectionFragment$28;

    invoke-direct {v3, p0, v1}, Lflipboard/gui/section/SectionFragment$28;-><init>(Lflipboard/gui/section/SectionFragment;I)V

    invoke-virtual {v0, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    :cond_a
    move v0, v5

    .line 3690
    goto :goto_2

    .line 3721
    :cond_b
    const/16 v0, 0x1e39

    if-ne p1, v0, :cond_c

    if-ne p2, v7, :cond_c

    if-eqz p3, :cond_c

    .line 3722
    const-string v0, "extra_invite"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3724
    :try_start_0
    new-instance v1, Lflipboard/json/JSONParser;

    invoke-direct {v1, v0}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lflipboard/json/JSONParser;->n()Lflipboard/objs/Invite;

    move-result-object v0

    .line 3725
    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/objs/Invite;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 3726
    :catch_0
    move-exception v0

    .line 3727
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 3730
    :cond_c
    const/16 v0, 0x4e43

    if-ne p1, v0, :cond_3

    .line 3731
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->a()Ljava/lang/String;

    move-result-object v0

    .line 3732
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->au:Ljava/lang/String;

    invoke-static {v0, v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3735
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 3736
    const-string v0, "source"

    const-string v2, "prominenceOverride"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3737
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 3738
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v2, v0, v1}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    .line 3739
    invoke-virtual {p0, v1}, Lflipboard/gui/section/SectionFragment;->startActivity(Landroid/content/Intent;)V

    .line 3740
    invoke-virtual {v0, v5, v5}, Lflipboard/activities/FlipboardActivity;->overridePendingTransition(II)V

    .line 3741
    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->finish()V

    goto/16 :goto_0

    :cond_d
    move v0, v5

    goto/16 :goto_1
.end method

.method public onBackToTopClicked(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1166
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getFlippableViews()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1167
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-boolean v0, v0, Lflipboard/service/Section;->F:Z

    if-eqz v0, :cond_1

    .line 1168
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->l()V

    .line 1173
    :cond_0
    :goto_0
    return-void

    .line 1170
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->e()Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->g()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1742
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 1788
    :cond_0
    :goto_0
    return-void

    .line 1745
    :cond_1
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1746
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 1747
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v2, 0x7f0d0218

    invoke-virtual {v1, v2}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 1750
    :cond_2
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1753
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lflipboard/objs/FeedItem;

    .line 1754
    if-eqz v5, :cond_0

    .line 1757
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v1, "nanoTappedItem"

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    .line 1758
    invoke-virtual {v5}, Lflipboard/objs/FeedItem;->ae()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, v5, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    if-eqz v0, :cond_4

    iget-object v0, v5, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1760
    new-instance v0, Lflipboard/service/Section;

    iget-object v1, v5, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v1, v1, Lflipboard/objs/FeedSection;->c:Ljava/lang/String;

    iget-object v2, v5, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v2, v2, Lflipboard/objs/FeedSection;->d:Ljava/lang/String;

    iget-object v3, v5, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->b:Ljava/lang/String;

    iget-object v4, v5, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    invoke-virtual {v4}, Lflipboard/objs/FeedSection;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v5, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-boolean v5, v5, Lflipboard/objs/FeedSection;->e:Z

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1761
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1762
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 1764
    :cond_3
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1765
    const-string v2, "source"

    const-string v3, "sectionItem"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1766
    const-string v2, "originSectionIdentifier"

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v3, v3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1767
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 1768
    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1769
    :cond_4
    invoke-virtual {v5}, Lflipboard/objs/FeedItem;->P()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1770
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1771
    const-string v0, "source"

    const-string v2, "item-album"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1772
    const-string v0, "originSectionIdentifier"

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1774
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-static {v5, v0, v1}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 1776
    :cond_5
    const/4 v0, 0x0

    .line 1777
    iget-boolean v1, v5, Lflipboard/objs/FeedItem;->bN:Z

    if-eqz v1, :cond_6

    .line 1778
    iget-object v0, v5, Lflipboard/objs/FeedItem;->bO:Ljava/lang/String;

    iget-object v1, v5, Lflipboard/objs/FeedItem;->bT:Ljava/util/List;

    invoke-static {v0, v1}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1779
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, v5, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1782
    :cond_6
    if-eqz v0, :cond_7

    .line 1783
    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1785
    :cond_7
    invoke-direct {p0, v5, p1}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/objs/FeedItem;Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/16 v5, 0x32

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 314
    if-nez p1, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->B:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 315
    iget-object p1, p0, Lflipboard/gui/section/SectionFragment;->B:Landroid/os/Bundle;

    .line 316
    iput-object v8, p0, Lflipboard/gui/section/SectionFragment;->B:Landroid/os/Bundle;

    .line 318
    :cond_0
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 319
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "sid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 321
    if-eqz v0, :cond_1

    .line 322
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    .line 324
    :cond_1
    new-instance v1, Lflipboard/gui/section/SectionFragment$1;

    invoke-direct {v1, p0}, Lflipboard/gui/section/SectionFragment$1;-><init>(Lflipboard/gui/section/SectionFragment;)V

    iput-object v1, p0, Lflipboard/gui/section/SectionFragment;->al:Lflipboard/util/Callback;

    .line 335
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    if-eqz v1, :cond_1c

    .line 336
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->l()Lflipboard/service/FlipboardManager$RootScreenStyle;

    move-result-object v1

    .line 337
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v1, v0, :cond_4

    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->ay:Z

    .line 338
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iput-boolean v3, v0, Lflipboard/service/Section;->i:Z

    .line 339
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->au:Ljava/lang/String;

    .line 340
    new-instance v0, Lflipboard/gui/section/ListSingleThreadWrapper;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-direct {v0, v4}, Lflipboard/gui/section/ListSingleThreadWrapper;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    .line 341
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->at:Ljava/util/Map;

    .line 342
    new-instance v0, Lflipboard/gui/section/ListSingleThreadWrapper;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-direct {v0, v4}, Lflipboard/gui/section/ListSingleThreadWrapper;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->i:Lflipboard/gui/section/ListSingleThreadWrapper;

    .line 343
    new-instance v0, Lflipboard/gui/section/ListSingleThreadWrapper;

    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    invoke-direct {v0, v4}, Lflipboard/gui/section/ListSingleThreadWrapper;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->g:Lflipboard/gui/section/ListSingleThreadWrapper;

    .line 344
    new-instance v0, Lflipboard/gui/section/ListSingleThreadWrapper;

    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    invoke-direct {v0, v4}, Lflipboard/gui/section/ListSingleThreadWrapper;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    .line 345
    new-instance v0, Lflipboard/gui/section/ListSingleThreadWrapper;

    new-instance v4, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    invoke-direct {v0, v4}, Lflipboard/gui/section/ListSingleThreadWrapper;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->m:Lflipboard/gui/section/ListSingleThreadWrapper;

    .line 348
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    if-eqz v0, :cond_5

    .line 349
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->m:Lflipboard/gui/section/ListSingleThreadWrapper;

    iget-object v4, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v4, v4, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v4, v4, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    invoke-virtual {v0, v4}, Lflipboard/gui/section/ListSingleThreadWrapper;->addAll(Ljava/util/Collection;)Z

    .line 350
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->m:Lflipboard/gui/section/ListSingleThreadWrapper;

    const-string v4, "pageboxGrid"

    invoke-direct {p0, v0, v4}, Lflipboard/gui/section/SectionFragment;->a(Ljava/util/List;Ljava/lang/String;)Z

    .line 351
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->m:Lflipboard/gui/section/ListSingleThreadWrapper;

    const-string v4, "pageboxCarousel"

    invoke-direct {p0, v0, v4}, Lflipboard/gui/section/SectionFragment;->a(Ljava/util/List;Ljava/lang/String;)Z

    .line 352
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->m:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    .line 353
    invoke-virtual {v0}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v5

    .line 354
    if-eqz v5, :cond_2

    iget-object v6, v5, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    const-string v7, "pageboxGrid"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    iget-object v5, v5, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    const-string v6, "pageboxCarousel"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 355
    :cond_3
    invoke-virtual {v0, v8}, Lflipboard/objs/SidebarGroup;->a(Lflipboard/service/Flap$TypedResultObserver;)V

    goto :goto_1

    :cond_4
    move v0, v3

    .line 337
    goto/16 :goto_0

    .line 361
    :cond_5
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-boolean v0, v0, Lflipboard/service/Section;->A:Z

    if-eqz v0, :cond_9

    :cond_6
    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->S:Z

    .line 363
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->R:Z

    .line 365
    new-instance v0, Ljava/util/ArrayList;

    iget-object v4, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v4}, Lflipboard/service/Section;->m()Ljava/util/List;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 366
    iget-object v4, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v4, v4, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v4}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 367
    iget-object v4, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v4, v4, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v5, "Syncing allItems and knownItems in onCreate\n"

    invoke-virtual {v4, v5}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 368
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 369
    iget-object v5, p0, Lflipboard/gui/section/SectionFragment;->i:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v5, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->add(Ljava/lang/Object;)Z

    .line 370
    iget-object v5, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v5, :cond_8

    .line 371
    iget-object v5, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v6, "sectionCover"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 373
    iget-object v5, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v5, v5, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v6, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v6}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Lflipboard/service/User;->a(Lflipboard/objs/FeedItem;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 374
    invoke-direct {p0, v0}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/objs/FeedItem;)V

    .line 379
    :cond_8
    iget-boolean v5, v0, Lflipboard/objs/FeedItem;->cd:Z

    if-eqz v5, :cond_7

    .line 380
    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->ak:Lflipboard/objs/FeedItem;

    goto :goto_3

    :cond_9
    move v0, v3

    .line 361
    goto :goto_2

    .line 385
    :cond_a
    sget-object v0, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v1, v0, :cond_10

    move v0, v2

    .line 386
    :goto_4
    if-eqz v0, :cond_c

    if-nez p1, :cond_c

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 388
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "last_viewed_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v5}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    invoke-interface {v1, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, v0, Lflipboard/service/Section;->l:J

    .line 389
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v4, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-wide v4, v4, Lflipboard/service/Section;->l:J

    sub-long/2addr v0, v4

    .line 390
    const-wide/16 v4, 0x2710

    cmp-long v0, v0, v4

    if-gtz v0, :cond_b

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->l()Z

    move-result v0

    if-nez v0, :cond_c

    .line 391
    :cond_b
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 392
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "fetching new because it\'s been a while\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 393
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0, v3}, Lflipboard/service/Section;->d(Z)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 395
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->l()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 396
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 397
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "we already had items\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 398
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->m()V

    .line 399
    iput-boolean v2, p0, Lflipboard/gui/section/SectionFragment;->y:Z

    .line 406
    :cond_c
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->a(Ljava/util/List;)V

    .line 408
    if-eqz p1, :cond_13

    .line 409
    const-string v0, "current_view_index"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->a(I)V

    .line 410
    const-string v0, "grouped_items"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 412
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    .line 413
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v1}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 414
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v4, " Groups restored from saved instance: "

    invoke-virtual {v1, v4}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 416
    :cond_d
    iget-boolean v1, p0, Lflipboard/gui/section/SectionFragment;->ay:Z

    if-eqz v1, :cond_e

    .line 417
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragment;->y()Lflipboard/gui/section/Group;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/section/SectionFragment;->av:Lflipboard/gui/section/Group;

    .line 418
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->av:Lflipboard/gui/section/Group;

    invoke-virtual {p0, v1}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/gui/section/Group;)V

    .line 420
    :cond_e
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_f
    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    .line 422
    invoke-virtual {v0}, Lflipboard/gui/section/Group;->b()Z

    move-result v1

    if-nez v1, :cond_11

    .line 423
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Restored group is not valid, skipping"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v5}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    :cond_10
    move v0, v3

    .line 385
    goto/16 :goto_4

    .line 426
    :cond_11
    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/gui/section/Group;)V

    .line 427
    invoke-direct {p0, v0}, Lflipboard/gui/section/SectionFragment;->b(Lflipboard/gui/section/Group;)V

    .line 430
    iget-object v1, v0, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    if-eqz v1, :cond_f

    .line 431
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->m:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_12
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/SidebarGroup;

    .line 432
    iget-object v6, v0, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    invoke-virtual {v1, v6}, Lflipboard/objs/SidebarGroup;->a(Lflipboard/objs/SidebarGroup;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 433
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->m:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->remove(Ljava/lang/Object;)Z

    goto :goto_5

    .line 441
    :cond_13
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->aj:Ljava/util/HashMap;

    .line 442
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->ai:Ljava/util/HashMap;

    .line 444
    new-instance v0, Lflipboard/gui/section/SectionFragment$2;

    invoke-direct {v0, p0}, Lflipboard/gui/section/SectionFragment$2;-><init>(Lflipboard/gui/section/SectionFragment;)V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->T:Lflipboard/util/Observer;

    .line 580
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->T:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    .line 582
    new-instance v0, Lflipboard/gui/section/SectionFragment$3;

    invoke-direct {v0, p0}, Lflipboard/gui/section/SectionFragment$3;-><init>(Lflipboard/gui/section/SectionFragment;)V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->U:Lflipboard/util/Observer;

    .line 602
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->U:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Lflipboard/util/Observer;)V

    .line 604
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "section"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    .line 605
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    const-string v1, "action"

    const-string v4, "viewed"

    invoke-virtual {v0, v1, v4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 606
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    const-string v1, "sectionType"

    const-string v4, "feed"

    invoke-virtual {v0, v1, v4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 607
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v0

    .line 608
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v4, "use_legacy_cover_stories"

    invoke-interface {v1, v4, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 609
    if-eqz v0, :cond_14

    if-eqz v1, :cond_14

    .line 610
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    const-string v1, "forceOldCoverStories"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 612
    :cond_14
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/io/UsageEvent;->d(Ljava/lang/String;)V

    .line 614
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 615
    const-string v1, "extra_is_top_level"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lflipboard/gui/section/SectionFragment;->D:Z

    .line 616
    const-string v1, "extra_content_discovery_from_source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 617
    if-eqz v0, :cond_17

    .line 620
    const-string v1, "sponsoredCampaign"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/section/SectionFragment;->W:Ljava/lang/String;

    .line 621
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->W:Ljava/lang/String;

    if-eqz v1, :cond_15

    .line 622
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    const-string v4, "sponsoredCampaign"

    iget-object v5, p0, Lflipboard/gui/section/SectionFragment;->W:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 623
    const-string v1, "sponsoredCampaign"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 625
    :cond_15
    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 626
    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lflipboard/usage/UsageV2MigrationHelper;->a(Ljava/lang/String;)Lflipboard/objs/UsageEventV2$SectionNavFrom;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/section/SectionFragment;->V:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    .line 628
    :cond_16
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    invoke-virtual {v1, v0}, Lflipboard/io/UsageEvent;->a(Landroid/os/Bundle;)V

    .line 639
    :cond_17
    :goto_6
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 640
    if-eqz v0, :cond_1a

    .line 641
    const-string v1, "extra_launched_from_samsung"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_18

    const-string v1, "launched_by_sstream"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_19

    :cond_18
    move v3, v2

    :cond_19
    iput-boolean v3, p0, Lflipboard/gui/section/SectionFragment;->C:Z

    .line 644
    :cond_1a
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    .line 645
    invoke-static {}, Lflipboard/util/AndroidUtil;->m()Z

    move-result v0

    if-nez v0, :cond_1b

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->b()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 659
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 660
    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->y()V

    .line 662
    :cond_1b
    return-void

    .line 635
    :cond_1c
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 636
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v1, v2, [Ljava/lang/Object;

    aput-object v0, v1, v3

    goto :goto_6
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v11, -0x1

    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v10, 0x0

    .line 1282
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    if-nez v0, :cond_0

    move-object v0, v8

    .line 1518
    :goto_0
    return-object v0

    .line 1286
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->ab:Lflipboard/objs/CrashInfo;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CrashInfo;->a:Ljava/lang/String;

    .line 1288
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1289
    new-instance v0, Lflipboard/gui/section/SectionTabletView;

    invoke-direct {v0, v1, p0}, Lflipboard/gui/section/SectionTabletView;-><init>(Landroid/content/Context;Lflipboard/gui/section/SectionFragment;)V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    .line 1290
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0, p0}, Lflipboard/gui/flipping/FlipTransitionViews;->a(Lflipboard/util/Observer;)V

    .line 1292
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    new-instance v2, Lflipboard/gui/section/SectionFragment$10;

    invoke-direct {v2, p0}, Lflipboard/gui/section/SectionFragment$10;-><init>(Lflipboard/gui/section/SectionFragment;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/flipping/FlipTransitionViews;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1305
    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->am:Z

    if-eqz v0, :cond_2

    .line 1306
    const v0, 0x7f03010f

    invoke-static {v1, v0, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/SectionScrubber;

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    .line 1307
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    .line 1308
    if-eqz v0, :cond_1

    const-string v2, "nytimes"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1309
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionScrubber;->a()V

    .line 1311
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {}, Lflipboard/service/User;->u()Z

    move-result v2

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v3, "SectionScrubber:showCompose"

    invoke-static {v3}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    if-eqz v2, :cond_4

    iget-object v0, v0, Lflipboard/gui/section/SectionScrubber;->d:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 1312
    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v0, p0}, Lflipboard/gui/section/SectionScrubber;->setScrubberListener(Lflipboard/gui/section/SectionScrubber$ScrubberListener;)V

    .line 1313
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionScrubber;->b()V

    .line 1314
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1317
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1320
    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->ay:Z

    if-eqz v0, :cond_5

    .line 1321
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragment;->y()Lflipboard/gui/section/Group;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->av:Lflipboard/gui/section/Group;

    .line 1322
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    .line 1323
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->av:Lflipboard/gui/section/Group;

    move-object v1, v8

    .line 1349
    :goto_2
    if-eqz v0, :cond_9

    .line 1350
    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/gui/section/Group;)V

    .line 1351
    invoke-direct {p0, v0}, Lflipboard/gui/section/SectionFragment;->c(Lflipboard/gui/section/Group;)Lflipboard/gui/section/SectionPage;

    move-result-object v0

    .line 1352
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1, v11, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    .line 1353
    invoke-virtual {p0, v10, v6}, Lflipboard/gui/section/SectionFragment;->a(ZZ)V

    .line 1376
    :goto_3
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragment;->r()V

    .line 1378
    invoke-virtual {p0, v6}, Lflipboard/gui/section/SectionFragment;->c(I)V

    .line 1504
    :goto_4
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_intent_start_time"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 1505
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_3

    .line 1506
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "extra_intent_start_time"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 1507
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v2}, Lflipboard/gui/flipping/FlipTransitionViews;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lflipboard/gui/section/SectionFragment$12;

    invoke-direct {v3, p0, v0, v1}, Lflipboard/gui/section/SectionFragment$12;-><init>(Lflipboard/gui/section/SectionFragment;J)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1517
    :cond_3
    sget-object v0, Lflipboard/gui/section/SectionFragment;->G:Lcom/squareup/otto/Bus;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->aA:Lflipboard/gui/section/SectionFragment$DetailActivityCommunicationsLink;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->a(Ljava/lang/Object;)V

    .line 1518
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    goto/16 :goto_0

    .line 1311
    :cond_4
    iget-object v0, v0, Lflipboard/gui/section/SectionScrubber;->d:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 1325
    :cond_5
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->D()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1326
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->z:Lflipboard/objs/FeedItem;

    .line 1327
    new-instance v0, Lflipboard/gui/section/Group;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-static {}, Lflipboard/gui/section/SectionFragment;->k()Lflipboard/objs/SectionPageTemplate;

    move-result-object v2

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->z:Lflipboard/objs/FeedItem;

    invoke-direct {v0, v1, v2, v3, v6}, Lflipboard/gui/section/Group;-><init>(Lflipboard/service/Section;Lflipboard/objs/SectionPageTemplate;Lflipboard/objs/FeedItem;Z)V

    .line 1328
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->z:Lflipboard/objs/FeedItem;

    invoke-virtual {v1, v2}, Lflipboard/gui/section/ListSingleThreadWrapper;->remove(Ljava/lang/Object;)Z

    move-object v1, v8

    goto :goto_2

    .line 1329
    :cond_6
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->C()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1330
    invoke-direct {p0, v8}, Lflipboard/gui/section/SectionFragment;->b(Lflipboard/objs/FeedItem;)Lflipboard/gui/section/Group;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/SectionFragment;->ax:Lflipboard/gui/section/Group;

    .line 1331
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ax:Lflipboard/gui/section/Group;

    if-eqz v0, :cond_12

    .line 1333
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v0

    if-nez v0, :cond_12

    .line 1334
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->ax:Lflipboard/gui/section/Group;

    invoke-direct {p0, v0}, Lflipboard/gui/section/SectionFragment;->b(Lflipboard/gui/section/Group;)V

    move-object v1, v8

    move-object v0, v8

    goto/16 :goto_2

    .line 1338
    :cond_7
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    .line 1340
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0, v10}, Lflipboard/gui/section/ListSingleThreadWrapper;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1341
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->O()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1343
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/objs/FeedItem;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    move-object v0, v8

    goto/16 :goto_2

    .line 1345
    :cond_8
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->m:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->g()I

    move-result v4

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->h()I

    move-result v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lflipboard/gui/section/SectionFragment;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;IIZ)Lflipboard/gui/section/Group;

    move-result-object v0

    move-object v1, v8

    goto/16 :goto_2

    .line 1354
    :cond_9
    if-eqz v1, :cond_b

    .line 1355
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0, v10}, Lflipboard/gui/section/ListSingleThreadWrapper;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1356
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 1360
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/section/Group;

    .line 1361
    invoke-virtual {p0, v1}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/gui/section/Group;)V

    .line 1362
    invoke-direct {p0, v1}, Lflipboard/gui/section/SectionFragment;->c(Lflipboard/gui/section/Group;)Lflipboard/gui/section/SectionPage;

    move-result-object v1

    .line 1363
    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v3, v11, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    goto :goto_5

    .line 1367
    :cond_a
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->h:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->remove(Ljava/lang/Object;)Z

    .line 1368
    invoke-virtual {p0, v10, v6}, Lflipboard/gui/section/SectionFragment;->a(ZZ)V

    goto/16 :goto_3

    .line 1370
    :cond_b
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragment;->w()V

    .line 1371
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragment;->u()V

    .line 1372
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->al:Lflipboard/util/Callback;

    invoke-virtual {v0, v10, v1, v10, v8}, Lflipboard/service/Section;->a(ZLflipboard/util/Callback;ZLandroid/os/Bundle;)Z

    goto/16 :goto_3

    .line 1382
    :cond_c
    iget v0, p0, Lflipboard/gui/section/SectionFragment;->q:I

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v2}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->a(I)V

    .line 1384
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    iget v2, p0, Lflipboard/gui/section/SectionFragment;->q:I

    invoke-virtual {v0, v2}, Lflipboard/gui/section/ListSingleThreadWrapper;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/gui/section/Group;

    .line 1386
    iget v7, p0, Lflipboard/gui/section/SectionFragment;->q:I

    .line 1388
    sget-object v0, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iget v3, p0, Lflipboard/gui/section/SectionFragment;->q:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v10

    instance-of v3, v2, Lflipboard/gui/section/Group$AdGroup;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v0, v6

    .line 1390
    instance-of v0, v2, Lflipboard/gui/section/Group$AdGroup;

    if-eqz v0, :cond_e

    move-object v0, v2

    .line 1391
    check-cast v0, Lflipboard/gui/section/Group$AdGroup;

    iget-object v9, v0, Lflipboard/gui/section/Group$AdGroup;->l:Lflipboard/service/FLAdManager$AdAsset;

    .line 1392
    new-instance v0, Lflipboard/gui/section/SectionAdPage;

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v4, v9, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget-boolean v5, p0, Lflipboard/gui/section/SectionFragment;->D:Z

    invoke-direct/range {v0 .. v5}, Lflipboard/gui/section/SectionAdPage;-><init>(Landroid/content/Context;Lflipboard/gui/section/Group;Lflipboard/service/Section;Lflipboard/objs/Ad;Z)V

    .line 1393
    iget-object v2, v9, Lflipboard/service/FLAdManager$AdAsset;->b:Lflipboard/objs/Ad$Asset;

    if-eqz v2, :cond_d

    .line 1394
    const v2, 0x7f03009b

    invoke-static {v1, v2, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/item/AdItem;

    .line 1395
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v2

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lflipboard/util/AndroidUtil;->a(Landroid/app/Activity;)I

    move-result v3

    invoke-virtual {v1, v9, v2, v3}, Lflipboard/gui/item/AdItem;->a(Lflipboard/service/FLAdManager$AdAsset;II)V

    .line 1396
    iget-object v2, v9, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget-object v2, v2, Lflipboard/objs/Ad;->k:Lflipboard/objs/Ad$VideoInfo;

    invoke-virtual {v1, v2}, Lflipboard/gui/item/AdItem;->setVideoInfo(Lflipboard/objs/Ad$VideoInfo;)V

    .line 1397
    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionPage;->a(Landroid/view/View;)V

    .line 1398
    iput-boolean v6, v0, Lflipboard/gui/section/SectionPage;->p:Z

    .line 1399
    invoke-virtual {v0}, Lflipboard/gui/section/SectionPage;->f()V

    .line 1400
    invoke-virtual {v0}, Lflipboard/gui/section/SectionPage;->h()V

    .line 1401
    sget-object v1, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    new-array v1, v6, [Ljava/lang/Object;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v2}, Lflipboard/gui/flipping/FlipTransitionViews;->getFlippableViews()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    .line 1402
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1, v11, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    move v0, v7

    .line 1435
    :goto_6
    iget v1, p0, Lflipboard/gui/section/SectionFragment;->q:I

    .line 1436
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v2}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v2

    invoke-virtual {p0, v2}, Lflipboard/gui/section/SectionFragment;->c(I)V

    .line 1437
    iget v2, p0, Lflipboard/gui/section/SectionFragment;->q:I

    invoke-virtual {p0, v2}, Lflipboard/gui/section/SectionFragment;->d(I)V

    .line 1439
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1440
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v4, "SectionFragment:onCreateView:!groupedItems.isEmpty()"

    new-instance v5, Lflipboard/gui/section/SectionFragment$11;

    invoke-direct {v5, p0, v2, v0, v1}, Lflipboard/gui/section/SectionFragment$11;-><init>(Lflipboard/gui/section/SectionFragment;Ljava/util/List;II)V

    invoke-virtual {v3, v4, v5}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto/16 :goto_4

    .line 1404
    :cond_d
    add-int/lit8 v2, v7, 0x1

    .line 1405
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0, v2}, Lflipboard/gui/section/ListSingleThreadWrapper;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    .line 1406
    invoke-virtual {p0, v1, v0}, Lflipboard/gui/section/SectionFragment;->a(Landroid/content/Context;Lflipboard/gui/section/Group;)Lflipboard/gui/section/SectionPage;

    move-result-object v0

    .line 1407
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionPage;->setScrubber(Lflipboard/gui/section/SectionScrubber;)V

    .line 1408
    iget-boolean v1, p0, Lflipboard/gui/section/SectionFragment;->C:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionPage;->setIsOpenedFromThirdParty(Z)V

    .line 1409
    invoke-virtual {v0}, Lflipboard/gui/section/SectionPage;->f()V

    .line 1410
    invoke-virtual {v0}, Lflipboard/gui/section/SectionPage;->h()V

    .line 1411
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1, v11, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    move v0, v2

    .line 1413
    goto :goto_6

    .line 1414
    :cond_e
    invoke-virtual {p0, v1, v2}, Lflipboard/gui/section/SectionFragment;->a(Landroid/content/Context;Lflipboard/gui/section/Group;)Lflipboard/gui/section/SectionPage;

    move-result-object v0

    .line 1415
    if-eqz v0, :cond_11

    .line 1416
    iget-boolean v1, v2, Lflipboard/gui/section/Group;->e:Z

    if-eqz v1, :cond_10

    .line 1417
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->f()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1418
    invoke-virtual {v0}, Lflipboard/gui/section/SectionPage;->d()V

    .line 1420
    :cond_f
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionPage;->a(Lflipboard/service/Section;)V

    .line 1423
    :cond_10
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionPage;->setScrubber(Lflipboard/gui/section/SectionScrubber;)V

    .line 1424
    iget-boolean v1, p0, Lflipboard/gui/section/SectionFragment;->C:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionPage;->setIsOpenedFromThirdParty(Z)V

    .line 1425
    invoke-virtual {v0}, Lflipboard/gui/section/SectionPage;->f()V

    .line 1426
    invoke-virtual {v0}, Lflipboard/gui/section/SectionPage;->h()V

    .line 1427
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1, v11, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    move-result-object v0

    .line 1428
    invoke-virtual {v2}, Lflipboard/gui/section/Group;->a()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1429
    iput-boolean v6, v0, Lflipboard/gui/flipping/FlippingContainer;->k:Z

    :cond_11
    move v0, v7

    goto/16 :goto_6

    :cond_12
    move-object v1, v8

    move-object v0, v8

    goto/16 :goto_2
.end method

.method public onDestroy()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 1604
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onDestroy()V

    .line 1605
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->T:Lflipboard/util/Observer;

    if-eqz v0, :cond_0

    .line 1606
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->T:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    .line 1608
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->U:Lflipboard/util/Observer;

    if-eqz v0, :cond_1

    .line 1609
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->U:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->c(Lflipboard/util/Observer;)V

    .line 1611
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_2

    .line 1612
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0, p0}, Lflipboard/gui/flipping/FlipTransitionViews;->b(Lflipboard/util/Observer;)V

    .line 1614
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->u:Lflipboard/service/audio/FLAudioManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->v:Lflipboard/util/Observer;

    if-eqz v0, :cond_3

    .line 1615
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->u:Lflipboard/service/audio/FLAudioManager;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->v:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/audio/FLAudioManager;->c(Lflipboard/util/Observer;)V

    .line 1618
    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    if-eqz v0, :cond_5

    .line 1619
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->O:Lflipboard/util/Observer;

    if-eqz v0, :cond_4

    .line 1620
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->O:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/FLAdManager;->c(Lflipboard/util/Observer;)V

    .line 1621
    iput-object v4, p0, Lflipboard/gui/section/SectionFragment;->O:Lflipboard/util/Observer;

    .line 1623
    :cond_4
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    invoke-virtual {v0, v5, v6}, Lflipboard/service/FLAdManager;->a(IZ)V

    .line 1627
    :cond_5
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->aj:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/FLAdManager;

    .line 1628
    if-eqz v0, :cond_6

    .line 1629
    iget-object v1, v0, Lflipboard/service/FLAdManager;->b:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v3, v1, Lflipboard/gui/section/GroupFranchiseMeta;->d:Ljava/lang/String;

    .line 1630
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->ai:Ljava/util/HashMap;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/FLAdManager;->c(Lflipboard/util/Observer;)V

    .line 1631
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->ai:Ljava/util/HashMap;

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1632
    invoke-virtual {v0, v5, v6}, Lflipboard/service/FLAdManager;->a(IZ)V

    goto :goto_0

    .line 1636
    :cond_7
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    if-eqz v0, :cond_8

    .line 1637
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v0, v4}, Lflipboard/gui/section/SectionScrubber;->setScrubberListener(Lflipboard/gui/section/SectionScrubber$ScrubberListener;)V

    .line 1640
    :cond_8
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    if-eqz v0, :cond_f

    .line 1641
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    const-string v1, "sectionIdentifier"

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1642
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    const-string v1, "partnerID"

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v2, v2, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1644
    iget v0, p0, Lflipboard/gui/section/SectionFragment;->e:I

    if-gtz v0, :cond_9

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->f:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 1645
    :cond_9
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    const-string v1, "viewCart"

    iget v2, p0, Lflipboard/gui/section/SectionFragment;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1647
    :cond_a
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget v0, v0, Lflipboard/service/FlipboardManager;->an:I

    .line 1648
    if-gtz v0, :cond_b

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v1, v1, Lflipboard/service/Section$Meta;->f:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 1649
    :cond_b
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    const-string v2, "addToCart"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1652
    :cond_c
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    iget-wide v2, p0, Lflipboard/gui/section/SectionFragment;->aa:J

    iget-wide v4, p0, Lflipboard/gui/section/SectionFragment;->ab:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Lflipboard/io/UsageEvent;->g:J

    .line 1653
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    const-string v1, "layoutViewDuration"

    iget-wide v2, p0, Lflipboard/gui/section/SectionFragment;->aa:J

    long-to-double v2, v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1654
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    const-string v1, "totalFlipCount"

    iget v2, p0, Lflipboard/gui/section/SectionFragment;->ad:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1655
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    const-string v1, "flipCount"

    iget v2, p0, Lflipboard/gui/section/SectionFragment;->ae:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1656
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    const-string v1, "maxSectionDepth"

    iget v2, p0, Lflipboard/gui/section/SectionFragment;->ah:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1657
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    invoke-direct {p0, v0}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/io/UsageEvent;)V

    .line 1659
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->E:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 1660
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    const-string v1, "strategy"

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1663
    :cond_d
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "enable_new_usage_v2_events"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1664
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    const-string v1, "deprecated"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1666
    :cond_e
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->X:Lflipboard/io/UsageEvent;

    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->b()V

    .line 1668
    :cond_f
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->c:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->a:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->z:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-wide v2, p0, Lflipboard/gui/section/SectionFragment;->aa:J

    iget-wide v4, p0, Lflipboard/gui/section/SectionFragment;->ab:J

    add-long/2addr v2, v4

    .line 1669
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->A:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-wide v2, p0, Lflipboard/gui/section/SectionFragment;->aa:J

    .line 1670
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    .line 1671
    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->k:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    .line 1672
    iget-object v2, v2, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v2, v2, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->t:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    .line 1673
    invoke-virtual {v2}, Lflipboard/service/Section;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->i:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->W:Ljava/lang/String;

    .line 1674
    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->x:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget v2, p0, Lflipboard/gui/section/SectionFragment;->af:I

    .line 1675
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->w:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->ag:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 1676
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->y:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget v2, p0, Lflipboard/gui/section/SectionFragment;->ae:I

    add-int/lit8 v2, v2, 0x1

    .line 1677
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->V:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    .line 1678
    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    .line 1679
    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    .line 1680
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 1681
    return-void
.end method

.method public onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1574
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_0

    .line 1575
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0, p0}, Lflipboard/gui/flipping/FlipTransitionViews;->b(Lflipboard/util/Observer;)V

    .line 1576
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0, v4}, Lflipboard/gui/flipping/FlipTransitionViews;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1577
    iget v0, p0, Lflipboard/gui/section/SectionFragment;->ad:I

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    iget v1, v1, Lflipboard/gui/flipping/FlipTransitionViews;->y:I

    add-int/2addr v0, v1

    iput v0, p0, Lflipboard/gui/section/SectionFragment;->ad:I

    .line 1578
    iget v0, p0, Lflipboard/gui/section/SectionFragment;->ae:I

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    iget v1, v1, Lflipboard/gui/flipping/FlipTransitionViews;->y:I

    add-int/2addr v0, v1

    iput v0, p0, Lflipboard/gui/section/SectionFragment;->ae:I

    .line 1579
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    iget v0, v0, Lflipboard/gui/flipping/FlipTransitionViews;->z:I

    iget v1, p0, Lflipboard/gui/section/SectionFragment;->ah:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/SectionFragment;->ah:I

    .line 1580
    iput-object v4, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    .line 1582
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->O:Lflipboard/util/Observer;

    if-eqz v0, :cond_1

    .line 1583
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->w:Lflipboard/service/FLAdManager;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->O:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/FLAdManager;->c(Lflipboard/util/Observer;)V

    .line 1584
    iput-object v4, p0, Lflipboard/gui/section/SectionFragment;->O:Lflipboard/util/Observer;

    .line 1587
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->aj:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/FLAdManager;

    .line 1588
    if-eqz v0, :cond_2

    .line 1589
    iget-object v1, v0, Lflipboard/service/FLAdManager;->b:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v3, v1, Lflipboard/gui/section/GroupFranchiseMeta;->d:Ljava/lang/String;

    .line 1590
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->ai:Ljava/util/HashMap;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/FLAdManager;->c(Lflipboard/util/Observer;)V

    .line 1591
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->aj:Ljava/util/HashMap;

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1595
    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lflipboard/service/Section;->i:Z

    .line 1596
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/service/Section;->l:J

    .line 1597
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "last_viewed_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-wide v2, v2, Lflipboard/service/Section;->l:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1598
    sget-object v0, Lflipboard/gui/section/SectionFragment;->G:Lcom/squareup/otto/Bus;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->aA:Lflipboard/gui/section/SectionFragment$DetailActivityCommunicationsLink;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->b(Ljava/lang/Object;)V

    .line 1599
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onDestroyView()V

    .line 1600
    return-void
.end method

.method public onLeftScrubberLabelClicked(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2869
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->e()Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v0

    .line 2870
    if-eqz v0, :cond_0

    .line 2871
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->g()V

    .line 2872
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    .line 2873
    if-eqz v1, :cond_0

    .line 2874
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getRenderer()Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    move-result-object v0

    new-instance v2, Lflipboard/gui/section/SectionFragment$21;

    invoke-direct {v2, p0, v1}, Lflipboard/gui/section/SectionFragment$21;-><init>(Lflipboard/gui/section/SectionFragment;Lflipboard/gui/section/SectionScrubber;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/Runnable;)V

    .line 2895
    :cond_0
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 276
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->e()Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionBase;->getRunningFlips()I

    move-result v1

    if-lez v1, :cond_1

    .line 277
    :cond_0
    :goto_0
    return v5

    .line 276
    :cond_1
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ag:Z

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    check-cast p1, Lflipboard/gui/item/TabletItem;

    invoke-interface {p1}, Lflipboard/gui/item/TabletItem;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v2

    sget-object v3, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->a:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    invoke-static {v0, v1, v2, v3}, Lflipboard/util/SocialHelper;->b(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UsageEventV2$FlipItemNavFrom;)V

    goto :goto_0

    :cond_2
    instance-of v1, p1, Lflipboard/gui/item/TabletItem;

    if-eqz v1, :cond_0

    new-instance v1, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Redo page and print scores to device log"

    aput-object v3, v2, v5

    const/4 v3, 0x1

    const-string v4, "Get score for current box"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "Remeasure current page"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "Pick layout"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "Check duplicates"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "More"

    aput-object v4, v2, v3

    iput-object v2, v1, Lflipboard/gui/dialog/FLAlertDialogFragment;->q:[Ljava/lang/CharSequence;

    new-instance v2, Lflipboard/gui/section/SectionFragment$30;

    invoke-direct {v2, p0, v0, p1}, Lflipboard/gui/section/SectionFragment$30;-><init>(Lflipboard/gui/section/SectionFragment;Lflipboard/gui/flipping/FlipTransitionBase;Landroid/view/View;)V

    iput-object v2, v1, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "debug_mark"

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPageboxClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 4353
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lflipboard/objs/FeedItem;

    .line 4354
    new-instance v0, Lflipboard/service/Section;

    iget-object v1, v6, Lflipboard/objs/FeedItem;->d:Ljava/lang/String;

    iget-object v2, v6, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 4355
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 4356
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 4357
    const-string v1, "source"

    const-string v4, "pagebox"

    invoke-virtual {v2, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4358
    const-string v1, "sectionGroupIdentifier"

    iget-object v4, v6, Lflipboard/objs/FeedItem;->c:Ljava/lang/String;

    invoke-virtual {v2, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4360
    iget-object v1, v6, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, "pageboxList"

    iget-object v4, v6, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4361
    const-string v1, "list"

    .line 4367
    :goto_0
    const-string v4, "presentationStyle"

    invoke-virtual {v2, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4368
    iget-object v1, v6, Lflipboard/objs/FeedItem;->bU:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4369
    const-string v1, "sponsoredCampaign"

    iget-object v4, v6, Lflipboard/objs/FeedItem;->bU:Ljava/lang/String;

    invoke-virtual {v2, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4371
    :cond_0
    const-string v1, "originSectionIdentifier"

    iget-object v4, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v4, v4, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v2, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4372
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->startActivity(Landroid/content/Intent;)V

    .line 4375
    iget-object v0, v6, Lflipboard/objs/FeedItem;->bO:Ljava/lang/String;

    invoke-static {v0, v3}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 4376
    return-void

    .line 4362
    :cond_1
    iget-object v1, v6, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, "pageboxGrid"

    iget-object v4, v6, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4363
    const-string v1, "grid"

    goto :goto_0

    .line 4365
    :cond_2
    iget-object v1, v6, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    goto :goto_0
.end method

.method public onPause()V
    .locals 6

    .prologue
    .line 1690
    iget-wide v0, p0, Lflipboard/gui/section/SectionFragment;->aa:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lflipboard/gui/section/SectionFragment;->Z:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lflipboard/gui/section/SectionFragment;->aa:J

    .line 1691
    const-string v0, "pause"

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->a(Ljava/lang/String;)V

    .line 1692
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onPause()V

    .line 1693
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 1

    .prologue
    .line 1244
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->d()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    .line 1245
    if-eqz v0, :cond_0

    .line 1246
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->a()V

    .line 1248
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 1697
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onResume()V

    .line 1698
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 1699
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v3, v0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v0, v0, Lflipboard/service/User;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    if-eqz v1, :cond_3

    .line 1700
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 1736
    :cond_1
    :goto_1
    return-void

    .line 1699
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1705
    :cond_3
    iget-wide v0, p0, Lflipboard/gui/section/SectionFragment;->Z:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 1706
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->b:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->a:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    .line 1707
    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->k:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    .line 1708
    iget-object v2, v2, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v2, v2, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->i:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->W:Ljava/lang/String;

    .line 1709
    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->V:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    .line 1710
    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->t:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    .line 1711
    invoke-virtual {v2}, Lflipboard/service/Section;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    .line 1712
    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    .line 1713
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 1716
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/gui/section/SectionFragment;->Z:J

    .line 1718
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_5

    .line 1720
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    .line 1723
    invoke-direct {p0, v0}, Lflipboard/gui/section/SectionFragment;->f(I)V

    .line 1724
    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionFragment;->e(I)V

    .line 1726
    if-ltz v0, :cond_5

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->aq:Lflipboard/gui/section/Group;

    if-nez v1, :cond_5

    .line 1727
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    invoke-direct {p0, v0}, Lflipboard/gui/section/SectionFragment;->d(Lflipboard/gui/section/Group;)V

    .line 1731
    :cond_5
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    invoke-virtual {v0}, Lflipboard/service/HintManager;->a()V

    .line 1732
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/SectionPage;

    .line 1733
    if-eqz v0, :cond_1

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->F:Landroid/support/v4/widget/DrawerLayout;

    invoke-static {v1}, Lflipboard/gui/personal/FLDrawerLayout;->b(Landroid/support/v4/widget/DrawerLayout;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1734
    invoke-virtual {v0}, Lflipboard/gui/section/SectionPage;->c()V

    goto/16 :goto_1
.end method

.method public onRightScrubberLabelClicked(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2905
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->e()Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v0

    .line 2906
    if-eqz v0, :cond_0

    .line 2907
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->h()I

    move-result v1

    .line 2908
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    .line 2909
    if-eqz v2, :cond_0

    .line 2910
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getRenderer()Lflipboard/gui/flipping/OpenGLTransitionRenderer;

    move-result-object v0

    new-instance v3, Lflipboard/gui/section/SectionFragment$22;

    invoke-direct {v3, p0, v2, v1}, Lflipboard/gui/section/SectionFragment$22;-><init>(Lflipboard/gui/section/SectionFragment;Lflipboard/gui/section/SectionScrubber;I)V

    invoke-virtual {v0, v3}, Lflipboard/gui/flipping/OpenGLTransitionRenderer;->a(Ljava/lang/Runnable;)V

    .line 2931
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1252
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1253
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1255
    iget v1, p0, Lflipboard/gui/section/SectionFragment;->q:I

    .line 1256
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 1257
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    .line 1258
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1259
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    .line 1260
    instance-of v0, v0, Lflipboard/gui/section/Group$AdGroup;

    if-eqz v0, :cond_0

    .line 1261
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 1264
    if-gt v1, v2, :cond_2

    if-lez v2, :cond_2

    .line 1265
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 1268
    :cond_0
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    .line 1270
    goto :goto_0

    .line 1272
    :cond_1
    const-string v0, "current_view_index"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1273
    const-string v0, "grouped_items"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1274
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public onSubsectionClicked(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 3624
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3649
    :goto_0
    return-void

    .line 3628
    :cond_0
    if-nez p1, :cond_1

    .line 3629
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->e()Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v0

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->e()Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v0

    .line 3633
    const v1, 0x7f0a02e0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 3635
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lflipboard/activities/SubsectionActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3636
    sget-object v1, Lflipboard/activities/SubsectionActivity;->n:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3637
    sget-object v1, Lflipboard/activities/SubsectionActivity;->o:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3638
    const/16 v1, 0x4e37

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/section/SectionFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3639
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f040001

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->overridePendingTransition(II)V

    .line 3640
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3641
    if-eqz v0, :cond_2

    const-string v1, "use_white_icons"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3642
    const v0, 0x7f080081

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 3646
    :goto_1
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment;->I:Landroid/view/View;

    .line 3648
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v1, "sectionTitleButtonTapped"

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 3644
    :cond_2
    const v0, 0x7f080080

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1546
    invoke-super {p0, p1, p2}, Lflipboard/activities/FlipboardFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1548
    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragment;->am:Z

    if-eqz v0, :cond_0

    .line 1549
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionScrubber;->bringToFront()V

    .line 1550
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lflipboard/gui/section/SectionAdPage;

    if-eqz v0, :cond_1

    .line 1551
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionScrubber;->setVisibility(I)V

    .line 1565
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->a()V

    .line 1566
    return-void

    .line 1552
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lflipboard/gui/section/SectionPage;

    if-eqz v0, :cond_3

    .line 1553
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/SectionPage;

    .line 1554
    iget-object v0, v0, Lflipboard/gui/section/SectionPage;->u:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 1555
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionScrubber;->setVisibility(I)V

    goto :goto_0

    .line 1557
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v0, v2}, Lflipboard/gui/section/SectionScrubber;->setVisibility(I)V

    goto :goto_0

    .line 1560
    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->s:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v0, v2}, Lflipboard/gui/section/SectionScrubber;->setVisibility(I)V

    goto :goto_0
.end method

.method public final p()Z
    .locals 5

    .prologue
    .line 4552
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    .line 4553
    iget-object v0, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 4554
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v4, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v4}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lflipboard/service/User;->a(Lflipboard/objs/FeedItem;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4555
    const/4 v0, 0x1

    .line 4559
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()V
    .locals 3

    .prologue
    .line 4594
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_1

    .line 4595
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lflipboard/activities/SearchTabletActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 4596
    const-string v1, "extra_origin_section_id"

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4597
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 4605
    :cond_0
    :goto_0
    return-void

    .line 4601
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->F:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    .line 4602
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->F:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->a()V

    goto :goto_0
.end method

.method public showContributors()V
    .locals 3

    .prologue
    .line 4389
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->G:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4390
    new-instance v1, Lflipboard/gui/section/ContributorsDialog;

    invoke-direct {v1}, Lflipboard/gui/section/ContributorsDialog;-><init>()V

    .line 4391
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iput-object v0, v1, Lflipboard/gui/section/ContributorsDialog;->j:Lflipboard/service/Section;

    .line 4392
    new-instance v0, Lflipboard/gui/section/SectionFragment$33;

    invoke-direct {v0, p0}, Lflipboard/gui/section/SectionFragment$33;-><init>(Lflipboard/gui/section/SectionFragment;)V

    iput-object v0, v1, Lflipboard/gui/section/ContributorsDialog;->k:Lflipboard/gui/section/ContributorsDialog$ContributorChangedListener;

    .line 4402
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 4403
    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "edit_contributors"

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/section/ContributorsDialog;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 4405
    :cond_0
    return-void
.end method
