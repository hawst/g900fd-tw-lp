.class public interface abstract Lflipboard/gui/section/Attribution;
.super Ljava/lang/Object;
.source "Attribution.java"


# virtual methods
.method public abstract a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
.end method

.method public abstract a()Z
.end method

.method public abstract b()V
.end method

.method public abstract getMeasuredHeight()I
.end method

.method public abstract getMeasuredWidth()I
.end method

.method public abstract getVisibility()I
.end method

.method public abstract layout(IIII)V
.end method

.method public abstract measure(II)V
.end method

.method public abstract setDrawTopDivider(Z)V
.end method

.method public abstract setId(I)V
.end method

.method public abstract setInverted(Z)V
.end method

.method public abstract setOnClickListener(Landroid/view/View$OnClickListener;)V
.end method

.method public abstract setVisibility(I)V
.end method
