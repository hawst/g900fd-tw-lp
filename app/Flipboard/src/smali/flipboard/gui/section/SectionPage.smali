.class public Lflipboard/gui/section/SectionPage;
.super Landroid/view/ViewGroup;
.source "SectionPage.java"

# interfaces
.implements Lflipboard/gui/FLViewIntf;


# instance fields
.field private A:Z

.field private B:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/Section;",
            "Lflipboard/service/Section$Message;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final C:Z

.field private D:Ljava/util/concurrent/atomic/AtomicInteger;

.field final a:Lflipboard/gui/section/SectionHeaderView;

.field b:Lflipboard/gui/actionbar/FLActionBarMenu;

.field c:Z

.field public final d:Lflipboard/service/Section;

.field final e:Lflipboard/objs/SectionPageTemplate;

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field h:Z

.field i:Z

.field j:Z

.field k:Z

.field l:Z

.field m:Z

.field n:I

.field o:I

.field p:Z

.field q:Z

.field r:Z

.field s:Z

.field public t:Lflipboard/gui/section/SectionScrubber;

.field public u:Landroid/view/ViewGroup;

.field v:Lflipboard/util/Log;

.field public final w:Lflipboard/gui/section/Group;

.field private x:Landroid/graphics/Paint;

.field private y:Z

.field private final z:Lflipboard/service/FlipboardManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lflipboard/gui/section/Group;Lflipboard/service/Section;Z)V
    .locals 8

    .prologue
    const v7, 0x7f0a03ad

    const v6, 0x7f0a03ab

    const/4 v1, 0x1

    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 108
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 92
    const-string v0, "layouts"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/SectionPage;->v:Lflipboard/util/Log;

    .line 97
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/gui/section/SectionPage;->z:Lflipboard/service/FlipboardManager;

    .line 109
    iget-object v0, p2, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    iput-object v0, p0, Lflipboard/gui/section/SectionPage;->e:Lflipboard/objs/SectionPageTemplate;

    .line 110
    iget-object v0, p2, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    iput-object v0, p0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    .line 111
    iput-object p2, p0, Lflipboard/gui/section/SectionPage;->w:Lflipboard/gui/section/Group;

    .line 112
    iput-object p3, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    .line 113
    iput-boolean p4, p0, Lflipboard/gui/section/SectionPage;->C:Z

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    .line 115
    invoke-virtual {p0, v2}, Lflipboard/gui/section/SectionPage;->setWillNotDraw(Z)V

    .line 117
    invoke-virtual {p0}, Lflipboard/gui/section/SectionPage;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f030107

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/SectionHeaderView;

    iput-object v0, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    .line 119
    new-instance v0, Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-virtual {p0}, Lflipboard/gui/section/SectionPage;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lflipboard/gui/actionbar/FLActionBarMenu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    .line 120
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->z:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {}, Lflipboard/service/User;->u()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 121
    invoke-virtual {p0}, Lflipboard/gui/section/SectionPage;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 122
    const v3, 0x7f0f0009

    iget-object v4, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-virtual {v0, v3, v4}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 124
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v3, 0x7f0a03b1

    invoke-virtual {v0, v3}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    invoke-virtual {v0, v5}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 125
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v3, 0x7f0a03aa

    invoke-virtual {v0, v3}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_0

    .line 127
    invoke-virtual {v0, v5}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 128
    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v3, v3, Lflipboard/app/FlipboardApplication;->f:Z

    iput-boolean v3, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    .line 130
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v3, 0x7f0a03b4

    invoke-virtual {v0, v3}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v3

    .line 131
    if-eqz v3, :cond_1

    .line 132
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->a:Lflipboard/service/audio/FLAudioManager;

    if-eqz v0, :cond_4

    sget-object v0, Lflipboard/service/audio/FLAudioManager;->a:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    .line 134
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v3, 0x7f0a03a8

    invoke-virtual {v0, v3}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 135
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v3}, Lflipboard/service/FlipboardManager;->l()Lflipboard/service/FlipboardManager$RootScreenStyle;

    move-result-object v3

    sget-object v4, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v3, v4, :cond_5

    .line 136
    :goto_1
    if-nez p4, :cond_2

    if-eqz v1, :cond_6

    .line 137
    :cond_2
    iput-boolean v2, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    .line 141
    :goto_2
    new-instance v0, Lflipboard/gui/FollowButton;

    invoke-direct {v0, p1}, Lflipboard/gui/FollowButton;-><init>(Landroid/content/Context;)V

    .line 142
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x2

    const/4 v4, -0x1

    invoke-direct {v1, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FollowButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 143
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v3, 0x7f0a03b0

    invoke-virtual {v1, v3}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    iput-object v0, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->m:Landroid/view/View;

    invoke-virtual {v1, v5}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 144
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x7f0a03b2

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    invoke-virtual {v0, v5}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 145
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x7f0a03b2

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    iput-boolean v2, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    .line 146
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-virtual {v0, v6}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    iput-boolean v2, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    .line 147
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-virtual {v0, v7}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    iput-boolean v2, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    .line 148
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x7f0a03ae

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    iput-boolean v2, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    .line 149
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-virtual {v0, v6}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    invoke-virtual {v0, v2}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 150
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x7f0a03b3

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    invoke-virtual {v0, v5}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 151
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x7f0a03ac

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    invoke-virtual {v0, v2}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 152
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-virtual {v0, v7}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    invoke-virtual {v0, v2}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 153
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x7f0a03ae

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    invoke-virtual {v0, v2}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 154
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_3

    .line 155
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Layout: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lflipboard/gui/section/SectionPage;->e:Lflipboard/objs/SectionPageTemplate;

    iget-object v3, v3, Lflipboard/objs/SectionPageTemplate;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(Ljava/lang/CharSequence;)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    .line 156
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x6fca3

    const-string v3, "ContentDrawer search"

    invoke-virtual {v0, v2, v1, v2, v3}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIILjava/lang/CharSequence;)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    .line 159
    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionHeaderView;->getActionBar()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->setMenu(Lflipboard/gui/actionbar/FLActionBarMenu;)V

    .line 160
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionHeaderView;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionPage;->addView(Landroid/view/View;)V

    .line 161
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/SectionPage;->x:Landroid/graphics/Paint;

    .line 162
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->x:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lflipboard/gui/section/SectionPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080052

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 163
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->x:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 164
    invoke-virtual {p0}, Lflipboard/gui/section/SectionPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 165
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v0

    if-eqz v0, :cond_7

    const/high16 v0, 0x3fc00000    # 1.5f

    .line 166
    :goto_3
    iget-object v2, p0, Lflipboard/gui/section/SectionPage;->x:Landroid/graphics/Paint;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v0, v1

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 174
    return-void

    :cond_4
    move v0, v2

    .line 132
    goto/16 :goto_0

    :cond_5
    move v1, v2

    .line 135
    goto/16 :goto_1

    .line 139
    :cond_6
    invoke-virtual {v0, v5}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    goto/16 :goto_2

    .line 165
    :cond_7
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private static a(F)F
    .locals 4

    .prologue
    .line 778
    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-int v0, v0

    .line 779
    int-to-float v1, v0

    mul-float/2addr v1, p0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    .line 780
    int-to-float v0, v0

    div-float v0, v1, v0

    .line 781
    return v0
.end method

.method static synthetic a(Lflipboard/gui/section/SectionPage;)Lflipboard/util/Observer;
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/section/SectionPage;->B:Lflipboard/util/Observer;

    return-object v0
.end method

.method private a(II)V
    .locals 16

    .prologue
    .line 252
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/section/SectionPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    const/4 v2, 0x1

    move v5, v2

    .line 253
    :goto_0
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v2}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 254
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_6

    const/4 v2, 0x1

    .line 255
    :goto_1
    if-eqz v2, :cond_14

    .line 256
    const/4 v2, 0x0

    move v4, v2

    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_14

    .line 257
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 258
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lflipboard/gui/section/SectionPage;->p:Z

    if-nez v3, :cond_0

    const-string v3, "image"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "video"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 259
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionPage;->e:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v2, v5}, Lflipboard/objs/SectionPageTemplate;->a(Z)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/SectionPageTemplate$Area;

    .line 260
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 261
    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v9

    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v6

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v10

    invoke-virtual {v3}, Landroid/view/View;->getPaddingBottom()I

    move-result v8

    .line 262
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/section/SectionPage;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v11, 0x7f09007b

    invoke-virtual {v7, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 263
    invoke-virtual {v2, v5}, Lflipboard/objs/SectionPageTemplate$Area;->d(Z)F

    move-result v11

    const/4 v12, 0x0

    cmpl-float v11, v11, v12

    if-lez v11, :cond_1

    move v6, v7

    .line 266
    :cond_1
    invoke-virtual {v2, v5}, Lflipboard/objs/SectionPageTemplate$Area;->d(Z)F

    move-result v11

    invoke-virtual {v2, v5}, Lflipboard/objs/SectionPageTemplate$Area;->b(Z)F

    move-result v12

    add-float/2addr v11, v12

    float-to-double v12, v11

    const-wide v14, 0x3fefae147ae147aeL    # 0.99

    cmpg-double v11, v12, v14

    if-gez v11, :cond_2

    move v8, v7

    .line 269
    :cond_2
    invoke-virtual {v2, v5}, Lflipboard/objs/SectionPageTemplate$Area;->c(Z)F

    move-result v11

    const/4 v12, 0x0

    cmpl-float v11, v11, v12

    if-lez v11, :cond_3

    move v9, v7

    .line 272
    :cond_3
    invoke-virtual {v2, v5}, Lflipboard/objs/SectionPageTemplate$Area;->c(Z)F

    move-result v11

    invoke-virtual {v2, v5}, Lflipboard/objs/SectionPageTemplate$Area;->a(Z)F

    move-result v2

    add-float/2addr v2, v11

    float-to-double v12, v2

    const-wide v14, 0x3fefae147ae147aeL    # 0.99

    cmpg-double v2, v12, v14

    if-gez v2, :cond_15

    .line 275
    :goto_3
    invoke-virtual {v3, v9, v6, v7, v8}, Landroid/view/View;->setPadding(IIII)V

    .line 256
    :cond_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_2

    .line 252
    :cond_5
    const/4 v2, 0x0

    move v5, v2

    goto/16 :goto_0

    .line 254
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 280
    :cond_7
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lflipboard/gui/section/SectionPage;->n:I

    .line 281
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lflipboard/gui/section/SectionPage;->o:I

    .line 282
    const/4 v2, 0x0

    move v6, v2

    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v6, v2, :cond_12

    .line 283
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 284
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/section/SectionPage;->e:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v3, v5}, Lflipboard/objs/SectionPageTemplate;->a(Z)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflipboard/objs/SectionPageTemplate$Area;

    .line 285
    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lflipboard/objs/FeedItem;

    .line 286
    sget-object v7, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v7}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v7

    if-eqz v7, :cond_a

    iget-object v7, v4, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v7, :cond_8

    iget-object v7, v4, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v8, "image"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 287
    :cond_8
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v2, v3, v4, v7, v8}, Landroid/view/View;->setPadding(IIII)V

    .line 282
    :cond_9
    :goto_5
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_4

    .line 288
    :cond_a
    iget-boolean v7, v3, Lflipboard/objs/SectionPageTemplate$Area;->g:Z

    if-nez v7, :cond_b

    iget-boolean v7, v3, Lflipboard/objs/SectionPageTemplate$Area;->f:Z

    if-eqz v7, :cond_d

    .line 289
    :cond_b
    iget-object v3, v4, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v3, :cond_c

    iget-object v3, v4, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v7, "image"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    iget-object v3, v4, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v4, "video"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 290
    :cond_c
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v2, v3, v4, v7, v8}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_5

    .line 292
    :cond_d
    iget-object v7, v4, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v7, :cond_f

    iget-object v7, v4, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v8, "synthetic-client-profile-page"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_e

    iget-object v4, v4, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v7, "synthetic-client-profile-summary-item"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 293
    :cond_e
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v2, v3, v4, v7, v8}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_5

    .line 296
    :cond_f
    invoke-virtual {v3, v5}, Lflipboard/objs/SectionPageTemplate$Area;->c(Z)F

    move-result v4

    const/4 v7, 0x0

    cmpl-float v4, v4, v7

    if-nez v4, :cond_10

    move/from16 v4, p2

    .line 303
    :goto_6
    invoke-virtual {v3, v5}, Lflipboard/objs/SectionPageTemplate$Area;->c(Z)F

    move-result v7

    invoke-virtual {v3, v5}, Lflipboard/objs/SectionPageTemplate$Area;->a(Z)F

    move-result v3

    add-float/2addr v3, v7

    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v7

    if-nez v3, :cond_11

    move/from16 v3, p2

    .line 308
    :goto_7
    move/from16 v0, p1

    move/from16 v1, p1

    invoke-virtual {v2, v4, v0, v3, v1}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_5

    :cond_10
    move/from16 v4, p1

    .line 299
    goto :goto_6

    :cond_11
    move/from16 v3, p1

    .line 306
    goto :goto_7

    .line 311
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lflipboard/gui/section/SectionPage;->p:Z

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v3, "post"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 312
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->setPadding(IIII)V

    .line 314
    :cond_13
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lflipboard/gui/section/SectionPage;->q:Z

    if-eqz v2, :cond_14

    .line 315
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 316
    const/4 v3, 0x0

    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->setPadding(IIII)V

    .line 319
    :cond_14
    return-void

    :cond_15
    move v7, v10

    goto/16 :goto_3
.end method

.method private a(IILjava/util/List;I)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 655
    if-ge p1, p2, :cond_3

    move v6, v0

    .line 656
    :goto_0
    if-eqz p4, :cond_4

    move v3, v0

    :goto_1
    move v4, v1

    .line 657
    :goto_2
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_5

    .line 658
    invoke-interface {p3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 659
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->e:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v1, v6}, Lflipboard/objs/SectionPageTemplate;->a(Z)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/SectionPageTemplate$Area;

    .line 660
    iget-object v2, p0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/FeedItem;

    .line 662
    if-eqz v3, :cond_0

    invoke-direct {p0, v2, v1, v0}, Lflipboard/gui/section/SectionPage;->a(Lflipboard/objs/FeedItem;Lflipboard/objs/SectionPageTemplate$Area;Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 663
    :cond_0
    invoke-virtual {v1, v6}, Lflipboard/objs/SectionPageTemplate$Area;->a(Z)F

    move-result v2

    int-to-float v5, p1

    mul-float/2addr v2, v5

    float-to-int v7, v2

    .line 666
    invoke-virtual {v1, v6}, Lflipboard/objs/SectionPageTemplate$Area;->b(Z)F

    move-result v2

    int-to-float v5, p2

    mul-float/2addr v2, v5

    float-to-int v2, v2

    .line 668
    iget-boolean v5, p0, Lflipboard/gui/section/SectionPage;->p:Z

    if-eqz v5, :cond_6

    invoke-virtual {v1, v6}, Lflipboard/objs/SectionPageTemplate$Area;->d(Z)F

    move-result v5

    const/4 v8, 0x0

    cmpl-float v5, v5, v8

    if-nez v5, :cond_6

    .line 669
    iget-object v5, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v5}, Lflipboard/gui/section/SectionHeaderView;->getView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v8, 0x8

    if-eq v5, v8, :cond_6

    .line 670
    iget-object v5, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v5}, Lflipboard/gui/section/SectionHeaderView;->getView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v2

    .line 671
    iget-object v2, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v2}, Lflipboard/gui/section/SectionHeaderView;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, p2

    .line 675
    :goto_3
    invoke-virtual {v1, v6}, Lflipboard/objs/SectionPageTemplate$Area;->c(Z)F

    move-result v8

    int-to-float v9, p1

    mul-float/2addr v8, v9

    float-to-int v8, v8

    .line 676
    invoke-virtual {v1, v6}, Lflipboard/objs/SectionPageTemplate$Area;->d(Z)F

    move-result v1

    int-to-float v9, p2

    mul-float/2addr v1, v9

    float-to-int v1, v1

    .line 679
    sub-int v8, p1, v8

    invoke-static {v8, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 680
    sub-int v1, v2, v1

    invoke-static {v1, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 681
    if-eqz v3, :cond_1

    move-object v1, v0

    .line 682
    check-cast v1, Lflipboard/gui/section/item/PostItem;

    invoke-virtual {v1}, Lflipboard/gui/section/item/PostItem;->getItemLayout()Lflipboard/gui/section/item/PostItem$ItemLayout;

    move-result-object v1

    invoke-interface {v1, p4}, Lflipboard/gui/section/item/PostItem$ItemLayout;->a(I)V

    .line 684
    :cond_1
    invoke-static {v7, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v2, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 657
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto/16 :goto_2

    :cond_3
    move v6, v1

    .line 655
    goto/16 :goto_0

    :cond_4
    move v3, v1

    .line 656
    goto/16 :goto_1

    .line 686
    :cond_5
    return-void

    :cond_6
    move v5, v2

    move v2, p2

    goto :goto_3
.end method

.method private a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 969
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 970
    if-eqz v0, :cond_0

    .line 971
    new-instance v3, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->a:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v4, Lflipboard/objs/UsageEventV2$EventCategory;->a:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v3, v1, v4}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 972
    const-string v1, "section_id"

    iget-object v4, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    invoke-virtual {v4}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lflipboard/objs/UsageEventV2;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 973
    const-string v1, "partner_id"

    iget-object v4, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    iget-object v4, v4, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v4, v4, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {v3, v1, v4}, Lflipboard/objs/UsageEventV2;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 974
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->D:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->y()Lflipboard/json/FLObject;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 975
    const-string v1, "item_type"

    iget-object v4, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v3, v1, v4}, Lflipboard/objs/UsageEventV2;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 976
    const-string v1, "item_id"

    iget-object v4, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v3, v1, v4}, Lflipboard/objs/UsageEventV2;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 977
    const-string v1, "url"

    iget-object v4, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-virtual {v3, v1, v4}, Lflipboard/objs/UsageEventV2;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 978
    const-string v1, "item_partner_id"

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->aa()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lflipboard/objs/UsageEventV2;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 981
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v1

    .line 982
    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 983
    if-eq v0, v1, :cond_2

    .line 986
    :goto_1
    new-instance v1, Lflipboard/json/FLObject;

    invoke-direct {v1}, Lflipboard/json/FLObject;-><init>()V

    .line 990
    const-string v4, "id"

    iget-object v5, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 991
    const-string v4, "serviceId"

    iget-object v0, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v1, v4, v0}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 992
    const-string v0, "source"

    invoke-virtual {v3, v0, v1}, Lflipboard/objs/UsageEventV2;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 993
    invoke-virtual {v3}, Lflipboard/objs/UsageEventV2;->a()V

    .line 994
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->D:Ljava/util/concurrent/atomic/AtomicInteger;

    if-eqz v0, :cond_0

    .line 995
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->D:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    goto/16 :goto_0

    .line 999
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/FeedItem;Lflipboard/objs/SectionPageTemplate$Area;Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 632
    if-eqz p1, :cond_1

    instance-of v1, p3, Lflipboard/gui/section/item/PostItem;

    if-eqz v1, :cond_1

    .line 634
    iget v1, p2, Lflipboard/objs/SectionPageTemplate$Area;->c:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_1

    .line 635
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v1, v1, Lflipboard/objs/TOCSection;->E:Z

    if-nez v1, :cond_1

    .line 636
    check-cast p3, Lflipboard/gui/section/item/PostItem;

    .line 637
    invoke-virtual {p3}, Lflipboard/gui/section/item/PostItem;->getItemLayout()Lflipboard/gui/section/item/PostItem$ItemLayout;

    move-result-object v1

    invoke-interface {v1}, Lflipboard/gui/section/item/PostItem$ItemLayout;->e()Lflipboard/gui/section/item/PostItemPhone$Layout;

    move-result-object v1

    .line 638
    if-eqz v1, :cond_1

    sget-object v2, Lflipboard/gui/section/item/PostItemPhone$Layout;->c:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-eq v1, v2, :cond_0

    sget-object v2, Lflipboard/gui/section/item/PostItemPhone$Layout;->b:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v1, v2, :cond_1

    .line 644
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lflipboard/gui/section/SectionPage;)Lflipboard/service/FlipboardManager;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->z:Lflipboard/service/FlipboardManager;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 323
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->e:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v1}, Lflipboard/objs/SectionPageTemplate;->a()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 324
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Too many items added to page, max allowed: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/gui/section/SectionPage;->e:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v4}, Lflipboard/objs/SectionPageTemplate;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 327
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 328
    invoke-virtual {p0, p1}, Lflipboard/gui/section/SectionPage;->addView(Landroid/view/View;)V

    .line 329
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->e:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v1}, Lflipboard/objs/SectionPageTemplate;->a()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 331
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionHeaderView;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionPage;->bringChildToFront(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 185
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 186
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 187
    iget-object v2, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 188
    iget-object v2, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 189
    invoke-virtual {p0, p1}, Lflipboard/gui/section/SectionPage;->removeView(Landroid/view/View;)V

    .line 190
    iget-object v2, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v2, v0, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 191
    invoke-virtual {p0, p2}, Lflipboard/gui/section/SectionPage;->addView(Landroid/view/View;)V

    .line 186
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 195
    :cond_1
    return-void
.end method

.method public final a(Lflipboard/service/Section;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 203
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->z:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {}, Lflipboard/service/User;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    invoke-virtual {p1}, Lflipboard/service/Section;->x()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {p1, v0}, Lflipboard/service/Section;->a(Lflipboard/service/User;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 212
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x7f0a03ab

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    iput-boolean v2, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    .line 213
    invoke-virtual {p1}, Lflipboard/service/Section;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x7f0a03b3

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    iput-boolean v2, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    .line 216
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x7f0a03ad

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    iput-boolean v2, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    .line 217
    new-instance v0, Lflipboard/gui/section/SectionPage$1;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/section/SectionPage$1;-><init>(Lflipboard/gui/section/SectionPage;Lflipboard/service/Section;)V

    invoke-virtual {p1, v0}, Lflipboard/service/Section;->a(Lflipboard/service/Flap$JSONResultObserver;)V

    goto :goto_0

    .line 230
    :cond_3
    invoke-virtual {p1}, Lflipboard/service/Section;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lflipboard/service/Section;->a(Lflipboard/service/Flap$JSONResultObserver;)V

    goto :goto_0
.end method

.method public final a(Lflipboard/service/audio/FLAudioManager$AudioState;Lflipboard/objs/FeedItem;)V
    .locals 4

    .prologue
    .line 869
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 870
    instance-of v2, v0, Lflipboard/gui/section/scrolling/AudioView;

    if-eqz v2, :cond_1

    .line 871
    check-cast v0, Lflipboard/gui/section/scrolling/AudioView;

    iget-object v2, v0, Lflipboard/gui/section/scrolling/AudioView;->k:Lflipboard/service/FlipboardManager;

    new-instance v3, Lflipboard/gui/section/scrolling/AudioView$4;

    invoke-direct {v3, v0, p2, p1}, Lflipboard/gui/section/scrolling/AudioView$4;-><init>(Lflipboard/gui/section/scrolling/AudioView;Lflipboard/objs/FeedItem;Lflipboard/service/audio/FLAudioManager$AudioState;)V

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 872
    :cond_1
    instance-of v2, v0, Lflipboard/gui/item/AudioItemTablet;

    if-eqz v2, :cond_0

    .line 873
    check-cast v0, Lflipboard/gui/item/AudioItemTablet;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/item/AudioItemTablet;->a(Lflipboard/service/audio/FLAudioManager$AudioState;Lflipboard/objs/FeedItem;)V

    goto :goto_0

    .line 876
    :cond_2
    return-void
.end method

.method public final a(ZI)V
    .locals 5

    .prologue
    .line 916
    if-eqz p1, :cond_4

    if-nez p2, :cond_4

    invoke-virtual {p0}, Lflipboard/gui/section/SectionPage;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->w:Lflipboard/gui/section/Group;

    iget-boolean v0, v0, Lflipboard/gui/section/Group;->j:Z

    if-nez v0, :cond_4

    .line 919
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 920
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 921
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 922
    if-eqz v0, :cond_1

    iget-object v3, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v4, "synthetic-client-app-cover"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 925
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 920
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 926
    :cond_1
    if-eqz v0, :cond_2

    iget-object v3, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v4, "list"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 927
    iget-object v3, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 928
    iget-object v0, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 931
    :cond_2
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 935
    :cond_3
    invoke-direct {p0, v2}, Lflipboard/gui/section/SectionPage;->a(Ljava/util/List;)V

    .line 937
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->w:Lflipboard/gui/section/Group;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/gui/section/Group;->j:Z

    .line 940
    :cond_4
    if-nez p2, :cond_6

    if-eqz p1, :cond_6

    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->w:Lflipboard/gui/section/Group;

    iget-boolean v0, v0, Lflipboard/gui/section/Group;->h:Z

    if-eqz v0, :cond_6

    .line 941
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->w:Lflipboard/gui/section/Group;

    iget-object v0, v0, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v1

    if-eqz v1, :cond_6

    new-instance v2, Lflipboard/objs/UsageEventV2;

    sget-object v3, Lflipboard/objs/UsageEventV2$EventAction;->J:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v4, Lflipboard/objs/UsageEventV2$EventCategory;->a:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v2, v3, v4}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v4, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    invoke-virtual {v4}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->s:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v4, v1, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->v:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget v1, v1, Lflipboard/objs/SidebarGroup$RenderHints;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, v0, Lflipboard/objs/SidebarGroup;->j:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    if-eqz v1, :cond_5

    iget-object v0, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->w:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    :cond_5
    invoke-virtual {v2}, Lflipboard/objs/UsageEventV2;->c()V

    .line 943
    :cond_6
    return-void
.end method

.method final a(Z)Z
    .locals 1

    .prologue
    .line 814
    iput-boolean p1, p0, Lflipboard/gui/section/SectionPage;->A:Z

    .line 815
    if-eqz p1, :cond_0

    .line 816
    const v0, 0x7f08009b

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionPage;->setBackgroundResource(I)V

    .line 820
    :goto_0
    return p1

    .line 819
    :cond_0
    const v0, 0x7f080004

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionPage;->setBackgroundResource(I)V

    .line 820
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 912
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 4

    .prologue
    .line 338
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 339
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 340
    iget-object v2, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 341
    const/4 v0, 0x0

    .line 342
    if-eqz v2, :cond_0

    .line 344
    const-string v3, "status"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 345
    const-string v0, "timelineItemView-status"

    .line 354
    :cond_1
    :goto_0
    if-eqz v0, :cond_0

    .line 355
    sget-object v1, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    invoke-virtual {v1, p0, v0}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 363
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x7f0a03b0

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 364
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 365
    sget-object v1, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    const-string v2, "timelineViewAddSectionButton"

    invoke-virtual {v1, v0, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 369
    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x7f0a03aa

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 370
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 371
    sget-object v1, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v2

    const-string v3, "CGSearch"

    invoke-virtual {v1, v2, v3}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 372
    sget-object v1, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    const-string v2, "timelineSearch"

    invoke-virtual {v1, v0, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 376
    :cond_4
    const v0, 0x7f0a02e2

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 377
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    .line 378
    const v0, 0x7f0a02e0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 379
    sget-object v1, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    const-string v2, "timelineSectionTitleButton"

    invoke-virtual {v1, v0, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 383
    :cond_5
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 384
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lflipboard/gui/section/item/PostItem;

    if-eqz v1, :cond_a

    .line 385
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/item/PostItem;

    .line 386
    iget-object v0, v0, Lflipboard/gui/section/item/PostItem;->a:Lflipboard/gui/section/item/PostItem$ItemLayout;

    invoke-interface {v0}, Lflipboard/gui/section/item/PostItem$ItemLayout;->b()V

    .line 390
    :cond_6
    return-void

    .line 346
    :cond_7
    const-string v3, "post"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 347
    const-string v0, "timelineItemView-post"

    goto/16 :goto_0

    .line 348
    :cond_8
    const-string v3, "image"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 349
    const-string v0, "timelineItemView-image"

    goto/16 :goto_0

    .line 350
    :cond_9
    const-string v3, "album"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 351
    const-string v0, "timelineItemView-album"

    goto/16 :goto_0

    .line 383
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->z:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {}, Lflipboard/service/User;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x7f0a03b2

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    .line 181
    :cond_0
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 15

    .prologue
    .line 732
    invoke-super/range {p0 .. p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 733
    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->l:Z

    if-nez v0, :cond_5

    .line 734
    invoke-virtual {p0}, Lflipboard/gui/section/SectionPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    move v6, v0

    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/section/SectionPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    const/4 v0, 0x0

    move v7, v0

    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_5

    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->e:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v0, v6}, Lflipboard/objs/SectionPageTemplate;->a(Z)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SectionPageTemplate$Area;

    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v0, v6}, Lflipboard/objs/SectionPageTemplate$Area;->d(Z)F

    move-result v2

    invoke-virtual {v0, v6}, Lflipboard/objs/SectionPageTemplate$Area;->c(Z)F

    move-result v9

    invoke-virtual {v0, v6}, Lflipboard/objs/SectionPageTemplate$Area;->a(Z)F

    move-result v3

    add-float/2addr v3, v9

    invoke-static {v3}, Lflipboard/gui/section/SectionPage;->a(F)F

    move-result v10

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v11

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v12

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v0, v6}, Lflipboard/objs/SectionPageTemplate$Area;->b(Z)F

    move-result v0

    add-float/2addr v0, v2

    invoke-static {v0}, Lflipboard/gui/section/SectionPage;->a(F)F

    move-result v13

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v14

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, v10, v0

    if-gez v0, :cond_0

    int-to-float v1, v12

    int-to-float v2, v3

    int-to-float v3, v12

    int-to-float v4, v14

    iget-object v5, p0, Lflipboard/gui/section/SectionPage;->x:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, v13, v0

    if-gez v0, :cond_1

    const/4 v0, 0x0

    cmpl-float v0, v9, v0

    if-lez v0, :cond_3

    const/4 v0, 0x0

    :goto_2
    add-int/2addr v0, v11

    int-to-float v1, v0

    int-to-float v2, v14

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, v10, v0

    if-gez v0, :cond_4

    const/4 v0, 0x0

    :goto_3
    sub-int v0, v12, v0

    int-to-float v3, v0

    int-to-float v4, v14

    iget-object v5, p0, Lflipboard/gui/section/SectionPage;->x:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    move v6, v0

    goto/16 :goto_0

    :cond_3
    move v0, v8

    goto :goto_2

    :cond_4
    move v0, v8

    goto :goto_3

    .line 739
    :cond_5
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 237
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionHeaderView;->getActionBar()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->setCenterViewHidden(Z)V

    .line 238
    iput-boolean v1, p0, Lflipboard/gui/section/SectionPage;->y:Z

    .line 239
    return-void
.end method

.method final f()V
    .locals 12

    .prologue
    const v11, 0x7f0900df

    const/4 v5, 0x0

    const/16 v10, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 394
    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->A:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->p:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->m:Z

    if-nez v0, :cond_2

    :cond_0
    move v0, v3

    :goto_0
    iput-boolean v0, p0, Lflipboard/gui/section/SectionPage;->c:Z

    .line 397
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    if-nez v0, :cond_3

    .line 564
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 394
    goto :goto_0

    .line 401
    :cond_3
    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->y:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->p:Z

    if-nez v0, :cond_9

    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->i:Z

    if-eqz v0, :cond_9

    :cond_4
    move v1, v3

    .line 403
    :goto_2
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->w:Lflipboard/gui/section/Group;

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->w:Lflipboard/gui/section/Group;

    iget-object v0, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    if-eqz v0, :cond_2a

    .line 404
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->w:Lflipboard/gui/section/Group;

    iget-object v0, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v0, v0, Lflipboard/gui/section/GroupFranchiseMeta;->b:Ljava/lang/String;

    if-eqz v0, :cond_2a

    .line 405
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->w:Lflipboard/gui/section/Group;

    iget-object v0, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v0, v0, Lflipboard/gui/section/GroupFranchiseMeta;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 408
    :goto_3
    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->h:Z

    if-eqz v0, :cond_a

    .line 409
    iget-object v4, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    iget-object v6, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    invoke-virtual {v4}, Lflipboard/gui/section/SectionHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v7, 0x7f03010a

    invoke-virtual {v0, v7, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/header/TopicHeaderView;

    invoke-virtual {v0, v2}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->setShowActionBar(Z)V

    invoke-virtual {v0, v6}, Lflipboard/gui/section/scrolling/header/TopicHeaderView;->setItem(Lflipboard/service/Section;)V

    invoke-virtual {v4, v0}, Lflipboard/gui/section/SectionHeaderView;->addView(Landroid/view/View;)V

    .line 410
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionHeaderView;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lflipboard/gui/section/SectionPage;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080004

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundColor(I)V

    move v4, v1

    .line 421
    :goto_4
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionHeaderView;->getActionBar()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    iget-boolean v1, p0, Lflipboard/gui/section/SectionPage;->c:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->setInverted(Z)V

    .line 423
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x7f0a03b0

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 424
    if-eqz v0, :cond_5

    .line 425
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->q()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 426
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FollowButton;

    .line 427
    new-instance v1, Lflipboard/objs/FeedSectionLink;

    iget-object v5, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    iget-object v6, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    iget-object v6, v6, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v6, v6, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    invoke-direct {v1, v5, v6}, Lflipboard/objs/FeedSectionLink;-><init>(Lflipboard/service/Section;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FollowButton;->setSectionLink(Lflipboard/objs/FeedSectionLink;)V

    .line 428
    iget-boolean v1, p0, Lflipboard/gui/section/SectionPage;->c:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/FollowButton;->setInverted(Z)V

    .line 434
    :cond_5
    :goto_5
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x7f0a03b2

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    .line 435
    if-eqz v1, :cond_6

    .line 436
    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->c:Z

    if-eqz v0, :cond_11

    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    :goto_6
    invoke-virtual {v1, v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    .line 438
    :cond_6
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x7f0a03b3

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    .line 439
    if-eqz v1, :cond_7

    .line 440
    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->c:Z

    if-eqz v0, :cond_12

    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    :goto_7
    invoke-virtual {v1, v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    .line 444
    :cond_7
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "settings_topic_feed"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 445
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->l()Lflipboard/service/FlipboardManager$RootScreenStyle;

    move-result-object v1

    .line 446
    sget-object v5, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v1, v5, :cond_13

    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->t()Z

    move-result v1

    if-nez v1, :cond_8

    if-eqz v0, :cond_13

    .line 447
    :cond_8
    invoke-direct {p0, v2, v2}, Lflipboard/gui/section/SectionPage;->a(II)V

    .line 448
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->x:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->reset()V

    .line 449
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionHeaderView;->getActionBar()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    invoke-virtual {v0, v10}, Lflipboard/gui/actionbar/FLActionBar;->setVisibility(I)V

    goto/16 :goto_1

    :cond_9
    move v1, v2

    .line 401
    goto/16 :goto_2

    .line 411
    :cond_a
    invoke-static {v4}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 412
    iget-object v6, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->w:Lflipboard/gui/section/Group;

    iget-object v0, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v7, v0, Lflipboard/gui/section/GroupFranchiseMeta;->b:Ljava/lang/String;

    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->w:Lflipboard/gui/section/Group;

    iget-object v0, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v8, v0, Lflipboard/gui/section/GroupFranchiseMeta;->c:Ljava/lang/String;

    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->w:Lflipboard/gui/section/Group;

    iget-object v0, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v0, v0, Lflipboard/gui/section/GroupFranchiseMeta;->a:Lflipboard/objs/FeedItem;

    iget-object v1, v0, Lflipboard/objs/FeedItem;->cp:Lflipboard/objs/FeedItemRenderHints;

    const/high16 v0, -0x1000000

    if-eqz v1, :cond_b

    iget-object v5, v1, Lflipboard/objs/FeedItemRenderHints;->b:Lflipboard/objs/Image;

    iget-object v0, v1, Lflipboard/objs/FeedItemRenderHints;->a:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/ColorFilterUtil;->a(Ljava/lang/String;)I

    move-result v0

    :cond_b
    invoke-virtual {v6, v0}, Lflipboard/gui/section/SectionHeaderView;->setBackgroundColor(I)V

    invoke-virtual {v6}, Lflipboard/gui/section/SectionHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030002

    invoke-virtual {v0, v1, v6, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    const v0, 0x7f0a0042

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    const v1, 0x7f0a0043

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    if-eqz v5, :cond_c

    invoke-virtual {v1, v5}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    invoke-virtual {v0, v10}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    :goto_8
    const v0, 0x7f0a0044

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v8, :cond_d

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    :goto_9
    invoke-virtual {v6, v9}, Lflipboard/gui/section/SectionHeaderView;->addView(Landroid/view/View;)V

    iget-object v0, v6, Lflipboard/gui/section/SectionHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0, v3}, Lflipboard/gui/actionbar/FLActionBar;->setInverted(Z)V

    .line 415
    iput-boolean v3, p0, Lflipboard/gui/section/SectionPage;->c:Z

    move v4, v2

    goto/16 :goto_4

    .line 412
    :cond_c
    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    invoke-virtual {v0, v4}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v10}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto :goto_8

    :cond_d
    new-instance v1, Lflipboard/gui/section/SectionHeaderView$1;

    invoke-direct {v1, v6, v8, v7}, Lflipboard/gui/section/SectionHeaderView$1;-><init>(Lflipboard/gui/section/SectionHeaderView;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_9

    .line 417
    :cond_e
    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->c:Z

    if-eqz v0, :cond_f

    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->m:Z

    if-nez v0, :cond_f

    .line 418
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionHeaderView;->getView()Landroid/view/View;

    move-result-object v0

    const v4, 0x7f02022d

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_f
    move v4, v1

    goto/16 :goto_4

    .line 430
    :cond_10
    iput-boolean v2, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    .line 431
    iget-object v1, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->m:Landroid/view/View;

    if-eqz v1, :cond_5

    iget-object v0, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->m:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5

    .line 436
    :cond_11
    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    goto/16 :goto_6

    .line 440
    :cond_12
    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    goto/16 :goto_7

    .line 456
    :cond_13
    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->i:Z

    if-nez v0, :cond_14

    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->j:Z

    if-nez v0, :cond_14

    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->k:Z

    if-eqz v0, :cond_1b

    .line 457
    :cond_14
    invoke-virtual {p0}, Lflipboard/gui/section/SectionPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/section/SectionPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lflipboard/gui/section/SectionPage;->a(II)V

    .line 461
    :goto_a
    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->c:Z

    if-eqz v0, :cond_1c

    .line 462
    invoke-virtual {p0}, Lflipboard/gui/section/SectionPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080089

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v1, v0

    .line 467
    :goto_b
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->w:Lflipboard/gui/section/Group;

    iget-boolean v0, v0, Lflipboard/gui/section/Group;->f:Z

    if-eqz v0, :cond_15

    .line 468
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionHeaderView;->getActionBar()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Lflipboard/gui/actionbar/FLActionBar;->setCenterViewHidden(Z)V

    .line 472
    :cond_15
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v5, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    iget-object v5, v5, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v5, v5, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v6

    .line 473
    const v0, 0x7f0a016c

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 474
    iget-object v5, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    iget-boolean v7, p0, Lflipboard/gui/section/SectionPage;->c:Z

    iget-object v8, v5, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    if-eqz v8, :cond_29

    iget-object v8, v5, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v8, v8, Lflipboard/objs/TOCSection;->D:Z

    if-eqz v8, :cond_29

    iget-object v8, v5, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v8, v8, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    if-eqz v8, :cond_1e

    iget-object v8, v5, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v8, v8, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    const-string v9, "nytimes"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1e

    if-eqz v7, :cond_1d

    const v5, 0x7f02019e

    .line 475
    :goto_c
    iget-boolean v6, v6, Lflipboard/objs/ConfigService;->bO:Z

    if-eqz v6, :cond_22

    if-eqz v5, :cond_22

    .line 476
    const v1, 0x7f0a02e1

    invoke-virtual {p0, v1}, Lflipboard/gui/section/SectionPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 477
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 478
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 479
    invoke-interface {v0, v10}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 514
    :goto_d
    const v0, 0x7f0a02e2

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 515
    invoke-virtual {p0}, Lflipboard/gui/section/SectionPage;->i()V

    .line 516
    iget-boolean v1, p0, Lflipboard/gui/section/SectionPage;->c:Z

    if-eqz v1, :cond_24

    const v1, 0x7f0200df

    :goto_e
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 517
    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->c:Z

    if-eqz v0, :cond_16

    .line 518
    const v0, 0x7f0a02e0

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 519
    const-string v1, "use_white_icons"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 522
    :cond_16
    if-nez v4, :cond_17

    .line 523
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionHeaderView;->getActionBar()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->e()V

    .line 525
    :cond_17
    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->C:Z

    if-nez v0, :cond_18

    .line 526
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionHeaderView;->getActionBar()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    iget-boolean v1, p0, Lflipboard/gui/section/SectionPage;->s:Z

    invoke-virtual {v0, v3, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    .line 528
    :cond_18
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionHeaderView;->getActionBar()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->g()V

    .line 530
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->z:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {}, Lflipboard/service/User;->u()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 533
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x7f0a03b1

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    .line 534
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->f:Ljava/lang/String;

    if-eqz v0, :cond_26

    .line 535
    iput-boolean v3, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    .line 536
    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->c:Z

    if-eqz v0, :cond_25

    const v0, 0x7f020202

    .line 537
    :goto_f
    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 543
    :goto_10
    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->c:Z

    if-eqz v0, :cond_27

    const v0, 0x7f020188

    .line 544
    :goto_11
    iget-boolean v1, p0, Lflipboard/gui/section/SectionPage;->C:Z

    if-nez v1, :cond_19

    .line 545
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v4, 0x7f0a03a8

    invoke-virtual {v1, v4}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    .line 546
    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 549
    :cond_19
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x7f0a03aa

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    .line 551
    if-eqz v1, :cond_1a

    .line 552
    iget-boolean v0, p0, Lflipboard/gui/section/SectionPage;->c:Z

    if-eqz v0, :cond_28

    const v0, 0x7f0201ee

    .line 553
    :goto_12
    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 557
    :cond_1a
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x85ba

    sget-object v4, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v5, 0x7f0d0250

    invoke-virtual {v4, v5}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v1, v2, v4}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIILjava/lang/CharSequence;)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 558
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->a()Ljava/lang/String;

    move-result-object v1

    .line 559
    invoke-static {v1, v3}, Lflipboard/activities/SettingsDensityActivity;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->e:Ljava/lang/CharSequence;

    .line 561
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_1

    .line 562
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->b:Lflipboard/gui/actionbar/FLActionBarMenu;

    const v1, 0x85bb

    const-string v3, "Fake refresh items"

    invoke-virtual {v0, v2, v1, v2, v3}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIILjava/lang/CharSequence;)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    goto/16 :goto_1

    .line 459
    :cond_1b
    invoke-virtual {p0}, Lflipboard/gui/section/SectionPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/section/SectionPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0900e0

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lflipboard/gui/section/SectionPage;->a(II)V

    goto/16 :goto_a

    .line 464
    :cond_1c
    invoke-virtual {p0}, Lflipboard/gui/section/SectionPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080083

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v1, v0

    goto/16 :goto_b

    .line 474
    :cond_1d
    const v5, 0x7f02019d

    goto/16 :goto_c

    :cond_1e
    iget-object v8, v5, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v8, v8, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    if-eqz v8, :cond_20

    iget-object v8, v5, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v8, v8, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    const-string v9, "ft"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_20

    if-eqz v7, :cond_1f

    const v5, 0x7f02010b

    goto/16 :goto_c

    :cond_1f
    const v5, 0x7f02010a

    goto/16 :goto_c

    :cond_20
    const-string v8, "cnn"

    iget-object v5, v5, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v5, v5, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_29

    if-eqz v7, :cond_21

    const v5, 0x7f02007e

    goto/16 :goto_c

    :cond_21
    const v5, 0x7f02007d

    goto/16 :goto_c

    .line 481
    :cond_22
    iget-object v5, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    invoke-virtual {v5}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 484
    iget-object v5, p0, Lflipboard/gui/section/SectionPage;->B:Lflipboard/util/Observer;

    if-nez v5, :cond_23

    .line 485
    new-instance v5, Lflipboard/gui/section/SectionPage$2;

    invoke-direct {v5, p0, v0}, Lflipboard/gui/section/SectionPage$2;-><init>(Lflipboard/gui/section/SectionPage;Lflipboard/gui/FLTextIntf;)V

    iput-object v5, p0, Lflipboard/gui/section/SectionPage;->B:Lflipboard/util/Observer;

    .line 509
    :cond_23
    iget-object v5, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    iget-object v6, p0, Lflipboard/gui/section/SectionPage;->B:Lflipboard/util/Observer;

    invoke-virtual {v5, v6}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    .line 511
    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setTextColor(I)V

    goto/16 :goto_d

    .line 516
    :cond_24
    const v1, 0x7f0200de

    goto/16 :goto_e

    .line 536
    :cond_25
    const v0, 0x7f020201

    goto/16 :goto_f

    .line 539
    :cond_26
    iput-boolean v2, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    goto/16 :goto_10

    .line 543
    :cond_27
    const v0, 0x7f020187

    goto/16 :goto_11

    .line 552
    :cond_28
    const v0, 0x7f0201ed

    goto/16 :goto_12

    :cond_29
    move v5, v2

    goto/16 :goto_c

    :cond_2a
    move-object v4, v5

    goto/16 :goto_3
.end method

.method final g()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 785
    invoke-virtual {p0}, Lflipboard/gui/section/SectionPage;->e()V

    .line 786
    iput-boolean v0, p0, Lflipboard/gui/section/SectionPage;->m:Z

    .line 787
    iput-boolean v0, p0, Lflipboard/gui/section/SectionPage;->h:Z

    .line 788
    return-void
.end method

.method public final h()V
    .locals 4

    .prologue
    .line 830
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    if-nez v0, :cond_0

    .line 831
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Section must be set before applying items"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 833
    :cond_0
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 834
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 835
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 836
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 837
    instance-of v3, v1, Lflipboard/gui/item/TabletItem;

    if-eqz v3, :cond_1

    .line 838
    check-cast v1, Lflipboard/gui/item/TabletItem;

    .line 839
    iget-object v3, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    invoke-interface {v1, v3, v0}, Lflipboard/gui/item/TabletItem;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 840
    iget-object v0, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v3, "pagebox"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 841
    check-cast v1, Lflipboard/gui/item/PageboxView;

    .line 842
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->w:Lflipboard/gui/section/Group;

    iget-object v0, v0, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    invoke-interface {v1, v0}, Lflipboard/gui/item/PageboxView;->setPagebox(Lflipboard/objs/SidebarGroup;)V

    .line 833
    :cond_1
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 846
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SectionPage: number of subViews mismatch with number of items"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 847
    new-instance v1, Lflipboard/gui/section/SectionPage$3;

    invoke-direct {v1, p0}, Lflipboard/gui/section/SectionPage$3;-><init>(Lflipboard/gui/section/SectionPage;)V

    invoke-static {v0, v1}, Lnet/hockeyapp/android/ExceptionHandler;->a(Ljava/lang/Throwable;Lnet/hockeyapp/android/CrashManagerListener;)V

    goto :goto_1

    .line 860
    :cond_3
    sget-object v1, Lflipboard/service/audio/FLAudioManager;->a:Lflipboard/service/audio/FLAudioManager;

    .line 861
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lflipboard/service/audio/FLAudioManager;->d()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v1}, Lflipboard/service/audio/FLAudioManager;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 862
    :cond_4
    invoke-virtual {v1}, Lflipboard/service/audio/FLAudioManager;->d()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->a:Lflipboard/service/audio/FLAudioManager$AudioState;

    .line 863
    :goto_2
    invoke-virtual {v1}, Lflipboard/service/audio/FLAudioManager;->f()Lflipboard/objs/FeedItem;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/section/SectionPage;->a(Lflipboard/service/audio/FLAudioManager$AudioState;Lflipboard/objs/FeedItem;)V

    .line 865
    :cond_5
    return-void

    .line 862
    :cond_6
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->c:Lflipboard/service/audio/FLAudioManager$AudioState;

    goto :goto_2
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 900
    const v0, 0x7f0a02e2

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 901
    if-eqz v0, :cond_0

    .line 902
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 903
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 908
    :cond_0
    :goto_0
    return-void

    .line 905
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 1003
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 1004
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->B:Lflipboard/util/Observer;

    if-eqz v0, :cond_0

    .line 1005
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->d:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->B:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    .line 1006
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/section/SectionPage;->B:Lflipboard/util/Observer;

    .line 1008
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    .line 691
    sub-int v6, p4, p2

    .line 692
    sub-int v0, p5, p3

    .line 693
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->t:Lflipboard/gui/section/SectionScrubber;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->u:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    .line 694
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->t:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v1}, Lflipboard/gui/section/SectionScrubber;->getMeasuredChildHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 697
    :cond_0
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->u:Landroid/view/ViewGroup;

    if-eqz v1, :cond_6

    .line 698
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->u:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/gui/section/SectionPage;->u:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v0, v3

    invoke-virtual {v1, v2, v3, v6, v0}, Landroid/view/ViewGroup;->layout(IIII)V

    .line 699
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->u:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    move v2, v0

    .line 702
    :goto_0
    const/4 v0, 0x0

    .line 704
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v1}, Lflipboard/gui/section/SectionHeaderView;->getVisibility()I

    move-result v1

    const/16 v3, 0x8

    if-eq v1, v3, :cond_5

    .line 705
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionHeaderView;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    move v5, v0

    .line 709
    :goto_1
    if-ge v6, v2, :cond_1

    const/4 v0, 0x1

    move v3, v0

    .line 710
    :goto_2
    const/4 v0, 0x0

    move v4, v0

    :goto_3
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_3

    .line 711
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 712
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->e:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v1, v3}, Lflipboard/objs/SectionPageTemplate;->a(Z)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/SectionPageTemplate$Area;

    .line 715
    invoke-virtual {v1, v3}, Lflipboard/objs/SectionPageTemplate$Area;->c(Z)F

    move-result v7

    int-to-float v8, v6

    mul-float/2addr v7, v8

    float-to-int v7, v7

    .line 716
    iget-boolean v8, p0, Lflipboard/gui/section/SectionPage;->p:Z

    if-eqz v8, :cond_2

    invoke-virtual {v1, v3}, Lflipboard/objs/SectionPageTemplate$Area;->d(Z)F

    move-result v8

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-nez v8, :cond_2

    .line 717
    const/4 v1, 0x0

    .line 721
    :goto_4
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v7

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v1

    invoke-virtual {v0, v7, v1, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 710
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    .line 709
    :cond_1
    const/4 v0, 0x0

    move v3, v0

    goto :goto_2

    .line 719
    :cond_2
    invoke-virtual {v1, v3}, Lflipboard/objs/SectionPageTemplate$Area;->d(Z)F

    move-result v1

    sub-int v8, v2, v5

    int-to-float v8, v8

    mul-float/2addr v1, v8

    float-to-int v1, v1

    add-int/2addr v1, v5

    goto :goto_4

    .line 724
    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionHeaderView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    .line 725
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionHeaderView;->getView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v3}, Lflipboard/gui/section/SectionHeaderView;->getView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v0, v1, v2, v6, v3}, Landroid/view/View;->layout(IIII)V

    .line 727
    :cond_4
    return-void

    :cond_5
    move v5, v0

    goto :goto_1

    :cond_6
    move v2, v0

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 577
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    .line 578
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 579
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->t:Lflipboard/gui/section/SectionScrubber;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->u:Landroid/view/ViewGroup;

    if-nez v1, :cond_1

    .line 581
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->t:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v1}, Lflipboard/gui/section/SectionScrubber;->getMeasuredHeight()I

    move-result v1

    if-nez v1, :cond_0

    .line 582
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->t:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v1, p1, p2}, Lflipboard/gui/section/SectionScrubber;->measure(II)V

    .line 584
    :cond_0
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->t:Lflipboard/gui/section/SectionScrubber;

    invoke-virtual {v1}, Lflipboard/gui/section/SectionScrubber;->getMeasuredChildHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 587
    :cond_1
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->u:Landroid/view/ViewGroup;

    if-eqz v1, :cond_2

    .line 588
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->u:Landroid/view/ViewGroup;

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, p1, v2}, Landroid/view/ViewGroup;->measure(II)V

    .line 589
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->u:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 593
    :cond_2
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v1}, Lflipboard/gui/section/SectionHeaderView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_7

    .line 594
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v1}, Lflipboard/gui/section/SectionHeaderView;->getView()Landroid/view/View;

    move-result-object v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v8, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v1, v2, v4}, Landroid/view/View;->measure(II)V

    .line 596
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v1}, Lflipboard/gui/section/SectionHeaderView;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 598
    :goto_0
    sub-int v9, v0, v1

    .line 600
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-direct {p0, v8, v9, v0, v3}, Lflipboard/gui/section/SectionPage;->a(IILjava/util/List;I)V

    move v4, v3

    move v5, v3

    move v6, v3

    .line 603
    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_4

    .line 604
    if-ge v8, v9, :cond_3

    move v1, v7

    .line 605
    :goto_2
    iget-object v0, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 606
    iget-object v2, p0, Lflipboard/gui/section/SectionPage;->e:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v2, v1}, Lflipboard/objs/SectionPageTemplate;->a(Z)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/SectionPageTemplate$Area;

    .line 607
    iget-object v2, p0, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/FeedItem;

    .line 608
    invoke-direct {p0, v2, v1, v0}, Lflipboard/gui/section/SectionPage;->a(Lflipboard/objs/FeedItem;Lflipboard/objs/SectionPageTemplate$Area;Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 609
    check-cast v0, Lflipboard/gui/section/item/PostItem;

    invoke-virtual {v0}, Lflipboard/gui/section/item/PostItem;->getItemLayout()Lflipboard/gui/section/item/PostItem$ItemLayout;

    move-result-object v0

    invoke-interface {v0}, Lflipboard/gui/section/item/PostItem$ItemLayout;->d()I

    move-result v0

    add-int v2, v6, v0

    .line 610
    add-int/lit8 v1, v5, 0x1

    .line 603
    :goto_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v5, v1

    move v6, v2

    goto :goto_1

    :cond_3
    move v1, v3

    .line 604
    goto :goto_2

    .line 615
    :cond_4
    if-le v5, v7, :cond_5

    .line 616
    div-int v0, v6, v5

    .line 617
    iget-object v1, p0, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-direct {p0, v8, v9, v1, v0}, Lflipboard/gui/section/SectionPage;->a(IILjava/util/List;I)V

    .line 619
    :cond_5
    invoke-virtual {p0, v8, v9}, Lflipboard/gui/section/SectionPage;->setMeasuredDimension(II)V

    .line 620
    return-void

    :cond_6
    move v1, v5

    move v2, v6

    goto :goto_3

    :cond_7
    move v1, v3

    goto :goto_0
.end method

.method setIsAudioPage(Z)V
    .locals 0

    .prologue
    .line 802
    iput-boolean p1, p0, Lflipboard/gui/section/SectionPage;->k:Z

    .line 803
    return-void
.end method

.method setIsImagePage(Z)V
    .locals 0

    .prologue
    .line 792
    iput-boolean p1, p0, Lflipboard/gui/section/SectionPage;->i:Z

    .line 793
    return-void
.end method

.method setIsOpenedFromThirdParty(Z)V
    .locals 0

    .prologue
    .line 825
    iput-boolean p1, p0, Lflipboard/gui/section/SectionPage;->s:Z

    .line 826
    return-void
.end method

.method setIsSectionTilePage(Z)V
    .locals 0

    .prologue
    .line 807
    iput-boolean p1, p0, Lflipboard/gui/section/SectionPage;->l:Z

    .line 808
    return-void
.end method

.method setIsVideoPage(Z)V
    .locals 0

    .prologue
    .line 797
    iput-boolean p1, p0, Lflipboard/gui/section/SectionPage;->j:Z

    .line 798
    return-void
.end method

.method public setItemDisplayedCounter(Ljava/util/concurrent/atomic/AtomicInteger;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lflipboard/gui/section/SectionPage;->D:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 243
    return-void
.end method

.method public setScrubber(Lflipboard/gui/section/SectionScrubber;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lflipboard/gui/section/SectionPage;->t:Lflipboard/gui/section/SectionScrubber;

    .line 248
    return-void
.end method
