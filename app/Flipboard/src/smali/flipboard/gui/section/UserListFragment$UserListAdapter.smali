.class Lflipboard/gui/section/UserListFragment$UserListAdapter;
.super Landroid/widget/BaseAdapter;
.source "UserListFragment.java"

# interfaces
.implements Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;


# instance fields
.field final synthetic a:Lflipboard/gui/section/UserListFragment;


# direct methods
.method private constructor <init>(Lflipboard/gui/section/UserListFragment;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a:Lflipboard/gui/section/UserListFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflipboard/gui/section/UserListFragment;B)V
    .locals 0

    .prologue
    .line 249
    invoke-direct {p0, p1}, Lflipboard/gui/section/UserListFragment$UserListAdapter;-><init>(Lflipboard/gui/section/UserListFragment;)V

    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/UserListFragment$UserListAdapter;ZI)V
    .locals 3

    .prologue
    .line 249
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->I:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->j:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->o:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a:Lflipboard/gui/section/UserListFragment;

    invoke-static {v2}, Lflipboard/gui/section/UserListFragment;->b(Lflipboard/gui/section/UserListFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    if-eqz p1, :cond_0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->B:Lflipboard/objs/UsageEventV2$CommonEventData;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->F:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    :goto_0
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    return-void

    :cond_0
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->B:Lflipboard/objs/UsageEventV2$CommonEventData;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 347
    if-nez p2, :cond_1

    .line 348
    new-instance v1, Lflipboard/gui/section/UserListFragment$HeaderHolder;

    invoke-direct {v1, v4}, Lflipboard/gui/section/UserListFragment$HeaderHolder;-><init>(B)V

    .line 349
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f03014f

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 350
    const v0, 0x7f0a016c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, v1, Lflipboard/gui/section/UserListFragment$HeaderHolder;->a:Lflipboard/gui/FLTextIntf;

    .line 351
    const v0, 0x7f0a038a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lflipboard/gui/section/UserListFragment$HeaderHolder;->b:Landroid/widget/TextView;

    .line 352
    iget-object v0, v1, Lflipboard/gui/section/UserListFragment$HeaderHolder;->b:Landroid/widget/TextView;

    new-instance v2, Lflipboard/gui/section/UserListFragment$UserListAdapter$2;

    invoke-direct {v2, p0, p1}, Lflipboard/gui/section/UserListFragment$UserListAdapter$2;-><init>(Lflipboard/gui/section/UserListFragment$UserListAdapter;I)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 403
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 409
    :goto_0
    iget-object v1, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a:Lflipboard/gui/section/UserListFragment;

    invoke-static {v1}, Lflipboard/gui/section/UserListFragment;->d(Lflipboard/gui/section/UserListFragment;)Lflipboard/service/Flap$FollowListType;

    move-result-object v1

    sget-object v2, Lflipboard/service/Flap$FollowListType;->c:Lflipboard/service/Flap$FollowListType;

    if-ne v1, v2, :cond_4

    .line 410
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a:Lflipboard/gui/section/UserListFragment;

    invoke-static {v2}, Lflipboard/gui/section/UserListFragment;->b(Lflipboard/gui/section/UserListFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    .line 411
    const-string v2, "flipboard"

    iget-object v3, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a:Lflipboard/gui/section/UserListFragment;

    invoke-static {v3}, Lflipboard/gui/section/UserListFragment;->b(Lflipboard/gui/section/UserListFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 413
    invoke-virtual {p0, p1}, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a(I)Lflipboard/objs/FeedSectionLink;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, p1}, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a(I)Lflipboard/objs/FeedSectionLink;

    move-result-object v1

    iget-object v1, v1, Lflipboard/objs/FeedSectionLink;->p:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 414
    iget-object v0, v0, Lflipboard/gui/section/UserListFragment$HeaderHolder;->a:Lflipboard/gui/FLTextIntf;

    invoke-virtual {p0, p1}, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a(I)Lflipboard/objs/FeedSectionLink;

    move-result-object v1

    iget-object v1, v1, Lflipboard/objs/FeedSectionLink;->p:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 432
    :cond_0
    :goto_1
    return-object p2

    .line 405
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/UserListFragment$HeaderHolder;

    goto :goto_0

    .line 417
    :cond_2
    iget-object v0, v0, Lflipboard/gui/section/UserListFragment$HeaderHolder;->a:Lflipboard/gui/FLTextIntf;

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v2, 0x7f0d00ed

    invoke-virtual {v1, v2}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 420
    :cond_3
    iget-object v0, v0, Lflipboard/gui/section/UserListFragment$HeaderHolder;->a:Lflipboard/gui/FLTextIntf;

    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v3, 0x7f0d00ec

    invoke-virtual {v2, v3}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1}, Lflipboard/objs/ConfigService;->d()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 422
    :cond_4
    iget-object v1, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a:Lflipboard/gui/section/UserListFragment;

    invoke-static {v1}, Lflipboard/gui/section/UserListFragment;->d(Lflipboard/gui/section/UserListFragment;)Lflipboard/service/Flap$FollowListType;

    move-result-object v1

    sget-object v2, Lflipboard/service/Flap$FollowListType;->d:Lflipboard/service/Flap$FollowListType;

    if-ne v1, v2, :cond_5

    .line 423
    iget-object v0, v0, Lflipboard/gui/section/UserListFragment$HeaderHolder;->a:Lflipboard/gui/FLTextIntf;

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v2, 0x7f0d00e9

    invoke-virtual {v1, v2}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 424
    :cond_5
    iget-object v1, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a:Lflipboard/gui/section/UserListFragment;

    invoke-static {v1}, Lflipboard/gui/section/UserListFragment;->d(Lflipboard/gui/section/UserListFragment;)Lflipboard/service/Flap$FollowListType;

    move-result-object v1

    sget-object v2, Lflipboard/service/Flap$FollowListType;->b:Lflipboard/service/Flap$FollowListType;

    if-ne v1, v2, :cond_6

    .line 425
    iget-object v1, v0, Lflipboard/gui/section/UserListFragment$HeaderHolder;->a:Lflipboard/gui/FLTextIntf;

    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v3, 0x7f0d0159

    invoke-virtual {v2, v3}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 426
    iget-object v0, v0, Lflipboard/gui/section/UserListFragment$HeaderHolder;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 427
    :cond_6
    iget-object v1, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a:Lflipboard/gui/section/UserListFragment;

    invoke-static {v1}, Lflipboard/gui/section/UserListFragment;->d(Lflipboard/gui/section/UserListFragment;)Lflipboard/service/Flap$FollowListType;

    move-result-object v1

    sget-object v2, Lflipboard/service/Flap$FollowListType;->a:Lflipboard/service/Flap$FollowListType;

    if-ne v1, v2, :cond_0

    .line 428
    iget-object v1, v0, Lflipboard/gui/section/UserListFragment$HeaderHolder;->a:Lflipboard/gui/FLTextIntf;

    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v3, 0x7f0d0157

    invoke-virtual {v2, v3}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 429
    iget-object v0, v0, Lflipboard/gui/section/UserListFragment$HeaderHolder;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method public final a(I)Lflipboard/objs/FeedSectionLink;
    .locals 2

    .prologue
    .line 260
    const/4 v0, 0x0

    .line 261
    iget-object v1, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a:Lflipboard/gui/section/UserListFragment;

    iget-object v1, v1, Lflipboard/gui/section/UserListFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 262
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a:Lflipboard/gui/section/UserListFragment;

    iget-object v0, v0, Lflipboard/gui/section/UserListFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 264
    :cond_0
    return-object v0
.end method

.method public final b(I)J
    .locals 4

    .prologue
    .line 437
    const-wide/16 v0, 0x0

    .line 438
    invoke-virtual {p0, p1}, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a(I)Lflipboard/objs/FeedSectionLink;

    move-result-object v2

    .line 439
    if-eqz v2, :cond_0

    iget-object v3, v2, Lflipboard/objs/FeedSectionLink;->p:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 440
    iget-object v0, v2, Lflipboard/objs/FeedSectionLink;->p:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    .line 443
    :cond_0
    return-wide v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a:Lflipboard/gui/section/UserListFragment;

    iget-object v0, v0, Lflipboard/gui/section/UserListFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 252
    iget-object v1, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a:Lflipboard/gui/section/UserListFragment;

    invoke-static {v1}, Lflipboard/gui/section/UserListFragment;->c(Lflipboard/gui/section/UserListFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 254
    add-int/lit8 v0, v0, 0x1

    .line 256
    :cond_0
    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 249
    invoke-virtual {p0, p1}, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a(I)Lflipboard/objs/FeedSectionLink;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 268
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a:Lflipboard/gui/section/UserListFragment;

    iget-object v0, v0, Lflipboard/gui/section/UserListFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 281
    const/4 v0, 0x0

    .line 286
    :goto_0
    return v0

    .line 284
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0x7f0a0266

    .line 297
    invoke-virtual {p0, p1}, Lflipboard/gui/section/UserListFragment$UserListAdapter;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_2

    .line 304
    if-nez p2, :cond_1

    .line 306
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030151

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 307
    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 308
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02005a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setPlaceholder(Landroid/graphics/drawable/Drawable;)V

    .line 310
    new-instance v1, Lflipboard/gui/section/UserListFragment$Holder;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lflipboard/gui/section/UserListFragment$Holder;-><init>(B)V

    .line 311
    const v0, 0x7f0a004f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, v1, Lflipboard/gui/section/UserListFragment$Holder;->b:Lflipboard/gui/FLTextIntf;

    .line 312
    const v0, 0x7f0a0268

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, v1, Lflipboard/gui/section/UserListFragment$Holder;->c:Lflipboard/gui/FLTextIntf;

    .line 313
    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, v1, Lflipboard/gui/section/UserListFragment$Holder;->f:Lflipboard/gui/FLImageView;

    .line 314
    const v0, 0x7f0a038e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, Lflipboard/gui/section/UserListFragment$Holder;->d:Landroid/view/ViewGroup;

    .line 315
    const v0, 0x7f0a01a3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FollowButton;

    iput-object v0, v1, Lflipboard/gui/section/UserListFragment$Holder;->e:Lflipboard/gui/FollowButton;

    .line 318
    iget-object v0, v1, Lflipboard/gui/section/UserListFragment$Holder;->d:Landroid/view/ViewGroup;

    new-instance v2, Lflipboard/gui/section/UserListFragment$UserListAdapter$1;

    invoke-direct {v2, p0, v1}, Lflipboard/gui/section/UserListFragment$UserListAdapter$1;-><init>(Lflipboard/gui/section/UserListFragment$UserListAdapter;Lflipboard/gui/section/UserListFragment$Holder;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 324
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 332
    :goto_0
    invoke-virtual {p0, p1}, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a(I)Lflipboard/objs/FeedSectionLink;

    move-result-object v1

    .line 333
    iget-object v2, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a:Lflipboard/gui/section/UserListFragment;

    invoke-static {v2, v0, v1}, Lflipboard/gui/section/UserListFragment;->a(Lflipboard/gui/section/UserListFragment;Lflipboard/gui/section/UserListFragment$Holder;Lflipboard/objs/FeedSectionLink;)V

    .line 341
    :cond_0
    :goto_1
    return-object p2

    .line 326
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/UserListFragment$Holder;

    .line 327
    iget-object v1, v0, Lflipboard/gui/section/UserListFragment$Holder;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->a()V

    goto :goto_0

    .line 336
    :cond_2
    if-nez p2, :cond_0

    .line 337
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030050

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a:Lflipboard/gui/section/UserListFragment;

    iget-object v0, v0, Lflipboard/gui/section/UserListFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
