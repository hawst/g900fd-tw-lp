.class Lflipboard/gui/section/SectionFragment$2;
.super Ljava/lang/Object;
.source "SectionFragment.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/Section;",
        "Lflipboard/service/Section$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/gui/section/SectionFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment;)V
    .locals 0

    .prologue
    .line 445
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 445
    check-cast p2, Lflipboard/service/Section$Message;

    sget-object v0, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    if-ne p2, v0, :cond_5

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/gui/section/SectionFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/SectionFragment$2$1;

    invoke-direct {v1, p0}, Lflipboard/gui/section/SectionFragment$2$1;-><init>(Lflipboard/gui/section/SectionFragment$2;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    check-cast p3, Lflipboard/json/FLObject;

    const-string v0, "refresh"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v1, v0}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/gui/section/SectionFragment;Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->c(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/LoadingPage;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->c(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/LoadingPage;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/section/LoadingPage;->a()V

    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->C()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->d(Lflipboard/gui/section/SectionFragment;)V

    :cond_4
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/SectionFragment$2$2;

    invoke-direct {v1, p0}, Lflipboard/gui/section/SectionFragment$2$2;-><init>(Lflipboard/gui/section/SectionFragment$2;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_5
    sget-object v0, Lflipboard/service/Section$Message;->f:Lflipboard/service/Section$Message;

    if-ne p2, v0, :cond_6

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/SectionFragment$2$3;

    invoke-direct {v1, p0}, Lflipboard/gui/section/SectionFragment$2$3;-><init>(Lflipboard/gui/section/SectionFragment$2;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_6
    sget-object v0, Lflipboard/service/Section$Message;->g:Lflipboard/service/Section$Message;

    if-ne p2, v0, :cond_7

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/SectionFragment$2$4;

    invoke-direct {v1, p0}, Lflipboard/gui/section/SectionFragment$2$4;-><init>(Lflipboard/gui/section/SectionFragment$2;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->f(Lflipboard/gui/section/SectionFragment;)V

    goto :goto_0

    :cond_7
    sget-object v0, Lflipboard/service/Section$Message;->h:Lflipboard/service/Section$Message;

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/SectionFragment$2$5;

    invoke-direct {v1, p0, p3}, Lflipboard/gui/section/SectionFragment$2$5;-><init>(Lflipboard/gui/section/SectionFragment$2;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
