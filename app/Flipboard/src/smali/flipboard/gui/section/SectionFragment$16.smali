.class Lflipboard/gui/section/SectionFragment$16;
.super Ljava/lang/Object;
.source "SectionFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/gui/section/SectionFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment;)V
    .locals 0

    .prologue
    .line 2018
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$16;->a:Lflipboard/gui/section/SectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 2021
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$16;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$16;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2040
    :cond_0
    :goto_0
    return-void

    .line 2026
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$16;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    .line 2027
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$16;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_2

    .line 2028
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$16;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2029
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v3, "SectionFragment:notify:FLIPS:IDLE"

    new-instance v4, Lflipboard/gui/section/SectionFragment$16$1;

    invoke-direct {v4, p0, v0, v1}, Lflipboard/gui/section/SectionFragment$16$1;-><init>(Lflipboard/gui/section/SectionFragment$16;ILjava/util/List;)V

    invoke-virtual {v2, v3, v4}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 2038
    :cond_2
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    goto :goto_0
.end method
