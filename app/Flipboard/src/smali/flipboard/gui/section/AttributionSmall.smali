.class public Lflipboard/gui/section/AttributionSmall;
.super Lflipboard/gui/FLRelativeLayout;
.source "AttributionSmall.java"

# interfaces
.implements Lflipboard/gui/section/Attribution;


# instance fields
.field a:Lflipboard/gui/FLTextView;

.field b:Lflipboard/gui/FLImageView;

.field c:Lflipboard/gui/FLCameleonImageView;

.field d:Lflipboard/service/Section;

.field e:Lflipboard/objs/FeedItem;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 58
    iput-object p2, p0, Lflipboard/gui/section/AttributionSmall;->e:Lflipboard/objs/FeedItem;

    .line 59
    iput-object p1, p0, Lflipboard/gui/section/AttributionSmall;->d:Lflipboard/service/Section;

    .line 60
    invoke-virtual {p0, p2}, Lflipboard/gui/section/AttributionSmall;->setTag(Ljava/lang/Object;)V

    .line 61
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 63
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->B()Lflipboard/objs/FeedSectionLink;

    move-result-object v1

    .line 64
    if-eqz v1, :cond_1

    .line 66
    iget-object v0, p0, Lflipboard/gui/section/AttributionSmall;->a:Lflipboard/gui/FLTextView;

    iget-object v1, v1, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    :goto_0
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Lflipboard/gui/section/AttributionSmall;->c:Lflipboard/gui/FLCameleonImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setVisibility(I)V

    .line 74
    :cond_0
    return-void

    .line 68
    :cond_1
    iget-object v1, p0, Lflipboard/gui/section/AttributionSmall;->a:Lflipboard/gui/FLTextView;

    invoke-static {v0}, Lflipboard/gui/section/ItemDisplayUtil;->f(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lflipboard/gui/section/AttributionSmall;->f:Z

    return v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 51
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onFinishInflate()V

    .line 52
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 53
    return-void
.end method

.method public setDrawTopDivider(Z)V
    .locals 0

    .prologue
    .line 95
    return-void
.end method

.method public setInverted(Z)V
    .locals 2

    .prologue
    .line 77
    iput-boolean p1, p0, Lflipboard/gui/section/AttributionSmall;->f:Z

    .line 78
    iget-object v0, p0, Lflipboard/gui/section/AttributionSmall;->c:Lflipboard/gui/FLCameleonImageView;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLCameleonImageView;ZZ)V

    .line 79
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionSmall;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p1, :cond_0

    const v0, 0x7f0800aa

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 80
    iget-object v1, p0, Lflipboard/gui/section/AttributionSmall;->a:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    .line 81
    return-void

    .line 79
    :cond_0
    const v0, 0x7f08002e

    goto :goto_0
.end method
