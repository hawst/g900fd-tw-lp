.class public Lflipboard/gui/section/MagazineConversationThreadDetailFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "MagazineConversationThreadDetailFragment.java"

# interfaces
.implements Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;
.implements Lflipboard/service/Flap$CommentaryObserver;


# instance fields
.field final a:Lflipboard/service/FlipboardManager;

.field b:Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;

.field private final c:Ljava/lang/String;

.field private d:Lflipboard/util/Log;

.field private e:Landroid/widget/ListView;

.field private f:Lflipboard/activities/GenericFragmentActivity;

.field private g:Landroid/widget/EditText;

.field private h:Landroid/widget/Button;

.field private i:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/CommentaryResult$Item$Commentary;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private q:Z

.field private r:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 56
    const-string v0, "Comments"

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->c:Ljava/lang/String;

    .line 58
    const-string v0, "magazine_conversation_thread_comment_creation"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->d:Lflipboard/util/Log;

    .line 59
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->a:Lflipboard/service/FlipboardManager;

    .line 74
    iput-boolean v1, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->q:Z

    .line 75
    iput-boolean v1, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->r:Z

    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->o:Ljava/util/List;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 141
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->b:Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;

    iget-boolean v0, v0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->q:Z

    if-nez v0, :cond_0

    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->q:Z

    .line 143
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 144
    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->i:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->a:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v3, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->n:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3, p0}, Lflipboard/service/Flap;->a(Lflipboard/service/User;Ljava/util/List;Ljava/lang/String;Lflipboard/service/Flap$CommentaryObserver;)Lflipboard/service/Flap$CommentaryRequest;

    .line 147
    :cond_0
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;Z)Z
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->r:Z

    return p1
.end method

.method static synthetic b(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->a()V

    return-void
.end method

.method static synthetic c(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->g:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic d(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->h:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic e(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)V
    .locals 6

    .prologue
    .line 45
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->f:Lflipboard/activities/GenericFragmentActivity;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->d(Landroid/app/Activity;)V

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v0

    sget-object v1, Lflipboard/objs/CommentaryResult$CommentType;->b:Lflipboard/objs/CommentaryResult$CommentType;

    iget-object v2, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->a:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v3, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->i:Ljava/lang/String;

    iget-object v4, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->g:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$6;

    invoke-direct {v5, p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$6;-><init>(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)V

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/Flap;->a(Lflipboard/objs/CommentaryResult$CommentType;Lflipboard/service/User;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/ServiceReloginObserver;)V

    return-void
.end method

.method static synthetic f(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Lflipboard/util/Log;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->d:Lflipboard/util/Log;

    return-object v0
.end method

.method static synthetic g(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->j:Z

    return v0
.end method

.method static synthetic h(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->e:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic j(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->r:Z

    return v0
.end method


# virtual methods
.method public final a(Lflipboard/objs/HasCommentaryItem;)V
    .locals 0

    .prologue
    .line 290
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 45
    check-cast p1, Lflipboard/objs/CommentaryResult;

    iget-object v0, p1, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iput-boolean v3, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->q:Z

    iget-object v0, p1, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item;

    if-eqz v0, :cond_1

    iget-object v1, v0, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/CommentaryResult$Item$Commentary;

    iget-object v7, v1, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    const-string v8, "comment"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->p:Ljava/util/Set;

    iget-object v8, v1, Lflipboard/objs/CommentaryResult$Item$Commentary;->h:Ljava/lang/String;

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->p:Ljava/util/Set;

    iget-object v1, v1, Lflipboard/objs/CommentaryResult$Item$Commentary;->h:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move v1, v4

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item;->v:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->n:Ljava/lang/String;

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->b:Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;

    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->n:Ljava/lang/String;

    if-eqz v1, :cond_2

    :goto_2
    iput-boolean v4, v0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->a:Z

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;

    invoke-direct {v1, p0, v2, v5}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;-><init>(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;ZLjava/util/List;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    :cond_1
    return-void

    :cond_2
    move v4, v3

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 347
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->f:Lflipboard/activities/GenericFragmentActivity;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->f:Lflipboard/activities/GenericFragmentActivity;

    invoke-virtual {v0}, Lflipboard/activities/GenericFragmentActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v0

    invoke-virtual {p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d01f8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0201cf

    invoke-virtual {v0, v2, v1}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    .line 350
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 80
    invoke-virtual {p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/GenericFragmentActivity;

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->f:Lflipboard/activities/GenericFragmentActivity;

    .line 81
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    .line 85
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FlipboardFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->o:Ljava/util/List;

    .line 88
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->p:Ljava/util/Set;

    .line 90
    invoke-virtual {p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "feedItemId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->i:Ljava/lang/String;

    .line 91
    invoke-virtual {p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "magazine_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->m:Ljava/lang/String;

    .line 93
    const v0, 0x7f0300d2

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 95
    new-instance v0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;

    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->f:Lflipboard/activities/GenericFragmentActivity;

    iget-object v2, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->o:Ljava/util/List;

    iget-object v3, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->m:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;-><init>(Lflipboard/activities/GenericFragmentActivity;Ljava/util/List;Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->b:Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;

    .line 96
    const v0, 0x7f0a026a

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->e:Landroid/widget/ListView;

    .line 97
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->e:Landroid/widget/ListView;

    new-instance v1, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$1;

    invoke-direct {v1, p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$1;-><init>(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 98
    iget-object v5, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->e:Landroid/widget/ListView;

    const v0, 0x7f0300d1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    const v0, 0x7f0a004f

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    const v1, 0x7f0a0267

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLLabelTextView;

    const v2, 0x7f0a0266

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLImageView;

    const v3, 0x7f0a0268

    invoke-virtual {v6, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lflipboard/gui/FLTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "magazine_conversation_thread_creation_time"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    invoke-virtual {p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    const-string v10, "magazine_conversation_thread_description"

    invoke-virtual {v7, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    const-string v10, "magazine_conversation_thread_creator_username"

    invoke-virtual {v7, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->f:Lflipboard/activities/GenericFragmentActivity;

    invoke-static {v0, v8, v9}, Lflipboard/util/JavaUtil;->a(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "magazine_conversation_thread_creator_image_url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "magazine_conversation_thread_description"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v3, v1, v2, v0}, Lflipboard/util/SocialHelper;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/List;Landroid/os/Bundle;)V

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 99
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->e:Landroid/widget/ListView;

    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->b:Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 101
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->f:Lflipboard/activities/GenericFragmentActivity;

    invoke-virtual {v0}, Lflipboard/activities/GenericFragmentActivity;->l()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->e()V

    .line 103
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->h()V

    .line 105
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->a:Lflipboard/service/FlipboardManager;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->a:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    if-eqz v1, :cond_1

    const v0, 0x7f0a026c

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setTag(Ljava/lang/Object;)V

    if-eqz v1, :cond_0

    iget-object v2, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v2}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v1, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v1}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    :goto_0
    const v0, 0x7f0a026e

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->g:Landroid/widget/EditText;

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->g:Landroid/widget/EditText;

    new-instance v1, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$2;

    invoke-direct {v1, p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$2;-><init>(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->g:Landroid/widget/EditText;

    new-instance v1, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$3;

    invoke-direct {v1, p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$3;-><init>(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->g:Landroid/widget/EditText;

    new-instance v1, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$4;

    invoke-direct {v1, p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$4;-><init>(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const v0, 0x7f0a026d

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->h:Landroid/widget/Button;

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->h:Landroid/widget/Button;

    new-instance v1, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$5;

    invoke-direct {v1, p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$5;-><init>(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->f:Lflipboard/activities/GenericFragmentActivity;

    const v1, 0x7f0a004d

    invoke-virtual {v0, v1}, Lflipboard/activities/GenericFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 108
    const-string v1, "Comments"

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->b:Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->a:Z

    .line 111
    invoke-direct {p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->a()V

    .line 112
    return-object v4

    .line 105
    :cond_0
    const v1, 0x7f02005a

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    goto :goto_0

    :cond_1
    const v0, 0x7f0a026b

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
