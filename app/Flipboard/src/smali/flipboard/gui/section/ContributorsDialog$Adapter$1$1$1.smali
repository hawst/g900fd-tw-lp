.class Lflipboard/gui/section/ContributorsDialog$Adapter$1$1$1;
.super Ljava/lang/Object;
.source "ContributorsDialog.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;


# direct methods
.method constructor <init>(Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1$1;->a:Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1$1;->a:Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;->b:Lflipboard/gui/section/ContributorsDialog$Adapter$1;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter$1;->b:Lflipboard/gui/section/ContributorsDialog$Adapter;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter;->b:Lflipboard/gui/section/ContributorsDialog;

    invoke-static {v0}, Lflipboard/gui/section/ContributorsDialog;->a(Lflipboard/gui/section/ContributorsDialog;)Lflipboard/service/Section;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/Section;->G:Ljava/util/List;

    iget-object v1, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1$1;->a:Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;

    iget-object v1, v1, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;->a:Lflipboard/objs/CommentaryResult$Item$Commentary;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 150
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1$1;->a:Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;->b:Lflipboard/gui/section/ContributorsDialog$Adapter$1;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter$1;->b:Lflipboard/gui/section/ContributorsDialog$Adapter;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter;->b:Lflipboard/gui/section/ContributorsDialog;

    invoke-static {v0}, Lflipboard/gui/section/ContributorsDialog;->b(Lflipboard/gui/section/ContributorsDialog;)Lflipboard/gui/section/ContributorsDialog$Adapter;

    move-result-object v0

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter;->a:Ljava/util/List;

    iget-object v1, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1$1;->a:Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;

    iget-object v1, v1, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;->a:Lflipboard/objs/CommentaryResult$Item$Commentary;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 151
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1$1;->a:Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;->b:Lflipboard/gui/section/ContributorsDialog$Adapter$1;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter$1;->b:Lflipboard/gui/section/ContributorsDialog$Adapter;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter;->b:Lflipboard/gui/section/ContributorsDialog;

    invoke-static {v0}, Lflipboard/gui/section/ContributorsDialog;->c(Lflipboard/gui/section/ContributorsDialog;)Lflipboard/gui/section/ContributorsDialog$ContributorChangedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1$1;->a:Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;->b:Lflipboard/gui/section/ContributorsDialog$Adapter$1;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter$1;->b:Lflipboard/gui/section/ContributorsDialog$Adapter;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter;->b:Lflipboard/gui/section/ContributorsDialog;

    invoke-static {v0}, Lflipboard/gui/section/ContributorsDialog;->c(Lflipboard/gui/section/ContributorsDialog;)Lflipboard/gui/section/ContributorsDialog$ContributorChangedListener;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1$1;->a:Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;

    iget-object v1, v1, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;->b:Lflipboard/gui/section/ContributorsDialog$Adapter$1;

    iget-object v1, v1, Lflipboard/gui/section/ContributorsDialog$Adapter$1;->b:Lflipboard/gui/section/ContributorsDialog$Adapter;

    iget-object v1, v1, Lflipboard/gui/section/ContributorsDialog$Adapter;->b:Lflipboard/gui/section/ContributorsDialog;

    invoke-static {v1}, Lflipboard/gui/section/ContributorsDialog;->a(Lflipboard/gui/section/ContributorsDialog;)Lflipboard/service/Section;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/section/ContributorsDialog$ContributorChangedListener;->a(Lflipboard/service/Section;)V

    .line 154
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1$1$1;

    invoke-direct {v1, p0}, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1$1$1;-><init>(Lflipboard/gui/section/ContributorsDialog$Adapter$1$1$1;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 161
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1$1;->a:Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter$1$1;->b:Lflipboard/gui/section/ContributorsDialog$Adapter$1;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter$1;->b:Lflipboard/gui/section/ContributorsDialog$Adapter;

    iget-object v0, v0, Lflipboard/gui/section/ContributorsDialog$Adapter;->b:Lflipboard/gui/section/ContributorsDialog;

    invoke-virtual {v0}, Lflipboard/gui/section/ContributorsDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 166
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_0

    .line 167
    const v1, 0x7f0d0304

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 169
    :cond_0
    return-void
.end method
