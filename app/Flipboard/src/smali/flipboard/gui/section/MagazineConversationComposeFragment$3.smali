.class Lflipboard/gui/section/MagazineConversationComposeFragment$3;
.super Lflipboard/service/ServiceReloginObserver;
.source "MagazineConversationComposeFragment.java"


# instance fields
.field final synthetic a:Lflipboard/gui/section/MagazineConversationComposeFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/MagazineConversationComposeFragment;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lflipboard/gui/section/MagazineConversationComposeFragment$3;->a:Lflipboard/gui/section/MagazineConversationComposeFragment;

    invoke-direct {p0}, Lflipboard/service/ServiceReloginObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment$3;->a:Lflipboard/gui/section/MagazineConversationComposeFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationComposeFragment;->d(Lflipboard/gui/section/MagazineConversationComposeFragment;)Lflipboard/util/Log;

    .line 148
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment$3;->a:Lflipboard/gui/section/MagazineConversationComposeFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/MagazineConversationComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 149
    invoke-super {p0, v0, p2, p3}, Lflipboard/service/ServiceReloginObserver;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    return-void
.end method

.method public final a(Lflipboard/json/FLObject;)V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment$3;->a:Lflipboard/gui/section/MagazineConversationComposeFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationComposeFragment;->d(Lflipboard/gui/section/MagazineConversationComposeFragment;)Lflipboard/util/Log;

    .line 123
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment$3;->a:Lflipboard/gui/section/MagazineConversationComposeFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationComposeFragment;->a(Lflipboard/gui/section/MagazineConversationComposeFragment;)Lflipboard/activities/GenericFragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment$3;->a:Lflipboard/gui/section/MagazineConversationComposeFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationComposeFragment;->a(Lflipboard/gui/section/MagazineConversationComposeFragment;)Lflipboard/activities/GenericFragmentActivity;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->c()V

    .line 126
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x7f0d01f7

    .line 129
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment$3;->a:Lflipboard/gui/section/MagazineConversationComposeFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationComposeFragment;->d(Lflipboard/gui/section/MagazineConversationComposeFragment;)Lflipboard/util/Log;

    .line 130
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationComposeFragment$3;->a:Lflipboard/gui/section/MagazineConversationComposeFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/MagazineConversationComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 131
    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationComposeFragment$3;->a:Lflipboard/gui/section/MagazineConversationComposeFragment;

    invoke-static {v1}, Lflipboard/gui/section/MagazineConversationComposeFragment;->e(Lflipboard/gui/section/MagazineConversationComposeFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 134
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 135
    const v1, 0x7f0d01f5

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 136
    invoke-virtual {v0, v3}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 141
    :goto_0
    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 143
    :cond_0
    return-void

    .line 138
    :cond_1
    const v1, 0x7f0d01f6

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 139
    invoke-virtual {v0, v3}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
