.class Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;
.super Ljava/lang/Object;
.source "MagazineConversationThreadDetailFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;ZLjava/util/List;)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;->c:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    iput-boolean p2, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;->a:Z

    iput-object p3, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 318
    iget-boolean v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;->a:Z

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;->c:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->a(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 323
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;->c:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    iget-object v0, v0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->b:Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;

    invoke-virtual {v0}, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->notifyDataSetChanged()V

    .line 327
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;->c:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->h(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;->c:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->i(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 328
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;->c:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->i(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;->c:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-static {v1}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->i(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 330
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "loading"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 331
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 334
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;->c:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->j(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 335
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;->c:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->i(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;->c:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-static {v1}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->a(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    .line 336
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$7;->c:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->a(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;Z)Z

    .line 338
    :cond_2
    return-void
.end method
