.class public Lflipboard/gui/section/AttributionServiceInfo;
.super Lflipboard/gui/section/AttributionBase;
.source "AttributionServiceInfo.java"


# static fields
.field public static s:I


# instance fields
.field private A:Landroid/widget/ImageView;

.field private B:Lflipboard/gui/FLLabelTextView;

.field private C:Lflipboard/objs/FeedItem;

.field private D:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private E:Z

.field public t:Z

.field public u:Lflipboard/gui/FLTextView;

.field public v:Landroid/view/View;

.field public w:Lflipboard/gui/FLTextView;

.field private x:Lflipboard/gui/FLLabelTextView;

.field private y:Lflipboard/gui/FLTextView;

.field private z:Lflipboard/gui/FLTextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/16 v0, 0x12c

    sput v0, Lflipboard/gui/section/AttributionServiceInfo;->s:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lflipboard/gui/section/AttributionBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 75
    check-cast p1, Lflipboard/activities/FlipboardActivity;

    iput-object p1, p0, Lflipboard/gui/section/AttributionServiceInfo;->a:Lflipboard/activities/FlipboardActivity;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->n:Ljava/util/List;

    .line 78
    new-instance v0, Lflipboard/gui/section/AttributionServiceInfo$1;

    invoke-direct {v0, p0}, Lflipboard/gui/section/AttributionServiceInfo$1;-><init>(Lflipboard/gui/section/AttributionServiceInfo;)V

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionServiceInfo;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/AttributionServiceInfo;)Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->t:Z

    return v0
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const v10, 0x7f03001f

    const/4 v9, 0x0

    const/4 v2, 0x0

    const/16 v8, 0x8

    .line 114
    iput-object p2, p0, Lflipboard/gui/section/AttributionServiceInfo;->d:Lflipboard/objs/FeedItem;

    .line 115
    iput-object p1, p0, Lflipboard/gui/section/AttributionServiceInfo;->b:Lflipboard/service/Section;

    .line 116
    invoke-virtual {p0, p2}, Lflipboard/gui/section/AttributionServiceInfo;->setTag(Ljava/lang/Object;)V

    .line 118
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    .line 120
    iget-boolean v0, p2, Lflipboard/objs/FeedItem;->bk:Z

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->x:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0, v8}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 128
    :goto_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 129
    const-string v0, "source"

    const-string v4, "sectionLink"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string v0, "originSectionIdentifier"

    iget-object v4, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const-string v0, "linkType"

    const-string v4, "magazine"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-virtual {p0, v3}, Lflipboard/gui/section/AttributionServiceInfo;->setReasonField(Landroid/os/Bundle;)V

    .line 135
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eq v0, p2, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    iget-boolean v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->t:Z

    if-nez v0, :cond_3

    .line 136
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v8}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 288
    :cond_0
    :goto_2
    return-void

    .line 123
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionServiceInfo;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-wide v4, p2, Lflipboard/objs/FeedItem;->Z:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-static {v0, v4, v5}, Lflipboard/util/JavaUtil;->e(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 124
    iget-object v3, p0, Lflipboard/gui/section/AttributionServiceInfo;->x:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v3, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->x:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 135
    goto :goto_1

    .line 139
    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v4

    .line 140
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {v5}, Lflipboard/objs/FeedItem;->ai()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    .line 142
    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v5, v2}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 143
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->B()Lflipboard/objs/FeedSectionLink;

    move-result-object v5

    .line 144
    iget-object v6, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    if-ne p2, v6, :cond_10

    if-eqz v5, :cond_10

    .line 146
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->h:Lflipboard/gui/FLTextView;

    iget-object v4, v5, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    iget-object v1, v5, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    if-eqz v1, :cond_f

    .line 148
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 149
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    iget-object v4, v5, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    invoke-virtual {v1, v4}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 153
    :goto_3
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v8}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    move-object v1, v0

    .line 180
    :goto_4
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->B()Lflipboard/objs/FeedSectionLink;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_4

    iget-object v4, v0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    if-eqz v4, :cond_4

    .line 182
    iget-object v4, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    new-instance v5, Lflipboard/gui/section/AttributionServiceInfo$2;

    invoke-direct {v5, p0, p1, v0}, Lflipboard/gui/section/AttributionServiceInfo$2;-><init>(Lflipboard/gui/section/AttributionServiceInfo;Lflipboard/service/Section;Lflipboard/objs/FeedSectionLink;)V

    invoke-virtual {v4, v5}, Lflipboard/gui/FLImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    :cond_4
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->ad:Z

    if-eqz v0, :cond_5

    .line 194
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->a:Lflipboard/activities/FlipboardActivity;

    invoke-static {v0, v10, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    iput-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->k:Lflipboard/gui/section/AttributionButtonWithText;

    .line 195
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->k:Lflipboard/gui/section/AttributionButtonWithText;

    const v4, 0x7f0a0003

    invoke-virtual {v0, v4}, Lflipboard/gui/section/AttributionButtonWithText;->setId(I)V

    .line 196
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->k:Lflipboard/gui/section/AttributionButtonWithText;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionServiceInfo;->addView(Landroid/view/View;)V

    .line 197
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->n:Ljava/util/List;

    iget-object v4, p0, Lflipboard/gui/section/AttributionServiceInfo;->k:Lflipboard/gui/section/AttributionButtonWithText;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->k:Lflipboard/gui/section/AttributionButtonWithText;

    iget-object v4, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v4}, Lflipboard/gui/section/AttributionButtonWithText;->setTag(Ljava/lang/Object;)V

    .line 199
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->k:Lflipboard/gui/section/AttributionButtonWithText;

    new-instance v4, Lflipboard/gui/section/AttributionServiceInfo$3;

    invoke-direct {v4, p0, p1}, Lflipboard/gui/section/AttributionServiceInfo$3;-><init>(Lflipboard/gui/section/AttributionServiceInfo;Lflipboard/service/Section;)V

    invoke-virtual {v0, v4}, Lflipboard/gui/section/AttributionButtonWithText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    :cond_5
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->ag:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->t:Z

    if-nez v0, :cond_6

    .line 212
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->a:Lflipboard/activities/FlipboardActivity;

    invoke-static {v0, v10, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    iput-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->l:Lflipboard/gui/section/AttributionButtonWithText;

    .line 213
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->l:Lflipboard/gui/section/AttributionButtonWithText;

    const v4, 0x7f0a0001

    invoke-virtual {v0, v4}, Lflipboard/gui/section/AttributionButtonWithText;->setId(I)V

    .line 214
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->l:Lflipboard/gui/section/AttributionButtonWithText;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionServiceInfo;->addView(Landroid/view/View;)V

    .line 215
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->n:Ljava/util/List;

    iget-object v4, p0, Lflipboard/gui/section/AttributionServiceInfo;->l:Lflipboard/gui/section/AttributionButtonWithText;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->l:Lflipboard/gui/section/AttributionButtonWithText;

    invoke-virtual {v0, p2}, Lflipboard/gui/section/AttributionButtonWithText;->setTag(Ljava/lang/Object;)V

    .line 217
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->l:Lflipboard/gui/section/AttributionButtonWithText;

    new-instance v4, Lflipboard/gui/section/AttributionServiceInfo$4;

    invoke-direct {v4, p0, p1}, Lflipboard/gui/section/AttributionServiceInfo$4;-><init>(Lflipboard/gui/section/AttributionServiceInfo;Lflipboard/service/Section;)V

    invoke-virtual {v0, v4}, Lflipboard/gui/section/AttributionButtonWithText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    :cond_6
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/objs/FeedItem;->b(Lflipboard/objs/ConfigService;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 228
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->a:Lflipboard/activities/FlipboardActivity;

    invoke-static {v0, v10, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionButtonWithText;

    iput-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->m:Lflipboard/gui/section/AttributionButtonWithText;

    .line 229
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->m:Lflipboard/gui/section/AttributionButtonWithText;

    const v1, 0x7f0a0005

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionButtonWithText;->setId(I)V

    .line 230
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->m:Lflipboard/gui/section/AttributionButtonWithText;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionServiceInfo;->addView(Landroid/view/View;)V

    .line 231
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->n:Ljava/util/List;

    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->m:Lflipboard/gui/section/AttributionButtonWithText;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->m:Lflipboard/gui/section/AttributionButtonWithText;

    new-instance v1, Lflipboard/gui/section/AttributionServiceInfo$5;

    invoke-direct {v1, p0, p1, p2}, Lflipboard/gui/section/AttributionServiceInfo$5;-><init>(Lflipboard/gui/section/AttributionServiceInfo;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionButtonWithText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 239
    :cond_7
    iget-object v0, p2, Lflipboard/objs/FeedItem;->br:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->t:Z

    if-nez v0, :cond_8

    .line 240
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->a:Lflipboard/activities/FlipboardActivity;

    const v1, 0x7f0300ea

    invoke-static {v0, v1, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    .line 241
    const v1, 0x7f0a0004

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setId(I)V

    .line 242
    iget-object v1, p2, Lflipboard/objs/FeedItem;->br:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 243
    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionServiceInfo;->addView(Landroid/view/View;)V

    .line 244
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->n:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    invoke-virtual {v0, p2}, Lflipboard/gui/FLButton;->setTag(Ljava/lang/Object;)V

    .line 247
    :cond_8
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->ag:Z

    if-nez v0, :cond_9

    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->ad:Z

    if-eqz v0, :cond_a

    .line 248
    :cond_9
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionServiceInfo;->a(Lflipboard/objs/CommentaryResult$Item;)V

    .line 251
    :cond_a
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->D()Ljava/lang/String;

    move-result-object v0

    .line 252
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->C:Lflipboard/objs/FeedItem;

    if-nez v1, :cond_14

    if-eqz v0, :cond_14

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_14

    .line 253
    iget-boolean v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->t:Z

    if-eqz v1, :cond_b

    .line 254
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->z:Lflipboard/gui/FLTextView;

    const v4, 0x186a0

    invoke-virtual {v1, v4}, Lflipboard/gui/FLTextView;->setMaxLines(I)V

    .line 256
    :cond_b
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->z:Lflipboard/gui/FLTextView;

    iget-object v4, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-static {v1, v0, v4, v9, v3}, Lflipboard/util/SocialHelper;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/List;Landroid/view/View$OnClickListener;Landroid/os/Bundle;)V

    .line 257
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->z:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 258
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->w:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 263
    :goto_5
    iget-boolean v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->t:Z

    if-eqz v0, :cond_c

    .line 264
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->w:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v8}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 267
    :cond_c
    invoke-virtual {p0, v3}, Lflipboard/gui/section/AttributionServiceInfo;->setSubtitle(Landroid/os/Bundle;)V

    .line 268
    iget-boolean v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->t:Z

    if-eqz v0, :cond_e

    .line 269
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_e

    .line 271
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 272
    iget-object v3, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v3, :cond_d

    iget-object v0, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    const-string v3, "author"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 279
    :cond_e
    iput-boolean v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->r:Z

    .line 281
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionServiceInfo;->c()V

    .line 282
    invoke-virtual {p1}, Lflipboard/service/Section;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->t:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->E:Z

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v8}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 284
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->z:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v0

    if-ne v0, v8, :cond_0

    .line 285
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v8}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 151
    :cond_f
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v8}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 155
    :cond_10
    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-object v5, v5, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v6, "flipboard"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 156
    iget-boolean v6, p0, Lflipboard/gui/section/AttributionServiceInfo;->t:Z

    if-nez v6, :cond_11

    iget-object v6, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    if-eq v4, v6, :cond_11

    if-nez v5, :cond_11

    .line 157
    iput-object v4, p0, Lflipboard/gui/section/AttributionServiceInfo;->C:Lflipboard/objs/FeedItem;

    .line 158
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->B:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 159
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->B:Lflipboard/gui/FLLabelTextView;

    iget-object v4, p0, Lflipboard/gui/section/AttributionServiceInfo;->C:Lflipboard/objs/FeedItem;

    invoke-virtual {v4}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->A:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 161
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/view/View;

    iget-object v4, p0, Lflipboard/gui/section/AttributionServiceInfo;->A:Landroid/widget/ImageView;

    aput-object v4, v0, v2

    iget-object v4, p0, Lflipboard/gui/section/AttributionServiceInfo;->B:Lflipboard/gui/FLLabelTextView;

    aput-object v4, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->D:Ljava/util/List;

    .line 162
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->ai()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    .line 164
    :cond_11
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v1, :cond_12

    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v1}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_12

    .line 165
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    iget-object v4, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v4}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 170
    :goto_6
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->h:Lflipboard/gui/FLTextView;

    iget-object v4, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {v4}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v4, "googlereader"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    if-nez v5, :cond_13

    if-eqz v0, :cond_13

    iget-object v1, v0, Lflipboard/objs/ConfigService;->m:Ljava/lang/String;

    if-eqz v1, :cond_13

    .line 173
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->j:Lflipboard/gui/FLImageView;

    iget-object v4, v0, Lflipboard/objs/ConfigService;->m:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    move-object v1, v0

    goto/16 :goto_4

    .line 168
    :cond_12
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    const v4, 0x7f02005a

    invoke-virtual {v1, v4}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    goto :goto_6

    .line 175
    :cond_13
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v8}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    move-object v1, v0

    goto/16 :goto_4

    .line 260
    :cond_14
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->z:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v8}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 261
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->w:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v8}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    goto/16 :goto_5
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 516
    iget-boolean v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->c:Z

    return v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 520
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->W()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->k:Lflipboard/gui/section/AttributionButtonWithText;

    const-string v2, "likeButtonInlineActionHint"

    invoke-virtual {v0, v1, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 522
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->l:Lflipboard/gui/section/AttributionButtonWithText;

    const-string v2, "commentButtonInlineActionHint"

    invoke-virtual {v0, v1, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 524
    :cond_0
    return-void
.end method

.method protected final c()V
    .locals 5

    .prologue
    const v1, 0x7f080085

    const v2, 0x7f080084

    .line 346
    invoke-super {p0}, Lflipboard/gui/section/AttributionBase;->c()V

    .line 347
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->W()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 349
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 350
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 352
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->E:Z

    .line 354
    :cond_1
    iget-object v3, p0, Lflipboard/gui/section/AttributionServiceInfo;->z:Lflipboard/gui/FLTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/AttributionServiceInfo;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-boolean v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->c:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    .line 355
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->x:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/AttributionServiceInfo;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-boolean v4, p0, Lflipboard/gui/section/AttributionServiceInfo;->c:Z

    if-eqz v4, :cond_3

    :goto_2
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setTextColor(I)V

    .line 356
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->v:Landroid/view/View;

    invoke-virtual {p0}, Lflipboard/gui/section/AttributionServiceInfo;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-boolean v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->c:Z

    if-eqz v0, :cond_4

    const v0, 0x7f080028

    :goto_3
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 357
    return-void

    :cond_2
    move v0, v2

    .line 354
    goto :goto_1

    :cond_3
    move v1, v2

    .line 355
    goto :goto_2

    .line 356
    :cond_4
    const v0, 0x7f080021

    goto :goto_3
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 93
    invoke-super {p0}, Lflipboard/gui/section/AttributionBase;->onFinishInflate()V

    .line 94
    const v0, 0x7f0a007c

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionServiceInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    .line 95
    const v0, 0x7f0a007d

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionServiceInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->h:Lflipboard/gui/FLTextView;

    .line 96
    const v0, 0x7f0a007e

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionServiceInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->j:Lflipboard/gui/FLImageView;

    .line 97
    const v0, 0x7f0a007f

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionServiceInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->x:Lflipboard/gui/FLLabelTextView;

    .line 98
    const v0, 0x7f0a0080

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionServiceInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->y:Lflipboard/gui/FLTextView;

    .line 99
    const v0, 0x7f0a0082

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionServiceInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->z:Lflipboard/gui/FLTextView;

    .line 100
    const v0, 0x7f0a0084

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionServiceInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->A:Landroid/widget/ImageView;

    .line 101
    const v0, 0x7f0a0085

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionServiceInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->B:Lflipboard/gui/FLLabelTextView;

    .line 103
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 104
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    .line 440
    sub-int v4, p4, p2

    .line 441
    iget v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    .line 442
    iget v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v2

    add-int v3, v1, v2

    .line 443
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->u:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_c

    .line 444
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->u:Lflipboard/gui/FLTextView;

    iget v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    iget v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    iget-object v6, p0, Lflipboard/gui/section/AttributionServiceInfo;->u:Lflipboard/gui/FLTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lflipboard/gui/section/AttributionServiceInfo;->u:Lflipboard/gui/FLTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v1, v2, v0, v5, v6}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 445
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->u:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 446
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->v:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_c

    .line 447
    iget v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    add-int/2addr v0, v1

    .line 448
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->v:Landroid/view/View;

    iget v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    iget v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    iget-object v6, p0, Lflipboard/gui/section/AttributionServiceInfo;->v:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lflipboard/gui/section/AttributionServiceInfo;->v:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v1, v2, v0, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 449
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->v:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    move v1, v0

    .line 454
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_0

    .line 455
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    iget v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v0, v2, v1, v3, v5}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 459
    :cond_0
    const/4 v2, 0x1

    .line 460
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 461
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v6, 0x8

    if-eq v0, v6, :cond_1

    .line 462
    const/4 v0, 0x0

    .line 466
    :goto_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->y:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->z:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->A:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-ne v0, v2, :cond_2

    .line 467
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 469
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_9

    iget v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    add-int/2addr v0, v3

    .line 470
    :goto_2
    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v0

    .line 471
    iget-object v3, p0, Lflipboard/gui/section/AttributionServiceInfo;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v1

    .line 472
    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v5, v0, v1, v2, v3}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 473
    iget v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->f:I

    add-int/2addr v5, v2

    .line 475
    iget-object v6, p0, Lflipboard/gui/section/AttributionServiceInfo;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v6}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-eq v6, v7, :cond_3

    .line 476
    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v5

    .line 480
    :cond_3
    iget-object v6, p0, Lflipboard/gui/section/AttributionServiceInfo;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v6

    iget-object v7, p0, Lflipboard/gui/section/AttributionServiceInfo;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v7}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v7

    sub-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    .line 481
    iget-object v7, p0, Lflipboard/gui/section/AttributionServiceInfo;->j:Lflipboard/gui/FLImageView;

    add-int v8, v1, v6

    add-int/2addr v6, v1

    iget-object v9, p0, Lflipboard/gui/section/AttributionServiceInfo;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v9}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v6, v9

    invoke-virtual {v7, v5, v8, v2, v6}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 482
    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->x:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v2

    const/16 v5, 0x8

    if-eq v2, v5, :cond_5

    .line 483
    iget v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    sub-int v2, v4, v2

    .line 484
    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->u:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_4

    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->v:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-ne v5, v6, :cond_4

    iget v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    .line 485
    :cond_4
    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->x:Lflipboard/gui/FLLabelTextView;

    iget-object v6, p0, Lflipboard/gui/section/AttributionServiceInfo;->x:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v6

    sub-int v6, v2, v6

    iget-object v7, p0, Lflipboard/gui/section/AttributionServiceInfo;->x:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v1

    invoke-virtual {v5, v6, v1, v2, v7}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 490
    :cond_5
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->y:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_a

    .line 491
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->y:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v3

    .line 492
    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->y:Lflipboard/gui/FLTextView;

    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->y:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v2, v0, v3, v5, v1}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 496
    :goto_3
    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->z:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_6

    .line 497
    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->z:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, v1

    .line 498
    iget-object v3, p0, Lflipboard/gui/section/AttributionServiceInfo;->z:Lflipboard/gui/FLTextView;

    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->z:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v3, v0, v1, v5, v2}, Lflipboard/gui/FLTextView;->layout(IIII)V

    move v1, v2

    .line 500
    :cond_6
    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->w:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_7

    .line 501
    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->w:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v1, v2

    .line 502
    iget v3, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    sub-int v3, v4, v3

    .line 503
    iget-object v4, p0, Lflipboard/gui/section/AttributionServiceInfo;->w:Lflipboard/gui/FLTextView;

    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->w:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v5

    sub-int v5, v3, v5

    invoke-virtual {v4, v5, v2, v3, v1}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 507
    :cond_7
    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->A:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_8

    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->B:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_8

    .line 508
    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->D:Ljava/util/List;

    invoke-virtual {p0, v0, v1, v2}, Lflipboard/gui/section/AttributionServiceInfo;->a(IILjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 511
    :cond_8
    iget v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->f:I

    add-int/2addr v1, v2

    .line 512
    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->n:Ljava/util/List;

    invoke-virtual {p0, v0, v1, v2}, Lflipboard/gui/section/AttributionServiceInfo;->a(IILjava/util/List;)I

    .line 513
    return-void

    .line 469
    :cond_9
    iget v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    goto/16 :goto_2

    :cond_a
    move v1, v3

    goto :goto_3

    :cond_b
    move v0, v2

    goto/16 :goto_1

    :cond_c
    move v1, v0

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 11

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/high16 v9, -0x80000000

    const/16 v8, 0x8

    const/4 v2, 0x0

    .line 361
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 362
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 363
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v0

    if-eq v0, v8, :cond_0

    .line 364
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    invoke-static {v0}, Lflipboard/gui/section/AttributionServiceInfo;->a(Landroid/view/View;)V

    .line 366
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v0

    if-eq v0, v8, :cond_1

    .line 367
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->j:Lflipboard/gui/FLImageView;

    invoke-static {v0}, Lflipboard/gui/section/AttributionServiceInfo;->a(Landroid/view/View;)V

    .line 371
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->x:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v0

    if-ne v0, v8, :cond_8

    .line 373
    iget v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->f:I

    mul-int/lit8 v0, v0, 0x2

    move v1, v2

    .line 380
    :goto_0
    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v5

    sub-int v5, v4, v5

    iget-object v6, p0, Lflipboard/gui/section/AttributionServiceInfo;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v6}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v6

    sub-int/2addr v5, v6

    sub-int v1, v5, v1

    iget v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    mul-int/lit8 v5, v5, 0x4

    sub-int/2addr v1, v5

    sub-int/2addr v1, v0

    .line 382
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v0

    if-eq v0, v8, :cond_e

    .line 383
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->h:Lflipboard/gui/FLTextView;

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v0, v5, v6}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 384
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v0

    iget v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    add-int/2addr v0, v5

    add-int/lit8 v0, v0, 0x0

    .line 386
    :goto_1
    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->y:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v5

    if-eq v5, v8, :cond_2

    .line 387
    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->y:Lflipboard/gui/FLTextView;

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 388
    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->y:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v0, v5

    .line 390
    :cond_2
    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->w:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v5

    if-eq v5, v8, :cond_3

    .line 391
    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->w:Lflipboard/gui/FLTextView;

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 393
    :cond_3
    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->z:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v5

    if-eq v5, v8, :cond_4

    .line 394
    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v5

    sub-int v5, v4, v5

    iget v6, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    mul-int/lit8 v6, v6, 0x4

    sub-int/2addr v5, v6

    iget-object v6, p0, Lflipboard/gui/section/AttributionServiceInfo;->w:Lflipboard/gui/FLTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v6

    sub-int/2addr v5, v6

    .line 395
    iget-object v6, p0, Lflipboard/gui/section/AttributionServiceInfo;->z:Lflipboard/gui/FLTextView;

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v6, v5, v7}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 396
    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->z:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v0, v5

    .line 398
    :cond_4
    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->A:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getVisibility()I

    move-result v5

    if-eq v5, v8, :cond_5

    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->B:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v5

    if-eq v5, v8, :cond_5

    .line 399
    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->A:Landroid/widget/ImageView;

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/widget/ImageView;->measure(II)V

    .line 400
    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->B:Lflipboard/gui/FLLabelTextView;

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, v1, v6}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 401
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->B:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v1

    iget-object v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->A:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v5

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 403
    :cond_5
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v1

    if-eq v1, v8, :cond_6

    .line 404
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->i:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v1

    iget v5, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    add-int/2addr v1, v5

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 406
    :cond_6
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->u:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v1

    if-eq v1, v8, :cond_7

    .line 407
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->u:Lflipboard/gui/FLTextView;

    invoke-static {v4, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v5, v3}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 408
    if-lez v0, :cond_9

    .line 410
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->v:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 411
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->v:Landroid/view/View;

    iget v3, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v4, v3

    invoke-static {v3, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {p0}, Lflipboard/gui/section/AttributionServiceInfo;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0900be

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-static {v5, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v1, v3, v5}, Landroid/view/View;->measure(II)V

    .line 412
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->v:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v3, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 417
    :goto_2
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->u:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 421
    :cond_7
    if-nez v0, :cond_d

    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->x:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v1

    if-eq v1, v8, :cond_d

    .line 422
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->x:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v0

    iget v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    add-int/2addr v0, v1

    move v1, v0

    .line 426
    :goto_3
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionServiceInfo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f090092

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 428
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v2

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 429
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-eq v7, v8, :cond_c

    .line 430
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v5, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v0, v3, v7}, Landroid/view/View;->measure(II)V

    .line 431
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    :goto_5
    move v3, v0

    .line 433
    goto :goto_4

    .line 376
    :cond_8
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->x:Lflipboard/gui/FLLabelTextView;

    invoke-static {v4, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v0, v1, v5}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 377
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->x:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v1

    .line 378
    iget v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->f:I

    mul-int/lit8 v0, v0, 0x3

    goto/16 :goto_0

    .line 414
    :cond_9
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->v:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 415
    iget v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    add-int/2addr v0, v1

    goto :goto_2

    .line 434
    :cond_a
    if-lez v1, :cond_b

    iget v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->g:I

    .line 435
    :cond_b
    add-int v0, v1, v3

    add-int/2addr v0, v2

    invoke-virtual {p0, v4, v0}, Lflipboard/gui/section/AttributionServiceInfo;->setMeasuredDimension(II)V

    .line 436
    return-void

    :cond_c
    move v0, v3

    goto :goto_5

    :cond_d
    move v1, v0

    goto :goto_3

    :cond_e
    move v0, v2

    goto/16 :goto_1
.end method

.method public setDrawTopDivider(Z)V
    .locals 0

    .prologue
    .line 332
    return-void
.end method

.method public setInverted(Z)V
    .locals 1

    .prologue
    .line 338
    iget-boolean v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->c:Z

    if-eq p1, v0, :cond_0

    .line 339
    iput-boolean p1, p0, Lflipboard/gui/section/AttributionServiceInfo;->c:Z

    .line 340
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionServiceInfo;->c()V

    .line 342
    :cond_0
    return-void
.end method

.method public setReasonField(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 314
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bf:Lflipboard/objs/FeedItem$Note;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bf:Lflipboard/objs/FeedItem$Note;

    iget-object v0, v0, Lflipboard/objs/FeedItem$Note;->b:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bf:Lflipboard/objs/FeedItem$Note;

    iget-object v0, v0, Lflipboard/objs/FeedItem$Note;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 315
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bf:Lflipboard/objs/FeedItem$Note;

    iget-object v1, v0, Lflipboard/objs/FeedItem$Note;->a:Ljava/lang/String;

    .line 316
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bf:Lflipboard/objs/FeedItem$Note;

    iget-object v0, v0, Lflipboard/objs/FeedItem$Note;->b:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 317
    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->b:Lflipboard/service/Section;

    invoke-virtual {v2, v0}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedSectionLink;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 319
    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->u:Lflipboard/gui/FLTextView;

    invoke-virtual {v2, v4}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 320
    if-nez v1, :cond_0

    .line 321
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionServiceInfo;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d01ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, v0, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 323
    :cond_0
    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->u:Lflipboard/gui/FLTextView;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v2, v1, v0, p1}, Lflipboard/util/SocialHelper;->b(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/List;Landroid/os/Bundle;)V

    .line 327
    :cond_1
    return-void
.end method

.method public setStatusMaxLines(I)V
    .locals 1

    .prologue
    .line 527
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->z:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLTextView;->setMaxLines(I)V

    .line 528
    return-void
.end method

.method setSubtitle(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 292
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 294
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 295
    iget-object v0, p0, Lflipboard/gui/section/AttributionServiceInfo;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 296
    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    const-string v3, "magazine"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 297
    if-eqz v0, :cond_1

    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->b:Lflipboard/service/Section;

    invoke-virtual {v1, v0}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedSectionLink;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 299
    iget-object v1, p0, Lflipboard/gui/section/AttributionServiceInfo;->y:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v4}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 300
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionServiceInfo;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d01ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, v0, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 303
    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo;->y:Lflipboard/gui/FLTextView;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v2, v1, v0, p1}, Lflipboard/util/SocialHelper;->b(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/List;Landroid/os/Bundle;)V

    .line 311
    :cond_1
    return-void
.end method
