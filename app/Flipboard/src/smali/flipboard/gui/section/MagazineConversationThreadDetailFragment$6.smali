.class Lflipboard/gui/section/MagazineConversationThreadDetailFragment$6;
.super Lflipboard/service/ServiceReloginObserver;
.source "MagazineConversationThreadDetailFragment.java"


# instance fields
.field final synthetic a:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$6;->a:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-direct {p0}, Lflipboard/service/ServiceReloginObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$6;->a:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->f(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Lflipboard/util/Log;

    .line 281
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$6;->a:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 282
    invoke-super {p0, v0, p2, p3}, Lflipboard/service/ServiceReloginObserver;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    return-void
.end method

.method public final a(Lflipboard/json/FLObject;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 247
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$6;->a:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->f(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Lflipboard/util/Log;

    .line 249
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$6;->a:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    iget-object v0, v0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->b:Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;

    iput-boolean v2, v0, Lflipboard/gui/section/MagazineConversationThreadCommentsAdapter;->a:Z

    .line 250
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$6;->a:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->b(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)V

    .line 251
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$6;->a:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    iget-object v0, v0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->a:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$6$1;

    invoke-direct {v1, p0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$6$1;-><init>(Lflipboard/gui/section/MagazineConversationThreadDetailFragment$6;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 258
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$6;->a:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-static {v0, v2}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->a(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;Z)Z

    .line 259
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x7f0d01fc

    .line 262
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$6;->a:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->f(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Lflipboard/util/Log;

    .line 263
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$6;->a:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 264
    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$6;->a:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-static {v1}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->g(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 267
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 268
    const v1, 0x7f0d01fa

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 269
    invoke-virtual {v0, v3}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 274
    :goto_0
    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 276
    :cond_0
    return-void

    .line 271
    :cond_1
    const v1, 0x7f0d01fb

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 272
    invoke-virtual {v0, v3}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
