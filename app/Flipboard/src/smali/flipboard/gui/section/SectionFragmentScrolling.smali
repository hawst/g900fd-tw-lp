.class public Lflipboard/gui/section/SectionFragmentScrolling;
.super Lflipboard/activities/FlipboardFragment;
.source "SectionFragmentScrolling.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lflipboard/swiperefresh/SwipeRefreshLayout$OnRefreshListener;
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/activities/FlipboardFragment;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lflipboard/swiperefresh/SwipeRefreshLayout$OnRefreshListener;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/Section;",
        "Lflipboard/service/Section$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lflipboard/swiperefresh/SwipeRefreshLayout;

.field public b:Lflipboard/gui/PullToRefreshVerticalViewPager;

.field private c:Lflipboard/gui/section/SectionScrollingAdapter;

.field private d:Lflipboard/service/Section;

.field private e:Lflipboard/gui/section/scrolling/header/HeaderView;

.field private f:Lflipboard/gui/FLSliderHider;

.field private g:Lflipboard/swiperefresh/FLSwipeProgressBar;

.field private h:Z

.field private i:Z

.field private m:Z

.field private n:I

.field private o:I

.field private p:I

.field private q:Lflipboard/service/FLAdManager;

.field private volatile r:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FLAdManager;",
            "Lflipboard/service/FLAdManager$Message;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private s:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lflipboard/service/FLAdManager$AdAsset;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lflipboard/gui/ViewWithDrawerLayout;

.field private u:Landroid/widget/FrameLayout;

.field private v:Lflipboard/gui/SmartScrollingListView;

.field private w:Lflipboard/gui/section/scrolling/header/SectionCover;

.field private x:Lflipboard/gui/DynamicViewPagerAdapter;

.field private y:Lflipboard/gui/NoContentView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 63
    iput-boolean v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->h:Z

    .line 64
    iput-boolean v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->i:Z

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->n:I

    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/SectionFragmentScrolling;I)I
    .locals 0

    .prologue
    .line 53
    iput p1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->p:I

    return p1
.end method

.method public static a(Lflipboard/service/Section;Z)Lflipboard/gui/section/SectionFragmentScrolling;
    .locals 4

    .prologue
    .line 79
    new-instance v0, Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-direct {v0}, Lflipboard/gui/section/SectionFragmentScrolling;-><init>()V

    .line 80
    iput-object p0, v0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    .line 81
    iput-boolean p1, v0, Lflipboard/gui/section/SectionFragmentScrolling;->i:Z

    .line 82
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 83
    const-string v2, "extra_section_id"

    invoke-virtual {p0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v2, "extra_is_tab"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 85
    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionFragmentScrolling;->setArguments(Landroid/os/Bundle;)V

    .line 86
    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lflipboard/gui/section/SectionFragmentScrolling;
    .locals 2

    .prologue
    .line 74
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p0}, Lflipboard/service/User;->e(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    .line 75
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflipboard/gui/section/SectionFragmentScrolling;->a(Lflipboard/service/Section;Z)Lflipboard/gui/section/SectionFragmentScrolling;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    return-object v0
.end method

.method static synthetic a(Lflipboard/gui/section/SectionFragmentScrolling;Lflipboard/swiperefresh/FLSwipeProgressBar;)Lflipboard/swiperefresh/FLSwipeProgressBar;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->g:Lflipboard/swiperefresh/FLSwipeProgressBar;

    return-object p1
.end method

.method static synthetic a(Lflipboard/gui/section/SectionFragmentScrolling;Lflipboard/service/Section;)V
    .locals 7

    .prologue
    const v2, 0x7f030109

    .line 53
    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->i:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->h:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->h:Z

    invoke-virtual {p1}, Lflipboard/service/Section;->y()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f03010a

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragmentScrolling;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v3, p0, Lflipboard/gui/section/SectionFragmentScrolling;->u:Landroid/widget/FrameLayout;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/header/HeaderView;

    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->e:Lflipboard/gui/section/scrolling/header/HeaderView;

    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->e:Lflipboard/gui/section/scrolling/header/HeaderView;

    invoke-interface {v0, p1}, Lflipboard/gui/section/scrolling/header/HeaderView;->setItem(Lflipboard/service/Section;)V

    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->e:Lflipboard/gui/section/scrolling/header/HeaderView;

    invoke-interface {v0}, Lflipboard/gui/section/scrolling/header/HeaderView;->getView()Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    new-instance v4, Landroid/view/View;

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragmentScrolling;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v5, -0x1

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    invoke-direct {v0, v5, v6}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->v:Lflipboard/gui/SmartScrollingListView;

    iget-object v0, v0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, v4}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->addHeaderView(Landroid/view/View;)V

    if-ne v1, v2, :cond_0

    const v0, 0x7f0a004d

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    invoke-virtual {p1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    new-instance v0, Lflipboard/gui/section/SectionFragmentScrolling$5;

    invoke-direct {v0, p0, v4, v3}, Lflipboard/gui/section/SectionFragmentScrolling$5;-><init>(Lflipboard/gui/section/SectionFragmentScrolling;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p1}, Lflipboard/service/Section;->z()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f030108

    move v1, v0

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method static synthetic b(Lflipboard/gui/section/SectionFragmentScrolling;I)I
    .locals 0

    .prologue
    .line 53
    iput p1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->n:I

    return p1
.end method

.method static synthetic b(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/ViewWithDrawerLayout;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->t:Lflipboard/gui/ViewWithDrawerLayout;

    return-object v0
.end method

.method static synthetic c(Lflipboard/gui/section/SectionFragmentScrolling;I)I
    .locals 0

    .prologue
    .line 53
    iput p1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->o:I

    return p1
.end method

.method static synthetic c(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/SmartScrollingListView;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->v:Lflipboard/gui/SmartScrollingListView;

    return-object v0
.end method

.method private c()Lflipboard/swiperefresh/FLSwipeProgressBar;
    .locals 6

    .prologue
    const v4, 0x7f08000b

    .line 339
    new-instance v0, Lflipboard/swiperefresh/FLSwipeProgressBar;

    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragmentScrolling;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lflipboard/swiperefresh/FLSwipeProgressBar;-><init>(Landroid/content/Context;)V

    .line 340
    invoke-virtual {v0}, Lflipboard/swiperefresh/FLSwipeProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const v3, 0x7f0800aa

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    const v5, 0x7f08009b

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iget-object v5, v0, Lflipboard/swiperefresh/FLSwipeProgressBar;->a:Lflipboard/swiperefresh/SwipeProgressBar;

    invoke-virtual {v5, v2, v3, v4, v1}, Lflipboard/swiperefresh/SwipeProgressBar;->a(IIII)V

    .line 341
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/swiperefresh/FLSwipeProgressBar;->setRefreshing(Z)V

    .line 342
    return-object v0
.end method

.method static synthetic d(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/activities/FlipboardActivity;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    return-object v0
.end method

.method static synthetic e(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/activities/FlipboardActivity;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 441
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->m:Z

    if-nez v0, :cond_0

    .line 442
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->w:Lflipboard/gui/section/scrolling/header/SectionCover;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/scrolling/header/SectionCover;->setItem(Lflipboard/service/Section;)V

    .line 443
    new-instance v0, Lflipboard/gui/DynamicViewPagerAdapter;

    invoke-direct {v0}, Lflipboard/gui/DynamicViewPagerAdapter;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->x:Lflipboard/gui/DynamicViewPagerAdapter;

    .line 444
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->x:Lflipboard/gui/DynamicViewPagerAdapter;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->w:Lflipboard/gui/section/scrolling/header/SectionCover;

    invoke-virtual {v0, v1}, Lflipboard/gui/DynamicViewPagerAdapter;->a(Landroid/view/View;)I

    .line 445
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->x:Lflipboard/gui/DynamicViewPagerAdapter;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Lflipboard/gui/DynamicViewPagerAdapter;->a(Landroid/view/View;)I

    .line 446
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->b:Lflipboard/gui/PullToRefreshVerticalViewPager;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->x:Lflipboard/gui/DynamicViewPagerAdapter;

    invoke-virtual {v0, v1}, Lflipboard/gui/PullToRefreshVerticalViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 447
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->m:Z

    .line 449
    :cond_0
    return-void
.end method

.method static synthetic f(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/activities/FlipboardActivity;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 470
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 472
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->q:Lflipboard/service/FLAdManager;

    if-nez v0, :cond_2

    .line 474
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;)Lflipboard/service/FLAdManager;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->q:Lflipboard/service/FLAdManager;

    .line 475
    const/4 v0, 0x1

    .line 478
    :goto_0
    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling;->r:Lflipboard/util/Observer;

    if-nez v2, :cond_0

    .line 481
    new-instance v2, Lflipboard/gui/section/SectionFragmentScrolling$7;

    invoke-direct {v2, p0}, Lflipboard/gui/section/SectionFragmentScrolling$7;-><init>(Lflipboard/gui/section/SectionFragmentScrolling;)V

    iput-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling;->r:Lflipboard/util/Observer;

    .line 501
    :cond_0
    if-eqz v0, :cond_1

    .line 506
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->q:Lflipboard/service/FLAdManager;

    iget v2, p0, Lflipboard/gui/section/SectionFragmentScrolling;->n:I

    invoke-virtual {v0, v2, v1}, Lflipboard/service/FLAdManager;->a(II)V

    .line 509
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method static synthetic g(Lflipboard/gui/section/SectionFragmentScrolling;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->n:I

    return v0
.end method

.method private g()Lflipboard/objs/FeedItem;
    .locals 2

    .prologue
    .line 570
    new-instance v0, Lflipboard/objs/FeedItem;

    invoke-direct {v0}, Lflipboard/objs/FeedItem;-><init>()V

    .line 571
    const-string v1, "ad"

    iput-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 572
    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    .line 573
    return-object v0
.end method

.method static synthetic h(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/service/FLAdManager;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->q:Lflipboard/service/FLAdManager;

    return-object v0
.end method

.method static synthetic i(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/section/scrolling/header/HeaderView;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->e:Lflipboard/gui/section/scrolling/header/HeaderView;

    return-object v0
.end method

.method static synthetic j(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/FLSliderHider;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->f:Lflipboard/gui/FLSliderHider;

    return-object v0
.end method

.method static synthetic k(Lflipboard/gui/section/SectionFragmentScrolling;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->o:I

    return v0
.end method

.method static synthetic l(Lflipboard/gui/section/SectionFragmentScrolling;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->s:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic m(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/swiperefresh/FLSwipeProgressBar;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->g:Lflipboard/swiperefresh/FLSwipeProgressBar;

    return-object v0
.end method

.method static synthetic n(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/swiperefresh/FLSwipeProgressBar;
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragmentScrolling;->c()Lflipboard/swiperefresh/FLSwipeProgressBar;

    move-result-object v0

    return-object v0
.end method

.method static synthetic o(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/section/SectionScrollingAdapter;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->c:Lflipboard/gui/section/SectionScrollingAdapter;

    return-object v0
.end method

.method static synthetic p(Lflipboard/gui/section/SectionFragmentScrolling;)V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->y:Lflipboard/gui/NoContentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/NoContentView;->setLoading(Z)V

    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->y:Lflipboard/gui/NoContentView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/NoContentView;->setVisibility(I)V

    return-void
.end method

.method static synthetic q(Lflipboard/gui/section/SectionFragmentScrolling;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragmentScrolling;->e()V

    return-void
.end method

.method static synthetic r(Lflipboard/gui/section/SectionFragmentScrolling;)V
    .locals 4

    .prologue
    .line 53
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->s:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->s:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->s:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/FLAdManager$AdAsset;

    iget-object v0, v0, Lflipboard/service/FLAdManager$AdAsset;->b:Lflipboard/objs/Ad$Asset;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lflipboard/gui/section/SectionFragmentScrolling;->g()Lflipboard/objs/FeedItem;

    move-result-object v0

    iget-object v3, p0, Lflipboard/gui/section/SectionFragmentScrolling;->c:Lflipboard/gui/section/SectionScrollingAdapter;

    invoke-virtual {v3, v2, v0}, Lflipboard/gui/section/SectionScrollingAdapter;->a(ILflipboard/objs/FeedItem;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 453
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    if-eqz v0, :cond_0

    .line 454
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 455
    :goto_0
    if-eqz v0, :cond_2

    .line 456
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->d(Z)Z

    .line 461
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 454
    goto :goto_0

    .line 458
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    invoke-virtual {v0, v2}, Lflipboard/swiperefresh/SwipeRefreshLayout;->setRefreshing(Z)V

    goto :goto_1
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 53
    check-cast p1, Lflipboard/service/Section;

    check-cast p2, Lflipboard/service/Section$Message;

    sget-object v0, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    if-ne p2, v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/SectionFragmentScrolling$6;

    invoke-direct {v1, p0, p1, p3}, Lflipboard/gui/section/SectionFragmentScrolling$6;-><init>(Lflipboard/gui/section/SectionFragmentScrolling;Lflipboard/service/Section;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/swiperefresh/SwipeRefreshLayout;->setRefreshing(Z)V

    invoke-direct {p0}, Lflipboard/gui/section/SectionFragmentScrolling;->f()V

    :cond_0
    return-void
.end method

.method final b()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 516
    .line 519
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->q:Lflipboard/service/FLAdManager;

    if-nez v0, :cond_0

    .line 520
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragmentScrolling;->f()V

    .line 522
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->q:Lflipboard/service/FLAdManager;

    if-eqz v0, :cond_3

    .line 523
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->q:Lflipboard/service/FLAdManager;

    iget v2, p0, Lflipboard/gui/section/SectionFragmentScrolling;->n:I

    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling;->c:Lflipboard/gui/section/SectionScrollingAdapter;

    invoke-virtual {v2}, Lflipboard/gui/section/SectionScrollingAdapter;->getCount()I

    invoke-virtual {v0}, Lflipboard/service/FLAdManager;->b()Z

    move-result v0

    .line 529
    :goto_0
    if-eqz v0, :cond_2

    .line 533
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->c:Lflipboard/gui/section/SectionScrollingAdapter;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionScrollingAdapter;->getCount()I

    move-result v0

    .line 535
    iget v2, p0, Lflipboard/gui/section/SectionFragmentScrolling;->n:I

    iget v3, p0, Lflipboard/gui/section/SectionFragmentScrolling;->p:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    .line 536
    iget-object v3, p0, Lflipboard/gui/section/SectionFragmentScrolling;->q:Lflipboard/service/FLAdManager;

    invoke-virtual {v3, v2, v0}, Lflipboard/service/FLAdManager;->b(II)Lflipboard/service/FLAdManager$AdAsset;

    move-result-object v2

    .line 537
    if-eqz v2, :cond_2

    iget-object v3, v2, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget v3, v3, Lflipboard/objs/Ad;->i:I

    if-gt v3, v0, :cond_2

    .line 538
    iget-object v3, v2, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget v3, v3, Lflipboard/objs/Ad;->i:I

    .line 539
    sget-object v4, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    .line 541
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->s:Landroid/util/SparseArray;

    invoke-virtual {v0, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 542
    iget-object v0, v2, Lflipboard/service/FLAdManager$AdAsset;->b:Lflipboard/objs/Ad$Asset;

    if-eqz v0, :cond_1

    .line 543
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragmentScrolling;->g()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 544
    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->c:Lflipboard/gui/section/SectionScrollingAdapter;

    invoke-virtual {v1, v3, v0}, Lflipboard/gui/section/SectionScrollingAdapter;->a(ILflipboard/objs/FeedItem;)V

    .line 546
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->s:Landroid/util/SparseArray;

    invoke-virtual {v0, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 549
    :cond_2
    return-void

    .line 525
    :cond_3
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "tryCreateAndInsertAd: unable to init ads %t"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0
.end method

.method public flipUiDone(Lflipboard/gui/FlipFragment$FlipFinishedEvent;)V
    .locals 1
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 360
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->t:Lflipboard/gui/ViewWithDrawerLayout;

    invoke-virtual {v0}, Lflipboard/gui/ViewWithDrawerLayout;->b()V

    .line 361
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 376
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 377
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lflipboard/activities/FlipboardActivity;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    invoke-static {v1, v2, v0}, Lflipboard/util/AndroidUtil;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 378
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 104
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 105
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragmentScrolling;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragmentScrolling;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_section_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 107
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->e(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    .line 108
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragmentScrolling;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_is_tab"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->i:Z

    .line 110
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    invoke-virtual {v0, p0}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    .line 111
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->s:Landroid/util/SparseArray;

    .line 112
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    .line 116
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 117
    new-instance v0, Lflipboard/gui/FLSliderHider;

    invoke-direct {v0}, Lflipboard/gui/FLSliderHider;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->f:Lflipboard/gui/FLSliderHider;

    .line 118
    const v0, 0x7f03010e

    const/4 v1, 0x0

    invoke-static {v7, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ViewWithDrawerLayout;

    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->t:Lflipboard/gui/ViewWithDrawerLayout;

    .line 119
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->t:Lflipboard/gui/ViewWithDrawerLayout;

    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 120
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    invoke-virtual {v0, p0}, Lflipboard/swiperefresh/SwipeRefreshLayout;->setOnRefreshListener(Lflipboard/swiperefresh/SwipeRefreshLayout$OnRefreshListener;)V

    .line 121
    new-instance v0, Lflipboard/gui/SmartScrollingListView;

    invoke-direct {v0, v7}, Lflipboard/gui/SmartScrollingListView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->v:Lflipboard/gui/SmartScrollingListView;

    .line 123
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->v:Lflipboard/gui/SmartScrollingListView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lflipboard/gui/SmartScrollingListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 124
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->v:Lflipboard/gui/SmartScrollingListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/SmartScrollingListView;->setTopOffset(I)V

    .line 125
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, v7}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->u:Landroid/widget/FrameLayout;

    .line 126
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->u:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->v:Lflipboard/gui/SmartScrollingListView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 127
    new-instance v0, Lflipboard/gui/DynamicViewPagerAdapter;

    invoke-direct {v0}, Lflipboard/gui/DynamicViewPagerAdapter;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->x:Lflipboard/gui/DynamicViewPagerAdapter;

    .line 128
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->x:Lflipboard/gui/DynamicViewPagerAdapter;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Lflipboard/gui/DynamicViewPagerAdapter;->a(Landroid/view/View;)I

    .line 129
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->b:Lflipboard/gui/PullToRefreshVerticalViewPager;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->x:Lflipboard/gui/DynamicViewPagerAdapter;

    invoke-virtual {v0, v1}, Lflipboard/gui/PullToRefreshVerticalViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 130
    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->b:Lflipboard/gui/PullToRefreshVerticalViewPager;

    new-instance v2, Lflipboard/gui/PagerTransformer;

    invoke-direct {v2}, Lflipboard/gui/PagerTransformer;-><init>()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_0

    const/4 v3, 0x1

    iget-object v0, v1, Lfr/castorflex/android/verticalviewpager/VerticalViewPager;->a:Landroid/support/v4/view/ViewPager$PageTransformer;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eq v3, v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    iput-object v2, v1, Lfr/castorflex/android/verticalviewpager/VerticalViewPager;->a:Landroid/support/v4/view/ViewPager$PageTransformer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lfr/castorflex/android/verticalviewpager/VerticalViewPager;->setChildrenDrawingOrderEnabledCompat(Z)V

    const/4 v2, 0x2

    iput v2, v1, Lfr/castorflex/android/verticalviewpager/VerticalViewPager;->b:I

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lfr/castorflex/android/verticalviewpager/VerticalViewPager;->b()V

    .line 131
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->b:Lflipboard/gui/PullToRefreshVerticalViewPager;

    new-instance v1, Lflipboard/gui/section/SectionFragmentScrolling$1;

    invoke-direct {v1, p0}, Lflipboard/gui/section/SectionFragmentScrolling$1;-><init>(Lflipboard/gui/section/SectionFragmentScrolling;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/PullToRefreshVerticalViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 145
    const v0, 0x7f030104

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/header/SectionCover;

    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->w:Lflipboard/gui/section/scrolling/header/SectionCover;

    .line 146
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragmentScrolling;->e()V

    .line 147
    new-instance v3, Lflipboard/gui/section/SectionFragmentScrolling$2;

    invoke-direct {v3, p0}, Lflipboard/gui/section/SectionFragmentScrolling$2;-><init>(Lflipboard/gui/section/SectionFragmentScrolling;)V

    .line 223
    invoke-virtual {p0}, Lflipboard/gui/section/SectionFragmentScrolling;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 224
    const v0, 0x7f090122

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 225
    new-instance v0, Lflipboard/gui/section/SectionScrollingAdapter;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    iget-object v4, p0, Lflipboard/gui/section/SectionFragmentScrolling;->v:Lflipboard/gui/SmartScrollingListView;

    iget-object v5, p0, Lflipboard/gui/section/SectionFragmentScrolling;->s:Landroid/util/SparseArray;

    iget-object v6, p0, Lflipboard/gui/section/SectionFragmentScrolling;->q:Lflipboard/service/FLAdManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lflipboard/gui/section/SectionScrollingAdapter;-><init>(Landroid/view/View$OnClickListener;Lflipboard/service/Section;Landroid/view/View$OnClickListener;Lflipboard/gui/SmartScrollingListView;Landroid/util/SparseArray;Lflipboard/service/FLAdManager;)V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->c:Lflipboard/gui/section/SectionScrollingAdapter;

    .line 226
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->v:Lflipboard/gui/SmartScrollingListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/SmartScrollingListView;->setStickyHeaderTopOffset(I)V

    .line 227
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->v:Lflipboard/gui/SmartScrollingListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/SmartScrollingListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 228
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->v:Lflipboard/gui/SmartScrollingListView;

    invoke-virtual {v0, p0}, Lflipboard/gui/SmartScrollingListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 229
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->v:Lflipboard/gui/SmartScrollingListView;

    new-instance v1, Lflipboard/gui/section/SectionFragmentScrolling$3;

    invoke-direct {v1, p0}, Lflipboard/gui/section/SectionFragmentScrolling$3;-><init>(Lflipboard/gui/section/SectionFragmentScrolling;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/SmartScrollingListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 271
    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->i:Z

    if-eqz v0, :cond_4

    .line 272
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    int-to-float v1, v9

    invoke-virtual {v0, v1}, Lflipboard/swiperefresh/SwipeRefreshLayout;->setProgressBarHeight(F)V

    .line 273
    const v0, 0x7f090123

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 274
    sub-int v0, v9, v0

    .line 275
    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->f:Lflipboard/gui/FLSliderHider;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Lflipboard/gui/FLSliderHider;->a(F)V

    .line 276
    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->f:Lflipboard/gui/FLSliderHider;

    new-instance v2, Lflipboard/gui/section/SectionFragmentScrolling$4;

    sget-object v3, Lflipboard/gui/FLSliderHider$ScrollTrackingType;->b:Lflipboard/gui/FLSliderHider$ScrollTrackingType;

    invoke-direct {v2, p0, v3, v9, v0}, Lflipboard/gui/section/SectionFragmentScrolling$4;-><init>(Lflipboard/gui/section/SectionFragmentScrolling;Lflipboard/gui/FLSliderHider$ScrollTrackingType;II)V

    invoke-virtual {v1, v2}, Lflipboard/gui/FLSliderHider;->a(Lflipboard/gui/FLSliderHider$Slider;)V

    .line 283
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    const v1, 0x7f08006a

    const v2, 0x7f08006b

    const v3, 0x7f08006c

    const v4, 0x7f08006d

    invoke-virtual {v0, v1, v2, v3, v4}, Lflipboard/swiperefresh/SwipeRefreshLayout;->a(IIII)V

    .line 288
    :goto_2
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->v:Lflipboard/gui/SmartScrollingListView;

    invoke-virtual {v0}, Lflipboard/gui/SmartScrollingListView;->getWrappedList()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    .line 289
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->v:Lflipboard/gui/SmartScrollingListView;

    invoke-virtual {v0}, Lflipboard/gui/SmartScrollingListView;->getWrappedList()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFooterDividersEnabled(Z)V

    .line 291
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 292
    invoke-direct {p0}, Lflipboard/gui/section/SectionFragmentScrolling;->c()Lflipboard/swiperefresh/FLSwipeProgressBar;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->g:Lflipboard/swiperefresh/FLSwipeProgressBar;

    .line 293
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->v:Lflipboard/gui/SmartScrollingListView;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->g:Lflipboard/swiperefresh/FLSwipeProgressBar;

    invoke-virtual {v0, v1}, Lflipboard/gui/SmartScrollingListView;->a(Landroid/view/View;)V

    .line 296
    :cond_1
    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300de

    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling;->u:Landroid/widget/FrameLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/NoContentView;

    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->y:Lflipboard/gui/NoContentView;

    .line 297
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->y:Lflipboard/gui/NoContentView;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    invoke-virtual {v0, v1}, Lflipboard/gui/NoContentView;->setSection(Lflipboard/service/Section;)V

    .line 298
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->y:Lflipboard/gui/NoContentView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/NoContentView;->setVisibility(I)V

    .line 299
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->u:Landroid/widget/FrameLayout;

    const v1, 0x7f080004

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 300
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->u:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->y:Lflipboard/gui/NoContentView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 301
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->y:Lflipboard/gui/NoContentView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/NoContentView;->setLoading(Z)V

    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->y:Lflipboard/gui/NoContentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/NoContentView;->setVisibility(I)V

    .line 303
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-object v0, v0, Lflipboard/activities/FlipboardActivity;->aa:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->a(Ljava/lang/Object;)V

    .line 304
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->t:Lflipboard/gui/ViewWithDrawerLayout;

    return-object v0

    .line 130
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 285
    :cond_4
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->a:Lflipboard/swiperefresh/SwipeRefreshLayout;

    const v1, 0x7f08000b

    const v2, 0x7f0800aa

    const v3, 0x7f08000b

    const v4, 0x7f08009b

    invoke-virtual {v0, v1, v2, v3, v4}, Lflipboard/swiperefresh/SwipeRefreshLayout;->a(IIII)V

    goto/16 :goto_2
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 347
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onDestroy()V

    .line 348
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    invoke-virtual {v0, p0}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    .line 349
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->q:Lflipboard/service/FLAdManager;

    if-eqz v0, :cond_1

    .line 350
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->r:Lflipboard/util/Observer;

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->q:Lflipboard/service/FLAdManager;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->r:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/FLAdManager;->c(Lflipboard/util/Observer;)V

    .line 352
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->r:Lflipboard/util/Observer;

    .line 354
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->q:Lflipboard/service/FLAdManager;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FLAdManager;->a(IZ)V

    .line 356
    :cond_1
    return-void
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 365
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onDestroyView()V

    .line 366
    iput-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling;->c:Lflipboard/gui/section/SectionScrollingAdapter;

    .line 367
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-object v0, v0, Lflipboard/activities/FlipboardActivity;->aa:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->b(Ljava/lang/Object;)V

    .line 368
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->q:Lflipboard/service/FLAdManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->r:Lflipboard/util/Observer;

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->q:Lflipboard/service/FLAdManager;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling;->r:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/FLAdManager;->c(Lflipboard/util/Observer;)V

    .line 370
    iput-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling;->r:Lflipboard/util/Observer;

    .line 372
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 421
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->c:Lflipboard/gui/section/SectionScrollingAdapter;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionScrollingAdapter;->getCount()I

    move-result v0

    int-to-long v0, v0

    cmp-long v0, p4, v0

    if-gez v0, :cond_0

    .line 424
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling;->c:Lflipboard/gui/section/SectionScrollingAdapter;

    long-to-int v1, p4

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionScrollingAdapter;->a(I)Lflipboard/objs/FeedItem;

    move-result-object v1

    .line 425
    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->U()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 426
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->z()Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->f()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 427
    invoke-virtual {v1, v0}, Lflipboard/objs/FeedItem;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->z()Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 428
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->z()Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    const-string v1, "pauseFromTimeline"

    invoke-virtual {v0, v1}, Lflipboard/service/audio/FLAudioManager;->a(Ljava/lang/String;)V

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 430
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->z()Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    const-string v3, "playFromTimeline"

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/audio/FLAudioManager;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Ljava/lang/String;)V

    goto :goto_0

    .line 432
    :cond_2
    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->ae()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 433
    new-instance v2, Lflipboard/service/Section;

    iget-object v0, v1, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    invoke-direct {v2, v0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    const/4 v1, 0x0

    invoke-static {v2, v0, v1}, Lflipboard/util/SocialHelper;->a(Lflipboard/service/Section;Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0

    .line 434
    :cond_3
    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->Q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 435
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling;->d:Lflipboard/service/Section;

    invoke-static {v0, v2, v1}, Lflipboard/util/AndroidUtil;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    goto :goto_0
.end method
