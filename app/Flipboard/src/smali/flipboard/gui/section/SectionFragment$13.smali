.class Lflipboard/gui/section/SectionFragment$13;
.super Ljava/lang/Object;
.source "SectionFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:I

.field final synthetic c:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

.field final synthetic d:Lflipboard/gui/section/SectionFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment;IILflipboard/gui/flipping/FlipTransitionBase$Direction;)V
    .locals 0

    .prologue
    .line 1923
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$13;->d:Lflipboard/gui/section/SectionFragment;

    iput p2, p0, Lflipboard/gui/section/SectionFragment$13;->a:I

    iput p3, p0, Lflipboard/gui/section/SectionFragment$13;->b:I

    iput-object p4, p0, Lflipboard/gui/section/SectionFragment$13;->c:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 1926
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$13;->d:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1927
    if-eqz v0, :cond_0

    .line 1928
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$13;->d:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->e()Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v0

    .line 1929
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$13;->d:Lflipboard/gui/section/SectionFragment;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v2}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x5

    if-lt v0, v2, :cond_0

    .line 1930
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$13;->d:Lflipboard/gui/section/SectionFragment;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lflipboard/gui/section/SectionFragment;->a(ZZ)V

    .line 1934
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$13;->d:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->j(Lflipboard/gui/section/SectionFragment;)Lflipboard/service/FLAdManager;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1936
    iget v0, p0, Lflipboard/gui/section/SectionFragment$13;->a:I

    if-gt v0, v1, :cond_1

    .line 1939
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$13;->d:Lflipboard/gui/section/SectionFragment;

    iget v2, p0, Lflipboard/gui/section/SectionFragment$13;->b:I

    invoke-virtual {v0, v2}, Lflipboard/gui/section/SectionFragment;->e(I)V

    .line 1944
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$13;->d:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/section/SectionFragment$13$1;

    invoke-direct {v2, p0}, Lflipboard/gui/section/SectionFragment$13$1;-><init>(Lflipboard/gui/section/SectionFragment$13;)V

    invoke-virtual {v0, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 1952
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$13;->d:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->k(Lflipboard/gui/section/SectionFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1953
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$13;->d:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->l(Lflipboard/gui/section/SectionFragment;)I

    move-result v2

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$13;->c:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    sget-object v3, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne v0, v3, :cond_4

    const/4 v0, -0x1

    :goto_0
    add-int/2addr v0, v2

    .line 1954
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$13;->d:Lflipboard/gui/section/SectionFragment;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1955
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v3, "SectionFragment:notify:doMarkAsReadOnFlip"

    new-instance v4, Lflipboard/gui/section/SectionFragment$13$2;

    invoke-direct {v4, p0, v0, v1}, Lflipboard/gui/section/SectionFragment$13$2;-><init>(Lflipboard/gui/section/SectionFragment$13;ILjava/util/List;)V

    invoke-virtual {v2, v3, v4}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 1964
    :cond_3
    return-void

    :cond_4
    move v0, v1

    .line 1953
    goto :goto_0
.end method
