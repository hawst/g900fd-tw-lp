.class public Lflipboard/gui/section/SectionAdPage;
.super Lflipboard/gui/section/SectionPage;
.source "SectionAdPage.java"


# instance fields
.field private x:Lflipboard/objs/Ad;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lflipboard/gui/section/Group;Lflipboard/service/Section;Lflipboard/objs/Ad;Z)V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0, p1, p2, p3, p5}, Lflipboard/gui/section/SectionPage;-><init>(Landroid/content/Context;Lflipboard/gui/section/Group;Lflipboard/service/Section;Z)V

    .line 16
    iput-object p4, p0, Lflipboard/gui/section/SectionAdPage;->x:Lflipboard/objs/Ad;

    .line 18
    iget-object v0, p0, Lflipboard/gui/section/SectionAdPage;->a:Lflipboard/gui/section/SectionHeaderView;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionHeaderView;->getView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 19
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionAdPage;->setIsImagePage(Z)V

    .line 20
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 0

    .prologue
    .line 35
    return-void
.end method

.method public getAd()Lflipboard/objs/Ad;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lflipboard/gui/section/SectionAdPage;->x:Lflipboard/objs/Ad;

    return-object v0
.end method
