.class Lflipboard/gui/section/SectionFragment$30$2;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SectionFragment.java"


# instance fields
.field final synthetic a:Lflipboard/gui/section/SectionFragment$30;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment$30;)V
    .locals 0

    .prologue
    .line 4018
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$30$2;->a:Lflipboard/gui/section/SectionFragment$30;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 4022
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$30$2;->a:Lflipboard/gui/section/SectionFragment$30;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$30;->b:Landroid/view/View;

    check-cast v0, Lflipboard/gui/section/item/PostItem;

    .line 4023
    invoke-virtual {v0}, Lflipboard/gui/section/item/PostItem;->getItemLayout()Lflipboard/gui/section/item/PostItem$ItemLayout;

    move-result-object v0

    .line 4024
    instance-of v1, v0, Lflipboard/gui/section/item/PostItemPhone;

    if-eqz v1, :cond_0

    .line 4025
    check-cast v0, Lflipboard/gui/section/item/PostItemPhone;

    .line 4026
    if-nez p2, :cond_1

    .line 4027
    sget-object v1, Lflipboard/gui/section/item/PostItemPhone$Layout;->d:Lflipboard/gui/section/item/PostItemPhone$Layout;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/item/PostItemPhone;->a(Lflipboard/gui/section/item/PostItemPhone$Layout;)V

    .line 4041
    :cond_0
    :goto_0
    return-void

    .line 4028
    :cond_1
    const/4 v1, 0x1

    if-ne p2, v1, :cond_2

    .line 4029
    sget-object v1, Lflipboard/gui/section/item/PostItemPhone$Layout;->b:Lflipboard/gui/section/item/PostItemPhone$Layout;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/item/PostItemPhone;->a(Lflipboard/gui/section/item/PostItemPhone$Layout;)V

    goto :goto_0

    .line 4030
    :cond_2
    const/4 v1, 0x2

    if-ne p2, v1, :cond_3

    .line 4031
    sget-object v1, Lflipboard/gui/section/item/PostItemPhone$Layout;->c:Lflipboard/gui/section/item/PostItemPhone$Layout;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/item/PostItemPhone;->a(Lflipboard/gui/section/item/PostItemPhone$Layout;)V

    goto :goto_0

    .line 4032
    :cond_3
    const/4 v1, 0x3

    if-ne p2, v1, :cond_4

    .line 4033
    sget-object v1, Lflipboard/gui/section/item/PostItemPhone$Layout;->e:Lflipboard/gui/section/item/PostItemPhone$Layout;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/item/PostItemPhone;->a(Lflipboard/gui/section/item/PostItemPhone$Layout;)V

    goto :goto_0

    .line 4034
    :cond_4
    const/4 v1, 0x4

    if-ne p2, v1, :cond_5

    .line 4035
    sget-object v1, Lflipboard/gui/section/item/PostItemPhone$Layout;->a:Lflipboard/gui/section/item/PostItemPhone$Layout;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/item/PostItemPhone;->a(Lflipboard/gui/section/item/PostItemPhone$Layout;)V

    goto :goto_0

    .line 4036
    :cond_5
    const/4 v1, 0x5

    if-ne p2, v1, :cond_0

    .line 4037
    iput-boolean v2, v0, Lflipboard/gui/section/item/PostItemPhone;->o:Z

    iget-object v1, v0, Lflipboard/gui/section/item/PostItemPhone;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1, v3, v2, v2}, Lflipboard/gui/FLStaticTextView;->a(Lflipboard/gui/FLStaticTextView$BlockType;II)V

    iget-object v1, v0, Lflipboard/gui/section/item/PostItemPhone;->e:Lflipboard/gui/FLImageViewGroup;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageViewGroup;->setVisibility(I)V

    iput-object v3, v0, Lflipboard/gui/section/item/PostItemPhone;->m:Lflipboard/gui/section/item/PostItemPhone$Layout;

    iget-object v0, v0, Lflipboard/gui/section/item/PostItemPhone;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    goto :goto_0
.end method
