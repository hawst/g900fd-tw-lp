.class public abstract Lflipboard/gui/section/AttributionBase;
.super Landroid/view/ViewGroup;
.source "AttributionBase.java"

# interfaces
.implements Lflipboard/gui/section/Attribution;
.implements Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;


# instance fields
.field protected a:Lflipboard/activities/FlipboardActivity;

.field protected b:Lflipboard/service/Section;

.field protected c:Z

.field protected d:Lflipboard/objs/FeedItem;

.field protected e:Lflipboard/objs/FeedItem;

.field protected final f:I

.field protected final g:I

.field protected h:Lflipboard/gui/FLTextView;

.field protected i:Lflipboard/gui/FLImageView;

.field protected j:Lflipboard/gui/FLImageView;

.field protected k:Lflipboard/gui/section/AttributionButtonWithText;

.field protected l:Lflipboard/gui/section/AttributionButtonWithText;

.field protected m:Lflipboard/gui/section/AttributionButtonWithText;

.field protected n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field protected o:Z

.field protected p:Z

.field protected q:Z

.field protected r:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 70
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionBase;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/AttributionBase;->g:I

    .line 71
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionBase;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900f9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/AttributionBase;->f:I

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 76
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionBase;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/AttributionBase;->g:I

    .line 77
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionBase;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900f9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/AttributionBase;->f:I

    .line 78
    return-void
.end method

.method protected static a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 105
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 106
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Landroid/view/View;->measure(II)V

    .line 107
    return-void
.end method


# virtual methods
.method protected final a(IILjava/util/List;)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 88
    const/4 v0, 0x0

    .line 90
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 91
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    .line 92
    goto :goto_0

    .line 93
    :cond_0
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, p1

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 94
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_2

    .line 95
    div-int/lit8 v4, v1, 0x2

    add-int/2addr v4, p2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    .line 96
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v2

    .line 97
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v4

    invoke-virtual {v0, v2, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 98
    iget v0, p0, Lflipboard/gui/section/AttributionBase;->f:I

    add-int/2addr v0, v5

    :goto_2
    move v2, v0

    .line 100
    goto :goto_1

    .line 101
    :cond_1
    return v1

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method protected a(Lflipboard/objs/CommentaryResult$Item;)V
    .locals 2

    .prologue
    .line 154
    if-eqz p1, :cond_2

    .line 155
    iget-object v0, p0, Lflipboard/gui/section/AttributionBase;->l:Lflipboard/gui/section/AttributionButtonWithText;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lflipboard/gui/section/AttributionBase;->l:Lflipboard/gui/section/AttributionButtonWithText;

    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->b:I

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionButtonWithText;->setNumber(I)V

    .line 158
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/AttributionBase;->k:Lflipboard/gui/section/AttributionButtonWithText;

    if-eqz v0, :cond_1

    .line 159
    iget-object v0, p0, Lflipboard/gui/section/AttributionBase;->k:Lflipboard/gui/section/AttributionButtonWithText;

    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->c:I

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionButtonWithText;->setNumber(I)V

    .line 161
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/AttributionBase;->m:Lflipboard/gui/section/AttributionButtonWithText;

    if-eqz v0, :cond_2

    .line 162
    iget-object v0, p0, Lflipboard/gui/section/AttributionBase;->m:Lflipboard/gui/section/AttributionButtonWithText;

    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->d:I

    invoke-virtual {v0, v1}, Lflipboard/gui/section/AttributionButtonWithText;->setNumber(I)V

    .line 166
    :cond_2
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionBase;->c()V

    .line 167
    return-void
.end method

.method public final a(Lflipboard/objs/HasCommentaryItem;)V
    .locals 2

    .prologue
    .line 144
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/AttributionBase$1;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/section/AttributionBase$1;-><init>(Lflipboard/gui/section/AttributionBase;Lflipboard/objs/HasCommentaryItem;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    .line 151
    return-void
.end method

.method protected c()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 111
    iget-object v0, p0, Lflipboard/gui/section/AttributionBase;->e:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lflipboard/gui/section/AttributionBase;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    if-eqz v0, :cond_9

    .line 112
    iget-object v0, p0, Lflipboard/gui/section/AttributionBase;->e:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    iget v0, v0, Lflipboard/objs/CommentaryResult$Item;->c:I

    if-lez v0, :cond_8

    move v0, v1

    .line 116
    :goto_0
    iget-object v3, p0, Lflipboard/gui/section/AttributionBase;->e:Lflipboard/objs/FeedItem;

    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->ai()Ljava/lang/String;

    move-result-object v3

    .line 117
    if-nez v3, :cond_0

    .line 118
    const-string v3, "flipboard"

    .line 120
    :cond_0
    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v4, v3}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v3

    .line 121
    iget-boolean v4, p0, Lflipboard/gui/section/AttributionBase;->r:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lflipboard/gui/section/AttributionBase;->e:Lflipboard/objs/FeedItem;

    iget-boolean v4, v4, Lflipboard/objs/FeedItem;->af:Z

    iget-boolean v5, p0, Lflipboard/gui/section/AttributionBase;->o:Z

    if-ne v4, v5, :cond_1

    iget-boolean v4, p0, Lflipboard/gui/section/AttributionBase;->p:Z

    if-ne v4, v0, :cond_1

    iget-boolean v4, p0, Lflipboard/gui/section/AttributionBase;->q:Z

    iget-boolean v5, p0, Lflipboard/gui/section/AttributionBase;->c:Z

    if-eq v4, v5, :cond_3

    .line 122
    :cond_1
    iget-object v4, p0, Lflipboard/gui/section/AttributionBase;->e:Lflipboard/objs/FeedItem;

    iget-boolean v4, v4, Lflipboard/objs/FeedItem;->af:Z

    iput-boolean v4, p0, Lflipboard/gui/section/AttributionBase;->o:Z

    .line 123
    iget-object v4, p0, Lflipboard/gui/section/AttributionBase;->k:Lflipboard/gui/section/AttributionButtonWithText;

    if-eqz v4, :cond_2

    .line 124
    iget-object v4, p0, Lflipboard/gui/section/AttributionBase;->k:Lflipboard/gui/section/AttributionButtonWithText;

    sget-object v5, Lflipboard/gui/section/AttributionButtonWithText$Type;->b:Lflipboard/gui/section/AttributionButtonWithText$Type;

    iget-boolean v6, p0, Lflipboard/gui/section/AttributionBase;->o:Z

    iget-boolean v7, p0, Lflipboard/gui/section/AttributionBase;->c:Z

    invoke-virtual {v4, v3, v5, v6, v7}, Lflipboard/gui/section/AttributionButtonWithText;->a(Lflipboard/objs/ConfigService;Lflipboard/gui/section/AttributionButtonWithText$Type;ZZ)V

    .line 126
    :cond_2
    iput-boolean v0, p0, Lflipboard/gui/section/AttributionBase;->p:Z

    .line 128
    :cond_3
    iget-boolean v0, p0, Lflipboard/gui/section/AttributionBase;->r:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lflipboard/gui/section/AttributionBase;->q:Z

    iget-boolean v4, p0, Lflipboard/gui/section/AttributionBase;->c:Z

    if-eq v0, v4, :cond_7

    .line 129
    :cond_4
    iget-object v0, p0, Lflipboard/gui/section/AttributionBase;->l:Lflipboard/gui/section/AttributionButtonWithText;

    if-eqz v0, :cond_5

    .line 130
    iget-object v0, p0, Lflipboard/gui/section/AttributionBase;->l:Lflipboard/gui/section/AttributionButtonWithText;

    sget-object v4, Lflipboard/gui/section/AttributionButtonWithText$Type;->a:Lflipboard/gui/section/AttributionButtonWithText$Type;

    iget-boolean v5, p0, Lflipboard/gui/section/AttributionBase;->c:Z

    invoke-virtual {v0, v3, v4, v2, v5}, Lflipboard/gui/section/AttributionButtonWithText;->a(Lflipboard/objs/ConfigService;Lflipboard/gui/section/AttributionButtonWithText$Type;ZZ)V

    .line 132
    :cond_5
    iget-object v0, p0, Lflipboard/gui/section/AttributionBase;->m:Lflipboard/gui/section/AttributionButtonWithText;

    if-eqz v0, :cond_6

    .line 133
    iget-object v0, p0, Lflipboard/gui/section/AttributionBase;->m:Lflipboard/gui/section/AttributionButtonWithText;

    sget-object v4, Lflipboard/gui/section/AttributionButtonWithText$Type;->c:Lflipboard/gui/section/AttributionButtonWithText$Type;

    iget-boolean v5, p0, Lflipboard/gui/section/AttributionBase;->c:Z

    invoke-virtual {v0, v3, v4, v2, v5}, Lflipboard/gui/section/AttributionButtonWithText;->a(Lflipboard/objs/ConfigService;Lflipboard/gui/section/AttributionButtonWithText$Type;ZZ)V

    .line 136
    :cond_6
    iget-object v2, p0, Lflipboard/gui/section/AttributionBase;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/AttributionBase;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-boolean v0, p0, Lflipboard/gui/section/AttributionBase;->c:Z

    if-eqz v0, :cond_a

    const v0, 0x7f0800aa

    :goto_1
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    .line 138
    :cond_7
    iput-boolean v1, p0, Lflipboard/gui/section/AttributionBase;->r:Z

    .line 139
    iget-boolean v0, p0, Lflipboard/gui/section/AttributionBase;->c:Z

    iput-boolean v0, p0, Lflipboard/gui/section/AttributionBase;->q:Z

    .line 140
    return-void

    :cond_8
    move v0, v2

    .line 112
    goto :goto_0

    :cond_9
    move v0, v2

    .line 114
    goto :goto_0

    .line 136
    :cond_a
    const v0, 0x7f08009b

    goto :goto_1
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 171
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 172
    iget-object v0, p0, Lflipboard/gui/section/AttributionBase;->d:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lflipboard/gui/section/AttributionBase;->d:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedItem;->a(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 175
    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 179
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 180
    iget-object v0, p0, Lflipboard/gui/section/AttributionBase;->d:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lflipboard/gui/section/AttributionBase;->d:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedItem;->b(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 183
    :cond_0
    return-void
.end method

.method public setDrawTopDivider(Z)V
    .locals 0

    .prologue
    .line 82
    return-void
.end method
