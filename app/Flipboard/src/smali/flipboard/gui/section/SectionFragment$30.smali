.class Lflipboard/gui/section/SectionFragment$30;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SectionFragment.java"


# instance fields
.field final synthetic a:Lflipboard/gui/flipping/FlipTransitionBase;

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Lflipboard/gui/section/SectionFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment;Lflipboard/gui/flipping/FlipTransitionBase;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 3982
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$30;->c:Lflipboard/gui/section/SectionFragment;

    iput-object p2, p0, Lflipboard/gui/section/SectionFragment$30;->a:Lflipboard/gui/flipping/FlipTransitionBase;

    iput-object p3, p0, Lflipboard/gui/section/SectionFragment$30;->b:Landroid/view/View;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;I)V
    .locals 10

    .prologue
    const/4 v7, 0x4

    const/4 v5, 0x3

    const/4 v3, 0x2

    const/4 v9, 0x0

    const/4 v6, 0x1

    .line 3986
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$30;->a:Lflipboard/gui/flipping/FlipTransitionBase;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionBase;->getCurrentViewIndex()I

    move-result v1

    .line 3987
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$30;->c:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lflipboard/gui/section/Group;

    .line 3988
    if-nez p2, :cond_3

    .line 3989
    if-lez v1, :cond_2

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$30;->c:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0, v9, v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 3990
    :goto_0
    sget-object v3, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    .line 3991
    iget-object v0, v4, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    if-eqz v0, :cond_0

    .line 3992
    iget-object v0, v4, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 3994
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$30;->c:Lflipboard/gui/section/SectionFragment;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v4, v4, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v4, p0, Lflipboard/gui/section/SectionFragment$30;->c:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v4}, Lflipboard/gui/section/SectionFragment;->g()I

    move-result v4

    iget-object v5, p0, Lflipboard/gui/section/SectionFragment$30;->c:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v5}, Lflipboard/gui/section/SectionFragment;->h()I

    move-result v5

    invoke-virtual/range {v0 .. v6}, Lflipboard/gui/section/SectionFragment;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;IIZ)Lflipboard/gui/section/Group;

    .line 4052
    :cond_1
    :goto_1
    return-void

    .line 3989
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    goto :goto_0

    .line 3995
    :cond_3
    if-ne p2, v6, :cond_4

    .line 3996
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$30;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lflipboard/objs/FeedItem;

    .line 3997
    iget-object v0, v4, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 3998
    if-ltz v0, :cond_1

    .line 3999
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v1, "current score: "

    invoke-direct {v8, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$30;->a:Lflipboard/gui/flipping/FlipTransitionBase;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionBase;->getWidth()I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$30;->a:Lflipboard/gui/flipping/FlipTransitionBase;

    invoke-virtual {v2}, Lflipboard/gui/flipping/FlipTransitionBase;->getHeight()I

    move-result v2

    iget-object v3, v4, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v3, v6}, Lflipboard/objs/SectionPageTemplate;->a(Z)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflipboard/objs/SectionPageTemplate$Area;

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$30;->c:Lflipboard/gui/section/SectionFragment;

    iget-object v4, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-static/range {v1 .. v6}, Lflipboard/gui/section/Group;->a(IILflipboard/objs/SectionPageTemplate$Area;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Z)I

    move-result v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 4000
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v1, "1up score: "

    invoke-direct {v7, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$30;->a:Lflipboard/gui/flipping/FlipTransitionBase;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionBase;->getWidth()I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$30;->a:Lflipboard/gui/flipping/FlipTransitionBase;

    invoke-virtual {v2}, Lflipboard/gui/flipping/FlipTransitionBase;->getHeight()I

    move-result v2

    invoke-static {}, Lflipboard/gui/section/SectionFragment;->k()Lflipboard/objs/SectionPageTemplate;

    move-result-object v3

    invoke-virtual {v3, v6}, Lflipboard/objs/SectionPageTemplate;->a(Z)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflipboard/objs/SectionPageTemplate$Area;

    iget-object v4, p0, Lflipboard/gui/section/SectionFragment$30;->c:Lflipboard/gui/section/SectionFragment;

    iget-object v4, v4, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-static/range {v1 .. v6}, Lflipboard/gui/section/Group;->a(IILflipboard/objs/SectionPageTemplate$Area;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Z)I

    move-result v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 4002
    :cond_4
    if-ne p2, v3, :cond_5

    .line 4003
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$30;->a:Lflipboard/gui/flipping/FlipTransitionBase;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionBase;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 4004
    invoke-virtual {v0}, Landroid/view/ViewGroup;->forceLayout()V

    .line 4005
    new-instance v1, Lflipboard/gui/section/SectionFragment$30$1;

    invoke-direct {v1, p0}, Lflipboard/gui/section/SectionFragment$30$1;-><init>(Lflipboard/gui/section/SectionFragment$30;)V

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/ViewGroup;Lflipboard/util/Callback;)V

    .line 4011
    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 4012
    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    goto/16 :goto_1

    .line 4013
    :cond_5
    if-ne p2, v5, :cond_7

    .line 4014
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$30;->b:Landroid/view/View;

    instance-of v0, v0, Lflipboard/gui/section/item/PostItem;

    if-eqz v0, :cond_6

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4015
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 4016
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "Image top"

    aput-object v2, v1, v9

    const-string v2, "Image right"

    aput-object v2, v1, v6

    const-string v2, "Image right (full height)"

    aput-object v2, v1, v3

    const-string v2, "Full bleed"

    aput-object v2, v1, v5

    const-string v2, "No Image"

    aput-object v2, v1, v7

    const/4 v2, 0x5

    const-string v3, "Reset"

    aput-object v3, v1, v2

    iput-object v1, v0, Lflipboard/gui/dialog/FLAlertDialogFragment;->q:[Ljava/lang/CharSequence;

    .line 4017
    new-instance v1, Lflipboard/gui/section/SectionFragment$30$2;

    invoke-direct {v1, p0}, Lflipboard/gui/section/SectionFragment$30$2;-><init>(Lflipboard/gui/section/SectionFragment$30;)V

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 4043
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$30;->c:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v1}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v1, v1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "debug_pick_layout"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 4045
    :cond_6
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$30;->c:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->w(Lflipboard/gui/section/SectionFragment;)Lflipboard/activities/FlipboardActivity;

    move-result-object v0

    const-string v1, "Sorry, only works for post items and phone"

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 4047
    :cond_7
    if-ne p2, v7, :cond_8

    .line 4048
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$30;->c:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->x(Lflipboard/gui/section/SectionFragment;)Z

    goto/16 :goto_1

    .line 4050
    :cond_8
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$30;->c:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$30;->c:Lflipboard/gui/section/SectionFragment;

    iget-object v2, v1, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$30;->b:Landroid/view/View;

    check-cast v1, Lflipboard/gui/item/TabletItem;

    invoke-interface {v1}, Lflipboard/gui/item/TabletItem;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v1

    sget-object v3, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->a:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    invoke-static {v0, v2, v1, v3}, Lflipboard/util/SocialHelper;->b(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UsageEventV2$FlipItemNavFrom;)V

    goto/16 :goto_1
.end method
