.class public Lflipboard/gui/section/ProfileFragmentScrolling$$ViewInjector;
.super Ljava/lang/Object;
.source "ProfileFragmentScrolling$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/section/ProfileFragmentScrolling;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a02a9

    const-string v1, "field \'headerView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    iput-object v0, p1, Lflipboard/gui/section/ProfileFragmentScrolling;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    .line 12
    const v0, 0x7f0a034a

    const-string v1, "method \'onClickFindFriends\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    new-instance v1, Lflipboard/gui/section/ProfileFragmentScrolling$$ViewInjector$1;

    invoke-direct {v1, p1}, Lflipboard/gui/section/ProfileFragmentScrolling$$ViewInjector$1;-><init>(Lflipboard/gui/section/ProfileFragmentScrolling;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 21
    const v0, 0x7f0a034b

    const-string v1, "method \'onClickSettings\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 22
    new-instance v1, Lflipboard/gui/section/ProfileFragmentScrolling$$ViewInjector$2;

    invoke-direct {v1, p1}, Lflipboard/gui/section/ProfileFragmentScrolling$$ViewInjector$2;-><init>(Lflipboard/gui/section/ProfileFragmentScrolling;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    return-void
.end method

.method public static reset(Lflipboard/gui/section/ProfileFragmentScrolling;)V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/section/ProfileFragmentScrolling;->a:Lflipboard/gui/section/scrolling/header/ProfileHeaderView;

    .line 34
    return-void
.end method
