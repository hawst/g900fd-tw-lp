.class Lflipboard/gui/section/SectionFragment$2$4;
.super Ljava/lang/Object;
.source "SectionFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/gui/section/SectionFragment$2;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment$2;)V
    .locals 0

    .prologue
    .line 495
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$2$4;->a:Lflipboard/gui/section/SectionFragment$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 499
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2$4;->a:Lflipboard/gui/section/SectionFragment$2;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->e()Lflipboard/gui/flipping/FlipTransitionViews;

    move-result-object v0

    .line 500
    if-eqz v0, :cond_5

    .line 501
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getFlippableViews()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    .line 502
    invoke-virtual {v0}, Lflipboard/gui/flipping/FlippingContainer;->getChild()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/SectionPage;

    .line 503
    invoke-virtual {v0}, Lflipboard/gui/section/SectionPage;->i()V

    goto :goto_0

    .line 506
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2$4;->a:Lflipboard/gui/section/SectionFragment$2;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->m:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->clear()V

    .line 508
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2$4;->a:Lflipboard/gui/section/SectionFragment$2;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    move v2, v3

    move v4, v3

    .line 511
    :goto_2
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$2$4;->a:Lflipboard/gui/section/SectionFragment$2;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1}, Lflipboard/gui/section/ListSingleThreadWrapper;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    if-nez v4, :cond_3

    .line 512
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$2$4;->a:Lflipboard/gui/section/SectionFragment$2;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->f:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1, v2}, Lflipboard/gui/section/ListSingleThreadWrapper;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/section/Group;

    .line 513
    iget-object v6, v1, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    if-eqz v6, :cond_2

    iget-object v1, v1, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    invoke-virtual {v1, v0}, Lflipboard/objs/SidebarGroup;->a(Lflipboard/objs/SidebarGroup;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 514
    const/4 v4, 0x1

    .line 516
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    .line 517
    goto :goto_2

    .line 518
    :cond_3
    if-nez v4, :cond_1

    .line 519
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$2$4;->a:Lflipboard/gui/section/SectionFragment$2;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->m:Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-virtual {v1, v0}, Lflipboard/gui/section/ListSingleThreadWrapper;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 522
    :cond_4
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$2$4;->a:Lflipboard/gui/section/SectionFragment$2;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$2$4;->a:Lflipboard/gui/section/SectionFragment$2;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$2;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->m:Lflipboard/gui/section/ListSingleThreadWrapper;

    const-string v2, "pageboxGrid"

    invoke-static {v0, v1, v2}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/gui/section/SectionFragment;Ljava/util/List;Ljava/lang/String;)Z

    .line 524
    :cond_5
    return-void
.end method
