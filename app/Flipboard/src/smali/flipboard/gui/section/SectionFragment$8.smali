.class Lflipboard/gui/section/SectionFragment$8;
.super Lflipboard/service/FlCrashListener;
.source "SectionFragment.java"


# instance fields
.field final synthetic a:Lflipboard/io/RequestLogEntry;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Ljava/util/List;

.field final synthetic d:Ljava/util/List;

.field final synthetic e:Lflipboard/gui/section/SectionFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment;Lflipboard/io/RequestLogEntry;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 903
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$8;->e:Lflipboard/gui/section/SectionFragment;

    iput-object p2, p0, Lflipboard/gui/section/SectionFragment$8;->a:Lflipboard/io/RequestLogEntry;

    iput-object p3, p0, Lflipboard/gui/section/SectionFragment$8;->b:Ljava/util/List;

    iput-object p4, p0, Lflipboard/gui/section/SectionFragment$8;->c:Ljava/util/List;

    iput-object p5, p0, Lflipboard/gui/section/SectionFragment$8;->d:Ljava/util/List;

    invoke-direct {p0}, Lflipboard/service/FlCrashListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0xa

    const/4 v2, 0x0

    .line 907
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-super {p0}, Lflipboard/service/FlCrashListener;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 908
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$8;->a:Lflipboard/io/RequestLogEntry;

    if-eqz v0, :cond_0

    .line 909
    const-string v0, "\n\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$8;->a:Lflipboard/io/RequestLogEntry;

    invoke-virtual {v1}, Lflipboard/io/RequestLogEntry;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 911
    :cond_0
    const-string v0, "\n\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 912
    const-string v0, "Known items:\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 914
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$8;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 915
    const-string v5, "Item "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 916
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 917
    goto :goto_0

    .line 919
    :cond_1
    const-string v0, "\n\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 920
    const-string v0, "Items in section:\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 921
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$8;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 922
    const-string v5, "Item "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 923
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 924
    goto :goto_1

    .line 925
    :cond_2
    const-string v0, "\n\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 927
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$8;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    .line 928
    const-string v4, "Group "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lflipboard/gui/section/Group;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 929
    add-int/lit8 v2, v2, 0x1

    .line 930
    goto :goto_2

    .line 932
    :cond_3
    const-string v0, "\n\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 933
    const-string v0, "Duplicate occurence log:\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 934
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$8;->e:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 935
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$8;->e:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 937
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
