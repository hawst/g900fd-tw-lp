.class Lflipboard/gui/section/SectionFragment$9;
.super Ljava/lang/Object;
.source "SectionFragment.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/activities/FlipboardActivity;

.field final synthetic b:Lflipboard/objs/Invite;

.field final synthetic c:Lflipboard/gui/section/SectionFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/Invite;)V
    .locals 0

    .prologue
    .line 955
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$9;->c:Lflipboard/gui/section/SectionFragment;

    iput-object p2, p0, Lflipboard/gui/section/SectionFragment$9;->a:Lflipboard/activities/FlipboardActivity;

    iput-object p3, p0, Lflipboard/gui/section/SectionFragment$9;->b:Lflipboard/objs/Invite;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 970
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 971
    iput-object p2, v0, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 972
    if-lez p1, :cond_0

    .line 973
    invoke-virtual {v0, p1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 975
    :cond_0
    const v1, 0x7f0d023f

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 976
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$9;->c:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/section/SectionFragment$9$1;

    invoke-direct {v2, p0, v0}, Lflipboard/gui/section/SectionFragment$9$1;-><init>(Lflipboard/gui/section/SectionFragment$9;Lflipboard/gui/dialog/FLAlertDialogFragment;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 985
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 959
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$9;->c:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->r()V

    .line 960
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$9;->a:Lflipboard/activities/FlipboardActivity;

    const v1, 0x7f0d000f

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$9;->b:Lflipboard/objs/Invite;

    iget-object v2, v2, Lflipboard/objs/Invite;->i:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v3, v0}, Lflipboard/gui/section/SectionFragment$9;->a(ILjava/lang/String;)V

    .line 961
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 965
    const v0, 0x7f0d000c

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$9;->a:Lflipboard/activities/FlipboardActivity;

    const v2, 0x7f0d000b

    invoke-virtual {v1, v2}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lflipboard/gui/section/SectionFragment$9;->a(ILjava/lang/String;)V

    .line 966
    return-void
.end method
