.class public Lflipboard/gui/section/AttributionButtonWithText;
.super Landroid/widget/LinearLayout;
.source "AttributionButtonWithText.java"


# instance fields
.field public a:Lflipboard/gui/FLCameleonImageView;

.field public b:Lflipboard/gui/FLLabelTextView;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/objs/ConfigService;Lflipboard/gui/section/AttributionButtonWithText$Type;ZZ)V
    .locals 3

    .prologue
    .line 39
    sget-object v0, Lflipboard/gui/section/AttributionButtonWithText$1;->a:[I

    invoke-virtual {p2}, Lflipboard/gui/section/AttributionButtonWithText$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 45
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/AttributionButtonWithText;->a:Lflipboard/gui/FLCameleonImageView;

    invoke-static {v0, p4, p3}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLCameleonImageView;ZZ)V

    .line 46
    if-eqz p3, :cond_0

    .line 47
    iget-object v0, p0, Lflipboard/gui/section/AttributionButtonWithText;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/AttributionButtonWithText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080054

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setTextColor(I)V

    .line 53
    :goto_1
    return-void

    .line 40
    :pswitch_0
    iget-object v0, p0, Lflipboard/gui/section/AttributionButtonWithText;->a:Lflipboard/gui/FLCameleonImageView;

    invoke-static {p1}, Lflipboard/util/AndroidUtil;->f(Lflipboard/objs/ConfigService;)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    goto :goto_0

    .line 41
    :pswitch_1
    iget-object v0, p0, Lflipboard/gui/section/AttributionButtonWithText;->a:Lflipboard/gui/FLCameleonImageView;

    iget v1, p0, Lflipboard/gui/section/AttributionButtonWithText;->c:I

    invoke-static {p1}, Lflipboard/util/AndroidUtil;->e(Lflipboard/objs/ConfigService;)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    goto :goto_0

    .line 42
    :pswitch_2
    iget-object v0, p0, Lflipboard/gui/section/AttributionButtonWithText;->a:Lflipboard/gui/FLCameleonImageView;

    invoke-static {p1}, Lflipboard/util/AndroidUtil;->g(Lflipboard/objs/ConfigService;)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    goto :goto_0

    .line 43
    :pswitch_3
    iget-object v0, p0, Lflipboard/gui/section/AttributionButtonWithText;->a:Lflipboard/gui/FLCameleonImageView;

    invoke-static {}, Lflipboard/util/AndroidUtil;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    goto :goto_0

    .line 48
    :cond_0
    if-eqz p4, :cond_1

    .line 49
    iget-object v0, p0, Lflipboard/gui/section/AttributionButtonWithText;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/AttributionButtonWithText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800aa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setTextColor(I)V

    goto :goto_1

    .line 51
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/AttributionButtonWithText;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {p0}, Lflipboard/gui/section/AttributionButtonWithText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080038

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setTextColor(I)V

    goto :goto_1

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 33
    const v0, 0x7f0a0040

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionButtonWithText;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLCameleonImageView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionButtonWithText;->a:Lflipboard/gui/FLCameleonImageView;

    .line 34
    const v0, 0x7f0a0041

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionButtonWithText;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/section/AttributionButtonWithText;->b:Lflipboard/gui/FLLabelTextView;

    .line 35
    return-void
.end method

.method public setNumber(I)V
    .locals 2

    .prologue
    .line 57
    iput p1, p0, Lflipboard/gui/section/AttributionButtonWithText;->c:I

    .line 58
    if-lez p1, :cond_0

    .line 59
    iget-object v0, p0, Lflipboard/gui/section/AttributionButtonWithText;->b:Lflipboard/gui/FLLabelTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 60
    iget-object v0, p0, Lflipboard/gui/section/AttributionButtonWithText;->b:Lflipboard/gui/FLLabelTextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/AttributionButtonWithText;->b:Lflipboard/gui/FLLabelTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    goto :goto_0
.end method
