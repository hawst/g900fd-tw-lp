.class Lflipboard/gui/section/ProfileFragmentScrolling$SectionsAndAccountObserver;
.super Ljava/lang/Object;
.source "ProfileFragmentScrolling.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/FlipboardManager;",
        "Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/gui/section/ProfileFragmentScrolling;


# direct methods
.method private constructor <init>(Lflipboard/gui/section/ProfileFragmentScrolling;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lflipboard/gui/section/ProfileFragmentScrolling$SectionsAndAccountObserver;->a:Lflipboard/gui/section/ProfileFragmentScrolling;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflipboard/gui/section/ProfileFragmentScrolling;B)V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0, p1}, Lflipboard/gui/section/ProfileFragmentScrolling$SectionsAndAccountObserver;-><init>(Lflipboard/gui/section/ProfileFragmentScrolling;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 169
    check-cast p2, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    sget-object v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->e:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    if-ne p2, v0, :cond_1

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v3, Lflipboard/gui/section/ProfileFragmentScrolling$SectionsAndAccountObserver$1;

    invoke-direct {v3, p0, v1, v0}, Lflipboard/gui/section/ProfileFragmentScrolling$SectionsAndAccountObserver$1;-><init>(Lflipboard/gui/section/ProfileFragmentScrolling$SectionsAndAccountObserver;Lflipboard/service/Account;Lflipboard/service/User;)V

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->f:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/ProfileFragmentScrolling$SectionsAndAccountObserver;->a:Lflipboard/gui/section/ProfileFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/ProfileFragmentScrolling;->b(Lflipboard/gui/section/ProfileFragmentScrolling;)V

    goto :goto_0
.end method
