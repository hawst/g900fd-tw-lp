.class Lflipboard/gui/section/MagazineConversationThreadDetailFragment$4;
.super Ljava/lang/Object;
.source "MagazineConversationThreadDetailFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$4;->a:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 220
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$4;->a:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->c(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 221
    :goto_0
    iget-object v2, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$4;->a:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-static {v2}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->d(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 222
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadDetailFragment$4;->a:Lflipboard/gui/section/MagazineConversationThreadDetailFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadDetailFragment;->d(Lflipboard/gui/section/MagazineConversationThreadDetailFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 223
    return-void

    :cond_0
    move v0, v1

    .line 220
    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 216
    return-void
.end method
