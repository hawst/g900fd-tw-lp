.class Lflipboard/gui/section/UserListFragment$UserListAdapter$2$1;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "UserListFragment.java"


# instance fields
.field final synthetic a:Lflipboard/gui/section/UserListFragment$UserListAdapter$2;


# direct methods
.method constructor <init>(Lflipboard/gui/section/UserListFragment$UserListAdapter$2;)V
    .locals 0

    .prologue
    .line 359
    iput-object p1, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter$2$1;->a:Lflipboard/gui/section/UserListFragment$UserListAdapter$2;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;)V
    .locals 3

    .prologue
    .line 396
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter$2$1;->a:Lflipboard/gui/section/UserListFragment$UserListAdapter$2;

    iget-object v0, v0, Lflipboard/gui/section/UserListFragment$UserListAdapter$2;->b:Lflipboard/gui/section/UserListFragment$UserListAdapter;

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a(Lflipboard/gui/section/UserListFragment$UserListAdapter;ZI)V

    .line 397
    return-void
.end method

.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 363
    iget-object v1, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter$2$1;->a:Lflipboard/gui/section/UserListFragment$UserListAdapter$2;

    iget-object v1, v1, Lflipboard/gui/section/UserListFragment$UserListAdapter$2;->b:Lflipboard/gui/section/UserListFragment$UserListAdapter;

    iget-object v2, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter$2$1;->a:Lflipboard/gui/section/UserListFragment$UserListAdapter$2;

    iget v2, v2, Lflipboard/gui/section/UserListFragment$UserListAdapter$2;->a:I

    invoke-virtual {v1, v2}, Lflipboard/gui/section/UserListFragment$UserListAdapter;->b(I)J

    move-result-wide v2

    .line 365
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    .line 366
    :goto_0
    iget-object v5, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter$2$1;->a:Lflipboard/gui/section/UserListFragment$UserListAdapter$2;

    iget-object v5, v5, Lflipboard/gui/section/UserListFragment$UserListAdapter$2;->b:Lflipboard/gui/section/UserListFragment$UserListAdapter;

    invoke-virtual {v5}, Lflipboard/gui/section/UserListFragment$UserListAdapter;->getCount()I

    move-result v5

    if-ge v0, v5, :cond_1

    .line 369
    iget-object v5, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter$2$1;->a:Lflipboard/gui/section/UserListFragment$UserListAdapter$2;

    iget-object v5, v5, Lflipboard/gui/section/UserListFragment$UserListAdapter$2;->b:Lflipboard/gui/section/UserListFragment$UserListAdapter;

    invoke-virtual {v5, v0}, Lflipboard/gui/section/UserListFragment$UserListAdapter;->b(I)J

    move-result-wide v6

    cmp-long v5, v2, v6

    if-nez v5, :cond_0

    .line 370
    iget-object v5, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter$2$1;->a:Lflipboard/gui/section/UserListFragment$UserListAdapter$2;

    iget-object v5, v5, Lflipboard/gui/section/UserListFragment$UserListAdapter$2;->b:Lflipboard/gui/section/UserListFragment$UserListAdapter;

    invoke-virtual {v5, v0}, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a(I)Lflipboard/objs/FeedSectionLink;

    move-result-object v5

    .line 371
    if-eqz v5, :cond_0

    .line 372
    const/4 v6, 0x1

    iput-boolean v6, v5, Lflipboard/objs/FeedSectionLink;->o:Z

    .line 373
    iget-object v5, v5, Lflipboard/objs/FeedSectionLink;->k:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 374
    add-int/lit8 v1, v1, 0x1

    .line 366
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 378
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/UserListFragment$UserListAdapter$2$1;->a:Lflipboard/gui/section/UserListFragment$UserListAdapter$2;

    iget-object v0, v0, Lflipboard/gui/section/UserListFragment$UserListAdapter$2;->b:Lflipboard/gui/section/UserListFragment$UserListAdapter;

    iget-object v0, v0, Lflipboard/gui/section/UserListFragment$UserListAdapter;->a:Lflipboard/gui/section/UserListFragment;

    iget-object v0, v0, Lflipboard/gui/section/UserListFragment;->a:Lflipboard/gui/section/UserListFragment$UserListAdapter;

    invoke-virtual {v0}, Lflipboard/gui/section/UserListFragment$UserListAdapter;->notifyDataSetChanged()V

    .line 380
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v3, "flipboard"

    new-instance v5, Lflipboard/gui/section/UserListFragment$UserListAdapter$2$1$1;

    invoke-direct {v5, p0, v1}, Lflipboard/gui/section/UserListFragment$UserListAdapter$2$1$1;-><init>(Lflipboard/gui/section/UserListFragment$UserListAdapter$2$1;I)V

    invoke-virtual {v0, v2, v4, v3, v5}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/User;Ljava/util/List;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V

    .line 392
    return-void
.end method
