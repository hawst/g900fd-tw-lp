.class Lflipboard/gui/section/SectionFragmentScrolling$2$1;
.super Ljava/lang/Object;
.source "SectionFragmentScrolling.java"

# interfaces
.implements Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;


# instance fields
.field a:I

.field b:I

.field c:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic d:Landroid/view/View;

.field final synthetic e:Lflipboard/gui/section/SectionFragmentScrolling$2;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragmentScrolling$2;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 155
    iput-object p1, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->e:Lflipboard/gui/section/SectionFragmentScrolling$2;

    iput-object p2, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->d:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->e:Lflipboard/gui/section/SectionFragmentScrolling$2;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->a:I

    .line 157
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->e:Lflipboard/gui/section/SectionFragmentScrolling$2;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->b:I

    .line 158
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->c:Ljava/util/LinkedList;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 187
    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->e:Lflipboard/gui/section/SectionFragmentScrolling$2;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragmentScrolling;->d(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/activities/FlipboardActivity;

    move-result-object v1

    iget-object v1, v1, Lflipboard/activities/FlipboardActivity;->aa:Lcom/squareup/otto/Bus;

    new-instance v2, Lflipboard/gui/FlipFragment$FlipUiEvent;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lflipboard/gui/FlipFragment$FlipUiEvent;-><init>(Z)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->c(Ljava/lang/Object;)V

    .line 188
    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->e:Lflipboard/gui/section/SectionFragmentScrolling$2;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragmentScrolling;->c(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/SmartScrollingListView;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/gui/SmartScrollingListView;->getWrappedList()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 189
    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->e:Lflipboard/gui/section/SectionFragmentScrolling$2;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragmentScrolling;->c(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/SmartScrollingListView;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/gui/SmartScrollingListView;->getChildCount()I

    move-result v1

    .line 190
    :goto_0
    if-ge v0, v1, :cond_0

    .line 191
    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->c:Ljava/util/LinkedList;

    iget-object v3, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->e:Lflipboard/gui/section/SectionFragmentScrolling$2;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v3}, Lflipboard/gui/section/SectionFragmentScrolling;->c(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/SmartScrollingListView;

    move-result-object v3

    invoke-virtual {v3, v0}, Lflipboard/gui/SmartScrollingListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 193
    :cond_0
    return-void
.end method

.method public final a(FI)V
    .locals 5

    .prologue
    const v4, 0x3e99999a    # 0.3f

    .line 163
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->e:Lflipboard/gui/section/SectionFragmentScrolling$2;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->b(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/ViewWithDrawerLayout;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/ViewWithDrawerLayout;->getHeight()I

    move-result v0

    sub-int/2addr v0, p2

    int-to-float v0, v0

    .line 164
    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 165
    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 166
    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    mul-float v1, v0, p1

    .line 167
    const/high16 v0, 0x3f800000    # 1.0f

    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->e:Lflipboard/gui/section/SectionFragmentScrolling$2;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v2}, Lflipboard/gui/section/SectionFragmentScrolling;->b(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/ViewWithDrawerLayout;

    move-result-object v2

    invoke-virtual {v2}, Lflipboard/gui/ViewWithDrawerLayout;->getHeight()I

    move-result v2

    sub-int/2addr v2, p2

    iget v3, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->b:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->d:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 168
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 169
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0

    .line 171
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleX(F)V

    .line 172
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleY(F)V

    .line 173
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->d:Landroid/view/View;

    iget v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->a:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 174
    iget v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->a:I

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->a:I

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p1

    mul-float/2addr v1, v4

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 175
    iget v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->a:I

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->a:I

    invoke-static {v2}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p1

    mul-float/2addr v2, v4

    sub-float/2addr v1, v2

    float-to-int v1, v1

    .line 176
    iget v2, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->a:I

    invoke-static {v2}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->a:I

    invoke-static {v3}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, p1

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    .line 177
    iget-object v3, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->e:Lflipboard/gui/section/SectionFragmentScrolling$2;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v3}, Lflipboard/gui/section/SectionFragmentScrolling;->c(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/SmartScrollingListView;

    move-result-object v3

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-virtual {v3, v0}, Lflipboard/gui/SmartScrollingListView;->setBackgroundColor(I)V

    .line 178
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->e:Lflipboard/gui/section/SectionFragmentScrolling$2;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->c(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/SmartScrollingListView;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/SmartScrollingListView;->getWrappedList()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 199
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->e:Lflipboard/gui/section/SectionFragmentScrolling$2;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->b(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/ViewWithDrawerLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/ViewWithDrawerLayout;->setAnimationListener(Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;)V

    .line 200
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 201
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0

    .line 203
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 208
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$2$1;->e:Lflipboard/gui/section/SectionFragmentScrolling$2;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragmentScrolling$2;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->e(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/activities/FlipboardActivity;

    move-result-object v0

    iget-object v0, v0, Lflipboard/activities/FlipboardActivity;->aa:Lcom/squareup/otto/Bus;

    new-instance v1, Lflipboard/gui/FlipFragment$FlipUiEvent;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lflipboard/gui/FlipFragment$FlipUiEvent;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->c(Ljava/lang/Object;)V

    .line 209
    return-void
.end method
