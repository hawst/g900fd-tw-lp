.class public Lflipboard/gui/section/AttributionPublisher;
.super Lflipboard/gui/FLTextView;
.source "AttributionPublisher.java"

# interfaces
.implements Lflipboard/gui/section/Attribution;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lflipboard/gui/FLTextView;-><init>(Landroid/content/Context;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 39
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->B()Lflipboard/objs/FeedSectionLink;

    move-result-object v6

    .line 40
    if-eqz v6, :cond_6

    .line 41
    iget-object v0, v6, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    .line 42
    if-eqz v0, :cond_0

    const-string v1, "cdn.flipboard.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    const/4 v0, 0x0

    .line 45
    :cond_0
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v4

    .line 46
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    .line 47
    :goto_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    move v5, v2

    .line 49
    :goto_1
    if-eqz v1, :cond_4

    if-eqz v5, :cond_4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 50
    invoke-virtual {p0}, Lflipboard/gui/section/AttributionPublisher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0d0258

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v3

    aput-object v4, v5, v2

    invoke-static {v1, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 56
    :cond_1
    :goto_2
    if-eqz v0, :cond_7

    .line 57
    invoke-virtual {p0, v3}, Lflipboard/gui/section/AttributionPublisher;->setVisibility(I)V

    .line 60
    invoke-virtual {p1, v6}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedSectionLink;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 62
    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionPublisher;->setText(Ljava/lang/CharSequence;)V

    .line 80
    :goto_3
    return-void

    :cond_2
    move v1, v3

    .line 46
    goto :goto_0

    :cond_3
    move v5, v3

    .line 47
    goto :goto_1

    .line 51
    :cond_4
    if-nez v1, :cond_1

    move-object v0, v4

    .line 52
    goto :goto_2

    .line 64
    :cond_5
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 65
    const-string v2, "source"

    const-string v3, "sectionLink"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v2, "originSectionIdentifier"

    iget-object v3, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v2, "linkType"

    const-string v3, "author"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iget-object v2, p2, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-static {p0, v0, v2, v1}, Lflipboard/util/SocialHelper;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/List;Landroid/os/Bundle;)V

    goto :goto_3

    .line 72
    :cond_6
    invoke-static {p2}, Lflipboard/gui/section/ItemDisplayUtil;->d(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v0

    .line 73
    if-eqz v0, :cond_7

    .line 74
    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionPublisher;->setText(Ljava/lang/CharSequence;)V

    .line 75
    invoke-virtual {p0, v3}, Lflipboard/gui/section/AttributionPublisher;->setVisibility(I)V

    goto :goto_3

    .line 77
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lflipboard/gui/section/AttributionPublisher;->setVisibility(I)V

    goto :goto_3
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 95
    return-void
.end method

.method public setDrawTopDivider(Z)V
    .locals 0

    .prologue
    .line 100
    return-void
.end method

.method public setInverted(Z)V
    .locals 0

    .prologue
    .line 85
    return-void
.end method
