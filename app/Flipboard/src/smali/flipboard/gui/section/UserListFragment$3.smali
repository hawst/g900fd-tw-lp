.class Lflipboard/gui/section/UserListFragment$3;
.super Ljava/lang/Object;
.source "UserListFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/gui/section/UserListFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/UserListFragment;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lflipboard/gui/section/UserListFragment$3;->a:Lflipboard/gui/section/UserListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 233
    iget-object v6, p0, Lflipboard/gui/section/UserListFragment$3;->a:Lflipboard/gui/section/UserListFragment;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget v7, v0, Lflipboard/model/ConfigSetting;->MaxNumberEmailsPerLookupRequest:I

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v6, Lflipboard/gui/section/UserListFragment;->d:Landroid/content/ContentResolver;

    if-nez v0, :cond_0

    invoke-virtual {v6}, Lflipboard/gui/section/UserListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, v6, Lflipboard/gui/section/UserListFragment;->d:Landroid/content/ContentResolver;

    :cond_0
    iget-object v0, v6, Lflipboard/gui/section/UserListFragment;->e:Landroid/database/Cursor;

    if-nez v0, :cond_1

    iget-object v0, v6, Lflipboard/gui/section/UserListFragment;->d:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, v6, Lflipboard/gui/section/UserListFragment;->e:Landroid/database/Cursor;

    :cond_1
    :goto_0
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v7, :cond_5

    iget-boolean v0, v6, Lflipboard/gui/section/UserListFragment;->c:Z

    if-eqz v0, :cond_5

    iget-object v0, v6, Lflipboard/gui/section/UserListFragment;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, v6, Lflipboard/gui/section/UserListFragment;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, v6, Lflipboard/gui/section/UserListFragment;->e:Landroid/database/Cursor;

    iget-object v1, v6, Lflipboard/gui/section/UserListFragment;->e:Landroid/database/Cursor;

    const-string v3, "_id"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v0, v6, Lflipboard/gui/section/UserListFragment;->d:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "contact_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object v5, v4, v9

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "data1"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_4
    iput-boolean v9, v6, Lflipboard/gui/section/UserListFragment;->c:Z

    goto :goto_0

    .line 234
    :cond_5
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/UserListFragment$3$1;

    invoke-direct {v1, p0, v8}, Lflipboard/gui/section/UserListFragment$3$1;-><init>(Lflipboard/gui/section/UserListFragment$3;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    .line 240
    return-void
.end method
