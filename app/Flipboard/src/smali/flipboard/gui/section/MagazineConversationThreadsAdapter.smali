.class public Lflipboard/gui/section/MagazineConversationThreadsAdapter;
.super Landroid/widget/BaseAdapter;
.source "MagazineConversationThreadsAdapter.java"


# instance fields
.field public a:Z

.field private b:Lflipboard/activities/GenericFragmentActivity;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/CommentaryResult$Item$Commentary;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lflipboard/activities/GenericFragmentActivity;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/activities/GenericFragmentActivity;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/CommentaryResult$Item$Commentary;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->a:Z

    .line 45
    iput-object p1, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->b:Lflipboard/activities/GenericFragmentActivity;

    .line 46
    iput-object p2, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->c:Ljava/util/List;

    .line 47
    iput-object p3, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->d:Ljava/lang/String;

    .line 48
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/MagazineConversationThreadsAdapter;)Lflipboard/activities/GenericFragmentActivity;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->b:Lflipboard/activities/GenericFragmentActivity;

    return-object v0
.end method

.method private a(I)Lflipboard/objs/CommentaryResult$Item$Commentary;
    .locals 2

    .prologue
    .line 66
    const/4 v0, 0x0

    .line 67
    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->c:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 68
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    .line 70
    :cond_0
    return-object v0
.end method

.method static synthetic a(Lflipboard/gui/section/MagazineConversationThreadsAdapter;Lflipboard/objs/CommentaryResult$Item$Commentary;)V
    .locals 4

    .prologue
    .line 36
    new-instance v0, Lflipboard/objs/FeedItem;

    invoke-direct {v0}, Lflipboard/objs/FeedItem;-><init>()V

    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->d:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedItem;->ci:Ljava/lang/String;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->h:Ljava/lang/String;

    new-instance v3, Lflipboard/gui/section/MagazineConversationThreadsAdapter$2;

    invoke-direct {v3, p0, p1}, Lflipboard/gui/section/MagazineConversationThreadsAdapter$2;-><init>(Lflipboard/gui/section/MagazineConversationThreadsAdapter;Lflipboard/objs/CommentaryResult$Item$Commentary;)V

    invoke-virtual {v1, v0, v2, v3}, Lflipboard/service/FlipboardManager;->a(Lflipboard/objs/FeedItem;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V

    return-void
.end method

.method static synthetic b(Lflipboard/gui/section/MagazineConversationThreadsAdapter;)Ljava/util/List;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->c:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lflipboard/gui/section/MagazineConversationThreadsAdapter;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 52
    const/4 v0, 0x0

    .line 53
    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->c:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 54
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 56
    :cond_0
    iget-boolean v1, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->a:Z

    if-eqz v1, :cond_1

    .line 59
    add-int/lit8 v0, v0, 0x1

    .line 61
    :cond_1
    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->a(I)Lflipboard/objs/CommentaryResult$Item$Commentary;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 75
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 95
    const/4 v0, 0x1

    .line 96
    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->c:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 98
    const/4 v0, 0x0

    .line 100
    :cond_0
    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 105
    invoke-virtual {p0, p1}, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_5

    .line 108
    new-instance v2, Lflipboard/gui/section/MagazineConversationThreadsAdapter$ConversationThreadClickListener;

    invoke-direct {v2, p0, v8}, Lflipboard/gui/section/MagazineConversationThreadsAdapter$ConversationThreadClickListener;-><init>(Lflipboard/gui/section/MagazineConversationThreadsAdapter;B)V

    .line 111
    if-nez p2, :cond_2

    .line 112
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300d3

    invoke-static {v0, v1, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 114
    new-instance v1, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;

    invoke-direct {v1, p0, v8}, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;-><init>(Lflipboard/gui/section/MagazineConversationThreadsAdapter;B)V

    .line 115
    const v0, 0x7f0a0266

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;->a:Lflipboard/gui/FLImageView;

    .line 116
    const v0, 0x7f0a004f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;->b:Lflipboard/gui/FLTextIntf;

    .line 117
    const v0, 0x7f0a0268

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;->c:Lflipboard/gui/FLTextIntf;

    .line 118
    const v0, 0x7f0a0270

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;->d:Lflipboard/gui/FLTextIntf;

    .line 119
    const v0, 0x7f0a0267

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;->e:Lflipboard/gui/FLTextIntf;

    .line 120
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 122
    invoke-virtual {p2, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    :goto_0
    invoke-direct {p0, p1}, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->a(I)Lflipboard/objs/CommentaryResult$Item$Commentary;

    move-result-object v3

    .line 129
    if-eqz v3, :cond_1

    .line 130
    iput p1, v1, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;->f:I

    .line 131
    iget-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;->b:Lflipboard/gui/FLTextIntf;

    iget-object v4, v3, Lflipboard/objs/CommentaryResult$Item$Commentary;->c:Ljava/lang/String;

    invoke-interface {v0, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;->a:Lflipboard/gui/FLImageView;

    iget-object v4, v3, Lflipboard/objs/CommentaryResult$Item$Commentary;->g:Lflipboard/objs/Image;

    invoke-virtual {v0, v4}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 133
    iget-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;->c:Lflipboard/gui/FLTextIntf;

    iget-object v4, v3, Lflipboard/objs/CommentaryResult$Item$Commentary;->j:Ljava/lang/String;

    invoke-interface {v0, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;->c:Lflipboard/gui/FLTextIntf;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 136
    iget v0, v3, Lflipboard/objs/CommentaryResult$Item$Commentary;->n:I

    .line 137
    if-nez v0, :cond_3

    const-string v0, "Start the conversation!"

    .line 138
    :goto_1
    iget-object v4, v1, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;->d:Lflipboard/gui/FLTextIntf;

    invoke-interface {v4, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 140
    iget-wide v4, v3, Lflipboard/objs/CommentaryResult$Item$Commentary;->k:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    .line 141
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->b:Lflipboard/activities/GenericFragmentActivity;

    invoke-static {v0, v4, v5}, Lflipboard/util/JavaUtil;->b(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    .line 143
    iget-object v4, v1, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;->e:Lflipboard/gui/FLTextIntf;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " ago"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v0, v3, Lflipboard/objs/CommentaryResult$Item$Commentary;->j:Ljava/lang/String;

    const/16 v4, 0x5dc

    invoke-static {v0, v4}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 147
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 149
    iget-object v0, v1, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;->c:Lflipboard/gui/FLTextIntf;

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, v3, Lflipboard/objs/CommentaryResult$Item$Commentary;->o:Ljava/util/List;

    invoke-static {v0, v4, v1, v2, v5}, Lflipboard/util/SocialHelper;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/List;Landroid/view/View$OnClickListener;Landroid/os/Bundle;)V

    .line 151
    new-instance v1, Lflipboard/gui/actionbar/FLActionBarMenu;

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->b:Lflipboard/activities/GenericFragmentActivity;

    invoke-direct {v1, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0a026f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0, v8, v8}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    sget-object v2, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->d:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    invoke-virtual {v0, v2}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    iget-object v2, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->b:Lflipboard/activities/GenericFragmentActivity;

    invoke-virtual {v2}, Lflipboard/activities/GenericFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f09007b

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v4, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->b:Lflipboard/activities/GenericFragmentActivity;

    invoke-virtual {v4}, Lflipboard/activities/GenericFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09007d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v0, v2, v4, v2, v2}, Lflipboard/gui/actionbar/FLActionBar;->a(IIII)V

    iget-object v2, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->b:Lflipboard/activities/GenericFragmentActivity;

    invoke-virtual {v0, v2, v10}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/gui/dialog/FLDialogFragment;)V

    iget-boolean v2, v3, Lflipboard/objs/CommentaryResult$Item$Commentary;->p:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0, v8}, Lflipboard/gui/actionbar/FLActionBar;->setVisibility(I)V

    const v2, 0x7f0d0019

    invoke-virtual {v1, v2}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v2

    new-instance v4, Lflipboard/gui/section/MagazineConversationThreadsAdapter$1;

    invoke-direct {v4, p0, v3}, Lflipboard/gui/section/MagazineConversationThreadsAdapter$1;-><init>(Lflipboard/gui/section/MagazineConversationThreadsAdapter;Lflipboard/objs/CommentaryResult$Item$Commentary;)V

    iput-object v4, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    iput-boolean v9, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    invoke-virtual {v2, v8}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    :cond_0
    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->setMenu(Lflipboard/gui/actionbar/FLActionBarMenu;)V

    .line 162
    :cond_1
    :goto_2
    return-object p2

    .line 124
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;

    .line 125
    iget-object v1, v0, Lflipboard/gui/section/MagazineConversationThreadsAdapter$Holder;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->a()V

    move-object v1, v0

    goto/16 :goto_0

    .line 137
    :cond_3
    if-ne v0, v9, :cond_4

    const-string v0, "1 reply"

    goto/16 :goto_1

    :cond_4
    const-string v4, "%d replies"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 155
    :cond_5
    if-nez p2, :cond_1

    iget-boolean v0, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->a:Z

    if-eqz v0, :cond_1

    .line 156
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030050

    invoke-static {v0, v1, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 157
    const v0, 0x7f0a012d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 158
    const-string v0, "loading"

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 80
    .line 81
    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->c:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    const/4 v0, 0x1

    .line 84
    :cond_0
    return v0
.end method
