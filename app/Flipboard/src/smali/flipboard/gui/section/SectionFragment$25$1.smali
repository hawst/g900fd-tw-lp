.class Lflipboard/gui/section/SectionFragment$25$1;
.super Ljava/lang/Object;
.source "SectionFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/json/FLObject;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lflipboard/gui/section/SectionFragment$25;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment$25;Lflipboard/json/FLObject;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 3491
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$25$1;->c:Lflipboard/gui/section/SectionFragment$25;

    iput-object p2, p0, Lflipboard/gui/section/SectionFragment$25$1;->a:Lflipboard/json/FLObject;

    iput-object p3, p0, Lflipboard/gui/section/SectionFragment$25$1;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 3495
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$25$1;->c:Lflipboard/gui/section/SectionFragment$25;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment$25;->a:Lflipboard/gui/dialog/FLProgressDialogFragment;

    invoke-virtual {v1}, Lflipboard/gui/dialog/FLProgressDialogFragment;->a()V

    .line 3496
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$25$1;->c:Lflipboard/gui/section/SectionFragment$25;

    iget-boolean v1, v1, Lflipboard/gui/section/SectionFragment$25;->b:Z

    if-eqz v1, :cond_1

    .line 3498
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$25$1;->a:Lflipboard/json/FLObject;

    if-eqz v1, :cond_2

    .line 3499
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$25$1;->a:Lflipboard/json/FLObject;

    const-string v2, "permalink"

    invoke-virtual {v1, v2, v0}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 3501
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$25$1;->c:Lflipboard/gui/section/SectionFragment$25;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$25;->c:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$25$1;->c:Lflipboard/gui/section/SectionFragment$25;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment$25;->c:Lflipboard/gui/section/SectionFragment;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment;->z:Lflipboard/objs/FeedItem;

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$25$1;->b:Ljava/lang/String;

    invoke-static {v0, v2, v3, v1}, Lflipboard/util/SocialHelper;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/FeedItem;Ljava/lang/String;Ljava/lang/String;)V

    .line 3511
    :cond_0
    :goto_1
    return-void

    .line 3503
    :cond_1
    new-instance v1, Lflipboard/objs/FeedItem;

    invoke-direct {v1}, Lflipboard/objs/FeedItem;-><init>()V

    .line 3504
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$25$1;->b:Ljava/lang/String;

    iput-object v0, v1, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    .line 3505
    const-string v0, "section"

    iput-object v0, v1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 3506
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$25$1;->c:Lflipboard/gui/section/SectionFragment$25;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment$25;->c:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 3507
    if-eqz v0, :cond_0

    .line 3508
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$25$1;->c:Lflipboard/gui/section/SectionFragment$25;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment$25;->c:Lflipboard/gui/section/SectionFragment;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    const v3, 0x7f0d02eb

    invoke-virtual {v0, v3}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method
