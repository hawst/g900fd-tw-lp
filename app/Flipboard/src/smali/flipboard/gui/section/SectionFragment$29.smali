.class public Lflipboard/gui/section/SectionFragment$29;
.super Ljava/lang/Object;
.source "SectionFragment.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/audio/FLAudioManager;",
        "Lflipboard/service/audio/FLAudioManager$AudioMessage;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/gui/section/SectionFragment;


# direct methods
.method public constructor <init>(Lflipboard/gui/section/SectionFragment;)V
    .locals 0

    .prologue
    .line 3871
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$29;->a:Lflipboard/gui/section/SectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 3871
    check-cast p2, Lflipboard/service/audio/FLAudioManager$AudioMessage;

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$29;->a:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->b:Lflipboard/service/audio/FLAudioManager$AudioState;

    sget-object v2, Lflipboard/gui/section/SectionFragment$35;->b:[I

    invoke-virtual {p2}, Lflipboard/service/audio/FLAudioManager$AudioMessage;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    move-object v1, v0

    :goto_1
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$29;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$29;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->t:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getFlippableViews()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlippingContainer;->getChild()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/SectionPage;

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$29;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v3}, Lflipboard/gui/section/SectionFragment;->v(Lflipboard/gui/section/SectionFragment;)Lflipboard/service/audio/FLAudioManager;

    move-result-object v3

    invoke-virtual {v3}, Lflipboard/service/audio/FLAudioManager;->f()Lflipboard/objs/FeedItem;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lflipboard/gui/section/SectionPage;->a(Lflipboard/service/audio/FLAudioManager$AudioState;Lflipboard/objs/FeedItem;)V

    goto :goto_2

    :pswitch_0
    check-cast p3, Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    sget-object v0, Lflipboard/gui/section/SectionFragment$35;->a:[I

    invoke-virtual {p3}, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->b:Lflipboard/service/audio/FLAudioManager$AudioState;

    move-object v1, v0

    goto :goto_1

    :pswitch_1
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->c:Lflipboard/service/audio/FLAudioManager$AudioState;

    move-object v1, v0

    goto :goto_1

    :pswitch_2
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->a:Lflipboard/service/audio/FLAudioManager$AudioState;

    move-object v1, v0

    goto :goto_1

    :pswitch_3
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->c:Lflipboard/service/audio/FLAudioManager$AudioState;

    move-object v1, v0

    goto :goto_1

    :pswitch_4
    instance-of v0, v1, Lflipboard/activities/FlipboardActivity;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-boolean v2, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v2, :cond_0

    sget-object v2, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v2}, Lflipboard/io/NetworkManager;->a()Z

    move-result v2

    if-nez v2, :cond_1

    const v2, 0x7f0d0041

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    :cond_0
    :goto_3
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->b:Lflipboard/service/audio/FLAudioManager$AudioState;

    move-object v1, v0

    goto :goto_1

    :cond_1
    const v2, 0x7f0d0042

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_3

    :pswitch_5
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->b:Lflipboard/service/audio/FLAudioManager$AudioState;

    move-object v1, v0

    goto :goto_1

    :pswitch_6
    check-cast v1, Lflipboard/activities/FlipboardActivity;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lflipboard/activities/FlipboardActivity;->setVolumeControlStream(I)V

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$29;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragment;->v(Lflipboard/gui/section/SectionFragment;)Lflipboard/service/audio/FLAudioManager;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/service/audio/FLAudioManager;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$29;->a:Lflipboard/gui/section/SectionFragment;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragment;->v(Lflipboard/gui/section/SectionFragment;)Lflipboard/service/audio/FLAudioManager;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/service/audio/FLAudioManager;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->c:Lflipboard/service/audio/FLAudioManager$AudioState;

    :cond_2
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$29;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/section/SectionFragment$29$1;

    invoke-direct {v2, p0}, Lflipboard/gui/section/SectionFragment$29$1;-><init>(Lflipboard/gui/section/SectionFragment$29;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    move-object v1, v0

    goto/16 :goto_1

    :pswitch_7
    check-cast v1, Lflipboard/activities/FlipboardActivity;

    const/high16 v2, -0x80000000

    invoke-virtual {v1, v2}, Lflipboard/activities/FlipboardActivity;->setVolumeControlStream(I)V

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$29;->a:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->r:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/section/SectionFragment$29$2;

    invoke-direct {v2, p0}, Lflipboard/gui/section/SectionFragment$29$2;-><init>(Lflipboard/gui/section/SectionFragment$29;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    :cond_3
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
