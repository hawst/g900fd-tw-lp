.class public Lflipboard/gui/section/SectionHeaderView;
.super Landroid/widget/LinearLayout;
.source "SectionHeaderView.java"

# interfaces
.implements Lflipboard/gui/section/scrolling/header/HeaderView;


# instance fields
.field a:Lflipboard/gui/actionbar/FLActionBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)V
    .locals 0

    .prologue
    .line 132
    return-void
.end method

.method public getActionBar()Lflipboard/gui/actionbar/FLActionBar;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lflipboard/gui/section/SectionHeaderView;->a:Lflipboard/gui/actionbar/FLActionBar;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 116
    return-object p0
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 55
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 56
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 57
    return-void
.end method

.method public setItem(Lflipboard/service/Section;)V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public setShowActionBar(Z)V
    .locals 0

    .prologue
    .line 122
    return-void
.end method
