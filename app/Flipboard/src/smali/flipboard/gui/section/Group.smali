.class public Lflipboard/gui/section/Group;
.super Ljava/lang/Object;
.source "Group.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lflipboard/gui/section/Group;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lflipboard/objs/SectionPageTemplate;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lflipboard/objs/SidebarGroup;

.field public final d:Lflipboard/service/Section;

.field public final e:Z

.field public f:Z

.field public g:I

.field h:Z

.field i:I

.field transient j:Z

.field public k:Lflipboard/gui/section/GroupFranchiseMeta;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 649
    new-instance v0, Lflipboard/gui/section/Group$1;

    invoke-direct {v0}, Lflipboard/gui/section/Group$1;-><init>()V

    sput-object v0, Lflipboard/gui/section/Group;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 659
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-boolean v0, p0, Lflipboard/gui/section/Group;->f:Z

    .line 660
    iput-boolean v0, p0, Lflipboard/gui/section/Group;->e:Z

    .line 661
    iput-object v1, p0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    .line 662
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v4

    .line 663
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const-string v3, "template"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflipboard/app/FlipboardApplication;->a(Ljava/lang/String;)Lflipboard/objs/SectionPageTemplate;

    move-result-object v2

    iput-object v2, p0, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    .line 664
    const-string v2, "itemIds"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 665
    new-instance v2, Ljava/util/ArrayList;

    array-length v3, v5

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    .line 666
    const-string v2, "sectionId"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 667
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v3, v2}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v2

    iput-object v2, p0, Lflipboard/gui/section/Group;->d:Lflipboard/service/Section;

    .line 668
    iget-object v2, p0, Lflipboard/gui/section/Group;->d:Lflipboard/service/Section;

    if-eqz v2, :cond_2

    .line 669
    array-length v6, v5

    move v3, v0

    :goto_0
    if-ge v3, v6, :cond_2

    aget-object v0, v5, v3

    .line 670
    iget-object v2, p0, Lflipboard/gui/section/Group;->d:Lflipboard/service/Section;

    invoke-virtual {v2, v0}, Lflipboard/service/Section;->a(Ljava/lang/String;)Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 671
    if-nez v0, :cond_1

    .line 674
    :try_start_0
    const-string v2, "pageboxItem"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 675
    if-eqz v2, :cond_0

    .line 676
    new-instance v7, Lflipboard/json/JSONParser;

    invoke-direct {v7, v2}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lflipboard/json/JSONParser;->k()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 678
    :cond_0
    const-string v2, "pageboxItem"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 683
    :cond_1
    :goto_1
    iget-object v2, p0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 669
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 679
    :catch_0
    move-exception v2

    .line 680
    sget-object v7, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v7, v2}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 686
    :cond_2
    const-string v0, "score"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/Group;->g:I

    .line 687
    const-string v0, "pagebox"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 689
    if-eqz v0, :cond_4

    .line 691
    :try_start_1
    new-instance v2, Lflipboard/json/JSONParser;

    invoke-direct {v2, v0}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lflipboard/json/JSONParser;->v()Lflipboard/objs/SidebarGroup;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 696
    :goto_2
    const-string v1, "franchiseMeta"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 697
    if-eqz v1, :cond_3

    .line 698
    invoke-static {v1}, Lflipboard/gui/section/GroupFranchiseMeta;->a(Landroid/os/Bundle;)Lflipboard/gui/section/GroupFranchiseMeta;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    .line 700
    :cond_3
    iput-object v0, p0, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    .line 701
    return-void

    .line 692
    :catch_1
    move-exception v0

    .line 693
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v2, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lflipboard/gui/section/Group;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Lflipboard/service/Section;Lflipboard/objs/SectionPageTemplate;Lflipboard/objs/FeedItem;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/section/Group;->f:Z

    .line 90
    iput-object p2, p0, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    .line 91
    invoke-static {p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    .line 92
    iget-object v0, p3, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 93
    iget v0, p0, Lflipboard/gui/section/Group;->g:I

    iget-object v1, p3, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0xa

    add-int/2addr v0, v1

    iput v0, p0, Lflipboard/gui/section/Group;->g:I

    .line 95
    :cond_0
    iput-object p1, p0, Lflipboard/gui/section/Group;->d:Lflipboard/service/Section;

    .line 96
    iput-object v2, p0, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    .line 97
    iput-boolean p4, p0, Lflipboard/gui/section/Group;->e:Z

    .line 98
    iput-object v2, p0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    .line 99
    return-void
.end method

.method public constructor <init>(Lflipboard/service/Section;Lflipboard/objs/SectionPageTemplate;Ljava/util/List;Lflipboard/objs/SidebarGroup;ZII)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/service/Section;",
            "Lflipboard/objs/SectionPageTemplate;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;",
            "Lflipboard/objs/SidebarGroup;",
            "ZII)V"
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lflipboard/gui/section/Group;->f:Z

    .line 112
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    .line 113
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lflipboard/gui/section/Group;->d:Lflipboard/service/Section;

    .line 114
    if-eqz p4, :cond_0

    .line 115
    invoke-virtual/range {p4 .. p4}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v2

    .line 116
    if-eqz v2, :cond_0

    sget-object v2, Lflipboard/gui/section/SectionFragment;->d:Ljava/util/List;

    invoke-virtual/range {p4 .. p4}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v3

    iget-object v3, v3, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 117
    const/16 p4, 0x0

    .line 120
    :cond_0
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    .line 121
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lflipboard/gui/section/Group;->e:Z

    .line 122
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    .line 124
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0x14

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    .line 125
    new-instance v17, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 126
    new-instance v18, Ljava/util/ArrayList;

    move-object/from16 v0, p2

    move/from16 v1, p5

    invoke-virtual {v0, v1}, Lflipboard/objs/SectionPageTemplate;->a(Z)Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 129
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v5

    .line 130
    const/4 v4, 0x0

    .line 131
    move-object/from16 v0, p2

    iget-boolean v2, v0, Lflipboard/objs/SectionPageTemplate;->n:Z

    if-eqz v2, :cond_24

    .line 132
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v2}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 133
    const/high16 v3, 0x40c00000    # 6.0f

    iget v6, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v6

    iget v2, v2, Landroid/util/DisplayMetrics;->scaledDensity:F

    div-float v2, v3, v2

    float-to-int v6, v2

    .line 134
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 135
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v2, v6, :cond_2

    mul-int/lit8 v2, v6, 0x3

    if-ge v3, v2, :cond_2

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    .line 136
    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/FeedItem;

    .line 137
    if-eqz v2, :cond_1

    const-string v8, "status"

    iget-object v9, v2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 138
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 141
    :cond_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v2, v6, :cond_24

    .line 142
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/FeedItem;

    .line 143
    iget-object v2, v2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 145
    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lflipboard/gui/section/Group;->g:I

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    mul-int/lit16 v3, v3, 0x3e8

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lflipboard/gui/section/Group;->g:I

    .line 148
    add-int/lit8 v2, v5, -0x1

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/2addr v3, v2

    .line 149
    new-instance v2, Lflipboard/objs/FeedItem;

    invoke-direct {v2}, Lflipboard/objs/FeedItem;-><init>()V

    .line 150
    const-string v4, "list"

    iput-object v4, v2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 151
    iput-object v7, v2, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    .line 152
    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    const/4 v2, 0x1

    .line 157
    :goto_2
    const/4 v4, 0x0

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v19

    move/from16 v16, v2

    .line 159
    :goto_3
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v16

    if-ge v0, v2, :cond_1d

    .line 160
    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lflipboard/objs/SectionPageTemplate$Area;

    .line 161
    const/4 v14, 0x0

    .line 162
    const v13, -0x186a0

    .line 163
    const/4 v2, 0x0

    .line 164
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    move v15, v2

    :goto_4
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lflipboard/objs/FeedItem;

    .line 165
    if-eqz v6, :cond_21

    invoke-virtual {v6}, Lflipboard/objs/FeedItem;->P()Z

    move-result v2

    if-nez v2, :cond_21

    iget-object v2, v6, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_21

    .line 166
    const/4 v2, 0x0

    .line 167
    move-object/from16 v0, p1

    iget-object v3, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v3, v3, Lflipboard/objs/TOCSection;->E:Z

    if-nez v3, :cond_4

    move-object/from16 v0, p1

    iget-object v3, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    if-eqz v3, :cond_23

    move-object/from16 v0, p1

    iget-object v3, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    const-string v5, "nytimes"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_23

    .line 168
    :cond_4
    move/from16 v0, v16

    if-eq v0, v15, :cond_23

    .line 169
    const/16 v2, -0x2710

    move v8, v2

    .line 172
    :goto_5
    move-object/from16 v0, p1

    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v2, v2, Lflipboard/objs/TOCSection;->E:Z

    if-nez v2, :cond_5

    move/from16 v2, p6

    move/from16 v3, p7

    move-object/from16 v5, p1

    move/from16 v7, p5

    .line 174
    invoke-static/range {v2 .. v7}, Lflipboard/gui/section/Group;->a(IILflipboard/objs/SectionPageTemplate$Area;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Z)I

    move-result v2

    add-int/2addr v8, v2

    .line 177
    :cond_5
    iget-object v2, v6, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v3, "synthetic-client-profile-summary-item"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_22

    const/4 v2, 0x1

    move/from16 v0, v16

    if-ne v0, v2, :cond_22

    .line 178
    if-eqz p5, :cond_b

    .line 179
    iget v2, v4, Lflipboard/objs/SectionPageTemplate$Area;->a:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_a

    iget v2, v4, Lflipboard/objs/SectionPageTemplate$Area;->b:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_a

    .line 180
    const/16 v8, 0x2710

    move v2, v8

    .line 190
    :goto_6
    const/4 v3, 0x0

    .line 191
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v5

    const/4 v7, 0x1

    if-le v5, v7, :cond_9

    sget-object v5, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v5}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 193
    iget-object v5, v6, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v7, "image"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, v6, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v7, "video"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, v6, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v7, "audio"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 194
    :cond_6
    const/4 v3, 0x1

    .line 207
    :cond_7
    :goto_7
    if-eqz v3, :cond_9

    .line 210
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v3

    const/4 v5, 0x4

    if-lt v3, v5, :cond_13

    .line 211
    add-int/lit16 v2, v2, -0x1f4

    .line 219
    :cond_8
    :goto_8
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v3

    const/4 v5, 0x4

    if-ge v3, v5, :cond_9

    if-nez v16, :cond_9

    .line 220
    add-int/lit16 v2, v2, 0x1f4

    .line 224
    :cond_9
    if-ge v13, v2, :cond_21

    .line 229
    :goto_9
    add-int/lit8 v3, v15, 0x1

    move v15, v3

    move v13, v2

    move-object v14, v6

    .line 230
    goto/16 :goto_4

    .line 182
    :cond_a
    const/16 v8, -0x2710

    move v2, v8

    goto :goto_6

    .line 184
    :cond_b
    iget v2, v4, Lflipboard/objs/SectionPageTemplate$Area;->a:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_c

    iget v2, v4, Lflipboard/objs/SectionPageTemplate$Area;->b:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_c

    .line 185
    const/16 v8, 0x2710

    move v2, v8

    goto :goto_6

    .line 187
    :cond_c
    const/16 v8, -0x2710

    move v2, v8

    goto :goto_6

    .line 195
    :cond_d
    iget-object v5, v6, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v7, "post"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 196
    move/from16 v0, p6

    int-to-float v5, v0

    iget v7, v4, Lflipboard/objs/SectionPageTemplate$Area;->c:F

    mul-float/2addr v5, v7

    float-to-int v7, v5

    .line 197
    move/from16 v0, p7

    int-to-float v5, v0

    iget v8, v4, Lflipboard/objs/SectionPageTemplate$Area;->d:F

    mul-float/2addr v5, v8

    float-to-int v8, v5

    .line 199
    move/from16 v0, p5

    invoke-virtual {v4, v0}, Lflipboard/objs/SectionPageTemplate$Area;->d(Z)F

    move-result v5

    const/4 v9, 0x0

    cmpl-float v5, v5, v9

    if-nez v5, :cond_10

    move/from16 v0, p5

    invoke-virtual {v4, v0}, Lflipboard/objs/SectionPageTemplate$Area;->a(Z)F

    move-result v5

    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v5, v5, v9

    if-nez v5, :cond_10

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v5

    const/4 v9, 0x3

    if-lt v5, v9, :cond_10

    const/4 v11, 0x1

    .line 200
    :goto_a
    iget-boolean v5, v4, Lflipboard/objs/SectionPageTemplate$Area;->f:Z

    if-nez v5, :cond_e

    iget-boolean v5, v6, Lflipboard/objs/FeedItem;->bG:Z

    if-eqz v5, :cond_11

    :cond_e
    const/4 v5, 0x1

    :goto_b
    if-eqz v5, :cond_12

    iget v5, v4, Lflipboard/objs/SectionPageTemplate$Area;->c:F

    iget v9, v4, Lflipboard/objs/SectionPageTemplate$Area;->d:F

    invoke-virtual {v6, v5, v9}, Lflipboard/objs/FeedItem;->a(FF)Z

    move-result v5

    if-eqz v5, :cond_12

    const/4 v10, 0x1

    :goto_c
    const/4 v12, 0x0

    move-object v9, v6

    invoke-static/range {v7 .. v12}, Lflipboard/gui/section/item/PostItemPhone;->a(IILflipboard/objs/FeedItem;ZZZ)Lflipboard/gui/section/item/PostItemPhone$Layout;

    move-result-object v5

    .line 202
    sget-object v7, Lflipboard/gui/section/item/PostItemPhone$Layout;->e:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-eq v5, v7, :cond_f

    sget-object v7, Lflipboard/gui/section/item/PostItemPhone$Layout;->d:Lflipboard/gui/section/item/PostItemPhone$Layout;

    if-ne v5, v7, :cond_7

    .line 203
    :cond_f
    const/4 v3, 0x1

    goto/16 :goto_7

    .line 199
    :cond_10
    const/4 v11, 0x0

    goto :goto_a

    .line 200
    :cond_11
    const/4 v5, 0x0

    goto :goto_b

    :cond_12
    const/4 v10, 0x0

    goto :goto_c

    .line 212
    :cond_13
    const/4 v3, 0x1

    move/from16 v0, v16

    if-ne v0, v3, :cond_14

    iget v3, v4, Lflipboard/objs/SectionPageTemplate$Area;->d:F

    const v5, 0x3ea8f5c3    # 0.33f

    cmpg-float v3, v3, v5

    if-gez v3, :cond_14

    .line 213
    add-int/lit16 v2, v2, -0x2710

    goto/16 :goto_8

    .line 214
    :cond_14
    const/4 v3, 0x1

    move/from16 v0, v16

    if-le v0, v3, :cond_8

    .line 215
    add-int/lit16 v2, v2, -0x2710

    goto/16 :goto_8

    .line 231
    :cond_15
    if-eqz p4, :cond_1c

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lflipboard/gui/section/Group;->h:Z

    if-nez v2, :cond_1c

    .line 232
    const/16 v2, 0x4e20

    .line 233
    iget v3, v4, Lflipboard/objs/SectionPageTemplate$Area;->a:F

    const/4 v5, 0x0

    cmpl-float v3, v3, v5

    if-nez v3, :cond_16

    .line 234
    const/16 v2, -0x2710

    .line 236
    :cond_16
    iget v3, v4, Lflipboard/objs/SectionPageTemplate$Area;->a:F

    iget v5, v4, Lflipboard/objs/SectionPageTemplate$Area;->c:F

    add-float/2addr v3, v5

    float-to-double v6, v3

    const-wide v8, 0x3fee666666666666L    # 0.95

    cmpl-double v3, v6, v8

    if-lez v3, :cond_17

    .line 237
    add-int/lit16 v2, v2, 0x4e20

    .line 239
    :cond_17
    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v3}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 240
    iget v5, v4, Lflipboard/objs/SectionPageTemplate$Area;->c:F

    move/from16 v0, p6

    int-to-float v6, v0

    mul-float/2addr v5, v6

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v5, v6

    float-to-int v5, v5

    .line 241
    iget v4, v4, Lflipboard/objs/SectionPageTemplate$Area;->d:F

    move/from16 v0, p7

    int-to-float v6, v0

    mul-float/2addr v4, v6

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    div-float v3, v4, v3

    float-to-int v3, v3

    .line 242
    invoke-virtual/range {p4 .. p4}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v4

    .line 243
    iget-object v4, v4, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    const-string v6, "pageboxList"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 244
    const/16 v4, 0x12c

    if-ge v5, v4, :cond_18

    .line 245
    add-int/lit16 v2, v2, -0x7530

    .line 247
    :cond_18
    const/16 v4, 0x1f4

    if-le v5, v4, :cond_19

    .line 248
    add-int/lit16 v2, v2, -0x7530

    .line 250
    :cond_19
    const/16 v4, 0x1f4

    if-ge v3, v4, :cond_1a

    .line 251
    const v4, 0xea60

    sub-int/2addr v2, v4

    .line 253
    :cond_1a
    const/16 v4, 0x384

    if-le v3, v4, :cond_1b

    .line 254
    add-int/lit16 v2, v2, -0x2710

    .line 265
    :cond_1b
    :goto_d
    if-le v2, v13, :cond_1c

    .line 267
    new-instance v14, Lflipboard/objs/FeedItem;

    invoke-direct {v14}, Lflipboard/objs/FeedItem;-><init>()V

    .line 268
    const-string v3, "pagebox"

    iput-object v3, v14, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 269
    move-object/from16 v0, p4

    iget-object v3, v0, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    iput-object v3, v14, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    .line 270
    move-object/from16 v0, p4

    iget-object v3, v0, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    iput-object v3, v14, Lflipboard/objs/FeedItem;->c:Ljava/lang/String;

    .line 271
    invoke-virtual/range {p4 .. p4}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v3

    iget-object v3, v3, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    iput-object v3, v14, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    .line 273
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lflipboard/gui/section/Group;->h:Z

    move v13, v2

    .line 277
    :cond_1c
    if-nez v14, :cond_20

    .line 278
    const v2, -0x186a0

    move-object/from16 v0, p0

    iput v2, v0, Lflipboard/gui/section/Group;->g:I

    .line 287
    :cond_1d
    return-void

    .line 258
    :cond_1e
    mul-int/2addr v3, v5

    .line 259
    const v4, 0x249f0

    if-ge v3, v4, :cond_1f

    .line 260
    const v3, 0xc350

    sub-int/2addr v2, v3

    goto :goto_d

    .line 261
    :cond_1f
    const v4, 0x6ddd0

    if-le v3, v4, :cond_1b

    .line 262
    const v3, 0xc350

    sub-int/2addr v2, v3

    goto :goto_d

    .line 281
    :cond_20
    const v2, -0x186a0

    invoke-static {v2, v13}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 283
    move-object/from16 v0, p0

    iget v3, v0, Lflipboard/gui/section/Group;->g:I

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lflipboard/gui/section/Group;->g:I

    .line 284
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v2, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    iget-object v2, v14, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 159
    add-int/lit8 v2, v16, 0x1

    move/from16 v16, v2

    goto/16 :goto_3

    :cond_21
    move v2, v13

    move-object v6, v14

    goto/16 :goto_9

    :cond_22
    move v2, v8

    goto/16 :goto_6

    :cond_23
    move v8, v2

    goto/16 :goto_5

    :cond_24
    move v2, v4

    move v3, v5

    goto/16 :goto_2
.end method

.method constructor <init>(Lflipboard/service/Section;Lflipboard/objs/SidebarGroup;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-boolean v3, p0, Lflipboard/gui/section/Group;->f:Z

    .line 68
    invoke-static {}, Lflipboard/gui/section/SectionFragment;->k()Lflipboard/objs/SectionPageTemplate;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    .line 69
    iput-object p1, p0, Lflipboard/gui/section/Group;->d:Lflipboard/service/Section;

    .line 70
    iput-object p2, p0, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    .line 71
    iput-boolean v2, p0, Lflipboard/gui/section/Group;->h:Z

    .line 72
    iput-boolean v3, p0, Lflipboard/gui/section/Group;->e:Z

    .line 73
    new-instance v0, Lflipboard/objs/FeedItem;

    invoke-direct {v0}, Lflipboard/objs/FeedItem;-><init>()V

    .line 74
    const-string v1, "pagebox"

    iput-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 75
    iget-object v1, p2, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    .line 76
    iget-object v1, p2, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedItem;->c:Ljava/lang/String;

    .line 77
    invoke-virtual {p2}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v1

    iget-object v1, v1, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    .line 78
    new-instance v1, Ljava/util/ArrayList;

    new-array v2, v2, [Lflipboard/objs/FeedItem;

    aput-object v0, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    .line 79
    return-void
.end method

.method static a(IILflipboard/objs/SectionPageTemplate$Area;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Z)I
    .locals 18

    .prologue
    .line 310
    move-object/from16 v0, p4

    iget v2, v0, Lflipboard/objs/FeedItem;->E:F

    .line 311
    invoke-virtual/range {p3 .. p3}, Lflipboard/service/Section;->a()Ljava/lang/String;

    move-result-object v3

    .line 312
    if-eqz v3, :cond_32

    .line 313
    sget-object v4, Lflipboard/service/Section;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 314
    const v2, 0x3e99999a    # 0.3f

    move v10, v2

    .line 324
    :goto_0
    const/high16 v2, 0x3f000000    # 0.5f

    cmpl-float v2, v10, v2

    if-lez v2, :cond_5

    .line 325
    const/high16 v2, 0x3fc00000    # 1.5f

    sub-float/2addr v2, v10

    .line 330
    :goto_1
    move-object/from16 v0, p2

    move/from16 v1, p5

    invoke-virtual {v0, v1}, Lflipboard/objs/SectionPageTemplate$Area;->a(Z)F

    move-result v7

    .line 331
    move-object/from16 v0, p2

    move/from16 v1, p5

    invoke-virtual {v0, v1}, Lflipboard/objs/SectionPageTemplate$Area;->b(Z)F

    move-result v8

    .line 332
    move/from16 v0, p0

    int-to-float v3, v0

    mul-float/2addr v3, v7

    float-to-int v6, v3

    .line 333
    move/from16 v0, p1

    int-to-float v3, v0

    mul-float/2addr v3, v8

    float-to-int v4, v3

    .line 334
    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v3}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 335
    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v11, v3, Landroid/util/DisplayMetrics;->density:F

    .line 336
    invoke-virtual/range {p4 .. p4}, Lflipboard/objs/FeedItem;->ab()Z

    move-result v3

    if-nez v3, :cond_0

    const/high16 v3, 0x3f000000    # 0.5f

    cmpl-float v3, v10, v3

    if-gez v3, :cond_0

    move-object/from16 v0, p4

    iget-object v3, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v5, "status"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_0
    const/4 v3, 0x1

    .line 337
    :goto_2
    sget-object v5, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v5}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v3, :cond_1

    .line 339
    move-object/from16 v0, p4

    iget-object v5, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v12, "status"

    invoke-virtual {v5, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 340
    int-to-float v4, v4

    const/high16 v5, 0x42480000    # 50.0f

    mul-float/2addr v5, v11

    sub-float/2addr v4, v5

    float-to-int v4, v4

    .line 341
    invoke-virtual/range {p4 .. p4}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v12

    invoke-virtual {v12}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v13

    iget-object v5, v12, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    if-eqz v5, :cond_7

    iget-object v5, v12, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v14, "flipboard"

    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    const/4 v5, 0x1

    :goto_3
    if-eq v13, v12, :cond_8

    if-nez v5, :cond_8

    const/4 v5, 0x1

    :goto_4
    if-eqz v5, :cond_1

    .line 342
    int-to-float v4, v4

    const/high16 v5, 0x41700000    # 15.0f

    mul-float/2addr v5, v11

    sub-float/2addr v4, v5

    float-to-int v4, v4

    .line 351
    :cond_1
    :goto_5
    int-to-float v5, v6

    mul-float/2addr v5, v2

    float-to-int v12, v5

    .line 352
    int-to-float v4, v4

    mul-float/2addr v2, v4

    float-to-int v6, v2

    .line 353
    int-to-float v2, v12

    div-float/2addr v2, v11

    float-to-int v13, v2

    .line 354
    int-to-float v2, v6

    div-float/2addr v2, v11

    float-to-int v4, v2

    .line 355
    if-lez v13, :cond_2

    if-gtz v4, :cond_a

    .line 356
    :cond_2
    const v2, -0xc350

    .line 562
    :cond_3
    :goto_6
    return v2

    .line 315
    :cond_4
    sget-object v4, Lflipboard/service/Section;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_32

    .line 316
    const v2, 0x3f19999a    # 0.6f

    move v10, v2

    goto/16 :goto_0

    .line 327
    :cond_5
    const/high16 v2, 0x40000000    # 2.0f

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, v10

    sub-float/2addr v2, v3

    goto/16 :goto_1

    .line 336
    :cond_6
    const/4 v3, 0x0

    goto :goto_2

    .line 341
    :cond_7
    const/4 v5, 0x0

    goto :goto_3

    :cond_8
    const/4 v5, 0x0

    goto :goto_4

    .line 345
    :cond_9
    int-to-float v4, v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v5, v11

    sub-float/2addr v4, v5

    float-to-int v4, v4

    .line 346
    invoke-virtual/range {p4 .. p4}, Lflipboard/objs/FeedItem;->ab()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 347
    int-to-float v4, v4

    const/high16 v5, 0x42200000    # 40.0f

    mul-float/2addr v5, v11

    sub-float/2addr v4, v5

    float-to-int v4, v4

    goto :goto_5

    .line 359
    :cond_a
    int-to-float v2, v12

    int-to-float v5, v6

    div-float v14, v2, v5

    .line 360
    const/4 v5, 0x0

    .line 361
    if-eqz p5, :cond_b

    move-object/from16 v0, p2

    iget-boolean v2, v0, Lflipboard/objs/SectionPageTemplate$Area;->f:Z

    if-nez v2, :cond_c

    :cond_b
    if-nez p5, :cond_11

    move-object/from16 v0, p2

    iget-boolean v2, v0, Lflipboard/objs/SectionPageTemplate$Area;->g:Z

    if-eqz v2, :cond_11

    :cond_c
    const/4 v2, 0x1

    .line 363
    :goto_7
    sget-object v15, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v15}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v15

    .line 364
    move-object/from16 v0, p4

    iget-object v0, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    move-object/from16 v16, v0

    const-string v17, "status"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_13

    .line 365
    const v2, 0x3f99999a    # 1.2f

    cmpl-float v2, v14, v2

    if-lez v2, :cond_31

    .line 366
    const/16 v2, 0xc8

    .line 369
    :goto_8
    const v3, 0x3f4ccccd    # 0.8f

    cmpg-float v3, v14, v3

    if-gez v3, :cond_30

    .line 370
    add-int/lit16 v2, v2, -0xc8

    move v3, v2

    .line 376
    :goto_9
    invoke-virtual/range {p4 .. p4}, Lflipboard/objs/FeedItem;->D()Ljava/lang/String;

    move-result-object v2

    .line 377
    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_e

    .line 378
    :cond_d
    move-object/from16 v0, p4

    iget-object v5, v0, Lflipboard/objs/FeedItem;->aB:Ljava/util/List;

    if-eqz v5, :cond_e

    move-object/from16 v0, p4

    iget-object v5, v0, Lflipboard/objs/FeedItem;->aB:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_e

    .line 379
    move-object/from16 v0, p4

    iget-object v2, v0, Lflipboard/objs/FeedItem;->aB:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 382
    :cond_e
    if-eqz v2, :cond_12

    .line 383
    sget-object v5, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v5}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0900f4

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v11

    float-to-int v5, v5

    .line 384
    sget-object v6, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v6}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0900f6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v11

    float-to-int v6, v6

    .line 385
    div-int v7, v13, v5

    .line 386
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    int-to-float v8, v8

    int-to-float v7, v7

    div-float v7, v8, v7

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v7, v8

    .line 387
    const/4 v8, 0x1

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 388
    mul-int/2addr v7, v13

    mul-int/2addr v5, v7

    int-to-float v5, v5

    iget v7, v15, Landroid/util/DisplayMetrics;->scaledDensity:F

    mul-float/2addr v5, v7

    div-float/2addr v5, v11

    float-to-int v5, v5

    .line 389
    div-int v7, v13, v6

    .line 390
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    int-to-float v2, v2

    int-to-float v7, v7

    div-float/2addr v2, v7

    float-to-double v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v2, v8

    .line 391
    const/4 v7, 0x1

    invoke-static {v2, v7}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 392
    mul-int/2addr v2, v13

    mul-int/2addr v2, v6

    int-to-float v2, v2

    iget v6, v15, Landroid/util/DisplayMetrics;->scaledDensity:F

    mul-float/2addr v2, v6

    div-float/2addr v2, v11

    float-to-int v6, v2

    .line 394
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v2, v2, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v2, :cond_2f

    .line 396
    add-int/lit8 v2, v4, -0x46

    .line 398
    :goto_a
    mul-int v4, v13, v2

    div-int/2addr v4, v5

    int-to-float v4, v4

    .line 401
    const/high16 v5, 0x40800000    # 4.0f

    cmpl-float v5, v4, v5

    if-ltz v5, :cond_f

    .line 402
    const-wide/high16 v8, -0x3fdc000000000000L    # -10.0

    const/high16 v5, 0x42c80000    # 100.0f

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    float-to-double v4, v4

    const-wide v10, 0x3ff199999999999aL    # 1.1

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v4, v8

    double-to-int v4, v4

    .line 403
    add-int/2addr v3, v4

    .line 406
    :cond_f
    mul-int/2addr v2, v13

    div-int v2, v6, v2

    int-to-float v2, v2

    .line 407
    float-to-double v4, v2

    const-wide v6, 0x3fe999999999999aL    # 0.8

    cmpl-double v4, v4, v6

    if-lez v4, :cond_10

    .line 408
    const v4, -0x3b448000    # -1500.0f

    mul-float/2addr v2, v4

    float-to-int v2, v2

    .line 409
    add-int/2addr v3, v2

    :cond_10
    move v2, v3

    .line 410
    goto/16 :goto_6

    .line 361
    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 413
    :cond_12
    add-int/lit16 v2, v3, -0x2710

    .line 414
    goto/16 :goto_6

    :cond_13
    move-object/from16 v0, p4

    iget-object v0, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    move-object/from16 v16, v0

    const-string v17, "image"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_14

    .line 417
    if-eqz v2, :cond_2e

    move-object/from16 v0, p4

    invoke-virtual {v0, v7, v8}, Lflipboard/objs/FeedItem;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_2e

    .line 419
    const/16 v2, 0xfa

    .line 422
    int-to-float v3, v6

    const/high16 v4, 0x42580000    # 54.0f

    mul-float/2addr v4, v11

    add-float/2addr v3, v4

    float-to-int v3, v3

    .line 425
    :goto_b
    move-object/from16 v0, p4

    invoke-static {v0, v12, v3}, Lflipboard/gui/section/Group;->a(Lflipboard/objs/FeedItem;II)I

    move-result v3

    add-int/2addr v2, v3

    .line 426
    goto/16 :goto_6

    :cond_14
    const-string v16, "web"

    move-object/from16 v0, p4

    iget-object v0, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_16

    .line 428
    const/16 v2, 0xb4

    if-lt v13, v2, :cond_15

    const/16 v2, 0x140

    if-ge v4, v2, :cond_2d

    .line 429
    :cond_15
    const v2, -0x186a0

    goto/16 :goto_6

    .line 431
    :cond_16
    move-object/from16 v0, p4

    iget-object v5, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v16, "post"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_23

    .line 432
    invoke-virtual/range {p4 .. p4}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v12

    .line 433
    const/16 v5, 0x96

    .line 435
    if-eqz v2, :cond_2c

    move-object/from16 v0, p4

    invoke-virtual {v0, v7, v8}, Lflipboard/objs/FeedItem;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 436
    const/16 v2, 0xfa

    .line 439
    :goto_c
    const/4 v6, 0x0

    .line 440
    const/4 v5, 0x0

    .line 441
    invoke-virtual/range {p4 .. p4}, Lflipboard/objs/FeedItem;->I()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_17

    .line 442
    sget-object v5, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v5}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0900f1

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v11

    float-to-int v6, v5

    .line 443
    sget-object v5, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v5, v5, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v5, :cond_1d

    const v5, 0x7f0900f3

    .line 444
    :goto_d
    sget-object v7, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v7}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v11

    float-to-int v5, v5

    .line 445
    div-int v7, v13, v6

    int-to-double v8, v7

    const-wide v16, 0x3ff999999999999aL    # 1.6

    mul-double v8, v8, v16

    double-to-int v7, v8

    .line 446
    invoke-virtual/range {p4 .. p4}, Lflipboard/objs/FeedItem;->I()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    int-to-float v8, v8

    int-to-float v7, v7

    div-float v7, v8, v7

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v7, v8

    .line 447
    mul-int/2addr v7, v13

    mul-int/2addr v6, v7

    int-to-float v6, v6

    iget v7, v15, Landroid/util/DisplayMetrics;->scaledDensity:F

    mul-float/2addr v6, v7

    div-float/2addr v6, v11

    float-to-int v6, v6

    .line 448
    div-int v7, v13, v5

    int-to-double v8, v7

    const-wide v16, 0x3ff999999999999aL    # 1.6

    mul-double v8, v8, v16

    double-to-int v7, v8

    .line 449
    invoke-virtual/range {p4 .. p4}, Lflipboard/objs/FeedItem;->I()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    int-to-float v8, v8

    int-to-float v7, v7

    div-float v7, v8, v7

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v7, v8

    .line 450
    mul-int/2addr v7, v13

    mul-int/2addr v5, v7

    int-to-float v5, v5

    iget v7, v15, Landroid/util/DisplayMetrics;->scaledDensity:F

    mul-float/2addr v5, v7

    div-float/2addr v5, v11

    float-to-int v5, v5

    .line 453
    :cond_17
    const/4 v8, 0x0

    .line 454
    const/4 v7, 0x0

    .line 455
    invoke-virtual/range {p4 .. p4}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v9

    .line 456
    if-nez v9, :cond_18

    .line 457
    invoke-virtual/range {p4 .. p4}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v9

    invoke-virtual {v9}, Lflipboard/objs/FeedItem;->G()Ljava/lang/String;

    move-result-object v9

    .line 459
    :cond_18
    if-eqz v9, :cond_1a

    .line 460
    sget-object v7, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v7}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0900e3

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v11

    float-to-int v7, v7

    .line 461
    div-int v8, v13, v7

    int-to-float v8, v8

    const v14, 0x3fcccccd    # 1.6f

    mul-float/2addr v8, v14

    float-to-int v8, v8

    .line 462
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    int-to-float v9, v9

    int-to-float v8, v8

    div-float v8, v9, v8

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v8, v8

    .line 463
    mul-int/2addr v8, v13

    mul-int/2addr v7, v8

    int-to-float v7, v7

    iget v8, v15, Landroid/util/DisplayMetrics;->scaledDensity:F

    mul-float/2addr v7, v8

    div-float/2addr v7, v11

    float-to-int v8, v7

    .line 465
    sget-object v7, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v7}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v9, 0x7f0900e2

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v11

    float-to-int v9, v7

    .line 468
    sget-object v7, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v7, v7, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v7, :cond_19

    if-nez v12, :cond_1e

    :cond_19
    const/4 v7, 0x5

    .line 469
    :goto_e
    int-to-float v7, v7

    div-int v14, v13, v9

    int-to-float v14, v14

    const v16, 0x3fcccccd    # 1.6f

    mul-float v14, v14, v16

    invoke-static {v7, v14}, Ljava/lang/Math;->min(FF)F

    move-result v7

    float-to-int v7, v7

    .line 470
    mul-int/2addr v7, v9

    mul-int/2addr v7, v13

    int-to-float v7, v7

    iget v9, v15, Landroid/util/DisplayMetrics;->scaledDensity:F

    mul-float/2addr v7, v9

    div-float/2addr v7, v11

    float-to-int v7, v7

    .line 473
    :cond_1a
    add-int/2addr v6, v8

    .line 474
    if-eqz v12, :cond_1b

    const/high16 v8, 0x3e800000    # 0.25f

    cmpl-float v8, v10, v8

    if-lez v8, :cond_1b

    .line 475
    iget v8, v12, Lflipboard/objs/Image;->f:I

    iget v9, v12, Lflipboard/objs/Image;->g:I

    mul-int/2addr v8, v9

    int-to-float v8, v8

    div-float/2addr v8, v11

    div-float/2addr v8, v11

    const/high16 v9, 0x40800000    # 4.0f

    mul-float/2addr v8, v9

    float-to-int v8, v8

    .line 476
    add-int/2addr v6, v8

    .line 479
    :cond_1b
    sget-object v8, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v8}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v8

    if-eqz v8, :cond_1c

    if-eqz v3, :cond_1c

    invoke-virtual/range {p4 .. p4}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v3

    move-object/from16 v0, p4

    if-eq v3, v0, :cond_1c

    .line 480
    invoke-virtual/range {p4 .. p4}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v3

    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->D()Ljava/lang/String;

    move-result-object v3

    .line 481
    add-int/lit8 v4, v4, -0x28

    .line 482
    if-eqz v3, :cond_1c

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1c

    .line 483
    add-int/lit8 v4, v4, -0x14

    .line 486
    :cond_1c
    mul-int v3, v13, v4

    int-to-float v3, v3

    int-to-float v6, v6

    div-float/2addr v3, v6

    .line 487
    if-gtz v4, :cond_1f

    .line 488
    const v3, 0x186a0

    sub-int/2addr v2, v3

    .line 489
    goto/16 :goto_6

    .line 443
    :cond_1d
    const v5, 0x7f0900f0

    goto/16 :goto_d

    .line 468
    :cond_1e
    const/4 v7, 0x2

    goto :goto_e

    .line 490
    :cond_1f
    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v6, v3, v6

    if-ltz v6, :cond_20

    .line 492
    const/high16 v4, -0x3c060000    # -500.0f

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v3, v5

    const/high16 v5, 0x41900000    # 18.0f

    invoke-static {v3, v5}, Ljava/lang/Math;->min(FF)F

    move-result v3

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 493
    add-int/2addr v2, v3

    .line 494
    goto/16 :goto_6

    .line 496
    :cond_20
    add-int v3, v5, v7

    int-to-float v3, v3

    .line 497
    invoke-virtual/range {p4 .. p4}, Lflipboard/objs/FeedItem;->f()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_21

    const/high16 v5, 0x3e800000    # 0.25f

    cmpl-float v5, v10, v5

    if-lez v5, :cond_21

    .line 498
    sget-object v5, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v5, v5, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v5, :cond_22

    .line 500
    const v5, 0x47aae600    # 87500.0f

    add-float/2addr v3, v5

    .line 506
    :cond_21
    :goto_f
    mul-int/2addr v4, v13

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 507
    float-to-double v4, v3

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_3

    .line 508
    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    float-to-double v6, v3

    const-wide v8, 0x3ff3333333333333L    # 1.2

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    const-wide v6, 0x3f847ae147ae147bL    # 0.01

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    .line 509
    const-wide/high16 v6, -0x3fa7000000000000L    # -100.0

    div-double v4, v6, v4

    double-to-int v4, v4

    .line 510
    add-int/2addr v2, v4

    .line 511
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "tooSmall("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 502
    :cond_22
    const v5, 0x47127c00    # 37500.0f

    add-float/2addr v3, v5

    goto :goto_f

    .line 514
    :cond_23
    move-object/from16 v0, p4

    iget-object v2, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v3, "video"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 515
    move-object/from16 v0, p4

    iget-object v2, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v2, :cond_24

    const/16 v2, 0x1e

    .line 518
    :goto_10
    int-to-float v3, v6

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v4, v11

    sub-float/2addr v3, v4

    const/high16 v4, 0x43480000    # 200.0f

    mul-float/2addr v4, v11

    sub-float/2addr v3, v4

    int-to-float v2, v2

    mul-float/2addr v2, v11

    sub-float v2, v3, v2

    .line 519
    float-to-int v2, v2

    move-object/from16 v0, p4

    invoke-static {v0, v12, v2}, Lflipboard/gui/section/Group;->a(Lflipboard/objs/FeedItem;II)I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    .line 520
    goto/16 :goto_6

    .line 515
    :cond_24
    const/4 v2, 0x0

    goto :goto_10

    .line 520
    :cond_25
    move-object/from16 v0, p4

    iget-object v2, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v3, "audio"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 522
    float-to-double v2, v14

    const-wide v6, 0x3fe6666666666666L    # 0.7

    cmpl-double v2, v2, v6

    if-lez v2, :cond_26

    float-to-double v2, v14

    const-wide v6, 0x3ff4cccccccccccdL    # 1.3

    cmpg-double v2, v2, v6

    if-gez v2, :cond_26

    .line 523
    const/16 v2, 0x1f4

    .line 527
    :goto_11
    const v3, 0x4826fe00    # 171000.0f

    mul-int/2addr v4, v13

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 532
    float-to-double v4, v3

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_3

    .line 533
    const-wide/high16 v4, -0x3fa7000000000000L    # -100.0

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    float-to-double v8, v3

    sub-double/2addr v6, v8

    const-wide v8, 0x3f847ae147ae147bL    # 0.01

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    div-double/2addr v4, v6

    double-to-int v3, v4

    .line 534
    add-int/2addr v2, v3

    goto/16 :goto_6

    .line 526
    :cond_26
    const/16 v2, -0x1388

    goto :goto_11

    .line 535
    :cond_27
    move-object/from16 v0, p4

    iget-object v2, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v3, "section"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 538
    move-object/from16 v0, p4

    invoke-static {v0, v12, v6}, Lflipboard/gui/section/Group;->a(Lflipboard/objs/FeedItem;II)I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    .line 539
    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v3, v14, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-double v4, v3

    const-wide v6, 0x3fd3333333333333L    # 0.3

    cmpl-double v3, v4, v6

    if-lez v3, :cond_28

    .line 540
    add-int/lit16 v2, v2, -0x1388

    .line 542
    :cond_28
    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 543
    mul-int/lit8 v4, v12, 0x3

    if-le v3, v4, :cond_3

    .line 546
    add-int/lit16 v2, v2, -0xbb8

    goto/16 :goto_6

    .line 548
    :cond_29
    move-object/from16 v0, p4

    iget-object v2, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v3, "synthetic-client-profile-page"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 550
    move-object/from16 v0, p2

    iget v2, v0, Lflipboard/objs/SectionPageTemplate$Area;->a:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2a

    move-object/from16 v0, p2

    iget v2, v0, Lflipboard/objs/SectionPageTemplate$Area;->b:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2a

    .line 551
    const/16 v2, 0x2710

    goto/16 :goto_6

    .line 553
    :cond_2a
    const/16 v2, -0x2710

    goto/16 :goto_6

    .line 556
    :cond_2b
    const/16 v2, -0x2710

    goto/16 :goto_6

    :cond_2c
    move v2, v5

    goto/16 :goto_c

    :cond_2d
    move v2, v5

    goto/16 :goto_6

    :cond_2e
    move v2, v5

    move v3, v6

    goto/16 :goto_b

    :cond_2f
    move v2, v4

    goto/16 :goto_a

    :cond_30
    move v3, v2

    goto/16 :goto_9

    :cond_31
    move v2, v5

    goto/16 :goto_8

    :cond_32
    move v10, v2

    goto/16 :goto_0
.end method

.method private static a(Lflipboard/objs/FeedItem;II)I
    .locals 12

    .prologue
    const/high16 v11, 0x40400000    # 3.0f

    const/high16 v10, -0x3d380000    # -100.0f

    const-wide v8, 0x3fb1eb851eb851ecL    # 0.07

    const/4 v0, 0x0

    .line 567
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v3

    .line 568
    int-to-float v1, p1

    int-to-float v2, p2

    div-float v4, v1, v2

    .line 569
    if-eqz v3, :cond_4

    iget v1, v3, Lflipboard/objs/Image;->f:I

    int-to-float v2, v1

    .line 571
    :goto_0
    if-eqz v3, :cond_5

    iget v1, v3, Lflipboard/objs/Image;->g:I

    int-to-float v1, v1

    .line 572
    :goto_1
    if-eqz v3, :cond_1

    .line 573
    iget v5, v3, Lflipboard/objs/Image;->f:I

    if-eqz v5, :cond_0

    iget v5, v3, Lflipboard/objs/Image;->g:I

    if-nez v5, :cond_1

    .line 574
    :cond_0
    const/high16 v2, 0x43f00000    # 480.0f

    .line 575
    const/high16 v1, 0x43a00000    # 320.0f

    .line 578
    :cond_1
    cmpl-float v5, v2, v0

    if-lez v5, :cond_8

    cmpl-float v5, v1, v0

    if-lez v5, :cond_8

    .line 579
    if-eqz v3, :cond_2

    div-float v0, v2, v1

    .line 580
    :cond_2
    sub-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 581
    float-to-double v4, v0

    const-wide v6, 0x3fc999999999999aL    # 0.2

    cmpl-double v3, v4, v6

    if-lez v3, :cond_6

    .line 582
    const-wide/high16 v4, -0x3f97000000000000L    # -200.0

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v0, v3, v0

    float-to-double v6, v0

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    div-double/2addr v4, v6

    double-to-int v0, v4

    .line 583
    add-int/lit8 v0, v0, 0x0

    .line 592
    :goto_2
    int-to-float v3, p1

    div-float v2, v3, v2

    .line 593
    int-to-float v3, p2

    div-float v1, v3, v1

    .line 595
    cmpl-float v3, v2, v11

    if-lez v3, :cond_7

    cmpl-float v3, v2, v1

    if-lez v3, :cond_7

    .line 596
    mul-float v1, v2, v10

    .line 597
    int-to-float v0, v0

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 609
    :cond_3
    :goto_3
    return v0

    :cond_4
    move v2, v0

    .line 569
    goto :goto_0

    :cond_5
    move v1, v0

    .line 571
    goto :goto_1

    .line 588
    :cond_6
    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    float-to-double v6, v0

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    div-double/2addr v4, v6

    double-to-int v0, v4

    .line 589
    add-int/lit8 v0, v0, 0x0

    goto :goto_2

    .line 599
    :cond_7
    cmpl-float v2, v1, v11

    if-lez v2, :cond_3

    .line 600
    mul-float/2addr v1, v10

    .line 601
    int-to-float v0, v0

    add-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_3

    .line 605
    :cond_8
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    mul-int/lit8 v0, v0, -0x2

    .line 606
    add-int/lit8 v0, v0, 0x0

    goto :goto_3
.end method


# virtual methods
.method final a()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    iget-object v0, p0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->S()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 624
    iget-object v0, p0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 625
    iget-object v0, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 626
    const/4 v0, 0x0

    .line 629
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 300
    iget-object v0, p0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 301
    if-nez v0, :cond_0

    move v0, v1

    .line 305
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    invoke-virtual {v2}, Lflipboard/objs/SectionPageTemplate;->a()I

    move-result v2

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 704
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 634
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v0, p0, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    iget-object v0, v0, Lflipboard/objs/SectionPageTemplate;->a:Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 635
    const-string v0, "franchise: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    iget-object v0, v0, Lflipboard/gui/section/GroupFranchiseMeta;->a:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 636
    const/4 v0, 0x0

    .line 637
    iget-object v1, p0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 638
    add-int/lit8 v1, v1, 0x1

    .line 639
    const-string v4, "\tItem "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 640
    iget-object v4, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 641
    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 642
    invoke-static {v0}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 643
    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 644
    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 635
    :cond_0
    const-string v0, "none"

    goto :goto_0

    .line 646
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 7

    .prologue
    .line 708
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 709
    const-string v0, "template"

    iget-object v1, p0, Lflipboard/gui/section/Group;->a:Lflipboard/objs/SectionPageTemplate;

    iget-object v1, v1, Lflipboard/objs/SectionPageTemplate;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    iget-object v0, p0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    .line 711
    const/4 v0, 0x0

    .line 712
    iget-object v1, p0, Lflipboard/gui/section/Group;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 713
    if-eqz v0, :cond_0

    iget-object v5, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 714
    iget-object v5, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    aput-object v5, v3, v1

    .line 715
    add-int/lit8 v1, v1, 0x1

    .line 716
    iget-object v5, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v6, "pagebox"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 717
    const-string v5, "pageboxItem"

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move v0, v1

    move v1, v0

    .line 720
    goto :goto_0

    .line 721
    :cond_1
    const-string v0, "itemIds"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 722
    const-string v0, "score"

    iget v1, p0, Lflipboard/gui/section/Group;->g:I

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 723
    const-string v0, "sectionId"

    iget-object v1, p0, Lflipboard/gui/section/Group;->d:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    iget-object v0, p0, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    if-eqz v0, :cond_2

    .line 725
    const-string v0, "pagebox"

    iget-object v1, p0, Lflipboard/gui/section/Group;->c:Lflipboard/objs/SidebarGroup;

    invoke-virtual {v1}, Lflipboard/objs/SidebarGroup;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    if-eqz v0, :cond_3

    .line 728
    const-string v0, "franchiseMeta"

    iget-object v1, p0, Lflipboard/gui/section/Group;->k:Lflipboard/gui/section/GroupFranchiseMeta;

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "franchiseGroupItemId"

    iget-object v5, v1, Lflipboard/gui/section/GroupFranchiseMeta;->a:Lflipboard/objs/FeedItem;

    iget-object v5, v5, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "franchiseGroupItemSectionId"

    iget-object v5, v1, Lflipboard/gui/section/GroupFranchiseMeta;->a:Lflipboard/objs/FeedItem;

    iget-object v5, v5, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "title"

    iget-object v5, v1, Lflipboard/gui/section/GroupFranchiseMeta;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "footerTitle"

    iget-object v5, v1, Lflipboard/gui/section/GroupFranchiseMeta;->g:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "remoteid"

    iget-object v5, v1, Lflipboard/gui/section/GroupFranchiseMeta;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "pageInFranchise"

    iget v5, v1, Lflipboard/gui/section/GroupFranchiseMeta;->e:I

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "totalPagesInFranchise"

    iget v1, v1, Lflipboard/gui/section/GroupFranchiseMeta;->f:I

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 730
    :cond_3
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 731
    return-void
.end method
