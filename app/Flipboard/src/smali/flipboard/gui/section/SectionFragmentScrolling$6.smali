.class Lflipboard/gui/section/SectionFragmentScrolling$6;
.super Ljava/lang/Object;
.source "SectionFragmentScrolling.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/service/Section;

.field final synthetic b:Ljava/lang/Object;

.field final synthetic c:Lflipboard/gui/section/SectionFragmentScrolling;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragmentScrolling;Lflipboard/service/Section;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 383
    iput-object p1, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    iput-object p2, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->a:Lflipboard/service/Section;

    iput-object p3, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->b:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 386
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->a:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 387
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->m(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/swiperefresh/FLSwipeProgressBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 388
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->c(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/SmartScrollingListView;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragmentScrolling;->m(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/swiperefresh/FLSwipeProgressBar;

    move-result-object v1

    iget-object v0, v0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, v1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->removeFooterView(Landroid/view/View;)Z

    .line 389
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflipboard/gui/section/SectionFragmentScrolling;->a(Lflipboard/gui/section/SectionFragmentScrolling;Lflipboard/swiperefresh/FLSwipeProgressBar;)Lflipboard/swiperefresh/FLSwipeProgressBar;

    .line 398
    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->a:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 399
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->o(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/section/SectionScrollingAdapter;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-virtual {v0, v1}, Lflipboard/gui/section/SectionScrollingAdapter;->a(Ljava/util/List;)V

    .line 400
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->a:Lflipboard/service/Section;

    invoke-static {v0, v1}, Lflipboard/gui/section/SectionFragmentScrolling;->a(Lflipboard/gui/section/SectionFragmentScrolling;Lflipboard/service/Section;)V

    .line 401
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->c(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/SmartScrollingListView;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragmentScrolling;->o(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/section/SectionScrollingAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/SmartScrollingListView;->setAdapter(Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;)V

    .line 402
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->p(Lflipboard/gui/section/SectionFragmentScrolling;)V

    .line 403
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->q(Lflipboard/gui/section/SectionFragmentScrolling;)V

    .line 404
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->b:Ljava/lang/Object;

    check-cast v0, Lflipboard/json/FLObject;

    .line 405
    const-string v1, "refresh"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 406
    if-eqz v0, :cond_3

    .line 407
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->l(Lflipboard/gui/section/SectionFragmentScrolling;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 412
    :cond_1
    :goto_1
    return-void

    .line 392
    :cond_2
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->m(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/swiperefresh/FLSwipeProgressBar;

    move-result-object v0

    if-nez v0, :cond_0

    .line 393
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragmentScrolling;->n(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/swiperefresh/FLSwipeProgressBar;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/section/SectionFragmentScrolling;->a(Lflipboard/gui/section/SectionFragmentScrolling;Lflipboard/swiperefresh/FLSwipeProgressBar;)Lflipboard/swiperefresh/FLSwipeProgressBar;

    .line 394
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->c(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/SmartScrollingListView;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragmentScrolling;->m(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/swiperefresh/FLSwipeProgressBar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/SmartScrollingListView;->a(Landroid/view/View;)V

    goto :goto_0

    .line 409
    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$6;->c:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->r(Lflipboard/gui/section/SectionFragmentScrolling;)V

    goto :goto_1
.end method
