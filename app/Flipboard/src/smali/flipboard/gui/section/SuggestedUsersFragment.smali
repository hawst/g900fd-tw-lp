.class public Lflipboard/gui/section/SuggestedUsersFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "SuggestedUsersFragment.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/support/v4/view/ViewPager;

.field private c:Lflipboard/gui/personal/TabStripStatic$ImageFragmentPagerAdapter;

.field private d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 41
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lflipboard/gui/section/SuggestedUsersFragment;->f:J

    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/SuggestedUsersFragment;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lflipboard/gui/section/SuggestedUsersFragment;->d:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic b(Lflipboard/gui/section/SuggestedUsersFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lflipboard/gui/section/SuggestedUsersFragment;->e:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 45
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FlipboardFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 47
    invoke-virtual {p0}, Lflipboard/gui/section/SuggestedUsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.permission.READ_CONTACTS"

    invoke-virtual {p0}, Lflipboard/gui/section/SuggestedUsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 48
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "flipboard"

    aput-object v1, v0, v3

    const-string v1, "facebook"

    aput-object v1, v0, v4

    const-string v1, "twitter"

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/service/Flap$FollowListType;->d:Lflipboard/service/Flap$FollowListType;

    invoke-virtual {v1}, Lflipboard/service/Flap$FollowListType;->name()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    const-string v1, "googleplus"

    aput-object v1, v0, v7

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/SuggestedUsersFragment;->e:Ljava/util/List;

    .line 53
    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/section/SuggestedUsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/SuggestedUsersFragment;->a:Ljava/lang/String;

    .line 54
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/SuggestedUsersFragment;->d:Landroid/util/SparseArray;

    .line 56
    const v0, 0x7f030133

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 57
    const v1, 0x7f0a0347

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    iput-object v1, p0, Lflipboard/gui/section/SuggestedUsersFragment;->b:Landroid/support/v4/view/ViewPager;

    .line 59
    new-instance v1, Lflipboard/gui/section/SuggestedUsersFragment$1;

    invoke-virtual {p0}, Lflipboard/gui/section/SuggestedUsersFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lflipboard/gui/section/SuggestedUsersFragment$1;-><init>(Lflipboard/gui/section/SuggestedUsersFragment;Landroid/support/v4/app/FragmentManager;)V

    iput-object v1, p0, Lflipboard/gui/section/SuggestedUsersFragment;->c:Lflipboard/gui/personal/TabStripStatic$ImageFragmentPagerAdapter;

    .line 121
    iget-object v1, p0, Lflipboard/gui/section/SuggestedUsersFragment;->b:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lflipboard/gui/section/SuggestedUsersFragment;->c:Lflipboard/gui/personal/TabStripStatic$ImageFragmentPagerAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 123
    const v1, 0x7f0a0346

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/personal/TabStripStatic;

    .line 124
    iget-object v2, p0, Lflipboard/gui/section/SuggestedUsersFragment;->c:Lflipboard/gui/personal/TabStripStatic$ImageFragmentPagerAdapter;

    iget-object v3, p0, Lflipboard/gui/section/SuggestedUsersFragment;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2, v3}, Lflipboard/gui/personal/TabStripStatic;->a(Lflipboard/gui/personal/TabStripStatic$ImageFragmentPagerAdapter;Landroid/support/v4/view/ViewPager;)V

    .line 125
    iget-object v2, p0, Lflipboard/gui/section/SuggestedUsersFragment;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 127
    iget-wide v2, p0, Lflipboard/gui/section/SuggestedUsersFragment;->f:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 128
    invoke-virtual {p0}, Lflipboard/gui/section/SuggestedUsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "navFrom"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 129
    new-instance v2, Lflipboard/objs/UsageEventV2;

    sget-object v3, Lflipboard/objs/UsageEventV2$EventAction;->G:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v4, Lflipboard/objs/UsageEventV2$EventCategory;->j:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v2, v3, v4}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 130
    invoke-virtual {v2, v3, v1}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v1

    .line 131
    invoke-virtual {v1}, Lflipboard/objs/UsageEventV2;->c()V

    .line 133
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lflipboard/gui/section/SuggestedUsersFragment;->f:J

    .line 135
    return-object v0

    .line 50
    :cond_1
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "flipboard"

    aput-object v1, v0, v3

    const-string v1, "facebook"

    aput-object v1, v0, v4

    const-string v1, "twitter"

    aput-object v1, v0, v5

    const-string v1, "googleplus"

    aput-object v1, v0, v6

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/SuggestedUsersFragment;->e:Ljava/util/List;

    goto/16 :goto_0
.end method
