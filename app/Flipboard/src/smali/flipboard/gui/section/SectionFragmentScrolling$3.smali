.class Lflipboard/gui/section/SectionFragmentScrolling$3;
.super Ljava/lang/Object;
.source "SectionFragmentScrolling.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lflipboard/gui/section/SectionFragmentScrolling;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragmentScrolling;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 232
    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v2, p3}, Lflipboard/gui/section/SectionFragmentScrolling;->a(Lflipboard/gui/section/SectionFragmentScrolling;I)I

    .line 233
    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v2}, Lflipboard/gui/section/SectionFragmentScrolling;->g(Lflipboard/gui/section/SectionFragmentScrolling;)I

    move-result v2

    if-eq v2, p2, :cond_1

    .line 234
    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v2}, Lflipboard/gui/section/SectionFragmentScrolling;->h(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/service/FLAdManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 235
    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v2}, Lflipboard/gui/section/SectionFragmentScrolling;->h(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/service/FLAdManager;

    move-result-object v2

    invoke-virtual {v2, p2}, Lflipboard/service/FLAdManager;->a(I)V

    .line 237
    :cond_0
    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v2, p2}, Lflipboard/gui/section/SectionFragmentScrolling;->b(Lflipboard/gui/section/SectionFragmentScrolling;I)I

    .line 239
    :cond_1
    new-instance v2, Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;

    invoke-direct {v2, p1, p2, p3}, Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;-><init>(Landroid/widget/AbsListView;II)V

    .line 240
    iget-object v3, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v3}, Lflipboard/gui/section/SectionFragmentScrolling;->i(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/section/scrolling/header/HeaderView;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 241
    iget-object v3, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v3}, Lflipboard/gui/section/SectionFragmentScrolling;->i(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/section/scrolling/header/HeaderView;

    move-result-object v3

    invoke-interface {v3, v2}, Lflipboard/gui/section/scrolling/header/HeaderView;->a(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)V

    .line 243
    :cond_2
    iget-object v3, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v3}, Lflipboard/gui/section/SectionFragmentScrolling;->j(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/FLSliderHider;

    move-result-object v3

    invoke-virtual {v3, v2}, Lflipboard/gui/FLSliderHider;->a(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)V

    .line 244
    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v2}, Lflipboard/gui/section/SectionFragmentScrolling;->a(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/service/Section;

    move-result-object v2

    iget-object v2, v2, Lflipboard/service/Section;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    .line 245
    if-nez v2, :cond_4

    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v2}, Lflipboard/gui/section/SectionFragmentScrolling;->a(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/service/Section;

    move-result-object v2

    invoke-virtual {v2}, Lflipboard/service/Section;->k()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v2}, Lflipboard/gui/section/SectionFragmentScrolling;->a(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/service/Section;

    move-result-object v2

    invoke-virtual {v2}, Lflipboard/service/Section;->l()Z

    move-result v2

    if-eqz v2, :cond_4

    move v2, v0

    .line 246
    :goto_0
    if-eqz v2, :cond_3

    .line 247
    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v2}, Lflipboard/gui/section/SectionFragmentScrolling;->c(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/SmartScrollingListView;

    move-result-object v2

    invoke-virtual {v2}, Lflipboard/gui/SmartScrollingListView;->getFooterViewsCount()I

    move-result v2

    iget-object v3, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v3}, Lflipboard/gui/section/SectionFragmentScrolling;->c(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/gui/SmartScrollingListView;

    move-result-object v3

    invoke-virtual {v3}, Lflipboard/gui/SmartScrollingListView;->getHeaderViewsCount()I

    move-result v3

    add-int/2addr v2, v3

    .line 249
    sub-int v2, p4, v2

    .line 250
    sub-int/2addr v2, p2

    .line 251
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v3}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v3

    iget v3, v3, Lflipboard/model/ConfigSetting;->FeedFetchLoadMorePagesFromEndCount:I

    if-gt v2, v3, :cond_5

    .line 252
    :goto_1
    if-eqz v0, :cond_3

    .line 253
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->a(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/service/Section;

    move-result-object v0

    invoke-virtual {v0, v1, v4, v4}, Lflipboard/service/Section;->a(ZLjava/lang/String;Landroid/os/Bundle;)Z

    .line 256
    :cond_3
    return-void

    :cond_4
    move v2, v1

    .line 245
    goto :goto_0

    :cond_5
    move v0, v1

    .line 251
    goto :goto_1
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 3

    .prologue
    .line 260
    if-nez p2, :cond_0

    .line 261
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->h(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/service/FLAdManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->k(Lflipboard/gui/section/SectionFragmentScrolling;)I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragmentScrolling;->g(Lflipboard/gui/section/SectionFragmentScrolling;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 262
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->b()V

    .line 263
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragmentScrolling;->l(Lflipboard/gui/section/SectionFragmentScrolling;)Landroid/util/SparseArray;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragmentScrolling;->g(Lflipboard/gui/section/SectionFragmentScrolling;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/FLAdManager$AdAsset;

    .line 264
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 265
    :goto_0
    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragmentScrolling;->h(Lflipboard/gui/section/SectionFragmentScrolling;)Lflipboard/service/FLAdManager;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v2}, Lflipboard/gui/section/SectionFragmentScrolling;->g(Lflipboard/gui/section/SectionFragmentScrolling;)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Lflipboard/service/FLAdManager;->a(ILflipboard/objs/Ad;)V

    .line 266
    iget-object v0, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragmentScrolling$3;->a:Lflipboard/gui/section/SectionFragmentScrolling;

    invoke-static {v1}, Lflipboard/gui/section/SectionFragmentScrolling;->g(Lflipboard/gui/section/SectionFragmentScrolling;)I

    move-result v1

    invoke-static {v0, v1}, Lflipboard/gui/section/SectionFragmentScrolling;->c(Lflipboard/gui/section/SectionFragmentScrolling;I)I

    .line 269
    :cond_0
    return-void

    .line 264
    :cond_1
    iget-object v0, v0, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    goto :goto_0
.end method
