.class Lflipboard/gui/section/MagazineConversationThreadsFragment$3;
.super Ljava/lang/Object;
.source "MagazineConversationThreadsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Lflipboard/gui/section/MagazineConversationThreadsFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/MagazineConversationThreadsFragment;ZLjava/util/List;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment$3;->c:Lflipboard/gui/section/MagazineConversationThreadsFragment;

    iput-boolean p2, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment$3;->a:Z

    iput-object p3, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment$3;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 182
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment$3;->c:Lflipboard/gui/section/MagazineConversationThreadsFragment;

    iget-object v1, v0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->a:Lflipboard/gui/section/MagazineConversationThreadsAdapter;

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment$3;->c:Lflipboard/gui/section/MagazineConversationThreadsFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadsFragment;->e(Lflipboard/gui/section/MagazineConversationThreadsFragment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v1, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->a:Z

    .line 184
    iget-boolean v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment$3;->a:Z

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment$3;->c:Lflipboard/gui/section/MagazineConversationThreadsFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadsFragment;->a(Lflipboard/gui/section/MagazineConversationThreadsFragment;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment$3;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 189
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment$3;->c:Lflipboard/gui/section/MagazineConversationThreadsFragment;

    iget-object v0, v0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->a:Lflipboard/gui/section/MagazineConversationThreadsAdapter;

    invoke-virtual {v0}, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->notifyDataSetChanged()V

    .line 193
    :cond_0
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment$3;->c:Lflipboard/gui/section/MagazineConversationThreadsFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadsFragment;->e(Lflipboard/gui/section/MagazineConversationThreadsFragment;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment$3;->c:Lflipboard/gui/section/MagazineConversationThreadsFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadsFragment;->f(Lflipboard/gui/section/MagazineConversationThreadsFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 194
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment$3;->c:Lflipboard/gui/section/MagazineConversationThreadsFragment;

    invoke-static {v0}, Lflipboard/gui/section/MagazineConversationThreadsFragment;->f(Lflipboard/gui/section/MagazineConversationThreadsFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment$3;->c:Lflipboard/gui/section/MagazineConversationThreadsFragment;

    invoke-static {v1}, Lflipboard/gui/section/MagazineConversationThreadsFragment;->f(Lflipboard/gui/section/MagazineConversationThreadsFragment;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 196
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "loading"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 197
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 200
    :cond_1
    return-void

    .line 182
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
