.class Lflipboard/gui/section/AttributionServiceInfo$2;
.super Ljava/lang/Object;
.source "AttributionServiceInfo.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/service/Section;

.field final synthetic b:Lflipboard/objs/FeedSectionLink;

.field final synthetic c:Lflipboard/gui/section/AttributionServiceInfo;


# direct methods
.method constructor <init>(Lflipboard/gui/section/AttributionServiceInfo;Lflipboard/service/Section;Lflipboard/objs/FeedSectionLink;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lflipboard/gui/section/AttributionServiceInfo$2;->c:Lflipboard/gui/section/AttributionServiceInfo;

    iput-object p2, p0, Lflipboard/gui/section/AttributionServiceInfo$2;->a:Lflipboard/service/Section;

    iput-object p3, p0, Lflipboard/gui/section/AttributionServiceInfo$2;->b:Lflipboard/objs/FeedSectionLink;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 185
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 186
    const-string v1, "source"

    sget-object v2, Lflipboard/objs/UsageEventV2$SectionNavFrom;->j:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    invoke-virtual {v2}, Lflipboard/objs/UsageEventV2$SectionNavFrom;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-string v1, "originSectionIdentifier"

    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo$2;->a:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    new-instance v1, Lflipboard/service/Section;

    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo$2;->b:Lflipboard/objs/FeedSectionLink;

    invoke-direct {v1, v2}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    iget-object v2, p0, Lflipboard/gui/section/AttributionServiceInfo$2;->c:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v2}, Lflipboard/gui/section/AttributionServiceInfo;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lflipboard/util/SocialHelper;->a(Lflipboard/service/Section;Landroid/content/Context;Landroid/os/Bundle;)V

    .line 189
    return-void
.end method
