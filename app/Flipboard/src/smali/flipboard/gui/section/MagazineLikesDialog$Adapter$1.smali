.class Lflipboard/gui/section/MagazineLikesDialog$Adapter$1;
.super Ljava/lang/Object;
.source "MagazineLikesDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/objs/CommentaryResult$Item$Commentary;

.field final synthetic b:Lflipboard/gui/section/MagazineLikesDialog$Adapter;


# direct methods
.method constructor <init>(Lflipboard/gui/section/MagazineLikesDialog$Adapter;Lflipboard/objs/CommentaryResult$Item$Commentary;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lflipboard/gui/section/MagazineLikesDialog$Adapter$1;->b:Lflipboard/gui/section/MagazineLikesDialog$Adapter;

    iput-object p2, p0, Lflipboard/gui/section/MagazineLikesDialog$Adapter$1;->a:Lflipboard/objs/CommentaryResult$Item$Commentary;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 280
    iget-object v0, p0, Lflipboard/gui/section/MagazineLikesDialog$Adapter$1;->b:Lflipboard/gui/section/MagazineLikesDialog$Adapter;

    iget-object v0, v0, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->a:Lflipboard/gui/section/MagazineLikesDialog;

    invoke-virtual {v0}, Lflipboard/gui/dialog/FLDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 281
    iget-object v1, p0, Lflipboard/gui/section/MagazineLikesDialog$Adapter$1;->a:Lflipboard/objs/CommentaryResult$Item$Commentary;

    iget-object v1, v1, Lflipboard/objs/CommentaryResult$Item$Commentary;->o:Ljava/util/List;

    .line 283
    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 284
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedSectionLink;

    move-object v3, v1

    .line 287
    :goto_0
    if-eqz v3, :cond_2

    .line 288
    new-instance v1, Lflipboard/service/Section;

    invoke-direct {v1, v3}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    .line 290
    :goto_1
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 293
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 294
    const-string v3, "source"

    const-string v4, "magazine-like"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    invoke-virtual {v1, v0, v2}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 296
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v3, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v2

    if-nez v2, :cond_0

    .line 297
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2, v1}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 299
    :cond_0
    iget-object v1, p0, Lflipboard/gui/section/MagazineLikesDialog$Adapter$1;->b:Lflipboard/gui/section/MagazineLikesDialog$Adapter;

    iget-object v1, v1, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->a:Lflipboard/gui/section/MagazineLikesDialog;

    invoke-virtual {v1}, Lflipboard/gui/section/MagazineLikesDialog;->a()V

    .line 300
    iget-object v1, p0, Lflipboard/gui/section/MagazineLikesDialog$Adapter$1;->b:Lflipboard/gui/section/MagazineLikesDialog$Adapter;

    iget-object v1, v1, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->a:Lflipboard/gui/section/MagazineLikesDialog;

    invoke-virtual {v1, v0}, Lflipboard/gui/section/MagazineLikesDialog;->startActivity(Landroid/content/Intent;)V

    .line 302
    :cond_1
    return-void

    :cond_2
    move-object v1, v2

    goto :goto_1

    :cond_3
    move-object v3, v2

    goto :goto_0
.end method
