.class Lflipboard/gui/section/SectionPage$3;
.super Lflipboard/service/FlCrashListener;
.source "SectionPage.java"


# instance fields
.field final synthetic a:Lflipboard/gui/section/SectionPage;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionPage;)V
    .locals 0

    .prologue
    .line 847
    iput-object p1, p0, Lflipboard/gui/section/SectionPage$3;->a:Lflipboard/gui/section/SectionPage;

    invoke-direct {p0}, Lflipboard/service/FlCrashListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 850
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-super {p0}, Lflipboard/service/FlCrashListener;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 851
    iget-object v1, p0, Lflipboard/gui/section/SectionPage$3;->a:Lflipboard/gui/section/SectionPage;

    iget-object v1, v1, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    invoke-static {v1}, Lflipboard/json/JSONSerializer;->d(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 852
    const-string v1, "number of items = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/section/SectionPage$3;->a:Lflipboard/gui/section/SectionPage;

    iget-object v2, v2, Lflipboard/gui/section/SectionPage;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 853
    const-string v1, "number of subViews = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/section/SectionPage$3;->a:Lflipboard/gui/section/SectionPage;

    iget-object v2, v2, Lflipboard/gui/section/SectionPage;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 854
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
