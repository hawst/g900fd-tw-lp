.class public Lflipboard/gui/section/ContributorsDialog;
.super Lflipboard/gui/dialog/FLDialogFragment;
.source "ContributorsDialog.java"


# instance fields
.field public j:Lflipboard/service/Section;

.field public k:Lflipboard/gui/section/ContributorsDialog$ContributorChangedListener;

.field private l:Lflipboard/gui/section/ContributorsDialog$Adapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogFragment;-><init>()V

    .line 295
    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/ContributorsDialog;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog;->j:Lflipboard/service/Section;

    return-object v0
.end method

.method static synthetic b(Lflipboard/gui/section/ContributorsDialog;)Lflipboard/gui/section/ContributorsDialog$Adapter;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog;->l:Lflipboard/gui/section/ContributorsDialog$Adapter;

    return-object v0
.end method

.method static synthetic c(Lflipboard/gui/section/ContributorsDialog;)Lflipboard/gui/section/ContributorsDialog$ContributorChangedListener;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog;->k:Lflipboard/gui/section/ContributorsDialog$ContributorChangedListener;

    return-object v0
.end method


# virtual methods
.method public final l_()Z
    .locals 1

    .prologue
    .line 307
    invoke-virtual {p0}, Lflipboard/gui/section/ContributorsDialog;->a()V

    .line 308
    const/4 v0, 0x1

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 64
    invoke-super {p0, p1}, Lflipboard/gui/dialog/FLDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 65
    const v0, 0x7f0e0005

    invoke-virtual {p0, v0}, Lflipboard/gui/section/ContributorsDialog;->a(I)V

    .line 67
    iget-object v0, p0, Lflipboard/gui/section/ContributorsDialog;->j:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->G:Ljava/util/List;

    if-nez v0, :cond_0

    .line 68
    invoke-virtual {p0}, Lflipboard/gui/section/ContributorsDialog;->a()V

    .line 70
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 75
    iget-object v1, p0, Lflipboard/gui/section/ContributorsDialog;->j:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->G:Ljava/util/List;

    if-nez v1, :cond_0

    .line 89
    :goto_0
    return-object v0

    .line 78
    :cond_0
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f030057

    invoke-static {v1, v2, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 79
    const v0, 0x7f0a004c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 80
    invoke-virtual {v0, v4, v5}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    .line 81
    invoke-virtual {p0}, Lflipboard/gui/section/ContributorsDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0, v1, p0}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/gui/dialog/FLDialogFragment;)V

    .line 82
    const v1, 0x7f0a0143

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 83
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0d01f4

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lflipboard/gui/section/ContributorsDialog;->j:Lflipboard/service/Section;

    invoke-virtual {v4}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 85
    const v0, 0x7f0a0144

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 86
    new-instance v1, Lflipboard/gui/section/ContributorsDialog$Adapter;

    invoke-direct {v1, p0}, Lflipboard/gui/section/ContributorsDialog$Adapter;-><init>(Lflipboard/gui/section/ContributorsDialog;)V

    iput-object v1, p0, Lflipboard/gui/section/ContributorsDialog;->l:Lflipboard/gui/section/ContributorsDialog$Adapter;

    .line 87
    iget-object v1, p0, Lflipboard/gui/section/ContributorsDialog;->l:Lflipboard/gui/section/ContributorsDialog$Adapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    move-object v0, v2

    .line 89
    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 94
    invoke-super {p0}, Lflipboard/gui/dialog/FLDialogFragment;->onDestroyView()V

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/section/ContributorsDialog;->k:Lflipboard/gui/section/ContributorsDialog$ContributorChangedListener;

    .line 96
    return-void
.end method
