.class public Lflipboard/gui/section/SectionScrollingAdapter;
.super Landroid/widget/BaseAdapter;
.source "SectionScrollingAdapter.java"

# interfaces
.implements Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static j:I

.field private static k:I


# instance fields
.field private final c:Lflipboard/service/FLAdManager;

.field private final d:Lflipboard/gui/SmartScrollingListView;

.field private final e:Lflipboard/service/Section;

.field private final f:Landroid/view/View$OnClickListener;

.field private g:Landroid/view/View$OnClickListener;

.field private final h:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lflipboard/service/FLAdManager$AdAsset;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field private final l:I

.field private final m:I

.field private final n:Z

.field private final o:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Landroid/view/LayoutInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x5

    const/4 v3, 0x4

    .line 71
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "post"

    aput-object v1, v0, v5

    const-string v1, "image"

    aput-object v1, v0, v6

    const-string v1, "group"

    aput-object v1, v0, v7

    const/4 v1, 0x3

    const-string v2, "video"

    aput-object v2, v0, v1

    const-string v1, "audio"

    aput-object v1, v0, v3

    const-string v1, "status"

    aput-object v1, v0, v4

    const/4 v1, 0x6

    const-string v2, "pageboxList"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "pageboxExpandable"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "ad"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "section"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lflipboard/gui/section/SectionScrollingAdapter;->a:Ljava/util/List;

    .line 72
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x3

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lflipboard/gui/section/SectionScrollingAdapter;->b:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/view/View$OnClickListener;Lflipboard/service/Section;Landroid/view/View$OnClickListener;Lflipboard/gui/SmartScrollingListView;Landroid/util/SparseArray;Lflipboard/service/FLAdManager;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View$OnClickListener;",
            "Lflipboard/service/Section;",
            "Landroid/view/View$OnClickListener;",
            "Lflipboard/gui/SmartScrollingListView;",
            "Landroid/util/SparseArray",
            "<",
            "Lflipboard/service/FLAdManager$AdAsset;",
            ">;",
            "Lflipboard/service/FLAdManager;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 83
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->n:Z

    .line 80
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->o:Ljava/util/HashMap;

    .line 84
    invoke-virtual {p4}, Lflipboard/gui/SmartScrollingListView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 85
    iput-object p2, p0, Lflipboard/gui/section/SectionScrollingAdapter;->e:Lflipboard/service/Section;

    .line 86
    iput-object p1, p0, Lflipboard/gui/section/SectionScrollingAdapter;->g:Landroid/view/View$OnClickListener;

    .line 87
    iput-object p3, p0, Lflipboard/gui/section/SectionScrollingAdapter;->f:Landroid/view/View$OnClickListener;

    .line 88
    iput-object p4, p0, Lflipboard/gui/section/SectionScrollingAdapter;->d:Lflipboard/gui/SmartScrollingListView;

    .line 89
    iput-object p5, p0, Lflipboard/gui/section/SectionScrollingAdapter;->h:Landroid/util/SparseArray;

    .line 90
    iput-object p6, p0, Lflipboard/gui/section/SectionScrollingAdapter;->c:Lflipboard/service/FLAdManager;

    .line 91
    const-string v0, "layout_inflater"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->p:Landroid/view/LayoutInflater;

    .line 93
    invoke-virtual {p2}, Lflipboard/service/Section;->m()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->i:Ljava/util/List;

    .line 94
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->i:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/gui/section/SectionScrollingAdapter;->a(Ljava/util/List;)V

    .line 96
    invoke-virtual {p2, v3}, Lflipboard/service/Section;->d(Z)Z

    .line 97
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09008f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lflipboard/gui/section/SectionScrollingAdapter;->j:I

    .line 98
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09008e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lflipboard/gui/section/SectionScrollingAdapter;->k:I

    .line 99
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->l:I

    .line 100
    iput v3, p0, Lflipboard/gui/section/SectionScrollingAdapter;->m:I

    .line 101
    return-void
.end method

.method private static b(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 367
    new-instance v2, Ljava/util/ArrayList;

    const/16 v0, 0x32

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 368
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 369
    iget-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 370
    sget-object v4, Lflipboard/gui/section/SectionScrollingAdapter;->a:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 371
    iget-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v4, "group"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 372
    iget-object v1, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-static {v1}, Lflipboard/gui/section/SectionScrollingAdapter;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 373
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    .line 374
    iget-object v6, v1, Lflipboard/objs/FeedItem;->co:Lflipboard/objs/FeedItem;

    if-nez v6, :cond_1

    .line 375
    iput-object v0, v1, Lflipboard/objs/FeedItem;->co:Lflipboard/objs/FeedItem;

    goto :goto_1

    .line 378
    :cond_2
    invoke-interface {v2, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 380
    :cond_3
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 384
    :cond_4
    return-object v2
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 395
    if-nez p2, :cond_0

    .line 396
    new-instance v1, Lflipboard/gui/section/SectionScrollingAdapter$HeaderViewHolder;

    invoke-direct {v1}, Lflipboard/gui/section/SectionScrollingAdapter$HeaderViewHolder;-><init>()V

    .line 397
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->p:Landroid/view/LayoutInflater;

    const v2, 0x7f030077

    invoke-virtual {v0, v2, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 398
    const v0, 0x7f0a0041

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lflipboard/gui/section/SectionScrollingAdapter$HeaderViewHolder;->a:Landroid/widget/TextView;

    .line 399
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 404
    :goto_0
    invoke-virtual {p0, p1}, Lflipboard/gui/section/SectionScrollingAdapter;->b(I)J

    move-result-wide v2

    .line 405
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 406
    iget-object v0, v0, Lflipboard/gui/section/SectionScrollingAdapter$HeaderViewHolder;->a:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 419
    :goto_1
    invoke-virtual {p2}, Landroid/view/View;->requestLayout()V

    .line 420
    return-object p2

    .line 401
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/SectionScrollingAdapter$HeaderViewHolder;

    goto :goto_0

    .line 408
    :cond_1
    iget-object v1, v0, Lflipboard/gui/section/SectionScrollingAdapter$HeaderViewHolder;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 409
    const/4 v1, 0x0

    .line 410
    invoke-virtual {p0, p1}, Lflipboard/gui/section/SectionScrollingAdapter;->a(I)Lflipboard/objs/FeedItem;

    move-result-object v2

    .line 411
    iget-object v2, v2, Lflipboard/objs/FeedItem;->co:Lflipboard/objs/FeedItem;

    .line 412
    if-eqz v2, :cond_2

    .line 413
    iget-object v3, v2, Lflipboard/objs/FeedItem;->e:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 414
    iget-object v1, v2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    .line 417
    :cond_2
    iget-object v0, v0, Lflipboard/gui/section/SectionScrollingAdapter$HeaderViewHolder;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final a(I)Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 172
    if-ltz p1, :cond_0

    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 173
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 175
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILflipboard/objs/FeedItem;)V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 106
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->i:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/section/SectionScrollingAdapter;->notifyDataSetChanged()V

    .line 112
    :cond_0
    return-void

    .line 108
    :cond_1
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->i:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method final a(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 115
    invoke-static {p1}, Lflipboard/gui/section/SectionScrollingAdapter;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 116
    new-instance v0, Lflipboard/gui/section/ListSingleThreadWrapper;

    invoke-direct {v0, v3}, Lflipboard/gui/section/ListSingleThreadWrapper;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->i:Ljava/util/List;

    move v1, v2

    .line 119
    :goto_0
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->e:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 120
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->e:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    .line 121
    invoke-virtual {v0}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v4

    .line 122
    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lflipboard/objs/SidebarGroup;->b()Z

    move-result v5

    if-eqz v5, :cond_0

    iget v5, v4, Lflipboard/objs/SidebarGroup$RenderHints;->c:I

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-gt v5, v6, :cond_0

    iget-boolean v5, v0, Lflipboard/objs/SidebarGroup;->b:Z

    if-eqz v5, :cond_0

    .line 124
    new-instance v5, Lflipboard/objs/FeedItem;

    invoke-direct {v5}, Lflipboard/objs/FeedItem;-><init>()V

    .line 125
    const-string v6, "pagebox"

    iput-object v6, v5, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 126
    iget-object v6, v0, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    iput-object v6, v5, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    .line 127
    iget-object v6, v0, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    iput-object v6, v5, Lflipboard/objs/FeedItem;->c:Ljava/lang/String;

    .line 128
    iget-object v6, v4, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    iput-object v6, v5, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    .line 129
    iget-object v6, v0, Lflipboard/objs/SidebarGroup;->c:Ljava/lang/String;

    iput-object v6, v5, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    .line 130
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v5, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    .line 131
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->i:Ljava/util/List;

    iget v4, v4, Lflipboard/objs/SidebarGroup$RenderHints;->c:I

    invoke-interface {v0, v4, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 119
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 136
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/section/SectionScrollingAdapter;->getCount()I

    move-result v3

    .line 137
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->d:Lflipboard/gui/SmartScrollingListView;

    invoke-virtual {v0}, Lflipboard/gui/SmartScrollingListView;->getWidth()I

    move-result v4

    .line 138
    if-lez v4, :cond_6

    move v1, v2

    .line 139
    :goto_1
    if-ge v1, v3, :cond_6

    .line 140
    invoke-virtual {p0, v1}, Lflipboard/gui/section/SectionScrollingAdapter;->a(I)Lflipboard/objs/FeedItem;

    move-result-object v5

    .line 142
    iget-object v0, v5, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->o:Ljava/util/HashMap;

    iget-object v6, v5, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 143
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->o:Ljava/util/HashMap;

    iget-object v6, v5, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 144
    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v6, v6, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v6, :cond_3

    if-nez v0, :cond_3

    .line 145
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "height in cache is 0"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_2
    invoke-virtual {p0, v1, v8, v8}, Lflipboard/gui/section/SectionScrollingAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 149
    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v0, v6, v7}, Landroid/view/View;->measure(II)V

    .line 150
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 152
    :cond_3
    iget-object v6, p0, Lflipboard/gui/section/SectionScrollingAdapter;->d:Lflipboard/gui/SmartScrollingListView;

    iget-object v6, v6, Lflipboard/gui/SmartScrollingListView;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v6, v1, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 153
    iget-object v6, v5, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v6, :cond_5

    .line 154
    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v6, v6, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v6, :cond_4

    if-nez v0, :cond_4

    .line 155
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "height putting into cache is 0"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :cond_4
    if-eqz v0, :cond_5

    .line 158
    iget-object v6, p0, Lflipboard/gui/section/SectionScrollingAdapter;->o:Ljava/util/HashMap;

    iget-object v5, v5, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 163
    :cond_6
    invoke-virtual {p0}, Lflipboard/gui/section/SectionScrollingAdapter;->notifyDataSetChanged()V

    .line 164
    return-void
.end method

.method public final b(I)J
    .locals 3

    .prologue
    .line 431
    const/4 v0, 0x0

    .line 432
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lflipboard/gui/section/SectionScrollingAdapter;->getCount()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 433
    invoke-virtual {p0, p1}, Lflipboard/gui/section/SectionScrollingAdapter;->a(I)Lflipboard/objs/FeedItem;

    move-result-object v1

    .line 434
    iget-object v1, v1, Lflipboard/objs/FeedItem;->co:Lflipboard/objs/FeedItem;

    .line 435
    if-eqz v1, :cond_0

    .line 436
    iget-object v2, v1, Lflipboard/objs/FeedItem;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 437
    iget-object v0, v1, Lflipboard/objs/FeedItem;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 441
    :cond_0
    int-to-long v0, v0

    return-wide v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lflipboard/gui/section/SectionScrollingAdapter;->a(I)Lflipboard/objs/FeedItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 181
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 10

    .prologue
    const/16 v6, 0xa

    const/4 v5, 0x7

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 187
    invoke-virtual {p0, p1}, Lflipboard/gui/section/SectionScrollingAdapter;->a(I)Lflipboard/objs/FeedItem;

    move-result-object v8

    invoke-virtual {v8}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v8}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v3, 0x1f4

    if-lt v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v8}, Lflipboard/objs/FeedItem;->i()Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-virtual {v8}, Lflipboard/objs/FeedItem;->i()Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-virtual {v8}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v3

    iget v3, v3, Lflipboard/objs/Image;->f:I

    const/16 v7, 0x1f4

    if-le v3, v7, :cond_2

    move v3, v1

    :goto_1
    invoke-virtual {v8}, Lflipboard/objs/FeedItem;->V()Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v2, 0x6

    :cond_0
    :goto_2
    return v2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    :cond_3
    invoke-virtual {v8}, Lflipboard/objs/FeedItem;->T()Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v2, 0x3

    goto :goto_2

    :cond_4
    invoke-virtual {v8}, Lflipboard/objs/FeedItem;->U()Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v2, 0x4

    goto :goto_2

    :cond_5
    const-string v7, "ad"

    iget-object v9, v8, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    const/16 v2, 0xb

    goto :goto_2

    :cond_6
    invoke-virtual {v8}, Lflipboard/objs/FeedItem;->R()Z

    move-result v7

    if-eqz v7, :cond_9

    if-eqz v0, :cond_7

    if-nez v3, :cond_7

    move v2, v4

    goto :goto_2

    :cond_7
    invoke-virtual {v8}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/Image;->b()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {v8}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    invoke-static {v0}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/Image;)I

    move-result v0

    sget v1, Lflipboard/gui/section/SectionScrollingAdapter;->k:I

    if-lt v0, v1, :cond_8

    move v2, v5

    goto :goto_2

    :cond_8
    const/4 v2, 0x5

    goto :goto_2

    :cond_9
    iget-object v7, v8, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v7, :cond_a

    iget-object v7, v8, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v9, "post"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    move v7, v1

    :goto_3
    if-eqz v7, :cond_e

    invoke-static {v8}, Lflipboard/gui/section/scrolling/AlbumViewKenBurns;->a(Lflipboard/objs/FeedItem;)Z

    move-result v7

    if-eqz v7, :cond_b

    move v2, v6

    goto :goto_2

    :cond_a
    move v7, v2

    goto :goto_3

    :cond_b
    invoke-virtual {v8}, Lflipboard/objs/FeedItem;->i()Z

    move-result v6

    if-eqz v6, :cond_0

    if-eqz v0, :cond_c

    if-nez v3, :cond_c

    move v2, v4

    goto :goto_2

    :cond_c
    invoke-virtual {v8}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/Image;->b()Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {v8}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    invoke-static {v0}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/Image;)I

    move-result v0

    sget v2, Lflipboard/gui/section/SectionScrollingAdapter;->j:I

    if-lt v0, v2, :cond_d

    iget-object v0, v8, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x2d

    if-ge v0, v2, :cond_d

    move v2, v5

    goto/16 :goto_2

    :cond_d
    move v2, v1

    goto/16 :goto_2

    :cond_e
    invoke-virtual {v8}, Lflipboard/objs/FeedItem;->Q()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, v8, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    const-string v1, "pageboxList"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    const/16 v2, 0x8

    goto/16 :goto_2

    :cond_f
    iget-object v0, v8, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    const-string v1, "pageboxExpandable"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v2, 0x9

    goto/16 :goto_2

    :cond_10
    invoke-virtual {v8}, Lflipboard/objs/FeedItem;->P()Z

    move-result v0

    if-eqz v0, :cond_11

    move v2, v6

    goto/16 :goto_2

    :cond_11
    invoke-virtual {v8}, Lflipboard/objs/FeedItem;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v2, 0xc

    goto/16 :goto_2

    :cond_12
    move v3, v2

    goto/16 :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v8, 0x7

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 255
    invoke-virtual {p0, p1}, Lflipboard/gui/section/SectionScrollingAdapter;->a(I)Lflipboard/objs/FeedItem;

    move-result-object v7

    .line 258
    invoke-virtual {p0, p1}, Lflipboard/gui/section/SectionScrollingAdapter;->getItemViewType(I)I

    move-result v4

    .line 259
    if-nez p1, :cond_2

    move v6, v2

    .line 260
    :goto_0
    if-eqz p2, :cond_3

    .line 261
    check-cast p2, Lflipboard/gui/section/scrolling/ScrollItemWrapper;

    .line 266
    :goto_1
    sget-object v0, Lflipboard/gui/section/SectionScrollingAdapter;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 267
    if-nez v6, :cond_4

    sget-object v0, Lflipboard/gui/section/SectionScrollingAdapter;->b:Ljava/util/List;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {p0, v1}, Lflipboard/gui/section/SectionScrollingAdapter;->getItemViewType(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    .line 268
    :goto_2
    iget-object v1, p0, Lflipboard/gui/section/SectionScrollingAdapter;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq p1, v1, :cond_5

    sget-object v1, Lflipboard/gui/section/SectionScrollingAdapter;->b:Ljava/util/List;

    add-int/lit8 v5, p1, 0x1

    invoke-virtual {p0, v5}, Lflipboard/gui/section/SectionScrollingAdapter;->getItemViewType(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    .line 271
    :goto_3
    packed-switch v4, :pswitch_data_0

    .line 311
    :cond_0
    :goto_4
    :pswitch_0
    iget v1, p0, Lflipboard/gui/section/SectionScrollingAdapter;->m:I

    iget v0, p2, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->c:I

    if-ne v0, v2, :cond_15

    move v0, v3

    :goto_5
    add-int/2addr v0, v1

    neg-int v0, v0

    invoke-virtual {p2, v0}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->a(I)V

    .line 312
    iget-object v1, p0, Lflipboard/gui/section/SectionScrollingAdapter;->e:Lflipboard/service/Section;

    iget-object v0, p2, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->a:Landroid/view/View;

    check-cast v0, Lflipboard/gui/section/scrolling/PhoneItemView;

    invoke-interface {v0, v1, v7}, Lflipboard/gui/section/scrolling/PhoneItemView;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 313
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "enable_scrolling_item_position"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 314
    if-eqz v0, :cond_1

    .line 315
    invoke-virtual {p2, p1}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->setItemPosition(I)V

    .line 317
    :cond_1
    return-object p2

    :cond_2
    move v6, v3

    .line 259
    goto :goto_0

    .line 263
    :cond_3
    const/4 v0, 0x0

    packed-switch v4, :pswitch_data_1

    :goto_6
    move-object p2, v0

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->p:Landroid/view/LayoutInflater;

    const v1, 0x7f0300b3

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;

    goto :goto_6

    :pswitch_2
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->p:Landroid/view/LayoutInflater;

    const v1, 0x7f0300ad

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;

    goto :goto_6

    :pswitch_3
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->p:Landroid/view/LayoutInflater;

    const v1, 0x7f0300b0

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;

    goto :goto_6

    :pswitch_4
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->p:Landroid/view/LayoutInflater;

    const v1, 0x7f0300b4

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;

    goto :goto_6

    :pswitch_5
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->p:Landroid/view/LayoutInflater;

    const v1, 0x7f0300ac

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;

    goto :goto_6

    :pswitch_6
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->p:Landroid/view/LayoutInflater;

    const v1, 0x7f0300ae

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;

    goto :goto_6

    :pswitch_7
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->p:Landroid/view/LayoutInflater;

    const v1, 0x7f0300b2

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;

    goto :goto_6

    :pswitch_8
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->p:Landroid/view/LayoutInflater;

    const v1, 0x7f0300af

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;

    goto :goto_6

    :pswitch_9
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->p:Landroid/view/LayoutInflater;

    const v1, 0x7f030131

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;

    goto :goto_6

    :pswitch_a
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->p:Landroid/view/LayoutInflater;

    const v1, 0x7f030099

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;

    goto :goto_6

    :pswitch_b
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->p:Landroid/view/LayoutInflater;

    const v1, 0x7f0300ab

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;

    goto/16 :goto_6

    :pswitch_c
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->p:Landroid/view/LayoutInflater;

    const v1, 0x7f03009c

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;

    goto/16 :goto_6

    :pswitch_d
    iget-object v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->p:Landroid/view/LayoutInflater;

    const v1, 0x7f0300b1

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/ScrollItemWrapper;

    goto/16 :goto_6

    :cond_4
    move v0, v3

    .line 267
    goto/16 :goto_2

    :cond_5
    move v1, v3

    .line 268
    goto/16 :goto_3

    .line 273
    :pswitch_e
    iget-object v0, p2, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->a:Landroid/view/View;

    check-cast v0, Lflipboard/gui/section/scrolling/CaptionView;

    .line 276
    if-nez v6, :cond_7

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {p0, v1}, Lflipboard/gui/section/SectionScrollingAdapter;->getItemViewType(I)I

    move-result v1

    if-ne v1, v8, :cond_7

    move v1, v2

    .line 277
    :goto_7
    iget-object v4, p0, Lflipboard/gui/section/SectionScrollingAdapter;->i:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-eq p1, v4, :cond_8

    add-int/lit8 v4, p1, 0x1

    invoke-virtual {p0, v4}, Lflipboard/gui/section/SectionScrollingAdapter;->getItemViewType(I)I

    move-result v4

    if-ne v4, v8, :cond_8

    move v5, v2

    .line 279
    :goto_8
    if-eqz v5, :cond_9

    if-nez v1, :cond_6

    if-eqz v6, :cond_9

    :cond_6
    move v4, v3

    .line 280
    :goto_9
    if-eqz v5, :cond_a

    move v1, v2

    .line 281
    :goto_a
    invoke-virtual {p2, v4, v1}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->a(II)V

    .line 282
    invoke-virtual {v0, v5}, Lflipboard/gui/section/scrolling/CaptionView;->setInverted(Z)V

    goto/16 :goto_4

    :cond_7
    move v1, v3

    .line 276
    goto :goto_7

    :cond_8
    move v5, v3

    .line 277
    goto :goto_8

    .line 279
    :cond_9
    iget v1, p0, Lflipboard/gui/section/SectionScrollingAdapter;->l:I

    move v4, v1

    goto :goto_9

    :cond_a
    move v1, v3

    .line 280
    goto :goto_a

    .line 285
    :pswitch_f
    if-nez v1, :cond_b

    if-eqz v0, :cond_c

    :cond_b
    move v4, v2

    .line 286
    :goto_b
    if-eqz v4, :cond_d

    if-eqz v6, :cond_d

    move v1, v3

    .line 287
    :goto_c
    if-eqz v4, :cond_e

    move v0, v2

    .line 288
    :goto_d
    invoke-virtual {p2, v1, v0}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->a(II)V

    goto/16 :goto_4

    :cond_c
    move v4, v3

    .line 285
    goto :goto_b

    .line 286
    :cond_d
    iget v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->l:I

    move v1, v0

    goto :goto_c

    :cond_e
    move v0, v3

    .line 287
    goto :goto_d

    .line 292
    :pswitch_10
    if-eqz v6, :cond_f

    move v0, v3

    .line 293
    :goto_e
    invoke-virtual {p2, v0, v2}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->a(II)V

    goto/16 :goto_4

    .line 292
    :cond_f
    iget v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->l:I

    goto :goto_e

    .line 296
    :pswitch_11
    invoke-virtual {p2, v3, v2}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->a(II)V

    .line 297
    invoke-virtual {p2}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->getWrappedItem()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/AdView;

    .line 298
    iget-object v1, p0, Lflipboard/gui/section/SectionScrollingAdapter;->h:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/service/FLAdManager$AdAsset;

    iget-object v4, p0, Lflipboard/gui/section/SectionScrollingAdapter;->c:Lflipboard/service/FLAdManager;

    invoke-static {}, Lflipboard/service/FLAdManager;->a()Landroid/graphics/Point;

    move-result-object v4

    iget v5, v4, Landroid/graphics/Point;->x:I

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v6, v1, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget-object v6, v6, Lflipboard/objs/Ad;->k:Lflipboard/objs/Ad$VideoInfo;

    iput-object v6, v0, Lflipboard/gui/section/scrolling/AdView;->i:Lflipboard/objs/Ad$VideoInfo;

    iget-object v6, v1, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget-object v6, v6, Lflipboard/objs/Ad;->c:Ljava/util/List;

    iput-object v6, v0, Lflipboard/gui/section/scrolling/AdView;->h:Ljava/util/List;

    invoke-virtual {v1, v5, v4}, Lflipboard/service/FLAdManager$AdAsset;->a(II)V

    iget-object v6, v1, Lflipboard/service/FLAdManager$AdAsset;->b:Lflipboard/objs/Ad$Asset;

    iput-object v6, v0, Lflipboard/gui/section/scrolling/AdView;->d:Lflipboard/objs/Ad$Asset;

    iget-object v1, v1, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget-object v1, v1, Lflipboard/objs/Ad;->f:Lflipboard/objs/Ad$ButtonInfo;

    iput-object v1, v0, Lflipboard/gui/section/scrolling/AdView;->g:Lflipboard/objs/Ad$ButtonInfo;

    iget-object v1, v0, Lflipboard/gui/section/scrolling/AdView;->d:Lflipboard/objs/Ad$Asset;

    iget-object v1, v1, Lflipboard/objs/Ad$Asset;->e:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lflipboard/gui/section/scrolling/AdView;->d:Lflipboard/objs/Ad$Asset;

    invoke-virtual {v1, v5, v4}, Lflipboard/objs/Ad$Asset;->a(II)F

    move-result v6

    new-instance v1, Ljava/util/HashMap;

    iget-object v8, v0, Lflipboard/gui/section/scrolling/AdView;->d:Lflipboard/objs/Ad$Asset;

    iget-object v8, v8, Lflipboard/objs/Ad$Asset;->e:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-direct {v1, v8}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, v0, Lflipboard/gui/section/scrolling/AdView;->f:Ljava/util/Map;

    iget-object v1, v0, Lflipboard/gui/section/scrolling/AdView;->d:Lflipboard/objs/Ad$Asset;

    iget-object v1, v1, Lflipboard/objs/Ad$Asset;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_f
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/Ad$HotSpot;

    iget-object v9, v0, Lflipboard/gui/section/scrolling/AdView;->f:Ljava/util/Map;

    iget-object v10, v0, Lflipboard/gui/section/scrolling/AdView;->d:Lflipboard/objs/Ad$Asset;

    invoke-virtual {v10, v1, v6, v5, v4}, Lflipboard/objs/Ad$Asset;->a(Lflipboard/objs/Ad$HotSpot;FII)Landroid/graphics/RectF;

    move-result-object v10

    invoke-interface {v9, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_f

    .line 301
    :pswitch_12
    if-nez v1, :cond_10

    if-eqz v0, :cond_11

    :cond_10
    move v4, v2

    .line 302
    :goto_10
    if-eqz v6, :cond_12

    move v1, v3

    .line 303
    :goto_11
    if-eqz v4, :cond_13

    move v0, v2

    .line 304
    :goto_12
    invoke-virtual {p2, v1, v0}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->a(II)V

    goto/16 :goto_4

    :cond_11
    move v4, v3

    .line 301
    goto :goto_10

    .line 302
    :cond_12
    iget v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->l:I

    move v1, v0

    goto :goto_11

    .line 303
    :cond_13
    const/4 v0, 0x2

    goto :goto_12

    .line 308
    :cond_14
    iget v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->l:I

    invoke-virtual {p2, v0, v3}, Lflipboard/gui/section/scrolling/ScrollItemWrapper;->a(II)V

    goto/16 :goto_4

    .line 311
    :cond_15
    iget v0, p0, Lflipboard/gui/section/SectionScrollingAdapter;->l:I

    goto/16 :goto_5

    .line 271
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_10
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch

    .line 263
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 249
    const/16 v0, 0xd

    return v0
.end method
