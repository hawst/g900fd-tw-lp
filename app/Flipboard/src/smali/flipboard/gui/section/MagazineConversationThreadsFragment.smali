.class public Lflipboard/gui/section/MagazineConversationThreadsFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "MagazineConversationThreadsFragment.java"

# interfaces
.implements Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;
.implements Lflipboard/service/Flap$CommentaryObserver;


# instance fields
.field a:Lflipboard/gui/section/MagazineConversationThreadsAdapter;

.field private final b:Ljava/lang/String;

.field private c:Landroid/widget/ListView;

.field private d:Lflipboard/activities/GenericFragmentActivity;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/CommentaryResult$Item$Commentary;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 44
    const-string v0, "Conversations"

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->b:Ljava/lang/String;

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->i:Z

    return-void
.end method

.method static synthetic a(Lflipboard/gui/section/MagazineConversationThreadsFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->g:Ljava/util/List;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 113
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->a:Lflipboard/gui/section/MagazineConversationThreadsAdapter;

    iget-boolean v0, v0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->i:Z

    if-nez v0, :cond_0

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->i:Z

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 116
    iget-object v1, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v1

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v3, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3, p0}, Lflipboard/service/Flap;->a(Lflipboard/service/User;Ljava/util/List;Ljava/lang/String;Lflipboard/service/Flap$CommentaryObserver;)Lflipboard/service/Flap$CommentaryRequest;

    .line 119
    :cond_0
    return-void
.end method

.method static synthetic b(Lflipboard/gui/section/MagazineConversationThreadsFragment;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lflipboard/gui/section/MagazineConversationThreadsFragment;->a()V

    return-void
.end method

.method static synthetic c(Lflipboard/gui/section/MagazineConversationThreadsFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lflipboard/gui/section/MagazineConversationThreadsFragment;)Lflipboard/activities/GenericFragmentActivity;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->d:Lflipboard/activities/GenericFragmentActivity;

    return-object v0
.end method

.method static synthetic e(Lflipboard/gui/section/MagazineConversationThreadsFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lflipboard/gui/section/MagazineConversationThreadsFragment;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->c:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public final a(Lflipboard/objs/HasCommentaryItem;)V
    .locals 0

    .prologue
    .line 155
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 36
    check-cast p1, Lflipboard/objs/CommentaryResult;

    iget-object v0, p1, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iput-boolean v1, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->i:Z

    iget-object v0, p1, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item;

    if-eqz v0, :cond_1

    iget-object v2, v0, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/CommentaryResult$Item$Commentary;

    iget-object v5, v1, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    const-string v6, "comment"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->h:Ljava/util/Set;

    iget-object v6, v1, Lflipboard/objs/CommentaryResult$Item$Commentary;->h:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->h:Ljava/util/Set;

    iget-object v1, v1, Lflipboard/objs/CommentaryResult$Item$Commentary;->h:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x1

    move v1, v2

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item;->v:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->f:Ljava/lang/String;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/MagazineConversationThreadsFragment$3;

    invoke-direct {v1, p0, v2, v3}, Lflipboard/gui/section/MagazineConversationThreadsFragment$3;-><init>(Lflipboard/gui/section/MagazineConversationThreadsFragment;ZLjava/util/List;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    :cond_1
    return-void

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 209
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->d:Lflipboard/activities/GenericFragmentActivity;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->d:Lflipboard/activities/GenericFragmentActivity;

    invoke-virtual {v0}, Lflipboard/activities/GenericFragmentActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v0

    invoke-virtual {p0}, Lflipboard/gui/section/MagazineConversationThreadsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d01f9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0201cf

    invoke-virtual {v0, v2, v1}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    .line 212
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 59
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lflipboard/gui/section/MagazineConversationThreadsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/GenericFragmentActivity;

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->d:Lflipboard/activities/GenericFragmentActivity;

    .line 61
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 65
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FlipboardFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->g:Ljava/util/List;

    .line 68
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->h:Ljava/util/Set;

    .line 69
    invoke-virtual {p0}, Lflipboard/gui/section/MagazineConversationThreadsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "feedItemId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->e:Ljava/lang/String;

    .line 71
    const v0, 0x7f0300d4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 72
    new-instance v0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;

    iget-object v2, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->d:Lflipboard/activities/GenericFragmentActivity;

    iget-object v3, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->g:Ljava/util/List;

    iget-object v4, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->e:Ljava/lang/String;

    invoke-direct {v0, v2, v3, v4}, Lflipboard/gui/section/MagazineConversationThreadsAdapter;-><init>(Lflipboard/activities/GenericFragmentActivity;Ljava/util/List;Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->a:Lflipboard/gui/section/MagazineConversationThreadsAdapter;

    .line 73
    const v0, 0x7f0a0272

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->c:Landroid/widget/ListView;

    .line 74
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->c:Landroid/widget/ListView;

    iget-object v2, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->a:Lflipboard/gui/section/MagazineConversationThreadsAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 75
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->c:Landroid/widget/ListView;

    new-instance v2, Lflipboard/gui/section/MagazineConversationThreadsFragment$1;

    invoke-direct {v2, p0}, Lflipboard/gui/section/MagazineConversationThreadsFragment$1;-><init>(Lflipboard/gui/section/MagazineConversationThreadsFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 77
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->d:Lflipboard/activities/GenericFragmentActivity;

    invoke-virtual {v0}, Lflipboard/activities/GenericFragmentActivity;->l()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->e()V

    new-instance v2, Lflipboard/gui/actionbar/FLActionBarMenu;

    iget-object v3, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->d:Lflipboard/activities/GenericFragmentActivity;

    invoke-virtual {v3}, Lflipboard/activities/GenericFragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lflipboard/gui/actionbar/FLActionBarMenu;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->d:Lflipboard/activities/GenericFragmentActivity;

    invoke-virtual {v3}, Lflipboard/activities/GenericFragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v3

    const v4, 0x7f0f0008

    invoke-virtual {v3, v4, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v3, 0x7f0a03a7

    invoke-virtual {v2, v3}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v3

    iput-boolean v5, v3, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    new-instance v4, Lflipboard/gui/section/MagazineConversationThreadsFragment$2;

    invoke-direct {v4, p0}, Lflipboard/gui/section/MagazineConversationThreadsFragment$2;-><init>(Lflipboard/gui/section/MagazineConversationThreadsFragment;)V

    iput-object v4, v3, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-virtual {v0, v2}, Lflipboard/gui/actionbar/FLActionBar;->setMenu(Lflipboard/gui/actionbar/FLActionBarMenu;)V

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->d()V

    .line 79
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->d:Lflipboard/activities/GenericFragmentActivity;

    const v2, 0x7f0a004d

    invoke-virtual {v0, v2}, Lflipboard/activities/GenericFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 80
    const-string v2, "Conversations"

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Lflipboard/gui/section/MagazineConversationThreadsFragment;->a:Lflipboard/gui/section/MagazineConversationThreadsAdapter;

    iput-boolean v5, v0, Lflipboard/gui/section/MagazineConversationThreadsAdapter;->a:Z

    .line 83
    invoke-direct {p0}, Lflipboard/gui/section/MagazineConversationThreadsFragment;->a()V

    .line 84
    return-object v1
.end method
