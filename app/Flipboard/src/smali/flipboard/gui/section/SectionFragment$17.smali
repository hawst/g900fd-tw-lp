.class Lflipboard/gui/section/SectionFragment$17;
.super Ljava/lang/Object;
.source "SectionFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/util/ArrayList;

.field final synthetic c:Ljava/util/List;

.field final synthetic d:Z

.field final synthetic e:Ljava/util/ArrayList;

.field final synthetic f:Z

.field final synthetic g:Z

.field final synthetic h:Ljava/util/List;

.field final synthetic i:Lflipboard/gui/section/SectionFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/section/SectionFragment;Ljava/util/ArrayList;Ljava/util/List;ZLjava/util/ArrayList;ZZLjava/util/List;)V
    .locals 1

    .prologue
    .line 2343
    iput-object p1, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    const/4 v0, 0x5

    iput v0, p0, Lflipboard/gui/section/SectionFragment$17;->a:I

    iput-object p2, p0, Lflipboard/gui/section/SectionFragment$17;->b:Ljava/util/ArrayList;

    iput-object p3, p0, Lflipboard/gui/section/SectionFragment$17;->c:Ljava/util/List;

    iput-boolean p4, p0, Lflipboard/gui/section/SectionFragment$17;->d:Z

    iput-object p5, p0, Lflipboard/gui/section/SectionFragment$17;->e:Ljava/util/ArrayList;

    iput-boolean p6, p0, Lflipboard/gui/section/SectionFragment$17;->f:Z

    iput-boolean p7, p0, Lflipboard/gui/section/SectionFragment$17;->g:Z

    iput-object p8, p0, Lflipboard/gui/section/SectionFragment$17;->h:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2347
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v0}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lflipboard/activities/FlipboardActivity;

    .line 2348
    if-nez v7, :cond_0

    .line 2349
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 2350
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "Activity is null when creating pages"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2351
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->n(Lflipboard/gui/section/SectionFragment;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2580
    :goto_0
    return-void

    .line 2356
    :cond_0
    new-instance v11, Ljava/util/ArrayList;

    iget v0, p0, Lflipboard/gui/section/SectionFragment$17;->a:I

    invoke-direct {v11, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2357
    new-instance v12, Ljava/util/LinkedList;

    invoke-direct {v12}, Ljava/util/LinkedList;-><init>()V

    .line 2360
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$17;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Lflipboard/gui/section/SectionFragment;->a(Ljava/util/List;)V

    .line 2362
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2363
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    invoke-virtual {v0}, Lflipboard/util/DuplicateOccurrenceLog;->a()V

    .line 2364
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v2, "SectionFragment.addPages precedingGroups is empty\n"

    invoke-virtual {v0, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2367
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->D()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2368
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "SectionFragment.addPages section.hasCover() is true\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2369
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    iput-object v1, v0, Lflipboard/gui/section/SectionFragment;->z:Lflipboard/objs/FeedItem;

    .line 2370
    new-instance v0, Lflipboard/gui/section/Group;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-static {}, Lflipboard/gui/section/SectionFragment;->k()Lflipboard/objs/SectionPageTemplate;

    move-result-object v2

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v3, v3, Lflipboard/gui/section/SectionFragment;->z:Lflipboard/objs/FeedItem;

    invoke-direct {v0, v1, v2, v3, v10}, Lflipboard/gui/section/Group;-><init>(Lflipboard/service/Section;Lflipboard/objs/SectionPageTemplate;Lflipboard/objs/FeedItem;Z)V

    .line 2371
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17;->b:Ljava/util/ArrayList;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment;->z:Lflipboard/objs/FeedItem;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 2372
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v2}, Lflipboard/gui/section/SectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lflipboard/gui/section/SectionFragment;->a(Landroid/content/Context;Lflipboard/gui/section/Group;)Lflipboard/gui/section/SectionPage;

    move-result-object v1

    .line 2373
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->f()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2374
    invoke-virtual {v1}, Lflipboard/gui/section/SectionPage;->d()V

    .line 2376
    :cond_1
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v2, v2, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v1, v2}, Lflipboard/gui/section/SectionPage;->a(Lflipboard/service/Section;)V

    .line 2381
    :goto_1
    if-eqz v1, :cond_2

    .line 2382
    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2383
    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$17;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2384
    invoke-virtual {v12, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_2
    move v8, v9

    .line 2389
    :goto_2
    iget v0, p0, Lflipboard/gui/section/SectionFragment$17;->a:I

    if-lt v8, v0, :cond_3

    iget-boolean v0, p0, Lflipboard/gui/section/SectionFragment$17;->d:Z

    if-eqz v0, :cond_4

    .line 2390
    :cond_3
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2391
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "SectionFragment.addPages itemsCopy is empty\n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2424
    :cond_4
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/section/SectionFragment$17$1;

    invoke-direct {v1, p0, v11, v12}, Lflipboard/gui/section/SectionFragment$17$1;-><init>(Lflipboard/gui/section/SectionFragment$17;Ljava/util/List;Ljava/util/LinkedList;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 2377
    :cond_5
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->o(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/Group;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 2378
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-static {v0}, Lflipboard/gui/section/SectionFragment;->o(Lflipboard/gui/section/SectionFragment;)Lflipboard/gui/section/Group;

    move-result-object v0

    .line 2379
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v1, v7, v0}, Lflipboard/gui/section/SectionFragment;->a(Landroid/content/Context;Lflipboard/gui/section/Group;)Lflipboard/gui/section/SectionPage;

    move-result-object v1

    goto :goto_1

    .line 2395
    :cond_6
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->O()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2396
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, v0, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v1, "SectionFragment.addPages itemsCopy.get(0) != null && itemsCopy.get(0).isGroup() == true \n"

    invoke-virtual {v0, v1}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2397
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$17;->c:Ljava/util/List;

    invoke-static {v1, v0, v2}, Lflipboard/gui/section/SectionFragment;->a(Lflipboard/gui/section/SectionFragment;Lflipboard/objs/FeedItem;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 2398
    if-eqz v1, :cond_8

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 2399
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2400
    invoke-interface {v11, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2401
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Group;

    .line 2402
    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v3, v7, v0}, Lflipboard/gui/section/SectionFragment;->a(Landroid/content/Context;Lflipboard/gui/section/Group;)Lflipboard/gui/section/SectionPage;

    move-result-object v0

    .line 2403
    invoke-virtual {v12, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2406
    :cond_7
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v8, v0

    .line 2409
    :cond_8
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2389
    :goto_4
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto/16 :goto_2

    .line 2411
    :cond_9
    iget-object v0, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17;->b:Ljava/util/ArrayList;

    iget-object v2, p0, Lflipboard/gui/section/SectionFragment$17;->c:Ljava/util/List;

    iget-object v3, p0, Lflipboard/gui/section/SectionFragment$17;->e:Ljava/util/ArrayList;

    iget-object v4, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v4}, Lflipboard/gui/section/SectionFragment;->g()I

    move-result v4

    iget-object v5, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v5}, Lflipboard/gui/section/SectionFragment;->h()I

    move-result v5

    iget-object v6, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v6, v6, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    invoke-virtual {v6}, Lflipboard/service/Section;->k()Z

    move-result v6

    if-nez v6, :cond_a

    iget-object v6, p0, Lflipboard/gui/section/SectionFragment$17;->b:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/16 v13, 0x19

    if-le v6, v13, :cond_b

    :cond_a
    move v6, v10

    :goto_5
    invoke-virtual/range {v0 .. v6}, Lflipboard/gui/section/SectionFragment;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;IIZ)Lflipboard/gui/section/Group;

    move-result-object v0

    .line 2412
    if-eqz v0, :cond_4

    .line 2413
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v2, "SectionFragment.addPages itemsCopy.get(0) != null && itemsCopy.get(0).isGroup() == false \n"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2414
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    iget-object v1, v1, Lflipboard/gui/section/SectionFragment;->o:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->p:Lflipboard/util/DuplicateOccurrenceLog;

    const-string v2, "SectionFragment.addPages group created \n"

    invoke-virtual {v1, v2}, Lflipboard/util/DuplicateOccurrenceLog;->a(Ljava/lang/Object;)Lflipboard/util/DuplicateOccurrenceLog;

    .line 2415
    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2416
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2417
    iget-object v1, p0, Lflipboard/gui/section/SectionFragment$17;->i:Lflipboard/gui/section/SectionFragment;

    invoke-virtual {v1, v7, v0}, Lflipboard/gui/section/SectionFragment;->a(Landroid/content/Context;Lflipboard/gui/section/Group;)Lflipboard/gui/section/SectionPage;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_b
    move v6, v9

    .line 2411
    goto :goto_5

    :cond_c
    move-object v0, v1

    goto/16 :goto_1
.end method
