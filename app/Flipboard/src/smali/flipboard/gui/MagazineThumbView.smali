.class public Lflipboard/gui/MagazineThumbView;
.super Lflipboard/gui/FLRelativeLayout;
.source "MagazineThumbView.java"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/objs/Image;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 93
    const v0, 0x7f0a02b5

    invoke-virtual {p0, v0}, Lflipboard/gui/MagazineThumbView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 94
    const v1, 0x7f0a0065

    invoke-virtual {p0, v1}, Lflipboard/gui/MagazineThumbView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 95
    if-eqz p1, :cond_0

    .line 96
    invoke-virtual {v0, p1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 97
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 106
    :goto_0
    return-void

    .line 98
    :cond_0
    if-eqz p2, :cond_1

    .line 99
    invoke-virtual {p0}, Lflipboard/gui/MagazineThumbView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020185

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setPlaceholder(Landroid/graphics/drawable/Drawable;)V

    .line 100
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 102
    :cond_1
    invoke-virtual {v0, p1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 103
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setPlaceholder(Landroid/graphics/drawable/Drawable;)V

    .line 104
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setAuthor(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 46
    if-eqz p1, :cond_0

    .line 47
    const v0, 0x7f0a0201

    invoke-virtual {p0, v0}, Lflipboard/gui/MagazineThumbView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 48
    invoke-static {v0, v3}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLTextIntf;I)V

    .line 49
    invoke-virtual {p0}, Lflipboard/gui/MagazineThumbView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0324

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 51
    :cond_0
    return-void
.end method

.method public setImage(Lflipboard/objs/Image;)V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lflipboard/gui/MagazineThumbView;->a(Lflipboard/objs/Image;Z)V

    .line 90
    return-void
.end method

.method public setMagazine(Lflipboard/objs/Magazine;)V
    .locals 5

    .prologue
    const v4, 0x7f0a02b6

    const/4 v3, 0x0

    .line 55
    invoke-virtual {p0, p1}, Lflipboard/gui/MagazineThumbView;->setTag(Ljava/lang/Object;)V

    .line 56
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    .line 57
    iget-object v1, p1, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    const-string v2, "private"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 58
    invoke-virtual {p0, v4}, Lflipboard/gui/MagazineThumbView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 59
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 60
    const v0, 0x7f0a02b7

    invoke-virtual {p0, v0}, Lflipboard/gui/MagazineThumbView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    .line 61
    invoke-virtual {v0, v3}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 68
    :cond_0
    :goto_0
    const v0, 0x7f0a02b5

    invoke-virtual {p0, v0}, Lflipboard/gui/MagazineThumbView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 69
    if-eqz v0, :cond_1

    .line 70
    iget-boolean v1, p1, Lflipboard/objs/Magazine;->n:Z

    if-eqz v1, :cond_3

    .line 72
    iget v1, p1, Lflipboard/objs/Magazine;->o:I

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBackgroundResource(I)V

    .line 78
    :cond_1
    :goto_1
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lflipboard/gui/MagazineThumbView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 79
    iget-object v1, p1, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 80
    return-void

    .line 62
    :cond_2
    iget-object v1, p1, Lflipboard/objs/Magazine;->i:Lflipboard/objs/Author;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lflipboard/objs/Magazine;->i:Lflipboard/objs/Author;

    iget-object v1, v1, Lflipboard/objs/Author;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    invoke-virtual {p0, v4}, Lflipboard/gui/MagazineThumbView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 64
    const v1, 0x7f02015a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 65
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 74
    :cond_3
    iget-object v1, p1, Lflipboard/objs/Magazine;->t:Lflipboard/objs/Image;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    goto :goto_1
.end method

.method public setSelected(Z)V
    .locals 2

    .prologue
    .line 35
    iget-boolean v0, p0, Lflipboard/gui/MagazineThumbView;->a:Z

    if-eq v0, p1, :cond_0

    .line 36
    iput-boolean p1, p0, Lflipboard/gui/MagazineThumbView;->a:Z

    .line 37
    const v0, 0x7f0a027f

    invoke-virtual {p0, v0}, Lflipboard/gui/MagazineThumbView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 38
    if-eqz v1, :cond_0

    .line 39
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 42
    :cond_0
    return-void

    .line 39
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 84
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lflipboard/gui/MagazineThumbView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 85
    invoke-interface {v0, p1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 86
    return-void
.end method
