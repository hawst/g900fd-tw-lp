.class final enum Lflipboard/gui/KenBurnsImageView$AnimationType;
.super Ljava/lang/Enum;
.source "KenBurnsImageView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/KenBurnsImageView$AnimationType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/KenBurnsImageView$AnimationType;

.field public static final enum b:Lflipboard/gui/KenBurnsImageView$AnimationType;

.field public static final enum c:Lflipboard/gui/KenBurnsImageView$AnimationType;

.field private static final synthetic d:[Lflipboard/gui/KenBurnsImageView$AnimationType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 96
    new-instance v0, Lflipboard/gui/KenBurnsImageView$AnimationType;

    const-string v1, "ZOOM"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/KenBurnsImageView$AnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/KenBurnsImageView$AnimationType;->a:Lflipboard/gui/KenBurnsImageView$AnimationType;

    new-instance v0, Lflipboard/gui/KenBurnsImageView$AnimationType;

    const-string v1, "TRANSLATE_X"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/KenBurnsImageView$AnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/KenBurnsImageView$AnimationType;->b:Lflipboard/gui/KenBurnsImageView$AnimationType;

    new-instance v0, Lflipboard/gui/KenBurnsImageView$AnimationType;

    const-string v1, "TRANSLATE_Y"

    invoke-direct {v0, v1, v4}, Lflipboard/gui/KenBurnsImageView$AnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/KenBurnsImageView$AnimationType;->c:Lflipboard/gui/KenBurnsImageView$AnimationType;

    const/4 v0, 0x3

    new-array v0, v0, [Lflipboard/gui/KenBurnsImageView$AnimationType;

    sget-object v1, Lflipboard/gui/KenBurnsImageView$AnimationType;->a:Lflipboard/gui/KenBurnsImageView$AnimationType;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/KenBurnsImageView$AnimationType;->b:Lflipboard/gui/KenBurnsImageView$AnimationType;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/gui/KenBurnsImageView$AnimationType;->c:Lflipboard/gui/KenBurnsImageView$AnimationType;

    aput-object v1, v0, v4

    sput-object v0, Lflipboard/gui/KenBurnsImageView$AnimationType;->d:[Lflipboard/gui/KenBurnsImageView$AnimationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/KenBurnsImageView$AnimationType;
    .locals 1

    .prologue
    .line 96
    const-class v0, Lflipboard/gui/KenBurnsImageView$AnimationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/KenBurnsImageView$AnimationType;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/KenBurnsImageView$AnimationType;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lflipboard/gui/KenBurnsImageView$AnimationType;->d:[Lflipboard/gui/KenBurnsImageView$AnimationType;

    invoke-virtual {v0}, [Lflipboard/gui/KenBurnsImageView$AnimationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/KenBurnsImageView$AnimationType;

    return-object v0
.end method
