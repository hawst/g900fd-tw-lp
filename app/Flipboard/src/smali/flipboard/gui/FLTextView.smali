.class public Lflipboard/gui/FLTextView;
.super Landroid/widget/TextView;
.source "FLTextView.java"

# interfaces
.implements Lflipboard/gui/FLTextIntf;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    invoke-virtual {p0}, Lflipboard/gui/FLTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/graphics/Paint;)V

    .line 20
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->v:Landroid/graphics/Typeface;

    invoke-virtual {p0, v0}, Lflipboard/gui/FLTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    invoke-direct {p0, p2}, Lflipboard/gui/FLTextView;->a(Landroid/util/AttributeSet;)V

    .line 27
    invoke-virtual {p0}, Lflipboard/gui/FLTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/graphics/Paint;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    invoke-direct {p0, p2}, Lflipboard/gui/FLTextView;->a(Landroid/util/AttributeSet;)V

    .line 34
    invoke-virtual {p0}, Lflipboard/gui/FLTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/graphics/Paint;)V

    .line 35
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 39
    invoke-virtual {p0}, Lflipboard/gui/FLTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lflipboard/app/R$styleable;->FLTextView:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 40
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 41
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 42
    if-nez v0, :cond_1

    .line 43
    const-string v0, "normal"

    .line 44
    if-eqz p1, :cond_1

    .line 45
    const-string v1, "http://schemas.android.com/apk/res/android"

    const-string v2, "textStyle"

    invoke-interface {p1, v1, v2}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 46
    if-eqz v1, :cond_1

    const-string v2, "0x1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "0x3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 47
    :cond_0
    const-string v0, "bold"

    .line 51
    :cond_1
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/FLTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 52
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 1

    .prologue
    .line 61
    int-to-float v0, p2

    invoke-super {p0, p1, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 62
    return-void
.end method

.method public getTextColor()I
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lflipboard/gui/FLTextView;->getCurrentTextColor()I

    move-result v0

    return v0
.end method
