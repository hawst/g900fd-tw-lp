.class public Lflipboard/gui/PullToRefreshVerticalViewPager;
.super Lfr/castorflex/android/verticalviewpager/VerticalViewPager;
.source "PullToRefreshVerticalViewPager.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Lfr/castorflex/android/verticalviewpager/VerticalViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    return-void
.end method

.method private getCurrentView()Landroid/view/View;
    .locals 4

    .prologue
    .line 27
    invoke-virtual {p0}, Lflipboard/gui/PullToRefreshVerticalViewPager;->getChildCount()I

    move-result v2

    .line 28
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 29
    invoke-virtual {p0, v1}, Lflipboard/gui/PullToRefreshVerticalViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 34
    :goto_1
    return-object v0

    .line 28
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 34
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public canScrollVertically(I)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 18
    invoke-direct {p0}, Lflipboard/gui/PullToRefreshVerticalViewPager;->getCurrentView()Landroid/view/View;

    move-result-object v1

    .line 19
    if-eqz v1, :cond_2

    .line 20
    invoke-virtual {p0, p1}, Lflipboard/gui/PullToRefreshVerticalViewPager;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    neg-int v3, p1

    invoke-virtual {p0}, Lflipboard/gui/PullToRefreshVerticalViewPager;->getWidth()I

    move-result v0

    div-int/lit8 v4, v0, 0x2

    invoke-virtual {p0}, Lflipboard/gui/PullToRefreshVerticalViewPager;->getHeight()I

    move-result v0

    div-int/lit8 v5, v0, 0x2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/PullToRefreshVerticalViewPager;->a(Landroid/view/View;ZIII)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22
    :cond_0
    :goto_0
    return v2

    .line 20
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 22
    :cond_2
    invoke-virtual {p0, p1}, Lflipboard/gui/PullToRefreshVerticalViewPager;->a(I)Z

    move-result v2

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-virtual {p0}, Lflipboard/gui/PullToRefreshVerticalViewPager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    :try_start_0
    invoke-super {p0, p1}, Lfr/castorflex/android/verticalviewpager/VerticalViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 49
    :cond_0
    :goto_0
    return v0

    .line 43
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-virtual {p0}, Lflipboard/gui/PullToRefreshVerticalViewPager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    :try_start_0
    invoke-super {p0, p1}, Lfr/castorflex/android/verticalviewpager/VerticalViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 63
    :cond_0
    :goto_0
    return v0

    .line 58
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
