.class Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;
.super Ljava/lang/Object;
.source "ContentDrawerDragDropGridAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;


# direct methods
.method constructor <init>(Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const v2, 0x7f02022d

    .line 164
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    invoke-virtual {v0}, Lflipboard/objs/TOCSection;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 165
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->a()V

    .line 166
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    invoke-virtual {v1}, Lflipboard/objs/TOCSection;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-boolean v0, v0, Lflipboard/service/Section$Meta;->b:Z

    if-eqz v0, :cond_2

    .line 169
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->a()V

    .line 170
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    .line 171
    iget-object v1, v0, Lflipboard/objs/ConfigService;->I:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 172
    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v3}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 173
    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#FF"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lflipboard/objs/ConfigService;->I:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 176
    :cond_2
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->a()V

    .line 177
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_3

    .line 178
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 179
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    goto :goto_0

    .line 181
    :cond_3
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 182
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$2;->a:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->c:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;

    invoke-static {v1}, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->b(Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080021

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBackgroundColor(I)V

    goto/16 :goto_0
.end method
