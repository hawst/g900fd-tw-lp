.class Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;
.super Ljava/lang/Object;
.source "ContentDrawerDragDropGridAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/LinkedHashMap;

.field final synthetic b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;


# direct methods
.method constructor <init>(Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;Ljava/util/LinkedHashMap;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iput-object p2, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->a:Ljava/util/LinkedHashMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0x7f02022d

    .line 124
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->a:Ljava/util/LinkedHashMap;

    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->a:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/RecentImage;

    .line 125
    if-eqz v0, :cond_1

    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/objs/RecentImage;->a(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 126
    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->a()V

    .line 127
    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    iget-object v2, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v2, v2, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0, v2}, Lflipboard/objs/RecentImage;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->J()Lflipboard/objs/Image;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 130
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->a()V

    .line 131
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->J()Lflipboard/objs/Image;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 132
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 134
    :cond_2
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    invoke-virtual {v0}, Lflipboard/objs/TOCSection;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 135
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->a()V

    .line 136
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    invoke-virtual {v1}, Lflipboard/objs/TOCSection;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    goto :goto_0

    .line 137
    :cond_3
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-boolean v0, v0, Lflipboard/service/Section$Meta;->b:Z

    if-eqz v0, :cond_4

    .line 138
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->a()V

    .line 139
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    .line 140
    iget-object v1, v0, Lflipboard/objs/ConfigService;->I:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 141
    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 142
    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#FF"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lflipboard/objs/ConfigService;->I:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setBackgroundColor(I)V

    goto/16 :goto_0

    .line 145
    :cond_4
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->a()V

    .line 146
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_5

    .line 147
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    goto/16 :goto_0

    .line 149
    :cond_5
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 150
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v0, v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2$1;->b:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    iget-object v1, v1, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;->c:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;

    invoke-static {v1}, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->b(Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080021

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBackgroundColor(I)V

    goto/16 :goto_0
.end method
