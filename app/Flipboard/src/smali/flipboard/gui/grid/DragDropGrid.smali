.class public Lflipboard/gui/grid/DragDropGrid;
.super Landroid/view/ViewGroup;
.source "DragDropGrid.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static a:I


# instance fields
.field private b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

.field private c:Landroid/view/View$OnClickListener;

.field private d:Lflipboard/gui/grid/PagedContainer;

.field private e:Landroid/util/SparseIntArray;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:Z

.field private n:I

.field private o:Landroid/view/View;

.field private p:I

.field private q:I

.field private r:Landroid/widget/ImageButton;

.field private s:I

.field private t:I

.field private u:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/16 v0, 0xfa

    sput v0, Lflipboard/gui/grid/DragDropGrid;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 88
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->c:Landroid/view/View$OnClickListener;

    .line 54
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->e:Landroid/util/SparseIntArray;

    .line 56
    iput v2, p0, Lflipboard/gui/grid/DragDropGrid;->f:I

    .line 57
    iput v2, p0, Lflipboard/gui/grid/DragDropGrid;->g:I

    .line 58
    iput v1, p0, Lflipboard/gui/grid/DragDropGrid;->h:I

    .line 64
    iput v1, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    .line 89
    invoke-direct {p0}, Lflipboard/gui/grid/DragDropGrid;->a()V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 82
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->c:Landroid/view/View$OnClickListener;

    .line 54
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->e:Landroid/util/SparseIntArray;

    .line 56
    iput v2, p0, Lflipboard/gui/grid/DragDropGrid;->f:I

    .line 57
    iput v2, p0, Lflipboard/gui/grid/DragDropGrid;->g:I

    .line 58
    iput v1, p0, Lflipboard/gui/grid/DragDropGrid;->h:I

    .line 64
    iput v1, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    .line 83
    invoke-direct {p0}, Lflipboard/gui/grid/DragDropGrid;->a()V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 76
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->c:Landroid/view/View$OnClickListener;

    .line 54
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->e:Landroid/util/SparseIntArray;

    .line 56
    iput v2, p0, Lflipboard/gui/grid/DragDropGrid;->f:I

    .line 57
    iput v2, p0, Lflipboard/gui/grid/DragDropGrid;->g:I

    .line 58
    iput v1, p0, Lflipboard/gui/grid/DragDropGrid;->h:I

    .line 64
    iput v1, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    .line 77
    invoke-direct {p0}, Lflipboard/gui/grid/DragDropGrid;->a()V

    .line 78
    return-void
.end method

.method private a(I)Landroid/graphics/Point;
    .locals 4

    .prologue
    .line 566
    new-instance v0, Lflipboard/gui/grid/DragDropGrid$ItemPosition;

    iget-object v1, p0, Lflipboard/gui/grid/DragDropGrid;->d:Lflipboard/gui/grid/PagedContainer;

    invoke-interface {v1}, Lflipboard/gui/grid/PagedContainer;->b()I

    invoke-direct {v0, p0, p1}, Lflipboard/gui/grid/DragDropGrid$ItemPosition;-><init>(Lflipboard/gui/grid/DragDropGrid;I)V

    .line 568
    iget v1, v0, Lflipboard/gui/grid/DragDropGrid$ItemPosition;->a:I

    iget-object v2, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    div-int/lit8 v1, v1, 0x3

    .line 569
    iget v0, v0, Lflipboard/gui/grid/DragDropGrid$ItemPosition;->a:I

    iget-object v2, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    mul-int/lit8 v2, v1, 0x3

    sub-int/2addr v0, v2

    .line 571
    iget-object v2, p0, Lflipboard/gui/grid/DragDropGrid;->d:Lflipboard/gui/grid/PagedContainer;

    invoke-interface {v2}, Lflipboard/gui/grid/PagedContainer;->b()I

    move-result v2

    iget v3, p0, Lflipboard/gui/grid/DragDropGrid;->f:I

    mul-int/2addr v2, v3

    iget v3, p0, Lflipboard/gui/grid/DragDropGrid;->i:I

    mul-int/2addr v0, v3

    add-int/2addr v0, v2

    .line 572
    iget v2, p0, Lflipboard/gui/grid/DragDropGrid;->j:I

    mul-int/2addr v1, v2

    .line 574
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object v2
.end method

.method private a(IILandroid/graphics/Point;)Landroid/graphics/Point;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 495
    if-eq p2, p1, :cond_0

    .line 496
    invoke-direct {p0, p1}, Lflipboard/gui/grid/DragDropGrid;->a(I)Landroid/graphics/Point;

    move-result-object v1

    .line 497
    new-instance v0, Landroid/graphics/Point;

    iget v2, v1, Landroid/graphics/Point;->x:I

    iget v3, p3, Landroid/graphics/Point;->x:I

    sub-int/2addr v2, v3

    iget v1, v1, Landroid/graphics/Point;->y:I

    iget v3, p3, Landroid/graphics/Point;->y:I

    sub-int/2addr v1, v3

    invoke-direct {v0, v2, v1}, Landroid/graphics/Point;-><init>(II)V

    .line 501
    :goto_0
    return-object v0

    .line 499
    :cond_0
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/gui/grid/DragDropGrid;)Landroid/view/View;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 118
    invoke-virtual {p0}, Lflipboard/gui/grid/DragDropGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/grid/DragDropGrid;->s:I

    .line 119
    invoke-virtual {p0, p0}, Lflipboard/gui/grid/DragDropGrid;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 120
    invoke-virtual {p0, p0}, Lflipboard/gui/grid/DragDropGrid;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 121
    return-void
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 329
    invoke-direct {p0}, Lflipboard/gui/grid/DragDropGrid;->c()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/gui/grid/DragDropGrid;->a(Ljava/util/List;)V

    .line 332
    invoke-virtual {p0, p1}, Lflipboard/gui/grid/DragDropGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 333
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    if-eq p1, p2, :cond_0

    .line 334
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    iget-object v1, p0, Lflipboard/gui/grid/DragDropGrid;->d:Lflipboard/gui/grid/PagedContainer;

    invoke-interface {v1}, Lflipboard/gui/grid/PagedContainer;->b()I

    invoke-interface {v0, p1, p2}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->d(II)V

    .line 336
    :cond_0
    return-void
.end method

.method private static a(Landroid/view/View;Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 11

    .prologue
    .line 506
    new-instance v10, Landroid/view/animation/AnimationSet;

    const/4 v0, 0x1

    invoke-direct {v10, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 508
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 509
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    const/4 v2, 0x0

    iget v3, p1, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    const/4 v4, 0x0

    iget v5, p2, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    const/4 v6, 0x0

    iget v7, p1, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    const/4 v8, 0x0

    iget v9, p2, Landroid/graphics/Point;->y:I

    int-to-float v9, v9

    invoke-direct/range {v1 .. v9}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    sget v2, Lflipboard/gui/grid/DragDropGrid;->a:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/animation/TranslateAnimation;->setFillEnabled(Z)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 511
    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 512
    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 514
    invoke-virtual {p0}, Landroid/view/View;->clearAnimation()V

    .line 515
    invoke-virtual {p0, v10}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 516
    return-void
.end method

.method static synthetic a(Lflipboard/gui/grid/DragDropGrid;I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const v1, 0x3fb33333    # 1.4f

    const/4 v2, 0x0

    .line 45
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    iget v3, p0, Lflipboard/gui/grid/DragDropGrid;->t:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v5, v3

    iget v3, p0, Lflipboard/gui/grid/DragDropGrid;->u:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v6, v3

    move v3, v1

    move v4, v2

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    invoke-virtual {v0, v7}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    invoke-virtual {v0, v7}, Landroid/view/animation/ScaleAnimation;->setFillEnabled(Z)V

    invoke-virtual {p0, p1}, Lflipboard/gui/grid/DragDropGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lflipboard/gui/grid/DragDropGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    invoke-virtual {p0, p1}, Lflipboard/gui/grid/DragDropGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method static synthetic a(Lflipboard/gui/grid/DragDropGrid;II)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lflipboard/gui/grid/DragDropGrid;->a(II)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0x1e

    .line 661
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [Landroid/view/View;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v8}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v8}, Ljava/util/ArrayList;-><init>(I)V

    move-object v3, v2

    move-object v2, v0

    move v0, v1

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_1

    iget-object v5, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    iget-object v6, p0, Lflipboard/gui/grid/DragDropGrid;->d:Lflipboard/gui/grid/PagedContainer;

    invoke-interface {v6}, Lflipboard/gui/grid/PagedContainer;->b()I

    move-result v6

    invoke-interface {v5, v6, v0}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->a(II)Landroid/view/View;

    move-result-object v5

    aput-object v5, v4, v0

    iget-object v5, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    invoke-interface {v5, v1, v0}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->b(II)Lflipboard/service/Section;

    move-result-object v5

    iget-object v6, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    aget-object v7, v4, v0

    invoke-interface {v6, v7, v5}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->a(Landroid/view/View;Lflipboard/service/Section;)Lflipboard/service/Flap$TypedResultObserver;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-ne v5, v8, :cond_0

    invoke-direct {p0, v3, v2}, Lflipboard/gui/grid/DragDropGrid;->a(Ljava/util/List;Ljava/util/List;)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v8}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v8}, Ljava/util/ArrayList;-><init>(I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, v3, v2}, Lflipboard/gui/grid/DragDropGrid;->a(Ljava/util/List;Ljava/util/List;)V

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 662
    iget-object v1, p0, Lflipboard/gui/grid/DragDropGrid;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v1}, Landroid/util/SparseIntArray;->clear()V

    .line 664
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 665
    if-eqz v0, :cond_3

    .line 666
    invoke-virtual {p0, v0}, Lflipboard/gui/grid/DragDropGrid;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 669
    :cond_4
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->r:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->bringToFront()V

    .line 670
    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/objs/RecentImage;",
            ">;>;>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 201
    new-instance v0, Lflipboard/gui/grid/DragDropGrid$3;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/grid/DragDropGrid$3;-><init>(Lflipboard/gui/grid/DragDropGrid;Ljava/util/List;)V

    invoke-static {p2, v0}, Lflipboard/util/AndroidUtil;->a(Ljava/util/List;Lflipboard/service/Flap$TypedResultObserver;)V

    .line 218
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 225
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0}, Lflipboard/gui/grid/DragDropGrid;->getItemViewCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 226
    iget v1, p0, Lflipboard/gui/grid/DragDropGrid;->h:I

    if-eq v0, v1, :cond_0

    .line 227
    invoke-virtual {p0, v0}, Lflipboard/gui/grid/DragDropGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 228
    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    .line 225
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 231
    :cond_1
    return-void
.end method

.method private b(II)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 395
    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->h:I

    invoke-virtual {p0, v0}, Lflipboard/gui/grid/DragDropGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 396
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 397
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 399
    div-int/lit8 v0, v4, 0x2

    sub-int v0, p1, v0

    .line 400
    div-int/lit8 v2, v5, 0x2

    sub-int v2, p2, v2

    .line 402
    if-gez v0, :cond_0

    move v0, v1

    .line 403
    :cond_0
    iget v6, p0, Lflipboard/gui/grid/DragDropGrid;->f:I

    sub-int/2addr v6, v4

    if-le v0, v6, :cond_1

    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->f:I

    sub-int/2addr v0, v4

    .line 404
    :cond_1
    if-gez v2, :cond_3

    .line 405
    :goto_0
    iget v2, p0, Lflipboard/gui/grid/DragDropGrid;->g:I

    sub-int/2addr v2, v5

    if-le v1, v2, :cond_2

    iget v1, p0, Lflipboard/gui/grid/DragDropGrid;->g:I

    sub-int/2addr v1, v5

    .line 406
    :cond_2
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 408
    iget-object v6, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v6

    add-int/lit8 v6, v6, -0x14

    iget-object v7, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v7

    add-int/lit8 v7, v7, -0x14

    iget-object v8, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getRight()I

    move-result v8

    iget-object v9, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getBottom()I

    move-result v9

    invoke-virtual {v2, v6, v7, v8, v9}, Landroid/graphics/Rect;->union(IIII)V

    .line 409
    add-int v6, v0, v4

    add-int v7, v1, v5

    invoke-virtual {v3, v0, v1, v6, v7}, Landroid/view/View;->layout(IIII)V

    .line 410
    iget-object v3, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    add-int/2addr v4, v0

    add-int/2addr v5, v1

    invoke-virtual {v3, v0, v1, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 411
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    add-int/lit8 v0, v0, -0x14

    iget-object v1, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    add-int/lit8 v1, v1, -0x14

    iget-object v3, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;->union(IIII)V

    .line 412
    invoke-virtual {p0, v2}, Lflipboard/gui/grid/DragDropGrid;->invalidate(Landroid/graphics/Rect;)V

    .line 413
    return-void

    :cond_3
    move v1, v2

    .line 404
    goto :goto_0
.end method

.method static synthetic b(Lflipboard/gui/grid/DragDropGrid;I)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Lflipboard/gui/grid/DragDropGrid;->c()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/gui/grid/DragDropGrid;->a(Ljava/util/List;)V

    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    iget-object v1, p0, Lflipboard/gui/grid/DragDropGrid;->d:Lflipboard/gui/grid/PagedContainer;

    invoke-interface {v1}, Lflipboard/gui/grid/PagedContainer;->b()I

    move-result v1

    invoke-interface {v0, v1, p1}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->c(II)V

    invoke-virtual {p0, p1}, Lflipboard/gui/grid/DragDropGrid;->removeViewAt(I)V

    return-void
.end method

.method static synthetic b(Lflipboard/gui/grid/DragDropGrid;II)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lflipboard/gui/grid/DragDropGrid;->b(II)V

    return-void
.end method

.method private c()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 641
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {p0}, Lflipboard/gui/grid/DragDropGrid;->getItemViewCount()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 642
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 643
    if-nez v0, :cond_0

    .line 644
    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 646
    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-direct {p0}, Lflipboard/gui/grid/DragDropGrid;->getItemViewCount()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 647
    invoke-virtual {p0, v0}, Lflipboard/gui/grid/DragDropGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 648
    if-eqz v2, :cond_2

    .line 649
    invoke-virtual {v2}, Landroid/view/View;->clearAnimation()V

    .line 650
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 646
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 653
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 654
    invoke-virtual {p0, v0}, Lflipboard/gui/grid/DragDropGrid;->removeView(Landroid/view/View;)V

    goto :goto_2

    .line 656
    :cond_4
    return-object v1
.end method

.method private c(II)V
    .locals 8

    .prologue
    .line 477
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p2, p2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    .line 479
    invoke-virtual {p0, v0}, Lflipboard/gui/grid/DragDropGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 481
    invoke-direct {p0, v0}, Lflipboard/gui/grid/DragDropGrid;->a(I)Landroid/graphics/Point;

    move-result-object v2

    .line 482
    invoke-direct {p0, p1}, Lflipboard/gui/grid/DragDropGrid;->a(I)Landroid/graphics/Point;

    move-result-object v3

    .line 484
    invoke-direct {p0, p2, v0, v2}, Lflipboard/gui/grid/DragDropGrid;->a(IILandroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v4

    .line 485
    new-instance v5, Landroid/graphics/Point;

    iget v6, v3, Landroid/graphics/Point;->x:I

    iget v7, v2, Landroid/graphics/Point;->x:I

    sub-int/2addr v6, v7

    iget v3, v3, Landroid/graphics/Point;->y:I

    iget v2, v2, Landroid/graphics/Point;->y:I

    sub-int v2, v3, v2

    invoke-direct {v5, v6, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 486
    invoke-static {v1, v4, v5}, Lflipboard/gui/grid/DragDropGrid;->a(Landroid/view/View;Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 487
    iget-object v1, p0, Lflipboard/gui/grid/DragDropGrid;->e:Landroid/util/SparseIntArray;

    iget-object v2, p0, Lflipboard/gui/grid/DragDropGrid;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v2, p1, p1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v2

    invoke-virtual {v1, p2, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 488
    iget-object v1, p0, Lflipboard/gui/grid/DragDropGrid;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 489
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    iget-object v1, p0, Lflipboard/gui/grid/DragDropGrid;->d:Lflipboard/gui/grid/PagedContainer;

    invoke-interface {v1}, Lflipboard/gui/grid/PagedContainer;->b()I

    move-result v1

    invoke-interface {v0, v1, p2, p1}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->a(III)V

    .line 490
    return-void
.end method

.method private d(II)I
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 586
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->d:Lflipboard/gui/grid/PagedContainer;

    invoke-interface {v0}, Lflipboard/gui/grid/PagedContainer;->b()I

    move-result v4

    .line 588
    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->f:I

    mul-int v5, v4, v0

    move v0, v1

    move v2, v3

    :goto_0
    iget-object v6, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    const/4 v6, 0x3

    if-gt v2, v6, :cond_0

    iget v6, p0, Lflipboard/gui/grid/DragDropGrid;->i:I

    mul-int/2addr v6, v2

    add-int/2addr v6, v5

    if-lt p1, v6, :cond_0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 589
    :cond_0
    :goto_1
    iget-object v2, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    invoke-interface {v2}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->b()I

    move-result v2

    if-gt v3, v2, :cond_1

    iget v2, p0, Lflipboard/gui/grid/DragDropGrid;->j:I

    mul-int/2addr v2, v3

    if-lt p2, v2, :cond_1

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 590
    :cond_1
    iget-object v2, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    mul-int/lit8 v1, v1, 0x3

    add-int/2addr v0, v1

    .line 592
    invoke-direct {p0, v4, v0}, Lflipboard/gui/grid/DragDropGrid;->e(II)I

    move-result v0

    return v0
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 833
    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->h:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()I
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 838
    move v0, v1

    :goto_0
    invoke-direct {p0}, Lflipboard/gui/grid/DragDropGrid;->getItemViewCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 839
    invoke-virtual {p0, v0}, Lflipboard/gui/grid/DragDropGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 840
    iget v4, p0, Lflipboard/gui/grid/DragDropGrid;->k:I

    int-to-float v4, v4

    iget v5, p0, Lflipboard/gui/grid/DragDropGrid;->l:I

    int-to-float v5, v5

    const/4 v6, 0x2

    new-array v6, v6, [I

    invoke-virtual {v2, v6}, Landroid/view/View;->getLocationOnScreen([I)V

    aget v7, v6, v1

    aget v6, v6, v3

    int-to-float v8, v7

    cmpl-float v8, v4, v8

    if-lez v8, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v8

    add-int/2addr v7, v8

    int-to-float v7, v7

    cmpg-float v4, v4, v7

    if-gez v4, :cond_0

    int-to-float v4, v6

    cmpl-float v4, v5, v4

    if-lez v4, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v2, v6

    int-to-float v2, v2

    cmpg-float v2, v5, v2

    if-gez v2, :cond_0

    move v2, v3

    :goto_1
    if-eqz v2, :cond_1

    .line 844
    :goto_2
    return v0

    :cond_0
    move v2, v1

    .line 840
    goto :goto_1

    .line 838
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 844
    :cond_2
    const/4 v0, -0x1

    goto :goto_2
.end method

.method private e(II)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 869
    move v0, v1

    move v2, v1

    .line 870
    :goto_0
    iget-object v3, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    invoke-interface {v3}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->a()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 871
    iget-object v3, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    invoke-interface {v3, v0}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->a(I)I

    move-result v5

    move v3, v1

    .line 872
    :goto_1
    if-ge v3, v5, :cond_1

    .line 873
    if-ne p1, v0, :cond_0

    if-ne p2, v3, :cond_0

    .line 879
    :goto_2
    return v2

    .line 876
    :cond_0
    add-int/lit8 v4, v2, 0x1

    .line 872
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_1

    .line 870
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 879
    :cond_2
    const/4 v2, -0x1

    goto :goto_2
.end method

.method private getItemViewCount()I
    .locals 1

    .prologue
    .line 743
    invoke-virtual {p0}, Lflipboard/gui/grid/DragDropGrid;->getChildCount()I

    move-result v0

    return v0
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lflipboard/gui/grid/DragDropGrid;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    .line 727
    add-int v0, p2, p4

    iget-object v1, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    invoke-interface {v1}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->a()I

    move-result v1

    div-int v5, v0, v1

    .line 728
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    invoke-interface {v1}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->a()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 729
    const/4 v3, 0x0

    .line 730
    const/4 v2, 0x0

    .line 731
    const/4 v1, 0x0

    :goto_1
    iget-object v4, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    invoke-interface {v4, v0}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->a(I)I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 732
    invoke-direct {p0, v0, v1}, Lflipboard/gui/grid/DragDropGrid;->e(II)I

    move-result v4

    invoke-virtual {p0, v4}, Lflipboard/gui/grid/DragDropGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v4

    if-eqz v4, :cond_2

    mul-int v4, v0, v5

    iget v7, p0, Lflipboard/gui/grid/DragDropGrid;->i:I

    mul-int/2addr v7, v3

    add-int/2addr v4, v7

    iget v7, p0, Lflipboard/gui/grid/DragDropGrid;->i:I

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    sub-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v4, v7

    iget v7, p0, Lflipboard/gui/grid/DragDropGrid;->s:I

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v4, v7

    :goto_2
    iget v7, p0, Lflipboard/gui/grid/DragDropGrid;->j:I

    mul-int/2addr v7, v2

    iget v8, p0, Lflipboard/gui/grid/DragDropGrid;->j:I

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    sub-int/2addr v8, v9

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v4

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v7

    invoke-virtual {v6, v4, v7, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 733
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 734
    iget-object v4, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1

    .line 735
    const/4 v3, 0x0

    .line 736
    add-int/lit8 v2, v2, 0x1

    .line 731
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 732
    :cond_2
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    mul-int/2addr v4, v3

    iget v7, p0, Lflipboard/gui/grid/DragDropGrid;->s:I

    mul-int/2addr v7, v3

    add-int/2addr v4, v7

    goto :goto_2

    .line 728
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 740
    :cond_4
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 11

    .prologue
    const v2, 0x3f933333    # 1.15f

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v10, 0x4

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 777
    invoke-direct {p0}, Lflipboard/gui/grid/DragDropGrid;->e()I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    .line 778
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->d:Lflipboard/gui/grid/PagedContainer;

    invoke-interface {v0}, Lflipboard/gui/grid/PagedContainer;->d()V

    .line 779
    iput-boolean v7, p0, Lflipboard/gui/grid/DragDropGrid;->m:Z

    .line 780
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->r:Landroid/widget/ImageButton;

    invoke-virtual {v0, v10}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 781
    invoke-direct {p0}, Lflipboard/gui/grid/DragDropGrid;->e()I

    move-result v0

    iput v0, p0, Lflipboard/gui/grid/DragDropGrid;->h:I

    .line 782
    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->h:I

    invoke-virtual {p0, v0}, Lflipboard/gui/grid/DragDropGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    iget v3, p0, Lflipboard/gui/grid/DragDropGrid;->h:I

    invoke-interface {v0, v8, v3}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->a(II)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    iget v3, p0, Lflipboard/gui/grid/DragDropGrid;->h:I

    invoke-interface {v0, v8, v3}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->b(II)Lflipboard/service/Section;

    move-result-object v0

    iget-object v3, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    iget-object v4, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    invoke-interface {v3, v4, v0}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->a(Landroid/view/View;Lflipboard/service/Section;)Lflipboard/service/Flap$TypedResultObserver;

    move-result-object v3

    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v3}, Lflipboard/util/AndroidUtil;->a(Ljava/util/List;Lflipboard/service/Flap$TypedResultObserver;)V

    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {v9}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {v9}, Landroid/view/View;->getRight()I

    move-result v5

    invoke-virtual {v9}, Landroid/view/View;->getBottom()I

    move-result v6

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    invoke-virtual {p0, v0}, Lflipboard/gui/grid/DragDropGrid;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/view/animation/ScaleAnimation;

    iget v3, p0, Lflipboard/gui/grid/DragDropGrid;->t:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v5, v3

    iget v3, p0, Lflipboard/gui/grid/DragDropGrid;->u:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v6, v3

    move v3, v1

    move v4, v2

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    invoke-virtual {v0, v7}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    invoke-virtual {v0, v7}, Landroid/view/animation/ScaleAnimation;->setFillEnabled(Z)V

    invoke-direct {p0}, Lflipboard/gui/grid/DragDropGrid;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    iget-object v1, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-wide/16 v2, 0x14

    new-instance v1, Lflipboard/gui/grid/DragDropGrid$5;

    invoke-direct {v1, p0}, Lflipboard/gui/grid/DragDropGrid$5;-><init>(Lflipboard/gui/grid/DragDropGrid;)V

    invoke-virtual {v0, v2, v3, v1}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 783
    const/4 v0, 0x2

    new-array v1, v0, [I

    .line 784
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 787
    aget v0, v1, v7

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    div-int/lit8 v2, v2, 0x3

    sub-int/2addr v0, v2

    aput v0, v1, v7

    .line 788
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->r:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    aget v1, v1, v7

    invoke-virtual {v0, v8, v1, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 789
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->r:Landroid/widget/ImageButton;

    invoke-virtual {v0, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    move v0, v7

    .line 793
    :goto_0
    return v0

    :cond_1
    move v0, v8

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 706
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iput v1, p0, Lflipboard/gui/grid/DragDropGrid;->f:I

    .line 708
    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->s:I

    iget-object v2, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    mul-int/lit8 v0, v0, 0x4

    .line 709
    sub-int v0, v1, v0

    iget-object v2, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    div-int/lit8 v2, v0, 0x3

    .line 710
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/grid/DragDropGrid;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 711
    invoke-virtual {p0, v0}, Lflipboard/gui/grid/DragDropGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 712
    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    .line 713
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    iput v4, p0, Lflipboard/gui/grid/DragDropGrid;->t:I

    .line 714
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    iput v3, p0, Lflipboard/gui/grid/DragDropGrid;->u:I

    .line 710
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 717
    :cond_0
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    div-int/lit8 v0, v1, 0x3

    iput v0, p0, Lflipboard/gui/grid/DragDropGrid;->i:I

    .line 718
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    invoke-interface {v0}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->b()I

    move-result v0

    mul-int/2addr v0, v2

    iget v2, p0, Lflipboard/gui/grid/DragDropGrid;->s:I

    iget-object v3, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    invoke-interface {v3}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->b()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 719
    iget-object v2, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    invoke-interface {v2}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->b()I

    move-result v2

    div-int v2, v0, v2

    iput v2, p0, Lflipboard/gui/grid/DragDropGrid;->j:I

    .line 720
    iput v0, p0, Lflipboard/gui/grid/DragDropGrid;->g:I

    .line 722
    iget-object v2, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    invoke-interface {v2}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->a()I

    move-result v2

    mul-int/2addr v1, v2

    invoke-virtual {p0, v1, v0}, Lflipboard/gui/grid/DragDropGrid;->setMeasuredDimension(II)V

    .line 723
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    .line 240
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 241
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 318
    :cond_0
    :goto_0
    invoke-direct {p0}, Lflipboard/gui/grid/DragDropGrid;->d()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 319
    const/4 v0, 0x1

    .line 320
    :goto_1
    return v0

    .line 243
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lflipboard/gui/grid/DragDropGrid;->k:I

    .line 244
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lflipboard/gui/grid/DragDropGrid;->l:I

    .line 245
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, Lflipboard/gui/grid/DragDropGrid;->d:Lflipboard/gui/grid/PagedContainer;

    invoke-interface {v1}, Lflipboard/gui/grid/PagedContainer;->b()I

    move-result v1

    iget v2, p0, Lflipboard/gui/grid/DragDropGrid;->f:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lflipboard/gui/grid/DragDropGrid;->p:I

    .line 246
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lflipboard/gui/grid/DragDropGrid;->q:I

    goto :goto_0

    .line 249
    :pswitch_1
    iget-boolean v0, p0, Lflipboard/gui/grid/DragDropGrid;->m:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lflipboard/gui/grid/DragDropGrid;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lflipboard/gui/grid/DragDropGrid;->p:I

    .line 251
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lflipboard/gui/grid/DragDropGrid;->q:I

    .line 252
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v2, v0

    .line 253
    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->p:I

    iget v1, p0, Lflipboard/gui/grid/DragDropGrid;->q:I

    invoke-direct {p0, v0, v1}, Lflipboard/gui/grid/DragDropGrid;->b(II)V

    .line 254
    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->p:I

    iget v1, p0, Lflipboard/gui/grid/DragDropGrid;->q:I

    invoke-direct {p0, v0, v1}, Lflipboard/gui/grid/DragDropGrid;->d(II)I

    move-result v3

    const/4 v0, -0x1

    if-eq v3, v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_7

    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    if-eq v3, v0, :cond_7

    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v3, v3}, Landroid/util/SparseIntArray;->get(II)I

    move-result v4

    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->h:I

    if-eq v4, v0, :cond_7

    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->h:I

    iput v0, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    :cond_1
    invoke-virtual {p0, v4}, Lflipboard/gui/grid/DragDropGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-direct {p0, v4}, Lflipboard/gui/grid/DragDropGrid;->a(I)Landroid/graphics/Point;

    move-result-object v6

    iget v1, p0, Lflipboard/gui/grid/DragDropGrid;->h:I

    const/4 v0, 0x0

    :goto_3
    iget-object v7, p0, Lflipboard/gui/grid/DragDropGrid;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v7}, Landroid/util/SparseIntArray;->size()I

    move-result v7

    if-ge v0, v7, :cond_f

    iget-object v7, p0, Lflipboard/gui/grid/DragDropGrid;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v7, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v7

    if-ne v7, v1, :cond_3

    iget-object v1, p0, Lflipboard/gui/grid/DragDropGrid;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v0

    :goto_4
    invoke-direct {p0, v0}, Lflipboard/gui/grid/DragDropGrid;->a(I)Landroid/graphics/Point;

    move-result-object v0

    invoke-direct {p0, v3, v4, v6}, Lflipboard/gui/grid/DragDropGrid;->a(IILandroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v1

    new-instance v7, Landroid/graphics/Point;

    iget v8, v0, Landroid/graphics/Point;->x:I

    iget v9, v6, Landroid/graphics/Point;->x:I

    sub-int/2addr v8, v9

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget v6, v6, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v6

    invoke-direct {v7, v8, v0}, Landroid/graphics/Point;-><init>(II)V

    iget v0, v1, Landroid/graphics/Point;->y:I

    iget v6, v7, Landroid/graphics/Point;->y:I

    if-le v0, v6, :cond_4

    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    :goto_5
    if-ge v0, v3, :cond_6

    add-int/lit8 v1, v0, 0x1

    invoke-direct {p0, v0, v1}, Lflipboard/gui/grid/DragDropGrid;->c(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    iget v0, v1, Landroid/graphics/Point;->y:I

    iget v6, v7, Landroid/graphics/Point;->y:I

    if-ge v0, v6, :cond_5

    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    :goto_6
    if-le v0, v3, :cond_6

    add-int/lit8 v1, v0, -0x1

    invoke-direct {p0, v0, v1}, Lflipboard/gui/grid/DragDropGrid;->c(II)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_6

    :cond_5
    invoke-static {v5, v1, v7}, Lflipboard/gui/grid/DragDropGrid;->a(Landroid/view/View;Landroid/graphics/Point;Landroid/graphics/Point;)V

    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->e:Landroid/util/SparseIntArray;

    iget-object v1, p0, Lflipboard/gui/grid/DragDropGrid;->e:Landroid/util/SparseIntArray;

    iget v5, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    iget v6, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    invoke-virtual {v1, v5, v6}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->e:Landroid/util/SparseIntArray;

    iget v1, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    iget-object v1, p0, Lflipboard/gui/grid/DragDropGrid;->d:Lflipboard/gui/grid/PagedContainer;

    invoke-interface {v1}, Lflipboard/gui/grid/PagedContainer;->b()I

    move-result v1

    iget v4, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    invoke-interface {v0, v1, v3, v4}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->a(III)V

    :cond_6
    iput v3, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    .line 255
    :cond_7
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->d:Lflipboard/gui/grid/PagedContainer;

    invoke-interface {v0}, Lflipboard/gui/grid/PagedContainer;->a()V

    .line 256
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iget-object v3, p0, Lflipboard/gui/grid/DragDropGrid;->r:Landroid/widget/ImageButton;

    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->getHitRect(Landroid/graphics/Rect;)V

    add-int/lit8 v3, v0, 0x1

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/graphics/Rect;->intersect(IIII)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->r:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lflipboard/gui/grid/DragDropGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200d9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    const v1, 0x7f0a010d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    invoke-virtual {p0}, Lflipboard/gui/grid/DragDropGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080057

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 257
    :goto_7
    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 258
    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->h:I

    iput v0, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    goto/16 :goto_0

    .line 256
    :cond_8
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->r:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lflipboard/gui/grid/DragDropGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200d8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    const v1, 0x7f0a010d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    invoke-virtual {p0}, Lflipboard/gui/grid/DragDropGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02022d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_7

    .line 263
    :pswitch_2
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    if-eqz v0, :cond_9

    .line 264
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 265
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    invoke-virtual {p0, v0}, Lflipboard/gui/grid/DragDropGrid;->removeView(Landroid/view/View;)V

    .line 266
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->o:Landroid/view/View;

    .line 268
    :cond_9
    invoke-direct {p0}, Lflipboard/gui/grid/DragDropGrid;->d()Z

    move-result v0

    if-nez v0, :cond_a

    .line 269
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->c:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 270
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lflipboard/gui/grid/DragDropGrid;->d(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/gui/grid/DragDropGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 271
    if-eqz v0, :cond_0

    .line 272
    iget-object v1, p0, Lflipboard/gui/grid/DragDropGrid;->c:Landroid/view/View$OnClickListener;

    invoke-interface {v1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_0

    .line 275
    :cond_a
    iget v1, p0, Lflipboard/gui/grid/DragDropGrid;->h:I

    .line 276
    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_b

    .line 277
    iget v0, p0, Lflipboard/gui/grid/DragDropGrid;->h:I

    iput v0, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    .line 279
    :cond_b
    iget v2, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    .line 280
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-int v3, v3

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iget-object v5, p0, Lflipboard/gui/grid/DragDropGrid;->r:Landroid/widget/ImageButton;

    invoke-virtual {v5, v4}, Landroid/widget/ImageButton;->getHitRect(Landroid/graphics/Rect;)V

    add-int/lit8 v5, v0, 0x1

    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v4, v0, v3, v5, v6}, Landroid/graphics/Rect;->intersect(IIII)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->r:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lflipboard/gui/grid/DragDropGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200d9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x1

    :goto_8
    if-eqz v0, :cond_d

    .line 282
    new-instance v3, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 283
    invoke-virtual {p0}, Lflipboard/gui/grid/DragDropGrid;->getContext()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0d00c3

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 284
    invoke-virtual {p0}, Lflipboard/gui/grid/DragDropGrid;->getContext()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0d00c4

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lflipboard/gui/grid/DragDropGrid;->b:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    const/4 v7, 0x0

    invoke-interface {v6, v7, v2}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->b(II)Lflipboard/service/Section;

    move-result-object v6

    invoke-virtual {v6}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 285
    const v0, 0x7f0d004a

    invoke-virtual {v3, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 286
    const v0, 0x7f0d00c3

    invoke-virtual {v3, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 287
    new-instance v0, Lflipboard/gui/grid/DragDropGrid$4;

    invoke-direct {v0, p0, v1, v2}, Lflipboard/gui/grid/DragDropGrid$4;-><init>(Lflipboard/gui/grid/DragDropGrid;II)V

    iput-object v0, v3, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 305
    invoke-virtual {p0}, Lflipboard/gui/grid/DragDropGrid;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "remove_subscription"

    invoke-virtual {v3, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 309
    :goto_9
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->r:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 310
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/grid/DragDropGrid;->m:Z

    .line 311
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/gui/grid/DragDropGrid;->h:I

    .line 312
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/gui/grid/DragDropGrid;->n:I

    .line 313
    iget-object v0, p0, Lflipboard/gui/grid/DragDropGrid;->d:Lflipboard/gui/grid/PagedContainer;

    invoke-interface {v0}, Lflipboard/gui/grid/PagedContainer;->c()V

    .line 314
    invoke-direct {p0}, Lflipboard/gui/grid/DragDropGrid;->b()V

    goto/16 :goto_0

    .line 280
    :cond_c
    const/4 v0, 0x0

    goto :goto_8

    .line 307
    :cond_d
    invoke-direct {p0, v1, v2}, Lflipboard/gui/grid/DragDropGrid;->a(II)V

    goto :goto_9

    .line 320
    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_f
    move v0, v1

    goto/16 :goto_4

    .line 241
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setContainer(Lflipboard/gui/grid/PagedDragDropGrid;)V
    .locals 0

    .prologue
    .line 864
    iput-object p1, p0, Lflipboard/gui/grid/DragDropGrid;->d:Lflipboard/gui/grid/PagedContainer;

    .line 865
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lflipboard/gui/grid/DragDropGrid;->c:Landroid/view/View$OnClickListener;

    .line 143
    return-void
.end method
