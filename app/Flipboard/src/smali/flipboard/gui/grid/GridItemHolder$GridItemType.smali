.class public final enum Lflipboard/gui/grid/GridItemHolder$GridItemType;
.super Ljava/lang/Enum;
.source "GridItemHolder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/grid/GridItemHolder$GridItemType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/grid/GridItemHolder$GridItemType;

.field public static final enum b:Lflipboard/gui/grid/GridItemHolder$GridItemType;

.field public static final enum c:Lflipboard/gui/grid/GridItemHolder$GridItemType;

.field public static final enum d:Lflipboard/gui/grid/GridItemHolder$GridItemType;

.field private static final synthetic e:[Lflipboard/gui/grid/GridItemHolder$GridItemType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    new-instance v0, Lflipboard/gui/grid/GridItemHolder$GridItemType;

    const-string v1, "DYNAMIC_GRID_ITEM"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/grid/GridItemHolder$GridItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/grid/GridItemHolder$GridItemType;->a:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    .line 18
    new-instance v0, Lflipboard/gui/grid/GridItemHolder$GridItemType;

    const-string v1, "MY_MAGAZINE_ITEM"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/grid/GridItemHolder$GridItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/grid/GridItemHolder$GridItemType;->b:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    .line 19
    new-instance v0, Lflipboard/gui/grid/GridItemHolder$GridItemType;

    const-string v1, "MY_PROFILE_ITEM"

    invoke-direct {v0, v1, v4}, Lflipboard/gui/grid/GridItemHolder$GridItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/grid/GridItemHolder$GridItemType;->c:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    .line 20
    new-instance v0, Lflipboard/gui/grid/GridItemHolder$GridItemType;

    const-string v1, "EDIT_OR_MORE_ITEM"

    invoke-direct {v0, v1, v5}, Lflipboard/gui/grid/GridItemHolder$GridItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/grid/GridItemHolder$GridItemType;->d:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    .line 16
    const/4 v0, 0x4

    new-array v0, v0, [Lflipboard/gui/grid/GridItemHolder$GridItemType;

    sget-object v1, Lflipboard/gui/grid/GridItemHolder$GridItemType;->a:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/grid/GridItemHolder$GridItemType;->b:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/gui/grid/GridItemHolder$GridItemType;->c:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/gui/grid/GridItemHolder$GridItemType;->d:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    aput-object v1, v0, v5

    sput-object v0, Lflipboard/gui/grid/GridItemHolder$GridItemType;->e:[Lflipboard/gui/grid/GridItemHolder$GridItemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/grid/GridItemHolder$GridItemType;
    .locals 1

    .prologue
    .line 16
    const-class v0, Lflipboard/gui/grid/GridItemHolder$GridItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/grid/GridItemHolder$GridItemType;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/grid/GridItemHolder$GridItemType;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lflipboard/gui/grid/GridItemHolder$GridItemType;->e:[Lflipboard/gui/grid/GridItemHolder$GridItemType;

    invoke-virtual {v0}, [Lflipboard/gui/grid/GridItemHolder$GridItemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/grid/GridItemHolder$GridItemType;

    return-object v0
.end method
