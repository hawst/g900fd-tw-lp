.class public Lflipboard/gui/grid/PagedDragDropGrid;
.super Landroid/widget/ScrollView;
.source "PagedDragDropGrid.java"

# interfaces
.implements Lflipboard/gui/grid/PagedContainer;


# instance fields
.field a:I

.field b:Lflipboard/gui/grid/DragDropGrid;

.field c:Lflipboard/gui/grid/PagedDragDropGridAdapter;

.field d:I

.field e:I

.field f:Lflipboard/gui/grid/PagedDragDropGrid$DragListener;

.field private g:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/grid/PagedDragDropGrid;->a:I

    .line 45
    invoke-direct {p0}, Lflipboard/gui/grid/PagedDragDropGrid;->f()V

    .line 46
    invoke-direct {p0}, Lflipboard/gui/grid/PagedDragDropGrid;->e()V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/grid/PagedDragDropGrid;->a:I

    .line 38
    invoke-direct {p0}, Lflipboard/gui/grid/PagedDragDropGrid;->f()V

    .line 39
    invoke-direct {p0}, Lflipboard/gui/grid/PagedDragDropGrid;->e()V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/grid/PagedDragDropGrid;->a:I

    .line 31
    invoke-direct {p0}, Lflipboard/gui/grid/PagedDragDropGrid;->f()V

    .line 32
    invoke-direct {p0}, Lflipboard/gui/grid/PagedDragDropGrid;->e()V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILflipboard/gui/grid/PagedDragDropGridAdapter;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/grid/PagedDragDropGrid;->a:I

    .line 52
    iput-object p4, p0, Lflipboard/gui/grid/PagedDragDropGrid;->c:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    .line 53
    invoke-direct {p0}, Lflipboard/gui/grid/PagedDragDropGrid;->f()V

    .line 54
    invoke-direct {p0}, Lflipboard/gui/grid/PagedDragDropGrid;->e()V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lflipboard/gui/grid/PagedDragDropGridAdapter;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/grid/PagedDragDropGrid;->a:I

    .line 60
    iput-object p3, p0, Lflipboard/gui/grid/PagedDragDropGrid;->c:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    .line 61
    invoke-direct {p0}, Lflipboard/gui/grid/PagedDragDropGrid;->f()V

    .line 62
    invoke-direct {p0}, Lflipboard/gui/grid/PagedDragDropGrid;->e()V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lflipboard/gui/grid/PagedDragDropGridAdapter;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/grid/PagedDragDropGrid;->a:I

    .line 68
    iput-object p2, p0, Lflipboard/gui/grid/PagedDragDropGrid;->c:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    .line 69
    invoke-direct {p0}, Lflipboard/gui/grid/PagedDragDropGrid;->f()V

    .line 70
    invoke-direct {p0}, Lflipboard/gui/grid/PagedDragDropGrid;->e()V

    .line 71
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 75
    new-instance v0, Lflipboard/gui/grid/DragDropGrid;

    invoke-virtual {p0}, Lflipboard/gui/grid/PagedDragDropGrid;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lflipboard/gui/grid/DragDropGrid;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/grid/PagedDragDropGrid;->b:Lflipboard/gui/grid/DragDropGrid;

    .line 76
    iget-object v0, p0, Lflipboard/gui/grid/PagedDragDropGrid;->b:Lflipboard/gui/grid/DragDropGrid;

    invoke-virtual {p0, v0}, Lflipboard/gui/grid/PagedDragDropGrid;->addView(Landroid/view/View;)V

    .line 77
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lflipboard/gui/grid/PagedDragDropGrid$1;

    invoke-direct {v0, p0}, Lflipboard/gui/grid/PagedDragDropGrid$1;-><init>(Lflipboard/gui/grid/PagedDragDropGrid;)V

    invoke-virtual {p0, v0}, Lflipboard/gui/grid/PagedDragDropGrid;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 99
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lflipboard/gui/grid/PagedDragDropGrid;->f:Lflipboard/gui/grid/PagedDragDropGrid$DragListener;

    .line 214
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lflipboard/gui/grid/PagedDragDropGrid;->a:I

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/grid/PagedDragDropGrid;->requestDisallowInterceptTouchEvent(Z)V

    .line 161
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflipboard/gui/grid/PagedDragDropGrid;->requestDisallowInterceptTouchEvent(Z)V

    .line 166
    return-void
.end method

.method public getGridHeight()I
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Lflipboard/gui/grid/PagedDragDropGrid;->d:I

    return v0
.end method

.method public getGridWidth()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lflipboard/gui/grid/PagedDragDropGrid;->e:I

    return v0
.end method

.method public setClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 111
    iput-object p1, p0, Lflipboard/gui/grid/PagedDragDropGrid;->g:Landroid/view/View$OnClickListener;

    .line 112
    iget-object v0, p0, Lflipboard/gui/grid/PagedDragDropGrid;->b:Lflipboard/gui/grid/DragDropGrid;

    invoke-virtual {v0, p1}, Lflipboard/gui/grid/DragDropGrid;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    return-void
.end method

.method public setDragListener(Lflipboard/gui/grid/PagedDragDropGrid$DragListener;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lflipboard/gui/grid/PagedDragDropGrid;->f:Lflipboard/gui/grid/PagedDragDropGrid$DragListener;

    .line 227
    return-void
.end method
