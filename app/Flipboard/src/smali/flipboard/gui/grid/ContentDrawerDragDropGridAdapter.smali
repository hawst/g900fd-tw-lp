.class public Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;
.super Ljava/lang/Object;
.source "ContentDrawerDragDropGridAdapter.java"

# interfaces
.implements Lflipboard/gui/grid/PagedDragDropGridAdapter;


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/grid/Page;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;

.field private d:Lflipboard/gui/grid/PagedDragDropGrid;


# direct methods
.method static synthetic a(Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;)Lflipboard/gui/grid/PagedDragDropGrid;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->d:Lflipboard/gui/grid/PagedDragDropGrid;

    return-object v0
.end method

.method static synthetic b(Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->b:Landroid/content/Context;

    return-object v0
.end method

.method private b(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 76
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/grid/Page;

    iget-object v0, v0, Lflipboard/gui/grid/Page;->a:Ljava/util/List;

    .line 78
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)I
    .locals 1

    .prologue
    .line 219
    invoke-direct {p0, p1}, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->b(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(II)Landroid/view/View;
    .locals 5

    .prologue
    .line 84
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f030049

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 86
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f090043

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 92
    :goto_0
    iget-object v2, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09007b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 93
    iget-object v3, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0004

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 94
    add-int/lit8 v4, v3, 0x1

    mul-int/2addr v2, v4

    sub-int/2addr v0, v2

    div-int/2addr v0, v3

    .line 95
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 96
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 99
    new-instance v0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$1;

    invoke-direct {v0, p0}, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$1;-><init>(Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 107
    new-instance v0, Lflipboard/gui/grid/GridItemHolder;

    sget-object v2, Lflipboard/gui/grid/GridItemHolder$GridItemType;->a:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    invoke-direct {v0, v2, p1, p2}, Lflipboard/gui/grid/GridItemHolder;-><init>(Lflipboard/gui/grid/GridItemHolder$GridItemType;II)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 108
    return-object v1

    .line 89
    :cond_0
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->d:Lflipboard/gui/grid/PagedDragDropGrid;

    invoke-virtual {v0}, Lflipboard/gui/grid/PagedDragDropGrid;->getMeasuredWidth()I

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Lflipboard/service/Section;)Lflipboard/service/Flap$TypedResultObserver;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lflipboard/service/Section;",
            ")",
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/objs/RecentImage;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 113
    const v0, 0x7f0a010e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    .line 114
    const v1, 0x7f0a010d

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    .line 115
    new-instance v2, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;

    invoke-direct {v2, p0, p2, v1}, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter$2;-><init>(Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;Lflipboard/service/Section;Lflipboard/gui/FLImageView;)V

    .line 189
    invoke-virtual {p2}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    return-object v2
.end method

.method public final a(III)V
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/grid/Page;

    iget-object v0, v0, Lflipboard/gui/grid/Page;->a:Ljava/util/List;

    invoke-static {v0, p2, p3}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 225
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 205
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->a(I)I

    move-result v0

    .line 206
    if-nez v0, :cond_0

    .line 207
    const/4 v0, 0x1

    .line 209
    :goto_0
    return v0

    :cond_0
    rem-int/lit8 v1, v0, 0x3

    if-nez v1, :cond_1

    div-int/lit8 v0, v0, 0x3

    goto :goto_0

    :cond_1
    div-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final b(II)Lflipboard/service/Section;
    .locals 2

    .prologue
    .line 195
    invoke-direct {p0, p1}, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->b(I)Ljava/util/List;

    move-result-object v0

    .line 196
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge p2, v1, :cond_0

    .line 197
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 199
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    goto :goto_0
.end method

.method public final c(II)V
    .locals 6

    .prologue
    .line 254
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/grid/Page;

    iget-object v0, v0, Lflipboard/gui/grid/Page;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 255
    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->d:Lflipboard/gui/grid/PagedDragDropGrid;

    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    iget-object v0, v1, Lflipboard/gui/grid/PagedDragDropGrid;->f:Lflipboard/gui/grid/PagedDragDropGrid$DragListener;

    if-eqz v0, :cond_0

    iget-object v0, v1, Lflipboard/gui/grid/PagedDragDropGrid;->f:Lflipboard/gui/grid/PagedDragDropGrid$DragListener;

    .line 256
    :cond_0
    iget-object v0, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/grid/Page;

    iget-object v0, v0, Lflipboard/gui/grid/Page;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 257
    iget-object v1, p0, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->d:Lflipboard/gui/grid/PagedDragDropGrid;

    invoke-virtual {v1}, Lflipboard/gui/grid/PagedDragDropGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09007b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1}, Lflipboard/gui/grid/PagedDragDropGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0004

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget v3, v1, Lflipboard/gui/grid/PagedDragDropGrid;->e:I

    add-int/2addr v3, v2

    add-int/lit8 v4, v0, 0x1

    mul-int/2addr v4, v2

    sub-int/2addr v3, v4

    div-int/2addr v3, v0

    iget-object v4, v1, Lflipboard/gui/grid/PagedDragDropGrid;->c:Lflipboard/gui/grid/PagedDragDropGridAdapter;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lflipboard/gui/grid/PagedDragDropGridAdapter;->a(I)I

    move-result v4

    rem-int v5, v4, v0

    if-nez v5, :cond_1

    div-int v0, v4, v0

    :goto_0
    add-int/2addr v2, v3

    mul-int/2addr v0, v2

    iput v0, v1, Lflipboard/gui/grid/PagedDragDropGrid;->d:I

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v2, v1, Lflipboard/gui/grid/PagedDragDropGrid;->e:I

    iget v3, v1, Lflipboard/gui/grid/PagedDragDropGrid;->d:I

    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Lflipboard/gui/grid/PagedDragDropGrid;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1}, Lflipboard/gui/grid/PagedDragDropGrid;->requestLayout()V

    .line 258
    return-void

    .line 257
    :cond_1
    div-int v0, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final d(II)V
    .locals 2

    .prologue
    .line 263
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 264
    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lflipboard/service/User;->a(IIZ)Z

    .line 265
    return-void
.end method
