.class public Lflipboard/gui/ViewWithDrawerLayout;
.super Landroid/view/ViewGroup;
.source "ViewWithDrawerLayout.java"


# instance fields
.field public a:Z

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method static synthetic a(Lflipboard/gui/ViewWithDrawerLayout;)Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->d:Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;

    return-object v0
.end method

.method static synthetic b(Lflipboard/gui/ViewWithDrawerLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    const-wide/16 v6, 0x12c

    const/4 v5, 0x1

    .line 106
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    if-lez v0, :cond_2

    .line 107
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    const-string v1, "translationY"

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    aput v4, v2, v3

    const/4 v3, 0x0

    aput v3, v2, v5

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 108
    new-instance v1, Lflipboard/gui/ViewWithDrawerLayout$AnimatorListener;

    invoke-direct {v1, p0}, Lflipboard/gui/ViewWithDrawerLayout$AnimatorListener;-><init>(Lflipboard/gui/ViewWithDrawerLayout;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 109
    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 110
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 111
    invoke-virtual {v0, v5}, Landroid/animation/ObjectAnimator;->setAutoCancel(Z)V

    .line 113
    :cond_0
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 114
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->d:Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->d:Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;

    invoke-interface {v0}, Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;->a()V

    .line 117
    :cond_1
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->d:Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;

    if-eqz v0, :cond_2

    .line 118
    new-instance v0, Lflipboard/gui/ViewWithDrawerLayout$1;

    invoke-direct {v0, p0}, Lflipboard/gui/ViewWithDrawerLayout$1;-><init>(Lflipboard/gui/ViewWithDrawerLayout;)V

    invoke-virtual {p0, v0, v6, v7}, Lflipboard/gui/ViewWithDrawerLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 128
    :cond_2
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 49
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->c:Landroid/view/View;

    if-nez v0, :cond_1

    .line 50
    iput-object p1, p0, Lflipboard/gui/ViewWithDrawerLayout;->c:Landroid/view/View;

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    if-nez v0, :cond_0

    .line 52
    iput-object p1, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    goto :goto_0
.end method

.method public final b()V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    const-wide/16 v6, 0x12c

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 132
    iget-boolean v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->a:Z

    if-eqz v0, :cond_2

    .line 133
    iput-boolean v4, p0, Lflipboard/gui/ViewWithDrawerLayout;->a:Z

    .line 134
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    const-string v1, "translationY"

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v3, v2, v4

    iget-object v3, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    aput v3, v2, v5

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 135
    new-instance v1, Lflipboard/gui/ViewWithDrawerLayout$AnimatorListener;

    invoke-direct {v1, p0}, Lflipboard/gui/ViewWithDrawerLayout$AnimatorListener;-><init>(Lflipboard/gui/ViewWithDrawerLayout;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 136
    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 137
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 138
    invoke-virtual {v0, v5}, Landroid/animation/ObjectAnimator;->setAutoCancel(Z)V

    .line 140
    :cond_0
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 141
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->d:Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->d:Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;

    invoke-interface {v0}, Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;->c()V

    .line 144
    :cond_1
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->d:Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;

    if-eqz v0, :cond_2

    .line 145
    new-instance v0, Lflipboard/gui/ViewWithDrawerLayout$2;

    invoke-direct {v0, p0}, Lflipboard/gui/ViewWithDrawerLayout$2;-><init>(Lflipboard/gui/ViewWithDrawerLayout;)V

    invoke-virtual {p0, v0, v6, v7}, Lflipboard/gui/ViewWithDrawerLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 155
    :cond_2
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 79
    sub-int v2, p5, p3

    .line 80
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->c:Landroid/view/View;

    iget-object v3, p0, Lflipboard/gui/ViewWithDrawerLayout;->c:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/ViewWithDrawerLayout;->c:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v0, v1, v1, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 83
    :cond_0
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 84
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 85
    :goto_0
    iget-object v3, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    iget-object v4, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v2, v4

    iget-object v5, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    invoke-virtual {v3, v1, v4, v5, v2}, Landroid/view/View;->layout(IIII)V

    .line 86
    iget-boolean v1, p0, Lflipboard/gui/ViewWithDrawerLayout;->a:Z

    if-nez v1, :cond_1

    .line 87
    iget-object v1, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    iget-object v2, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 89
    :cond_1
    iget-boolean v1, p0, Lflipboard/gui/ViewWithDrawerLayout;->a:Z

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 91
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    iget-object v1, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 92
    invoke-virtual {p0}, Lflipboard/gui/ViewWithDrawerLayout;->a()V

    .line 95
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 84
    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    .line 68
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 69
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->c:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 72
    :cond_0
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    invoke-virtual {p0}, Lflipboard/gui/ViewWithDrawerLayout;->getMeasuredWidth()I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/ViewWithDrawerLayout;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    const v3, 0x3ee66666    # 0.45f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    const/high16 v3, -0x80000000

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 75
    :cond_1
    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 59
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 60
    iput-object v1, p0, Lflipboard/gui/ViewWithDrawerLayout;->b:Landroid/view/View;

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    iget-object v0, p0, Lflipboard/gui/ViewWithDrawerLayout;->c:Landroid/view/View;

    if-ne v0, p1, :cond_0

    .line 62
    iput-object v1, p0, Lflipboard/gui/ViewWithDrawerLayout;->c:Landroid/view/View;

    goto :goto_0
.end method

.method public setAnimationListener(Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lflipboard/gui/ViewWithDrawerLayout;->d:Lflipboard/gui/ViewWithDrawerLayout$AnimationListener;

    .line 167
    return-void
.end method
