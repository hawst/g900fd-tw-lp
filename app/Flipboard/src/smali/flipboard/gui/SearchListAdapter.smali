.class public Lflipboard/gui/SearchListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SearchListAdapter.java"


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SearchResultItem;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lflipboard/activities/FlipboardActivity;

.field private c:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lflipboard/activities/FlipboardActivity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/activities/FlipboardActivity;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 39
    iput-object p1, p0, Lflipboard/gui/SearchListAdapter;->b:Lflipboard/activities/FlipboardActivity;

    .line 40
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Lflipboard/activities/FlipboardActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lflipboard/gui/SearchListAdapter;->c:Landroid/view/LayoutInflater;

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    .line 45
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 400
    const/4 v1, -0x1

    .line 401
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 402
    iget-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultItem;

    .line 403
    iget-object v3, v0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v0, v0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v2, v1

    .line 401
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 407
    :cond_1
    return v2
.end method

.method public final a(I)Lflipboard/objs/SearchResultItem;
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultItem;

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 443
    return-void
.end method

.method public final a(ILflipboard/objs/SearchResultItem;)V
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 381
    invoke-virtual {p0}, Lflipboard/gui/SearchListAdapter;->notifyDataSetChanged()V

    .line 382
    return-void
.end method

.method public final a(Lflipboard/objs/SearchResultItem;)V
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 376
    invoke-virtual {p0}, Lflipboard/gui/SearchListAdapter;->notifyDataSetChanged()V

    .line 377
    return-void
.end method

.method public final a(Ljava/util/Comparator;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<",
            "Lflipboard/objs/SearchResultItem;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 416
    iget-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    iget-object v1, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, p2, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 417
    invoke-virtual {p0}, Lflipboard/gui/SearchListAdapter;->notifyDataSetChanged()V

    .line 418
    return-void
.end method

.method public final b(Lflipboard/objs/SearchResultItem;)I
    .locals 2

    .prologue
    .line 429
    iget-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 430
    if-ltz v0, :cond_0

    .line 431
    iget-object v1, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 432
    invoke-virtual {p0}, Lflipboard/gui/SearchListAdapter;->notifyDataSetChanged()V

    .line 434
    :cond_0
    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 96
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 54
    .line 57
    iget-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 81
    :goto_0
    return v0

    .line 60
    :cond_1
    iget-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultItem;

    .line 61
    iget-object v0, v0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    .line 62
    sget-object v2, Lflipboard/objs/SearchResultItem;->j:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 63
    const/4 v0, 0x0

    goto :goto_0

    .line 64
    :cond_2
    sget-object v2, Lflipboard/objs/SearchResultItem;->k:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 65
    const/4 v0, 0x1

    goto :goto_0

    .line 66
    :cond_3
    sget-object v2, Lflipboard/objs/SearchResultItem;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    sget-object v2, Lflipboard/objs/SearchResultItem;->m:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    sget-object v2, Lflipboard/objs/SearchResultItem;->q:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 67
    :cond_4
    const/4 v0, 0x2

    goto :goto_0

    .line 68
    :cond_5
    sget-object v2, Lflipboard/objs/SearchResultItem;->n:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 69
    const/4 v0, 0x3

    goto :goto_0

    .line 70
    :cond_6
    sget-object v2, Lflipboard/objs/SearchResultItem;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 71
    const/4 v0, 0x4

    goto :goto_0

    .line 72
    :cond_7
    sget-object v2, Lflipboard/objs/SearchResultItem;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 73
    const/4 v0, 0x5

    goto :goto_0

    .line 74
    :cond_8
    sget-object v2, Lflipboard/objs/SearchResultItem;->o:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 75
    const/4 v0, 0x6

    goto :goto_0

    .line 76
    :cond_9
    sget-object v2, Lflipboard/objs/SearchResultItem;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 77
    const/4 v0, 0x7

    goto :goto_0

    .line 78
    :cond_a
    sget-object v2, Lflipboard/objs/SearchResultItem;->p:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 79
    const/16 v0, 0x8

    goto :goto_0

    :cond_b
    move v0, v1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const v3, 0x7f0a0273

    const/16 v9, 0x8

    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 104
    iget-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 366
    :cond_0
    :goto_0
    return-object p2

    .line 107
    :cond_1
    iget-object v0, p0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultItem;

    .line 109
    iget-object v1, v0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v2, Lflipboard/objs/SearchResultItem;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 111
    if-eqz p2, :cond_2

    .line 113
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/SearchListAdapter$ViewHolderSeeAll;

    .line 120
    :goto_1
    iget-object v2, p0, Lflipboard/gui/SearchListAdapter;->b:Lflipboard/activities/FlipboardActivity;

    const v3, 0x7f0d0299

    invoke-virtual {v2, v3}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 121
    iget-object v1, v1, Lflipboard/gui/SearchListAdapter$ViewHolderSeeAll;->a:Lflipboard/gui/FLLabelTextView;

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v0, v0, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    aput-object v0, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 115
    :cond_2
    iget-object v1, p0, Lflipboard/gui/SearchListAdapter;->b:Lflipboard/activities/FlipboardActivity;

    const v2, 0x7f030100

    invoke-static {v1, v2, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 116
    new-instance v2, Lflipboard/gui/SearchListAdapter$ViewHolderSeeAll;

    invoke-direct {v2}, Lflipboard/gui/SearchListAdapter$ViewHolderSeeAll;-><init>()V

    .line 117
    const v1, 0x7f0a02d2

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLLabelTextView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSeeAll;->a:Lflipboard/gui/FLLabelTextView;

    .line 118
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    goto :goto_1

    .line 122
    :cond_3
    iget-object v1, v0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v2, Lflipboard/objs/SearchResultItem;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 124
    if-eqz p2, :cond_4

    .line 126
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/SearchListAdapter$ViewHolderHeader;

    .line 133
    :goto_2
    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 134
    iget-object v1, v1, Lflipboard/gui/SearchListAdapter$ViewHolderHeader;->a:Lflipboard/gui/FLTextIntf;

    iget-object v0, v0, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 128
    :cond_4
    iget-object v1, p0, Lflipboard/gui/SearchListAdapter;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f0300fd

    invoke-virtual {v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 129
    new-instance v2, Lflipboard/gui/SearchListAdapter$ViewHolderHeader;

    invoke-direct {v2}, Lflipboard/gui/SearchListAdapter$ViewHolderHeader;-><init>()V

    .line 130
    const v1, 0x7f0a02cf

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderHeader;->a:Lflipboard/gui/FLTextIntf;

    .line 131
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    goto :goto_2

    .line 136
    :cond_5
    iget-object v1, v0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v2, Lflipboard/objs/SearchResultItem;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 138
    if-eqz p2, :cond_7

    .line 140
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/SearchListAdapter$ViewHolderHeader;

    .line 148
    :goto_3
    iget-object v2, v1, Lflipboard/gui/SearchListAdapter$ViewHolderHeader;->a:Lflipboard/gui/FLTextIntf;

    iget-object v3, v0, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 149
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v0, Lflipboard/objs/SearchResultItem;->v:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v2

    .line 151
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->j()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 152
    iget-object v3, v1, Lflipboard/gui/SearchListAdapter$ViewHolderHeader;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 153
    iget-object v2, v1, Lflipboard/gui/SearchListAdapter$ViewHolderHeader;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v2, v8}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 157
    :goto_4
    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->L:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 159
    :try_start_0
    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->L:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    .line 160
    invoke-virtual {p2, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 161
    iget-object v2, v1, Lflipboard/gui/SearchListAdapter$ViewHolderHeader;->a:Lflipboard/gui/FLTextIntf;

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0800aa

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-interface {v2, v3}, Lflipboard/gui/FLTextIntf;->setTextColor(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 162
    :catch_0
    move-exception v2

    .line 163
    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v0, v0, Lflipboard/objs/SearchResultItem;->L:Ljava/lang/String;

    aput-object v0, v3, v8

    .line 164
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 165
    :cond_6
    const v0, 0x7f0200ca

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, v1, Lflipboard/gui/SearchListAdapter$ViewHolderHeader;->a:Lflipboard/gui/FLTextIntf;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080023

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setTextColor(I)V

    goto/16 :goto_0

    .line 142
    :cond_7
    iget-object v1, p0, Lflipboard/gui/SearchListAdapter;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f0300fe

    invoke-virtual {v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 143
    new-instance v2, Lflipboard/gui/SearchListAdapter$ViewHolderHeader;

    invoke-direct {v2}, Lflipboard/gui/SearchListAdapter$ViewHolderHeader;-><init>()V

    .line 144
    const v1, 0x7f0a004f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderHeader;->a:Lflipboard/gui/FLTextIntf;

    .line 145
    const v1, 0x7f0a0133

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderHeader;->b:Lflipboard/gui/FLImageView;

    .line 146
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    goto/16 :goto_3

    .line 155
    :cond_8
    iget-object v2, v1, Lflipboard/gui/SearchListAdapter$ViewHolderHeader;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v2, v9}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto :goto_4

    .line 170
    :cond_9
    iget-object v1, v0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v2, Lflipboard/objs/SearchResultItem;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 172
    if-eqz p2, :cond_a

    .line 174
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/SearchListAdapter$ViewHolderTopic;

    .line 181
    :goto_5
    iget-object v1, v1, Lflipboard/gui/SearchListAdapter$ViewHolderTopic;->a:Lflipboard/gui/FLLabelTextView;

    iget-object v0, v0, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 364
    :goto_6
    invoke-virtual {v1, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 176
    :cond_a
    iget-object v1, p0, Lflipboard/gui/SearchListAdapter;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f03014c

    invoke-virtual {v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 177
    new-instance v2, Lflipboard/gui/SearchListAdapter$ViewHolderTopic;

    invoke-direct {v2}, Lflipboard/gui/SearchListAdapter$ViewHolderTopic;-><init>()V

    .line 178
    const v1, 0x7f0a0089

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLLabelTextView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderTopic;->a:Lflipboard/gui/FLLabelTextView;

    .line 179
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    goto :goto_5

    .line 182
    :cond_b
    iget-object v1, v0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v2, Lflipboard/objs/SearchResultItem;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 184
    if-eqz p2, :cond_c

    .line 186
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/SearchListAdapter$ViewHolderSource;

    .line 195
    :goto_7
    iget-object v2, v1, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->a:Lflipboard/gui/FLImageView;

    iget-object v3, v0, Lflipboard/objs/SearchResultItem;->t:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 196
    iget-object v2, v1, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->b:Lflipboard/gui/FLLabelTextView;

    iget-object v3, v0, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v1, v1, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->c:Lflipboard/gui/FLLabelTextView;

    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->s:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v0, v0, Lflipboard/objs/SearchResultItem;->s:Ljava/lang/String;

    :goto_8
    invoke-virtual {v1, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 188
    :cond_c
    iget-object v1, p0, Lflipboard/gui/SearchListAdapter;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f03012b

    invoke-virtual {v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 189
    new-instance v2, Lflipboard/gui/SearchListAdapter$ViewHolderSource;

    invoke-direct {v2}, Lflipboard/gui/SearchListAdapter$ViewHolderSource;-><init>()V

    .line 190
    const v1, 0x7f0a0336

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->a:Lflipboard/gui/FLImageView;

    .line 191
    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLLabelTextView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->b:Lflipboard/gui/FLLabelTextView;

    .line 192
    const v1, 0x7f0a0337

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLLabelTextView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->c:Lflipboard/gui/FLLabelTextView;

    .line 193
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    goto :goto_7

    .line 197
    :cond_d
    const-string v0, ""

    goto :goto_8

    .line 198
    :cond_e
    iget-object v1, v0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v2, Lflipboard/objs/SearchResultItem;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 200
    if-eqz p2, :cond_f

    .line 202
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/SearchListAdapter$ViewHolderSource;

    .line 211
    :goto_9
    iget-object v2, v1, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->a:Lflipboard/gui/FLImageView;

    iget-object v3, v0, Lflipboard/objs/SearchResultItem;->t:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 212
    iget-object v2, v1, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->b:Lflipboard/gui/FLLabelTextView;

    iget-object v3, v0, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    iget-object v1, v1, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->c:Lflipboard/gui/FLLabelTextView;

    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->s:Ljava/lang/String;

    if-eqz v2, :cond_10

    iget-object v0, v0, Lflipboard/objs/SearchResultItem;->s:Ljava/lang/String;

    :goto_a
    invoke-virtual {v1, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 204
    :cond_f
    iget-object v1, p0, Lflipboard/gui/SearchListAdapter;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f03012b

    invoke-virtual {v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 205
    new-instance v2, Lflipboard/gui/SearchListAdapter$ViewHolderSource;

    invoke-direct {v2}, Lflipboard/gui/SearchListAdapter$ViewHolderSource;-><init>()V

    .line 206
    const v1, 0x7f0a0336

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->a:Lflipboard/gui/FLImageView;

    .line 207
    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLLabelTextView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->b:Lflipboard/gui/FLLabelTextView;

    .line 208
    const v1, 0x7f0a0337

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLLabelTextView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->c:Lflipboard/gui/FLLabelTextView;

    .line 209
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    goto :goto_9

    .line 213
    :cond_10
    const-string v0, ""

    goto :goto_a

    .line 214
    :cond_11
    iget-object v1, v0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v2, Lflipboard/objs/SearchResultItem;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 216
    if-eqz p2, :cond_12

    .line 218
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/SearchListAdapter$ViewHolderMagazine;

    .line 229
    :goto_b
    iget-object v2, v1, Lflipboard/gui/SearchListAdapter$ViewHolderMagazine;->a:Lflipboard/gui/FLImageView;

    iget-object v3, v0, Lflipboard/objs/SearchResultItem;->t:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 230
    iget-object v2, v1, Lflipboard/gui/SearchListAdapter$ViewHolderMagazine;->b:Lflipboard/gui/FLLabelTextView;

    iget-object v3, v0, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    iget-object v3, v1, Lflipboard/gui/SearchListAdapter$ViewHolderMagazine;->e:Lflipboard/gui/FLTextView;

    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->s:Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->s:Ljava/lang/String;

    :goto_c
    invoke-virtual {v3, v2}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    iget-object v3, v1, Lflipboard/gui/SearchListAdapter$ViewHolderMagazine;->c:Lflipboard/gui/FLLabelTextView;

    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->z:Ljava/lang/String;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lflipboard/gui/SearchListAdapter;->b:Lflipboard/activities/FlipboardActivity;

    const v4, 0x7f0d0324

    invoke-virtual {v2, v4}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, v0, Lflipboard/objs/SearchResultItem;->z:Ljava/lang/String;

    aput-object v5, v4, v8

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_d
    invoke-virtual {v3, v2}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    iget-object v1, v1, Lflipboard/gui/SearchListAdapter$ViewHolderMagazine;->d:Lflipboard/gui/FLLabelTextView;

    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->y:Ljava/lang/String;

    if-eqz v2, :cond_15

    iget-object v0, v0, Lflipboard/objs/SearchResultItem;->y:Ljava/lang/String;

    :goto_e
    invoke-virtual {v1, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 220
    :cond_12
    iget-object v1, p0, Lflipboard/gui/SearchListAdapter;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f0300d5

    invoke-virtual {v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 221
    new-instance v2, Lflipboard/gui/SearchListAdapter$ViewHolderMagazine;

    invoke-direct {v2}, Lflipboard/gui/SearchListAdapter$ViewHolderMagazine;-><init>()V

    .line 222
    const v1, 0x7f0a025e

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderMagazine;->a:Lflipboard/gui/FLImageView;

    .line 223
    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLLabelTextView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderMagazine;->b:Lflipboard/gui/FLLabelTextView;

    .line 224
    const v1, 0x7f0a0274

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLLabelTextView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderMagazine;->c:Lflipboard/gui/FLLabelTextView;

    .line 225
    const v1, 0x7f0a0276

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderMagazine;->e:Lflipboard/gui/FLTextView;

    .line 226
    const v1, 0x7f0a0275

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLLabelTextView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderMagazine;->d:Lflipboard/gui/FLLabelTextView;

    .line 227
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    goto/16 :goto_b

    .line 231
    :cond_13
    const-string v2, ""

    goto :goto_c

    .line 232
    :cond_14
    const-string v2, ""

    goto :goto_d

    .line 233
    :cond_15
    const-string v0, ""

    goto :goto_e

    .line 234
    :cond_16
    iget-object v1, v0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v2, Lflipboard/objs/SearchResultItem;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 236
    if-eqz p2, :cond_17

    .line 238
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/SearchListAdapter$ViewHolderSeeAll;

    .line 245
    :goto_f
    iget-object v1, v1, Lflipboard/gui/SearchListAdapter$ViewHolderSeeAll;->a:Lflipboard/gui/FLLabelTextView;

    check-cast v0, Lflipboard/gui/FLSearchView$CollapsedItemList;

    iget-object v2, v0, Lflipboard/gui/FLSearchView$CollapsedItemList;->d:Lflipboard/gui/FLSearchView;

    invoke-virtual {v2}, Lflipboard/gui/FLSearchView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d02f2

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v0, v0, Lflipboard/gui/FLSearchView$CollapsedItemList;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_6

    .line 240
    :cond_17
    iget-object v1, p0, Lflipboard/gui/SearchListAdapter;->b:Lflipboard/activities/FlipboardActivity;

    const v2, 0x7f030100

    invoke-static {v1, v2, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 241
    new-instance v2, Lflipboard/gui/SearchListAdapter$ViewHolderSeeAll;

    invoke-direct {v2}, Lflipboard/gui/SearchListAdapter$ViewHolderSeeAll;-><init>()V

    .line 242
    const v1, 0x7f0a02d2

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLLabelTextView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSeeAll;->a:Lflipboard/gui/FLLabelTextView;

    .line 243
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    goto :goto_f

    .line 247
    :cond_18
    iget-object v1, v0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v2, Lflipboard/objs/SearchResultItem;->p:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 248
    if-nez p2, :cond_0

    .line 251
    iget-object v0, p0, Lflipboard/gui/SearchListAdapter;->b:Lflipboard/activities/FlipboardActivity;

    const v1, 0x7f030050

    invoke-static {v0, v1, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    .line 253
    :cond_19
    iget-object v1, v0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v2, Lflipboard/objs/SearchResultItem;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_26

    .line 256
    if-eqz p2, :cond_1e

    .line 258
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;

    .line 259
    iget-object v2, v1, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->a()V

    .line 276
    :goto_10
    iget-object v2, v1, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;->b:Lflipboard/gui/FLTextIntf;

    .line 277
    iget-object v3, v1, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;->d:Lflipboard/gui/FLTextIntf;

    .line 278
    iget-object v4, v1, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;->a:Lflipboard/gui/FLImageView;

    .line 279
    sget-object v5, Lflipboard/gui/FLImageView$Align;->a:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v4, v5}, Lflipboard/gui/FLImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 280
    if-eqz v2, :cond_1a

    .line 281
    iget-object v5, v0, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    invoke-interface {v2, v5}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 283
    :cond_1a
    if-eqz v3, :cond_1b

    .line 284
    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->s:Ljava/lang/String;

    if-eqz v2, :cond_1f

    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->s:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1f

    .line 285
    invoke-static {v3, v8}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLTextIntf;I)V

    .line 286
    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->s:Ljava/lang/String;

    invoke-interface {v3, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 291
    :cond_1b
    :goto_11
    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->t:Ljava/lang/String;

    if-eqz v2, :cond_23

    .line 294
    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    if-eqz v2, :cond_20

    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    const-string v3, "users"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 295
    invoke-virtual {v4, v6}, Lflipboard/gui/FLImageView;->setClipRound(Z)V

    .line 299
    :goto_12
    invoke-static {v4, v8}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 302
    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->t:Ljava/lang/String;

    const-string v3, "R.drawable"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 304
    iget-object v2, p0, Lflipboard/gui/SearchListAdapter;->b:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v2}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, v0, Lflipboard/objs/SearchResultItem;->t:Ljava/lang/String;

    const-string v5, "drawable"

    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 305
    iget-object v6, v6, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 304
    invoke-virtual {v2, v3, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 306
    if-eqz v2, :cond_1c

    .line 307
    invoke-virtual {v4, v2}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    .line 312
    :cond_1c
    :goto_13
    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 313
    iget-boolean v3, v0, Lflipboard/objs/SearchResultItem;->K:Z

    if-eqz v3, :cond_22

    .line 314
    iget-object v3, p0, Lflipboard/gui/SearchListAdapter;->b:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v3}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09010f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 315
    iget-object v3, p0, Lflipboard/gui/SearchListAdapter;->b:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v3}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09010f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 326
    :goto_14
    iget-object v2, v1, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;->c:Lflipboard/gui/FLTextIntf;

    .line 327
    if-eqz v2, :cond_1d

    .line 328
    iget-object v3, v0, Lflipboard/objs/SearchResultItem;->J:Ljava/lang/String;

    .line 329
    if-eqz v3, :cond_24

    .line 330
    invoke-interface {v2, v3}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 336
    :cond_1d
    :goto_15
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v0, Lflipboard/objs/SearchResultItem;->v:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v2

    .line 339
    iget-object v1, v1, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;->e:Lflipboard/gui/FLImageView;

    .line 340
    if-eqz v1, :cond_0

    .line 341
    iget-boolean v3, v0, Lflipboard/objs/SearchResultItem;->I:Z

    if-eqz v3, :cond_25

    iget-object v0, v0, Lflipboard/objs/SearchResultItem;->v:Ljava/lang/String;

    if-eqz v0, :cond_25

    .line 343
    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 344
    invoke-static {v1, v8}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_0

    .line 261
    :cond_1e
    iget-object v1, p0, Lflipboard/gui/SearchListAdapter;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f030053

    invoke-virtual {v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 262
    new-instance v2, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;

    invoke-direct {v2}, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;-><init>()V

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 263
    const v1, 0x7f0a00c0

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;->b:Lflipboard/gui/FLTextIntf;

    .line 264
    const v1, 0x7f0a00c1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;->c:Lflipboard/gui/FLTextIntf;

    .line 265
    const v1, 0x7f0a00c3

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;->d:Lflipboard/gui/FLTextIntf;

    .line 266
    const v1, 0x7f0a00c2

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;->e:Lflipboard/gui/FLImageView;

    .line 267
    const v1, 0x7f0a00be

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;->a:Lflipboard/gui/FLImageView;

    .line 268
    const v1, 0x7f0a0136

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;->i:Lflipboard/gui/FLImageView;

    .line 269
    const v1, 0x7f0a0134

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;->f:Lflipboard/gui/FLImageView;

    .line 270
    const v1, 0x7f0a0135

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;->g:Lflipboard/gui/FLTextIntf;

    .line 271
    const v1, 0x7f0a011d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;->h:Landroid/view/View;

    .line 272
    const v1, 0x7f0a0126

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;->j:Landroid/view/ViewGroup;

    .line 273
    const v1, 0x7f0a0124

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSearchResult;->k:Landroid/view/ViewGroup;

    move-object v1, v2

    goto/16 :goto_10

    .line 288
    :cond_1f
    invoke-static {v3, v9}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLTextIntf;I)V

    goto/16 :goto_11

    .line 297
    :cond_20
    invoke-virtual {v4, v8}, Lflipboard/gui/FLImageView;->setClipRound(Z)V

    goto/16 :goto_12

    .line 310
    :cond_21
    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->t:Ljava/lang/String;

    invoke-virtual {v4, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    goto/16 :goto_13

    .line 317
    :cond_22
    iget-object v3, p0, Lflipboard/gui/SearchListAdapter;->b:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v3}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900b7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 318
    iget-object v3, p0, Lflipboard/gui/SearchListAdapter;->b:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v3}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900b7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_14

    .line 322
    :cond_23
    invoke-static {v4, v9}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_14

    .line 332
    :cond_24
    invoke-interface {v2, v7}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_15

    .line 346
    :cond_25
    invoke-static {v1, v9}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_0

    .line 351
    :cond_26
    if-eqz p2, :cond_27

    .line 353
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/SearchListAdapter$ViewHolderSource;

    .line 362
    :goto_16
    iget-object v2, v1, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->a:Lflipboard/gui/FLImageView;

    iget-object v3, v0, Lflipboard/objs/SearchResultItem;->t:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 363
    iget-object v2, v1, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->b:Lflipboard/gui/FLLabelTextView;

    iget-object v3, v0, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 364
    iget-object v1, v1, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->c:Lflipboard/gui/FLLabelTextView;

    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->s:Ljava/lang/String;

    if-eqz v2, :cond_28

    iget-object v0, v0, Lflipboard/objs/SearchResultItem;->s:Ljava/lang/String;

    goto/16 :goto_6

    .line 355
    :cond_27
    iget-object v1, p0, Lflipboard/gui/SearchListAdapter;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f03012b

    invoke-virtual {v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 356
    new-instance v2, Lflipboard/gui/SearchListAdapter$ViewHolderSource;

    invoke-direct {v2}, Lflipboard/gui/SearchListAdapter$ViewHolderSource;-><init>()V

    .line 357
    const v1, 0x7f0a0336

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->a:Lflipboard/gui/FLImageView;

    .line 358
    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLLabelTextView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->b:Lflipboard/gui/FLLabelTextView;

    .line 359
    const v1, 0x7f0a0337

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLLabelTextView;

    iput-object v1, v2, Lflipboard/gui/SearchListAdapter$ViewHolderSource;->c:Lflipboard/gui/FLLabelTextView;

    .line 360
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    goto :goto_16

    .line 364
    :cond_28
    const-string v0, ""

    goto/16 :goto_6
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 49
    const/16 v0, 0x9

    return v0
.end method
