.class Lflipboard/gui/CommentaryList$12;
.super Ljava/lang/Object;
.source "CommentaryList.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lflipboard/gui/FLPopoverWindow;

.field final synthetic c:Lflipboard/gui/CommentaryList$AuthorDetails;

.field final synthetic d:Lflipboard/gui/CommentaryList;


# direct methods
.method constructor <init>(Lflipboard/gui/CommentaryList;Landroid/view/View;Lflipboard/gui/FLPopoverWindow;Lflipboard/gui/CommentaryList$AuthorDetails;)V
    .locals 0

    .prologue
    .line 1089
    iput-object p1, p0, Lflipboard/gui/CommentaryList$12;->d:Lflipboard/gui/CommentaryList;

    iput-object p2, p0, Lflipboard/gui/CommentaryList$12;->a:Landroid/view/View;

    iput-object p3, p0, Lflipboard/gui/CommentaryList$12;->b:Lflipboard/gui/FLPopoverWindow;

    iput-object p4, p0, Lflipboard/gui/CommentaryList$12;->c:Lflipboard/gui/CommentaryList$AuthorDetails;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1092
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1103
    :cond_0
    :goto_0
    return-void

    .line 1095
    :cond_1
    iget-object v0, p0, Lflipboard/gui/CommentaryList$12;->a:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 1097
    iget-object v0, p0, Lflipboard/gui/CommentaryList$12;->b:Lflipboard/gui/FLPopoverWindow;

    invoke-virtual {v0}, Lflipboard/gui/FLPopoverWindow;->dismiss()V

    .line 1098
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1099
    const-string v1, "source"

    const-string v2, "authorPopover"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1100
    const-string v1, "originSectionIdentifier"

    iget-object v2, p0, Lflipboard/gui/CommentaryList$12;->d:Lflipboard/gui/CommentaryList;

    iget-object v2, v2, Lflipboard/gui/CommentaryList;->d:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1101
    new-instance v1, Lflipboard/service/Section;

    iget-object v2, p0, Lflipboard/gui/CommentaryList$12;->c:Lflipboard/gui/CommentaryList$AuthorDetails;

    iget-object v2, v2, Lflipboard/gui/CommentaryList$AuthorDetails;->f:Lflipboard/objs/FeedSectionLink;

    invoke-direct {v1, v2}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    iget-object v2, p0, Lflipboard/gui/CommentaryList$12;->d:Lflipboard/gui/CommentaryList;

    iget-object v2, v2, Lflipboard/gui/CommentaryList;->e:Lflipboard/activities/FlipboardActivity;

    invoke-static {v1, v2, v0}, Lflipboard/util/SocialHelper;->a(Lflipboard/service/Section;Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0
.end method
