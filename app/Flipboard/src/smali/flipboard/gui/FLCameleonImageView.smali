.class public Lflipboard/gui/FLCameleonImageView;
.super Landroid/widget/ImageView;
.source "FLCameleonImageView.java"

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field private static final h:[I


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:Z

.field public f:Z

.field public g:Z

.field private i:Landroid/content/res/ColorStateList;

.field private j:Z

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lflipboard/gui/FLCameleonImageView;->h:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 29
    const v0, -0x777778

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    .line 30
    const v0, -0xbbbbbc

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->b:I

    .line 31
    iget v0, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->c:I

    .line 32
    iget v0, p0, Lflipboard/gui/FLCameleonImageView;->b:I

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->d:I

    .line 34
    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->e:Z

    .line 35
    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->f:Z

    .line 36
    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->g:Z

    .line 39
    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->j:Z

    .line 41
    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->k:Z

    .line 45
    invoke-virtual {p0}, Lflipboard/gui/FLCameleonImageView;->a()V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const v0, -0x777778

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    .line 30
    const v0, -0xbbbbbc

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->b:I

    .line 31
    iget v0, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->c:I

    .line 32
    iget v0, p0, Lflipboard/gui/FLCameleonImageView;->b:I

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->d:I

    .line 34
    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->e:Z

    .line 35
    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->f:Z

    .line 36
    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->g:Z

    .line 39
    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->j:Z

    .line 41
    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->k:Z

    .line 50
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLCameleonImageView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    invoke-virtual {p0}, Lflipboard/gui/FLCameleonImageView;->a()V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    const v0, -0x777778

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    .line 30
    const v0, -0xbbbbbc

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->b:I

    .line 31
    iget v0, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->c:I

    .line 32
    iget v0, p0, Lflipboard/gui/FLCameleonImageView;->b:I

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->d:I

    .line 34
    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->e:Z

    .line 35
    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->f:Z

    .line 36
    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->g:Z

    .line 39
    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->j:Z

    .line 41
    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->k:Z

    .line 56
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLCameleonImageView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    invoke-virtual {p0}, Lflipboard/gui/FLCameleonImageView;->a()V

    .line 58
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 61
    if-eqz p2, :cond_3

    .line 62
    sget-object v0, Lflipboard/app/R$styleable;->FLCameleonImageView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 64
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->j:Z

    .line 66
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    iget v1, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    .line 68
    iput-boolean v3, p0, Lflipboard/gui/FLCameleonImageView;->e:Z

    .line 70
    :cond_0
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 71
    iget v1, p0, Lflipboard/gui/FLCameleonImageView;->b:I

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lflipboard/gui/FLCameleonImageView;->b:I

    .line 72
    iput-boolean v3, p0, Lflipboard/gui/FLCameleonImageView;->f:Z

    .line 74
    :cond_1
    const/4 v1, 0x3

    iget v2, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lflipboard/gui/FLCameleonImageView;->c:I

    .line 75
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 76
    iget v1, p0, Lflipboard/gui/FLCameleonImageView;->d:I

    invoke-virtual {v0, v4, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lflipboard/gui/FLCameleonImageView;->d:I

    .line 77
    iput-boolean v3, p0, Lflipboard/gui/FLCameleonImageView;->g:Z

    .line 79
    :cond_2
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 81
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 84
    iget-boolean v0, p0, Lflipboard/gui/FLCameleonImageView;->e:Z

    if-nez v0, :cond_0

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLCameleonImageView;->i:Landroid/content/res/ColorStateList;

    iput-boolean v3, p0, Lflipboard/gui/FLCameleonImageView;->e:Z

    iput-boolean v3, p0, Lflipboard/gui/FLCameleonImageView;->f:Z

    iput-boolean v3, p0, Lflipboard/gui/FLCameleonImageView;->g:Z

    invoke-virtual {p0}, Lflipboard/gui/FLCameleonImageView;->clearColorFilter()V

    .line 115
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-boolean v0, p0, Lflipboard/gui/FLCameleonImageView;->j:Z

    if-eqz v0, :cond_1

    .line 88
    iget v0, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    invoke-static {v0}, Lflipboard/util/ColorFilterUtil;->b(I)Landroid/graphics/ColorFilter;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/FLCameleonImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0

    .line 90
    :cond_1
    iget-boolean v0, p0, Lflipboard/gui/FLCameleonImageView;->f:Z

    if-nez v0, :cond_2

    .line 92
    iget v0, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    invoke-static {v0}, Lflipboard/util/ColorFilterUtil;->a(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->b:I

    .line 93
    iput-boolean v4, p0, Lflipboard/gui/FLCameleonImageView;->f:Z

    .line 95
    :cond_2
    iget-boolean v0, p0, Lflipboard/gui/FLCameleonImageView;->g:Z

    if-nez v0, :cond_3

    .line 97
    iget v0, p0, Lflipboard/gui/FLCameleonImageView;->c:I

    invoke-static {v0}, Lflipboard/util/ColorFilterUtil;->a(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->d:I

    .line 98
    iput-boolean v4, p0, Lflipboard/gui/FLCameleonImageView;->g:Z

    .line 100
    :cond_3
    new-array v0, v7, [[I

    new-array v1, v5, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v3

    new-array v1, v4, [I

    const v2, 0x10100a0

    aput v2, v1, v3

    aput-object v1, v0, v4

    new-array v1, v4, [I

    const v2, 0x10100a7

    aput v2, v1, v3

    aput-object v1, v0, v5

    new-array v1, v3, [I

    aput-object v1, v0, v6

    .line 106
    new-array v1, v7, [I

    iget v2, p0, Lflipboard/gui/FLCameleonImageView;->d:I

    aput v2, v1, v3

    iget v2, p0, Lflipboard/gui/FLCameleonImageView;->c:I

    aput v2, v1, v4

    iget v2, p0, Lflipboard/gui/FLCameleonImageView;->b:I

    aput v2, v1, v5

    iget v2, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    aput v2, v1, v6

    .line 112
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iput-object v2, p0, Lflipboard/gui/FLCameleonImageView;->i:Landroid/content/res/ColorStateList;

    .line 113
    invoke-virtual {p0}, Lflipboard/gui/FLCameleonImageView;->refreshDrawableState()V

    goto :goto_0

    .line 100
    :array_0
    .array-data 4
        0x10100a0
        0x10100a7
    .end array-data
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 127
    invoke-virtual {p0}, Lflipboard/gui/FLCameleonImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    .line 128
    iput-boolean v2, p0, Lflipboard/gui/FLCameleonImageView;->e:Z

    .line 129
    iget v0, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->c:I

    .line 130
    invoke-virtual {p0}, Lflipboard/gui/FLCameleonImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08005a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->b:I

    .line 131
    iput-boolean v2, p0, Lflipboard/gui/FLCameleonImageView;->f:Z

    .line 132
    iget v0, p0, Lflipboard/gui/FLCameleonImageView;->b:I

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->d:I

    .line 133
    iput-boolean v2, p0, Lflipboard/gui/FLCameleonImageView;->g:Z

    .line 134
    invoke-virtual {p0}, Lflipboard/gui/FLCameleonImageView;->a()V

    .line 135
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 3

    .prologue
    .line 167
    invoke-super {p0}, Landroid/widget/ImageView;->drawableStateChanged()V

    .line 168
    iget-boolean v0, p0, Lflipboard/gui/FLCameleonImageView;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLCameleonImageView;->i:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lflipboard/gui/FLCameleonImageView;->i:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lflipboard/gui/FLCameleonImageView;->getDrawableState()[I

    move-result-object v1

    iget v2, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 170
    invoke-static {v0}, Lflipboard/util/ColorFilterUtil;->b(I)Landroid/graphics/ColorFilter;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/FLCameleonImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 172
    :cond_0
    return-void
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 191
    iget-boolean v0, p0, Lflipboard/gui/FLCameleonImageView;->k:Z

    return v0
.end method

.method public onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 176
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/widget/ImageView;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 177
    invoke-virtual {p0}, Lflipboard/gui/FLCameleonImageView;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    sget-object v1, Lflipboard/gui/FLCameleonImageView;->h:[I

    invoke-static {v0, v1}, Lflipboard/gui/FLCameleonImageView;->mergeDrawableStates([I[I)[I

    .line 180
    :cond_0
    return-object v0
.end method

.method public setChecked(Z)V
    .locals 0

    .prologue
    .line 185
    iput-boolean p1, p0, Lflipboard/gui/FLCameleonImageView;->k:Z

    .line 186
    invoke-virtual {p0}, Lflipboard/gui/FLCameleonImageView;->refreshDrawableState()V

    .line 187
    return-void
.end method

.method public final setDefaultAndCheckedColors$255f295(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 138
    invoke-virtual {p0}, Lflipboard/gui/FLCameleonImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/FLCameleonImageView;->e:Z

    .line 140
    invoke-virtual {p0}, Lflipboard/gui/FLCameleonImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->c:I

    .line 141
    iput-boolean v2, p0, Lflipboard/gui/FLCameleonImageView;->f:Z

    .line 142
    iput-boolean v2, p0, Lflipboard/gui/FLCameleonImageView;->g:Z

    .line 143
    invoke-virtual {p0}, Lflipboard/gui/FLCameleonImageView;->a()V

    .line 144
    return-void
.end method

.method public setDefaultColor(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 118
    invoke-virtual {p0}, Lflipboard/gui/FLCameleonImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/FLCameleonImageView;->e:Z

    .line 120
    iget v0, p0, Lflipboard/gui/FLCameleonImageView;->a:I

    iput v0, p0, Lflipboard/gui/FLCameleonImageView;->c:I

    .line 121
    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->f:Z

    .line 122
    iput-boolean v1, p0, Lflipboard/gui/FLCameleonImageView;->g:Z

    .line 123
    invoke-virtual {p0}, Lflipboard/gui/FLCameleonImageView;->a()V

    .line 124
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lflipboard/gui/FLCameleonImageView;->k:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lflipboard/gui/FLCameleonImageView;->setChecked(Z)V

    .line 197
    return-void

    .line 196
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
