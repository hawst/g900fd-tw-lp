.class public Lflipboard/gui/IconBar;
.super Landroid/widget/LinearLayout;
.source "IconBar.java"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field private e:Lflipboard/gui/IconBar$OnChangeObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 23
    iput v1, p0, Lflipboard/gui/IconBar;->d:I

    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lflipboard/gui/IconBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lflipboard/gui/IconBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/IconBar;->d:I

    .line 41
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/IconBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method static synthetic a(Lflipboard/gui/IconBar;)Lflipboard/gui/IconBar$OnChangeObserver;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lflipboard/gui/IconBar;->e:Lflipboard/gui/IconBar$OnChangeObserver;

    return-object v0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 46
    sget-object v1, Lflipboard/app/R$styleable;->IconBar:[I

    invoke-virtual {p1, p2, v1, p3, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 47
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    .line 48
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v3

    .line 49
    packed-switch v3, :pswitch_data_0

    .line 47
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51
    :pswitch_0
    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lflipboard/gui/IconBar;->a:I

    goto :goto_1

    .line 54
    :pswitch_1
    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lflipboard/gui/IconBar;->b:I

    goto :goto_1

    .line 57
    :pswitch_2
    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lflipboard/gui/IconBar;->c:I

    goto :goto_1

    .line 61
    :cond_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 62
    return-void

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic b(Lflipboard/gui/IconBar;)I
    .locals 2

    .prologue
    .line 16
    iget v0, p0, Lflipboard/gui/IconBar;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lflipboard/gui/IconBar;->d:I

    return v0
.end method

.method static synthetic c(Lflipboard/gui/IconBar;)I
    .locals 2

    .prologue
    .line 16
    iget v0, p0, Lflipboard/gui/IconBar;->d:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lflipboard/gui/IconBar;->d:I

    return v0
.end method


# virtual methods
.method public getNumSelectedIcons()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lflipboard/gui/IconBar;->d:I

    return v0
.end method

.method public setObserver(Lflipboard/gui/IconBar$OnChangeObserver;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lflipboard/gui/IconBar;->e:Lflipboard/gui/IconBar$OnChangeObserver;

    .line 97
    return-void
.end method
