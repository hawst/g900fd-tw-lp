.class final Lflipboard/regex/Regex$Edge;
.super Ljava/lang/Object;
.source "Regex.java"


# instance fields
.field a:Lflipboard/regex/Regex$State;

.field b:Z

.field c:Ljava/util/BitSet;


# direct methods
.method constructor <init>(Lflipboard/regex/Regex$State;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lflipboard/regex/Regex$Edge;->a:Lflipboard/regex/Regex$State;

    .line 48
    return-void
.end method

.method constructor <init>(Lflipboard/regex/Regex$State;ZLjava/util/BitSet;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lflipboard/regex/Regex$Edge;->a:Lflipboard/regex/Regex$State;

    .line 53
    iput-boolean p2, p0, Lflipboard/regex/Regex$Edge;->b:Z

    .line 54
    iput-object p3, p0, Lflipboard/regex/Regex$Edge;->c:Ljava/util/BitSet;

    .line 55
    return-void
.end method

.method private static a(Ljava/util/BitSet;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 104
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 105
    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/util/BitSet;->length()I

    move-result v3

    :goto_0
    if-ge v1, v3, :cond_4

    .line 106
    invoke-virtual {p0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 108
    :goto_1
    add-int/lit8 v4, v0, 0x1

    if-ge v4, v3, :cond_0

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, v4}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 109
    :cond_0
    if-ge v1, v0, :cond_2

    .line 110
    add-int/lit8 v4, v1, 0x1

    if-ne v4, v0, :cond_1

    .line 111
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Lflipboard/regex/Regex$Edge;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Lflipboard/regex/Regex$Edge;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    :goto_2
    add-int/lit8 v1, v0, 0x1

    goto :goto_0

    .line 113
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Lflipboard/regex/Regex$Edge;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "-"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Lflipboard/regex/Regex$Edge;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 117
    :cond_2
    invoke-static {v1}, Lflipboard/regex/Regex$Edge;->b(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    move v0, v1

    goto :goto_2

    .line 121
    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    sparse-switch p0, :sswitch_data_0

    .line 92
    const/16 v0, 0x20

    if-ge p0, v0, :cond_0

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\\"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 98
    :goto_0
    return-object v0

    .line 84
    :sswitch_0
    const-string v0, "\\n"

    goto :goto_0

    .line 85
    :sswitch_1
    const-string v0, "\\r"

    goto :goto_0

    .line 86
    :sswitch_2
    const-string v0, "\\f"

    goto :goto_0

    .line 87
    :sswitch_3
    const-string v0, "\\t"

    goto :goto_0

    .line 88
    :sswitch_4
    const-string v0, "\\["

    goto :goto_0

    .line 89
    :sswitch_5
    const-string v0, "\\]"

    goto :goto_0

    .line 90
    :sswitch_6
    const-string v0, "\\-"

    goto :goto_0

    .line 95
    :cond_0
    const/16 v0, 0xff

    if-le p0, v0, :cond_1

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\\u"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 98
    :cond_1
    int-to-char v0, p0

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 83
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_3
        0xa -> :sswitch_0
        0xc -> :sswitch_2
        0xd -> :sswitch_1
        0x2d -> :sswitch_6
        0x5b -> :sswitch_4
        0x5d -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method final a(I)Z
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lflipboard/regex/Regex$Edge;->c:Ljava/util/BitSet;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/regex/Regex$Edge;->b:Z

    iget-object v1, p0, Lflipboard/regex/Regex$Edge;->c:Ljava/util/BitSet;

    invoke-virtual {v1, p1}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lflipboard/regex/Regex$Edge;->c:Ljava/util/BitSet;

    if-nez v0, :cond_0

    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "*:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/regex/Regex$Edge;->a:Lflipboard/regex/Regex$State;

    iget v1, v1, Lflipboard/regex/Regex$State;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 129
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v0, p0, Lflipboard/regex/Regex$Edge;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "[^"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflipboard/regex/Regex$Edge;->c:Ljava/util/BitSet;

    invoke-static {v1}, Lflipboard/regex/Regex$Edge;->a(Ljava/util/BitSet;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflipboard/regex/Regex$Edge;->a:Lflipboard/regex/Regex$State;

    iget v1, v1, Lflipboard/regex/Regex$State;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, "["

    goto :goto_1
.end method
