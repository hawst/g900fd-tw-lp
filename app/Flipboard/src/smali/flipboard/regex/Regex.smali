.class public final Lflipboard/regex/Regex;
.super Ljava/lang/Object;
.source "Regex.java"


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/regex/Regex$State;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/regex/Regex$Action;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/regex/Regex;->a:Ljava/util/ArrayList;

    .line 310
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/regex/Regex;->b:Ljava/util/ArrayList;

    .line 315
    invoke-virtual {p0}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    .line 316
    return-void
.end method

.method private a(Ljava/io/Reader;Lflipboard/regex/Regex$State;Lflipboard/regex/Regex$State;)V
    .locals 12

    .prologue
    const/16 v1, 0xa

    const/4 v10, 0x2

    const/4 v2, -0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 332
    const/4 v4, 0x0

    .line 333
    invoke-virtual {p0}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v3

    .line 334
    new-instance v0, Lflipboard/regex/Regex$Edge;

    invoke-direct {v0, v3}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;)V

    invoke-virtual {p2, v0}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    .line 335
    invoke-virtual {p1}, Ljava/io/Reader;->read()I

    move-result v0

    .line 336
    const/16 v5, 0x5e

    if-ne v0, v5, :cond_b

    .line 337
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    .line 338
    invoke-virtual {v0, v6}, Ljava/util/BitSet;->set(I)V

    .line 339
    invoke-virtual {p0}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v4

    .line 340
    new-instance v5, Lflipboard/regex/Regex$Edge;

    invoke-direct {v5, v4}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;)V

    invoke-virtual {v3, v5}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    .line 341
    invoke-virtual {p0}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v3

    .line 342
    new-instance v5, Lflipboard/regex/Regex$Edge;

    invoke-direct {v5, v3, v7, v0}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;ZLjava/util/BitSet;)V

    invoke-virtual {v4, v5}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    .line 343
    invoke-virtual {p1}, Ljava/io/Reader;->read()I

    move-result v0

    move v11, v0

    move-object v0, v4

    move v4, v11

    .line 347
    :goto_0
    if-ltz v4, :cond_a

    .line 348
    sparse-switch v4, :sswitch_data_0

    .line 461
    new-instance v5, Ljava/util/BitSet;

    invoke-direct {v5}, Ljava/util/BitSet;-><init>()V

    .line 462
    invoke-virtual {v5, v4}, Ljava/util/BitSet;->set(I)V

    .line 463
    invoke-virtual {p0}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v4

    .line 464
    new-instance v0, Lflipboard/regex/Regex$Edge;

    invoke-direct {v0, v4}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;)V

    invoke-virtual {v3, v0}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    .line 465
    invoke-virtual {p0}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v0

    .line 466
    new-instance v3, Lflipboard/regex/Regex$Edge;

    invoke-direct {v3, v0, v7, v5}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;ZLjava/util/BitSet;)V

    invoke-virtual {v4, v3}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    :cond_0
    move-object v3, v4

    .line 467
    :goto_1
    invoke-virtual {p1}, Ljava/io/Reader;->read()I

    move-result v4

    move-object v11, v0

    move-object v0, v3

    move-object v3, v11

    goto :goto_0

    .line 350
    :sswitch_0
    if-nez v0, :cond_1

    .line 351
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid regex at ?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 353
    :cond_1
    new-instance v4, Lflipboard/regex/Regex$Edge;

    invoke-direct {v4, v3}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;)V

    invoke-virtual {v0, v4}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    move-object v11, v3

    move-object v3, v0

    move-object v0, v11

    .line 354
    goto :goto_1

    .line 356
    :sswitch_1
    if-nez v0, :cond_2

    .line 357
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid regex at +"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359
    :cond_2
    new-instance v4, Lflipboard/regex/Regex$Edge;

    invoke-direct {v4, v0}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;)V

    invoke-virtual {v3, v4}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    move-object v11, v3

    move-object v3, v0

    move-object v0, v11

    .line 360
    goto :goto_1

    .line 362
    :sswitch_2
    if-nez v0, :cond_3

    .line 363
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid regex at *"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 365
    :cond_3
    new-instance v4, Lflipboard/regex/Regex$Edge;

    invoke-direct {v4, v0}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;)V

    invoke-virtual {v3, v4}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    .line 366
    new-instance v4, Lflipboard/regex/Regex$Edge;

    invoke-direct {v4, v3}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;)V

    invoke-virtual {v0, v4}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    move-object v11, v3

    move-object v3, v0

    move-object v0, v11

    .line 367
    goto :goto_1

    .line 369
    :sswitch_3
    invoke-virtual {p0}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v4

    .line 370
    new-instance v0, Lflipboard/regex/Regex$Edge;

    invoke-direct {v0, v4}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;)V

    invoke-virtual {v3, v0}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    .line 371
    invoke-virtual {p0}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v0

    .line 372
    invoke-direct {p0, p1, v4, v0}, Lflipboard/regex/Regex;->a(Ljava/io/Reader;Lflipboard/regex/Regex$State;Lflipboard/regex/Regex$State;)V

    move-object v3, v4

    .line 373
    goto :goto_1

    .line 375
    :sswitch_4
    new-instance v0, Lflipboard/regex/Regex$Edge;

    invoke-direct {v0, p3}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;)V

    invoke-virtual {v3, v0}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    .line 472
    :goto_2
    return-void

    .line 379
    :sswitch_5
    new-instance v8, Ljava/util/BitSet;

    invoke-direct {v8}, Ljava/util/BitSet;-><init>()V

    .line 380
    invoke-virtual {p1}, Ljava/io/Reader;->read()I

    move-result v0

    .line 381
    const/16 v4, 0x5e

    if-ne v0, v4, :cond_5

    move v5, v6

    .line 382
    :goto_3
    if-eqz v5, :cond_4

    .line 383
    invoke-virtual {p1}, Ljava/io/Reader;->read()I

    move-result v0

    .line 384
    invoke-virtual {v8, v6}, Ljava/util/BitSet;->set(I)V

    .line 385
    invoke-virtual {v8, v10}, Ljava/util/BitSet;->set(I)V

    :cond_4
    move v4, v2

    .line 387
    :goto_4
    if-ltz v0, :cond_9

    const/16 v9, 0x5d

    if-eq v0, v9, :cond_9

    .line 388
    const/16 v9, 0x2d

    if-ne v0, v9, :cond_7

    if-eq v4, v2, :cond_7

    .line 389
    invoke-virtual {p1}, Ljava/io/Reader;->read()I

    move-result v9

    move v0, v4

    .line 390
    :goto_5
    if-gt v0, v9, :cond_6

    .line 391
    invoke-virtual {v8, v0}, Ljava/util/BitSet;->set(I)V

    .line 390
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_5
    move v5, v7

    .line 381
    goto :goto_3

    :cond_6
    move v0, v2

    .line 387
    :goto_6
    invoke-virtual {p1}, Ljava/io/Reader;->read()I

    move-result v4

    move v11, v0

    move v0, v4

    move v4, v11

    goto :goto_4

    .line 395
    :cond_7
    const/16 v4, 0x5c

    if-ne v0, v4, :cond_8

    .line 396
    invoke-virtual {p1}, Ljava/io/Reader;->read()I

    move-result v0

    .line 397
    sparse-switch v0, :sswitch_data_1

    .line 405
    :cond_8
    :goto_7
    invoke-virtual {v8, v0}, Ljava/util/BitSet;->set(I)V

    goto :goto_6

    :sswitch_6
    move v0, v1

    .line 398
    goto :goto_7

    .line 399
    :sswitch_7
    const/16 v0, 0xd

    goto :goto_7

    .line 400
    :sswitch_8
    const/16 v0, 0xc

    goto :goto_7

    .line 401
    :sswitch_9
    const/16 v0, 0x9

    goto :goto_7

    .line 408
    :cond_9
    invoke-virtual {p0}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v4

    .line 409
    new-instance v0, Lflipboard/regex/Regex$Edge;

    invoke-direct {v0, v4}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;)V

    invoke-virtual {v3, v0}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    .line 410
    invoke-virtual {p0}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v0

    .line 411
    new-instance v3, Lflipboard/regex/Regex$Edge;

    invoke-direct {v3, v0, v5, v8}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;ZLjava/util/BitSet;)V

    invoke-virtual {v4, v3}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    move-object v3, v4

    .line 412
    goto/16 :goto_1

    .line 415
    :sswitch_a
    new-instance v0, Lflipboard/regex/Regex$Edge;

    invoke-direct {v0, p3}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;)V

    invoke-virtual {v3, v0}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    .line 416
    const/4 v3, 0x0

    .line 417
    invoke-virtual {p0}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v0

    .line 418
    new-instance v4, Lflipboard/regex/Regex$Edge;

    invoke-direct {v4, v0}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;)V

    invoke-virtual {p2, v4}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    goto/16 :goto_1

    .line 422
    :sswitch_b
    new-instance v5, Ljava/util/BitSet;

    invoke-direct {v5}, Ljava/util/BitSet;-><init>()V

    .line 423
    invoke-virtual {p1}, Ljava/io/Reader;->read()I

    move-result v0

    .line 424
    sparse-switch v0, :sswitch_data_2

    .line 430
    :goto_8
    invoke-virtual {v5, v0}, Ljava/util/BitSet;->set(I)V

    .line 431
    invoke-virtual {p0}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v4

    .line 432
    new-instance v0, Lflipboard/regex/Regex$Edge;

    invoke-direct {v0, v4}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;)V

    invoke-virtual {v3, v0}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    .line 433
    invoke-virtual {p0}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v0

    .line 434
    new-instance v3, Lflipboard/regex/Regex$Edge;

    invoke-direct {v3, v0, v7, v5}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;ZLjava/util/BitSet;)V

    invoke-virtual {v4, v3}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    move-object v3, v4

    .line 435
    goto/16 :goto_1

    :sswitch_c
    move v0, v1

    .line 425
    goto :goto_8

    .line 426
    :sswitch_d
    const/16 v0, 0xd

    goto :goto_8

    .line 427
    :sswitch_e
    const/16 v0, 0xc

    goto :goto_8

    .line 428
    :sswitch_f
    const/16 v0, 0x9

    goto :goto_8

    .line 438
    :sswitch_10
    invoke-virtual {p0}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v4

    .line 439
    new-instance v0, Lflipboard/regex/Regex$Edge;

    invoke-direct {v0, v4}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;)V

    invoke-virtual {v3, v0}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    .line 440
    invoke-virtual {p0}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v0

    .line 441
    new-instance v3, Ljava/util/BitSet;

    invoke-direct {v3}, Ljava/util/BitSet;-><init>()V

    .line 442
    invoke-virtual {v3, v1}, Ljava/util/BitSet;->set(I)V

    .line 443
    invoke-virtual {v3, v6}, Ljava/util/BitSet;->set(I)V

    .line 444
    invoke-virtual {v3, v10}, Ljava/util/BitSet;->set(I)V

    .line 445
    new-instance v5, Lflipboard/regex/Regex$Edge;

    invoke-direct {v5, v0, v6, v3}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;ZLjava/util/BitSet;)V

    invoke-virtual {v4, v5}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    move-object v3, v4

    .line 446
    goto/16 :goto_1

    .line 449
    :sswitch_11
    new-instance v5, Ljava/util/BitSet;

    invoke-direct {v5}, Ljava/util/BitSet;-><init>()V

    .line 450
    invoke-virtual {v5, v10}, Ljava/util/BitSet;->set(I)V

    .line 451
    invoke-virtual {p0}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v4

    .line 452
    new-instance v0, Lflipboard/regex/Regex$Edge;

    invoke-direct {v0, v4}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;)V

    invoke-virtual {v3, v0}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    .line 453
    invoke-virtual {p0}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v0

    .line 454
    new-instance v3, Lflipboard/regex/Regex$Edge;

    invoke-direct {v3, v0, v7, v5}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;ZLjava/util/BitSet;)V

    invoke-virtual {v4, v3}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    .line 455
    invoke-virtual {p1}, Ljava/io/Reader;->read()I

    move-result v3

    if-ltz v3, :cond_0

    .line 456
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "stuff after $"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 471
    :cond_a
    new-instance v0, Lflipboard/regex/Regex$Edge;

    invoke-direct {v0, p3}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;)V

    invoke-virtual {v3, v0}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$Edge;)V

    goto/16 :goto_2

    :cond_b
    move v11, v0

    move-object v0, v4

    move v4, v11

    goto/16 :goto_0

    .line 348
    :sswitch_data_0
    .sparse-switch
        0x24 -> :sswitch_11
        0x28 -> :sswitch_3
        0x29 -> :sswitch_4
        0x2a -> :sswitch_2
        0x2b -> :sswitch_1
        0x2e -> :sswitch_10
        0x3f -> :sswitch_0
        0x5b -> :sswitch_5
        0x5c -> :sswitch_b
        0x7c -> :sswitch_a
    .end sparse-switch

    .line 397
    :sswitch_data_1
    .sparse-switch
        0x66 -> :sswitch_8
        0x6e -> :sswitch_6
        0x72 -> :sswitch_7
        0x74 -> :sswitch_9
    .end sparse-switch

    .line 424
    :sswitch_data_2
    .sparse-switch
        0x66 -> :sswitch_e
        0x6e -> :sswitch_c
        0x72 -> :sswitch_d
        0x74 -> :sswitch_f
    .end sparse-switch
.end method


# virtual methods
.method public final a()Lflipboard/regex/Regex$State;
    .locals 2

    .prologue
    .line 325
    new-instance v0, Lflipboard/regex/Regex$State;

    iget-object v1, p0, Lflipboard/regex/Regex;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Lflipboard/regex/Regex$State;-><init>(I)V

    .line 326
    iget-object v1, p0, Lflipboard/regex/Regex;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 327
    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 648
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "---- "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ----"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move v1, v2

    .line 649
    :goto_0
    iget-object v0, p0, Lflipboard/regex/Regex;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 650
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "<"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "> = /"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p0, Lflipboard/regex/Regex;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$Action;

    iget-object v0, v0, Lflipboard/regex/Regex$Action;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/ --> "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p0, Lflipboard/regex/Regex;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$Action;

    iget v0, v0, Lflipboard/regex/Regex$Action;->b:I

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 649
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 652
    :cond_0
    :goto_1
    iget-object v0, p0, Lflipboard/regex/Regex;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 653
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v1, p0, Lflipboard/regex/Regex;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 652
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 655
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;I)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 491
    iget-boolean v0, p0, Lflipboard/regex/Regex;->c:Z

    if-eqz v0, :cond_0

    .line 492
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Regex already compiled"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 494
    :cond_0
    new-instance v0, Lflipboard/regex/Regex$Action;

    invoke-direct {v0, p1, p2}, Lflipboard/regex/Regex$Action;-><init>(Ljava/lang/String;I)V

    .line 495
    iget-object v3, p0, Lflipboard/regex/Regex;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 497
    new-instance v3, Lflipboard/regex/Regex$State;

    iget-object v0, p0, Lflipboard/regex/Regex;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v4, p0, Lflipboard/regex/Regex;->b:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {v3, v0, v4}, Lflipboard/regex/Regex$State;-><init>(II)V

    .line 498
    iget-object v0, p0, Lflipboard/regex/Regex;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 500
    :try_start_0
    new-instance v4, Ljava/io/StringReader;

    invoke-direct {v4, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lflipboard/regex/Regex;->a:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$State;

    invoke-direct {p0, v4, v0, v3}, Lflipboard/regex/Regex;->a(Ljava/io/Reader;Lflipboard/regex/Regex$State;Lflipboard/regex/Regex$State;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 504
    :goto_0
    return v0

    .line 502
    :catch_0
    move-exception v0

    .line 503
    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v4, "invalid regular expression: \'%s\', %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-virtual {v3, v4, v5}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 504
    goto :goto_0
.end method
