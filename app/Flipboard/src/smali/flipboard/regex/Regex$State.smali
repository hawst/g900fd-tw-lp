.class public final Lflipboard/regex/Regex$State;
.super Ljava/lang/Object;
.source "Regex.java"


# instance fields
.field a:I

.field public b:I

.field public c:Ljava/util/BitSet;

.field d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/regex/Regex$Edge;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(I)V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    iput p1, p0, Lflipboard/regex/Regex$State;->a:I

    .line 146
    return-void
.end method

.method constructor <init>(II)V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    iput p1, p0, Lflipboard/regex/Regex$State;->a:I

    .line 151
    iput p2, p0, Lflipboard/regex/Regex$State;->b:I

    .line 152
    return-void
.end method


# virtual methods
.method public final a(I)Lflipboard/regex/Regex$State;
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_0

    .line 202
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$Edge;

    invoke-virtual {v0, p1}, Lflipboard/regex/Regex$Edge;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$Edge;

    iget-object v0, v0, Lflipboard/regex/Regex$Edge;->a:Lflipboard/regex/Regex$State;

    .line 207
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final a(ILjava/util/BitSet;)V
    .locals 3

    .prologue
    .line 212
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 213
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_1

    .line 214
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$Edge;

    invoke-virtual {v0, p1}, Lflipboard/regex/Regex$Edge;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lflipboard/regex/Regex$Edge;->a:Lflipboard/regex/Regex$State;

    iget-object v0, v0, Lflipboard/regex/Regex$State;->c:Ljava/util/BitSet;

    invoke-virtual {p2, v0}, Ljava/util/BitSet;->or(Ljava/util/BitSet;)V

    :cond_0
    move v0, v1

    goto :goto_0

    .line 217
    :cond_1
    return-void
.end method

.method final a(Lflipboard/regex/Regex$Edge;)V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    .line 159
    :cond_0
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    return-void
.end method

.method public final a(Lflipboard/regex/Regex$State;I)V
    .locals 4

    .prologue
    .line 163
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 164
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    .line 166
    :cond_0
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_2

    .line 167
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$Edge;

    .line 168
    iget-object v2, v0, Lflipboard/regex/Regex$Edge;->a:Lflipboard/regex/Regex$State;

    if-ne v2, p1, :cond_1

    .line 169
    iget-object v0, v0, Lflipboard/regex/Regex$Edge;->c:Ljava/util/BitSet;

    invoke-virtual {v0, p2}, Ljava/util/BitSet;->set(I)V

    .line 176
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 172
    goto :goto_0

    .line 173
    :cond_2
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    .line 174
    invoke-virtual {v0, p2}, Ljava/util/BitSet;->set(I)V

    .line 175
    iget-object v1, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    new-instance v2, Lflipboard/regex/Regex$Edge;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v3, v0}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;ZLjava/util/BitSet;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final a(Lflipboard/regex/Regex$State;Ljava/util/BitSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 179
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 180
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    .line 182
    :cond_0
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_3

    .line 183
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$Edge;

    .line 184
    iget-object v2, v0, Lflipboard/regex/Regex$Edge;->a:Lflipboard/regex/Regex$State;

    if-ne v2, p1, :cond_2

    .line 185
    iget-boolean v1, v0, Lflipboard/regex/Regex$Edge;->b:Z

    if-eqz v1, :cond_1

    .line 186
    iget-object v0, v0, Lflipboard/regex/Regex$Edge;->c:Ljava/util/BitSet;

    invoke-virtual {v0, p2}, Ljava/util/BitSet;->and(Ljava/util/BitSet;)V

    .line 196
    :goto_1
    return-void

    .line 188
    :cond_1
    iput-boolean v3, v0, Lflipboard/regex/Regex$Edge;->b:Z

    .line 189
    iget-object v1, v0, Lflipboard/regex/Regex$Edge;->c:Ljava/util/BitSet;

    invoke-virtual {p2, v1}, Ljava/util/BitSet;->andNot(Ljava/util/BitSet;)V

    .line 190
    iput-object p2, v0, Lflipboard/regex/Regex$Edge;->c:Ljava/util/BitSet;

    goto :goto_1

    :cond_2
    move v0, v1

    .line 194
    goto :goto_0

    .line 195
    :cond_3
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    new-instance v1, Lflipboard/regex/Regex$Edge;

    invoke-direct {v1, p1, v3, p2}, Lflipboard/regex/Regex$Edge;-><init>(Lflipboard/regex/Regex$State;ZLjava/util/BitSet;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final a(Ljava/util/BitSet;)V
    .locals 3

    .prologue
    .line 221
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 222
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_1

    .line 223
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$Edge;

    iget-boolean v2, v0, Lflipboard/regex/Regex$Edge;->b:Z

    if-eqz v2, :cond_0

    iget-object v0, v0, Lflipboard/regex/Regex$Edge;->a:Lflipboard/regex/Regex$State;

    iget-object v0, v0, Lflipboard/regex/Regex$State;->c:Ljava/util/BitSet;

    invoke-virtual {p1, v0}, Ljava/util/BitSet;->or(Ljava/util/BitSet;)V

    :cond_0
    move v0, v1

    goto :goto_0

    .line 226
    :cond_1
    return-void
.end method

.method public final a(Ljava/util/BitSet;Ljava/util/BitSet;)V
    .locals 3

    .prologue
    .line 260
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 261
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_2

    .line 262
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$Edge;

    .line 263
    iget-object v2, v0, Lflipboard/regex/Regex$Edge;->c:Ljava/util/BitSet;

    if-eqz v2, :cond_1

    .line 264
    iget-boolean v2, v0, Lflipboard/regex/Regex$Edge;->b:Z

    if-eqz v2, :cond_0

    .line 265
    iget-object v0, v0, Lflipboard/regex/Regex$Edge;->c:Ljava/util/BitSet;

    invoke-virtual {p2, v0}, Ljava/util/BitSet;->or(Ljava/util/BitSet;)V

    move v0, v1

    goto :goto_0

    .line 267
    :cond_0
    iget-object v0, v0, Lflipboard/regex/Regex$Edge;->c:Ljava/util/BitSet;

    invoke-virtual {p1, v0}, Ljava/util/BitSet;->or(Ljava/util/BitSet;)V

    :cond_1
    move v0, v1

    .line 270
    goto :goto_0

    .line 272
    :cond_2
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 244
    iget v0, p0, Lflipboard/regex/Regex$State;->b:I

    if-eqz v0, :cond_0

    move v0, v1

    .line 255
    :goto_0
    return v0

    .line 247
    :cond_0
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 248
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_1
    add-int/lit8 v2, v0, -0x1

    if-lez v0, :cond_2

    .line 249
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$Edge;

    .line 250
    iget-object v0, v0, Lflipboard/regex/Regex$Edge;->c:Ljava/util/BitSet;

    if-eqz v0, :cond_1

    move v0, v1

    .line 251
    goto :goto_0

    :cond_1
    move v0, v2

    .line 253
    goto :goto_1

    .line 255
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/util/BitSet;)V
    .locals 3

    .prologue
    .line 230
    iget-object v0, p0, Lflipboard/regex/Regex$State;->c:Ljava/util/BitSet;

    if-eqz v0, :cond_1

    .line 231
    iget-object v0, p0, Lflipboard/regex/Regex$State;->c:Ljava/util/BitSet;

    invoke-virtual {p1, v0}, Ljava/util/BitSet;->or(Ljava/util/BitSet;)V

    .line 240
    :cond_0
    return-void

    .line 232
    :cond_1
    iget v0, p0, Lflipboard/regex/Regex$State;->a:I

    invoke-virtual {p1, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    iget v0, p0, Lflipboard/regex/Regex$State;->a:I

    invoke-virtual {p1, v0}, Ljava/util/BitSet;->set(I)V

    .line 234
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_0

    .line 236
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$Edge;

    iget-object v2, v0, Lflipboard/regex/Regex$Edge;->c:Ljava/util/BitSet;

    if-nez v2, :cond_2

    iget-object v0, v0, Lflipboard/regex/Regex$Edge;->a:Lflipboard/regex/Regex$State;

    invoke-virtual {v0, p1}, Lflipboard/regex/Regex$State;->b(Ljava/util/BitSet;)V

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 276
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 277
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lflipboard/regex/Regex$State;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    iget v0, p0, Lflipboard/regex/Regex$State;->b:I

    if-eqz v0, :cond_1

    .line 279
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " <"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lflipboard/regex/Regex$State;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    :goto_0
    iget-object v0, p0, Lflipboard/regex/Regex$State;->c:Ljava/util/BitSet;

    if-eqz v0, :cond_3

    .line 284
    const-string v0, " {"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    iget-object v0, p0, Lflipboard/regex/Regex$State;->c:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->length()I

    move-result v5

    move v1, v2

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_2

    .line 286
    iget-object v0, p0, Lflipboard/regex/Regex$State;->c:Ljava/util/BitSet;

    invoke-virtual {v0, v3}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 287
    add-int/lit8 v0, v1, 0x1

    if-lez v1, :cond_0

    .line 288
    const-string v1, ","

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    :cond_0
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 285
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    .line 281
    :cond_1
    const-string v0, "    "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 293
    :cond_2
    const-string v0, "}"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    :cond_3
    iget-object v0, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    move v0, v2

    .line 296
    :goto_3
    iget-object v1, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 297
    const/16 v1, 0x20

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 298
    iget-object v1, p0, Lflipboard/regex/Regex$State;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 296
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 301
    :cond_4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_5
    move v0, v1

    goto :goto_2
.end method
