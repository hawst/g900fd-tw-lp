.class public Lflipboard/regex/Tokenizer;
.super Ljava/lang/Object;
.source "Tokenizer.java"


# instance fields
.field public a:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lflipboard/regex/Regex;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/io/BufferedReader;

.field public c:I

.field public d:Ljava/lang/StringBuilder;

.field e:Z

.field f:I

.field g:I

.field h:I

.field i:I


# direct methods
.method private constructor <init>(Ljava/io/BufferedReader;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lflipboard/regex/Tokenizer;->d:Ljava/lang/StringBuilder;

    .line 36
    iput-object p1, p0, Lflipboard/regex/Tokenizer;->b:Ljava/io/BufferedReader;

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/regex/Tokenizer;->e:Z

    .line 38
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lflipboard/regex/Tokenizer;->a:Ljava/util/Stack;

    .line 39
    return-void
.end method

.method private constructor <init>(Ljava/io/Reader;)V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, p1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-direct {p0, v0}, Lflipboard/regex/Tokenizer;-><init>(Ljava/io/BufferedReader;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lflipboard/regex/Tokenizer;-><init>(Ljava/io/Reader;)V

    .line 29
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 15

    .prologue
    const/4 v13, -0x1

    const/4 v3, 0x1

    const/16 v5, -0xd05

    const/4 v12, 0x0

    const/16 v4, 0xa

    .line 82
    iget-object v0, p0, Lflipboard/regex/Tokenizer;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex;

    .line 84
    iget-object v1, p0, Lflipboard/regex/Tokenizer;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 88
    iput v12, p0, Lflipboard/regex/Tokenizer;->c:I

    .line 90
    iget-boolean v1, p0, Lflipboard/regex/Tokenizer;->e:Z

    if-eqz v1, :cond_11

    .line 92
    iput-boolean v12, p0, Lflipboard/regex/Tokenizer;->e:Z

    .line 93
    iget v1, p0, Lflipboard/regex/Tokenizer;->f:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lflipboard/regex/Tokenizer;->f:I

    move v2, v4

    .line 100
    :goto_0
    if-ne v2, v5, :cond_0

    .line 101
    iget-object v1, p0, Lflipboard/regex/Tokenizer;->b:Ljava/io/BufferedReader;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->read()I

    move-result v1

    move v9, v2

    .line 106
    :goto_1
    if-gez v1, :cond_1

    .line 107
    iput v13, p0, Lflipboard/regex/Tokenizer;->c:I

    .line 197
    :goto_2
    return-void

    :cond_0
    move v1, v2

    move v9, v5

    .line 104
    goto :goto_1

    .line 112
    :cond_1
    const/16 v2, 0xd

    if-ne v1, v2, :cond_2

    .line 113
    iget-object v1, p0, Lflipboard/regex/Tokenizer;->b:Ljava/io/BufferedReader;

    invoke-virtual {v1, v3}, Ljava/io/BufferedReader;->mark(I)V

    .line 114
    iget-object v1, p0, Lflipboard/regex/Tokenizer;->b:Ljava/io/BufferedReader;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->read()I

    move-result v1

    .line 115
    if-eq v1, v4, :cond_2

    .line 117
    iget-object v1, p0, Lflipboard/regex/Tokenizer;->b:Ljava/io/BufferedReader;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->reset()V

    move v1, v4

    .line 120
    :cond_2
    if-ne v1, v4, :cond_5

    .line 121
    iget v1, p0, Lflipboard/regex/Tokenizer;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lflipboard/regex/Tokenizer;->f:I

    .line 122
    iput v12, p0, Lflipboard/regex/Tokenizer;->g:I

    .line 124
    iget v1, p0, Lflipboard/regex/Tokenizer;->f:I

    iput v1, p0, Lflipboard/regex/Tokenizer;->h:I

    .line 125
    iget v1, p0, Lflipboard/regex/Tokenizer;->g:I

    iput v1, p0, Lflipboard/regex/Tokenizer;->i:I

    move v2, v3

    .line 132
    :goto_3
    iget-object v1, v0, Lflipboard/regex/Regex;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/regex/Regex$State;

    invoke-virtual {v1, v2}, Lflipboard/regex/Regex$State;->a(I)Lflipboard/regex/Regex$State;

    move-result-object v6

    .line 133
    if-eqz v6, :cond_10

    .line 134
    iget v8, p0, Lflipboard/regex/Tokenizer;->f:I

    .line 135
    iget v7, p0, Lflipboard/regex/Tokenizer;->g:I

    .line 136
    iget-object v1, p0, Lflipboard/regex/Tokenizer;->b:Ljava/io/BufferedReader;

    const v10, 0x8000

    invoke-virtual {v1, v10}, Ljava/io/BufferedReader;->mark(I)V

    .line 137
    if-eq v2, v3, :cond_3

    .line 138
    iget-object v1, p0, Lflipboard/regex/Tokenizer;->d:Ljava/lang/StringBuilder;

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 140
    :cond_3
    iget-object v1, p0, Lflipboard/regex/Tokenizer;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    .line 141
    iget v1, v6, Lflipboard/regex/Regex$State;->b:I

    move v10, v8

    move-object v8, v6

    move v6, v1

    move v14, v7

    move v7, v2

    move v2, v9

    move v9, v14

    .line 145
    :cond_4
    :goto_4
    if-ne v2, v5, :cond_6

    .line 146
    iget-object v1, p0, Lflipboard/regex/Tokenizer;->b:Ljava/io/BufferedReader;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->read()I

    move-result v1

    .line 151
    :goto_5
    if-gez v1, :cond_9

    .line 152
    const/4 v1, 0x2

    invoke-virtual {v8, v1}, Lflipboard/regex/Regex$State;->a(I)Lflipboard/regex/Regex$State;

    move-result-object v1

    .line 153
    if-eqz v1, :cond_7

    .line 154
    iget-object v0, v0, Lflipboard/regex/Regex;->b:Ljava/util/ArrayList;

    iget v1, v1, Lflipboard/regex/Regex$State;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$Action;

    iget v0, v0, Lflipboard/regex/Regex$Action;->b:I

    iput v0, p0, Lflipboard/regex/Tokenizer;->c:I

    goto :goto_2

    .line 127
    :cond_5
    iget v2, p0, Lflipboard/regex/Tokenizer;->f:I

    iput v2, p0, Lflipboard/regex/Tokenizer;->h:I

    .line 128
    iget v2, p0, Lflipboard/regex/Tokenizer;->g:I

    iput v2, p0, Lflipboard/regex/Tokenizer;->i:I

    .line 129
    iget v2, p0, Lflipboard/regex/Tokenizer;->g:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lflipboard/regex/Tokenizer;->g:I

    move v2, v1

    goto :goto_3

    :cond_6
    move v1, v2

    move v2, v5

    .line 149
    goto :goto_5

    .line 155
    :cond_7
    if-eqz v6, :cond_8

    .line 156
    iget-object v1, p0, Lflipboard/regex/Tokenizer;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 157
    iput v10, p0, Lflipboard/regex/Tokenizer;->f:I

    .line 158
    iput v9, p0, Lflipboard/regex/Tokenizer;->g:I

    .line 159
    iget-object v1, p0, Lflipboard/regex/Tokenizer;->b:Ljava/io/BufferedReader;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->reset()V

    .line 160
    iget-object v0, v0, Lflipboard/regex/Regex;->b:Ljava/util/ArrayList;

    add-int/lit8 v1, v6, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$Action;

    iget v0, v0, Lflipboard/regex/Regex$Action;->b:I

    iput v0, p0, Lflipboard/regex/Tokenizer;->c:I

    goto/16 :goto_2

    .line 162
    :cond_8
    iput v13, p0, Lflipboard/regex/Tokenizer;->c:I

    goto/16 :goto_2

    .line 168
    :cond_9
    const/16 v11, 0xd

    if-ne v1, v11, :cond_a

    .line 169
    iget-object v1, p0, Lflipboard/regex/Tokenizer;->b:Ljava/io/BufferedReader;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->read()I

    move-result v1

    .line 170
    if-eq v1, v4, :cond_a

    move v2, v1

    move v1, v4

    .line 175
    :cond_a
    if-ne v1, v4, :cond_b

    .line 176
    iget v11, p0, Lflipboard/regex/Tokenizer;->f:I

    add-int/lit8 v11, v11, 0x1

    iput v11, p0, Lflipboard/regex/Tokenizer;->f:I

    .line 177
    iput v12, p0, Lflipboard/regex/Tokenizer;->g:I

    .line 179
    const/4 v11, 0x2

    invoke-virtual {v8, v11}, Lflipboard/regex/Regex$State;->a(I)Lflipboard/regex/Regex$State;

    move-result-object v11

    .line 180
    if-eqz v11, :cond_c

    .line 181
    iget-object v0, v0, Lflipboard/regex/Regex;->b:Ljava/util/ArrayList;

    iget v1, v11, Lflipboard/regex/Regex$State;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$Action;

    iget v0, v0, Lflipboard/regex/Regex$Action;->b:I

    iput v0, p0, Lflipboard/regex/Tokenizer;->c:I

    .line 182
    iput-boolean v3, p0, Lflipboard/regex/Tokenizer;->e:Z

    goto/16 :goto_2

    .line 186
    :cond_b
    iget v11, p0, Lflipboard/regex/Tokenizer;->g:I

    add-int/lit8 v11, v11, 0x1

    iput v11, p0, Lflipboard/regex/Tokenizer;->g:I

    .line 189
    :cond_c
    invoke-virtual {v8, v1}, Lflipboard/regex/Regex$State;->a(I)Lflipboard/regex/Regex$State;

    move-result-object v8

    .line 190
    if-nez v8, :cond_f

    .line 191
    if-eqz v6, :cond_d

    .line 192
    iget-object v1, p0, Lflipboard/regex/Tokenizer;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 193
    iput v10, p0, Lflipboard/regex/Tokenizer;->f:I

    .line 194
    iput v9, p0, Lflipboard/regex/Tokenizer;->g:I

    .line 195
    iget-object v1, p0, Lflipboard/regex/Tokenizer;->b:Ljava/io/BufferedReader;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->reset()V

    .line 196
    iget-object v0, v0, Lflipboard/regex/Regex;->b:Ljava/util/ArrayList;

    add-int/lit8 v1, v6, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$Action;

    iget v0, v0, Lflipboard/regex/Regex$Action;->b:I

    iput v0, p0, Lflipboard/regex/Tokenizer;->c:I

    goto/16 :goto_2

    .line 199
    :cond_d
    iget-object v2, p0, Lflipboard/regex/Tokenizer;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 200
    if-ne v1, v4, :cond_e

    .line 201
    iget v2, p0, Lflipboard/regex/Tokenizer;->f:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lflipboard/regex/Tokenizer;->f:I

    :goto_6
    move v2, v1

    .line 206
    goto/16 :goto_0

    .line 203
    :cond_e
    iget v2, p0, Lflipboard/regex/Tokenizer;->g:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lflipboard/regex/Tokenizer;->g:I

    goto :goto_6

    .line 209
    :cond_f
    iget-object v11, p0, Lflipboard/regex/Tokenizer;->d:Ljava/lang/StringBuilder;

    int-to-char v1, v1

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 210
    iget v1, v8, Lflipboard/regex/Regex$State;->b:I

    if-eqz v1, :cond_4

    .line 211
    iget v1, v8, Lflipboard/regex/Regex$State;->b:I

    .line 212
    iget-object v6, p0, Lflipboard/regex/Tokenizer;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    .line 213
    iget v9, p0, Lflipboard/regex/Tokenizer;->f:I

    .line 214
    iget v7, p0, Lflipboard/regex/Tokenizer;->g:I

    .line 215
    iget-object v10, p0, Lflipboard/regex/Tokenizer;->b:Ljava/io/BufferedReader;

    const v11, 0x8000

    invoke-virtual {v10, v11}, Ljava/io/BufferedReader;->mark(I)V

    move v10, v9

    move v9, v7

    move v7, v6

    move v6, v1

    goto/16 :goto_4

    :cond_10
    move v2, v9

    .line 219
    goto/16 :goto_0

    :cond_11
    move v2, v5

    goto/16 :goto_0
.end method
