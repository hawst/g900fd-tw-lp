.class Lflipboard/daydream/FlipboardDream$1;
.super Ljava/lang/Object;
.source "FlipboardDream.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/toc/CoverPage;

.field final synthetic b:Lflipboard/daydream/FlipboardDream;


# direct methods
.method constructor <init>(Lflipboard/daydream/FlipboardDream;Lflipboard/gui/toc/CoverPage;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lflipboard/daydream/FlipboardDream$1;->b:Lflipboard/daydream/FlipboardDream;

    iput-object p2, p0, Lflipboard/daydream/FlipboardDream$1;->a:Lflipboard/gui/toc/CoverPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 54
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lflipboard/daydream/FlipboardDream$1;->b:Lflipboard/daydream/FlipboardDream;

    const-class v2, Lflipboard/activities/LaunchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 55
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 56
    iget-object v1, p0, Lflipboard/daydream/FlipboardDream$1;->a:Lflipboard/gui/toc/CoverPage;

    invoke-virtual {v1}, Lflipboard/gui/toc/CoverPage;->getCurrent()Lflipboard/objs/FeedItem;

    move-result-object v1

    .line 57
    if-eqz v1, :cond_0

    iget-object v2, v1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 58
    const-string v2, "item"

    iget-object v1, v1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    const-string v1, "sid"

    iget-object v2, p0, Lflipboard/daydream/FlipboardDream$1;->a:Lflipboard/gui/toc/CoverPage;

    invoke-virtual {v2}, Lflipboard/gui/toc/CoverPage;->getSection()Lflipboard/service/Section;

    move-result-object v2

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    :cond_0
    iget-object v1, p0, Lflipboard/daydream/FlipboardDream$1;->b:Lflipboard/daydream/FlipboardDream;

    invoke-virtual {v1, v0}, Lflipboard/daydream/FlipboardDream;->startActivity(Landroid/content/Intent;)V

    .line 62
    iget-object v0, p0, Lflipboard/daydream/FlipboardDream$1;->b:Lflipboard/daydream/FlipboardDream;

    invoke-virtual {v0}, Lflipboard/daydream/FlipboardDream;->finish()V

    .line 63
    iget-object v0, p0, Lflipboard/daydream/FlipboardDream$1;->b:Lflipboard/daydream/FlipboardDream;

    invoke-static {v0}, Lflipboard/daydream/FlipboardDream;->a(Lflipboard/daydream/FlipboardDream;)Z

    .line 64
    return-void
.end method
