.class Lflipboard/daydream/FlipboardDream$2;
.super Ljava/util/TimerTask;
.source "FlipboardDream.java"


# instance fields
.field final synthetic a:Lflipboard/daydream/FlipboardDream;


# direct methods
.method constructor <init>(Lflipboard/daydream/FlipboardDream;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lflipboard/daydream/FlipboardDream$2;->a:Lflipboard/daydream/FlipboardDream;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 86
    invoke-static {}, Lflipboard/activities/FlipboardDreamSettings;->j()Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    move-result-object v0

    .line 87
    sget-object v1, Lflipboard/activities/FlipboardDreamSettings$UpdateType;->b:Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lflipboard/activities/FlipboardDreamSettings$UpdateType;->a:Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    if-ne v0, v1, :cond_1

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    :cond_0
    sget-object v0, Lflipboard/daydream/FlipboardDream;->a:Lflipboard/util/Log;

    .line 89
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->h:Lflipboard/service/Section;

    invoke-virtual {v0, v2, v3, v2, v3}, Lflipboard/service/Section;->a(ZLflipboard/util/Callback;ZLandroid/os/Bundle;)Z

    .line 92
    :cond_1
    iget-object v0, p0, Lflipboard/daydream/FlipboardDream$2;->a:Lflipboard/daydream/FlipboardDream;

    iget-object v1, p0, Lflipboard/daydream/FlipboardDream$2;->a:Lflipboard/daydream/FlipboardDream;

    invoke-virtual {v1}, Lflipboard/daydream/FlipboardDream;->a()Ljava/util/TimerTask;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/daydream/FlipboardDream;->a(Lflipboard/daydream/FlipboardDream;Ljava/util/TimerTask;)Ljava/util/TimerTask;

    .line 93
    sget-object v0, Lflipboard/daydream/FlipboardDream;->a:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Scheduling next update in %d ms"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/daydream/FlipboardDream$2;->a:Lflipboard/daydream/FlipboardDream;

    invoke-static {v1}, Lflipboard/daydream/FlipboardDream;->b(Lflipboard/daydream/FlipboardDream;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 94
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    iget-object v1, p0, Lflipboard/daydream/FlipboardDream$2;->a:Lflipboard/daydream/FlipboardDream;

    invoke-static {v1}, Lflipboard/daydream/FlipboardDream;->c(Lflipboard/daydream/FlipboardDream;)Ljava/util/TimerTask;

    move-result-object v1

    iget-object v2, p0, Lflipboard/daydream/FlipboardDream$2;->a:Lflipboard/daydream/FlipboardDream;

    invoke-static {v2}, Lflipboard/daydream/FlipboardDream;->b(Lflipboard/daydream/FlipboardDream;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 97
    iget-object v0, p0, Lflipboard/daydream/FlipboardDream$2;->a:Lflipboard/daydream/FlipboardDream;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v1

    iget v1, v1, Lflipboard/model/ConfigSetting;->DaydreamFeedFetchIntervalMax:I

    int-to-long v2, v1

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iget-object v1, p0, Lflipboard/daydream/FlipboardDream$2;->a:Lflipboard/daydream/FlipboardDream;

    invoke-static {v1}, Lflipboard/daydream/FlipboardDream;->b(Lflipboard/daydream/FlipboardDream;)J

    move-result-wide v4

    const-wide/16 v6, 0x2

    mul-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lflipboard/daydream/FlipboardDream;->a(Lflipboard/daydream/FlipboardDream;J)J

    .line 98
    return-void
.end method
