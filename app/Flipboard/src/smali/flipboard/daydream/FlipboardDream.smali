.class public Lflipboard/daydream/FlipboardDream;
.super Landroid/service/dreams/DreamService;
.source "FlipboardDream.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# static fields
.field public static a:Lflipboard/util/Log;


# instance fields
.field private b:Ljava/util/TimerTask;

.field private c:J

.field private d:Lflipboard/io/UsageEvent;

.field private e:Z

.field private f:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-string v0, "daydream"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/daydream/FlipboardDream;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/service/dreams/DreamService;-><init>()V

    return-void
.end method

.method static synthetic a(Lflipboard/daydream/FlipboardDream;J)J
    .locals 1

    .prologue
    .line 30
    iput-wide p1, p0, Lflipboard/daydream/FlipboardDream;->f:J

    return-wide p1
.end method

.method static synthetic a(Lflipboard/daydream/FlipboardDream;Ljava/util/TimerTask;)Ljava/util/TimerTask;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lflipboard/daydream/FlipboardDream;->b:Ljava/util/TimerTask;

    return-object p1
.end method

.method static synthetic a(Lflipboard/daydream/FlipboardDream;)Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/daydream/FlipboardDream;->e:Z

    return v0
.end method

.method static synthetic b(Lflipboard/daydream/FlipboardDream;)J
    .locals 2

    .prologue
    .line 30
    iget-wide v0, p0, Lflipboard/daydream/FlipboardDream;->f:J

    return-wide v0
.end method

.method static synthetic c(Lflipboard/daydream/FlipboardDream;)Ljava/util/TimerTask;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lflipboard/daydream/FlipboardDream;->b:Ljava/util/TimerTask;

    return-object v0
.end method


# virtual methods
.method final a()Ljava/util/TimerTask;
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lflipboard/daydream/FlipboardDream$2;

    invoke-direct {v0, p0}, Lflipboard/daydream/FlipboardDream$2;-><init>(Lflipboard/daydream/FlipboardDream;)V

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 42
    invoke-super {p0}, Landroid/service/dreams/DreamService;->onAttachedToWindow()V

    .line 44
    invoke-virtual {p0, v2}, Lflipboard/daydream/FlipboardDream;->setInteractive(Z)V

    .line 45
    const v0, 0x7f030138

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/CoverPage;

    .line 46
    invoke-virtual {v0}, Lflipboard/gui/toc/CoverPage;->a()V

    .line 47
    invoke-virtual {v0, v2}, Lflipboard/gui/toc/CoverPage;->setNeverShare(Z)V

    .line 48
    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lflipboard/gui/toc/CoverPage;->a(ZI)V

    .line 49
    invoke-virtual {p0, v0}, Lflipboard/daydream/FlipboardDream;->setContentView(Landroid/view/View;)V

    .line 50
    const v1, 0x7f0a022d

    invoke-virtual {p0, v1}, Lflipboard/daydream/FlipboardDream;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 51
    const v1, 0x7f0a029a

    invoke-virtual {p0, v1}, Lflipboard/daydream/FlipboardDream;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lflipboard/daydream/FlipboardDream$1;

    invoke-direct {v2, p0, v0}, Lflipboard/daydream/FlipboardDream$1;-><init>(Lflipboard/daydream/FlipboardDream;Lflipboard/gui/toc/CoverPage;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    return-void
.end method

.method public onDreamingStarted()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 71
    invoke-super {p0}, Landroid/service/dreams/DreamService;->onDreamingStarted()V

    .line 72
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "appdaydream"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/daydream/FlipboardDream;->d:Lflipboard/io/UsageEvent;

    .line 73
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/daydream/FlipboardDream;->c:J

    .line 74
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->g()V

    .line 75
    invoke-virtual {p0}, Lflipboard/daydream/FlipboardDream;->a()Ljava/util/TimerTask;

    move-result-object v0

    iput-object v0, p0, Lflipboard/daydream/FlipboardDream;->b:Ljava/util/TimerTask;

    .line 76
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget v0, v0, Lflipboard/model/ConfigSetting;->DaydreamFeedFetchInterval:I

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lflipboard/daydream/FlipboardDream;->f:J

    .line 77
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    iget-object v1, p0, Lflipboard/daydream/FlipboardDream;->b:Ljava/util/TimerTask;

    iget-wide v2, p0, Lflipboard/daydream/FlipboardDream;->f:J

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 78
    return-void
.end method

.method public onDreamingStopped()V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 105
    invoke-super {p0}, Landroid/service/dreams/DreamService;->onDreamingStopped()V

    .line 106
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget v0, v0, Lflipboard/service/FlipboardManager;->aB:I

    if-nez v0, :cond_0

    .line 107
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->h()V

    .line 109
    :cond_0
    iget-object v0, p0, Lflipboard/daydream/FlipboardDream;->b:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 110
    iget-object v0, p0, Lflipboard/daydream/FlipboardDream;->d:Lflipboard/io/UsageEvent;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lflipboard/daydream/FlipboardDream;->c:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lflipboard/io/UsageEvent;->g:J

    .line 111
    iget-object v1, p0, Lflipboard/daydream/FlipboardDream;->d:Lflipboard/io/UsageEvent;

    const-string v2, "exitPath"

    iget-boolean v0, p0, Lflipboard/daydream/FlipboardDream;->e:Z

    if-eqz v0, :cond_1

    const-string v0, "appLaunch"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 112
    iget-object v0, p0, Lflipboard/daydream/FlipboardDream;->d:Lflipboard/io/UsageEvent;

    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 113
    return-void

    .line 111
    :cond_1
    const-string v0, "cancel"

    goto :goto_0
.end method
