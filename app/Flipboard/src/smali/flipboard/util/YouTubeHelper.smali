.class public Lflipboard/util/YouTubeHelper;
.super Ljava/lang/Object;
.source "YouTubeHelper.java"


# static fields
.field public static final a:Lflipboard/util/Log;

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "youtube"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/util/YouTubeHelper;->a:Lflipboard/util/Log;

    .line 24
    const/4 v0, -0x1

    sput v0, Lflipboard/util/YouTubeHelper;->b:I

    return-void
.end method

.method public static a(Lcom/google/android/youtube/player/YouTubePlayer$OnInitializedListener;)Lcom/google/android/youtube/player/YouTubePlayerSupportFragment;
    .locals 3

    .prologue
    .line 67
    new-instance v0, Lcom/google/android/youtube/player/YouTubePlayerSupportFragment;

    invoke-direct {v0}, Lcom/google/android/youtube/player/YouTubePlayerSupportFragment;-><init>()V

    .line 68
    const-string v1, "AI39si7W-95H4SHtCZl7og8-j4X0ki0NG3_P12qfCRQx79ufFB47EumR5xvsb58PuM6wfNVUI3GOfo0U5LWzanNyxkOpwjZ8bQ"

    const-string v2, "Developer key cannot be null or empty"

    invoke-static {v1, v2}, Lcom/google/android/youtube/player/internal/ac;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/youtube/player/YouTubePlayerSupportFragment;->a:Ljava/lang/String;

    iput-object p0, v0, Lcom/google/android/youtube/player/YouTubePlayerSupportFragment;->b:Lcom/google/android/youtube/player/YouTubePlayer$OnInitializedListener;

    invoke-virtual {v0}, Lcom/google/android/youtube/player/YouTubePlayerSupportFragment;->a()V

    .line 69
    return-object v0
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 30
    invoke-static {}, Lflipboard/util/YouTubeHelper;->c()V

    .line 31
    sget v0, Lflipboard/util/YouTubeHelper;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()I
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lflipboard/util/YouTubeHelper;->c()V

    .line 37
    sget v0, Lflipboard/util/YouTubeHelper;->b:I

    return v0
.end method

.method private static c()V
    .locals 3

    .prologue
    .line 42
    sget v0, Lflipboard/util/YouTubeHelper;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 45
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-static {v0}, Lcom/google/android/youtube/player/YouTubeApiServiceUtil;->a(Landroid/content/Context;)Lcom/google/android/youtube/player/YouTubeInitializationResult;

    move-result-object v0

    .line 46
    sget-object v1, Lcom/google/android/youtube/player/YouTubeInitializationResult;->a:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/player/YouTubeInitializationResult;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    const/4 v0, 0x2

    sput v0, Lflipboard/util/YouTubeHelper;->b:I

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 56
    :try_start_0
    const-string v1, "com.google.android.youtube"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 57
    const/4 v0, 0x1

    sput v0, Lflipboard/util/YouTubeHelper;->b:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 59
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    sput v0, Lflipboard/util/YouTubeHelper;->b:I

    goto :goto_0
.end method
