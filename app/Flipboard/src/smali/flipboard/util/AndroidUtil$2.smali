.class final Lflipboard/util/AndroidUtil$2;
.super Ljava/lang/Object;
.source "AndroidUtil.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Ljava/util/LinkedHashMap;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Lflipboard/service/Flap$TypedResultObserver;


# direct methods
.method constructor <init>(Ljava/util/LinkedHashMap;Ljava/util/List;Lflipboard/service/Flap$TypedResultObserver;)V
    .locals 0

    .prologue
    .line 674
    iput-object p1, p0, Lflipboard/util/AndroidUtil$2;->a:Ljava/util/LinkedHashMap;

    iput-object p2, p0, Lflipboard/util/AndroidUtil$2;->b:Ljava/util/List;

    iput-object p3, p0, Lflipboard/util/AndroidUtil$2;->c:Lflipboard/service/Flap$TypedResultObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 7

    .prologue
    .line 677
    iget-object v2, p0, Lflipboard/util/AndroidUtil$2;->a:Ljava/util/LinkedHashMap;

    monitor-enter v2

    .line 678
    :try_start_0
    iget-object v0, p0, Lflipboard/util/AndroidUtil$2;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 679
    invoke-virtual {p1, v0}, Lflipboard/json/FLObject;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/json/FLObject;

    check-cast v1, Lflipboard/json/FLObject;

    .line 680
    if-eqz v1, :cond_0

    .line 682
    const-string v4, "largeURL"

    invoke-virtual {v1, v4}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 683
    const-string v5, "mediumURL"

    invoke-virtual {v1, v5}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 684
    const-string v6, "smallURL"

    invoke-virtual {v1, v6}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 685
    new-instance v6, Lflipboard/objs/RecentImage;

    invoke-direct {v6, v4, v5, v1}, Lflipboard/objs/RecentImage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    iget-object v1, p0, Lflipboard/util/AndroidUtil$2;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 689
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 690
    iget-object v0, p0, Lflipboard/util/AndroidUtil$2;->c:Lflipboard/service/Flap$TypedResultObserver;

    if-eqz v0, :cond_2

    .line 691
    iget-object v0, p0, Lflipboard/util/AndroidUtil$2;->c:Lflipboard/service/Flap$TypedResultObserver;

    iget-object v1, p0, Lflipboard/util/AndroidUtil$2;->a:Ljava/util/LinkedHashMap;

    invoke-interface {v0, v1}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/Object;)V

    .line 693
    :cond_2
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 697
    iget-object v0, p0, Lflipboard/util/AndroidUtil$2;->c:Lflipboard/service/Flap$TypedResultObserver;

    if-eqz v0, :cond_0

    .line 698
    iget-object v0, p0, Lflipboard/util/AndroidUtil$2;->c:Lflipboard/service/Flap$TypedResultObserver;

    invoke-interface {v0, p1}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V

    .line 700
    :cond_0
    return-void
.end method
