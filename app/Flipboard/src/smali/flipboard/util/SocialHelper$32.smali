.class final Lflipboard/util/SocialHelper$32;
.super Ljava/lang/Object;
.source "SocialHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/service/Section;

.field final synthetic b:Lflipboard/objs/FeedItem;

.field final synthetic c:Landroid/support/v4/app/FragmentActivity;

.field final synthetic d:Lflipboard/objs/ConfigService;


# direct methods
.method constructor <init>(Lflipboard/service/Section;Lflipboard/objs/FeedItem;Landroid/support/v4/app/FragmentActivity;Lflipboard/objs/ConfigService;)V
    .locals 0

    .prologue
    .line 1303
    iput-object p1, p0, Lflipboard/util/SocialHelper$32;->a:Lflipboard/service/Section;

    iput-object p2, p0, Lflipboard/util/SocialHelper$32;->b:Lflipboard/objs/FeedItem;

    iput-object p3, p0, Lflipboard/util/SocialHelper$32;->c:Landroid/support/v4/app/FragmentActivity;

    iput-object p4, p0, Lflipboard/util/SocialHelper$32;->d:Lflipboard/objs/ConfigService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1306
    iget-object v0, p0, Lflipboard/util/SocialHelper$32;->a:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v0

    .line 1307
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 1308
    const v2, 0x7f0d00a0

    invoke-virtual {v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 1309
    iget-object v2, p0, Lflipboard/util/SocialHelper$32;->b:Lflipboard/objs/FeedItem;

    invoke-static {v2}, Lflipboard/gui/section/ItemDisplayUtil;->f(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v2

    .line 1310
    if-eqz v0, :cond_0

    .line 1311
    iget-object v3, p0, Lflipboard/util/SocialHelper$32;->c:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d009f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v5

    iget-object v2, p0, Lflipboard/util/SocialHelper$32;->d:Lflipboard/objs/ConfigService;

    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v6

    iget-object v2, p0, Lflipboard/util/SocialHelper$32;->a:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v7

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 1315
    :goto_0
    const v2, 0x7f0d004a

    invoke-virtual {v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 1316
    const v2, 0x7f0d0178

    invoke-virtual {v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 1317
    new-instance v2, Lflipboard/util/SocialHelper$32$1;

    invoke-direct {v2, p0, v0}, Lflipboard/util/SocialHelper$32$1;-><init>(Lflipboard/util/SocialHelper$32;Z)V

    iput-object v2, v1, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 1330
    iget-object v0, p0, Lflipboard/util/SocialHelper$32;->c:Landroid/support/v4/app/FragmentActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "mute"

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1331
    return-void

    .line 1313
    :cond_0
    iget-object v3, p0, Lflipboard/util/SocialHelper$32;->c:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d009e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v2, v4, v5

    iget-object v2, p0, Lflipboard/util/SocialHelper$32;->d:Lflipboard/objs/ConfigService;

    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v6

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    goto :goto_0
.end method
