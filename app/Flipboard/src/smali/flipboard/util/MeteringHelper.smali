.class public Lflipboard/util/MeteringHelper;
.super Ljava/lang/Object;
.source "MeteringHelper.java"


# static fields
.field public static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field static b:Lflipboard/util/Log;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-string v0, "metering"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/util/MeteringHelper;->b:Lflipboard/util/Log;

    return-void
.end method

.method public static a(Landroid/content/Context;Lflipboard/objs/FeedItem;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 51
    const/4 v2, 0x0

    .line 52
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->cr:Z

    if-eqz v0, :cond_1

    .line 53
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->aj()Lflipboard/objs/ConfigService;

    move-result-object v3

    .line 54
    if-eqz v3, :cond_1

    .line 55
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v3, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 56
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lflipboard/service/Account;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 57
    :cond_0
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-static {p0, v0}, Lflipboard/util/MeteringHelper;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    .line 58
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-static {p0, v0}, Lflipboard/util/MeteringHelper;->c(Landroid/content/Context;Ljava/lang/String;)V

    sget-object v0, Lflipboard/util/MeteringHelper;->a:Ljava/util/Map;

    iget-object v4, p1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iget-object v4, p1, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-static {v4}, Lflipboard/util/MeteringHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 59
    add-int/lit8 v0, v1, 0x1

    .line 62
    :goto_0
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->aj()Lflipboard/objs/ConfigService;

    move-result-object v1

    .line 63
    if-eqz v1, :cond_1

    .line 64
    invoke-static {v1}, Lflipboard/util/MeteringHelper;->b(Lflipboard/objs/ConfigService;)Ljava/lang/String;

    move-result-object v1

    .line 66
    if-eqz v1, :cond_1

    if-lez v0, :cond_1

    .line 67
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v4, v3, Lflipboard/objs/ConfigService;->bI:I

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v5

    iget v0, v3, Lflipboard/objs/ConfigService;->bI:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v6

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 68
    const-string v1, "window.flipboardMagazineData = { \"meterMessage\": \"%s\" };"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 74
    :goto_1
    return-object v0

    :cond_1
    move-object v0, v2

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static a(Lflipboard/objs/ConfigService;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lflipboard/objs/ConfigService;->bG:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/ConfigService;->bG:Ljava/lang/String;

    const-string v1, "day"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "NYTMeteringRoadblockExplanationDay"

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 93
    :goto_0
    return-object v0

    .line 88
    :cond_1
    iget-object v0, p0, Lflipboard/objs/ConfigService;->bG:Ljava/lang/String;

    const-string v1, "week"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 89
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "NYTMeteringRoadblockExplanationWeek"

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 91
    :cond_2
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "NYTMeteringRoadblockExplanationMonth"

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    if-nez p0, :cond_1

    .line 236
    const/4 p0, 0x0

    .line 243
    :cond_0
    :goto_0
    return-object p0

    .line 238
    :cond_1
    const/16 v0, 0x3a

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 240
    if-lez v0, :cond_0

    .line 241
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 39
    invoke-static {p0, p1}, Lflipboard/util/MeteringHelper;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 40
    sget-object v0, Lflipboard/util/MeteringHelper;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 41
    return-void
.end method

.method public static a(Lflipboard/util/MeterView;Ljava/lang/String;Lflipboard/util/MeteringHelper$ExitPath;)V
    .locals 3

    .prologue
    .line 272
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "event"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 273
    const-string v1, "id"

    invoke-interface {p0}, Lflipboard/util/MeterView;->getViewType()Lflipboard/util/MeteringHelper$MeteringViewUsageType;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 274
    const-string v1, "service"

    invoke-virtual {v0, v1, p1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 275
    if-eqz p2, :cond_0

    .line 276
    const-string v1, "exitPath"

    invoke-virtual {v0, v1, p2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 278
    :cond_0
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 279
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 208
    invoke-static {p0, p1}, Lflipboard/util/MeteringHelper;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 209
    sget-object v0, Lflipboard/util/MeteringHelper;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    .line 211
    const-string v1, "start_of_period"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 212
    const-string v1, "start_of_period"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 213
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1, p1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v4

    .line 214
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 215
    invoke-virtual {v5, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 216
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 218
    iget-object v1, v4, Lflipboard/objs/ConfigService;->bG:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, v4, Lflipboard/objs/ConfigService;->bG:Ljava/lang/String;

    const-string v3, "day"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 219
    :cond_0
    const/4 v1, 0x6

    .line 225
    :goto_0
    invoke-virtual {v2, v1}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v5, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    sub-int v1, v3, v1

    .line 226
    iget v3, v4, Lflipboard/objs/ConfigService;->bH:I

    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-le v1, v2, :cond_2

    .line 227
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "start_of_period"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "items_read_this_period"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v1}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 230
    :cond_2
    const-string v1, "items_read_this_period"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0

    .line 220
    :cond_3
    iget-object v1, v4, Lflipboard/objs/ConfigService;->bG:Ljava/lang/String;

    const-string v3, "week"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 221
    const/4 v1, 0x3

    goto :goto_0

    .line 223
    :cond_4
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lflipboard/objs/FeedItem;)Lflipboard/util/MeteringHelper$AccessType;
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 119
    sget-object v1, Lflipboard/util/MeteringHelper$AccessType;->c:Lflipboard/util/MeteringHelper$AccessType;

    .line 120
    if-eqz p1, :cond_3

    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->cr:Z

    if-eqz v0, :cond_3

    iget-object v0, p1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    const-string v2, "nytimes"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 121
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v2, "nytimes"

    invoke-virtual {v0, v2}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 122
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lflipboard/service/Account;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    sget-object v0, Lflipboard/util/MeteringHelper;->b:Lflipboard/util/Log;

    .line 124
    sget-object v0, Lflipboard/util/MeteringHelper$AccessType;->a:Lflipboard/util/MeteringHelper$AccessType;

    .line 157
    :cond_0
    :goto_0
    sget-object v1, Lflipboard/util/MeteringHelper;->b:Lflipboard/util/Log;

    new-array v1, v11, [Ljava/lang/Object;

    aput-object v0, v1, v10

    .line 159
    return-object v0

    .line 127
    :cond_1
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-static {p0, v0}, Lflipboard/util/MeteringHelper;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 128
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, p1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v2

    .line 129
    sget-object v0, Lflipboard/util/MeteringHelper;->a:Ljava/util/Map;

    iget-object v3, p1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    .line 132
    iget-object v3, p1, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-static {v3}, Lflipboard/util/MeteringHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 133
    invoke-interface {v0, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 134
    sget-object v4, Lflipboard/util/MeteringHelper;->b:Lflipboard/util/Log;

    new-array v4, v11, [Ljava/lang/Object;

    aput-object v3, v4, v10

    .line 135
    const-wide/16 v4, -0x1

    invoke-interface {v0, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 136
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    .line 137
    iget-wide v6, v2, Lflipboard/objs/ConfigService;->bJ:J

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    cmp-long v6, v4, v6

    if-lez v6, :cond_2

    .line 138
    sget-object v6, Lflipboard/util/MeteringHelper;->b:Lflipboard/util/Log;

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v6, v10

    .line 139
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-object v0, v1

    .line 146
    :goto_1
    iget-object v1, p1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-static {p0, v1}, Lflipboard/util/MeteringHelper;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    .line 147
    sget-object v3, Lflipboard/util/MeteringHelper$AccessType;->c:Lflipboard/util/MeteringHelper$AccessType;

    if-ne v0, v3, :cond_0

    iget v3, v2, Lflipboard/objs/ConfigService;->bI:I

    if-ge v1, v3, :cond_0

    .line 148
    sget-object v0, Lflipboard/util/MeteringHelper;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v10

    iget v1, v2, Lflipboard/objs/ConfigService;->bI:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    .line 149
    sget-object v0, Lflipboard/util/MeteringHelper$AccessType;->b:Lflipboard/util/MeteringHelper$AccessType;

    goto :goto_0

    .line 141
    :cond_2
    sget-object v0, Lflipboard/util/MeteringHelper$AccessType;->a:Lflipboard/util/MeteringHelper$AccessType;

    goto :goto_1

    .line 154
    :cond_3
    sget-object v0, Lflipboard/util/MeteringHelper;->b:Lflipboard/util/Log;

    .line 155
    sget-object v0, Lflipboard/util/MeteringHelper$AccessType;->a:Lflipboard/util/MeteringHelper$AccessType;

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public static b(Lflipboard/objs/ConfigService;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lflipboard/objs/ConfigService;->bG:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/ConfigService;->bG:Ljava/lang/String;

    const-string v1, "day"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "NYTMeteringStatusDay"

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    :goto_0
    return-object v0

    .line 107
    :cond_1
    iget-object v0, p0, Lflipboard/objs/ConfigService;->bG:Ljava/lang/String;

    const-string v1, "week"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 108
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "NYTMeteringStatusWeek"

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 110
    :cond_2
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "NYTMeteringStatusMonth"

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Lflipboard/objs/FeedItem;)V
    .locals 6

    .prologue
    .line 172
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->cr:Z

    if-eqz v0, :cond_1

    .line 173
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->aj()Lflipboard/objs/ConfigService;

    move-result-object v0

    .line 174
    if-eqz v0, :cond_1

    .line 175
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-static {p0, v0}, Lflipboard/util/MeteringHelper;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 176
    sget-object v0, Lflipboard/util/MeteringHelper;->a:Ljava/util/Map;

    iget-object v1, p1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    .line 177
    iget-object v1, p1, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-static {v1}, Lflipboard/util/MeteringHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 178
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 180
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-static {p0, v2}, Lflipboard/util/MeteringHelper;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 181
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 182
    const-string v4, "items_read_this_period"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 183
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 184
    invoke-interface {v3, v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 185
    const-string v1, "start_of_period"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 186
    const-string v0, "start_of_period"

    invoke-interface {v3, v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 188
    :cond_0
    invoke-static {v3}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 189
    sget-object v0, Lflipboard/util/MeteringHelper;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 193
    :cond_1
    return-void
.end method

.method private static c(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 197
    sget-object v0, Lflipboard/util/MeteringHelper;->a:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 198
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lflipboard/util/MeteringHelper;->a:Ljava/util/Map;

    .line 200
    :cond_0
    sget-object v0, Lflipboard/util/MeteringHelper;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "shared_prefs_metering_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 202
    sget-object v1, Lflipboard/util/MeteringHelper;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    :cond_1
    return-void
.end method
