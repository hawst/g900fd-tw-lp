.class Lflipboard/util/SocialHelper$33$1;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SocialHelper.java"


# instance fields
.field final synthetic a:Lflipboard/util/SocialHelper$33;


# direct methods
.method constructor <init>(Lflipboard/util/SocialHelper$33;)V
    .locals 0

    .prologue
    .line 1356
    iput-object p1, p0, Lflipboard/util/SocialHelper$33$1;->a:Lflipboard/util/SocialHelper$33;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1360
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 1361
    iget-object v1, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 1362
    iget-object v0, p0, Lflipboard/util/SocialHelper$33$1;->a:Lflipboard/util/SocialHelper$33;

    iget-object v0, v0, Lflipboard/util/SocialHelper$33;->a:Landroid/support/v4/app/FragmentActivity;

    instance-of v0, v0, Lflipboard/activities/FeedActivity;

    if-eqz v0, :cond_0

    .line 1363
    iget-object v0, p0, Lflipboard/util/SocialHelper$33$1;->a:Lflipboard/util/SocialHelper$33;

    iget-object v0, v0, Lflipboard/util/SocialHelper$33;->a:Landroid/support/v4/app/FragmentActivity;

    check-cast v0, Lflipboard/activities/FeedActivity;

    iget-object v3, p0, Lflipboard/util/SocialHelper$33$1;->a:Lflipboard/util/SocialHelper$33;

    iget-object v3, v3, Lflipboard/util/SocialHelper$33;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v3}, Lflipboard/activities/FeedActivity;->b(Lflipboard/objs/FeedItem;)V

    .line 1368
    :goto_0
    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v0

    iget-object v2, p0, Lflipboard/util/SocialHelper$33$1;->a:Lflipboard/util/SocialHelper$33;

    iget-object v2, v2, Lflipboard/util/SocialHelper$33;->c:Lflipboard/service/Section;

    iget-object v3, p0, Lflipboard/util/SocialHelper$33$1;->a:Lflipboard/util/SocialHelper$33;

    iget-object v3, v3, Lflipboard/util/SocialHelper$33;->b:Lflipboard/objs/FeedItem;

    const-string v5, "reportUser"

    new-instance v6, Lflipboard/util/SocialHelper$33$1$1;

    invoke-direct {v6, p0}, Lflipboard/util/SocialHelper$33$1$1;-><init>(Lflipboard/util/SocialHelper$33$1;)V

    invoke-virtual/range {v0 .. v6}, Lflipboard/service/Flap;->a(Lflipboard/service/User;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/CommentaryResult$Item$Commentary;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V

    .line 1381
    new-instance v0, Lflipboard/objs/UserState$MutedAuthor;

    invoke-direct {v0}, Lflipboard/objs/UserState$MutedAuthor;-><init>()V

    .line 1382
    iget-object v2, p0, Lflipboard/util/SocialHelper$33$1;->a:Lflipboard/util/SocialHelper$33;

    iget-object v2, v2, Lflipboard/util/SocialHelper$33;->b:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    iput-object v2, v0, Lflipboard/objs/UserState$MutedAuthor;->c:Ljava/lang/String;

    .line 1383
    iget-object v2, p0, Lflipboard/util/SocialHelper$33$1;->a:Lflipboard/util/SocialHelper$33;

    iget-object v2, v2, Lflipboard/util/SocialHelper$33;->b:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    iput-object v2, v0, Lflipboard/objs/UserState$MutedAuthor;->e:Ljava/lang/String;

    .line 1384
    iget-object v2, p0, Lflipboard/util/SocialHelper$33$1;->a:Lflipboard/util/SocialHelper$33;

    iget-object v2, v2, Lflipboard/util/SocialHelper$33;->b:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    iput-object v2, v0, Lflipboard/objs/UserState$MutedAuthor;->d:Ljava/lang/String;

    .line 1385
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0, v4}, Lflipboard/service/User;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 1386
    return-void

    .line 1365
    :cond_0
    iget-object v0, p0, Lflipboard/util/SocialHelper$33$1;->a:Lflipboard/util/SocialHelper$33;

    iget-object v0, v0, Lflipboard/util/SocialHelper$33;->b:Lflipboard/objs/FeedItem;

    invoke-static {v0}, Lflipboard/util/SocialHelper;->b(Lflipboard/objs/FeedItem;)V

    goto :goto_0
.end method
