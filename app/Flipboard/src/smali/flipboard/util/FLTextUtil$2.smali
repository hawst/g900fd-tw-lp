.class final Lflipboard/util/FLTextUtil$2;
.super Landroid/text/style/ClickableSpan;
.source "FLTextUtil.java"


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager;

.field final synthetic b:Lflipboard/objs/FeedSectionLink;

.field final synthetic c:Landroid/os/Bundle;

.field final synthetic d:I


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;Lflipboard/objs/FeedSectionLink;Landroid/os/Bundle;I)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lflipboard/util/FLTextUtil$2;->a:Lflipboard/service/FlipboardManager;

    iput-object p2, p0, Lflipboard/util/FLTextUtil$2;->b:Lflipboard/objs/FeedSectionLink;

    iput-object p3, p0, Lflipboard/util/FLTextUtil$2;->c:Landroid/os/Bundle;

    iput p4, p0, Lflipboard/util/FLTextUtil$2;->d:I

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lflipboard/util/FLTextUtil$2;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/util/FLTextUtil$2;->b:Lflipboard/objs/FeedSectionLink;

    invoke-direct {v0, v1}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lflipboard/util/FLTextUtil$2;->c:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lflipboard/util/SocialHelper;->a(Lflipboard/service/Section;Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v0

    if-nez v0, :cond_0

    .line 110
    iget v0, p0, Lflipboard/util/FLTextUtil$2;->d:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 112
    :cond_0
    return-void
.end method
