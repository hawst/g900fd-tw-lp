.class public Lflipboard/util/TouchInfo;
.super Ljava/lang/Object;
.source "TouchInfo.java"


# static fields
.field public static a:I

.field public static b:I

.field public static c:F


# instance fields
.field public final d:J

.field final e:Landroid/content/Context;

.field public final f:Z

.field public final g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:F

.field public l:F

.field public m:F

.field public n:F

.field public o:I


# direct methods
.method public constructor <init>(Landroid/view/MotionEvent;ZZ)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-boolean p2, p0, Lflipboard/util/TouchInfo;->f:Z

    .line 39
    iput-boolean p3, p0, Lflipboard/util/TouchInfo;->g:Z

    .line 40
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iput-wide v2, p0, Lflipboard/util/TouchInfo;->d:J

    .line 41
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    iput-object v1, p0, Lflipboard/util/TouchInfo;->e:Landroid/content/Context;

    .line 43
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iput v1, p0, Lflipboard/util/TouchInfo;->m:F

    .line 44
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iput v1, p0, Lflipboard/util/TouchInfo;->n:F

    .line 45
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object p0, v1, Lflipboard/service/FlipboardManager;->aw:Lflipboard/util/TouchInfo;

    .line 46
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-le v1, v0, :cond_1

    :goto_0
    iput-boolean v0, p0, Lflipboard/util/TouchInfo;->j:Z

    .line 48
    sget v0, Lflipboard/util/TouchInfo;->b:I

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lflipboard/util/TouchInfo;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    .line 51
    mul-int/lit8 v1, v0, 0x5

    sput v1, Lflipboard/util/TouchInfo;->b:I

    .line 52
    mul-int/lit8 v0, v0, 0x3

    sput v0, Lflipboard/util/TouchInfo;->a:I

    .line 53
    iget-object v0, p0, Lflipboard/util/TouchInfo;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    sput v0, Lflipboard/util/TouchInfo;->c:F

    .line 55
    :cond_0
    return-void

    .line 46
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
