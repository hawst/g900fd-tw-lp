.class Lflipboard/util/TOCBuilder$TOCBuilderObserver$3;
.super Ljava/lang/Object;
.source "TOCBuilder.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager;

.field final synthetic b:Lflipboard/util/TOCBuilder$TOCBuilderObserver;


# direct methods
.method constructor <init>(Lflipboard/util/TOCBuilder$TOCBuilderObserver;Lflipboard/service/FlipboardManager;)V
    .locals 0

    .prologue
    .line 461
    iput-object p1, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver$3;->b:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    iput-object p2, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver$3;->a:Lflipboard/service/FlipboardManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 465
    iget-object v0, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver$3;->b:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    iget-object v0, v0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->b:Lflipboard/activities/FlipboardActivity;

    sget-object v1, Lflipboard/util/TOCBuilder;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 468
    iget-object v0, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver$3;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->n()V

    .line 472
    iget-object v0, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver$3;->b:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    iget-object v0, v0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->b:Lflipboard/activities/FlipboardActivity;

    invoke-static {v0}, Lflipboard/activities/LaunchActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 473
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_0

    .line 474
    const-string v1, "page"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 476
    :cond_0
    iget-object v1, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver$3;->b:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    iget-object v1, v1, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->b:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v1}, Lflipboard/activities/FlipboardActivity;->finish()V

    .line 477
    iget-object v1, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver$3;->b:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    iget-object v1, v1, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->b:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v1, v0}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    .line 479
    iget-object v0, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver$3;->a:Lflipboard/service/FlipboardManager;

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->Y:Lflipboard/util/Observable$Proxy;

    sget-object v2, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->j:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-virtual {v1, v2, v0}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 482
    invoke-static {}, Lflipboard/util/TOCBuilder;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 483
    invoke-static {}, Lflipboard/util/TOCBuilder;->c()Z

    .line 484
    iget-object v0, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver$3;->b:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    invoke-virtual {v0}, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->b()V

    .line 485
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->e:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 486
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 488
    :cond_1
    return-void
.end method
