.class final Lflipboard/util/SocialHelper$8;
.super Landroid/text/style/ClickableSpan;
.source "SocialHelper.java"


# instance fields
.field final synthetic a:Lflipboard/objs/FeedSectionLink;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic c:Landroid/content/Context;


# direct methods
.method constructor <init>(Lflipboard/objs/FeedSectionLink;Landroid/os/Bundle;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 551
    iput-object p1, p0, Lflipboard/util/SocialHelper$8;->a:Lflipboard/objs/FeedSectionLink;

    iput-object p2, p0, Lflipboard/util/SocialHelper$8;->b:Landroid/os/Bundle;

    iput-object p3, p0, Lflipboard/util/SocialHelper$8;->c:Landroid/content/Context;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 555
    sget-object v0, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 562
    :cond_0
    :goto_0
    return-void

    .line 558
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v0

    if-nez v0, :cond_0

    .line 560
    new-instance v0, Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/util/SocialHelper$8;->a:Lflipboard/objs/FeedSectionLink;

    invoke-direct {v0, v1}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lflipboard/util/SocialHelper$8;->b:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lflipboard/util/SocialHelper;->a(Lflipboard/service/Section;Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 567
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v0

    if-nez v0, :cond_0

    .line 569
    iget-object v0, p0, Lflipboard/util/SocialHelper$8;->a:Lflipboard/objs/FeedSectionLink;

    iget v0, v0, Lflipboard/objs/FeedSectionLink;->q:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/util/SocialHelper$8;->a:Lflipboard/objs/FeedSectionLink;

    iget v0, v0, Lflipboard/objs/FeedSectionLink;->q:I

    :goto_0
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 571
    :cond_0
    iget-object v0, p0, Lflipboard/util/SocialHelper$8;->a:Lflipboard/objs/FeedSectionLink;

    iget-boolean v0, v0, Lflipboard/objs/FeedSectionLink;->r:Z

    if-eqz v0, :cond_2

    sget-object v0, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->z:Landroid/graphics/Typeface;

    :goto_1
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 572
    return-void

    .line 569
    :cond_1
    iget-object v0, p0, Lflipboard/util/SocialHelper$8;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080087

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0

    .line 571
    :cond_2
    sget-object v0, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->v:Landroid/graphics/Typeface;

    goto :goto_1
.end method
