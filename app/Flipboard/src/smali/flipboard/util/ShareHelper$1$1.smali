.class Lflipboard/util/ShareHelper$1$1;
.super Ljava/lang/Object;
.source "ShareHelper.java"

# interfaces
.implements Lflipboard/service/Flap$AlsoFlippedObserver;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lflipboard/util/ShareHelper$1;


# direct methods
.method constructor <init>(Lflipboard/util/ShareHelper$1;I)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lflipboard/util/ShareHelper$1$1;->b:Lflipboard/util/ShareHelper$1;

    iput p2, p0, Lflipboard/util/ShareHelper$1$1;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 162
    check-cast p1, Lflipboard/objs/AlsoFlippedResult;

    sget-object v1, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p1, v1, v0

    if-eqz p1, :cond_2

    iget-object v1, p1, Lflipboard/objs/AlsoFlippedResult;->a:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lflipboard/objs/AlsoFlippedResult;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p1, Lflipboard/objs/AlsoFlippedResult;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v4, v3, [Ljava/lang/String;

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p1, Lflipboard/objs/AlsoFlippedResult;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget v0, p0, Lflipboard/util/ShareHelper$1$1;->a:I

    if-nez v0, :cond_1

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "first.also.flip.timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-interface {v0, v1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "also.flip.seen.count"

    iget v3, p0, Lflipboard/util/ShareHelper$1$1;->a:I

    add-int/lit8 v3, v3, 0x1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    const-string v0, "flipboard.extra.feeditem.result"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    const-string v0, "flipboard.extra.show.also.flipped"

    invoke-virtual {v2, v0, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "flipboard.extra.from.section.remote.id"

    iget-object v1, p0, Lflipboard/util/ShareHelper$1$1;->b:Lflipboard/util/ShareHelper$1;

    iget-object v1, v1, Lflipboard/util/ShareHelper$1;->c:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "flipboard.extra.from.item.remote.id"

    iget-object v1, p0, Lflipboard/util/ShareHelper$1$1;->b:Lflipboard/util/ShareHelper$1;

    iget-object v1, v1, Lflipboard/util/ShareHelper$1;->d:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-class v1, Lflipboard/activities/SharePromoteActivity;

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Class;Landroid/os/Bundle;)V

    sget-object v0, Lflipboard/objs/UsageEventV2$EventDataType;->b:Lflipboard/objs/UsageEventV2$EventDataType;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;->a:Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    iget-object v2, p0, Lflipboard/util/ShareHelper$1$1;->b:Lflipboard/util/ShareHelper$1;

    iget-object v2, v2, Lflipboard/util/ShareHelper$1;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lflipboard/util/ShareHelper;->a(Lflipboard/objs/UsageEventV2$EventDataType;Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 193
    sget-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 195
    sget-object v0, Lflipboard/objs/UsageEventV2$EventDataType;->b:Lflipboard/objs/UsageEventV2$EventDataType;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;->b:Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    iget-object v2, p0, Lflipboard/util/ShareHelper$1$1;->b:Lflipboard/util/ShareHelper$1;

    iget-object v2, v2, Lflipboard/util/ShareHelper$1;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lflipboard/util/ShareHelper;->a(Lflipboard/objs/UsageEventV2$EventDataType;Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;Ljava/lang/String;)V

    .line 196
    return-void
.end method
