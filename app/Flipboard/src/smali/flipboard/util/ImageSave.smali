.class public Lflipboard/util/ImageSave;
.super Ljava/lang/Object;
.source "ImageSave.java"


# static fields
.field public static a:Lflipboard/util/Log;

.field static final synthetic b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lflipboard/util/ImageSave;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lflipboard/util/ImageSave;->b:Z

    .line 29
    const-string v0, "imageSave"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/util/ImageSave;->a:Lflipboard/util/Log;

    return-void

    .line 27
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 38
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 39
    if-ltz v0, :cond_0

    .line 40
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 42
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/activities/FlipboardActivity;Lflipboard/io/Download;Lflipboard/io/Download$Observer;)V
    .locals 0

    .prologue
    .line 27
    invoke-static {p0, p1, p2}, Lflipboard/util/ImageSave;->b(Lflipboard/activities/FlipboardActivity;Lflipboard/io/Download;Lflipboard/io/Download$Observer;)V

    return-void
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V
    .locals 5

    .prologue
    .line 55
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->ak()Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    const-string v0, "unwanted.try_save_invalid_image"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    .line 190
    :goto_0
    return-void

    .line 60
    :cond_0
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "event"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 61
    const-string v1, "id"

    const-string v2, "saveImageToDevice"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 62
    if-eqz p2, :cond_1

    .line 63
    const-string v1, "sectionIdentifier"

    invoke-virtual {p2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 65
    :cond_1
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 66
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 67
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->h()Ljava/lang/String;

    move-result-object v1

    .line 68
    sget-boolean v2, Lflipboard/util/ImageSave;->b:Z

    if-nez v2, :cond_2

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 69
    :cond_2
    sget-object v2, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;ZZ)Lflipboard/io/Download;

    move-result-object v1

    .line 70
    new-instance v2, Lflipboard/util/ImageSave$1;

    invoke-direct {v2, p1, v0, p0}, Lflipboard/util/ImageSave$1;-><init>(Lflipboard/objs/FeedItem;Lflipboard/service/FlipboardManager;Lflipboard/activities/FlipboardActivity;)V

    .line 186
    invoke-virtual {v1}, Lflipboard/io/Download;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 187
    invoke-static {p0, v1, v2}, Lflipboard/util/ImageSave;->b(Lflipboard/activities/FlipboardActivity;Lflipboard/io/Download;Lflipboard/io/Download$Observer;)V

    .line 189
    :cond_3
    invoke-virtual {v1, v2}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Observer;)V

    goto :goto_0
.end method

.method private static b(Lflipboard/activities/FlipboardActivity;Lflipboard/io/Download;Lflipboard/io/Download$Observer;)V
    .locals 3

    .prologue
    .line 203
    new-instance v0, Lflipboard/gui/dialog/FLProgressDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLProgressDialogFragment;-><init>()V

    .line 204
    const v1, 0x7f0d0286

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLProgressDialogFragment;->f(I)V

    .line 205
    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/gui/dialog/FLProgressDialogFragment;->j:Z

    .line 206
    const/4 v1, 0x0

    iput-boolean v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->C:Z

    .line 207
    new-instance v1, Lflipboard/util/ImageSave$2;

    invoke-direct {v1, p1, p2}, Lflipboard/util/ImageSave$2;-><init>(Lflipboard/io/Download;Lflipboard/io/Download$Observer;)V

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 215
    iget-boolean v1, p0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_0

    .line 216
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "saving_image"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 218
    :cond_0
    return-void
.end method
