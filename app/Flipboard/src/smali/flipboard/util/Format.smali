.class public Lflipboard/util/Format;
.super Ljava/lang/Object;
.source "Format.java"


# static fields
.field private static final j:[C

.field private static final k:[C


# instance fields
.field a:Ljava/lang/String;

.field b:[Ljava/lang/Object;

.field c:Lflipboard/util/Format$CharBuf;

.field d:Z

.field e:Z

.field f:Z

.field g:Z

.field h:I

.field i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "0123456789ABCDEF"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lflipboard/util/Format;->j:[C

    .line 18
    const-string v0, "0123456789abcdef"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lflipboard/util/Format;->k:[C

    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, Lflipboard/util/Format;->a:Ljava/lang/String;

    .line 109
    iput-object p2, p0, Lflipboard/util/Format;->b:[Ljava/lang/Object;

    .line 110
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;[Ljava/lang/Object;B)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Lflipboard/util/Format;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 65
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 66
    const/4 v0, 0x0

    .line 67
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 68
    add-int/lit8 v1, v0, 0x1

    if-lez v0, :cond_0

    .line 69
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    :cond_0
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 72
    goto :goto_0

    .line 73
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Iterable;Lflipboard/util/Format$Selector;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Iterable",
            "<TT;>;",
            "Lflipboard/util/Format$Selector",
            "<TT;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 82
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 83
    const/4 v0, 0x0

    .line 84
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 85
    invoke-interface {p2, v1}, Lflipboard/util/Format$Selector;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 86
    add-int/lit8 v1, v0, 0x1

    if-lez v0, :cond_0

    .line 87
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    :cond_0
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 90
    goto :goto_0

    .line 91
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lflipboard/util/Format;

    invoke-direct {v0, p0, p1}, Lflipboard/util/Format;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0}, Lflipboard/util/Format;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lflipboard/util/Format;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Lflipboard/util/Format;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Ljava/lang/RuntimeException;)V
    .locals 0

    .prologue
    .line 12
    invoke-static {p0}, Lflipboard/util/Format;->b(Ljava/lang/RuntimeException;)V

    return-void
.end method

.method private a(II)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 262
    if-lt p1, p2, :cond_0

    .line 263
    new-instance v2, Lflipboard/util/Format$Exception;

    const-string v3, "malformed format: %s at index %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lflipboard/util/Format;->a:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-direct {v2, v3, v4}, Lflipboard/util/Format$Exception;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v2}, Lflipboard/util/Format;->b(Ljava/lang/RuntimeException;)V

    .line 266
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic a()[C
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lflipboard/util/Format;->j:[C

    return-object v0
.end method

.method private static b(Ljava/lang/RuntimeException;)V
    .locals 3

    .prologue
    .line 272
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ERROR: suppressing format exception: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 278
    return-void
.end method

.method private b(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 114
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    .line 115
    const/4 v4, 0x0

    .line 116
    const/4 v6, 0x0

    :goto_0
    if-ge v6, v7, :cond_c

    .line 117
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 118
    const/16 v1, 0x25

    if-eq v0, v1, :cond_0

    .line 119
    iget-object v1, p0, Lflipboard/util/Format;->c:Lflipboard/util/Format$CharBuf;

    invoke-virtual {v1, v0}, Lflipboard/util/Format$CharBuf;->a(C)V

    move v2, v6

    move v3, v4

    .line 116
    :goto_1
    add-int/lit8 v6, v2, 0x1

    move v4, v3

    goto :goto_0

    .line 127
    :cond_0
    const/4 v1, 0x0

    .line 128
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/util/Format;->h:I

    .line 129
    add-int/lit8 v2, v6, 0x1

    invoke-direct {p0, v2, v7}, Lflipboard/util/Format;->a(II)Z

    move-result v0

    if-nez v0, :cond_c

    .line 130
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 133
    const/16 v3, 0x31

    if-lt v0, v3, :cond_2

    const/16 v3, 0x39

    if-gt v0, v3, :cond_2

    .line 134
    add-int/lit8 v3, v0, -0x30

    move v11, v3

    move v3, v2

    move v2, v0

    move v0, v11

    .line 136
    :goto_2
    add-int/lit8 v3, v3, 0x1

    if-ge v3, v7, :cond_1

    .line 137
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 138
    packed-switch v2, :pswitch_data_0

    .line 153
    :pswitch_0
    iput v0, p0, Lflipboard/util/Format;->h:I

    :cond_1
    move v0, v2

    move v2, v3

    .line 154
    :cond_2
    :goto_3
    const/4 v3, 0x0

    iput-boolean v3, p0, Lflipboard/util/Format;->g:Z

    iput-boolean v3, p0, Lflipboard/util/Format;->f:Z

    iput-boolean v3, p0, Lflipboard/util/Format;->e:Z

    iput-boolean v3, p0, Lflipboard/util/Format;->d:Z

    .line 161
    iget v3, p0, Lflipboard/util/Format;->h:I

    if-nez v3, :cond_d

    .line 164
    :goto_4
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 165
    packed-switch v3, :pswitch_data_1

    .line 180
    :cond_3
    :pswitch_1
    invoke-direct {p0, v2, v7}, Lflipboard/util/Format;->a(II)Z

    move-result v0

    if-nez v0, :cond_c

    .line 181
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/util/Format;->h:I

    .line 186
    iget-boolean v0, p0, Lflipboard/util/Format;->g:Z

    if-eqz v0, :cond_e

    .line 187
    array-length v0, p2

    if-lt v4, v0, :cond_5

    .line 188
    new-instance v0, Lflipboard/util/Format$Exception;

    const-string v5, "missing argument for %s at index %d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    add-int/lit8 v10, v2, 0x1

    invoke-virtual {p1, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-direct {v0, v5, v8}, Lflipboard/util/Format$Exception;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v0}, Lflipboard/util/Format;->b(Ljava/lang/RuntimeException;)V

    move v0, v3

    move v3, v4

    .line 212
    :goto_5
    invoke-direct {p0, v2, v7}, Lflipboard/util/Format;->a(II)Z

    move-result v4

    if-nez v4, :cond_c

    .line 213
    const/4 v4, -0x1

    iput v4, p0, Lflipboard/util/Format;->i:I

    .line 218
    const/16 v4, 0x2e

    if-ne v0, v4, :cond_4

    .line 219
    const/4 v4, 0x0

    iput v4, p0, Lflipboard/util/Format;->i:I

    .line 221
    :goto_6
    add-int/lit8 v2, v2, 0x1

    if-ge v2, v7, :cond_4

    .line 222
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 223
    packed-switch v0, :pswitch_data_2

    .line 229
    :cond_4
    invoke-direct {p0, v2, v7}, Lflipboard/util/Format;->a(II)Z

    move-result v4

    if-nez v4, :cond_c

    .line 235
    const/16 v4, 0x25

    if-ne v0, v4, :cond_6

    .line 240
    iget-object v0, p0, Lflipboard/util/Format;->c:Lflipboard/util/Format$CharBuf;

    const/16 v1, 0x25

    invoke-virtual {v0, v1}, Lflipboard/util/Format$CharBuf;->a(C)V

    goto/16 :goto_1

    .line 141
    :pswitch_2
    mul-int/lit8 v0, v0, 0xa

    add-int/lit8 v5, v2, -0x30

    add-int/2addr v0, v5

    .line 142
    goto/16 :goto_2

    .line 148
    :pswitch_3
    add-int/lit8 v1, v3, 0x1

    move v11, v0

    move v0, v2

    move v2, v1

    move v1, v11

    .line 149
    goto :goto_3

    .line 166
    :pswitch_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/util/Format;->d:Z

    .line 173
    :goto_7
    add-int/lit8 v2, v2, 0x1

    if-ge v2, v7, :cond_3

    .line 174
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    goto :goto_4

    .line 167
    :pswitch_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/util/Format;->e:Z

    goto :goto_7

    .line 168
    :pswitch_6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/util/Format;->f:Z

    goto :goto_7

    .line 169
    :pswitch_7
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/util/Format;->g:Z

    goto :goto_7

    .line 191
    :cond_5
    add-int/lit8 v5, v4, 0x1

    aget-object v0, p2, v4

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    iput v0, p0, Lflipboard/util/Format;->h:I

    move v0, v3

    move v3, v5

    goto :goto_5

    .line 199
    :pswitch_8
    iget v3, p0, Lflipboard/util/Format;->h:I

    mul-int/lit8 v3, v3, 0xa

    add-int/lit8 v5, v0, -0x30

    add-int/2addr v3, v5

    iput v3, p0, Lflipboard/util/Format;->h:I

    .line 204
    add-int/lit8 v2, v2, 0x1

    if-ge v2, v7, :cond_d

    .line 205
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 196
    :goto_8
    packed-switch v0, :pswitch_data_3

    move v3, v4

    .line 202
    goto :goto_5

    .line 226
    :pswitch_9
    iget v4, p0, Lflipboard/util/Format;->i:I

    mul-int/lit8 v4, v4, 0xa

    add-int/lit8 v5, v0, -0x30

    add-int/2addr v4, v5

    iput v4, p0, Lflipboard/util/Format;->i:I

    goto :goto_6

    .line 244
    :cond_6
    const/16 v4, 0x74

    if-eq v0, v4, :cond_7

    const/16 v4, 0x54

    if-ne v0, v4, :cond_9

    .line 246
    :cond_7
    iget-object v1, p0, Lflipboard/util/Format;->c:Lflipboard/util/Format$CharBuf;

    const/16 v4, 0x54

    if-ne v0, v4, :cond_8

    const/4 v0, 0x1

    :goto_9
    iget v4, p0, Lflipboard/util/Format;->h:I

    new-instance v5, Lflipboard/util/Format$Exception;

    const-string v6, "dump stack"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-direct {v5, v6, v8}, Lflipboard/util/Format$Exception;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v1, v5, v0, v4}, Lflipboard/util/Format$CharBuf;->a(Ljava/lang/Throwable;ZI)Z

    goto/16 :goto_1

    :cond_8
    const/4 v0, 0x0

    goto :goto_9

    .line 249
    :cond_9
    if-lez v1, :cond_a

    add-int/lit8 v1, v1, -0x1

    move v11, v1

    move v1, v3

    move v3, v11

    .line 250
    :goto_a
    array-length v4, p2

    if-lt v3, v4, :cond_b

    .line 251
    new-instance v0, Lflipboard/util/Format$Exception;

    const-string v3, "missing argument for %s at index %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    add-int/lit8 v8, v2, 0x1

    invoke-virtual {p1, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v0, v3, v4}, Lflipboard/util/Format$Exception;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v0}, Lflipboard/util/Format;->b(Ljava/lang/RuntimeException;)V

    .line 252
    iget-object v0, p0, Lflipboard/util/Format;->c:Lflipboard/util/Format$CharBuf;

    const/16 v3, 0x73

    const-string v4, "???"

    invoke-virtual {v0, v3, v4}, Lflipboard/util/Format$CharBuf;->a(CLjava/lang/Object;)V

    move v3, v1

    goto/16 :goto_1

    .line 249
    :cond_a
    add-int/lit8 v1, v3, 0x1

    goto :goto_a

    .line 254
    :cond_b
    iget-object v4, p0, Lflipboard/util/Format;->c:Lflipboard/util/Format$CharBuf;

    aget-object v3, p2, v3

    invoke-virtual {v4, v0, v3}, Lflipboard/util/Format$CharBuf;->a(CLjava/lang/Object;)V

    move v3, v1

    goto/16 :goto_1

    .line 258
    :cond_c
    return-void

    :cond_d
    move v3, v4

    goto/16 :goto_5

    :cond_e
    move v0, v3

    goto :goto_8

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x24
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 165
    :pswitch_data_1
    .packed-switch 0x2a
        :pswitch_7
        :pswitch_1
        :pswitch_5
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_6
    .end packed-switch

    .line 223
    :pswitch_data_2
    .packed-switch 0x30
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
    .end packed-switch

    .line 196
    :pswitch_data_3
    .packed-switch 0x30
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method static synthetic b()[C
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lflipboard/util/Format;->k:[C

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Lflipboard/util/Format;->b:[Ljava/lang/Object;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lflipboard/util/Format;->c:Lflipboard/util/Format$CharBuf;

    if-nez v0, :cond_0

    new-instance v0, Lflipboard/util/Format$CharBuf;

    iget-object v1, p0, Lflipboard/util/Format;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    mul-int/lit8 v1, v1, 0x3

    invoke-direct {v0, p0, v1}, Lflipboard/util/Format$CharBuf;-><init>(Lflipboard/util/Format;I)V

    iput-object v0, p0, Lflipboard/util/Format;->c:Lflipboard/util/Format$CharBuf;

    iget-object v0, p0, Lflipboard/util/Format;->a:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/util/Format;->b:[Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lflipboard/util/Format;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 293
    :cond_0
    iget-object v0, p0, Lflipboard/util/Format;->c:Lflipboard/util/Format$CharBuf;

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/util/Format;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lflipboard/util/Format;->c:Lflipboard/util/Format$CharBuf;

    invoke-virtual {v0}, Lflipboard/util/Format$CharBuf;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
