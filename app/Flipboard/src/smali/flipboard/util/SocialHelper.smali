.class public Lflipboard/util/SocialHelper;
.super Ljava/lang/Object;
.source "SocialHelper.java"


# static fields
.field public static final a:Lflipboard/util/Log;

.field public static final b:Lflipboard/service/FlipboardManager;

.field static final synthetic c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    const-class v0, Lflipboard/util/SocialHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lflipboard/util/SocialHelper;->c:Z

    .line 87
    const-string v0, "SocialHelper"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/util/SocialHelper;->a:Lflipboard/util/Log;

    .line 88
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sput-object v0, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    return-void

    .line 85
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    return-void
.end method

.method private static a(Landroid/support/v4/app/FragmentActivity;Lflipboard/objs/ConfigService;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 1302
    new-instance v0, Lflipboard/util/SocialHelper$32;

    invoke-direct {v0, p2, p3, p0, p1}, Lflipboard/util/SocialHelper$32;-><init>(Lflipboard/service/Section;Lflipboard/objs/FeedItem;Landroid/support/v4/app/FragmentActivity;Lflipboard/objs/ConfigService;)V

    return-object v0
.end method

.method public static a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;)Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 432
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 433
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 434
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v3, "enable_flip_no_attrib"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 435
    const-string v2, "flipboard.extra.reference"

    iget-object v3, p0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    :cond_0
    const-string v2, "flipboard.extra.reference.type"

    iget-object v3, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const-string v2, "flipboard.extra.reference.item.partner.id"

    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->aa()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    if-eqz p1, :cond_1

    .line 440
    const-string v2, "extra_section_id"

    iget-object v3, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    const-string v2, "flipboard.extra.reference.title"

    invoke-virtual {p1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    :cond_1
    const-string v2, "extra_current_item"

    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->Z()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    iget-object v2, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v3, "status"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 445
    iget-object v2, v0, Lflipboard/objs/FeedItem;->z:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 446
    const-string v2, "flipboard.extra.reference.title"

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->G()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    :cond_2
    iget-object v2, v0, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 449
    const-string v2, "flipboard.extra.reference.author"

    iget-object v3, v0, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    :cond_3
    :goto_0
    iget-object v2, p0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 460
    const-string v0, "flipboard.extra.reference.link"

    iget-object v2, p0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    :cond_4
    :goto_1
    iget-object v0, p0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 465
    const-string v0, "flipboard.extra.reference.service"

    iget-object v2, p0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    :cond_5
    return-object v1

    .line 452
    :cond_6
    iget-object v2, p0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 453
    const-string v2, "flipboard.extra.reference.title"

    iget-object v3, p0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    :cond_7
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 456
    const-string v2, "flipboard.extra.reference.excerpt"

    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 461
    :cond_8
    iget-object v2, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 462
    const-string v2, "flipboard.extra.reference.link"

    iget-object v0, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static a(Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 332
    sget-object v0, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    .line 333
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/objs/ConfigService;->ay:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 334
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d02ec

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/FeedItem;Lflipboard/objs/UsageEventV2$SocialCardNavFrom;)V
    .locals 3

    .prologue
    .line 1450
    sget-object v0, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1476
    :cond_0
    :goto_0
    return-void

    .line 1453
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 1454
    iget-boolean v1, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_0

    .line 1456
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->ag()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->ah()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1457
    :cond_2
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v2

    iget-object v2, v2, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    .line 1464
    :goto_1
    iget-object v1, v1, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    const-string v2, "googlereader"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1468
    new-instance v1, Lflipboard/activities/SocialCardFragment;

    invoke-direct {v1}, Lflipboard/activities/SocialCardFragment;-><init>()V

    .line 1469
    iput-object p2, v1, Lflipboard/activities/SocialCardFragment;->j:Lflipboard/objs/FeedItem;

    iput-object p2, v1, Lflipboard/activities/SocialCardFragment;->k:Lflipboard/objs/FeedItem;

    iput-object p3, v1, Lflipboard/activities/SocialCardFragment;->l:Lflipboard/objs/FeedItem;

    .line 1470
    iput-object p1, v1, Lflipboard/activities/SocialCardFragment;->m:Lflipboard/service/Section;

    .line 1471
    const/4 v2, 0x1

    iput-boolean v2, v1, Lflipboard/activities/SocialCardFragment;->o:Z

    .line 1472
    const v2, 0x7f0e002b

    invoke-virtual {v1, v2}, Lflipboard/activities/SocialCardFragment;->a(I)V

    .line 1473
    iput-object p4, v1, Lflipboard/activities/SocialCardFragment;->z:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    .line 1474
    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "social_card"

    invoke-virtual {v1, v0, v2}, Lflipboard/activities/SocialCardFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 1459
    :cond_3
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "flipboard"

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    goto :goto_1
.end method

.method public static a(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/List;Landroid/os/Bundle;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedSectionLink;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 508
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0, p3}, Lflipboard/util/SocialHelper;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/List;Landroid/view/View$OnClickListener;Landroid/os/Bundle;)V

    .line 509
    return-void
.end method

.method public static a(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/List;Landroid/view/View$OnClickListener;Landroid/os/Bundle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedSectionLink;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 522
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2, p4}, Lflipboard/util/FLTextUtil;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Landroid/os/Bundle;)Landroid/text/SpannableString;

    move-result-object v0

    .line 524
    if-eqz v0, :cond_1

    .line 525
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 526
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 530
    :goto_0
    if-eqz p3, :cond_0

    .line 531
    invoke-virtual {p0, p3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 533
    :cond_0
    return-void

    .line 528
    :cond_1
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/ConfigService;Lflipboard/util/SocialHelper$ServiceLoginObserver;)V
    .locals 5

    .prologue
    .line 780
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 781
    const v1, 0x7f0d01f0

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 782
    const v1, 0x7f0d01ea

    invoke-virtual {p0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 783
    const v1, 0x7f0d004a

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 784
    const v1, 0x7f0d023f

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 785
    new-instance v1, Lflipboard/util/SocialHelper$14;

    invoke-direct {v1, p0, p1, p2}, Lflipboard/util/SocialHelper$14;-><init>(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/ConfigService;Lflipboard/util/SocialHelper$ServiceLoginObserver;)V

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 820
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "login"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 821
    return-void
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/FeedItem;Lflipboard/service/Account;Lflipboard/service/Section;)V
    .locals 7

    .prologue
    .line 1400
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 1401
    sget-object v1, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v2, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    .line 1403
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 1404
    invoke-virtual {v1}, Lflipboard/objs/ConfigService;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lflipboard/util/JavaUtil;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 1405
    invoke-static {p0, v1}, Lflipboard/gui/SocialFormatter;->b(Landroid/content/Context;Lflipboard/objs/ConfigService;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v0, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v6, v6, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v1}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 1406
    const v3, 0x7f0d004a

    invoke-virtual {v2, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 1407
    invoke-static {p0, v1}, Lflipboard/gui/SocialFormatter;->a(Landroid/content/Context;Lflipboard/objs/ConfigService;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lflipboard/gui/dialog/FLAlertDialogFragment;->k:Ljava/lang/String;

    .line 1408
    new-instance v3, Lflipboard/util/SocialHelper$34;

    invoke-direct {v3, v0, p3, p0, v1}, Lflipboard/util/SocialHelper$34;-><init>(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/ConfigService;)V

    iput-object v3, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 1445
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "follow"

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1446
    return-void
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/FeedItem;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/16 v9, 0x258

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 338
    const v0, 0x7f0d00dc

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    iget-object v2, p1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 340
    const/4 v0, 0x0

    .line 341
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->g()Lflipboard/gui/flipping/FlippingBitmap;

    move-result-object v2

    .line 342
    if-eqz v2, :cond_0

    .line 343
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getExternalCacheDir()Ljava/io/File;

    move-result-object v3

    .line 344
    if-eqz v3, :cond_0

    .line 345
    new-instance v0, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/cover.jpg"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 346
    iget-object v3, v2, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-le v3, v9, :cond_1

    .line 347
    iget-object v3, v2, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    iget-object v4, v2, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/lit16 v4, v4, 0x258

    iget-object v5, v2, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/2addr v4, v5

    invoke-static {v3, v9, v4, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 348
    invoke-static {v3, v0}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Landroid/graphics/Bitmap;Ljava/io/File;)V

    .line 349
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 353
    :goto_0
    sget-object v3, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    invoke-virtual {v3, v2}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Lflipboard/gui/flipping/FlippingBitmap;)V

    .line 354
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 357
    :cond_0
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 358
    const v3, 0x7f0d00db

    invoke-virtual {p0, v3}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    .line 359
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    iget-object v5, p1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    aput-object v5, v4, v6

    aput-object p2, v4, v8

    const/4 v5, 0x3

    aput-object p2, v4, v5

    const/4 v5, 0x4

    aput-object p3, v4, v5

    const/4 v5, 0x5

    aput-object p3, v4, v5

    .line 358
    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 362
    const v3, 0x7f0d00da

    invoke-virtual {p0, v3}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    const-string v5, "http://flpbd.it/now"

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 363
    const-string v4, "%s<BR/><BR/><BR/>%s"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v7

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 365
    invoke-static {p0, v1, v2, v0}, Lflipboard/util/SocialHelper;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 366
    return-void

    .line 351
    :cond_1
    iget-object v3, v2, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    invoke-static {v3, v0}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Landroid/graphics/Bitmap;Ljava/io/File;)V

    goto :goto_0
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 4

    .prologue
    .line 638
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 639
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0218

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 682
    :cond_0
    :goto_0
    return-void

    .line 643
    :cond_1
    if-eqz p0, :cond_0

    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_0

    .line 645
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 646
    sget-object v1, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v2, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    .line 649
    sget-object v2, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v3, v1, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v2

    .line 650
    if-nez v2, :cond_2

    .line 651
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 652
    const v3, 0x7f0d01f0

    invoke-virtual {v2, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 653
    invoke-static {p0, v1}, Lflipboard/gui/SocialFormatter;->f(Landroid/content/Context;Lflipboard/objs/ConfigService;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 654
    const v3, 0x7f0d004a

    invoke-virtual {v2, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 655
    const v3, 0x7f0d023f

    invoke-virtual {v2, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 656
    new-instance v3, Lflipboard/util/SocialHelper$9;

    invoke-direct {v3, p0, v1, p1, v0}, Lflipboard/util/SocialHelper$9;-><init>(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/ConfigService;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    iput-object v3, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 677
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "login"

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 679
    :cond_2
    invoke-static {p0, p1, v0}, Lflipboard/util/SocialHelper;->b(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    goto :goto_0
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UsageEventV2$FlipItemNavFrom;)V
    .locals 4

    .prologue
    .line 600
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 634
    :cond_0
    :goto_0
    return-void

    .line 605
    :cond_1
    if-eqz p0, :cond_0

    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_0

    .line 606
    invoke-static {p2, p1}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;)Landroid/os/Bundle;

    move-result-object v0

    .line 607
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 609
    invoke-virtual {v1}, Lflipboard/service/User;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 610
    new-instance v0, Lflipboard/gui/hints/LightBoxFragment;

    invoke-direct {v0}, Lflipboard/gui/hints/LightBoxFragment;-><init>()V

    .line 611
    sget-object v1, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;->a:Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    invoke-virtual {v0, v1}, Lflipboard/gui/hints/LightBoxFragment;->a(Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;)V

    .line 612
    const-string v1, "light_box"

    invoke-virtual {v0, p0, v1}, Lflipboard/gui/hints/LightBoxFragment;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 615
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflipboard/activities/ShareActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 616
    const-string v2, "flipboard.extra.reference"

    const-string v3, "flipboard.extra.reference"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 617
    const-string v2, "flipboard.extra.reference.author"

    const-string v3, "flipboard.extra.reference.author"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 618
    const-string v2, "flipboard.extra.reference.link"

    const-string v3, "flipboard.extra.reference.link"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 619
    const-string v2, "flipboard.extra.reference.service"

    const-string v3, "flipboard.extra.reference.service"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 620
    const-string v2, "flipboard.extra.reference.title"

    const-string v3, "flipboard.extra.reference.title"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 621
    const-string v0, "flipboard.extra.navigating.from"

    invoke-virtual {p3}, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 622
    if-eqz p1, :cond_3

    .line 623
    const-string v0, "extra_section_id"

    iget-object v2, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 627
    :cond_3
    const-string v0, "flipboard.extra.magazine.share.item.section.id"

    iget-object v2, p2, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 628
    const-string v0, "flipboard.extra.magazine.share.item.partner.id"

    iget-object v2, p2, Lflipboard/objs/FeedItem;->bE:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 630
    invoke-virtual {p0, v1}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    .line 631
    const v0, 0x7f040001

    const v1, 0x7f040004

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/FlipboardActivity;->overridePendingTransition(II)V

    goto/16 :goto_0
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedSectionLink;Lflipboard/objs/FeedItem;Lflipboard/gui/FLPopoverWindow;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1210
    sget-boolean v0, Lflipboard/util/SocialHelper;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p2, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1211
    :cond_0
    if-eqz p3, :cond_5

    .line 1212
    :goto_0
    sget-object v0, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v3, p3, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lflipboard/objs/FeedSectionLink;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v6, 0x7f0d02f3

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lflipboard/util/SocialHelper$29;

    invoke-direct {v0, p4, p1, p2, p0}, Lflipboard/util/SocialHelper$29;-><init>(Lflipboard/gui/FLPopoverWindow;Lflipboard/service/Section;Lflipboard/objs/FeedSectionLink;Lflipboard/activities/FlipboardActivity;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v6, p3, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v6

    iget-object v0, v3, Lflipboard/objs/ConfigService;->Q:Ljava/lang/String;

    if-eqz v0, :cond_6

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    if-eqz v6, :cond_2

    invoke-virtual {v3}, Lflipboard/objs/ConfigService;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/JavaUtil;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lflipboard/util/SocialHelper$30;

    invoke-direct {v0, p0, p3, v6, p1}, Lflipboard/util/SocialHelper$30;-><init>(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/FeedItem;Lflipboard/service/Account;Lflipboard/service/Section;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {p1}, Lflipboard/service/Section;->t()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v6, 0x7f0d017b

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v6, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v0, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p0, v3, p1, p3}, Lflipboard/util/SocialHelper;->a(Landroid/support/v4/app/FragmentActivity;Lflipboard/objs/ConfigService;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {p1}, Lflipboard/service/Section;->t()Z

    move-result v0

    if-eqz v0, :cond_7

    const v0, 0x7f0d017a

    :goto_2
    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v0, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p0, v3, p1, p3}, Lflipboard/util/SocialHelper;->a(Landroid/support/v4/app/FragmentActivity;Lflipboard/objs/ConfigService;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p3, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p3, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v3, "flipboard"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_3
    iget-object v3, p3, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v3, v1

    :goto_4
    and-int/2addr v0, v3

    if-eqz v0, :cond_a

    iget-object v0, p3, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v3, v3, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    :goto_5
    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d012d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lflipboard/util/SocialHelper$33;

    invoke-direct {v0, p0, p3, p1}, Lflipboard/util/SocialHelper$33;-><init>(Landroid/support/v4/app/FragmentActivity;Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    new-array v0, v2, [Ljava/lang/CharSequence;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    iput-object v0, v1, Lflipboard/gui/dialog/FLAlertDialogFragment;->q:[Ljava/lang/CharSequence;

    new-instance v0, Lflipboard/util/SocialHelper$31;

    invoke-direct {v0, v5}, Lflipboard/util/SocialHelper$31;-><init>(Ljava/util/List;)V

    iput-object v0, v1, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "choose"

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1213
    return-void

    .line 1211
    :cond_5
    new-instance p3, Lflipboard/objs/FeedItem;

    invoke-direct {p3, p2}, Lflipboard/objs/FeedItem;-><init>(Lflipboard/objs/FeedSectionLink;)V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 1212
    goto/16 :goto_1

    :cond_7
    const v0, 0x7f0d0179

    goto/16 :goto_2

    :cond_8
    move v0, v2

    goto :goto_3

    :cond_9
    move v3, v2

    goto :goto_4

    :cond_a
    move v1, v2

    goto :goto_5
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 369
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 370
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 373
    const-string v1, "message/rfc822"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 375
    if-eqz p1, :cond_0

    .line 376
    const-string v1, "android.intent.extra.SUBJECT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 379
    :cond_0
    if-eqz p3, :cond_1

    .line 380
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 384
    :cond_1
    if-eqz p2, :cond_2

    .line 385
    const-string v1, "android.intent.extra.TEXT"

    invoke-static {p2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 388
    :cond_2
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d01d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    .line 389
    return-void
.end method

.method static synthetic a(Lflipboard/objs/ConfigService;Lflipboard/objs/FeedItem;ZLflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V
    .locals 0

    .prologue
    .line 85
    invoke-static {p0, p1, p2, p3, p4}, Lflipboard/util/SocialHelper;->c(Lflipboard/objs/ConfigService;Lflipboard/objs/FeedItem;ZLflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V

    return-void
.end method

.method public static a(Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 481
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v1

    .line 483
    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->C()Lflipboard/objs/FeedSectionLink;

    move-result-object v0

    if-nez v0, :cond_2

    .line 487
    iget-object v0, v1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v2, "status"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 488
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 489
    iget-object v3, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-nez v3, :cond_0

    .line 490
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    goto :goto_0

    .line 495
    :cond_1
    new-instance v0, Lflipboard/service/Section;

    invoke-direct {v0, p0, v1}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedItem;Lflipboard/objs/FeedItem;)V

    .line 496
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 497
    invoke-virtual {v0, p1, p2}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 503
    :goto_1
    invoke-virtual {p1, v0}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    .line 504
    return-void

    .line 499
    :cond_2
    new-instance v0, Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->C()Lflipboard/objs/FeedSectionLink;

    move-result-object v1

    invoke-direct {v0, v1}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    .line 500
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 501
    invoke-virtual {v0, p1, p2}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V
    .locals 7

    .prologue
    const v6, 0x7f0d023f

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 98
    iget-boolean v0, p0, Lflipboard/objs/FeedItem;->af:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 99
    :goto_0
    sget-object v3, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->ai()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v4

    .line 101
    sget-object v3, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v3}, Lflipboard/io/NetworkManager;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 105
    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {p0, v1}, Lflipboard/objs/FeedItem;->a(Z)V

    .line 106
    invoke-virtual {p1}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0218

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 188
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 98
    goto :goto_0

    :cond_1
    move v1, v2

    .line 105
    goto :goto_1

    .line 110
    :cond_2
    if-nez v0, :cond_5

    invoke-virtual {p0, v4}, Lflipboard/objs/FeedItem;->a(Lflipboard/objs/ConfigService;)Z

    move-result v3

    if-nez v3, :cond_3

    move v3, v2

    :goto_3
    if-nez v3, :cond_5

    .line 112
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 113
    const v3, 0x7f0d0336

    invoke-virtual {v0, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 114
    const v3, 0x7f0d0337

    invoke-virtual {p1, v3}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v2

    invoke-static {v3, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 115
    invoke-virtual {v0, v6}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 116
    iget-object v2, p1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v3, "error_like"

    invoke-virtual {v0, v2, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 118
    invoke-virtual {p0, v1}, Lflipboard/objs/FeedItem;->a(Z)V

    goto :goto_2

    .line 110
    :cond_3
    iget-boolean v3, v4, Lflipboard/objs/ConfigService;->Z:Z

    if-nez v3, :cond_4

    move v3, v2

    goto :goto_3

    :cond_4
    iget-boolean v3, p0, Lflipboard/objs/FeedItem;->ae:Z

    goto :goto_3

    .line 123
    :cond_5
    sget-object v1, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v3, v4, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    .line 124
    if-nez v1, :cond_7

    .line 128
    if-nez v0, :cond_6

    .line 129
    invoke-virtual {p0, v2}, Lflipboard/objs/FeedItem;->a(Z)V

    goto :goto_2

    .line 133
    :cond_6
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 134
    const v1, 0x7f0d01f0

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 135
    invoke-static {p1, v4}, Lflipboard/gui/SocialFormatter;->g(Landroid/content/Context;Lflipboard/objs/ConfigService;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 136
    const v1, 0x7f0d004a

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 137
    invoke-virtual {v0, v6}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 138
    new-instance v1, Lflipboard/util/SocialHelper$1;

    invoke-direct {v1, p0, v4, p1, p2}, Lflipboard/util/SocialHelper$1;-><init>(Lflipboard/objs/FeedItem;Lflipboard/objs/ConfigService;Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 183
    iget-object v1, p1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "login"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 186
    :cond_7
    invoke-static {v4, p0, v0, p1, p2}, Lflipboard/util/SocialHelper;->c(Lflipboard/objs/ConfigService;Lflipboard/objs/FeedItem;ZLflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V

    goto/16 :goto_2
.end method

.method public static a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;)V
    .locals 3

    .prologue
    .line 393
    if-eqz p2, :cond_0

    iget-boolean v0, p2, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_0

    .line 394
    new-instance v0, Lflipboard/gui/dialog/ShareDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/ShareDialogFragment;-><init>()V

    .line 395
    invoke-static {p0, p1}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/ShareDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 396
    iget-object v1, p2, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "share"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/ShareDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 398
    :cond_0
    return-void
.end method

.method public static a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/UsageEventV2$SocialCardNavFrom;)V
    .locals 3

    .prologue
    .line 472
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/SocialCardActivity;

    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 473
    const-string v1, "sid"

    invoke-virtual {p1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 474
    const-string v1, "extra_current_item"

    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->Z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 475
    const-string v1, "extraNavFrom"

    invoke-virtual {p3}, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 476
    const/16 v1, 0x65

    invoke-virtual {p2, v0, v1}, Lflipboard/activities/FlipboardActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 477
    return-void
.end method

.method public static a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 282
    iget-boolean v0, p2, Lflipboard/activities/FlipboardActivity;->P:Z

    if-nez v0, :cond_1

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 286
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 287
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 289
    iget-boolean v2, p0, Lflipboard/objs/FeedItem;->aj:Z

    if-eqz v2, :cond_3

    .line 291
    sget-object v2, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {}, Lflipboard/service/User;->u()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 292
    invoke-static {p0, p2}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 293
    new-instance v2, Lflipboard/util/SocialHelper$5;

    invoke-direct {v2, p0, p2, p1}, Lflipboard/util/SocialHelper$5;-><init>(Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 302
    :cond_2
    invoke-virtual {p2}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d02ee

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 303
    new-instance v2, Lflipboard/util/SocialHelper$6;

    invoke-direct {v2, p0, p1, p2}, Lflipboard/util/SocialHelper$6;-><init>(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 312
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 313
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 314
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/CharSequence;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    iput-object v0, v2, Lflipboard/gui/dialog/FLAlertDialogFragment;->q:[Ljava/lang/CharSequence;

    .line 315
    new-instance v0, Lflipboard/util/SocialHelper$7;

    invoke-direct {v0, v1}, Lflipboard/util/SocialHelper$7;-><init>(Ljava/util/List;)V

    iput-object v0, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 323
    if-eqz p3, :cond_4

    .line 324
    iput-object p3, v2, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 326
    :cond_4
    iget-object v0, p2, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "choose"

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lflipboard/service/Section;Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 590
    sget-object v0, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    if-nez v0, :cond_0

    .line 591
    sget-object v0, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p0}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 594
    :cond_0
    invoke-virtual {p0, p1, p2}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 595
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 596
    return-void
.end method

.method public static a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;)V
    .locals 5

    .prologue
    .line 1166
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->l()Ljava/lang/String;

    move-result-object v0

    .line 1167
    if-eqz v0, :cond_0

    .line 1168
    iget-object v1, p1, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    if-eqz v1, :cond_0

    sget-object v1, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v2

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v4, Lflipboard/util/SocialHelper$28;

    invoke-direct {v4, p2, v1, v0, p1}, Lflipboard/util/SocialHelper$28;-><init>(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Lflipboard/objs/FeedItem;)V

    new-instance v1, Lflipboard/service/Flap$SaveRequest;

    invoke-direct {v1, v2, v3}, Lflipboard/service/Flap$SaveRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v1, v0, p0, p1, v4}, Lflipboard/service/Flap$SaveRequest;->a(Ljava/lang/String;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$SaveRequest;

    .line 1170
    :cond_0
    return-void
.end method

.method public static a(Lflipboard/objs/FeedItem;)Z
    .locals 2

    .prologue
    .line 762
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 763
    iget-object v0, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "status"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/List;Landroid/os/Bundle;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedSectionLink;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 537
    const/4 v1, 0x0

    .line 538
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 540
    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 541
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 542
    iget-object v4, v0, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 545
    iget-object v4, v0, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 546
    if-ltz v4, :cond_0

    .line 547
    if-nez v1, :cond_1

    .line 548
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 550
    :cond_1
    new-instance v5, Lflipboard/util/SocialHelper$8;

    invoke-direct {v5, v0, p3, v2}, Lflipboard/util/SocialHelper$8;-><init>(Lflipboard/objs/FeedSectionLink;Landroid/os/Bundle;Landroid/content/Context;)V

    iget-object v0, v0, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    .line 573
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v4

    const/4 v6, 0x0

    .line 550
    invoke-virtual {v1, v5, v4, v0, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 579
    :cond_2
    if-eqz v1, :cond_3

    .line 580
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 581
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 586
    :goto_1
    return-void

    .line 583
    :cond_3
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public static b(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 686
    sget-object v0, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    .line 688
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 689
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 690
    new-instance v3, Lflipboard/util/SocialHelper$10;

    invoke-direct {v3, p2, v0, p0}, Lflipboard/util/SocialHelper$10;-><init>(Lflipboard/objs/FeedItem;Lflipboard/objs/ConfigService;Lflipboard/activities/FlipboardActivity;)V

    .line 716
    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v5, v0, Lflipboard/objs/ConfigService;->ao:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 717
    if-eqz v4, :cond_0

    .line 718
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 719
    new-instance v4, Lflipboard/util/SocialHelper$11;

    invoke-direct {v4, p1, p2, v3}, Lflipboard/util/SocialHelper$11;-><init>(Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/service/Flap$JSONResultObserver;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 730
    :cond_0
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/objs/ConfigService;->ap:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 731
    if-eqz v0, :cond_1

    .line 732
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 733
    new-instance v0, Lflipboard/util/SocialHelper$12;

    invoke-direct {v0, p2, p0, p1}, Lflipboard/util/SocialHelper$12;-><init>(Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 743
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v7, :cond_3

    .line 744
    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v6}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    .line 758
    :cond_2
    :goto_0
    return-void

    .line 745
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v7, :cond_2

    .line 746
    new-instance v3, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 747
    new-array v0, v6, [Ljava/lang/CharSequence;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    iput-object v0, v3, Lflipboard/gui/dialog/FLAlertDialogFragment;->q:[Ljava/lang/CharSequence;

    .line 748
    new-instance v0, Lflipboard/util/SocialHelper$13;

    invoke-direct {v0, v2}, Lflipboard/util/SocialHelper$13;-><init>(Ljava/util/List;)V

    iput-object v0, v3, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 756
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "choose"

    invoke-virtual {v3, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UsageEventV2$FlipItemNavFrom;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 825
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_0

    iget-object v0, p2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "sectionCover"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "synthetic-client-profile-page"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1162
    :cond_0
    :goto_0
    return-void

    .line 829
    :cond_1
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 830
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 831
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 834
    iget-boolean v2, p2, Lflipboard/objs/FeedItem;->aj:Z

    if-eqz v2, :cond_3

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v2

    if-nez v2, :cond_3

    .line 837
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0137

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 838
    new-instance v2, Lflipboard/util/SocialHelper$15;

    invoke-direct {v2, p0, p1, p2, p3}, Lflipboard/util/SocialHelper$15;-><init>(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UsageEventV2$FlipItemNavFrom;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 847
    sget-object v2, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {}, Lflipboard/service/User;->u()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 849
    invoke-static {p2, p0}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 850
    new-instance v2, Lflipboard/util/SocialHelper$16;

    invoke-direct {v2, p2, p0, p1}, Lflipboard/util/SocialHelper$16;-><init>(Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 861
    :cond_2
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d02ee

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 862
    new-instance v2, Lflipboard/util/SocialHelper$17;

    invoke-direct {v2, p2, p1}, Lflipboard/util/SocialHelper$17;-><init>(Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 874
    :cond_3
    sget-object v2, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {p1, v2}, Lflipboard/service/Section;->a(Lflipboard/service/User;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    if-eqz v2, :cond_4

    iget-object v2, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->K:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 877
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 878
    new-instance v2, Lflipboard/util/SocialHelper$18;

    invoke-direct {v2, p0, p1, p2}, Lflipboard/util/SocialHelper$18;-><init>(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 917
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0017

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 918
    new-instance v2, Lflipboard/util/SocialHelper$19;

    invoke-direct {v2, p0, p1, p2}, Lflipboard/util/SocialHelper$19;-><init>(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 952
    :cond_4
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->ak()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 953
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0288

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 954
    new-instance v2, Lflipboard/util/SocialHelper$20;

    invoke-direct {v2, p0, p2, p1}, Lflipboard/util/SocialHelper$20;-><init>(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 964
    :cond_5
    sget-object v2, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {}, Lflipboard/service/User;->u()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 967
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0261

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 968
    new-instance v2, Lflipboard/util/SocialHelper$21;

    invoke-direct {v2, p0, p1, p2}, Lflipboard/util/SocialHelper$21;-><init>(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1006
    :cond_6
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0345

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1007
    new-instance v2, Lflipboard/util/SocialHelper$22;

    invoke-direct {v2, p2, p0}, Lflipboard/util/SocialHelper$22;-><init>(Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1042
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d012d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1043
    new-instance v2, Lflipboard/util/SocialHelper$23;

    invoke-direct {v2, p0, p2, p1}, Lflipboard/util/SocialHelper$23;-><init>(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1090
    sget-object v2, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-boolean v2, v2, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v2, :cond_7

    sget-object v2, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-boolean v2, v2, Lflipboard/service/FlipboardManager;->ah:Z

    if-eqz v2, :cond_7

    .line 1091
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0275

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1092
    new-instance v2, Lflipboard/util/SocialHelper$24;

    invoke-direct {v2, p0, p1, p2}, Lflipboard/util/SocialHelper$24;-><init>(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1103
    :cond_7
    sget-object v2, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-boolean v2, v2, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v2, :cond_8

    .line 1104
    const-string v2, "Debug"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1105
    new-instance v2, Lflipboard/util/SocialHelper$25;

    invoke-direct {v2, p0, p1, p2}, Lflipboard/util/SocialHelper$25;-><init>(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1122
    :cond_8
    iget-boolean v2, p2, Lflipboard/objs/FeedItem;->aQ:Z

    if-eqz v2, :cond_9

    iget-boolean v2, p2, Lflipboard/objs/FeedItem;->aM:Z

    if-eqz v2, :cond_9

    iget-boolean v2, p2, Lflipboard/objs/FeedItem;->be:Z

    if-eqz v2, :cond_9

    .line 1123
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0206

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1124
    new-instance v2, Lflipboard/util/SocialHelper$26;

    invoke-direct {v2, p2, p1, p0}, Lflipboard/util/SocialHelper$26;-><init>(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1151
    :cond_9
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 1152
    new-array v3, v4, [Ljava/lang/CharSequence;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    iput-object v0, v2, Lflipboard/gui/dialog/FLAlertDialogFragment;->q:[Ljava/lang/CharSequence;

    .line 1153
    new-instance v0, Lflipboard/util/SocialHelper$27;

    invoke-direct {v0, v1}, Lflipboard/util/SocialHelper$27;-><init>(Ljava/util/List;)V

    iput-object v0, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 1161
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "choose"

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic b(Lflipboard/objs/ConfigService;Lflipboard/objs/FeedItem;ZLflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V
    .locals 0

    .prologue
    .line 85
    invoke-static {p0, p1, p2, p3, p4}, Lflipboard/util/SocialHelper;->d(Lflipboard/objs/ConfigService;Lflipboard/objs/FeedItem;ZLflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V

    return-void
.end method

.method public static b(Lflipboard/objs/FeedItem;)V
    .locals 3

    .prologue
    .line 769
    sget-object v0, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v2, Lflipboard/service/User$35;

    invoke-direct {v2, v0, v1}, Lflipboard/service/User$35;-><init>(Lflipboard/service/User;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lflipboard/service/User;->a(Lflipboard/service/User$StateChanger;)V

    .line 770
    :cond_0
    return-void
.end method

.method public static b(Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V
    .locals 4

    .prologue
    .line 417
    invoke-static {p0, p2}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;)Landroid/os/Bundle;

    move-result-object v0

    .line 418
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflipboard/activities/ComposeActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 419
    const-string v2, "flipboard.extra.reference"

    const-string v3, "flipboard.extra.reference"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 420
    const-string v2, "flipboard.extra.reference.author"

    const-string v3, "flipboard.extra.reference.author"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 421
    const-string v2, "flipboard.extra.reference.link"

    const-string v3, "flipboard.extra.reference.link"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 422
    const-string v2, "flipboard.extra.reference.service"

    const-string v3, "flipboard.extra.reference.service"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 423
    const-string v2, "flipboard.extra.reference.title"

    const-string v3, "flipboard.extra.reference.title"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 424
    if-eqz p2, :cond_0

    .line 425
    const-string v0, "extra_section_id"

    iget-object v2, p2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 427
    :cond_0
    invoke-virtual {p1, v1}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    .line 428
    return-void
.end method

.method public static b(Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V
    .locals 1

    .prologue
    .line 775
    sget-object v0, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {p0, p1}, Lflipboard/service/User;->b(Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V

    .line 776
    return-void
.end method

.method private static c(Lflipboard/objs/ConfigService;Lflipboard/objs/FeedItem;ZLflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V
    .locals 8

    .prologue
    .line 193
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 194
    const-string v1, "facebook"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v2

    .line 195
    const-class v1, Lflipboard/settings/Facebook;

    invoke-static {v1}, Lflipboard/settings/Settings;->getPrefsFor(Ljava/lang/Class;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 196
    const-string v3, "implicit_share_facebook"

    invoke-interface {v1, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v4, "flipboard"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lflipboard/service/User;->b()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v2, :cond_0

    iget-object v0, p4, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->L:Ljava/lang/String;

    const-string v2, "public"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    new-instance v7, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v7}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 198
    const v0, 0x7f0d00e0

    invoke-virtual {v7, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 199
    const v0, 0x7f0d00df

    invoke-virtual {v7, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 200
    const v0, 0x7f0d0357

    invoke-virtual {v7, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 201
    const v0, 0x7f0d021e

    invoke-virtual {v7, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 202
    new-instance v0, Lflipboard/util/SocialHelper$2;

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lflipboard/util/SocialHelper$2;-><init>(Landroid/content/SharedPreferences;Lflipboard/objs/ConfigService;Lflipboard/objs/FeedItem;ZLflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V

    iput-object v0, v7, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 223
    iget-object v0, p3, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "implicit_share_facebook"

    invoke-virtual {v7, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 228
    :goto_0
    return-void

    .line 226
    :cond_0
    invoke-static {p0, p1, p2, p3, p4}, Lflipboard/util/SocialHelper;->d(Lflipboard/objs/ConfigService;Lflipboard/objs/FeedItem;ZLflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V

    goto :goto_0
.end method

.method private static d(Lflipboard/objs/ConfigService;Lflipboard/objs/FeedItem;ZLflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V
    .locals 6

    .prologue
    .line 236
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/objs/ConfigService;->aq:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 237
    if-eqz p2, :cond_0

    if-eqz v0, :cond_0

    .line 238
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/util/SocialHelper$3;

    invoke-direct {v2, p3, v0}, Lflipboard/util/SocialHelper$3;-><init>(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 255
    :cond_0
    new-instance v0, Lflipboard/util/SocialHelper$4;

    invoke-direct {v0, p2, p3, p0}, Lflipboard/util/SocialHelper$4;-><init>(ZLflipboard/activities/FlipboardActivity;Lflipboard/objs/ConfigService;)V

    .line 277
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sget-boolean v0, Lflipboard/objs/FeedItem;->cs:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eq p1, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {p1, p2}, Lflipboard/objs/FeedItem;->a(Z)V

    new-instance v2, Lflipboard/objs/FeedItem$5;

    invoke-direct {v2, p1, v1, p4, p2}, Lflipboard/objs/FeedItem$5;-><init>(Lflipboard/objs/FeedItem;Ljava/lang/ref/WeakReference;Lflipboard/service/Section;Z)V

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v0, "facebook"

    invoke-virtual {v1, v0}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v3

    const/4 v0, 0x0

    sget-boolean v4, Lflipboard/settings/Facebook;->implicit_share_facebook:Z

    if-eqz v4, :cond_2

    iget-object v4, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v5, "flipboard"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Lflipboard/service/User;->b()Z

    move-result v4

    if-nez v4, :cond_2

    if-eqz v3, :cond_2

    iget-object v3, p4, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->L:Ljava/lang/String;

    const-string v4, "public"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v0, Landroid/os/Bundle;

    const/4 v3, 0x1

    invoke-direct {v0, v3}, Landroid/os/Bundle;-><init>(I)V

    const-string v3, "implicitShare"

    const-string v4, "facebook"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v3}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v3

    new-instance v4, Lflipboard/service/Flap$LikeUnlikeRequest;

    invoke-direct {v4, v3, v1, p4, p1}, Lflipboard/service/Flap$LikeUnlikeRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    invoke-virtual {v4, p2, v0, v2}, Lflipboard/service/Flap$LikeUnlikeRequest;->a(ZLandroid/os/Bundle;Lflipboard/service/ServiceReloginObserver;)Lflipboard/service/Flap$LikeUnlikeRequest;

    .line 278
    return-void
.end method
