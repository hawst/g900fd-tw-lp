.class final Lflipboard/util/SocialHelper$19;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SocialHelper.java"


# instance fields
.field final synthetic a:Lflipboard/activities/FlipboardActivity;

.field final synthetic b:Lflipboard/service/Section;

.field final synthetic c:Lflipboard/objs/FeedItem;


# direct methods
.method constructor <init>(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 919
    iput-object p1, p0, Lflipboard/util/SocialHelper$19;->a:Lflipboard/activities/FlipboardActivity;

    iput-object p2, p0, Lflipboard/util/SocialHelper$19;->b:Lflipboard/service/Section;

    iput-object p3, p0, Lflipboard/util/SocialHelper$19;->c:Lflipboard/objs/FeedItem;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;I)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 922
    iget-object v0, p0, Lflipboard/util/SocialHelper$19;->a:Lflipboard/activities/FlipboardActivity;

    iget-object v0, p0, Lflipboard/util/SocialHelper$19;->b:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/util/SocialHelper$19;->c:Lflipboard/objs/FeedItem;

    new-instance v2, Lflipboard/util/SocialHelper$19$1;

    invoke-direct {v2, p0}, Lflipboard/util/SocialHelper$19$1;-><init>(Lflipboard/util/SocialHelper$19;)V

    invoke-static {v0, v1, v2}, Lflipboard/util/ShareHelper;->b(Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/service/Flap$JSONResultObserver;)V

    .line 937
    iget-object v0, p0, Lflipboard/util/SocialHelper$19;->b:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/util/SocialHelper$19;->c:Lflipboard/objs/FeedItem;

    iget-object v2, v0, Lflipboard/service/Section;->t:Lflipboard/objs/FeedItem;

    iput-object v1, v2, Lflipboard/objs/FeedItem;->bF:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->N:Lflipboard/objs/Image;

    iput-object v1, v2, Lflipboard/objs/FeedItem;->N:Lflipboard/objs/Image;

    invoke-virtual {v0, v2}, Lflipboard/service/Section;->b(Lflipboard/objs/FeedItem;)V

    invoke-virtual {v0, v4}, Lflipboard/service/Section;->e(Z)V

    invoke-virtual {v0}, Lflipboard/service/Section;->u()V

    invoke-virtual {v0, v3}, Lflipboard/service/Section;->a(Ljava/lang/Runnable;)V

    sget-object v1, Lflipboard/service/Section$Message;->b:Lflipboard/service/Section$Message;

    invoke-virtual {v0, v1, v3}, Lflipboard/service/Section;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    new-instance v1, Lflipboard/json/FLObject;

    invoke-direct {v1}, Lflipboard/json/FLObject;-><init>()V

    const-string v2, "changes"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "refresh"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "EOF"

    invoke-virtual {v0}, Lflipboard/service/Section;->k()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "strategy"

    iget-object v3, v0, Lflipboard/service/Section;->E:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lflipboard/json/FLObject;->b(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    sget-object v2, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    invoke-virtual {v0, v2, v1}, Lflipboard/service/Section;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 939
    iget-object v0, p0, Lflipboard/util/SocialHelper$19;->a:Lflipboard/activities/FlipboardActivity;

    instance-of v0, v0, Lflipboard/activities/FeedActivity;

    if-eqz v0, :cond_0

    .line 940
    sget-object v0, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/util/SocialHelper$19$2;

    invoke-direct {v1, p0}, Lflipboard/util/SocialHelper$19$2;-><init>(Lflipboard/util/SocialHelper$19;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 948
    :cond_0
    return-void
.end method
