.class public abstract Lflipboard/util/FLAnimation;
.super Ljava/lang/Object;
.source "FLAnimation.java"


# static fields
.field static final a:Landroid/graphics/Rect;

.field static b:Lflipboard/util/FLAnimation$AnimatorThread;


# instance fields
.field final c:I

.field d:J

.field private e:Z

.field private f:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lflipboard/util/FLAnimation;->a:Landroid/graphics/Rect;

    .line 26
    new-instance v0, Lflipboard/util/FLAnimation$AnimatorThread;

    invoke-direct {v0}, Lflipboard/util/FLAnimation$AnimatorThread;-><init>()V

    sput-object v0, Lflipboard/util/FLAnimation;->b:Lflipboard/util/FLAnimation$AnimatorThread;

    return-void
.end method

.method constructor <init>(Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lflipboard/util/FLAnimation;->f:Ljava/lang/Object;

    .line 41
    iput p2, p0, Lflipboard/util/FLAnimation;->c:I

    .line 42
    return-void
.end method

.method static synthetic a(Lflipboard/util/FLAnimation;J)J
    .locals 1

    .prologue
    .line 18
    iput-wide p1, p0, Lflipboard/util/FLAnimation;->d:J

    return-wide p1
.end method

.method static synthetic a(Lflipboard/util/FLAnimation;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lflipboard/util/FLAnimation;->f:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lflipboard/util/FLAnimation;)Z
    .locals 1

    .prologue
    .line 18
    iget-boolean v0, p0, Lflipboard/util/FLAnimation;->e:Z

    return v0
.end method

.method static synthetic c(Lflipboard/util/FLAnimation;)Z
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/util/FLAnimation;->e:Z

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lflipboard/util/FLAnimation;->b()V

    .line 47
    sget-object v0, Lflipboard/util/FLAnimation;->b:Lflipboard/util/FLAnimation$AnimatorThread;

    invoke-virtual {v0, p0}, Lflipboard/util/FLAnimation$AnimatorThread;->b(Lflipboard/util/FLAnimation;)V

    .line 48
    return-void
.end method

.method abstract a(F)V
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lflipboard/util/FLAnimation;->b:Lflipboard/util/FLAnimation$AnimatorThread;

    invoke-virtual {v0, p0}, Lflipboard/util/FLAnimation$AnimatorThread;->a(Lflipboard/util/FLAnimation;)V

    .line 53
    return-void
.end method
