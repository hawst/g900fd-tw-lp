.class public abstract Lflipboard/util/ProcrastinatingTimerTask;
.super Ljava/lang/Object;
.source "ProcrastinatingTimerTask.java"


# static fields
.field private static a:Ljava/util/Timer;


# instance fields
.field private final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/util/TimerTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    sput-object v0, Lflipboard/util/ProcrastinatingTimerTask;->a:Ljava/util/Timer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lflipboard/util/ProcrastinatingTimerTask;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 30
    return-void
.end method

.method static synthetic a(Lflipboard/util/ProcrastinatingTimerTask;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lflipboard/util/ProcrastinatingTimerTask;->b:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final declared-synchronized b()V
    .locals 4

    .prologue
    .line 34
    monitor-enter p0

    :try_start_0
    new-instance v1, Lflipboard/util/ProcrastinatingTimerTask$1;

    invoke-direct {v1, p0}, Lflipboard/util/ProcrastinatingTimerTask$1;-><init>(Lflipboard/util/ProcrastinatingTimerTask;)V

    .line 42
    iget-object v0, p0, Lflipboard/util/ProcrastinatingTimerTask;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimerTask;

    .line 43
    iget-object v2, p0, Lflipboard/util/ProcrastinatingTimerTask;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 47
    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "Timer task ran even though we canceled...argh"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    :cond_0
    sget-object v0, Lflipboard/util/ProcrastinatingTimerTask;->a:Ljava/util/Timer;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    :goto_0
    monitor-exit p0

    return-void

    .line 58
    :cond_1
    :try_start_1
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v1, v0, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
