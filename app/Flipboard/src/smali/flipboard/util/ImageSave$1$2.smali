.class Lflipboard/util/ImageSave$1$2;
.super Ljava/lang/Object;
.source "ImageSave.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/io/Download$Observer;

.field final synthetic b:Lflipboard/util/ImageSave$1;


# direct methods
.method constructor <init>(Lflipboard/util/ImageSave$1;Lflipboard/io/Download$Observer;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lflipboard/util/ImageSave$1$2;->b:Lflipboard/util/ImageSave$1;

    iput-object p2, p0, Lflipboard/util/ImageSave$1$2;->a:Lflipboard/io/Download$Observer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 143
    iget-object v0, p0, Lflipboard/util/ImageSave$1$2;->b:Lflipboard/util/ImageSave$1;

    iget-object v0, v0, Lflipboard/util/ImageSave$1;->d:Lflipboard/activities/FlipboardActivity;

    iget-boolean v0, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lflipboard/util/ImageSave$1$2;->b:Lflipboard/util/ImageSave$1;

    iget-object v0, v0, Lflipboard/util/ImageSave$1;->d:Lflipboard/activities/FlipboardActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "saving_image"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lflipboard/gui/dialog/FLDialogFragment;

    .line 145
    if-eqz v0, :cond_0

    .line 146
    invoke-virtual {v0}, Lflipboard/gui/dialog/FLDialogFragment;->a()V

    .line 148
    :cond_0
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 149
    const v1, 0x7f0d0285

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 150
    const/4 v1, 0x0

    iput-boolean v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->C:Z

    .line 151
    const v1, 0x7f0d0280

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 152
    const v1, 0x7f0d004a

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 153
    new-instance v1, Lflipboard/util/ImageSave$1$2$1;

    invoke-direct {v1, p0}, Lflipboard/util/ImageSave$1$2$1;-><init>(Lflipboard/util/ImageSave$1$2;)V

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 179
    iget-object v1, p0, Lflipboard/util/ImageSave$1$2;->b:Lflipboard/util/ImageSave$1;

    iget-object v1, v1, Lflipboard/util/ImageSave$1;->d:Lflipboard/activities/FlipboardActivity;

    iget-object v1, v1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "save_failed"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 181
    :cond_1
    return-void
.end method
