.class final Lflipboard/util/SocialHelper$14;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SocialHelper.java"


# instance fields
.field final synthetic a:Lflipboard/activities/FlipboardActivity;

.field final synthetic b:Lflipboard/objs/ConfigService;

.field final synthetic c:Lflipboard/util/SocialHelper$ServiceLoginObserver;


# direct methods
.method constructor <init>(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/ConfigService;Lflipboard/util/SocialHelper$ServiceLoginObserver;)V
    .locals 0

    .prologue
    .line 786
    iput-object p1, p0, Lflipboard/util/SocialHelper$14;->a:Lflipboard/activities/FlipboardActivity;

    iput-object p2, p0, Lflipboard/util/SocialHelper$14;->b:Lflipboard/objs/ConfigService;

    iput-object p3, p0, Lflipboard/util/SocialHelper$14;->c:Lflipboard/util/SocialHelper$ServiceLoginObserver;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;)V
    .locals 0

    .prologue
    .line 791
    return-void
.end method

.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 796
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lflipboard/util/SocialHelper$14;->a:Lflipboard/activities/FlipboardActivity;

    const-class v2, Lflipboard/activities/ServiceLoginActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 797
    const-string v1, "service"

    iget-object v2, p0, Lflipboard/util/SocialHelper$14;->b:Lflipboard/objs/ConfigService;

    iget-object v2, v2, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 798
    const-string v1, "viewSectionAfterSuccess"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 799
    const-string v1, "extra_content_discovery_from_source"

    const-string v2, "usageSocialLoginOriginSocialAction"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 800
    iget-object v1, p0, Lflipboard/util/SocialHelper$14;->a:Lflipboard/activities/FlipboardActivity;

    new-instance v2, Lflipboard/util/SocialHelper$14$1;

    invoke-direct {v2, p0}, Lflipboard/util/SocialHelper$14$1;-><init>(Lflipboard/util/SocialHelper$14;)V

    invoke-virtual {v1, v0, v3, v2}, Lflipboard/activities/FlipboardActivity;->a(Landroid/content/Intent;ILflipboard/activities/FlipboardActivity$ActivityResultListener;)V

    .line 813
    return-void
.end method

.method public final d(Landroid/support/v4/app/DialogFragment;)V
    .locals 0

    .prologue
    .line 818
    return-void
.end method
