.class final Lflipboard/util/Log$1;
.super Ljava/lang/Object;
.source "Log.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 35
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    sget-object v1, Lflipboard/util/Log$Level;->e:Lflipboard/util/Log$Level;

    const-string v2, "%-E"

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v0, v1, v2, v3}, Lflipboard/util/Log;->a(Lflipboard/util/Log;Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    invoke-virtual {v0, p1, p2, v5}, Lflipboard/io/UsageManager;->a(Ljava/lang/Thread;Ljava/lang/Throwable;Z)V

    .line 37
    invoke-static {}, Lflipboard/util/HappyUser;->a()V

    .line 39
    const-wide/16 v0, 0x3e8

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :goto_0
    sget-object v0, Lflipboard/util/Log;->c:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 44
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
