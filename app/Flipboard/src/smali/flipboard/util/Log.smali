.class public final Lflipboard/util/Log;
.super Ljava/lang/Object;
.source "Log.java"


# static fields
.field static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/util/Log;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lflipboard/util/Log;

.field static final c:Ljava/lang/Thread$UncaughtExceptionHandler;


# instance fields
.field final d:Ljava/lang/String;

.field final e:Lflipboard/util/Log$Level;

.field public f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lflipboard/util/Log;->a:Ljava/util/Map;

    .line 28
    new-instance v0, Lflipboard/util/Log;

    const-string v1, "main"

    sget-object v2, Lflipboard/util/Log$Level;->b:Lflipboard/util/Log$Level;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lflipboard/util/Log;-><init>(Ljava/lang/String;Lflipboard/util/Log$Level;Z)V

    sput-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 30
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    sput-object v0, Lflipboard/util/Log;->c:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 32
    new-instance v0, Lflipboard/util/Log$1;

    invoke-direct {v0}, Lflipboard/util/Log$1;-><init>()V

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 46
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lflipboard/util/Log$Level;Z)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lflipboard/util/Log;->d:Ljava/lang/String;

    .line 72
    iput-object p2, p0, Lflipboard/util/Log;->e:Lflipboard/util/Log$Level;

    .line 73
    iput-boolean p3, p0, Lflipboard/util/Log;->f:Z

    .line 74
    return-void
.end method

.method public static declared-synchronized a(Ljava/lang/String;)Lflipboard/util/Log;
    .locals 5

    .prologue
    .line 50
    const-class v1, Lflipboard/util/Log;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lflipboard/util/Log;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/util/Log;

    .line 51
    if-nez v0, :cond_0

    .line 52
    sget-object v2, Lflipboard/util/Log;->a:Ljava/util/Map;

    new-instance v0, Lflipboard/util/Log;

    sget-object v3, Lflipboard/util/Log$Level;->a:Lflipboard/util/Log$Level;

    const/4 v4, 0x0

    invoke-direct {v0, p0, v3, v4}, Lflipboard/util/Log;-><init>(Ljava/lang/String;Lflipboard/util/Log$Level;Z)V

    invoke-interface {v2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    :cond_0
    monitor-exit v1

    return-object v0

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(ILjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 187
    if-nez p1, :cond_0

    .line 188
    const-string v0, "n/a"

    .line 194
    :goto_0
    return-object v0

    .line 191
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, p0, :cond_2

    .line 192
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    sub-int/2addr v0, p0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 194
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-gt v1, p0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "*"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "****"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, p1

    goto :goto_1
.end method

.method public static declared-synchronized a()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    const-class v1, Lflipboard/util/Log;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    sget-object v2, Lflipboard/util/Log;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 59
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    monitor-exit v1

    return-object v0

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Lflipboard/util/Log$Level;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 104
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 105
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 106
    sub-int v2, v1, v0

    const/16 v3, 0x800

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 108
    add-int v3, v0, v2

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 109
    sget-object v4, Lflipboard/util/Log$2;->a:[I

    invoke-virtual {p0}, Lflipboard/util/Log$Level;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 105
    :goto_1
    :pswitch_0
    add-int/2addr v0, v2

    goto :goto_0

    .line 111
    :pswitch_1
    const-string v4, "flip"

    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 112
    :pswitch_2
    const-string v4, "flip"

    invoke-static {v4, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 113
    :pswitch_3
    const-string v4, "flip"

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 116
    :cond_0
    return-void

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static varargs a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 163
    const-string v1, "flipapi"

    .line 164
    invoke-static {p1, p2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 165
    const-string v3, "[%s:%d] %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v0

    const/4 v5, 0x1

    invoke-virtual {p0}, Lflipboard/util/Log$Level;->ordinal()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 166
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    .line 167
    :goto_0
    if-ge v0, v3, :cond_0

    .line 168
    sub-int v4, v3, v0

    const/16 v5, 0x800

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 170
    add-int v5, v0, v4

    invoke-virtual {v2, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 171
    sget-object v6, Lflipboard/util/Log$2;->a:[I

    invoke-virtual {p0}, Lflipboard/util/Log$Level;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 167
    :goto_1
    :pswitch_0
    add-int/2addr v0, v4

    goto :goto_0

    .line 173
    :pswitch_1
    invoke-static {v1, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 174
    :pswitch_2
    invoke-static {v1, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 175
    :pswitch_3
    invoke-static {v1, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 178
    :cond_0
    return-void

    .line 171
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lflipboard/util/Log;Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Lflipboard/util/Log;->b(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private varargs declared-synchronized b(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 90
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lflipboard/util/Log$Level;->ordinal()I

    move-result v0

    iget-object v1, p0, Lflipboard/util/Log;->e:Lflipboard/util/Log$Level;

    invoke-virtual {v1}, Lflipboard/util/Log$Level;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget-boolean v0, p0, Lflipboard/util/Log;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 100
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 93
    :cond_1
    :try_start_1
    invoke-static {p2, p3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 94
    iget-object v1, p0, Lflipboard/util/Log;->d:Ljava/lang/String;

    const-string v2, "main"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 95
    const-string v1, "[%s:%d] %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Lflipboard/util/Log$Level;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 99
    :goto_1
    invoke-static {p1, v0}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 90
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 97
    :cond_2
    :try_start_2
    const-string v1, "%s: [%s:%d] %s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/util/Log;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p1}, Lflipboard/util/Log$Level;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Z)Lflipboard/util/Log;
    .locals 5

    .prologue
    .line 81
    iget-boolean v0, p0, Lflipboard/util/Log;->f:Z

    if-eq v0, p1, :cond_0

    .line 82
    iput-boolean p1, p0, Lflipboard/util/Log;->f:Z

    .line 83
    sget-object v1, Lflipboard/util/Log$Level;->b:Lflipboard/util/Log$Level;

    const-string v2, "%s: logging %s"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v4, p0, Lflipboard/util/Log;->d:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v4, 0x1

    if-eqz p1, :cond_1

    const-string v0, "on"

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;)V

    .line 85
    :cond_0
    return-object p0

    .line 83
    :cond_1
    const-string v0, "off"

    goto :goto_0
.end method

.method public final varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lflipboard/util/Log$Level;->d:Lflipboard/util/Log$Level;

    invoke-direct {p0, v0, p1, p2}, Lflipboard/util/Log;->b(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 130
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 145
    sget-object v0, Lflipboard/util/Log$Level;->d:Lflipboard/util/Log$Level;

    const-string v1, "%-e"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {p0, v0, v1, v2}, Lflipboard/util/Log;->b(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    return-void
.end method

.method public final varargs b(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 133
    sget-object v0, Lflipboard/util/Log$Level;->e:Lflipboard/util/Log$Level;

    invoke-direct {p0, v0, p1, p2}, Lflipboard/util/Log;->b(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    return-void
.end method

.method public final b(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 149
    sget-object v0, Lflipboard/util/Log$Level;->e:Lflipboard/util/Log$Level;

    const-string v1, "%-E"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {p0, v0, v1, v2}, Lflipboard/util/Log;->b(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 150
    iget-boolean v0, p0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    if-eqz v0, :cond_0

    .line 151
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    invoke-virtual {v0, p1}, Lflipboard/io/UsageManager;->a(Ljava/lang/Throwable;)V

    .line 153
    :cond_0
    return-void
.end method
