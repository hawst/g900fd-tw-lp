.class final Lflipboard/util/ImageSave$1;
.super Lflipboard/io/Download$Observer;
.source "ImageSave.java"


# instance fields
.field a:I

.field final synthetic b:Lflipboard/objs/FeedItem;

.field final synthetic c:Lflipboard/service/FlipboardManager;

.field final synthetic d:Lflipboard/activities/FlipboardActivity;


# direct methods
.method constructor <init>(Lflipboard/objs/FeedItem;Lflipboard/service/FlipboardManager;Lflipboard/activities/FlipboardActivity;)V
    .locals 1

    .prologue
    const/16 v0, 0x1f4

    .line 70
    iput-object p1, p0, Lflipboard/util/ImageSave$1;->b:Lflipboard/objs/FeedItem;

    iput-object p2, p0, Lflipboard/util/ImageSave$1;->c:Lflipboard/service/FlipboardManager;

    iput-object p3, p0, Lflipboard/util/ImageSave$1;->d:Lflipboard/activities/FlipboardActivity;

    invoke-direct {p0, v0}, Lflipboard/io/Download$Observer;-><init>(I)V

    .line 71
    iput v0, p0, Lflipboard/util/ImageSave$1;->a:I

    return-void
.end method

.method static synthetic a(Lflipboard/util/ImageSave$1;)Lflipboard/io/Download;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lflipboard/util/ImageSave$1;->f:Lflipboard/io/Download;

    return-object v0
.end method

.method static synthetic b(Lflipboard/util/ImageSave$1;)Lflipboard/io/Download;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lflipboard/util/ImageSave$1;->f:Lflipboard/io/Download;

    return-object v0
.end method

.method static synthetic c(Lflipboard/util/ImageSave$1;)Lflipboard/io/Download;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lflipboard/util/ImageSave$1;->f:Lflipboard/io/Download;

    return-object v0
.end method

.method static synthetic d(Lflipboard/util/ImageSave$1;)Lflipboard/io/Download;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lflipboard/util/ImageSave$1;->f:Lflipboard/io/Download;

    return-object v0
.end method

.method static synthetic e(Lflipboard/util/ImageSave$1;)Lflipboard/io/Download;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lflipboard/util/ImageSave$1;->f:Lflipboard/io/Download;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 70
    check-cast p1, Lflipboard/io/Download;

    check-cast p2, Lflipboard/io/Download$Status;

    check-cast p3, Lflipboard/io/Download$Data;

    sget-object v0, Lflipboard/io/Download$Status;->h:Lflipboard/io/Download$Status;

    if-ne p2, v0, :cond_3

    invoke-virtual {p1, p0}, Lflipboard/io/Download;->b(Lflipboard/io/Download$Observer;)V

    :try_start_0
    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {v0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_0
    invoke-virtual {p3}, Lflipboard/io/Download$Data;->c()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lflipboard/io/Download$Data;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    const-string v4, "image/"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {v1}, Lflipboard/util/ImageSave;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z

    new-instance v0, Ljava/io/FileInputStream;

    invoke-virtual {p3}, Lflipboard/io/Download$Data;->c()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    const-wide/16 v2, 0x0

    :try_start_2
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "title"

    iget-object v4, p0, Lflipboard/util/ImageSave$1;->b:Lflipboard/objs/FeedItem;

    invoke-static {v4}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "_display_name"

    iget-object v4, p0, Lflipboard/util/ImageSave$1;->b:Lflipboard/objs/FeedItem;

    invoke-static {v4}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "description"

    iget-object v4, p0, Lflipboard/util/ImageSave$1;->b:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->by:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "mime_type"

    invoke-virtual {p3}, Lflipboard/io/Download$Data;->c()Ljava/io/File;

    move-result-object v4

    invoke-static {v4}, Lflipboard/util/HttpUtil;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v5, "bucket_id"

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "bucket_display_name"

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "_size"

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "_data"

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v3}, Lflipboard/app/FlipboardApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    sget-object v2, Lflipboard/util/ImageSave;->a:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v6}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v4

    aput-object v4, v2, v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object v2, v1

    move-object v1, v0

    :goto_1
    :try_start_3
    iget-object v0, p0, Lflipboard/util/ImageSave$1;->c:Lflipboard/service/FlipboardManager;

    new-instance v3, Lflipboard/util/ImageSave$1$1;

    invoke-direct {v3, p0}, Lflipboard/util/ImageSave$1$1;-><init>(Lflipboard/util/ImageSave$1;)V

    invoke-virtual {v0, v3}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :try_start_4
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :cond_2
    :goto_2
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_3
    :try_start_5
    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v3, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lflipboard/util/ImageSave$1;->d:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v0

    iget-object v3, p0, Lflipboard/util/ImageSave$1;->d:Lflipboard/activities/FlipboardActivity;

    const v4, 0x7f0d0285

    invoke-virtual {v3, v4}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0201cf

    invoke-virtual {v0, v4, v3}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :try_start_6
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_4
    :try_start_7
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    :goto_5
    throw v0

    :catch_3
    move-exception v1

    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v2, v1}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_3
    sget-object v0, Lflipboard/io/Download$Status;->g:Lflipboard/io/Download$Status;

    if-eq p2, v0, :cond_4

    sget-object v0, Lflipboard/io/Download$Status;->i:Lflipboard/io/Download$Status;

    if-eq p2, v0, :cond_4

    sget-object v0, Lflipboard/io/Download$Status;->f:Lflipboard/io/Download$Status;

    if-ne p2, v0, :cond_2

    :cond_4
    iget-object v0, p0, Lflipboard/util/ImageSave$1;->c:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/util/ImageSave$1$2;

    invoke-direct {v1, p0, p0}, Lflipboard/util/ImageSave$1$2;-><init>(Lflipboard/util/ImageSave$1;Lflipboard/io/Download$Observer;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_4

    :catchall_2
    move-exception v2

    move-object v7, v2

    move-object v2, v0

    move-object v0, v7

    goto :goto_4

    :catchall_3
    move-exception v0

    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    goto :goto_4

    :catch_4
    move-exception v0

    move-object v7, v2

    move-object v2, v1

    move-object v1, v7

    goto :goto_3

    :catch_5
    move-exception v2

    move-object v7, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_3

    :catch_6
    move-exception v0

    goto :goto_3

    :cond_5
    move-object v1, v2

    goto/16 :goto_0

    :cond_6
    move-object v1, v2

    goto/16 :goto_1
.end method
