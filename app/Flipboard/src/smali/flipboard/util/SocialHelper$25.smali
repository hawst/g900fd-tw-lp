.class final Lflipboard/util/SocialHelper$25;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SocialHelper.java"


# instance fields
.field final synthetic a:Lflipboard/activities/FlipboardActivity;

.field final synthetic b:Lflipboard/service/Section;

.field final synthetic c:Lflipboard/objs/FeedItem;


# direct methods
.method constructor <init>(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 1106
    iput-object p1, p0, Lflipboard/util/SocialHelper$25;->a:Lflipboard/activities/FlipboardActivity;

    iput-object p2, p0, Lflipboard/util/SocialHelper$25;->b:Lflipboard/service/Section;

    iput-object p3, p0, Lflipboard/util/SocialHelper$25;->c:Lflipboard/objs/FeedItem;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;I)V
    .locals 3

    .prologue
    .line 1109
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lflipboard/util/SocialHelper$25;->a:Lflipboard/activities/FlipboardActivity;

    const-class v2, Lflipboard/activities/DebugMenuActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1111
    iget-object v1, p0, Lflipboard/util/SocialHelper$25;->b:Lflipboard/service/Section;

    if-eqz v1, :cond_0

    .line 1112
    const-string v1, "sectionId"

    iget-object v2, p0, Lflipboard/util/SocialHelper$25;->b:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1114
    :cond_0
    const-string v1, "feedItemId"

    iget-object v2, p0, Lflipboard/util/SocialHelper$25;->c:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1115
    iget-object v1, p0, Lflipboard/util/SocialHelper$25;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v1, v0}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    .line 1116
    return-void
.end method
