.class final Lflipboard/util/ShareHelper$6;
.super Ljava/lang/Object;
.source "ShareHelper.java"

# interfaces
.implements Lflipboard/service/Flap$SectionListObserver;


# instance fields
.field final synthetic a:Lflipboard/objs/ConfigService;

.field final synthetic b:Lflipboard/service/Account;


# direct methods
.method constructor <init>(Lflipboard/objs/ConfigService;Lflipboard/service/Account;)V
    .locals 0

    .prologue
    .line 620
    iput-object p1, p0, Lflipboard/util/ShareHelper$6;->a:Lflipboard/objs/ConfigService;

    iput-object p2, p0, Lflipboard/util/ShareHelper$6;->b:Lflipboard/service/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 620
    check-cast p1, Lflipboard/objs/SectionListResult;

    new-instance v1, Lflipboard/json/FLObject;

    invoke-direct {v1}, Lflipboard/json/FLObject;-><init>()V

    iget-object v0, p1, Lflipboard/objs/SectionListResult;->a:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lflipboard/objs/SectionListResult;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p1, Lflipboard/objs/SectionListResult;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SectionListItem;

    iget-boolean v3, v0, Lflipboard/objs/SectionListItem;->i:Z

    if-eqz v3, :cond_0

    iget-object v3, v0, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    invoke-static {v3}, Lflipboard/util/JavaUtil;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lflipboard/objs/SectionListItem;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lflipboard/util/ShareHelper$6;->a:Lflipboard/objs/ConfigService;

    iget-boolean v0, v0, Lflipboard/objs/ConfigService;->aw:Z

    if-nez v0, :cond_0

    :cond_1
    invoke-virtual {v1}, Lflipboard/json/FLObject;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lflipboard/objs/SectionListResult;->a:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SectionListItem;

    iget-object v2, v0, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    invoke-static {v2}, Lflipboard/util/JavaUtil;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lflipboard/objs/SectionListItem;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    invoke-virtual {v1}, Lflipboard/json/FLObject;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    const-string v2, "unable to fetch share targets for %s: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lflipboard/util/ShareHelper$6;->a:Lflipboard/objs/ConfigService;

    invoke-virtual {v4}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    iget-object v0, p0, Lflipboard/util/ShareHelper$6;->b:Lflipboard/service/Account;

    invoke-virtual {v0, v1}, Lflipboard/service/Account;->a(Lflipboard/json/FLObject;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 657
    sget-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    const-string v1, "failed to fetch share targets for %s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/util/ShareHelper$6;->a:Lflipboard/objs/ConfigService;

    invoke-virtual {v4}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 658
    return-void
.end method
