.class final Lflipboard/util/Format$CharBuf;
.super Ljava/lang/Object;
.source "Format.java"

# interfaces
.implements Ljava/lang/CharSequence;


# instance fields
.field a:[C

.field b:I

.field final synthetic c:Lflipboard/util/Format;


# direct methods
.method constructor <init>(Lflipboard/util/Format;I)V
    .locals 1

    .prologue
    .line 320
    iput-object p1, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 321
    new-array v0, p2, [C

    iput-object v0, p0, Lflipboard/util/Format$CharBuf;->a:[C

    .line 322
    return-void
.end method

.method private a(I)V
    .locals 5

    .prologue
    .line 500
    iget v0, p0, Lflipboard/util/Format$CharBuf;->b:I

    .line 501
    :goto_0
    sub-int v1, v0, p1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    .line 502
    iget-object v1, p0, Lflipboard/util/Format$CharBuf;->a:[C

    aget-char v2, v1, p1

    .line 503
    iget-object v3, p0, Lflipboard/util/Format$CharBuf;->a:[C

    add-int/lit8 v1, p1, 0x1

    iget-object v4, p0, Lflipboard/util/Format$CharBuf;->a:[C

    add-int/lit8 v0, v0, -0x1

    aget-char v4, v4, v0

    aput-char v4, v3, p1

    .line 504
    iget-object v3, p0, Lflipboard/util/Format$CharBuf;->a:[C

    aput-char v2, v3, v0

    move p1, v1

    .line 505
    goto :goto_0

    .line 506
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 529
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 530
    iget-object v0, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    iget-boolean v0, v0, Lflipboard/util/Format;->f:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x30

    .line 531
    :goto_0
    iget-object v1, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    iget-boolean v1, v1, Lflipboard/util/Format;->d:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    iget v1, v1, Lflipboard/util/Format;->h:I

    if-lez v1, :cond_1

    .line 532
    iget-object v1, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    iget v1, v1, Lflipboard/util/Format;->h:I

    sub-int/2addr v1, v2

    :goto_1
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_1

    .line 533
    invoke-virtual {p0, v0}, Lflipboard/util/Format$CharBuf;->a(C)V

    goto :goto_1

    .line 530
    :cond_0
    const/16 v0, 0x20

    goto :goto_0

    .line 536
    :cond_1
    iget v0, p0, Lflipboard/util/Format$CharBuf;->b:I

    add-int/2addr v0, v2

    iget-object v1, p0, Lflipboard/util/Format$CharBuf;->a:[C

    array-length v1, v1

    if-lt v0, v1, :cond_2

    .line 537
    iget v0, p0, Lflipboard/util/Format$CharBuf;->b:I

    add-int/2addr v0, v2

    invoke-direct {p0, v0}, Lflipboard/util/Format$CharBuf;->b(I)V

    .line 539
    :cond_2
    const/4 v0, 0x0

    iget-object v1, p0, Lflipboard/util/Format$CharBuf;->a:[C

    iget v3, p0, Lflipboard/util/Format$CharBuf;->b:I

    invoke-virtual {p1, v0, v2, v1, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 540
    iget v0, p0, Lflipboard/util/Format$CharBuf;->b:I

    add-int/2addr v0, v2

    iput v0, p0, Lflipboard/util/Format$CharBuf;->b:I

    .line 541
    return-void
.end method

.method private b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 619
    iget-object v0, p0, Lflipboard/util/Format$CharBuf;->a:[C

    mul-int/lit8 v1, p1, 0x2

    new-array v1, v1, [C

    iput-object v1, p0, Lflipboard/util/Format$CharBuf;->a:[C

    iget v2, p0, Lflipboard/util/Format$CharBuf;->b:I

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 620
    return-void
.end method


# virtual methods
.method final a(C)V
    .locals 3

    .prologue
    .line 343
    iget v0, p0, Lflipboard/util/Format$CharBuf;->b:I

    iget-object v1, p0, Lflipboard/util/Format$CharBuf;->a:[C

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 344
    iget v0, p0, Lflipboard/util/Format$CharBuf;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lflipboard/util/Format$CharBuf;->b(I)V

    .line 346
    :cond_0
    iget-object v0, p0, Lflipboard/util/Format$CharBuf;->a:[C

    iget v1, p0, Lflipboard/util/Format$CharBuf;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lflipboard/util/Format$CharBuf;->b:I

    aput-char p1, v0, v1

    .line 347
    return-void
.end method

.method final a(CLjava/lang/Object;)V
    .locals 14

    .prologue
    .line 350
    iget v5, p0, Lflipboard/util/Format$CharBuf;->b:I

    .line 352
    sparse-switch p1, :sswitch_data_0

    .line 415
    :try_start_0
    iget-object v2, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "%"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": not supported. SORRY!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lflipboard/util/Format;->a(Ljava/lang/RuntimeException;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 425
    :cond_0
    :goto_0
    iget-object v2, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    iget v2, v2, Lflipboard/util/Format;->h:I

    if-lez v2, :cond_16

    .line 426
    iget v2, p0, Lflipboard/util/Format$CharBuf;->b:I

    sub-int v3, v2, v5

    .line 427
    iget-object v2, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    iget v2, v2, Lflipboard/util/Format;->h:I

    sub-int v4, v2, v3

    .line 428
    if-lez v4, :cond_16

    .line 429
    iget-object v2, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    iget-boolean v2, v2, Lflipboard/util/Format;->f:Z

    if-eqz v2, :cond_14

    const/16 v2, 0x30

    .line 430
    :goto_1
    iget-object v6, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    iget-boolean v6, v6, Lflipboard/util/Format;->d:Z

    if-nez v6, :cond_15

    .line 432
    iget v6, p0, Lflipboard/util/Format$CharBuf;->b:I

    add-int/2addr v6, v4

    iget-object v7, p0, Lflipboard/util/Format$CharBuf;->a:[C

    array-length v7, v7

    if-lt v6, v7, :cond_1

    .line 433
    iget v6, p0, Lflipboard/util/Format$CharBuf;->b:I

    add-int/2addr v6, v4

    invoke-direct {p0, v6}, Lflipboard/util/Format$CharBuf;->b(I)V

    .line 435
    :cond_1
    iget-object v6, p0, Lflipboard/util/Format$CharBuf;->a:[C

    iget-object v7, p0, Lflipboard/util/Format$CharBuf;->a:[C

    add-int v8, v5, v4

    invoke-static {v6, v5, v7, v8, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 436
    iget v3, p0, Lflipboard/util/Format$CharBuf;->b:I

    add-int/2addr v3, v4

    iput v3, p0, Lflipboard/util/Format$CharBuf;->b:I

    move v3, v4

    move v4, v5

    .line 437
    :goto_2
    add-int/lit8 v3, v3, -0x1

    if-ltz v3, :cond_16

    .line 438
    iget-object v6, p0, Lflipboard/util/Format$CharBuf;->a:[C

    add-int/lit8 v5, v4, 0x1

    aput-char v2, v6, v4

    move v4, v5

    goto :goto_2

    .line 354
    :sswitch_0
    :try_start_1
    move-object/from16 v0, p2

    instance-of v2, v0, Ljava/lang/Character;

    if-eqz v2, :cond_2

    .line 355
    move-object/from16 v0, p2

    check-cast v0, Ljava/lang/Character;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v2

    invoke-virtual {p0, v2}, Lflipboard/util/Format$CharBuf;->a(C)V
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 418
    :catch_0
    move-exception v2

    :goto_3
    iget-object v2, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    new-instance v2, Lflipboard/util/Format$Exception;

    const-string v3, "%s not compatible with %%%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x1

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-direct {v2, v3, v4}, Lflipboard/util/Format$Exception;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v2}, Lflipboard/util/Format;->a(Ljava/lang/RuntimeException;)V

    .line 419
    const-string v2, "???"

    invoke-direct {p0, v2}, Lflipboard/util/Format$CharBuf;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 357
    :cond_2
    :try_start_2
    move-object/from16 v0, p2

    check-cast v0, Ljava/lang/Number;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    const v3, 0xffff

    and-int/2addr v2, v3

    .line 358
    int-to-char v2, v2

    invoke-virtual {p0, v2}, Lflipboard/util/Format$CharBuf;->a(C)V

    goto/16 :goto_0

    .line 365
    :sswitch_1
    move-object/from16 v0, p2

    instance-of v2, v0, Ljava/lang/Long;

    if-nez v2, :cond_3

    move-object/from16 v0, p2

    instance-of v2, v0, Ljava/lang/Double;

    if-eqz v2, :cond_7

    .line 366
    :cond_3
    move-object/from16 v0, p2

    check-cast v0, Ljava/lang/Number;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    .line 374
    :goto_4
    const-wide/16 v6, 0x0

    cmp-long v4, v2, v6

    if-gez v4, :cond_4

    const/16 v4, 0x2d

    invoke-virtual {p0, v4}, Lflipboard/util/Format$CharBuf;->a(C)V

    neg-long v2, v2

    :cond_4
    iget v8, p0, Lflipboard/util/Format$CharBuf;->b:I

    const/4 v4, 0x0

    move-wide v6, v2

    move v2, v4

    :cond_5
    iget-object v3, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    iget-boolean v3, v3, Lflipboard/util/Format;->e:Z

    if-eqz v3, :cond_6

    const/4 v3, 0x3

    if-ne v2, v3, :cond_6

    const/16 v2, 0x2c

    invoke-virtual {p0, v2}, Lflipboard/util/Format$CharBuf;->a(C)V

    const/4 v2, 0x0

    :cond_6
    const-wide/16 v10, 0x30

    const-wide/16 v12, 0xa

    rem-long v12, v6, v12

    add-long/2addr v10, v12

    long-to-int v3, v10

    int-to-char v3, v3

    invoke-virtual {p0, v3}, Lflipboard/util/Format$CharBuf;->a(C)V

    const-wide/16 v10, 0xa

    div-long/2addr v6, v10

    add-int/lit8 v2, v2, 0x1

    const-wide/16 v10, 0x0

    cmp-long v3, v6, v10

    if-nez v3, :cond_5

    invoke-direct {p0, v8}, Lflipboard/util/Format$CharBuf;->a(I)V

    goto/16 :goto_0

    .line 367
    :cond_7
    move-object/from16 v0, p2

    instance-of v2, v0, Ljava/lang/Integer;

    if-nez v2, :cond_8

    move-object/from16 v0, p2

    instance-of v2, v0, Ljava/lang/Float;

    if-eqz v2, :cond_9

    .line 368
    :cond_8
    move-object/from16 v0, p2

    check-cast v0, Ljava/lang/Number;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    int-to-long v2, v2

    goto :goto_4

    .line 369
    :cond_9
    move-object/from16 v0, p2

    instance-of v2, v0, Ljava/lang/Short;

    if-eqz v2, :cond_a

    .line 370
    move-object/from16 v0, p2

    check-cast v0, Ljava/lang/Number;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Number;->shortValue()S

    move-result v2

    int-to-long v2, v2

    goto :goto_4

    .line 372
    :cond_a
    move-object/from16 v0, p2

    check-cast v0, Ljava/lang/Number;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Number;->byteValue()B

    move-result v2

    int-to-long v2, v2

    goto :goto_4

    .line 380
    :sswitch_2
    move-object/from16 v0, p2

    check-cast v0, Ljava/lang/Throwable;

    move-object v2, v0

    const/16 v3, 0x45

    if-ne p1, v3, :cond_b

    const/4 v3, 0x1

    :goto_5
    iget-object v4, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    iget v4, v4, Lflipboard/util/Format;->h:I

    invoke-virtual {p0, v2, v3, v4}, Lflipboard/util/Format$CharBuf;->a(Ljava/lang/Throwable;ZI)Z

    move-result v2

    if-nez v2, :cond_0

    .line 381
    const-string v2, "null"

    invoke-direct {p0, v2}, Lflipboard/util/Format$CharBuf;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 380
    :cond_b
    const/4 v3, 0x0

    goto :goto_5

    .line 386
    :sswitch_3
    move-object/from16 v0, p2

    check-cast v0, Ljava/lang/Number;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    iget v3, v3, Lflipboard/util/Format;->i:I

    if-ltz v3, :cond_c

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x2e

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v6, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    iget v6, v6, Lflipboard/util/Format;->i:I

    if-le v3, v6, :cond_c

    const/4 v3, 0x0

    add-int/lit8 v4, v4, 0x1

    iget-object v6, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    iget v6, v6, Lflipboard/util/Format;->i:I

    add-int/2addr v4, v6

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    :cond_c
    invoke-direct {p0, v2}, Lflipboard/util/Format$CharBuf;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 390
    :sswitch_4
    if-nez p2, :cond_17

    .line 391
    const-string v3, "null"
    :try_end_2
    .catch Ljava/lang/ClassCastException; {:try_start_2 .. :try_end_2} :catch_0

    .line 393
    :goto_6
    :try_start_3
    instance-of v2, v3, Lflipboard/util/Format;

    if-eqz v2, :cond_d

    .line 394
    move-object v0, v3

    check-cast v0, Lflipboard/util/Format;

    move-object v2, v0

    .line 395
    iget-object v4, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    iget-object v6, v2, Lflipboard/util/Format;->a:Ljava/lang/String;

    iget-object v2, v2, Lflipboard/util/Format;->b:[Ljava/lang/Object;

    invoke-static {v4, v6, v2}, Lflipboard/util/Format;->a(Lflipboard/util/Format;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 418
    :catch_1
    move-exception v2

    move-object/from16 p2, v3

    goto/16 :goto_3

    .line 397
    :cond_d
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lflipboard/util/Format$CharBuf;->a(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 402
    :sswitch_5
    :try_start_4
    move-object/from16 v0, p2

    check-cast v0, Ljava/lang/Number;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    .line 403
    move-object/from16 v0, p2

    instance-of v4, v0, Ljava/lang/Integer;

    if-eqz v4, :cond_10

    .line 404
    const-wide v6, 0xffffffffL

    and-long/2addr v2, v6

    .line 410
    :cond_e
    :goto_7
    const/16 v4, 0x58

    if-ne p1, v4, :cond_12

    const/4 v4, 0x1

    :goto_8
    if-eqz v4, :cond_13

    invoke-static {}, Lflipboard/util/Format;->a()[C

    move-result-object v4

    :goto_9
    iget v6, p0, Lflipboard/util/Format$CharBuf;->b:I

    :cond_f
    const-wide/16 v8, 0xf

    and-long/2addr v8, v2

    long-to-int v7, v8

    aget-char v7, v4, v7

    invoke-virtual {p0, v7}, Lflipboard/util/Format$CharBuf;->a(C)V

    const/4 v7, 0x4

    ushr-long/2addr v2, v7

    const-wide/16 v8, 0x0

    cmp-long v7, v2, v8

    if-nez v7, :cond_f

    invoke-direct {p0, v6}, Lflipboard/util/Format$CharBuf;->a(I)V

    goto/16 :goto_0

    .line 405
    :cond_10
    move-object/from16 v0, p2

    instance-of v4, v0, Ljava/lang/Short;

    if-eqz v4, :cond_11

    .line 406
    const-wide/32 v6, 0xffff

    and-long/2addr v2, v6

    goto :goto_7

    .line 407
    :cond_11
    move-object/from16 v0, p2

    instance-of v4, v0, Ljava/lang/Byte;

    if-eqz v4, :cond_e

    .line 408
    const-wide/16 v6, 0xff

    and-long/2addr v2, v6

    goto :goto_7

    .line 410
    :cond_12
    const/4 v4, 0x0

    goto :goto_8

    :cond_13
    invoke-static {}, Lflipboard/util/Format;->b()[C
    :try_end_4
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v4

    goto :goto_9

    .line 429
    :cond_14
    const/16 v2, 0x20

    goto/16 :goto_1

    .line 441
    :cond_15
    :goto_a
    iget-object v4, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    iget v4, v4, Lflipboard/util/Format;->h:I

    if-ge v3, v4, :cond_16

    .line 442
    invoke-virtual {p0, v2}, Lflipboard/util/Format$CharBuf;->a(C)V

    .line 443
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    .line 448
    :cond_16
    return-void

    :cond_17
    move-object/from16 v3, p2

    goto/16 :goto_6

    .line 352
    :sswitch_data_0
    .sparse-switch
        0x45 -> :sswitch_2
        0x58 -> :sswitch_5
        0x63 -> :sswitch_0
        0x64 -> :sswitch_1
        0x65 -> :sswitch_2
        0x66 -> :sswitch_3
        0x73 -> :sswitch_4
        0x75 -> :sswitch_1
        0x78 -> :sswitch_5
    .end sparse-switch
.end method

.method final a(Ljava/lang/Throwable;ZI)Z
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 550
    if-nez p1, :cond_0

    .line 557
    :goto_0
    return v1

    .line 553
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lflipboard/util/Format$CharBuf;->a(Ljava/lang/Throwable;ZI)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 554
    if-eqz p2, :cond_7

    const-string v0, "\n"

    :goto_1
    invoke-direct {p0, v0}, Lflipboard/util/Format$CharBuf;->a(Ljava/lang/String;)V

    .line 556
    :cond_1
    iget-object v0, p0, Lflipboard/util/Format$CharBuf;->c:Lflipboard/util/Format;

    iget-boolean v7, v0, Lflipboard/util/Format;->d:Z

    invoke-virtual {p1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v8

    if-gez p3, :cond_8

    neg-int p3, p3

    :cond_2
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/util/Format$CharBuf;->a(Ljava/lang/String;)V

    move v0, v1

    move v2, v3

    move v4, v1

    move v5, v3

    :goto_3
    array-length v6, v8

    if-ge v0, v6, :cond_c

    if-lez p3, :cond_c

    aget-object v9, v8, v0

    invoke-virtual {v9}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v6

    if-eqz v5, :cond_3

    const-string v10, "flipboard.util.Format"

    invoke-virtual {v6, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_6

    move v5, v1

    :cond_3
    if-nez v7, :cond_4

    const-string v2, "flipboard."

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_4
    if-nez p2, :cond_9

    const/16 v2, 0x2e

    invoke-virtual {v6, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    if-ltz v2, :cond_e

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v6, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    :goto_4
    const-string v4, "%s:%s:%d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v1

    invoke-virtual {v9}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v3

    const/4 v2, 0x2

    invoke-virtual {v9}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v2

    invoke-static {v4, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_5
    add-int/lit8 p3, p3, -0x1

    move-object v6, v2

    move v4, v1

    :goto_6
    if-eqz p2, :cond_b

    const-string v2, "\n    "

    :goto_7
    invoke-direct {p0, v2}, Lflipboard/util/Format$CharBuf;->a(Ljava/lang/String;)V

    invoke-direct {p0, v6}, Lflipboard/util/Format$CharBuf;->a(Ljava/lang/String;)V

    :cond_5
    move v2, v1

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 554
    :cond_7
    const-string v0, ", "

    goto/16 :goto_1

    .line 556
    :cond_8
    if-nez p3, :cond_2

    const/16 p3, 0x14

    goto/16 :goto_2

    :cond_9
    invoke-virtual {v9}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    :cond_a
    if-nez v4, :cond_5

    const-string v2, "..."

    move-object v6, v2

    move v4, v3

    goto :goto_6

    :cond_b
    const-string v2, ", "

    goto :goto_7

    :cond_c
    if-eqz v2, :cond_d

    const-string v0, "<no stack trace>"

    invoke-direct {p0, v0}, Lflipboard/util/Format$CharBuf;->a(Ljava/lang/String;)V

    :cond_d
    move v1, v3

    .line 557
    goto/16 :goto_0

    :cond_e
    move-object v2, v6

    goto :goto_4
.end method

.method public final charAt(I)C
    .locals 3

    .prologue
    .line 327
    iget v0, p0, Lflipboard/util/Format$CharBuf;->b:I

    if-ge p1, v0, :cond_0

    .line 328
    iget-object v0, p0, Lflipboard/util/Format$CharBuf;->a:[C

    aget-char v0, v0, p1

    return v0

    .line 330
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " >= limit="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lflipboard/util/Format$CharBuf;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final length()I
    .locals 1

    .prologue
    .line 338
    iget v0, p0, Lflipboard/util/Format$CharBuf;->b:I

    return v0
.end method

.method public final subSequence(II)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 334
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 624
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lflipboard/util/Format$CharBuf;->a:[C

    const/4 v2, 0x0

    iget v3, p0, Lflipboard/util/Format$CharBuf;->b:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method
