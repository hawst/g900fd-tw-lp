.class final Lflipboard/util/SocialHelper$1;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SocialHelper.java"


# instance fields
.field final synthetic a:Lflipboard/objs/FeedItem;

.field final synthetic b:Z

.field final synthetic c:Lflipboard/objs/ConfigService;

.field final synthetic d:Lflipboard/activities/FlipboardActivity;

.field final synthetic f:Lflipboard/service/Section;


# direct methods
.method constructor <init>(Lflipboard/objs/FeedItem;Lflipboard/objs/ConfigService;Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V
    .locals 1

    .prologue
    .line 139
    iput-object p1, p0, Lflipboard/util/SocialHelper$1;->a:Lflipboard/objs/FeedItem;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/util/SocialHelper$1;->b:Z

    iput-object p2, p0, Lflipboard/util/SocialHelper$1;->c:Lflipboard/objs/ConfigService;

    iput-object p3, p0, Lflipboard/util/SocialHelper$1;->d:Lflipboard/activities/FlipboardActivity;

    iput-object p4, p0, Lflipboard/util/SocialHelper$1;->f:Lflipboard/service/Section;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;)V
    .locals 2

    .prologue
    .line 146
    iget-object v1, p0, Lflipboard/util/SocialHelper$1;->a:Lflipboard/objs/FeedItem;

    iget-boolean v0, p0, Lflipboard/util/SocialHelper$1;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lflipboard/objs/FeedItem;->a(Z)V

    .line 147
    return-void

    .line 146
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 4

    .prologue
    .line 152
    iget-object v0, p0, Lflipboard/util/SocialHelper$1;->c:Lflipboard/objs/ConfigService;

    iget-object v0, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lflipboard/util/SocialHelper$1;->d:Lflipboard/activities/FlipboardActivity;

    invoke-static {v0}, Lflipboard/util/ActivityUtil;->a(Landroid/app/Activity;)V

    .line 176
    :goto_0
    return-void

    .line 156
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lflipboard/util/SocialHelper$1;->d:Lflipboard/activities/FlipboardActivity;

    const-class v2, Lflipboard/activities/ServiceLoginActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 157
    const-string v1, "service"

    iget-object v2, p0, Lflipboard/util/SocialHelper$1;->c:Lflipboard/objs/ConfigService;

    iget-object v2, v2, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    const-string v1, "viewSectionAfterSuccess"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 159
    const-string v1, "extra_content_discovery_from_source"

    const-string v2, "usageSocialLoginOriginSocialAction"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    iget-object v1, p0, Lflipboard/util/SocialHelper$1;->d:Lflipboard/activities/FlipboardActivity;

    const/16 v2, 0xc9

    new-instance v3, Lflipboard/util/SocialHelper$1$1;

    invoke-direct {v3, p0}, Lflipboard/util/SocialHelper$1$1;-><init>(Lflipboard/util/SocialHelper$1;)V

    invoke-virtual {v1, v0, v2, v3}, Lflipboard/activities/FlipboardActivity;->a(Landroid/content/Intent;ILflipboard/activities/FlipboardActivity$ActivityResultListener;)V

    goto :goto_0
.end method

.method public final d(Landroid/support/v4/app/DialogFragment;)V
    .locals 2

    .prologue
    .line 180
    iget-object v1, p0, Lflipboard/util/SocialHelper$1;->a:Lflipboard/objs/FeedItem;

    iget-boolean v0, p0, Lflipboard/util/SocialHelper$1;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lflipboard/objs/FeedItem;->a(Z)V

    .line 181
    return-void

    .line 180
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
