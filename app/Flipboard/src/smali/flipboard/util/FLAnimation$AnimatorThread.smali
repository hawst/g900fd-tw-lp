.class Lflipboard/util/FLAnimation$AnimatorThread;
.super Ljava/lang/Thread;
.source "FLAnimation.java"


# instance fields
.field final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/util/FLAnimation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 76
    const-string v0, "FLAnimatorThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/util/FLAnimation$AnimatorThread;->a:Ljava/util/ArrayList;

    .line 77
    invoke-virtual {p0}, Lflipboard/util/FLAnimation$AnimatorThread;->start()V

    .line 78
    return-void
.end method


# virtual methods
.method final declared-synchronized a(Lflipboard/util/FLAnimation;)V
    .locals 4

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lflipboard/util/FLAnimation$AnimatorThread;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_1

    .line 83
    iget-object v0, p0, Lflipboard/util/FLAnimation$AnimatorThread;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/util/FLAnimation;

    .line 84
    invoke-static {v0}, Lflipboard/util/FLAnimation;->a(Lflipboard/util/FLAnimation;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {p1}, Lflipboard/util/FLAnimation;->a(Lflipboard/util/FLAnimation;)Ljava/lang/Object;

    move-result-object v3

    if-ne v2, v3, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v0, v2, :cond_0

    .line 85
    iget-object v0, p0, Lflipboard/util/FLAnimation$AnimatorThread;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    move v0, v1

    .line 87
    goto :goto_0

    .line 88
    :cond_1
    monitor-exit p0

    return-void

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b(Lflipboard/util/FLAnimation;)V
    .locals 3

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lflipboard/util/FLAnimation;->b(Lflipboard/util/FLAnimation;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "animation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already running"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 96
    :cond_0
    :try_start_1
    invoke-static {p1}, Lflipboard/util/FLAnimation;->c(Lflipboard/util/FLAnimation;)Z

    .line 97
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lflipboard/util/FLAnimation;->a(Lflipboard/util/FLAnimation;J)J

    .line 99
    iget-object v0, p0, Lflipboard/util/FLAnimation$AnimatorThread;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 101
    monitor-exit p0

    return-void
.end method

.method public run()V
    .locals 5

    .prologue
    .line 105
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 112
    :goto_0
    :try_start_0
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v3, Lflipboard/util/FLAnimation$AnimatorThread$1;

    invoke-direct {v3, p0, v0, v1}, Lflipboard/util/FLAnimation$AnimatorThread$1;-><init>(Lflipboard/util/FLAnimation$AnimatorThread;J)V

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 134
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 135
    :try_start_2
    iget-object v2, p0, Lflipboard/util/FLAnimation$AnimatorThread;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 137
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    .line 145
    :cond_0
    :goto_1
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0

    throw v0
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catch_0
    move-exception v0

    .line 147
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 152
    return-void

    .line 139
    :cond_1
    const-wide/16 v2, 0x21

    add-long/2addr v2, v0

    .line 140
    :try_start_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 141
    cmp-long v4, v2, v0

    if-lez v4, :cond_0

    .line 142
    sub-long/2addr v2, v0

    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 151
    :catchall_1
    move-exception v0

    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    throw v0
.end method
