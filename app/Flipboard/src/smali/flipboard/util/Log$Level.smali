.class public final enum Lflipboard/util/Log$Level;
.super Ljava/lang/Enum;
.source "Log.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/util/Log$Level;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/util/Log$Level;

.field public static final enum b:Lflipboard/util/Log$Level;

.field public static final enum c:Lflipboard/util/Log$Level;

.field public static final enum d:Lflipboard/util/Log$Level;

.field public static final enum e:Lflipboard/util/Log$Level;

.field private static final synthetic f:[Lflipboard/util/Log$Level;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 63
    new-instance v0, Lflipboard/util/Log$Level;

    const-string v1, "CUSTOM"

    invoke-direct {v0, v1, v2}, Lflipboard/util/Log$Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/util/Log$Level;->a:Lflipboard/util/Log$Level;

    new-instance v0, Lflipboard/util/Log$Level;

    const-string v1, "DEBUG"

    invoke-direct {v0, v1, v3}, Lflipboard/util/Log$Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/util/Log$Level;->b:Lflipboard/util/Log$Level;

    new-instance v0, Lflipboard/util/Log$Level;

    const-string v1, "INFO"

    invoke-direct {v0, v1, v4}, Lflipboard/util/Log$Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/util/Log$Level;->c:Lflipboard/util/Log$Level;

    new-instance v0, Lflipboard/util/Log$Level;

    const-string v1, "WARN"

    invoke-direct {v0, v1, v5}, Lflipboard/util/Log$Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/util/Log$Level;->d:Lflipboard/util/Log$Level;

    new-instance v0, Lflipboard/util/Log$Level;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v6}, Lflipboard/util/Log$Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/util/Log$Level;->e:Lflipboard/util/Log$Level;

    const/4 v0, 0x5

    new-array v0, v0, [Lflipboard/util/Log$Level;

    sget-object v1, Lflipboard/util/Log$Level;->a:Lflipboard/util/Log$Level;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/util/Log$Level;->b:Lflipboard/util/Log$Level;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/util/Log$Level;->c:Lflipboard/util/Log$Level;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/util/Log$Level;->d:Lflipboard/util/Log$Level;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/util/Log$Level;->e:Lflipboard/util/Log$Level;

    aput-object v1, v0, v6

    sput-object v0, Lflipboard/util/Log$Level;->f:[Lflipboard/util/Log$Level;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/util/Log$Level;
    .locals 1

    .prologue
    .line 63
    const-class v0, Lflipboard/util/Log$Level;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/util/Log$Level;

    return-object v0
.end method

.method public static values()[Lflipboard/util/Log$Level;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lflipboard/util/Log$Level;->f:[Lflipboard/util/Log$Level;

    invoke-virtual {v0}, [Lflipboard/util/Log$Level;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/util/Log$Level;

    return-object v0
.end method
