.class final Lflipboard/util/TOCBuilder$3;
.super Ljava/lang/Object;
.source "TOCBuilder.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/activities/FlipboardActivity;

.field final synthetic b:Lflipboard/model/ConfigFirstLaunch;

.field final synthetic c:Lflipboard/util/TOCBuilder$TOCBuilderObserver;


# direct methods
.method constructor <init>(Lflipboard/activities/FlipboardActivity;Lflipboard/model/ConfigFirstLaunch;Lflipboard/util/TOCBuilder$TOCBuilderObserver;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lflipboard/util/TOCBuilder$3;->a:Lflipboard/activities/FlipboardActivity;

    iput-object p2, p0, Lflipboard/util/TOCBuilder$3;->b:Lflipboard/model/ConfigFirstLaunch;

    iput-object p3, p0, Lflipboard/util/TOCBuilder$3;->c:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 182
    :try_start_0
    iget-object v0, p0, Lflipboard/util/TOCBuilder$3;->a:Lflipboard/activities/FlipboardActivity;

    iget-object v1, p0, Lflipboard/util/TOCBuilder$3;->b:Lflipboard/model/ConfigFirstLaunch;

    iget-object v2, p0, Lflipboard/util/TOCBuilder$3;->c:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    invoke-static {v0, v1, v2}, Lflipboard/util/TOCBuilder;->b(Lflipboard/activities/FlipboardActivity;Lflipboard/model/ConfigFirstLaunch;Lflipboard/util/TOCBuilder$TOCBuilderObserver;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 193
    :goto_0
    return-void

    .line 184
    :catch_0
    move-exception v0

    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 186
    const v1, 0x7f0d0105

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 187
    const-string v1, "Cannot build Flipboard"

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 188
    const v1, 0x7f0d023f

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 189
    iget-object v1, p0, Lflipboard/util/TOCBuilder$3;->a:Lflipboard/activities/FlipboardActivity;

    iget-object v1, v1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "governor"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 190
    :catch_1
    move-exception v0

    .line 191
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
