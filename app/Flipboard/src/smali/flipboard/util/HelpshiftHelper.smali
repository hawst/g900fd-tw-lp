.class public Lflipboard/util/HelpshiftHelper;
.super Ljava/lang/Object;
.source "HelpshiftHelper.java"


# static fields
.field private static a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    sput-boolean v0, Lflipboard/util/HelpshiftHelper;->a:Z

    return-void
.end method

.method public static a()V
    .locals 4

    .prologue
    .line 79
    sget-boolean v0, Lflipboard/util/HelpshiftHelper;->a:Z

    if-nez v0, :cond_0

    invoke-static {}, Lflipboard/util/HelpshiftHelper;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const-string v1, "72eda0007e15c46844c73b8e124523d7"

    const-string v2, "flipboard.helpshift.com"

    const-string v3, "flipboard_platform_20121115193321241-14b6ae51a7202f7"

    invoke-static {v0, v1, v2, v3}, Lcom/helpshift/Helpshift;->a(Landroid/app/Application;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    new-instance v0, Lflipboard/util/HelpshiftHelper$1;

    invoke-direct {v0}, Lflipboard/util/HelpshiftHelper$1;-><init>()V

    invoke-static {v0}, Lcom/helpshift/Helpshift;->a(Lcom/helpshift/HSCallable;)V

    .line 89
    const/4 v0, 0x1

    sput-boolean v0, Lflipboard/util/HelpshiftHelper;->a:Z

    .line 91
    :cond_0
    return-void
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 182
    if-nez p1, :cond_2

    invoke-static {}, Lflipboard/util/HelpshiftHelper;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 183
    invoke-static {}, Lflipboard/util/HelpshiftHelper;->a()V

    .line 184
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 185
    if-eqz v0, :cond_0

    .line 186
    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 187
    if-eqz v0, :cond_0

    .line 189
    iget-object v1, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/helpshift/Helpshift;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_0
    invoke-static {p0}, Lcom/helpshift/Helpshift;->a(Landroid/app/Activity;)V

    .line 200
    :cond_1
    :goto_0
    return-void

    .line 194
    :cond_2
    if-eqz p1, :cond_3

    .line 195
    :goto_1
    if-eqz p1, :cond_1

    .line 197
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2, p1}, Lflipboard/service/FlipboardManager;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 194
    :cond_3
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object p1, v0, Lflipboard/model/ConfigSetting;->HelpURLString:Ljava/lang/String;

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 207
    invoke-static {}, Lflipboard/util/HelpshiftHelper;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "flipboard.app.helpshift_gcm_on_registered"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 209
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 211
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 220
    invoke-static {}, Lflipboard/util/HelpshiftHelper;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "helpshift"

    const-string v1, "origin"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "flipboard.app.helpshift_gcm_on_message_received"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 222
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 223
    const/4 v0, 0x1

    .line 225
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b()Ljava/util/HashMap;
    .locals 6

    .prologue
    .line 30
    new-instance v1, Ljava/util/HashMap;

    const/16 v0, 0x9

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->H()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Bundle"

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "bundled"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    const-string v3, "samsung"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Samsung"

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_2

    const-string v0, "China"

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    sget-boolean v0, Lflipboard/service/FlipboardManager;->p:Z

    if-eqz v0, :cond_3

    const-string v0, "Nook"

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    sget-boolean v0, Lflipboard/service/FlipboardManager;->q:Z

    if-eqz v0, :cond_4

    const-string v0, "Kindle"

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    sget-object v0, Lflipboard/util/AndroidUtil;->c:Landroid/util/DisplayMetrics;

    const-string v3, "Display Info"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", densityScale="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "Font Scale"

    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    invoke-static {v0}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v3, "Locale"

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v3}, Lflipboard/service/FlipboardManager;->l()Lflipboard/service/FlipboardManager$RootScreenStyle;

    move-result-object v3

    invoke-virtual {v3}, Lflipboard/service/FlipboardManager$RootScreenStyle;->name()Ljava/lang/String;

    move-result-object v3

    const-string v4, "UI Style"

    invoke-virtual {v1, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v4, "content_guide_locale"

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "Content Guide Edition"

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "UDID"

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "TUUID"

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    if-eqz v3, :cond_7

    iget-object v0, v3, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/helpshift/Helpshift;->a(Ljava/lang/String;)V

    const-string v0, "Accounts"

    invoke-virtual {v3}, Lflipboard/service/User;->w()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v3, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const-string v4, "Number of Subscriptions"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v4, 0x28

    if-lt v0, v4, :cond_6

    const-string v0, "power user"

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    const-string v0, "Number of Globally Muted Authors"

    invoke-virtual {v3}, Lflipboard/service/User;->k()Lflipboard/objs/UserState;

    move-result-object v4

    iget-object v4, v4, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v4, v4, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v4, v4, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3}, Lflipboard/service/User;->p()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_9

    const/4 v0, 0x0

    :goto_0
    const-string v4, "Number of Magazines"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v3, Lflipboard/service/User;->p:Ljava/util/List;

    if-eqz v0, :cond_7

    const-string v0, "Number of Contributor Magazines"

    iget-object v3, v3, Lflipboard/service/User;->p:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "hs-tags"

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    return-object v1

    :cond_9
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method private static c()Z
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/model/ConfigSetting;->EnableHelpshift:Z

    return v0
.end method
