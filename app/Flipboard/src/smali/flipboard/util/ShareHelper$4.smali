.class final Lflipboard/util/ShareHelper$4;
.super Ljava/lang/Object;
.source "ShareHelper.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lflipboard/service/FlipboardManager;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 552
    iput-object p1, p0, Lflipboard/util/ShareHelper$4;->a:Lflipboard/service/FlipboardManager;

    iput-object p2, p0, Lflipboard/util/ShareHelper$4;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 3

    .prologue
    .line 557
    sget-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 558
    iget-object v0, p0, Lflipboard/util/ShareHelper$4;->a:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/util/ShareHelper$4;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->h(Ljava/lang/String;)Lflipboard/objs/Magazine;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, v0, Lflipboard/service/User;->n:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v1, v1, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->g(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lflipboard/service/User;->b:Lflipboard/util/Log;

    :cond_0
    sget-object v1, Lflipboard/service/User$Message;->i:Lflipboard/service/User$Message;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v1, v0, Lflipboard/service/User;->c:Lflipboard/service/User$MagazineChangeListener;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lflipboard/service/User;->c:Lflipboard/service/User$MagazineChangeListener;

    invoke-interface {v0}, Lflipboard/service/User$MagazineChangeListener;->a()V

    .line 560
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 565
    sget-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 567
    return-void
.end method
