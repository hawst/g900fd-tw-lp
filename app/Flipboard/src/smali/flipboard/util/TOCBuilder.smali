.class public Lflipboard/util/TOCBuilder;
.super Ljava/lang/Object;
.source "TOCBuilder.java"


# static fields
.field public static a:Lflipboard/util/Log;

.field public static b:Ljava/lang/String;

.field private static c:J

.field private static d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-string v0, "tocBuilder"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/util/TOCBuilder;->a:Lflipboard/util/Log;

    .line 45
    const-string v0, "dialog_building_toc"

    sput-object v0, Lflipboard/util/TOCBuilder;->b:Ljava/lang/String;

    return-void
.end method

.method static synthetic a()J
    .locals 2

    .prologue
    .line 41
    sget-wide v0, Lflipboard/util/TOCBuilder;->c:J

    return-wide v0
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Lflipboard/model/ConfigFirstLaunch;Lflipboard/util/TOCBuilder$TOCBuilderObserver;)V
    .locals 7

    .prologue
    const-wide/16 v2, 0x1f4

    .line 143
    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 144
    const/4 v0, 0x0

    sput-boolean v0, Lflipboard/util/TOCBuilder;->d:Z

    .line 147
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    new-instance v0, Lflipboard/util/TOCBuilder$1;

    invoke-direct {v0, p0}, Lflipboard/util/TOCBuilder$1;-><init>(Lflipboard/activities/FlipboardActivity;)V

    invoke-virtual {v4, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 214
    :goto_0
    return-void

    .line 164
    :cond_0
    invoke-virtual {v4}, Lflipboard/service/FlipboardManager;->w()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, v4, Lflipboard/service/FlipboardManager;->az:Z

    if-eqz v0, :cond_1

    .line 165
    new-instance v0, Lflipboard/util/TOCBuilder$2;

    invoke-direct {v0, p0}, Lflipboard/util/TOCBuilder$2;-><init>(Lflipboard/activities/FlipboardActivity;)V

    invoke-virtual {v4, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 175
    :cond_1
    invoke-virtual {p0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lflipboard/util/TOCBuilder;->b:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    new-instance v5, Lflipboard/util/TOCBuilder$3;

    invoke-direct {v5, p0, p1, p2}, Lflipboard/util/TOCBuilder$3;-><init>(Lflipboard/activities/FlipboardActivity;Lflipboard/model/ConfigFirstLaunch;Lflipboard/util/TOCBuilder$TOCBuilderObserver;)V

    .line 196
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "fresh_user"

    const/4 v6, 0x1

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 198
    invoke-virtual {v4}, Lflipboard/service/FlipboardManager;->w()Z

    move-result v0

    if-nez v0, :cond_2

    .line 199
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 200
    iget-object v0, v4, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    new-instance v1, Lflipboard/util/TOCBuilder$4;

    invoke-direct {v1, v4, v5, p2}, Lflipboard/util/TOCBuilder$4;-><init>(Lflipboard/service/FlipboardManager;Ljava/lang/Runnable;Lflipboard/util/TOCBuilder$TOCBuilderObserver;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto :goto_0

    .line 212
    :cond_2
    const-string v0, "TOCBuilder:buildFlipboardWithDefaultCategories"

    invoke-virtual {v4, v0, v5}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Lflipboard/model/ConfigFirstLaunch;Ljava/util/Set;Lflipboard/util/TOCBuilder$TOCBuilderObserver;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/activities/FlipboardActivity;",
            "Lflipboard/model/ConfigFirstLaunch;",
            "Ljava/util/Set",
            "<",
            "Lflipboard/model/FirstRunSection;",
            ">;",
            "Lflipboard/util/TOCBuilder$TOCBuilderObserver;",
            ")V"
        }
    .end annotation

    .prologue
    .line 54
    sget-object v10, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 55
    iget-object v11, v10, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 56
    const/4 v2, 0x0

    sput-boolean v2, Lflipboard/util/TOCBuilder;->d:Z

    .line 57
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sput-wide v2, Lflipboard/util/TOCBuilder;->c:J

    .line 59
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 61
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v2, v2, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v2, :cond_1

    .line 62
    move-object/from16 v0, p1

    iget-object v2, v0, Lflipboard/model/ConfigFirstLaunch;->SectionsToChooseFrom:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/model/FirstRunSection;

    .line 63
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 64
    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 68
    :cond_1
    move-object/from16 v0, p2

    invoke-interface {v12, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 73
    :cond_2
    const/4 v3, 0x2

    .line 74
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v2, v2, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v2, :cond_3

    move-object/from16 v0, p1

    iget-object v2, v0, Lflipboard/model/ConfigFirstLaunch;->TopicPickerDefaultSections:Ljava/util/List;

    if-nez v2, :cond_4

    .line 76
    :cond_3
    move-object/from16 v0, p1

    iget-object v2, v0, Lflipboard/model/ConfigFirstLaunch;->DefaultSections:Ljava/util/List;

    .line 80
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/model/FirstRunSection;

    .line 81
    iget-object v4, v2, Lflipboard/model/FirstRunSection;->remoteid:Ljava/lang/String;

    invoke-virtual {v10, v4}, Lflipboard/service/FlipboardManager;->d(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 83
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v4

    if-gt v3, v4, :cond_5

    move v4, v3

    .line 84
    :goto_3
    invoke-interface {v12, v4, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 85
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    .line 86
    goto :goto_2

    .line 78
    :cond_4
    move-object/from16 v0, p1

    iget-object v2, v0, Lflipboard/model/ConfigFirstLaunch;->TopicPickerDefaultSections:Ljava/util/List;

    goto :goto_1

    .line 83
    :cond_5
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v4

    goto :goto_3

    .line 88
    :cond_6
    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 93
    :cond_7
    invoke-virtual {v10}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v2

    iget-object v2, v2, Lflipboard/model/ConfigSetting;->ConditionalFirstLaunchSections:Ljava/util/List;

    .line 94
    if-eqz v2, :cond_f

    .line 95
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_8
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/model/ConditionalSection;

    .line 96
    iget-object v5, v2, Lflipboard/model/ConditionalSection;->conditions:Lflipboard/model/ConditionalSection$Conditions;

    .line 97
    if-nez v5, :cond_d

    const/4 v3, 0x1

    .line 99
    :goto_5
    if-nez v3, :cond_9

    iget-object v6, v5, Lflipboard/model/ConditionalSection$Conditions;->devices:Ljava/util/List;

    if-eqz v6, :cond_9

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    if-eqz v6, :cond_9

    .line 100
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iget-object v6, v5, Lflipboard/model/ConditionalSection$Conditions;->devices:Ljava/util/List;

    invoke-static {v3, v6}, Lflipboard/util/TOCBuilder;->a(Ljava/lang/String;Ljava/util/Collection;)Z

    move-result v3

    .line 102
    :cond_9
    if-nez v3, :cond_a

    iget-object v6, v5, Lflipboard/model/ConditionalSection$Conditions;->brands:Ljava/util/List;

    if-eqz v6, :cond_a

    sget-object v6, Landroid/os/Build;->BRAND:Ljava/lang/String;

    if-eqz v6, :cond_a

    .line 103
    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    iget-object v6, v5, Lflipboard/model/ConditionalSection$Conditions;->brands:Ljava/util/List;

    invoke-static {v3, v6}, Lflipboard/util/TOCBuilder;->a(Ljava/lang/String;Ljava/util/Collection;)Z

    move-result v3

    .line 105
    :cond_a
    if-nez v3, :cond_b

    iget-object v6, v5, Lflipboard/model/ConditionalSection$Conditions;->locales:Ljava/util/List;

    if-eqz v6, :cond_b

    .line 106
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v6, v5, Lflipboard/model/ConditionalSection$Conditions;->locales:Ljava/util/List;

    invoke-static {v3, v6}, Lflipboard/util/TOCBuilder;->a(Ljava/lang/String;Ljava/util/Collection;)Z

    move-result v3

    .line 108
    :cond_b
    if-nez v3, :cond_c

    iget-object v6, v5, Lflipboard/model/ConditionalSection$Conditions;->manufacturers:Ljava/util/List;

    if-eqz v6, :cond_c

    sget-object v6, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    if-eqz v6, :cond_c

    .line 109
    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iget-object v5, v5, Lflipboard/model/ConditionalSection$Conditions;->manufacturers:Ljava/util/List;

    invoke-static {v3, v5}, Lflipboard/util/TOCBuilder;->a(Ljava/lang/String;Ljava/util/Collection;)Z

    move-result v3

    .line 111
    :cond_c
    if-eqz v3, :cond_8

    .line 112
    iget v3, v2, Lflipboard/model/ConditionalSection;->index:I

    if-ltz v3, :cond_e

    .line 113
    iget v3, v2, Lflipboard/model/ConditionalSection;->index:I

    iget-object v2, v2, Lflipboard/model/ConditionalSection;->section:Lflipboard/model/FirstRunSection;

    invoke-interface {v12, v3, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_4

    .line 97
    :cond_d
    const/4 v3, 0x0

    goto :goto_5

    .line 115
    :cond_e
    iget-object v2, v2, Lflipboard/model/ConditionalSection;->section:Lflipboard/model/FirstRunSection;

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 120
    :cond_f
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 121
    const/4 v2, 0x0

    .line 122
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    move v9, v2

    :goto_6
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Lflipboard/model/FirstRunSection;

    .line 123
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v9, v2, :cond_10

    const/4 v2, 0x1

    move v8, v2

    .line 124
    :goto_7
    iget-object v2, v5, Lflipboard/model/FirstRunSection;->remoteid:Ljava/lang/String;

    invoke-virtual {v10, v2}, Lflipboard/service/FlipboardManager;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 125
    new-instance v2, Lflipboard/service/Section;

    iget-object v3, v5, Lflipboard/model/FirstRunSection;->remoteid:Ljava/lang/String;

    iget-object v4, v5, Lflipboard/model/FirstRunSection;->title:Ljava/lang/String;

    iget-object v5, v5, Lflipboard/model/FirstRunSection;->remoteid:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 126
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lflipboard/service/Section;->a(Z)V

    .line 127
    const/4 v3, 0x0

    sget-object v4, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-virtual {v4}, Lflipboard/objs/UsageEventV2$EventCategory;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v2, v3, v8, v4}, Lflipboard/service/User;->a(Lflipboard/service/Section;ZZLjava/lang/String;)Lflipboard/service/Section;

    .line 135
    :goto_8
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    .line 136
    goto :goto_6

    .line 123
    :cond_10
    const/4 v2, 0x0

    move v8, v2

    goto :goto_7

    .line 129
    :cond_11
    new-instance v2, Lflipboard/service/Section;

    iget-object v3, v5, Lflipboard/model/FirstRunSection;->title:Ljava/lang/String;

    invoke-direct {v2, v5, v3}, Lflipboard/service/Section;-><init>(Lflipboard/model/FirstRunSection;Ljava/lang/String;)V

    .line 130
    const/4 v3, 0x1

    sget-object v4, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-virtual {v4}, Lflipboard/objs/UsageEventV2$EventCategory;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v2, v3, v8, v4}, Lflipboard/service/User;->a(Lflipboard/service/Section;ZZLjava/lang/String;)Lflipboard/service/Section;

    move-result-object v2

    .line 133
    invoke-interface {v13, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 138
    :cond_12
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-static {p0, v13, v0, v2}, Lflipboard/util/TOCBuilder;->a(Lflipboard/activities/FlipboardActivity;Ljava/util/List;Lflipboard/util/TOCBuilder$TOCBuilderObserver;Z)V

    .line 139
    return-void
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Ljava/util/List;Lflipboard/util/TOCBuilder$TOCBuilderObserver;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/activities/FlipboardActivity;",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;",
            "Lflipboard/util/TOCBuilder$TOCBuilderObserver;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 296
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 297
    iget-object v3, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 300
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 301
    const/4 v5, 0x1

    iput-boolean v5, v0, Lflipboard/service/Section;->B:Z

    goto :goto_0

    .line 305
    :cond_0
    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v0

    .line 306
    invoke-virtual {v0, v3, v2, v2}, Lflipboard/service/Flap;->a(Lflipboard/service/User;ZZ)Lflipboard/service/Flap$UpdateRequest;

    move-result-object v4

    .line 308
    if-eqz p2, :cond_1

    .line 310
    iget-object v0, v3, Lflipboard/service/User;->h:Lflipboard/service/Section;

    invoke-virtual {v0, p2}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    .line 312
    :cond_1
    iget-object v0, v3, Lflipboard/service/User;->h:Lflipboard/service/Section;

    invoke-virtual {v4, v0, v6, v6}, Lflipboard/service/Flap$UpdateRequest;->a(Lflipboard/service/Section;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 314
    if-nez p3, :cond_4

    .line 315
    const/4 v0, 0x2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v1, v0

    .line 318
    :goto_1
    if-gt v2, v1, :cond_3

    .line 319
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 320
    invoke-virtual {v4, v0, v6, v6}, Lflipboard/service/Flap$UpdateRequest;->a(Lflipboard/service/Section;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 321
    if-eqz p2, :cond_2

    .line 322
    invoke-virtual {v0, p2}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    .line 318
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 326
    :cond_3
    invoke-virtual {v3}, Lflipboard/service/User;->g()V

    .line 327
    invoke-virtual {v4, p1}, Lflipboard/service/Flap$UpdateRequest;->a(Ljava/util/List;)V

    .line 328
    new-instance v0, Lflipboard/util/TOCBuilder$6;

    invoke-direct {v0, p0}, Lflipboard/util/TOCBuilder$6;-><init>(Lflipboard/activities/FlipboardActivity;)V

    iput-object v0, v4, Lflipboard/service/Flap$UpdateRequest;->j:Lflipboard/util/Callback;

    .line 335
    invoke-virtual {v4}, Lflipboard/service/Flap$UpdateRequest;->c()V

    .line 336
    return-void

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 340
    if-eqz p0, :cond_1

    .line 341
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 342
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    const/4 v0, 0x1

    .line 347
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lflipboard/activities/FlipboardActivity;Lflipboard/model/ConfigFirstLaunch;Lflipboard/util/TOCBuilder$TOCBuilderObserver;)V
    .locals 12

    .prologue
    .line 41
    sget-object v8, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v9, v8, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const/4 v0, 0x0

    sput-boolean v0, Lflipboard/util/TOCBuilder;->d:Z

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sput-wide v0, Lflipboard/util/TOCBuilder;->c:J

    const/4 v0, 0x0

    iget-object v1, p1, Lflipboard/model/ConfigFirstLaunch;->SectionsToChooseFrom:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v7, v0

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lflipboard/model/FirstRunSection;

    const/4 v0, 0x2

    if-ne v7, v0, :cond_1

    iget-object v0, p1, Lflipboard/model/ConfigFirstLaunch;->DefaultSections:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lflipboard/model/FirstRunSection;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v3, Lflipboard/model/FirstRunSection;->remoteid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lflipboard/service/Section;

    iget-object v1, v3, Lflipboard/model/FirstRunSection;->remoteid:Ljava/lang/String;

    iget-object v2, v3, Lflipboard/model/FirstRunSection;->title:Ljava/lang/String;

    iget-object v3, v3, Lflipboard/model/FirstRunSection;->remoteid:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->a(Z)V

    const/4 v1, 0x0

    const/4 v2, 0x1

    sget-object v3, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-virtual {v3}, Lflipboard/objs/UsageEventV2$EventCategory;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v0, v1, v2, v3}, Lflipboard/service/User;->a(Lflipboard/service/Section;ZZLjava/lang/String;)Lflipboard/service/Section;

    goto :goto_1

    :cond_1
    iget-boolean v0, v6, Lflipboard/model/FirstRunSection;->selectedIfNoCategoryPicker:Z

    if-eqz v0, :cond_4

    new-instance v0, Lflipboard/service/Section;

    iget-object v1, v6, Lflipboard/model/FirstRunSection;->title:Ljava/lang/String;

    invoke-direct {v0, v6, v1}, Lflipboard/service/Section;-><init>(Lflipboard/model/FirstRunSection;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x1

    sget-object v3, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-virtual {v3}, Lflipboard/objs/UsageEventV2$EventCategory;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v0, v1, v2, v3}, Lflipboard/service/User;->a(Lflipboard/service/Section;ZZLjava/lang/String;)Lflipboard/service/Section;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/service/Section;->B:Z

    add-int/lit8 v0, v7, 0x1

    :goto_2
    move v7, v0

    goto :goto_0

    :cond_2
    iget-object v2, v9, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-virtual {v8}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v9, v1, v3}, Lflipboard/service/Flap;->a(Lflipboard/service/User;ZZ)Lflipboard/service/Flap$UpdateRequest;

    move-result-object v3

    iget-object v0, v9, Lflipboard/service/User;->h:Lflipboard/service/Section;

    invoke-virtual {v0, p2}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    iget-object v0, v9, Lflipboard/service/User;->h:Lflipboard/service/Section;

    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v1, v4}, Lflipboard/service/Flap$UpdateRequest;->a(Lflipboard/service/Section;Ljava/lang/String;Landroid/os/Bundle;)Z

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    const/4 v0, 0x2

    if-ge v1, v0, :cond_3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v4, v5}, Lflipboard/service/Flap$UpdateRequest;->a(Lflipboard/service/Section;Ljava/lang/String;Landroid/os/Bundle;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    invoke-virtual {v9}, Lflipboard/service/User;->g()V

    invoke-virtual {v3, v2}, Lflipboard/service/Flap$UpdateRequest;->a(Ljava/util/List;)V

    new-instance v0, Lflipboard/util/TOCBuilder$5;

    invoke-direct {v0, v8, p0}, Lflipboard/util/TOCBuilder$5;-><init>(Lflipboard/service/FlipboardManager;Lflipboard/activities/FlipboardActivity;)V

    iput-object v0, v3, Lflipboard/service/Flap$UpdateRequest;->j:Lflipboard/util/Callback;

    invoke-virtual {v3}, Lflipboard/service/Flap$UpdateRequest;->c()V

    const-string v0, "CategorySelector Done"

    const-string v1, "numSelections"

    iget-object v2, v9, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void

    :cond_4
    move v0, v7

    goto :goto_2
.end method

.method static synthetic b()Z
    .locals 1

    .prologue
    .line 41
    sget-boolean v0, Lflipboard/util/TOCBuilder;->d:Z

    return v0
.end method

.method static synthetic c()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x1

    sput-boolean v0, Lflipboard/util/TOCBuilder;->d:Z

    return v0
.end method
