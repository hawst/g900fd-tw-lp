.class public Lflipboard/util/GooglePlusSignInClient;
.super Ljava/lang/Object;
.source "GooglePlusSignInClient.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;


# instance fields
.field a:I

.field b:I

.field private c:Lflipboard/util/Log;

.field private d:Landroid/content/Context;

.field private e:Lcom/google/android/gms/plus/PlusClient;

.field private f:I

.field private g:Lcom/google/android/gms/common/ConnectionResult;

.field private h:Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-string v0, "GooglePlusSignInClient"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    iput-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->c:Lflipboard/util/Log;

    .line 77
    iput v3, p0, Lflipboard/util/GooglePlusSignInClient;->a:I

    .line 78
    iput v3, p0, Lflipboard/util/GooglePlusSignInClient;->b:I

    .line 95
    iput-object p1, p0, Lflipboard/util/GooglePlusSignInClient;->d:Landroid/content/Context;

    .line 96
    iput-object p3, p0, Lflipboard/util/GooglePlusSignInClient;->h:Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;

    .line 97
    iput p2, p0, Lflipboard/util/GooglePlusSignInClient;->f:I

    .line 99
    packed-switch p2, :pswitch_data_0

    .line 124
    :goto_0
    return-void

    .line 101
    :pswitch_0
    new-instance v0, Lcom/google/android/gms/plus/PlusClient$Builder;

    invoke-direct {v0, p1, p0, p0}, Lcom/google/android/gms/plus/PlusClient$Builder;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "profile"

    aput-object v2, v1, v3

    const-string v2, "https://www.googleapis.com/auth/plus.profile.emails.read"

    aput-object v2, v1, v4

    .line 102
    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/PlusClient$Builder;->a([Ljava/lang/String;)Lcom/google/android/gms/plus/PlusClient$Builder;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient$Builder;->a()Lcom/google/android/gms/plus/PlusClient;

    move-result-object v0

    iput-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    goto :goto_0

    .line 109
    :pswitch_1
    new-instance v0, Lcom/google/android/gms/plus/PlusClient$Builder;

    invoke-direct {v0, p1, p0, p0}, Lcom/google/android/gms/plus/PlusClient$Builder;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "https://www.googleapis.com/auth/plus.login"

    aput-object v2, v1, v3

    const-string v2, "profile"

    aput-object v2, v1, v4

    const-string v2, "https://www.googleapis.com/auth/plus.stream.read"

    aput-object v2, v1, v5

    const/4 v2, 0x3

    const-string v3, "https://www.googleapis.com/auth/plus.stream.write"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "https://www.googleapis.com/auth/plus.circles.read"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "https://www.googleapis.com/auth/plus.stream.write"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "https://www.googleapis.com/auth/plus.circles.write"

    aput-object v3, v1, v2

    .line 110
    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/PlusClient$Builder;->a([Ljava/lang/String;)Lcom/google/android/gms/plus/PlusClient$Builder;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient$Builder;->a()Lcom/google/android/gms/plus/PlusClient;

    move-result-object v0

    iput-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    goto :goto_0

    .line 119
    :pswitch_2
    new-instance v0, Lcom/google/android/gms/plus/PlusClient$Builder;

    invoke-direct {v0, p1, p0, p0}, Lcom/google/android/gms/plus/PlusClient$Builder;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "https://www.googleapis.com/auth/plus.login"

    aput-object v2, v1, v3

    const-string v2, "https://gdata.youtube.com"

    aput-object v2, v1, v4

    .line 120
    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/PlusClient$Builder;->a([Ljava/lang/String;)Lcom/google/android/gms/plus/PlusClient$Builder;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient$Builder;->a()Lcom/google/android/gms/plus/PlusClient;

    move-result-object v0

    iput-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    goto :goto_0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2

    .prologue
    .line 168
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    :try_start_0
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->d:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/ConnectionResult;->a(Landroid/app/Activity;)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    :cond_0
    :goto_0
    iput-object p1, p0, Lflipboard/util/GooglePlusSignInClient;->g:Lcom/google/android/gms/common/ConnectionResult;

    .line 186
    return-void

    .line 173
    :catch_0
    move-exception v0

    iget v0, p0, Lflipboard/util/GooglePlusSignInClient;->a:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_1

    .line 174
    iget v0, p0, Lflipboard/util/GooglePlusSignInClient;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/util/GooglePlusSignInClient;->a:I

    .line 175
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    iget-object v0, v0, Lcom/google/android/gms/plus/PlusClient;->a:Lcom/google/android/gms/plus/internal/e;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/e;->c()V

    goto :goto_0

    .line 177
    :cond_1
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d016b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 178
    iget-object v1, p0, Lflipboard/util/GooglePlusSignInClient;->h:Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;

    invoke-interface {v1, v0}, Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 313
    :try_start_0
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->d:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/auth/GoogleAuthUtil;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/GooglePlayServicesAvailabilityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 321
    :goto_0
    return-void

    .line 314
    :catch_0
    move-exception v0

    .line 315
    iget-object v1, p0, Lflipboard/util/GooglePlusSignInClient;->c:Lflipboard/util/Log;

    const-string v2, "Failed to clear Google Plus token %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/GooglePlayServicesAvailabilityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 316
    :catch_1
    move-exception v0

    .line 317
    iget-object v1, p0, Lflipboard/util/GooglePlusSignInClient;->c:Lflipboard/util/Log;

    const-string v2, "Failed to clear Google Plus token %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/GoogleAuthException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 318
    :catch_2
    move-exception v0

    .line 319
    iget-object v1, p0, Lflipboard/util/GooglePlusSignInClient;->c:Lflipboard/util/Log;

    const-string v2, "Failed to clear Google Plus token %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->c:Lflipboard/util/Log;

    .line 211
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    if-nez v0, :cond_0

    .line 144
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->g:Lcom/google/android/gms/common/ConnectionResult;

    if-nez v0, :cond_1

    .line 133
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    iget-object v0, v0, Lcom/google/android/gms/plus/PlusClient;->a:Lcom/google/android/gms/plus/internal/e;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/e;->c()V

    goto :goto_0

    .line 136
    :cond_1
    :try_start_0
    iget-object v1, p0, Lflipboard/util/GooglePlusSignInClient;->g:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->d:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/ConnectionResult;->a(Landroid/app/Activity;)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 139
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->g:Lcom/google/android/gms/common/ConnectionResult;

    .line 140
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    iget-object v0, v0, Lcom/google/android/gms/plus/PlusClient;->a:Lcom/google/android/gms/plus/internal/e;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/e;->c()V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->g:Lcom/google/android/gms/common/ConnectionResult;

    .line 158
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    iget-object v0, v0, Lcom/google/android/gms/plus/PlusClient;->a:Lcom/google/android/gms/plus/internal/e;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/e;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    iget-object v0, v0, Lcom/google/android/gms/plus/PlusClient;->a:Lcom/google/android/gms/plus/internal/e;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/e;->c()V

    .line 161
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 222
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "oauth2:profile https://www.googleapis.com/auth/plus.profile.emails.read"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 225
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v3, "oauth2:server:client_id:334069016917.apps.googleusercontent.com:api_scope:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 227
    iget v3, p0, Lflipboard/util/GooglePlusSignInClient;->f:I

    packed-switch v3, :pswitch_data_0

    .line 243
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The Google Sign-in authScopeType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lflipboard/util/GooglePlusSignInClient;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is NOT supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :pswitch_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 247
    :goto_0
    :try_start_0
    iget v1, p0, Lflipboard/util/GooglePlusSignInClient;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lflipboard/util/GooglePlusSignInClient;->b:I

    .line 251
    iget-object v1, p0, Lflipboard/util/GooglePlusSignInClient;->d:Landroid/content/Context;

    iget-object v3, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    .line 253
    iget-object v3, v3, Lcom/google/android/gms/plus/PlusClient;->a:Lcom/google/android/gms/plus/internal/e;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/internal/e;->j()Ljava/lang/String;

    move-result-object v3

    .line 251
    invoke-static {v1, v3, v0}, Lcom/google/android/gms/auth/GoogleAuthUtil;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 257
    iget-object v1, p0, Lflipboard/util/GooglePlusSignInClient;->h:Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;

    iget v3, p0, Lflipboard/util/GooglePlusSignInClient;->f:I

    invoke-interface {v1, v3, v0}, Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;->a(ILjava/lang/String;)V

    .line 258
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->c:Lflipboard/util/Log;

    .line 261
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->g:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    iget-object v0, v0, Lcom/google/android/gms/plus/PlusClient;->a:Lcom/google/android/gms/plus/internal/e;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/e;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    iget-object v0, v0, Lcom/google/android/gms/plus/PlusClient;->a:Lcom/google/android/gms/plus/internal/e;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/e;->g()V
    :try_end_0
    .catch Lcom/google/android/gms/auth/GooglePlayServicesAvailabilityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    :try_start_1
    iput-object v1, v0, Lcom/google/android/gms/plus/internal/e;->f:Lcom/google/android/gms/plus/model/people/Person;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/e;->h()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/d;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/d;->b()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/auth/GooglePlayServicesAvailabilityException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    iget-object v0, v0, Lcom/google/android/gms/plus/PlusClient;->a:Lcom/google/android/gms/plus/internal/e;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/e;->f()V

    .line 262
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/util/GooglePlusSignInClient;->b:I
    :try_end_2
    .catch Lcom/google/android/gms/auth/GooglePlayServicesAvailabilityException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 307
    :goto_1
    return-void

    .line 235
    :pswitch_1
    const-string v0, "https://www.googleapis.com/auth/plus.login profile https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 236
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 239
    :pswitch_2
    const-string v0, "https://www.googleapis.com/auth/plus.login https://gdata.youtube.com"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 240
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 261
    :catch_0
    move-exception v0

    :try_start_3
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catch Lcom/google/android/gms/auth/GooglePlayServicesAvailabilityException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 264
    :catch_1
    move-exception v0

    .line 265
    :try_start_4
    iget-object v1, p0, Lflipboard/util/GooglePlusSignInClient;->c:Lflipboard/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Google Plus GooglePlayServicesAvailabilityException "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    iget-object v2, v2, Lcom/google/android/gms/plus/PlusClient;->a:Lcom/google/android/gms/plus/internal/e;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/e;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    iget v1, v0, Lcom/google/android/gms/auth/GooglePlayServicesAvailabilityException;->a:I

    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->d:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->a(ILandroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v0

    .line 268
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 305
    :catchall_0
    move-exception v0

    throw v0

    .line 273
    :catch_2
    move-exception v0

    :try_start_5
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->c:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Google Plus IOException "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    iget-object v1, v1, Lcom/google/android/gms/plus/PlusClient;->a:Lcom/google/android/gms/plus/internal/e;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/e;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d016d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 275
    iget-object v1, p0, Lflipboard/util/GooglePlusSignInClient;->h:Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;

    iget v2, p0, Lflipboard/util/GooglePlusSignInClient;->f:I

    invoke-interface {v1, v0}, Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 276
    :catch_3
    move-exception v0

    move-object v1, v0

    .line 277
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->c:Lflipboard/util/Log;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Google Plus UserRecoverableAuthException "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    iget-object v4, v4, Lcom/google/android/gms/plus/PlusClient;->a:Lcom/google/android/gms/plus/internal/e;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/internal/e;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",  Message is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 286
    iget v0, p0, Lflipboard/util/GooglePlusSignInClient;->b:I

    const/4 v3, 0x3

    if-gt v0, v3, :cond_2

    .line 287
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->d:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    iget-object v3, v1, Lcom/google/android/gms/auth/UserRecoverableAuthException;->b:Landroid/content/Intent;

    if-nez v3, :cond_1

    move-object v1, v2

    :goto_2
    const/16 v2, 0x1e39

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    :cond_1
    new-instance v2, Landroid/content/Intent;

    iget-object v1, v1, Lcom/google/android/gms/auth/UserRecoverableAuthException;->b:Landroid/content/Intent;

    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    move-object v1, v2

    goto :goto_2

    .line 289
    :cond_2
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0169

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 290
    iget-object v1, p0, Lflipboard/util/GooglePlusSignInClient;->h:Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;

    iget v2, p0, Lflipboard/util/GooglePlusSignInClient;->f:I

    invoke-interface {v1, v0}, Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;->c(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 293
    :catch_4
    move-exception v0

    .line 296
    iget-object v1, p0, Lflipboard/util/GooglePlusSignInClient;->c:Lflipboard/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Google Plus GoogleAuthException "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    iget-object v3, v3, Lcom/google/android/gms/plus/PlusClient;->a:Lcom/google/android/gms/plus/internal/e;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/internal/e;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",  Message is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/auth/GoogleAuthException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 297
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d016a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 298
    iget-object v1, p0, Lflipboard/util/GooglePlusSignInClient;->h:Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;

    iget v2, p0, Lflipboard/util/GooglePlusSignInClient;->f:I

    invoke-interface {v1, v0}, Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;->c(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 299
    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 301
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0169

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 302
    iget-object v1, p0, Lflipboard/util/GooglePlusSignInClient;->h:Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;

    iget v2, p0, Lflipboard/util/GooglePlusSignInClient;->f:I

    invoke-interface {v1, v0}, Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;->c(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 227
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final h_()V
    .locals 3

    .prologue
    .line 191
    iget-object v0, p0, Lflipboard/util/GooglePlusSignInClient;->h:Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;

    iget v1, p0, Lflipboard/util/GooglePlusSignInClient;->f:I

    invoke-interface {v0}, Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;->j()V

    .line 192
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 193
    iget-object v1, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    iget-object v1, v1, Lcom/google/android/gms/plus/PlusClient;->a:Lcom/google/android/gms/plus/internal/e;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/e;->k()Lcom/google/android/gms/plus/model/people/Person;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 194
    iget-object v1, p0, Lflipboard/util/GooglePlusSignInClient;->e:Lcom/google/android/gms/plus/PlusClient;

    iget-object v1, v1, Lcom/google/android/gms/plus/PlusClient;->a:Lcom/google/android/gms/plus/internal/e;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/e;->k()Lcom/google/android/gms/plus/model/people/Person;

    move-result-object v1

    .line 195
    invoke-interface {v1}, Lcom/google/android/gms/plus/model/people/Person;->e()Ljava/lang/String;

    move-result-object v1

    .line 196
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 200
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "GooglePlusSignInClient:onConnected"

    new-instance v2, Lflipboard/util/GooglePlusSignInClient$1;

    invoke-direct {v2, p0}, Lflipboard/util/GooglePlusSignInClient$1;-><init>(Lflipboard/util/GooglePlusSignInClient;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 206
    return-void
.end method
