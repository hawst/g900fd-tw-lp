.class Lflipboard/util/SocialHelper$23$1;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SocialHelper.java"


# instance fields
.field final synthetic a:Lflipboard/util/SocialHelper$23;


# direct methods
.method constructor <init>(Lflipboard/util/SocialHelper$23;)V
    .locals 0

    .prologue
    .line 1055
    iput-object p1, p0, Lflipboard/util/SocialHelper$23$1;->a:Lflipboard/util/SocialHelper$23;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 7

    .prologue
    .line 1059
    sget-object v0, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 1060
    iget-object v0, p0, Lflipboard/util/SocialHelper$23$1;->a:Lflipboard/util/SocialHelper$23;

    iget-object v0, v0, Lflipboard/util/SocialHelper$23;->a:Lflipboard/activities/FlipboardActivity;

    instance-of v0, v0, Lflipboard/activities/FeedActivity;

    if-eqz v0, :cond_0

    .line 1061
    iget-object v0, p0, Lflipboard/util/SocialHelper$23$1;->a:Lflipboard/util/SocialHelper$23;

    iget-object v0, v0, Lflipboard/util/SocialHelper$23;->a:Lflipboard/activities/FlipboardActivity;

    check-cast v0, Lflipboard/activities/FeedActivity;

    iget-object v2, p0, Lflipboard/util/SocialHelper$23$1;->a:Lflipboard/util/SocialHelper$23;

    iget-object v2, v2, Lflipboard/util/SocialHelper$23;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v2}, Lflipboard/activities/FeedActivity;->b(Lflipboard/objs/FeedItem;)V

    .line 1069
    :goto_0
    sget-object v0, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v0

    iget-object v2, p0, Lflipboard/util/SocialHelper$23$1;->a:Lflipboard/util/SocialHelper$23;

    iget-object v2, v2, Lflipboard/util/SocialHelper$23;->c:Lflipboard/service/Section;

    iget-object v3, p0, Lflipboard/util/SocialHelper$23$1;->a:Lflipboard/util/SocialHelper$23;

    iget-object v3, v3, Lflipboard/util/SocialHelper$23;->b:Lflipboard/objs/FeedItem;

    const/4 v4, 0x0

    const-string v5, "inappropriate"

    new-instance v6, Lflipboard/util/SocialHelper$23$1$1;

    invoke-direct {v6, p0}, Lflipboard/util/SocialHelper$23$1$1;-><init>(Lflipboard/util/SocialHelper$23$1;)V

    invoke-virtual/range {v0 .. v6}, Lflipboard/service/Flap;->a(Lflipboard/service/User;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/CommentaryResult$Item$Commentary;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V

    .line 1081
    return-void

    .line 1063
    :cond_0
    iget-object v0, p0, Lflipboard/util/SocialHelper$23$1;->a:Lflipboard/util/SocialHelper$23;

    iget-object v0, v0, Lflipboard/util/SocialHelper$23;->a:Lflipboard/activities/FlipboardActivity;

    instance-of v0, v0, Lflipboard/activities/MainActivity;

    if-eqz v0, :cond_1

    .line 1064
    iget-object v0, p0, Lflipboard/util/SocialHelper$23$1;->a:Lflipboard/util/SocialHelper$23;

    iget-object v0, v0, Lflipboard/util/SocialHelper$23;->a:Lflipboard/activities/FlipboardActivity;

    check-cast v0, Lflipboard/activities/MainActivity;

    iget-object v2, p0, Lflipboard/util/SocialHelper$23$1;->a:Lflipboard/util/SocialHelper$23;

    iget-object v2, v2, Lflipboard/util/SocialHelper$23;->b:Lflipboard/objs/FeedItem;

    iget-object v3, v2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v0, v0, Lflipboard/activities/MainActivity;->p:Lflipboard/gui/personal/TabPagerAdapter;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lflipboard/gui/personal/TabPagerAdapter;->d(I)Lflipboard/activities/FlipboardFragment;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/SectionFragment;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    const v3, 0x7f0d0176

    invoke-virtual {v0, v2, v3}, Lflipboard/gui/section/SectionFragment;->a(Ljava/lang/String;I)V

    .line 1066
    :cond_1
    iget-object v0, p0, Lflipboard/util/SocialHelper$23$1;->a:Lflipboard/util/SocialHelper$23;

    iget-object v0, v0, Lflipboard/util/SocialHelper$23;->b:Lflipboard/objs/FeedItem;

    invoke-static {v0}, Lflipboard/util/SocialHelper;->b(Lflipboard/objs/FeedItem;)V

    goto :goto_0
.end method
