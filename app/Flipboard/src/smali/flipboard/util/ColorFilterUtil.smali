.class public Lflipboard/util/ColorFilterUtil;
.super Ljava/lang/Object;
.source "ColorFilterUtil.java"


# static fields
.field private static final a:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/ColorFilter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 12
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lflipboard/util/ColorFilterUtil;->a:Landroid/util/LruCache;

    return-void
.end method

.method public static a(I)I
    .locals 4

    .prologue
    .line 24
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3f2b851f    # 0.67f

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    const/4 v1, 0x0

    const/16 v2, 0xff

    invoke-static {v0, v1, v2}, Lflipboard/util/JavaUtil;->a(III)I

    move-result v0

    .line 25
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 82
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 86
    :goto_0
    return v0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bad color: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    .line 86
    const/high16 v0, -0x1000000

    goto :goto_0
.end method

.method public static a()Landroid/graphics/ColorFilter;
    .locals 2

    .prologue
    .line 34
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08005a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Lflipboard/util/ColorFilterUtil;->b(I)Landroid/graphics/ColorFilter;

    move-result-object v0

    return-object v0
.end method

.method public static b(I)Landroid/graphics/ColorFilter;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 55
    sget-object v0, Lflipboard/util/ColorFilterUtil;->a:Landroid/util/LruCache;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/ColorFilter;

    .line 56
    if-nez v0, :cond_0

    .line 57
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    int-to-float v1, v0

    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v3

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    new-instance v0, Landroid/graphics/ColorMatrixColorFilter;

    const/16 v5, 0x14

    new-array v5, v5, [F

    const/4 v6, 0x0

    aput v7, v5, v6

    const/4 v6, 0x1

    aput v7, v5, v6

    const/4 v6, 0x2

    aput v7, v5, v6

    const/4 v6, 0x3

    aput v7, v5, v6

    const/4 v6, 0x4

    int-to-float v2, v2

    aput v2, v5, v6

    const/4 v2, 0x5

    aput v7, v5, v2

    const/4 v2, 0x6

    aput v7, v5, v2

    const/4 v2, 0x7

    aput v7, v5, v2

    const/16 v2, 0x8

    aput v7, v5, v2

    const/16 v2, 0x9

    int-to-float v3, v3

    aput v3, v5, v2

    const/16 v2, 0xa

    aput v7, v5, v2

    const/16 v2, 0xb

    aput v7, v5, v2

    const/16 v2, 0xc

    aput v7, v5, v2

    const/16 v2, 0xd

    aput v7, v5, v2

    const/16 v2, 0xe

    int-to-float v3, v4

    aput v3, v5, v2

    const/16 v2, 0xf

    aput v7, v5, v2

    const/16 v2, 0x10

    aput v7, v5, v2

    const/16 v2, 0x11

    aput v7, v5, v2

    const/16 v2, 0x12

    const/high16 v3, 0x437f0000    # 255.0f

    div-float/2addr v1, v3

    aput v1, v5, v2

    const/16 v1, 0x13

    aput v7, v5, v1

    invoke-direct {v0, v5}, Landroid/graphics/ColorMatrixColorFilter;-><init>([F)V

    .line 58
    sget-object v1, Lflipboard/util/ColorFilterUtil;->a:Landroid/util/LruCache;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    :cond_0
    return-object v0
.end method
