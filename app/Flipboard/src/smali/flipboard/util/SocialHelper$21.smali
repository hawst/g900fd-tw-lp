.class final Lflipboard/util/SocialHelper$21;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SocialHelper.java"


# instance fields
.field final synthetic a:Lflipboard/activities/FlipboardActivity;

.field final synthetic b:Lflipboard/service/Section;

.field final synthetic c:Lflipboard/objs/FeedItem;


# direct methods
.method constructor <init>(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 969
    iput-object p1, p0, Lflipboard/util/SocialHelper$21;->a:Lflipboard/activities/FlipboardActivity;

    iput-object p2, p0, Lflipboard/util/SocialHelper$21;->b:Lflipboard/service/Section;

    iput-object p3, p0, Lflipboard/util/SocialHelper$21;->c:Lflipboard/objs/FeedItem;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;I)V
    .locals 3

    .prologue
    .line 974
    iget-object v0, p0, Lflipboard/util/SocialHelper$21;->a:Lflipboard/activities/FlipboardActivity;

    const/4 v1, 0x0

    iput-object v1, v0, Lflipboard/activities/FlipboardActivity;->O:Lflipboard/objs/FeedItem;

    .line 975
    sget-object v0, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 976
    invoke-virtual {v0}, Lflipboard/service/User;->l()Ljava/lang/String;

    move-result-object v0

    .line 977
    if-eqz v0, :cond_1

    .line 978
    iget-object v0, p0, Lflipboard/util/SocialHelper$21;->b:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/util/SocialHelper$21;->c:Lflipboard/objs/FeedItem;

    iget-object v2, p0, Lflipboard/util/SocialHelper$21;->a:Lflipboard/activities/FlipboardActivity;

    invoke-static {v0, v1, v2}, Lflipboard/util/SocialHelper;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;)V

    .line 1001
    :cond_0
    :goto_0
    return-void

    .line 979
    :cond_1
    iget-object v0, p0, Lflipboard/util/SocialHelper$21;->a:Lflipboard/activities/FlipboardActivity;

    iget-boolean v0, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_0

    .line 981
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 982
    const v1, 0x7f0d01f0

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 983
    const v1, 0x7f0d01ed

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 984
    const v1, 0x7f0d004a

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 985
    const v1, 0x7f0d01f1

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 986
    new-instance v1, Lflipboard/util/SocialHelper$21$1;

    invoke-direct {v1, p0}, Lflipboard/util/SocialHelper$21$1;-><init>(Lflipboard/util/SocialHelper$21;)V

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 999
    iget-object v1, p0, Lflipboard/util/SocialHelper$21;->a:Lflipboard/activities/FlipboardActivity;

    iget-object v1, v1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "read_later"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
