.class public Lflipboard/util/FLAnimation$Move;
.super Lflipboard/util/FLAnimation;
.source "FLAnimation.java"


# instance fields
.field final e:Landroid/view/View;

.field final f:Landroid/graphics/Rect;

.field final g:Landroid/graphics/Rect;

.field final h:Landroid/graphics/Rect;

.field final i:Ljava/lang/Runnable;

.field final j:I

.field final k:I


# direct methods
.method public constructor <init>(Landroid/view/View;ILandroid/graphics/Rect;Landroid/graphics/Rect;Ljava/lang/Runnable;)V
    .locals 5

    .prologue
    .line 171
    invoke-direct {p0, p1, p2}, Lflipboard/util/FLAnimation;-><init>(Ljava/lang/Object;I)V

    .line 172
    iput-object p1, p0, Lflipboard/util/FLAnimation$Move;->e:Landroid/view/View;

    .line 173
    if-nez p4, :cond_0

    sget-object p4, Lflipboard/util/FLAnimation$Move;->a:Landroid/graphics/Rect;

    :cond_0
    iput-object p4, p0, Lflipboard/util/FLAnimation$Move;->h:Landroid/graphics/Rect;

    .line 174
    iput-object p5, p0, Lflipboard/util/FLAnimation$Move;->i:Ljava/lang/Runnable;

    .line 176
    iput-object p3, p0, Lflipboard/util/FLAnimation$Move;->g:Landroid/graphics/Rect;

    .line 177
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lflipboard/util/FLAnimation$Move;->f:Landroid/graphics/Rect;

    .line 179
    instance-of v0, p1, Lflipboard/util/FLAnimation$Scaleable;

    if-eqz v0, :cond_1

    .line 180
    iget-object v0, p0, Lflipboard/util/FLAnimation$Move;->f:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lflipboard/util/FLAnimation$Move;->f:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iput v0, p0, Lflipboard/util/FLAnimation$Move;->j:I

    .line 181
    iget-object v0, p0, Lflipboard/util/FLAnimation$Move;->f:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lflipboard/util/FLAnimation$Move;->f:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    iput v0, p0, Lflipboard/util/FLAnimation$Move;->k:I

    .line 186
    :goto_0
    invoke-virtual {p0}, Lflipboard/util/FLAnimation$Move;->a()V

    .line 187
    return-void

    .line 183
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/util/FLAnimation$Move;->k:I

    iput v0, p0, Lflipboard/util/FLAnimation$Move;->j:I

    goto :goto_0
.end method


# virtual methods
.method protected final a(F)V
    .locals 8

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    .line 222
    iget-object v0, p0, Lflipboard/util/FLAnimation$Move;->f:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lflipboard/util/FLAnimation$Move;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lflipboard/util/FLAnimation$Move;->f:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    add-int v2, v0, v1

    .line 223
    iget-object v0, p0, Lflipboard/util/FLAnimation$Move;->f:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lflipboard/util/FLAnimation$Move;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lflipboard/util/FLAnimation$Move;->f:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v3

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    add-int v3, v0, v1

    .line 226
    iget-object v0, p0, Lflipboard/util/FLAnimation$Move;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 228
    if-eqz v0, :cond_0

    .line 230
    iget v1, p0, Lflipboard/util/FLAnimation$Move;->j:I

    if-lez v1, :cond_1

    .line 231
    iget-object v1, p0, Lflipboard/util/FLAnimation$Move;->e:Landroid/view/View;

    check-cast v1, Lflipboard/util/FLAnimation$Scaleable;

    invoke-interface {v1}, Lflipboard/util/FLAnimation$Scaleable;->getScale()F

    move-result v1

    .line 232
    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v4, v1, v4

    if-lez v4, :cond_1

    .line 233
    iget v4, p0, Lflipboard/util/FLAnimation$Move;->j:I

    int-to-float v4, v4

    mul-float/2addr v4, v1

    add-float/2addr v4, v6

    float-to-int v4, v4

    .line 234
    iget v5, p0, Lflipboard/util/FLAnimation$Move;->k:I

    int-to-float v5, v5

    mul-float/2addr v1, v5

    add-float/2addr v1, v6

    float-to-int v1, v1

    .line 235
    iget-object v5, p0, Lflipboard/util/FLAnimation$Move;->h:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    sub-int v5, v2, v5

    sub-int/2addr v5, v4

    iget-object v6, p0, Lflipboard/util/FLAnimation$Move;->h:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    sub-int v6, v3, v6

    sub-int/2addr v6, v1

    iget v7, p0, Lflipboard/util/FLAnimation$Move;->j:I

    add-int/2addr v2, v7

    iget-object v7, p0, Lflipboard/util/FLAnimation$Move;->h:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v7

    add-int/2addr v2, v4

    iget v4, p0, Lflipboard/util/FLAnimation$Move;->k:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lflipboard/util/FLAnimation$Move;->h:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    invoke-virtual {v0, v5, v6, v2, v1}, Landroid/view/View;->invalidate(IIII)V

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    iget-object v1, p0, Lflipboard/util/FLAnimation$Move;->h:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int v1, v2, v1

    iget-object v4, p0, Lflipboard/util/FLAnimation$Move;->h:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int v4, v3, v4

    iget-object v5, p0, Lflipboard/util/FLAnimation$Move;->e:Landroid/view/View;

    .line 241
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v2, v5

    iget-object v5, p0, Lflipboard/util/FLAnimation$Move;->h:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v5

    iget-object v5, p0, Lflipboard/util/FLAnimation$Move;->e:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v3, v5

    iget-object v5, p0, Lflipboard/util/FLAnimation$Move;->h:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v5

    .line 240
    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/view/View;->invalidate(IIII)V

    goto :goto_0
.end method

.method protected final b()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 196
    invoke-super {p0}, Lflipboard/util/FLAnimation;->b()V

    .line 199
    iget-object v0, p0, Lflipboard/util/FLAnimation$Move;->e:Landroid/view/View;

    iget-object v1, p0, Lflipboard/util/FLAnimation$Move;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lflipboard/util/FLAnimation$Move;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lflipboard/util/FLAnimation$Move;->g:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lflipboard/util/FLAnimation$Move;->g:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 202
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v1, p0, Lflipboard/util/FLAnimation$Move;->f:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lflipboard/util/FLAnimation$Move;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lflipboard/util/FLAnimation$Move;->f:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lflipboard/util/FLAnimation$Move;->g:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-direct {v0, v1, v5, v2, v5}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 203
    iget v1, p0, Lflipboard/util/FLAnimation$Move;->c:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 204
    iget-object v1, p0, Lflipboard/util/FLAnimation$Move;->e:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 205
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget v2, p0, Lflipboard/util/FLAnimation$Move;->c:I

    int-to-long v2, v2

    new-instance v4, Lflipboard/util/FLAnimation$Move$1;

    invoke-direct {v4, p0, v0}, Lflipboard/util/FLAnimation$Move$1;-><init>(Lflipboard/util/FLAnimation$Move;Landroid/view/animation/Animation;)V

    invoke-virtual {v1, v2, v3, v4}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    .line 218
    return-void
.end method
