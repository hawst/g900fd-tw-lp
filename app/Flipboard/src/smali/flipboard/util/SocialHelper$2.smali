.class final Lflipboard/util/SocialHelper$2;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SocialHelper.java"


# instance fields
.field final synthetic a:Landroid/content/SharedPreferences;

.field final synthetic b:Lflipboard/objs/ConfigService;

.field final synthetic c:Lflipboard/objs/FeedItem;

.field final synthetic d:Z

.field final synthetic f:Lflipboard/activities/FlipboardActivity;

.field final synthetic g:Lflipboard/service/Section;


# direct methods
.method constructor <init>(Landroid/content/SharedPreferences;Lflipboard/objs/ConfigService;Lflipboard/objs/FeedItem;ZLflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lflipboard/util/SocialHelper$2;->a:Landroid/content/SharedPreferences;

    iput-object p2, p0, Lflipboard/util/SocialHelper$2;->b:Lflipboard/objs/ConfigService;

    iput-object p3, p0, Lflipboard/util/SocialHelper$2;->c:Lflipboard/objs/FeedItem;

    iput-boolean p4, p0, Lflipboard/util/SocialHelper$2;->d:Z

    iput-object p5, p0, Lflipboard/util/SocialHelper$2;->f:Lflipboard/activities/FlipboardActivity;

    iput-object p6, p0, Lflipboard/util/SocialHelper$2;->g:Lflipboard/service/Section;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    .line 218
    sput-boolean p1, Lflipboard/settings/Facebook;->implicit_share_facebook:Z

    .line 219
    iget-object v0, p0, Lflipboard/util/SocialHelper$2;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "implicit_share_facebook"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 220
    iget-object v0, p0, Lflipboard/util/SocialHelper$2;->b:Lflipboard/objs/ConfigService;

    iget-object v1, p0, Lflipboard/util/SocialHelper$2;->c:Lflipboard/objs/FeedItem;

    iget-boolean v2, p0, Lflipboard/util/SocialHelper$2;->d:Z

    iget-object v3, p0, Lflipboard/util/SocialHelper$2;->f:Lflipboard/activities/FlipboardActivity;

    iget-object v4, p0, Lflipboard/util/SocialHelper$2;->g:Lflipboard/service/Section;

    invoke-static {v0, v1, v2, v3, v4}, Lflipboard/util/SocialHelper;->b(Lflipboard/objs/ConfigService;Lflipboard/objs/FeedItem;ZLflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V

    .line 221
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;)V
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/util/SocialHelper$2;->a(Z)V

    .line 214
    return-void
.end method

.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lflipboard/util/SocialHelper$2;->a(Z)V

    .line 208
    return-void
.end method
