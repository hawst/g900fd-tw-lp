.class public Lflipboard/util/FLAnimation$Scale;
.super Lflipboard/util/FLAnimation;
.source "FLAnimation.java"


# instance fields
.field final e:Lflipboard/util/FLAnimation$Scaleable;

.field final f:F

.field final g:F


# direct methods
.method public constructor <init>(Lflipboard/util/FLAnimation$Scaleable;FI)V
    .locals 1

    .prologue
    .line 261
    invoke-direct {p0, p1, p3}, Lflipboard/util/FLAnimation;-><init>(Ljava/lang/Object;I)V

    .line 262
    iput-object p1, p0, Lflipboard/util/FLAnimation$Scale;->e:Lflipboard/util/FLAnimation$Scaleable;

    .line 263
    invoke-interface {p1}, Lflipboard/util/FLAnimation$Scaleable;->getScale()F

    move-result v0

    iput v0, p0, Lflipboard/util/FLAnimation$Scale;->f:F

    .line 264
    iput p2, p0, Lflipboard/util/FLAnimation$Scale;->g:F

    .line 265
    invoke-virtual {p0}, Lflipboard/util/FLAnimation$Scale;->a()V

    .line 266
    return-void
.end method


# virtual methods
.method protected final a(F)V
    .locals 4

    .prologue
    .line 270
    iget-object v0, p0, Lflipboard/util/FLAnimation$Scale;->e:Lflipboard/util/FLAnimation$Scaleable;

    iget v1, p0, Lflipboard/util/FLAnimation$Scale;->f:F

    iget v2, p0, Lflipboard/util/FLAnimation$Scale;->g:F

    iget v3, p0, Lflipboard/util/FLAnimation$Scale;->f:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    invoke-interface {v0, v1}, Lflipboard/util/FLAnimation$Scaleable;->setScale(F)V

    .line 271
    return-void
.end method
