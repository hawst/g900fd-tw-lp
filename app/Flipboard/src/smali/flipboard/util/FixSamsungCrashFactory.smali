.class public Lflipboard/util/FixSamsungCrashFactory;
.super Ljava/lang/Object;
.source "FixSamsungCrashFactory.java"

# interfaces
.implements Landroid/view/LayoutInflater$Factory2;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 17

    .prologue
    .line 37
    const/4 v4, 0x0

    .line 38
    const/4 v3, 0x0

    .line 39
    const/4 v2, 0x0

    .line 42
    const-string v5, "flipboard.gui.FLTextView"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 43
    new-instance v2, Lflipboard/gui/FLTextView;

    move-object/from16 v0, p3

    invoke-direct {v2, v0}, Lflipboard/gui/FLTextView;-><init>(Landroid/content/Context;)V

    move-object v11, v2

    move-object v12, v3

    move-object v13, v2

    .line 53
    :goto_0
    if-eqz v13, :cond_f

    .line 56
    const/4 v2, 0x0

    :goto_1
    invoke-interface/range {p4 .. p4}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 57
    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v3

    .line 58
    const-string v4, "visibility"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 59
    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v2, v3}, Landroid/util/AttributeSet;->getAttributeIntValue(II)I

    move-result v2

    .line 60
    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 61
    const/16 v2, 0x8

    invoke-virtual {v13, v2}, Landroid/view/View;->setVisibility(I)V

    .line 70
    :cond_0
    const/4 v9, 0x0

    .line 71
    const/4 v8, 0x0

    .line 72
    const/4 v7, 0x0

    .line 73
    const/4 v6, 0x3

    .line 75
    const/4 v3, 0x0

    .line 77
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    .line 78
    sget-object v4, Lflipboard/app/R$styleable;->FLStaticTextView:[I

    const v5, 0x7f0e0015

    const v10, 0x7f0e0015

    move-object/from16 v0, p4

    invoke-virtual {v2, v0, v4, v5, v10}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v14

    .line 80
    const/4 v2, 0x0

    move v10, v2

    move v4, v3

    move v5, v3

    move v2, v3

    :goto_2
    invoke-virtual {v14}, Landroid/content/res/TypedArray;->length()I

    move-result v15

    if-ge v10, v15, :cond_7

    .line 81
    invoke-virtual {v14, v10}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v15

    .line 82
    packed-switch v15, :pswitch_data_0

    .line 80
    :cond_1
    :goto_3
    :pswitch_0
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 44
    :cond_2
    const-string v5, "TextView"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 45
    new-instance v2, Landroid/widget/TextView;

    move-object/from16 v0, p3

    invoke-direct {v2, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    move-object v11, v2

    move-object v12, v3

    move-object v13, v2

    goto :goto_0

    .line 46
    :cond_3
    const-string v5, "flipboard.gui.FLButton"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 47
    new-instance v3, Lflipboard/gui/FLButton;

    move-object/from16 v0, p3

    invoke-direct {v3, v0}, Lflipboard/gui/FLButton;-><init>(Landroid/content/Context;)V

    move-object v11, v2

    move-object v12, v3

    move-object v13, v3

    goto :goto_0

    .line 48
    :cond_4
    const-string v5, "Button"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 49
    new-instance v3, Landroid/widget/Button;

    move-object/from16 v0, p3

    invoke-direct {v3, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    move-object v11, v2

    move-object v12, v3

    move-object v13, v3

    goto/16 :goto_0

    .line 56
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 84
    :pswitch_1
    invoke-virtual {v14, v15, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v9

    goto :goto_3

    .line 88
    :pswitch_2
    const/16 v8, 0x14

    invoke-virtual {v14, v8}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_3

    .line 91
    :pswitch_3
    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v15

    .line 92
    if-eqz v12, :cond_6

    .line 93
    invoke-virtual {v12, v15}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_3

    .line 94
    :cond_6
    if-eqz v11, :cond_1

    .line 95
    invoke-virtual {v11, v15}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3

    .line 99
    :pswitch_4
    invoke-virtual {v14, v15}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    goto :goto_3

    .line 102
    :pswitch_5
    const/4 v6, 0x0

    invoke-virtual {v14, v15, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    goto :goto_3

    .line 105
    :pswitch_6
    const/4 v2, 0x0

    invoke-virtual {v14, v15, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    move v3, v2

    move v4, v2

    move v5, v2

    .line 106
    goto :goto_3

    .line 108
    :pswitch_7
    const/4 v5, 0x0

    invoke-virtual {v14, v15, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    goto :goto_3

    .line 111
    :pswitch_8
    const/4 v4, 0x0

    invoke-virtual {v14, v15, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    goto/16 :goto_3

    .line 114
    :pswitch_9
    const/4 v3, 0x0

    invoke-virtual {v14, v15, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    goto/16 :goto_3

    .line 117
    :pswitch_a
    const/4 v2, 0x0

    invoke-virtual {v14, v15, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    goto/16 :goto_3

    .line 121
    :cond_7
    invoke-virtual {v13, v5, v3, v4, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 123
    if-lez v9, :cond_8

    .line 124
    if-eqz v12, :cond_10

    .line 125
    const/4 v2, 0x1

    int-to-float v3, v9

    invoke-virtual {v12, v2, v3}, Landroid/widget/Button;->setTextSize(IF)V

    .line 130
    :cond_8
    :goto_4
    if-eqz v12, :cond_11

    .line 131
    invoke-virtual {v12, v6}, Landroid/widget/Button;->setGravity(I)V

    .line 135
    :cond_9
    :goto_5
    if-eqz v8, :cond_a

    .line 136
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2, v8}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    .line 137
    if-eqz v12, :cond_12

    .line 138
    invoke-virtual {v12, v2}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 143
    :cond_a
    :goto_6
    if-eqz v7, :cond_b

    .line 144
    if-eqz v12, :cond_13

    .line 145
    invoke-virtual {v12, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 152
    :cond_b
    :goto_7
    const/4 v2, 0x3

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    .line 159
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 160
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    .line 161
    invoke-virtual {v4, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    .line 162
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 163
    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v13, v2}, Landroid/view/View;->setId(I)V

    .line 165
    :cond_c
    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 166
    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v13, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 168
    :cond_d
    if-eqz p1, :cond_e

    .line 169
    check-cast p1, Landroid/view/ViewGroup;

    .line 170
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 171
    invoke-virtual {v13, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 176
    :cond_e
    const/4 v2, 0x2

    invoke-virtual {v3, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 177
    if-eqz v2, :cond_f

    .line 178
    new-instance v3, Lflipboard/util/FixSamsungCrashFactory$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lflipboard/util/FixSamsungCrashFactory$1;-><init>(Lflipboard/util/FixSamsungCrashFactory;Ljava/lang/String;)V

    invoke-virtual {v13, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    :cond_f
    return-object v13

    .line 126
    :cond_10
    if-eqz v11, :cond_8

    .line 127
    const/4 v2, 0x1

    int-to-float v3, v9

    invoke-virtual {v11, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_4

    .line 132
    :cond_11
    if-eqz v11, :cond_9

    .line 133
    invoke-virtual {v11, v6}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_5

    .line 139
    :cond_12
    if-eqz v11, :cond_a

    .line 140
    invoke-virtual {v11, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_6

    .line 146
    :cond_13
    if-eqz v11, :cond_b

    .line 147
    invoke-virtual {v11, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_7

    :cond_14
    move-object v11, v2

    move-object v12, v3

    move-object v13, v4

    goto/16 :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_8
        :pswitch_a
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 152
    :array_0
    .array-data 4
        0x10100d0
        0x10100d4
        0x101026f
    .end array-data
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2, p3}, Lflipboard/util/FixSamsungCrashFactory;->onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
