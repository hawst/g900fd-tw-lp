.class public Lflipboard/util/VideoUtil;
.super Ljava/lang/Object;
.source "VideoUtil.java"


# direct methods
.method public static a(Lflipboard/objs/FeedItem;)Lflipboard/util/VideoUtil$VideoType;
    .locals 3

    .prologue
    .line 153
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aX:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 154
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aX:Ljava/lang/String;

    .line 159
    :goto_0
    iget-object v1, p0, Lflipboard/objs/FeedItem;->bj:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, ".mp4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const-string v1, "h264"

    .line 161
    :goto_1
    iget-object v2, p0, Lflipboard/objs/FeedItem;->aY:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 162
    sget-object v0, Lflipboard/util/VideoUtil$VideoType;->a:Lflipboard/util/VideoUtil$VideoType;

    .line 176
    :goto_2
    return-object v0

    .line 156
    :cond_1
    iget-object v0, p0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    goto :goto_0

    .line 159
    :cond_2
    const-string v1, "video"

    goto :goto_1

    .line 163
    :cond_3
    const-string v2, "h264"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 164
    sget-object v0, Lflipboard/util/VideoUtil$VideoType;->b:Lflipboard/util/VideoUtil$VideoType;

    goto :goto_2

    .line 165
    :cond_4
    const-string v1, "youtube.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    .line 166
    invoke-static {}, Lflipboard/util/YouTubeHelper;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 172
    sget-object v0, Lflipboard/util/VideoUtil$VideoType;->e:Lflipboard/util/VideoUtil$VideoType;

    goto :goto_2

    .line 168
    :pswitch_0
    sget-object v0, Lflipboard/util/VideoUtil$VideoType;->c:Lflipboard/util/VideoUtil$VideoType;

    goto :goto_2

    .line 170
    :pswitch_1
    sget-object v0, Lflipboard/util/VideoUtil$VideoType;->d:Lflipboard/util/VideoUtil$VideoType;

    goto :goto_2

    .line 176
    :cond_5
    sget-object v0, Lflipboard/util/VideoUtil$VideoType;->f:Lflipboard/util/VideoUtil$VideoType;

    goto :goto_2

    .line 166
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lflipboard/util/VideoUtil$VideoType;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 182
    sget-object v0, Lflipboard/util/VideoUtil$1;->a:[I

    invoke-virtual {p0}, Lflipboard/util/VideoUtil$VideoType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 196
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 184
    :pswitch_0
    const-string v0, "embeddedWebview"

    goto :goto_0

    .line 186
    :pswitch_1
    const-string v0, "native"

    goto :goto_0

    .line 188
    :pswitch_2
    const-string v0, "webview"

    goto :goto_0

    .line 190
    :pswitch_3
    const-string v0, "youtubeApi"

    goto :goto_0

    .line 192
    :pswitch_4
    const-string v0, "youtubeApp"

    goto :goto_0

    .line 194
    :pswitch_5
    const-string v0, "youtubeWebview"

    goto :goto_0

    .line 182
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(Landroid/app/Activity;Lflipboard/objs/FeedItem;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 43
    invoke-static {}, Lflipboard/util/YouTubeHelper;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v1, "youtube"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 47
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aX:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 48
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aX:Ljava/lang/String;

    .line 53
    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 54
    const-string v1, "v"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 56
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lflipboard/activities/YouTubePlayerActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 57
    const-string v3, "youtube_video_id"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    const-string v0, "sid"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    const-string v0, "extra_current_item"

    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->Z()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    const-string v0, "fromSection"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 61
    iget-object v0, p1, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 62
    const-string v0, "sid"

    iget-object v3, p1, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    :cond_0
    const-string v0, "extra_current_item"

    iget-object v3, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    check-cast p0, Lflipboard/activities/FlipboardActivity;

    .line 67
    invoke-virtual {p0, v1}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    .line 68
    const v0, 0x7f040001

    invoke-virtual {p0, v0, v2}, Lflipboard/activities/FlipboardActivity;->overridePendingTransition(II)V

    .line 126
    :goto_1
    return-void

    .line 50
    :cond_1
    iget-object v0, p1, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    goto :goto_0

    .line 70
    :cond_2
    const-string v0, "usage"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    .line 73
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aX:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 74
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aX:Ljava/lang/String;

    .line 79
    :goto_2
    const-string v1, "player.vimeo.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 80
    const-string v1, "video/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 85
    if-eq v1, v3, :cond_4

    .line 86
    add-int/lit8 v1, v1, 0x6

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 87
    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 88
    if-eq v1, v3, :cond_3

    .line 89
    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 91
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "http://vimeo.com/"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 95
    :cond_4
    iget-object v1, p1, Lflipboard/objs/FeedItem;->bj:Ljava/lang/String;

    if-nez v1, :cond_5

    const-string v1, ".mp4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    :cond_5
    const-string v1, "h264"

    .line 96
    :goto_3
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "flvideo://"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "?url="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p1, Lflipboard/objs/FeedItem;->bj:Ljava/lang/String;

    if-eqz v3, :cond_6

    iget-object v0, p1, Lflipboard/objs/FeedItem;->bj:Ljava/lang/String;

    :cond_6
    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 102
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lflipboard/activities/VideoActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 104
    const-string v3, "uri"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 105
    const-string v0, "extra_current_item"

    iget-object v3, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 107
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aY:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p1, Lflipboard/objs/FeedItem;->bj:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 108
    const-string v0, "videoEmbedHTML"

    iget-object v3, p1, Lflipboard/objs/FeedItem;->aY:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 110
    :cond_7
    iget v0, p1, Lflipboard/objs/FeedItem;->v:I

    if-lez v0, :cond_8

    iget v0, p1, Lflipboard/objs/FeedItem;->w:I

    if-lez v0, :cond_8

    .line 111
    const-string v0, "height"

    iget v3, p1, Lflipboard/objs/FeedItem;->w:I

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 112
    const-string v0, "width"

    iget v3, p1, Lflipboard/objs/FeedItem;->v:I

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 114
    :cond_8
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aY:Ljava/lang/String;

    if-eqz v0, :cond_d

    const-string v2, ".*<video.*\\s+loop\\s*.*>.*"

    invoke-static {v2, v0}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    :goto_4
    if-eqz v0, :cond_9

    .line 115
    const-string v0, "should_loop"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 117
    :cond_9
    const-string v0, "usageVideoType"

    invoke-static {p1}, Lflipboard/util/VideoUtil;->a(Lflipboard/objs/FeedItem;)Lflipboard/util/VideoUtil$VideoType;

    move-result-object v2

    invoke-static {v2}, Lflipboard/util/VideoUtil;->a(Lflipboard/util/VideoUtil$VideoType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    const-string v0, "partnerID"

    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->aa()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 119
    iget-object v0, p1, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 120
    const-string v0, "sid"

    iget-object v2, p1, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 122
    :cond_a
    const-string v0, "fromSection"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 124
    const/16 v0, 0x65

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 76
    :cond_b
    iget-object v0, p1, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    goto/16 :goto_2

    .line 95
    :cond_c
    const-string v1, "video"

    goto/16 :goto_3

    :cond_d
    move v0, v2

    goto :goto_4
.end method

.method public static a(Landroid/content/Context;Lflipboard/objs/Ad$VideoInfo;)V
    .locals 3

    .prologue
    .line 129
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/VideoAdActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 130
    const-string v1, "url"

    iget-object v2, p1, Lflipboard/objs/Ad$VideoInfo;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 131
    const-string v1, "impressionValues"

    iget-object v2, p1, Lflipboard/objs/Ad$VideoInfo;->b:Lflipboard/objs/Ad$MetricValues;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 132
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 133
    return-void
.end method
