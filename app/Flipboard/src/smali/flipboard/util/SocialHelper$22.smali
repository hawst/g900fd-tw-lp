.class final Lflipboard/util/SocialHelper$22;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SocialHelper.java"


# instance fields
.field final synthetic a:Lflipboard/objs/FeedItem;

.field final synthetic b:Lflipboard/activities/FlipboardActivity;


# direct methods
.method constructor <init>(Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;)V
    .locals 0

    .prologue
    .line 1008
    iput-object p1, p0, Lflipboard/util/SocialHelper$22;->a:Lflipboard/objs/FeedItem;

    iput-object p2, p0, Lflipboard/util/SocialHelper$22;->b:Lflipboard/activities/FlipboardActivity;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;I)V
    .locals 3

    .prologue
    .line 1012
    iget-object v0, p0, Lflipboard/util/SocialHelper$22;->a:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1014
    iget-object v0, p0, Lflipboard/util/SocialHelper$22;->a:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aB:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/util/SocialHelper$22;->a:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1015
    :cond_0
    iget-object v0, p0, Lflipboard/util/SocialHelper$22;->a:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    .line 1020
    :goto_0
    iget-object v1, p0, Lflipboard/util/SocialHelper$22;->a:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1024
    sget-object v1, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 1025
    iget-object v2, p0, Lflipboard/util/SocialHelper$22;->a:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    .line 1026
    if-eqz v1, :cond_3

    iget-object v1, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->o:Ljava/util/List;

    if-eqz v1, :cond_3

    .line 1028
    iget-object v1, p0, Lflipboard/util/SocialHelper$22;->b:Lflipboard/activities/FlipboardActivity;

    iget-object v2, p0, Lflipboard/util/SocialHelper$22;->a:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1038
    :cond_1
    :goto_1
    return-void

    .line 1017
    :cond_2
    iget-object v0, p0, Lflipboard/util/SocialHelper$22;->a:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aB:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 1033
    :cond_3
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1034
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1035
    const-string v0, "com.android.browser.application_id"

    const-string v2, "flipboard"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1036
    iget-object v0, p0, Lflipboard/util/SocialHelper$22;->b:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method
