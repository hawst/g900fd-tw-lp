.class Lflipboard/util/FLAnimation$AnimatorThread$1;
.super Ljava/lang/Object;
.source "FLAnimation.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:J

.field final synthetic b:Lflipboard/util/FLAnimation$AnimatorThread;


# direct methods
.method constructor <init>(Lflipboard/util/FLAnimation$AnimatorThread;J)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lflipboard/util/FLAnimation$AnimatorThread$1;->b:Lflipboard/util/FLAnimation$AnimatorThread;

    iput-wide p2, p0, Lflipboard/util/FLAnimation$AnimatorThread$1;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 115
    iget-object v0, p0, Lflipboard/util/FLAnimation$AnimatorThread$1;->b:Lflipboard/util/FLAnimation$AnimatorThread;

    iget-object v0, v0, Lflipboard/util/FLAnimation$AnimatorThread;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v3, v0, -0x1

    if-ltz v3, :cond_3

    .line 116
    iget-object v0, p0, Lflipboard/util/FLAnimation$AnimatorThread$1;->b:Lflipboard/util/FLAnimation$AnimatorThread;

    iget-object v0, v0, Lflipboard/util/FLAnimation$AnimatorThread;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/util/FLAnimation;

    .line 117
    iget-wide v4, p0, Lflipboard/util/FLAnimation$AnimatorThread$1;->a:J

    iget v1, v0, Lflipboard/util/FLAnimation;->c:I

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    const v4, 0x3f7ae148    # 0.98f

    cmpl-float v4, v1, v4

    if-lez v4, :cond_0

    move v1, v2

    .line 119
    :cond_0
    :try_start_0
    invoke-virtual {v0, v1}, Lflipboard/util/FLAnimation;->a(F)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 125
    :goto_2
    cmpl-float v0, v0, v2

    if-ltz v0, :cond_1

    .line 126
    iget-object v0, p0, Lflipboard/util/FLAnimation$AnimatorThread$1;->b:Lflipboard/util/FLAnimation$AnimatorThread;

    iget-object v0, v0, Lflipboard/util/FLAnimation$AnimatorThread;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_1
    move v0, v3

    .line 128
    goto :goto_0

    .line 117
    :cond_2
    iget-wide v6, v0, Lflipboard/util/FLAnimation;->d:J

    sub-long/2addr v4, v6

    long-to-float v1, v4

    iget v4, v0, Lflipboard/util/FLAnimation;->c:I

    int-to-float v4, v4

    div-float/2addr v1, v4

    goto :goto_1

    .line 120
    :catch_0
    move-exception v0

    .line 121
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v4, "error in animation: %-E"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v1, v4, v5}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 123
    goto :goto_2

    .line 129
    :cond_3
    return-void
.end method
