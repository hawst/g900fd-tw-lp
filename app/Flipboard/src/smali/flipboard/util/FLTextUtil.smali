.class public Lflipboard/util/FLTextUtil;
.super Ljava/lang/Object;
.source "FLTextUtil.java"


# direct methods
.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Landroid/os/Bundle;)Landroid/text/SpannableString;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedSectionLink;",
            ">;",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/text/SpannableString;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 51
    const/4 v0, 0x0

    .line 52
    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 53
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080087

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 57
    sget-object v1, Lflipboard/util/AndroidUtil;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 58
    :goto_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 59
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    .line 60
    if-nez v0, :cond_0

    .line 61
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 63
    :cond_0
    new-instance v4, Lflipboard/util/FLTextUtil$1;

    invoke-direct {v4, v2, v6}, Lflipboard/util/FLTextUtil$1;-><init>(Ljava/lang/String;I)V

    .line 69
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v2

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v7

    .line 63
    invoke-virtual {v0, v4, v2, v7, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 73
    :cond_1
    if-eqz p2, :cond_6

    .line 74
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v1, v0

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 75
    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    if-nez v2, :cond_3

    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 78
    :cond_3
    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 79
    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 80
    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    .line 86
    :goto_2
    if-ltz v4, :cond_2

    .line 87
    if-nez v1, :cond_4

    .line 88
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 90
    :cond_4
    new-instance v8, Lflipboard/util/FLTextUtil$2;

    invoke-direct {v8, v5, v0, p3, v6}, Lflipboard/util/FLTextUtil$2;-><init>(Lflipboard/service/FlipboardManager;Lflipboard/objs/FeedSectionLink;Landroid/os/Bundle;I)V

    add-int v0, v4, v2

    invoke-virtual {v1, v8, v4, v0, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    .line 82
    :cond_5
    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 83
    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 84
    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_2

    :cond_6
    move-object v1, v0

    .line 119
    :cond_7
    return-object v1

    :cond_8
    move v2, v3

    move v4, v3

    goto :goto_2
.end method

.method public static a(Landroid/widget/TextView;)V
    .locals 3

    .prologue
    .line 138
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 140
    instance-of v1, v0, Landroid/text/SpannedString;

    if-nez v1, :cond_1

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    check-cast v0, Landroid/text/SpannedString;

    .line 150
    invoke-virtual {p0}, Landroid/widget/TextView;->getLineCount()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    .line 153
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/Layout;->getLineVisibleEnd(I)I

    move-result v1

    .line 156
    const/4 v2, 0x0

    add-int/lit8 v1, v1, -0x3

    invoke-virtual {v0, v2, v1}, Landroid/text/SpannedString;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/SpannableString;

    .line 159
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u2026"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static b(Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 172
    new-instance v0, Lflipboard/util/FLTextUtil$3;

    invoke-direct {v0}, Lflipboard/util/FLTextUtil$3;-><init>()V

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 208
    return-void
.end method
