.class public Lflipboard/util/ShareHelper;
.super Ljava/lang/Object;
.source "ShareHelper.java"


# static fields
.field public static final a:Lflipboard/util/Log;

.field static final synthetic b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lflipboard/util/ShareHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lflipboard/util/ShareHelper;->b:Z

    .line 56
    sget-object v0, Lflipboard/activities/ShareActivity;->n:Lflipboard/util/Log;

    sput-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    return-void

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Intent;)Lflipboard/json/FLObject;
    .locals 5

    .prologue
    .line 679
    const-string v0, "flipboard.extra.selectedShareTargets"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 680
    if-eqz v4, :cond_2

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 681
    new-instance v3, Lflipboard/json/FLObject;

    invoke-direct {v3}, Lflipboard/json/FLObject;-><init>()V

    .line 682
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v2, v0, :cond_1

    .line 683
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 684
    add-int/lit8 v1, v2, 0x1

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 685
    if-eqz v1, :cond_0

    .line 686
    invoke-virtual {v3, v0, v1}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 682
    :cond_0
    add-int/lit8 v0, v2, 0x2

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v3

    .line 691
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Lflipboard/objs/UsageEventV2;Lflipboard/objs/Magazine;Ljava/lang/String;Ljava/lang/String;)Lflipboard/service/Flap$CancellableJSONResultObserver;
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lflipboard/util/ShareHelper$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lflipboard/util/ShareHelper$1;-><init>(Lflipboard/objs/UsageEventV2;Lflipboard/objs/Magazine;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const/high16 v0, 0x90000

    .line 390
    sget-boolean v1, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v1, :cond_2

    .line 394
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 407
    :cond_0
    :goto_0
    :try_start_0
    invoke-static {p0, p1, v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 413
    :goto_1
    return-object v0

    .line 397
    :cond_1
    const/high16 v0, 0x40000

    goto :goto_0

    .line 400
    :cond_2
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 401
    const/high16 v0, 0x200000

    goto :goto_0

    .line 408
    :catch_0
    move-exception v0

    .line 409
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    .line 410
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/content/Intent;Lflipboard/json/FLObject;)V
    .locals 4

    .prologue
    .line 667
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lflipboard/json/FLObject;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 668
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lflipboard/json/FLObject;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 669
    invoke-virtual {p1}, Lflipboard/json/FLObject;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 670
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 671
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 673
    :cond_0
    const-string v0, "flipboard.extra.selectedShareTargets"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 675
    :cond_1
    return-void
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V
    .locals 6

    .prologue
    .line 515
    iget-object v0, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->K:Ljava/lang/String;

    .line 516
    if-eqz v0, :cond_0

    .line 518
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->h(Ljava/lang/String;)Lflipboard/objs/Magazine;

    move-result-object v1

    .line 519
    if-nez v1, :cond_1

    .line 521
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->r()V

    .line 546
    :cond_0
    :goto_0
    return-void

    .line 526
    :cond_1
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 527
    const v3, 0x7f0d00c5

    invoke-virtual {p0, v3}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 528
    const v3, 0x7f0d00c4

    invoke-virtual {p0, v3}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v1, v1, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 529
    const v1, 0x7f0d004a

    invoke-virtual {v2, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 530
    const v1, 0x7f0d00c3

    invoke-virtual {v2, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 531
    new-instance v1, Lflipboard/util/ShareHelper$3;

    invoke-direct {v1, p0, v0}, Lflipboard/util/ShareHelper$3;-><init>(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    iput-object v1, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 544
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "delete_magzine_dialog"

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lflipboard/objs/ConfigService;Lflipboard/service/Account;)V
    .locals 3

    .prologue
    .line 602
    iget-boolean v0, p0, Lflipboard/objs/ConfigService;->av:Z

    if-nez v0, :cond_0

    .line 603
    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    .line 604
    iget-object v1, p0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    iget-object v2, p1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 605
    invoke-virtual {p1, v0}, Lflipboard/service/Account;->a(Lflipboard/json/FLObject;)V

    .line 660
    :goto_0
    return-void

    .line 614
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 619
    iget-object v1, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v2, Lflipboard/util/ShareHelper$6;

    invoke-direct {v2, p0, p1}, Lflipboard/util/ShareHelper$6;-><init>(Lflipboard/objs/ConfigService;Lflipboard/service/Account;)V

    invoke-virtual {v0, v1, p0, v2}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/User;Lflipboard/objs/ConfigService;Lflipboard/service/Flap$SectionListObserver;)Lflipboard/service/Flap$ShareListRequest;

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/objs/UsageEventV2$EventDataType;Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 54
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->f:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->d:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v0, v1, p0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->s:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v0, v1, p1}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->b:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v0, v1, p2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    return-void
.end method

.method public static a(Lflipboard/service/Flap$JSONResultObserver;Lflipboard/service/Flap$JSONResultObserver;Ljava/util/Map;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/service/Flap$JSONResultObserver;",
            "Lflipboard/service/Flap$JSONResultObserver;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lflipboard/service/Section;",
            "Lflipboard/activities/FlipboardActivity;",
            ")V"
        }
    .end annotation

    .prologue
    .line 462
    if-eqz p2, :cond_6

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_6

    .line 464
    const/4 v2, 0x0

    .line 465
    const/4 v1, 0x0

    .line 466
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 467
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 468
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 469
    :cond_0
    sget-boolean v3, Lflipboard/util/ShareHelper;->b:Z

    if-nez v3, :cond_2

    if-eqz v2, :cond_1

    if-nez v1, :cond_2

    :cond_1
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 474
    :cond_2
    sget-object v3, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v3}, Lflipboard/io/NetworkManager;->a()Z

    move-result v3

    if-nez v3, :cond_3

    const v1, 0x7f0d0218

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p9

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 481
    :goto_0
    return-void

    .line 474
    :cond_3
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v5, 0x1

    iput-boolean v5, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    :try_start_0
    invoke-static {v1, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    iget v5, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v6, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v3}, Lflipboard/util/HttpUtil;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    const-string v4, "image/jpeg"

    :cond_4
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v7

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v8, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v1, Lflipboard/service/Flap$UploadImageRequest;

    invoke-direct {v1, v7, v8}, Lflipboard/service/Flap$UploadImageRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    move-object v7, p0

    invoke-virtual/range {v1 .. v7}, Lflipboard/service/Flap$UploadImageRequest;->a(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;IILflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$UploadImageRequest;

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v2, v1}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_5
    sget-object v1, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    const-string v2, "Could not get bitmap for path %s - image cancelled"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v5

    invoke-virtual {v1, v2, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_6
    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    .line 479
    invoke-static/range {v1 .. v7}, Lflipboard/util/ShareHelper;->a(Lflipboard/service/Flap$JSONResultObserver;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Section;)V

    goto :goto_0
.end method

.method public static a(Lflipboard/service/Flap$JSONResultObserver;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Section;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/service/Flap$JSONResultObserver;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lflipboard/service/Section;",
            ")V"
        }
    .end annotation

    .prologue
    .line 485
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    move-object v1, p3

    move-object v2, p2

    move-object v3, p4

    move-object v4, p1

    move-object v5, p5

    move-object v6, p6

    move-object v7, p0

    .line 486
    invoke-virtual/range {v0 .. v7}, Lflipboard/service/FlipboardManager;->compose(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lflipboard/service/Section;Lflipboard/service/Flap$JSONResultObserver;)V

    .line 487
    return-void
.end method

.method public static a(Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;)V
    .locals 2

    .prologue
    .line 573
    iget-object v0, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->K:Ljava/lang/String;

    .line 574
    if-eqz v0, :cond_0

    .line 575
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->l(Ljava/lang/String;)Lflipboard/objs/Magazine;

    move-result-object v0

    .line 576
    if-eqz v0, :cond_0

    .line 577
    invoke-static {p1, v0, p0}, Lflipboard/activities/ShareActivity;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/Magazine;Lflipboard/service/Section;)V

    .line 580
    :cond_0
    return-void
.end method

.method public static a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 6

    .prologue
    .line 494
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 495
    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->K:Ljava/lang/String;

    .line 496
    iget-object v2, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {p0, v2}, Lflipboard/service/Section;->a(Lflipboard/service/User;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    .line 497
    :cond_0
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "can\'t remove item %s from magazine %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 502
    :goto_0
    return-void

    .line 501
    :cond_1
    iget-object v2, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v3, Lflipboard/service/Flap$RemoveFromMagazineRequest;

    invoke-direct {v3, v0, v2}, Lflipboard/service/Flap$RemoveFromMagazineRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v3, v1, p1, p2}, Lflipboard/service/Flap$RemoveFromMagazineRequest;->a(Ljava/lang/String;Lflipboard/objs/FeedItem;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$RemoveFromMagazineRequest;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 550
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 551
    iget-object v1, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v2, Lflipboard/util/ShareHelper$4;

    invoke-direct {v2, v0, p0}, Lflipboard/util/ShareHelper$4;-><init>(Lflipboard/service/FlipboardManager;Ljava/lang/String;)V

    new-instance v3, Lflipboard/service/Flap$DeleteMagazineRequest;

    invoke-direct {v3, v0, v1}, Lflipboard/service/Flap$DeleteMagazineRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v3, p0, v2}, Lflipboard/service/Flap$DeleteMagazineRequest;->a(Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$DeleteMagazineRequest;

    .line 569
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 584
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 585
    iget-object v1, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v2, Lflipboard/util/ShareHelper$5;

    invoke-direct {v2}, Lflipboard/util/ShareHelper$5;-><init>()V

    new-instance v3, Lflipboard/service/Flap$MoveMagazineRequest;

    invoke-direct {v3, v0, v1}, Lflipboard/service/Flap$MoveMagazineRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v3, p0, p1, v2}, Lflipboard/service/Flap$MoveMagazineRequest;->a(Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$MoveMagazineRequest;

    .line 597
    return-void
.end method

.method public static addToMagazine(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lflipboard/service/Section;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lflipboard/service/Section;",
            "Lflipboard/service/Flap$JSONResultObserver;",
            ")V"
        }
    .end annotation

    .prologue
    .line 104
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lflipboard/service/FlipboardManager;->compose(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lflipboard/service/Section;Lflipboard/service/Flap$JSONResultObserver;)V

    .line 106
    return-void
.end method

.method public static b(Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/service/Flap$JSONResultObserver;)V
    .locals 4

    .prologue
    .line 506
    sget-boolean v0, Lflipboard/util/ShareHelper;->b:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 507
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 508
    iget-object v1, p0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->K:Ljava/lang/String;

    .line 509
    iget-object v2, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    if-eqz p1, :cond_2

    new-instance v3, Lflipboard/service/Flap$PromoteToCoverRequest;

    invoke-direct {v3, v0, v2}, Lflipboard/service/Flap$PromoteToCoverRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v3, v1, p1, p2}, Lflipboard/service/Flap$PromoteToCoverRequest;->a(Ljava/lang/String;Lflipboard/objs/FeedItem;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$PromoteToCoverRequest;

    .line 510
    :cond_1
    return-void

    .line 509
    :cond_2
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Item is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
