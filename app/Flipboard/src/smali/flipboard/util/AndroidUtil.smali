.class public Lflipboard/util/AndroidUtil;
.super Ljava/lang/Object;
.source "AndroidUtil.java"


# static fields
.field public static final a:Ljava/util/regex/Pattern;

.field public static b:I

.field public static c:Landroid/util/DisplayMetrics;

.field public static d:Ljava/lang/String;

.field private static final e:[Ljava/lang/String;

.field private static final f:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 133
    const-string v0, "(((http[s]?|market)://?|www[.])[^\\s()<>\\p{Ideographic}]+(?:\\([\\w\\d]+\\)|([^[:punct:]\\s\\p{Ideographic}]|/)))"

    invoke-static {v0, v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lflipboard/util/AndroidUtil;->a:Ljava/util/regex/Pattern;

    .line 153
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "sstream.app"

    aput-object v1, v0, v4

    const-string v1, "com.sec.android.magazinewidget"

    aput-object v1, v0, v5

    const-string v1, "com.samsung.android.internal.headlines"

    aput-object v1, v0, v3

    const-string v1, "com.samsung.android.app.headlines"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "com.samsung.android.internal.providers.news"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "flipboard.boxer.debug"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "flipboard.boxer.internal"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "flipboard.boxer.app"

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/util/AndroidUtil;->e:[Ljava/lang/String;

    .line 163
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "flipboard.boxer.app"

    aput-object v1, v0, v4

    const-string v1, "flipboard.boxer.internal"

    aput-object v1, v0, v5

    const-string v1, "flipboard.boxer.debug"

    aput-object v1, v0, v3

    sput-object v0, Lflipboard/util/AndroidUtil;->f:[Ljava/lang/String;

    return-void
.end method

.method public static a(FLandroid/content/Context;)I
    .locals 2

    .prologue
    .line 861
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 862
    mul-float/2addr v0, p0

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static a(ILandroid/content/Context;)I
    .locals 2

    .prologue
    .line 867
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 868
    int-to-float v1, p0

    div-float v0, v1, v0

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static a(Landroid/app/Activity;)I
    .locals 2

    .prologue
    .line 536
    sget-object v0, Lflipboard/util/AndroidUtil;->c:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {p0}, Lflipboard/util/AndroidUtil;->b(Landroid/app/Activity;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public static a(Landroid/graphics/Bitmap$Config;)I
    .locals 4

    .prologue
    const/4 v1, 0x4

    const/4 v0, 0x2

    .line 873
    if-eqz p0, :cond_0

    .line 874
    sget-object v2, Lflipboard/util/AndroidUtil$7;->a:[I

    invoke-virtual {p0}, Landroid/graphics/Bitmap$Config;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    move v0, v1

    .line 885
    :goto_0
    :pswitch_0
    return v0

    .line 876
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 882
    goto :goto_0

    .line 874
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/graphics/Rect;)I
    .locals 3

    .prologue
    .line 568
    iget v0, p0, Landroid/graphics/Rect;->left:I

    mul-int/lit16 v0, v0, 0x4cf

    iget v1, p0, Landroid/graphics/Rect;->top:I

    const v2, 0x1e137

    mul-int/2addr v1, v2

    xor-int/2addr v0, v1

    iget v1, p0, Landroid/graphics/Rect;->right:I

    mul-int/lit16 v1, v1, 0x6eb

    xor-int/2addr v0, v1

    iget v1, p0, Landroid/graphics/Rect;->bottom:I

    const v2, 0x76944d

    mul-int/2addr v1, v2

    xor-int/2addr v0, v1

    return v0
.end method

.method public static a(Lflipboard/objs/ConfigService;)I
    .locals 2

    .prologue
    .line 573
    iget-object v0, p0, Lflipboard/objs/ConfigService;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/ConfigService;->i:Ljava/lang/String;

    .line 574
    :goto_0
    const-string v1, "heart"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 575
    const v0, 0x7f02000b

    .line 584
    :goto_1
    return v0

    .line 573
    :cond_0
    const-string v0, "thumbsUp"

    goto :goto_0

    .line 576
    :cond_1
    const-string v1, "thumbsUp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 577
    const v0, 0x7f02000c

    goto :goto_1

    .line 578
    :cond_2
    const-string v1, "star"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 579
    const v0, 0x7f02000e

    goto :goto_1

    .line 580
    :cond_3
    const-string v1, "plusOne"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 581
    const v0, 0x7f02000d

    goto :goto_1

    .line 584
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/io/File;)J
    .locals 4

    .prologue
    .line 238
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/os/StatFs;

    invoke-direct {v1, v0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    goto :goto_0
.end method

.method public static a(Landroid/location/Location;Landroid/location/Location;)Landroid/location/Location;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1009
    if-nez p1, :cond_1

    .line 1047
    :cond_0
    :goto_0
    return-object p0

    .line 1011
    :cond_1
    if-nez p0, :cond_2

    move-object p0, p1

    .line 1012
    goto :goto_0

    .line 1016
    :cond_2
    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 1017
    const-wide/32 v6, 0x1d4c0

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    move v3, v1

    .line 1018
    :goto_1
    const-wide/32 v6, -0x1d4c0

    cmp-long v0, v4, v6

    if-gez v0, :cond_4

    move v0, v1

    .line 1019
    :goto_2
    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_5

    move v5, v1

    .line 1023
    :goto_3
    if-nez v3, :cond_0

    .line 1026
    if-eqz v0, :cond_6

    move-object p0, p1

    .line 1027
    goto :goto_0

    :cond_3
    move v3, v2

    .line 1017
    goto :goto_1

    :cond_4
    move v0, v2

    .line 1018
    goto :goto_2

    :cond_5
    move v5, v2

    .line 1019
    goto :goto_3

    .line 1031
    :cond_6
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    sub-float/2addr v0, v3

    float-to-int v0, v0

    .line 1032
    if-lez v0, :cond_9

    move v4, v1

    .line 1033
    :goto_4
    if-gez v0, :cond_a

    move v3, v1

    .line 1034
    :goto_5
    const/16 v6, 0xc8

    if-le v0, v6, :cond_b

    move v0, v1

    .line 1037
    :goto_6
    invoke-virtual {p0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v7

    if-nez v6, :cond_d

    if-nez v7, :cond_c

    .line 1040
    :goto_7
    if-nez v3, :cond_0

    .line 1042
    if-eqz v5, :cond_7

    if-eqz v4, :cond_0

    .line 1044
    :cond_7
    if-eqz v5, :cond_8

    if-nez v0, :cond_8

    if-nez v1, :cond_0

    :cond_8
    move-object p0, p1

    .line 1047
    goto :goto_0

    :cond_9
    move v4, v2

    .line 1032
    goto :goto_4

    :cond_a
    move v3, v2

    .line 1033
    goto :goto_5

    :cond_b
    move v0, v2

    .line 1034
    goto :goto_6

    :cond_c
    move v1, v2

    .line 1037
    goto :goto_7

    :cond_d
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_7
.end method

.method public static a(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1235
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1236
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1237
    check-cast v0, Landroid/view/View;

    .line 1245
    :goto_1
    return-object v0

    .line 1239
    :cond_0
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_1

    .line 1240
    check-cast v0, Landroid/view/View;

    move-object p0, v0

    .line 1244
    goto :goto_0

    .line 1245
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)Lflipboard/gui/dialog/FLProgressDialogFragment;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 359
    new-instance v0, Lflipboard/gui/dialog/FLProgressDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLProgressDialogFragment;-><init>()V

    .line 360
    iput-boolean v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->C:Z

    .line 361
    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLProgressDialogFragment;->a(Z)V

    .line 362
    iput-object p1, v0, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 363
    invoke-virtual {v0, p0, p1}, Lflipboard/gui/dialog/FLProgressDialogFragment;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 364
    return-object v0
.end method

.method public static a(Ljava/util/List;I)Lflipboard/objs/FeedItem;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;I)",
            "Lflipboard/objs/FeedItem;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 306
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    const/4 v0, 0x0

    .line 332
    :goto_0
    return-object v0

    .line 311
    :cond_0
    const/16 v0, 0xa

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v6

    move v2, v4

    move v1, v4

    move v3, v4

    :goto_1
    if-ge v2, v6, :cond_7

    .line 312
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 313
    iget-boolean v5, v0, Lflipboard/objs/FeedItem;->an:Z

    if-eqz v5, :cond_1

    move v5, v4

    :goto_2
    if-eqz v5, :cond_8

    .line 314
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    .line 318
    if-nez v2, :cond_6

    move v0, v4

    .line 327
    :goto_3
    if-le v0, v1, :cond_8

    move v1, v2

    .line 311
    :goto_4
    add-int/lit8 v2, v2, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    .line 313
    :cond_1
    iget v5, v0, Lflipboard/objs/FeedItem;->ak:I

    if-lez v5, :cond_2

    move v5, v4

    goto :goto_2

    :cond_2
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->g()Z

    move-result v5

    if-nez v5, :cond_3

    move v5, v4

    goto :goto_2

    :cond_3
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->i()Z

    move-result v5

    if-nez v5, :cond_4

    move v5, v4

    goto :goto_2

    :cond_4
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v5

    invoke-virtual {v5}, Lflipboard/objs/Image;->b()Z

    move-result v5

    if-eqz v5, :cond_5

    move v5, v4

    goto :goto_2

    :cond_5
    const/4 v5, 0x1

    goto :goto_2

    .line 323
    :cond_6
    invoke-virtual {v0}, Lflipboard/objs/Image;->a()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 325
    div-int/lit8 v5, v2, 0xa

    rsub-int/lit8 v5, v5, 0x1

    mul-int/2addr v0, v5

    goto :goto_3

    .line 332
    :cond_7
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    goto :goto_0

    :cond_8
    move v0, v1

    move v1, v3

    goto :goto_4
.end method

.method public static a()Lflipboard/objs/SectionPageTemplate;
    .locals 5

    .prologue
    const/16 v4, 0x28a

    const/high16 v3, 0x43200000    # 160.0f

    .line 285
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 286
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    iget v2, v0, Landroid/util/DisplayMetrics;->xdpi:F

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    .line 287
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v2

    iget v0, v0, Landroid/util/DisplayMetrics;->ydpi:F

    div-float v0, v2, v0

    mul-float/2addr v0, v3

    float-to-int v0, v0

    .line 290
    if-le v1, v4, :cond_0

    if-le v0, v4, :cond_0

    .line 291
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const-string v1, "Trio"

    invoke-virtual {v0, v1}, Lflipboard/app/FlipboardApplication;->a(Ljava/lang/String;)Lflipboard/objs/SectionPageTemplate;

    move-result-object v0

    .line 295
    :goto_0
    return-object v0

    .line 293
    :cond_0
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const-string v1, "Double team"

    invoke-virtual {v0, v1}, Lflipboard/app/FlipboardApplication;->a(Ljava/lang/String;)Lflipboard/objs/SectionPageTemplate;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 221
    const-string v0, "external"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 222
    const-string v0, "cache"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 223
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 233
    :cond_0
    :goto_0
    return-object v0

    .line 227
    :cond_1
    const-string v0, "internal"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 228
    const-string v0, "cache"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 229
    if-nez v0, :cond_0

    .line 233
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1146
    sparse-switch p0, :sswitch_data_0

    .line 1154
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 1148
    :sswitch_0
    const-string v0, "exactly"

    goto :goto_0

    .line 1150
    :sswitch_1
    const-string v0, "at_most"

    goto :goto_0

    .line 1152
    :sswitch_2
    const-string v0, "unspecified"

    goto :goto_0

    .line 1146
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_2
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 823
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 824
    if-eqz v0, :cond_0

    const-string v1, "9774d56d682e549c"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 826
    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 828
    :cond_1
    return-object v0
.end method

.method private static a(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/media/ExifInterface;)Ljava/lang/String;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1321
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 1322
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1323
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    const/16 v2, 0x2f

    if-eq v0, v2, :cond_0

    .line 1324
    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1326
    :cond_0
    const-string v0, "share_image.jpg"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1327
    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1328
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1329
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 1331
    invoke-static {p1, v0}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Landroid/graphics/Bitmap;Ljava/io/File;)V

    .line 1332
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1333
    if-eqz p2, :cond_1

    .line 1334
    new-instance v1, Landroid/media/ExifInterface;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 1335
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1336
    const-string v3, "FNumber"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1337
    const-string v3, "ExposureTime"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1338
    const-string v3, "ISOSpeedRatings"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1339
    const-string v3, "GPSAltitude"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1340
    const-string v3, "GPSAltitudeRef"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1341
    const-string v3, "DateTime"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1342
    const-string v3, "Flash"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1343
    const-string v3, "FocalLength"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1344
    const-string v3, "GPSDateStamp"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1345
    const-string v3, "GPSLatitude"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1346
    const-string v3, "GPSLatitudeRef"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1347
    const-string v3, "GPSLongitude"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1348
    const-string v3, "GPSLongitudeRef"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1349
    const-string v3, "GPSProcessingMethod"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1350
    const-string v3, "GPSTimeStamp"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1351
    const-string v3, "ImageLength"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1352
    const-string v3, "ImageWidth"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1353
    const-string v3, "Make"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1354
    const-string v3, "Model"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1355
    const-string v3, "Orientation"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1356
    const-string v3, "WhiteBalance"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1358
    invoke-static {p2, v1, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/media/ExifInterface;Landroid/media/ExifInterface;Ljava/util/List;)V

    .line 1360
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 1466
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1467
    iput-boolean v8, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1468
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1469
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    mul-int/2addr v1, v2

    div-int/2addr v1, p2

    int-to-double v2, v1

    .line 1470
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_0

    .line 1472
    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1473
    new-instance v4, Landroid/media/ExifInterface;

    invoke-direct {v4, p1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 1474
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 1475
    iget v5, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-double v6, v5

    div-double/2addr v6, v2

    double-to-int v5, v6

    .line 1476
    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-double v6, v0

    div-double v2, v6, v2

    double-to-int v0, v2

    .line 1477
    invoke-static {v1, v5, v0, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1478
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1479
    invoke-static {p0, v0, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/media/ExifInterface;)Ljava/lang/String;

    move-result-object p1

    .line 1483
    :cond_0
    return-object p1
.end method

.method public static a(Landroid/net/Uri;Landroid/app/Activity;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 1268
    if-nez p0, :cond_1

    .line 1303
    :cond_0
    :goto_0
    return-object v3

    .line 1273
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content://com.android.gallery3d.provider"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1275
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.android.gallery3d"

    const-string v2, "com.google.android.gallery3d"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1278
    :goto_1
    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v6

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    .line 1280
    invoke-virtual/range {v0 .. v5}, Landroid/app/Activity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1281
    if-eqz v0, :cond_2

    .line 1282
    const-string v2, "_data"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 1283
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1284
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-eqz v4, :cond_0

    .line 1286
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1291
    :goto_2
    if-nez v0, :cond_3

    .line 1295
    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1296
    if-eqz v0, :cond_0

    .line 1297
    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/media/ExifInterface;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 1289
    :cond_2
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1299
    :catch_0
    move-exception v0

    .line 1300
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "Error sharing image to somewhere else, %s"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v0, v4, v6

    invoke-virtual {v1, v2, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move-object v3, v0

    goto :goto_0

    :cond_4
    move-object v1, p0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 201
    const-string v0, " "

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 202
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 203
    invoke-virtual {v1, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 206
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 207
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 191
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 192
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v4, v0, Lflipboard/objs/FeedItem;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v3

    .line 193
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lflipboard/service/Section;->s()Z

    move-result v3

    if-nez v3, :cond_0

    .line 194
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 197
    :cond_2
    return-object v1
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 960
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/DetailActivityStayOnRotation;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 961
    const-string v1, "detail_open_url"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 962
    const-string v1, "use_wide_viewport"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 963
    const/16 v1, 0x65

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 964
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1595
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1596
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1597
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 950
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/DetailActivityStayOnRotation;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 951
    const-string v1, "detail_open_url"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 952
    if-eqz p2, :cond_0

    .line 953
    const-string v1, "sid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 955
    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 956
    return-void
.end method

.method public static a(Landroid/content/SharedPreferences$Editor;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1708
    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1709
    return-void
.end method

.method public static a(Landroid/graphics/Paint;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1538
    invoke-virtual {p0}, Landroid/graphics/Paint;->getFlags()I

    move-result v0

    or-int/lit8 v0, v0, 0x1

    .line 1542
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setFlags(I)V

    .line 1543
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setHinting(I)V

    .line 1544
    return-void
.end method

.method private static a(Landroid/media/ExifInterface;Landroid/media/ExifInterface;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/media/ExifInterface;",
            "Landroid/media/ExifInterface;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1365
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1366
    invoke-virtual {p0, v0}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1367
    if-eqz v2, :cond_0

    .line 1368
    invoke-virtual {p1, v0, v2}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1371
    :cond_1
    invoke-virtual {p1}, Landroid/media/ExifInterface;->saveAttributes()V

    .line 1372
    return-void
.end method

.method public static a(Landroid/os/AsyncTask;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1824
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v0, Lflipboard/service/FlipboardManager;->av:Ljava/util/concurrent/Executor;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1825
    return-void
.end method

.method public static a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 269
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object p0, v0

    goto :goto_0

    .line 271
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 272
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Landroid/view/ViewParent;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ",layout=true"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    aput-object v1, v0, v3

    .line 273
    const/4 v0, 0x2

    invoke-static {p0, v3, v0}, Lflipboard/util/AndroidUtil;->b(Landroid/view/View;ZI)V

    .line 277
    :goto_1
    return-void

    .line 275
    :cond_2
    invoke-static {p0, v3, v3}, Lflipboard/util/AndroidUtil;->b(Landroid/view/View;ZI)V

    goto :goto_1
.end method

.method public static a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1406
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 1407
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1409
    :cond_0
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/graphics/Rect;ILandroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 1111
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;Landroid/graphics/Rect;ILandroid/graphics/Rect;Ljava/lang/Runnable;)V

    .line 1112
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/graphics/Rect;ILandroid/graphics/Rect;Ljava/lang/Runnable;)V
    .locals 6

    .prologue
    .line 1116
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 1117
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    .line 1118
    iget v2, p1, Landroid/graphics/Rect;->left:I

    if-ne v0, v2, :cond_1

    iget v0, p1, Landroid/graphics/Rect;->top:I

    if-ne v1, v0, :cond_1

    .line 1120
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 1121
    if-eqz p4, :cond_0

    .line 1122
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, p4}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 1127
    :cond_0
    :goto_0
    return-void

    .line 1126
    :cond_1
    new-instance v0, Lflipboard/util/FLAnimation$Move;

    move-object v1, p0

    move v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lflipboard/util/FLAnimation$Move;-><init>(Landroid/view/View;ILandroid/graphics/Rect;Landroid/graphics/Rect;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Ljava/lang/Class;Lflipboard/util/Callback;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lflipboard/util/Callback",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1211
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 1212
    check-cast v0, Landroid/view/ViewGroup;

    .line 1213
    :goto_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/view/View;

    if-eqz v1, :cond_1

    .line 1214
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1215
    if-eqz v0, :cond_1

    .line 1216
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1219
    invoke-interface {p2, v0}, Lflipboard/util/Callback;->a(Ljava/lang/Object;)V

    .line 1230
    :goto_1
    return-void

    .line 1222
    :cond_0
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 1223
    check-cast v0, Landroid/view/ViewGroup;

    goto :goto_0

    .line 1229
    :cond_1
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Could not find view ancestor of type %s for %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static a(Landroid/view/View;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 765
    instance-of v0, p0, Lflipboard/gui/FLViewIntf;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 766
    check-cast v0, Lflipboard/gui/FLViewIntf;

    invoke-interface {v0, p1, v1}, Lflipboard/gui/FLViewIntf;->a(ZI)V

    .line 768
    :cond_0
    instance-of v0, p0, Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_1

    .line 769
    check-cast p0, Lflipboard/gui/flipping/FlipTransitionViews;

    .line 770
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    neg-int v0, v0

    .line 771
    invoke-virtual {p0}, Lflipboard/gui/flipping/FlipTransitionViews;->getFlippableViews()Ljava/util/List;

    move-result-object v3

    .line 772
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v1

    move v1, v0

    :goto_0
    if-ge v2, v4, :cond_2

    .line 773
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;ZI)V

    .line 772
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 775
    :cond_1
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 776
    check-cast p0, Landroid/view/ViewGroup;

    .line 777
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_2

    .line 778
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1, p1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;Z)V

    .line 777
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 781
    :cond_2
    return-void
.end method

.method public static a(Landroid/view/View;ZI)V
    .locals 3

    .prologue
    .line 785
    instance-of v0, p0, Lflipboard/gui/FLViewIntf;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 786
    check-cast v0, Lflipboard/gui/FLViewIntf;

    invoke-interface {v0, p1, p2}, Lflipboard/gui/FLViewIntf;->a(ZI)V

    .line 788
    :cond_0
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 789
    check-cast p0, Landroid/view/ViewGroup;

    .line 790
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    .line 791
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2, p1, p2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;ZI)V

    .line 790
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 794
    :cond_1
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;Lflipboard/util/Callback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Lflipboard/util/Callback",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1179
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 1180
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1181
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1182
    invoke-interface {p1, v0}, Lflipboard/util/Callback;->a(Ljava/lang/Object;)V

    .line 1183
    instance-of v3, v0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    .line 1184
    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {v0, p1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/ViewGroup;Lflipboard/util/Callback;)V

    .line 1180
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1187
    :cond_1
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;Ljava/lang/Class;Lflipboard/util/Callback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/ViewGroup;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lflipboard/util/Callback",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1194
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 1195
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 1196
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1197
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1198
    invoke-interface {p2, v0}, Lflipboard/util/Callback;->a(Ljava/lang/Object;)V

    .line 1200
    :cond_0
    instance-of v3, v0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_1

    .line 1201
    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {v0, p1, p2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/ViewGroup;Ljava/lang/Class;Lflipboard/util/Callback;)V

    .line 1195
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1204
    :cond_2
    return-void
.end method

.method public static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1425
    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    .line 1426
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1427
    invoke-interface {v2}, Landroid/text/Spanned;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 1428
    invoke-interface {v2}, Landroid/text/Spanned;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const-class v3, Landroid/text/style/URLSpan;

    invoke-interface {v2, v1, v0, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 1429
    array-length v3, v0

    if-lez v3, :cond_1

    .line 1430
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 1431
    invoke-interface {v2, v4}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    .line 1432
    invoke-interface {v2, v4}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    .line 1433
    new-instance v7, Lflipboard/util/AndroidUtil$FlURLSpan;

    invoke-virtual {v4}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lflipboard/util/AndroidUtil$FlURLSpan;-><init>(Ljava/lang/String;)V

    new-instance v8, Landroid/text/SpannableString;

    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v8, v4}, Landroid/text/SpannableString;->removeSpan(Ljava/lang/Object;)V

    const/16 v4, 0x21

    invoke-virtual {v8, v7, v5, v6, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {p0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1430
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1435
    :cond_0
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1438
    :cond_1
    return-void
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 974
    iget-object v3, p2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const/4 v0, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 982
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lflipboard/activities/DetailActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 983
    const-string v2, "sid"

    invoke-virtual {p1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 984
    const-string v2, "extra_current_item"

    iget-object v3, p2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 985
    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    .line 986
    iget-object v0, p2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v2, "image"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 987
    const v0, 0x7f040001

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/FlipboardActivity;->overridePendingTransition(II)V

    .line 991
    :cond_1
    :goto_1
    return-void

    .line 974
    :sswitch_0
    const-string v4, "status"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v4, "video"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    goto :goto_0

    .line 976
    :pswitch_0
    sget-object v0, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->c:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    invoke-static {p2, p1, p0, v0}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/UsageEventV2$SocialCardNavFrom;)V

    goto :goto_1

    .line 979
    :pswitch_1
    invoke-virtual {p1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p2, v0, v2}, Lflipboard/util/VideoUtil;->a(Landroid/app/Activity;Lflipboard/objs/FeedItem;Ljava/lang/String;Z)V

    goto :goto_1

    .line 974
    :sswitch_data_0
    .sparse-switch
        -0x3532300e -> :sswitch_0
        0x6b0147b -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lflipboard/gui/FLCameleonImageView;ZZ)V
    .locals 1

    .prologue
    .line 630
    if-eqz p1, :cond_0

    const v0, 0x7f0800aa

    .line 631
    :goto_0
    invoke-virtual {p0, v0}, Lflipboard/gui/FLCameleonImageView;->setDefaultAndCheckedColors$255f295(I)V

    .line 632
    invoke-virtual {p0, p2}, Lflipboard/gui/FLCameleonImageView;->setChecked(Z)V

    .line 633
    return-void

    .line 630
    :cond_0
    const v0, 0x7f080038

    goto :goto_0
.end method

.method public static a(Lflipboard/gui/FLTextIntf;I)V
    .locals 0

    .prologue
    .line 1401
    check-cast p0, Landroid/view/View;

    invoke-static {p0, p1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 1402
    return-void
.end method

.method public static a(Lflipboard/objs/Magazine;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1881
    new-instance v1, Lflipboard/objs/UsageEventV2;

    sget-object v0, Lflipboard/objs/UsageEventV2$EventAction;->r:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->e:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v1, v0, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 1883
    iget-object v0, p0, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    const-string v2, "auth/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1884
    iget-object v0, p0, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1888
    :goto_0
    iget-boolean v2, p0, Lflipboard/objs/Magazine;->m:Z

    if-eqz v2, :cond_0

    .line 1889
    const-string v2, "default_"

    iget-object v3, p0, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1890
    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->f:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v1, v3, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 1892
    :cond_0
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->e:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v1, v2, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 1893
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->h:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/objs/Magazine;->l:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 1894
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->g:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 1895
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {p0}, Lflipboard/objs/Magazine;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lflipboard/objs/UsageEventV2$EventDataType;->c:Lflipboard/objs/UsageEventV2$EventDataType;

    :goto_1
    invoke-virtual {v1, v2, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 1896
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v1, v0, p1}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 1897
    invoke-virtual {v1}, Lflipboard/objs/UsageEventV2;->c()V

    .line 1898
    return-void

    .line 1886
    :cond_1
    iget-object v0, p0, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    goto :goto_0

    .line 1895
    :cond_2
    sget-object v0, Lflipboard/objs/UsageEventV2$EventDataType;->d:Lflipboard/objs/UsageEventV2$EventDataType;

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1713
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1714
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    .line 1718
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->K:Ljava/lang/String;

    .line 1719
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v0, Lflipboard/service/FlipboardManager;->L:Ljava/lang/String;

    .line 1721
    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    .line 1722
    new-instance v3, Landroid/content/Intent;

    const-string v0, "flipboard.app.broadcast.SYNC_USER_CHANGE"

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1724
    const-string v0, "uid"

    invoke-virtual {v3, v0, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1725
    const-string v0, "tuuid"

    invoke-virtual {v3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1726
    const-string v0, "service_name"

    invoke-virtual {v3, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1727
    const-string v4, "sstream.api.action.id"

    if-eqz p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1729
    const/16 v0, 0x20

    invoke-virtual {v3, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1730
    const-string v0, "sstream.api.access.token"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1731
    const-string v0, "sstream.api.refresh.token"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1735
    sget-object v4, Lflipboard/util/AndroidUtil;->e:[Ljava/lang/String;

    array-length v5, v4

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v5, :cond_2

    aget-object v6, v4, v0

    .line 1736
    invoke-virtual {v3, v6}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1737
    sget-object v7, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const-string v8, "sstream.app.broadcast.SYNC_USER"

    invoke-virtual {v7, v3, v8}, Lflipboard/app/FlipboardApplication;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 1738
    sget-object v7, Lflipboard/util/Log$Level;->b:Lflipboard/util/Log$Level;

    const-string v8, "Broadcast sync intent to api lib package %s [userid: %s]  [service: %s]  [logout: %s] [accessToken: %s]  [refreshToken: %s]"

    const/4 v9, 0x6

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v6, v9, v10

    const/4 v6, 0x1

    aput-object p0, v9, v6

    const/4 v6, 0x2

    aput-object p2, v9, v6

    const/4 v6, 0x3

    .line 1739
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v9, v6

    const/4 v6, 0x4

    const/4 v10, 0x6

    invoke-static {v10, v1}, Lflipboard/util/Log;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v6

    const/4 v6, 0x5

    const/4 v10, 0x6

    invoke-static {v10, v2}, Lflipboard/util/Log;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v6

    .line 1738
    invoke-static {v7, v8, v9}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1735
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1727
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1741
    :cond_1
    if-eqz p0, :cond_2

    const-string v0, "0"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1745
    sget-object v0, Lflipboard/util/Log$Level;->b:Lflipboard/util/Log$Level;

    const-string v1, "Send request to fetch api accesstoken from flap (uid is %s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1746
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/util/AndroidUtil$5;

    invoke-direct {v1}, Lflipboard/util/AndroidUtil$5;-><init>()V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/Flap$JSONResultObserver;)V

    .line 1759
    :cond_2
    return-void
.end method

.method public static a(Ljava/util/List;Lflipboard/service/Flap$TypedResultObserver;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/objs/RecentImage;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 644
    const/16 v0, 0x64

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x6

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    sput v0, Lflipboard/util/AndroidUtil;->b:I

    .line 645
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-object v0, v0, Lflipboard/app/FlipboardApplication;->i:Ljava/util/LinkedHashMap;

    if-nez v0, :cond_0

    .line 646
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    new-instance v1, Lflipboard/util/AndroidUtil$1;

    sget v4, Lflipboard/util/AndroidUtil;->b:I

    add-int/lit8 v4, v4, 0x1

    invoke-direct {v1, v4}, Lflipboard/util/AndroidUtil$1;-><init>(I)V

    iput-object v1, v0, Lflipboard/app/FlipboardApplication;->i:Ljava/util/LinkedHashMap;

    .line 654
    :cond_0
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-object v4, v0, Lflipboard/app/FlipboardApplication;->i:Ljava/util/LinkedHashMap;

    .line 656
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 657
    invoke-virtual {v4, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/RecentImage;

    .line 658
    if-eqz v0, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, v0, Lflipboard/objs/RecentImage;->a:J

    sub-long/2addr v6, v8

    const-wide/32 v8, 0x927c0

    cmp-long v0, v6, v8

    if-lez v0, :cond_2

    move v0, v2

    :goto_1
    if-eqz v0, :cond_6

    :cond_1
    move v0, v3

    :goto_2
    move v1, v0

    .line 661
    goto :goto_0

    :cond_2
    move v0, v3

    .line 658
    goto :goto_1

    .line 663
    :cond_3
    if-eqz v1, :cond_5

    .line 664
    if-eqz p1, :cond_4

    .line 665
    invoke-interface {p1, v4}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/Object;)V

    .line 702
    :cond_4
    :goto_3
    return-void

    .line 673
    :cond_5
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v2, Lflipboard/util/AndroidUtil$2;

    invoke-direct {v2, v4, p0, p1}, Lflipboard/util/AndroidUtil$2;-><init>(Ljava/util/LinkedHashMap;Ljava/util/List;Lflipboard/service/Flap$TypedResultObserver;)V

    invoke-virtual {v0, v1, p0, v2}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/User;Ljava/util/List;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$ImageRequest;

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public static a(Lorg/apache/http/HttpRequest;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 841
    invoke-interface {p0}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    .line 842
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getMethod()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getProtocolVersion()Lorg/apache/http/ProtocolVersion;

    move-result-object v0

    aput-object v0, v2, v8

    .line 844
    invoke-interface {p0}, Lorg/apache/http/HttpRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 845
    new-array v5, v8, [Ljava/lang/Object;

    invoke-interface {v4}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-interface {v4}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v7

    .line 844
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 847
    :cond_0
    return-void
.end method

.method public static a(Lorg/apache/http/HttpResponse;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 851
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    .line 852
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getProtocolVersion()Lorg/apache/http/ProtocolVersion;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v8

    .line 854
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 855
    new-array v5, v8, [Ljava/lang/Object;

    invoke-interface {v4}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-interface {v4}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v7

    .line 854
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 857
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 945
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v1, 0x10000

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/view/MotionEvent;Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 833
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 1132
    :goto_0
    if-ne p1, p0, :cond_0

    .line 1133
    const/4 v0, 0x1

    .line 1139
    :goto_1
    return v0

    .line 1135
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1136
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_1

    .line 1137
    check-cast v0, Landroid/view/View;

    move-object p1, v0

    goto :goto_0

    .line 1139
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1781
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v0

    .line 1783
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 1784
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1785
    const/4 v0, 0x1

    .line 1789
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static a(ZI)Z
    .locals 6

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    const/4 v4, -0x1

    const/4 v1, 0x1

    .line 798
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v0, v5, :cond_2

    move v0, v1

    .line 801
    :goto_0
    sget-object v5, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v5, v5, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v5, :cond_4

    .line 803
    if-eqz v0, :cond_3

    move v0, v3

    :goto_1
    move v3, v0

    move v0, v4

    .line 808
    :goto_2
    if-eqz p0, :cond_0

    if-lt p1, v0, :cond_0

    if-le p1, v3, :cond_1

    :cond_0
    if-nez p1, :cond_7

    :cond_1
    :goto_3
    return v1

    :cond_2
    move v0, v2

    .line 798
    goto :goto_0

    :cond_3
    move v0, v1

    .line 803
    goto :goto_1

    .line 805
    :cond_4
    if-eqz v0, :cond_5

    const/4 v4, -0x3

    .line 806
    :cond_5
    if-eqz v0, :cond_6

    const/4 v3, 0x5

    :cond_6
    move v0, v4

    goto :goto_2

    :cond_7
    move v1, v2

    .line 808
    goto :goto_3
.end method

.method public static b()I
    .locals 2

    .prologue
    .line 511
    sget-object v0, Lflipboard/util/AndroidUtil;->c:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v0, v0

    const v1, 0x3f59999a    # 0.85f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static b(Landroid/app/Activity;)I
    .locals 2

    .prologue
    .line 1548
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1549
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1550
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1551
    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method

.method public static b(Lflipboard/objs/ConfigService;)I
    .locals 2

    .prologue
    .line 589
    iget-object v0, p0, Lflipboard/objs/ConfigService;->ah:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/ConfigService;->ah:Ljava/lang/String;

    .line 591
    :goto_0
    const-string v1, "reblog"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 592
    const v0, 0x7f020013

    .line 597
    :goto_1
    return v0

    .line 589
    :cond_0
    const-string v0, "retweet"

    goto :goto_0

    .line 593
    :cond_1
    const-string v1, "retweet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 594
    const v0, 0x7f020015

    goto :goto_1

    .line 597
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 899
    new-instance v2, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 900
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 903
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 904
    const v3, 0x10040

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 907
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 908
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 910
    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    if-eqz v5, :cond_0

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    invoke-virtual {v5}, Landroid/content/IntentFilter;->countDataPaths()I

    move-result v5

    if-le v5, v1, :cond_0

    .line 911
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 915
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 925
    new-instance v4, Landroid/content/ComponentName;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v4, v5, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 928
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    new-array v4, v0, [Landroid/content/Intent;

    .line 929
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 930
    new-instance v5, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 931
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 932
    new-instance v6, Landroid/content/ComponentName;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v6, v7, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 933
    add-int/lit8 v0, v1, -0x1

    aput-object v5, v4, v0

    .line 929
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 918
    :pswitch_0
    const/4 v0, 0x0

    .line 939
    :goto_2
    return-object v0

    .line 921
    :pswitch_1
    new-instance v1, Landroid/content/ComponentName;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v4, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-object v0, v2

    .line 922
    goto :goto_2

    .line 937
    :cond_2
    const v0, 0x7f0d0347

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 938
    const-string v1, "android.intent.extra.INITIAL_INTENTS"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_2

    .line 915
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Ljava/lang/String;)Landroid/text/SpannableString;
    .locals 7

    .prologue
    .line 1840
    const/4 v0, 0x0

    .line 1841
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080087

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 1845
    sget-object v2, Lflipboard/util/AndroidUtil;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 1846
    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1847
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v3

    .line 1848
    if-nez v0, :cond_0

    .line 1849
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1851
    :cond_0
    new-instance v4, Lflipboard/util/AndroidUtil$6;

    invoke-direct {v4, v3, v1}, Lflipboard/util/AndroidUtil$6;-><init>(Ljava/lang/String;I)V

    .line 1858
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->start()I

    move-result v3

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->end()I

    move-result v5

    const/4 v6, 0x0

    .line 1851
    invoke-virtual {v0, v4, v3, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 1860
    :cond_1
    return-object v0
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1161
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 1162
    new-instance v1, Ljava/util/zip/ZipFile;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/util/zip/ZipFile;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1164
    :try_start_1
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd HH:mm, Z"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/util/Date;

    const-string v3, "classes.dex"

    invoke-virtual {v1, v3}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/zip/ZipEntry;->getTime()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1166
    :try_start_2
    invoke-virtual {v1}, Ljava/util/zip/ZipFile;->close()V

    .line 1170
    :goto_0
    return-object v0

    .line 1166
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/util/zip/ZipFile;->close()V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1168
    :catch_0
    move-exception v0

    .line 1169
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 1170
    const-string v0, "unknown"

    goto :goto_0
.end method

.method private static b(Landroid/view/View;ZI)V
    .locals 7

    .prologue
    const/16 v6, 0x40

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 402
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    :goto_0
    if-ge v0, p2, :cond_0

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/view/View;->getId()I

    move-result v4

    const/4 v0, -0x1

    if-eq v4, v0, :cond_3

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz v5, :cond_1

    :try_start_0
    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_1
    :goto_1
    if-nez v0, :cond_2

    const-string v0, "id(0x%x)"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v1

    invoke-static {v0, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "x"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "("

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v4, 0x78

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, ",visibile=false"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    :goto_2
    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, ",alpha="

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    invoke-virtual {p0}, Landroid/view/View;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, ",layout=true"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    invoke-virtual {p0}, Landroid/view/View;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, ",focusable"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    invoke-virtual {p0}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, ",focused"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    invoke-virtual {p0}, Landroid/view/View;->isOpaque()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, ",opaque"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xf

    if-lt v0, v4, :cond_a

    invoke-virtual {p0}, Landroid/view/View;->hasOnClickListeners()Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, ",onclick"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_b

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_b

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ",nchildren="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    instance-of v0, p0, Landroid/widget/TextView;

    if-eqz v0, :cond_d

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, ",size="

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextSize()F

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, p0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-le v4, v6, :cond_c

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    :cond_c
    if-nez v0, :cond_10

    const-string v0, ",text=null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    :goto_3
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v0, v3, [Ljava/lang/Object;

    aput-object v2, v0, v1

    .line 404
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_14

    move-object v0, p0

    .line 405
    check-cast v0, Landroid/view/ViewGroup;

    .line 406
    if-nez p1, :cond_e

    instance-of v2, p0, Lflipboard/gui/flipping/FlippingContainer;

    if-eqz v2, :cond_11

    check-cast p0, Lflipboard/gui/flipping/FlippingContainer;

    iget-boolean v2, p0, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    :goto_4
    if-eqz v2, :cond_13

    .line 407
    :cond_e
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    :goto_5
    if-ge v1, v2, :cond_14

    .line 408
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    add-int/lit8 v4, p2, 0x2

    invoke-static {v3, p1, v4}, Lflipboard/util/AndroidUtil;->b(Landroid/view/View;ZI)V

    .line 407
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 402
    :cond_f
    invoke-virtual {p0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, ",shown=false"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_10
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ",text=\'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\'"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 406
    :cond_11
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_12

    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v2

    const/4 v4, 0x0

    cmpl-float v2, v2, v4

    if-lez v2, :cond_12

    move v2, v3

    goto :goto_4

    :cond_12
    move v2, v1

    goto :goto_4

    .line 410
    :cond_13
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-lez v2, :cond_14

    .line 411
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    add-int/lit8 v4, p2, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v1

    const-string v1, ""

    aput-object v1, v2, v3

    const/4 v1, 0x2

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v1

    .line 414
    :cond_14
    return-void

    :catch_0
    move-exception v5

    goto/16 :goto_1
.end method

.method public static b(Landroid/view/View;)Z
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 546
    new-array v0, v7, [I

    .line 547
    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 549
    aget v4, v0, v2

    .line 550
    aget v5, v0, v1

    .line 552
    sget-object v0, Lflipboard/util/AndroidUtil;->c:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 553
    if-ltz v4, :cond_0

    if-ge v4, v0, :cond_0

    move v0, v1

    .line 555
    :goto_0
    sget-object v3, Lflipboard/util/AndroidUtil;->c:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 556
    if-ltz v5, :cond_1

    if-ge v5, v3, :cond_1

    move v3, v1

    .line 558
    :goto_1
    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    move v0, v1

    .line 559
    :goto_2
    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v3, v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v7

    const/4 v1, 0x3

    aput-object p0, v3, v1

    .line 560
    return v0

    :cond_0
    move v0, v2

    .line 553
    goto :goto_0

    :cond_1
    move v3, v2

    .line 556
    goto :goto_1

    :cond_2
    move v0, v2

    .line 558
    goto :goto_2
.end method

.method public static c()I
    .locals 2

    .prologue
    .line 519
    sget-object v0, Lflipboard/util/AndroidUtil;->c:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    const v1, 0x3f59999a    # 0.85f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static c(Lflipboard/objs/ConfigService;)I
    .locals 2

    .prologue
    .line 601
    const-string v0, "twitter"

    iget-object v1, p0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602
    const v0, 0x7f020014

    .line 604
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f020005

    goto :goto_0
.end method

.method public static c(Landroid/app/Activity;)Landroid/graphics/Point;
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 1556
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 1557
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 1559
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_0

    .line 1560
    invoke-virtual {v1, v2}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 1571
    :goto_0
    return-object v2

    .line 1563
    :cond_0
    :try_start_0
    const-class v0, Landroid/view/Display;

    const-string v3, "getRawWidth"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 1564
    const-class v3, Landroid/view/Display;

    const-string v4, "getRawHeight"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 1565
    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v2, Landroid/graphics/Point;->x:I

    .line 1566
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v2, Landroid/graphics/Point;->y:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1567
    :catch_0
    move-exception v0

    .line 1568
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 968
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/DetailActivityStayOnRotation;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 969
    const-string v1, "flipmag_load_article"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 970
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 971
    return-void
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 1393
    const-string v0, "power"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/view/View;)[I
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 737
    new-array v1, v8, [I

    .line 739
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    aput v0, v1, v7

    .line 740
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v0

    aput v0, v1, v6

    .line 742
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 743
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 744
    :goto_0
    instance-of v3, v0, Landroid/view/View;

    if-eqz v3, :cond_0

    .line 745
    check-cast v0, Landroid/view/View;

    .line 746
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 748
    aget v3, v1, v7

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v5

    sub-int/2addr v4, v5

    add-int/2addr v3, v4

    aput v3, v1, v7

    .line 749
    aget v3, v1, v6

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v5

    sub-int/2addr v4, v5

    add-int/2addr v3, v4

    aput v3, v1, v6

    .line 750
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    .line 754
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v8, :cond_1

    .line 755
    aget v3, v1, v6

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, v3, v0

    aput v0, v1, v6

    .line 757
    :cond_1
    return-object v1
.end method

.method public static d()I
    .locals 1

    .prologue
    .line 527
    sget-object v0, Lflipboard/util/AndroidUtil;->c:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    return v0
.end method

.method public static d(Lflipboard/objs/ConfigService;)I
    .locals 1

    .prologue
    .line 610
    invoke-static {p0}, Lflipboard/util/AndroidUtil;->e(Lflipboard/objs/ConfigService;)I

    move-result v0

    return v0
.end method

.method public static d(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x11

    .line 1925
    sget-object v0, Lflipboard/util/AndroidUtil;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 1926
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_4

    const/4 v0, 0x1

    .line 1927
    :goto_0
    if-eqz v0, :cond_1

    .line 1929
    const/4 v0, 0x0

    :try_start_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v2, :cond_0

    invoke-static {p0}, Landroid/webkit/WebSettings;->getDefaultUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    sput-object v0, Lflipboard/util/AndroidUtil;->d:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1936
    :cond_1
    :goto_1
    sget-object v0, Lflipboard/util/AndroidUtil;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 1937
    invoke-static {p0}, Lflipboard/util/AndroidUtil;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lflipboard/util/AndroidUtil;->d:Ljava/lang/String;

    .line 1941
    :cond_2
    sget-object v0, Lflipboard/util/AndroidUtil;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 1942
    const-string v0, "unwanted.defaultUserAgent_null"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    .line 1946
    :cond_3
    sget-object v0, Lflipboard/util/AndroidUtil;->d:Ljava/lang/String;

    return-object v0

    .line 1926
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 1930
    :catch_0
    move-exception v0

    .line 1931
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static d(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 1901
    if-eqz p0, :cond_0

    .line 1902
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1906
    invoke-virtual {p0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 1907
    if-eqz v1, :cond_0

    .line 1908
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1911
    :cond_0
    return-void
.end method

.method public static d(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1830
    invoke-virtual {p0, p1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    .line 1831
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 1250
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1251
    instance-of v1, v0, Lflipboard/activities/FlipboardActivity;

    if-eqz v1, :cond_0

    .line 1252
    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-boolean v0, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    .line 1254
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static e()I
    .locals 1

    .prologue
    .line 532
    sget-object v0, Lflipboard/util/AndroidUtil;->c:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    return v0
.end method

.method public static e(Lflipboard/objs/ConfigService;)I
    .locals 2

    .prologue
    .line 615
    iget-object v0, p0, Lflipboard/objs/ConfigService;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/ConfigService;->i:Ljava/lang/String;

    .line 617
    :goto_0
    const-string v1, "heart"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 618
    const v0, 0x7f020207

    .line 625
    :goto_1
    return v0

    .line 615
    :cond_0
    const-string v0, "thumbsUp"

    goto :goto_0

    .line 619
    :cond_1
    const-string v1, "thumbsUp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 620
    const v0, 0x7f020208

    goto :goto_1

    .line 621
    :cond_2
    const-string v1, "plusOne"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 623
    const v0, 0x7f020209

    goto :goto_1

    .line 625
    :cond_3
    const v0, 0x7f02020d

    goto :goto_1
.end method

.method private static e(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x7
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1976
    .line 1980
    :try_start_0
    const-string v0, "android.webkit.WebSettingsClassic"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 1983
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/content/Context;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "android.webkit.WebViewClassic"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    .line 1984
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1987
    :try_start_1
    const-string v3, "getUserAgentString"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 1990
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1992
    const/4 v1, 0x0

    :try_start_2
    invoke-virtual {v2, v1}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 1998
    :goto_0
    return-object v0

    .line 1992
    :catchall_0
    move-exception v0

    const/4 v3, 0x0

    :try_start_3
    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public static f()I
    .locals 1

    .prologue
    .line 725
    const v0, 0x7f020206

    return v0
.end method

.method public static f(Lflipboard/objs/ConfigService;)I
    .locals 2

    .prologue
    .line 706
    iget-object v0, p0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    const-string v1, "twitter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 707
    const v0, 0x7f02020b

    .line 709
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f020205

    goto :goto_0
.end method

.method public static g(Lflipboard/objs/ConfigService;)I
    .locals 2

    .prologue
    .line 715
    iget-object v0, p0, Lflipboard/objs/ConfigService;->ah:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/ConfigService;->ah:Ljava/lang/String;

    .line 716
    :goto_0
    const-string v1, "reblog"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 717
    const v0, 0x7f02020a

    .line 719
    :goto_1
    return v0

    .line 715
    :cond_0
    const-string v0, "retweet"

    goto :goto_0

    .line 719
    :cond_1
    const v0, 0x7f02020c

    goto :goto_1
.end method

.method public static g()Z
    .locals 3

    .prologue
    .line 1493
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v1, 0x7f0d008d

    invoke-virtual {v0, v1}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1494
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "en"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "All text and attachments will be removed."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h()Ljava/util/concurrent/Executor;
    .locals 9

    .prologue
    .line 1498
    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v0, 0x2710

    invoke-direct {v7, v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    .line 1503
    new-instance v8, Lflipboard/util/AndroidUtil$4;

    invoke-direct {v8}, Lflipboard/util/AndroidUtil$4;-><init>()V

    .line 1510
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/16 v2, 0x18

    const/16 v3, 0x20

    const-wide/16 v4, 0x1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    return-object v1
.end method

.method public static i()Z
    .locals 2

    .prologue
    .line 1690
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j()Z
    .locals 2

    .prologue
    .line 1699
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k()Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1763
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v0

    .line 1764
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 1765
    sget-object v4, Lflipboard/util/AndroidUtil;->f:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 1766
    iget-object v7, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1767
    const/4 v0, 0x1

    .line 1771
    :goto_1
    return v0

    .line 1765
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1771
    goto :goto_1
.end method

.method public static l()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1798
    const/4 v0, 0x0

    .line 1800
    :try_start_0
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1802
    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 1803
    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 1805
    sget-object v5, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v5, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 1806
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1807
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1814
    :cond_0
    :goto_1
    return-object v0

    .line 1802
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1812
    :catch_0
    move-exception v1

    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    goto :goto_1
.end method

.method public static m()Z
    .locals 3

    .prologue
    .line 1870
    const/4 v0, 0x1

    .line 1872
    :try_start_0
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1876
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method
