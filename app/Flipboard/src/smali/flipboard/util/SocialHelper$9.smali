.class final Lflipboard/util/SocialHelper$9;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SocialHelper.java"


# instance fields
.field final synthetic a:Lflipboard/activities/FlipboardActivity;

.field final synthetic b:Lflipboard/objs/ConfigService;

.field final synthetic c:Lflipboard/service/Section;

.field final synthetic d:Lflipboard/objs/FeedItem;


# direct methods
.method constructor <init>(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/ConfigService;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 657
    iput-object p1, p0, Lflipboard/util/SocialHelper$9;->a:Lflipboard/activities/FlipboardActivity;

    iput-object p2, p0, Lflipboard/util/SocialHelper$9;->b:Lflipboard/objs/ConfigService;

    iput-object p3, p0, Lflipboard/util/SocialHelper$9;->c:Lflipboard/service/Section;

    iput-object p4, p0, Lflipboard/util/SocialHelper$9;->d:Lflipboard/objs/FeedItem;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 4

    .prologue
    .line 661
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lflipboard/util/SocialHelper$9;->a:Lflipboard/activities/FlipboardActivity;

    const-class v2, Lflipboard/activities/ServiceLoginActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 662
    const-string v1, "service"

    iget-object v2, p0, Lflipboard/util/SocialHelper$9;->b:Lflipboard/objs/ConfigService;

    iget-object v2, v2, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 663
    const-string v1, "viewSectionAfterSuccess"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 664
    const-string v1, "extra_content_discovery_from_source"

    const-string v2, "usageSocialLoginOriginSocialAction"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 665
    iget-object v1, p0, Lflipboard/util/SocialHelper$9;->a:Lflipboard/activities/FlipboardActivity;

    const/16 v2, 0xc9

    new-instance v3, Lflipboard/util/SocialHelper$9$1;

    invoke-direct {v3, p0}, Lflipboard/util/SocialHelper$9$1;-><init>(Lflipboard/util/SocialHelper$9;)V

    invoke-virtual {v1, v0, v2, v3}, Lflipboard/activities/FlipboardActivity;->a(Landroid/content/Intent;ILflipboard/activities/FlipboardActivity$ActivityResultListener;)V

    .line 675
    return-void
.end method
