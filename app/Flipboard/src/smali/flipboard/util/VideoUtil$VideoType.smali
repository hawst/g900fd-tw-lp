.class public final enum Lflipboard/util/VideoUtil$VideoType;
.super Ljava/lang/Enum;
.source "VideoUtil.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/util/VideoUtil$VideoType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/util/VideoUtil$VideoType;

.field public static final enum b:Lflipboard/util/VideoUtil$VideoType;

.field public static final enum c:Lflipboard/util/VideoUtil$VideoType;

.field public static final enum d:Lflipboard/util/VideoUtil$VideoType;

.field public static final enum e:Lflipboard/util/VideoUtil$VideoType;

.field public static final enum f:Lflipboard/util/VideoUtil$VideoType;

.field private static final synthetic g:[Lflipboard/util/VideoUtil$VideoType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 29
    new-instance v0, Lflipboard/util/VideoUtil$VideoType;

    const-string v1, "EMBEDDED_WEBVIEW"

    invoke-direct {v0, v1, v3}, Lflipboard/util/VideoUtil$VideoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/util/VideoUtil$VideoType;->a:Lflipboard/util/VideoUtil$VideoType;

    new-instance v0, Lflipboard/util/VideoUtil$VideoType;

    const-string v1, "NATIVE"

    invoke-direct {v0, v1, v4}, Lflipboard/util/VideoUtil$VideoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/util/VideoUtil$VideoType;->b:Lflipboard/util/VideoUtil$VideoType;

    new-instance v0, Lflipboard/util/VideoUtil$VideoType;

    const-string v1, "YOUTUBE_API"

    invoke-direct {v0, v1, v5}, Lflipboard/util/VideoUtil$VideoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/util/VideoUtil$VideoType;->c:Lflipboard/util/VideoUtil$VideoType;

    new-instance v0, Lflipboard/util/VideoUtil$VideoType;

    const-string v1, "YOUTUBE_APP"

    invoke-direct {v0, v1, v6}, Lflipboard/util/VideoUtil$VideoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/util/VideoUtil$VideoType;->d:Lflipboard/util/VideoUtil$VideoType;

    new-instance v0, Lflipboard/util/VideoUtil$VideoType;

    const-string v1, "YOUTUBE_WEBVIEW"

    invoke-direct {v0, v1, v7}, Lflipboard/util/VideoUtil$VideoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/util/VideoUtil$VideoType;->e:Lflipboard/util/VideoUtil$VideoType;

    new-instance v0, Lflipboard/util/VideoUtil$VideoType;

    const-string v1, "WEBVIEW"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/util/VideoUtil$VideoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/util/VideoUtil$VideoType;->f:Lflipboard/util/VideoUtil$VideoType;

    .line 28
    const/4 v0, 0x6

    new-array v0, v0, [Lflipboard/util/VideoUtil$VideoType;

    sget-object v1, Lflipboard/util/VideoUtil$VideoType;->a:Lflipboard/util/VideoUtil$VideoType;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/util/VideoUtil$VideoType;->b:Lflipboard/util/VideoUtil$VideoType;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/util/VideoUtil$VideoType;->c:Lflipboard/util/VideoUtil$VideoType;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/util/VideoUtil$VideoType;->d:Lflipboard/util/VideoUtil$VideoType;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/util/VideoUtil$VideoType;->e:Lflipboard/util/VideoUtil$VideoType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/util/VideoUtil$VideoType;->f:Lflipboard/util/VideoUtil$VideoType;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/util/VideoUtil$VideoType;->g:[Lflipboard/util/VideoUtil$VideoType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/util/VideoUtil$VideoType;
    .locals 1

    .prologue
    .line 28
    const-class v0, Lflipboard/util/VideoUtil$VideoType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/util/VideoUtil$VideoType;

    return-object v0
.end method

.method public static values()[Lflipboard/util/VideoUtil$VideoType;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lflipboard/util/VideoUtil$VideoType;->g:[Lflipboard/util/VideoUtil$VideoType;

    invoke-virtual {v0}, [Lflipboard/util/VideoUtil$VideoType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/util/VideoUtil$VideoType;

    return-object v0
.end method
