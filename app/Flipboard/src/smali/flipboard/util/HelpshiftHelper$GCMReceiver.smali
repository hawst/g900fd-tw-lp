.class public Lflipboard/util/HelpshiftHelper$GCMReceiver;
.super Landroid/content/BroadcastReceiver;
.source "HelpshiftHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 231
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 235
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 236
    const-string v1, "flipboard.app.helpshift_gcm_on_registered"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 237
    invoke-static {p1}, Lcom/google/android/gcm/GCMRegistrar;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 238
    invoke-static {}, Lflipboard/util/HelpshiftHelper;->a()V

    .line 239
    invoke-static {p1, v0}, Lcom/helpshift/Helpshift;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    const-string v1, "flipboard.app.helpshift_gcm_on_message_received"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    invoke-static {}, Lflipboard/util/HelpshiftHelper;->a()V

    .line 242
    invoke-static {p1, p2}, Lcom/helpshift/Helpshift;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method
