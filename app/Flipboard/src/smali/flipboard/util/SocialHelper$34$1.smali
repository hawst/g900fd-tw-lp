.class Lflipboard/util/SocialHelper$34$1;
.super Ljava/lang/Object;
.source "SocialHelper.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/util/SocialHelper$34;


# direct methods
.method constructor <init>(Lflipboard/util/SocialHelper$34;)V
    .locals 0

    .prologue
    .line 1415
    iput-object p1, p0, Lflipboard/util/SocialHelper$34$1;->a:Lflipboard/util/SocialHelper$34;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 3

    .prologue
    .line 1418
    sget-object v0, Lflipboard/util/SocialHelper;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/util/SocialHelper$34$1;->a:Lflipboard/util/SocialHelper$34;

    iget-object v2, v2, Lflipboard/util/SocialHelper$34;->a:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lflipboard/util/SocialHelper$34$1;->a:Lflipboard/util/SocialHelper$34;

    iget-object v2, v2, Lflipboard/util/SocialHelper$34;->a:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 1419
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1423
    sget-object v1, Lflipboard/util/SocialHelper;->a:Lflipboard/util/Log;

    const-string v2, "failed to follow %s on %s: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lflipboard/util/SocialHelper$34$1;->a:Lflipboard/util/SocialHelper$34;

    iget-object v4, v4, Lflipboard/util/SocialHelper$34;->a:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v4, 0x1

    iget-object v5, p0, Lflipboard/util/SocialHelper$34$1;->a:Lflipboard/util/SocialHelper$34;

    iget-object v5, v5, Lflipboard/util/SocialHelper$34;->a:Lflipboard/objs/FeedItem;

    iget-object v5, v5, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1426
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "already followed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 1427
    :cond_0
    sget-object v1, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/util/SocialHelper$34$1$1;

    invoke-direct {v2, p0, v0}, Lflipboard/util/SocialHelper$34$1$1;-><init>(Lflipboard/util/SocialHelper$34$1;Z)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 1440
    return-void
.end method
