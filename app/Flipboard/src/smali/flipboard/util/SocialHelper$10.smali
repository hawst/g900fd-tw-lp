.class final Lflipboard/util/SocialHelper$10;
.super Ljava/lang/Object;
.source "SocialHelper.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/objs/FeedItem;

.field final synthetic b:Lflipboard/objs/ConfigService;

.field final synthetic c:Lflipboard/activities/FlipboardActivity;


# direct methods
.method constructor <init>(Lflipboard/objs/FeedItem;Lflipboard/objs/ConfigService;Lflipboard/activities/FlipboardActivity;)V
    .locals 0

    .prologue
    .line 691
    iput-object p1, p0, Lflipboard/util/SocialHelper$10;->a:Lflipboard/objs/FeedItem;

    iput-object p2, p0, Lflipboard/util/SocialHelper$10;->b:Lflipboard/objs/ConfigService;

    iput-object p3, p0, Lflipboard/util/SocialHelper$10;->c:Lflipboard/activities/FlipboardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 3

    .prologue
    .line 694
    sget-object v0, Lflipboard/util/SocialHelper;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/util/SocialHelper$10;->a:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 695
    iget-object v0, p0, Lflipboard/util/SocialHelper$10;->b:Lflipboard/objs/ConfigService;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/objs/ConfigService;->as:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 696
    if-eqz v0, :cond_0

    .line 697
    sget-object v1, Lflipboard/util/SocialHelper;->b:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/util/SocialHelper$10$1;

    invoke-direct {v2, p0, v0}, Lflipboard/util/SocialHelper$10$1;-><init>(Lflipboard/util/SocialHelper$10;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 707
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 711
    sget-object v0, Lflipboard/util/SocialHelper;->a:Lflipboard/util/Log;

    const-string v1, "failed to share %s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/util/SocialHelper$10;->a:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 712
    iget-object v0, p0, Lflipboard/util/SocialHelper$10;->c:Lflipboard/activities/FlipboardActivity;

    iget-object v1, p0, Lflipboard/util/SocialHelper$10;->c:Lflipboard/activities/FlipboardActivity;

    iget-object v2, p0, Lflipboard/util/SocialHelper$10;->b:Lflipboard/objs/ConfigService;

    invoke-static {v1, v2}, Lflipboard/gui/SocialFormatter;->e(Landroid/content/Context;Lflipboard/objs/ConfigService;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 713
    return-void
.end method
