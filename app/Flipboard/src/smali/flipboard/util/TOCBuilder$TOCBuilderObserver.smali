.class public Lflipboard/util/TOCBuilder$TOCBuilderObserver;
.super Ljava/lang/Object;
.source "TOCBuilder.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/Section;",
        "Lflipboard/service/Section$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field b:Lflipboard/activities/FlipboardActivity;

.field c:I

.field d:Ljava/lang/Exception;

.field e:Z

.field public f:Z


# direct methods
.method public constructor <init>(Lflipboard/activities/FlipboardActivity;)V
    .locals 0

    .prologue
    .line 361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 362
    iput-object p1, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->b:Lflipboard/activities/FlipboardActivity;

    .line 363
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 520
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 353
    check-cast p1, Lflipboard/service/Section;

    check-cast p2, Lflipboard/service/Section$Message;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v0, Lflipboard/util/TOCBuilder$7;->a:[I

    invoke-virtual {p2}, Lflipboard/service/Section$Message;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->d:Ljava/lang/Exception;

    if-nez v0, :cond_0

    check-cast p3, Ljava/lang/Exception;

    iput-object p3, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->d:Ljava/lang/Exception;

    goto :goto_0

    :pswitch_1
    check-cast p3, Ljava/lang/Boolean;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, p0}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    iget-boolean v0, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->e:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lflipboard/service/Section;->l()Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->e:Z

    sget-object v0, Lflipboard/util/TOCBuilder;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    iget-boolean v3, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->f:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v0, v2

    iget-boolean v0, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->d:Ljava/lang/Exception;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->a()V

    :cond_1
    iget-object v0, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->d:Ljava/lang/Exception;

    instance-of v0, v0, Lflipboard/service/Flap$GovernorException;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->d:Ljava/lang/Exception;

    check-cast v0, Lflipboard/service/Flap$GovernorException;

    iget-object v0, v0, Lflipboard/service/Flap$GovernorException;->a:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    iget-object v2, v0, Lflipboard/objs/FeedItem;->bg:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/objs/FeedItem;->bg:Ljava/lang/String;

    const-string v3, "alert"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/objs/FeedItem;->o:Ljava/lang/String;

    if-eqz v2, :cond_0

    new-instance v2, Lflipboard/util/TOCBuilder$TOCBuilderObserver$2;

    invoke-direct {v2, p0, v0}, Lflipboard/util/TOCBuilder$TOCBuilderObserver$2;-><init>(Lflipboard/util/TOCBuilder$TOCBuilderObserver;Lflipboard/objs/FeedItem;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->d:Ljava/lang/Exception;

    instance-of v0, v0, Lflipboard/service/Flap$ServiceDownForMaintenanceException;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->e:Z

    if-eqz v0, :cond_3

    new-instance v0, Lflipboard/util/TOCBuilder$TOCBuilderObserver$3;

    invoke-direct {v0, p0, v1}, Lflipboard/util/TOCBuilder$TOCBuilderObserver$3;-><init>(Lflipboard/util/TOCBuilder$TOCBuilderObserver;Lflipboard/service/FlipboardManager;)V

    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_3
    new-instance v0, Lflipboard/util/TOCBuilder$TOCBuilderObserver$4;

    invoke-direct {v0, p0}, Lflipboard/util/TOCBuilder$TOCBuilderObserver$4;-><init>(Lflipboard/util/TOCBuilder$TOCBuilderObserver;)V

    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    invoke-virtual {p0}, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->a()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()V
    .locals 0

    .prologue
    .line 522
    return-void
.end method

.method public cancel()V
    .locals 6

    .prologue
    .line 367
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 368
    iget-boolean v1, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->f:Z

    if-nez v1, :cond_0

    .line 369
    const/4 v1, 0x1

    iput-boolean v1, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->f:Z

    .line 370
    const-string v1, "BuildTOCCancelled"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {}, Lflipboard/util/TOCBuilder;->a()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v1, v2, v3}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;J)V

    .line 371
    iget-object v1, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->b:Lflipboard/activities/FlipboardActivity;

    sget-object v2, Lflipboard/util/TOCBuilder;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 377
    const-string v1, "TOCBuilder:cancel"

    new-instance v2, Lflipboard/util/TOCBuilder$TOCBuilderObserver$1;

    invoke-direct {v2, p0, v0}, Lflipboard/util/TOCBuilder$TOCBuilderObserver$1;-><init>(Lflipboard/util/TOCBuilder$TOCBuilderObserver;Lflipboard/service/FlipboardManager;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 393
    :cond_0
    return-void
.end method
