.class final Lflipboard/util/ShareHelper$1;
.super Ljava/lang/Object;
.source "ShareHelper.java"

# interfaces
.implements Lflipboard/service/Flap$CancellableJSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/objs/UsageEventV2;

.field final synthetic b:Lflipboard/objs/Magazine;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lflipboard/objs/UsageEventV2;Lflipboard/objs/Magazine;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lflipboard/util/ShareHelper$1;->a:Lflipboard/objs/UsageEventV2;

    iput-object p2, p0, Lflipboard/util/ShareHelper$1;->b:Lflipboard/objs/Magazine;

    iput-object p3, p0, Lflipboard/util/ShareHelper$1;->c:Ljava/lang/String;

    iput-object p4, p0, Lflipboard/util/ShareHelper$1;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 14

    .prologue
    .line 123
    sget-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 124
    iget-object v0, p0, Lflipboard/util/ShareHelper$1;->a:Lflipboard/objs/UsageEventV2;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lflipboard/util/ShareHelper$1;->a:Lflipboard/objs/UsageEventV2;

    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 127
    :cond_0
    iget-object v0, p0, Lflipboard/util/ShareHelper$1;->b:Lflipboard/objs/Magazine;

    if-nez v0, :cond_2

    .line 284
    :cond_1
    :goto_0
    return-void

    .line 130
    :cond_2
    iget-object v0, p0, Lflipboard/util/ShareHelper$1;->b:Lflipboard/objs/Magazine;

    iget-object v11, v0, Lflipboard/objs/Magazine;->c:Ljava/lang/String;

    .line 132
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v4, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    .line 133
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget v5, v0, Lflipboard/model/ConfigSetting;->FirstFlipsPerDayLimit:I

    .line 135
    const-string v0, "also.flip.seen.count"

    const/4 v1, 0x0

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 136
    const-string v0, "first.also.flip.timestamp"

    const-wide/16 v6, 0x0

    invoke-interface {v4, v0, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 137
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v0, v6, v0

    .line 138
    const-wide/32 v6, 0x5265c00

    cmp-long v3, v0, v6

    if-lez v3, :cond_3

    .line 139
    const-wide/16 v0, 0x0

    .line 140
    const/4 v2, 0x0

    .line 143
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "targetResponses."

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ".firstFlip"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 144
    invoke-virtual {p1, v3}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v6

    .line 145
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "targetResponses."

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ".id"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 146
    invoke-virtual {p1, v3}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 147
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v8, "targetResponses."

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, ".canPromote"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 148
    invoke-virtual {p1, v3}, Lflipboard/json/FLObject;->c(Ljava/lang/String;)Z

    move-result v8

    .line 150
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v3}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v9

    .line 151
    const/16 v3, 0x23

    iget v10, v9, Lflipboard/model/ConfigSetting;->AlsoFlippedUserSampleSize:F

    invoke-static {v3, v10}, Lflipboard/abtest/testcase/ABTestCases;->a(IF)I

    move-result v10

    .line 152
    const/4 v3, 0x1

    if-eq v10, v3, :cond_9

    const/4 v3, 0x1

    .line 154
    :goto_1
    if-nez v6, :cond_a

    if-eqz v7, :cond_a

    if-eqz v3, :cond_a

    iget-object v3, p0, Lflipboard/util/ShareHelper$1;->b:Lflipboard/objs/Magazine;

    invoke-virtual {v3}, Lflipboard/objs/Magazine;->a()Z

    move-result v3

    if-eqz v3, :cond_a

    const/4 v3, 0x1

    .line 156
    :goto_2
    if-eqz v3, :cond_b

    if-ge v2, v5, :cond_b

    const-wide/32 v12, 0x5265c00

    cmp-long v0, v0, v12

    if-gez v0, :cond_b

    if-nez v8, :cond_b

    const/4 v0, 0x1

    .line 157
    :goto_3
    if-eqz v0, :cond_c

    const/4 v0, 0x2

    if-ne v10, v0, :cond_c

    const/4 v0, 0x1

    move v10, v0

    .line 159
    :goto_4
    if-eqz v10, :cond_d

    .line 160
    new-instance v0, Lflipboard/util/ShareHelper$1$1;

    invoke-direct {v0, p0, v2}, Lflipboard/util/ShareHelper$1$1;-><init>(Lflipboard/util/ShareHelper$1;I)V

    .line 198
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v3, Lflipboard/service/Flap$AlsoFlippedRequest;

    invoke-direct {v3, v1, v2}, Lflipboard/service/Flap$AlsoFlippedRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v3, v7, v0}, Lflipboard/service/Flap$AlsoFlippedRequest;->a(Ljava/lang/String;Lflipboard/service/Flap$AlsoFlippedObserver;)Lflipboard/service/Flap$AlsoFlippedRequest;

    .line 204
    :cond_4
    :goto_5
    const-string v0, "first.flip.seen"

    const/4 v1, 0x0

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 205
    const-string v0, "first.flip.timestamp"

    const-wide/16 v12, 0x0

    invoke-interface {v4, v0, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 206
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long v0, v12, v0

    .line 207
    const-wide/32 v12, 0x5265c00

    cmp-long v3, v0, v12

    if-lez v3, :cond_5

    .line 208
    const-wide/16 v0, 0x0

    .line 209
    const/4 v2, 0x0

    .line 212
    :cond_5
    const/16 v3, 0x24

    iget v5, v9, Lflipboard/model/ConfigSetting;->AlsoFlippedUserSampleSize:F

    invoke-static {v3, v5}, Lflipboard/abtest/testcase/ABTestCases;->a(IF)I

    move-result v5

    .line 213
    const/4 v3, 0x1

    if-eq v5, v3, :cond_e

    const/4 v3, 0x1

    .line 214
    :goto_6
    if-eqz v6, :cond_f

    if-nez v7, :cond_f

    if-eqz v3, :cond_f

    const/4 v3, 0x1

    .line 215
    :goto_7
    if-eqz v3, :cond_10

    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v6}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v6

    iget v6, v6, Lflipboard/model/ConfigSetting;->FirstFlipsPerDayLimit:I

    if-ge v2, v6, :cond_10

    const-wide/32 v6, 0x5265c00

    cmp-long v0, v0, v6

    if-gez v0, :cond_10

    if-nez v8, :cond_10

    const/4 v0, 0x1

    .line 216
    :goto_8
    if-eqz v0, :cond_11

    const/4 v0, 0x2

    if-ne v5, v0, :cond_11

    const/4 v0, 0x1

    move v9, v0

    .line 219
    :goto_9
    if-eqz v9, :cond_12

    .line 220
    if-nez v2, :cond_6

    .line 221
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "first.flip.timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-interface {v0, v1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 223
    :cond_6
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "first.flip.seen"

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 224
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 225
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/util/ShareHelper$1$2;

    invoke-direct {v1, p0}, Lflipboard/util/ShareHelper$1$2;-><init>(Lflipboard/util/ShareHelper$1;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 242
    :cond_7
    :goto_a
    if-eqz v8, :cond_13

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->y()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_13

    iget-object v0, p0, Lflipboard/util/ShareHelper$1;->b:Lflipboard/objs/Magazine;

    invoke-virtual {v0}, Lflipboard/objs/Magazine;->a()Z

    move-result v0

    if-eqz v0, :cond_13

    const/4 v0, 0x1

    move v8, v0

    .line 243
    :goto_b
    if-eqz v8, :cond_8

    .line 244
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "targetResponses."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".numberOfItems"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lflipboard/json/FLObject;->d(Ljava/lang/String;)I

    move-result v12

    .line 245
    sget-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 246
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/util/ShareHelper$1;->b:Lflipboard/objs/Magazine;

    iget-object v1, v1, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/util/ShareHelper$1;->b:Lflipboard/objs/Magazine;

    iget-object v2, v2, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/util/ShareHelper$1;->b:Lflipboard/objs/Magazine;

    iget-object v3, v3, Lflipboard/objs/Magazine;->s:Ljava/lang/String;

    const-string v4, ""

    const-string v5, ""

    const/4 v6, 0x0

    new-instance v7, Lflipboard/util/ShareHelper$1$3;

    invoke-direct {v7, p0, v11, v12}, Lflipboard/util/ShareHelper$1$3;-><init>(Lflipboard/util/ShareHelper$1;Ljava/lang/String;I)V

    invoke-virtual/range {v0 .. v7}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLflipboard/service/Flap$JSONResultObserver;)V

    .line 272
    :cond_8
    if-nez v8, :cond_1

    if-nez v10, :cond_1

    if-nez v9, :cond_1

    .line 274
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/util/ShareHelper$1$4;

    invoke-direct {v1, p0}, Lflipboard/util/ShareHelper$1$4;-><init>(Lflipboard/util/ShareHelper$1;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 152
    :cond_9
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 154
    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 156
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 157
    :cond_c
    const/4 v0, 0x0

    move v10, v0

    goto/16 :goto_4

    .line 199
    :cond_d
    if-eqz v3, :cond_4

    .line 202
    sget-object v0, Lflipboard/objs/UsageEventV2$EventDataType;->b:Lflipboard/objs/UsageEventV2$EventDataType;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;->b:Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    iget-object v2, p0, Lflipboard/util/ShareHelper$1;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lflipboard/util/ShareHelper;->a(Lflipboard/objs/UsageEventV2$EventDataType;Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 213
    :cond_e
    const/4 v3, 0x0

    goto/16 :goto_6

    .line 214
    :cond_f
    const/4 v3, 0x0

    goto/16 :goto_7

    .line 215
    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_8

    .line 216
    :cond_11
    const/4 v0, 0x0

    move v9, v0

    goto/16 :goto_9

    .line 235
    :cond_12
    if-eqz v3, :cond_7

    .line 238
    sget-object v0, Lflipboard/objs/UsageEventV2$EventDataType;->a:Lflipboard/objs/UsageEventV2$EventDataType;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;->b:Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    iget-object v2, p0, Lflipboard/util/ShareHelper$1;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lflipboard/util/ShareHelper;->a(Lflipboard/objs/UsageEventV2$EventDataType;Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 242
    :cond_13
    const/4 v0, 0x0

    move v8, v0

    goto/16 :goto_b
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 288
    sget-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 290
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    .line 291
    if-eqz v0, :cond_0

    .line 293
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/util/ShareHelper$1$5;

    invoke-direct {v2, p0, v0}, Lflipboard/util/ShareHelper$1$5;-><init>(Lflipboard/util/ShareHelper$1;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 311
    :cond_0
    return-void
.end method

.method public final cancel()V
    .locals 1

    .prologue
    .line 315
    sget-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    .line 316
    return-void
.end method
