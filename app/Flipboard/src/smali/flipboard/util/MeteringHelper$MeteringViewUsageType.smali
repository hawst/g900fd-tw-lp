.class public final enum Lflipboard/util/MeteringHelper$MeteringViewUsageType;
.super Ljava/lang/Enum;
.source "MeteringHelper.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/util/MeteringHelper$MeteringViewUsageType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/util/MeteringHelper$MeteringViewUsageType;

.field public static final enum b:Lflipboard/util/MeteringHelper$MeteringViewUsageType;

.field private static final synthetic d:[Lflipboard/util/MeteringHelper$MeteringViewUsageType;


# instance fields
.field public final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 251
    new-instance v0, Lflipboard/util/MeteringHelper$MeteringViewUsageType;

    const-string v1, "roadblockPage"

    const-string v2, "usageSocialLoginOriginRoadblock"

    invoke-direct {v0, v1, v3, v2}, Lflipboard/util/MeteringHelper$MeteringViewUsageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lflipboard/util/MeteringHelper$MeteringViewUsageType;->a:Lflipboard/util/MeteringHelper$MeteringViewUsageType;

    new-instance v0, Lflipboard/util/MeteringHelper$MeteringViewUsageType;

    const-string v1, "interstitialPage"

    const-string v2, "usageSocialLoginOriginInterstitial"

    invoke-direct {v0, v1, v4, v2}, Lflipboard/util/MeteringHelper$MeteringViewUsageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lflipboard/util/MeteringHelper$MeteringViewUsageType;->b:Lflipboard/util/MeteringHelper$MeteringViewUsageType;

    .line 249
    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/util/MeteringHelper$MeteringViewUsageType;

    sget-object v1, Lflipboard/util/MeteringHelper$MeteringViewUsageType;->a:Lflipboard/util/MeteringHelper$MeteringViewUsageType;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/util/MeteringHelper$MeteringViewUsageType;->b:Lflipboard/util/MeteringHelper$MeteringViewUsageType;

    aput-object v1, v0, v4

    sput-object v0, Lflipboard/util/MeteringHelper$MeteringViewUsageType;->d:[Lflipboard/util/MeteringHelper$MeteringViewUsageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 255
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 256
    iput-object p3, p0, Lflipboard/util/MeteringHelper$MeteringViewUsageType;->c:Ljava/lang/String;

    .line 257
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/util/MeteringHelper$MeteringViewUsageType;
    .locals 1

    .prologue
    .line 249
    const-class v0, Lflipboard/util/MeteringHelper$MeteringViewUsageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/util/MeteringHelper$MeteringViewUsageType;

    return-object v0
.end method

.method public static values()[Lflipboard/util/MeteringHelper$MeteringViewUsageType;
    .locals 1

    .prologue
    .line 249
    sget-object v0, Lflipboard/util/MeteringHelper$MeteringViewUsageType;->d:[Lflipboard/util/MeteringHelper$MeteringViewUsageType;

    invoke-virtual {v0}, [Lflipboard/util/MeteringHelper$MeteringViewUsageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/util/MeteringHelper$MeteringViewUsageType;

    return-object v0
.end method
