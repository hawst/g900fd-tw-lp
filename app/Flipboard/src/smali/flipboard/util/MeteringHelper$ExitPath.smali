.class public final enum Lflipboard/util/MeteringHelper$ExitPath;
.super Ljava/lang/Enum;
.source "MeteringHelper.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/util/MeteringHelper$ExitPath;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/util/MeteringHelper$ExitPath;

.field public static final enum b:Lflipboard/util/MeteringHelper$ExitPath;

.field public static final enum c:Lflipboard/util/MeteringHelper$ExitPath;

.field public static final enum d:Lflipboard/util/MeteringHelper$ExitPath;

.field private static final synthetic e:[Lflipboard/util/MeteringHelper$ExitPath;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 284
    new-instance v0, Lflipboard/util/MeteringHelper$ExitPath;

    const-string v1, "signIn"

    invoke-direct {v0, v1, v2}, Lflipboard/util/MeteringHelper$ExitPath;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/util/MeteringHelper$ExitPath;->a:Lflipboard/util/MeteringHelper$ExitPath;

    new-instance v0, Lflipboard/util/MeteringHelper$ExitPath;

    const-string v1, "cancel"

    invoke-direct {v0, v1, v3}, Lflipboard/util/MeteringHelper$ExitPath;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/util/MeteringHelper$ExitPath;->b:Lflipboard/util/MeteringHelper$ExitPath;

    new-instance v0, Lflipboard/util/MeteringHelper$ExitPath;

    const-string v1, "readArticle"

    invoke-direct {v0, v1, v4}, Lflipboard/util/MeteringHelper$ExitPath;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/util/MeteringHelper$ExitPath;->c:Lflipboard/util/MeteringHelper$ExitPath;

    new-instance v0, Lflipboard/util/MeteringHelper$ExitPath;

    const-string v1, "subscribe"

    invoke-direct {v0, v1, v5}, Lflipboard/util/MeteringHelper$ExitPath;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/util/MeteringHelper$ExitPath;->d:Lflipboard/util/MeteringHelper$ExitPath;

    const/4 v0, 0x4

    new-array v0, v0, [Lflipboard/util/MeteringHelper$ExitPath;

    sget-object v1, Lflipboard/util/MeteringHelper$ExitPath;->a:Lflipboard/util/MeteringHelper$ExitPath;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/util/MeteringHelper$ExitPath;->b:Lflipboard/util/MeteringHelper$ExitPath;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/util/MeteringHelper$ExitPath;->c:Lflipboard/util/MeteringHelper$ExitPath;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/util/MeteringHelper$ExitPath;->d:Lflipboard/util/MeteringHelper$ExitPath;

    aput-object v1, v0, v5

    sput-object v0, Lflipboard/util/MeteringHelper$ExitPath;->e:[Lflipboard/util/MeteringHelper$ExitPath;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 284
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/util/MeteringHelper$ExitPath;
    .locals 1

    .prologue
    .line 284
    const-class v0, Lflipboard/util/MeteringHelper$ExitPath;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/util/MeteringHelper$ExitPath;

    return-object v0
.end method

.method public static values()[Lflipboard/util/MeteringHelper$ExitPath;
    .locals 1

    .prologue
    .line 284
    sget-object v0, Lflipboard/util/MeteringHelper$ExitPath;->e:[Lflipboard/util/MeteringHelper$ExitPath;

    invoke-virtual {v0}, [Lflipboard/util/MeteringHelper$ExitPath;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/util/MeteringHelper$ExitPath;

    return-object v0
.end method
