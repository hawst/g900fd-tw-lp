.class Lflipboard/util/ShareHelper$1$3;
.super Ljava/lang/Object;
.source "ShareHelper.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:I

.field final synthetic c:Lflipboard/util/ShareHelper$1;


# direct methods
.method constructor <init>(Lflipboard/util/ShareHelper$1;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lflipboard/util/ShareHelper$1$3;->c:Lflipboard/util/ShareHelper$1;

    iput-object p2, p0, Lflipboard/util/ShareHelper$1$3;->a:Ljava/lang/String;

    iput p3, p0, Lflipboard/util/ShareHelper$1$3;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 250
    sget-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    new-array v0, v4, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 251
    const-string v0, "result"

    invoke-virtual {p1, v0}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 252
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 256
    const-string v2, "flipboard.extra.magazine.target"

    iget-object v3, p0, Lflipboard/util/ShareHelper$1$3;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    const-string v2, "flipboard.extra.magazine.num.items"

    iget v3, p0, Lflipboard/util/ShareHelper$1$3;->b:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 258
    const-string v2, "flipboard.extra.reference.link"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    const-string v0, "flipboard.extra.show.share.magazine"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 261
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-class v2, Lflipboard/activities/SharePromoteActivity;

    invoke-virtual {v0, v2, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 266
    sget-object v0, Lflipboard/util/ShareHelper;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 268
    return-void
.end method
