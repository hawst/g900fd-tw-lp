.class final Lflipboard/util/SocialHelper$29;
.super Ljava/lang/Object;
.source "SocialHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/FLPopoverWindow;

.field final synthetic b:Lflipboard/service/Section;

.field final synthetic c:Lflipboard/objs/FeedSectionLink;

.field final synthetic d:Lflipboard/activities/FlipboardActivity;


# direct methods
.method constructor <init>(Lflipboard/gui/FLPopoverWindow;Lflipboard/service/Section;Lflipboard/objs/FeedSectionLink;Lflipboard/activities/FlipboardActivity;)V
    .locals 0

    .prologue
    .line 1226
    iput-object p1, p0, Lflipboard/util/SocialHelper$29;->a:Lflipboard/gui/FLPopoverWindow;

    iput-object p2, p0, Lflipboard/util/SocialHelper$29;->b:Lflipboard/service/Section;

    iput-object p3, p0, Lflipboard/util/SocialHelper$29;->c:Lflipboard/objs/FeedSectionLink;

    iput-object p4, p0, Lflipboard/util/SocialHelper$29;->d:Lflipboard/activities/FlipboardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1229
    iget-object v0, p0, Lflipboard/util/SocialHelper$29;->a:Lflipboard/gui/FLPopoverWindow;

    if-eqz v0, :cond_0

    .line 1230
    iget-object v0, p0, Lflipboard/util/SocialHelper$29;->a:Lflipboard/gui/FLPopoverWindow;

    invoke-virtual {v0}, Lflipboard/gui/FLPopoverWindow;->dismiss()V

    .line 1232
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1233
    const-string v1, "source"

    const-string v2, "sectionLink"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1234
    const-string v1, "originSectionIdentifier"

    iget-object v2, p0, Lflipboard/util/SocialHelper$29;->b:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1235
    const-string v1, "linkType"

    const-string v2, "magazine"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1236
    new-instance v1, Lflipboard/service/Section;

    iget-object v2, p0, Lflipboard/util/SocialHelper$29;->c:Lflipboard/objs/FeedSectionLink;

    invoke-direct {v1, v2}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    iget-object v2, p0, Lflipboard/util/SocialHelper$29;->d:Lflipboard/activities/FlipboardActivity;

    invoke-static {v1, v2, v0}, Lflipboard/util/SocialHelper;->a(Lflipboard/service/Section;Landroid/content/Context;Landroid/os/Bundle;)V

    .line 1237
    return-void
.end method
