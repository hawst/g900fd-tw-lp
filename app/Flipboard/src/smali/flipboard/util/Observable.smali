.class public Lflipboard/util/Observable;
.super Ljava/lang/Object;
.source "Observable.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        "M:",
        "Ljava/lang/Object;",
        "A:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final H:[Ljava/lang/Object;


# instance fields
.field public I:[Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, Lflipboard/util/Observable;->H:[Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    sget-object v0, Lflipboard/util/Observable;->H:[Ljava/lang/Object;

    iput-object v0, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    .line 17
    return-void
.end method


# virtual methods
.method public final E()Z
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public F()Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 29
    return-object p0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;TA;)V"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v2, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    .line 79
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 80
    check-cast v0, Lflipboard/util/Observer;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflipboard/util/Observable;->F()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v4, p1, p2}, Lflipboard/util/Observer;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 79
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 82
    :cond_1
    return-void
.end method

.method public final declared-synchronized b(Lflipboard/util/Observer;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/util/Observer",
            "<TE;TM;TA;>;)V"
        }
    .end annotation

    .prologue
    .line 48
    monitor-enter p0

    if-nez p1, :cond_0

    .line 49
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null observer"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 51
    :cond_0
    :try_start_1
    iget-object v0, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    array-length v2, v2

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iput-object v2, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 52
    iget-object v0, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aput-object p1, v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 53
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized c(Lflipboard/util/Observer;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/util/Observer",
            "<TE;TM;TA;>;)V"
        }
    .end annotation

    .prologue
    .line 57
    monitor-enter p0

    if-nez p1, :cond_0

    .line 58
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null observer"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 60
    :cond_0
    :try_start_1
    iget-object v0, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    array-length v0, v0

    :cond_1
    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_4

    .line 61
    iget-object v1, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-ne v1, p1, :cond_1

    .line 62
    iget-object v1, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    array-length v1, v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 63
    sget-object v1, Lflipboard/util/Observable;->H:[Ljava/lang/Object;

    iput-object v1, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    goto :goto_0

    .line 65
    :cond_2
    iget-object v1, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 66
    iget-object v2, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 67
    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    array-length v3, v3

    if-ge v2, v3, :cond_3

    .line 68
    iget-object v2, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    add-int/lit8 v3, v0, 0x1

    iget-object v4, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    array-length v4, v4

    add-int/lit8 v5, v0, 0x1

    sub-int/2addr v4, v5

    invoke-static {v2, v3, v1, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 70
    :cond_3
    iput-object v1, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 74
    :cond_4
    monitor-exit p0

    return-void
.end method
