.class Lflipboard/util/TOCBuilder$TOCBuilderObserver$4;
.super Ljava/lang/Object;
.source "TOCBuilder.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/util/TOCBuilder$TOCBuilderObserver;


# direct methods
.method constructor <init>(Lflipboard/util/TOCBuilder$TOCBuilderObserver;)V
    .locals 0

    .prologue
    .line 491
    iput-object p1, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver$4;->a:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const v2, 0x7f0d0105

    .line 494
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialog$Builder;

    iget-object v1, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver$4;->a:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    iget-object v1, v1, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->b:Lflipboard/activities/FlipboardActivity;

    invoke-direct {v0, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 495
    invoke-virtual {v0, v2}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 496
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d0104

    .line 497
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d023f

    const/4 v2, 0x0

    .line 498
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 499
    const v1, 0x7f0d0173

    new-instance v2, Lflipboard/util/TOCBuilder$TOCBuilderObserver$4$1;

    invoke-direct {v2, p0}, Lflipboard/util/TOCBuilder$TOCBuilderObserver$4$1;-><init>(Lflipboard/util/TOCBuilder$TOCBuilderObserver$4;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 505
    iget-object v1, p0, Lflipboard/util/TOCBuilder$TOCBuilderObserver$4;->a:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    iget-object v1, v1, Lflipboard/util/TOCBuilder$TOCBuilderObserver;->b:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v1, v0}, Lflipboard/activities/FlipboardActivity;->a(Landroid/app/AlertDialog$Builder;)Landroid/app/AlertDialog;

    .line 506
    const-string v0, "BuildTOCFailed"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {}, Lflipboard/util/TOCBuilder;->a()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;J)V

    .line 507
    return-void
.end method
