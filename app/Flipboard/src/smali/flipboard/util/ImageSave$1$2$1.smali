.class Lflipboard/util/ImageSave$1$2$1;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "ImageSave.java"


# instance fields
.field final synthetic a:Lflipboard/util/ImageSave$1$2;


# direct methods
.method constructor <init>(Lflipboard/util/ImageSave$1$2;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lflipboard/util/ImageSave$1$2$1;->a:Lflipboard/util/ImageSave$1$2;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;)V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lflipboard/util/ImageSave$1$2$1;->a:Lflipboard/util/ImageSave$1$2;

    iget-object v0, v0, Lflipboard/util/ImageSave$1$2;->b:Lflipboard/util/ImageSave$1;

    invoke-static {v0}, Lflipboard/util/ImageSave$1;->d(Lflipboard/util/ImageSave$1;)Lflipboard/io/Download;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lflipboard/util/ImageSave$1$2$1;->a:Lflipboard/util/ImageSave$1$2;

    iget-object v0, v0, Lflipboard/util/ImageSave$1$2;->b:Lflipboard/util/ImageSave$1;

    invoke-static {v0}, Lflipboard/util/ImageSave$1;->e(Lflipboard/util/ImageSave$1;)Lflipboard/io/Download;

    move-result-object v0

    iget-object v1, p0, Lflipboard/util/ImageSave$1$2$1;->a:Lflipboard/util/ImageSave$1$2;

    iget-object v1, v1, Lflipboard/util/ImageSave$1$2;->a:Lflipboard/io/Download$Observer;

    invoke-virtual {v0, v1}, Lflipboard/io/Download;->b(Lflipboard/io/Download$Observer;)V

    .line 177
    :cond_0
    return-void
.end method

.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 3

    .prologue
    .line 159
    iget-object v0, p0, Lflipboard/util/ImageSave$1$2$1;->a:Lflipboard/util/ImageSave$1$2;

    iget-object v0, v0, Lflipboard/util/ImageSave$1$2;->a:Lflipboard/io/Download$Observer;

    iget-object v1, p0, Lflipboard/util/ImageSave$1$2$1;->a:Lflipboard/util/ImageSave$1$2;

    iget-object v1, v1, Lflipboard/util/ImageSave$1$2;->b:Lflipboard/util/ImageSave$1;

    iget v2, v1, Lflipboard/util/ImageSave$1;->a:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lflipboard/util/ImageSave$1;->a:I

    invoke-virtual {v0, v2}, Lflipboard/io/Download$Observer;->a(I)V

    .line 160
    iget-object v0, p0, Lflipboard/util/ImageSave$1$2$1;->a:Lflipboard/util/ImageSave$1$2;

    iget-object v0, v0, Lflipboard/util/ImageSave$1$2;->b:Lflipboard/util/ImageSave$1;

    iget-object v0, v0, Lflipboard/util/ImageSave$1;->d:Lflipboard/activities/FlipboardActivity;

    iget-object v1, p0, Lflipboard/util/ImageSave$1$2$1;->a:Lflipboard/util/ImageSave$1$2;

    iget-object v1, v1, Lflipboard/util/ImageSave$1$2;->b:Lflipboard/util/ImageSave$1;

    invoke-static {v1}, Lflipboard/util/ImageSave$1;->a(Lflipboard/util/ImageSave$1;)Lflipboard/io/Download;

    move-result-object v1

    iget-object v2, p0, Lflipboard/util/ImageSave$1$2$1;->a:Lflipboard/util/ImageSave$1$2;

    iget-object v2, v2, Lflipboard/util/ImageSave$1$2;->a:Lflipboard/io/Download$Observer;

    invoke-static {v0, v1, v2}, Lflipboard/util/ImageSave;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/io/Download;Lflipboard/io/Download$Observer;)V

    .line 161
    return-void
.end method

.method public final d(Landroid/support/v4/app/DialogFragment;)V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lflipboard/util/ImageSave$1$2$1;->a:Lflipboard/util/ImageSave$1$2;

    iget-object v0, v0, Lflipboard/util/ImageSave$1$2;->b:Lflipboard/util/ImageSave$1;

    invoke-static {v0}, Lflipboard/util/ImageSave$1;->b(Lflipboard/util/ImageSave$1;)Lflipboard/io/Download;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lflipboard/util/ImageSave$1$2$1;->a:Lflipboard/util/ImageSave$1$2;

    iget-object v0, v0, Lflipboard/util/ImageSave$1$2;->b:Lflipboard/util/ImageSave$1;

    invoke-static {v0}, Lflipboard/util/ImageSave$1;->c(Lflipboard/util/ImageSave$1;)Lflipboard/io/Download;

    move-result-object v0

    iget-object v1, p0, Lflipboard/util/ImageSave$1$2$1;->a:Lflipboard/util/ImageSave$1$2;

    iget-object v1, v1, Lflipboard/util/ImageSave$1$2;->a:Lflipboard/io/Download$Observer;

    invoke-virtual {v0, v1}, Lflipboard/io/Download;->b(Lflipboard/io/Download$Observer;)V

    .line 169
    :cond_0
    return-void
.end method
