.class final Lflipboard/util/FLTextUtil$3;
.super Ljava/lang/Object;
.source "FLTextUtil.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 175
    move-object v0, p1

    .line 176
    check-cast v0, Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 177
    invoke-static {}, Landroid/text/Spannable$Factory;->getInstance()Landroid/text/Spannable$Factory;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/text/Spannable$Factory;->newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v0

    .line 178
    check-cast p1, Lflipboard/gui/FLTextView;

    .line 179
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 181
    if-eq v3, v2, :cond_0

    if-nez v3, :cond_2

    .line 183
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    .line 184
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    .line 186
    invoke-virtual {p1}, Lflipboard/gui/FLTextView;->getTotalPaddingLeft()I

    move-result v6

    sub-int/2addr v4, v6

    .line 187
    invoke-virtual {p1}, Lflipboard/gui/FLTextView;->getTotalPaddingTop()I

    move-result v6

    sub-int/2addr v5, v6

    .line 189
    invoke-virtual {p1}, Lflipboard/gui/FLTextView;->getScrollX()I

    move-result v6

    add-int/2addr v4, v6

    .line 190
    invoke-virtual {p1}, Lflipboard/gui/FLTextView;->getScrollY()I

    move-result v6

    add-int/2addr v5, v6

    .line 192
    invoke-virtual {p1}, Lflipboard/gui/FLTextView;->getLayout()Landroid/text/Layout;

    move-result-object v6

    .line 193
    invoke-virtual {v6, v5}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v5

    .line 194
    int-to-float v4, v4

    invoke-virtual {v6, v5, v4}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v4

    .line 196
    const-class v5, Landroid/text/style/ClickableSpan;

    invoke-interface {v0, v4, v4, v5}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/ClickableSpan;

    .line 198
    array-length v4, v0

    if-eqz v4, :cond_2

    .line 199
    if-ne v3, v2, :cond_1

    .line 200
    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Landroid/text/style/ClickableSpan;->onClick(Landroid/view/View;)V

    :cond_1
    move v0, v2

    .line 205
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
