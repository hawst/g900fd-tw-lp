.class Lflipboard/gcm/GCMPushServiceManager$1;
.super Ljava/lang/Object;
.source "GCMPushServiceManager.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lflipboard/service/User;

.field final synthetic c:Lflipboard/gcm/GCMPushServiceManager;


# direct methods
.method constructor <init>(Lflipboard/gcm/GCMPushServiceManager;Ljava/lang/String;Lflipboard/service/User;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lflipboard/gcm/GCMPushServiceManager$1;->c:Lflipboard/gcm/GCMPushServiceManager;

    iput-object p2, p0, Lflipboard/gcm/GCMPushServiceManager$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lflipboard/gcm/GCMPushServiceManager$1;->b:Lflipboard/service/User;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 148
    iget-object v0, p0, Lflipboard/gcm/GCMPushServiceManager$1;->a:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gcm/GCMPushServiceManager$1;->c:Lflipboard/gcm/GCMPushServiceManager;

    invoke-static {v1}, Lflipboard/gcm/GCMPushServiceManager;->a(Lflipboard/gcm/GCMPushServiceManager;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gcm/GCMRegistrar;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    sget-object v0, Lflipboard/service/PushServiceManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/gcm/GCMPushServiceManager$1;->b:Lflipboard/service/User;

    aput-object v2, v0, v1

    iget-object v1, p0, Lflipboard/gcm/GCMPushServiceManager$1;->a:Ljava/lang/String;

    aput-object v1, v0, v3

    .line 150
    iget-object v0, p0, Lflipboard/gcm/GCMPushServiceManager$1;->c:Lflipboard/gcm/GCMPushServiceManager;

    invoke-static {v0}, Lflipboard/gcm/GCMPushServiceManager;->b(Lflipboard/gcm/GCMPushServiceManager;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/google/android/gcm/GCMRegistrar;->a(Landroid/content/Context;Z)V

    .line 152
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 156
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "registerNotification failed: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 159
    iget-object v0, p0, Lflipboard/gcm/GCMPushServiceManager$1;->a:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gcm/GCMPushServiceManager$1;->c:Lflipboard/gcm/GCMPushServiceManager;

    invoke-static {v1}, Lflipboard/gcm/GCMPushServiceManager;->c(Lflipboard/gcm/GCMPushServiceManager;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gcm/GCMRegistrar;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lflipboard/gcm/GCMPushServiceManager$1;->c:Lflipboard/gcm/GCMPushServiceManager;

    invoke-static {v0}, Lflipboard/gcm/GCMPushServiceManager;->d(Lflipboard/gcm/GCMPushServiceManager;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gcm/GCMRegistrar;->c(Landroid/content/Context;)V

    .line 162
    :cond_0
    return-void
.end method
