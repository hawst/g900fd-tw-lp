.class public Lflipboard/gcm/FlipboardGCMIntentService;
.super Lcom/google/android/gcm/GCMBaseIntentService;
.source "FlipboardGCMIntentService.java"


# static fields
.field public static a:Lflipboard/util/Log;

.field public static b:Lflipboard/persist/Persister;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-string v0, "gcm"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gcm/FlipboardGCMIntentService;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 64
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v2

    iget-object v2, v2, Lflipboard/model/ConfigSetting;->GoogleNotificationSenderID:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gcm/GCMBaseIntentService;-><init>([Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method private static a()Lflipboard/persist/Persister;
    .locals 3

    .prologue
    .line 72
    sget-object v0, Lflipboard/gcm/FlipboardGCMIntentService;->b:Lflipboard/persist/Persister;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Lflipboard/persist/DiskPersister;

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1}, Lflipboard/app/FlipboardApplication;->getCacheDir()Ljava/io/File;

    move-result-object v1

    new-instance v2, Lflipboard/persist/GsonSerializer;

    invoke-direct {v2}, Lflipboard/persist/GsonSerializer;-><init>()V

    invoke-direct {v0, v1, v2}, Lflipboard/persist/DiskPersister;-><init>(Ljava/io/File;Lflipboard/persist/Serializer;)V

    sput-object v0, Lflipboard/gcm/FlipboardGCMIntentService;->b:Lflipboard/persist/Persister;

    .line 75
    :cond_0
    sget-object v0, Lflipboard/gcm/FlipboardGCMIntentService;->b:Lflipboard/persist/Persister;

    return-object v0
.end method

.method private static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "notification_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(ILjava/util/List;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lflipboard/model/GCMMessage;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 218
    const/4 v0, 0x0

    .line 219
    packed-switch p1, :pswitch_data_0

    .line 256
    :goto_0
    :pswitch_0
    return-object v0

    .line 221
    :pswitch_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 229
    const v1, 0x7f0d022b

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/GCMMessage;

    iget-object v0, v0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    iget-object v0, v0, Lflipboard/model/GCMMessage$Group;->actor:Lflipboard/model/GCMMessage$Group$Actor;

    iget-object v0, v0, Lflipboard/model/GCMMessage$Group$Actor;->name:Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Lflipboard/gcm/FlipboardGCMIntentService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 223
    :pswitch_2
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/GCMMessage;

    invoke-virtual {v0, p0}, Lflipboard/model/GCMMessage;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 226
    :pswitch_3
    const v1, 0x7f0d022c

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/GCMMessage;

    iget-object v0, v0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    iget-object v0, v0, Lflipboard/model/GCMMessage$Group;->actor:Lflipboard/model/GCMMessage$Group$Actor;

    iget-object v0, v0, Lflipboard/model/GCMMessage$Group$Actor;->name:Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/GCMMessage;

    iget-object v0, v0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    iget-object v0, v0, Lflipboard/model/GCMMessage$Group;->actor:Lflipboard/model/GCMMessage$Group$Actor;

    iget-object v0, v0, Lflipboard/model/GCMMessage$Group$Actor;->name:Ljava/lang/String;

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Lflipboard/gcm/FlipboardGCMIntentService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 234
    :pswitch_4
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    .line 242
    const v1, 0x7f0d022e

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/GCMMessage;

    iget-object v0, v0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    iget-object v0, v0, Lflipboard/model/GCMMessage$Group;->actor:Lflipboard/model/GCMMessage$Group$Actor;

    iget-object v0, v0, Lflipboard/model/GCMMessage$Group$Actor;->name:Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Lflipboard/gcm/FlipboardGCMIntentService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 236
    :pswitch_5
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/GCMMessage;

    invoke-virtual {v0, p0}, Lflipboard/model/GCMMessage;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 239
    :pswitch_6
    const v1, 0x7f0d022f

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/GCMMessage;

    iget-object v0, v0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    iget-object v0, v0, Lflipboard/model/GCMMessage$Group;->actor:Lflipboard/model/GCMMessage$Group$Actor;

    iget-object v0, v0, Lflipboard/model/GCMMessage$Group$Actor;->name:Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/GCMMessage;

    iget-object v0, v0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    iget-object v0, v0, Lflipboard/model/GCMMessage$Group;->actor:Lflipboard/model/GCMMessage$Group$Actor;

    iget-object v0, v0, Lflipboard/model/GCMMessage$Group$Actor;->name:Ljava/lang/String;

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Lflipboard/gcm/FlipboardGCMIntentService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 247
    :pswitch_7
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_3

    .line 252
    const v0, 0x7f0d0231

    new-array v1, v4, [Ljava/lang/Object;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lflipboard/gcm/FlipboardGCMIntentService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 249
    :pswitch_8
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/GCMMessage;

    invoke-virtual {v0, p0}, Lflipboard/model/GCMMessage;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 219
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch

    .line 221
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 234
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 247
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_8
    .end packed-switch
.end method

.method private static a(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 303
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->w:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->g:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 304
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->u:Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v2, "display_group"

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 305
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->w:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 306
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v0, v1, p1}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 307
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 308
    return-void
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 59
    invoke-static {}, Lflipboard/gcm/FlipboardGCMIntentService;->a()Lflipboard/persist/Persister;

    move-result-object v0

    invoke-static {p1}, Lflipboard/gcm/FlipboardGCMIntentService;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/persist/Persister;->a(Ljava/lang/String;)V

    .line 60
    invoke-static {p0, p1}, Lflipboard/notifications/FLNotification;->a(Landroid/content/Context;I)V

    .line 61
    return-void
.end method

.method private a(Ljava/util/List;)[Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/model/GCMMessage;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 260
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 261
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 262
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/GCMMessage;

    invoke-virtual {v0, p0}, Lflipboard/model/GCMMessage;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 261
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 264
    :cond_0
    return-object v2
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 19

    .prologue
    .line 96
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lflipboard/util/HelpshiftHelper;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    new-instance v18, Lflipboard/model/GCMMessage;

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lflipboard/model/GCMMessage;-><init>(Landroid/content/Intent;)V

    .line 101
    new-instance v2, Lflipboard/objs/UsageEventV2;

    sget-object v3, Lflipboard/objs/UsageEventV2$EventAction;->w:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v4, Lflipboard/objs/UsageEventV2$EventCategory;->g:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v2, v3, v4}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->u:Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v4, "received"

    invoke-virtual {v2, v3, v4}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->p:Lflipboard/objs/UsageEventV2$CommonEventData;

    move-object/from16 v0, v18

    iget-object v4, v0, Lflipboard/model/GCMMessage;->actionURL:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    move-object/from16 v0, v18

    iget-object v4, v0, Lflipboard/model/GCMMessage;->usageEventType:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    invoke-virtual {v2}, Lflipboard/objs/UsageEventV2;->c()V

    .line 103
    sget-object v2, Lflipboard/gcm/FlipboardGCMIntentService;->a:Lflipboard/util/Log;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, v18

    iget-object v4, v0, Lflipboard/model/GCMMessage;->uid:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v18, v2, v3

    const/4 v3, 0x2

    move-object/from16 v0, v18

    iget-object v4, v0, Lflipboard/model/GCMMessage;->actionURL:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 105
    invoke-static {}, Lflipboard/service/PushServiceManager;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 106
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 107
    move-object/from16 v0, v18

    iget-object v2, v0, Lflipboard/model/GCMMessage;->ignoreUid:Ljava/lang/String;

    if-eqz v2, :cond_8

    move-object/from16 v0, v18

    iget-object v2, v0, Lflipboard/model/GCMMessage;->ignoreUid:Ljava/lang/String;

    const-string v4, "true"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    .line 109
    :goto_1
    invoke-virtual/range {v18 .. v19}, Lflipboard/model/GCMMessage;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_10

    if-nez v2, :cond_2

    move-object/from16 v0, v18

    iget-object v2, v0, Lflipboard/model/GCMMessage;->uid:Ljava/lang/String;

    if-eqz v2, :cond_10

    move-object/from16 v0, v18

    iget-object v2, v0, Lflipboard/model/GCMMessage;->uid:Ljava/lang/String;

    iget-object v3, v3, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 110
    :cond_2
    move-object/from16 v0, v18

    iget-object v2, v0, Lflipboard/model/GCMMessage;->flabExperimentId:Ljava/lang/String;

    if-eqz v2, :cond_3

    move-object/from16 v0, v18

    iget-object v2, v0, Lflipboard/model/GCMMessage;->flabCellId:Ljava/lang/String;

    if-eqz v2, :cond_3

    move-object/from16 v0, v18

    iget-object v2, v0, Lflipboard/model/GCMMessage;->flabExperimentId:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v3, v0, Lflipboard/model/GCMMessage;->flabCellId:Ljava/lang/String;

    invoke-static {v2, v3}, Lflipboard/abtest/Experiments;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_3
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    const-string v2, "timeReceived"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v8, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    move-object/from16 v0, v18

    iget-object v2, v0, Lflipboard/model/GCMMessage;->actionURL:Ljava/lang/String;

    if-eqz v2, :cond_4

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->p:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v2}, Lflipboard/objs/UsageEventV2$CommonEventData;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iget-object v3, v0, Lflipboard/model/GCMMessage;->actionURL:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object/from16 v0, v18

    iget-object v2, v0, Lflipboard/model/GCMMessage;->usageEventType:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v2}, Lflipboard/objs/UsageEventV2$CommonEventData;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iget-object v3, v0, Lflipboard/model/GCMMessage;->usageEventType:Ljava/lang/String;

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const/4 v2, 0x0

    invoke-virtual/range {v18 .. v18}, Lflipboard/model/GCMMessage;->isOfKnownType()Z

    move-result v3

    if-eqz v3, :cond_9

    move-object/from16 v0, v18

    iget-object v3, v0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    iget-object v4, v3, Lflipboard/model/GCMMessage$Group;->type:Ljava/lang/String;

    const/4 v3, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_6
    :goto_2
    packed-switch v3, :pswitch_data_0

    :goto_3
    move v3, v2

    :goto_4
    new-instance v2, Lflipboard/gcm/FlipboardGCMIntentService$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lflipboard/gcm/FlipboardGCMIntentService$1;-><init>(Lflipboard/gcm/FlipboardGCMIntentService;)V

    iget-object v2, v2, Lcom/google/gson/reflect/TypeToken;->b:Ljava/lang/reflect/Type;

    invoke-static {}, Lflipboard/gcm/FlipboardGCMIntentService;->a()Lflipboard/persist/Persister;

    move-result-object v4

    invoke-static {v3}, Lflipboard/gcm/FlipboardGCMIntentService;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v2}, Lflipboard/persist/Persister;->a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-nez v2, :cond_12

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v17, v2

    :goto_5
    invoke-virtual/range {v18 .. v18}, Lflipboard/model/GCMMessage;->isOfKnownType()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface/range {v17 .. v18}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v5, 0x0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_a

    move-object/from16 v0, v18

    iget-object v2, v0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    iget-object v2, v2, Lflipboard/model/GCMMessage$Group;->actor:Lflipboard/model/GCMMessage$Group$Actor;

    iget-object v5, v2, Lflipboard/model/GCMMessage$Group$Actor;->image:Ljava/lang/String;

    :goto_6
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->w:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v2}, Lflipboard/objs/UsageEventV2$CommonEventData;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v8, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v3, v1}, Lflipboard/gcm/FlipboardGCMIntentService;->a(ILjava/util/List;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lflipboard/gcm/FlipboardGCMIntentService;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v7

    new-instance v2, Lflipboard/notifications/InboxStyleNotification;

    move-object/from16 v0, v18

    iget-object v4, v0, Lflipboard/model/GCMMessage;->actionURL:Ljava/lang/String;

    invoke-direct/range {v2 .. v8}, Lflipboard/notifications/InboxStyleNotification;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)V

    move-object v9, v2

    :cond_7
    :goto_7
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lflipboard/gcm/ClearNotificationReciever;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "extra_clear_id"

    iget v5, v9, Lflipboard/notifications/FLNotification;->c:I

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v4, 0x0

    const/high16 v5, 0x10000000

    move-object/from16 v0, p0

    invoke-static {v0, v4, v2, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, v9, Lflipboard/notifications/FLNotification;->d:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Lflipboard/notifications/FLNotification;->b(Landroid/content/Context;)V

    sget-object v2, Lflipboard/gcm/FlipboardGCMIntentService;->b:Lflipboard/persist/Persister;

    invoke-static {v3}, Lflipboard/gcm/FlipboardGCMIntentService;->a(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-interface {v2, v3, v0}, Lflipboard/persist/Persister;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 107
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 110
    :sswitch_0
    const-string v5, "followedYou"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v3, 0x0

    goto/16 :goto_2

    :sswitch_1
    const-string v5, "sharedWithYou"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v3, 0x1

    goto/16 :goto_2

    :pswitch_0
    const/4 v3, 0x1

    goto/16 :goto_4

    :pswitch_1
    const/4 v2, 0x2

    goto/16 :goto_3

    :cond_9
    const/4 v3, 0x6

    goto/16 :goto_4

    :cond_a
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v2}, Lflipboard/objs/UsageEventV2$CommonEventData;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iget-object v4, v0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    iget-object v4, v4, Lflipboard/model/GCMMessage$Group;->type:Ljava/lang/String;

    invoke-virtual {v8, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, v18

    iget-object v4, v0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    iget-object v4, v4, Lflipboard/model/GCMMessage$Group;->type:Ljava/lang/String;

    invoke-static {v2, v4}, Lflipboard/gcm/FlipboardGCMIntentService;->a(ILjava/lang/String;)V

    goto/16 :goto_6

    :cond_b
    move-object/from16 v0, v18

    iget-object v2, v0, Lflipboard/model/GCMMessage;->actionURL:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d

    move-object/from16 v0, v18

    iget-object v2, v0, Lflipboard/model/GCMMessage;->largeImage:Ljava/lang/String;

    if-eqz v2, :cond_c

    new-instance v9, Lflipboard/notifications/BigPictureStyleNotification;

    sget v10, Lflipboard/notifications/FLNotification;->a:I

    add-int/lit8 v2, v10, 0x1

    sput v2, Lflipboard/notifications/FLNotification;->a:I

    invoke-virtual/range {v18 .. v19}, Lflipboard/model/GCMMessage;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v18

    iget-object v12, v0, Lflipboard/model/GCMMessage;->actionURL:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v13, v0, Lflipboard/model/GCMMessage;->largeImage:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-wide v14, v0, Lflipboard/model/GCMMessage;->dateSent:J

    move-object/from16 v16, v8

    invoke-direct/range {v9 .. v16}, Lflipboard/notifications/BigPictureStyleNotification;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;)V

    :goto_8
    move-object/from16 v0, v18

    iget-wide v4, v0, Lflipboard/model/GCMMessage;->expireAt:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_7

    new-instance v2, Lflipboard/notifications/ExpireNotification;

    move-object/from16 v0, v18

    iget-wide v4, v0, Lflipboard/model/GCMMessage;->expireAt:J

    invoke-direct {v2, v9, v4, v5}, Lflipboard/notifications/ExpireNotification;-><init>(Lflipboard/notifications/FLNotification;J)V

    move-object v9, v2

    goto/16 :goto_7

    :cond_c
    new-instance v9, Lflipboard/notifications/BigTextStyleNotification;

    sget v10, Lflipboard/notifications/FLNotification;->a:I

    add-int/lit8 v2, v10, 0x1

    sput v2, Lflipboard/notifications/FLNotification;->a:I

    invoke-virtual/range {v18 .. v19}, Lflipboard/model/GCMMessage;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v18

    iget-object v12, v0, Lflipboard/model/GCMMessage;->actionURL:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v13, v0, Lflipboard/model/GCMMessage;->largeImage:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-wide v14, v0, Lflipboard/model/GCMMessage;->dateSent:J

    move-object/from16 v16, v8

    invoke-direct/range {v9 .. v16}, Lflipboard/notifications/BigTextStyleNotification;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;)V

    goto :goto_8

    :cond_d
    invoke-interface/range {v17 .. v18}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->w:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v2}, Lflipboard/objs/UsageEventV2$CommonEventData;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v8, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_f

    move-object/from16 v0, v18

    iget-object v2, v0, Lflipboard/model/GCMMessage;->largeImage:Ljava/lang/String;

    if-eqz v2, :cond_e

    new-instance v9, Lflipboard/notifications/BigPictureStyleNotification;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v3, v1}, Lflipboard/gcm/FlipboardGCMIntentService;->a(ILjava/util/List;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v18

    iget-object v12, v0, Lflipboard/model/GCMMessage;->actionURL:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v13, v0, Lflipboard/model/GCMMessage;->largeImage:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-wide v14, v0, Lflipboard/model/GCMMessage;->dateSent:J

    move v10, v3

    move-object/from16 v16, v8

    invoke-direct/range {v9 .. v16}, Lflipboard/notifications/BigPictureStyleNotification;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;)V

    goto/16 :goto_7

    :cond_e
    new-instance v9, Lflipboard/notifications/BigTextStyleNotification;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v3, v1}, Lflipboard/gcm/FlipboardGCMIntentService;->a(ILjava/util/List;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v18

    iget-object v12, v0, Lflipboard/model/GCMMessage;->actionURL:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v13, v0, Lflipboard/model/GCMMessage;->largeImage:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-wide v14, v0, Lflipboard/model/GCMMessage;->dateSent:J

    move v10, v3

    move-object/from16 v16, v8

    invoke-direct/range {v9 .. v16}, Lflipboard/notifications/BigTextStyleNotification;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;)V

    goto/16 :goto_7

    :cond_f
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lflipboard/gcm/FlipboardGCMIntentService;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v6

    new-instance v2, Lflipboard/notifications/InboxStyleNotification;

    move-object/from16 v0, v18

    iget-object v4, v0, Lflipboard/model/GCMMessage;->actionURL:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v3, v1}, Lflipboard/gcm/FlipboardGCMIntentService;->a(ILjava/util/List;)Ljava/lang/String;

    move-result-object v5

    move-object v7, v8

    invoke-direct/range {v2 .. v7}, Lflipboard/notifications/InboxStyleNotification;-><init>(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)V

    sget-object v4, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v4}, Lflipboard/objs/UsageEventV2$CommonEventData;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    iget-object v5, v0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    iget-object v5, v5, Lflipboard/model/GCMMessage$Group;->type:Ljava/lang/String;

    invoke-virtual {v8, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v4

    move-object/from16 v0, v18

    iget-object v5, v0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    iget-object v5, v5, Lflipboard/model/GCMMessage$Group;->type:Ljava/lang/String;

    invoke-static {v4, v5}, Lflipboard/gcm/FlipboardGCMIntentService;->a(ILjava/lang/String;)V

    move-object v9, v2

    goto/16 :goto_7

    .line 113
    :cond_10
    invoke-virtual/range {v18 .. v19}, Lflipboard/model/GCMMessage;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 114
    const-string v2, "no message"

    .line 118
    :goto_9
    move-object/from16 v0, v18

    iget-object v3, v0, Lflipboard/model/GCMMessage;->actionURL:Ljava/lang/String;

    new-instance v4, Lflipboard/objs/UsageEventV2;

    sget-object v5, Lflipboard/objs/UsageEventV2$EventAction;->w:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v6, Lflipboard/objs/UsageEventV2$EventCategory;->g:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v4, v5, v6}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    sget-object v5, Lflipboard/objs/UsageEventV2$CommonEventData;->u:Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v6, "dropped"

    invoke-virtual {v4, v5, v6}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v5, Lflipboard/objs/UsageEventV2$CommonEventData;->p:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v4, v5, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    const-string v3, "usage_event_type"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v5, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v4, v5, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->c:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v4, v3, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    invoke-virtual {v4}, Lflipboard/objs/UsageEventV2;->c()V

    .line 119
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "unable to display notification: (uid=%s): %s [%s]"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, v18

    iget-object v6, v0, Lflipboard/model/GCMMessage;->uid:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v18, v4, v5

    const/4 v5, 0x2

    move-object/from16 v0, v18

    iget-object v6, v0, Lflipboard/model/GCMMessage;->actionURL:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 116
    :cond_11
    const-string v2, "bad uid"

    goto :goto_9

    :cond_12
    move-object/from16 v17, v2

    goto/16 :goto_5

    .line 110
    :sswitch_data_0
    .sparse-switch
        0x3f56cd54 -> :sswitch_1
        0x5f7781ef -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 269
    sget-object v0, Lflipboard/gcm/FlipboardGCMIntentService;->a:Lflipboard/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 270
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 275
    sget-object v0, Lflipboard/gcm/FlipboardGCMIntentService;->a:Lflipboard/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received recoverable error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 276
    invoke-super {p0, p1, p2}, Lcom/google/android/gcm/GCMBaseIntentService;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 79
    sget-object v0, Lflipboard/gcm/FlipboardGCMIntentService;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 80
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 81
    invoke-static {}, Lflipboard/gcm/GCMPushServiceManager;->a()Lflipboard/gcm/GCMPushServiceManager;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_0

    .line 83
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p1, v1}, Lflipboard/gcm/GCMPushServiceManager;->a(Ljava/lang/String;Lflipboard/service/User;)V

    .line 85
    :cond_0
    invoke-static {p0}, Lflipboard/util/HelpshiftHelper;->a(Landroid/content/Context;)V

    .line 87
    :cond_1
    return-void
.end method

.method protected final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 91
    sget-object v0, Lflipboard/gcm/FlipboardGCMIntentService;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 92
    return-void
.end method
