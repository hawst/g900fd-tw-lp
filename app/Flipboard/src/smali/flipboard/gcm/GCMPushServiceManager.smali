.class public Lflipboard/gcm/GCMPushServiceManager;
.super Lflipboard/service/PushServiceManager;
.source "GCMPushServiceManager.java"


# instance fields
.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lflipboard/service/PushServiceManager;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gcm/GCMPushServiceManager;->d:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lflipboard/gcm/GCMPushServiceManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    return-object v0
.end method

.method static a()Lflipboard/gcm/GCMPushServiceManager;
    .locals 3

    .prologue
    .line 63
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->O:Lflipboard/service/PushServiceManager;

    .line 65
    if-eqz v0, :cond_0

    instance-of v1, v0, Lflipboard/gcm/GCMPushServiceManager;

    if-eqz v1, :cond_0

    .line 66
    check-cast v0, Lflipboard/gcm/GCMPushServiceManager;

    .line 72
    :goto_0
    return-object v0

    .line 71
    :cond_0
    sget-object v0, Lflipboard/gcm/GCMPushServiceManager;->a:Lflipboard/util/Log;

    const-string v1, "GCMPushServiceManager not available"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lflipboard/gcm/GCMPushServiceManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    return-object v0
.end method

.method private b(Ljava/lang/String;Lflipboard/service/User;)V
    .locals 3

    .prologue
    .line 180
    invoke-static {p2}, Lflipboard/gcm/GCMPushServiceManager;->c(Lflipboard/service/User;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gcm/GCMRegistrar;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gcm/GCMRegistrar;->a(Landroid/content/Context;Z)V

    .line 186
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gcm/GCMPushServiceManager$2;

    invoke-direct {v1, p0, p2, p1}, Lflipboard/gcm/GCMPushServiceManager$2;-><init>(Lflipboard/gcm/GCMPushServiceManager;Lflipboard/service/User;Ljava/lang/String;)V

    new-instance v2, Lflipboard/service/Flap$NotificationRequest;

    invoke-direct {v2, v0, p2}, Lflipboard/service/Flap$NotificationRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v2, p1, v1}, Lflipboard/service/Flap$NotificationRequest;->b(Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V

    .line 202
    :cond_0
    return-void
.end method

.method static synthetic c(Lflipboard/gcm/GCMPushServiceManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lflipboard/gcm/GCMPushServiceManager;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gcm/GCMRegistrar;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gcm/GCMPushServiceManager;->d:Ljava/lang/String;

    .line 85
    :cond_0
    iget-object v0, p0, Lflipboard/gcm/GCMPushServiceManager;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lflipboard/gcm/GCMPushServiceManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    return-object v0
.end method

.method private d()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 90
    :try_start_0
    iget-object v1, p0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gcm/GCMRegistrar;->a(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 95
    :try_start_1
    iget-object v1, p0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gcm/GCMRegistrar;->b(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    .line 99
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 97
    :catch_0
    move-exception v1

    goto :goto_0

    .line 92
    :catch_1
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lflipboard/service/User;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 31
    invoke-direct {p0}, Lflipboard/gcm/GCMPushServiceManager;->c()Ljava/lang/String;

    move-result-object v0

    .line 32
    invoke-static {}, Lflipboard/gcm/GCMPushServiceManager;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p1}, Lflipboard/gcm/GCMPushServiceManager;->c(Lflipboard/service/User;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 33
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 35
    sget-object v1, Lflipboard/gcm/GCMPushServiceManager;->a:Lflipboard/util/Log;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v4

    aput-object v0, v1, v5

    .line 36
    invoke-virtual {p0, v0, p1}, Lflipboard/gcm/GCMPushServiceManager;->a(Ljava/lang/String;Lflipboard/service/User;)V

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 39
    :cond_1
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v1

    invoke-static {}, Lflipboard/gcm/GCMPushServiceManager;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lflipboard/gcm/GCMPushServiceManager;->c(Lflipboard/service/User;)Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gcm/GCMRegistrar;->a(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v2, p0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gcm/GCMRegistrar;->b(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v2, p0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    const-string v3, "com.google.android.c2dm.permission.RECEIVE"

    invoke-static {v2, v3}, Lflipboard/util/AndroidUtil;->d(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "GCM not supported. Device does not have GCM receive permission"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v0, Lflipboard/gcm/GCMPushServiceManager;->a:Lflipboard/util/Log;

    iget-object v0, p0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    new-array v2, v5, [Ljava/lang/String;

    iget-object v1, v1, Lflipboard/model/ConfigSetting;->GoogleNotificationSenderID:Ljava/lang/String;

    aput-object v1, v2, v4

    invoke-static {v0, v2}, Lcom/google/android/gcm/GCMRegistrar;->a(Landroid/content/Context;[Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    sget-object v1, Lflipboard/gcm/GCMPushServiceManager;->a:Lflipboard/util/Log;

    new-array v1, v5, [Ljava/lang/Object;

    aput-object v0, v1, v4

    sget-object v1, Lflipboard/gcm/GCMPushServiceManager;->a:Lflipboard/util/Log;

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p1, v1, v4

    invoke-virtual {p0, v0, p1}, Lflipboard/gcm/GCMPushServiceManager;->a(Ljava/lang/String;Lflipboard/service/User;)V

    goto :goto_0

    .line 41
    :cond_4
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ac:Z

    if-nez v1, :cond_0

    .line 42
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lflipboard/gcm/GCMPushServiceManager;->d()Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Lflipboard/gcm/GCMPushServiceManager;->a:Lflipboard/util/Log;

    new-array v1, v5, [Ljava/lang/Object;

    aput-object v0, v1, v4

    iget-object v1, p0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gcm/GCMRegistrar;->c(Landroid/content/Context;)V

    :cond_5
    invoke-direct {p0, v0, p1}, Lflipboard/gcm/GCMPushServiceManager;->b(Ljava/lang/String;Lflipboard/service/User;)V

    goto/16 :goto_0
.end method

.method final a(Ljava/lang/String;Lflipboard/service/User;)V
    .locals 6

    .prologue
    .line 137
    invoke-static {p2}, Lflipboard/gcm/GCMPushServiceManager;->c(Lflipboard/service/User;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gcm/GCMRegistrar;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    .line 139
    iget-wide v2, v0, Lflipboard/model/ConfigSetting;->NotificationIdRefreshOnServerInterval:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 141
    iget-object v1, p0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    iget-wide v2, v0, Lflipboard/model/ConfigSetting;->NotificationIdRefreshOnServerInterval:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gcm/GCMRegistrar;->a(Landroid/content/Context;J)V

    .line 145
    :goto_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gcm/GCMPushServiceManager$1;

    invoke-direct {v1, p0, p1, p2}, Lflipboard/gcm/GCMPushServiceManager$1;-><init>(Lflipboard/gcm/GCMPushServiceManager;Ljava/lang/String;Lflipboard/service/User;)V

    new-instance v2, Lflipboard/service/Flap$NotificationRequest;

    invoke-direct {v2, v0, p2}, Lflipboard/service/Flap$NotificationRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v2, p1, v1}, Lflipboard/service/Flap$NotificationRequest;->a(Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V

    .line 165
    :cond_0
    return-void

    .line 143
    :cond_1
    iget-object v0, p0, Lflipboard/service/PushServiceManager;->c:Landroid/content/Context;

    const-wide/32 v2, 0x240c8400

    invoke-static {v0, v2, v3}, Lcom/google/android/gcm/GCMRegistrar;->a(Landroid/content/Context;J)V

    goto :goto_0
.end method

.method public final b(Lflipboard/service/User;)V
    .locals 3

    .prologue
    .line 48
    invoke-direct {p0}, Lflipboard/gcm/GCMPushServiceManager;->c()Ljava/lang/String;

    move-result-object v0

    .line 50
    invoke-static {}, Lflipboard/gcm/GCMPushServiceManager;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lflipboard/gcm/GCMPushServiceManager;->c(Lflipboard/service/User;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 55
    sget-object v1, Lflipboard/gcm/GCMPushServiceManager;->a:Lflipboard/util/Log;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    .line 56
    invoke-direct {p0, v0, p1}, Lflipboard/gcm/GCMPushServiceManager;->b(Ljava/lang/String;Lflipboard/service/User;)V

    .line 59
    :cond_0
    return-void
.end method
