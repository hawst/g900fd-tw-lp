.class public Lflipboard/widget/FlipboardWidget$UpdateService;
.super Landroid/app/Service;
.source "FlipboardWidget.java"


# instance fields
.field a:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method final a()V
    .locals 3

    .prologue
    .line 235
    invoke-static {}, Lflipboard/widget/FlipboardWidget;->c()Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lflipboard/widget/FlipboardWidget$UpdateService;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 236
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "FlipboardWidget:UpdateService:done"

    new-instance v2, Lflipboard/widget/FlipboardWidget$UpdateService$2;

    invoke-direct {v2, p0}, Lflipboard/widget/FlipboardWidget$UpdateService$2;-><init>(Lflipboard/widget/FlipboardWidget$UpdateService;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 247
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 251
    invoke-static {}, Lflipboard/widget/FlipboardWidget;->c()Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 252
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 191
    invoke-static {}, Lflipboard/widget/FlipboardWidget;->c()Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 192
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 256
    invoke-static {}, Lflipboard/widget/FlipboardWidget;->c()Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 257
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 196
    monitor-enter p0

    .line 197
    :try_start_0
    iget v0, p0, Lflipboard/widget/FlipboardWidget$UpdateService;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/widget/FlipboardWidget$UpdateService;->a:I

    .line 198
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    if-nez p1, :cond_0

    .line 200
    invoke-static {}, Lflipboard/widget/FlipboardWidget;->c()Lflipboard/util/Log;

    .line 201
    invoke-virtual {p0}, Lflipboard/widget/FlipboardWidget$UpdateService;->a()V

    .line 231
    :goto_0
    return v8

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 205
    :cond_0
    const-string v0, "id"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 206
    const-string v0, "widget"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 207
    const-string v1, "refresh"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 208
    const-string v1, "keepCurrentItem"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 211
    :try_start_1
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/widget/FlipboardWidget;

    .line 213
    invoke-static {}, Lflipboard/widget/FlipboardWidget;->c()Lflipboard/util/Log;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v1, v6

    const/4 v6, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v1, v6

    const/4 v6, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v1, v6

    const/4 v6, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v1, v6

    const/4 v6, 0x4

    iget v7, p0, Lflipboard/widget/FlipboardWidget$UpdateService;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v1, v6

    const/4 v6, 0x5

    aput-object v0, v1, v6

    .line 215
    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v7, "FlipboardWidget:UpdateService:onStartCommand"

    new-instance v0, Lflipboard/widget/FlipboardWidget$UpdateService$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lflipboard/widget/FlipboardWidget$UpdateService$1;-><init>(Lflipboard/widget/FlipboardWidget$UpdateService;Lflipboard/widget/FlipboardWidget;IZZ)V

    invoke-virtual {v6, v7, v0}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 226
    :catch_0
    move-exception v0

    .line 227
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    .line 228
    invoke-virtual {p0}, Lflipboard/widget/FlipboardWidget$UpdateService;->a()V

    goto :goto_0
.end method
