.class public Lflipboard/widget/FlipboardWidgetManager$AlarmReceiver;
.super Landroid/content/BroadcastReceiver;
.source "FlipboardWidgetManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 386
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 390
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->i()Lflipboard/util/Log;

    new-array v0, v5, [Ljava/lang/Object;

    aput-object p2, v0, v4

    .line 392
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->a()Lflipboard/widget/FlipboardWidgetManager;

    move-result-object v0

    .line 395
    invoke-virtual {v0}, Lflipboard/widget/FlipboardWidgetManager;->b()I

    move-result v1

    if-lez v1, :cond_1

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-wide/32 v2, 0x1499700

    invoke-virtual {v1, v2, v3}, Lflipboard/service/User;->b(J)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 396
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->i()Lflipboard/util/Log;

    new-array v0, v5, [Ljava/lang/Object;

    aput-object p2, v0, v4

    .line 403
    :cond_0
    :goto_0
    return-void

    .line 400
    :cond_1
    invoke-virtual {v0, v4}, Lflipboard/widget/FlipboardWidgetManager;->a(Z)I

    move-result v1

    if-nez v1, :cond_0

    .line 401
    const-wide/16 v2, -0x1

    invoke-virtual {v0, v2, v3, v4}, Lflipboard/widget/FlipboardWidgetManager;->a(JZ)V

    goto :goto_0
.end method
