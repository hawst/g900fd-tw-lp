.class Lflipboard/widget/FlipboardWidget$UpdateService$1;
.super Ljava/lang/Object;
.source "FlipboardWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/widget/FlipboardWidget;

.field final synthetic b:I

.field final synthetic c:Z

.field final synthetic d:Z

.field final synthetic e:Lflipboard/widget/FlipboardWidget$UpdateService;


# direct methods
.method constructor <init>(Lflipboard/widget/FlipboardWidget$UpdateService;Lflipboard/widget/FlipboardWidget;IZZ)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lflipboard/widget/FlipboardWidget$UpdateService$1;->e:Lflipboard/widget/FlipboardWidget$UpdateService;

    iput-object p2, p0, Lflipboard/widget/FlipboardWidget$UpdateService$1;->a:Lflipboard/widget/FlipboardWidget;

    iput p3, p0, Lflipboard/widget/FlipboardWidget$UpdateService$1;->b:I

    iput-boolean p4, p0, Lflipboard/widget/FlipboardWidget$UpdateService$1;->c:Z

    iput-boolean p5, p0, Lflipboard/widget/FlipboardWidget$UpdateService$1;->d:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 26

    .prologue
    .line 218
    move-object/from16 v0, p0

    iget-object v11, v0, Lflipboard/widget/FlipboardWidget$UpdateService$1;->e:Lflipboard/widget/FlipboardWidget$UpdateService;

    .line 220
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lflipboard/widget/FlipboardWidget$UpdateService$1;->a:Lflipboard/widget/FlipboardWidget;

    invoke-static {v11}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v13

    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->a()Lflipboard/widget/FlipboardWidgetManager;

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lflipboard/widget/FlipboardWidget$UpdateService$1;->b:I

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lflipboard/widget/FlipboardWidget$UpdateService$1;->c:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lflipboard/widget/FlipboardWidget$UpdateService$1;->d:Z

    new-instance v17, Lflipboard/io/UsageEvent;

    const-string v4, "appwidget"

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "action"

    const-string v6, "update"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v6}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v4, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x1

    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->d()Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v12, v11, v13, v15}, Lflipboard/widget/FlipboardWidget;->c(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)Landroid/widget/RemoteViews;

    move-result-object v4

    invoke-virtual {v13, v15, v4}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    const-wide/16 v4, 0x0

    iput-wide v4, v14, Lflipboard/widget/FlipboardWidgetManager;->h:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual/range {v17 .. v17}, Lflipboard/io/UsageEvent;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 222
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/widget/FlipboardWidget$UpdateService$1;->e:Lflipboard/widget/FlipboardWidget$UpdateService;

    invoke-virtual {v4}, Lflipboard/widget/FlipboardWidget$UpdateService;->a()V

    .line 223
    return-void

    .line 220
    :cond_0
    :try_start_3
    invoke-virtual {v14}, Lflipboard/widget/FlipboardWidgetManager;->f()Lflipboard/service/Section;

    move-result-object v18

    if-eqz v18, :cond_1

    sget-object v4, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/4 v4, 0x2

    new-array v6, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v18, v6, v4

    const/4 v7, 0x1

    move-object/from16 v0, v18

    iget-object v4, v0, Lflipboard/service/Section;->u:Ljava/util/List;

    if-nez v4, :cond_4

    const/4 v4, 0x0

    :goto_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v7

    :cond_1
    if-eqz v18, :cond_2

    invoke-virtual/range {v18 .. v18}, Lflipboard/service/Section;->l()Z

    move-result v4

    if-nez v4, :cond_6

    :cond_2
    invoke-virtual {v12, v11, v13, v15}, Lflipboard/widget/FlipboardWidget;->c(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)Landroid/widget/RemoteViews;

    move-result-object v4

    invoke-virtual {v13, v15, v4}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    const-wide/16 v4, 0x0

    iput-wide v4, v14, Lflipboard/widget/FlipboardWidgetManager;->h:J

    sget-object v4, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v4}, Lflipboard/io/NetworkManager;->a()Z

    move-result v4

    if-nez v4, :cond_5

    const/4 v4, 0x1

    :goto_2
    iput-boolean v4, v14, Lflipboard/widget/FlipboardWidgetManager;->e:Z

    :cond_3
    :goto_3
    sget-object v4, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-virtual/range {v17 .. v17}, Lflipboard/io/UsageEvent;->a()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 222
    :catchall_0
    move-exception v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lflipboard/widget/FlipboardWidget$UpdateService$1;->e:Lflipboard/widget/FlipboardWidget$UpdateService;

    invoke-virtual {v5}, Lflipboard/widget/FlipboardWidget$UpdateService;->a()V

    throw v4

    .line 220
    :cond_4
    :try_start_5
    move-object/from16 v0, v18

    iget-object v4, v0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    goto :goto_1

    :cond_5
    const/4 v4, 0x0

    goto :goto_2

    :cond_6
    iget-object v4, v14, Lflipboard/widget/FlipboardWidgetManager;->i:Ljava/util/HashMap;

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;

    if-eqz v4, :cond_7

    if-nez v5, :cond_8

    :cond_7
    if-eqz v16, :cond_1a

    const-wide/16 v4, -0x1

    :goto_4
    move-object/from16 v0, v18

    invoke-virtual {v14, v0, v4, v5, v15}, Lflipboard/widget/FlipboardWidgetManager;->a(Lflipboard/service/Section;JI)Lflipboard/widget/FlipboardWidgetManager$WidgetItem;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v4

    :cond_8
    if-eqz v4, :cond_43

    const/4 v5, 0x0

    :try_start_6
    invoke-virtual {v12, v11, v13, v15}, Lflipboard/widget/FlipboardWidget;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)Landroid/widget/RemoteViews;
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v5

    move-object v10, v5

    :goto_5
    if-eqz v10, :cond_19

    if-eqz v4, :cond_19

    if-eqz v16, :cond_9

    const v5, 0x7f0a006f

    const/4 v6, 0x4

    :try_start_7
    invoke-virtual {v10, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v5, 0x7f0a0070

    const/4 v6, 0x0

    invoke-virtual {v10, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {v13, v15, v10}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget(ILandroid/widget/RemoteViews;)V

    :cond_9
    if-eqz v16, :cond_a

    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v5, v5, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-wide/32 v6, 0x927c0

    invoke-virtual {v5, v6, v7}, Lflipboard/service/User;->b(J)Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, v14, Lflipboard/widget/FlipboardWidgetManager;->g:J

    :cond_a
    iget-object v5, v4, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a:Lflipboard/objs/FeedItem;

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Lflipboard/io/UsageEvent;->a(Lflipboard/objs/FeedItem;)V

    if-eqz v16, :cond_1b

    const-wide/32 v6, 0x15f90

    :goto_6
    if-eqz v11, :cond_17

    iget-object v9, v4, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a:Lflipboard/objs/FeedItem;

    iget-object v0, v4, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->b:Lflipboard/objs/FeedItem;

    move-object/from16 v19, v0

    sget-object v5, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/4 v5, 0x1

    new-array v8, v5, [Ljava/lang/Object;

    const/16 v20, 0x0

    if-eqz v9, :cond_1c

    iget-object v5, v9, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v5, :cond_1c

    iget-object v5, v9, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    :goto_7
    aput-object v5, v8, v20

    invoke-virtual {v4, v6, v7}, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a(J)Z

    move-result v5

    if-nez v5, :cond_b

    sget-object v5, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/4 v5, 0x1

    new-array v6, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    if-eqz v9, :cond_1d

    move-object v5, v9

    :goto_8
    aput-object v5, v6, v7

    :cond_b
    iget-object v5, v4, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->c:Ljava/lang/String;

    invoke-static {v5}, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->b(Ljava/lang/String;)Lflipboard/io/BitmapManager$Handle;

    move-result-object v5

    iget-object v6, v4, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->d:Ljava/lang/String;

    invoke-static {v6}, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->b(Ljava/lang/String;)Lflipboard/io/BitmapManager$Handle;

    move-result-object v20

    invoke-virtual {v12}, Lflipboard/widget/FlipboardWidget;->a()Z

    move-result v6

    if-nez v6, :cond_d

    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v6, v6, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v6}, Lflipboard/service/User;->p()Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_1e

    invoke-virtual {v6}, Lflipboard/service/User;->p()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1e

    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v6

    if-nez v6, :cond_1e

    new-instance v6, Landroid/content/Intent;

    const-class v7, Lflipboard/activities/ShareActivity;

    invoke-direct {v6, v11, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v7, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v7, v7, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v7, v7, Lflipboard/service/User;->h:Lflipboard/service/Section;

    invoke-static {v9, v7}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;)Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "flipboard.extra.reference"

    const-string v21, "flipboard.extra.reference"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v6, v8, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v8, "flipboard.extra.reference.author"

    const-string v21, "flipboard.extra.reference.author"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v6, v8, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v8, "flipboard.extra.reference.link"

    const-string v21, "flipboard.extra.reference.link"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v6, v8, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v8, "flipboard.extra.reference.service"

    const-string v21, "flipboard.extra.reference.service"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v6, v8, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v8, "flipboard.extra.navigating.from"

    sget-object v21, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->c:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    invoke-virtual/range {v21 .. v21}, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v6, v8, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v8, "flipboard.extra.reference.title"

    const-string v21, "flipboard.extra.reference.title"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz v18, :cond_c

    const-string v7, "extra_section_id"

    invoke-virtual/range {v18 .. v18}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_c
    const-string v7, "flipboard.extra.magazine.share.item.section.id"

    iget-object v8, v9, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v7, "flipboard.extra.magazine.share.item.partner.id"

    iget-object v8, v9, Lflipboard/objs/FeedItem;->bE:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const v7, 0x7f0a006e

    const/4 v8, 0x0

    invoke-virtual {v10, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v7, 0x7f0a006e

    const/4 v8, 0x0

    const/high16 v21, 0x8000000

    move/from16 v0, v21

    invoke-static {v11, v8, v6, v0}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v10, v7, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :cond_d
    :goto_9
    const v7, 0x7f0a004f

    :try_start_8
    iget-object v6, v9, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-nez v6, :cond_1f

    const-string v6, ""

    :goto_a
    invoke-virtual {v10, v7, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-static {v13, v15}, Lflipboard/widget/FlipboardWidget;->a(Landroid/appwidget/AppWidgetManager;I)Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    move-result-object v21

    sget-object v6, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->d:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    move-object/from16 v0, v21

    if-ne v0, v6, :cond_e

    const v6, 0x7f0a0046

    move-object/from16 v0, v18

    invoke-static {v0, v11}, Lflipboard/widget/FlipboardWidget;->a(Lflipboard/service/Section;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v10, v6, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :cond_e
    invoke-virtual {v12}, Lflipboard/widget/FlipboardWidget;->a()Z

    move-result v6

    if-eqz v6, :cond_20

    const-string v6, "small"

    move-object v8, v6

    :goto_b
    if-eqz v5, :cond_34

    const/4 v6, 0x0

    invoke-virtual {v9}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v7

    if-eqz v7, :cond_f

    invoke-virtual {v7}, Lflipboard/objs/Image;->e()Landroid/graphics/PointF;

    move-result-object v6

    :cond_f
    if-eqz v6, :cond_10

    sget-object v7, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/16 v22, 0x0

    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v23

    aput-object v23, v7, v22

    const/16 v22, 0x1

    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v23

    aput-object v23, v7, v22

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v7

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v22

    move/from16 v0, v22

    if-le v7, v0, :cond_22

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v7

    const/16 v22, 0x190

    move/from16 v0, v22

    if-le v7, v0, :cond_22

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v7

    int-to-float v7, v7

    iget v6, v6, Landroid/graphics/PointF;->x:F

    mul-float/2addr v6, v7

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    sub-float/2addr v6, v7

    float-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v22

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v23

    sub-int v22, v22, v23

    move/from16 v0, v22

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    new-instance v7, Landroid/graphics/Rect;

    const/16 v22, 0x0

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v23

    add-int v23, v23, v6

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v24

    move/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v7, v6, v0, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    const/high16 v22, 0x3f800000    # 1.0f

    move/from16 v0, v22

    invoke-virtual {v5, v7, v0}, Lflipboard/io/BitmapManager$Handle;->a(Landroid/graphics/Rect;F)Lflipboard/io/BitmapManager$Handle;

    move-result-object v5

    sget-object v7, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/16 v22, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v7, v22

    const/4 v6, 0x1

    aput-object v5, v7, v6

    :cond_10
    :goto_c
    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->b()Landroid/graphics/Bitmap;

    move-result-object v6

    if-eqz v6, :cond_23

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v6

    invoke-static {v6}, Lflipboard/util/AndroidUtil;->a(Landroid/graphics/Bitmap$Config;)I

    move-result v6

    move v7, v6

    :goto_d
    const/16 v6, 0x140

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v22

    move-object/from16 v0, v22

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v6

    const/16 v22, 0x1e0

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v23

    move-object/from16 v0, v23

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v23, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->min(II)I

    move-result v22

    mul-int v6, v6, v22

    mul-int/lit8 v6, v6, 0x4

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v22

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v23

    mul-int v22, v22, v23

    mul-int v22, v22, v7

    move/from16 v0, v22

    if-le v0, v6, :cond_24

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v22

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v23

    mul-int v22, v22, v23

    mul-int v22, v22, v7

    move/from16 v0, v22

    int-to-double v0, v0

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v22

    int-to-double v0, v6

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v24

    div-double v22, v22, v24

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v6

    int-to-double v0, v6

    move-wide/from16 v24, v0

    div-double v24, v24, v22

    move-wide/from16 v0, v24

    double-to-int v6, v0

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v24

    move/from16 v0, v24

    int-to-double v0, v0

    move-wide/from16 v24, v0

    div-double v22, v24, v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    sget-object v23, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/16 v23, 0x5

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v5, v23, v24

    const/16 v24, 0x1

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x2

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x4

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v5, v6, v0, v1}, Lflipboard/io/BitmapManager$Handle;->a(IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    :goto_e
    if-eqz v6, :cond_31

    sget-object v22, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/16 v22, 0x4

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v22, v23

    const/4 v7, 0x3

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v23

    aput-object v23, v22, v7

    const v7, 0x7f0a0040

    invoke-virtual {v10, v7, v6}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->m()F

    move-result v6

    sget-object v7, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/16 v22, 0x0

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v23

    aput-object v23, v7, v22

    const v7, 0x3f333333    # 0.7f

    cmpl-float v6, v6, v7

    if-lez v6, :cond_2a

    sget-boolean v6, Lflipboard/service/FlipboardManager;->n:Z

    if-eqz v6, :cond_27

    invoke-virtual {v12}, Lflipboard/widget/FlipboardWidget;->a()Z

    move-result v6

    if-eqz v6, :cond_25

    const v6, 0x7f020034

    :goto_f
    const v7, 0x7f0a0066

    invoke-virtual {v10, v7, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    invoke-static {v11}, Lflipboard/activities/LaunchActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v21, "launch"

    move-object/from16 v0, v21

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v7, "appWidgetIds"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [I

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput v15, v21, v22

    move-object/from16 v0, v21

    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    const-string v7, "launch_from"

    const-string v21, "widget"

    move-object/from16 v0, v21

    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v7, Lflipboard/widget/FlipboardWidget;->b:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v7, Lflipboard/widget/FlipboardWidget;->c:Ljava/lang/String;

    const-string v21, "logo"

    move-object/from16 v0, v21

    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v7, 0x20000

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/high16 v7, 0x4000000

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    sget-object v7, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v7, v7, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v7, :cond_11

    const-string v7, "page"

    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_11
    const v7, 0x7f0a0066

    const/16 v21, 0x0

    const/16 v22, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-static {v11, v0, v6, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v10, v7, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v6}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v6

    iget-boolean v6, v6, Lflipboard/model/ConfigSetting;->WidgetLogoHintEnabled:Z

    if-eqz v6, :cond_12

    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v6, v6, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v7, "show_widget_logo_hint"

    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-interface {v6, v7, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_30

    const/4 v6, 0x0

    :goto_10
    const v7, 0x7f0a0069

    invoke-virtual {v10, v7, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :cond_12
    :goto_11
    if-eqz v20, :cond_39

    invoke-virtual/range {v20 .. v20}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v6

    invoke-virtual/range {v20 .. v20}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v7

    mul-int/2addr v6, v7

    const/16 v7, 0x1000

    if-le v6, v7, :cond_37

    invoke-virtual/range {v20 .. v20}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v6

    invoke-virtual/range {v20 .. v20}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x42800000    # 64.0f

    div-float/2addr v6, v7

    invoke-virtual/range {v20 .. v20}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v6

    float-to-int v7, v7

    invoke-virtual/range {v20 .. v20}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v6, v21, v6

    float-to-int v6, v6

    sget-object v21, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/16 v21, 0x5

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v20, v21, v22

    const/16 v22, 0x1

    invoke-virtual/range {v20 .. v20}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x2

    invoke-virtual/range {v20 .. v20}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v7, v6, v1}, Lflipboard/io/BitmapManager$Handle;->a(IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    :goto_12
    if-eqz v6, :cond_38

    const v7, 0x7f0a006b

    invoke-virtual {v10, v7, v6}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    :goto_13
    iget-object v6, v9, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    if-eqz v6, :cond_3c

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, v9, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v9, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    if-eqz v6, :cond_14

    const-string v6, ", "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v9, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-static {v6}, Lflipboard/util/HttpUtil;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v21, "www."

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_13

    const/16 v21, 0x4

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    :cond_13
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_14
    const v6, 0x7f0a006c

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v10, v6, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    if-eqz v19, :cond_3b

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    iget-object v7, v0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    if-eqz v7, :cond_3a

    const v7, 0x7f0d02ef

    invoke-virtual {v11, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, v19

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    move-object/from16 v23, v0

    aput-object v23, v21, v22

    move-object/from16 v0, v21

    invoke-static {v7, v0}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_15
    :goto_14
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_16

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_16
    move-object/from16 v0, v19

    iget-wide v0, v0, Lflipboard/objs/FeedItem;->Z:J

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x3e8

    mul-long v22, v22, v24

    invoke-static/range {v22 .. v23}, Lflipboard/widget/FlipboardWidget;->a(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v7, 0x7f0a006d

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10, v7, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :goto_15
    new-instance v6, Landroid/content/Intent;

    const-class v7, Lflipboard/activities/DetailActivity;

    invoke-direct {v6, v11, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v19, "launch"

    move-object/from16 v0, v19

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v7, "appWidgetIds"

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [I

    move-object/from16 v19, v0

    const/16 v21, 0x0

    aput v15, v19, v21

    move-object/from16 v0, v19

    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    const-string v7, "extra_opened_from_widget"

    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v7, "sid"

    invoke-virtual/range {v18 .. v18}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v7, "extra_current_item"

    iget-object v9, v9, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v6, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v7, Lflipboard/widget/FlipboardWidget;->b:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v7, Lflipboard/widget/FlipboardWidget;->c:Ljava/lang/String;

    const-string v8, "main"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const v7, 0x7f0a0064

    const/4 v8, 0x0

    const/high16 v9, 0x10000000

    invoke-static {v11, v8, v6, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v10, v7, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    new-instance v6, Landroid/content/Intent;

    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-direct {v6, v11, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v7, "appWidgetIds"

    const/4 v8, 0x1

    new-array v8, v8, [I

    const/4 v9, 0x0

    aput v15, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v7, "refresh"

    const-string v8, "true"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const v7, 0x7f0a006f

    const/4 v8, 0x0

    const/high16 v9, 0x8000000

    invoke-static {v11, v8, v6, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v10, v7, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v6, v6, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    new-instance v7, Lflipboard/widget/FlipboardWidget$1;

    move-object/from16 v0, v20

    invoke-direct {v7, v12, v5, v0}, Lflipboard/widget/FlipboardWidget$1;-><init>(Lflipboard/widget/FlipboardWidget;Lflipboard/io/BitmapManager$Handle;Lflipboard/io/BitmapManager$Handle;)V

    const-wide/16 v8, 0x2710

    invoke-virtual {v6, v7, v8, v9}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    :cond_17
    if-eqz v16, :cond_18

    const v5, 0x7f0a006f

    const/4 v6, 0x0

    invoke-virtual {v10, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v5, 0x7f0a0070

    const/4 v6, 0x4

    invoke-virtual {v10, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :cond_18
    invoke-virtual {v13, v15, v10}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    :cond_19
    :goto_16
    if-nez v4, :cond_3

    sget-object v4, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    invoke-virtual {v12, v11, v13, v15}, Lflipboard/widget/FlipboardWidget;->c(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)Landroid/widget/RemoteViews;

    move-result-object v4

    invoke-virtual {v13, v15, v4}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    const-wide/16 v4, 0x0

    iput-wide v4, v14, Lflipboard/widget/FlipboardWidgetManager;->h:J

    sget-object v4, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v4}, Lflipboard/io/NetworkManager;->a()Z

    move-result v4

    if-nez v4, :cond_44

    const/4 v4, 0x1

    :goto_17
    iput-boolean v4, v14, Lflipboard/widget/FlipboardWidgetManager;->e:Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto/16 :goto_3

    :catchall_1
    move-exception v4

    :try_start_a
    invoke-virtual/range {v17 .. v17}, Lflipboard/io/UsageEvent;->a()V

    throw v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_1a
    const-wide/32 v4, 0xea60

    goto/16 :goto_4

    :catch_0
    move-exception v4

    :try_start_b
    sget-object v6, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v6, v4}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    const/4 v4, 0x0

    move-object v10, v5

    goto/16 :goto_5

    :cond_1b
    const-wide/16 v6, 0x7530

    goto/16 :goto_6

    :cond_1c
    const-string v5, "null"

    goto/16 :goto_7

    :cond_1d
    const-string v5, "null"

    goto/16 :goto_8

    :cond_1e
    const v6, 0x7f0a006e

    const/16 v7, 0x8

    invoke-virtual {v10, v6, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto/16 :goto_9

    :cond_1f
    :try_start_c
    iget-object v6, v9, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    goto/16 :goto_a

    :cond_20
    sget-object v6, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->d:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    move-object/from16 v0, v21

    if-ne v0, v6, :cond_21

    const-string v6, "large"

    move-object v8, v6

    goto/16 :goto_b

    :cond_21
    const-string v6, "medium"

    move-object v8, v6

    goto/16 :goto_b

    :cond_22
    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v7

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v22

    move/from16 v0, v22

    if-ge v7, v0, :cond_10

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v7

    const/16 v22, 0x190

    move/from16 v0, v22

    if-le v7, v0, :cond_10

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v7

    int-to-float v7, v7

    iget v6, v6, Landroid/graphics/PointF;->y:F

    mul-float/2addr v6, v7

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    sub-float/2addr v6, v7

    float-to-int v6, v6

    const/4 v7, 0x0

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v22

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v23

    sub-int v22, v22, v23

    move/from16 v0, v22

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    new-instance v7, Landroid/graphics/Rect;

    const/16 v22, 0x0

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v23

    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v24

    add-int v24, v24, v6

    move/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v7, v0, v6, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    const/high16 v22, 0x3f800000    # 1.0f

    move/from16 v0, v22

    invoke-virtual {v5, v7, v0}, Lflipboard/io/BitmapManager$Handle;->a(Landroid/graphics/Rect;F)Lflipboard/io/BitmapManager$Handle;

    move-result-object v5

    sget-object v7, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/16 v22, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v7, v22

    const/4 v6, 0x1

    aput-object v5, v7, v6
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto/16 :goto_c

    :catchall_2
    move-exception v4

    :try_start_d
    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v6, v6, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    new-instance v7, Lflipboard/widget/FlipboardWidget$1;

    move-object/from16 v0, v20

    invoke-direct {v7, v12, v5, v0}, Lflipboard/widget/FlipboardWidget$1;-><init>(Lflipboard/widget/FlipboardWidget;Lflipboard/io/BitmapManager$Handle;Lflipboard/io/BitmapManager$Handle;)V

    const-wide/16 v8, 0x2710

    invoke-virtual {v6, v7, v8, v9}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    throw v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    :cond_23
    const/4 v6, 0x4

    move v7, v6

    goto/16 :goto_d

    :cond_24
    :try_start_e
    invoke-virtual {v5}, Lflipboard/io/BitmapManager$Handle;->l()Landroid/graphics/Bitmap;

    move-result-object v6

    goto/16 :goto_e

    :cond_25
    sget-object v6, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->d:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    move-object/from16 v0, v21

    if-ne v0, v6, :cond_26

    const v6, 0x7f020024

    goto/16 :goto_f

    :cond_26
    const v6, 0x7f02002c

    goto/16 :goto_f

    :cond_27
    invoke-virtual {v12}, Lflipboard/widget/FlipboardWidget;->a()Z

    move-result v6

    if-eqz v6, :cond_28

    const v6, 0x7f020032

    goto/16 :goto_f

    :cond_28
    sget-object v6, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->d:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    move-object/from16 v0, v21

    if-ne v0, v6, :cond_29

    const v6, 0x7f020022

    goto/16 :goto_f

    :cond_29
    const v6, 0x7f02002a

    goto/16 :goto_f

    :cond_2a
    sget-boolean v6, Lflipboard/service/FlipboardManager;->n:Z

    if-eqz v6, :cond_2d

    invoke-virtual {v12}, Lflipboard/widget/FlipboardWidget;->a()Z

    move-result v6

    if-eqz v6, :cond_2b

    const v6, 0x7f020035

    goto/16 :goto_f

    :cond_2b
    sget-object v6, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->d:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    move-object/from16 v0, v21

    if-ne v0, v6, :cond_2c

    const v6, 0x7f020025

    goto/16 :goto_f

    :cond_2c
    const v6, 0x7f02002d

    goto/16 :goto_f

    :cond_2d
    invoke-virtual {v12}, Lflipboard/widget/FlipboardWidget;->a()Z

    move-result v6

    if-eqz v6, :cond_2e

    const v6, 0x7f020033

    goto/16 :goto_f

    :cond_2e
    sget-object v6, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->d:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    move-object/from16 v0, v21

    if-ne v0, v6, :cond_2f

    const v6, 0x7f020023

    goto/16 :goto_f

    :cond_2f
    const v6, 0x7f02002b

    goto/16 :goto_f

    :cond_30
    const/16 v6, 0x8

    goto/16 :goto_10

    :cond_31
    sget-object v6, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    invoke-virtual {v12}, Lflipboard/widget/FlipboardWidget;->a()Z

    move-result v6

    if-eqz v6, :cond_32

    const v6, 0x7f020030

    :goto_18
    const v7, 0x7f0a0066

    invoke-virtual {v10, v7, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    const v6, 0x7f0a0040

    const v7, 0x7f02001d

    invoke-virtual {v10, v6, v7}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    const-wide/16 v6, 0x0

    iput-wide v6, v14, Lflipboard/widget/FlipboardWidgetManager;->h:J

    goto/16 :goto_11

    :cond_32
    sget-object v6, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->d:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    move-object/from16 v0, v21

    if-ne v0, v6, :cond_33

    const v6, 0x7f020020

    goto :goto_18

    :cond_33
    const v6, 0x7f020028

    goto :goto_18

    :cond_34
    sget-object v6, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    invoke-virtual {v12}, Lflipboard/widget/FlipboardWidget;->a()Z

    move-result v6

    if-eqz v6, :cond_35

    const v6, 0x7f020031

    :goto_19
    const v7, 0x7f0a0066

    invoke-virtual {v10, v7, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    const v6, 0x7f0a0040

    const v7, 0x7f02001d

    invoke-virtual {v10, v6, v7}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    const-wide/16 v6, 0x0

    iput-wide v6, v14, Lflipboard/widget/FlipboardWidgetManager;->h:J

    goto/16 :goto_11

    :cond_35
    sget-object v6, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->d:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    move-object/from16 v0, v21

    if-ne v0, v6, :cond_36

    const v6, 0x7f020021

    goto :goto_19

    :cond_36
    const v6, 0x7f020029

    goto :goto_19

    :cond_37
    invoke-virtual/range {v20 .. v20}, Lflipboard/io/BitmapManager$Handle;->l()Landroid/graphics/Bitmap;

    move-result-object v6

    goto/16 :goto_12

    :cond_38
    const v6, 0x7f0a006b

    const v7, 0x7f020167

    invoke-virtual {v10, v6, v7}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_13

    :cond_39
    const v6, 0x7f0a006b

    const v7, 0x7f020167

    invoke-virtual {v10, v6, v7}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_13

    :cond_3a
    move-object/from16 v0, v19

    iget-object v7, v0, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    if-eqz v7, :cond_15

    const v7, 0x7f0d02ef

    invoke-virtual {v11, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, v19

    iget-object v0, v0, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    move-object/from16 v23, v0

    aput-object v23, v21, v22

    move-object/from16 v0, v21

    invoke-static {v7, v0}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_14

    :cond_3b
    const v6, 0x7f0a006d

    iget-wide v0, v9, Lflipboard/objs/FeedItem;->Z:J

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x3e8

    mul-long v22, v22, v24

    invoke-static/range {v22 .. v23}, Lflipboard/widget/FlipboardWidget;->a(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v10, v6, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_15

    :cond_3c
    if-eqz v19, :cond_42

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    iget-object v7, v0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    if-eqz v7, :cond_3f

    move-object/from16 v0, v19

    iget-object v7, v0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3d
    :goto_1a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_3e

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3e
    move-object/from16 v0, v19

    iget-wide v0, v0, Lflipboard/objs/FeedItem;->Z:J

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x3e8

    mul-long v22, v22, v24

    invoke-static/range {v22 .. v23}, Lflipboard/widget/FlipboardWidget;->a(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v7, 0x7f0a006d

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10, v7, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget-object v6, v9, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v6, :cond_40

    iget-object v6, v9, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    move-object/from16 v0, v19

    iget-object v7, v0, Lflipboard/objs/FeedItem;->z:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_40

    invoke-virtual {v12}, Lflipboard/widget/FlipboardWidget;->b()Z

    move-result v6

    if-eqz v6, :cond_40

    const v6, 0x7f0a004f

    const-string v7, ""

    invoke-virtual {v10, v6, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v6, 0x7f0a006c

    iget-object v7, v9, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v10, v6, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_15

    :cond_3f
    move-object/from16 v0, v19

    iget-object v7, v0, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    if-eqz v7, :cond_3d

    move-object/from16 v0, v19

    iget-object v7, v0, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1a

    :cond_40
    move-object/from16 v0, v19

    iget-object v6, v0, Lflipboard/objs/FeedItem;->z:Ljava/lang/String;

    if-eqz v6, :cond_41

    const v6, 0x7f0a006c

    move-object/from16 v0, v19

    iget-object v7, v0, Lflipboard/objs/FeedItem;->z:Ljava/lang/String;

    invoke-virtual {v10, v6, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_15

    :cond_41
    const v6, 0x7f0a006c

    const-string v7, ""

    invoke-virtual {v10, v6, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_15

    :cond_42
    const v6, 0x7f0a006c

    const-string v7, ""

    invoke-virtual {v10, v6, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    goto/16 :goto_15

    :cond_43
    :try_start_f
    sget-object v5, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sget-object v7, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v7}, Lflipboard/io/NetworkManager;->a()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto/16 :goto_16

    :cond_44
    const/4 v4, 0x0

    goto/16 :goto_17
.end method
