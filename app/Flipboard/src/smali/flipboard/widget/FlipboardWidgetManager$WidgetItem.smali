.class Lflipboard/widget/FlipboardWidgetManager$WidgetItem;
.super Lflipboard/io/Download$Observer;
.source "FlipboardWidgetManager.java"


# instance fields
.field final a:Lflipboard/objs/FeedItem;

.field final b:Lflipboard/objs/FeedItem;

.field final c:Ljava/lang/String;

.field final d:Ljava/lang/String;

.field g:I


# direct methods
.method constructor <init>(Lflipboard/objs/FeedItem;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 421
    invoke-direct {p0}, Lflipboard/io/Download$Observer;-><init>()V

    .line 422
    iput-object p1, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a:Lflipboard/objs/FeedItem;

    .line 423
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p1, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    :goto_0
    iput-object v0, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->b:Lflipboard/objs/FeedItem;

    .line 425
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->f()Ljava/lang/String;

    move-result-object v0

    .line 426
    if-eqz v0, :cond_1

    .line 427
    iput-object v0, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->c:Ljava/lang/String;

    .line 428
    iget v0, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->g:I

    .line 433
    :goto_1
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v0, :cond_2

    .line 434
    iget-object v0, p1, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->d:Ljava/lang/String;

    .line 435
    iget v0, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->g:I

    .line 442
    :goto_2
    return-void

    :cond_0
    move-object v0, v1

    .line 423
    goto :goto_0

    .line 430
    :cond_1
    iput-object v1, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->c:Ljava/lang/String;

    goto :goto_1

    .line 436
    :cond_2
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->b:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v0, :cond_3

    .line 437
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->d:Ljava/lang/String;

    .line 438
    iget v0, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->g:I

    goto :goto_2

    .line 440
    :cond_3
    iput-object v1, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->d:Ljava/lang/String;

    goto :goto_2
.end method

.method private declared-synchronized a(Lflipboard/io/Download;Lflipboard/io/Download$Status;)V
    .locals 3

    .prologue
    .line 509
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->i()Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lflipboard/io/Download;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 510
    invoke-virtual {p2}, Lflipboard/io/Download$Status;->ordinal()I

    move-result v0

    sget-object v1, Lflipboard/io/Download$Status;->d:Lflipboard/io/Download$Status;

    invoke-virtual {v1}, Lflipboard/io/Download$Status;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 511
    iget v0, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->g:I

    .line 512
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 514
    :cond_0
    monitor-exit p0

    return-void

    .line 509
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 469
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->e()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 470
    :cond_0
    sget-object v0, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    invoke-virtual {v0, p0, v1, v1}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;ZZ)Lflipboard/io/Download;

    move-result-object v1

    .line 473
    :try_start_0
    invoke-virtual {v1}, Lflipboard/io/Download;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 475
    invoke-virtual {v1}, Lflipboard/io/Download;->b()V

    .line 478
    :goto_0
    return v0

    .line 475
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lflipboard/io/Download;->b()V

    throw v0

    .line 478
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static b(Ljava/lang/String;)Lflipboard/io/BitmapManager$Handle;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 494
    if-eqz p0, :cond_1

    .line 495
    sget-object v0, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    invoke-virtual {v0, p0, v2, v2}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;ZZ)Lflipboard/io/Download;

    move-result-object v0

    .line 496
    invoke-virtual {v0}, Lflipboard/io/Download;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 497
    sget-object v1, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    invoke-virtual {v1, v0, v2}, Lflipboard/io/BitmapManager;->a(Lflipboard/io/Download;Z)Lflipboard/io/BitmapManager$Handle;

    move-result-object v0

    .line 501
    :goto_0
    return-object v0

    .line 499
    :cond_0
    invoke-virtual {v0}, Lflipboard/io/Download;->b()V

    .line 501
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 411
    check-cast p1, Lflipboard/io/Download;

    check-cast p2, Lflipboard/io/Download$Status;

    invoke-direct {p0, p1, p2}, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a(Lflipboard/io/Download;Lflipboard/io/Download$Status;)V

    return-void
.end method

.method final declared-synchronized a(J)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 522
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->i()Lflipboard/util/Log;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a:Lflipboard/objs/FeedItem;

    iget-object v5, v5, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->g:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    .line 523
    iget v3, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_0

    move v0, v1

    .line 558
    :goto_0
    monitor-exit p0

    return v0

    .line 526
    :cond_0
    const/16 v3, 0x32

    :try_start_1
    invoke-virtual {p0, v3}, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a(I)V

    .line 530
    iget-object v3, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->c:Ljava/lang/String;

    if-eqz v3, :cond_c

    .line 531
    sget-object v3, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    iget-object v4, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->c:Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;ZZ)Lflipboard/io/Download;

    move-result-object v3

    .line 532
    invoke-virtual {v3, p0}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Observer;)V

    .line 534
    :goto_1
    iget-object v4, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->d:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 535
    sget-object v2, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    iget-object v4, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->d:Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v6}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;ZZ)Lflipboard/io/Download;

    move-result-object v2

    .line 536
    invoke-virtual {v2, p0}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Observer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 539
    :cond_1
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    add-long/2addr v4, p1

    .line 540
    :goto_2
    iget v6, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->g:I

    if-lez v6, :cond_4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v6, v6, v4

    if-gez v6, :cond_4

    sget-object v6, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v6}, Lflipboard/io/NetworkManager;->a()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 541
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->i()Lflipboard/util/Log;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a:Lflipboard/objs/FeedItem;

    iget-object v8, v8, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget v8, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->g:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    .line 542
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->i()Lflipboard/util/Log;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    .line 543
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->i()Lflipboard/util/Log;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    .line 540
    const-wide/16 v6, 0x3e8

    invoke-virtual {p0, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    .line 546
    :catch_0
    move-exception v1

    .line 547
    :try_start_3
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->i()Lflipboard/util/Log;

    move-result-object v4

    invoke-virtual {v4, v1}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 548
    if-eqz v3, :cond_2

    .line 551
    :try_start_4
    invoke-virtual {v3, p0}, Lflipboard/io/Download;->b(Lflipboard/io/Download$Observer;)V

    .line 552
    invoke-virtual {v3}, Lflipboard/io/Download;->b()V

    .line 554
    :cond_2
    if-eqz v2, :cond_3

    .line 555
    invoke-virtual {v2, p0}, Lflipboard/io/Download;->b(Lflipboard/io/Download$Observer;)V

    .line 556
    invoke-virtual {v2}, Lflipboard/io/Download;->b()V

    .line 558
    :cond_3
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->i()Lflipboard/util/Log;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 522
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 545
    :cond_4
    if-eqz v3, :cond_5

    :try_start_5
    invoke-virtual {v3}, Lflipboard/io/Download;->a()Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_5
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lflipboard/io/Download;->a()Z
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v4

    if-eqz v4, :cond_7

    :cond_6
    move v0, v1

    .line 550
    :cond_7
    if-eqz v3, :cond_8

    .line 551
    :try_start_6
    invoke-virtual {v3, p0}, Lflipboard/io/Download;->b(Lflipboard/io/Download$Observer;)V

    .line 552
    invoke-virtual {v3}, Lflipboard/io/Download;->b()V

    .line 554
    :cond_8
    if-eqz v2, :cond_9

    .line 555
    invoke-virtual {v2, p0}, Lflipboard/io/Download;->b(Lflipboard/io/Download$Observer;)V

    .line 556
    invoke-virtual {v2}, Lflipboard/io/Download;->b()V

    .line 558
    :cond_9
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->i()Lflipboard/util/Log;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    goto/16 :goto_0

    .line 550
    :catchall_1
    move-exception v0

    if-eqz v3, :cond_a

    .line 551
    invoke-virtual {v3, p0}, Lflipboard/io/Download;->b(Lflipboard/io/Download$Observer;)V

    .line 552
    invoke-virtual {v3}, Lflipboard/io/Download;->b()V

    .line 554
    :cond_a
    if-eqz v2, :cond_b

    .line 555
    invoke-virtual {v2, p0}, Lflipboard/io/Download;->b(Lflipboard/io/Download$Observer;)V

    .line 556
    invoke-virtual {v2}, Lflipboard/io/Download;->b()V

    .line 558
    :cond_b
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->i()Lflipboard/util/Log;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_c
    move-object v3, v2

    goto/16 :goto_1
.end method
