.class public Lflipboard/widget/FlipboardWidgetManager;
.super Ljava/lang/Object;
.source "FlipboardWidgetManager.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/Section;",
        "Lflipboard/service/Section$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field static a:Lflipboard/util/Log;

.field static b:Lflipboard/widget/FlipboardWidgetManager;


# instance fields
.field final c:Landroid/content/Context;

.field d:Landroid/content/SharedPreferences;

.field e:Z

.field f:Z

.field g:J

.field h:J

.field i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lflipboard/widget/FlipboardWidgetManager$WidgetItem;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lflipboard/service/Section;

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-string v0, "widget"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/widget/FlipboardWidgetManager;->a:Lflipboard/util/Log;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput v4, p0, Lflipboard/widget/FlipboardWidgetManager;->m:I

    .line 62
    iput-object p1, p0, Lflipboard/widget/FlipboardWidgetManager;->c:Landroid/content/Context;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->j:Ljava/util/List;

    .line 64
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->k:Ljava/util/Set;

    .line 65
    const-string v0, "widget_state"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->d:Landroid/content/SharedPreferences;

    .line 66
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0x112a880

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lflipboard/widget/FlipboardWidgetManager;->g:J

    .line 67
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->d:Landroid/content/SharedPreferences;

    const-string v1, "widget_count"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lflipboard/widget/FlipboardWidgetManager;->m:I

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->i:Ljava/util/HashMap;

    .line 71
    invoke-direct {p0}, Lflipboard/widget/FlipboardWidgetManager;->j()V

    .line 74
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->h:Lflipboard/service/Section;

    iput-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->l:Lflipboard/service/Section;

    .line 75
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->l:Lflipboard/service/Section;

    invoke-virtual {v0, p0}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    .line 77
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/widget/FlipboardWidgetManager$1;

    invoke-direct {v1, p0}, Lflipboard/widget/FlipboardWidgetManager$1;-><init>(Lflipboard/widget/FlipboardWidgetManager;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/util/Observer;)V

    .line 104
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    new-instance v1, Lflipboard/widget/FlipboardWidgetManager$2;

    invoke-direct {v1, p0}, Lflipboard/widget/FlipboardWidgetManager$2;-><init>(Lflipboard/widget/FlipboardWidgetManager;)V

    invoke-virtual {v0, v1}, Lflipboard/io/NetworkManager;->a(Lflipboard/util/Observer;)V

    .line 114
    invoke-virtual {p0}, Lflipboard/widget/FlipboardWidgetManager;->b()I

    move-result v0

    if-lez v0, :cond_0

    .line 115
    invoke-virtual {p0}, Lflipboard/widget/FlipboardWidgetManager;->h()V

    .line 117
    :cond_0
    return-void
.end method

.method static synthetic a(Lflipboard/widget/FlipboardWidgetManager;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->l:Lflipboard/service/Section;

    return-object v0
.end method

.method static synthetic a(Lflipboard/widget/FlipboardWidgetManager;Lflipboard/service/Section;)Lflipboard/service/Section;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lflipboard/widget/FlipboardWidgetManager;->l:Lflipboard/service/Section;

    return-object p1
.end method

.method public static declared-synchronized a()Lflipboard/widget/FlipboardWidgetManager;
    .locals 3

    .prologue
    .line 42
    const-class v1, Lflipboard/widget/FlipboardWidgetManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lflipboard/widget/FlipboardWidgetManager;->b:Lflipboard/widget/FlipboardWidgetManager;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lflipboard/widget/FlipboardWidgetManager;

    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-direct {v0, v2}, Lflipboard/widget/FlipboardWidgetManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lflipboard/widget/FlipboardWidgetManager;->b:Lflipboard/widget/FlipboardWidgetManager;

    .line 45
    :cond_0
    sget-object v0, Lflipboard/widget/FlipboardWidgetManager;->b:Lflipboard/widget/FlipboardWidgetManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 138
    if-gez p1, :cond_0

    move p1, v0

    .line 141
    :cond_0
    iget v1, p0, Lflipboard/widget/FlipboardWidgetManager;->m:I

    if-eq v1, p1, :cond_1

    .line 142
    sget-object v1, Lflipboard/widget/FlipboardWidgetManager;->a:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 143
    iput p1, p0, Lflipboard/widget/FlipboardWidgetManager;->m:I

    .line 144
    if-nez p1, :cond_2

    .line 145
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "widget_count"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 150
    :cond_1
    :goto_0
    return-void

    .line 147
    :cond_2
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "widget_count"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 565
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v1, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->c:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->aD:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    .line 566
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "event"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 567
    iput-object p2, v0, Lflipboard/io/UsageEvent;->i:Ljava/lang/String;

    .line 568
    const-string v1, "id"

    const-string v2, "widget tap"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 569
    const-string v1, "sectionIdentifier"

    invoke-virtual {v0, v1, p1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 570
    const-string v1, "type"

    sget-object v2, Lflipboard/widget/FlipboardWidget;->b:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 571
    const-string v1, "action"

    sget-object v2, Lflipboard/widget/FlipboardWidget;->c:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 572
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 575
    const-string v0, "launch_from"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 576
    sget-object v0, Lflipboard/widget/FlipboardWidget;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 577
    sget-object v0, Lflipboard/widget/FlipboardWidget;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 578
    const-string v0, "extra_opened_from_widget"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 579
    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 260
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 261
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->O()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 262
    iget-object v0, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lflipboard/widget/FlipboardWidgetManager;->a(Ljava/util/List;Ljava/util/List;)V

    goto :goto_0

    .line 263
    :cond_1
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->P()Z

    move-result v2

    if-nez v2, :cond_0

    .line 275
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 278
    :cond_2
    return-void
.end method

.method private b(I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 242
    sget-object v0, Lflipboard/widget/FlipboardWidgetManager;->a:Lflipboard/util/Log;

    new-array v0, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 243
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 244
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->j:Ljava/util/List;

    iget-object v1, p0, Lflipboard/widget/FlipboardWidgetManager;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-interface {v0, v3, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 245
    sget-object v0, Lflipboard/widget/FlipboardWidgetManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v3

    iget-object v2, p0, Lflipboard/widget/FlipboardWidgetManager;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v4

    .line 246
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 247
    iget-object v3, p0, Lflipboard/widget/FlipboardWidgetManager;->k:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 249
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 250
    invoke-direct {p0}, Lflipboard/widget/FlipboardWidgetManager;->k()V

    .line 252
    :cond_1
    return-void
.end method

.method static d()Z
    .locals 1

    .prologue
    .line 159
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->t()Z

    move-result v0

    return v0
.end method

.method static synthetic i()Lflipboard/util/Log;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lflipboard/widget/FlipboardWidgetManager;->a:Lflipboard/util/Log;

    return-object v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 320
    :try_start_0
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->d:Landroid/content/SharedPreferences;

    const-string v1, "used_ids"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 321
    sget-object v1, Lflipboard/widget/FlipboardWidgetManager;->a:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 322
    if-eqz v0, :cond_0

    .line 323
    new-instance v1, Lflipboard/json/JSONParser;

    invoke-direct {v1, v0}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lflipboard/json/JSONParser;->R()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 324
    iget-object v2, p0, Lflipboard/widget/FlipboardWidgetManager;->j:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 325
    iget-object v2, p0, Lflipboard/widget/FlipboardWidgetManager;->k:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 328
    :catch_0
    move-exception v0

    .line 329
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    .line 331
    :cond_0
    return-void
.end method

.method private k()V
    .locals 3

    .prologue
    .line 335
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->j:Ljava/util/List;

    invoke-static {v0}, Lflipboard/json/JSONSerializer;->d(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 336
    iget-object v1, p0, Lflipboard/widget/FlipboardWidgetManager;->d:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "used_ids"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 337
    sget-object v1, Lflipboard/widget/FlipboardWidgetManager;->a:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 338
    return-void
.end method


# virtual methods
.method public final a(Z)I
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v1, 0x0

    .line 201
    sget-object v0, Lflipboard/widget/FlipboardWidgetManager;->a:Lflipboard/util/Log;

    new-array v0, v12, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 202
    if-nez p1, :cond_0

    iget-wide v2, p0, Lflipboard/widget/FlipboardWidgetManager;->h:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v6, 0xea60

    sub-long/2addr v4, v6

    cmp-long v0, v2, v4

    if-gez v0, :cond_3

    .line 204
    :cond_0
    new-array v4, v13, [Ljava/lang/Class;

    const-class v0, Lflipboard/widget/FlipboardWidgetSmall;

    aput-object v0, v4, v1

    const-class v0, Lflipboard/widget/FlipboardWidgetMedium;

    aput-object v0, v4, v12

    move v3, v1

    move v0, v1

    :goto_0
    if-ge v3, v13, :cond_2

    aget-object v5, v4, v3

    .line 205
    iget-object v2, p0, Lflipboard/widget/FlipboardWidgetManager;->c:Landroid/content/Context;

    invoke-static {v2}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    new-instance v6, Landroid/content/ComponentName;

    iget-object v7, p0, Lflipboard/widget/FlipboardWidgetManager;->c:Landroid/content/Context;

    invoke-direct {v6, v7, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v6}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v6

    array-length v7, v6

    move v2, v0

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_1

    aget v8, v6, v0

    .line 206
    sget-object v9, Lflipboard/widget/FlipboardWidgetManager;->a:Lflipboard/util/Log;

    new-array v9, v13, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v12

    new-instance v9, Landroid/content/Intent;

    iget-object v10, p0, Lflipboard/widget/FlipboardWidgetManager;->c:Landroid/content/Context;

    invoke-direct {v9, v10, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v10, "appWidgetIds"

    new-array v11, v12, [I

    aput v8, v11, v1

    invoke-virtual {v9, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    const-string v8, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v9, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v8, p0, Lflipboard/widget/FlipboardWidgetManager;->c:Landroid/content/Context;

    invoke-virtual {v8, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 207
    add-int/lit8 v2, v2, 0x1

    .line 205
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 204
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_0

    .line 210
    :cond_2
    invoke-direct {p0, v0}, Lflipboard/widget/FlipboardWidgetManager;->a(I)V

    .line 215
    :goto_2
    return v0

    .line 212
    :cond_3
    if-nez p1, :cond_4

    .line 213
    sget-object v0, Lflipboard/widget/FlipboardWidgetManager;->a:Lflipboard/util/Log;

    .line 215
    :cond_4
    const/4 v0, -0x1

    goto :goto_2
.end method

.method public final declared-synchronized a(Lflipboard/service/Section;JI)Lflipboard/widget/FlipboardWidgetManager$WidgetItem;
    .locals 12

    .prologue
    const/16 v10, 0xc8

    const/4 v3, 0x0

    .line 282
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lflipboard/service/Section;->m()Ljava/util/List;

    move-result-object v0

    .line 283
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 284
    invoke-direct {p0, v6, v0}, Lflipboard/widget/FlipboardWidgetManager;->a(Ljava/util/List;Ljava/util/List;)V

    .line 285
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    .line 286
    sget-object v0, Lflipboard/widget/FlipboardWidgetManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    sget-object v2, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v2}, Lflipboard/io/NetworkManager;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lflipboard/widget/FlipboardWidgetManager;->k:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lflipboard/widget/FlipboardWidgetManager;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    move v5, v3

    .line 289
    :goto_0
    const/16 v0, 0xa

    if-ge v5, v0, :cond_9

    if-lez v7, :cond_9

    move v4, v3

    .line 290
    :goto_1
    if-ge v4, v7, :cond_8

    .line 291
    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 292
    iget-object v1, p0, Lflipboard/widget/FlipboardWidgetManager;->k:Ljava/util/Set;

    iget-object v2, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 294
    new-instance v2, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;

    invoke-direct {v2, v0}, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;-><init>(Lflipboard/objs/FeedItem;)V

    .line 297
    iget-object v1, v2, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a:Lflipboard/objs/FeedItem;

    iget-boolean v1, v1, Lflipboard/objs/FeedItem;->an:Z

    if-nez v1, :cond_0

    iget-object v1, v2, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a:Lflipboard/objs/FeedItem;

    iget v1, v1, Lflipboard/objs/FeedItem;->ak:I

    if-nez v1, :cond_0

    iget-object v1, v2, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_0
    move v1, v3

    :goto_2
    if-eqz v1, :cond_5

    .line 298
    const-wide/16 v8, 0x0

    cmp-long v1, p2, v8

    if-ltz v1, :cond_1

    invoke-virtual {v2, p2, p3}, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a(J)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 299
    :cond_1
    iget-object v1, p0, Lflipboard/widget/FlipboardWidgetManager;->k:Ljava/util/Set;

    iget-object v3, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 300
    iget-object v1, p0, Lflipboard/widget/FlipboardWidgetManager;->j:Ljava/util/List;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    invoke-direct {p0}, Lflipboard/widget/FlipboardWidgetManager;->k()V

    .line 302
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->i:Ljava/util/HashMap;

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v2

    .line 314
    :goto_3
    monitor-exit p0

    return-object v0

    .line 297
    :cond_2
    :try_start_1
    iget-object v1, v2, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->P()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v2, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, v2, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_3

    iget-object v1, v2, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    iget v1, v1, Lflipboard/objs/Image;->f:I

    if-le v1, v10, :cond_3

    iget-object v1, v2, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, v2, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->c:Ljava/lang/String;

    invoke-static {v1}, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a(Ljava/lang/String;)Z

    move-result v1

    goto :goto_2

    :cond_3
    iget-object v1, v2, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    iget v1, v1, Lflipboard/objs/Image;->f:I

    if-le v1, v10, :cond_4

    iget-object v1, v2, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, v2, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a:Lflipboard/objs/FeedItem;

    iget v1, v1, Lflipboard/objs/FeedItem;->ak:I

    if-nez v1, :cond_4

    iget-object v1, v2, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->c:Ljava/lang/String;

    invoke-static {v1}, Lflipboard/widget/FlipboardWidgetManager$WidgetItem;->a(Ljava/lang/String;)Z

    move-result v1

    goto :goto_2

    :cond_4
    move v1, v3

    goto :goto_2

    .line 308
    :cond_5
    iget-object v1, p0, Lflipboard/widget/FlipboardWidgetManager;->k:Ljava/util/Set;

    iget-object v2, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 310
    :cond_6
    sget-object v1, Lflipboard/widget/FlipboardWidgetManager;->a:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    aput-object v0, v1, v2

    .line 290
    :cond_7
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_1

    .line 312
    :cond_8
    div-int/lit8 v0, v7, 0x2

    invoke-direct {p0, v0}, Lflipboard/widget/FlipboardWidgetManager;->b(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 289
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto/16 :goto_0

    .line 314
    :cond_9
    const/4 v0, 0x0

    goto :goto_3

    .line 282
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(JZ)V
    .locals 11

    .prologue
    const-wide/16 v8, 0x0

    const/4 v4, 0x0

    .line 366
    sget-object v0, Lflipboard/widget/FlipboardWidgetManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x1

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 367
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->c:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 368
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lflipboard/widget/FlipboardWidgetManager;->c:Landroid/content/Context;

    const-class v3, Lflipboard/widget/FlipboardWidgetManager$AlarmReceiver;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 369
    const-string v2, "widget_alarm_update"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 370
    iget-object v2, p0, Lflipboard/widget/FlipboardWidgetManager;->c:Landroid/content/Context;

    const/high16 v3, 0x10000000

    invoke-static {v2, v4, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 371
    cmp-long v1, p1, v8

    if-lez v1, :cond_1

    .line 372
    const/4 v1, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v2, p1

    move-wide v4, p1

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 377
    :goto_0
    if-eqz p3, :cond_0

    .line 378
    cmp-long v0, p1, v8

    if-ltz v0, :cond_2

    .line 379
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "widget_updates"

    const-wide/16 v2, 0x3e8

    div-long v2, p1, v2

    long-to-int v2, v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 384
    :cond_0
    :goto_1
    return-void

    .line 374
    :cond_1
    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 381
    :cond_2
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "widget_updates"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 32
    check-cast p1, Lflipboard/service/Section;

    check-cast p2, Lflipboard/service/Section$Message;

    sget-object v0, Lflipboard/widget/FlipboardWidgetManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v3

    aput-object p2, v0, v4

    const/4 v1, 0x2

    iget-boolean v2, p0, Lflipboard/widget/FlipboardWidgetManager;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->l:Lflipboard/service/Section;

    if-ne p1, v0, :cond_0

    sget-object v0, Lflipboard/service/Section$Message;->e:Lflipboard/service/Section$Message;

    if-ne p2, v0, :cond_0

    iput-boolean v4, p0, Lflipboard/widget/FlipboardWidgetManager;->f:Z

    :cond_0
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->l:Lflipboard/service/Section;

    if-ne p1, v0, :cond_1

    sget-object v0, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    if-ne p2, v0, :cond_1

    iget-boolean v0, p0, Lflipboard/widget/FlipboardWidgetManager;->f:Z

    if-eqz v0, :cond_2

    iput-boolean v3, p0, Lflipboard/widget/FlipboardWidgetManager;->f:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p1}, Lflipboard/service/Section;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v3}, Lflipboard/widget/FlipboardWidgetManager;->a(Z)I

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 121
    iget v0, p0, Lflipboard/widget/FlipboardWidgetManager;->m:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 122
    invoke-virtual {p0}, Lflipboard/widget/FlipboardWidgetManager;->c()V

    .line 124
    :cond_0
    iget v0, p0, Lflipboard/widget/FlipboardWidgetManager;->m:I

    return v0
.end method

.method final c()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 129
    .line 130
    new-array v2, v7, [Ljava/lang/Class;

    const-class v1, Lflipboard/widget/FlipboardWidgetSmall;

    aput-object v1, v2, v0

    const/4 v1, 0x1

    const-class v3, Lflipboard/widget/FlipboardWidgetMedium;

    aput-object v3, v2, v1

    move v1, v0

    :goto_0
    if-ge v0, v7, :cond_0

    aget-object v3, v2, v0

    .line 131
    iget-object v4, p0, Lflipboard/widget/FlipboardWidgetManager;->c:Landroid/content/Context;

    invoke-static {v4}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v4

    new-instance v5, Landroid/content/ComponentName;

    iget-object v6, p0, Lflipboard/widget/FlipboardWidgetManager;->c:Landroid/content/Context;

    invoke-direct {v5, v6, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v4, v5}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v3

    array-length v3, v3

    add-int/2addr v1, v3

    .line 130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 133
    :cond_0
    invoke-direct {p0, v1}, Lflipboard/widget/FlipboardWidgetManager;->a(I)V

    .line 134
    return-void
.end method

.method final declared-synchronized e()V
    .locals 1

    .prologue
    .line 186
    monitor-enter p0

    :try_start_0
    sget-object v0, Lflipboard/widget/FlipboardWidgetManager;->a:Lflipboard/util/Log;

    .line 187
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 188
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 189
    invoke-direct {p0}, Lflipboard/widget/FlipboardWidgetManager;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    monitor-exit p0

    return-void

    .line 186
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Lflipboard/service/Section;
    .locals 2

    .prologue
    .line 231
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->l:Lflipboard/service/Section;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->l:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->u:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->l:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 232
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/widget/FlipboardWidgetManager;->l:Lflipboard/service/Section;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->a(Lflipboard/service/Section;)V

    .line 234
    :cond_1
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x14

    if-le v0, v1, :cond_2

    .line 235
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lflipboard/widget/FlipboardWidgetManager;->b(I)V

    .line 237
    :cond_2
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->l:Lflipboard/service/Section;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 231
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()J
    .locals 4

    .prologue
    .line 345
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager;->d:Landroid/content/SharedPreferences;

    const-string v1, "widget_updates"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 362
    invoke-virtual {p0}, Lflipboard/widget/FlipboardWidgetManager;->g()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lflipboard/widget/FlipboardWidgetManager;->a(JZ)V

    .line 363
    return-void
.end method
