.class public Lflipboard/widget/FlipboardWidgetMedium;
.super Lflipboard/widget/FlipboardWidget;
.source "FlipboardWidgetMedium.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lflipboard/widget/FlipboardWidget;-><init>()V

    return-void
.end method


# virtual methods
.method final a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)Landroid/widget/RemoteViews;
    .locals 3

    .prologue
    .line 23
    invoke-static {p2, p3}, Lflipboard/widget/FlipboardWidgetMedium;->a(Landroid/appwidget/AppWidgetManager;I)Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    move-result-object v0

    .line 24
    sget-object v1, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->d:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    if-ne v0, v1, :cond_0

    .line 25
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030015

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 29
    :goto_0
    return-object v0

    .line 26
    :cond_0
    sget-object v1, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->a:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    if-ne v0, v1, :cond_1

    .line 27
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f03001d

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    goto :goto_0

    .line 29
    :cond_1
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030016

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method final a()Z
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return v0
.end method

.method final b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)Landroid/widget/RemoteViews;
    .locals 5

    .prologue
    const v4, 0x7f0d0037

    const v3, 0x7f0a0073

    .line 36
    invoke-static {p2, p3}, Lflipboard/widget/FlipboardWidgetMedium;->a(Landroid/appwidget/AppWidgetManager;I)Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    move-result-object v0

    .line 37
    sget-object v1, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->d:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    if-eq v0, v1, :cond_0

    sget-object v1, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->c:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    if-ne v0, v1, :cond_3

    .line 38
    :cond_0
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030017

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 39
    sget-boolean v1, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v1, :cond_2

    .line 40
    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 44
    :goto_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 45
    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 46
    const-string v2, "us"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 47
    const v1, 0x7f0a0074

    const v2, 0x7f02020e

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 57
    :cond_1
    :goto_1
    return-object v0

    .line 42
    :cond_2
    const v1, 0x7f0d0034

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 50
    :cond_3
    sget-object v1, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->a:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    if-ne v0, v1, :cond_4

    .line 51
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f03001b

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    goto :goto_1

    .line 53
    :cond_4
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030018

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 54
    sget-boolean v1, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v1, :cond_1

    .line 55
    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_1
.end method

.method final b()Z
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x1

    return v0
.end method
