.class public abstract Lflipboard/widget/FlipboardWidget;
.super Landroid/appwidget/AppWidgetProvider;
.source "FlipboardWidget.java"


# static fields
.field static a:Lflipboard/util/Log;

.field static b:Ljava/lang/String;

.field static c:Ljava/lang/String;

.field private static final e:Ljava/text/SimpleDateFormat;

.field private static final f:Ljava/text/SimpleDateFormat;


# instance fields
.field d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 58
    const-string v0, "widget"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    .line 61
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEE hh:mm a"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lflipboard/widget/FlipboardWidget;->e:Ljava/text/SimpleDateFormat;

    .line 62
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "hh:mm a"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lflipboard/widget/FlipboardWidget;->f:Ljava/text/SimpleDateFormat;

    .line 63
    const-string v0, "extra_widget_type"

    sput-object v0, Lflipboard/widget/FlipboardWidget;->b:Ljava/lang/String;

    .line 64
    const-string v0, "extra_widget_tap_type"

    sput-object v0, Lflipboard/widget/FlipboardWidget;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    .line 74
    sget-object v0, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 75
    return-void
.end method

.method public static a(Landroid/appwidget/AppWidgetManager;I)Lflipboard/widget/FlipboardWidget$FLWidgetSize;
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 363
    sget-object v0, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->b:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    .line 364
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_1

    .line 365
    invoke-virtual {p0, p1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object v1

    .line 366
    if-eqz v1, :cond_1

    .line 367
    const-string v0, "appWidgetMinWidth"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 368
    const-string v2, "appWidgetMinHeight"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 369
    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 370
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 371
    if-eqz v2, :cond_2

    const-string v4, "sony"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    const-string v2, "c660"

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "c650"

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 373
    :cond_0
    sget-object v0, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->c:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    .line 385
    :cond_1
    :goto_0
    return-object v0

    .line 374
    :cond_2
    const/16 v2, 0x17c

    if-le v0, v2, :cond_3

    const/16 v2, 0x1a4

    if-le v1, v2, :cond_3

    .line 375
    sget-object v0, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->d:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    goto :goto_0

    .line 376
    :cond_3
    const/16 v2, 0x12c

    if-le v0, v2, :cond_4

    const/16 v0, 0xc8

    if-le v1, v0, :cond_4

    .line 377
    sget-object v0, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->c:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    goto :goto_0

    .line 378
    :cond_4
    const/16 v0, 0x82

    if-ge v1, v0, :cond_5

    .line 379
    sget-object v0, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->a:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    goto :goto_0

    .line 381
    :cond_5
    sget-object v0, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->b:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    goto :goto_0
.end method

.method static a(J)Ljava/lang/String;
    .locals 4

    .prologue
    .line 704
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 705
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p0, p1}, Ljava/util/Date;-><init>(J)V

    .line 706
    invoke-virtual {v0}, Ljava/util/Date;->getDate()I

    move-result v0

    invoke-virtual {v1}, Ljava/util/Date;->getDate()I

    move-result v2

    if-eq v0, v2, :cond_0

    .line 708
    sget-object v2, Lflipboard/widget/FlipboardWidget;->e:Ljava/text/SimpleDateFormat;

    monitor-enter v2

    .line 709
    :try_start_0
    sget-object v0, Lflipboard/widget/FlipboardWidget;->e:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 714
    :goto_0
    return-object v0

    .line 710
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 713
    :cond_0
    sget-object v2, Lflipboard/widget/FlipboardWidget;->f:Ljava/text/SimpleDateFormat;

    monitor-enter v2

    .line 714
    :try_start_1
    sget-object v0, Lflipboard/widget/FlipboardWidget;->f:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 715
    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method static a(Lflipboard/service/Section;Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0xa

    const/4 v4, 0x4

    .line 150
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 151
    invoke-virtual {p0}, Lflipboard/service/Section;->m()Ljava/util/List;

    .line 152
    iget-object v0, p0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 153
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 154
    if-eqz v0, :cond_1

    iget-object v3, v0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 155
    iget-object v0, v0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v3, 0x5

    if-ne v0, v3, :cond_0

    .line 158
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 164
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 165
    if-lez v1, :cond_3

    .line 166
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 168
    :cond_3
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 170
    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v4, :cond_5

    .line 171
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 172
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00b4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    :cond_5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c()Lflipboard/util/Log;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    return-object v0
.end method


# virtual methods
.method abstract a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)Landroid/widget/RemoteViews;
.end method

.method abstract a()Z
.end method

.method abstract b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)Landroid/widget/RemoteViews;
.end method

.method abstract b()Z
.end method

.method final c(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)Landroid/widget/RemoteViews;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 726
    invoke-virtual {p0, p1, p2, p3}, Lflipboard/widget/FlipboardWidget;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)Landroid/widget/RemoteViews;

    move-result-object v1

    .line 728
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lflipboard/activities/LaunchActivity;

    invoke-direct {v2, p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 729
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "launch"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 730
    const-string v0, "appWidgetIds"

    const/4 v3, 0x1

    new-array v3, v3, [I

    aput p3, v3, v6

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 731
    const-string v0, "launch_from"

    const-string v3, "widget"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 732
    invoke-static {p2, p3}, Lflipboard/widget/FlipboardWidget;->a(Landroid/appwidget/AppWidgetManager;I)Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    move-result-object v0

    .line 734
    invoke-virtual {p0}, Lflipboard/widget/FlipboardWidget;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 735
    const-string v0, "small"

    .line 741
    :goto_0
    sget-object v3, Lflipboard/widget/FlipboardWidget;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 742
    sget-object v0, Lflipboard/widget/FlipboardWidget;->c:Ljava/lang/String;

    const-string v3, "sample"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 743
    const v0, 0x7f0a0064

    invoke-static {p1, v6, v2, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 744
    const v0, 0x7f0a0072

    invoke-virtual {v1, v0, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 745
    return-object v1

    .line 736
    :cond_0
    sget-object v3, Lflipboard/widget/FlipboardWidget$FLWidgetSize;->d:Lflipboard/widget/FlipboardWidget$FLWidgetSize;

    if-ne v0, v3, :cond_1

    .line 737
    const-string v0, "large"

    goto :goto_0

    .line 739
    :cond_1
    const-string v0, "medium"

    goto :goto_0
.end method

.method public onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 107
    invoke-super {p0, p1, p2, p3, p4}, Landroid/appwidget/AppWidgetProvider;->onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V

    .line 108
    sget-object v0, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    new-array v0, v3, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    .line 109
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/widget/FlipboardWidget$UpdateService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 110
    const-string v1, "id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 111
    const-string v1, "widget"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 112
    const-string v1, "keepCurrentItem"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 113
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 114
    sget-object v0, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    new-array v0, v3, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    .line 115
    return-void
.end method

.method public onDeleted(Landroid/content/Context;[I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 140
    sget-object v0, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    new-array v0, v4, [Ljava/lang/Object;

    array-length v2, p2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    move v0, v1

    .line 141
    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_0

    .line 142
    sget-object v2, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    aget v3, p2, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    .line 141
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 144
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 145
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->a()Lflipboard/widget/FlipboardWidgetManager;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/widget/FlipboardWidgetManager;->c()V

    .line 146
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    .line 136
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 137
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    .line 80
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 81
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->a()Lflipboard/widget/FlipboardWidgetManager;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/widget/FlipboardWidgetManager;->c()V

    .line 82
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 118
    sget-object v0, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    new-array v0, v2, [Ljava/lang/Object;

    aput-object p2, v0, v3

    .line 119
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 120
    const-string v1, "android.appwidget.action.APPWIDGET_UPDATE_OPTIONS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 121
    const-string v0, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 122
    const-string v0, "true"

    const-string v1, "refresh"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/widget/FlipboardWidget;->d:Z

    .line 123
    iget-boolean v0, p0, Lflipboard/widget/FlipboardWidget;->d:Z

    if-eqz v0, :cond_1

    .line 124
    sget-object v0, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    .line 125
    const-string v0, "widget refresh"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->c(Ljava/lang/String;)V

    .line 126
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->a()Lflipboard/widget/FlipboardWidgetManager;

    move-result-object v0

    sget-object v1, Lflipboard/widget/FlipboardWidgetManager;->a:Lflipboard/util/Log;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v3

    iget-object v1, v0, Lflipboard/widget/FlipboardWidgetManager;->d:Landroid/content/SharedPreferences;

    const-string v2, "widget_updates"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ac:Z

    if-eqz v1, :cond_2

    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, v0, Lflipboard/widget/FlipboardWidgetManager;->c:Landroid/content/Context;

    const-class v3, Lflipboard/activities/WidgetConfigActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "configure_widget"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v0, v0, Lflipboard/widget/FlipboardWidgetManager;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 131
    :cond_1
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 132
    return-void

    .line 126
    :cond_2
    invoke-virtual {v0}, Lflipboard/widget/FlipboardWidgetManager;->h()V

    goto :goto_0

    .line 128
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.widgetapp.APPWIDGET_RESIZE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    goto :goto_0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 85
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->a()Lflipboard/widget/FlipboardWidgetManager;

    move-result-object v0

    .line 86
    iget-boolean v3, p0, Lflipboard/widget/FlipboardWidget;->d:Z

    if-nez v3, :cond_1

    .line 87
    iget-wide v4, v0, Lflipboard/widget/FlipboardWidgetManager;->g:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/32 v8, 0x2932e00

    sub-long/2addr v6, v8

    cmp-long v0, v4, v6

    if-gez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lflipboard/widget/FlipboardWidget;->d:Z

    :goto_1
    move v0, v2

    .line 91
    :goto_2
    array-length v3, p3

    if-ge v0, v3, :cond_2

    .line 92
    aget v3, p3, v0

    .line 93
    sget-object v4, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    array-length v5, p3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v5, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 94
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lflipboard/widget/FlipboardWidget$UpdateService;

    invoke-direct {v4, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 95
    const-string v5, "id"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 96
    const-string v5, "widget"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 97
    const-string v5, "refresh"

    iget-boolean v6, p0, Lflipboard/widget/FlipboardWidget;->d:Z

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 98
    invoke-virtual {p1, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 99
    sget-object v4, Lflipboard/widget/FlipboardWidget;->a:Lflipboard/util/Log;

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, v2

    .line 91
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_0
    move v0, v2

    .line 87
    goto :goto_0

    .line 89
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v0, Lflipboard/widget/FlipboardWidgetManager;->h:J

    goto :goto_1

    .line 101
    :cond_2
    return-void
.end method
