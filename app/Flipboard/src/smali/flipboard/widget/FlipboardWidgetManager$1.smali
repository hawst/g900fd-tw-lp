.class Lflipboard/widget/FlipboardWidgetManager$1;
.super Ljava/lang/Object;
.source "FlipboardWidgetManager.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/FlipboardManager;",
        "Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/widget/FlipboardWidgetManager;


# direct methods
.method constructor <init>(Lflipboard/widget/FlipboardWidgetManager;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lflipboard/widget/FlipboardWidgetManager$1;->a:Lflipboard/widget/FlipboardWidgetManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    .prologue
    const-wide/16 v2, -0x1

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 77
    check-cast p2, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    sget-object v0, Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;->a:Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager$1;->a:Lflipboard/widget/FlipboardWidgetManager;

    invoke-static {v0}, Lflipboard/widget/FlipboardWidgetManager;->a(Lflipboard/widget/FlipboardWidgetManager;)Lflipboard/service/Section;

    move-result-object v0

    iget-object v1, p0, Lflipboard/widget/FlipboardWidgetManager$1;->a:Lflipboard/widget/FlipboardWidgetManager;

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager$1;->a:Lflipboard/widget/FlipboardWidgetManager;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/service/User;->h:Lflipboard/service/Section;

    invoke-static {v0, v1}, Lflipboard/widget/FlipboardWidgetManager;->a(Lflipboard/widget/FlipboardWidgetManager;Lflipboard/service/Section;)Lflipboard/service/Section;

    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager$1;->a:Lflipboard/widget/FlipboardWidgetManager;

    invoke-static {v0}, Lflipboard/widget/FlipboardWidgetManager;->a(Lflipboard/widget/FlipboardWidgetManager;)Lflipboard/service/Section;

    move-result-object v0

    iget-object v1, p0, Lflipboard/widget/FlipboardWidgetManager$1;->a:Lflipboard/widget/FlipboardWidgetManager;

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->i()Lflipboard/util/Log;

    new-array v0, v4, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/widget/FlipboardWidgetManager$1;->a:Lflipboard/widget/FlipboardWidgetManager;

    invoke-static {v1}, Lflipboard/widget/FlipboardWidgetManager;->a(Lflipboard/widget/FlipboardWidgetManager;)Lflipboard/service/Section;

    move-result-object v1

    aput-object v1, v0, v6

    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager$1;->a:Lflipboard/widget/FlipboardWidgetManager;

    invoke-virtual {v0}, Lflipboard/widget/FlipboardWidgetManager;->e()V

    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager$1;->a:Lflipboard/widget/FlipboardWidgetManager;

    invoke-virtual {v0, v4}, Lflipboard/widget/FlipboardWidgetManager;->a(Z)I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager$1;->a:Lflipboard/widget/FlipboardWidgetManager;

    invoke-virtual {v0}, Lflipboard/widget/FlipboardWidgetManager;->g()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-gez v4, :cond_0

    const-wide/32 v0, 0xdbba00

    :cond_0
    iget-object v4, p0, Lflipboard/widget/FlipboardWidgetManager$1;->a:Lflipboard/widget/FlipboardWidgetManager;

    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->d()Z

    move-result v4

    if-nez v4, :cond_1

    move-wide v0, v2

    :cond_1
    iget-object v2, p0, Lflipboard/widget/FlipboardWidgetManager$1;->a:Lflipboard/widget/FlipboardWidgetManager;

    invoke-virtual {v2, v0, v1, v6}, Lflipboard/widget/FlipboardWidgetManager;->a(JZ)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lflipboard/widget/FlipboardWidgetManager$1;->a:Lflipboard/widget/FlipboardWidgetManager;

    invoke-virtual {v0, v2, v3, v6}, Lflipboard/widget/FlipboardWidgetManager;->a(JZ)V

    goto :goto_0
.end method
