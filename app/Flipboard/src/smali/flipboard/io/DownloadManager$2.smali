.class Lflipboard/io/DownloadManager$2;
.super Ljava/lang/Thread;
.source "DownloadManager.java"


# instance fields
.field a:I

.field final synthetic b:[Ljava/io/File;

.field final synthetic c:Lflipboard/io/DownloadManager;


# direct methods
.method constructor <init>(Lflipboard/io/DownloadManager;Ljava/lang/String;[Ljava/io/File;)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lflipboard/io/DownloadManager$2;->c:Lflipboard/io/DownloadManager;

    iput-object p3, p0, Lflipboard/io/DownloadManager$2;->b:[Ljava/io/File;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/io/File;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 265
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 266
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 268
    invoke-direct {p0, v1, v4}, Lflipboard/io/DownloadManager$2;->a(Ljava/io/File;Ljava/lang/String;)V

    .line 267
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 271
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 273
    iget v0, p0, Lflipboard/io/DownloadManager$2;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/io/DownloadManager$2;->a:I

    .line 275
    :cond_1
    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 245
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 247
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 248
    iget-object v3, p0, Lflipboard/io/DownloadManager$2;->b:[Ljava/io/File;

    array-length v6, v3

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_2

    aget-object v7, v3, v2

    .line 249
    if-eqz v7, :cond_1

    .line 250
    invoke-virtual {v7}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v8

    .line 251
    if-eqz v8, :cond_1

    .line 252
    array-length v9, v8

    move v0, v1

    :goto_1
    if-ge v0, v9, :cond_1

    aget-object v10, v8, v0

    .line 253
    const-string v11, ".tmp"

    invoke-virtual {v10, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 254
    invoke-direct {p0, v7, v10}, Lflipboard/io/DownloadManager$2;->a(Ljava/io/File;Ljava/lang/String;)V

    .line 252
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 248
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 260
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v4

    .line 261
    sget-object v0, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iget v4, p0, Lflipboard/io/DownloadManager$2;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    .line 262
    return-void
.end method
