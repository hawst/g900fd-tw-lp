.class Lflipboard/io/NetworkManager$1;
.super Landroid/content/BroadcastReceiver;
.source "NetworkManager.java"


# instance fields
.field final synthetic a:Lflipboard/io/NetworkManager;


# direct methods
.method constructor <init>(Lflipboard/io/NetworkManager;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lflipboard/io/NetworkManager$1;->a:Lflipboard/io/NetworkManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 189
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 190
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->g()V

    .line 191
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 192
    const-string v0, "noConnectivity"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    move v1, v2

    .line 193
    :goto_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->g()V

    .line 194
    const-string v0, "otherNetwork"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 195
    if-nez v0, :cond_0

    .line 196
    iget-object v0, p0, Lflipboard/io/NetworkManager$1;->a:Lflipboard/io/NetworkManager;

    invoke-static {v0}, Lflipboard/io/NetworkManager;->a(Lflipboard/io/NetworkManager;)Landroid/net/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 198
    :cond_0
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-ne v0, v2, :cond_4

    .line 200
    :goto_1
    iget-object v0, p0, Lflipboard/io/NetworkManager$1;->a:Lflipboard/io/NetworkManager;

    iget-object v3, p0, Lflipboard/io/NetworkManager$1;->a:Lflipboard/io/NetworkManager;

    invoke-static {v3}, Lflipboard/io/NetworkManager;->b(Lflipboard/io/NetworkManager;)Z

    move-result v3

    invoke-virtual {v0, v1, v3, v2}, Lflipboard/io/NetworkManager;->a(ZZZ)V

    .line 202
    :cond_1
    sget-object v0, Lflipboard/io/NetworkManager;->a:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_2

    .line 203
    iget-object v0, p0, Lflipboard/io/NetworkManager$1;->a:Lflipboard/io/NetworkManager;

    invoke-virtual {v0, v4}, Lflipboard/io/NetworkManager;->c(Ljava/lang/String;)V

    .line 205
    :cond_2
    return-void

    :cond_3
    move v1, v3

    .line 192
    goto :goto_0

    :cond_4
    move v2, v3

    .line 198
    goto :goto_1
.end method
