.class Lflipboard/io/NetworkManager$NetworkInputStream;
.super Ljava/io/FilterInputStream;
.source "NetworkManager.java"


# instance fields
.field final a:Lorg/apache/http/HttpRequest;

.field final b:Lorg/apache/http/HttpResponse;

.field final c:J

.field d:Z

.field e:Z

.field final f:Lflipboard/io/RequestLogEntry;

.field final synthetic g:Lflipboard/io/NetworkManager;


# direct methods
.method constructor <init>(Lflipboard/io/NetworkManager;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Ljava/io/InputStream;J)V
    .locals 1

    .prologue
    .line 631
    iput-object p1, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->g:Lflipboard/io/NetworkManager;

    .line 632
    invoke-direct {p0, p4}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 633
    iput-object p2, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->a:Lorg/apache/http/HttpRequest;

    .line 634
    iput-object p3, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->b:Lorg/apache/http/HttpResponse;

    .line 635
    iput-wide p5, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->c:J

    .line 636
    invoke-static {p1}, Lflipboard/io/NetworkManager;->c(Lflipboard/io/NetworkManager;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 637
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ah:Z

    if-eqz v0, :cond_0

    .line 638
    iget-object v0, p1, Lflipboard/io/NetworkManager;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/RequestLogEntry;

    iput-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    .line 642
    :goto_0
    return-void

    .line 640
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    .line 726
    iget-boolean v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->e:Z

    if-nez v0, :cond_1

    .line 727
    iget-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->g:Lflipboard/io/NetworkManager;

    invoke-static {v0}, Lflipboard/io/NetworkManager;->c(Lflipboard/io/NetworkManager;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    .line 728
    iget-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->g:Lflipboard/io/NetworkManager;

    invoke-static {v0}, Lflipboard/io/NetworkManager;->g(Lflipboard/io/NetworkManager;)V

    .line 730
    :cond_0
    iput-boolean v1, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->e:Z

    .line 731
    iget-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    if-eqz v0, :cond_1

    .line 732
    iget-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    iput-boolean v1, v0, Lflipboard/io/RequestLogEntry;->h:Z

    .line 733
    iget-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    iget-object v0, v0, Lflipboard/io/RequestLogEntry;->k:Ljava/io/ByteArrayOutputStream;

    if-eqz v0, :cond_1

    .line 736
    :try_start_0
    iget-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    iget-object v0, v0, Lflipboard/io/RequestLogEntry;->b:Ljava/lang/String;

    const-string v1, "updateFeed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737
    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    iget-object v0, v0, Lflipboard/io/RequestLogEntry;->k:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 739
    :try_start_1
    new-instance v2, Lflipboard/service/Flap$FLObjectInputStream;

    invoke-direct {v2, v1}, Lflipboard/service/Flap$FLObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 740
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 742
    :goto_0
    invoke-virtual {v2}, Lflipboard/service/Flap$FLObjectInputStream;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    if-eqz v0, :cond_3

    .line 743
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 747
    :catch_0
    move-exception v0

    .line 748
    :try_start_2
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "Out of memory parsing network request response for debugging, %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 749
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 750
    iget-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    new-instance v2, Lflipboard/json/FLObject;

    const-string v3, "client-side error"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "out of memory while parsing for this log entry"

    aput-object v6, v4, v5

    invoke-direct {v2, v3, v4}, Lflipboard/json/FLObject;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object v2, v0, Lflipboard/io/RequestLogEntry;->j:Lflipboard/json/FLObject;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 752
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 761
    :goto_1
    iget-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    iput-object v7, v0, Lflipboard/io/RequestLogEntry;->k:Ljava/io/ByteArrayOutputStream;

    .line 767
    :cond_1
    :goto_2
    :try_start_4
    invoke-super {p0}, Ljava/io/FilterInputStream;->close()V

    .line 768
    iget-boolean v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->d:Z

    if-nez v0, :cond_2

    .line 769
    sget-object v0, Lflipboard/io/NetworkManager;->a:Lflipboard/util/Log;

    const-string v1, "end of file not reached, aborting connection: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->a:Lorg/apache/http/HttpRequest;

    invoke-interface {v4}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 770
    iget-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->a:Lorg/apache/http/HttpRequest;

    check-cast v0, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3

    .line 778
    :cond_2
    :goto_3
    return-void

    .line 745
    :cond_3
    :try_start_5
    iget-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    new-instance v2, Lflipboard/json/FLObject;

    invoke-direct {v2}, Lflipboard/json/FLObject;-><init>()V

    iput-object v2, v0, Lflipboard/io/RequestLogEntry;->j:Lflipboard/json/FLObject;

    .line 746
    iget-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    iget-object v0, v0, Lflipboard/io/RequestLogEntry;->j:Lflipboard/json/FLObject;

    const-string v2, "streamed"

    invoke-virtual {v0, v2, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 752
    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    .line 757
    :catch_1
    move-exception v0

    .line 758
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 759
    iget-object v1, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    new-instance v2, Lflipboard/json/FLObject;

    const-string v3, "client-side error"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-direct {v2, v3, v4}, Lflipboard/json/FLObject;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object v2, v1, Lflipboard/io/RequestLogEntry;->j:Lflipboard/json/FLObject;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 761
    iget-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    iput-object v7, v0, Lflipboard/io/RequestLogEntry;->k:Ljava/io/ByteArrayOutputStream;

    goto :goto_2

    .line 752
    :catchall_0
    move-exception v0

    :try_start_8
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 761
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    iput-object v7, v1, Lflipboard/io/RequestLogEntry;->k:Ljava/io/ByteArrayOutputStream;

    throw v0

    .line 754
    :cond_4
    :try_start_9
    iget-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    new-instance v1, Lflipboard/json/JSONParser;

    iget-object v2, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    iget-object v2, v2, Lflipboard/io/RequestLogEntry;->k:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-direct {v1, v2}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v1}, Lflipboard/json/JSONParser;->Q()Lflipboard/json/FLObject;

    move-result-object v1

    iput-object v1, v0, Lflipboard/io/RequestLogEntry;->j:Lflipboard/json/FLObject;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_1

    .line 772
    :catch_2
    move-exception v0

    .line 773
    iget-object v1, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->g:Lflipboard/io/NetworkManager;

    invoke-static {v1}, Lflipboard/io/NetworkManager;->d(Lflipboard/io/NetworkManager;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_4
    throw v0

    :cond_5
    new-instance v0, Lflipboard/io/NetworkManager$NoNetworkException;

    invoke-direct {v0}, Lflipboard/io/NetworkManager$NoNetworkException;-><init>()V

    goto :goto_4

    .line 776
    :catch_3
    move-exception v0

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    goto :goto_3
.end method

.method public finalize()V
    .locals 5

    .prologue
    .line 712
    iget-boolean v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->e:Z

    if-nez v0, :cond_0

    .line 713
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "leaked connection: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->b:Lorg/apache/http/HttpResponse;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 715
    :try_start_0
    invoke-virtual {p0}, Lflipboard/io/NetworkManager$NetworkInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 720
    :cond_0
    :goto_0
    return-void

    .line 716
    :catch_0
    move-exception v0

    .line 717
    sget-object v1, Lflipboard/io/NetworkManager;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public read()I
    .locals 6

    .prologue
    .line 648
    iget-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->g:Lflipboard/io/NetworkManager;

    invoke-static {v0}, Lflipboard/io/NetworkManager;->d(Lflipboard/io/NetworkManager;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 649
    new-instance v0, Lflipboard/io/NetworkManager$NoNetworkException;

    invoke-direct {v0}, Lflipboard/io/NetworkManager$NoNetworkException;-><init>()V

    throw v0

    .line 652
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    .line 654
    :goto_0
    :try_start_0
    invoke-super {p0}, Ljava/io/FilterInputStream;->read()I

    move-result v0

    .line 655
    if-ltz v0, :cond_1

    .line 656
    iget-object v2, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->g:Lflipboard/io/NetworkManager;

    invoke-static {v2}, Lflipboard/io/NetworkManager;->e(Lflipboard/io/NetworkManager;)J

    .line 657
    iget-object v2, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    if-eqz v2, :cond_1

    .line 658
    iget-object v2, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    iget-object v2, v2, Lflipboard/io/RequestLogEntry;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 659
    iget-object v2, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    iget-object v2, v2, Lflipboard/io/RequestLogEntry;->k:Ljava/io/ByteArrayOutputStream;

    if-eqz v2, :cond_1

    .line 660
    iget-object v2, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    iget-object v2, v2, Lflipboard/io/RequestLogEntry;->k:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 664
    :cond_1
    if-gez v0, :cond_2

    .line 665
    const/4 v2, 0x1

    iput-boolean v2, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->d:Z
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 667
    :cond_2
    return v0

    .line 668
    :catch_0
    move-exception v0

    .line 669
    int-to-long v2, v1

    const-wide/16 v4, 0x4e20

    mul-long/2addr v2, v4

    :try_start_1
    iget-wide v4, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->c:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    iget-object v2, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->g:Lflipboard/io/NetworkManager;

    invoke-static {v2}, Lflipboard/io/NetworkManager;->d(Lflipboard/io/NetworkManager;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 670
    :cond_3
    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 674
    :catch_1
    move-exception v0

    .line 675
    iget-object v1, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->g:Lflipboard/io/NetworkManager;

    invoke-static {v1}, Lflipboard/io/NetworkManager;->d(Lflipboard/io/NetworkManager;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_1
    throw v0

    .line 652
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 675
    :cond_5
    new-instance v0, Lflipboard/io/NetworkManager$NoNetworkException;

    invoke-direct {v0}, Lflipboard/io/NetworkManager$NoNetworkException;-><init>()V

    goto :goto_1
.end method

.method public read([BII)I
    .locals 8

    .prologue
    .line 680
    iget-object v0, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->g:Lflipboard/io/NetworkManager;

    invoke-static {v0}, Lflipboard/io/NetworkManager;->d(Lflipboard/io/NetworkManager;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 681
    new-instance v0, Lflipboard/io/NetworkManager$NoNetworkException;

    invoke-direct {v0}, Lflipboard/io/NetworkManager$NoNetworkException;-><init>()V

    throw v0

    .line 684
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    .line 686
    :goto_0
    :try_start_0
    invoke-super {p0, p1, p2, p3}, Ljava/io/FilterInputStream;->read([BII)I

    move-result v0

    .line 687
    if-lez v0, :cond_1

    .line 688
    iget-object v2, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->g:Lflipboard/io/NetworkManager;

    iget-object v3, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->g:Lflipboard/io/NetworkManager;

    invoke-static {v3}, Lflipboard/io/NetworkManager;->f(Lflipboard/io/NetworkManager;)J

    move-result-wide v4

    int-to-long v6, v0

    add-long/2addr v4, v6

    invoke-static {v2, v4, v5}, Lflipboard/io/NetworkManager;->a(Lflipboard/io/NetworkManager;J)J

    .line 689
    iget-object v2, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    if-eqz v2, :cond_1

    .line 690
    iget-object v2, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    iget-object v2, v2, Lflipboard/io/RequestLogEntry;->i:Ljava/util/concurrent/atomic/AtomicLong;

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 691
    iget-object v2, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    iget-object v2, v2, Lflipboard/io/RequestLogEntry;->k:Ljava/io/ByteArrayOutputStream;

    if-eqz v2, :cond_1

    .line 692
    iget-object v2, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->f:Lflipboard/io/RequestLogEntry;

    iget-object v2, v2, Lflipboard/io/RequestLogEntry;->k:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2, p1, p2, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 696
    :cond_1
    if-gez v0, :cond_2

    .line 697
    const/4 v2, 0x1

    iput-boolean v2, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->d:Z
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 699
    :cond_2
    return v0

    .line 700
    :catch_0
    move-exception v0

    .line 701
    int-to-long v2, v1

    const-wide/16 v4, 0x4e20

    mul-long/2addr v2, v4

    :try_start_1
    iget-wide v4, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->c:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    iget-object v2, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->g:Lflipboard/io/NetworkManager;

    invoke-static {v2}, Lflipboard/io/NetworkManager;->d(Lflipboard/io/NetworkManager;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 702
    :cond_3
    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 706
    :catch_1
    move-exception v0

    .line 707
    iget-object v1, p0, Lflipboard/io/NetworkManager$NetworkInputStream;->g:Lflipboard/io/NetworkManager;

    invoke-static {v1}, Lflipboard/io/NetworkManager;->d(Lflipboard/io/NetworkManager;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_1
    throw v0

    .line 684
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 707
    :cond_5
    new-instance v0, Lflipboard/io/NetworkManager$NoNetworkException;

    invoke-direct {v0}, Lflipboard/io/NetworkManager$NoNetworkException;-><init>()V

    goto :goto_1
.end method
