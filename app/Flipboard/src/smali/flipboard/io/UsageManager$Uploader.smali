.class public final Lflipboard/io/UsageManager$Uploader;
.super Ljava/lang/Thread;
.source "UsageManager.java"


# instance fields
.field a:Z

.field b:Z

.field c:Lflipboard/io/UsageEvent;

.field d:Lflipboard/io/UsageEvent;

.field final synthetic e:Lflipboard/io/UsageManager;


# direct methods
.method constructor <init>(Lflipboard/io/UsageManager;)V
    .locals 1

    .prologue
    .line 337
    iput-object p1, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    .line 338
    const-string v0, "usage uploader"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 339
    return-void
.end method

.method static a(Ljava/io/RandomAccessFile;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x3e8

    .line 699
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 700
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x400

    invoke-direct {v2, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 701
    new-instance v3, Lflipboard/io/UTF8StreamWriter;

    invoke-direct {v3, v2}, Lflipboard/io/UTF8StreamWriter;-><init>(Ljava/io/OutputStream;)V

    .line 702
    const-string v4, "],\"endTime\":%d.%03d}"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    div-long v8, v0, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    rem-long/2addr v0, v10

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 703
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-static {p0, v0}, Lflipboard/io/UsageManager$Uploader;->a(Ljava/io/RandomAccessFile;[B)V

    .line 704
    return-void
.end method

.method static a(Ljava/io/RandomAccessFile;[B)V
    .locals 5

    .prologue
    const/16 v4, 0x25

    const/4 v0, 0x0

    .line 644
    move v1, v0

    .line 645
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_3

    .line 646
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    .line 647
    if-eq v2, v4, :cond_0

    const/16 v3, 0x26

    if-eq v2, v3, :cond_0

    const/16 v3, 0x20

    if-lt v2, v3, :cond_0

    const/16 v3, 0x7f

    if-lt v2, v3, :cond_2

    .line 648
    :cond_0
    if-ge v1, v0, :cond_1

    .line 649
    sub-int v3, v0, v1

    invoke-virtual {p0, p1, v1, v3}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 651
    :cond_1
    invoke-virtual {p0, v4}, Ljava/io/RandomAccessFile;->write(I)V

    .line 652
    const-string v1, "0123456789abcdef"

    shr-int/lit8 v3, v2, 0x4

    and-int/lit8 v3, v3, 0xf

    invoke-virtual {v1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, Ljava/io/RandomAccessFile;->write(I)V

    .line 653
    const-string v1, "0123456789abcdef"

    and-int/lit8 v2, v2, 0xf

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, Ljava/io/RandomAccessFile;->write(I)V

    .line 654
    add-int/lit8 v1, v0, 0x1

    .line 645
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 657
    :cond_3
    array-length v0, p1

    if-ge v1, v0, :cond_4

    .line 658
    array-length v0, p1

    sub-int/2addr v0, v1

    invoke-virtual {p0, p1, v1, v0}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 660
    :cond_4
    return-void
.end method

.method private declared-synchronized c()V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 455
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-wide v2, v1, Lflipboard/io/UsageManager;->r:J

    const-wide/32 v4, 0x500000

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 456
    iget-object v1, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-wide v2, v1, Lflipboard/io/UsageManager;->r:J

    .line 458
    :goto_0
    iget-object v1, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget v1, v1, Lflipboard/io/UsageManager;->l:I

    if-gt v0, v1, :cond_1

    iget-object v1, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-wide v4, v1, Lflipboard/io/UsageManager;->r:J

    const-wide/32 v6, 0x3c0000

    cmp-long v1, v4, v6

    if-lez v1, :cond_1

    .line 459
    new-instance v1, Ljava/io/File;

    iget-object v4, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v4, v4, Lflipboard/io/UsageManager;->d:Ljava/io/File;

    const-string v5, "pending-%d.json"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 460
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 461
    sget-object v4, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    const-string v5, "trimming: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-virtual {v4, v5, v6}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 462
    iget-object v4, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-wide v6, v4, Lflipboard/io/UsageManager;->r:J

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v8

    sub-long/2addr v6, v8

    iput-wide v6, v4, Lflipboard/io/UsageManager;->r:J

    .line 463
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 458
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 466
    :cond_1
    sget-object v0, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    const-string v1, "trimmed %,d bytes"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-wide v6, v6, Lflipboard/io/UsageManager;->r:J

    sub-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-virtual {v0, v1, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 468
    :cond_2
    monitor-exit p0

    return-void

    .line 455
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    .line 413
    iget-object v1, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    monitor-enter v1

    .line 415
    :goto_0
    :try_start_0
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->c:Lflipboard/io/UsageEvent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 419
    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a(Lflipboard/io/UsageEvent;)V
    .locals 2

    .prologue
    .line 344
    iget-object v0, p1, Lflipboard/io/UsageEvent;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v1, p1, Lflipboard/io/UsageEvent;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    .line 349
    :cond_0
    iget-object v1, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    monitor-enter v1

    .line 350
    :try_start_0
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->d:Lflipboard/io/UsageEvent;

    if-nez v0, :cond_1

    .line 351
    iput-object p1, p0, Lflipboard/io/UsageManager$Uploader;->d:Lflipboard/io/UsageEvent;

    iput-object p1, p0, Lflipboard/io/UsageManager$Uploader;->c:Lflipboard/io/UsageEvent;

    .line 356
    :goto_0
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 357
    monitor-exit v1

    return-void

    .line 353
    :cond_1
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->d:Lflipboard/io/UsageEvent;

    iput-object p1, v0, Lflipboard/io/UsageEvent;->h:Lflipboard/io/UsageEvent;

    .line 354
    iput-object p1, p0, Lflipboard/io/UsageManager$Uploader;->d:Lflipboard/io/UsageEvent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 357
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a(Ljava/io/File;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 573
    invoke-static {}, Lflipboard/io/UsageManager;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 574
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->e()Ljava/lang/String;

    move-result-object v0

    .line 578
    :goto_0
    sget-object v1, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    iget-boolean v1, v1, Lflipboard/util/Log;->f:Z

    if-eqz v1, :cond_0

    .line 579
    sget-object v1, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    const/4 v3, 0x1

    aput-object v0, v1, v3

    .line 580
    iget-object v1, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    const-string v1, "upload"

    invoke-static {v1, p1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;Ljava/io/File;)V

    .line 582
    :cond_0
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v1, :cond_1

    .line 584
    iget-object v1, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    invoke-virtual {v1, p1}, Lflipboard/io/UsageManager;->a(Ljava/io/File;)Z

    .line 586
    :cond_1
    iget-object v1, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    invoke-static {}, Lflipboard/io/UsageManager;->a()Ljava/lang/String;

    move-result-object v1

    .line 587
    if-eqz v1, :cond_2

    invoke-static {}, Lflipboard/io/UsageManager;->e()Z

    move-result v3

    if-nez v3, :cond_2

    .line 588
    const-string v3, "://"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 589
    const/16 v4, 0x2f

    add-int/lit8 v5, v3, 0x3

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    .line 590
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v3, v3, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 594
    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-int v1, v4

    new-array v1, v1, [B

    .line 595
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 597
    :goto_1
    :try_start_0
    array-length v4, v1

    if-ge v2, v4, :cond_4

    .line 598
    array-length v4, v1

    sub-int/2addr v4, v2

    invoke-virtual {v3, v1, v2, v4}, Ljava/io/FileInputStream;->read([BII)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1

    .line 576
    :cond_3
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "/v1/usage/up"

    const/4 v3, 0x0

    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3, v4}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 601
    :cond_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 605
    invoke-static {}, Lflipboard/io/UsageManager;->e()Z

    move-result v2

    if-nez v2, :cond_5

    .line 606
    invoke-static {v1}, Lflipboard/util/JavaUtil;->b([B)[B

    move-result-object v1

    .line 610
    :cond_5
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v2, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 611
    const-string v3, "Content-Type"

    const-string v4, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    const-string v3, "Content-Encoding"

    const-string v4, "deflate"

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    new-instance v3, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v3, v1}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 616
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1, v2}, Lflipboard/io/NetworkManager;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 617
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    .line 618
    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    .line 619
    const/16 v4, 0xc8

    if-eq v3, v4, :cond_6

    .line 620
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "unexpected response: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 601
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    throw v0

    .line 624
    :cond_6
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0, v2, v1}, Lflipboard/io/NetworkManager;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    .line 626
    :cond_7
    :try_start_1
    invoke-virtual {v0}, Ljava/io/InputStream;->read()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-gez v1, :cond_7

    .line 628
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 629
    return-void

    .line 628
    :catchall_1
    move-exception v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v1
.end method

.method final a(Ljava/io/RandomAccessFile;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 664
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 667
    sget-object v2, Lflipboard/io/UsageManager;->c:Ljava/text/SimpleDateFormat;

    monitor-enter v2

    .line 668
    :try_start_0
    sget-object v3, Lflipboard/io/UsageManager;->c:Ljava/text/SimpleDateFormat;

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 669
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 671
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v4, 0x2800

    invoke-direct {v2, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 672
    new-instance v4, Lflipboard/io/UTF8StreamWriter;

    invoke-direct {v4, v2}, Lflipboard/io/UTF8StreamWriter;-><init>(Ljava/io/OutputStream;)V

    .line 673
    const-string v5, "json={\"userId\":%s,"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    invoke-virtual {v4, v5, v6}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 674
    const-string v5, "\"deviceId\":\"%s\","

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v8, v8, Lflipboard/io/UsageManager;->e:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 675
    const-string v5, "\"messageId\":\"%s\","

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 676
    const-string v5, "\"beginTime\":%d.%03d,"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-wide/16 v8, 0x3e8

    div-long v8, v0, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-wide/16 v8, 0x3e8

    rem-long v8, v0, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 677
    const-string v5, "\"createTime\":%d.%03d,"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-wide/16 v8, 0x3e8

    div-long v8, v0, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-wide/16 v8, 0x3e8

    rem-long/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 678
    const-string v0, "\"timestamp\":\"%s\","

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v1, v5

    invoke-virtual {v4, v0, v1}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 679
    const-string v0, "\"utcOffset\":%d,"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v5, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget v5, v5, Lflipboard/io/UsageManager;->h:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v3

    invoke-virtual {v4, v0, v1}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 680
    const-string v0, "\"locale\":\"%s\","

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v5, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v5, v5, Lflipboard/io/UsageManager;->g:Ljava/lang/String;

    aput-object v5, v1, v3

    invoke-virtual {v4, v0, v1}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 681
    const-string v0, "\"version\":\"%s\","

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v5, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v5, v5, Lflipboard/io/UsageManager;->f:Ljava/lang/String;

    aput-object v5, v1, v3

    invoke-virtual {v4, v0, v1}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 682
    const-string v0, "\"usageData\":["

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4, v0, v1}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 683
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-static {p1, v0}, Lflipboard/io/UsageManager$Uploader;->a(Ljava/io/RandomAccessFile;[B)V

    .line 684
    return-void

    .line 669
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method final a(Z)V
    .locals 2

    .prologue
    .line 424
    iget-object v1, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    monitor-enter v1

    .line 425
    :try_start_0
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget v0, v0, Lflipboard/io/UsageManager;->l:I

    if-lez v0, :cond_1

    .line 426
    if-nez p1, :cond_0

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->c()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lflipboard/io/UsageManager$Uploader;->a:Z

    .line 427
    iput-boolean p1, p0, Lflipboard/io/UsageManager$Uploader;->b:Z

    .line 428
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 430
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 426
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 430
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final declared-synchronized b()V
    .locals 9

    .prologue
    .line 435
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget v0, v0, Lflipboard/io/UsageManager;->m:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_1

    .line 437
    :try_start_1
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v0, v0, Lflipboard/io/UsageManager;->k:Ljava/io/RandomAccessFile;

    if-nez v0, :cond_0

    .line 438
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    new-instance v1, Ljava/io/RandomAccessFile;

    iget-object v2, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v2, v2, Lflipboard/io/UsageManager;->p:Ljava/io/File;

    const-string v3, "rw"

    invoke-direct {v1, v2, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, v0, Lflipboard/io/UsageManager;->k:Ljava/io/RandomAccessFile;

    .line 439
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v0, v0, Lflipboard/io/UsageManager;->k:Ljava/io/RandomAccessFile;

    iget-object v1, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v1, v1, Lflipboard/io/UsageManager;->k:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 441
    :cond_0
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v0, v0, Lflipboard/io/UsageManager;->k:Ljava/io/RandomAccessFile;

    invoke-static {v0}, Lflipboard/io/UsageManager$Uploader;->a(Ljava/io/RandomAccessFile;)V

    .line 442
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v0, v0, Lflipboard/io/UsageManager;->k:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 443
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    const/4 v1, 0x0

    iput-object v1, v0, Lflipboard/io/UsageManager;->k:Ljava/io/RandomAccessFile;

    .line 444
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-wide v2, v0, Lflipboard/io/UsageManager;->r:J

    iget-object v1, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v1, v1, Lflipboard/io/UsageManager;->p:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, v0, Lflipboard/io/UsageManager;->r:J

    .line 445
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v0, v0, Lflipboard/io/UsageManager;->p:Ljava/io/File;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v2, v2, Lflipboard/io/UsageManager;->d:Ljava/io/File;

    const-string v3, "pending-%d.json"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget v7, v6, Lflipboard/io/UsageManager;->l:I

    add-int/lit8 v8, v7, 0x1

    iput v8, v6, Lflipboard/io/UsageManager;->l:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 446
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    const/4 v1, 0x0

    iput v1, v0, Lflipboard/io/UsageManager;->m:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 451
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 447
    :catch_0
    move-exception v0

    .line 448
    :try_start_2
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 435
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final run()V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 472
    const/16 v0, 0x13

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 474
    sget-object v5, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    .line 477
    :goto_0
    :try_start_0
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-wide v6, v0, Lflipboard/io/UsageManager;->r:J

    const-wide/32 v8, 0x500000

    cmp-long v0, v6, v8

    if-lez v0, :cond_0

    .line 478
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v0, v0, Lflipboard/io/UsageManager;->n:Lflipboard/io/UsageManager$Uploader;

    invoke-direct {v0}, Lflipboard/io/UsageManager$Uploader;->c()V

    .line 483
    :cond_0
    iget-object v6, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 484
    :try_start_1
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 485
    :goto_1
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->c:Lflipboard/io/UsageEvent;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lflipboard/io/UsageManager$Uploader;->a:Z

    if-nez v0, :cond_1

    .line 486
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 498
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v6

    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    .line 559
    :catch_0
    move-exception v0

    return-void

    .line 488
    :cond_1
    :try_start_3
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->c:Lflipboard/io/UsageEvent;

    if-eqz v0, :cond_9

    .line 489
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->c:Lflipboard/io/UsageEvent;

    .line 490
    iget-object v3, p0, Lflipboard/io/UsageManager$Uploader;->c:Lflipboard/io/UsageEvent;

    iget-object v3, v3, Lflipboard/io/UsageEvent;->h:Lflipboard/io/UsageEvent;

    iput-object v3, p0, Lflipboard/io/UsageManager$Uploader;->c:Lflipboard/io/UsageEvent;

    .line 491
    iget-object v3, p0, Lflipboard/io/UsageManager$Uploader;->c:Lflipboard/io/UsageEvent;

    if-nez v3, :cond_10

    .line 492
    const/4 v3, 0x0

    iput-object v3, p0, Lflipboard/io/UsageManager$Uploader;->d:Lflipboard/io/UsageEvent;

    move-object v3, v0

    move v0, v1

    .line 498
    :goto_2
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 499
    if-eqz v3, :cond_6

    .line 500
    :try_start_4
    monitor-enter p0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    .line 501
    :try_start_5
    sget-object v6, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    iget-boolean v6, v6, Lflipboard/util/Log;->f:Z

    if-eqz v6, :cond_2

    .line 502
    sget-object v6, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v3}, Lflipboard/io/UsageEvent;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    .line 504
    :cond_2
    iget-object v6, v3, Lflipboard/io/UsageEvent;->b:Ljava/lang/String;

    iget-object v7, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v7, v7, Lflipboard/io/UsageManager;->q:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 505
    invoke-virtual {p0}, Lflipboard/io/UsageManager$Uploader;->b()V

    .line 506
    iget-object v6, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v7, v3, Lflipboard/io/UsageEvent;->b:Ljava/lang/String;

    iput-object v7, v6, Lflipboard/io/UsageManager;->q:Ljava/lang/String;

    .line 507
    iget-object v6, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    new-instance v7, Ljava/io/File;

    iget-object v8, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v8, v8, Lflipboard/io/UsageManager;->d:Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "current-"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v10, v10, Lflipboard/io/UsageManager;->q:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".json"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v7, v6, Lflipboard/io/UsageManager;->p:Ljava/io/File;

    .line 509
    :cond_3
    iget-object v6, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v6, v6, Lflipboard/io/UsageManager;->k:Ljava/io/RandomAccessFile;

    if-nez v6, :cond_4

    .line 510
    iget-object v6, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    new-instance v7, Ljava/io/RandomAccessFile;

    iget-object v8, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v8, v8, Lflipboard/io/UsageManager;->p:Ljava/io/File;

    const-string v9, "rw"

    invoke-direct {v7, v8, v9}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v7, v6, Lflipboard/io/UsageManager;->k:Ljava/io/RandomAccessFile;

    .line 511
    iget-object v6, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v6, v6, Lflipboard/io/UsageManager;->k:Ljava/io/RandomAccessFile;

    iget-object v7, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v7, v7, Lflipboard/io/UsageManager;->k:Ljava/io/RandomAccessFile;

    invoke-virtual {v7}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 513
    :cond_4
    iget-object v6, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v6, v6, Lflipboard/io/UsageManager;->k:Ljava/io/RandomAccessFile;

    iget-object v7, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget v7, v7, Lflipboard/io/UsageManager;->m:I

    if-nez v7, :cond_a

    iget-object v7, v3, Lflipboard/io/UsageEvent;->b:Ljava/lang/String;

    invoke-virtual {p0, v6, v7}, Lflipboard/io/UsageManager$Uploader;->a(Ljava/io/RandomAccessFile;Ljava/lang/String;)V

    :goto_3
    iget-object v7, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget v8, v7, Lflipboard/io/UsageManager;->m:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v7, Lflipboard/io/UsageManager;->m:I

    invoke-virtual {v3}, Lflipboard/io/UsageEvent;->d()[B

    move-result-object v3

    invoke-static {v6, v3}, Lflipboard/io/UsageManager$Uploader;->a(Ljava/io/RandomAccessFile;[B)V

    .line 514
    iget-object v3, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget v3, v3, Lflipboard/io/UsageManager;->m:I

    const/16 v6, 0x64

    if-le v3, v6, :cond_5

    .line 515
    invoke-virtual {p0}, Lflipboard/io/UsageManager$Uploader;->b()V

    .line 517
    :cond_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 519
    :cond_6
    if-eqz v0, :cond_8

    move v0, v1

    .line 520
    :goto_4
    :try_start_6
    iget-object v3, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget v3, v3, Lflipboard/io/UsageManager;->l:I

    if-gt v0, v3, :cond_7

    .line 521
    monitor-enter p0
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    .line 522
    :try_start_7
    iget-object v3, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget v3, v3, Lflipboard/io/UsageManager;->l:I

    if-ne v0, v3, :cond_b

    .line 523
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    const/4 v3, 0x0

    iput v3, v0, Lflipboard/io/UsageManager;->l:I

    .line 524
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 553
    :cond_7
    :goto_5
    const/4 v0, 0x0

    :try_start_8
    iput-boolean v0, p0, Lflipboard/io/UsageManager$Uploader;->b:Z

    .line 555
    :cond_8
    iget-object v3, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    monitor-enter v3
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1

    .line 556
    :try_start_9
    iget-object v0, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 557
    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    :try_start_a
    monitor-exit v3

    throw v0
    :try_end_a
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_1

    .line 560
    :catch_1
    move-exception v0

    .line 561
    sget-object v3, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    invoke-virtual {v3, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 494
    :cond_9
    :try_start_b
    iget-boolean v0, p0, Lflipboard/io/UsageManager$Uploader;->a:Z

    if-eqz v0, :cond_f

    .line 496
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/io/UsageManager$Uploader;->a:Z
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move v0, v2

    move-object v3, v4

    goto/16 :goto_2

    .line 513
    :cond_a
    const/16 v7, 0x2c

    :try_start_c
    invoke-virtual {v6, v7}, Ljava/io/RandomAccessFile;->write(I)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto :goto_3

    .line 517
    :catchall_2
    move-exception v0

    :try_start_d
    monitor-exit p0

    throw v0

    .line 526
    :cond_b
    monitor-exit p0

    .line 527
    new-instance v3, Ljava/io/File;

    iget-object v6, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v6, v6, Lflipboard/io/UsageManager;->d:Ljava/io/File;

    const-string v7, "pending-%d.json"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 528
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_e

    .line 529
    invoke-virtual {v5}, Lflipboard/io/NetworkManager;->a()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 530
    iget-object v6, v5, Lflipboard/io/NetworkManager;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v6

    if-eqz v6, :cond_c

    iget-boolean v6, p0, Lflipboard/io/UsageManager$Uploader;->b:Z

    if-eqz v6, :cond_7

    .line 534
    :cond_c
    iget-boolean v6, p0, Lflipboard/io/UsageManager$Uploader;->b:Z

    if-nez v6, :cond_d

    invoke-virtual {v5}, Lflipboard/io/NetworkManager;->e()Z
    :try_end_d
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_1

    move-result v6

    if-nez v6, :cond_7

    .line 538
    :cond_d
    :try_start_e
    invoke-virtual {p0, v3}, Lflipboard/io/UsageManager$Uploader;->a(Ljava/io/File;)V

    .line 542
    iget-object v6, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-wide v8, v6, Lflipboard/io/UsageManager;->r:J

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v10

    sub-long/2addr v8, v10

    iput-wide v8, v6, Lflipboard/io/UsageManager;->r:J

    .line 543
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 545
    iget-object v6, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iput-wide v8, v6, Lflipboard/io/UsageManager;->s:J

    .line 546
    iget-object v6, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v6, v6, Lflipboard/io/UsageManager;->i:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "uploadTime"

    iget-object v8, p0, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-wide v8, v8, Lflipboard/io/UsageManager;->s:J

    invoke-interface {v6, v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-static {v6}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_1

    .line 520
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_4

    .line 526
    :catchall_3
    move-exception v0

    :try_start_f
    monitor-exit p0

    throw v0

    .line 548
    :catch_2
    move-exception v0

    sget-object v0, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    const-string v6, "usage: upload failed: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    invoke-virtual {v0, v6, v7}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_1

    goto/16 :goto_5

    :cond_f
    move v0, v1

    move-object v3, v4

    goto/16 :goto_2

    :cond_10
    move-object v3, v0

    move v0, v1

    goto/16 :goto_2
.end method
