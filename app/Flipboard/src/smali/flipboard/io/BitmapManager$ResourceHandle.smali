.class Lflipboard/io/BitmapManager$ResourceHandle;
.super Lflipboard/io/BitmapManager$Handle;
.source "BitmapManager.java"


# instance fields
.field final a:I

.field final synthetic b:Lflipboard/io/BitmapManager;


# direct methods
.method constructor <init>(Lflipboard/io/BitmapManager;I)V
    .locals 0

    .prologue
    .line 735
    iput-object p1, p0, Lflipboard/io/BitmapManager$ResourceHandle;->b:Lflipboard/io/BitmapManager;

    invoke-direct {p0, p1}, Lflipboard/io/BitmapManager$Handle;-><init>(Lflipboard/io/BitmapManager;)V

    .line 736
    iput p2, p0, Lflipboard/io/BitmapManager$ResourceHandle;->a:I

    .line 737
    return-void
.end method


# virtual methods
.method final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 741
    iget v0, p0, Lflipboard/io/BitmapManager$ResourceHandle;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method final c()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 746
    iget-object v0, p0, Lflipboard/io/BitmapManager$ResourceHandle;->b:Lflipboard/io/BitmapManager;

    invoke-static {v0}, Lflipboard/io/BitmapManager;->c(Lflipboard/io/BitmapManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lflipboard/io/BitmapManager$ResourceHandle;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 747
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method final f()V
    .locals 1

    .prologue
    .line 752
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/io/BitmapManager$ResourceHandle;->g:Landroid/graphics/Bitmap;

    .line 753
    return-void
.end method
