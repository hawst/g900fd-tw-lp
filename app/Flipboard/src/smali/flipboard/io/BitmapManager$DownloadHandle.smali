.class Lflipboard/io/BitmapManager$DownloadHandle;
.super Lflipboard/io/BitmapManager$Handle;
.source "BitmapManager.java"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Lflipboard/io/Download;

.field final synthetic c:Lflipboard/io/BitmapManager;


# direct methods
.method constructor <init>(Lflipboard/io/BitmapManager;Ljava/lang/String;Lflipboard/io/Download;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x2

    .line 765
    iput-object p1, p0, Lflipboard/io/BitmapManager$DownloadHandle;->c:Lflipboard/io/BitmapManager;

    invoke-direct {p0, p1}, Lflipboard/io/BitmapManager$Handle;-><init>(Lflipboard/io/BitmapManager;)V

    .line 766
    iput-object p2, p0, Lflipboard/io/BitmapManager$DownloadHandle;->a:Ljava/lang/String;

    .line 767
    iput-object p3, p0, Lflipboard/io/BitmapManager$DownloadHandle;->b:Lflipboard/io/Download;

    .line 768
    iput-boolean p4, p0, Lflipboard/io/BitmapManager$DownloadHandle;->j:Z

    .line 770
    iget-object v0, p3, Lflipboard/io/Download;->f:Lflipboard/io/Download$Data;

    invoke-virtual {v0}, Lflipboard/io/Download$Data;->e()I

    move-result v3

    .line 771
    iget-object v0, p3, Lflipboard/io/Download;->f:Lflipboard/io/Download$Data;

    invoke-virtual {v0}, Lflipboard/io/Download$Data;->f()I

    move-result v2

    .line 772
    if-eqz p4, :cond_0

    invoke-static {p1}, Lflipboard/io/BitmapManager;->d(Lflipboard/io/BitmapManager;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    move v4, v0

    .line 773
    :goto_0
    mul-int v0, v3, v2

    if-le v0, v4, :cond_2

    move v0, v1

    .line 775
    :goto_1
    mul-int v5, v3, v2

    div-int/2addr v5, v0

    if-le v5, v4, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 772
    :cond_0
    invoke-static {p1}, Lflipboard/io/BitmapManager;->d(Lflipboard/io/BitmapManager;)I

    move-result v0

    move v4, v0

    goto :goto_0

    .line 776
    :cond_1
    sget-object v4, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    div-int v6, v3, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    div-int v5, v2, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x5

    aput-object p3, v4, v1

    .line 777
    div-int v1, v3, v0

    .line 778
    div-int v0, v2, v0

    .line 780
    :goto_2
    iput v1, p0, Lflipboard/io/BitmapManager$DownloadHandle;->h:I

    .line 781
    iput v0, p0, Lflipboard/io/BitmapManager$DownloadHandle;->i:I

    .line 782
    return-void

    :cond_2
    move v0, v2

    move v1, v3

    goto :goto_2
.end method

.method private a(Lflipboard/io/Download$Data;)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 801
    invoke-virtual {p1}, Lflipboard/io/Download$Data;->e()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lflipboard/io/Download$Data;->f()I

    move-result v0

    if-nez v0, :cond_1

    .line 802
    :cond_0
    sget-object v0, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const-string v1, "bogus image data %s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 803
    invoke-static {}, Lflipboard/io/BitmapManager;->d()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 824
    :goto_0
    return-object v1

    .line 806
    :cond_1
    iget v0, p0, Lflipboard/io/BitmapManager$DownloadHandle;->h:I

    if-lez v0, :cond_4

    invoke-virtual {p1}, Lflipboard/io/Download$Data;->e()I

    move-result v0

    iget v2, p0, Lflipboard/io/BitmapManager$DownloadHandle;->h:I

    if-eq v0, v2, :cond_4

    .line 807
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 808
    invoke-virtual {p1}, Lflipboard/io/Download$Data;->e()I

    move-result v2

    iget v3, p0, Lflipboard/io/BitmapManager$DownloadHandle;->h:I

    div-int/2addr v2, v3

    iput v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 809
    sget-object v2, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {p1}, Lflipboard/io/Download$Data;->e()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x2

    invoke-virtual {p1}, Lflipboard/io/Download$Data;->f()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 811
    :goto_1
    invoke-virtual {p1}, Lflipboard/io/Download$Data;->g()Ljava/io/InputStream;

    move-result-object v3

    .line 813
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v3, v2, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 814
    if-nez v2, :cond_2

    .line 815
    sget-object v0, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const-string v2, "failed to decode bitmap: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-virtual {v0, v2, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 816
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 818
    :cond_2
    if-eqz v0, :cond_3

    :try_start_1
    iget v0, p0, Lflipboard/io/BitmapManager$DownloadHandle;->h:I

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 820
    iget v0, p0, Lflipboard/io/BitmapManager$DownloadHandle;->h:I

    iget v1, p0, Lflipboard/io/BitmapManager$DownloadHandle;->i:I

    const/4 v4, 0x1

    invoke-static {v2, v0, v1, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 824
    :goto_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    move-object v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    throw v0

    :cond_3
    move-object v0, v2

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/io/Download$Data;Landroid/graphics/Rect;F)Landroid/graphics/Bitmap;
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0xa
    .end annotation

    .prologue
    .line 829
    invoke-virtual {p1}, Lflipboard/io/Download$Data;->e()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lflipboard/io/Download$Data;->f()I

    move-result v0

    if-nez v0, :cond_2

    .line 830
    :cond_0
    sget-object v0, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const-string v1, "bogus image data %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 831
    invoke-static {}, Lflipboard/io/BitmapManager;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 926
    :cond_1
    :goto_0
    return-object v0

    .line 833
    :cond_2
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-nez v0, :cond_4

    .line 834
    :cond_3
    sget-object v0, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const-string v1, "bogus crop %d,%d,%dx%d, %s"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p2, Landroid/graphics/Rect;->left:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p2, Landroid/graphics/Rect;->top:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 835
    invoke-static {}, Lflipboard/io/BitmapManager;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 839
    :cond_4
    iget v0, p2, Landroid/graphics/Rect;->left:I

    if-nez v0, :cond_5

    iget v0, p2, Landroid/graphics/Rect;->top:I

    if-nez v0, :cond_5

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget v1, p0, Lflipboard/io/BitmapManager$DownloadHandle;->h:I

    if-ne v0, v1, :cond_5

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget v1, p0, Lflipboard/io/BitmapManager$DownloadHandle;->i:I

    if-ne v0, v1, :cond_5

    .line 840
    sget-object v0, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lflipboard/io/BitmapManager$DownloadHandle;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lflipboard/io/BitmapManager$DownloadHandle;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    .line 841
    invoke-direct {p0, p1}, Lflipboard/io/BitmapManager$DownloadHandle;->a(Lflipboard/io/Download$Data;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 842
    if-eqz v0, :cond_1

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, p3, v1

    if-eqz v1, :cond_1

    .line 844
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 845
    invoke-virtual {v5, p3, p3}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 846
    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v3, p0, Lflipboard/io/BitmapManager$DownloadHandle;->h:I

    iget v4, p0, Lflipboard/io/BitmapManager$DownloadHandle;->i:I

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_0

    .line 851
    :cond_5
    invoke-virtual {p1}, Lflipboard/io/Download$Data;->b()Ljava/lang/String;

    move-result-object v0

    .line 852
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v2

    .line 853
    iget-boolean v1, v2, Lflipboard/model/ConfigSetting;->AllowBitmapRegionDecoder:Z

    if-eqz v1, :cond_6

    if-eqz v0, :cond_6

    const-string v1, "image/jpeg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "image/png"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 854
    :cond_6
    invoke-direct {p0, p1}, Lflipboard/io/BitmapManager$DownloadHandle;->a(Lflipboard/io/Download$Data;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 855
    if-eqz v0, :cond_1

    .line 857
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 858
    invoke-virtual {v5, p3, p3}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 859
    iget v1, p2, Landroid/graphics/Rect;->left:I

    iget v2, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v4

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_0

    .line 865
    :cond_7
    const/4 v0, 0x0

    .line 866
    iget v1, p0, Lflipboard/io/BitmapManager$DownloadHandle;->h:I

    if-lez v1, :cond_a

    invoke-virtual {p1}, Lflipboard/io/Download$Data;->e()I

    move-result v1

    iget v3, p0, Lflipboard/io/BitmapManager$DownloadHandle;->h:I

    if-eq v1, v3, :cond_a

    .line 867
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 868
    invoke-virtual {p1}, Lflipboard/io/Download$Data;->e()I

    move-result v1

    iget v3, p0, Lflipboard/io/BitmapManager$DownloadHandle;->h:I

    div-int v3, v1, v3

    .line 869
    iput v3, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 870
    new-instance v1, Landroid/graphics/Rect;

    iget v4, p2, Landroid/graphics/Rect;->left:I

    mul-int/2addr v4, v3

    iget v5, p2, Landroid/graphics/Rect;->top:I

    mul-int/2addr v5, v3

    iget v6, p2, Landroid/graphics/Rect;->right:I

    mul-int/2addr v6, v3

    iget v7, p2, Landroid/graphics/Rect;->bottom:I

    mul-int/2addr v7, v3

    invoke-direct {v1, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 871
    int-to-float v4, v3

    div-float/2addr p3, v4

    .line 872
    sget-object v4, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p1}, Lflipboard/io/Download$Data;->e()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {p1}, Lflipboard/io/Download$Data;->f()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget v6, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v4, v5

    .line 873
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v4

    if-nez v4, :cond_9

    .line 874
    :cond_8
    sget-object v0, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const-string v2, "bogus sampled crop %d,%d,%dx%d, sample=%d, %s"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, v1, Landroid/graphics/Rect;->left:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, v1, Landroid/graphics/Rect;->top:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, v1

    const/4 v1, 0x5

    aput-object p1, v4, v1

    invoke-virtual {v0, v2, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 875
    invoke-static {}, Lflipboard/io/BitmapManager;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    move-object p2, v1

    .line 879
    :cond_a
    invoke-virtual {p1}, Lflipboard/io/Download$Data;->g()Ljava/io/InputStream;

    move-result-object v7

    .line 883
    const/4 v1, 0x0

    :try_start_0
    invoke-static {v7, v1}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    .line 899
    :try_start_1
    sget-object v1, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lflipboard/io/BitmapManager$DownloadHandle;->h:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v4, 0x1

    iget v5, p0, Lflipboard/io/BitmapManager$DownloadHandle;->i:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v4, 0x2

    iget v5, p2, Landroid/graphics/Rect;->left:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v4, 0x3

    iget v5, p2, Landroid/graphics/Rect;->top:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v4, 0x4

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v4, 0x5

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 902
    :try_start_2
    invoke-virtual {v3, p2, v0}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 907
    :goto_1
    if-nez v0, :cond_d

    .line 908
    :try_start_3
    sget-object v0, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const-string v1, "failed to decode bitmap: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-virtual {v0, v1, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 909
    :try_start_4
    iget-boolean v0, v2, Lflipboard/model/ConfigSetting;->RecycleBitmapRegionDecoder:Z

    if-eqz v0, :cond_b

    .line 922
    invoke-virtual {v3}, Landroid/graphics/BitmapRegionDecoder;->recycle()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 926
    :cond_b
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    const/4 v0, 0x0

    goto/16 :goto_0

    .line 886
    :catch_0
    move-exception v0

    :try_start_5
    sget-object v0, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const-string v1, "BitmapRegionDecoder.newInstance failed: %s, %dx%d, crop=%d,%d,%dx%d"

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lflipboard/io/BitmapManager$DownloadHandle;->h:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lflipboard/io/BitmapManager$DownloadHandle;->i:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, p2, Landroid/graphics/Rect;->left:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget v4, p2, Landroid/graphics/Rect;->top:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 889
    invoke-direct {p0, p1}, Lflipboard/io/BitmapManager$DownloadHandle;->a(Lflipboard/io/Download$Data;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 890
    if-eqz v0, :cond_c

    .line 892
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 893
    invoke-virtual {v5, p3, p3}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 894
    iget v1, p2, Landroid/graphics/Rect;->left:I

    iget v2, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v4

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v0

    .line 926
    :cond_c
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    goto/16 :goto_0

    .line 905
    :catch_1
    move-exception v0

    :try_start_6
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    invoke-virtual {v3, p2, v0}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_1

    .line 911
    :cond_d
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p3

    float-to-int v4, v1

    .line 912
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p3

    float-to-int v5, v1

    .line 914
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-ne v1, v4, :cond_e

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-eq v1, v5, :cond_f

    .line 916
    :cond_e
    const/4 v1, 0x1

    invoke-static {v0, v4, v5, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 917
    sget-object v6, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const-string v8, "scaled %dx%d (%dx%d) -> %dx%d (%dx%d)"

    const/16 v9, 0x8

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v9, v10

    const/4 v0, 0x2

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v0

    const/4 v0, 0x3

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v0

    const/4 v0, 0x4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v0

    const/4 v0, 0x5

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v0

    const/4 v0, 0x6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v9, v0

    const/4 v0, 0x7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v9, v0

    invoke-virtual {v6, v8, v9}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-object v0, v1

    .line 921
    :cond_f
    :try_start_7
    iget-boolean v1, v2, Lflipboard/model/ConfigSetting;->RecycleBitmapRegionDecoder:Z

    if-eqz v1, :cond_10

    .line 922
    invoke-virtual {v3}, Landroid/graphics/BitmapRegionDecoder;->recycle()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 926
    :cond_10
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    goto/16 :goto_0

    .line 921
    :catchall_0
    move-exception v0

    :try_start_8
    iget-boolean v1, v2, Lflipboard/model/ConfigSetting;->RecycleBitmapRegionDecoder:Z

    if-eqz v1, :cond_11

    .line 922
    invoke-virtual {v3}, Landroid/graphics/BitmapRegionDecoder;->recycle()V

    :cond_11
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 926
    :catchall_1
    move-exception v0

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    throw v0
.end method


# virtual methods
.method final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 796
    iget-object v0, p0, Lflipboard/io/BitmapManager$DownloadHandle;->a:Ljava/lang/String;

    return-object v0
.end method

.method final b(Landroid/graphics/Rect;F)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 965
    :try_start_0
    iget-object v0, p0, Lflipboard/io/BitmapManager$DownloadHandle;->b:Lflipboard/io/Download;

    iget-object v0, v0, Lflipboard/io/Download;->f:Lflipboard/io/Download$Data;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    .line 967
    :try_start_1
    invoke-direct {p0, v0, p1, p2}, Lflipboard/io/BitmapManager$DownloadHandle;->a(Lflipboard/io/Download$Data;Landroid/graphics/Rect;F)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    .line 979
    :goto_0
    return-object v0

    .line 969
    :catch_0
    move-exception v1

    :try_start_2
    iget-object v1, p0, Lflipboard/io/BitmapManager$DownloadHandle;->c:Lflipboard/io/BitmapManager;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    mul-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lflipboard/io/BitmapManager;->c(I)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 971
    :try_start_3
    invoke-direct {p0, v0, p1, p2}, Lflipboard/io/BitmapManager$DownloadHandle;->a(Lflipboard/io/Download$Data;Landroid/graphics/Rect;F)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v0

    goto :goto_0

    .line 972
    :catch_1
    move-exception v0

    .line 973
    :try_start_4
    iget-object v1, p0, Lflipboard/io/BitmapManager$DownloadHandle;->c:Lflipboard/io/BitmapManager;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    mul-int/2addr v1, v2

    invoke-static {v1}, Lflipboard/io/BitmapManager;->b(I)V

    .line 974
    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    .line 978
    :catch_2
    move-exception v0

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 979
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 937
    :try_start_0
    iget-object v0, p0, Lflipboard/io/BitmapManager$DownloadHandle;->b:Lflipboard/io/Download;

    iget-object v0, v0, Lflipboard/io/Download;->f:Lflipboard/io/Download$Data;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    .line 939
    :try_start_1
    invoke-direct {p0, v0}, Lflipboard/io/BitmapManager$DownloadHandle;->a(Lflipboard/io/Download$Data;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    .line 951
    :goto_0
    return-object v0

    .line 941
    :catch_0
    move-exception v1

    :try_start_2
    iget-object v1, p0, Lflipboard/io/BitmapManager$DownloadHandle;->c:Lflipboard/io/BitmapManager;

    invoke-virtual {p0}, Lflipboard/io/BitmapManager$DownloadHandle;->h()I

    move-result v2

    invoke-virtual {p0}, Lflipboard/io/BitmapManager$DownloadHandle;->i()I

    move-result v3

    mul-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lflipboard/io/BitmapManager;->c(I)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 943
    :try_start_3
    invoke-direct {p0, v0}, Lflipboard/io/BitmapManager$DownloadHandle;->a(Lflipboard/io/Download$Data;)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v0

    goto :goto_0

    .line 944
    :catch_1
    move-exception v0

    .line 945
    :try_start_4
    iget-object v1, p0, Lflipboard/io/BitmapManager$DownloadHandle;->c:Lflipboard/io/BitmapManager;

    invoke-virtual {p0}, Lflipboard/io/BitmapManager$DownloadHandle;->h()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/io/BitmapManager$DownloadHandle;->i()I

    move-result v2

    mul-int/2addr v1, v2

    invoke-static {v1}, Lflipboard/io/BitmapManager;->b(I)V

    .line 946
    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    .line 950
    :catch_2
    move-exception v0

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 951
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 786
    iget-object v0, p0, Lflipboard/io/BitmapManager$DownloadHandle;->b:Lflipboard/io/Download;

    iget-object v0, v0, Lflipboard/io/Download;->f:Lflipboard/io/Download$Data;

    .line 787
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 788
    :goto_0
    if-eqz v0, :cond_1

    const-string v1, "image/jpeg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 789
    const/4 v0, 0x0

    .line 791
    :goto_1
    return v0

    .line 787
    :cond_0
    invoke-virtual {v0}, Lflipboard/io/Download$Data;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 791
    :cond_1
    invoke-super {p0}, Lflipboard/io/BitmapManager$Handle;->e()Z

    move-result v0

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 985
    instance-of v0, p1, Lflipboard/io/BitmapManager$DownloadHandle;

    if-eqz v0, :cond_0

    .line 986
    check-cast p1, Lflipboard/io/BitmapManager$DownloadHandle;

    .line 987
    iget-object v0, p0, Lflipboard/io/BitmapManager$DownloadHandle;->b:Lflipboard/io/Download;

    iget-object v1, p1, Lflipboard/io/BitmapManager$DownloadHandle;->b:Lflipboard/io/Download;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 988
    const/4 v0, 0x1

    .line 991
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final declared-synchronized f()V
    .locals 1

    .prologue
    .line 996
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lflipboard/io/BitmapManager$DownloadHandle;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 997
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/io/BitmapManager$DownloadHandle;->g:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 999
    :cond_0
    monitor-exit p0

    return-void

    .line 996
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final g()V
    .locals 1

    .prologue
    .line 1003
    invoke-super {p0}, Lflipboard/io/BitmapManager$Handle;->g()V

    .line 1004
    iget-object v0, p0, Lflipboard/io/BitmapManager$DownloadHandle;->b:Lflipboard/io/Download;

    invoke-virtual {v0}, Lflipboard/io/Download;->b()V

    .line 1005
    return-void
.end method
