.class public abstract Lflipboard/io/Download$Observer;
.super Ljava/lang/Object;
.source "Download.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/io/Download;",
        "Lflipboard/io/Download$Status;",
        "Lflipboard/io/Download$Data;",
        ">;"
    }
.end annotation


# instance fields
.field protected e:I

.field public f:Lflipboard/io/Download;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/io/Download$Observer;-><init>(I)V

    .line 51
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput p1, p0, Lflipboard/io/Download$Observer;->e:I

    .line 55
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 62
    iput p1, p0, Lflipboard/io/Download$Observer;->e:I

    .line 63
    iget-object v0, p0, Lflipboard/io/Download$Observer;->f:Lflipboard/io/Download;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lflipboard/io/Download$Observer;->f:Lflipboard/io/Download;

    invoke-static {v0}, Lflipboard/io/Download;->a(Lflipboard/io/Download;)V

    .line 66
    :cond_0
    return-void
.end method
