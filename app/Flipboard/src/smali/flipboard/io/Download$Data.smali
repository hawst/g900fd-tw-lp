.class public abstract Lflipboard/io/Download$Data;
.super Ljava/lang/Object;
.source "Download.java"


# instance fields
.field protected final a:Ljava/lang/String;

.field protected final b:J

.field protected final c:J

.field protected final d:J

.field protected final e:Ljava/lang/String;

.field protected final f:Ljava/lang/String;

.field protected final g:I

.field protected final h:I


# direct methods
.method protected constructor <init>(Ljava/lang/String;JJJLjava/lang/String;Ljava/lang/String;II)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lflipboard/io/Download$Data;->a:Ljava/lang/String;

    .line 87
    iput-wide p2, p0, Lflipboard/io/Download$Data;->b:J

    .line 88
    iput-wide p4, p0, Lflipboard/io/Download$Data;->c:J

    .line 89
    iput-wide p6, p0, Lflipboard/io/Download$Data;->d:J

    .line 90
    invoke-virtual {p8}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    move-object p8, v0

    :cond_0
    iput-object p8, p0, Lflipboard/io/Download$Data;->e:Ljava/lang/String;

    .line 91
    invoke-virtual {p9}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    :goto_0
    iput-object v0, p0, Lflipboard/io/Download$Data;->f:Ljava/lang/String;

    .line 92
    iput p10, p0, Lflipboard/io/Download$Data;->g:I

    .line 93
    iput p11, p0, Lflipboard/io/Download$Data;->h:I

    .line 94
    return-void

    :cond_1
    move-object v0, p9

    .line 91
    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 102
    iget-wide v0, p0, Lflipboard/io/Download$Data;->d:J

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lflipboard/io/Download$Data;->e:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/io/File;
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()[B
    .locals 5

    .prologue
    .line 127
    invoke-virtual {p0}, Lflipboard/io/Download$Data;->g()Ljava/io/InputStream;

    move-result-object v2

    .line 129
    :try_start_0
    iget-wide v0, p0, Lflipboard/io/Download$Data;->d:J

    long-to-int v3, v0

    .line 130
    new-array v0, v3, [B

    .line 131
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 132
    sub-int v4, v3, v1

    invoke-virtual {v2, v0, v1, v4}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    .line 133
    if-gez v4, :cond_0

    .line 134
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    const/4 v0, 0x0

    .line 140
    :goto_1
    return-object v0

    .line 136
    :cond_0
    add-int/2addr v1, v4

    .line 137
    goto :goto_0

    .line 140
    :cond_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lflipboard/io/Download$Data;->g:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lflipboard/io/Download$Data;->h:I

    return v0
.end method

.method public abstract g()Ljava/io/InputStream;
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 157
    const-string v0, "data[%s,clen=%,d,ctype=%s,%dx%d]"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/io/Download$Data;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lflipboard/io/Download$Data;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lflipboard/io/Download$Data;->e:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lflipboard/io/Download$Data;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget v3, p0, Lflipboard/io/Download$Data;->h:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
