.class public Lflipboard/io/RequestLogEntry;
.super Lflipboard/objs/Base;
.source "RequestLogEntry.java"


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:J

.field public d:I

.field public e:J

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:Ljava/util/concurrent/atomic/AtomicLong;

.field public j:Lflipboard/json/FLObject;

.field public transient k:Ljava/io/ByteArrayOutputStream;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 25
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Lflipboard/io/RequestLogEntry;->i:Ljava/util/concurrent/atomic/AtomicLong;

    .line 12
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lflipboard/io/RequestLogEntry;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/io/RequestLogEntry;->b:Ljava/lang/String;

    const-string v1, "flipboard.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/io/RequestLogEntry;->b:Ljava/lang/String;

    const-string v1, "cdn.flipboard.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/io/RequestLogEntry;->b:Ljava/lang/String;

    const-string v1, "jira.flipboard.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/io/RequestLogEntry;->b:Ljava/lang/String;

    const-string v1, "flipboard.com/v1/static"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 35
    iput-wide v2, p0, Lflipboard/io/RequestLogEntry;->a:J

    .line 36
    iput-object v1, p0, Lflipboard/io/RequestLogEntry;->b:Ljava/lang/String;

    .line 37
    iput-wide v2, p0, Lflipboard/io/RequestLogEntry;->c:J

    .line 38
    iput v0, p0, Lflipboard/io/RequestLogEntry;->d:I

    .line 39
    iput-wide v2, p0, Lflipboard/io/RequestLogEntry;->e:J

    .line 40
    iput-object v1, p0, Lflipboard/io/RequestLogEntry;->f:Ljava/lang/String;

    .line 41
    iput-object v1, p0, Lflipboard/io/RequestLogEntry;->g:Ljava/lang/String;

    .line 42
    iput-boolean v0, p0, Lflipboard/io/RequestLogEntry;->h:Z

    .line 43
    iget-object v0, p0, Lflipboard/io/RequestLogEntry;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 44
    iput-object v1, p0, Lflipboard/io/RequestLogEntry;->j:Lflipboard/json/FLObject;

    .line 45
    iput-object v1, p0, Lflipboard/io/RequestLogEntry;->k:Ljava/io/ByteArrayOutputStream;

    .line 46
    return-void
.end method
