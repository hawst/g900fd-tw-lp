.class public Lflipboard/io/UsageEvent;
.super Ljava/lang/Object;
.source "UsageEvent.java"


# static fields
.field static a:Lflipboard/util/Log;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public e:J

.field public final f:Lflipboard/json/FLObject;

.field public g:J

.field h:Lflipboard/io/UsageEvent;

.field public i:Ljava/lang/String;

.field j:Ljava/lang/String;

.field public k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-string v0, "usage"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/io/UsageEvent;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object v1, p0, Lflipboard/io/UsageEvent;->i:Ljava/lang/String;

    .line 53
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 54
    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lflipboard/io/UsageEvent;->b:Ljava/lang/String;

    .line 55
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    if-nez v0, :cond_1

    .line 56
    :goto_1
    iput-object v1, p0, Lflipboard/io/UsageEvent;->c:Ljava/lang/String;

    .line 57
    iput-object p1, p0, Lflipboard/io/UsageEvent;->d:Ljava/lang/String;

    .line 58
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/io/UsageEvent;->e:J

    .line 59
    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    iput-object v0, p0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    .line 61
    return-void

    .line 54
    :cond_0
    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    goto :goto_0

    .line 55
    :cond_1
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    iget-object v1, v0, Lflipboard/io/UsageManager;->o:Ljava/lang/String;

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Number;Z)Lflipboard/json/FLObject;
    .locals 3

    .prologue
    .line 538
    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    .line 539
    const-string v1, "name"

    invoke-virtual {v0, v1, p0}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 540
    const-string v1, "value"

    invoke-virtual {v0, v1, p1}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 541
    if-eqz p2, :cond_0

    .line 542
    const-string v1, "aggregation"

    const-string v2, "sum"

    invoke-virtual {v0, v1, v2}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 544
    :cond_0
    return-object v0
.end method

.method public static a(JLjava/lang/String;Ljava/lang/String;Lflipboard/service/Section;)V
    .locals 4

    .prologue
    .line 275
    new-instance v1, Lflipboard/io/UsageEvent;

    const-string v0, "social"

    invoke-direct {v1, v0}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 276
    const-wide/16 v2, 0x0

    cmp-long v0, p0, v2

    if-lez v0, :cond_0

    .line 277
    iget-wide v2, v1, Lflipboard/io/UsageEvent;->e:J

    sub-long/2addr v2, p0

    iput-wide v2, v1, Lflipboard/io/UsageEvent;->e:J

    .line 279
    :cond_0
    iput-wide p0, v1, Lflipboard/io/UsageEvent;->g:J

    .line 280
    const-string v0, "type"

    const-string v2, "compose"

    invoke-virtual {v1, v0, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 281
    const-string v0, "compose"

    iput-object v0, v1, Lflipboard/io/UsageEvent;->j:Ljava/lang/String;

    .line 282
    sget-object v0, Lflipboard/io/UsageEvent;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    if-nez p2, :cond_4

    const-string v0, "null"

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x1

    const-string v3, "null"

    aput-object v3, v2, v0

    .line 283
    if-eqz p2, :cond_1

    .line 284
    const-string v0, "targetService"

    invoke-virtual {v1, v0, p2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 286
    :cond_1
    if-eqz p3, :cond_2

    .line 290
    const-string v0, "sourceURL"

    invoke-virtual {v1, v0, p3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 292
    :cond_2
    if-eqz p4, :cond_3

    .line 293
    const-string v0, "sectionPartnerID"

    iget-object v2, p4, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v2, v2, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295
    :cond_3
    invoke-virtual {v1}, Lflipboard/io/UsageEvent;->a()V

    .line 296
    return-void

    :cond_4
    move-object v0, p2

    .line 282
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;JLflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UserState$TargetAuthor;)V
    .locals 5

    .prologue
    .line 216
    new-instance v1, Lflipboard/io/UsageEvent;

    const-string v0, "social"

    invoke-direct {v1, v0}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 217
    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-lez v0, :cond_0

    .line 218
    iget-wide v2, v1, Lflipboard/io/UsageEvent;->e:J

    sub-long/2addr v2, p1

    iput-wide v2, v1, Lflipboard/io/UsageEvent;->e:J

    .line 220
    :cond_0
    iput-wide p1, v1, Lflipboard/io/UsageEvent;->g:J

    .line 221
    if-eqz p4, :cond_2

    .line 222
    iget-object v0, p4, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 223
    iget-object v0, p4, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    iput-object v0, v1, Lflipboard/io/UsageEvent;->i:Ljava/lang/String;

    .line 225
    :cond_1
    const-string v0, "partnerID"

    invoke-virtual {p4}, Lflipboard/objs/FeedItem;->aa()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 228
    :cond_2
    const-string v0, "type"

    invoke-virtual {v1, v0, p0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 229
    if-eqz p5, :cond_3

    iget-object v0, p5, Lflipboard/objs/UserState$TargetAuthor;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 230
    const-string v0, "targetService"

    iget-object v2, p5, Lflipboard/objs/UserState$TargetAuthor;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 231
    iget-object v0, p5, Lflipboard/objs/UserState$TargetAuthor;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 232
    const-string v0, "targetId"

    iget-object v2, p5, Lflipboard/objs/UserState$TargetAuthor;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 235
    :cond_3
    const/4 v0, 0x0

    .line 236
    if-eqz p3, :cond_7

    invoke-virtual {p3}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 237
    invoke-virtual {p3}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    .line 241
    :cond_4
    :goto_0
    if-eqz p3, :cond_5

    .line 242
    const-string v2, "sectionPartnerID"

    iget-object v3, p3, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v3, v3, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 244
    :cond_5
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_8

    .line 245
    const-string v2, "sectionIdentifier"

    invoke-virtual {v1, v2, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 249
    :goto_1
    if-eqz p4, :cond_6

    .line 250
    const-string v0, "sourceURL"

    iget-object v2, p4, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 251
    invoke-virtual {v1, p4}, Lflipboard/io/UsageEvent;->a(Lflipboard/objs/FeedItem;)V

    .line 253
    :cond_6
    invoke-virtual {v1}, Lflipboard/io/UsageEvent;->a()V

    .line 254
    return-void

    .line 238
    :cond_7
    if-eqz p4, :cond_4

    iget-object v2, p4, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 239
    iget-object v0, p4, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    goto :goto_0

    .line 247
    :cond_8
    sget-object v0, Lflipboard/io/UsageEvent;->a:Lflipboard/util/Log;

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 303
    new-instance v1, Lflipboard/io/UsageEvent;

    const-string v0, "configure"

    invoke-direct {v1, v0}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 304
    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-lez v0, :cond_0

    .line 305
    iget-wide v2, v1, Lflipboard/io/UsageEvent;->e:J

    sub-long/2addr v2, p1

    iput-wide v2, v1, Lflipboard/io/UsageEvent;->e:J

    .line 307
    :cond_0
    iput-wide p1, v1, Lflipboard/io/UsageEvent;->g:J

    .line 308
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    .line 309
    const-string v0, "serviceAction"

    invoke-virtual {v1, v0, p0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 311
    :cond_1
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_3

    .line 315
    const-string v0, "service"

    invoke-virtual {v1, v0, p3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 316
    const-string v0, "nytimes"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 317
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p3}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 318
    if-eqz v0, :cond_2

    .line 319
    const-string v2, "subscriptionLevel"

    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 322
    :cond_2
    const-string v0, "ft"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 323
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p3}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 324
    if-eqz v0, :cond_3

    .line 325
    const-string v2, "subscriptionLevel"

    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 329
    :cond_3
    if-eqz p5, :cond_4

    .line 330
    invoke-virtual {p5}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 331
    invoke-virtual {p5, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 334
    :cond_4
    if-eqz p4, :cond_5

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_5

    .line 335
    const-string v0, "loginType"

    invoke-virtual {v1, v0, p4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 337
    :cond_5
    invoke-virtual {v1}, Lflipboard/io/UsageEvent;->a()V

    .line 344
    return-void
.end method

.method public static a(ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 258
    new-instance v1, Lflipboard/io/UsageEvent;

    const-string v0, "social"

    invoke-direct {v1, v0}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 259
    const-string v0, "count"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 260
    const-string v2, "exitPath"

    if-eqz p0, :cond_0

    const-string v0, "success"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 261
    const-string v0, "from"

    const-string v2, "promote_mag_prompt"

    invoke-virtual {v1, v0, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 262
    const-string v0, "isSectionShare"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 263
    const-string v0, "itemType"

    const-string v2, "section"

    invoke-virtual {v1, v0, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 264
    const-string v0, "sectionIdentifier"

    invoke-virtual {v1, v0, p2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 265
    const-string v0, "sourceURL"

    invoke-virtual {v1, v0, p3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 266
    const-string v0, "targetService"

    invoke-virtual {v1, v0, p4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 267
    const-string v0, "type"

    const-string v2, "shared"

    invoke-virtual {v1, v0, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 268
    invoke-virtual {v1}, Lflipboard/io/UsageEvent;->a()V

    .line 269
    return-void

    .line 260
    :cond_0
    const-string v0, "fail"

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;J)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 487
    invoke-static {}, Lflipboard/io/UsageEvent;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490
    long-to-double v0, p1

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    .line 492
    :try_start_0
    new-instance v2, Lflipboard/io/UsageEvent;

    const-string v3, "performance"

    invoke-direct {v2, v3}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 494
    new-instance v3, Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 495
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Number;Z)Lflipboard/json/FLObject;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 496
    const-string v0, "metrics"

    invoke-virtual {v2, v0, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 497
    invoke-static {}, Lflipboard/io/UsageEvent;->f()F

    move-result v0

    .line 498
    const-string v1, "sample"

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 499
    invoke-virtual {v2}, Lflipboard/io/UsageEvent;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 504
    :cond_0
    :goto_0
    return-void

    .line 500
    :catch_0
    move-exception v0

    .line 501
    sget-object v1, Lflipboard/io/UsageEvent;->a:Lflipboard/util/Log;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 384
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "event"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "id"

    invoke-virtual {v0, v1, p0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1, p2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 385
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 368
    new-instance v2, Lflipboard/io/UsageEvent;

    const-string v0, "event"

    invoke-direct {v2, v0}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "id"

    invoke-virtual {v2, v0, p0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Lflipboard/io/UsageEvent;->a()V

    .line 369
    return-void
.end method

.method public static c(Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 388
    invoke-static {p0, v0, v0}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 389
    return-void
.end method

.method public static e(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 514
    invoke-static {}, Lflipboard/io/UsageEvent;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 516
    :try_start_0
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "performance"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 518
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 519
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {p0, v2, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Number;Z)Lflipboard/json/FLObject;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 520
    const-string v2, "metrics"

    invoke-virtual {v0, v2, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 521
    invoke-static {}, Lflipboard/io/UsageEvent;->f()F

    move-result v1

    .line 522
    const-string v2, "sample"

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 523
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 528
    :cond_0
    :goto_0
    return-void

    .line 524
    :catch_0
    move-exception v0

    .line 525
    sget-object v1, Lflipboard/io/UsageEvent;->a:Lflipboard/util/Log;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static e()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 467
    invoke-static {}, Lflipboard/io/UsageEvent;->f()F

    move-result v2

    .line 468
    const/4 v3, 0x0

    cmpl-float v3, v2, v3

    if-nez v3, :cond_0

    .line 475
    :goto_0
    return v0

    .line 470
    :cond_0
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v2, v3

    if-nez v3, :cond_1

    move v0, v1

    .line 471
    goto :goto_0

    .line 473
    :cond_1
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    .line 474
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v4, 0x3e8

    const/high16 v5, 0x447a0000    # 1000.0f

    mul-float/2addr v2, v5

    float-to-int v2, v2

    invoke-static {v0, v3, v4, v1, v2}, Lflipboard/abtest/PseudoRandom;->a(Ljava/lang/Integer;Ljava/lang/String;III)Z

    move-result v0

    goto :goto_0
.end method

.method private static f()F
    .locals 3

    .prologue
    .line 462
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget v0, v0, Lflipboard/model/ConfigSetting;->PerformanceUsageSample:F

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2}, Lflipboard/util/JavaUtil;->a(FFF)F

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0}, Lflipboard/io/UsageEvent;->c()V

    .line 130
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    iget-object v0, v0, Lflipboard/io/UsageManager;->n:Lflipboard/io/UsageManager$Uploader;

    invoke-virtual {v0, p0}, Lflipboard/io/UsageManager$Uploader;->a(Lflipboard/io/UsageEvent;)V

    .line 131
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 123
    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0, p1}, Lflipboard/json/FLObject;-><init>(Landroid/os/Bundle;)V

    .line 124
    const-string v1, "from"

    invoke-virtual {p0, v1, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 125
    return-void
.end method

.method public final a(Lflipboard/objs/FeedItem;)V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 97
    if-eqz p1, :cond_2

    iget-object v0, p1, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 98
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 101
    if-eqz v0, :cond_0

    .line 102
    iget-object v1, p0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    const-string v3, "source"

    invoke-virtual {v1, v3}, Lflipboard/json/FLObject;->g(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 103
    if-nez v1, :cond_1

    .line 104
    iget-object v3, p0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    const-string v4, "source"

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3, v4, v1}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    :cond_1
    new-instance v3, Lflipboard/json/FLObject;

    const-string v4, "id"

    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, v0, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    aput-object v6, v5, v7

    const-string v6, "serviceId"

    aput-object v6, v5, v8

    iget-object v0, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    aput-object v0, v5, v9

    invoke-direct {v3, v4, v5}, Lflipboard/json/FLObject;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 109
    :cond_2
    if-eqz p1, :cond_4

    iget-object v0, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 110
    sget-object v0, Lflipboard/io/UsageEvent;->a:Lflipboard/util/Log;

    .line 111
    sget-object v0, Lflipboard/io/UsageEvent;->a:Lflipboard/util/Log;

    new-array v0, v9, [Ljava/lang/Object;

    iget-object v1, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    aput-object v1, v0, v7

    iget-object v1, p1, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    aput-object v1, v0, v8

    .line 112
    iget-object v0, p0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    const-string v1, "source"

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->g(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 113
    if-nez v0, :cond_3

    .line 114
    iget-object v1, p0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    const-string v2, "source"

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v2, v0}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    :cond_3
    new-instance v1, Lflipboard/json/FLObject;

    const-string v2, "id"

    new-array v3, v10, [Ljava/lang/Object;

    iget-object v4, p1, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    aput-object v4, v3, v7

    const-string v4, "serviceId"

    aput-object v4, v3, v8

    iget-object v4, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    aput-object v4, v3, v9

    invoke-direct {v1, v2, v3}, Lflipboard/json/FLObject;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    :cond_4
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 71
    if-eqz p1, :cond_0

    .line 76
    if-nez p2, :cond_1

    .line 77
    iget-object v0, p0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    invoke-virtual {v0, p1}, Lflipboard/json/FLObject;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    iget-object v0, p0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    invoke-virtual {v0, p1, p2}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 85
    const/4 v0, 0x0

    .line 86
    iget-object v1, p0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    if-eqz v1, :cond_0

    .line 87
    iget-object v0, p0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    invoke-virtual {v0, p1}, Lflipboard/json/FLObject;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 89
    :cond_0
    return-object v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 150
    new-instance v0, Lflipboard/io/UsageEvent$1;

    invoke-direct {v0, p0}, Lflipboard/io/UsageEvent$1;-><init>(Lflipboard/io/UsageEvent;)V

    .line 158
    sget-object v1, Lflipboard/usage/UsageManagerV2;->b:Lflipboard/usage/UsageManagerV2;

    iget-object v1, v1, Lflipboard/usage/UsageManagerV2;->l:Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 159
    return-void
.end method

.method final c()V
    .locals 3

    .prologue
    .line 163
    const-string v0, "OS"

    const-string v1, "android"

    invoke-virtual {p0, v0, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 164
    const-string v0, "OSVersion"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 165
    const-string v0, "deviceModel"

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 166
    const-string v0, "deviceVersion"

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 167
    const-string v0, "appMode"

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->f:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 168
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    if-eqz v0, :cond_0

    .line 169
    const-string v1, "network"

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v2

    if-nez v2, :cond_4

    const-string v0, "none"

    :goto_0
    invoke-virtual {p0, v1, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 171
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v0, :cond_2

    .line 172
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->H()Ljava/lang/String;

    move-result-object v0

    .line 173
    if-eqz v0, :cond_1

    .line 174
    const-string v0, "activatedVersion"

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 176
    :cond_1
    invoke-static {}, Lflipboard/abtest/Experiments;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 177
    const-string v0, "tests"

    invoke-static {}, Lflipboard/abtest/Experiments;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 180
    :cond_2
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_3

    .line 181
    const-string v0, "variant"

    const-string v1, "china"

    invoke-virtual {p0, v0, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 183
    :cond_3
    return-void

    .line 169
    :cond_4
    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v0, "wifi"

    goto :goto_0

    :cond_5
    iget-object v2, v0, Lflipboard/io/NetworkManager;->j:Landroid/telephony/TelephonyManager;

    if-eqz v2, :cond_6

    iget-object v0, v0, Lflipboard/io/NetworkManager;->j:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_6
    const-string v0, "unknown"

    goto :goto_0

    :pswitch_0
    const-string v0, "gprs"

    goto :goto_0

    :pswitch_1
    const-string v0, "edge"

    goto :goto_0

    :pswitch_2
    const-string v0, "umts"

    goto :goto_0

    :pswitch_3
    const-string v0, "hsdpa"

    goto :goto_0

    :pswitch_4
    const-string v0, "hudpa"

    goto :goto_0

    :pswitch_5
    const-string v0, "hspa"

    goto :goto_0

    :pswitch_6
    const-string v0, "cdma"

    goto :goto_0

    :pswitch_7
    const-string v0, "evdo_0"

    goto :goto_0

    :pswitch_8
    const-string v0, "evdo_a"

    goto :goto_0

    :pswitch_9
    const-string v0, "evdo_b"

    goto :goto_0

    :pswitch_a
    const-string v0, "1xRTT"

    goto :goto_0

    :pswitch_b
    const-string v0, "iden"

    goto :goto_0

    :pswitch_c
    const-string v0, "lte"

    goto :goto_0

    :pswitch_d
    const-string v0, "ehrpd"

    goto :goto_0

    :pswitch_e
    const-string v0, "hspap"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_b
        :pswitch_9
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public final d(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 442
    if-eqz p1, :cond_2

    const-string v2, "nytimes"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 443
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v3, "nytimes"

    invoke-virtual {v2, v3}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v2

    .line 444
    if-eqz v2, :cond_1

    .line 445
    :goto_0
    const-string v1, "authenticated"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {p0, v1, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 446
    if-eqz v0, :cond_0

    .line 447
    const-string v0, "subscribed"

    iget-object v1, v2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 448
    const-string v0, "entitled"

    invoke-virtual {v2}, Lflipboard/service/Account;->i()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 458
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 444
    goto :goto_0

    .line 450
    :cond_2
    if-eqz p1, :cond_0

    const-string v2, "ft"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 451
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v3, "ft"

    invoke-virtual {v2, v3}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v2

    .line 452
    if-eqz v2, :cond_3

    .line 453
    :goto_2
    const-string v1, "authenticated"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {p0, v1, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 454
    if-eqz v0, :cond_0

    .line 455
    const-string v0, "subscribed"

    iget-object v1, v2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    move v0, v1

    .line 452
    goto :goto_2
.end method

.method final d()[B
    .locals 10

    .prologue
    .line 187
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x400

    invoke-direct {v1, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 189
    :try_start_0
    new-instance v0, Lflipboard/io/UTF8StreamWriter;

    invoke-direct {v0, v1}, Lflipboard/io/UTF8StreamWriter;-><init>(Ljava/io/OutputStream;)V

    .line 190
    const-string v2, "{\"sessionId\":\"%s\","

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lflipboard/io/UsageEvent;->c:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 191
    iget-object v2, p0, Lflipboard/io/UsageEvent;->i:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 192
    const-string v2, "\"contentid\":\"%s\","

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lflipboard/io/UsageEvent;->i:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 194
    :cond_0
    const-string v2, "\"interactionType\":\"%s\","

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lflipboard/io/UsageEvent;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 195
    const-string v2, "\"time\":%d.%03d,"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, p0, Lflipboard/io/UsageEvent;->e:J

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-wide v6, p0, Lflipboard/io/UsageEvent;->e:J

    const-wide/16 v8, 0x3e8

    rem-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 196
    iget-wide v2, p0, Lflipboard/io/UsageEvent;->g:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 197
    const-string v2, "\"duration\":%d.%03d,"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, p0, Lflipboard/io/UsageEvent;->g:J

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-wide v6, p0, Lflipboard/io/UsageEvent;->g:J

    const-wide/16 v8, 0x3e8

    rem-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 199
    :cond_1
    const-string v2, "\"interactionData\":"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 200
    iget-object v2, p0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    invoke-static {v2}, Lflipboard/json/JSONSerializer;->c(Ljava/lang/Object;)[B

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/io/UTF8StreamWriter;->a([B)V

    .line 201
    const-string v2, "}"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    :goto_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    .line 202
    :catch_0
    move-exception v0

    .line 203
    sget-object v2, Lflipboard/io/UsageEvent;->a:Lflipboard/util/Log;

    invoke-virtual {v2, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    invoke-virtual {p0}, Lflipboard/io/UsageEvent;->d()[B

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/JavaUtil;->a([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
