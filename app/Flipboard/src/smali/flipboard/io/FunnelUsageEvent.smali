.class public Lflipboard/io/FunnelUsageEvent;
.super Lflipboard/io/UsageEvent;
.source "FunnelUsageEvent.java"


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/io/FunnelUsageEvent;->k:Z

    .line 14
    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lflipboard/io/FunnelUsageEvent;

    const-string v1, "event"

    invoke-direct {v0, v1}, Lflipboard/io/FunnelUsageEvent;-><init>(Ljava/lang/String;)V

    .line 24
    const-string v1, "id"

    invoke-virtual {v0, v1, p0}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 25
    invoke-virtual {v0}, Lflipboard/io/FunnelUsageEvent;->a()V

    .line 26
    return-void
.end method

.method public static a(Ljava/lang/String;J)V
    .locals 5

    .prologue
    .line 34
    new-instance v0, Lflipboard/io/FunnelUsageEvent;

    const-string v1, "event"

    invoke-direct {v0, v1}, Lflipboard/io/FunnelUsageEvent;-><init>(Ljava/lang/String;)V

    .line 35
    const-string v1, "id"

    invoke-virtual {v0, v1, p0}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 36
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-lez v1, :cond_0

    .line 37
    iput-wide p1, v0, Lflipboard/io/FunnelUsageEvent;->g:J

    .line 39
    :cond_0
    invoke-virtual {v0}, Lflipboard/io/FunnelUsageEvent;->a()V

    .line 40
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 44
    new-instance v0, Lflipboard/io/FunnelUsageEvent;

    const-string v1, "event"

    invoke-direct {v0, v1}, Lflipboard/io/FunnelUsageEvent;-><init>(Ljava/lang/String;)V

    .line 45
    const-string v1, "id"

    invoke-virtual {v0, v1, p0}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 46
    invoke-virtual {v0, p1, p2}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 49
    invoke-virtual {v0}, Lflipboard/io/FunnelUsageEvent;->a()V

    .line 50
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v2, Lflipboard/io/FunnelUsageEvent;

    const-string v0, "event"

    invoke-direct {v2, v0}, Lflipboard/io/FunnelUsageEvent;-><init>(Ljava/lang/String;)V

    .line 55
    const-string v0, "id"

    invoke-virtual {v2, v0, p0}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 56
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 58
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 61
    :cond_0
    invoke-virtual {v2}, Lflipboard/io/FunnelUsageEvent;->a()V

    .line 62
    return-void
.end method
