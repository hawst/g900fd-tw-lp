.class public final enum Lflipboard/io/Download$Status;
.super Ljava/lang/Enum;
.source "Download.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/io/Download$Status;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/io/Download$Status;

.field public static final enum b:Lflipboard/io/Download$Status;

.field public static final enum c:Lflipboard/io/Download$Status;

.field public static final enum d:Lflipboard/io/Download$Status;

.field public static final enum e:Lflipboard/io/Download$Status;

.field public static final enum f:Lflipboard/io/Download$Status;

.field public static final enum g:Lflipboard/io/Download$Status;

.field public static final enum h:Lflipboard/io/Download$Status;

.field public static final enum i:Lflipboard/io/Download$Status;

.field private static final synthetic j:[Lflipboard/io/Download$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 27
    new-instance v0, Lflipboard/io/Download$Status;

    const-string v1, "Pending"

    invoke-direct {v0, v1, v3}, Lflipboard/io/Download$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/io/Download$Status;->a:Lflipboard/io/Download$Status;

    .line 28
    new-instance v0, Lflipboard/io/Download$Status;

    const-string v1, "Scheduled"

    invoke-direct {v0, v1, v4}, Lflipboard/io/Download$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/io/Download$Status;->b:Lflipboard/io/Download$Status;

    .line 29
    new-instance v0, Lflipboard/io/Download$Status;

    const-string v1, "Progress"

    invoke-direct {v0, v1, v5}, Lflipboard/io/Download$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/io/Download$Status;->c:Lflipboard/io/Download$Status;

    .line 30
    new-instance v0, Lflipboard/io/Download$Status;

    const-string v1, "Loading"

    invoke-direct {v0, v1, v6}, Lflipboard/io/Download$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/io/Download$Status;->d:Lflipboard/io/Download$Status;

    .line 31
    new-instance v0, Lflipboard/io/Download$Status;

    const-string v1, "Paused"

    invoke-direct {v0, v1, v7}, Lflipboard/io/Download$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/io/Download$Status;->e:Lflipboard/io/Download$Status;

    .line 32
    new-instance v0, Lflipboard/io/Download$Status;

    const-string v1, "Timeout"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/io/Download$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/io/Download$Status;->f:Lflipboard/io/Download$Status;

    .line 33
    new-instance v0, Lflipboard/io/Download$Status;

    const-string v1, "Failed"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lflipboard/io/Download$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/io/Download$Status;->g:Lflipboard/io/Download$Status;

    .line 34
    new-instance v0, Lflipboard/io/Download$Status;

    const-string v1, "Ready"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lflipboard/io/Download$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/io/Download$Status;->h:Lflipboard/io/Download$Status;

    .line 35
    new-instance v0, Lflipboard/io/Download$Status;

    const-string v1, "NotFound"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lflipboard/io/Download$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/io/Download$Status;->i:Lflipboard/io/Download$Status;

    .line 24
    const/16 v0, 0x9

    new-array v0, v0, [Lflipboard/io/Download$Status;

    sget-object v1, Lflipboard/io/Download$Status;->a:Lflipboard/io/Download$Status;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/io/Download$Status;->b:Lflipboard/io/Download$Status;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/io/Download$Status;->c:Lflipboard/io/Download$Status;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/io/Download$Status;->d:Lflipboard/io/Download$Status;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/io/Download$Status;->e:Lflipboard/io/Download$Status;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/io/Download$Status;->f:Lflipboard/io/Download$Status;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lflipboard/io/Download$Status;->g:Lflipboard/io/Download$Status;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lflipboard/io/Download$Status;->h:Lflipboard/io/Download$Status;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lflipboard/io/Download$Status;->i:Lflipboard/io/Download$Status;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/io/Download$Status;->j:[Lflipboard/io/Download$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/io/Download$Status;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lflipboard/io/Download$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/io/Download$Status;

    return-object v0
.end method

.method public static values()[Lflipboard/io/Download$Status;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lflipboard/io/Download$Status;->j:[Lflipboard/io/Download$Status;

    invoke-virtual {v0}, [Lflipboard/io/Download$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/io/Download$Status;

    return-object v0
.end method
