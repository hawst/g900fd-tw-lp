.class public Lflipboard/io/BitmapManager;
.super Ljava/lang/Object;
.source "BitmapManager.java"


# static fields
.field public static final a:Lflipboard/util/Log;

.field public static b:Lflipboard/io/BitmapManager;

.field private static final f:Landroid/graphics/Bitmap;


# instance fields
.field final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lflipboard/io/BitmapManager$Handle;",
            ">;"
        }
    .end annotation
.end field

.field d:I

.field public e:Landroid/os/Handler;

.field private final g:Landroid/content/Context;

.field private h:Lflipboard/io/BitmapManager$Handle;

.field private i:Lflipboard/io/BitmapManager$Handle;

.field private final j:I

.field private final k:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 58
    const-string v0, "bitmaps"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    .line 61
    new-array v0, v2, [I

    const/4 v3, -0x1

    aput v3, v0, v1

    sget-object v5, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move v3, v2

    move v4, v2

    invoke-static/range {v0 .. v5}, Landroid/graphics/Bitmap;->createBitmap([IIIIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lflipboard/io/BitmapManager;->f:Landroid/graphics/Bitmap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    sput-object p0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    .line 78
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    .line 79
    iput-object p1, p0, Lflipboard/io/BitmapManager;->g:Landroid/content/Context;

    .line 80
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lflipboard/io/BitmapManager;->c:Ljava/util/Map;

    .line 81
    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    const-wide/16 v4, 0xa

    div-long/2addr v2, v4

    const-wide/32 v4, 0x2625a00

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 82
    long-to-int v1, v2

    div-int/lit8 v1, v1, 0x4

    iput v1, p0, Lflipboard/io/BitmapManager;->j:I

    .line 83
    iget v1, p0, Lflipboard/io/BitmapManager;->j:I

    div-int/lit8 v1, v1, 0x4

    iput v1, p0, Lflipboard/io/BitmapManager;->k:I

    .line 85
    sget-object v1, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x2

    iget v2, p0, Lflipboard/io/BitmapManager;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x3

    iget v2, p0, Lflipboard/io/BitmapManager;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 88
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    new-instance v1, Lflipboard/io/BitmapManager$1;

    invoke-direct {v1, p0}, Lflipboard/io/BitmapManager$1;-><init>(Lflipboard/io/BitmapManager;)V

    invoke-virtual {v0, v1}, Lflipboard/app/FlipboardApplication;->a(Lflipboard/util/Observer;)V

    .line 94
    return-void
.end method

.method static synthetic a(Lflipboard/io/BitmapManager;)I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lflipboard/io/BitmapManager;->j:I

    return v0
.end method

.method static synthetic a(Lflipboard/io/BitmapManager;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lflipboard/io/BitmapManager;->e:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic a(Lflipboard/io/BitmapManager;Lflipboard/io/BitmapManager$Handle;)V
    .locals 0

    .prologue
    .line 56
    invoke-virtual {p0, p1}, Lflipboard/io/BitmapManager;->b(Lflipboard/io/BitmapManager$Handle;)V

    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 1190
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    .line 1191
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v6

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v8

    sub-long/2addr v6, v8

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1192
    monitor-exit p0

    return-void

    .line 1190
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lflipboard/io/BitmapManager;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lflipboard/io/BitmapManager;->c:Ljava/util/Map;

    return-object v0
.end method

.method static b(I)V
    .locals 10

    .prologue
    .line 311
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    .line 312
    invoke-virtual {v0}, Ljava/lang/Runtime;->gc()V

    .line 313
    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v6

    sub-long/2addr v4, v6

    sub-long/2addr v2, v4

    .line 314
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v4, "totally out of memory, needed %,d pixels, max=%,d, total=%,d, free=%,d available=%,d"

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v0

    invoke-virtual {v1, v4, v5}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 329
    return-void
.end method

.method static synthetic b(Lflipboard/io/BitmapManager;Lflipboard/io/BitmapManager$Handle;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lflipboard/io/BitmapManager;->d(Lflipboard/io/BitmapManager$Handle;)V

    return-void
.end method

.method static synthetic c(Lflipboard/io/BitmapManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lflipboard/io/BitmapManager;->g:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lflipboard/io/BitmapManager;Lflipboard/io/BitmapManager$Handle;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 56
    iget v0, p1, Lflipboard/io/BitmapManager$Handle;->h:I

    iget v1, p1, Lflipboard/io/BitmapManager$Handle;->i:I

    mul-int/2addr v0, v1

    monitor-enter p0

    :try_start_0
    iget v1, p0, Lflipboard/io/BitmapManager;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Lflipboard/io/BitmapManager;->d:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget v0, p0, Lflipboard/io/BitmapManager;->d:I

    iget v1, p0, Lflipboard/io/BitmapManager;->j:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lflipboard/io/BitmapManager;->j:I

    mul-int/lit8 v0, v0, 0x4b

    div-int/lit8 v0, v0, 0x64

    invoke-virtual {p0, v0, v4}, Lflipboard/io/BitmapManager;->b(IZ)V

    iget v0, p0, Lflipboard/io/BitmapManager;->d:I

    iget v1, p0, Lflipboard/io/BitmapManager;->j:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lflipboard/io/BitmapManager;->j:I

    mul-int/lit8 v0, v0, 0x4b

    div-int/lit8 v0, v0, 0x64

    invoke-virtual {p0, v0, v4}, Lflipboard/io/BitmapManager;->a(IZ)V

    :cond_0
    iget v0, p0, Lflipboard/io/BitmapManager;->d:I

    iget v1, p0, Lflipboard/io/BitmapManager;->j:I

    if-le v0, v1, :cond_1

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "exceeding bitmap budget: %,d pixels of %,d (%d%%)"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lflipboard/io/BitmapManager;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    iget v4, p0, Lflipboard/io/BitmapManager;->j:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lflipboard/io/BitmapManager;->d:I

    mul-int/lit8 v4, v4, 0x64

    iget v5, p0, Lflipboard/io/BitmapManager;->j:I

    div-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic d(Lflipboard/io/BitmapManager;)I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lflipboard/io/BitmapManager;->k:I

    return v0
.end method

.method static synthetic d()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lflipboard/io/BitmapManager;->f:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private declared-synchronized d(Lflipboard/io/BitmapManager$Handle;)V
    .locals 1

    .prologue
    .line 218
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lflipboard/io/BitmapManager;->h:Lflipboard/io/BitmapManager$Handle;

    if-nez v0, :cond_0

    .line 219
    iput-object p1, p0, Lflipboard/io/BitmapManager;->i:Lflipboard/io/BitmapManager$Handle;

    iput-object p1, p0, Lflipboard/io/BitmapManager;->h:Lflipboard/io/BitmapManager$Handle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    :goto_0
    monitor-exit p0

    return-void

    .line 221
    :cond_0
    :try_start_1
    iget-object v0, p0, Lflipboard/io/BitmapManager;->h:Lflipboard/io/BitmapManager$Handle;

    iput-object p1, v0, Lflipboard/io/BitmapManager$Handle;->e:Lflipboard/io/BitmapManager$Handle;

    .line 222
    iget-object v0, p0, Lflipboard/io/BitmapManager;->h:Lflipboard/io/BitmapManager$Handle;

    iput-object v0, p1, Lflipboard/io/BitmapManager$Handle;->f:Lflipboard/io/BitmapManager$Handle;

    .line 223
    iput-object p1, p0, Lflipboard/io/BitmapManager;->h:Lflipboard/io/BitmapManager$Handle;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 218
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 139
    :try_start_0
    invoke-static {p1, p2, p3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 143
    :goto_0
    return-object v0

    .line 141
    :catch_0
    move-exception v0

    mul-int v0, p1, p2

    invoke-virtual {p0, v0}, Lflipboard/io/BitmapManager;->c(I)V

    .line 143
    :try_start_1
    invoke-static {p1, p2, p3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 144
    :catch_1
    move-exception v0

    .line 145
    mul-int v1, p1, p2

    invoke-static {v1}, Lflipboard/io/BitmapManager;->b(I)V

    .line 146
    throw v0
.end method

.method public final declared-synchronized a(I)Lflipboard/io/BitmapManager$Handle;
    .locals 3

    .prologue
    .line 157
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 158
    iget-object v0, p0, Lflipboard/io/BitmapManager;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/BitmapManager$Handle;

    .line 159
    if-nez v0, :cond_0

    .line 160
    iget-object v2, p0, Lflipboard/io/BitmapManager;->c:Ljava/util/Map;

    new-instance v0, Lflipboard/io/BitmapManager$ResourceHandle;

    invoke-direct {v0, p0, p1}, Lflipboard/io/BitmapManager$ResourceHandle;-><init>(Lflipboard/io/BitmapManager;I)V

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    :cond_0
    iget v1, v0, Lflipboard/io/BitmapManager$Handle;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lflipboard/io/BitmapManager$Handle;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    monitor-exit p0

    return-object v0

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/graphics/Bitmap;)Lflipboard/io/BitmapManager$Handle;
    .locals 1

    .prologue
    .line 211
    monitor-enter p0

    :try_start_0
    new-instance v0, Lflipboard/io/BitmapManager$BitmapHandle;

    invoke-direct {v0, p0, p1}, Lflipboard/io/BitmapManager$BitmapHandle;-><init>(Lflipboard/io/BitmapManager;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    monitor-exit p0

    return-object v0

    .line 211
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lflipboard/io/BitmapManager$Handle;)Lflipboard/io/BitmapManager$Handle;
    .locals 1

    .prologue
    .line 198
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 199
    :try_start_0
    iget v0, p1, Lflipboard/io/BitmapManager$Handle;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lflipboard/io/BitmapManager$Handle;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    :cond_0
    monitor-exit p0

    return-object p1

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lflipboard/io/Download;Z)Lflipboard/io/BitmapManager$Handle;
    .locals 3

    .prologue
    .line 177
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lflipboard/io/Download;->b:Ljava/lang/String;

    .line 178
    if-eqz p2, :cond_1

    .line 179
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "hires:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 181
    :goto_0
    iget-object v0, p0, Lflipboard/io/BitmapManager;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/BitmapManager$Handle;

    .line 182
    if-nez v0, :cond_0

    .line 183
    iget-object v2, p0, Lflipboard/io/BitmapManager;->c:Ljava/util/Map;

    new-instance v0, Lflipboard/io/BitmapManager$DownloadHandle;

    invoke-direct {v0, p0, v1, p1, p2}, Lflipboard/io/BitmapManager$DownloadHandle;-><init>(Lflipboard/io/BitmapManager;Ljava/lang/String;Lflipboard/io/Download;Z)V

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    :goto_1
    iget v1, v0, Lflipboard/io/BitmapManager$Handle;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lflipboard/io/BitmapManager$Handle;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    monitor-exit p0

    return-object v0

    .line 185
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lflipboard/io/Download;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    new-instance v0, Lflipboard/io/BitmapManager$2;

    const-string v1, "bitmap-mgr"

    invoke-direct {v0, p0, v1}, Lflipboard/io/BitmapManager$2;-><init>(Lflipboard/io/BitmapManager;Ljava/lang/String;)V

    .line 109
    invoke-virtual {v0}, Lflipboard/io/BitmapManager$2;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    :goto_0
    :try_start_1
    iget-object v0, p0, Lflipboard/io/BitmapManager;->e:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 113
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 115
    :catch_0
    move-exception v0

    .line 116
    :try_start_2
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 118
    :cond_0
    monitor-exit p0

    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(IZ)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 351
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 353
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 354
    monitor-enter p0

    move v1, v2

    .line 356
    :goto_0
    :try_start_0
    iget v0, p0, Lflipboard/io/BitmapManager;->d:I

    if-le v0, p1, :cond_1

    iget-object v0, p0, Lflipboard/io/BitmapManager;->i:Lflipboard/io/BitmapManager$Handle;

    if-eqz v0, :cond_1

    .line 357
    iget-object v6, p0, Lflipboard/io/BitmapManager;->i:Lflipboard/io/BitmapManager$Handle;

    .line 358
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    iget v0, v6, Lflipboard/io/BitmapManager$Handle;->h:I

    iget v7, v6, Lflipboard/io/BitmapManager$Handle;->i:I

    mul-int/2addr v0, v7

    .line 360
    iget v7, p0, Lflipboard/io/BitmapManager;->d:I

    sub-int/2addr v7, v0

    iput v7, p0, Lflipboard/io/BitmapManager;->d:I

    .line 361
    add-int/2addr v0, v1

    .line 362
    invoke-virtual {p0, v6}, Lflipboard/io/BitmapManager;->b(Lflipboard/io/BitmapManager$Handle;)V

    .line 364
    iget v1, v6, Lflipboard/io/BitmapManager$Handle;->d:I

    if-nez v1, :cond_0

    .line 365
    iget-object v1, p0, Lflipboard/io/BitmapManager;->c:Ljava/util/Map;

    invoke-virtual {v6}, Lflipboard/io/BitmapManager$Handle;->a()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    invoke-virtual {v6}, Lflipboard/io/BitmapManager$Handle;->g()V

    :cond_0
    move v1, v0

    .line 368
    goto :goto_0

    .line 369
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 370
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/BitmapManager$Handle;

    .line 371
    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->f()V

    goto :goto_1

    .line 369
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 373
    :cond_2
    if-nez p2, :cond_3

    sget-object v0, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_4

    .line 374
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    .line 375
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x1

    iget v2, p0, Lflipboard/io/BitmapManager;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lflipboard/io/BitmapManager;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lflipboard/io/BitmapManager;->d:I

    mul-int/lit8 v2, v2, 0x64

    iget v3, p0, Lflipboard/io/BitmapManager;->j:I

    div-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    .line 378
    :cond_4
    return-void
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 1178
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    .line 1179
    const-string v1, "heap before         "

    invoke-direct {p0, v1}, Lflipboard/io/BitmapManager;->a(Ljava/lang/String;)V

    .line 1180
    invoke-virtual {v0}, Ljava/lang/Runtime;->gc()V

    .line 1181
    const-string v1, "heap gc             "

    invoke-direct {p0, v1}, Lflipboard/io/BitmapManager;->a(Ljava/lang/String;)V

    .line 1182
    invoke-virtual {v0}, Ljava/lang/Runtime;->runFinalization()V

    .line 1183
    const-string v1, "heap finalization   "

    invoke-direct {p0, v1}, Lflipboard/io/BitmapManager;->a(Ljava/lang/String;)V

    .line 1184
    invoke-virtual {v0}, Ljava/lang/Runtime;->gc()V

    .line 1185
    const-string v0, "heap after          "

    invoke-direct {p0, v0}, Lflipboard/io/BitmapManager;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1186
    monitor-exit p0

    return-void

    .line 1178
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(IZ)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v4, 0x0

    .line 385
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 387
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 388
    monitor-enter p0

    move v3, v4

    move v1, v4

    .line 389
    :goto_0
    if-ge v3, v10, :cond_2

    .line 390
    :try_start_0
    iget-object v0, p0, Lflipboard/io/BitmapManager;->i:Lflipboard/io/BitmapManager$Handle;

    move v2, v1

    :goto_1
    if-eqz v0, :cond_1

    iget v1, p0, Lflipboard/io/BitmapManager;->d:I

    if-le v1, p1, :cond_1

    .line 391
    iget v1, v0, Lflipboard/io/BitmapManager$Handle;->d:I

    if-nez v1, :cond_0

    .line 394
    iget-object v1, v0, Lflipboard/io/BitmapManager$Handle;->e:Lflipboard/io/BitmapManager$Handle;

    .line 395
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 396
    iget v8, v0, Lflipboard/io/BitmapManager$Handle;->h:I

    iget v9, v0, Lflipboard/io/BitmapManager$Handle;->i:I

    mul-int/2addr v8, v9

    .line 397
    iget v9, p0, Lflipboard/io/BitmapManager;->d:I

    sub-int/2addr v9, v8

    iput v9, p0, Lflipboard/io/BitmapManager;->d:I

    .line 398
    add-int/2addr v2, v8

    .line 399
    invoke-virtual {p0, v0}, Lflipboard/io/BitmapManager;->b(Lflipboard/io/BitmapManager$Handle;)V

    .line 400
    iget-object v8, p0, Lflipboard/io/BitmapManager;->c:Ljava/util/Map;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->a()Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->g()V

    move-object v0, v1

    .line 402
    goto :goto_1

    .line 403
    :cond_0
    iget-object v0, v0, Lflipboard/io/BitmapManager$Handle;->e:Lflipboard/io/BitmapManager$Handle;

    goto :goto_1

    .line 389
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v1, v2

    goto :goto_0

    .line 407
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 408
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/BitmapManager$Handle;

    .line 409
    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->f()V

    goto :goto_2

    .line 407
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 411
    :cond_3
    if-nez p2, :cond_4

    sget-object v0, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_5

    .line 412
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v6

    .line 413
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x1

    iget v4, p0, Lflipboard/io/BitmapManager;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v1

    iget v1, p0, Lflipboard/io/BitmapManager;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v10

    const/4 v1, 0x3

    iget v4, p0, Lflipboard/io/BitmapManager;->d:I

    mul-int/lit8 v4, v4, 0x64

    iget v5, p0, Lflipboard/io/BitmapManager;->j:I

    div-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    .line 415
    :cond_5
    return-void
.end method

.method final declared-synchronized b(Lflipboard/io/BitmapManager$Handle;)V
    .locals 2

    .prologue
    .line 228
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lflipboard/io/BitmapManager$Handle;->e:Lflipboard/io/BitmapManager$Handle;

    if-nez v0, :cond_0

    .line 229
    iget-object v0, p1, Lflipboard/io/BitmapManager$Handle;->f:Lflipboard/io/BitmapManager$Handle;

    iput-object v0, p0, Lflipboard/io/BitmapManager;->h:Lflipboard/io/BitmapManager$Handle;

    .line 233
    :goto_0
    iget-object v0, p1, Lflipboard/io/BitmapManager$Handle;->f:Lflipboard/io/BitmapManager$Handle;

    if-nez v0, :cond_1

    .line 234
    iget-object v0, p1, Lflipboard/io/BitmapManager$Handle;->e:Lflipboard/io/BitmapManager$Handle;

    iput-object v0, p0, Lflipboard/io/BitmapManager;->i:Lflipboard/io/BitmapManager$Handle;

    .line 238
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p1, Lflipboard/io/BitmapManager$Handle;->e:Lflipboard/io/BitmapManager$Handle;

    iput-object v0, p1, Lflipboard/io/BitmapManager$Handle;->f:Lflipboard/io/BitmapManager$Handle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    monitor-exit p0

    return-void

    .line 231
    :cond_0
    :try_start_1
    iget-object v0, p1, Lflipboard/io/BitmapManager$Handle;->e:Lflipboard/io/BitmapManager$Handle;

    iget-object v1, p1, Lflipboard/io/BitmapManager$Handle;->f:Lflipboard/io/BitmapManager$Handle;

    iput-object v1, v0, Lflipboard/io/BitmapManager$Handle;->f:Lflipboard/io/BitmapManager$Handle;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 228
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 236
    :cond_1
    :try_start_2
    iget-object v0, p1, Lflipboard/io/BitmapManager$Handle;->f:Lflipboard/io/BitmapManager$Handle;

    iget-object v1, p1, Lflipboard/io/BitmapManager$Handle;->e:Lflipboard/io/BitmapManager$Handle;

    iput-object v1, v0, Lflipboard/io/BitmapManager$Handle;->e:Lflipboard/io/BitmapManager$Handle;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized c()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1199
    monitor-enter p0

    const/4 v0, 0x0

    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {p0, v0, v2}, Lflipboard/io/BitmapManager;->b(IZ)V

    .line 1202
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lflipboard/io/BitmapManager;->c:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    iget v3, p0, Lflipboard/io/BitmapManager;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x2

    iget v3, p0, Lflipboard/io/BitmapManager;->j:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x3

    iget v3, p0, Lflipboard/io/BitmapManager;->d:I

    mul-int/lit8 v3, v3, 0x64

    iget v4, p0, Lflipboard/io/BitmapManager;->j:I

    div-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    .line 1203
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 1204
    iget-object v0, p0, Lflipboard/io/BitmapManager;->h:Lflipboard/io/BitmapManager$Handle;

    :goto_0
    if-eqz v0, :cond_0

    .line 1205
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    add-int/lit8 v2, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    iget v5, v0, Lflipboard/io/BitmapManager$Handle;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x2

    iget v5, v0, Lflipboard/io/BitmapManager$Handle;->h:I

    iget v6, v0, Lflipboard/io/BitmapManager$Handle;->i:I

    mul-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x3

    aput-object v0, v4, v1

    .line 1206
    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1204
    iget-object v0, v0, Lflipboard/io/BitmapManager$Handle;->f:Lflipboard/io/BitmapManager$Handle;

    move v1, v2

    goto :goto_0

    .line 1208
    :cond_0
    iget-object v0, p0, Lflipboard/io/BitmapManager;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/BitmapManager$Handle;

    .line 1209
    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1210
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, v0, Lflipboard/io/BitmapManager$Handle;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x1

    iget v5, v0, Lflipboard/io/BitmapManager$Handle;->h:I

    iget v6, v0, Lflipboard/io/BitmapManager$Handle;->i:I

    mul-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x2

    aput-object v0, v2, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1199
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1213
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public final c(I)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 333
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    .line 334
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "emergency bitmap purge, pixels.needed=%,d, heap.total=%,d, heap.free=%,d, heap.max=%,d, pixels.total=%,d, pixels.max=%,d"

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v9

    iget v4, p0, Lflipboard/io/BitmapManager;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v10

    const/4 v4, 0x5

    iget v5, p0, Lflipboard/io/BitmapManager;->j:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 335
    invoke-virtual {p0, v7, v6}, Lflipboard/io/BitmapManager;->b(IZ)V

    .line 336
    iget v1, p0, Lflipboard/io/BitmapManager;->j:I

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0, v1, v6}, Lflipboard/io/BitmapManager;->a(IZ)V

    .line 339
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 340
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->gc()V

    .line 341
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->runFinalization()V

    .line 342
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 343
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v4, "emergency gc, tm=%,dms, heap.total=%,d, heap.free=%,d, heap.max=%,d, pixels.total=%,d, pixels.max=%,d"

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v7

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v8

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v9

    iget v0, p0, Lflipboard/io/BitmapManager;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v10

    const/4 v0, 0x5

    iget v2, p0, Lflipboard/io/BitmapManager;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v0

    invoke-virtual {v1, v4, v5}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 344
    return-void
.end method

.method public final c(Lflipboard/io/BitmapManager$Handle;)V
    .locals 3

    .prologue
    .line 291
    iget v0, p1, Lflipboard/io/BitmapManager$Handle;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lflipboard/io/BitmapManager$Handle;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 292
    monitor-enter p0

    .line 293
    :try_start_0
    invoke-virtual {p0, p1}, Lflipboard/io/BitmapManager;->b(Lflipboard/io/BitmapManager$Handle;)V

    .line 294
    iget v0, p0, Lflipboard/io/BitmapManager;->d:I

    iget v1, p1, Lflipboard/io/BitmapManager$Handle;->h:I

    iget v2, p1, Lflipboard/io/BitmapManager$Handle;->i:I

    mul-int/2addr v1, v2

    sub-int/2addr v0, v1

    iput v0, p0, Lflipboard/io/BitmapManager;->d:I

    .line 295
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296
    invoke-virtual {p1}, Lflipboard/io/BitmapManager$Handle;->f()V

    .line 298
    :cond_0
    return-void

    .line 295
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
