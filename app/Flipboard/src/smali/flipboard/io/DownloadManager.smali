.class public final Lflipboard/io/DownloadManager;
.super Ljava/lang/Object;
.source "DownloadManager.java"


# static fields
.field public static final a:Lflipboard/util/Log;

.field public static b:Lflipboard/io/DownloadManager;


# instance fields
.field final c:Landroid/content/Context;

.field final d:Ljava/io/File;

.field public final e:Ljava/io/File;

.field f:Landroid/database/sqlite/SQLiteDatabase;

.field final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/io/Download;",
            ">;"
        }
    .end annotation
.end field

.field final h:Ljava/util/SortedSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedSet",
            "<",
            "Lflipboard/io/Download;",
            ">;"
        }
    .end annotation
.end field

.field final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/io/DownloadManager$FileData;",
            ">;"
        }
    .end annotation
.end field

.field final j:J

.field k:Z

.field l:Z

.field m:Z

.field public n:I

.field public o:J

.field p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-string v0, "downloads"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x400

    const/4 v6, 0x1

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, Lflipboard/io/DownloadManager;->c:Landroid/content/Context;

    .line 90
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "cache_location"

    const-string v2, "external"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    iget-object v1, p0, Lflipboard/io/DownloadManager;->c:Landroid/content/Context;

    invoke-static {v1, v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 92
    if-nez v0, :cond_0

    .line 93
    iget-object v0, p0, Lflipboard/io/DownloadManager;->c:Landroid/content/Context;

    const-string v1, "internal"

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 96
    :cond_0
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v2, "cache_size"

    const-string v3, "128MB"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 97
    const-wide/16 v2, 0x20

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    mul-long/2addr v2, v8

    mul-long/2addr v2, v8

    .line 99
    iput-object v0, p0, Lflipboard/io/DownloadManager;->d:Ljava/io/File;

    .line 100
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lflipboard/io/DownloadManager;->d:Ljava/io/File;

    const-string v4, "downloads"

    invoke-direct {v0, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/io/DownloadManager;->e:Ljava/io/File;

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflipboard/io/DownloadManager;->g:Ljava/util/Map;

    .line 102
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    .line 103
    sput-object p0, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    .line 104
    iput-wide v2, p0, Lflipboard/io/DownloadManager;->j:J

    .line 105
    new-instance v0, Ljava/util/LinkedHashMap;

    const/16 v1, 0x100

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2, v6}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput-object v0, p0, Lflipboard/io/DownloadManager;->i:Ljava/util/Map;

    .line 106
    iput-boolean v6, p0, Lflipboard/io/DownloadManager;->l:Z

    .line 108
    new-instance v0, Lflipboard/io/DownloadManager$1;

    invoke-direct {v0, p0}, Lflipboard/io/DownloadManager$1;-><init>(Lflipboard/io/DownloadManager;)V

    .line 232
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 233
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)J
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 836
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, Lflipboard/io/DownloadManager;->e:Ljava/io/File;

    invoke-direct {v1, v0, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 837
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 839
    :try_start_0
    iget-object v0, p0, Lflipboard/io/DownloadManager;->f:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "download"

    const-string v5, "url=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    invoke-virtual {v0, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-eq v0, v8, :cond_0

    .line 840
    sget-object v0, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const-string v4, "failed to delete %s from cache"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v0, v4, v5}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 841
    const-string v0, "unwanted.DownloadManager_cant_delete_url"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 847
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 848
    return-wide v2

    .line 843
    :catch_0
    move-exception v0

    .line 844
    sget-object v4, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const-string v5, "failed to delete %s from cache, %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v9

    aput-object v0, v6, v8

    invoke-virtual {v4, v5, v6}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 845
    const-string v0, "unwanted.DownloadManager_cant_delete_url"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 399
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v1

    .line 400
    if-eqz p0, :cond_0

    if-eqz v1, :cond_0

    iget-object v0, v1, Lflipboard/model/ConfigSetting;->FlipboardCDNHost:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, v1, Lflipboard/model/ConfigSetting;->geoCountryCode:Ljava/util/Map;

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, v1, Lflipboard/model/ConfigSetting;->geoCountryCode:Ljava/util/Map;

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->j:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/ConfigSetting$GeoCountryCode;

    .line 403
    if-eqz v0, :cond_0

    iget-object v2, v0, Lflipboard/model/ConfigSetting$GeoCountryCode;->FlipboardCDNPrefix:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 404
    const-string v2, "://"

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 405
    iget-object v1, v1, Lflipboard/model/ConfigSetting;->FlipboardCDNHost:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 406
    add-int/lit8 v3, v2, 0x3

    if-ne v3, v1, :cond_0

    .line 407
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v2, v2, 0x3

    invoke-virtual {p0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lflipboard/model/ConfigSetting$GeoCountryCode;->FlipboardCDNPrefix:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 408
    sget-object v0, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p0, v0, v4

    .line 412
    :cond_0
    return-object p0
.end method

.method static synthetic a(Lflipboard/io/DownloadManager;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lflipboard/io/DownloadManager;->e()V

    return-void
.end method

.method static synthetic a(Lflipboard/io/DownloadManager;Lflipboard/io/Download;)V
    .locals 28

    .prologue
    .line 56
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    iget-object v5, v0, Lflipboard/io/Download;->b:Ljava/lang/String;

    invoke-static {v5}, Lflipboard/util/JavaUtil;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    new-instance v20, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/io/DownloadManager;->d:Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".tmp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    const-wide/16 v6, 0x0

    const-wide/16 v10, 0x0

    sget-object v2, Lflipboard/io/Download$Status;->d:Lflipboard/io/Download$Status;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V

    const/4 v2, 0x0

    move v12, v2

    :goto_0
    if-lez v12, :cond_2

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lflipboard/io/Download;->E()Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lflipboard/io/Download$Status;->e:Lflipboard/io/Download$Status;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    monitor-enter v3
    :try_end_0
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_11

    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_1
    const-wide/16 v2, 0x0

    cmp-long v2, v10, v2

    if-lez v2, :cond_1

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->delete()Z

    :cond_1
    :goto_2
    return-void

    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3

    throw v2
    :try_end_2
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_11

    :catch_0
    move-exception v2

    :goto_3
    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    sget-object v2, Lflipboard/io/Download$Status;->e:Lflipboard/io/Download$Status;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V

    goto :goto_1

    :cond_2
    const/4 v2, 0x2

    if-le v12, v2, :cond_3

    :try_start_3
    sget-object v2, Lflipboard/io/Download$Status;->g:Lflipboard/io/Download$Status;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V
    :try_end_3
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_11

    goto :goto_1

    :catch_1
    move-exception v2

    :goto_4
    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    sget-object v2, Lflipboard/io/Download$Status;->f:Lflipboard/io/Download$Status;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V

    goto :goto_1

    :cond_3
    :try_start_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    invoke-static {v5}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    const-wide/16 v8, 0x0

    cmp-long v2, v2, v8

    if-lez v2, :cond_4

    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v3

    const/4 v3, 0x1

    aput-object v5, v2, v3

    const/4 v3, 0x2

    aput-object v20, v2, v3

    const/4 v3, 0x3

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v2, v3

    const-string v2, "Range"

    const-string v3, "bytes=0-"

    invoke-virtual {v4, v2, v3}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    sget-object v2, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v2, v4}, Lflipboard/io/NetworkManager;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;
    :try_end_4
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_11

    move-result-object v21

    :try_start_5
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v16

    const/16 v3, 0xc8

    move/from16 v0, v16

    if-eq v0, v3, :cond_6

    const/16 v3, 0xce

    move/from16 v0, v16

    if-eq v0, v3, :cond_6

    sget-object v3, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const-string v6, "%s: unexpected response: %s, url=%s, file=%s"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v13, v7, v8

    const/4 v8, 0x1

    aput-object v2, v7, v8

    const/4 v2, 0x2

    aput-object v5, v7, v2

    const/4 v2, 0x3

    aput-object v20, v7, v2

    invoke-virtual {v3, v6, v7}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->delete()Z

    const-wide/16 v10, 0x0

    sget-object v2, Lflipboard/io/Download$Status;->i:Lflipboard/io/Download$Status;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V

    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    iget-boolean v2, v2, Lflipboard/util/Log;->f:Z

    if-eqz v2, :cond_5

    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    invoke-static {v4}, Lflipboard/util/AndroidUtil;->a(Lorg/apache/http/HttpRequest;)V

    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    invoke-static/range {v21 .. v21}, Lflipboard/util/AndroidUtil;->a(Lorg/apache/http/HttpResponse;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :cond_5
    if-eqz v21, :cond_1

    :try_start_6
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_6
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_11

    move-result-object v2

    if-eqz v2, :cond_1

    :try_start_7
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V

    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_11

    goto/16 :goto_2

    :catch_2
    move-exception v2

    :try_start_8
    sget-object v3, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4
    :try_end_8
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_8 .. :try_end_8} :catch_5
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_11

    goto/16 :goto_2

    :catch_3
    move-exception v2

    :goto_5
    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    sget-object v2, Lflipboard/io/Download$Status;->f:Lflipboard/io/Download$Status;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V

    goto/16 :goto_1

    :cond_6
    const-wide/16 v2, -0x1

    :try_start_9
    const-string v8, "Content-Length"

    move-object/from16 v0, v21

    invoke-interface {v0, v8}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v8

    if-eqz v8, :cond_7

    invoke-interface {v8}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v2, v2

    const-wide/32 v8, 0x6400000

    cmp-long v8, v2, v8

    if-lez v8, :cond_7

    sget-object v4, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const-string v6, "%s: content too big: url=%s, clen=%,d"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v13, v7, v8

    const/4 v8, 0x1

    aput-object v5, v7, v8

    const/4 v5, 0x2

    aput-object v20, v7, v5

    const/4 v5, 0x3

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v7, v5

    invoke-virtual {v4, v6, v7}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->delete()Z

    const-wide/16 v10, 0x0

    sget-object v2, Lflipboard/io/Download$Status;->i:Lflipboard/io/Download$Status;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    if-eqz v21, :cond_0

    :try_start_a
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_a
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_a .. :try_end_a} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_11

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_b
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V

    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_b .. :try_end_b} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_b .. :try_end_b} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_11

    goto/16 :goto_1

    :catch_4
    move-exception v2

    :try_start_c
    sget-object v3, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4
    :try_end_c
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_c .. :try_end_c} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_c .. :try_end_c} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_c .. :try_end_c} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_c .. :try_end_c} :catch_5
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_11

    goto/16 :goto_1

    :catch_5
    move-exception v2

    :goto_6
    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    sget-object v2, Lflipboard/io/Download$Status;->g:Lflipboard/io/Download$Status;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V

    goto/16 :goto_1

    :cond_7
    move-wide v8, v2

    :try_start_d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v14

    const-wide/16 v14, 0x7d0

    cmp-long v14, v2, v14

    if-lez v14, :cond_8

    sget-object v14, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const-string v15, "%s: connection took %,d ms, %s"

    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v13, v18, v19

    const/16 v19, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v18, v19

    const/4 v2, 0x2

    aput-object p1, v18, v2

    move-object/from16 v0, v18

    invoke-virtual {v14, v15, v0}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_8
    new-instance v14, Ljava/io/RandomAccessFile;

    const-string v2, "rw"

    move-object/from16 v0, v20

    invoke-direct {v14, v0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    const/16 v2, 0xce

    move/from16 v0, v16

    if-ne v0, v2, :cond_10

    :try_start_e
    const-string v2, "Content-Range"

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v2

    if-nez v2, :cond_a

    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const-string v3, "%s: invalid partial response: no Content-Range header, url=%s, file=%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v13, v4, v8

    const/4 v8, 0x1

    aput-object v5, v4, v8

    const/4 v8, 0x2

    aput-object v20, v4, v8

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->delete()Z
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    const-wide/16 v2, 0x0

    :try_start_f
    invoke-virtual {v14}, Ljava/io/RandomAccessFile;->close()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_6

    if-eqz v21, :cond_9

    :try_start_10
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_10
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_10 .. :try_end_10} :catch_7
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_10 .. :try_end_10} :catch_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_10 .. :try_end_10} :catch_b
    .catch Ljava/net/UnknownHostException; {:try_start_10 .. :try_end_10} :catch_d
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_15
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_14

    move-result-object v4

    if-eqz v4, :cond_9

    :try_start_11
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->consumeContent()V

    sget-object v4, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_6
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_11 .. :try_end_11} :catch_7
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_11 .. :try_end_11} :catch_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_11 .. :try_end_11} :catch_b
    .catch Ljava/net/UnknownHostException; {:try_start_11 .. :try_end_11} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_14

    :cond_9
    :goto_7
    add-int/lit8 v4, v12, 0x1

    move-wide v10, v2

    move v12, v4

    goto/16 :goto_0

    :catch_6
    move-exception v4

    :try_start_12
    sget-object v8, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v8, v9
    :try_end_12
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_12 .. :try_end_12} :catch_7
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_12 .. :try_end_12} :catch_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_12 .. :try_end_12} :catch_b
    .catch Ljava/net/UnknownHostException; {:try_start_12 .. :try_end_12} :catch_d
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_15
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_14

    goto :goto_7

    :catch_7
    move-exception v4

    move-wide v10, v2

    goto/16 :goto_3

    :cond_a
    :try_start_13
    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_b

    const-string v3, "bytes "

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_c

    :cond_b
    sget-object v3, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const-string v4, "%s: invalid partial response: %s, url=%s, file=%s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v13, v8, v9

    const/4 v9, 0x1

    aput-object v2, v8, v9

    const/4 v2, 0x2

    aput-object v5, v8, v2

    const/4 v2, 0x3

    aput-object v20, v8, v2

    invoke-virtual {v3, v4, v8}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->delete()Z
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    const-wide/16 v2, 0x0

    :try_start_14
    invoke-virtual {v14}, Ljava/io/RandomAccessFile;->close()V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_6

    if-eqz v21, :cond_9

    :try_start_15
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_15
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_15 .. :try_end_15} :catch_7
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_15 .. :try_end_15} :catch_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_15 .. :try_end_15} :catch_b
    .catch Ljava/net/UnknownHostException; {:try_start_15 .. :try_end_15} :catch_d
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_15
    .catch Ljava/lang/Throwable; {:try_start_15 .. :try_end_15} :catch_14

    move-result-object v4

    if-eqz v4, :cond_9

    :try_start_16
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->consumeContent()V

    sget-object v4, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_8
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_16 .. :try_end_16} :catch_7
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_16 .. :try_end_16} :catch_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_16 .. :try_end_16} :catch_b
    .catch Ljava/net/UnknownHostException; {:try_start_16 .. :try_end_16} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_14

    goto :goto_7

    :catch_8
    move-exception v4

    :try_start_17
    sget-object v8, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v8, v9
    :try_end_17
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_17 .. :try_end_17} :catch_7
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_17 .. :try_end_17} :catch_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_17 .. :try_end_17} :catch_b
    .catch Ljava/net/UnknownHostException; {:try_start_17 .. :try_end_17} :catch_d
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_15
    .catch Ljava/lang/Throwable; {:try_start_17 .. :try_end_17} :catch_14

    goto :goto_7

    :catch_9
    move-exception v4

    move-wide v10, v2

    goto/16 :goto_4

    :cond_c
    const/16 v3, 0x2d

    :try_start_18
    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-gez v3, :cond_d

    sget-object v3, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const-string v4, "%s: invalid partial response: %s, url=%s, file=%s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v13, v8, v9

    const/4 v9, 0x1

    aput-object v2, v8, v9

    const/4 v2, 0x2

    aput-object v5, v8, v2

    const/4 v2, 0x3

    aput-object v20, v8, v2

    invoke-virtual {v3, v4, v8}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->delete()Z
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_2

    const-wide/16 v2, 0x0

    :try_start_19
    invoke-virtual {v14}, Ljava/io/RandomAccessFile;->close()V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_6

    if-eqz v21, :cond_9

    :try_start_1a
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_1a
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_1a .. :try_end_1a} :catch_7
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_1a .. :try_end_1a} :catch_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_1a .. :try_end_1a} :catch_b
    .catch Ljava/net/UnknownHostException; {:try_start_1a .. :try_end_1a} :catch_d
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_15
    .catch Ljava/lang/Throwable; {:try_start_1a .. :try_end_1a} :catch_14

    move-result-object v4

    if-eqz v4, :cond_9

    :try_start_1b
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->consumeContent()V

    sget-object v4, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_a
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_1b .. :try_end_1b} :catch_7
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_1b .. :try_end_1b} :catch_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_1b .. :try_end_1b} :catch_b
    .catch Ljava/net/UnknownHostException; {:try_start_1b .. :try_end_1b} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_1b .. :try_end_1b} :catch_14

    goto/16 :goto_7

    :catch_a
    move-exception v4

    :try_start_1c
    sget-object v8, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v8, v9
    :try_end_1c
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_1c .. :try_end_1c} :catch_7
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_1c .. :try_end_1c} :catch_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_1c .. :try_end_1c} :catch_b
    .catch Ljava/net/UnknownHostException; {:try_start_1c .. :try_end_1c} :catch_d
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_15
    .catch Ljava/lang/Throwable; {:try_start_1c .. :try_end_1c} :catch_14

    goto/16 :goto_7

    :catch_b
    move-exception v4

    move-wide v10, v2

    goto/16 :goto_5

    :cond_d
    const/4 v15, 0x6

    :try_start_1d
    invoke-virtual {v2, v15, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v10, v3

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->length()J

    move-result-wide v18

    cmp-long v3, v10, v18

    if-lez v3, :cond_e

    sget-object v3, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const-string v4, "%s: invalid partial response: %s, url=%s, file=%s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v13, v8, v9

    const/4 v9, 0x1

    aput-object v2, v8, v9

    const/4 v2, 0x2

    aput-object v5, v8, v2

    const/4 v2, 0x3

    aput-object v20, v8, v2

    invoke-virtual {v3, v4, v8}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->delete()Z
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_2

    const-wide/16 v2, 0x0

    :try_start_1e
    invoke-virtual {v14}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_6

    if-eqz v21, :cond_9

    :try_start_1f
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_1f
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_1f .. :try_end_1f} :catch_7
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_1f .. :try_end_1f} :catch_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_1f .. :try_end_1f} :catch_b
    .catch Ljava/net/UnknownHostException; {:try_start_1f .. :try_end_1f} :catch_d
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_1f} :catch_15
    .catch Ljava/lang/Throwable; {:try_start_1f .. :try_end_1f} :catch_14

    move-result-object v4

    if-eqz v4, :cond_9

    :try_start_20
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->consumeContent()V

    sget-object v4, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_20} :catch_c
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_20 .. :try_end_20} :catch_7
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_20 .. :try_end_20} :catch_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_20 .. :try_end_20} :catch_b
    .catch Ljava/net/UnknownHostException; {:try_start_20 .. :try_end_20} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_20 .. :try_end_20} :catch_14

    goto/16 :goto_7

    :catch_c
    move-exception v4

    :try_start_21
    sget-object v8, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v8, v9
    :try_end_21
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_21 .. :try_end_21} :catch_7
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_21 .. :try_end_21} :catch_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_21 .. :try_end_21} :catch_b
    .catch Ljava/net/UnknownHostException; {:try_start_21 .. :try_end_21} :catch_d
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_21} :catch_15
    .catch Ljava/lang/Throwable; {:try_start_21 .. :try_end_21} :catch_14

    goto/16 :goto_7

    :catch_d
    move-exception v4

    move-wide v10, v2

    goto/16 :goto_6

    :cond_e
    :try_start_22
    invoke-virtual {v14, v10, v11}, Ljava/io/RandomAccessFile;->seek(J)V

    :cond_f
    :goto_8
    sget-object v2, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    move-object/from16 v0, v21

    invoke-virtual {v2, v4, v0}, Lflipboard/io/NetworkManager;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_2

    move-result-object v4

    const/16 v2, 0x1000

    :try_start_23
    new-array v12, v2, [B

    move-wide/from16 v18, v6

    :goto_9
    const/4 v2, 0x0

    const/16 v3, 0x1000

    invoke-virtual {v4, v12, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    if-lez v6, :cond_12

    int-to-long v2, v6

    add-long v2, v2, v18

    long-to-float v7, v2

    long-to-float v15, v8

    div-float/2addr v7, v15

    move-object/from16 v0, p1

    iput v7, v0, Lflipboard/io/Download;->i:F

    sget-object v7, Lflipboard/io/Download$Status;->c:Lflipboard/io/Download$Status;

    move-object/from16 v0, p1

    iget-object v15, v0, Lflipboard/io/Download;->f:Lflipboard/io/Download$Data;

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v15}, Lflipboard/io/Download;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_1

    const/4 v7, 0x0

    :try_start_24
    invoke-virtual {v14, v12, v7, v6}, Ljava/io/RandomAccessFile;->write([BII)V
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_24} :catch_e
    .catchall {:try_start_24 .. :try_end_24} :catchall_1

    :goto_a
    int-to-long v6, v6

    add-long/2addr v10, v6

    move-wide/from16 v18, v2

    goto :goto_9

    :cond_10
    const/16 v2, 0xc8

    move/from16 v0, v16

    if-ne v0, v2, :cond_f

    const-wide/16 v2, 0x0

    const-wide/16 v18, 0x0

    cmp-long v2, v2, v18

    if-lez v2, :cond_f

    const-wide/16 v2, 0x0

    :try_start_25
    invoke-virtual {v14, v2, v3}, Ljava/io/RandomAccessFile;->setLength(J)V
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_2

    const-wide/16 v10, 0x0

    goto :goto_8

    :catch_e
    move-exception v7

    :try_start_26
    sget-object v15, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    invoke-virtual {v15, v7}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    invoke-direct/range {p0 .. p0}, Lflipboard/io/DownloadManager;->e()V

    const/4 v7, 0x0

    invoke-virtual {v14, v12, v7, v6}, Ljava/io/RandomAccessFile;->write([BII)V
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_1

    goto :goto_a

    :catchall_1
    move-exception v2

    :try_start_27
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    throw v2
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_2

    :catchall_2
    move-exception v2

    :try_start_28
    invoke-virtual {v14}, Ljava/io/RandomAccessFile;->close()V

    throw v2
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_3

    :catchall_3
    move-exception v2

    :goto_b
    if-eqz v21, :cond_11

    :try_start_29
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_29
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_29 .. :try_end_29} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_29 .. :try_end_29} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_29 .. :try_end_29} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_29 .. :try_end_29} :catch_5
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_29} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_29 .. :try_end_29} :catch_11

    move-result-object v3

    if-eqz v3, :cond_11

    :try_start_2a
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->consumeContent()V

    sget-object v3, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;
    :try_end_2a
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_2a} :catch_13
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_2a .. :try_end_2a} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_2a .. :try_end_2a} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_2a .. :try_end_2a} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_2a .. :try_end_2a} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_2a .. :try_end_2a} :catch_11

    :cond_11
    :goto_c
    :try_start_2b
    throw v2
    :try_end_2b
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_2b .. :try_end_2b} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_2b .. :try_end_2b} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_2b .. :try_end_2b} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_2b .. :try_end_2b} :catch_5
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_2b} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_2b .. :try_end_2b} :catch_11

    :catch_f
    move-exception v2

    :goto_d
    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    sget-object v2, Lflipboard/io/Download$Status;->g:Lflipboard/io/Download$Status;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V

    goto/16 :goto_1

    :cond_12
    :try_start_2c
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2c
    .catchall {:try_start_2c .. :try_end_2c} :catchall_2

    :try_start_2d
    invoke-virtual {v14}, Ljava/io/RandomAccessFile;->close()V

    const-wide/16 v2, 0x0

    cmp-long v2, v8, v2

    if-lez v2, :cond_13

    cmp-long v2, v10, v8

    if-gez v2, :cond_13

    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const-string v3, "%s: data truncated: url=%s, file=%s, clen=%,d,  offset=%,d"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v13, v4, v6

    const/4 v6, 0x1

    aput-object v5, v4, v6

    const/4 v5, 0x2

    aput-object v20, v4, v5

    const/4 v5, 0x3

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v2, Lflipboard/io/Download$Status;->g:Lflipboard/io/Download$Status;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V
    :try_end_2d
    .catchall {:try_start_2d .. :try_end_2d} :catchall_3

    if-eqz v21, :cond_0

    :try_start_2e
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_2e
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_2e .. :try_end_2e} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_2e .. :try_end_2e} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_2e .. :try_end_2e} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_2e .. :try_end_2e} :catch_5
    .catch Ljava/io/IOException; {:try_start_2e .. :try_end_2e} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_2e .. :try_end_2e} :catch_11

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_2f
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V

    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;
    :try_end_2f
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_2f} :catch_10
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_2f .. :try_end_2f} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_2f .. :try_end_2f} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_2f .. :try_end_2f} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_2f .. :try_end_2f} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_2f .. :try_end_2f} :catch_11

    goto/16 :goto_1

    :catch_10
    move-exception v2

    :try_start_30
    sget-object v3, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4
    :try_end_30
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_30 .. :try_end_30} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_30 .. :try_end_30} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_30 .. :try_end_30} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_30 .. :try_end_30} :catch_5
    .catch Ljava/io/IOException; {:try_start_30 .. :try_end_30} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_30 .. :try_end_30} :catch_11

    goto/16 :goto_1

    :catch_11
    move-exception v2

    :goto_e
    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v3, v2}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    sget-object v2, Lflipboard/io/Download$Status;->g:Lflipboard/io/Download$Status;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V

    goto/16 :goto_1

    :cond_13
    :try_start_31
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/32 v2, 0x240c8400

    add-long v8, v6, v2

    const-string v2, "Content-Type"

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v2

    if-eqz v2, :cond_1b

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v12

    :goto_f
    const-string v13, ""

    if-eqz v12, :cond_15

    const/16 v2, 0x3b

    invoke-virtual {v12, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-ltz v2, :cond_15

    const-string v3, "charset="

    invoke-virtual {v12, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_14

    add-int/lit8 v3, v3, 0x8

    invoke-virtual {v12, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    :cond_14
    const/4 v3, 0x0

    invoke-virtual {v12, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    :cond_15
    const-string v2, "Expires"

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;
    :try_end_31
    .catchall {:try_start_31 .. :try_end_31} :catchall_3

    move-result-object v2

    if-eqz v2, :cond_16

    :try_start_32
    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/http/impl/cookie/DateUtils;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J
    :try_end_32
    .catch Lorg/apache/http/impl/cookie/DateParseException; {:try_start_32 .. :try_end_32} :catch_16
    .catchall {:try_start_32 .. :try_end_32} :catchall_3

    move-result-wide v8

    :cond_16
    :goto_10
    if-eqz v12, :cond_17

    :try_start_33
    const-string v2, "/octet-stream"

    invoke-virtual {v12, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_18

    :cond_17
    invoke-static {v5}, Lflipboard/util/HttpUtil;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-static {v0, v2}, Lflipboard/util/HttpUtil;->a(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_1c

    sget-object v3, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const-string v4, "guessed contentType=%s from sufix %d"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v12, v14, v15

    const/4 v15, 0x1

    aput-object v2, v14, v15

    invoke-virtual {v3, v4, v14}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_18
    :goto_11
    const/4 v14, -0x1

    const/4 v15, -0x1

    if-eqz v12, :cond_1a

    const-string v2, "image/"

    invoke-virtual {v12, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1a

    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v3, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-lez v3, :cond_19

    iget v14, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    :cond_19
    iget v3, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-lez v3, :cond_1a

    iget v15, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    :cond_1a
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/io/DownloadManager;->e:Ljava/io/File;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v0, v2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    const-wide/16 v2, 0x3e8

    div-long v2, v6, v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    const-string v3, "INSERT OR REPLACE INTO download (url,touched,downloaded,expires,contentLength,contentType,charset,location,width,height) VALUES (?,?,?,?,?,?,?,?,?,?)"

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/io/DownloadManager;->f:Landroid/database/sqlite/SQLiteDatabase;

    const/16 v24, 0xa

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v5, v24, v25

    const/16 v25, 0x1

    aput-object v2, v24, v25

    const/16 v25, 0x2

    aput-object v2, v24, v25

    const/4 v2, 0x3

    const-wide/16 v26, 0x3e8

    div-long v26, v8, v26

    invoke-static/range {v26 .. v27}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v25

    aput-object v25, v24, v2

    const/4 v2, 0x4

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v25

    aput-object v25, v24, v2

    const/4 v2, 0x5

    aput-object v12, v24, v2

    const/4 v2, 0x6

    aput-object v13, v24, v2

    const/4 v2, 0x7

    aput-object v17, v24, v2

    const/16 v2, 0x8

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v24, v2

    const/16 v2, 0x9

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v24, v2

    move-object/from16 v0, v24

    invoke-virtual {v4, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_33
    .catchall {:try_start_33 .. :try_end_33} :catchall_3

    move-result-object v2

    :try_start_34
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_34
    .catchall {:try_start_34 .. :try_end_34} :catchall_4

    :try_start_35
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    new-instance v3, Lflipboard/io/DownloadManager$FileData;

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v16}, Lflipboard/io/DownloadManager$FileData;-><init>(Lflipboard/io/DownloadManager;Ljava/lang/String;JJJLjava/lang/String;Ljava/lang/String;IILjava/io/File;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/io/DownloadManager;->i:Ljava/util/Map;

    monitor-enter v4
    :try_end_35
    .catchall {:try_start_35 .. :try_end_35} :catchall_3

    :try_start_36
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/io/DownloadManager;->i:Ljava/util/Map;

    invoke-interface {v2, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget v2, v0, Lflipboard/io/DownloadManager;->n:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lflipboard/io/DownloadManager;->n:I

    move-object/from16 v0, p0

    iget-wide v6, v0, Lflipboard/io/DownloadManager;->o:J

    add-long/2addr v6, v10

    move-object/from16 v0, p0

    iput-wide v6, v0, Lflipboard/io/DownloadManager;->o:J

    monitor-exit v4
    :try_end_36
    .catchall {:try_start_36 .. :try_end_36} :catchall_5

    :try_start_37
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v8, v6, v22

    const-wide/16 v6, 0x0

    cmp-long v2, v8, v6

    if-lez v2, :cond_1d

    const-wide/16 v6, 0x3e8

    mul-long v6, v6, v18

    div-long/2addr v6, v8

    :goto_12
    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v2, v4

    const/4 v4, 0x1

    aput-object v5, v2, v4

    const/4 v4, 0x2

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x3

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v2, v4

    sget-object v2, Lflipboard/io/Download$Status;->h:Lflipboard/io/Download$Status;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V
    :try_end_37
    .catchall {:try_start_37 .. :try_end_37} :catchall_3

    if-eqz v21, :cond_1

    :try_start_38
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_38
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_38 .. :try_end_38} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_38 .. :try_end_38} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_38 .. :try_end_38} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_38 .. :try_end_38} :catch_5
    .catch Ljava/io/IOException; {:try_start_38 .. :try_end_38} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_38 .. :try_end_38} :catch_11

    move-result-object v2

    if-eqz v2, :cond_1

    :try_start_39
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V

    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_39 .. :try_end_39} :catch_12
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_39 .. :try_end_39} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_39 .. :try_end_39} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_39 .. :try_end_39} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_39 .. :try_end_39} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_39 .. :try_end_39} :catch_11

    goto/16 :goto_2

    :cond_1b
    const/4 v12, 0x0

    goto/16 :goto_f

    :cond_1c
    :try_start_3a
    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const-string v3, "could not guess contentType, %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v5, v4, v14

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_11

    :catchall_4
    move-exception v3

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v3

    :catchall_5
    move-exception v2

    monitor-exit v4

    throw v2
    :try_end_3a
    .catchall {:try_start_3a .. :try_end_3a} :catchall_3

    :cond_1d
    const-wide/16 v6, 0x0

    goto :goto_12

    :catch_12
    move-exception v2

    :try_start_3b
    sget-object v3, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    goto/16 :goto_2

    :catch_13
    move-exception v3

    sget-object v4, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v4, v5
    :try_end_3b
    .catch Lflipboard/io/NetworkManager$NoNetworkException; {:try_start_3b .. :try_end_3b} :catch_0
    .catch Lflipboard/io/NetworkManager$DNSTimeoutException; {:try_start_3b .. :try_end_3b} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_3b .. :try_end_3b} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_3b .. :try_end_3b} :catch_5
    .catch Ljava/io/IOException; {:try_start_3b .. :try_end_3b} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_3b .. :try_end_3b} :catch_11

    goto/16 :goto_c

    :catch_14
    move-exception v4

    move-wide v10, v2

    move-object v2, v4

    goto/16 :goto_e

    :catch_15
    move-exception v4

    move-wide v10, v2

    goto/16 :goto_d

    :catchall_6
    move-exception v4

    move-wide v10, v2

    move-object v2, v4

    goto/16 :goto_b

    :catch_16
    move-exception v2

    goto/16 :goto_10
.end method

.method private b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 364
    iget-object v1, p0, Lflipboard/io/DownloadManager;->g:Ljava/util/Map;

    monitor-enter v1

    .line 365
    :try_start_0
    iget-object v0, p0, Lflipboard/io/DownloadManager;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 366
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private e()V
    .locals 6

    .prologue
    .line 787
    iget-wide v0, p0, Lflipboard/io/DownloadManager;->o:J

    iget-wide v2, p0, Lflipboard/io/DownloadManager;->j:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 788
    iget-wide v0, p0, Lflipboard/io/DownloadManager;->j:J

    iget-wide v2, p0, Lflipboard/io/DownloadManager;->j:J

    const-wide/16 v4, 0x3

    div-long/2addr v2, v4

    const-wide/32 v4, 0x200000

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lflipboard/io/DownloadManager;->a(J)V

    .line 790
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;ZZ)Lflipboard/io/Download;
    .locals 4

    .prologue
    .line 325
    if-nez p1, :cond_0

    .line 329
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "DownloadManager trying to download a null url"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 330
    const-string v0, "unwanted.download_url_null"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    .line 331
    const/4 v0, 0x0

    .line 355
    :goto_0
    return-object v0

    .line 334
    :cond_0
    iget-object v2, p0, Lflipboard/io/DownloadManager;->g:Ljava/util/Map;

    monitor-enter v2

    .line 335
    :try_start_0
    iget-object v0, p0, Lflipboard/io/DownloadManager;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/Download;

    .line 336
    if-nez v0, :cond_1

    .line 337
    new-instance v1, Lflipboard/io/Download;

    invoke-direct {v1, p0, p1}, Lflipboard/io/Download;-><init>(Lflipboard/io/DownloadManager;Ljava/lang/String;)V

    .line 338
    iget-object v0, p0, Lflipboard/io/DownloadManager;->g:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    iget-object v3, p0, Lflipboard/io/DownloadManager;->i:Ljava/util/Map;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 340
    :try_start_1
    iget-object v0, p0, Lflipboard/io/DownloadManager;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/Download$Data;

    iput-object v0, v1, Lflipboard/io/Download;->f:Lflipboard/io/Download$Data;

    .line 341
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 342
    if-nez p3, :cond_2

    :try_start_2
    iget-object v0, v1, Lflipboard/io/Download;->f:Lflipboard/io/Download$Data;

    if-eqz v0, :cond_2

    .line 343
    sget-object v0, Lflipboard/io/Download$Status;->h:Lflipboard/io/Download$Status;

    iput-object v0, v1, Lflipboard/io/Download;->d:Lflipboard/io/Download$Status;

    move-object v0, v1

    .line 354
    :cond_1
    :goto_1
    iget v1, v0, Lflipboard/io/Download;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lflipboard/io/Download;->g:I

    .line 355
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 356
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 341
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3

    throw v0

    .line 344
    :cond_2
    if-nez p3, :cond_3

    if-nez p2, :cond_3

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 345
    sget-object v0, Lflipboard/io/Download$Status;->e:Lflipboard/io/Download$Status;

    iput-object v0, v1, Lflipboard/io/Download;->d:Lflipboard/io/Download$Status;

    move-object v0, v1

    goto :goto_1

    .line 347
    :cond_3
    sget-object v0, Lflipboard/io/Download$Status;->b:Lflipboard/io/Download$Status;

    iput-object v0, v1, Lflipboard/io/Download;->d:Lflipboard/io/Download$Status;

    .line 348
    iget-object v3, p0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    monitor-enter v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 349
    :try_start_4
    iget-object v0, p0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-interface {v0, v1}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    .line 350
    iget-object v0, p0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 351
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-object v0, v1

    goto :goto_1

    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v3

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method final a()Ljava/lang/Thread;
    .locals 4

    .prologue
    .line 237
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/io/File;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/io/DownloadManager;->c:Landroid/content/Context;

    const-string v3, "internal"

    .line 238
    invoke-static {v2, v3}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lflipboard/io/DownloadManager;->c:Landroid/content/Context;

    const-string v3, "external"

    .line 239
    invoke-static {v2, v3}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    aput-object v2, v0, v1

    .line 241
    new-instance v1, Lflipboard/io/DownloadManager$2;

    const-string v2, "cache-cleanup"

    invoke-direct {v1, p0, v2, v0}, Lflipboard/io/DownloadManager$2;-><init>(Lflipboard/io/DownloadManager;Ljava/lang/String;[Ljava/io/File;)V

    return-object v1
.end method

.method public final a(J)V
    .locals 15

    .prologue
    .line 793
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 794
    const/4 v4, 0x0

    .line 795
    const-wide/16 v2, 0x0

    .line 797
    sget-object v0, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v1

    const/4 v1, 0x1

    iget-wide v8, p0, Lflipboard/io/DownloadManager;->o:J

    sub-long v8, v8, p1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v0, v1

    .line 800
    iget-object v1, p0, Lflipboard/io/DownloadManager;->i:Ljava/util/Map;

    monitor-enter v1

    .line 801
    :try_start_0
    iget-object v0, p0, Lflipboard/io/DownloadManager;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v5, p0, Lflipboard/io/DownloadManager;->i:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    new-array v5, v5, [Lflipboard/io/DownloadManager$FileData;

    invoke-interface {v0, v5}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/io/DownloadManager$FileData;

    .line 802
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 803
    const/4 v1, 0x0

    :goto_0
    array-length v5, v0

    if-ge v1, v5, :cond_1

    iget-wide v8, p0, Lflipboard/io/DownloadManager;->o:J

    cmp-long v5, v8, p1

    if-lez v5, :cond_1

    .line 804
    aget-object v5, v0, v1

    iget-object v5, v5, Lflipboard/io/DownloadManager$FileData;->a:Ljava/lang/String;

    .line 805
    invoke-direct {p0, v5}, Lflipboard/io/DownloadManager;->b(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 806
    iget-object v8, p0, Lflipboard/io/DownloadManager;->i:Ljava/util/Map;

    monitor-enter v8

    .line 807
    :try_start_1
    iget-object v9, p0, Lflipboard/io/DownloadManager;->i:Ljava/util/Map;

    invoke-interface {v9, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 808
    aget-object v9, v0, v1

    iget-wide v10, v9, Lflipboard/io/DownloadManager$FileData;->d:J

    .line 809
    invoke-static {v5}, Lflipboard/util/JavaUtil;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v5, v9}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;Ljava/lang/String;)J

    .line 810
    iget-wide v12, p0, Lflipboard/io/DownloadManager;->o:J

    sub-long/2addr v12, v10

    iput-wide v12, p0, Lflipboard/io/DownloadManager;->o:J

    .line 811
    iget v9, p0, Lflipboard/io/DownloadManager;->n:I

    add-int/lit8 v9, v9, -0x1

    iput v9, p0, Lflipboard/io/DownloadManager;->n:I

    .line 812
    add-long/2addr v2, v10

    .line 813
    add-int/lit8 v4, v4, 0x1

    .line 814
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 815
    sget-object v8, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    .line 803
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 802
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 814
    :catchall_1
    move-exception v0

    monitor-exit v8

    throw v0

    .line 819
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v6, v8, v6

    .line 820
    sget-object v1, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v1, v5

    const/4 v5, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v5

    const/4 v4, 0x2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x3

    iget-wide v4, p0, Lflipboard/io/DownloadManager;->o:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-wide v4, p0, Lflipboard/io/DownloadManager;->j:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-wide v4, p0, Lflipboard/io/DownloadManager;->o:J

    const-wide/16 v8, 0x64

    mul-long/2addr v4, v8

    iget-wide v8, p0, Lflipboard/io/DownloadManager;->j:J

    div-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 821
    iget-wide v2, p0, Lflipboard/io/DownloadManager;->o:J

    iget-wide v4, p0, Lflipboard/io/DownloadManager;->j:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 822
    sget-object v1, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const-string v2, "failed to purge enough, size=%,d, goal=%,d, limit=%,d, candidates=%d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, p0, Lflipboard/io/DownloadManager;->o:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-wide v6, p0, Lflipboard/io/DownloadManager;->j:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 824
    :cond_2
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    .line 297
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "clearing cache: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/io/DownloadManager;->d:Ljava/io/File;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 298
    iget-object v0, p0, Lflipboard/io/DownloadManager;->e:Ljava/io/File;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lflipboard/io/DownloadManager;->d:Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".tmp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 299
    if-eqz p1, :cond_0

    .line 301
    :try_start_0
    invoke-virtual {p0}, Lflipboard/io/DownloadManager;->a()Ljava/lang/Thread;

    move-result-object v0

    .line 302
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 303
    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    :cond_0
    :goto_0
    return-void

    .line 304
    :catch_0
    move-exception v0

    .line 305
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final declared-synchronized b()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 858
    monitor-enter p0

    :try_start_0
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    new-instance v3, Lflipboard/io/DownloadManager$3;

    invoke-direct {v3, p0}, Lflipboard/io/DownloadManager$3;-><init>(Lflipboard/io/DownloadManager;)V

    invoke-virtual {v0, v3}, Lflipboard/io/NetworkManager;->a(Lflipboard/util/Observer;)V

    .line 871
    iget-boolean v0, p0, Lflipboard/io/DownloadManager;->l:Z

    if-eqz v0, :cond_0

    .line 872
    sget-object v0, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    :goto_0
    move v4, v2

    .line 876
    :goto_1
    const/16 v0, 0x8

    if-ge v4, v0, :cond_3

    .line 877
    if-gtz v4, :cond_1

    move v3, v1

    .line 878
    :goto_2
    const/4 v0, 0x5

    if-le v4, v0, :cond_2

    move v0, v1

    .line 880
    :goto_3
    new-instance v5, Lflipboard/io/DownloadManager$4;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "downloader-"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, p0, v6, v3, v0}, Lflipboard/io/DownloadManager$4;-><init>(Lflipboard/io/DownloadManager;Ljava/lang/String;ZZ)V

    .line 937
    invoke-virtual {v5}, Lflipboard/io/DownloadManager$4;->start()V

    .line 876
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 874
    :cond_0
    sget-object v0, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_1
    move v3, v2

    .line 877
    goto :goto_2

    :cond_2
    move v0, v2

    .line 878
    goto :goto_3

    .line 939
    :cond_3
    monitor-exit p0

    return-void

    .line 858
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b(Z)V
    .locals 7

    .prologue
    const/16 v6, 0x19

    const/4 v0, 0x0

    .line 984
    monitor-enter p0

    :try_start_0
    sget-object v1, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lflipboard/io/DownloadManager;->l:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lflipboard/io/DownloadManager;->g:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 985
    const/4 v1, 0x0

    iput-boolean v1, p0, Lflipboard/io/DownloadManager;->l:Z

    .line 989
    iget-object v2, p0, Lflipboard/io/DownloadManager;->g:Ljava/util/Map;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 990
    :try_start_1
    iget-object v1, p0, Lflipboard/io/DownloadManager;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/Download;

    .line 991
    sget-object v4, Lflipboard/io/DownloadManager$5;->a:[I

    iget-object v5, v0, Lflipboard/io/Download;->d:Lflipboard/io/Download$Status;

    invoke-virtual {v5}, Lflipboard/io/Download$Status;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 995
    :pswitch_0
    iget v4, v0, Lflipboard/io/Download;->e:I

    if-ge v4, v6, :cond_1

    if-eqz p1, :cond_0

    .line 996
    :cond_1
    sget-object v4, Lflipboard/io/Download$Status;->b:Lflipboard/io/Download$Status;

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V

    .line 997
    iget-object v4, p0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 998
    :try_start_2
    iget-object v5, p0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-interface {v5, v0}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    .line 999
    add-int/lit8 v0, v1, 0x1

    .line 1000
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1013
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v2

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 984
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1004
    :pswitch_1
    :try_start_5
    iget v4, v0, Lflipboard/io/Download;->e:I

    if-ge v4, v6, :cond_0

    if-nez p1, :cond_0

    .line 1005
    sget-object v4, Lflipboard/io/Download$Status;->e:Lflipboard/io/Download$Status;

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V

    .line 1006
    iget-object v4, p0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    monitor-enter v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1007
    :try_start_6
    iget-object v5, p0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-interface {v5, v0}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z

    .line 1008
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    goto :goto_0

    :catchall_3
    move-exception v0

    :try_start_7
    monitor-exit v4

    throw v0

    .line 1013
    :cond_2
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1014
    :try_start_8
    sget-object v0, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    .line 1017
    iget-object v1, p0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    monitor-enter v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 1018
    :try_start_9
    iget-object v0, p0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1019
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    monitor-exit p0

    return-void

    :catchall_4
    move-exception v0

    :try_start_a
    monitor-exit v1

    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 991
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final declared-synchronized c()V
    .locals 6

    .prologue
    .line 946
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lflipboard/io/DownloadManager;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_1

    .line 973
    :cond_0
    monitor-exit p0

    return-void

    .line 949
    :cond_1
    :try_start_1
    sget-object v0, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    .line 950
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/io/DownloadManager;->l:Z

    .line 953
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 954
    iget-object v2, p0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 955
    :try_start_2
    iget-object v0, p0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 956
    iget-object v0, p0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->clear()V

    .line 957
    iget-object v0, p0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 958
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 961
    :try_start_3
    iget-object v2, p0, Lflipboard/io/DownloadManager;->g:Ljava/util/Map;

    monitor-enter v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 962
    :try_start_4
    iget-object v0, p0, Lflipboard/io/DownloadManager;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/Download;

    .line 963
    sget-object v4, Lflipboard/io/DownloadManager$5;->a:[I

    iget-object v5, v0, Lflipboard/io/Download;->d:Lflipboard/io/Download$Status;

    invoke-virtual {v5}, Lflipboard/io/Download$Status;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 965
    :pswitch_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 969
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v2

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 946
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 958
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v2

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 969
    :cond_2
    :try_start_7
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 970
    :try_start_8
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/Download;

    .line 971
    sget-object v2, Lflipboard/io/Download$Status;->e:Lflipboard/io/Download$Status;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    .line 963
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public final declared-synchronized d()V
    .locals 10

    .prologue
    const/4 v2, -0x1

    const/4 v0, 0x0

    .line 1043
    monitor-enter p0

    :try_start_0
    new-instance v3, Ljava/util/TreeSet;

    invoke-direct {v3}, Ljava/util/TreeSet;-><init>()V

    .line 1044
    iget-object v1, p0, Lflipboard/io/DownloadManager;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/SortedSet;->addAll(Ljava/util/Collection;)Z

    .line 1047
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/16 v1, 0x8

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v5, p0, Lflipboard/io/DownloadManager;->g:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v5, 0x1

    iget-boolean v1, p0, Lflipboard/io/DownloadManager;->l:Z

    if-eqz v1, :cond_0

    const-string v1, "paused"

    :goto_0
    aput-object v1, v4, v5

    const/4 v1, 0x2

    iget-object v5, p0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-interface {v5}, Ljava/util/SortedSet;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x3

    iget v5, p0, Lflipboard/io/DownloadManager;->p:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x4

    iget v5, p0, Lflipboard/io/DownloadManager;->n:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x5

    iget-wide v6, p0, Lflipboard/io/DownloadManager;->o:J

    const-wide/16 v8, 0x400

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x6

    iget-wide v6, p0, Lflipboard/io/DownloadManager;->j:J

    const-wide/16 v8, 0x400

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x7

    iget-wide v6, p0, Lflipboard/io/DownloadManager;->o:J

    const-wide/16 v8, 0x64

    mul-long/2addr v6, v8

    iget-wide v8, p0, Lflipboard/io/DownloadManager;->j:J

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v1

    .line 1048
    invoke-interface {v3}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v0

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/Download;

    .line 1049
    iget-object v7, v0, Lflipboard/io/Download;->f:Lflipboard/io/Download$Data;

    .line 1050
    if-nez v7, :cond_1

    const-wide/16 v4, -0x1

    .line 1051
    :goto_2
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/16 v1, 0x8

    new-array v8, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v1

    const/4 v1, 0x1

    iget-object v9, v0, Lflipboard/io/Download;->d:Lflipboard/io/Download$Status;

    aput-object v9, v8, v1

    const/4 v1, 0x2

    iget v9, v0, Lflipboard/io/Download;->g:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v1

    const/4 v1, 0x3

    iget v9, v0, Lflipboard/io/Download;->e:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v1

    const/4 v1, 0x4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v8, v1

    const/4 v4, 0x5

    if-nez v7, :cond_2

    move v1, v2

    :goto_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v8, v4

    const/4 v4, 0x6

    if-nez v7, :cond_3

    move v1, v2

    :goto_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v8, v4

    const/4 v1, 0x7

    iget-object v0, v0, Lflipboard/io/Download;->b:Ljava/lang/String;

    aput-object v0, v8, v1

    .line 1052
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    .line 1053
    goto :goto_1

    .line 1047
    :cond_0
    const-string v1, "active"

    goto/16 :goto_0

    .line 1050
    :cond_1
    iget-wide v4, v7, Lflipboard/io/Download$Data;->d:J

    goto :goto_2

    .line 1051
    :cond_2
    invoke-virtual {v7}, Lflipboard/io/Download$Data;->e()I

    move-result v1

    goto :goto_3

    :cond_3
    invoke-virtual {v7}, Lflipboard/io/Download$Data;->f()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    goto :goto_4

    .line 1054
    :cond_4
    monitor-exit p0

    return-void

    .line 1043
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
