.class public final Lflipboard/io/UsageManager;
.super Ljava/lang/Object;
.source "UsageManager.java"


# static fields
.field public static final a:Lflipboard/util/Log;

.field public static b:Lflipboard/io/UsageManager;

.field static final c:Ljava/text/SimpleDateFormat;


# instance fields
.field final d:Ljava/io/File;

.field final e:Ljava/lang/String;

.field final f:Ljava/lang/String;

.field final g:Ljava/lang/String;

.field final h:I

.field final i:Landroid/content/SharedPreferences;

.field public final j:Landroid/content/SharedPreferences;

.field k:Ljava/io/RandomAccessFile;

.field l:I

.field m:I

.field public final n:Lflipboard/io/UsageManager$Uploader;

.field o:Ljava/lang/String;

.field p:Ljava/io/File;

.field q:Ljava/lang/String;

.field r:J

.field s:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 67
    const-string v0, "usage"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    .line 77
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MM-dd-yy HH:mm:ss +0000"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 83
    sput-object v0, Lflipboard/io/UsageManager;->c:Ljava/text/SimpleDateFormat;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 13

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    sput-object p0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    .line 126
    const-string v0, "usage"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lflipboard/io/UsageManager;->d:Ljava/io/File;

    .line 127
    new-instance v0, Lflipboard/io/UsageManager$Uploader;

    invoke-direct {v0, p0}, Lflipboard/io/UsageManager$Uploader;-><init>(Lflipboard/io/UsageManager;)V

    iput-object v0, p0, Lflipboard/io/UsageManager;->n:Lflipboard/io/UsageManager$Uploader;

    .line 128
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    iput-object v0, p0, Lflipboard/io/UsageManager;->i:Landroid/content/SharedPreferences;

    .line 129
    const-string v0, "flipboard_local_usage"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lflipboard/io/UsageManager;->j:Landroid/content/SharedPreferences;

    .line 131
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 133
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 134
    iget-object v1, v0, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    iput-object v1, p0, Lflipboard/io/UsageManager;->e:Ljava/lang/String;

    .line 135
    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/io/UsageManager;->f:Ljava/lang/String;

    .line 136
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/io/UsageManager;->g:Ljava/lang/String;

    .line 137
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 138
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/16 v4, 0x10

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/2addr v0, v1

    int-to-long v0, v0

    const-wide/32 v4, 0x36ee80

    div-long/2addr v0, v4

    long-to-int v0, v0

    iput v0, p0, Lflipboard/io/UsageManager;->h:I

    .line 139
    iget-object v0, p0, Lflipboard/io/UsageManager;->i:Landroid/content/SharedPreferences;

    const-string v1, "uploadTime"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/io/UsageManager;->s:J

    .line 141
    iget-wide v0, p0, Lflipboard/io/UsageManager;->s:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 142
    sget-object v0, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    .line 147
    :goto_0
    invoke-virtual {p0}, Lflipboard/io/UsageManager;->b()V

    .line 150
    iget-object v0, p0, Lflipboard/io/UsageManager;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    array-length v4, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_5

    aget-object v5, v1, v0

    .line 151
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lflipboard/io/UsageManager;->d:Ljava/io/File;

    invoke-direct {v6, v7, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 152
    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v8

    .line 153
    const-string v7, "current-"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 154
    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-nez v7, :cond_1

    .line 155
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 171
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 144
    :cond_0
    sget-object v0, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lflipboard/io/UsageManager;->s:J

    sub-long/2addr v4, v6

    const-wide/32 v6, 0xea60

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v0, v1

    goto :goto_0

    .line 158
    :cond_1
    iget-object v6, p0, Lflipboard/io/UsageManager;->p:Ljava/io/File;

    if-eqz v6, :cond_2

    .line 159
    sget-object v6, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v7, "multiple current usage files: %s, %s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lflipboard/io/UsageManager;->p:Ljava/io/File;

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v5, v10, v11

    invoke-virtual {v6, v7, v10}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 160
    iget-object v6, p0, Lflipboard/io/UsageManager;->p:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 162
    :cond_2
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lflipboard/io/UsageManager;->d:Ljava/io/File;

    invoke-direct {v6, v7, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v6, p0, Lflipboard/io/UsageManager;->p:Ljava/io/File;

    .line 163
    const/16 v6, 0x8

    const/16 v7, 0x2e

    const/16 v10, 0x8

    invoke-virtual {v5, v7, v10}, Ljava/lang/String;->indexOf(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lflipboard/io/UsageManager;->q:Ljava/lang/String;

    .line 164
    const-wide/16 v6, 0x64

    div-long v6, v8, v6

    long-to-int v5, v6

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lflipboard/io/UsageManager;->m:I

    goto :goto_2

    .line 165
    :cond_3
    const-string v7, "pending-"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 166
    iget v6, p0, Lflipboard/io/UsageManager;->l:I

    const/16 v7, 0x8

    const/16 v10, 0x2e

    const/16 v11, 0x8

    invoke-virtual {v5, v10, v11}, Ljava/lang/String;->indexOf(II)I

    move-result v10

    invoke-virtual {v5, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lflipboard/io/UsageManager;->l:I

    .line 167
    iget-wide v6, p0, Lflipboard/io/UsageManager;->r:J

    add-long/2addr v6, v8

    iput-wide v6, p0, Lflipboard/io/UsageManager;->r:J

    goto/16 :goto_2

    .line 169
    :cond_4
    sget-object v5, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    const-string v7, "invalid usage file: %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v6, v8, v9

    invoke-virtual {v5, v7, v8}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 170
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    goto/16 :goto_2

    .line 175
    :cond_5
    iget-object v0, p0, Lflipboard/io/UsageManager;->p:Ljava/io/File;

    if-eqz v0, :cond_6

    .line 176
    sget-object v0, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v4, p0, Lflipboard/io/UsageManager;->m:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x1

    iget-object v4, p0, Lflipboard/io/UsageManager;->q:Ljava/lang/String;

    aput-object v4, v0, v1

    const/4 v1, 0x2

    iget-object v4, p0, Lflipboard/io/UsageManager;->p:Ljava/io/File;

    aput-object v4, v0, v1

    .line 180
    :cond_6
    iget-object v0, p0, Lflipboard/io/UsageManager;->p:Ljava/io/File;

    if-eqz v0, :cond_8

    iget v0, p0, Lflipboard/io/UsageManager;->m:I

    const/16 v1, 0x64

    if-gt v0, v1, :cond_7

    iget-object v0, p0, Lflipboard/io/UsageManager;->p:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    const-wide/32 v4, 0x36ee80

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_8

    .line 181
    :cond_7
    iget-object v0, p0, Lflipboard/io/UsageManager;->n:Lflipboard/io/UsageManager$Uploader;

    invoke-virtual {v0}, Lflipboard/io/UsageManager$Uploader;->b()V

    .line 183
    :cond_8
    return-void
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    .line 236
    iget-object v0, v0, Lflipboard/model/ConfigSetting;->UsageHost:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 760
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    invoke-direct {v0, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 762
    :try_start_0
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v0, v2

    const/4 v2, 0x1

    aput-object p1, v0, v2

    .line 763
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    .line 764
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 763
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 766
    :cond_0
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 768
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 769
    return-void

    .line 768
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    throw v0
.end method

.method public static e()Z
    .locals 3

    .prologue
    .line 809
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "usage_redirect_for_monitoring"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 293
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "UsageManager:logEventLocally"

    new-instance v2, Lflipboard/io/UsageManager$3;

    invoke-direct {v2, p0, p1}, Lflipboard/io/UsageManager$3;-><init>(Lflipboard/io/UsageManager;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 304
    return-void
.end method

.method public final a(Ljava/lang/Thread;Ljava/lang/Throwable;Z)V
    .locals 7

    .prologue
    .line 194
    :try_start_0
    new-instance v1, Lflipboard/io/UsageEvent;

    const-string v0, "performance"

    invoke-direct {v1, v0}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 195
    new-instance v2, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 197
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "exception."

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 198
    if-eqz p3, :cond_0

    .line 199
    const-string v0, "uncaught."

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    :goto_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 205
    :goto_1
    invoke-virtual {p2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_2

    move-object p2, v0

    .line 206
    goto :goto_1

    .line 201
    :cond_0
    const-string v0, "caught."

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 227
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 231
    :cond_1
    :goto_2
    return-void

    .line 208
    :cond_2
    :try_start_1
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Name: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 209
    instance-of v0, p2, Ljava/lang/NullPointerException;

    if-eqz v0, :cond_3

    .line 210
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "NullPointerException"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v0, v5, v6}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Number;Z)Lflipboard/json/FLObject;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    :cond_3
    instance-of v0, p2, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_4

    .line 213
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "RuntimeException"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v0, v4, v5}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Number;Z)Lflipboard/json/FLObject;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    :goto_3
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    const-string v0, "metrics"

    invoke-virtual {v1, v0, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 221
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "event: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lflipboard/io/UsageEvent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 222
    invoke-virtual {v1}, Lflipboard/io/UsageEvent;->a()V

    .line 224
    iget-object v0, p0, Lflipboard/io/UsageManager;->n:Lflipboard/io/UsageManager$Uploader;

    invoke-virtual {v0}, Lflipboard/io/UsageManager$Uploader;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p0, Lflipboard/io/UsageManager;->n:Lflipboard/io/UsageManager$Uploader;

    invoke-virtual {v0}, Lflipboard/io/UsageManager$Uploader;->a()V

    goto/16 :goto_2

    .line 214
    :cond_4
    instance-of v0, p2, Ljava/io/IOException;

    if-eqz v0, :cond_5

    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "IOException"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v0, v4, v5}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Number;Z)Lflipboard/json/FLObject;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 217
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "other"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v0, v4, v5}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Number;Z)Lflipboard/json/FLObject;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 187
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/Thread;Ljava/lang/Throwable;Z)V

    .line 188
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 713
    iget-object v0, p0, Lflipboard/io/UsageManager;->n:Lflipboard/io/UsageManager$Uploader;

    invoke-virtual {v0}, Lflipboard/io/UsageManager$Uploader;->b()V

    .line 714
    iget-object v0, p0, Lflipboard/io/UsageManager;->n:Lflipboard/io/UsageManager$Uploader;

    invoke-virtual {v0, p1}, Lflipboard/io/UsageManager$Uploader;->a(Z)V

    .line 715
    return-void
.end method

.method final a(Ljava/io/File;)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 781
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 782
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 785
    :try_start_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_0

    .line 786
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 785
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 789
    :cond_0
    :try_start_1
    new-instance v2, Lorg/json/JSONObject;

    const/4 v5, 0x5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 790
    sget-object v5, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v2}, Lorg/json/JSONObject;->length()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 802
    :goto_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 804
    return v0

    .line 792
    :catch_0
    move-exception v0

    .line 793
    :try_start_2
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v5, "Error parsing: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v2, v5, v6}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 794
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v5}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 795
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 797
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->J()V

    .line 798
    invoke-virtual {p0, v0}, Lflipboard/io/UsageManager;->a(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    .line 799
    goto :goto_1

    .line 802
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    throw v0
.end method

.method public final b()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 241
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 242
    iget-object v1, p0, Lflipboard/io/UsageManager;->i:Landroid/content/SharedPreferences;

    const-string v2, "launchCount"

    invoke-interface {v1, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 243
    iget-object v2, p0, Lflipboard/io/UsageManager;->i:Landroid/content/SharedPreferences;

    const-string v3, "currentVersionLaunchCount"

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 244
    sget-object v3, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lflipboard/io/UsageManager;->o:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    .line 245
    add-int/lit8 v1, v1, 0x1

    .line 246
    add-int/lit8 v2, v2, 0x1

    .line 247
    iget-object v3, p0, Lflipboard/io/UsageManager;->i:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "launchCount"

    .line 248
    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "currentVersionLaunchCount"

    .line 249
    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 247
    invoke-static {v3}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 250
    const-string v3, "%02d-%03d-%02d"

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    rem-int/lit8 v5, v5, 0x64

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x6

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v7

    rem-int/lit8 v0, v1, 0x64

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v8

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/io/UsageManager;->o:Ljava/lang/String;

    .line 251
    sget-object v0, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    new-array v0, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lflipboard/io/UsageManager;->o:Ljava/lang/String;

    aput-object v3, v0, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v8

    .line 252
    return-void
.end method

.method public final c()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 723
    iget-object v1, p0, Lflipboard/io/UsageManager;->n:Lflipboard/io/UsageManager$Uploader;

    monitor-enter v1

    .line 724
    :try_start_0
    iget-object v2, p0, Lflipboard/io/UsageManager;->n:Lflipboard/io/UsageManager$Uploader;

    invoke-virtual {v2}, Lflipboard/io/UsageManager$Uploader;->b()V

    .line 725
    iget-object v2, p0, Lflipboard/io/UsageManager;->d:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 726
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lflipboard/io/UsageManager;->d:Ljava/io/File;

    invoke-direct {v5, v6, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 727
    sget-object v4, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    .line 728
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 725
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 730
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/io/UsageManager;->m:I

    .line 731
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/io/UsageManager;->l:I

    .line 732
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lflipboard/io/UsageManager;->r:J

    .line 733
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()V
    .locals 7

    .prologue
    .line 742
    iget-object v2, p0, Lflipboard/io/UsageManager;->n:Lflipboard/io/UsageManager$Uploader;

    monitor-enter v2

    .line 743
    :try_start_0
    iget-object v0, p0, Lflipboard/io/UsageManager;->n:Lflipboard/io/UsageManager$Uploader;

    invoke-virtual {v0}, Lflipboard/io/UsageManager$Uploader;->b()V

    .line 744
    iget-object v0, p0, Lflipboard/io/UsageManager;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v0, v3, v1

    .line 745
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lflipboard/io/UsageManager;->d:Ljava/io/File;

    invoke-direct {v5, v6, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 747
    :try_start_1
    const-string v0, "dump usage"

    invoke-static {v0, v5}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 744
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 748
    :catch_0
    move-exception v0

    .line 749
    :try_start_2
    sget-object v5, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v5, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 752
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method
