.class public abstract Lflipboard/io/BitmapManager$Handle;
.super Ljava/lang/Object;
.source "BitmapManager.java"


# instance fields
.field d:I

.field e:Lflipboard/io/BitmapManager$Handle;

.field f:Lflipboard/io/BitmapManager$Handle;

.field g:Landroid/graphics/Bitmap;

.field h:I

.field i:I

.field j:Z

.field k:Z

.field final synthetic l:Lflipboard/io/BitmapManager;


# direct methods
.method public constructor <init>(Lflipboard/io/BitmapManager;)V
    .locals 0

    .prologue
    .line 420
    iput-object p1, p0, Lflipboard/io/BitmapManager$Handle;->l:Lflipboard/io/BitmapManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(IIZ)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 642
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 643
    if-eqz v0, :cond_0

    .line 644
    invoke-static {v0, p1, p2, p3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 646
    :cond_0
    monitor-exit p0

    return-object v0

    .line 642
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Landroid/graphics/Rect;F)Lflipboard/io/BitmapManager$Handle;
    .locals 3

    .prologue
    .line 553
    iget-object v2, p0, Lflipboard/io/BitmapManager$Handle;->l:Lflipboard/io/BitmapManager;

    monitor-enter v2

    .line 554
    :try_start_0
    new-instance v1, Lflipboard/io/BitmapManager$ScaledHandle;

    iget-object v0, p0, Lflipboard/io/BitmapManager$Handle;->l:Lflipboard/io/BitmapManager;

    invoke-direct {v1, v0, p0, p1, p2}, Lflipboard/io/BitmapManager$ScaledHandle;-><init>(Lflipboard/io/BitmapManager;Lflipboard/io/BitmapManager$Handle;Landroid/graphics/Rect;F)V

    .line 555
    iget-object v0, p0, Lflipboard/io/BitmapManager$Handle;->l:Lflipboard/io/BitmapManager;

    invoke-static {v0}, Lflipboard/io/BitmapManager;->b(Lflipboard/io/BitmapManager;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/BitmapManager$ScaledHandle;

    .line 556
    if-eqz v0, :cond_0

    .line 557
    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->d()V

    .line 558
    iget v1, v0, Lflipboard/io/BitmapManager$ScaledHandle;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lflipboard/io/BitmapManager$ScaledHandle;->d:I

    .line 559
    monitor-exit v2

    .line 563
    :goto_0
    return-object v0

    .line 561
    :cond_0
    iget-object v0, p0, Lflipboard/io/BitmapManager$Handle;->l:Lflipboard/io/BitmapManager;

    invoke-static {v0}, Lflipboard/io/BitmapManager;->b(Lflipboard/io/BitmapManager;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 562
    iget v0, v1, Lflipboard/io/BitmapManager$ScaledHandle;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, Lflipboard/io/BitmapManager$ScaledHandle;->d:I

    .line 563
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    goto :goto_0

    .line 564
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method abstract a()Ljava/lang/Object;
.end method

.method public final declared-synchronized a(Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    .locals 1

    .prologue
    .line 634
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p1, v0, p2, p3, p4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 635
    monitor-exit p0

    return-void

    .line 634
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(Ljava/lang/StringBuilder;)V
    .locals 1

    .prologue
    .line 698
    iget v0, p0, Lflipboard/io/BitmapManager$Handle;->h:I

    if-gtz v0, :cond_0

    iget v0, p0, Lflipboard/io/BitmapManager$Handle;->i:I

    if-lez v0, :cond_1

    .line 699
    :cond_0
    const/16 v0, 0x2c

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 700
    iget v0, p0, Lflipboard/io/BitmapManager$Handle;->h:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 701
    const/16 v0, 0x78

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 702
    iget v0, p0, Lflipboard/io/BitmapManager$Handle;->i:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 704
    :cond_1
    return-void
.end method

.method public b()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 436
    :goto_0
    monitor-enter p0

    .line 437
    :try_start_0
    iget-object v0, p0, Lflipboard/io/BitmapManager$Handle;->g:Landroid/graphics/Bitmap;

    .line 438
    if-eqz v0, :cond_0

    .line 439
    iget-object v1, p0, Lflipboard/io/BitmapManager$Handle;->l:Lflipboard/io/BitmapManager;

    invoke-static {v1, p0}, Lflipboard/io/BitmapManager;->a(Lflipboard/io/BitmapManager;Lflipboard/io/BitmapManager$Handle;)V

    .line 440
    iget-object v1, p0, Lflipboard/io/BitmapManager$Handle;->l:Lflipboard/io/BitmapManager;

    invoke-static {v1, p0}, Lflipboard/io/BitmapManager;->b(Lflipboard/io/BitmapManager;Lflipboard/io/BitmapManager$Handle;)V

    .line 441
    monitor-exit p0

    .line 474
    :goto_1
    return-object v0

    .line 443
    :cond_0
    iget-boolean v0, p0, Lflipboard/io/BitmapManager$Handle;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 445
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 449
    monitor-exit p0

    goto :goto_0

    .line 447
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 452
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 451
    :cond_1
    const/4 v0, 0x1

    :try_start_3
    iput-boolean v0, p0, Lflipboard/io/BitmapManager$Handle;->k:Z

    .line 452
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 455
    :try_start_4
    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->c()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 456
    if-nez v0, :cond_2

    .line 457
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "failed to create bitmap for: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 458
    invoke-static {}, Lflipboard/io/BitmapManager;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 460
    :cond_2
    monitor-enter p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 461
    :try_start_5
    iput-object v0, p0, Lflipboard/io/BitmapManager$Handle;->g:Landroid/graphics/Bitmap;

    .line 462
    iget v1, p0, Lflipboard/io/BitmapManager$Handle;->h:I

    if-eqz v1, :cond_3

    iget v1, p0, Lflipboard/io/BitmapManager$Handle;->i:I

    if-nez v1, :cond_4

    .line 463
    :cond_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iput v1, p0, Lflipboard/io/BitmapManager$Handle;->h:I

    .line 464
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iput v1, p0, Lflipboard/io/BitmapManager$Handle;->i:I

    .line 466
    :cond_4
    iget-object v1, p0, Lflipboard/io/BitmapManager$Handle;->l:Lflipboard/io/BitmapManager;

    invoke-static {v1, p0}, Lflipboard/io/BitmapManager;->b(Lflipboard/io/BitmapManager;Lflipboard/io/BitmapManager$Handle;)V

    .line 467
    iget-object v1, p0, Lflipboard/io/BitmapManager$Handle;->l:Lflipboard/io/BitmapManager;

    invoke-static {v1, p0}, Lflipboard/io/BitmapManager;->c(Lflipboard/io/BitmapManager;Lflipboard/io/BitmapManager$Handle;)V

    .line 468
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 471
    monitor-enter p0

    .line 472
    const/4 v1, 0x0

    :try_start_6
    iput-boolean v1, p0, Lflipboard/io/BitmapManager$Handle;->k:Z

    .line 473
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 474
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 469
    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit p0

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 471
    :catchall_3
    move-exception v0

    monitor-enter p0

    .line 472
    const/4 v1, 0x0

    :try_start_8
    iput-boolean v1, p0, Lflipboard/io/BitmapManager$Handle;->k:Z

    .line 473
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 474
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    throw v0

    :catchall_4
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method b(Landroid/graphics/Rect;F)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 604
    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->c()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 606
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 607
    invoke-virtual {v5, p2, p2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 610
    :try_start_0
    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 623
    :goto_0
    return-object v0

    .line 612
    :catch_0
    move-exception v1

    :try_start_1
    iget-object v1, p0, Lflipboard/io/BitmapManager$Handle;->l:Lflipboard/io/BitmapManager;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    mul-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lflipboard/io/BitmapManager;->c(I)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    .line 614
    :try_start_2
    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    goto :goto_0

    .line 615
    :catch_1
    move-exception v1

    .line 616
    :try_start_3
    iget-object v2, p0, Lflipboard/io/BitmapManager$Handle;->l:Lflipboard/io/BitmapManager;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    mul-int/2addr v2, v3

    invoke-static {v2}, Lflipboard/io/BitmapManager;->b(I)V

    .line 617
    throw v1
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2

    .line 620
    :catch_2
    move-exception v1

    .line 621
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "scale: %s, scale=%f, crop=%d,%d,%dx%d"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v4, v7

    const/4 v0, 0x2

    iget v5, p1, Landroid/graphics/Rect;->left:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x3

    iget v5, p1, Landroid/graphics/Rect;->top:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x4

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x5

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 622
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v0, v1}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    .line 623
    invoke-static {}, Lflipboard/io/BitmapManager;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method abstract c()Landroid/graphics/Bitmap;
.end method

.method public d()V
    .locals 5

    .prologue
    .line 578
    iget-object v1, p0, Lflipboard/io/BitmapManager$Handle;->l:Lflipboard/io/BitmapManager;

    const/4 v0, 0x0

    monitor-enter v1

    :try_start_0
    iget v2, p0, Lflipboard/io/BitmapManager$Handle;->d:I

    if-gtz v2, :cond_2

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "refcount error: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    sget-object v0, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    invoke-virtual {v0}, Lflipboard/io/DownloadManager;->d()V

    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager;->c()V

    :cond_0
    monitor-exit v1

    .line 579
    :cond_1
    :goto_0
    return-void

    .line 578
    :cond_2
    iget v2, p0, Lflipboard/io/BitmapManager$Handle;->d:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lflipboard/io/BitmapManager$Handle;->d:I

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lflipboard/io/BitmapManager$Handle;->j:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lflipboard/io/BitmapManager$Handle;->g:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    sget-object v2, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->f()V

    invoke-virtual {v1, p0}, Lflipboard/io/BitmapManager;->b(Lflipboard/io/BitmapManager$Handle;)V

    iget v2, v1, Lflipboard/io/BitmapManager;->d:I

    iget v3, p0, Lflipboard/io/BitmapManager$Handle;->h:I

    iget v4, p0, Lflipboard/io/BitmapManager$Handle;->i:I

    mul-int/2addr v3, v4

    sub-int/2addr v2, v3

    iput v2, v1, Lflipboard/io/BitmapManager;->d:I

    :cond_3
    iget-object v2, p0, Lflipboard/io/BitmapManager$Handle;->g:Landroid/graphics/Bitmap;

    if-nez v2, :cond_4

    iget-object v0, v1, Lflipboard/io/BitmapManager;->c:Ljava/util/Map;

    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->g()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_4
    move-object p0, v0

    goto :goto_1
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 536
    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v0

    return v0
.end method

.method f()V
    .locals 0

    .prologue
    .line 586
    return-void
.end method

.method g()V
    .locals 0

    .prologue
    .line 593
    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->f()V

    .line 594
    return-void
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 485
    iget v0, p0, Lflipboard/io/BitmapManager$Handle;->h:I

    if-nez v0, :cond_0

    iget v0, p0, Lflipboard/io/BitmapManager$Handle;->i:I

    if-nez v0, :cond_0

    .line 486
    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->b()Landroid/graphics/Bitmap;

    .line 488
    :cond_0
    iget v0, p0, Lflipboard/io/BitmapManager$Handle;->h:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 569
    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 497
    iget v0, p0, Lflipboard/io/BitmapManager$Handle;->h:I

    if-nez v0, :cond_0

    iget v0, p0, Lflipboard/io/BitmapManager$Handle;->i:I

    if-nez v0, :cond_0

    .line 498
    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->b()Landroid/graphics/Bitmap;

    .line 500
    :cond_0
    iget v0, p0, Lflipboard/io/BitmapManager$Handle;->i:I

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lflipboard/io/BitmapManager$Handle;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 629
    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized l()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 650
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 651
    if-eqz v0, :cond_0

    .line 652
    invoke-static {v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 654
    :cond_0
    monitor-exit p0

    return-object v0

    .line 650
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized m()F
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 678
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->b()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 679
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v3, v1, 0x8

    .line 680
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 681
    const/4 v1, 0x4

    if-le v4, v1, :cond_0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lt v3, v1, :cond_1

    .line 693
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 685
    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_3

    .line 686
    :try_start_1
    invoke-virtual {v2, v3, v1}, Landroid/graphics/Bitmap;->getPixel(II)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    .line 687
    ushr-int/lit8 v6, v5, 0x18

    const/16 v7, 0xff

    if-ne v6, v7, :cond_2

    .line 688
    shr-int/lit8 v6, v5, 0x10

    and-int/lit16 v6, v6, 0xff

    int-to-float v6, v6

    add-float/2addr v0, v6

    .line 689
    shr-int/lit8 v6, v5, 0x8

    and-int/lit16 v6, v6, 0xff

    int-to-float v6, v6

    add-float/2addr v0, v6

    .line 690
    and-int/lit16 v5, v5, 0xff

    int-to-float v5, v5

    add-float/2addr v0, v5

    .line 685
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 693
    :cond_3
    mul-int/lit8 v1, v4, 0x3

    int-to-float v1, v1

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    goto :goto_0

    .line 678
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 708
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 709
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 710
    const/16 v2, 0x24

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 711
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 712
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 713
    const-string v1, "0x%08x,"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 714
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lflipboard/io/BitmapManager$Handle;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 715
    iget-object v1, p0, Lflipboard/io/BitmapManager$Handle;->g:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    .line 716
    const-string v1, "purged,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 720
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lflipboard/io/BitmapManager$Handle;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 721
    invoke-virtual {p0, v0}, Lflipboard/io/BitmapManager$Handle;->a(Ljava/lang/StringBuilder;)V

    .line 722
    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 723
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 717
    :cond_1
    iget-object v1, p0, Lflipboard/io/BitmapManager$Handle;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 718
    const-string v1, "RECYCLED,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
