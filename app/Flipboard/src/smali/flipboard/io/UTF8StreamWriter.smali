.class public Lflipboard/io/UTF8StreamWriter;
.super Ljava/io/Writer;
.source "UTF8StreamWriter.java"


# instance fields
.field public final a:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/io/Writer;-><init>()V

    .line 18
    iput-object p1, p0, Lflipboard/io/UTF8StreamWriter;->a:Ljava/io/OutputStream;

    .line 19
    return-void
.end method


# virtual methods
.method public final varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 97
    invoke-static {p1, p2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/io/UTF8StreamWriter;->write(Ljava/lang/String;)V

    .line 98
    return-void
.end method

.method public final a([B)V
    .locals 2

    .prologue
    .line 44
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lflipboard/io/UTF8StreamWriter;->a([BII)V

    .line 45
    return-void
.end method

.method public final a([BII)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lflipboard/io/UTF8StreamWriter;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 49
    return-void
.end method

.method public append(C)Ljava/io/Writer;
    .locals 0

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lflipboard/io/UTF8StreamWriter;->write(I)V

    .line 24
    return-object p0
.end method

.method public append(Ljava/lang/CharSequence;)Ljava/io/Writer;
    .locals 2

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lflipboard/io/UTF8StreamWriter;->append(Ljava/lang/CharSequence;II)Ljava/io/Writer;

    move-result-object v0

    return-object v0
.end method

.method public append(Ljava/lang/CharSequence;II)Ljava/io/Writer;
    .locals 2

    .prologue
    .line 32
    :goto_0
    if-ge p2, p3, :cond_0

    .line 33
    add-int/lit8 v0, p2, 0x1

    invoke-interface {p1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, Lflipboard/io/UTF8StreamWriter;->write(I)V

    move p2, v0

    goto :goto_0

    .line 35
    :cond_0
    return-object p0
.end method

.method public bridge synthetic append(C)Ljava/lang/Appendable;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lflipboard/io/UTF8StreamWriter;->append(C)Ljava/io/Writer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lflipboard/io/UTF8StreamWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0, p1, p2, p3}, Lflipboard/io/UTF8StreamWriter;->append(Ljava/lang/CharSequence;II)Ljava/io/Writer;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lflipboard/io/UTF8StreamWriter;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 107
    return-void
.end method

.method public flush()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lflipboard/io/UTF8StreamWriter;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 103
    return-void
.end method

.method public write(I)V
    .locals 2

    .prologue
    .line 73
    const/16 v0, 0x80

    if-ge p1, v0, :cond_0

    .line 74
    iget-object v0, p0, Lflipboard/io/UTF8StreamWriter;->a:Ljava/io/OutputStream;

    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 93
    :goto_0
    return-void

    .line 75
    :cond_0
    const/16 v0, 0x800

    if-ge p1, v0, :cond_1

    .line 76
    iget-object v0, p0, Lflipboard/io/UTF8StreamWriter;->a:Ljava/io/OutputStream;

    shr-int/lit8 v1, p1, 0x6

    add-int/lit16 v1, v1, 0xc0

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 77
    iget-object v0, p0, Lflipboard/io/UTF8StreamWriter;->a:Ljava/io/OutputStream;

    and-int/lit8 v1, p1, 0x3f

    add-int/lit16 v1, v1, 0x80

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0

    .line 79
    :cond_1
    invoke-static {p1}, Lflipboard/util/JavaUtil;->a(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 80
    const p1, 0xfffd

    .line 82
    :cond_2
    const/high16 v0, 0x10000

    if-ge p1, v0, :cond_3

    .line 83
    iget-object v0, p0, Lflipboard/io/UTF8StreamWriter;->a:Ljava/io/OutputStream;

    shr-int/lit8 v1, p1, 0xc

    add-int/lit16 v1, v1, 0xe0

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 84
    iget-object v0, p0, Lflipboard/io/UTF8StreamWriter;->a:Ljava/io/OutputStream;

    shr-int/lit8 v1, p1, 0x6

    and-int/lit8 v1, v1, 0x3f

    add-int/lit16 v1, v1, 0x80

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 85
    iget-object v0, p0, Lflipboard/io/UTF8StreamWriter;->a:Ljava/io/OutputStream;

    and-int/lit8 v1, p1, 0x3f

    add-int/lit16 v1, v1, 0x80

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0

    .line 87
    :cond_3
    iget-object v0, p0, Lflipboard/io/UTF8StreamWriter;->a:Ljava/io/OutputStream;

    shr-int/lit8 v1, p1, 0x12

    add-int/lit16 v1, v1, 0xf0

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 88
    iget-object v0, p0, Lflipboard/io/UTF8StreamWriter;->a:Ljava/io/OutputStream;

    shr-int/lit8 v1, p1, 0xc

    and-int/lit8 v1, v1, 0x3f

    add-int/lit16 v1, v1, 0x80

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 89
    iget-object v0, p0, Lflipboard/io/UTF8StreamWriter;->a:Ljava/io/OutputStream;

    shr-int/lit8 v1, p1, 0x6

    and-int/lit8 v1, v1, 0x3f

    add-int/lit16 v1, v1, 0x80

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 90
    iget-object v0, p0, Lflipboard/io/UTF8StreamWriter;->a:Ljava/io/OutputStream;

    and-int/lit8 v1, p1, 0x3f

    add-int/lit16 v1, v1, 0x80

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0
.end method

.method public write(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lflipboard/io/UTF8StreamWriter;->write(Ljava/lang/String;II)V

    .line 64
    return-void
.end method

.method public write(Ljava/lang/String;II)V
    .locals 2

    .prologue
    .line 67
    :goto_0
    add-int/lit8 p3, p3, -0x1

    if-ltz p3, :cond_0

    .line 68
    add-int/lit8 v0, p2, 0x1

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, Lflipboard/io/UTF8StreamWriter;->write(I)V

    move p2, v0

    goto :goto_0

    .line 70
    :cond_0
    return-void
.end method

.method public write([C)V
    .locals 2

    .prologue
    .line 53
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lflipboard/io/UTF8StreamWriter;->write([CII)V

    .line 54
    return-void
.end method

.method public write([CII)V
    .locals 2

    .prologue
    .line 57
    :goto_0
    add-int/lit8 p3, p3, -0x1

    if-ltz p3, :cond_0

    .line 58
    add-int/lit8 v0, p2, 0x1

    aget-char v1, p1, p2

    invoke-virtual {p0, v1}, Lflipboard/io/UTF8StreamWriter;->write(I)V

    move p2, v0

    goto :goto_0

    .line 60
    :cond_0
    return-void
.end method
