.class public Lflipboard/io/NetworkManager;
.super Ljava/lang/Object;
.source "NetworkManager.java"


# static fields
.field public static final a:Lflipboard/util/Log;

.field public static final b:J

.field public static c:Lflipboard/io/NetworkManager;


# instance fields
.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/io/RequestLogEntry;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/io/RequestLogEntry;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lorg/apache/http/HttpRequest;",
            "Lflipboard/io/RequestLogEntry;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lorg/apache/http/client/HttpClient;

.field public h:Ljava/util/concurrent/atomic/AtomicInteger;

.field public i:J

.field final j:Landroid/telephony/TelephonyManager;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field private m:I

.field private n:I

.field private final o:Landroid/content/Context;

.field private final p:Lorg/apache/http/params/HttpParams;

.field private q:Ljava/util/concurrent/atomic/AtomicInteger;

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private final v:Landroid/net/ConnectivityManager;

.field private final w:Lflipboard/util/Observable$Proxy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observable$Proxy",
            "<",
            "Lflipboard/io/NetworkManager;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final x:Lflipboard/util/Observable$Proxy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observable$Proxy",
            "<",
            "Lflipboard/io/NetworkManager;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 70
    const-string v0, "network"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/io/NetworkManager;->a:Lflipboard/util/Log;

    .line 81
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_0

    const-wide/32 v0, 0x9c40

    :goto_0
    sput-wide v0, Lflipboard/io/NetworkManager;->b:J

    return-void

    :cond_0
    const-wide/16 v0, 0x4e20

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/16 v6, 0x4e20

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    const/16 v0, 0x32

    iput v0, p0, Lflipboard/io/NetworkManager;->m:I

    .line 95
    const/16 v0, 0x96

    iput v0, p0, Lflipboard/io/NetworkManager;->n:I

    .line 146
    sput-object p0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    .line 147
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ah:Z

    if-eqz v0, :cond_0

    .line 148
    new-instance v0, Ljava/util/ArrayList;

    iget v3, p0, Lflipboard/io/NetworkManager;->m:I

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/io/NetworkManager;->d:Ljava/util/List;

    .line 149
    new-instance v0, Ljava/util/ArrayList;

    iget v3, p0, Lflipboard/io/NetworkManager;->n:I

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/io/NetworkManager;->e:Ljava/util/List;

    .line 150
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lflipboard/io/NetworkManager;->f:Ljava/util/concurrent/ConcurrentHashMap;

    .line 152
    :cond_0
    iput-object p1, p0, Lflipboard/io/NetworkManager;->o:Landroid/content/Context;

    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "user.agent"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " (FlipboardProxy/1.1; +http://flipboard.com/browserproxy)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v0

    iput-object v0, p0, Lflipboard/io/NetworkManager;->g:Lorg/apache/http/client/HttpClient;

    .line 155
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    .line 156
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lflipboard/io/NetworkManager;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 157
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lflipboard/io/NetworkManager;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 158
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lflipboard/io/NetworkManager;->i:J

    .line 159
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lflipboard/io/NetworkManager;->v:Landroid/net/ConnectivityManager;

    .line 160
    iget-object v0, p0, Lflipboard/io/NetworkManager;->v:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v4

    .line 161
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lflipboard/io/NetworkManager;->r:Z

    .line 162
    iput-boolean v1, p0, Lflipboard/io/NetworkManager;->t:Z

    .line 163
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lflipboard/io/NetworkManager;->s:Z

    .line 164
    new-instance v0, Lflipboard/util/Observable$Proxy;

    invoke-direct {v0, p0}, Lflipboard/util/Observable$Proxy;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lflipboard/io/NetworkManager;->w:Lflipboard/util/Observable$Proxy;

    .line 165
    new-instance v0, Lflipboard/util/Observable$Proxy;

    invoke-direct {v0, p0}, Lflipboard/util/Observable$Proxy;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lflipboard/io/NetworkManager;->x:Lflipboard/util/Observable$Proxy;

    .line 166
    const-string v0, "location_override"

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/io/NetworkManager;->l:Ljava/lang/String;

    .line 167
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lflipboard/io/NetworkManager;->j:Landroid/telephony/TelephonyManager;

    .line 168
    const-string v0, "mobile_data"

    const-string v4, "enabled"

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/io/NetworkManager;->k:Ljava/lang/String;

    .line 170
    iget-object v0, p0, Lflipboard/io/NetworkManager;->l:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 171
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "location override: %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lflipboard/io/NetworkManager;->l:Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-virtual {v0, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 174
    :cond_1
    iget-object v0, p0, Lflipboard/io/NetworkManager;->g:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 175
    invoke-static {v0, v6}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 176
    invoke-static {v0, v6}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 177
    invoke-static {v0, v1}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 181
    invoke-interface {v0}, Lorg/apache/http/params/HttpParams;->copy()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    iput-object v0, p0, Lflipboard/io/NetworkManager;->p:Lorg/apache/http/params/HttpParams;

    .line 182
    iget-object v0, p0, Lflipboard/io/NetworkManager;->p:Lorg/apache/http/params/HttpParams;

    const-string v2, "http.protocol.allow-circular-redirects"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 184
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 185
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 186
    new-instance v1, Lflipboard/io/NetworkManager$1;

    invoke-direct {v1, p0}, Lflipboard/io/NetworkManager$1;-><init>(Lflipboard/io/NetworkManager;)V

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 208
    sget-object v0, Lflipboard/io/NetworkManager;->a:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_2

    .line 209
    const-string v0, "started"

    invoke-virtual {p0, v0}, Lflipboard/io/NetworkManager;->c(Ljava/lang/String;)V

    .line 211
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 161
    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 163
    goto :goto_1
.end method

.method static synthetic a(Lflipboard/io/NetworkManager;J)J
    .locals 1

    .prologue
    .line 68
    iput-wide p1, p0, Lflipboard/io/NetworkManager;->i:J

    return-wide p1
.end method

.method static synthetic a(Lflipboard/io/NetworkManager;)Landroid/net/ConnectivityManager;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lflipboard/io/NetworkManager;->v:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method static synthetic b(Lflipboard/io/NetworkManager;)Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->t:Z

    return v0
.end method

.method static synthetic c(Lflipboard/io/NetworkManager;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lflipboard/io/NetworkManager;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic d(Lflipboard/io/NetworkManager;)Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->r:Z

    return v0
.end method

.method static synthetic e(Lflipboard/io/NetworkManager;)J
    .locals 4

    .prologue
    .line 68
    iget-wide v0, p0, Lflipboard/io/NetworkManager;->i:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Lflipboard/io/NetworkManager;->i:J

    return-wide v0
.end method

.method static synthetic f(Lflipboard/io/NetworkManager;)J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lflipboard/io/NetworkManager;->i:J

    return-wide v0
.end method

.method public static f()V
    .locals 0

    .prologue
    .line 392
    return-void
.end method

.method static synthetic g(Lflipboard/io/NetworkManager;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lflipboard/io/NetworkManager;->j()V

    return-void
.end method

.method public static i()V
    .locals 0

    .prologue
    .line 408
    return-void
.end method

.method private declared-synchronized j()V
    .locals 3

    .prologue
    .line 369
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lflipboard/io/NetworkManager;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lflipboard/io/NetworkManager;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-nez v0, :cond_2

    .line 370
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/io/NetworkManager;->u:Z

    .line 371
    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->r:Z

    if-eqz v0, :cond_3

    .line 372
    sget-object v0, Lflipboard/io/NetworkManager;->a:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_0

    .line 373
    const-string v0, "network idle, notifying"

    invoke-virtual {p0, v0}, Lflipboard/io/NetworkManager;->c(Ljava/lang/String;)V

    .line 375
    :cond_0
    iget-object v0, p0, Lflipboard/io/NetworkManager;->x:Lflipboard/util/Observable$Proxy;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 381
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->t:Z

    if-eqz v0, :cond_2

    .line 382
    sget-object v0, Lflipboard/io/NetworkManager;->a:Lflipboard/util/Log;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385
    :cond_2
    monitor-exit p0

    return-void

    .line 377
    :cond_3
    :try_start_1
    sget-object v0, Lflipboard/io/NetworkManager;->a:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_1

    .line 378
    const-string v0, "network idle, not connected"

    invoke-virtual {p0, v0}, Lflipboard/io/NetworkManager;->c(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 369
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;
    .locals 8

    .prologue
    .line 587
    sget-wide v6, Lflipboard/io/NetworkManager;->b:J

    sget-object v0, Lflipboard/io/NetworkManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lflipboard/io/NetworkManager$NoNetworkException;

    invoke-direct {v0}, Lflipboard/io/NetworkManager$NoNetworkException;-><init>()V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "null response"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string v1, "null response entity"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    const-string v1, "Content-Encoding"

    invoke-interface {p2, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    const-string v2, "gzip"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v5, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v5, v0}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    :goto_0
    new-instance v1, Lflipboard/io/NetworkManager$NetworkInputStream;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v7}, Lflipboard/io/NetworkManager$NetworkInputStream;-><init>(Lflipboard/io/NetworkManager;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Ljava/io/InputStream;J)V

    return-object v1

    :cond_3
    const-string v2, "deflate"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v5, Ljava/util/zip/InflaterInputStream;

    invoke-direct {v5, v0}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown content encoding: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move-object v5, v0

    goto :goto_0
.end method

.method public final a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 483
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->a()Lorg/apache/http/protocol/HttpContext;

    move-result-object v0

    invoke-virtual {p0, p1, v0, v1, v1}, Lflipboard/io/NetworkManager;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;ZZ)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;ZZ)Lorg/apache/http/HttpResponse;
    .locals 14

    .prologue
    .line 490
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v2}, Lflipboard/app/FlipboardApplication;->l()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v2

    const-string v3, "https"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 491
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Date not set, can\'t do https request"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 493
    :cond_0
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v2, v2, Lflipboard/service/FlipboardManager;->ac:Z

    if-eqz v2, :cond_1

    if-nez p4, :cond_1

    .line 494
    new-instance v2, Ljava/io/IOException;

    const-string v3, "network is paused, or data use is restricted"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 496
    :cond_1
    sget-object v2, Lflipboard/io/NetworkManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 497
    const/4 v2, 0x0

    .line 498
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v3, v3, Lflipboard/service/FlipboardManager;->ah:Z

    if-eqz v3, :cond_10

    .line 500
    iget-object v2, p0, Lflipboard/io/NetworkManager;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget v3, p0, Lflipboard/io/NetworkManager;->m:I

    if-lt v2, v3, :cond_4

    .line 501
    iget-object v2, p0, Lflipboard/io/NetworkManager;->d:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/io/RequestLogEntry;

    .line 502
    invoke-virtual {v2}, Lflipboard/io/RequestLogEntry;->b()V

    .line 509
    :goto_0
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lflipboard/io/RequestLogEntry;->b:Ljava/lang/String;

    .line 510
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v2, Lflipboard/io/RequestLogEntry;->a:J

    .line 511
    invoke-virtual {v2}, Lflipboard/io/RequestLogEntry;->a()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 512
    iget-object v3, p0, Lflipboard/io/NetworkManager;->d:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 516
    :goto_1
    iget-object v3, p0, Lflipboard/io/NetworkManager;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, p1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v3, v2

    .line 518
    :goto_2
    iget-object v2, p0, Lflipboard/io/NetworkManager;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 520
    if-eqz p3, :cond_2

    .line 521
    :try_start_0
    iget-object v2, p0, Lflipboard/io/NetworkManager;->p:Lorg/apache/http/params/HttpParams;

    invoke-interface {p1, v2}, Lorg/apache/http/HttpRequest;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 523
    :cond_2
    const-string v2, "Accept-Encoding"

    const-string v4, "gzip, deflate"

    invoke-interface {p1, v2, v4}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    iget-object v2, p0, Lflipboard/io/NetworkManager;->l:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 525
    const-string v2, "X-Forwarded-For"

    iget-object v4, p0, Lflipboard/io/NetworkManager;->l:Ljava/lang/String;

    invoke-interface {p1, v2, v4}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    :cond_3
    invoke-virtual {p0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v2

    if-nez v2, :cond_7

    .line 529
    new-instance v2, Lflipboard/io/NetworkManager$NoNetworkException;

    invoke-direct {v2}, Lflipboard/io/NetworkManager$NoNetworkException;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 574
    :catch_0
    move-exception v2

    :try_start_1
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "invalid url: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 578
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lflipboard/io/NetworkManager;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    throw v2

    .line 503
    :cond_4
    iget-object v2, p0, Lflipboard/io/NetworkManager;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget v3, p0, Lflipboard/io/NetworkManager;->n:I

    if-lt v2, v3, :cond_5

    .line 504
    iget-object v2, p0, Lflipboard/io/NetworkManager;->e:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/io/RequestLogEntry;

    .line 505
    invoke-virtual {v2}, Lflipboard/io/RequestLogEntry;->b()V

    goto/16 :goto_0

    .line 507
    :cond_5
    new-instance v2, Lflipboard/io/RequestLogEntry;

    invoke-direct {v2}, Lflipboard/io/RequestLogEntry;-><init>()V

    goto/16 :goto_0

    .line 514
    :cond_6
    iget-object v3, p0, Lflipboard/io/NetworkManager;->e:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 531
    :cond_7
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 532
    iget-object v4, p0, Lflipboard/io/NetworkManager;->g:Lorg/apache/http/client/HttpClient;

    move-object v0, p1

    check-cast v0, Lorg/apache/http/client/methods/HttpUriRequest;

    move-object v2, v0

    move-object/from16 v0, p2

    invoke-interface {v4, v2, v0}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v8

    .line 533
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v2, v2, Lflipboard/service/FlipboardManager;->ah:Z

    if-eqz v2, :cond_b

    .line 534
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v2

    .line 535
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Lflipboard/io/RequestLogEntry;->a()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 536
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v4, v3, Lflipboard/io/RequestLogEntry;->k:Ljava/io/ByteArrayOutputStream;

    .line 538
    :cond_8
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 539
    const-string v5, "FL-JOB-ID"

    invoke-interface {v8, v5}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v5

    .line 540
    array-length v9, v5

    if-eqz v9, :cond_b

    .line 541
    if-eqz v3, :cond_9

    .line 542
    const/4 v9, 0x0

    aget-object v9, v5, v9

    invoke-interface {v9}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v3, Lflipboard/io/RequestLogEntry;->f:Ljava/lang/String;

    .line 544
    :cond_9
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    const-string v9, "\\/\\d"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 545
    array-length v9, v4

    if-eqz v9, :cond_a

    .line 547
    const/4 v2, 0x0

    aget-object v2, v4, v2

    .line 549
    :cond_a
    sget-object v4, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v9, 0x0

    const/4 v10, 0x0

    aget-object v5, v5, v10

    invoke-interface {v5}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    const/4 v5, 0x1

    aput-object v2, v4, v5

    .line 552
    :cond_b
    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v9

    .line 553
    const-wide/16 v4, 0x0

    .line 554
    const/16 v2, 0xc8

    if-ne v9, v2, :cond_c

    instance-of v2, p1, Lorg/apache/http/client/methods/HttpPost;

    if-eqz v2, :cond_c

    .line 555
    move-object v0, p1

    check-cast v0, Lorg/apache/http/client/methods/HttpPost;

    move-object v2, v0

    invoke-virtual {v2}, Lorg/apache/http/client/methods/HttpPost;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 556
    if-eqz v2, :cond_c

    .line 557
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v4

    .line 558
    iget-wide v10, p0, Lflipboard/io/NetworkManager;->i:J

    add-long/2addr v10, v4

    iput-wide v10, p0, Lflipboard/io/NetworkManager;->i:J

    .line 559
    sget-object v10, Lflipboard/io/NetworkManager;->a:Lflipboard/util/Log;

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v10, v11

    .line 562
    :cond_c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long v6, v10, v6

    .line 563
    const-wide/16 v10, 0x7d0

    cmp-long v2, v6, v10

    if-lez v2, :cond_d

    .line 564
    sget-object v2, Lflipboard/io/NetworkManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v2, v10

    const/4 v10, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v2, v10

    const/4 v10, 0x2

    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v11

    invoke-interface {v11}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v2, v10

    .line 566
    :cond_d
    if-eqz v3, :cond_e

    .line 567
    iput v9, v3, Lflipboard/io/RequestLogEntry;->d:I

    .line 568
    iput-wide v6, v3, Lflipboard/io/RequestLogEntry;->c:J

    .line 569
    iput-wide v4, v3, Lflipboard/io/RequestLogEntry;->e:J

    .line 570
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/RequestLine;->getMethod()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lflipboard/io/RequestLogEntry;->g:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 578
    :cond_e
    iget-object v2, p0, Lflipboard/io/NetworkManager;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    return-object v8

    .line 575
    :catch_1
    move-exception v2

    .line 576
    :try_start_3
    iget-boolean v3, p0, Lflipboard/io/NetworkManager;->r:Z

    if-eqz v3, :cond_f

    :goto_3
    throw v2

    :cond_f
    new-instance v2, Lflipboard/io/NetworkManager$NoNetworkException;

    invoke-direct {v2}, Lflipboard/io/NetworkManager$NoNetworkException;-><init>()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    :cond_10
    move-object v3, v2

    goto/16 :goto_2
.end method

.method public final a(Lflipboard/util/Observer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/io/NetworkManager;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 437
    iget-object v0, p0, Lflipboard/io/NetworkManager;->w:Lflipboard/util/Observable$Proxy;

    invoke-virtual {v0, p1}, Lflipboard/util/Observable$Proxy;->b(Lflipboard/util/Observer;)V

    .line 438
    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 312
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lflipboard/io/NetworkManager;->k:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    :goto_0
    monitor-exit p0

    return-void

    .line 315
    :cond_0
    :try_start_1
    iput-object p1, p0, Lflipboard/io/NetworkManager;->k:Ljava/lang/String;

    .line 318
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    .line 319
    const-string v1, "enabled"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 320
    const/4 p1, 0x0

    .line 322
    :cond_1
    if-nez p1, :cond_2

    .line 323
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "mobile_data"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 327
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mobile "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/io/NetworkManager;->c(Ljava/lang/String;)V

    .line 330
    iget-object v0, p0, Lflipboard/io/NetworkManager;->w:Lflipboard/util/Observable$Proxy;

    invoke-virtual {p0}, Lflipboard/io/NetworkManager;->d()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0}, Lflipboard/io/NetworkManager;->b()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 312
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 325
    :cond_2
    :try_start_2
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "mobile_data"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method final declared-synchronized a(ZZZ)V
    .locals 3

    .prologue
    .line 416
    monitor-enter p0

    :try_start_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ac:Z

    or-int/2addr v0, p2

    .line 417
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->g()V

    .line 418
    iget-boolean v1, p0, Lflipboard/io/NetworkManager;->r:Z

    if-ne v1, p1, :cond_0

    iget-boolean v1, p0, Lflipboard/io/NetworkManager;->t:Z

    if-ne v1, v0, :cond_0

    iget-boolean v1, p0, Lflipboard/io/NetworkManager;->s:Z

    if-eq v1, p3, :cond_3

    .line 419
    :cond_0
    iput-boolean p1, p0, Lflipboard/io/NetworkManager;->r:Z

    .line 420
    iput-boolean v0, p0, Lflipboard/io/NetworkManager;->t:Z

    .line 421
    iput-boolean p3, p0, Lflipboard/io/NetworkManager;->s:Z

    .line 422
    if-eqz p1, :cond_1

    iget-object v1, p0, Lflipboard/io/NetworkManager;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    .line 423
    invoke-direct {p0}, Lflipboard/io/NetworkManager;->j()V

    .line 425
    :cond_1
    sget-object v0, Lflipboard/io/NetworkManager;->a:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_2

    .line 426
    const-string v0, "network status"

    invoke-virtual {p0, v0}, Lflipboard/io/NetworkManager;->c(Ljava/lang/String;)V

    .line 428
    :cond_2
    iget-object v0, p0, Lflipboard/io/NetworkManager;->w:Lflipboard/util/Observable$Proxy;

    invoke-virtual {p0}, Lflipboard/io/NetworkManager;->d()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Observable$Proxy;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 430
    :cond_3
    monitor-exit p0

    return-void

    .line 416
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 240
    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->r:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->s:Z

    if-nez v0, :cond_0

    const-string v0, "disabled"

    iget-object v1, p0, Lflipboard/io/NetworkManager;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lflipboard/util/Observer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/io/NetworkManager;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 445
    iget-object v0, p0, Lflipboard/io/NetworkManager;->w:Lflipboard/util/Observable$Proxy;

    invoke-virtual {v0, p1}, Lflipboard/util/Observable$Proxy;->c(Lflipboard/util/Observer;)V

    .line 446
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 462
    const-string v0, "https://beacon.flipboard.com/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467
    :goto_0
    return-void

    .line 465
    :cond_0
    sget-object v0, Lflipboard/io/NetworkManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 466
    iget-wide v0, p0, Lflipboard/io/NetworkManager;->i:J

    const-wide/16 v2, 0x2800

    add-long/2addr v0, v2

    iput-wide v0, p0, Lflipboard/io/NetworkManager;->i:J

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 256
    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->r:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->s:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lflipboard/util/Observer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/io/NetworkManager;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 453
    iget-object v0, p0, Lflipboard/io/NetworkManager;->x:Lflipboard/util/Observable$Proxy;

    invoke-virtual {v0, p1}, Lflipboard/util/Observable$Proxy;->b(Lflipboard/util/Observer;)V

    .line 454
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 790
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 791
    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->r:Z

    if-eqz v0, :cond_9

    const-string v0, "connected"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 792
    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->r:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->s:Z

    if-eqz v0, :cond_0

    .line 793
    const-string v0, ", wifi"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 795
    :cond_0
    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->r:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->s:Z

    if-nez v0, :cond_1

    .line 796
    const-string v0, ", mobile"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 797
    iget-object v0, p0, Lflipboard/io/NetworkManager;->k:Ljava/lang/String;

    const-string v2, "enabled"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 798
    const-string v0, "="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 799
    iget-object v0, p0, Lflipboard/io/NetworkManager;->k:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 802
    :cond_1
    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->r:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->u:Z

    if-eqz v0, :cond_2

    .line 803
    const-string v0, ", idle"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 805
    :cond_2
    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->t:Z

    if-eqz v0, :cond_3

    .line 806
    const-string v0, ", paused"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 808
    :cond_3
    iget-object v0, p0, Lflipboard/io/NetworkManager;->o:Landroid/content/Context;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 809
    const-string v0, ", screenlocked"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 811
    :cond_4
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->aA:Z

    if-nez v0, :cond_5

    .line 812
    const-string v0, ", nouser"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 814
    :cond_5
    iget-object v0, p0, Lflipboard/io/NetworkManager;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_6

    .line 815
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ", requests="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lflipboard/io/NetworkManager;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 817
    :cond_6
    iget-object v0, p0, Lflipboard/io/NetworkManager;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_7

    .line 818
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ", connections="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lflipboard/io/NetworkManager;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 820
    :cond_7
    iget-wide v2, p0, Lflipboard/io/NetworkManager;->i:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_8

    .line 821
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ", total="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lflipboard/io/NetworkManager;->i:J

    invoke-static {v2, v3}, Lflipboard/util/JavaUtil;->b(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 824
    :cond_8
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    const/4 v2, 0x1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    .line 825
    return-void

    .line 791
    :cond_9
    const-string v0, "disconnected"

    goto/16 :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 274
    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->t:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/io/NetworkManager;->o:Landroid/content/Context;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276
    const/4 v0, 0x1

    .line 278
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->t:Z

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 286
    invoke-virtual {p0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflipboard/io/NetworkManager;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 295
    invoke-virtual {p0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflipboard/io/NetworkManager;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ondemand"

    iget-object v1, p0, Lflipboard/io/NetworkManager;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 396
    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->r:Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lflipboard/io/NetworkManager;->s:Z

    invoke-virtual {p0, v0, v1, v2}, Lflipboard/io/NetworkManager;->a(ZZZ)V

    .line 397
    return-void
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 400
    iget-boolean v0, p0, Lflipboard/io/NetworkManager;->r:Z

    const/4 v1, 0x1

    iget-boolean v2, p0, Lflipboard/io/NetworkManager;->s:Z

    invoke-virtual {p0, v0, v1, v2}, Lflipboard/io/NetworkManager;->a(ZZZ)V

    .line 401
    return-void
.end method
