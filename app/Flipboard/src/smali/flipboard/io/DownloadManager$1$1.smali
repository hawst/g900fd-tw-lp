.class Lflipboard/io/DownloadManager$1$1;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DownloadManager.java"


# instance fields
.field final synthetic a:Lflipboard/io/DownloadManager$1;


# direct methods
.method constructor <init>(Lflipboard/io/DownloadManager$1;Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 114
    iput-object p1, p0, Lflipboard/io/DownloadManager$1$1;->a:Lflipboard/io/DownloadManager$1;

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p2, p3, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 117
    const-string v0, "CREATE TABLE download(url TEXT, downloaded INTEGER, touched INTEGER, expires INTEGER, contentLength INTEGER, contentType TEXT, charset Text, location TEXT, width INTEGER, height INTEGER, PRIMARY KEY(url))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 118
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 121
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 122
    if-ne p2, v3, :cond_0

    .line 123
    const-string v0, "ALTER TABLE download ADD COLUMN width INTEGER"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 124
    const-string v0, "ALTER TABLE download ADD COLUMN height INTEGER"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 126
    :cond_0
    return-void
.end method
