.class Lflipboard/io/DownloadManager$4;
.super Ljava/lang/Thread;
.source "DownloadManager.java"


# instance fields
.field final synthetic a:Z

.field final synthetic b:Z

.field final synthetic c:Lflipboard/io/DownloadManager;


# direct methods
.method constructor <init>(Lflipboard/io/DownloadManager;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 880
    iput-object p1, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    iput-boolean p3, p0, Lflipboard/io/DownloadManager$4;->a:Z

    iput-boolean p4, p0, Lflipboard/io/DownloadManager$4;->b:Z

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 883
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    move v0, v2

    move v1, v5

    .line 888
    :goto_0
    :try_start_0
    iget-object v3, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    iget-boolean v3, v3, Lflipboard/io/DownloadManager;->k:Z

    if-nez v3, :cond_7

    .line 889
    iget-boolean v3, p0, Lflipboard/io/DownloadManager$4;->a:Z

    if-eqz v3, :cond_2

    sget-object v3, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v3}, Lflipboard/io/NetworkManager;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    move v4, v2

    .line 893
    :goto_1
    if-eqz v0, :cond_9

    .line 896
    iget-object v0, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    iget-wide v0, v0, Lflipboard/io/DownloadManager;->o:J

    iget-object v3, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    iget-wide v6, v3, Lflipboard/io/DownloadManager;->j:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_0

    .line 897
    iget-object v0, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    invoke-static {v0}, Lflipboard/io/DownloadManager;->a(Lflipboard/io/DownloadManager;)V

    .line 899
    :cond_0
    iget-object v0, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lflipboard/io/DownloadManager;->m:Z

    move v1, v2

    move v3, v2

    .line 903
    :goto_2
    iget-object v0, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    iget-object v6, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 904
    :try_start_1
    iget-object v0, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    iget-object v0, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    iget-object v0, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/Download;

    iget v0, v0, Lflipboard/io/Download;->e:I

    if-lt v0, v4, :cond_1

    iget-boolean v0, p0, Lflipboard/io/DownloadManager$4;->b:Z

    if-eqz v0, :cond_6

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 905
    :cond_1
    if-eqz v3, :cond_3

    iget-object v0, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    iget-boolean v0, v0, Lflipboard/io/DownloadManager;->m:Z

    if-nez v0, :cond_3

    .line 907
    iget-object v0, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/io/DownloadManager;->m:Z

    .line 908
    monitor-exit v6

    move v0, v5

    move v1, v3

    goto :goto_0

    .line 889
    :cond_2
    const/16 v3, 0x19

    move v4, v3

    goto :goto_1

    .line 910
    :cond_3
    iget-object v0, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    iget v7, v0, Lflipboard/io/DownloadManager;->p:I

    add-int/lit8 v7, v7, 0x1

    iput v7, v0, Lflipboard/io/DownloadManager;->p:I

    .line 911
    iget-object v0, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    iget-object v0, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    const-wide/16 v8, 0x3e8

    invoke-virtual {v0, v8, v9}, Ljava/lang/Object;->wait(J)V

    .line 912
    iget-object v0, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    iget-object v0, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    iget-object v0, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/Download;

    iget v0, v0, Lflipboard/io/Download;->e:I

    if-lt v0, v4, :cond_4

    iget-boolean v0, p0, Lflipboard/io/DownloadManager$4;->b:Z

    if-eqz v0, :cond_5

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 913
    :cond_4
    sget-object v0, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v0, v4

    .line 914
    iget-object v0, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    iget-object v0, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    .line 916
    :cond_5
    iget-object v0, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    iget v4, v0, Lflipboard/io/DownloadManager;->p:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v0, Lflipboard/io/DownloadManager;->p:I

    .line 917
    monitor-exit v6

    move v0, v1

    move v1, v3

    goto/16 :goto_0

    .line 919
    :cond_6
    iget-object v0, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    iget-object v0, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/Download;

    .line 920
    iget-object v4, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    iget-object v4, v4, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-interface {v4, v0}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z

    .line 921
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 923
    :try_start_2
    monitor-enter v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 924
    :try_start_3
    iget-object v4, v0, Lflipboard/io/Download;->d:Lflipboard/io/Download$Status;

    invoke-virtual {v4}, Lflipboard/io/Download$Status;->ordinal()I

    move-result v4

    sget-object v6, Lflipboard/io/Download$Status;->h:Lflipboard/io/Download$Status;

    invoke-virtual {v6}, Lflipboard/io/Download$Status;->ordinal()I

    move-result v6

    if-lt v4, v6, :cond_8

    .line 925
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move v0, v1

    move v1, v3

    goto/16 :goto_0

    .line 921
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v6

    throw v0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 933
    :catch_0
    move-exception v0

    .line 934
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    .line 936
    :cond_7
    return-void

    .line 927
    :cond_8
    :try_start_5
    monitor-exit v0

    .line 930
    iget-object v3, p0, Lflipboard/io/DownloadManager$4;->c:Lflipboard/io/DownloadManager;

    invoke-static {v3, v0}, Lflipboard/io/DownloadManager;->a(Lflipboard/io/DownloadManager;Lflipboard/io/Download;)V

    move v0, v1

    move v1, v5

    .line 932
    goto/16 :goto_0

    .line 927
    :catchall_1
    move-exception v1

    monitor-exit v0

    throw v1
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0

    :cond_9
    move v3, v1

    move v1, v0

    goto/16 :goto_2
.end method
