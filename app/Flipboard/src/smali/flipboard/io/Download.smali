.class public Lflipboard/io/Download;
.super Lflipboard/util/Observable;
.source "Download.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/util/Observable",
        "<",
        "Lflipboard/io/Download;",
        "Lflipboard/io/Download$Status;",
        "Lflipboard/io/Download$Data;",
        ">;",
        "Ljava/lang/Comparable",
        "<",
        "Lflipboard/io/Download;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic j:Z


# instance fields
.field final a:Lflipboard/io/DownloadManager;

.field public final b:Ljava/lang/String;

.field final c:J

.field d:Lflipboard/io/Download$Status;

.field e:I

.field public f:Lflipboard/io/Download$Data;

.field g:I

.field h:Z

.field public i:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lflipboard/io/Download;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lflipboard/io/Download;->j:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lflipboard/io/DownloadManager;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 175
    invoke-direct {p0}, Lflipboard/util/Observable;-><init>()V

    .line 176
    sget-boolean v0, Lflipboard/io/Download;->j:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 177
    :cond_0
    iput-object p1, p0, Lflipboard/io/Download;->a:Lflipboard/io/DownloadManager;

    .line 178
    iput-object p2, p0, Lflipboard/io/Download;->b:Ljava/lang/String;

    .line 179
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/io/Download;->e:I

    .line 180
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/io/Download;->c:J

    .line 181
    sget-object v0, Lflipboard/io/Download$Status;->a:Lflipboard/io/Download$Status;

    iput-object v0, p0, Lflipboard/io/Download;->d:Lflipboard/io/Download$Status;

    .line 182
    return-void
.end method

.method static synthetic a(Lflipboard/io/Download;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lflipboard/io/Download;->c()V

    return-void
.end method

.method private final c()V
    .locals 7

    .prologue
    const/16 v6, 0x19

    const/4 v1, 0x0

    .line 208
    .line 209
    monitor-enter p0

    .line 210
    :try_start_0
    iget-object v0, p0, Lflipboard/io/Download;->I:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 211
    iget-object v3, p0, Lflipboard/io/Download;->I:[Ljava/lang/Object;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 212
    check-cast v0, Lflipboard/io/Download$Observer;

    .line 213
    iget v5, v0, Lflipboard/io/Download$Observer;->e:I

    if-ge v1, v5, :cond_7

    .line 214
    iget v0, v0, Lflipboard/io/Download$Observer;->e:I

    .line 211
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 218
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    iget-object v0, p0, Lflipboard/io/Download;->a:Lflipboard/io/DownloadManager;

    iget-object v2, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    monitor-enter v2

    :try_start_1
    iget-object v3, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-interface {v3, p0}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iput v1, p0, Lflipboard/io/Download;->e:I

    if-gt v1, v6, :cond_1

    sget-object v3, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v3}, Lflipboard/io/NetworkManager;->b()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_1
    iget-object v3, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-interface {v3, p0}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    iget-object v3, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    :cond_2
    :goto_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/16 v2, 0x64

    if-lt v1, v2, :cond_3

    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, Lflipboard/io/DownloadManager;->g:Ljava/util/Map;

    monitor-enter v1

    :try_start_2
    sget-object v2, Lflipboard/io/DownloadManager$5;->a:[I

    iget-object v3, p0, Lflipboard/io/Download;->d:Lflipboard/io/Download$Status;

    invoke-virtual {v3}, Lflipboard/io/Download$Status;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_3
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 220
    :cond_3
    return-void

    .line 218
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 219
    :cond_4
    :try_start_3
    sget-object v3, Lflipboard/io/Download$Status;->e:Lflipboard/io/Download$Status;

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_5
    :try_start_4
    iput v1, p0, Lflipboard/io/Download;->e:I

    iget-boolean v3, v0, Lflipboard/io/DownloadManager;->l:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lflipboard/io/Download;->d:Lflipboard/io/Download$Status;

    sget-object v4, Lflipboard/io/Download$Status;->e:Lflipboard/io/Download$Status;

    if-ne v3, v4, :cond_2

    if-gt v1, v6, :cond_6

    sget-object v3, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v3}, Lflipboard/io/NetworkManager;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_6
    iget-object v3, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-interface {v3, p0}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    iget-object v3, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    sget-object v3, Lflipboard/io/Download$Status;->b:Lflipboard/io/Download$Status;

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    :pswitch_0
    :try_start_5
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    sget-object v2, Lflipboard/io/Download$Status;->b:Lflipboard/io/Download$Status;

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V

    iget-object v2, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    monitor-enter v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :try_start_6
    iget-object v3, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-interface {v3, p0}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_3

    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit v2

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    :catchall_3
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_7
    move v0, v1

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final declared-synchronized a(Lflipboard/io/Download$Observer;)V
    .locals 2

    .prologue
    .line 325
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lflipboard/util/Observable;->b(Lflipboard/util/Observer;)V

    .line 326
    iput-object p0, p1, Lflipboard/io/Download$Observer;->f:Lflipboard/io/Download;

    .line 327
    invoke-direct {p0}, Lflipboard/io/Download;->c()V

    .line 328
    iget-object v0, p0, Lflipboard/io/Download;->d:Lflipboard/io/Download$Status;

    iget-object v1, p0, Lflipboard/io/Download;->f:Lflipboard/io/Download$Data;

    invoke-virtual {p1, p0, v0, v1}, Lflipboard/io/Download$Observer;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 329
    monitor-exit p0

    return-void

    .line 325
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a(Lflipboard/io/Download$Status;Lflipboard/io/Download$Data;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 224
    .line 225
    monitor-enter p0

    .line 226
    :try_start_0
    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/io/Download;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    .line 227
    iget-object v2, p0, Lflipboard/io/Download;->d:Lflipboard/io/Download$Status;

    if-ne v2, p1, :cond_0

    iget-object v2, p0, Lflipboard/io/Download;->f:Lflipboard/io/Download$Data;

    if-eq v2, p2, :cond_1

    .line 228
    :cond_0
    iput-object p1, p0, Lflipboard/io/Download;->d:Lflipboard/io/Download$Status;

    .line 229
    iput-object p2, p0, Lflipboard/io/Download;->f:Lflipboard/io/Download$Data;

    move v0, v1

    .line 232
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 233
    if-eqz v0, :cond_2

    .line 234
    invoke-virtual {p0, p1, p2}, Lflipboard/io/Download;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 236
    :cond_2
    return-void

    .line 232
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lflipboard/io/Download;->d:Lflipboard/io/Download$Status;

    sget-object v1, Lflipboard/io/Download$Status;->h:Lflipboard/io/Download$Status;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 363
    iget-object v0, p0, Lflipboard/io/Download;->a:Lflipboard/io/DownloadManager;

    iget-object v1, v0, Lflipboard/io/DownloadManager;->g:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget v2, p0, Lflipboard/io/Download;->g:I

    if-gtz v2, :cond_1

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "refcount error: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    sget-object v0, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    iget-boolean v0, v0, Lflipboard/util/Log;->f:Z

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    invoke-virtual {v0}, Lflipboard/io/DownloadManager;->d()V

    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager;->c()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 364
    :goto_0
    iput-boolean v7, p0, Lflipboard/io/Download;->h:Z

    .line 365
    return-void

    .line 363
    :cond_1
    :try_start_1
    iget v2, p0, Lflipboard/io/Download;->g:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lflipboard/io/Download;->g:I

    if-nez v2, :cond_3

    iget-object v2, v0, Lflipboard/io/DownloadManager;->g:Ljava/util/Map;

    iget-object v3, p0, Lflipboard/io/Download;->b:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lflipboard/io/Download;->E()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const-string v3, "%d observers on close: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    array-length v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lflipboard/io/Download;->b:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    iget-object v2, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, v0, Lflipboard/io/DownloadManager;->h:Ljava/util/SortedSet;

    invoke-interface {v0, p0}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_3
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v2

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public final declared-synchronized b(Lflipboard/io/Download$Observer;)V
    .locals 1

    .prologue
    .line 336
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lflipboard/util/Observable;->c(Lflipboard/util/Observer;)V

    .line 337
    const/4 v0, 0x0

    iput-object v0, p1, Lflipboard/io/Download$Observer;->f:Lflipboard/io/Download;

    .line 338
    invoke-direct {p0}, Lflipboard/io/Download;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    monitor-exit p0

    return-void

    .line 336
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, -0x1

    .line 22
    check-cast p1, Lflipboard/io/Download;

    iget v2, p0, Lflipboard/io/Download;->e:I

    iget v3, p1, Lflipboard/io/Download;->e:I

    if-eq v2, v3, :cond_2

    iget v2, p0, Lflipboard/io/Download;->e:I

    iget v3, p1, Lflipboard/io/Download;->e:I

    if-ge v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-wide v2, p0, Lflipboard/io/Download;->c:J

    iget-wide v4, p1, Lflipboard/io/Download;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lflipboard/io/Download;->c:J

    iget-wide v4, p1, Lflipboard/io/Download;->c:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lflipboard/io/Download;->b:Ljava/lang/String;

    iget-object v1, p1, Lflipboard/io/Download;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public finalize()V
    .locals 5

    .prologue
    .line 372
    iget-boolean v0, p0, Lflipboard/io/Download;->h:Z

    if-nez v0, :cond_0

    .line 373
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "download not closed: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/io/Download;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 375
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 382
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "download["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/io/Download;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflipboard/io/Download;->d:Lflipboard/io/Download$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
