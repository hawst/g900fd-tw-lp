.class Lflipboard/io/BitmapManager$BitmapHandle;
.super Lflipboard/io/BitmapManager$Handle;
.source "BitmapManager.java"


# instance fields
.field a:Z

.field final synthetic b:Lflipboard/io/BitmapManager;


# direct methods
.method constructor <init>(Lflipboard/io/BitmapManager;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 1116
    iput-object p1, p0, Lflipboard/io/BitmapManager$BitmapHandle;->b:Lflipboard/io/BitmapManager;

    invoke-direct {p0, p1}, Lflipboard/io/BitmapManager$Handle;-><init>(Lflipboard/io/BitmapManager;)V

    .line 1117
    iput-object p2, p0, Lflipboard/io/BitmapManager$BitmapHandle;->g:Landroid/graphics/Bitmap;

    .line 1118
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lflipboard/io/BitmapManager$BitmapHandle;->h:I

    .line 1119
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lflipboard/io/BitmapManager$BitmapHandle;->i:I

    .line 1120
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/graphics/Rect;F)Lflipboard/io/BitmapManager$Handle;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1145
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lflipboard/io/BitmapManager$BitmapHandle;->a:Z

    if-eqz v1, :cond_0

    .line 1146
    sget-object v1, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const-string v2, "scaling already scaled bitmap: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1162
    :goto_0
    monitor-exit p0

    return-object v0

    .line 1149
    :cond_0
    :try_start_1
    iget-object v1, p0, Lflipboard/io/BitmapManager$BitmapHandle;->g:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    .line 1150
    sget-object v1, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const-string v2, "Trying to get a scaled version of null bitmap"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1145
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1156
    :cond_1
    :try_start_2
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 1157
    invoke-virtual {v5, p2, p2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 1158
    iget-object v0, p0, Lflipboard/io/BitmapManager$BitmapHandle;->g:Landroid/graphics/Bitmap;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lflipboard/io/BitmapManager$BitmapHandle;->g:Landroid/graphics/Bitmap;

    .line 1159
    iget-object v0, p0, Lflipboard/io/BitmapManager$BitmapHandle;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lflipboard/io/BitmapManager$BitmapHandle;->h:I

    .line 1160
    iget-object v0, p0, Lflipboard/io/BitmapManager$BitmapHandle;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lflipboard/io/BitmapManager$BitmapHandle;->i:I

    .line 1161
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/io/BitmapManager$BitmapHandle;->a:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, p0

    .line 1162
    goto :goto_0
.end method

.method public final a()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1124
    iget-object v0, p0, Lflipboard/io/BitmapManager$BitmapHandle;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 1125
    const-string v0, "null"

    .line 1127
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "bm-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/io/BitmapManager$BitmapHandle;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lflipboard/io/BitmapManager$BitmapHandle;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lflipboard/io/BitmapManager$BitmapHandle;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1132
    iget-object v0, p0, Lflipboard/io/BitmapManager$BitmapHandle;->g:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method final c()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1136
    iget-object v0, p0, Lflipboard/io/BitmapManager$BitmapHandle;->g:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1172
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/io/BitmapManager$BitmapHandle;->g:Landroid/graphics/Bitmap;

    .line 1173
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1167
    const/4 v0, 0x0

    return v0
.end method
