.class Lflipboard/io/DownloadManager$1;
.super Ljava/lang/Thread;
.source "DownloadManager.java"


# instance fields
.field final synthetic a:Lflipboard/io/DownloadManager;


# direct methods
.method constructor <init>(Lflipboard/io/DownloadManager;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 22

    .prologue
    .line 114
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    new-instance v3, Lflipboard/io/DownloadManager$1$1;

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-object v4, v4, Lflipboard/io/DownloadManager;->c:Landroid/content/Context;

    const-string v5, "downloads-v1.db"

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v5}, Lflipboard/io/DownloadManager$1$1;-><init>(Lflipboard/io/DownloadManager$1;Landroid/content/Context;Ljava/lang/String;)V

    .line 127
    invoke-virtual {v3}, Lflipboard/io/DownloadManager$1$1;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    iput-object v3, v2, Lflipboard/io/DownloadManager;->f:Landroid/database/sqlite/SQLiteDatabase;

    .line 130
    const/4 v2, 0x0

    .line 134
    const-string v3, "SELECT url,downloaded,expires,contentLength,contentType,charset,location,width,height FROM download ORDER BY touched"

    .line 135
    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-object v4, v4, Lflipboard/io/DownloadManager;->f:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 138
    :try_start_0
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v3

    if-eqz v3, :cond_3

    .line 141
    :try_start_1
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-object v4, v4, Lflipboard/io/DownloadManager;->e:Ljava/io/File;

    const/4 v5, 0x6

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 142
    sget-object v4, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v3}, Ljava/io/File;->canRead()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    .line 143
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 144
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-object v0, v3, Lflipboard/io/DownloadManager;->i:Ljava/util/Map;

    move-object/from16 v18, v0

    monitor-enter v18
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 145
    :goto_0
    :try_start_2
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 146
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 147
    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    .line 148
    const/4 v3, 0x2

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    .line 149
    const/4 v3, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 150
    const/4 v3, 0x4

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 151
    const/4 v3, 0x5

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 152
    const/4 v3, 0x6

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 153
    const/4 v4, 0x7

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 154
    const/16 v4, 0x8

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 156
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-object v4, v4, Lflipboard/io/DownloadManager;->e:Ljava/io/File;

    move-object/from16 v0, v16

    invoke-direct {v0, v4, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 157
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-wide v0, v3, Lflipboard/io/DownloadManager;->o:J

    move-wide/from16 v20, v0

    add-long v20, v20, v10

    move-wide/from16 v0, v20

    iput-wide v0, v3, Lflipboard/io/DownloadManager;->o:J

    .line 158
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget v4, v3, Lflipboard/io/DownloadManager;->n:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Lflipboard/io/DownloadManager;->n:I

    .line 160
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-object v0, v3, Lflipboard/io/DownloadManager;->i:Ljava/util/Map;

    move-object/from16 v19, v0

    new-instance v3, Lflipboard/io/DownloadManager$FileData;

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    invoke-direct/range {v3 .. v16}, Lflipboard/io/DownloadManager$FileData;-><init>(Lflipboard/io/DownloadManager;Ljava/lang/String;JJJLjava/lang/String;Ljava/lang/String;IILjava/io/File;)V

    move-object/from16 v0, v19

    invoke-interface {v0, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 162
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v18

    throw v2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 167
    :catch_0
    move-exception v2

    .line 170
    :try_start_4
    sget-object v3, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 171
    const/4 v2, 0x1

    .line 178
    :goto_1
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 183
    if-eqz v2, :cond_0

    .line 184
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "new cache directory: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-object v6, v6, Lflipboard/io/DownloadManager;->e:Ljava/io/File;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 187
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-object v2, v2, Lflipboard/io/DownloadManager;->f:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "download"

    const-string v4, ""

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 188
    sget-object v3, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v4

    .line 189
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-object v2, v2, Lflipboard/io/DownloadManager;->e:Ljava/io/File;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-object v4, v4, Lflipboard/io/DownloadManager;->d:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".tmp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 190
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-object v2, v2, Lflipboard/io/DownloadManager;->e:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    .line 193
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 194
    invoke-static {v2}, Lflipboard/util/JavaUtil;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 195
    const-string v4, "INSERT INTO download (url,touched,location) VALUES (?,?,?)"

    .line 196
    move-object/from16 v0, p0

    iget-object v5, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-object v5, v5, Lflipboard/io/DownloadManager;->f:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/4 v2, 0x1

    const-string v7, "0"

    aput-object v7, v6, v2

    const/4 v2, 0x2

    aput-object v3, v6, v2

    invoke-virtual {v5, v4, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 198
    :try_start_5
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 200
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 203
    :try_start_6
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-object v4, v4, Lflipboard/io/DownloadManager;->e:Ljava/io/File;

    invoke-direct {v2, v4, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 204
    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-object v5, v5, Lflipboard/io/DownloadManager;->e:Ljava/io/File;

    aput-object v5, v2, v4

    const/4 v4, 0x1

    aput-object v3, v2, v4
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    .line 212
    :cond_0
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    invoke-virtual {v2}, Lflipboard/io/DownloadManager;->a()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 214
    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget v4, v4, Lflipboard/io/DownloadManager;->n:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-wide v4, v4, Lflipboard/io/DownloadManager;->o:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-object v4, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-wide v4, v4, Lflipboard/io/DownloadManager;->j:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 218
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    new-instance v3, Lflipboard/io/DownloadManager$1$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lflipboard/io/DownloadManager$1$2;-><init>(Lflipboard/io/DownloadManager$1;)V

    invoke-virtual {v2, v3}, Lflipboard/app/FlipboardApplication;->a(Lflipboard/util/Observer;)V

    .line 228
    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    sget-object v2, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v2}, Lflipboard/io/NetworkManager;->a()Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x1

    :goto_3
    iput-boolean v2, v3, Lflipboard/io/DownloadManager;->l:Z

    .line 229
    return-void

    .line 162
    :cond_1
    :try_start_7
    monitor-exit v18
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_1

    .line 164
    :cond_2
    :try_start_8
    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v3, v2, v4
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 165
    const/4 v2, 0x1

    goto/16 :goto_1

    .line 174
    :cond_3
    :try_start_9
    sget-object v2, Lflipboard/io/DownloadManager;->a:Lflipboard/util/Log;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 175
    const/4 v2, 0x1

    goto/16 :goto_1

    .line 178
    :catchall_1
    move-exception v2

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    throw v2

    .line 200
    :catchall_2
    move-exception v3

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v3

    .line 206
    :catch_1
    move-exception v2

    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v4, "could not create: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    new-instance v7, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v8, v0, Lflipboard/io/DownloadManager$1;->a:Lflipboard/io/DownloadManager;

    iget-object v8, v8, Lflipboard/io/DownloadManager;->e:Ljava/io/File;

    invoke-direct {v7, v8, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 228
    :cond_4
    const/4 v2, 0x0

    goto :goto_3
.end method
