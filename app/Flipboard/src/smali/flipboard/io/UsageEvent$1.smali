.class Lflipboard/io/UsageEvent$1;
.super Landroid/os/AsyncTask;
.source "UsageEvent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/io/UsageEvent;


# direct methods
.method constructor <init>(Lflipboard/io/UsageEvent;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lflipboard/io/UsageEvent$1;->a:Lflipboard/io/UsageEvent;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 154
    iget-object v1, p0, Lflipboard/io/UsageEvent$1;->a:Lflipboard/io/UsageEvent;

    invoke-virtual {v1}, Lflipboard/io/UsageEvent;->c()V

    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    iget-object v2, v0, Lflipboard/io/UsageManager;->n:Lflipboard/io/UsageManager$Uploader;

    sget-object v0, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v3

    if-ne v0, v3, :cond_1

    const-string v0, "Wrong thread. This method does network calls, so don\'t run it on the UI thread"

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v3, v3, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v3, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v3, v0, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ac:Z

    if-eqz v0, :cond_3

    sget-object v0, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    invoke-virtual {v2, v1}, Lflipboard/io/UsageManager$Uploader;->a(Lflipboard/io/UsageEvent;)V

    .line 155
    :cond_2
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 154
    :cond_3
    :try_start_0
    new-instance v3, Ljava/io/File;

    iget-object v0, v2, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v0, v0, Lflipboard/io/UsageManager;->d:Ljava/io/File;

    const-string v4, "highprio-%s.json"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v4, "rw"

    invoke-direct {v0, v3, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iget-object v4, v2, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v4, v4, Lflipboard/io/UsageManager;->q:Ljava/lang/String;

    invoke-virtual {v2, v0, v4}, Lflipboard/io/UsageManager$Uploader;->a(Ljava/io/RandomAccessFile;Ljava/lang/String;)V

    invoke-virtual {v1}, Lflipboard/io/UsageEvent;->d()[B

    move-result-object v4

    invoke-static {v0, v4}, Lflipboard/io/UsageManager$Uploader;->a(Ljava/io/RandomAccessFile;[B)V

    invoke-static {v0}, Lflipboard/io/UsageManager$Uploader;->a(Ljava/io/RandomAccessFile;)V

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {v2, v3}, Lflipboard/io/UsageManager$Uploader;->a(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    iget-object v0, v1, Lflipboard/io/UsageEvent;->j:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, v2, Lflipboard/io/UsageManager$Uploader;->e:Lflipboard/io/UsageManager;

    iget-object v1, v1, Lflipboard/io/UsageEvent;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2, v1}, Lflipboard/io/UsageManager$Uploader;->a(Lflipboard/io/UsageEvent;)V

    sget-object v4, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v5, "Failed to upload high priority usage right away, putting it on the normal schedule"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v3, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 150
    invoke-direct {p0}, Lflipboard/io/UsageEvent$1;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
