.class Lflipboard/io/BitmapManager$ScaledHandle;
.super Lflipboard/io/BitmapManager$Handle;
.source "BitmapManager.java"


# instance fields
.field final a:Lflipboard/io/BitmapManager$Handle;

.field final b:Landroid/graphics/Rect;

.field final c:F

.field final synthetic m:Lflipboard/io/BitmapManager;


# direct methods
.method constructor <init>(Lflipboard/io/BitmapManager;Lflipboard/io/BitmapManager$Handle;Landroid/graphics/Rect;F)V
    .locals 1

    .prologue
    .line 1018
    iput-object p1, p0, Lflipboard/io/BitmapManager$ScaledHandle;->m:Lflipboard/io/BitmapManager;

    invoke-direct {p0, p1}, Lflipboard/io/BitmapManager$Handle;-><init>(Lflipboard/io/BitmapManager;)V

    .line 1019
    iput-object p2, p0, Lflipboard/io/BitmapManager$ScaledHandle;->a:Lflipboard/io/BitmapManager$Handle;

    .line 1020
    iput-object p3, p0, Lflipboard/io/BitmapManager$ScaledHandle;->b:Landroid/graphics/Rect;

    .line 1021
    iput p4, p0, Lflipboard/io/BitmapManager$ScaledHandle;->c:F

    .line 1022
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p4

    float-to-int v0, v0

    iput v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->h:I

    .line 1023
    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p4

    float-to-int v0, v0

    iput v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->i:I

    .line 1024
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;F)Lflipboard/io/BitmapManager$Handle;
    .locals 4

    .prologue
    .line 1040
    sget-object v0, Lflipboard/io/BitmapManager;->a:Lflipboard/util/Log;

    const-string v1, "scaling bitmap that cant be scaled: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1041
    const/4 v0, 0x0

    return-object v0
.end method

.method final a()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 1028
    return-object p0
.end method

.method final a(Ljava/lang/StringBuilder;)V
    .locals 3

    .prologue
    const/16 v2, 0x2c

    .line 1073
    invoke-super {p0, p1}, Lflipboard/io/BitmapManager$Handle;->a(Ljava/lang/StringBuilder;)V

    .line 1074
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1075
    iget-object v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1076
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1077
    iget-object v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1078
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1079
    iget-object v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1080
    const/16 v0, 0x78

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1081
    iget-object v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1082
    iget v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->c:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 1083
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1084
    iget v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->c:F

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1086
    :cond_0
    iget-object v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->a:Lflipboard/io/BitmapManager$Handle;

    if-eqz v0, :cond_1

    .line 1087
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1088
    iget-object v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->a:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1092
    :goto_0
    return-void

    .line 1090
    :cond_1
    const-string v0, ",null"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method final c()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 1052
    iget-object v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->a:Lflipboard/io/BitmapManager$Handle;

    iget-object v1, p0, Lflipboard/io/BitmapManager$ScaledHandle;->b:Landroid/graphics/Rect;

    iget v2, p0, Lflipboard/io/BitmapManager$ScaledHandle;->c:F

    invoke-virtual {v0, v1, v2}, Lflipboard/io/BitmapManager$Handle;->b(Landroid/graphics/Rect;F)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1057
    instance-of v0, p1, Lflipboard/io/BitmapManager$ScaledHandle;

    if-eqz v0, :cond_0

    .line 1058
    check-cast p1, Lflipboard/io/BitmapManager$ScaledHandle;

    .line 1059
    iget-object v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->a:Lflipboard/io/BitmapManager$Handle;

    iget-object v1, p1, Lflipboard/io/BitmapManager$ScaledHandle;->a:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->c:F

    iget v1, p1, Lflipboard/io/BitmapManager$ScaledHandle;->c:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->b:Landroid/graphics/Rect;

    iget-object v1, p1, Lflipboard/io/BitmapManager$ScaledHandle;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1060
    const/4 v0, 0x1

    .line 1063
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final declared-synchronized f()V
    .locals 1

    .prologue
    .line 1096
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 1097
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->g:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1099
    :cond_0
    monitor-exit p0

    return-void

    .line 1096
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final g()V
    .locals 1

    .prologue
    .line 1103
    invoke-super {p0}, Lflipboard/io/BitmapManager$Handle;->g()V

    .line 1104
    iget-object v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->a:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->d()V

    .line 1105
    return-void
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 1068
    iget-object v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->a:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->hashCode()I

    move-result v0

    iget-object v1, p0, Lflipboard/io/BitmapManager$ScaledHandle;->b:Landroid/graphics/Rect;

    invoke-static {v1}, Lflipboard/util/AndroidUtil;->a(Landroid/graphics/Rect;)I

    move-result v1

    xor-int/2addr v0, v1

    iget v1, p0, Lflipboard/io/BitmapManager$ScaledHandle;->c:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1032
    iget-object v0, p0, Lflipboard/io/BitmapManager$ScaledHandle;->a:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
