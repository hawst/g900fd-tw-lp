.class public final enum Lflipboard/objs/UsageEventV2$AppEnterNavFrom;
.super Ljava/lang/Enum;
.source "UsageEventV2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/objs/UsageEventV2$AppEnterNavFrom;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

.field public static final enum b:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

.field public static final enum c:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

.field public static final enum d:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

.field public static final enum e:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

.field public static final enum f:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

.field public static final enum g:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

.field public static final enum h:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

.field public static final enum i:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

.field private static final synthetic j:[Lflipboard/objs/UsageEventV2$AppEnterNavFrom;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 82
    new-instance v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    const-string v1, "sstream"

    invoke-direct {v0, v1, v3}, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->a:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    .line 83
    new-instance v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    const-string v1, "web"

    invoke-direct {v0, v1, v4}, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->b:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    .line 85
    new-instance v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    const-string v1, "widget"

    invoke-direct {v0, v1, v5}, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->c:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    .line 86
    new-instance v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    const-string v1, "push_notification"

    invoke-direct {v0, v1, v6}, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->d:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    .line 88
    new-instance v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    const-string v1, "share"

    invoke-direct {v0, v1, v7}, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->e:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    .line 90
    new-instance v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    const-string v1, "system_settings"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->f:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    .line 92
    new-instance v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    const-string v1, "reminder_notification"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->g:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    .line 94
    new-instance v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    const-string v1, "flipboard_data_lib"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->h:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    .line 96
    new-instance v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    const-string v1, "facebook_app"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->i:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    .line 80
    const/16 v0, 0x9

    new-array v0, v0, [Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    sget-object v1, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->a:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->b:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->c:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->d:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->e:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->f:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->g:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->h:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->i:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->j:[Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/objs/UsageEventV2$AppEnterNavFrom;
    .locals 1

    .prologue
    .line 80
    const-class v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    return-object v0
.end method

.method public static values()[Lflipboard/objs/UsageEventV2$AppEnterNavFrom;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->j:[Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    invoke-virtual {v0}, [Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    return-object v0
.end method
