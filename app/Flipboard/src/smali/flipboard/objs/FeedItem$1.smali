.class Lflipboard/objs/FeedItem$1;
.super Ljava/lang/Object;
.source "FeedItem.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lflipboard/objs/FeedItem;


# direct methods
.method constructor <init>(Lflipboard/objs/FeedItem;Z)V
    .locals 0

    .prologue
    .line 923
    iput-object p1, p0, Lflipboard/objs/FeedItem$1;->b:Lflipboard/objs/FeedItem;

    iput-boolean p2, p0, Lflipboard/objs/FeedItem$1;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 926
    iget-object v0, p0, Lflipboard/objs/FeedItem$1;->b:Lflipboard/objs/FeedItem;

    iget-boolean v1, v0, Lflipboard/objs/FeedItem;->af:Z

    .line 927
    iget-object v0, p0, Lflipboard/objs/FeedItem$1;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->am()V

    .line 928
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, p0, Lflipboard/objs/FeedItem$1;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->ai()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v2

    .line 929
    if-eqz v2, :cond_1

    .line 930
    iget-object v0, p0, Lflipboard/objs/FeedItem$1;->b:Lflipboard/objs/FeedItem;

    iget-object v3, v0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    iget-boolean v0, p0, Lflipboard/objs/FeedItem$1;->a:Z

    invoke-virtual {v3, v2}, Lflipboard/objs/CommentaryResult$Item;->a(Lflipboard/service/Account;)Z

    move-result v4

    if-eq v0, v4, :cond_0

    if-eqz v0, :cond_2

    new-instance v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    invoke-direct {v0}, Lflipboard/objs/CommentaryResult$Item$Commentary;-><init>()V

    const-string v4, "like"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5, v2}, Lflipboard/objs/CommentaryResult$Item$Commentary;->a(Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Account;)Lflipboard/objs/CommentaryResult$Item$Commentary;

    move-result-object v0

    iget-object v4, v3, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget v0, v3, Lflipboard/objs/CommentaryResult$Item;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v3, Lflipboard/objs/CommentaryResult$Item;->c:I

    .line 933
    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/objs/FeedItem$1;->b:Lflipboard/objs/FeedItem;

    iget-object v3, p0, Lflipboard/objs/FeedItem$1;->b:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    invoke-virtual {v3, v2}, Lflipboard/objs/CommentaryResult$Item;->a(Lflipboard/service/Account;)Z

    move-result v2

    iput-boolean v2, v0, Lflipboard/objs/FeedItem;->af:Z

    .line 935
    iget-object v0, p0, Lflipboard/objs/FeedItem$1;->b:Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->af:Z

    if-eq v1, v0, :cond_1

    .line 936
    iget-object v0, p0, Lflipboard/objs/FeedItem$1;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->an()V

    .line 939
    :cond_1
    return-void

    .line 930
    :cond_2
    iget-object v0, v3, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    const-string v5, "like"

    iget-object v6, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->a:Ljava/lang/String;

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->e:Ljava/lang/String;

    invoke-virtual {v2, v5, v0}, Lflipboard/service/Account;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    iget v0, v3, Lflipboard/objs/CommentaryResult$Item;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v3, Lflipboard/objs/CommentaryResult$Item;->c:I

    goto :goto_0
.end method
