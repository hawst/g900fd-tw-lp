.class public Lflipboard/objs/ConfigHints$Hint;
.super Lflipboard/objs/Base;
.source "ConfigHints.java"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;


# instance fields
.field public A:Z

.field public B:Ljava/lang/String;

.field public C:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public D:Ljava/lang/String;

.field public E:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigHints$Condition;",
            ">;"
        }
    .end annotation
.end field

.field public F:Z

.field public G:Ljava/lang/String;

.field public H:Ljava/lang/String;

.field public I:Ljava/lang/String;

.field public d:Z

.field public e:I

.field public f:I

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigHints$Condition;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Z

.field public n:F

.field public o:F

.field public p:Z

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:F

.field public w:F

.field public x:F

.field public y:F

.field public z:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "pulse"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lflipboard/objs/ConfigHints$Hint;->a:Ljava/lang/String;

    .line 18
    const-string v0, "alert"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lflipboard/objs/ConfigHints$Hint;->b:Ljava/lang/String;

    .line 19
    const-string v0, "lightbox"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lflipboard/objs/ConfigHints$Hint;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 15
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 21
    iput-boolean v1, p0, Lflipboard/objs/ConfigHints$Hint;->d:Z

    .line 22
    iput v0, p0, Lflipboard/objs/ConfigHints$Hint;->e:I

    .line 23
    iput v0, p0, Lflipboard/objs/ConfigHints$Hint;->f:I

    .line 58
    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lflipboard/objs/ConfigHints$Hint;->v:F

    .line 62
    const v0, 0x3f333333    # 0.7f

    iput v0, p0, Lflipboard/objs/ConfigHints$Hint;->w:F

    .line 67
    const/high16 v0, 0x42b40000    # 90.0f

    iput v0, p0, Lflipboard/objs/ConfigHints$Hint;->x:F

    .line 72
    const/high16 v0, 0x42200000    # 40.0f

    iput v0, p0, Lflipboard/objs/ConfigHints$Hint;->y:F

    .line 108
    iput-boolean v1, p0, Lflipboard/objs/ConfigHints$Hint;->F:Z

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 90
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/objs/ConfigHints$Hint;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 95
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/objs/ConfigHints$Hint;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
