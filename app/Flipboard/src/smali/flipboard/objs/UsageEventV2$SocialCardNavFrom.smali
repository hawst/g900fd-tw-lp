.class public final enum Lflipboard/objs/UsageEventV2$SocialCardNavFrom;
.super Ljava/lang/Enum;
.source "UsageEventV2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/objs/UsageEventV2$SocialCardNavFrom;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

.field public static final enum b:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

.field public static final enum c:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

.field private static final synthetic d:[Lflipboard/objs/UsageEventV2$SocialCardNavFrom;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 127
    new-instance v0, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    const-string v1, "detail_button"

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->a:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    .line 129
    new-instance v0, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    const-string v1, "layout_button"

    invoke-direct {v0, v1, v3}, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->b:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    .line 131
    new-instance v0, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    const-string v1, "layout_item"

    invoke-direct {v0, v1, v4}, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->c:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    .line 125
    const/4 v0, 0x3

    new-array v0, v0, [Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    sget-object v1, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->a:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->b:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->c:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    aput-object v1, v0, v4

    sput-object v0, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->d:[Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/objs/UsageEventV2$SocialCardNavFrom;
    .locals 1

    .prologue
    .line 125
    const-class v0, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    return-object v0
.end method

.method public static values()[Lflipboard/objs/UsageEventV2$SocialCardNavFrom;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->d:[Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    invoke-virtual {v0}, [Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    return-object v0
.end method
