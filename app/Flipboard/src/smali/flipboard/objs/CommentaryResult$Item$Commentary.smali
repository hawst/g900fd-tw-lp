.class public Lflipboard/objs/CommentaryResult$Item$Commentary;
.super Lflipboard/objs/Author;
.source "CommentaryResult.java"


# instance fields
.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:J

.field public l:Z

.field public m:Ljava/lang/String;

.field public n:I

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedSectionLink;",
            ">;"
        }
    .end annotation
.end field

.field public p:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lflipboard/objs/Author;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Account;)Lflipboard/objs/CommentaryResult$Item$Commentary;
    .locals 4

    .prologue
    .line 103
    iget-object v0, p3, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/Author;->a:Ljava/lang/String;

    iget-object v0, p3, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/Author;->b:Ljava/lang/String;

    iget-object v0, p3, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/Author;->c:Ljava/lang/String;

    iget-object v0, p3, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->c:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/Author;->d:Ljava/lang/String;

    iget-object v0, p3, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/Author;->e:Ljava/lang/String;

    .line 104
    iput-object p1, p0, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    .line 105
    iput-object p2, p0, Lflipboard/objs/CommentaryResult$Item$Commentary;->j:Ljava/lang/String;

    .line 106
    iget-object v0, p0, Lflipboard/objs/CommentaryResult$Item$Commentary;->g:Lflipboard/objs/Image;

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Lflipboard/objs/Image;

    invoke-direct {v0}, Lflipboard/objs/Image;-><init>()V

    iput-object v0, p0, Lflipboard/objs/CommentaryResult$Item$Commentary;->g:Lflipboard/objs/Image;

    .line 109
    :cond_0
    iget-object v0, p0, Lflipboard/objs/CommentaryResult$Item$Commentary;->g:Lflipboard/objs/Image;

    iget-object v1, p3, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v1}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    .line 110
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, Lflipboard/objs/CommentaryResult$Item$Commentary;->k:J

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/objs/CommentaryResult$Item$Commentary;->o:Ljava/util/List;

    .line 112
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/objs/CommentaryResult$Item$Commentary;->n:I

    .line 113
    return-object p0
.end method
