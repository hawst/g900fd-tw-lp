.class public Lflipboard/objs/CommentaryResult$Item;
.super Lflipboard/objs/Base;
.source "CommentaryResult.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/CommentaryResult$Item$Commentary;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/CommentaryResult$Item$ProfileMetric;",
            ">;"
        }
    .end annotation
.end field

.field public j:J

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field public s:I

.field public t:I

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 88
    return-void
.end method

.method public static a()Lflipboard/objs/CommentaryResult$Item;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Lflipboard/objs/CommentaryResult$Item;

    invoke-direct {v0}, Lflipboard/objs/CommentaryResult$Item;-><init>()V

    .line 53
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    .line 54
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item;->h:Ljava/util/List;

    .line 55
    return-object v0
.end method


# virtual methods
.method public final a(Lflipboard/objs/CommentaryResult$Item;)Lflipboard/objs/CommentaryResult$Item;
    .locals 4

    .prologue
    .line 69
    new-instance v1, Ljava/util/HashSet;

    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 70
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    .line 71
    invoke-virtual {v0}, Lflipboard/objs/CommentaryResult$Item$Commentary;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 73
    :cond_0
    iget-object v0, p0, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    .line 74
    invoke-virtual {v0}, Lflipboard/objs/CommentaryResult$Item$Commentary;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 75
    iget-object v3, p1, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 78
    :cond_2
    return-object p1
.end method

.method public final a(Lflipboard/service/Account;)Z
    .locals 4

    .prologue
    .line 193
    if-eqz p1, :cond_1

    .line 194
    iget-object v0, p0, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    .line 195
    const-string v2, "like"

    iget-object v3, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->a:Ljava/lang/String;

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->e:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Lflipboard/service/Account;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    const/4 v0, 0x1

    .line 200
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lflipboard/objs/CommentaryResult$Item;)Z
    .locals 2

    .prologue
    .line 225
    iget v0, p0, Lflipboard/objs/CommentaryResult$Item;->b:I

    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->b:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lflipboard/objs/CommentaryResult$Item;->c:I

    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->c:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lflipboard/objs/CommentaryResult$Item;->d:I

    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->d:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lflipboard/objs/CommentaryResult$Item;->f:I

    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->f:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lflipboard/objs/CommentaryResult$Item;->k:Ljava/util/List;

    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item;->k:Ljava/util/List;

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
