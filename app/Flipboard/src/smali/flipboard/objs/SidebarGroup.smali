.class public Lflipboard/objs/SidebarGroup;
.super Lflipboard/objs/Base;
.source "SidebarGroup.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SidebarGroup$RenderHints;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SidebarGroup$Metrics;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    .line 43
    return-void
.end method

.method private static b(Lflipboard/service/Flap$TypedResultObserver;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/objs/RecentImage;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 164
    if-eqz p0, :cond_0

    .line 165
    const-string v0, "No items"

    invoke-interface {p0, v0}, Lflipboard/service/Flap$TypedResultObserver;->a(Ljava/lang/String;)V

    .line 167
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lflipboard/objs/SidebarGroup$RenderHints;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 100
    iget-object v0, p0, Lflipboard/objs/SidebarGroup;->e:Ljava/util/List;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 120
    :goto_0
    :pswitch_0
    return-object v0

    .line 103
    :cond_0
    iget-object v0, p0, Lflipboard/objs/SidebarGroup;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup$RenderHints;

    .line 104
    iget-object v4, v0, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_1
    :goto_2
    packed-switch v2, :pswitch_data_0

    goto :goto_1

    :sswitch_0
    const-string v5, "pageboxGrid"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v2, 0x0

    goto :goto_2

    :sswitch_1
    const-string v5, "pageboxList"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v2, 0x1

    goto :goto_2

    :sswitch_2
    const-string v5, "pageboxSuggestedFollows"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v2, 0x2

    goto :goto_2

    :sswitch_3
    const-string v5, "recommendedTopics"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v2, 0x3

    goto :goto_2

    :sswitch_4
    const-string v5, "recommendedMagmakers"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v2, 0x4

    goto :goto_2

    :sswitch_5
    const-string v5, "recommendedMagazines"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v2, 0x5

    goto :goto_2

    :sswitch_6
    const-string v5, "pageboxExpandable"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v2, 0x6

    goto :goto_2

    :sswitch_7
    const-string v5, "pageboxCarousel"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v2, 0x7

    goto :goto_2

    :sswitch_8
    const-string v5, "pageboxCreateAccount"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/16 v2, 0x8

    goto :goto_2

    :sswitch_9
    const-string v5, "pageboxAddService"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/16 v2, 0x9

    goto :goto_2

    :sswitch_a
    const-string v5, "pageboxFindFriends"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/16 v2, 0xa

    goto :goto_2

    :cond_2
    move-object v0, v1

    .line 120
    goto/16 :goto_0

    .line 104
    nop

    :sswitch_data_0
    .sparse-switch
        -0x620a2cc5 -> :sswitch_2
        -0x4c18dce0 -> :sswitch_a
        -0x32a239eb -> :sswitch_8
        -0x2b6b8b30 -> :sswitch_9
        0xcc4ecf0 -> :sswitch_6
        0x44f7ff44 -> :sswitch_5
        0x4995335f -> :sswitch_3
        0x5455065c -> :sswitch_7
        0x5812a3a7 -> :sswitch_4
        0x5b8b0e42 -> :sswitch_0
        0x5b8d339a -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lflipboard/service/Flap$TypedResultObserver;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/service/Flap$TypedResultObserver",
            "<",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/objs/RecentImage;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 146
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 147
    iget-object v0, p0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 148
    iget-object v3, v0, Lflipboard/objs/FeedItem;->d:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 149
    iget-object v0, v0, Lflipboard/objs/FeedItem;->d:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 152
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 153
    invoke-static {v1, p1}, Lflipboard/util/AndroidUtil;->a(Ljava/util/List;Lflipboard/service/Flap$TypedResultObserver;)V

    .line 160
    :goto_1
    return-void

    .line 155
    :cond_2
    invoke-static {p1}, Lflipboard/objs/SidebarGroup;->b(Lflipboard/service/Flap$TypedResultObserver;)V

    goto :goto_1

    .line 158
    :cond_3
    invoke-static {p1}, Lflipboard/objs/SidebarGroup;->b(Lflipboard/service/Flap$TypedResultObserver;)V

    goto :goto_1
.end method

.method public final a(Lflipboard/objs/SidebarGroup;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 175
    if-nez p1, :cond_1

    .line 183
    :cond_0
    :goto_0
    return v0

    .line 178
    :cond_1
    invoke-virtual {p0}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v2

    .line 179
    invoke-virtual {p1}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v3

    .line 180
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    .line 181
    invoke-virtual {p0}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v2

    iget v2, v2, Lflipboard/objs/SidebarGroup$RenderHints;->c:I

    invoke-virtual {p1}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v3

    iget v3, v3, Lflipboard/objs/SidebarGroup$RenderHints;->c:I

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 183
    :cond_2
    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
