.class public final enum Lflipboard/objs/UsageEventV2$EventDataType;
.super Ljava/lang/Enum;
.source "UsageEventV2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/objs/UsageEventV2$EventDataType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/objs/UsageEventV2$EventDataType;

.field public static final enum b:Lflipboard/objs/UsageEventV2$EventDataType;

.field public static final enum c:Lflipboard/objs/UsageEventV2$EventDataType;

.field public static final enum d:Lflipboard/objs/UsageEventV2$EventDataType;

.field private static final synthetic e:[Lflipboard/objs/UsageEventV2$EventDataType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 208
    new-instance v0, Lflipboard/objs/UsageEventV2$EventDataType;

    const-string v1, "first_flip"

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventDataType;->a:Lflipboard/objs/UsageEventV2$EventDataType;

    new-instance v0, Lflipboard/objs/UsageEventV2$EventDataType;

    const-string v1, "also_flipped"

    invoke-direct {v0, v1, v3}, Lflipboard/objs/UsageEventV2$EventDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventDataType;->b:Lflipboard/objs/UsageEventV2$EventDataType;

    new-instance v0, Lflipboard/objs/UsageEventV2$EventDataType;

    const-string v1, "public_mag"

    invoke-direct {v0, v1, v4}, Lflipboard/objs/UsageEventV2$EventDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventDataType;->c:Lflipboard/objs/UsageEventV2$EventDataType;

    new-instance v0, Lflipboard/objs/UsageEventV2$EventDataType;

    const-string v1, "private_mag"

    invoke-direct {v0, v1, v5}, Lflipboard/objs/UsageEventV2$EventDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventDataType;->d:Lflipboard/objs/UsageEventV2$EventDataType;

    const/4 v0, 0x4

    new-array v0, v0, [Lflipboard/objs/UsageEventV2$EventDataType;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventDataType;->a:Lflipboard/objs/UsageEventV2$EventDataType;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/objs/UsageEventV2$EventDataType;->b:Lflipboard/objs/UsageEventV2$EventDataType;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/objs/UsageEventV2$EventDataType;->c:Lflipboard/objs/UsageEventV2$EventDataType;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/objs/UsageEventV2$EventDataType;->d:Lflipboard/objs/UsageEventV2$EventDataType;

    aput-object v1, v0, v5

    sput-object v0, Lflipboard/objs/UsageEventV2$EventDataType;->e:[Lflipboard/objs/UsageEventV2$EventDataType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 208
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/objs/UsageEventV2$EventDataType;
    .locals 1

    .prologue
    .line 208
    const-class v0, Lflipboard/objs/UsageEventV2$EventDataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UsageEventV2$EventDataType;

    return-object v0
.end method

.method public static values()[Lflipboard/objs/UsageEventV2$EventDataType;
    .locals 1

    .prologue
    .line 208
    sget-object v0, Lflipboard/objs/UsageEventV2$EventDataType;->e:[Lflipboard/objs/UsageEventV2$EventDataType;

    invoke-virtual {v0}, [Lflipboard/objs/UsageEventV2$EventDataType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/objs/UsageEventV2$EventDataType;

    return-object v0
.end method
