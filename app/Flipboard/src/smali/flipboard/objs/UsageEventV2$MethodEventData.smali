.class public final enum Lflipboard/objs/UsageEventV2$MethodEventData;
.super Ljava/lang/Enum;
.source "UsageEventV2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/objs/UsageEventV2$MethodEventData;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/objs/UsageEventV2$MethodEventData;

.field public static final enum b:Lflipboard/objs/UsageEventV2$MethodEventData;

.field private static final synthetic c:[Lflipboard/objs/UsageEventV2$MethodEventData;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 145
    new-instance v0, Lflipboard/objs/UsageEventV2$MethodEventData;

    const-string v1, "new_user"

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$MethodEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$MethodEventData;->a:Lflipboard/objs/UsageEventV2$MethodEventData;

    .line 146
    new-instance v0, Lflipboard/objs/UsageEventV2$MethodEventData;

    const-string v1, "existing_user"

    invoke-direct {v0, v1, v3}, Lflipboard/objs/UsageEventV2$MethodEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$MethodEventData;->b:Lflipboard/objs/UsageEventV2$MethodEventData;

    .line 144
    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/objs/UsageEventV2$MethodEventData;

    sget-object v1, Lflipboard/objs/UsageEventV2$MethodEventData;->a:Lflipboard/objs/UsageEventV2$MethodEventData;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/objs/UsageEventV2$MethodEventData;->b:Lflipboard/objs/UsageEventV2$MethodEventData;

    aput-object v1, v0, v3

    sput-object v0, Lflipboard/objs/UsageEventV2$MethodEventData;->c:[Lflipboard/objs/UsageEventV2$MethodEventData;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/objs/UsageEventV2$MethodEventData;
    .locals 1

    .prologue
    .line 144
    const-class v0, Lflipboard/objs/UsageEventV2$MethodEventData;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UsageEventV2$MethodEventData;

    return-object v0
.end method

.method public static values()[Lflipboard/objs/UsageEventV2$MethodEventData;
    .locals 1

    .prologue
    .line 144
    sget-object v0, Lflipboard/objs/UsageEventV2$MethodEventData;->c:[Lflipboard/objs/UsageEventV2$MethodEventData;

    invoke-virtual {v0}, [Lflipboard/objs/UsageEventV2$MethodEventData;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/objs/UsageEventV2$MethodEventData;

    return-object v0
.end method
