.class public final enum Lflipboard/objs/UsageEventV2$SectionNavFrom;
.super Ljava/lang/Enum;
.source "UsageEventV2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/objs/UsageEventV2$SectionNavFrom;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/objs/UsageEventV2$SectionNavFrom;

.field public static final enum b:Lflipboard/objs/UsageEventV2$SectionNavFrom;

.field public static final enum c:Lflipboard/objs/UsageEventV2$SectionNavFrom;

.field public static final enum d:Lflipboard/objs/UsageEventV2$SectionNavFrom;

.field public static final enum e:Lflipboard/objs/UsageEventV2$SectionNavFrom;

.field public static final enum f:Lflipboard/objs/UsageEventV2$SectionNavFrom;

.field public static final enum g:Lflipboard/objs/UsageEventV2$SectionNavFrom;

.field public static final enum h:Lflipboard/objs/UsageEventV2$SectionNavFrom;

.field public static final enum i:Lflipboard/objs/UsageEventV2$SectionNavFrom;

.field public static final enum j:Lflipboard/objs/UsageEventV2$SectionNavFrom;

.field public static final enum k:Lflipboard/objs/UsageEventV2$SectionNavFrom;

.field public static final enum l:Lflipboard/objs/UsageEventV2$SectionNavFrom;

.field private static final synthetic m:[Lflipboard/objs/UsageEventV2$SectionNavFrom;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 102
    new-instance v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;

    const-string v1, "toc"

    invoke-direct {v0, v1, v3}, Lflipboard/objs/UsageEventV2$SectionNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->a:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    .line 103
    new-instance v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;

    const-string v1, "search"

    invoke-direct {v0, v1, v4}, Lflipboard/objs/UsageEventV2$SectionNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->b:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    .line 105
    new-instance v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;

    const-string v1, "section_item"

    invoke-direct {v0, v1, v5}, Lflipboard/objs/UsageEventV2$SectionNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->c:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    .line 107
    new-instance v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;

    const-string v1, "startup"

    invoke-direct {v0, v1, v6}, Lflipboard/objs/UsageEventV2$SectionNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->d:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    .line 109
    new-instance v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;

    const-string v1, "simple_content_guide"

    invoke-direct {v0, v1, v7}, Lflipboard/objs/UsageEventV2$SectionNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->e:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    .line 111
    new-instance v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;

    const-string v1, "back_to_home_feed"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$SectionNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->f:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    .line 113
    new-instance v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;

    const-string v1, "fast_section_switcher"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$SectionNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->g:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    .line 115
    new-instance v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;

    const-string v1, "content_guide"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$SectionNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->h:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    .line 116
    new-instance v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;

    const-string v1, "content_guide_brick"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$SectionNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->i:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    .line 118
    new-instance v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;

    const-string v1, "layout"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$SectionNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->j:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    .line 120
    new-instance v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;

    const-string v1, "pagebox"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$SectionNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->k:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    .line 122
    new-instance v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;

    const-string v1, "topic_tag"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$SectionNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->l:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    .line 100
    const/16 v0, 0xc

    new-array v0, v0, [Lflipboard/objs/UsageEventV2$SectionNavFrom;

    sget-object v1, Lflipboard/objs/UsageEventV2$SectionNavFrom;->a:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/objs/UsageEventV2$SectionNavFrom;->b:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/objs/UsageEventV2$SectionNavFrom;->c:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/objs/UsageEventV2$SectionNavFrom;->d:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/objs/UsageEventV2$SectionNavFrom;->e:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/objs/UsageEventV2$SectionNavFrom;->f:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lflipboard/objs/UsageEventV2$SectionNavFrom;->g:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lflipboard/objs/UsageEventV2$SectionNavFrom;->h:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lflipboard/objs/UsageEventV2$SectionNavFrom;->i:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lflipboard/objs/UsageEventV2$SectionNavFrom;->j:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lflipboard/objs/UsageEventV2$SectionNavFrom;->k:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lflipboard/objs/UsageEventV2$SectionNavFrom;->l:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->m:[Lflipboard/objs/UsageEventV2$SectionNavFrom;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/objs/UsageEventV2$SectionNavFrom;
    .locals 1

    .prologue
    .line 100
    const-class v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;

    return-object v0
.end method

.method public static values()[Lflipboard/objs/UsageEventV2$SectionNavFrom;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->m:[Lflipboard/objs/UsageEventV2$SectionNavFrom;

    invoke-virtual {v0}, [Lflipboard/objs/UsageEventV2$SectionNavFrom;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/objs/UsageEventV2$SectionNavFrom;

    return-object v0
.end method
