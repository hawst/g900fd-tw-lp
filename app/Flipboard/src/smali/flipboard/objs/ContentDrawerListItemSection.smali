.class public abstract Lflipboard/objs/ContentDrawerListItemSection;
.super Lflipboard/objs/ContentDrawerListItemBase;
.source "ContentDrawerListItemSection.java"


# instance fields
.field public o:Ljava/lang/String;

.field public p:Ljava/lang/Object;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:Z

.field public v:I

.field public w:Z

.field public x:Z

.field public y:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lflipboard/objs/ContentDrawerListItemBase;-><init>()V

    return-void
.end method


# virtual methods
.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lflipboard/objs/ContentDrawerListItemSection;->t:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-super {p0}, Lflipboard/objs/ContentDrawerListItemBase;->j()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/objs/ContentDrawerListItemSection;->t:Ljava/lang/String;

    goto :goto_0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lflipboard/objs/ContentDrawerListItemSection;->q:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-super {p0}, Lflipboard/objs/ContentDrawerListItemBase;->p()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/objs/ContentDrawerListItemSection;->q:Ljava/lang/String;

    goto :goto_0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lflipboard/objs/ContentDrawerListItemSection;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lflipboard/objs/ContentDrawerListItemSection;->u:Z

    return v0
.end method

.method public final w()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lflipboard/objs/ContentDrawerListItemSection;->v:I

    return v0
.end method
