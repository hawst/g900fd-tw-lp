.class public Lflipboard/objs/Image;
.super Lflipboard/objs/Base;
.source "Image.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:I

.field public g:I

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public transient k:Ljava/lang/String;

.field transient l:Lflipboard/objs/Image$AvailabilityMap;

.field private transient m:I

.field private transient n:I

.field private transient o:I

.field private transient p:I

.field private transient q:I

.field private transient r:I

.field private transient s:Z

.field private transient t:Landroid/graphics/PointF;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 16
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 39
    iput v0, p0, Lflipboard/objs/Image;->m:I

    .line 40
    iput v0, p0, Lflipboard/objs/Image;->n:I

    .line 41
    iput v0, p0, Lflipboard/objs/Image;->o:I

    .line 42
    iput v0, p0, Lflipboard/objs/Image;->p:I

    .line 43
    iput v0, p0, Lflipboard/objs/Image;->q:I

    .line 44
    iput v0, p0, Lflipboard/objs/Image;->r:I

    .line 45
    iput-boolean v1, p0, Lflipboard/objs/Image;->s:Z

    .line 50
    const-string v0, "N/A"

    iput-object v0, p0, Lflipboard/objs/Image;->k:Ljava/lang/String;

    .line 52
    new-instance v0, Lflipboard/objs/Image$AvailabilityMap;

    invoke-direct {v0, p0, v1}, Lflipboard/objs/Image$AvailabilityMap;-><init>(Lflipboard/objs/Image;B)V

    iput-object v0, p0, Lflipboard/objs/Image;->l:Lflipboard/objs/Image$AvailabilityMap;

    .line 391
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 66
    iget v0, p0, Lflipboard/objs/Image;->f:I

    iget v1, p0, Lflipboard/objs/Image;->g:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public final a(II)Ljava/lang/String;
    .locals 13

    .prologue
    const/high16 v12, 0x43fa0000    # 500.0f

    const/high16 v11, 0x43700000    # 240.0f

    const/4 v5, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v9, 0x3f000000    # 0.5f

    .line 266
    sget-object v3, Lflipboard/objs/Image$Size;->a:Lflipboard/objs/Image$Size;

    .line 267
    const/4 v2, 0x0

    .line 268
    iget-object v0, p0, Lflipboard/objs/Image;->l:Lflipboard/objs/Image$AvailabilityMap;

    invoke-static {v0}, Lflipboard/objs/Image$AvailabilityMap;->a(Lflipboard/objs/Image$AvailabilityMap;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 270
    iget-object v0, p0, Lflipboard/objs/Image;->l:Lflipboard/objs/Image$AvailabilityMap;

    invoke-static {v0}, Lflipboard/objs/Image$AvailabilityMap;->c(Lflipboard/objs/Image$AvailabilityMap;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 271
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/Image$Size;

    .line 272
    iget-boolean v4, p0, Lflipboard/objs/Image;->s:Z

    if-nez v4, :cond_1

    iget v4, p0, Lflipboard/objs/Image;->f:I

    int-to-float v4, v4

    div-float v4, v11, v4

    iget v7, p0, Lflipboard/objs/Image;->g:I

    int-to-float v7, v7

    div-float v7, v11, v7

    invoke-static {v4, v7}, Ljava/lang/Math;->min(FF)F

    move-result v4

    cmpl-float v7, v4, v10

    if-ltz v7, :cond_4

    iget v4, p0, Lflipboard/objs/Image;->f:I

    iput v4, p0, Lflipboard/objs/Image;->m:I

    iget v4, p0, Lflipboard/objs/Image;->g:I

    iput v4, p0, Lflipboard/objs/Image;->n:I

    :goto_0
    iget v4, p0, Lflipboard/objs/Image;->f:I

    int-to-float v4, v4

    div-float v4, v12, v4

    iget v7, p0, Lflipboard/objs/Image;->g:I

    int-to-float v7, v7

    div-float v7, v12, v7

    invoke-static {v4, v7}, Ljava/lang/Math;->min(FF)F

    move-result v4

    cmpl-float v7, v4, v10

    if-ltz v7, :cond_5

    iget v4, p0, Lflipboard/objs/Image;->f:I

    iput v4, p0, Lflipboard/objs/Image;->o:I

    iget v4, p0, Lflipboard/objs/Image;->g:I

    iput v4, p0, Lflipboard/objs/Image;->p:I

    :goto_1
    const/high16 v4, 0x44800000    # 1024.0f

    iget v7, p0, Lflipboard/objs/Image;->f:I

    int-to-float v7, v7

    div-float/2addr v4, v7

    const/high16 v7, 0x44800000    # 1024.0f

    iget v8, p0, Lflipboard/objs/Image;->g:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    invoke-static {v4, v7}, Ljava/lang/Math;->min(FF)F

    move-result v4

    cmpl-float v7, v4, v10

    if-ltz v7, :cond_6

    iget v4, p0, Lflipboard/objs/Image;->f:I

    iput v4, p0, Lflipboard/objs/Image;->q:I

    iget v4, p0, Lflipboard/objs/Image;->g:I

    iput v4, p0, Lflipboard/objs/Image;->r:I

    :goto_2
    const/4 v4, 0x1

    iput-boolean v4, p0, Lflipboard/objs/Image;->s:Z

    :cond_1
    sget-object v4, Lflipboard/objs/Image$1;->a:[I

    invoke-virtual {v1}, Lflipboard/objs/Image$Size;->ordinal()I

    move-result v7

    aget v4, v4, v7

    packed-switch v4, :pswitch_data_0

    :cond_2
    const/4 v4, 0x1

    :goto_3
    if-eqz v4, :cond_0

    .line 275
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 280
    :goto_4
    if-nez v0, :cond_3

    .line 281
    iget-object v0, p0, Lflipboard/objs/Image;->l:Lflipboard/objs/Image$AvailabilityMap;

    invoke-static {v0}, Lflipboard/objs/Image$AvailabilityMap;->b(Lflipboard/objs/Image$AvailabilityMap;)Lflipboard/objs/Image$Size;

    move-result-object v1

    .line 282
    iget-object v0, p0, Lflipboard/objs/Image;->l:Lflipboard/objs/Image$AvailabilityMap;

    invoke-static {v0, v1}, Lflipboard/objs/Image$AvailabilityMap;->a(Lflipboard/objs/Image$AvailabilityMap;Lflipboard/objs/Image$Size;)Ljava/lang/String;

    move-result-object v0

    .line 285
    :cond_3
    :goto_5
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v2, v2, Lflipboard/service/FlipboardManager;->ag:Z

    if-nez v2, :cond_a

    const-string v1, ""

    :goto_6
    iput-object v1, p0, Lflipboard/objs/Image;->k:Ljava/lang/String;

    .line 286
    return-object v0

    .line 272
    :cond_4
    iget v7, p0, Lflipboard/objs/Image;->f:I

    int-to-float v7, v7

    mul-float/2addr v7, v4

    add-float/2addr v7, v9

    float-to-int v7, v7

    iput v7, p0, Lflipboard/objs/Image;->m:I

    iget v7, p0, Lflipboard/objs/Image;->g:I

    int-to-float v7, v7

    mul-float/2addr v4, v7

    add-float/2addr v4, v9

    float-to-int v4, v4

    iput v4, p0, Lflipboard/objs/Image;->n:I

    goto :goto_0

    :cond_5
    iget v7, p0, Lflipboard/objs/Image;->f:I

    int-to-float v7, v7

    mul-float/2addr v7, v4

    add-float/2addr v7, v9

    float-to-int v7, v7

    iput v7, p0, Lflipboard/objs/Image;->o:I

    iget v7, p0, Lflipboard/objs/Image;->g:I

    int-to-float v7, v7

    mul-float/2addr v4, v7

    add-float/2addr v4, v9

    float-to-int v4, v4

    iput v4, p0, Lflipboard/objs/Image;->p:I

    goto :goto_1

    :cond_6
    iget v7, p0, Lflipboard/objs/Image;->f:I

    int-to-float v7, v7

    mul-float/2addr v7, v4

    add-float/2addr v7, v9

    float-to-int v7, v7

    iput v7, p0, Lflipboard/objs/Image;->q:I

    iget v7, p0, Lflipboard/objs/Image;->g:I

    int-to-float v7, v7

    mul-float/2addr v4, v7

    add-float/2addr v4, v9

    float-to-int v4, v4

    iput v4, p0, Lflipboard/objs/Image;->r:I

    goto :goto_2

    :pswitch_0
    iget v4, p0, Lflipboard/objs/Image;->m:I

    if-lt v4, p1, :cond_7

    iget v4, p0, Lflipboard/objs/Image;->n:I

    if-ge v4, p2, :cond_2

    :cond_7
    move v4, v5

    goto :goto_3

    :pswitch_1
    iget v4, p0, Lflipboard/objs/Image;->o:I

    if-lt v4, p1, :cond_8

    iget v4, p0, Lflipboard/objs/Image;->p:I

    if-ge v4, p2, :cond_2

    :cond_8
    move v4, v5

    goto :goto_3

    :pswitch_2
    iget v4, p0, Lflipboard/objs/Image;->q:I

    if-lt v4, p1, :cond_9

    iget v4, p0, Lflipboard/objs/Image;->r:I

    if-ge v4, p2, :cond_2

    :cond_9
    move v4, v5

    goto :goto_3

    .line 285
    :cond_a
    if-nez v0, :cond_b

    const-string v1, "N/A\n"

    goto :goto_6

    :cond_b
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "O: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lflipboard/objs/Image;->f:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "x"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lflipboard/objs/Image;->g:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lflipboard/objs/Image$Size;->b:Lflipboard/objs/Image$Size;

    if-ne v1, v2, :cond_d

    const-string v2, " > "

    :goto_7
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v4, Lflipboard/objs/Image$Size;->b:Lflipboard/objs/Image$Size;

    invoke-virtual {v4}, Lflipboard/objs/Image$Size;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    if-nez v2, :cond_e

    const-string v2, "N/A\n"

    :goto_8
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lflipboard/objs/Image$Size;->c:Lflipboard/objs/Image$Size;

    if-ne v1, v2, :cond_f

    const-string v2, " > "

    :goto_9
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v4, Lflipboard/objs/Image$Size;->c:Lflipboard/objs/Image$Size;

    invoke-virtual {v4}, Lflipboard/objs/Image$Size;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    if-nez v2, :cond_10

    const-string v2, "N/A\n"

    :goto_a
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lflipboard/objs/Image$Size;->d:Lflipboard/objs/Image$Size;

    if-ne v1, v2, :cond_11

    const-string v2, " > "

    :goto_b
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v4, Lflipboard/objs/Image$Size;->d:Lflipboard/objs/Image$Size;

    invoke-virtual {v4}, Lflipboard/objs/Image$Size;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lflipboard/objs/Image;->c:Ljava/lang/String;

    if-nez v2, :cond_12

    const-string v2, "N/A\n"

    :goto_c
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lflipboard/objs/Image$Size;->e:Lflipboard/objs/Image$Size;

    if-ne v1, v2, :cond_c

    const-string v1, " > "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lflipboard/objs/Image$Size;->e:Lflipboard/objs/Image$Size;

    invoke-virtual {v2}, Lflipboard/objs/Image$Size;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_6

    :cond_d
    const-string v2, "    "

    goto :goto_7

    :cond_e
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lflipboard/objs/Image;->m:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "x"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lflipboard/objs/Image;->n:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_8

    :cond_f
    const-string v2, "    "

    goto :goto_9

    :cond_10
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lflipboard/objs/Image;->o:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "x"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lflipboard/objs/Image;->p:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_a

    :cond_11
    const-string v2, "    "

    goto/16 :goto_b

    :cond_12
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lflipboard/objs/Image;->q:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "x"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lflipboard/objs/Image;->r:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_c

    :cond_13
    move-object v0, v2

    move-object v1, v3

    goto/16 :goto_4

    :cond_14
    move-object v0, v2

    move-object v1, v3

    goto/16 :goto_5

    .line 272
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    const-string v1, "graphic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    const-string v1, "nocrop"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    const-string v1, "graphic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    const-string v1, "tiny"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    const-string v1, "nocrop"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    :cond_0
    const/4 v0, 0x0

    .line 111
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final e()Landroid/graphics/PointF;
    .locals 5

    .prologue
    .line 121
    iget-object v0, p0, Lflipboard/objs/Image;->t:Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lflipboard/objs/Image;->t:Landroid/graphics/PointF;

    .line 139
    :goto_0
    return-object v0

    .line 124
    :cond_0
    iget-object v0, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 125
    const/4 v0, 0x0

    goto :goto_0

    .line 127
    :cond_1
    iget-object v0, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    const-string v1, "focus-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 128
    if-ltz v1, :cond_2

    .line 130
    :try_start_0
    iget-object v0, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    const/16 v2, 0x2d

    add-int/lit8 v3, v1, 0x6

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 131
    iget-object v0, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    const/16 v3, 0x2c

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 132
    iget-object v3, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    add-int/lit8 v1, v1, 0x6

    invoke-virtual {v3, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    .line 133
    iget-object v3, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    add-int/lit8 v2, v2, 0x1

    if-ltz v0, :cond_3

    :goto_1
    invoke-virtual {v3, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    .line 134
    new-instance v2, Landroid/graphics/PointF;

    iget v3, p0, Lflipboard/objs/Image;->f:I

    int-to-float v3, v3

    div-float/2addr v1, v3

    iget v3, p0, Lflipboard/objs/Image;->g:I

    int-to-float v3, v3

    div-float/2addr v0, v3

    invoke-direct {v2, v1, v0}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v2, p0, Lflipboard/objs/Image;->t:Landroid/graphics/PointF;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    :cond_2
    :goto_2
    iget-object v0, p0, Lflipboard/objs/Image;->t:Landroid/graphics/PointF;

    goto :goto_0

    .line 133
    :cond_3
    :try_start_1
    iget-object v0, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_1

    .line 135
    :catch_0
    move-exception v0

    .line 136
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 479
    if-ne p0, p1, :cond_1

    .line 519
    :cond_0
    :goto_0
    return v0

    .line 482
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 483
    goto :goto_0

    .line 486
    :cond_3
    check-cast p1, Lflipboard/objs/Image;

    .line 488
    iget v2, p0, Lflipboard/objs/Image;->g:I

    iget v3, p1, Lflipboard/objs/Image;->g:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 489
    goto :goto_0

    .line 491
    :cond_4
    iget v2, p0, Lflipboard/objs/Image;->f:I

    iget v3, p1, Lflipboard/objs/Image;->f:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 492
    goto :goto_0

    .line 494
    :cond_5
    iget-object v2, p0, Lflipboard/objs/Image;->j:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lflipboard/objs/Image;->j:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/Image;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_6
    move v0, v1

    .line 495
    goto :goto_0

    .line 494
    :cond_7
    iget-object v2, p1, Lflipboard/objs/Image;->j:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 497
    :cond_8
    iget-object v2, p0, Lflipboard/objs/Image;->c:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lflipboard/objs/Image;->c:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/Image;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    move v0, v1

    .line 498
    goto :goto_0

    .line 497
    :cond_a
    iget-object v2, p1, Lflipboard/objs/Image;->c:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 500
    :cond_b
    iget-object v2, p0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/Image;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    :cond_c
    move v0, v1

    .line 501
    goto :goto_0

    .line 500
    :cond_d
    iget-object v2, p1, Lflipboard/objs/Image;->b:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 503
    :cond_e
    iget-object v2, p0, Lflipboard/objs/Image;->i:Ljava/lang/String;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lflipboard/objs/Image;->i:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/Image;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    :cond_f
    move v0, v1

    .line 504
    goto :goto_0

    .line 503
    :cond_10
    iget-object v2, p1, Lflipboard/objs/Image;->i:Ljava/lang/String;

    if-nez v2, :cond_f

    .line 506
    :cond_11
    iget-object v2, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/Image;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    :cond_12
    move v0, v1

    .line 507
    goto :goto_0

    .line 506
    :cond_13
    iget-object v2, p1, Lflipboard/objs/Image;->h:Ljava/lang/String;

    if-nez v2, :cond_12

    .line 509
    :cond_14
    iget-object v2, p0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    if-eqz v2, :cond_16

    iget-object v2, p0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/Image;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    :cond_15
    move v0, v1

    .line 510
    goto/16 :goto_0

    .line 509
    :cond_16
    iget-object v2, p1, Lflipboard/objs/Image;->a:Ljava/lang/String;

    if-nez v2, :cond_15

    .line 512
    :cond_17
    iget-object v2, p0, Lflipboard/objs/Image;->e:Ljava/lang/String;

    if-eqz v2, :cond_19

    iget-object v2, p0, Lflipboard/objs/Image;->e:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/Image;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    :cond_18
    move v0, v1

    .line 513
    goto/16 :goto_0

    .line 512
    :cond_19
    iget-object v2, p1, Lflipboard/objs/Image;->e:Ljava/lang/String;

    if-nez v2, :cond_18

    .line 515
    :cond_1a
    iget-object v2, p0, Lflipboard/objs/Image;->d:Ljava/lang/String;

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lflipboard/objs/Image;->d:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/Image;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 516
    goto/16 :goto_0

    .line 515
    :cond_1b
    iget-object v2, p1, Lflipboard/objs/Image;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lflipboard/objs/Image;->l:Lflipboard/objs/Image$AvailabilityMap;

    invoke-static {v0}, Lflipboard/objs/Image$AvailabilityMap;->b(Lflipboard/objs/Image$AvailabilityMap;)Lflipboard/objs/Image$Size;

    move-result-object v0

    sget-object v1, Lflipboard/objs/Image$Size;->b:Lflipboard/objs/Image$Size;

    invoke-virtual {v0, v1}, Lflipboard/objs/Image$Size;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 216
    const/4 v0, 0x0

    .line 217
    iget-object v1, p0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 218
    iget-object v0, p0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    .line 228
    :cond_0
    :goto_0
    return-object v0

    .line 220
    :cond_1
    iget-object v1, p0, Lflipboard/objs/Image;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 221
    iget-object v0, p0, Lflipboard/objs/Image;->c:Ljava/lang/String;

    goto :goto_0

    .line 223
    :cond_2
    iget-object v1, p0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 224
    iget-object v0, p0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final h()F
    .locals 2

    .prologue
    .line 258
    iget v0, p0, Lflipboard/objs/Image;->f:I

    if-lez v0, :cond_0

    iget v0, p0, Lflipboard/objs/Image;->g:I

    if-lez v0, :cond_0

    .line 259
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lflipboard/objs/Image;->f:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lflipboard/objs/Image;->g:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 261
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 524
    iget-object v0, p0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 525
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 526
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lflipboard/objs/Image;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/objs/Image;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 527
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lflipboard/objs/Image;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/objs/Image;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 528
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lflipboard/objs/Image;->e:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflipboard/objs/Image;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    .line 529
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lflipboard/objs/Image;->f:I

    add-int/2addr v0, v2

    .line 530
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lflipboard/objs/Image;->g:I

    add-int/2addr v0, v2

    .line 531
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    .line 532
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lflipboard/objs/Image;->i:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/objs/Image;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    .line 533
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lflipboard/objs/Image;->j:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lflipboard/objs/Image;->j:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 534
    return v0

    :cond_1
    move v0, v1

    .line 524
    goto :goto_0

    :cond_2
    move v0, v1

    .line 525
    goto :goto_1

    :cond_3
    move v0, v1

    .line 526
    goto :goto_2

    :cond_4
    move v0, v1

    .line 527
    goto :goto_3

    :cond_5
    move v0, v1

    .line 528
    goto :goto_4

    :cond_6
    move v0, v1

    .line 531
    goto :goto_5

    :cond_7
    move v0, v1

    .line 532
    goto :goto_6
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    .line 369
    if-nez v0, :cond_0

    .line 370
    iget-object v0, p0, Lflipboard/objs/Image;->e:Ljava/lang/String;

    .line 372
    :cond_0
    if-nez v0, :cond_1

    .line 373
    iget-object v0, p0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    .line 375
    :cond_1
    if-nez v0, :cond_2

    .line 376
    iget-object v0, p0, Lflipboard/objs/Image;->c:Ljava/lang/String;

    .line 378
    :cond_2
    if-nez v0, :cond_3

    .line 379
    iget-object v0, p0, Lflipboard/objs/Image;->d:Ljava/lang/String;

    .line 381
    :cond_3
    return-object v0
.end method
