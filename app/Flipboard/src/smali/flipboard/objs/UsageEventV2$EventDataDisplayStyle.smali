.class public final enum Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;
.super Ljava/lang/Enum;
.source "UsageEventV2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

.field public static final enum b:Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

.field private static final synthetic c:[Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 209
    new-instance v0, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    const-string v1, "display"

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;->a:Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    new-instance v0, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    const-string v1, "hide"

    invoke-direct {v0, v1, v3}, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;->b:Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;->a:Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;->b:Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    aput-object v1, v0, v3

    sput-object v0, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;->c:[Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 209
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;
    .locals 1

    .prologue
    .line 209
    const-class v0, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    return-object v0
.end method

.method public static values()[Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;
    .locals 1

    .prologue
    .line 209
    sget-object v0, Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;->c:[Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    invoke-virtual {v0}, [Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/objs/UsageEventV2$EventDataDisplayStyle;

    return-object v0
.end method
