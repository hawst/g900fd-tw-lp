.class public Lflipboard/objs/UserState$State;
.super Lflipboard/objs/Base;
.source "UserState.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:J

.field public c:Ljava/lang/String;

.field public d:Lflipboard/objs/UserState$Data;

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 192
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    return-void
.end method

.method public constructor <init>(Lflipboard/objs/UserState$State;)V
    .locals 4

    .prologue
    .line 194
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 195
    new-instance v1, Lflipboard/objs/UserState$Data;

    invoke-direct {v1}, Lflipboard/objs/UserState$Data;-><init>()V

    iput-object v1, p0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    .line 196
    if-eqz p1, :cond_3

    .line 197
    iget-object v0, p1, Lflipboard/objs/UserState$State;->a:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/UserState$State;->a:Ljava/lang/String;

    .line 198
    iget-wide v2, p1, Lflipboard/objs/UserState$State;->b:J

    iput-wide v2, p0, Lflipboard/objs/UserState$State;->b:J

    .line 199
    iget-object v0, p1, Lflipboard/objs/UserState$State;->c:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/UserState$State;->c:Ljava/lang/String;

    .line 200
    iget-object v0, p1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    if-eqz v0, :cond_3

    .line 201
    iget-object v0, p1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 202
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v2, v2, Lflipboard/objs/UserState$Data;->a:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, v1, Lflipboard/objs/UserState$Data;->a:Ljava/util/List;

    .line 204
    :cond_0
    iget-object v0, p1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 205
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v2, v2, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, v1, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    .line 207
    :cond_1
    iget-object v0, p1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 208
    new-instance v0, Ljava/util/HashMap;

    iget-object v2, p1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v2, v2, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, v1, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    .line 210
    :cond_2
    iget-object v0, p1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->f:Lflipboard/json/FLObject;

    iput-object v0, v1, Lflipboard/objs/UserState$Data;->f:Lflipboard/json/FLObject;

    .line 211
    iget-object v0, p1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->h:Ljava/util/List;

    iput-object v0, v1, Lflipboard/objs/UserState$Data;->h:Ljava/util/List;

    .line 212
    iget-object v0, p1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->i:Ljava/lang/String;

    iput-object v0, v1, Lflipboard/objs/UserState$Data;->i:Ljava/lang/String;

    .line 213
    iget-object v0, p1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->j:Lflipboard/json/FLObject;

    if-eqz v0, :cond_3

    .line 214
    iget-object v0, p1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->j:Lflipboard/json/FLObject;

    invoke-virtual {v0}, Lflipboard/json/FLObject;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    iput-object v0, v1, Lflipboard/objs/UserState$Data;->j:Lflipboard/json/FLObject;

    .line 218
    :cond_3
    iget-object v0, v1, Lflipboard/objs/UserState$Data;->a:Ljava/util/List;

    if-nez v0, :cond_4

    .line 219
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v1, Lflipboard/objs/UserState$Data;->a:Ljava/util/List;

    .line 221
    :cond_4
    iget-object v0, v1, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    if-nez v0, :cond_5

    .line 222
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v1, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    .line 224
    :cond_5
    iget-object v0, v1, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    if-nez v0, :cond_6

    .line 225
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, v1, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    .line 227
    :cond_6
    iget-object v0, p0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lflipboard/objs/UserState$Data;->c:Ljava/util/List;

    .line 228
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lflipboard/json/FLObject;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 236
    if-nez p1, :cond_1

    .line 252
    :cond_0
    return-void

    .line 239
    :cond_1
    iget-object v0, p0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->c:Ljava/util/List;

    if-nez v0, :cond_2

    .line 240
    iget-object v0, p0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lflipboard/objs/UserState$Data;->c:Ljava/util/List;

    .line 242
    :cond_2
    const/4 v1, 0x0

    .line 243
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 244
    invoke-static {v0}, Lflipboard/json/JSONSerializer;->c(Ljava/lang/Object;)[B

    move-result-object v3

    .line 245
    if-nez v1, :cond_3

    .line 246
    new-instance v0, Lflipboard/json/JSONParser;

    invoke-direct {v0, v3}, Lflipboard/json/JSONParser;-><init>([B)V

    .line 250
    :goto_1
    iget-object v1, p0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v1, v1, Lflipboard/objs/UserState$Data;->c:Ljava/util/List;

    invoke-virtual {v0}, Lflipboard/json/JSONParser;->x()Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-object v1, v0

    .line 251
    goto :goto_0

    .line 248
    :cond_3
    const/4 v0, 0x0

    array-length v4, v3

    invoke-virtual {v1, v3, v0, v4}, Lflipboard/json/JSONParser;->a([BII)V

    move-object v0, v1

    goto :goto_1
.end method
