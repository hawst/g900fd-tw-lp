.class public final enum Lflipboard/objs/UsageEventV2$EventAction;
.super Ljava/lang/Enum;
.source "UsageEventV2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/objs/UsageEventV2$EventAction;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum B:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum C:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum D:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum E:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum F:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum G:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum H:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum I:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum J:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum K:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum L:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum M:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum N:Lflipboard/objs/UsageEventV2$EventAction;

.field private static final synthetic O:[Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum a:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum b:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum c:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum d:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum e:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum f:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum g:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum h:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum i:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum j:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum k:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum l:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum m:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum n:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum o:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum p:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum q:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum r:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum s:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum t:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum u:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum v:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum w:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum x:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum y:Lflipboard/objs/UsageEventV2$EventAction;

.field public static final enum z:Lflipboard/objs/UsageEventV2$EventAction;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 27
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "display_item"

    invoke-direct {v0, v1, v3}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->a:Lflipboard/objs/UsageEventV2$EventAction;

    .line 28
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "enter"

    invoke-direct {v0, v1, v4}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->b:Lflipboard/objs/UsageEventV2$EventAction;

    .line 29
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "viewed"

    invoke-direct {v0, v1, v5}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->c:Lflipboard/objs/UsageEventV2$EventAction;

    .line 30
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "exit"

    invoke-direct {v0, v1, v6}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->d:Lflipboard/objs/UsageEventV2$EventAction;

    .line 31
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "activated"

    invoke-direct {v0, v1, v7}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->e:Lflipboard/objs/UsageEventV2$EventAction;

    .line 32
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "flip_response_dialog"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->f:Lflipboard/objs/UsageEventV2$EventAction;

    .line 33
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "flip_response_also_flipped_tap"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->g:Lflipboard/objs/UsageEventV2$EventAction;

    .line 34
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "like"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->h:Lflipboard/objs/UsageEventV2$EventAction;

    .line 35
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "unlike"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->i:Lflipboard/objs/UsageEventV2$EventAction;

    .line 36
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "show_default_mag_opts"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->j:Lflipboard/objs/UsageEventV2$EventAction;

    .line 37
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "flip_to_default_mag"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->k:Lflipboard/objs/UsageEventV2$EventAction;

    .line 38
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "subscribe"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->l:Lflipboard/objs/UsageEventV2$EventAction;

    .line 39
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "unsubscribe"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->m:Lflipboard/objs/UsageEventV2$EventAction;

    .line 40
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "show_user_profile"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->n:Lflipboard/objs/UsageEventV2$EventAction;

    .line 41
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "social_card_view"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->o:Lflipboard/objs/UsageEventV2$EventAction;

    .line 42
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "flip"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->p:Lflipboard/objs/UsageEventV2$EventAction;

    .line 43
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "tap_flip"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->q:Lflipboard/objs/UsageEventV2$EventAction;

    .line 44
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "create"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->r:Lflipboard/objs/UsageEventV2$EventAction;

    .line 45
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "add_to_cart"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->s:Lflipboard/objs/UsageEventV2$EventAction;

    .line 46
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "view_cart"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->t:Lflipboard/objs/UsageEventV2$EventAction;

    .line 48
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "schedule_notification"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->u:Lflipboard/objs/UsageEventV2$EventAction;

    .line 50
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "click_notification"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->v:Lflipboard/objs/UsageEventV2$EventAction;

    .line 52
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "system_notification"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->w:Lflipboard/objs/UsageEventV2$EventAction;

    .line 54
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "trigger_notification"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->x:Lflipboard/objs/UsageEventV2$EventAction;

    .line 56
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "receive"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->y:Lflipboard/objs/UsageEventV2$EventAction;

    .line 58
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "navigate"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->z:Lflipboard/objs/UsageEventV2$EventAction;

    .line 60
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "follow"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->A:Lflipboard/objs/UsageEventV2$EventAction;

    .line 61
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "unfollow"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->B:Lflipboard/objs/UsageEventV2$EventAction;

    .line 62
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "hint"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->C:Lflipboard/objs/UsageEventV2$EventAction;

    .line 63
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "hint_dismiss"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->D:Lflipboard/objs/UsageEventV2$EventAction;

    .line 64
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "hint_tap"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->E:Lflipboard/objs/UsageEventV2$EventAction;

    .line 65
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "pagebox_tap"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->F:Lflipboard/objs/UsageEventV2$EventAction;

    .line 66
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "show_find_friends"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->G:Lflipboard/objs/UsageEventV2$EventAction;

    .line 67
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "connect_find_friends"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->H:Lflipboard/objs/UsageEventV2$EventAction;

    .line 68
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "follow_all_find_friends"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->I:Lflipboard/objs/UsageEventV2$EventAction;

    .line 69
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "pagebox_display"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->J:Lflipboard/objs/UsageEventV2$EventAction;

    .line 70
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "exit_signup"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->K:Lflipboard/objs/UsageEventV2$EventAction;

    .line 71
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "enter_signup"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->L:Lflipboard/objs/UsageEventV2$EventAction;

    .line 72
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "exit_signin"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->M:Lflipboard/objs/UsageEventV2$EventAction;

    .line 73
    new-instance v0, Lflipboard/objs/UsageEventV2$EventAction;

    const-string v1, "enter_signin"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->N:Lflipboard/objs/UsageEventV2$EventAction;

    .line 26
    const/16 v0, 0x28

    new-array v0, v0, [Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->a:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->b:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->c:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->d:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->e:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->f:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->g:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->h:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->i:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->j:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->k:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->l:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->m:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->n:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->o:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->p:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->q:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->r:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->s:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->t:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->u:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->v:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->w:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->x:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->y:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->z:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->A:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->B:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->C:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->D:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->E:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->F:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->G:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->H:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->I:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->J:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->K:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->L:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->M:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->N:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/objs/UsageEventV2$EventAction;->O:[Lflipboard/objs/UsageEventV2$EventAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/objs/UsageEventV2$EventAction;
    .locals 1

    .prologue
    .line 26
    const-class v0, Lflipboard/objs/UsageEventV2$EventAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UsageEventV2$EventAction;

    return-object v0
.end method

.method public static values()[Lflipboard/objs/UsageEventV2$EventAction;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lflipboard/objs/UsageEventV2$EventAction;->O:[Lflipboard/objs/UsageEventV2$EventAction;

    invoke-virtual {v0}, [Lflipboard/objs/UsageEventV2$EventAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/objs/UsageEventV2$EventAction;

    return-object v0
.end method
