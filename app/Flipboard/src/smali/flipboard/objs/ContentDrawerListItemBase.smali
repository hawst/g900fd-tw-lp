.class public abstract Lflipboard/objs/ContentDrawerListItemBase;
.super Lflipboard/objs/Base;
.source "ContentDrawerListItemBase.java"

# interfaces
.implements Lflipboard/objs/ContentDrawerListItem;


# instance fields
.field public bP:Ljava/lang/String;

.field public bQ:Ljava/lang/String;

.field public bR:Ljava/lang/String;

.field public bS:Z

.field public transient bT:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation
.end field

.field public transient bU:Z

.field public transient bV:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/objs/ContentDrawerListItemBase;->bV:Z

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 26
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/objs/ContentDrawerListItemBase;->bP:Ljava/lang/String;

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lflipboard/objs/ContentDrawerListItemBase;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Lflipboard/objs/ContentDrawerListItemBase;->bU:Z

    return-void
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lflipboard/objs/ContentDrawerListItemBase;->bR:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/objs/ContentDrawerListItemBase;->bR:Ljava/lang/String;

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lflipboard/objs/ContentDrawerListItemBase;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lflipboard/objs/ContentDrawerListItemBase;->bP:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/objs/ContentDrawerListItemBase;->bP:Ljava/lang/String;

    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lflipboard/objs/ContentDrawerListItemBase;->bQ:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/objs/ContentDrawerListItemBase;->bQ:Ljava/lang/String;

    goto :goto_0
.end method

.method public p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return-object v0
.end method

.method public q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return-object v0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lflipboard/objs/ContentDrawerListItemBase;->bS:Z

    return v0
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lflipboard/objs/ContentDrawerListItemBase;->bU:Z

    return v0
.end method

.method public w()I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method

.method public x()Z
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    return v0
.end method
