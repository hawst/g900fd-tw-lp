.class public final enum Lflipboard/objs/UsageEventV2$SourceData;
.super Ljava/lang/Enum;
.source "UsageEventV2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/objs/UsageEventV2$SourceData;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/objs/UsageEventV2$SourceData;

.field public static final enum b:Lflipboard/objs/UsageEventV2$SourceData;

.field public static final enum c:Lflipboard/objs/UsageEventV2$SourceData;

.field public static final enum d:Lflipboard/objs/UsageEventV2$SourceData;

.field private static final synthetic e:[Lflipboard/objs/UsageEventV2$SourceData;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 206
    new-instance v0, Lflipboard/objs/UsageEventV2$SourceData;

    const-string v1, "id"

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$SourceData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SourceData;->a:Lflipboard/objs/UsageEventV2$SourceData;

    new-instance v0, Lflipboard/objs/UsageEventV2$SourceData;

    const-string v1, "service_id"

    invoke-direct {v0, v1, v3}, Lflipboard/objs/UsageEventV2$SourceData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SourceData;->b:Lflipboard/objs/UsageEventV2$SourceData;

    new-instance v0, Lflipboard/objs/UsageEventV2$SourceData;

    const-string v1, "original_id"

    invoke-direct {v0, v1, v4}, Lflipboard/objs/UsageEventV2$SourceData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SourceData;->c:Lflipboard/objs/UsageEventV2$SourceData;

    new-instance v0, Lflipboard/objs/UsageEventV2$SourceData;

    const-string v1, "original_service_id"

    invoke-direct {v0, v1, v5}, Lflipboard/objs/UsageEventV2$SourceData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SourceData;->d:Lflipboard/objs/UsageEventV2$SourceData;

    const/4 v0, 0x4

    new-array v0, v0, [Lflipboard/objs/UsageEventV2$SourceData;

    sget-object v1, Lflipboard/objs/UsageEventV2$SourceData;->a:Lflipboard/objs/UsageEventV2$SourceData;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/objs/UsageEventV2$SourceData;->b:Lflipboard/objs/UsageEventV2$SourceData;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/objs/UsageEventV2$SourceData;->c:Lflipboard/objs/UsageEventV2$SourceData;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/objs/UsageEventV2$SourceData;->d:Lflipboard/objs/UsageEventV2$SourceData;

    aput-object v1, v0, v5

    sput-object v0, Lflipboard/objs/UsageEventV2$SourceData;->e:[Lflipboard/objs/UsageEventV2$SourceData;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 206
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/objs/UsageEventV2$SourceData;
    .locals 1

    .prologue
    .line 206
    const-class v0, Lflipboard/objs/UsageEventV2$SourceData;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UsageEventV2$SourceData;

    return-object v0
.end method

.method public static values()[Lflipboard/objs/UsageEventV2$SourceData;
    .locals 1

    .prologue
    .line 206
    sget-object v0, Lflipboard/objs/UsageEventV2$SourceData;->e:[Lflipboard/objs/UsageEventV2$SourceData;

    invoke-virtual {v0}, [Lflipboard/objs/UsageEventV2$SourceData;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/objs/UsageEventV2$SourceData;

    return-object v0
.end method
