.class public final enum Lflipboard/objs/SearchResultHeader$HeaderType;
.super Ljava/lang/Enum;
.source "SearchResultHeader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/objs/SearchResultHeader$HeaderType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/objs/SearchResultHeader$HeaderType;

.field public static final enum b:Lflipboard/objs/SearchResultHeader$HeaderType;

.field private static final synthetic c:[Lflipboard/objs/SearchResultHeader$HeaderType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Lflipboard/objs/SearchResultHeader$HeaderType;

    const-string v1, "Top"

    invoke-direct {v0, v1, v2}, Lflipboard/objs/SearchResultHeader$HeaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/SearchResultHeader$HeaderType;->a:Lflipboard/objs/SearchResultHeader$HeaderType;

    new-instance v0, Lflipboard/objs/SearchResultHeader$HeaderType;

    const-string v1, "Sub"

    invoke-direct {v0, v1, v3}, Lflipboard/objs/SearchResultHeader$HeaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/SearchResultHeader$HeaderType;->b:Lflipboard/objs/SearchResultHeader$HeaderType;

    .line 8
    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/objs/SearchResultHeader$HeaderType;

    sget-object v1, Lflipboard/objs/SearchResultHeader$HeaderType;->a:Lflipboard/objs/SearchResultHeader$HeaderType;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/objs/SearchResultHeader$HeaderType;->b:Lflipboard/objs/SearchResultHeader$HeaderType;

    aput-object v1, v0, v3

    sput-object v0, Lflipboard/objs/SearchResultHeader$HeaderType;->c:[Lflipboard/objs/SearchResultHeader$HeaderType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/objs/SearchResultHeader$HeaderType;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lflipboard/objs/SearchResultHeader$HeaderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultHeader$HeaderType;

    return-object v0
.end method

.method public static values()[Lflipboard/objs/SearchResultHeader$HeaderType;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lflipboard/objs/SearchResultHeader$HeaderType;->c:[Lflipboard/objs/SearchResultHeader$HeaderType;

    invoke-virtual {v0}, [Lflipboard/objs/SearchResultHeader$HeaderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/objs/SearchResultHeader$HeaderType;

    return-object v0
.end method
