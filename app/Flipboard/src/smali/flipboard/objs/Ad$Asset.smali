.class public Lflipboard/objs/Ad$Asset;
.super Lflipboard/objs/Base;
.source "Ad.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:I

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Ad$HotSpot;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    return-void
.end method

.method static synthetic a(Lflipboard/objs/Ad$Asset;IIF)Z
    .locals 4

    .prologue
    .line 31
    iget v0, p0, Lflipboard/objs/Ad$Asset;->d:I

    int-to-float v0, v0

    mul-float/2addr v0, p3

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    iget v1, p0, Lflipboard/objs/Ad$Asset;->c:I

    int-to-float v1, v1

    mul-float/2addr v1, p3

    int-to-float v2, p1

    sub-float/2addr v1, v2

    iget v2, p0, Lflipboard/objs/Ad$Asset;->b:I

    int-to-float v2, v2

    mul-float/2addr v2, p3

    int-to-float v3, p2

    sub-float/2addr v2, v3

    cmpg-float v1, v1, v0

    if-gtz v1, :cond_0

    cmpg-float v0, v2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(II)F
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 41
    iget v0, p0, Lflipboard/objs/Ad$Asset;->c:I

    int-to-float v0, v0

    mul-float/2addr v0, v3

    iget v1, p0, Lflipboard/objs/Ad$Asset;->b:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 42
    iget v1, p0, Lflipboard/objs/Ad$Asset;->c:I

    int-to-float v1, v1

    mul-float/2addr v1, v3

    iget v2, p0, Lflipboard/objs/Ad$Asset;->b:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 43
    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 45
    int-to-float v0, p2

    mul-float/2addr v0, v3

    iget v1, p0, Lflipboard/objs/Ad$Asset;->b:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 48
    :goto_0
    return v0

    :cond_0
    int-to-float v0, p1

    mul-float/2addr v0, v3

    iget v1, p0, Lflipboard/objs/Ad$Asset;->c:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public final a(Lflipboard/objs/Ad$HotSpot;FII)Landroid/graphics/RectF;
    .locals 5

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 58
    iget v0, p1, Lflipboard/objs/Ad$HotSpot;->b:I

    int-to-float v0, v0

    iget v1, p0, Lflipboard/objs/Ad$Asset;->c:I

    int-to-float v1, v1

    div-float/2addr v1, v3

    sub-float/2addr v0, v1

    mul-float/2addr v0, p2

    int-to-float v1, p3

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    .line 59
    iget v1, p1, Lflipboard/objs/Ad$HotSpot;->c:I

    int-to-float v1, v1

    iget v2, p0, Lflipboard/objs/Ad$Asset;->b:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    mul-float/2addr v1, p2

    int-to-float v2, p4

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    .line 60
    new-instance v2, Landroid/graphics/RectF;

    iget v3, p1, Lflipboard/objs/Ad$HotSpot;->d:I

    int-to-float v3, v3

    mul-float/2addr v3, p2

    add-float/2addr v3, v0

    iget v4, p1, Lflipboard/objs/Ad$HotSpot;->e:I

    int-to-float v4, v4

    mul-float/2addr v4, p2

    add-float/2addr v4, v1

    invoke-direct {v2, v0, v1, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v2
.end method
