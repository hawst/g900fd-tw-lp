.class public Lflipboard/objs/FeedSection;
.super Lflipboard/objs/HasCommentaryItem;
.source "FeedSection.java"


# instance fields
.field public A:Z

.field public B:Z

.field public C:Z

.field public D:Ljava/lang/String;

.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Z

.field public l:Ljava/lang/String;

.field public m:Lflipboard/objs/Image;

.field public n:Lflipboard/objs/Image;

.field public o:Ljava/lang/String;

.field public p:Lflipboard/objs/Author;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:Lflipboard/objs/Image;

.field public v:Lflipboard/objs/Image;

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/String;

.field public y:Ljava/lang/String;

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lflipboard/objs/HasCommentaryItem;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lflipboard/objs/FeedSection;->m:Lflipboard/objs/Image;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lflipboard/objs/FeedSection;->m:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v0

    .line 47
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lflipboard/objs/CommentaryResult$Item;)V
    .locals 2

    .prologue
    .line 74
    if-eqz p1, :cond_0

    .line 75
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/objs/FeedSection$2;

    invoke-direct {v1, p0, p1}, Lflipboard/objs/FeedSection$2;-><init>(Lflipboard/objs/FeedSection;Lflipboard/objs/CommentaryResult$Item;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 84
    :cond_0
    return-void
.end method

.method public final a(Lflipboard/objs/CommentaryResult$Item;J)V
    .locals 2

    .prologue
    .line 53
    if-eqz p1, :cond_0

    .line 54
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/objs/FeedSection$1;

    invoke-direct {v1, p0, p2, p3, p1}, Lflipboard/objs/FeedSection$1;-><init>(Lflipboard/objs/FeedSection;JLflipboard/objs/CommentaryResult$Item;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 69
    :cond_0
    return-void
.end method
