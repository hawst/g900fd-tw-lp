.class public Lflipboard/objs/SectionListItem;
.super Lflipboard/objs/ContentDrawerListItemSection;
.source "SectionListItem.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SectionListItem;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public g:Z

.field public h:Lflipboard/objs/Image;

.field public i:Z

.field public j:Lflipboard/objs/ConfigBrick;

.field public k:Lflipboard/objs/Author;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lflipboard/objs/ContentDrawerListItemSection;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 46
    const/16 v0, 0x8

    return v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lflipboard/objs/SectionListItem;->bT:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/SectionListItem;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lflipboard/objs/SectionListItem;->e:Ljava/util/List;

    invoke-static {v0}, Lflipboard/objs/SectionListResult;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/objs/SectionListItem;->bT:Ljava/util/List;

    .line 34
    :cond_0
    iget-object v0, p0, Lflipboard/objs/SectionListItem;->bT:Ljava/util/List;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Lflipboard/objs/ContentDrawerListItemSection;->k()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/SectionListItem;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lflipboard/objs/SectionListItem;->f:Z

    return v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lflipboard/objs/SectionListItem;->g:Z

    return v0
.end method
