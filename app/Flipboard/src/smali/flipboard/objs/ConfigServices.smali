.class public Lflipboard/objs/ConfigServices;
.super Lflipboard/objs/ContentDrawerListItemBase;
.source "ConfigServices.java"


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigService;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lflipboard/objs/ContentDrawerListItemBase;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x6

    return v0
.end method

.method public final c()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17
    iget-object v0, p0, Lflipboard/objs/ConfigServices;->bT:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 18
    iget-object v0, p0, Lflipboard/objs/ConfigServices;->bT:Ljava/util/List;

    .line 31
    :goto_0
    return-object v0

    .line 21
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/objs/ConfigServices;->bT:Ljava/util/List;

    .line 23
    iget-object v0, p0, Lflipboard/objs/ConfigServices;->b:Ljava/util/List;

    if-nez v0, :cond_1

    .line 24
    iget-object v0, p0, Lflipboard/objs/ConfigServices;->bT:Ljava/util/List;

    goto :goto_0

    .line 27
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/objs/ConfigServices;->bT:Ljava/util/List;

    .line 28
    iget-object v0, p0, Lflipboard/objs/ConfigServices;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 29
    iget-object v2, p0, Lflipboard/objs/ConfigServices;->bT:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 31
    :cond_2
    iget-object v0, p0, Lflipboard/objs/ConfigServices;->bT:Ljava/util/List;

    goto :goto_0
.end method
