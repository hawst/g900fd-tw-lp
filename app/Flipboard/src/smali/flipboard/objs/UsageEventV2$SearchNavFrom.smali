.class public final enum Lflipboard/objs/UsageEventV2$SearchNavFrom;
.super Ljava/lang/Enum;
.source "UsageEventV2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/objs/UsageEventV2$SearchNavFrom;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/objs/UsageEventV2$SearchNavFrom;

.field public static final enum b:Lflipboard/objs/UsageEventV2$SearchNavFrom;

.field private static final synthetic c:[Lflipboard/objs/UsageEventV2$SearchNavFrom;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 138
    new-instance v0, Lflipboard/objs/UsageEventV2$SearchNavFrom;

    const-string v1, "topic_picker"

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$SearchNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SearchNavFrom;->a:Lflipboard/objs/UsageEventV2$SearchNavFrom;

    new-instance v0, Lflipboard/objs/UsageEventV2$SearchNavFrom;

    const-string v1, "main_search"

    invoke-direct {v0, v1, v3}, Lflipboard/objs/UsageEventV2$SearchNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$SearchNavFrom;->b:Lflipboard/objs/UsageEventV2$SearchNavFrom;

    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/objs/UsageEventV2$SearchNavFrom;

    sget-object v1, Lflipboard/objs/UsageEventV2$SearchNavFrom;->a:Lflipboard/objs/UsageEventV2$SearchNavFrom;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/objs/UsageEventV2$SearchNavFrom;->b:Lflipboard/objs/UsageEventV2$SearchNavFrom;

    aput-object v1, v0, v3

    sput-object v0, Lflipboard/objs/UsageEventV2$SearchNavFrom;->c:[Lflipboard/objs/UsageEventV2$SearchNavFrom;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 138
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/objs/UsageEventV2$SearchNavFrom;
    .locals 1

    .prologue
    .line 138
    const-class v0, Lflipboard/objs/UsageEventV2$SearchNavFrom;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UsageEventV2$SearchNavFrom;

    return-object v0
.end method

.method public static values()[Lflipboard/objs/UsageEventV2$SearchNavFrom;
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lflipboard/objs/UsageEventV2$SearchNavFrom;->c:[Lflipboard/objs/UsageEventV2$SearchNavFrom;

    invoke-virtual {v0}, [Lflipboard/objs/UsageEventV2$SearchNavFrom;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/objs/UsageEventV2$SearchNavFrom;

    return-object v0
.end method
