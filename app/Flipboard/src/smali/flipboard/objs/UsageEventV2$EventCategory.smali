.class public final enum Lflipboard/objs/UsageEventV2$EventCategory;
.super Ljava/lang/Enum;
.source "UsageEventV2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/objs/UsageEventV2$EventCategory;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/objs/UsageEventV2$EventCategory;

.field public static final enum b:Lflipboard/objs/UsageEventV2$EventCategory;

.field public static final enum c:Lflipboard/objs/UsageEventV2$EventCategory;

.field public static final enum d:Lflipboard/objs/UsageEventV2$EventCategory;

.field public static final enum e:Lflipboard/objs/UsageEventV2$EventCategory;

.field public static final enum f:Lflipboard/objs/UsageEventV2$EventCategory;

.field public static final enum g:Lflipboard/objs/UsageEventV2$EventCategory;

.field public static final enum h:Lflipboard/objs/UsageEventV2$EventCategory;

.field public static final enum i:Lflipboard/objs/UsageEventV2$EventCategory;

.field public static final enum j:Lflipboard/objs/UsageEventV2$EventCategory;

.field private static final synthetic k:[Lflipboard/objs/UsageEventV2$EventCategory;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 76
    new-instance v0, Lflipboard/objs/UsageEventV2$EventCategory;

    const-string v1, "section"

    invoke-direct {v0, v1, v3}, Lflipboard/objs/UsageEventV2$EventCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->a:Lflipboard/objs/UsageEventV2$EventCategory;

    new-instance v0, Lflipboard/objs/UsageEventV2$EventCategory;

    const-string v1, "firstlaunch"

    invoke-direct {v0, v1, v4}, Lflipboard/objs/UsageEventV2$EventCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    new-instance v0, Lflipboard/objs/UsageEventV2$EventCategory;

    const-string v1, "app"

    invoke-direct {v0, v1, v5}, Lflipboard/objs/UsageEventV2$EventCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->c:Lflipboard/objs/UsageEventV2$EventCategory;

    new-instance v0, Lflipboard/objs/UsageEventV2$EventCategory;

    const-string v1, "alert"

    invoke-direct {v0, v1, v6}, Lflipboard/objs/UsageEventV2$EventCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->d:Lflipboard/objs/UsageEventV2$EventCategory;

    new-instance v0, Lflipboard/objs/UsageEventV2$EventCategory;

    const-string v1, "magazine"

    invoke-direct {v0, v1, v7}, Lflipboard/objs/UsageEventV2$EventCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->e:Lflipboard/objs/UsageEventV2$EventCategory;

    new-instance v0, Lflipboard/objs/UsageEventV2$EventCategory;

    const-string v1, "item"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->f:Lflipboard/objs/UsageEventV2$EventCategory;

    new-instance v0, Lflipboard/objs/UsageEventV2$EventCategory;

    const-string v1, "push_message"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->g:Lflipboard/objs/UsageEventV2$EventCategory;

    new-instance v0, Lflipboard/objs/UsageEventV2$EventCategory;

    const-string v1, "general"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->h:Lflipboard/objs/UsageEventV2$EventCategory;

    new-instance v0, Lflipboard/objs/UsageEventV2$EventCategory;

    const-string v1, "search"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->i:Lflipboard/objs/UsageEventV2$EventCategory;

    new-instance v0, Lflipboard/objs/UsageEventV2$EventCategory;

    const-string v1, "social"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$EventCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->j:Lflipboard/objs/UsageEventV2$EventCategory;

    const/16 v0, 0xa

    new-array v0, v0, [Lflipboard/objs/UsageEventV2$EventCategory;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventCategory;->a:Lflipboard/objs/UsageEventV2$EventCategory;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/objs/UsageEventV2$EventCategory;->c:Lflipboard/objs/UsageEventV2$EventCategory;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/objs/UsageEventV2$EventCategory;->d:Lflipboard/objs/UsageEventV2$EventCategory;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/objs/UsageEventV2$EventCategory;->e:Lflipboard/objs/UsageEventV2$EventCategory;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->f:Lflipboard/objs/UsageEventV2$EventCategory;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->g:Lflipboard/objs/UsageEventV2$EventCategory;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->h:Lflipboard/objs/UsageEventV2$EventCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->i:Lflipboard/objs/UsageEventV2$EventCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->j:Lflipboard/objs/UsageEventV2$EventCategory;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->k:[Lflipboard/objs/UsageEventV2$EventCategory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/objs/UsageEventV2$EventCategory;
    .locals 1

    .prologue
    .line 76
    const-class v0, Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UsageEventV2$EventCategory;

    return-object v0
.end method

.method public static values()[Lflipboard/objs/UsageEventV2$EventCategory;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->k:[Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-virtual {v0}, [Lflipboard/objs/UsageEventV2$EventCategory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/objs/UsageEventV2$EventCategory;

    return-object v0
.end method
