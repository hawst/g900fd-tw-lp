.class public final enum Lflipboard/objs/UsageEventV2$CommonEventData;
.super Ljava/lang/Enum;
.source "UsageEventV2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/objs/UsageEventV2$CommonEventData;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum B:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum C:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum D:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum E:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum F:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum G:Lflipboard/objs/UsageEventV2$CommonEventData;

.field private static final synthetic H:[Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum a:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum b:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum c:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum d:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum e:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum f:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum g:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum h:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum i:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum j:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum k:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum l:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum m:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum n:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum o:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum p:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum q:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum r:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum s:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum t:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum u:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum v:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum w:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum x:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum y:Lflipboard/objs/UsageEventV2$CommonEventData;

.field public static final enum z:Lflipboard/objs/UsageEventV2$CommonEventData;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 172
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "section_id"

    invoke-direct {v0, v1, v3}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 173
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "item_id"

    invoke-direct {v0, v1, v4}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->b:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 174
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "item_type"

    invoke-direct {v0, v1, v5}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->c:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 175
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "magazine_list"

    invoke-direct {v0, v1, v6}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->d:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 176
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "magazine_id"

    invoke-direct {v0, v1, v7}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->e:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 177
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "magazine_type"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->f:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 178
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "magazine_name"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->g:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 179
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "magazine_category"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->h:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 180
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "sponsored_campaign"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->i:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 181
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "item_sponsored_campaign"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->j:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 182
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "partner_id"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->k:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 183
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "item_partner_id"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->l:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 184
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "partner_paywall_status"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->m:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 185
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "partner_paywall_access_level"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->n:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 186
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "target_id"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->o:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 187
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "url"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->p:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 188
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "source"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->q:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 189
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "type"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 190
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "display_style"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->s:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 191
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "item_density_override"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->t:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 192
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "method"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->u:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 193
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "page_num"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->v:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 194
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "number_items"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->w:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 195
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "tap_count"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->x:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 196
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "flip_count"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->y:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 197
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "time_spent"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->z:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 198
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "layout_time_spent"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->A:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 199
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "success"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->B:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 200
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "nav_from"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 201
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "server_properties"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->D:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 202
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "view_count"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->E:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 203
    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "num_items"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->F:Lflipboard/objs/UsageEventV2$CommonEventData;

    new-instance v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "search_term"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->G:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 171
    const/16 v0, 0x21

    new-array v0, v0, [Lflipboard/objs/UsageEventV2$CommonEventData;

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->b:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->c:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->d:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->e:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->f:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->g:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->h:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->i:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->j:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->k:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->l:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->m:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->n:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->o:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->p:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->q:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->s:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->t:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->u:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->v:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->w:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->x:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->y:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->z:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->A:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->B:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->D:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->E:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->F:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->G:Lflipboard/objs/UsageEventV2$CommonEventData;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->H:[Lflipboard/objs/UsageEventV2$CommonEventData;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 171
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/objs/UsageEventV2$CommonEventData;
    .locals 1

    .prologue
    .line 171
    const-class v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UsageEventV2$CommonEventData;

    return-object v0
.end method

.method public static values()[Lflipboard/objs/UsageEventV2$CommonEventData;
    .locals 1

    .prologue
    .line 171
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->H:[Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v0}, [Lflipboard/objs/UsageEventV2$CommonEventData;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/objs/UsageEventV2$CommonEventData;

    return-object v0
.end method
