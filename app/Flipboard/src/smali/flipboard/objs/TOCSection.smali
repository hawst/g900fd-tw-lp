.class public Lflipboard/objs/TOCSection;
.super Lflipboard/objs/Base;
.source "TOCSection.java"


# instance fields
.field public A:I

.field public B:I

.field public C:J

.field public D:Z

.field public E:Z

.field public F:Ljava/lang/String;

.field public G:Lflipboard/json/FLObject;

.field public H:Z

.field public I:J

.field public J:Ljava/lang/String;

.field public K:Ljava/lang/String;

.field public L:Ljava/lang/String;

.field public M:Ljava/lang/String;

.field public N:Z

.field public O:Ljava/lang/String;

.field public P:Z

.field public a:Lflipboard/objs/Image;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Lflipboard/objs/Image;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:F

.field public y:F

.field public z:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 62
    const-string v0, "public"

    iput-object v0, p0, Lflipboard/objs/TOCSection;->L:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lflipboard/objs/TOCSection;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lflipboard/objs/TOCSection;->h:Ljava/lang/String;

    .line 86
    :goto_0
    return-object v0

    .line 83
    :cond_0
    iget-object v0, p0, Lflipboard/objs/TOCSection;->i:Lflipboard/objs/Image;

    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p0, Lflipboard/objs/TOCSection;->i:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 86
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lflipboard/objs/TOCSection;->i:Lflipboard/objs/Image;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Lflipboard/objs/Image;

    invoke-direct {v0}, Lflipboard/objs/Image;-><init>()V

    iput-object v0, p0, Lflipboard/objs/TOCSection;->i:Lflipboard/objs/Image;

    .line 75
    :cond_0
    iget-object v0, p0, Lflipboard/objs/TOCSection;->i:Lflipboard/objs/Image;

    iput-object p1, v0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    .line 76
    return-void
.end method
