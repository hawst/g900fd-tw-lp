.class public Lflipboard/objs/Magazine;
.super Lflipboard/objs/Base;
.source "Magazine.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Z

.field public g:J

.field public h:J

.field public i:Lflipboard/objs/Author;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Z

.field public n:Z

.field public o:I

.field public p:Lflipboard/objs/Link;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Lflipboard/objs/Image;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILflipboard/objs/Link;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 42
    iput-object p1, p0, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    .line 44
    iput p3, p0, Lflipboard/objs/Magazine;->o:I

    .line 45
    iput-object p4, p0, Lflipboard/objs/Magazine;->p:Lflipboard/objs/Link;

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/objs/Magazine;->n:Z

    .line 47
    return-void
.end method

.method public static a(Lflipboard/service/DatabaseHandler;)Lflipboard/objs/Magazine;
    .locals 4

    .prologue
    .line 51
    const-string v0, "descriptor"

    invoke-virtual {p0, v0}, Lflipboard/service/DatabaseHandler;->d(Ljava/lang/String;)[B

    move-result-object v1

    .line 52
    const/4 v0, 0x0

    .line 53
    if-eqz v1, :cond_0

    .line 55
    :try_start_0
    new-instance v2, Lflipboard/json/JSONParser;

    invoke-direct {v2, v1}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v2}, Lflipboard/json/JSONParser;->p()Lflipboard/objs/Magazine;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 62
    :cond_0
    :goto_0
    return-object v0

    .line 58
    :catch_0
    move-exception v1

    sget-object v1, Lflipboard/util/Log$Level;->e:Lflipboard/util/Log$Level;

    invoke-virtual {v1}, Lflipboard/util/Log$Level;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v1

    const-string v2, "Unable to generate magazine from database handler"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    const-string v1, "public"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 73
    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-static {p0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Magazine;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :goto_0
    return-object v0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 76
    const/4 v0, 0x0

    goto :goto_0
.end method
