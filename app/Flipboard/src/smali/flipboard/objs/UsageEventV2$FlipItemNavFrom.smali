.class public final enum Lflipboard/objs/UsageEventV2$FlipItemNavFrom;
.super Ljava/lang/Enum;
.source "UsageEventV2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/objs/UsageEventV2$FlipItemNavFrom;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

.field public static final enum b:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

.field public static final enum c:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

.field public static final enum d:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

.field public static final enum e:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

.field public static final enum f:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

.field public static final enum g:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

.field private static final synthetic h:[Lflipboard/objs/UsageEventV2$FlipItemNavFrom;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 135
    new-instance v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    const-string v1, "layout"

    invoke-direct {v0, v1, v3}, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->a:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    new-instance v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    const-string v1, "detail"

    invoke-direct {v0, v1, v4}, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->b:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    new-instance v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    const-string v1, "widget"

    invoke-direct {v0, v1, v5}, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->c:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    new-instance v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    const-string v1, "other_app"

    invoke-direct {v0, v1, v6}, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->d:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    new-instance v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    const-string v1, "link_mode"

    invoke-direct {v0, v1, v7}, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->e:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    new-instance v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    const-string v1, "social_card"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->f:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    new-instance v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    const-string v1, "cover_page"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->g:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    const/4 v0, 0x7

    new-array v0, v0, [Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    sget-object v1, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->a:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->b:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->c:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->d:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->e:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->f:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->g:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->h:[Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 135
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/objs/UsageEventV2$FlipItemNavFrom;
    .locals 1

    .prologue
    .line 135
    const-class v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    return-object v0
.end method

.method public static values()[Lflipboard/objs/UsageEventV2$FlipItemNavFrom;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->h:[Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    invoke-virtual {v0}, [Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    return-object v0
.end method
