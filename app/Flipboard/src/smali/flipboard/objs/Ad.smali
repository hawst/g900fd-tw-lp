.class public Lflipboard/objs/Ad;
.super Lflipboard/objs/Base;
.source "Ad.java"


# static fields
.field public static final a:Lflipboard/objs/Ad$Asset;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Ad$Asset;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lflipboard/objs/Ad$ButtonInfo;

.field public g:Ljava/lang/String;

.field public h:I

.field public transient i:I

.field public transient j:J

.field public k:Lflipboard/objs/Ad$VideoInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lflipboard/objs/Ad$Asset;

    invoke-direct {v0}, Lflipboard/objs/Ad$Asset;-><init>()V

    sput-object v0, Lflipboard/objs/Ad;->a:Lflipboard/objs/Ad$Asset;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/objs/Ad;->h:I

    .line 117
    return-void
.end method


# virtual methods
.method public final a(IIZ)Lflipboard/objs/Ad$Asset;
    .locals 10

    .prologue
    .line 190
    const/4 v1, 0x0

    .line 191
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 192
    const/4 v3, 0x0

    .line 193
    const v2, 0x7f7fffff    # Float.MAX_VALUE

    .line 194
    iget-object v4, p0, Lflipboard/objs/Ad;->e:Ljava/util/List;

    if-eqz v4, :cond_6

    if-lez p1, :cond_6

    if-lez p2, :cond_6

    .line 195
    iget-object v4, p0, Lflipboard/objs/Ad;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v0

    move-object v5, v1

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Ad$Asset;

    .line 197
    const/high16 v1, 0x3f800000    # 1.0f

    iget v7, v0, Lflipboard/objs/Ad$Asset;->c:I

    int-to-float v7, v7

    mul-float/2addr v1, v7

    iget v7, v0, Lflipboard/objs/Ad$Asset;->b:I

    int-to-float v7, v7

    div-float/2addr v1, v7

    .line 198
    const/high16 v7, 0x3f800000    # 1.0f

    int-to-float v8, p1

    mul-float/2addr v7, v8

    int-to-float v8, p2

    div-float/2addr v7, v8

    .line 200
    cmpl-float v1, v1, v7

    if-lez v1, :cond_1

    int-to-float v1, p2

    iget v7, v0, Lflipboard/objs/Ad$Asset;->b:I

    int-to-float v7, v7

    div-float/2addr v1, v7

    .line 202
    :goto_1
    cmpg-float v7, v1, v4

    if-gez v7, :cond_0

    invoke-static {v0, p1, p2, v1}, Lflipboard/objs/Ad$Asset;->a(Lflipboard/objs/Ad$Asset;IIF)Z

    move-result v7

    if-eqz v7, :cond_0

    move v4, v1

    move-object v5, v0

    .line 207
    :cond_0
    cmpg-float v7, v1, v2

    if-gez v7, :cond_5

    move v9, v1

    move-object v1, v0

    move v0, v9

    :goto_2
    move v2, v0

    move-object v3, v1

    .line 211
    goto :goto_0

    .line 200
    :cond_1
    int-to-float v1, p1

    iget v7, v0, Lflipboard/objs/Ad$Asset;->c:I

    int-to-float v7, v7

    div-float/2addr v1, v7

    goto :goto_1

    :cond_2
    move-object v1, v3

    move-object v0, v5

    .line 213
    :goto_3
    if-eqz v0, :cond_3

    .line 220
    :goto_4
    return-object v0

    .line 215
    :cond_3
    if-eqz v1, :cond_4

    if-eqz p3, :cond_4

    .line 216
    sget-object v0, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x2

    aput-object p0, v0, v2

    move-object v0, v1

    .line 217
    goto :goto_4

    .line 219
    :cond_4
    sget-object v0, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p0, v0, v1

    .line 220
    const/4 v0, 0x0

    goto :goto_4

    :cond_5
    move v0, v2

    move-object v1, v3

    goto :goto_2

    :cond_6
    move-object v0, v1

    move-object v1, v3

    goto :goto_3
.end method

.method public final a(J)Z
    .locals 5

    .prologue
    .line 242
    iget-wide v0, p0, Lflipboard/objs/Ad;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lflipboard/objs/Ad;->j:J

    sub-long v0, p1, v0

    const-wide/32 v2, 0x927c0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
