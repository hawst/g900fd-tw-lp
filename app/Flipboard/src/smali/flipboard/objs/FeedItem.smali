.class public Lflipboard/objs/FeedItem;
.super Lflipboard/objs/HasCommentaryItem;
.source "FeedItem.java"

# interfaces
.implements Lflipboard/objs/ContentDrawerListItem;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final synthetic cs:Z


# instance fields
.field public A:Ljava/lang/String;

.field public B:Ljava/lang/String;

.field public C:Ljava/lang/String;

.field public D:Ljava/lang/String;

.field public E:F

.field public F:Ljava/lang/String;

.field public G:Ljava/lang/String;

.field public H:Ljava/lang/String;

.field public I:Lflipboard/objs/Image;

.field public J:Lflipboard/objs/Image;

.field public K:Lflipboard/objs/Image;

.field public L:Lflipboard/objs/Image;

.field public M:Lflipboard/objs/Image;

.field public N:Lflipboard/objs/Image;

.field public O:Lflipboard/objs/Image;

.field public P:Ljava/lang/String;

.field public Q:Ljava/lang/String;

.field public R:Ljava/lang/String;

.field public S:Z

.field public T:Ljava/lang/String;

.field public U:Ljava/lang/String;

.field public V:Ljava/lang/String;

.field public W:Ljava/lang/String;

.field public X:Lflipboard/objs/Image;

.field public Y:Ljava/lang/String;

.field public Z:J

.field public a:Ljava/lang/String;

.field public aA:Z

.field public aB:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public aC:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field public aD:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field public aE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field public aF:Lflipboard/objs/FeedArticle;

.field public aG:Ljava/lang/String;

.field public aH:Ljava/lang/String;

.field public aI:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedSectionLink;",
            ">;"
        }
    .end annotation
.end field

.field public aJ:Lflipboard/objs/FeedItem;

.field public aK:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public aL:Z

.field public aM:Z

.field public aN:Ljava/lang/String;

.field public aO:I

.field public aP:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field public aQ:Z

.field public aR:Ljava/lang/String;

.field public aS:Lflipboard/objs/FeedItem$Location;

.field public aT:Ljava/lang/String;

.field public aU:Lflipboard/json/FLObject;

.field public aV:I

.field public aW:I

.field public aX:Ljava/lang/String;

.field public aY:Ljava/lang/String;

.field public aZ:Ljava/lang/String;

.field public aa:J

.field public ab:Z

.field public ac:Ljava/lang/String;

.field public ad:Z

.field public ae:Z

.field public af:Z

.field public ag:Z

.field public ah:Z

.field public ai:Z

.field public aj:Z

.field public ak:I

.field public al:Ljava/lang/String;

.field public am:Z

.field public an:Z

.field public ao:Ljava/lang/String;

.field public ap:Ljava/lang/String;

.field public aq:Ljava/lang/String;

.field public ar:Ljava/lang/String;

.field public as:Ljava/lang/String;

.field public at:Ljava/lang/String;

.field public au:Ljava/lang/String;

.field public av:Ljava/lang/String;

.field public aw:I

.field public ax:Z

.field public ay:Z

.field public az:Z

.field public b:Ljava/lang/String;

.field public bA:J

.field public bB:Ljava/lang/String;

.field public bC:Z

.field public bD:Ljava/lang/String;

.field public bE:Ljava/lang/String;

.field public bF:Lflipboard/objs/FeedItem;

.field public bG:Z

.field public bH:Lflipboard/objs/FeedItem;

.field public bI:Ljava/lang/String;

.field public bJ:Ljava/lang/String;

.field public bK:Ljava/lang/String;

.field public bL:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SidebarGroup;",
            ">;"
        }
    .end annotation
.end field

.field public bM:Lflipboard/objs/FeedSectionLink;

.field public bN:Z

.field public bO:Ljava/lang/String;

.field public bP:Ljava/lang/String;

.field public bQ:Ljava/lang/String;

.field public bR:Ljava/lang/String;

.field public bS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public bT:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public bU:Ljava/lang/String;

.field public transient bV:Ljava/lang/String;

.field public bW:Z

.field public bX:Ljava/lang/String;

.field public bY:Ljava/lang/String;

.field public bZ:Ljava/lang/String;

.field public ba:Ljava/lang/String;

.field public bb:Z

.field public bc:Ljava/lang/String;

.field public bd:Ljava/lang/String;

.field public be:Z

.field public bf:Lflipboard/objs/FeedItem$Note;

.field public bg:Ljava/lang/String;

.field public bh:Ljava/lang/String;

.field public bi:Lflipboard/objs/UserServices;

.field public bj:Ljava/lang/String;

.field public bk:Z

.field public bl:Z

.field public bm:Z

.field public bn:Z

.field public bo:Ljava/lang/String;

.field public bp:Ljava/lang/String;

.field public bq:Ljava/lang/String;

.field public br:Ljava/lang/String;

.field public bs:Ljava/lang/String;

.field public bt:Ljava/lang/String;

.field public bu:Ljava/lang/String;

.field public bv:Ljava/lang/String;

.field public bw:Ljava/lang/String;

.field public bx:Ljava/lang/String;

.field public by:Ljava/lang/String;

.field public bz:F

.field public c:Ljava/lang/String;

.field public ca:I

.field public cb:Ljava/lang/String;

.field public cc:Z

.field public cd:Z

.field public ce:Ljava/lang/String;

.field public cf:Ljava/lang/String;

.field public cg:Ljava/lang/String;

.field public ch:Lflipboard/objs/FeedItem;

.field public ci:Ljava/lang/String;

.field public cj:Z

.field public ck:Z

.field public cl:Z

.field public cm:Ljava/lang/String;

.field public cn:Lflipboard/json/FLObject;

.field public transient co:Lflipboard/objs/FeedItem;

.field public cp:Lflipboard/objs/FeedItemRenderHints;

.field public cq:Ljava/lang/String;

.field public cr:Z

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Lflipboard/objs/FeedSection;

.field public k:Lflipboard/objs/Invite;

.field public l:Ljava/lang/String;

.field public m:Z

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:I

.field public s:I

.field public t:I

.field public u:J

.field public v:I

.field public w:I

.field public x:Ljava/lang/String;

.field public y:Ljava/lang/String;

.field public z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lflipboard/objs/FeedItem;->cs:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 268
    invoke-direct {p0}, Lflipboard/objs/HasCommentaryItem;-><init>()V

    .line 76
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lflipboard/objs/FeedItem;->E:F

    .line 110
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->ad:Z

    .line 111
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->ae:Z

    .line 113
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->ag:Z

    .line 114
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->ah:Z

    .line 115
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->ai:Z

    .line 116
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->aj:Z

    .line 145
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->aM:Z

    .line 188
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->bC:Z

    .line 193
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/objs/FeedItem;->bG:Z

    .line 238
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->cl:Z

    .line 270
    return-void
.end method

.method public constructor <init>(Lflipboard/objs/FeedSectionLink;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 273
    invoke-direct {p0}, Lflipboard/objs/HasCommentaryItem;-><init>()V

    .line 76
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lflipboard/objs/FeedItem;->E:F

    .line 110
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->ad:Z

    .line 111
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->ae:Z

    .line 113
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->ag:Z

    .line 114
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->ah:Z

    .line 115
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->ai:Z

    .line 116
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->aj:Z

    .line 145
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->aM:Z

    .line 188
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->bC:Z

    .line 193
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/objs/FeedItem;->bG:Z

    .line 238
    iput-boolean v1, p0, Lflipboard/objs/FeedItem;->cl:Z

    .line 274
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->b:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    .line 275
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    .line 276
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->k:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    .line 277
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    .line 278
    iget-object v0, p0, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-nez v0, :cond_0

    .line 279
    new-instance v0, Lflipboard/objs/Image;

    invoke-direct {v0}, Lflipboard/objs/Image;-><init>()V

    iput-object v0, p0, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    .line 281
    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->f:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    .line 282
    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 1

    .prologue
    .line 591
    iget-object v0, p0, Lflipboard/objs/FeedItem;->av:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->av:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 592
    iget-object v0, p0, Lflipboard/objs/FeedItem;->av:Ljava/lang/String;

    .line 595
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    goto :goto_0
.end method

.method public final B()Lflipboard/objs/FeedSectionLink;
    .locals 4

    .prologue
    .line 600
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 601
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 602
    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    const-string v3, "author"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 613
    :goto_0
    return-object v0

    .line 606
    :cond_1
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eq v0, p0, :cond_3

    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 607
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 608
    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    const-string v3, "author"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 613
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final C()Lflipboard/objs/FeedSectionLink;
    .locals 4

    .prologue
    .line 618
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 619
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 620
    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    const-string v3, "link"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 625
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final D()Ljava/lang/String;
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Lflipboard/objs/FeedItem;->bc:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->z:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 631
    iget-object v0, p0, Lflipboard/objs/FeedItem;->z:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/HttpUtil;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/objs/FeedItem;->bc:Ljava/lang/String;

    .line 633
    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->bc:Ljava/lang/String;

    return-object v0
.end method

.method public final E()Lflipboard/objs/FeedItem;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 638
    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "magazine"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "sectionCover"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "status"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 639
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 641
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method public final F()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 646
    :goto_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aJ:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 647
    iget-object p0, p0, Lflipboard/objs/FeedItem;->aJ:Lflipboard/objs/FeedItem;

    goto :goto_0

    .line 649
    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_1

    .line 650
    iget-object p0, p0, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    goto :goto_0

    .line 652
    :cond_1
    return-object p0
.end method

.method public final G()Ljava/lang/String;
    .locals 2

    .prologue
    .line 657
    iget-object v0, p0, Lflipboard/objs/FeedItem;->bc:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 658
    iget-object v0, p0, Lflipboard/objs/FeedItem;->bc:Ljava/lang/String;

    .line 665
    :goto_0
    return-object v0

    .line 660
    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->z:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 661
    iget-object v0, p0, Lflipboard/objs/FeedItem;->z:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/HttpUtil;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 662
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lflipboard/objs/FeedItem;->bc:Ljava/lang/String;

    goto :goto_0

    .line 665
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final H()Ljava/lang/String;
    .locals 1

    .prologue
    .line 670
    iget-object v0, p0, Lflipboard/objs/FeedItem;->H:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->F:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Lflipboard/objs/FeedItem;->F:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/HttpUtil;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/objs/FeedItem;->H:Ljava/lang/String;

    .line 673
    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->H:Ljava/lang/String;

    return-object v0
.end method

.method public final I()Ljava/lang/String;
    .locals 1

    .prologue
    .line 678
    iget-object v0, p0, Lflipboard/objs/FeedItem;->bV:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 679
    iget-object v0, p0, Lflipboard/objs/FeedItem;->bV:Ljava/lang/String;

    .line 685
    :goto_0
    return-object v0

    .line 681
    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 682
    iget-object v0, p0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/HttpUtil;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/objs/FeedItem;->bV:Ljava/lang/String;

    .line 683
    iget-object v0, p0, Lflipboard/objs/FeedItem;->bV:Ljava/lang/String;

    goto :goto_0

    .line 685
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final J()Lflipboard/objs/Image;
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Lflipboard/objs/FeedItem;->N:Lflipboard/objs/Image;

    if-eqz v0, :cond_0

    .line 803
    iget-object v0, p0, Lflipboard/objs/FeedItem;->N:Lflipboard/objs/Image;

    .line 813
    :goto_0
    return-object v0

    .line 804
    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->v:Lflipboard/objs/Image;

    if-eqz v0, :cond_1

    .line 805
    iget-object v0, p0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->v:Lflipboard/objs/Image;

    goto :goto_0

    .line 806
    :cond_1
    iget-object v0, p0, Lflipboard/objs/FeedItem;->L:Lflipboard/objs/Image;

    if-eqz v0, :cond_2

    .line 807
    iget-object v0, p0, Lflipboard/objs/FeedItem;->L:Lflipboard/objs/Image;

    goto :goto_0

    .line 808
    :cond_2
    iget-object v0, p0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->m:Lflipboard/objs/Image;

    if-eqz v0, :cond_3

    .line 809
    iget-object v0, p0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->m:Lflipboard/objs/Image;

    goto :goto_0

    .line 811
    :cond_3
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    goto :goto_0
.end method

.method public final K()I
    .locals 2

    .prologue
    .line 818
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    .line 819
    if-eqz v0, :cond_0

    iget v1, v0, Lflipboard/objs/Image;->f:I

    if-lez v1, :cond_0

    .line 820
    iget v0, v0, Lflipboard/objs/Image;->f:I

    .line 822
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lflipboard/objs/FeedItem;->v:I

    goto :goto_0
.end method

.method public final L()I
    .locals 2

    .prologue
    .line 828
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    .line 829
    if-eqz v0, :cond_0

    iget v1, v0, Lflipboard/objs/Image;->g:I

    if-lez v1, :cond_0

    .line 830
    iget v0, v0, Lflipboard/objs/Image;->g:I

    .line 832
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lflipboard/objs/FeedItem;->w:I

    goto :goto_0
.end method

.method public final M()Lflipboard/objs/FeedSectionLink;
    .locals 5

    .prologue
    .line 840
    const/4 v1, 0x0

    .line 841
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 842
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 843
    iget-object v3, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    const-string v4, "magazine"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 849
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final N()Z
    .locals 2

    .prologue
    .line 853
    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "sectionCover"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final O()Z
    .locals 2

    .prologue
    .line 857
    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "group"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final P()Z
    .locals 2

    .prologue
    .line 861
    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "album"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Q()Z
    .locals 2

    .prologue
    .line 865
    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "pagebox"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final R()Z
    .locals 2

    .prologue
    .line 869
    const-string v0, "image"

    iget-object v1, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final S()Z
    .locals 2

    .prologue
    .line 874
    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "synthetic-client-app-cover"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final T()Z
    .locals 2

    .prologue
    .line 882
    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final U()Z
    .locals 2

    .prologue
    .line 886
    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final V()Z
    .locals 2

    .prologue
    .line 890
    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "status"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final W()Z
    .locals 2

    .prologue
    .line 894
    iget-object v0, p0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v1, "googlereader"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final X()Z
    .locals 2

    .prologue
    .line 995
    sget-object v0, Lflipboard/gui/section/SectionFragment;->c:Ljava/util/List;

    iget-object v1, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final Y()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1127
    iget-object v0, p0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    iget v3, v0, Lflipboard/objs/CommentaryResult$Item;->b:I

    iget v4, v0, Lflipboard/objs/CommentaryResult$Item;->c:I

    add-int/2addr v3, v4

    iget v4, v0, Lflipboard/objs/CommentaryResult$Item;->d:I

    add-int/2addr v3, v4

    iget v4, v0, Lflipboard/objs/CommentaryResult$Item;->f:I

    add-int/2addr v3, v4

    if-gtz v3, :cond_0

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    move v0, v1

    .line 1138
    :goto_1
    return v0

    :cond_1
    move v0, v2

    .line 1127
    goto :goto_0

    .line 1130
    :cond_2
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aP:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 1131
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aP:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_2
    add-int/lit8 v3, v0, -0x1

    if-ltz v3, :cond_4

    .line 1132
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aP:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1133
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->Y()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 1134
    goto :goto_1

    :cond_3
    move v0, v3

    .line 1136
    goto :goto_2

    :cond_4
    move v0, v2

    .line 1138
    goto :goto_1
.end method

.method public final Z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1143
    iget-object v0, p0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()I
    .locals 1

    .prologue
    .line 690
    const/16 v0, 0xb

    return v0
.end method

.method public final a(II)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 445
    .line 447
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v3

    .line 448
    if-nez v3, :cond_1

    .line 469
    :cond_0
    :goto_0
    return-object v0

    .line 454
    :cond_1
    iget-object v1, v3, Lflipboard/objs/Image;->c:Ljava/lang/String;

    .line 455
    if-eqz v1, :cond_2

    .line 457
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v4, "animate_gifs"

    sget-boolean v5, Lflipboard/activities/SettingsFragment;->a:Z

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 458
    invoke-static {v1}, Lflipboard/util/HttpUtil;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lflipboard/util/HttpUtil;->a(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 460
    :goto_1
    if-eqz v2, :cond_2

    const-string v4, "gif"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 465
    :cond_2
    if-nez v0, :cond_0

    .line 466
    invoke-virtual {v3, p1, p2}, Lflipboard/objs/Image;->a(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v2, v0

    goto :goto_1
.end method

.method public final a(Lflipboard/objs/CommentaryResult$Item$Commentary;)V
    .locals 2

    .prologue
    .line 1055
    sget-boolean v0, Lflipboard/objs/FeedItem;->cs:Z

    if-nez v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->h()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1056
    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    if-eqz v0, :cond_2

    .line 1057
    iget-object v0, p0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    iget-object v1, v0, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, v0, Lflipboard/objs/CommentaryResult$Item;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lflipboard/objs/CommentaryResult$Item;->b:I

    .line 1058
    :cond_1
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->an()V

    .line 1060
    :cond_2
    return-void
.end method

.method public final a(Lflipboard/objs/CommentaryResult$Item;)V
    .locals 2

    .prologue
    .line 968
    sget-boolean v0, Lflipboard/objs/FeedItem;->cs:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eq p0, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 971
    :cond_0
    if-eqz p1, :cond_1

    .line 972
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/objs/FeedItem$3;

    invoke-direct {v1, p0, p1}, Lflipboard/objs/FeedItem$3;-><init>(Lflipboard/objs/FeedItem;Lflipboard/objs/CommentaryResult$Item;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 986
    :cond_1
    return-void
.end method

.method public final a(Lflipboard/objs/CommentaryResult$Item;J)V
    .locals 2

    .prologue
    .line 945
    sget-boolean v0, Lflipboard/objs/FeedItem;->cs:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eq p0, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 948
    :cond_0
    if-eqz p1, :cond_1

    .line 949
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/objs/FeedItem$2;

    invoke-direct {v1, p0, p2, p3, p1}, Lflipboard/objs/FeedItem$2;-><init>(Lflipboard/objs/FeedItem;JLflipboard/objs/CommentaryResult$Item;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 964
    :cond_1
    return-void
.end method

.method public final a(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V
    .locals 1

    .prologue
    .line 1028
    sget-boolean v0, Lflipboard/objs/FeedItem;->cs:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eq p0, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1029
    :cond_0
    invoke-super {p0, p1}, Lflipboard/objs/HasCommentaryItem;->a(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 1030
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 700
    iput-object p1, p0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    .line 701
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 786
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 920
    sget-boolean v0, Lflipboard/objs/FeedItem;->cs:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eq p0, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 922
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/objs/FeedItem$1;

    invoke-direct {v1, p0, p1}, Lflipboard/objs/FeedItem$1;-><init>(Lflipboard/objs/FeedItem;Z)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 941
    return-void
.end method

.method public final a(FF)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1148
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    .line 1150
    if-eqz v1, :cond_0

    .line 1151
    iget v2, p0, Lflipboard/objs/FeedItem;->ak:I

    if-lez v2, :cond_1

    .line 1173
    :cond_0
    :goto_0
    return v0

    .line 1156
    :cond_1
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v2}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 1157
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, p1

    div-float/2addr v3, v2

    .line 1158
    invoke-static {}, Lflipboard/util/AndroidUtil;->d()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p2

    div-float v2, v4, v2

    .line 1159
    iget v4, v1, Lflipboard/objs/Image;->f:I

    int-to-float v4, v4

    cmpg-float v4, v4, v3

    if-ltz v4, :cond_0

    iget v4, v1, Lflipboard/objs/Image;->g:I

    int-to-float v4, v4

    cmpg-float v4, v4, v2

    if-ltz v4, :cond_0

    .line 1163
    invoke-virtual {v1}, Lflipboard/objs/Image;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1168
    div-float v2, v3, v2

    .line 1169
    invoke-virtual {v1}, Lflipboard/objs/Image;->h()F

    move-result v1

    .line 1170
    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 1171
    float-to-double v2, v1

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    cmpg-double v1, v2, v4

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(J)Z
    .locals 3

    .prologue
    .line 1000
    sget-boolean v0, Lflipboard/objs/FeedItem;->cs:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eq p0, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1001
    :cond_0
    iget-wide v0, p0, Lflipboard/objs/FeedItem;->ct:J

    cmp-long v0, v0, p1

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lflipboard/objs/ConfigService;)Z
    .locals 1

    .prologue
    .line 490
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->Y:Z

    if-nez v0, :cond_0

    .line 491
    const/4 v0, 0x0

    .line 493
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lflipboard/objs/FeedItem;->ad:Z

    goto :goto_0
.end method

.method public final aa()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1178
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aF:Lflipboard/objs/FeedArticle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aF:Lflipboard/objs/FeedArticle;

    iget-object v0, v0, Lflipboard/objs/FeedArticle;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1179
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aF:Lflipboard/objs/FeedArticle;

    iget-object v0, v0, Lflipboard/objs/FeedArticle;->d:Ljava/lang/String;

    .line 1182
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->bE:Ljava/lang/String;

    goto :goto_0
.end method

.method public final ab()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1187
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v3

    .line 1188
    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v4

    .line 1189
    iget-object v2, v3, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v3, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v5, "flipboard"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    .line 1190
    :goto_0
    if-eq v4, v3, :cond_1

    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 1189
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1190
    goto :goto_1
.end method

.method public final ac()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1212
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1213
    iget-object v0, p0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->o:Ljava/lang/String;

    .line 1221
    :goto_0
    return-object v0

    .line 1216
    :cond_0
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->ai()Ljava/lang/String;

    move-result-object v0

    .line 1217
    if-eqz v0, :cond_1

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/objs/FeedItem;->ci:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1218
    iget-object v0, p0, Lflipboard/objs/FeedItem;->ci:Ljava/lang/String;

    goto :goto_0

    .line 1221
    :cond_1
    iget-object v0, p0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final ad()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1226
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->ai()Ljava/lang/String;

    move-result-object v0

    .line 1227
    if-eqz v0, :cond_0

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->ci:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1228
    iget-object v0, p0, Lflipboard/objs/FeedItem;->ci:Ljava/lang/String;

    .line 1231
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final ae()Z
    .locals 2

    .prologue
    .line 1237
    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "section"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final af()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1242
    iget-object v0, p0, Lflipboard/objs/FeedItem;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->f:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final ag()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1247
    iget-object v1, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v2, "status"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1255
    :cond_0
    :goto_0
    return v0

    .line 1251
    :cond_1
    iget-object v1, p0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_0

    .line 1255
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ah()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1260
    iget-object v1, p0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    const-string v2, "googlereader"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1261
    iget-object v1, p0, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    const-string v2, "googlereader"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1268
    :cond_0
    :goto_0
    return v0

    .line 1263
    :cond_1
    iget-object v1, p0, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    const-string v2, "auth/googlereader"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1268
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ai()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1273
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->ci:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->m()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflipboard/objs/FeedItem;->ci:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1275
    const-string v0, "flipboard"

    .line 1279
    :goto_0
    return-object v0

    .line 1277
    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    goto :goto_0
.end method

.method public final aj()Lflipboard/objs/ConfigService;
    .locals 2

    .prologue
    .line 1284
    iget-object v0, p0, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1285
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    .line 1287
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ak()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1297
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    if-eqz v1, :cond_2

    move v1, v0

    .line 1301
    :goto_0
    :try_start_0
    new-instance v4, Ljava/net/URL;

    iget-object v5, p0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v4

    .line 1308
    :goto_1
    if-eqz v0, :cond_3

    .line 1309
    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 1312
    :goto_2
    if-eqz v0, :cond_1

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v3}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v3

    iget-object v3, v3, Lflipboard/model/ConfigSetting;->PhotoSaveDomainBlacklist:Ljava/util/List;

    if-eqz v3, :cond_1

    .line 1313
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 1314
    const-string v3, "www."

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1315
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object v0, v0, Lflipboard/model/ConfigSetting;->PhotoSaveDomainBlacklist:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1316
    const-string v5, "www."

    const-string v6, ""

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1317
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    .line 1324
    :cond_1
    return v1

    :cond_2
    move v1, v2

    .line 1297
    goto :goto_0

    .line 1304
    :catch_0
    move-exception v4

    sget-object v4, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v5, "Unable to parse sourceURL as String %s"

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v6, p0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    aput-object v6, v0, v2

    invoke-virtual {v4, v5, v0}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v3

    goto :goto_1

    :cond_3
    move-object v0, v3

    goto :goto_2
.end method

.method public final al()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1346
    iget-object v0, p0, Lflipboard/objs/FeedItem;->W:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1348
    const-string v0, "magazine"

    .line 1359
    :goto_0
    return-object v0

    .line 1349
    :cond_0
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->R()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1350
    const-string v0, "image"

    goto :goto_0

    .line 1351
    :cond_1
    iget-object v0, p0, Lflipboard/objs/FeedItem;->A:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/objs/FeedItem;->B:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1352
    const-string v0, "rss"

    goto :goto_0

    .line 1353
    :cond_2
    const-string v0, "video"

    iget-object v1, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1354
    invoke-static {p0}, Lflipboard/util/VideoUtil;->a(Lflipboard/objs/FeedItem;)Lflipboard/util/VideoUtil$VideoType;

    move-result-object v0

    .line 1355
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "video_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lflipboard/util/VideoUtil;->a(Lflipboard/util/VideoUtil$VideoType;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1357
    :cond_3
    const-string v0, "embeddedWebview"

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lflipboard/objs/CommentaryResult$Item$Commentary;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1040
    sget-boolean v0, Lflipboard/objs/FeedItem;->cs:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eq p0, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1041
    :cond_0
    sget-boolean v0, Lflipboard/objs/FeedItem;->cs:Z

    if-nez v0, :cond_1

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->h()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1043
    :cond_1
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->am()V

    .line 1044
    const/4 v0, 0x0

    .line 1045
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->ai()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    .line 1046
    if-eqz v1, :cond_2

    .line 1047
    iget-object v2, p0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    new-instance v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    invoke-direct {v0}, Lflipboard/objs/CommentaryResult$Item$Commentary;-><init>()V

    const-string v3, "comment"

    invoke-virtual {v0, v3, p1, v1}, Lflipboard/objs/CommentaryResult$Item$Commentary;->a(Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Account;)Lflipboard/objs/CommentaryResult$Item$Commentary;

    move-result-object v0

    iput-boolean v4, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->p:Z

    iget-object v1, v2, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    invoke-interface {v1, v4, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget v1, v2, Lflipboard/objs/CommentaryResult$Item;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v2, Lflipboard/objs/CommentaryResult$Item;->b:I

    .line 1048
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->an()V

    .line 1050
    :cond_2
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 715
    iget-object v0, p0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->z:Ljava/lang/String;

    goto :goto_0
.end method

.method public final b(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V
    .locals 1

    .prologue
    .line 1034
    sget-boolean v0, Lflipboard/objs/FeedItem;->cs:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eq p0, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1035
    :cond_0
    invoke-super {p0, p1}, Lflipboard/objs/HasCommentaryItem;->b(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 1036
    return-void
.end method

.method public final b(Lflipboard/objs/ConfigService;)Z
    .locals 1

    .prologue
    .line 518
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->H:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lflipboard/objs/ConfigService;->ao:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p1, Lflipboard/objs/ConfigService;->ap:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 519
    :cond_0
    const/4 v0, 0x0

    .line 521
    :goto_0
    return v0

    :cond_1
    iget-boolean v0, p0, Lflipboard/objs/FeedItem;->ah:Z

    goto :goto_0
.end method

.method public final c()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 776
    const/4 v0, 0x0

    .line 777
    iget-object v1, p0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 778
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 779
    iget-object v1, p0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 781
    :cond_0
    return-object v0
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 766
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 261
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 263
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lflipboard/objs/Image;
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 288
    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->X:Lflipboard/objs/Image;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lflipboard/objs/FeedItem;->X:Lflipboard/objs/Image;

    .line 314
    :goto_0
    return-object v0

    .line 292
    :cond_0
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->P()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 293
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 294
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    .line 295
    if-eqz v0, :cond_1

    goto :goto_0

    .line 302
    :cond_2
    iget-object v0, p0, Lflipboard/objs/FeedItem;->I:Lflipboard/objs/Image;

    if-eqz v0, :cond_3

    .line 303
    iget-object v0, p0, Lflipboard/objs/FeedItem;->I:Lflipboard/objs/Image;

    goto :goto_0

    .line 306
    :cond_3
    iget-object v0, p0, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    if-eqz v0, :cond_4

    .line 307
    iget-object v0, p0, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    goto :goto_0

    .line 310
    :cond_4
    iget-object v0, p0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    if-eqz v0, :cond_5

    .line 311
    iget-object v0, p0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    goto :goto_0

    .line 314
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1329
    instance-of v1, p1, Lflipboard/objs/FeedItem;

    if-eqz v1, :cond_0

    .line 1330
    check-cast p1, Lflipboard/objs/FeedItem;

    .line 1331
    iget-object v1, p0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 1333
    :cond_0
    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 319
    const/4 v1, 0x0

    .line 321
    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 323
    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v2, "video"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->X:Lflipboard/objs/Image;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lflipboard/objs/FeedItem;->X:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v0

    .line 369
    :goto_0
    return-object v0

    .line 327
    :cond_0
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->P()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 328
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 329
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->f()Ljava/lang/String;

    move-result-object v0

    .line 330
    if-eqz v0, :cond_1

    goto :goto_0

    .line 343
    :cond_2
    iget-object v0, p0, Lflipboard/objs/FeedItem;->I:Lflipboard/objs/Image;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/objs/FeedItem;->I:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 344
    iget-object v0, p0, Lflipboard/objs/FeedItem;->I:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    goto :goto_0

    .line 345
    :cond_3
    iget-object v0, p0, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 346
    iget-object v0, p0, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    goto :goto_0

    .line 347
    :cond_4
    iget-object v0, p0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 348
    iget-object v0, p0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    goto :goto_0

    .line 352
    :cond_5
    iget-object v0, p0, Lflipboard/objs/FeedItem;->I:Lflipboard/objs/Image;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lflipboard/objs/FeedItem;->I:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->c:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 353
    iget-object v0, p0, Lflipboard/objs/FeedItem;->I:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->c:Ljava/lang/String;

    goto :goto_0

    .line 354
    :cond_6
    iget-object v0, p0, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->c:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 355
    iget-object v0, p0, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->c:Ljava/lang/String;

    goto :goto_0

    .line 356
    :cond_7
    iget-object v0, p0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->c:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 357
    iget-object v0, p0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 361
    :cond_8
    iget-object v0, p0, Lflipboard/objs/FeedItem;->I:Lflipboard/objs/Image;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lflipboard/objs/FeedItem;->I:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 362
    iget-object v0, p0, Lflipboard/objs/FeedItem;->I:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 363
    :cond_9
    iget-object v0, p0, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 364
    iget-object v0, p0, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 365
    :cond_a
    iget-object v0, p0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 366
    iget-object v0, p0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    goto/16 :goto_0

    :cond_b
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lflipboard/objs/FeedItem;->I:Lflipboard/objs/Image;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->I:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->f()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->f()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    const v0, 0x7fffffff

    .line 385
    invoke-virtual {p0, v0, v0}, Lflipboard/objs/FeedItem;->a(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1339
    iget v0, p0, Lflipboard/objs/FeedItem;->t:I

    return v0
.end method

.method public final i()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 389
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v2

    .line 390
    if-eqz v2, :cond_1

    iget-object v2, v2, Lflipboard/objs/Image;->l:Lflipboard/objs/Image$AvailabilityMap;

    invoke-static {v2}, Lflipboard/objs/Image$AvailabilityMap;->a(Lflipboard/objs/Image$AvailabilityMap;)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 710
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 736
    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "folder"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Image;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x4

    .line 424
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 425
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/Image;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 426
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 428
    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 429
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 430
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v5, :cond_2

    if-ge v1, v3, :cond_2

    .line 431
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 432
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->R()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->i()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 433
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v4

    .line 434
    invoke-virtual {v4}, Lflipboard/objs/Image;->c()Z

    move-result v4

    if-nez v4, :cond_1

    .line 435
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 430
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 440
    :cond_2
    return-object v2
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lflipboard/objs/FeedItem;->ci:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedItem;->ci:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 705
    iget-object v0, p0, Lflipboard/objs/FeedItem;->by:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 720
    const/4 v0, 0x0

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 726
    iget-object v0, p0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 731
    const/4 v0, 0x0

    return v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 741
    const/4 v0, 0x0

    return v0
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 746
    iget-boolean v0, p0, Lflipboard/objs/FeedItem;->bW:Z

    return v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 751
    iget-boolean v0, p0, Lflipboard/objs/FeedItem;->cj:Z

    return v0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 761
    const/4 v0, 0x0

    return v0
.end method

.method public final w()I
    .locals 1

    .prologue
    .line 771
    iget v0, p0, Lflipboard/objs/FeedItem;->ca:I

    return v0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 1293
    const/4 v0, 0x1

    return v0
.end method

.method public final y()Lflipboard/json/FLObject;
    .locals 1

    .prologue
    .line 551
    :goto_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->cn:Lflipboard/json/FLObject;

    if-eqz v0, :cond_0

    .line 552
    iget-object v0, p0, Lflipboard/objs/FeedItem;->cn:Lflipboard/json/FLObject;

    .line 556
    :goto_1
    return-object v0

    .line 553
    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->co:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_1

    .line 554
    iget-object p0, p0, Lflipboard/objs/FeedItem;->co:Lflipboard/objs/FeedItem;

    goto :goto_0

    .line 556
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final z()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/json/FLObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 561
    const/4 v0, 0x0

    .line 562
    iget-object v1, p0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_6

    .line 563
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 564
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 567
    if-eqz v0, :cond_0

    .line 568
    new-instance v3, Lflipboard/json/FLObject;

    invoke-direct {v3}, Lflipboard/json/FLObject;-><init>()V

    .line 569
    iget-object v4, v0, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 570
    sget-object v4, Lflipboard/objs/UsageEventV2$SourceData;->a:Lflipboard/objs/UsageEventV2$SourceData;

    invoke-virtual {v4}, Lflipboard/objs/UsageEventV2$SourceData;->name()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 572
    :cond_1
    iget-object v4, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 573
    sget-object v4, Lflipboard/objs/UsageEventV2$SourceData;->b:Lflipboard/objs/UsageEventV2$SourceData;

    invoke-virtual {v4}, Lflipboard/objs/UsageEventV2$SourceData;->name()Ljava/lang/String;

    move-result-object v4

    iget-object v0, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 575
    :cond_2
    iget-object v0, p0, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 576
    sget-object v0, Lflipboard/objs/UsageEventV2$SourceData;->c:Lflipboard/objs/UsageEventV2$SourceData;

    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2$SourceData;->name()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 578
    :cond_3
    iget-object v0, p0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 579
    sget-object v0, Lflipboard/objs/UsageEventV2$SourceData;->d:Lflipboard/objs/UsageEventV2$SourceData;

    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2$SourceData;->name()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 581
    :cond_4
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    move-object v0, v1

    .line 585
    :cond_6
    return-object v0
.end method
