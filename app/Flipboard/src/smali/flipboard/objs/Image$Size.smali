.class public final enum Lflipboard/objs/Image$Size;
.super Ljava/lang/Enum;
.source "Image.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/objs/Image$Size;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/objs/Image$Size;

.field public static final enum b:Lflipboard/objs/Image$Size;

.field public static final enum c:Lflipboard/objs/Image$Size;

.field public static final enum d:Lflipboard/objs/Image$Size;

.field public static final enum e:Lflipboard/objs/Image$Size;

.field private static final synthetic f:[Lflipboard/objs/Image$Size;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 56
    new-instance v0, Lflipboard/objs/Image$Size;

    const-string v1, "NA"

    invoke-direct {v0, v1, v2}, Lflipboard/objs/Image$Size;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/Image$Size;->a:Lflipboard/objs/Image$Size;

    new-instance v0, Lflipboard/objs/Image$Size;

    const-string v1, "S"

    invoke-direct {v0, v1, v3}, Lflipboard/objs/Image$Size;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/Image$Size;->b:Lflipboard/objs/Image$Size;

    new-instance v0, Lflipboard/objs/Image$Size;

    const-string v1, "M"

    invoke-direct {v0, v1, v4}, Lflipboard/objs/Image$Size;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/Image$Size;->c:Lflipboard/objs/Image$Size;

    new-instance v0, Lflipboard/objs/Image$Size;

    const-string v1, "L"

    invoke-direct {v0, v1, v5}, Lflipboard/objs/Image$Size;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/Image$Size;->d:Lflipboard/objs/Image$Size;

    new-instance v0, Lflipboard/objs/Image$Size;

    const-string v1, "XL"

    invoke-direct {v0, v1, v6}, Lflipboard/objs/Image$Size;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/Image$Size;->e:Lflipboard/objs/Image$Size;

    .line 55
    const/4 v0, 0x5

    new-array v0, v0, [Lflipboard/objs/Image$Size;

    sget-object v1, Lflipboard/objs/Image$Size;->a:Lflipboard/objs/Image$Size;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/objs/Image$Size;->b:Lflipboard/objs/Image$Size;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/objs/Image$Size;->c:Lflipboard/objs/Image$Size;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/objs/Image$Size;->d:Lflipboard/objs/Image$Size;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/objs/Image$Size;->e:Lflipboard/objs/Image$Size;

    aput-object v1, v0, v6

    sput-object v0, Lflipboard/objs/Image$Size;->f:[Lflipboard/objs/Image$Size;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/objs/Image$Size;
    .locals 1

    .prologue
    .line 55
    const-class v0, Lflipboard/objs/Image$Size;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Image$Size;

    return-object v0
.end method

.method public static values()[Lflipboard/objs/Image$Size;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lflipboard/objs/Image$Size;->f:[Lflipboard/objs/Image$Size;

    invoke-virtual {v0}, [Lflipboard/objs/Image$Size;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/objs/Image$Size;

    return-object v0
.end method
