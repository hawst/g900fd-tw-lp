.class Lflipboard/objs/Image$AvailabilityMap;
.super Ljava/lang/Object;
.source "Image.java"


# instance fields
.field final synthetic a:Lflipboard/objs/Image;

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lflipboard/objs/Image$Size;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Lflipboard/objs/Image$Size;

.field private e:Lflipboard/objs/Image$Size;


# direct methods
.method private constructor <init>(Lflipboard/objs/Image;)V
    .locals 2

    .prologue
    .line 391
    iput-object p1, p0, Lflipboard/objs/Image$AvailabilityMap;->a:Lflipboard/objs/Image;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-static {}, Lflipboard/objs/Image$Size;->values()[Lflipboard/objs/Image$Size;

    move-result-object v1

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    iput-object v0, p0, Lflipboard/objs/Image$AvailabilityMap;->b:Ljava/util/Map;

    .line 396
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/objs/Image$AvailabilityMap;->c:Z

    return-void
.end method

.method synthetic constructor <init>(Lflipboard/objs/Image;B)V
    .locals 0

    .prologue
    .line 391
    invoke-direct {p0, p1}, Lflipboard/objs/Image$AvailabilityMap;-><init>(Lflipboard/objs/Image;)V

    return-void
.end method

.method static synthetic a(Lflipboard/objs/Image$AvailabilityMap;Lflipboard/objs/Image$Size;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 391
    invoke-direct {p0, p1}, Lflipboard/objs/Image$AvailabilityMap;->a(Lflipboard/objs/Image$Size;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lflipboard/objs/Image$Size;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 408
    invoke-direct {p0}, Lflipboard/objs/Image$AvailabilityMap;->a()V

    .line 409
    iget-object v0, p0, Lflipboard/objs/Image$AvailabilityMap;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private declared-synchronized a()V
    .locals 2

    .prologue
    .line 434
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lflipboard/objs/Image$AvailabilityMap;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 452
    :goto_0
    monitor-exit p0

    return-void

    .line 437
    :cond_0
    :try_start_1
    iget-object v0, p0, Lflipboard/objs/Image$AvailabilityMap;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 439
    iget-object v0, p0, Lflipboard/objs/Image$AvailabilityMap;->a:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 440
    sget-object v0, Lflipboard/objs/Image$Size;->b:Lflipboard/objs/Image$Size;

    iget-object v1, p0, Lflipboard/objs/Image$AvailabilityMap;->a:Lflipboard/objs/Image;

    iget-object v1, v1, Lflipboard/objs/Image;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lflipboard/objs/Image$AvailabilityMap;->a(Lflipboard/objs/Image$Size;Ljava/lang/String;)V

    .line 442
    :cond_1
    iget-object v0, p0, Lflipboard/objs/Image$AvailabilityMap;->a:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 443
    sget-object v0, Lflipboard/objs/Image$Size;->c:Lflipboard/objs/Image$Size;

    iget-object v1, p0, Lflipboard/objs/Image$AvailabilityMap;->a:Lflipboard/objs/Image;

    iget-object v1, v1, Lflipboard/objs/Image;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lflipboard/objs/Image$AvailabilityMap;->a(Lflipboard/objs/Image$Size;Ljava/lang/String;)V

    .line 445
    :cond_2
    iget-object v0, p0, Lflipboard/objs/Image$AvailabilityMap;->a:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 446
    sget-object v0, Lflipboard/objs/Image$Size;->d:Lflipboard/objs/Image$Size;

    iget-object v1, p0, Lflipboard/objs/Image$AvailabilityMap;->a:Lflipboard/objs/Image;

    iget-object v1, v1, Lflipboard/objs/Image;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lflipboard/objs/Image$AvailabilityMap;->a(Lflipboard/objs/Image$Size;Ljava/lang/String;)V

    .line 448
    :cond_3
    iget-object v0, p0, Lflipboard/objs/Image$AvailabilityMap;->a:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 449
    sget-object v0, Lflipboard/objs/Image$Size;->e:Lflipboard/objs/Image$Size;

    iget-object v1, p0, Lflipboard/objs/Image$AvailabilityMap;->a:Lflipboard/objs/Image;

    iget-object v1, v1, Lflipboard/objs/Image;->d:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lflipboard/objs/Image$AvailabilityMap;->a(Lflipboard/objs/Image$Size;Ljava/lang/String;)V

    .line 451
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/objs/Image$AvailabilityMap;->c:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 434
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Lflipboard/objs/Image$Size;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lflipboard/objs/Image$AvailabilityMap;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 458
    iput-object p1, p0, Lflipboard/objs/Image$AvailabilityMap;->d:Lflipboard/objs/Image$Size;

    .line 461
    :cond_0
    iput-object p1, p0, Lflipboard/objs/Image$AvailabilityMap;->e:Lflipboard/objs/Image$Size;

    .line 462
    iget-object v0, p0, Lflipboard/objs/Image$AvailabilityMap;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463
    return-void
.end method

.method static synthetic a(Lflipboard/objs/Image$AvailabilityMap;)Z
    .locals 1

    .prologue
    .line 391
    invoke-direct {p0}, Lflipboard/objs/Image$AvailabilityMap;->a()V

    iget-object v0, p0, Lflipboard/objs/Image$AvailabilityMap;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lflipboard/objs/Image$AvailabilityMap;)Lflipboard/objs/Image$Size;
    .locals 1

    .prologue
    .line 391
    invoke-direct {p0}, Lflipboard/objs/Image$AvailabilityMap;->a()V

    iget-object v0, p0, Lflipboard/objs/Image$AvailabilityMap;->e:Lflipboard/objs/Image$Size;

    if-nez v0, :cond_0

    sget-object v0, Lflipboard/objs/Image$Size;->a:Lflipboard/objs/Image$Size;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflipboard/objs/Image$AvailabilityMap;->e:Lflipboard/objs/Image$Size;

    goto :goto_0
.end method

.method static synthetic c(Lflipboard/objs/Image$AvailabilityMap;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 391
    invoke-direct {p0}, Lflipboard/objs/Image$AvailabilityMap;->a()V

    iget-object v0, p0, Lflipboard/objs/Image$AvailabilityMap;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v1, 0x2d

    .line 467
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "availabilityMap="

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 468
    sget-object v0, Lflipboard/objs/Image$Size;->b:Lflipboard/objs/Image$Size;

    invoke-direct {p0, v0}, Lflipboard/objs/Image$AvailabilityMap;->a(Lflipboard/objs/Image$Size;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 469
    sget-object v0, Lflipboard/objs/Image$Size;->c:Lflipboard/objs/Image$Size;

    invoke-direct {p0, v0}, Lflipboard/objs/Image$AvailabilityMap;->a(Lflipboard/objs/Image$Size;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 470
    sget-object v0, Lflipboard/objs/Image$Size;->d:Lflipboard/objs/Image$Size;

    invoke-direct {p0, v0}, Lflipboard/objs/Image$AvailabilityMap;->a(Lflipboard/objs/Image$Size;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 471
    sget-object v0, Lflipboard/objs/Image$Size;->e:Lflipboard/objs/Image$Size;

    invoke-direct {p0, v0}, Lflipboard/objs/Image$AvailabilityMap;->a(Lflipboard/objs/Image$Size;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_3
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 472
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 468
    :cond_0
    const/16 v0, 0x53

    goto :goto_0

    .line 469
    :cond_1
    const/16 v0, 0x4d

    goto :goto_1

    .line 470
    :cond_2
    const/16 v0, 0x4c

    goto :goto_2

    .line 471
    :cond_3
    const/16 v1, 0x58

    goto :goto_3
.end method
