.class public Lflipboard/objs/UsageEventV2;
.super Lflipboard/objs/Base;
.source "UsageEventV2.java"


# static fields
.field static a:Lflipboard/util/Log;


# instance fields
.field public final b:Lflipboard/objs/UsageEventV2$EventAction;

.field public final c:Lflipboard/objs/UsageEventV2$EventCategory;

.field public final d:Lflipboard/objs/UsageEventV2$ProductType;

.field public e:Lflipboard/json/FLObject;

.field public f:Lflipboard/objs/UsageEventV2$Properties;

.field public transient g:Lflipboard/objs/UsageEventV2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-string v0, "usage"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/objs/UsageEventV2;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 322
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 214
    sget-object v0, Lflipboard/objs/UsageEventV2$ProductType;->a:Lflipboard/objs/UsageEventV2$ProductType;

    iput-object v0, p0, Lflipboard/objs/UsageEventV2;->d:Lflipboard/objs/UsageEventV2$ProductType;

    .line 323
    iput-object v1, p0, Lflipboard/objs/UsageEventV2;->b:Lflipboard/objs/UsageEventV2$EventAction;

    .line 324
    iput-object v1, p0, Lflipboard/objs/UsageEventV2;->c:Lflipboard/objs/UsageEventV2$EventCategory;

    .line 325
    return-void
.end method

.method public constructor <init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V
    .locals 2

    .prologue
    .line 278
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 214
    sget-object v0, Lflipboard/objs/UsageEventV2$ProductType;->a:Lflipboard/objs/UsageEventV2$ProductType;

    iput-object v0, p0, Lflipboard/objs/UsageEventV2;->d:Lflipboard/objs/UsageEventV2$ProductType;

    .line 279
    iput-object p1, p0, Lflipboard/objs/UsageEventV2;->b:Lflipboard/objs/UsageEventV2$EventAction;

    .line 280
    iput-object p2, p0, Lflipboard/objs/UsageEventV2;->c:Lflipboard/objs/UsageEventV2$EventCategory;

    .line 281
    new-instance v0, Lflipboard/objs/UsageEventV2$Properties;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-direct {v0, v1}, Lflipboard/objs/UsageEventV2$Properties;-><init>(Lflipboard/service/FlipboardManager;)V

    iput-object v0, p0, Lflipboard/objs/UsageEventV2;->f:Lflipboard/objs/UsageEventV2$Properties;

    .line 282
    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    iput-object v0, p0, Lflipboard/objs/UsageEventV2;->e:Lflipboard/json/FLObject;

    .line 283
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;
    .locals 1

    .prologue
    .line 314
    invoke-virtual {p1}, Lflipboard/objs/UsageEventV2$CommonEventData;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lflipboard/objs/UsageEventV2;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 315
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 329
    sget-object v0, Lflipboard/usage/UsageManagerV2;->b:Lflipboard/usage/UsageManagerV2;

    iget-object v0, v0, Lflipboard/usage/UsageManagerV2;->p:Lflipboard/usage/UsageManagerV2$Uploader;

    invoke-virtual {v0, p0}, Lflipboard/usage/UsageManagerV2$Uploader;->a(Lflipboard/objs/UsageEventV2;)V

    .line 330
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 291
    if-eqz p1, :cond_1

    .line 296
    iget-object v0, p0, Lflipboard/objs/UsageEventV2;->e:Lflipboard/json/FLObject;

    if-nez v0, :cond_0

    .line 297
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempting to set values on a usage event that was initialized without using the designated constructor (takes event_action and event_category)"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 299
    :cond_0
    if-nez p2, :cond_2

    .line 300
    iget-object v0, p0, Lflipboard/objs/UsageEventV2;->e:Lflipboard/json/FLObject;

    invoke-virtual {v0, p1}, Lflipboard/json/FLObject;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    :cond_1
    :goto_0
    return-void

    .line 302
    :cond_2
    iget-object v0, p0, Lflipboard/objs/UsageEventV2;->e:Lflipboard/json/FLObject;

    invoke-virtual {v0, p1, p2}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    goto :goto_0
.end method

.method public final b()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 339
    sget-object v0, Lflipboard/usage/UsageManagerV2;->b:Lflipboard/usage/UsageManagerV2;

    iget-object v1, v0, Lflipboard/usage/UsageManagerV2;->p:Lflipboard/usage/UsageManagerV2$Uploader;

    sget-object v0, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v2

    if-ne v0, v2, :cond_1

    const-string v0, "Wrong thread. This method does network calls, so don\'t run it on the UI thread"

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v2, v2, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v2, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ac:Z

    if-eqz v0, :cond_2

    sget-object v0, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    invoke-virtual {v1, p0}, Lflipboard/usage/UsageManagerV2$Uploader;->a(Lflipboard/objs/UsageEventV2;)V

    .line 340
    :goto_0
    return-void

    .line 339
    :cond_2
    :try_start_0
    new-instance v2, Ljava/io/File;

    iget-object v0, v1, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v0, v0, Lflipboard/usage/UsageManagerV2;->d:Ljava/io/File;

    const-string v3, "highprio-%s-%s-%s.json"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lflipboard/objs/UsageEventV2;->b:Lflipboard/objs/UsageEventV2$EventAction;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lflipboard/objs/UsageEventV2;->c:Lflipboard/objs/UsageEventV2$EventCategory;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v3, "rw"

    invoke-direct {v0, v2, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0}, Lflipboard/usage/UsageManagerV2$Uploader;->a(Ljava/io/RandomAccessFile;)V

    invoke-virtual {p0}, Lflipboard/objs/UsageEventV2;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-static {v0, v3}, Lflipboard/usage/UsageManagerV2$Uploader;->a(Ljava/io/RandomAccessFile;[B)V

    invoke-static {v0}, Lflipboard/usage/UsageManagerV2$Uploader;->b(Ljava/io/RandomAccessFile;)V

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v1, v2}, Lflipboard/usage/UsageManagerV2$Uploader;->a(Ljava/io/File;)V

    sget-object v0, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lflipboard/objs/UsageEventV2;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v3
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    invoke-virtual {v1, p0}, Lflipboard/usage/UsageManagerV2$Uploader;->a(Lflipboard/objs/UsageEventV2;)V

    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "Failed to upload high priority usage right away, putting it on the normal schedule"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v1, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 348
    new-instance v0, Lflipboard/objs/UsageEventV2$1;

    invoke-direct {v0, p0}, Lflipboard/objs/UsageEventV2$1;-><init>(Lflipboard/objs/UsageEventV2;)V

    .line 356
    sget-object v1, Lflipboard/usage/UsageManagerV2;->b:Lflipboard/usage/UsageManagerV2;

    iget-object v1, v1, Lflipboard/usage/UsageManagerV2;->l:Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 357
    return-void
.end method
