.class public final enum Lflipboard/objs/CommentaryResult$CommentType;
.super Ljava/lang/Enum;
.source "CommentaryResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/objs/CommentaryResult$CommentType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/objs/CommentaryResult$CommentType;

.field public static final enum b:Lflipboard/objs/CommentaryResult$CommentType;

.field public static final enum c:Lflipboard/objs/CommentaryResult$CommentType;

.field private static final synthetic d:[Lflipboard/objs/CommentaryResult$CommentType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lflipboard/objs/CommentaryResult$CommentType;

    const-string v1, "SOCIAL_CARD_COMMENT"

    invoke-direct {v0, v1, v2}, Lflipboard/objs/CommentaryResult$CommentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/CommentaryResult$CommentType;->a:Lflipboard/objs/CommentaryResult$CommentType;

    new-instance v0, Lflipboard/objs/CommentaryResult$CommentType;

    const-string v1, "MAGAZINE_CONVERSATION_THREAD"

    invoke-direct {v0, v1, v3}, Lflipboard/objs/CommentaryResult$CommentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/CommentaryResult$CommentType;->b:Lflipboard/objs/CommentaryResult$CommentType;

    new-instance v0, Lflipboard/objs/CommentaryResult$CommentType;

    const-string v1, "MAGAZINE_CONVERSATION_THREAD_COMMENT"

    invoke-direct {v0, v1, v4}, Lflipboard/objs/CommentaryResult$CommentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/CommentaryResult$CommentType;->c:Lflipboard/objs/CommentaryResult$CommentType;

    .line 22
    const/4 v0, 0x3

    new-array v0, v0, [Lflipboard/objs/CommentaryResult$CommentType;

    sget-object v1, Lflipboard/objs/CommentaryResult$CommentType;->a:Lflipboard/objs/CommentaryResult$CommentType;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/objs/CommentaryResult$CommentType;->b:Lflipboard/objs/CommentaryResult$CommentType;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/objs/CommentaryResult$CommentType;->c:Lflipboard/objs/CommentaryResult$CommentType;

    aput-object v1, v0, v4

    sput-object v0, Lflipboard/objs/CommentaryResult$CommentType;->d:[Lflipboard/objs/CommentaryResult$CommentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/objs/CommentaryResult$CommentType;
    .locals 1

    .prologue
    .line 22
    const-class v0, Lflipboard/objs/CommentaryResult$CommentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$CommentType;

    return-object v0
.end method

.method public static values()[Lflipboard/objs/CommentaryResult$CommentType;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lflipboard/objs/CommentaryResult$CommentType;->d:[Lflipboard/objs/CommentaryResult$CommentType;

    invoke-virtual {v0}, [Lflipboard/objs/CommentaryResult$CommentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/objs/CommentaryResult$CommentType;

    return-object v0
.end method
