.class public Lflipboard/objs/UserService$Cookie;
.super Lflipboard/objs/Base;
.source "UserService.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 88
    instance-of v1, p1, Lflipboard/objs/UserService$Cookie;

    if-eqz v1, :cond_0

    .line 89
    check-cast p1, Lflipboard/objs/UserService$Cookie;

    .line 90
    iget-object v1, p0, Lflipboard/objs/UserService$Cookie;->a:Ljava/lang/String;

    iget-object v2, p1, Lflipboard/objs/UserService$Cookie;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lflipboard/util/JavaUtil;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/objs/UserService$Cookie;->b:Ljava/lang/String;

    iget-object v2, p1, Lflipboard/objs/UserService$Cookie;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lflipboard/util/JavaUtil;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/objs/UserService$Cookie;->c:Ljava/lang/String;

    iget-object v2, p1, Lflipboard/objs/UserService$Cookie;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lflipboard/util/JavaUtil;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 92
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 97
    iget-object v0, p0, Lflipboard/objs/UserService$Cookie;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/objs/UserService$Cookie;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/lit8 v0, v0, 0x45

    .line 99
    mul-int/lit8 v2, v0, 0x17

    iget-object v0, p0, Lflipboard/objs/UserService$Cookie;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/objs/UserService$Cookie;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 100
    mul-int/lit8 v0, v0, 0x17

    iget-object v2, p0, Lflipboard/objs/UserService$Cookie;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lflipboard/objs/UserService$Cookie;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 101
    return v0

    :cond_1
    move v0, v1

    .line 97
    goto :goto_0

    :cond_2
    move v0, v1

    .line 99
    goto :goto_1
.end method
