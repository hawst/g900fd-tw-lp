.class public final enum Lflipboard/objs/UsageEventV2$FollowFrom;
.super Ljava/lang/Enum;
.source "UsageEventV2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/objs/UsageEventV2$FollowFrom;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum b:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum c:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum d:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum e:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum f:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum g:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum h:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum i:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum j:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum k:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum l:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum m:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum n:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum o:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum p:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum q:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum r:Lflipboard/objs/UsageEventV2$FollowFrom;

.field public static final enum s:Lflipboard/objs/UsageEventV2$FollowFrom;

.field private static final synthetic t:[Lflipboard/objs/UsageEventV2$FollowFrom;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 150
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "pagebox"

    invoke-direct {v0, v1, v3}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->a:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 151
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "user_profile_layout"

    invoke-direct {v0, v1, v4}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->b:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 152
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "profile"

    invoke-direct {v0, v1, v5}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->c:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 153
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "user_rec"

    invoke-direct {v0, v1, v6}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->d:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 154
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "find_friends"

    invoke-direct {v0, v1, v7}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->e:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 155
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "reflip_list"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->f:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 156
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "likes_list"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->g:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 157
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "self_followers_list"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->h:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 158
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "followers_list"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->i:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 159
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "find_friends_contacts"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->j:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 160
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "find_friends_flipboard"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->k:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 161
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "find_friends_facebook"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->l:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 162
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "find_friends_twitter"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->m:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 163
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "find_friends_googleplus"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->n:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 164
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "social_card"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->o:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 165
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "notifications_list"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->p:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 166
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "house_ad"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->q:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 167
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "hint"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->r:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 168
    new-instance v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    const-string v1, "flip_feed"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2$FollowFrom;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->s:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 149
    const/16 v0, 0x13

    new-array v0, v0, [Lflipboard/objs/UsageEventV2$FollowFrom;

    sget-object v1, Lflipboard/objs/UsageEventV2$FollowFrom;->a:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/objs/UsageEventV2$FollowFrom;->b:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/objs/UsageEventV2$FollowFrom;->c:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v1, v0, v5

    sget-object v1, Lflipboard/objs/UsageEventV2$FollowFrom;->d:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/objs/UsageEventV2$FollowFrom;->e:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lflipboard/objs/UsageEventV2$FollowFrom;->f:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lflipboard/objs/UsageEventV2$FollowFrom;->g:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lflipboard/objs/UsageEventV2$FollowFrom;->h:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lflipboard/objs/UsageEventV2$FollowFrom;->i:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lflipboard/objs/UsageEventV2$FollowFrom;->j:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lflipboard/objs/UsageEventV2$FollowFrom;->k:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lflipboard/objs/UsageEventV2$FollowFrom;->l:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lflipboard/objs/UsageEventV2$FollowFrom;->m:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lflipboard/objs/UsageEventV2$FollowFrom;->n:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lflipboard/objs/UsageEventV2$FollowFrom;->o:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lflipboard/objs/UsageEventV2$FollowFrom;->p:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lflipboard/objs/UsageEventV2$FollowFrom;->q:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lflipboard/objs/UsageEventV2$FollowFrom;->r:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lflipboard/objs/UsageEventV2$FollowFrom;->s:Lflipboard/objs/UsageEventV2$FollowFrom;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->t:[Lflipboard/objs/UsageEventV2$FollowFrom;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 149
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/objs/UsageEventV2$FollowFrom;
    .locals 1

    .prologue
    .line 149
    const-class v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UsageEventV2$FollowFrom;

    return-object v0
.end method

.method public static values()[Lflipboard/objs/UsageEventV2$FollowFrom;
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lflipboard/objs/UsageEventV2$FollowFrom;->t:[Lflipboard/objs/UsageEventV2$FollowFrom;

    invoke-virtual {v0}, [Lflipboard/objs/UsageEventV2$FollowFrom;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/objs/UsageEventV2$FollowFrom;

    return-object v0
.end method
