.class public Lflipboard/objs/FeedItem$5;
.super Lflipboard/service/ServiceReloginObserver;
.source "FeedItem.java"


# instance fields
.field final synthetic a:Ljava/lang/ref/WeakReference;

.field final synthetic b:Lflipboard/service/Section;

.field final synthetic c:Z

.field final synthetic d:Lflipboard/objs/FeedItem;


# direct methods
.method public constructor <init>(Lflipboard/objs/FeedItem;Ljava/lang/ref/WeakReference;Lflipboard/service/Section;Z)V
    .locals 0

    .prologue
    .line 1074
    iput-object p1, p0, Lflipboard/objs/FeedItem$5;->d:Lflipboard/objs/FeedItem;

    iput-object p2, p0, Lflipboard/objs/FeedItem$5;->a:Ljava/lang/ref/WeakReference;

    iput-object p3, p0, Lflipboard/objs/FeedItem$5;->b:Lflipboard/service/Section;

    iput-boolean p4, p0, Lflipboard/objs/FeedItem$5;->c:Z

    invoke-direct {p0}, Lflipboard/service/ServiceReloginObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1104
    iget-object v1, p0, Lflipboard/objs/FeedItem$5;->d:Lflipboard/objs/FeedItem;

    iget-boolean v0, p0, Lflipboard/objs/FeedItem$5;->c:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lflipboard/objs/FeedItem;->a(Z)V

    .line 1105
    iget-object v0, p0, Lflipboard/objs/FeedItem$5;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/ServiceReloginObserver;

    .line 1106
    if-eqz v0, :cond_0

    .line 1107
    invoke-virtual {v0, p1, p2, p3}, Lflipboard/service/ServiceReloginObserver;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 1109
    :cond_0
    return-void

    .line 1104
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lflipboard/json/FLObject;)V
    .locals 3

    .prologue
    .line 1077
    iget-object v0, p0, Lflipboard/objs/FeedItem$5;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/ServiceReloginObserver;

    .line 1078
    if-eqz v0, :cond_0

    .line 1079
    invoke-virtual {v0, p1}, Lflipboard/service/ServiceReloginObserver;->a(Lflipboard/json/FLObject;)V

    .line 1080
    iget-object v0, p0, Lflipboard/objs/FeedItem$5;->b:Lflipboard/service/Section;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->e(Z)V

    .line 1081
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "FeedItem:notifySuccess"

    new-instance v2, Lflipboard/objs/FeedItem$5$1;

    invoke-direct {v2, p0}, Lflipboard/objs/FeedItem$5$1;-><init>(Lflipboard/objs/FeedItem$5;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 1089
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1094
    iget-object v1, p0, Lflipboard/objs/FeedItem$5;->d:Lflipboard/objs/FeedItem;

    iget-boolean v0, p0, Lflipboard/objs/FeedItem$5;->c:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lflipboard/objs/FeedItem;->a(Z)V

    .line 1095
    iget-object v0, p0, Lflipboard/objs/FeedItem$5;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/ServiceReloginObserver;

    .line 1096
    if-eqz v0, :cond_0

    .line 1097
    invoke-virtual {v0, p1}, Lflipboard/service/ServiceReloginObserver;->a(Ljava/lang/String;)V

    .line 1099
    :cond_0
    return-void

    .line 1094
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
