.class public Lflipboard/objs/UserState$MutedAuthor;
.super Lflipboard/objs/UserState$TargetAuthor;
.source "UserState.java"


# instance fields
.field public a:Lflipboard/json/FLObject;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 332
    invoke-direct {p0}, Lflipboard/objs/UserState$TargetAuthor;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 338
    instance-of v1, p1, Lflipboard/objs/UserState$MutedAuthor;

    if-eqz v1, :cond_2

    .line 339
    check-cast p1, Lflipboard/objs/UserState$MutedAuthor;

    .line 341
    iget-object v1, p1, Lflipboard/objs/UserState$MutedAuthor;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lflipboard/objs/UserState$MutedAuthor;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lflipboard/objs/UserState$MutedAuthor;->d:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/objs/UserState$MutedAuthor;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lflipboard/util/JavaUtil;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 343
    iget-object v1, p1, Lflipboard/objs/UserState$MutedAuthor;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lflipboard/objs/UserState$MutedAuthor;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lflipboard/objs/UserState$MutedAuthor;->b:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/objs/UserState$MutedAuthor;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 351
    :cond_0
    :goto_0
    return v0

    .line 346
    :cond_1
    iget-object v1, p1, Lflipboard/objs/UserState$MutedAuthor;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lflipboard/objs/UserState$MutedAuthor;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lflipboard/objs/UserState$MutedAuthor;->c:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/objs/UserState$MutedAuthor;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lflipboard/util/JavaUtil;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 351
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
