.class public Lflipboard/objs/UserService;
.super Lflipboard/objs/Base;
.source "UserService.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Z

.field public j:Z

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/UserService$Cookie;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lflipboard/objs/Image;

.field public q:Lflipboard/objs/FeedSection;

.field public r:Z

.field public s:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/objs/UserService;->r:Z

    .line 79
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lflipboard/objs/UserService;->p:Lflipboard/objs/Image;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/UserService;->p:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lflipboard/objs/UserService;->p:Lflipboard/objs/Image;

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Lflipboard/objs/Image;

    invoke-direct {v0}, Lflipboard/objs/Image;-><init>()V

    iput-object v0, p0, Lflipboard/objs/UserService;->p:Lflipboard/objs/Image;

    .line 114
    :cond_0
    iget-object v0, p0, Lflipboard/objs/UserService;->p:Lflipboard/objs/Image;

    iput-object p1, v0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 55
    instance-of v0, p1, Lflipboard/objs/UserService;

    if-eqz v0, :cond_0

    .line 56
    check-cast p1, Lflipboard/objs/UserService;

    .line 58
    iget-object v0, p0, Lflipboard/objs/UserService;->a:Ljava/lang/String;

    iget-object v1, p1, Lflipboard/objs/UserService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/UserService;->p:Lflipboard/objs/Image;

    iget-object v1, p1, Lflipboard/objs/UserService;->p:Lflipboard/objs/Image;

    .line 59
    invoke-static {v0, v1}, Lflipboard/util/JavaUtil;->a(Lflipboard/objs/Image;Lflipboard/objs/Image;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    iget-object v1, p1, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    .line 60
    invoke-static {v0, v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/UserService;->c:Ljava/lang/String;

    iget-object v1, p1, Lflipboard/objs/UserService;->c:Ljava/lang/String;

    .line 61
    invoke-static {v0, v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/UserService;->d:Ljava/lang/String;

    iget-object v1, p1, Lflipboard/objs/UserService;->d:Ljava/lang/String;

    .line 62
    invoke-static {v0, v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/UserService;->s:Ljava/lang/String;

    iget-object v1, p1, Lflipboard/objs/UserService;->s:Ljava/lang/String;

    .line 63
    invoke-static {v0, v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    iget-object v1, p1, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    .line 64
    invoke-static {v0, v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    iget-object v1, p1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    .line 65
    invoke-static {v0, v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/UserService;->g:Ljava/lang/String;

    iget-object v1, p1, Lflipboard/objs/UserService;->g:Ljava/lang/String;

    .line 66
    invoke-static {v0, v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    iget-object v1, p1, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    .line 67
    invoke-static {v0, v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    iget-object v1, p1, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    .line 68
    invoke-static {v0, v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/UserService;->m:Ljava/lang/String;

    iget-object v1, p1, Lflipboard/objs/UserService;->m:Ljava/lang/String;

    .line 69
    invoke-static {v0, v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/UserService;->n:Ljava/lang/String;

    iget-object v1, p1, Lflipboard/objs/UserService;->n:Ljava/lang/String;

    .line 70
    invoke-static {v0, v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/objs/UserService;->r:Z

    iget-boolean v1, p1, Lflipboard/objs/UserService;->r:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lflipboard/objs/UserService;->i:Z

    iget-boolean v1, p1, Lflipboard/objs/UserService;->i:Z

    if-ne v0, v1, :cond_0

    .line 73
    const/4 v0, 0x1

    .line 76
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 34
    iget-object v0, p0, Lflipboard/objs/UserService;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/UserService;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/lit16 v0, v0, 0xb7

    .line 36
    mul-int/lit8 v3, v0, 0x3d

    iget-object v0, p0, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    .line 37
    mul-int/lit8 v3, v0, 0x3d

    iget-object v0, p0, Lflipboard/objs/UserService;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/objs/UserService;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v3

    .line 38
    mul-int/lit8 v3, v0, 0x3d

    iget-object v0, p0, Lflipboard/objs/UserService;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/objs/UserService;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v3

    .line 39
    mul-int/lit8 v3, v0, 0x3d

    iget-object v0, p0, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v3

    .line 40
    mul-int/lit8 v3, v0, 0x3d

    iget-object v0, p0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v3

    .line 41
    mul-int/lit8 v3, v0, 0x3d

    iget-object v0, p0, Lflipboard/objs/UserService;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lflipboard/objs/UserService;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v3

    .line 42
    mul-int/lit8 v3, v0, 0x3d

    iget-object v0, p0, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v0, v3

    .line 43
    mul-int/lit8 v3, v0, 0x3d

    iget-boolean v0, p0, Lflipboard/objs/UserService;->i:Z

    if-eqz v0, :cond_8

    move v0, v2

    :goto_8
    add-int/2addr v0, v3

    .line 44
    mul-int/lit8 v3, v0, 0x3d

    iget-object v0, p0, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_9
    add-int/2addr v0, v3

    .line 45
    mul-int/lit8 v3, v0, 0x3d

    iget-object v0, p0, Lflipboard/objs/UserService;->m:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lflipboard/objs/UserService;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_a
    add-int/2addr v0, v3

    .line 46
    mul-int/lit8 v3, v0, 0x3d

    iget-object v0, p0, Lflipboard/objs/UserService;->n:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lflipboard/objs/UserService;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_b
    add-int/2addr v0, v3

    .line 47
    mul-int/lit8 v3, v0, 0x3d

    iget-object v0, p0, Lflipboard/objs/UserService;->p:Lflipboard/objs/Image;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lflipboard/objs/UserService;->p:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->hashCode()I

    move-result v0

    :goto_c
    add-int/2addr v0, v3

    .line 48
    mul-int/lit8 v0, v0, 0x3d

    iget-boolean v3, p0, Lflipboard/objs/UserService;->r:Z

    if-eqz v3, :cond_d

    :goto_d
    add-int/2addr v0, v2

    .line 49
    return v0

    :cond_0
    move v0, v1

    .line 34
    goto/16 :goto_0

    :cond_1
    move v0, v1

    .line 36
    goto/16 :goto_1

    :cond_2
    move v0, v1

    .line 37
    goto/16 :goto_2

    :cond_3
    move v0, v1

    .line 38
    goto/16 :goto_3

    :cond_4
    move v0, v1

    .line 39
    goto :goto_4

    :cond_5
    move v0, v1

    .line 40
    goto :goto_5

    :cond_6
    move v0, v1

    .line 41
    goto :goto_6

    :cond_7
    move v0, v1

    .line 42
    goto :goto_7

    :cond_8
    move v0, v1

    .line 43
    goto :goto_8

    :cond_9
    move v0, v1

    .line 44
    goto :goto_9

    :cond_a
    move v0, v1

    .line 45
    goto :goto_a

    :cond_b
    move v0, v1

    .line 46
    goto :goto_b

    :cond_c
    move v0, v1

    .line 47
    goto :goto_c

    :cond_d
    move v2, v1

    .line 48
    goto :goto_d
.end method
