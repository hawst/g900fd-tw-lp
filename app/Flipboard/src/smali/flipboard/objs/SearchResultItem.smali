.class public Lflipboard/objs/SearchResultItem;
.super Ljava/lang/Object;
.source "SearchResultItem.java"


# static fields
.field public static e:Ljava/lang/String;

.field public static f:Ljava/lang/String;

.field public static g:Ljava/lang/String;

.field public static h:Ljava/lang/String;

.field public static i:Ljava/lang/String;

.field public static j:Ljava/lang/String;

.field public static k:Ljava/lang/String;

.field public static l:Ljava/lang/String;

.field public static m:Ljava/lang/String;

.field public static n:Ljava/lang/String;

.field public static o:Ljava/lang/String;

.field public static p:Ljava/lang/String;

.field public static q:Ljava/lang/String;


# instance fields
.field public A:Ljava/lang/String;

.field public B:Ljava/lang/String;

.field public C:Ljava/lang/String;

.field public D:Ljava/lang/String;

.field public E:F

.field public F:Ljava/lang/String;

.field public G:Ljava/lang/String;

.field public H:F

.field public I:Z

.field public J:Ljava/lang/String;

.field public K:Z

.field public L:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:F

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/Object;

.field public x:Ljava/lang/String;

.field public y:Ljava/lang/String;

.field public z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-string v0, "header_top"

    sput-object v0, Lflipboard/objs/SearchResultItem;->e:Ljava/lang/String;

    .line 9
    const-string v0, "header_sub"

    sput-object v0, Lflipboard/objs/SearchResultItem;->f:Ljava/lang/String;

    .line 10
    const-string v0, "collapsed_item"

    sput-object v0, Lflipboard/objs/SearchResultItem;->g:Ljava/lang/String;

    .line 11
    const-string v0, "top_result"

    sput-object v0, Lflipboard/objs/SearchResultItem;->h:Ljava/lang/String;

    .line 12
    const-string v0, "promote"

    sput-object v0, Lflipboard/objs/SearchResultItem;->i:Ljava/lang/String;

    .line 13
    const-string v0, "topic"

    sput-object v0, Lflipboard/objs/SearchResultItem;->j:Ljava/lang/String;

    .line 14
    const-string v0, "magazine"

    sput-object v0, Lflipboard/objs/SearchResultItem;->k:Ljava/lang/String;

    .line 15
    const-string v0, "source"

    sput-object v0, Lflipboard/objs/SearchResultItem;->l:Ljava/lang/String;

    .line 16
    const-string v0, "profile"

    sput-object v0, Lflipboard/objs/SearchResultItem;->m:Ljava/lang/String;

    .line 17
    const-string v0, "seemore"

    sput-object v0, Lflipboard/objs/SearchResultItem;->n:Ljava/lang/String;

    .line 18
    const-string v0, "3rd_party"

    sput-object v0, Lflipboard/objs/SearchResultItem;->o:Ljava/lang/String;

    .line 19
    const-string v0, "loading"

    sput-object v0, Lflipboard/objs/SearchResultItem;->p:Ljava/lang/String;

    .line 20
    const-string v0, "keyword_search"

    sput-object v0, Lflipboard/objs/SearchResultItem;->q:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
