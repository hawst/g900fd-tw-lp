.class public Lflipboard/objs/FeedSectionLink;
.super Lflipboard/objs/Base;
.source "FeedSectionLink.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Z

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Lflipboard/objs/Image;

.field public m:Ljava/lang/String;

.field public n:Z

.field public o:Z

.field public p:Ljava/lang/String;

.field public transient q:I

.field public transient r:Z

.field public s:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 38
    return-void
.end method

.method public constructor <init>(Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 59
    iget-object v0, p1, Lflipboard/objs/FeedItem;->d:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    .line 60
    iget-object v0, p1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    .line 61
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/objs/FeedSectionLink;->b(Ljava/lang/String;)V

    .line 64
    :cond_0
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->m:Z

    iput-boolean v0, p0, Lflipboard/objs/FeedSectionLink;->i:Z

    .line 65
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_1

    .line 67
    invoke-virtual {v0}, Lflipboard/service/Section;->s()Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/objs/FeedSectionLink;->o:Z

    .line 69
    :cond_1
    iget-object v0, p1, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/FeedSectionLink;->k:Ljava/lang/String;

    .line 70
    iget-object v0, p1, Lflipboard/objs/FeedItem;->by:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    .line 71
    iget-object v0, p1, Lflipboard/objs/FeedItem;->q:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    .line 72
    iget-object v0, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/FeedSectionLink;->b:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public constructor <init>(Lflipboard/service/Section;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 44
    iget-object v0, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    .line 45
    invoke-virtual {p1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    .line 46
    iget-object v0, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    invoke-virtual {v0}, Lflipboard/objs/TOCSection;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/objs/FeedSectionLink;->b(Ljava/lang/String;)V

    .line 47
    iget-object v0, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-boolean v0, v0, Lflipboard/objs/TOCSection;->s:Z

    iput-boolean v0, p0, Lflipboard/objs/FeedSectionLink;->i:Z

    .line 48
    invoke-virtual {p1}, Lflipboard/service/Section;->s()Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/objs/FeedSectionLink;->o:Z

    .line 49
    iget-object v0, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->J:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/FeedSectionLink;->k:Ljava/lang/String;

    .line 50
    invoke-virtual {p1}, Lflipboard/service/Section;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    .line 51
    iput-object p2, p0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    .line 52
    iget-object v0, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/FeedSectionLink;->b:Ljava/lang/String;

    .line 53
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    if-nez v0, :cond_0

    .line 115
    new-instance v0, Lflipboard/objs/Image;

    invoke-direct {v0}, Lflipboard/objs/Image;-><init>()V

    iput-object v0, p0, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    .line 117
    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    iput-object p1, v0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    .line 118
    return-void
.end method


# virtual methods
.method public final a()Lflipboard/objs/FeedSectionLink;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Lflipboard/objs/FeedSectionLink;

    invoke-direct {v0}, Lflipboard/objs/FeedSectionLink;-><init>()V

    .line 78
    iget-object v1, p0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    .line 79
    iget-object v1, p0, Lflipboard/objs/FeedSectionLink;->b:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->b:Ljava/lang/String;

    .line 80
    iget-object v1, p0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    .line 81
    iget-object v1, p0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    .line 82
    iget-object v1, p0, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    .line 83
    iget-object v1, p0, Lflipboard/objs/FeedSectionLink;->f:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->f:Ljava/lang/String;

    .line 84
    iget-object v1, p0, Lflipboard/objs/FeedSectionLink;->g:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->g:Ljava/lang/String;

    .line 85
    iget-object v1, p0, Lflipboard/objs/FeedSectionLink;->h:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->h:Ljava/lang/String;

    .line 86
    iget-boolean v1, p0, Lflipboard/objs/FeedSectionLink;->i:Z

    iput-boolean v1, v0, Lflipboard/objs/FeedSectionLink;->i:Z

    .line 87
    iget-object v1, p0, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    .line 88
    iget-object v1, p0, Lflipboard/objs/FeedSectionLink;->k:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->k:Ljava/lang/String;

    .line 89
    iget-object v1, p0, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    .line 90
    iget-object v1, p0, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    .line 91
    iget-boolean v1, p0, Lflipboard/objs/FeedSectionLink;->o:Z

    iput-boolean v1, v0, Lflipboard/objs/FeedSectionLink;->o:Z

    .line 93
    return-object v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    const-string v1, "magazine"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    const-string v1, "profile"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    const-string v1, "topic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lflipboard/objs/FeedSectionLink;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lflipboard/objs/FeedSectionLink;->f:Ljava/lang/String;

    .line 127
    :goto_0
    return-object v0

    .line 124
    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p0, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 127
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 137
    if-ne p0, p1, :cond_1

    .line 149
    :cond_0
    :goto_0
    return v0

    .line 140
    :cond_1
    instance-of v2, p1, Lflipboard/objs/FeedSectionLink;

    if-nez v2, :cond_2

    move v0, v1

    .line 141
    goto :goto_0

    .line 144
    :cond_2
    check-cast p1, Lflipboard/objs/FeedSectionLink;

    .line 146
    iget-object v2, p0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 147
    iget-object v2, p0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    iget-object v3, p1, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 149
    goto :goto_0
.end method
