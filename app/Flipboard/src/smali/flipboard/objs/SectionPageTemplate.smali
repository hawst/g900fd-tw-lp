.class public Lflipboard/objs/SectionPageTemplate;
.super Lflipboard/objs/Base;
.source "SectionPageTemplate.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:F

.field public i:Z

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SectionPageTemplate$Area;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SectionPageTemplate$Area;",
            ">;"
        }
    .end annotation
.end field

.field public l:Z

.field public m:Z

.field public n:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const v0, 0x7fffffff

    .line 7
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 14
    iput v0, p0, Lflipboard/objs/SectionPageTemplate;->f:I

    .line 15
    iput v0, p0, Lflipboard/objs/SectionPageTemplate;->g:I

    .line 70
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lflipboard/objs/SectionPageTemplate;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(Z)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SectionPageTemplate$Area;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    if-nez p1, :cond_0

    iget-object v0, p0, Lflipboard/objs/SectionPageTemplate;->k:Ljava/util/List;

    if-nez v0, :cond_1

    .line 64
    :cond_0
    iget-object v0, p0, Lflipboard/objs/SectionPageTemplate;->j:Ljava/util/List;

    .line 66
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lflipboard/objs/SectionPageTemplate;->k:Ljava/util/List;

    goto :goto_0
.end method
