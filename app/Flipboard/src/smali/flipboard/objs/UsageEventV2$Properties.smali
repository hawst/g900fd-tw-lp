.class public Lflipboard/objs/UsageEventV2$Properties;
.super Ljava/lang/Object;
.source "UsageEventV2.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:J

.field public e:I

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    return-void
.end method

.method public constructor <init>(Lflipboard/service/FlipboardManager;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246
    iget-object v0, p1, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/UsageEventV2$Properties;->a:Ljava/lang/String;

    .line 247
    sget-object v0, Lflipboard/usage/UsageManagerV2;->b:Lflipboard/usage/UsageManagerV2;

    iget-object v0, v0, Lflipboard/usage/UsageManagerV2;->q:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/UsageEventV2$Properties;->b:Ljava/lang/String;

    .line 248
    :try_start_0
    iget-object v0, p1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 255
    iput v0, p0, Lflipboard/objs/UsageEventV2$Properties;->c:I

    .line 257
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/objs/UsageEventV2$Properties;->d:J

    .line 258
    sget-object v0, Lflipboard/usage/UsageManagerV2;->b:Lflipboard/usage/UsageManagerV2;

    iget v0, v0, Lflipboard/usage/UsageManagerV2;->i:I

    iput v0, p0, Lflipboard/objs/UsageEventV2$Properties;->e:I

    .line 259
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/objs/UsageEventV2$Properties;->f:Ljava/lang/String;

    .line 260
    const-string v0, "android"

    iput-object v0, p0, Lflipboard/objs/UsageEventV2$Properties;->g:Ljava/lang/String;

    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/objs/UsageEventV2$Properties;->h:Ljava/lang/String;

    .line 262
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/UsageEventV2$Properties;->i:Ljava/lang/String;

    .line 263
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/UsageEventV2$Properties;->j:Ljava/lang/String;

    .line 264
    iget-object v0, p1, Lflipboard/service/FlipboardManager;->f:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/UsageEventV2$Properties;->k:Ljava/lang/String;

    .line 265
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 266
    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lflipboard/objs/UsageEventV2$Properties;->l:Ljava/lang/String;

    .line 267
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/objs/UsageEventV2$Properties;->m:Ljava/lang/String;

    .line 268
    invoke-virtual {p1}, Lflipboard/service/FlipboardManager;->H()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/objs/UsageEventV2$Properties;->n:Ljava/lang/String;

    .line 269
    invoke-static {}, Lflipboard/abtest/Experiments;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/objs/UsageEventV2$Properties;->o:Ljava/lang/String;

    .line 270
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_0

    .line 271
    const-string v0, "china"

    iput-object v0, p0, Lflipboard/objs/UsageEventV2$Properties;->p:Ljava/lang/String;

    .line 273
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->aj:Lflipboard/objs/ConfigEdition;

    if-eqz v0, :cond_1

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->aj:Lflipboard/objs/ConfigEdition;

    iget-object v0, v0, Lflipboard/objs/ConfigEdition;->b:Ljava/lang/String;

    :goto_1
    iput-object v0, p0, Lflipboard/objs/UsageEventV2$Properties;->q:Ljava/lang/String;

    .line 275
    return-void

    .line 251
    :catch_0
    move-exception v0

    .line 252
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 253
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 255
    iput v2, p0, Lflipboard/objs/UsageEventV2$Properties;->c:I

    goto :goto_0

    :catchall_0
    move-exception v0

    iput v2, p0, Lflipboard/objs/UsageEventV2$Properties;->c:I

    throw v0

    .line 273
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method
