.class public Lflipboard/objs/FeedItem$4;
.super Ljava/lang/Object;
.source "FeedItem.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/objs/FeedItem;


# direct methods
.method public constructor <init>(Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 1011
    iput-object p1, p0, Lflipboard/objs/FeedItem$4;->a:Lflipboard/objs/FeedItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1016
    iget-object v0, p0, Lflipboard/objs/FeedItem$4;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->am()V

    .line 1017
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v3, p0, Lflipboard/objs/FeedItem$4;->a:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v3

    .line 1018
    if-eqz v3, :cond_2

    .line 1019
    iget-object v0, p0, Lflipboard/objs/FeedItem$4;->a:Lflipboard/objs/FeedItem;

    iget-object v4, v0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    if-eqz v3, :cond_3

    iget-object v0, v4, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    const-string v6, "share"

    iget-object v7, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->a:Ljava/lang/String;

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->e:Ljava/lang/String;

    invoke-virtual {v3, v6, v0}, Lflipboard/service/Account;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eq v1, v0, :cond_1

    new-instance v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    invoke-direct {v0}, Lflipboard/objs/CommentaryResult$Item$Commentary;-><init>()V

    const-string v1, "share"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5, v3}, Lflipboard/objs/CommentaryResult$Item$Commentary;->a(Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Account;)Lflipboard/objs/CommentaryResult$Item$Commentary;

    move-result-object v0

    iget-object v1, v4, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget v0, v4, Lflipboard/objs/CommentaryResult$Item;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v4, Lflipboard/objs/CommentaryResult$Item;->d:I

    .line 1020
    :cond_1
    iget-object v0, p0, Lflipboard/objs/FeedItem$4;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->an()V

    .line 1022
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 1019
    goto :goto_0
.end method
