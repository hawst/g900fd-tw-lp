.class public Lflipboard/objs/RecentImage;
.super Ljava/lang/Object;
.source "RecentImage.java"


# instance fields
.field public final a:J

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lflipboard/objs/RecentImage;->b:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lflipboard/objs/RecentImage;->c:Ljava/lang/String;

    .line 20
    iput-object p3, p0, Lflipboard/objs/RecentImage;->d:Ljava/lang/String;

    .line 21
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/objs/RecentImage;->a:J

    .line 22
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 63
    .line 66
    iget-object v0, p0, Lflipboard/objs/RecentImage;->b:Ljava/lang/String;

    .line 67
    if-eqz v0, :cond_6

    .line 68
    invoke-static {v0}, Lflipboard/util/HttpUtil;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lflipboard/util/HttpUtil;->a(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 69
    if-eqz v2, :cond_0

    const-string v3, "gif"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const/16 v2, 0x320

    if-lt p1, v2, :cond_6

    .line 73
    :cond_1
    :goto_0
    const/16 v1, 0xc8

    if-le p1, v1, :cond_2

    if-nez v0, :cond_2

    .line 74
    iget-object v0, p0, Lflipboard/objs/RecentImage;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/objs/RecentImage;->c:Ljava/lang/String;

    .line 76
    :cond_2
    :goto_1
    if-nez v0, :cond_3

    .line 77
    iget-object v0, p0, Lflipboard/objs/RecentImage;->d:Ljava/lang/String;

    .line 79
    :cond_3
    return-object v0

    .line 74
    :cond_4
    iget-object v0, p0, Lflipboard/objs/RecentImage;->d:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflipboard/objs/RecentImage;->d:Ljava/lang/String;

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lflipboard/objs/RecentImage;->b:Ljava/lang/String;

    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_0
.end method
