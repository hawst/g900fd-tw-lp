.class public Lflipboard/objs/UserState;
.super Lflipboard/objs/FlapObjectResult;
.source "UserState.java"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field public i:I

.field public j:Lflipboard/objs/UserState$State;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public transient m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-string v0, "user"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lflipboard/objs/UserState;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lflipboard/objs/FlapObjectResult;-><init>()V

    .line 24
    sget-object v0, Lflipboard/objs/UserState;->a:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/UserState;->k:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public constructor <init>(Lflipboard/objs/UserInfo;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Lflipboard/objs/FlapObjectResult;-><init>()V

    .line 24
    sget-object v0, Lflipboard/objs/UserState;->a:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/UserState;->k:Ljava/lang/String;

    .line 44
    invoke-virtual {p1}, Lflipboard/objs/UserInfo;->a()Lflipboard/objs/UserInfo;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/UserInfo;->l:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserState$State;

    iput-object v0, p0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    .line 45
    return-void
.end method

.method public constructor <init>(Lflipboard/objs/UserState;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Lflipboard/objs/FlapObjectResult;-><init>()V

    .line 24
    sget-object v0, Lflipboard/objs/UserState;->a:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/objs/UserState;->k:Ljava/lang/String;

    .line 31
    new-instance v1, Lflipboard/objs/UserState$State;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {v1, v0}, Lflipboard/objs/UserState$State;-><init>(Lflipboard/objs/UserState$State;)V

    iput-object v1, p0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    .line 32
    return-void

    .line 31
    :cond_0
    iget-object v0, p1, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v1, v0, Lflipboard/objs/UserState$State;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, v0, Lflipboard/objs/UserState$State;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method
