.class public Lflipboard/objs/UserInfo;
.super Lflipboard/objs/FlapObjectResult;
.source "UserInfo.java"


# instance fields
.field public a:I

.field public i:Ljava/lang/String;

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/UserService;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/UserService;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/UserState$State;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lflipboard/objs/UserInfo;

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Magazine;",
            ">;"
        }
    .end annotation
.end field

.field public o:Z

.field public p:Lflipboard/json/FLObject;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Lflipboard/objs/FlapObjectResult;-><init>()V

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/objs/UserInfo;->o:Z

    return-void
.end method


# virtual methods
.method public final a()Lflipboard/objs/UserInfo;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lflipboard/objs/UserInfo;->m:Lflipboard/objs/UserInfo;

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    iget-object p0, p0, Lflipboard/objs/UserInfo;->m:Lflipboard/objs/UserInfo;

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lflipboard/json/FLObject;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/objs/UserInfo;->m:Lflipboard/objs/UserInfo;

    iget-object v0, v0, Lflipboard/objs/UserInfo;->l:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserState$State;

    invoke-virtual {v0, p1}, Lflipboard/objs/UserState$State;->a(Ljava/util/List;)V

    .line 37
    return-void
.end method
