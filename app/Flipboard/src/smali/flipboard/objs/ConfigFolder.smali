.class public Lflipboard/objs/ConfigFolder;
.super Lflipboard/objs/ContentDrawerListItemBase;
.source "ConfigFolder.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Z

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigSection;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lflipboard/objs/ContentDrawerListItemBase;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x3

    return v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    iput-object p1, p0, Lflipboard/objs/ConfigFolder;->bT:Ljava/util/List;

    .line 34
    return-void
.end method

.method public final c()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lflipboard/objs/ConfigFolder;->bT:Ljava/util/List;

    if-nez v0, :cond_0

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/objs/ConfigFolder;->bT:Ljava/util/List;

    .line 23
    iget-object v0, p0, Lflipboard/objs/ConfigFolder;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lflipboard/objs/ConfigFolder;->bT:Ljava/util/List;

    iget-object v1, p0, Lflipboard/objs/ConfigFolder;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 27
    :cond_0
    iget-object v0, p0, Lflipboard/objs/ConfigFolder;->bT:Ljava/util/List;

    return-object v0
.end method
