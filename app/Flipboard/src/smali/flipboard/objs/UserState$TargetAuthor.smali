.class public Lflipboard/objs/UserState$TargetAuthor;
.super Lflipboard/objs/Base;
.source "UserState.java"


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 307
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 316
    instance-of v1, p1, Lflipboard/objs/UserState$TargetAuthor;

    if-eqz v1, :cond_2

    .line 317
    check-cast p1, Lflipboard/objs/UserState$TargetAuthor;

    .line 319
    iget-object v1, p1, Lflipboard/objs/UserState$TargetAuthor;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lflipboard/objs/UserState$TargetAuthor;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lflipboard/objs/UserState$TargetAuthor;->d:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/objs/UserState$TargetAuthor;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lflipboard/util/JavaUtil;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 321
    iget-object v1, p1, Lflipboard/objs/UserState$TargetAuthor;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lflipboard/objs/UserState$TargetAuthor;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lflipboard/objs/UserState$TargetAuthor;->b:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/objs/UserState$TargetAuthor;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 329
    :cond_0
    :goto_0
    return v0

    .line 324
    :cond_1
    iget-object v1, p1, Lflipboard/objs/UserState$TargetAuthor;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lflipboard/objs/UserState$TargetAuthor;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lflipboard/objs/UserState$TargetAuthor;->c:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/objs/UserState$TargetAuthor;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lflipboard/util/JavaUtil;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 329
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
