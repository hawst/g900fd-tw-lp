.class public abstract Lflipboard/objs/HasCommentaryItem;
.super Lflipboard/objs/Base;
.source "HasCommentaryItem.java"


# static fields
.field static final synthetic cw:Z


# instance fields
.field public transient ct:J

.field public transient cu:Lflipboard/objs/CommentaryResult$Item;

.field protected volatile transient cv:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lflipboard/objs/HasCommentaryItem;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lflipboard/objs/HasCommentaryItem;->cw:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 35
    return-void
.end method


# virtual methods
.method public abstract a(Lflipboard/objs/CommentaryResult$Item;)V
.end method

.method public abstract a(Lflipboard/objs/CommentaryResult$Item;J)V
.end method

.method public a(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lflipboard/objs/HasCommentaryItem;->cv:Ljava/util/List;

    if-nez v0, :cond_1

    .line 50
    monitor-enter p0

    .line 51
    :try_start_0
    iget-object v0, p0, Lflipboard/objs/HasCommentaryItem;->cv:Ljava/util/List;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/objs/HasCommentaryItem;->cv:Ljava/util/List;

    .line 54
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    :cond_1
    iget-object v0, p0, Lflipboard/objs/HasCommentaryItem;->cv:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    return-void

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final am()V
    .locals 1

    .prologue
    .line 27
    sget-boolean v0, Lflipboard/objs/HasCommentaryItem;->cw:Z

    if-nez v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->h()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 28
    :cond_0
    iget-object v0, p0, Lflipboard/objs/HasCommentaryItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    if-nez v0, :cond_1

    .line 29
    invoke-static {}, Lflipboard/objs/CommentaryResult$Item;->a()Lflipboard/objs/CommentaryResult$Item;

    move-result-object v0

    iput-object v0, p0, Lflipboard/objs/HasCommentaryItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    .line 31
    :cond_1
    return-void
.end method

.method protected final an()V
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lflipboard/objs/HasCommentaryItem;->cv:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lflipboard/objs/HasCommentaryItem;->cv:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 78
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 79
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;

    .line 80
    if-eqz v1, :cond_0

    .line 81
    invoke-interface {v1, p0}, Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;->a(Lflipboard/objs/HasCommentaryItem;)V

    goto :goto_0

    .line 83
    :cond_0
    iget-object v1, p0, Lflipboard/objs/HasCommentaryItem;->cv:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 87
    :cond_1
    return-void
.end method

.method public b(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lflipboard/objs/HasCommentaryItem;->cv:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 63
    iget-object v0, p0, Lflipboard/objs/HasCommentaryItem;->cv:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 64
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 65
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 66
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;

    .line 67
    if-eqz v0, :cond_1

    if-ne v0, p1, :cond_0

    .line 68
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 72
    :cond_2
    return-void
.end method
