.class public Lflipboard/model/FirstLaunchTopicInfo;
.super Lflipboard/json/JsonSerializable;
.source "FirstLaunchTopicInfo.java"


# instance fields
.field public alwaysFirstTrancheTopics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;",
            ">;"
        }
    .end annotation
.end field

.field public secondTrancheTopics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;",
            ">;"
        }
    .end annotation
.end field

.field public thirdTrancheTopics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;",
            ">;"
        }
    .end annotation
.end field

.field public topicHierarchy:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lflipboard/json/JsonSerializable;-><init>()V

    .line 16
    return-void
.end method
