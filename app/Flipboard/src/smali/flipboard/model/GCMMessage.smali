.class public Lflipboard/model/GCMMessage;
.super Ljava/lang/Object;
.source "GCMMessage.java"


# static fields
.field public static log:Lflipboard/util/Log;


# instance fields
.field public final actionURL:Ljava/lang/String;

.field public final dateSent:J

.field public final expireAt:J

.field public final flabCellId:Ljava/lang/String;

.field public final flabExperimentId:Ljava/lang/String;

.field public final group:Lflipboard/model/GCMMessage$Group;

.field public final ignoreUid:Ljava/lang/String;

.field public final largeImage:Ljava/lang/String;

.field public final message:Ljava/lang/String;

.field public final smallImage:Ljava/lang/String;

.field public final uid:Ljava/lang/String;

.field public final usageEventType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-string v0, "notification"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/model/GCMMessage;->log:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const-string v0, "uid"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/model/GCMMessage;->uid:Ljava/lang/String;

    .line 58
    const-string v0, "alert"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/model/GCMMessage;->message:Ljava/lang/String;

    .line 59
    const-string v0, "actionURL"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/model/GCMMessage;->actionURL:Ljava/lang/String;

    .line 60
    const-string v0, "ignoreUid"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/model/GCMMessage;->ignoreUid:Ljava/lang/String;

    .line 61
    const-string v0, "smallImage"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/model/GCMMessage;->smallImage:Ljava/lang/String;

    .line 62
    const-string v0, "largeImage"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/model/GCMMessage;->largeImage:Ljava/lang/String;

    .line 65
    :try_start_0
    const-string v0, "dateSent"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 69
    :goto_0
    iput-wide v0, p0, Lflipboard/model/GCMMessage;->dateSent:J

    .line 70
    const-string v0, "usage_event_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/model/GCMMessage;->usageEventType:Ljava/lang/String;

    .line 71
    const-string v0, "flab_cell_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/model/GCMMessage;->flabCellId:Ljava/lang/String;

    .line 72
    const-string v0, "flab_experiment_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/model/GCMMessage;->flabExperimentId:Ljava/lang/String;

    .line 75
    :try_start_1
    const-string v0, "expireAt"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v2

    .line 79
    :goto_1
    iput-wide v2, p0, Lflipboard/model/GCMMessage;->expireAt:J

    .line 81
    const-string v0, "group"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_0

    .line 83
    const-class v1, Lflipboard/model/GCMMessage$Group;

    invoke-static {v0, v1}, Lflipboard/json/JsonSerializationWrapper;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/GCMMessage$Group;

    iput-object v0, p0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    .line 87
    :goto_2
    return-void

    .line 66
    :catch_0
    move-exception v0

    .line 67
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    move-wide v0, v2

    goto :goto_0

    .line 76
    :catch_1
    move-exception v0

    .line 77
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 85
    :cond_0
    new-instance v0, Lflipboard/model/GCMMessage$Group;

    invoke-direct {v0}, Lflipboard/model/GCMMessage$Group;-><init>()V

    iput-object v0, p0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    goto :goto_2
.end method


# virtual methods
.method public getTitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 90
    const/4 v0, 0x0

    .line 91
    invoke-virtual {p0}, Lflipboard/model/GCMMessage;->isOfKnownType()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 92
    iget-object v1, p0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    iget-object v4, v1, Lflipboard/model/GCMMessage$Group;->type:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 103
    :goto_1
    return-object v0

    .line 92
    :sswitch_0
    const-string v5, "followedYou"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v2

    goto :goto_0

    :sswitch_1
    const-string v5, "sharedWithYou"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v3

    goto :goto_0

    .line 94
    :pswitch_0
    const v0, 0x7f0d022a

    new-array v1, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    iget-object v3, v3, Lflipboard/model/GCMMessage$Group;->actor:Lflipboard/model/GCMMessage$Group$Actor;

    iget-object v3, v3, Lflipboard/model/GCMMessage$Group$Actor;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 97
    :pswitch_1
    const v0, 0x7f0d022d

    new-array v1, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    iget-object v3, v3, Lflipboard/model/GCMMessage$Group;->actor:Lflipboard/model/GCMMessage$Group$Actor;

    iget-object v3, v3, Lflipboard/model/GCMMessage$Group$Actor;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 101
    :cond_1
    iget-object v0, p0, Lflipboard/model/GCMMessage;->message:Ljava/lang/String;

    goto :goto_1

    .line 92
    :sswitch_data_0
    .sparse-switch
        0x3f56cd54 -> :sswitch_1
        0x5f7781ef -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isOfKnownType()Z
    .locals 3

    .prologue
    .line 107
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "followedYou"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "sharedWithYou"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lflipboard/model/GCMMessage;->group:Lflipboard/model/GCMMessage$Group;

    iget-object v1, v1, Lflipboard/model/GCMMessage$Group;->type:Ljava/lang/String;

    .line 110
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
