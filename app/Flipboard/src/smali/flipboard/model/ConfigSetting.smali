.class public Lflipboard/model/ConfigSetting;
.super Lflipboard/objs/Base;
.source "ConfigSetting.java"


# instance fields
.field public AccountHelpURLString:Ljava/lang/String;

.field public ActiveUserHasAccount:Z

.field public ActiveUserMinimumAppUsesLastPeriod:I

.field public ActiveUserMinimumAppUsesPeriod:I

.field public ActiveUserMinimumMagazineCount:I

.field public ActivityRequestMaxItemsCount:I

.field public AdvertiseReferer:Z

.field public AdvertiseUserAgent:Z

.field public AllowBitmapRegionDecoder:Z

.field public AllowFourInchRetinaSnapshots:Z

.field public AllowMultiCoreRetinaSnapshots:Z

.field public AllowRetinaPadFlipmagPreloadedSnapshots:Z

.field public AllowRetinaPadFlipmagSnapshots:Z

.field public AllowRetinaPadSnapshots:Z

.field public AllowSingleCoreFlipmagRetinaSnapshots:Z

.field public AlsoFlippedUserSampleSize:F

.field public AppDownloadURL:Ljava/lang/String;

.field public AppDownloadURLChina:Ljava/lang/String;

.field public AppID:Ljava/lang/String;

.field public AppLatestVersion:Ljava/lang/String;

.field public AppMinimumVersion:Ljava/lang/String;

.field public AppUpdateAlertMessage:Ljava/lang/String;

.field public AppUpdateAlertTitle:Ljava/lang/String;

.field public AppUpdateRequiredAlertMessage:Ljava/lang/String;

.field public AppUpdateRequiredAlertTitle:Ljava/lang/String;

.field public AudioPlayerPlaybackButtonLifetime:F

.field public BetaUsageV2Host:Ljava/lang/String;

.field public ConditionalFirstLaunchSections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/model/ConditionalSection;",
            ">;"
        }
    .end annotation
.end field

.field public CoverStoriesFeedIdentifier:Ljava/lang/String;

.field public DaydreamFeedFetchInterval:I

.field public DaydreamFeedFetchIntervalMax:I

.field public DefaultTextSizeOverridesForDeviceVersions:Lflipboard/json/FLObject;

.field public DisableContentSearch:Z

.field public DisableFirstLaunchLightboxes:Z

.field public DisableMagazineLikes:Z

.field public DisablePeerToPeerSharing:Z

.field public DisableShareAddedSections:Z

.field public DisplayRateMeOnlyIfHappyUser:Z

.field public DomainBlacklist:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public EmbeddedHTML5VideoFormatString:Ljava/lang/String;

.field public EmbeddedVideoHTMLFormatStrings:Lflipboard/model/ConfigSetting$EmbeddedVideoHTMLFormats;

.field public EmbeddedVimeoHTMLFormatString:Ljava/lang/String;

.field public EmbeddedYoutubeHTMLFormatString:Ljava/lang/String;

.field public EnableCoverStoriesInterstitials:Z

.field public EnableHelpshift:Z

.field public FacebookSingleSignOnPermissions:Ljava/lang/String;

.field public FacebookSingleSignOnPublishPermissions:Ljava/lang/String;

.field public FacebookSingleSignOnReadPermissions:Ljava/lang/String;

.field public FeedFetchInitialItemCount:I

.field public FeedFetchLibraryTimeoutInterval:I

.field public FeedFetchLoadMoreItemCount:I

.field public FeedFetchLoadMorePagesFromEndCount:I

.field public FeedFetchTimeoutInterval:F

.field public FeedTemplateCSSURLString:Ljava/lang/String;

.field public FeedTemplateCSSURLStringLarge:Ljava/lang/String;

.field public FeedTemplateCSSURLStringXLarge:Ljava/lang/String;

.field public FeedTemplateHTMLURLString:Ljava/lang/String;

.field public FirstFlipsPerDayLimit:I

.field public FirstRunNotificationEnabled:Z

.field public FirstRunNotificationInitialDelay:J

.field public FirstRunNotificationMaxTimes:I

.field public FirstRunNotificationRepeatDelay:J

.field public FirstRunSurveyNotificationEnabled:Z

.field public FirstRunSurveyNotificationUrl:Ljava/lang/String;

.field public FlipItLaterFeedIdentifier:Ljava/lang/String;

.field public FlipboardAccountPullInterval:I

.field public FlipboardAccountRequiredFromFDL:Z

.field public FlipboardCDNHost:Ljava/lang/String;

.field public FlipmagBaseURLMatchString:Ljava/lang/String;

.field public ForceHideFlipButton:Z

.field public ForcedTOCReloadInterval:F

.field public GoogleNotificationSenderID:Ljava/lang/String;

.field public GoogleReaderDisabled:Z

.field public GoogleReaderMaxDisplayedUnreadCount:I

.field public HappyUserHasOwnLocale:Z

.field public HappyUserMinimumConnectedServices:I

.field public HappyUserMinimumDaysSinceLastCrash:I

.field public HappyUserMinimumDetailViewsSinceLastCrash:I

.field public HappyUserMinimumFlipsSinceLastCrash:I

.field public HeartbeatInterval:F

.field public HelpURLString:Ljava/lang/String;

.field public InStoreLightboxURL:Ljava/lang/String;

.field public InactiveAutomaticReloadInterval:F

.field public InactiveInStoreResetInterval:F

.field public InfoPageHTML:Ljava/lang/String;

.field public InfoPageLandscapeHTML:Ljava/lang/String;

.field public InfoPageMaxCount:I

.field public InfoPageVersion:I

.field public InlineAMLSampleRate:F

.field public LogStartFlipTime:Z

.field public MagazineCategories:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/model/ConfigSetting$MagazineCategory;",
            ">;"
        }
    .end annotation
.end field

.field public MagazineEnabled:Z

.field public MagazineFetchInterval:J

.field public MagazinesHelpURLString:Ljava/lang/String;

.field public MarkLastPageReadDelay:F

.field public MaxNumberEmailsPerLookupRequest:I

.field public MaxSavedItemCount:I

.field public MaxUpdateAlerts:I

.field public MediumSearchDelayInterval:F

.field public MinLaunchesToDisplayRateMe:I

.field public MinLaunchesToDisplayUpdate:I

.field public MinTimeToDisplayRateMe:F

.field public MinTimeToDisplayRateMeAfterRateLater:F

.field public MinimumTopicPickerCount:I

.field public ModifyUserAgentForTabletServiceLogin:Z

.field public NYTSubscribeLinkKindle:Ljava/lang/String;

.field public NanoInlineAML:Z

.field public NotificationIdRefreshOnServerInterval:J

.field public NotificationsEnabled:Z

.field public PauseNetworkAfterBackgroundedDelay:J

.field public PauseNetworkAfterBackgroundedDelayWifi:J

.field public PerformanceUsageSample:F

.field public PhotoSaveDomainBlacklist:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public PrivacyPolicyURLString:Ljava/lang/String;

.field public PushNotificationSettings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public PushNotificationSettingsDefaults:Lflipboard/json/FLObject;

.field public RecycleBitmapRegionDecoder:Z

.field public RefetchSectionsAndConfigJSONBackgroundDuration:I

.field public SavedSectionStateValidTime:I

.field public SectionPickerFlipboardIconImageURLString:Ljava/lang/String;

.field public SectionPickerFolderIconImageURLString:Ljava/lang/String;

.field public ShortenedURLCharacterCount:I

.field public SystemFontLanguages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public TermsOfUseURLString:Ljava/lang/String;

.field public TopLevelAccountSubsectionMaximumAge:I

.field public TopStoriesLayoutFrequency:I

.field public TopStoriesRequestCount:I

.field public TopicCountryCodes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public TopicLanguageCodes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public TopicLocales:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public TrustedWebViewDomainPattern:Ljava/lang/String;

.field public UsageHost:Ljava/lang/String;

.field public UsageSendFirstInterval:F

.field public UsageSendRepeatInterval:F

.field public UsageSessionRefreshInterval:F

.field public UsageV2Host:Ljava/lang/String;

.field public WebViewRefererString:Ljava/lang/String;

.field public WidgetLogoHintEnabled:Z

.field public allowBackFillForSocialFollow:Z

.field public allowMetaDataRequestForSections:Z

.field public forceUpgradeHiddenLauncherIconBuild:Z

.field public forceUpgradeHiddenLauncherIconBuildAfterViewSectionOrItemCount:I

.field public geoCountryCode:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/model/ConfigSetting$GeoCountryCode;",
            ">;"
        }
    .end annotation
.end field

.field public idioms:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/model/ConfigSetting;",
            ">;"
        }
    .end annotation
.end field

.field public stores:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/model/ConfigSetting;",
            ">;"
        }
    .end annotation
.end field

.field public variants:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/model/ConfigSetting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x7

    const/4 v5, 0x2

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 17
    invoke-direct {p0}, Lflipboard/objs/Base;-><init>()V

    .line 19
    iput-boolean v2, p0, Lflipboard/model/ConfigSetting;->allowMetaDataRequestForSections:Z

    .line 20
    iput-boolean v2, p0, Lflipboard/model/ConfigSetting;->allowBackFillForSocialFollow:Z

    .line 44
    const/16 v0, 0xa

    iput v0, p0, Lflipboard/model/ConfigSetting;->FeedFetchInitialItemCount:I

    .line 45
    const/16 v0, 0x14

    iput v0, p0, Lflipboard/model/ConfigSetting;->FeedFetchLoadMoreItemCount:I

    .line 46
    const/16 v0, 0x1e

    iput v0, p0, Lflipboard/model/ConfigSetting;->MaxSavedItemCount:I

    .line 47
    const/16 v0, 0xa

    iput v0, p0, Lflipboard/model/ConfigSetting;->FeedFetchLoadMorePagesFromEndCount:I

    .line 68
    iput-boolean v2, p0, Lflipboard/model/ConfigSetting;->EnableHelpshift:Z

    .line 70
    const-string v0, ""

    iput-object v0, p0, Lflipboard/model/ConfigSetting;->TermsOfUseURLString:Ljava/lang/String;

    .line 71
    const-string v0, ""

    iput-object v0, p0, Lflipboard/model/ConfigSetting;->PrivacyPolicyURLString:Ljava/lang/String;

    .line 78
    iput-boolean v2, p0, Lflipboard/model/ConfigSetting;->DisableMagazineLikes:Z

    .line 81
    iput-boolean v2, p0, Lflipboard/model/ConfigSetting;->EnableCoverStoriesInterstitials:Z

    .line 82
    iput v5, p0, Lflipboard/model/ConfigSetting;->FirstFlipsPerDayLimit:I

    .line 86
    const v0, 0x3e4ccccd    # 0.2f

    iput v0, p0, Lflipboard/model/ConfigSetting;->AlsoFlippedUserSampleSize:F

    .line 90
    const-string v0, "publish_stream,read_stream,user_photos,friends_photos,user_likes,user_groups,read_friendlists,manage_pages,email"

    iput-object v0, p0, Lflipboard/model/ConfigSetting;->FacebookSingleSignOnPermissions:Ljava/lang/String;

    .line 91
    const-string v0, "read_stream,user_photos,friends_photos,user_likes,user_groups,read_friendlists,email"

    iput-object v0, p0, Lflipboard/model/ConfigSetting;->FacebookSingleSignOnReadPermissions:Ljava/lang/String;

    .line 92
    const-string v0, "publish_stream,manage_pages"

    iput-object v0, p0, Lflipboard/model/ConfigSetting;->FacebookSingleSignOnPublishPermissions:Ljava/lang/String;

    .line 95
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "www.500px.com"

    aput-object v1, v0, v3

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/model/ConfigSetting;->PhotoSaveDomainBlacklist:Ljava/util/List;

    .line 98
    const-string v0, "https://ue.flipboard.com/usage"

    iput-object v0, p0, Lflipboard/model/ConfigSetting;->UsageV2Host:Ljava/lang/String;

    .line 99
    const-string v0, "https://ue-test.flipboard.com/usage"

    iput-object v0, p0, Lflipboard/model/ConfigSetting;->BetaUsageV2Host:Ljava/lang/String;

    .line 101
    iput v4, p0, Lflipboard/model/ConfigSetting;->InlineAMLSampleRate:F

    .line 121
    iput-boolean v2, p0, Lflipboard/model/ConfigSetting;->NotificationsEnabled:Z

    .line 128
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lflipboard/model/ConfigSetting;->MagazineFetchInterval:J

    .line 129
    const-string v0, "334069016917"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/model/ConfigSetting;->GoogleNotificationSenderID:Ljava/lang/String;

    .line 133
    const/16 v0, 0x258

    iput v0, p0, Lflipboard/model/ConfigSetting;->DaydreamFeedFetchInterval:I

    .line 134
    const/16 v0, 0x1c20

    iput v0, p0, Lflipboard/model/ConfigSetting;->DaydreamFeedFetchIntervalMax:I

    .line 140
    const/16 v0, 0x64

    iput v0, p0, Lflipboard/model/ConfigSetting;->FeedFetchLibraryTimeoutInterval:I

    .line 141
    iput-boolean v3, p0, Lflipboard/model/ConfigSetting;->DisableFirstLaunchLightboxes:Z

    .line 143
    iput-boolean v3, p0, Lflipboard/model/ConfigSetting;->ForceHideFlipButton:Z

    .line 144
    iput-boolean v3, p0, Lflipboard/model/ConfigSetting;->DisableContentSearch:Z

    .line 153
    iput-boolean v2, p0, Lflipboard/model/ConfigSetting;->LogStartFlipTime:Z

    .line 156
    iput v4, p0, Lflipboard/model/ConfigSetting;->PerformanceUsageSample:F

    .line 168
    const-wide/16 v0, 0x3c

    iput-wide v0, p0, Lflipboard/model/ConfigSetting;->PauseNetworkAfterBackgroundedDelay:J

    .line 171
    iget-wide v0, p0, Lflipboard/model/ConfigSetting;->PauseNetworkAfterBackgroundedDelay:J

    iput-wide v0, p0, Lflipboard/model/ConfigSetting;->PauseNetworkAfterBackgroundedDelayWifi:J

    .line 174
    iput-boolean v2, p0, Lflipboard/model/ConfigSetting;->DisplayRateMeOnlyIfHappyUser:Z

    .line 176
    iput v6, p0, Lflipboard/model/ConfigSetting;->HappyUserMinimumDaysSinceLastCrash:I

    .line 178
    const/16 v0, 0x46

    iput v0, p0, Lflipboard/model/ConfigSetting;->HappyUserMinimumFlipsSinceLastCrash:I

    .line 180
    iput v5, p0, Lflipboard/model/ConfigSetting;->HappyUserMinimumDetailViewsSinceLastCrash:I

    .line 182
    iput-boolean v2, p0, Lflipboard/model/ConfigSetting;->HappyUserHasOwnLocale:Z

    .line 184
    iput v3, p0, Lflipboard/model/ConfigSetting;->HappyUserMinimumConnectedServices:I

    .line 186
    iput v6, p0, Lflipboard/model/ConfigSetting;->ActiveUserMinimumAppUsesPeriod:I

    .line 188
    const/4 v0, 0x4

    iput v0, p0, Lflipboard/model/ConfigSetting;->ActiveUserMinimumAppUsesLastPeriod:I

    .line 190
    iput v2, p0, Lflipboard/model/ConfigSetting;->ActiveUserMinimumMagazineCount:I

    .line 192
    iput-boolean v2, p0, Lflipboard/model/ConfigSetting;->ActiveUserHasAccount:Z

    .line 194
    iput-boolean v3, p0, Lflipboard/model/ConfigSetting;->FirstRunNotificationEnabled:Z

    .line 196
    const-wide/32 v0, 0x15180

    iput-wide v0, p0, Lflipboard/model/ConfigSetting;->FirstRunNotificationInitialDelay:J

    .line 198
    const-wide/32 v0, 0x54600

    iput-wide v0, p0, Lflipboard/model/ConfigSetting;->FirstRunNotificationRepeatDelay:J

    .line 200
    const/4 v0, 0x3

    iput v0, p0, Lflipboard/model/ConfigSetting;->FirstRunNotificationMaxTimes:I

    .line 203
    iput-boolean v3, p0, Lflipboard/model/ConfigSetting;->FirstRunSurveyNotificationEnabled:Z

    .line 205
    const-string v0, "https://www.surveymonkey.com/s/KH2NQC9"

    iput-object v0, p0, Lflipboard/model/ConfigSetting;->FirstRunSurveyNotificationUrl:Ljava/lang/String;

    .line 208
    const/16 v0, 0x12c

    iput v0, p0, Lflipboard/model/ConfigSetting;->SavedSectionStateValidTime:I

    .line 211
    iput-boolean v3, p0, Lflipboard/model/ConfigSetting;->WidgetLogoHintEnabled:Z

    .line 214
    iput-boolean v2, p0, Lflipboard/model/ConfigSetting;->FlipboardAccountRequiredFromFDL:Z

    .line 217
    iput-boolean v2, p0, Lflipboard/model/ConfigSetting;->ModifyUserAgentForTabletServiceLogin:Z

    .line 220
    const-string v0, "http://www.amazon.com/gp/mas/dl/android?p=com.nytimes.android&ref=mas_pm_The_New_York_Times"

    iput-object v0, p0, Lflipboard/model/ConfigSetting;->NYTSubscribeLinkKindle:Ljava/lang/String;

    .line 224
    iput-boolean v3, p0, Lflipboard/model/ConfigSetting;->AllowBitmapRegionDecoder:Z

    .line 227
    iput-boolean v3, p0, Lflipboard/model/ConfigSetting;->RecycleBitmapRegionDecoder:Z

    .line 230
    iput-boolean v2, p0, Lflipboard/model/ConfigSetting;->MagazineEnabled:Z

    .line 233
    const/16 v0, 0x64

    iput v0, p0, Lflipboard/model/ConfigSetting;->MaxNumberEmailsPerLookupRequest:I

    .line 235
    const/4 v0, 0x5

    iput v0, p0, Lflipboard/model/ConfigSetting;->MinimumTopicPickerCount:I

    .line 248
    return-void
.end method


# virtual methods
.method public getWebViewRefererString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 256
    iget-object v0, p0, Lflipboard/model/ConfigSetting;->WebViewRefererString:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lflipboard/model/ConfigSetting;->WebViewRefererString:Ljava/lang/String;

    const-string v1, "%@"

    const-string v2, "%s"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 260
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public shouldUseInlineAML()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 266
    iget v1, p0, Lflipboard/model/ConfigSetting;->InlineAMLSampleRate:F

    invoke-static {v1, v2, v3}, Lflipboard/util/JavaUtil;->a(FFF)F

    move-result v1

    .line 268
    cmpl-float v2, v1, v2

    if-nez v2, :cond_1

    .line 269
    const/4 v0, 0x0

    .line 275
    :cond_0
    :goto_0
    return v0

    .line 270
    :cond_1
    cmpl-float v2, v1, v3

    if-eqz v2, :cond_0

    .line 273
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    .line 274
    const/16 v3, 0x1a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/16 v4, 0x3e8

    const/high16 v5, 0x447a0000    # 1000.0f

    mul-float/2addr v1, v5

    float-to-int v1, v1

    invoke-static {v3, v2, v4, v0, v1}, Lflipboard/abtest/PseudoRandom;->a(Ljava/lang/Integer;Ljava/lang/String;III)Z

    move-result v0

    goto :goto_0
.end method
