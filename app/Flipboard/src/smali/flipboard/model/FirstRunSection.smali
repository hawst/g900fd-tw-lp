.class public Lflipboard/model/FirstRunSection;
.super Lflipboard/json/JsonSerializable;
.source "FirstRunSection.java"


# instance fields
.field public description:Ljava/lang/String;

.field public feedType:Ljava/lang/String;

.field public imageURL:Ljava/lang/String;

.field public keywords:Ljava/lang/String;

.field public maskImageName:Ljava/lang/String;

.field public position:Ljava/lang/String;

.field public preselected:Z

.field public remoteid:Ljava/lang/String;

.field public selectedBackgroundColor:Ljava/lang/String;

.field public selectedIfNoCategoryPicker:Z

.field public selectedImageName:Ljava/lang/String;

.field public selectedPhotoImageName:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public unselectedImageName:Ljava/lang/String;

.field public unselectedPhotoImageName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lflipboard/json/JsonSerializable;-><init>()V

    return-void
.end method
