.class public Lflipboard/model/ConfigFirstLaunch;
.super Lflipboard/json/JsonSerializable;
.source "ConfigFirstLaunch.java"


# instance fields
.field public DefaultSections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/model/FirstRunSection;",
            ">;"
        }
    .end annotation
.end field

.field public SectionsToChooseFrom:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/model/FirstRunSection;",
            ">;"
        }
    .end annotation
.end field

.field public TopicPickerDefaultSections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/model/FirstRunSection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lflipboard/json/JsonSerializable;-><init>()V

    return-void
.end method
