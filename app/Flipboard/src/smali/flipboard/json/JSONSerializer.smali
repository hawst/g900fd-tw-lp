.class public final Lflipboard/json/JSONSerializer;
.super Lflipboard/json/JSONSerializerBase;
.source "JSONSerializer.java"


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lflipboard/io/UTF8StreamWriter;

    invoke-direct {v0, p1}, Lflipboard/io/UTF8StreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializerBase;-><init>(Lflipboard/io/UTF8StreamWriter;)V

    .line 18
    return-void
.end method

.method private a(Lflipboard/io/RequestLogEntry;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 353
    if-nez p1, :cond_0

    .line 354
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 400
    :goto_0
    return-void

    .line 357
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 358
    iget-wide v2, p1, Lflipboard/io/RequestLogEntry;->a:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_a

    .line 359
    const/4 v0, 0x1

    const-string v2, "startTimeUTC"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 360
    iget-wide v2, p1, Lflipboard/io/RequestLogEntry;->a:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    .line 362
    :goto_1
    iget-object v1, p1, Lflipboard/io/RequestLogEntry;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 363
    add-int/lit8 v1, v0, 0x1

    const-string v2, "url"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 364
    iget-object v0, p1, Lflipboard/io/RequestLogEntry;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 366
    :cond_1
    iget-wide v2, p1, Lflipboard/io/RequestLogEntry;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 367
    add-int/lit8 v1, v0, 0x1

    const-string v2, "timeTakenMillis"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 368
    iget-wide v2, p1, Lflipboard/io/RequestLogEntry;->c:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    move v0, v1

    .line 370
    :cond_2
    iget v1, p1, Lflipboard/io/RequestLogEntry;->d:I

    if-eqz v1, :cond_3

    .line 371
    add-int/lit8 v1, v0, 0x1

    const-string v2, "statusCode"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 372
    iget v0, p1, Lflipboard/io/RequestLogEntry;->d:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 374
    :cond_3
    iget-wide v2, p1, Lflipboard/io/RequestLogEntry;->e:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 375
    add-int/lit8 v1, v0, 0x1

    const-string v2, "postBodySizeBytes"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 376
    iget-wide v2, p1, Lflipboard/io/RequestLogEntry;->e:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    move v0, v1

    .line 378
    :cond_4
    iget-object v1, p1, Lflipboard/io/RequestLogEntry;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 379
    add-int/lit8 v1, v0, 0x1

    const-string v2, "flJobId"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 380
    iget-object v0, p1, Lflipboard/io/RequestLogEntry;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 382
    :cond_5
    iget-object v1, p1, Lflipboard/io/RequestLogEntry;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 383
    add-int/lit8 v1, v0, 0x1

    const-string v2, "httpMethod"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 384
    iget-object v0, p1, Lflipboard/io/RequestLogEntry;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 386
    :cond_6
    iget-boolean v1, p1, Lflipboard/io/RequestLogEntry;->h:Z

    if-eqz v1, :cond_7

    .line 387
    add-int/lit8 v1, v0, 0x1

    const-string v2, "finished"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 388
    iget-boolean v0, p1, Lflipboard/io/RequestLogEntry;->h:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 390
    :cond_7
    iget-object v1, p1, Lflipboard/io/RequestLogEntry;->i:Ljava/util/concurrent/atomic/AtomicLong;

    if-eqz v1, :cond_8

    .line 391
    add-int/lit8 v1, v0, 0x1

    const-string v2, "responseSizeBytes"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 392
    iget-object v0, p1, Lflipboard/io/RequestLogEntry;->i:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v2, p0, Lflipboard/json/JSONSerializerBase;->e:Lflipboard/io/UTF8StreamWriter;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lflipboard/io/UTF8StreamWriter;->write(Ljava/lang/String;)V

    move v0, v1

    .line 394
    :cond_8
    iget-object v1, p1, Lflipboard/io/RequestLogEntry;->j:Lflipboard/json/FLObject;

    if-eqz v1, :cond_9

    .line 395
    const-string v1, "response"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 396
    iget-object v0, p1, Lflipboard/io/RequestLogEntry;->j:Lflipboard/json/FLObject;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/json/FLObject;)V

    .line 398
    :cond_9
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_a
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/Ad$Asset;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 573
    if-nez p1, :cond_0

    .line 574
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 600
    :goto_0
    return-void

    .line 577
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 578
    iget-object v0, p1, Lflipboard/objs/Ad$Asset;->a:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 579
    const/4 v0, 0x1

    const-string v2, "url"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 580
    iget-object v2, p1, Lflipboard/objs/Ad$Asset;->a:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 582
    :goto_1
    iget v2, p1, Lflipboard/objs/Ad$Asset;->b:I

    if-eqz v2, :cond_1

    .line 583
    add-int/lit8 v2, v0, 0x1

    const-string v3, "height"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 584
    iget v0, p1, Lflipboard/objs/Ad$Asset;->b:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 586
    :cond_1
    iget v2, p1, Lflipboard/objs/Ad$Asset;->c:I

    if-eqz v2, :cond_2

    .line 587
    add-int/lit8 v2, v0, 0x1

    const-string v3, "width"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 588
    iget v0, p1, Lflipboard/objs/Ad$Asset;->c:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 590
    :cond_2
    iget v2, p1, Lflipboard/objs/Ad$Asset;->d:I

    if-eqz v2, :cond_3

    .line 591
    add-int/lit8 v2, v0, 0x1

    const-string v3, "safe_zone"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 592
    iget v0, p1, Lflipboard/objs/Ad$Asset;->d:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 594
    :cond_3
    iget-object v2, p1, Lflipboard/objs/Ad$Asset;->e:Ljava/util/List;

    if-eqz v2, :cond_4

    .line 595
    const-string v2, "hot_spots"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 596
    iget-object v0, p1, Lflipboard/objs/Ad$Asset;->e:Ljava/util/List;

    if-nez v0, :cond_5

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 598
    :cond_4
    :goto_2
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    .line 596
    :cond_5
    const/16 v2, 0x5b

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Ad$HotSpot;

    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_6

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_6
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Ad$HotSpot;)V

    move v1, v2

    goto :goto_3

    :cond_7
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_2

    :cond_8
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/Ad$Button;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 667
    if-nez p1, :cond_0

    .line 668
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 698
    :goto_0
    return-void

    .line 671
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 672
    iget-object v0, p1, Lflipboard/objs/Ad$Button;->a:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 673
    const/4 v0, 0x1

    const-string v2, "click_url"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 674
    iget-object v1, p1, Lflipboard/objs/Ad$Button;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 676
    :goto_1
    iget-object v1, p1, Lflipboard/objs/Ad$Button;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 677
    add-int/lit8 v1, v0, 0x1

    const-string v2, "click_value"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 678
    iget-object v0, p1, Lflipboard/objs/Ad$Button;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 680
    :cond_1
    iget-object v1, p1, Lflipboard/objs/Ad$Button;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 681
    add-int/lit8 v1, v0, 0x1

    const-string v2, "color"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 682
    iget-object v0, p1, Lflipboard/objs/Ad$Button;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 684
    :cond_2
    iget-object v1, p1, Lflipboard/objs/Ad$Button;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 685
    add-int/lit8 v1, v0, 0x1

    const-string v2, "icon_url"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 686
    iget-object v0, p1, Lflipboard/objs/Ad$Button;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 688
    :cond_3
    iget-object v1, p1, Lflipboard/objs/Ad$Button;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 689
    add-int/lit8 v1, v0, 0x1

    const-string v2, "text"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 690
    iget-object v0, p1, Lflipboard/objs/Ad$Button;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 692
    :cond_4
    iget-boolean v1, p1, Lflipboard/objs/Ad$Button;->f:Z

    if-eqz v1, :cond_5

    .line 693
    const-string v1, "video_supported"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 694
    iget-boolean v0, p1, Lflipboard/objs/Ad$Button;->f:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    .line 696
    :cond_5
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/Ad$ButtonInfo;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 765
    if-nez p1, :cond_0

    .line 766
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 780
    :goto_0
    return-void

    .line 769
    :cond_0
    const/16 v1, 0x7b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 770
    iget-object v1, p1, Lflipboard/objs/Ad$ButtonInfo;->a:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 771
    const/4 v3, 0x1

    const-string v1, "buttons"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 772
    iget-object v1, p1, Lflipboard/objs/Ad$ButtonInfo;->a:Ljava/util/List;

    if-nez v1, :cond_3

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    move v0, v3

    .line 774
    :cond_1
    :goto_1
    iget-boolean v1, p1, Lflipboard/objs/Ad$ButtonInfo;->b:Z

    if-eqz v1, :cond_2

    .line 775
    const-string v1, "disable_gradient"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 776
    iget-boolean v0, p1, Lflipboard/objs/Ad$ButtonInfo;->b:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    .line 778
    :cond_2
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    .line 772
    :cond_3
    const/16 v2, 0x5b

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Ad$Button;

    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_4

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_4
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Ad$Button;)V

    move v1, v2

    goto :goto_2

    :cond_5
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    move v0, v3

    goto :goto_1
.end method

.method private a(Lflipboard/objs/Ad$HotSpot;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 847
    if-nez p1, :cond_0

    .line 848
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 890
    :goto_0
    return-void

    .line 851
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 852
    iget-object v0, p1, Lflipboard/objs/Ad$HotSpot;->a:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 853
    const/4 v0, 0x1

    const-string v2, "id"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 854
    iget-object v1, p1, Lflipboard/objs/Ad$HotSpot;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 856
    :goto_1
    iget v1, p1, Lflipboard/objs/Ad$HotSpot;->b:I

    if-eqz v1, :cond_1

    .line 857
    add-int/lit8 v1, v0, 0x1

    const-string v2, "x"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 858
    iget v0, p1, Lflipboard/objs/Ad$HotSpot;->b:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 860
    :cond_1
    iget v1, p1, Lflipboard/objs/Ad$HotSpot;->c:I

    if-eqz v1, :cond_2

    .line 861
    add-int/lit8 v1, v0, 0x1

    const-string v2, "y"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 862
    iget v0, p1, Lflipboard/objs/Ad$HotSpot;->c:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 864
    :cond_2
    iget v1, p1, Lflipboard/objs/Ad$HotSpot;->d:I

    if-eqz v1, :cond_3

    .line 865
    add-int/lit8 v1, v0, 0x1

    const-string v2, "width"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 866
    iget v0, p1, Lflipboard/objs/Ad$HotSpot;->d:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 868
    :cond_3
    iget v1, p1, Lflipboard/objs/Ad$HotSpot;->e:I

    if-eqz v1, :cond_4

    .line 869
    add-int/lit8 v1, v0, 0x1

    const-string v2, "height"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 870
    iget v0, p1, Lflipboard/objs/Ad$HotSpot;->e:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 872
    :cond_4
    iget-object v1, p1, Lflipboard/objs/Ad$HotSpot;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 873
    add-int/lit8 v1, v0, 0x1

    const-string v2, "click_url"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 874
    iget-object v0, p1, Lflipboard/objs/Ad$HotSpot;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 876
    :cond_5
    iget-object v1, p1, Lflipboard/objs/Ad$HotSpot;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 877
    add-int/lit8 v1, v0, 0x1

    const-string v2, "click_value"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 878
    iget-object v0, p1, Lflipboard/objs/Ad$HotSpot;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 880
    :cond_6
    iget-object v1, p1, Lflipboard/objs/Ad$HotSpot;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 881
    add-int/lit8 v1, v0, 0x1

    const-string v2, "click_url_browser_safe"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 882
    iget-object v0, p1, Lflipboard/objs/Ad$HotSpot;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 884
    :cond_7
    iget-boolean v1, p1, Lflipboard/objs/Ad$HotSpot;->i:Z

    if-eqz v1, :cond_8

    .line 885
    const-string v1, "video_supported"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 886
    iget-boolean v0, p1, Lflipboard/objs/Ad$HotSpot;->i:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    .line 888
    :cond_8
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/Ad$MetricValues;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 957
    if-nez p1, :cond_0

    .line 958
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 988
    :goto_0
    return-void

    .line 961
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 962
    iget-object v0, p1, Lflipboard/objs/Ad$MetricValues;->a:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 963
    const/4 v0, 0x1

    const-string v2, "playback_percent_0"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 964
    iget-object v1, p1, Lflipboard/objs/Ad$MetricValues;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 966
    :goto_1
    iget-object v1, p1, Lflipboard/objs/Ad$MetricValues;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 967
    add-int/lit8 v1, v0, 0x1

    const-string v2, "playback_percent_25"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 968
    iget-object v0, p1, Lflipboard/objs/Ad$MetricValues;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 970
    :cond_1
    iget-object v1, p1, Lflipboard/objs/Ad$MetricValues;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 971
    add-int/lit8 v1, v0, 0x1

    const-string v2, "playback_percent_50"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 972
    iget-object v0, p1, Lflipboard/objs/Ad$MetricValues;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 974
    :cond_2
    iget-object v1, p1, Lflipboard/objs/Ad$MetricValues;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 975
    add-int/lit8 v1, v0, 0x1

    const-string v2, "playback_percent_75"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 976
    iget-object v0, p1, Lflipboard/objs/Ad$MetricValues;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 978
    :cond_3
    iget-object v1, p1, Lflipboard/objs/Ad$MetricValues;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 979
    add-int/lit8 v1, v0, 0x1

    const-string v2, "playback_percent_100"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 980
    iget-object v0, p1, Lflipboard/objs/Ad$MetricValues;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 982
    :cond_4
    iget-object v1, p1, Lflipboard/objs/Ad$MetricValues;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 983
    const-string v1, "playback_duration"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 984
    iget-object v0, p1, Lflipboard/objs/Ad$MetricValues;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 986
    :cond_5
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/Ad$VideoInfo;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1055
    if-nez p1, :cond_0

    .line 1056
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 1070
    :goto_0
    return-void

    .line 1059
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 1060
    iget-object v0, p1, Lflipboard/objs/Ad$VideoInfo;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1061
    const/4 v0, 0x1

    const-string v2, "url"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1062
    iget-object v1, p1, Lflipboard/objs/Ad$VideoInfo;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 1064
    :goto_1
    iget-object v1, p1, Lflipboard/objs/Ad$VideoInfo;->b:Lflipboard/objs/Ad$MetricValues;

    if-eqz v1, :cond_1

    .line 1065
    const-string v1, "metric_values"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1066
    iget-object v0, p1, Lflipboard/objs/Ad$VideoInfo;->b:Lflipboard/objs/Ad$MetricValues;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Ad$MetricValues;)V

    .line 1068
    :cond_1
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/Author;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1243
    if-nez p1, :cond_0

    .line 1244
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 1278
    :goto_0
    return-void

    .line 1247
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 1248
    iget-object v0, p1, Lflipboard/objs/Author;->a:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1249
    const/4 v0, 0x1

    const-string v2, "service"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1250
    iget-object v1, p1, Lflipboard/objs/Author;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 1252
    :goto_1
    iget-object v1, p1, Lflipboard/objs/Author;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1253
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorUsername"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1254
    iget-object v0, p1, Lflipboard/objs/Author;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1256
    :cond_1
    iget-object v1, p1, Lflipboard/objs/Author;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1257
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorDisplayName"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1258
    iget-object v0, p1, Lflipboard/objs/Author;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1260
    :cond_2
    iget-object v1, p1, Lflipboard/objs/Author;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1261
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1262
    iget-object v0, p1, Lflipboard/objs/Author;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1264
    :cond_3
    iget-object v1, p1, Lflipboard/objs/Author;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1265
    add-int/lit8 v1, v0, 0x1

    const-string v2, "userid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1266
    iget-object v0, p1, Lflipboard/objs/Author;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1268
    :cond_4
    iget-object v1, p1, Lflipboard/objs/Author;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1269
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorDescription"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1270
    iget-object v0, p1, Lflipboard/objs/Author;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1272
    :cond_5
    iget-object v1, p1, Lflipboard/objs/Author;->g:Lflipboard/objs/Image;

    if-eqz v1, :cond_6

    .line 1273
    const-string v1, "authorImage"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1274
    iget-object v0, p1, Lflipboard/objs/Author;->g:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    .line 1276
    :cond_6
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/CommentaryResult$Item$Commentary;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1715
    if-nez p1, :cond_0

    .line 1716
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 1786
    :goto_0
    return-void

    .line 1719
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 1720
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 1721
    const/4 v0, 0x1

    const-string v2, "type"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1722
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 1724
    :goto_1
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1725
    add-int/lit8 v1, v0, 0x1

    const-string v2, "service"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1726
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1728
    :cond_1
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1729
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorUsername"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1730
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1732
    :cond_2
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1733
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorDisplayName"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1734
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1736
    :cond_3
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->d:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1737
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1738
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1740
    :cond_4
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->e:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1741
    add-int/lit8 v1, v0, 0x1

    const-string v2, "userid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1742
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1744
    :cond_5
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->f:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 1745
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorDescription"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1746
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1748
    :cond_6
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->g:Lflipboard/objs/Image;

    if-eqz v1, :cond_7

    .line 1749
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorImage"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1750
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->g:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v1

    .line 1752
    :cond_7
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->h:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 1753
    add-int/lit8 v1, v0, 0x1

    const-string v2, "id"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1754
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1756
    :cond_8
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->j:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1757
    add-int/lit8 v1, v0, 0x1

    const-string v2, "text"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1758
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1760
    :cond_9
    iget-wide v2, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->k:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_a

    .line 1761
    add-int/lit8 v1, v0, 0x1

    const-string v2, "dateCreated"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1762
    iget-wide v2, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->k:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    move v0, v1

    .line 1764
    :cond_a
    iget-boolean v1, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->l:Z

    if-eqz v1, :cond_b

    .line 1765
    add-int/lit8 v1, v0, 0x1

    const-string v2, "connection"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1766
    iget-boolean v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->l:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 1768
    :cond_b
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->m:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 1769
    add-int/lit8 v1, v0, 0x1

    const-string v2, "sourceURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1770
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->m:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1772
    :cond_c
    iget v1, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->n:I

    if-eqz v1, :cond_d

    .line 1773
    add-int/lit8 v1, v0, 0x1

    const-string v2, "commentCount"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1774
    iget v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->n:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 1776
    :cond_d
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->o:Ljava/util/List;

    if-eqz v1, :cond_e

    .line 1777
    add-int/lit8 v1, v0, 0x1

    const-string v2, "sectionLinks"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1778
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->o:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->i(Ljava/util/List;)V

    move v0, v1

    .line 1780
    :cond_e
    iget-boolean v1, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->p:Z

    if-eqz v1, :cond_f

    .line 1781
    const-string v1, "canDelete"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1782
    iget-boolean v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->p:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    .line 1784
    :cond_f
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_10
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/CommentaryResult$Item$ProfileMetric;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1853
    if-nez p1, :cond_0

    .line 1854
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 1872
    :goto_0
    return-void

    .line 1857
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 1858
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1859
    const/4 v0, 0x1

    const-string v2, "type"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1860
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;->c:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 1862
    :goto_1
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1863
    add-int/lit8 v1, v0, 0x1

    const-string v2, "displayName"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1864
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1866
    :cond_1
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1867
    const-string v1, "value"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1868
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 1870
    :cond_2
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/CommentaryResult$Item;)V
    .locals 9

    .prologue
    const/16 v8, 0x5d

    const/16 v7, 0x5b

    const/16 v6, 0x2c

    const/4 v1, 0x0

    .line 1553
    if-nez p1, :cond_0

    .line 1554
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 1648
    :goto_0
    return-void

    .line 1557
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 1558
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item;->a:Ljava/lang/String;

    if-eqz v0, :cond_1c

    .line 1559
    const/4 v0, 0x1

    const-string v2, "id"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1560
    iget-object v2, p1, Lflipboard/objs/CommentaryResult$Item;->a:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 1562
    :goto_1
    iget v2, p1, Lflipboard/objs/CommentaryResult$Item;->b:I

    if-eqz v2, :cond_1

    .line 1563
    add-int/lit8 v2, v0, 0x1

    const-string v3, "commentCount"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1564
    iget v0, p1, Lflipboard/objs/CommentaryResult$Item;->b:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 1566
    :cond_1
    iget v2, p1, Lflipboard/objs/CommentaryResult$Item;->c:I

    if-eqz v2, :cond_2

    .line 1567
    add-int/lit8 v2, v0, 0x1

    const-string v3, "likeCount"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1568
    iget v0, p1, Lflipboard/objs/CommentaryResult$Item;->c:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 1570
    :cond_2
    iget v2, p1, Lflipboard/objs/CommentaryResult$Item;->d:I

    if-eqz v2, :cond_3

    .line 1571
    add-int/lit8 v2, v0, 0x1

    const-string v3, "shareCount"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1572
    iget v0, p1, Lflipboard/objs/CommentaryResult$Item;->d:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 1574
    :cond_3
    iget v2, p1, Lflipboard/objs/CommentaryResult$Item;->e:I

    if-eqz v2, :cond_4

    .line 1575
    add-int/lit8 v2, v0, 0x1

    const-string v3, "sharedwithCount"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1576
    iget v0, p1, Lflipboard/objs/CommentaryResult$Item;->e:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 1578
    :cond_4
    iget v2, p1, Lflipboard/objs/CommentaryResult$Item;->f:I

    if-eqz v2, :cond_5

    .line 1579
    add-int/lit8 v2, v0, 0x1

    const-string v3, "genericCount"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1580
    iget v0, p1, Lflipboard/objs/CommentaryResult$Item;->f:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 1582
    :cond_5
    iget-object v2, p1, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    if-eqz v2, :cond_6

    .line 1583
    add-int/lit8 v4, v0, 0x1

    const-string v2, "commentary"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1584
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    if-nez v0, :cond_16

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    move v0, v4

    .line 1586
    :cond_6
    :goto_2
    iget-object v2, p1, Lflipboard/objs/CommentaryResult$Item;->h:Ljava/util/List;

    if-eqz v2, :cond_7

    .line 1587
    add-int/lit8 v2, v0, 0x1

    const-string v3, "inReplyToChain"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1588
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item;->h:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->c(Ljava/util/List;)V

    move v0, v2

    .line 1590
    :cond_7
    iget-object v2, p1, Lflipboard/objs/CommentaryResult$Item;->i:Ljava/util/List;

    if-eqz v2, :cond_8

    .line 1591
    add-int/lit8 v3, v0, 0x1

    const-string v2, "profileMetrics"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1592
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item;->i:Ljava/util/List;

    if-nez v0, :cond_19

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    move v0, v3

    .line 1594
    :cond_8
    :goto_3
    iget-wide v2, p1, Lflipboard/objs/CommentaryResult$Item;->j:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_9

    .line 1595
    add-int/lit8 v1, v0, 0x1

    const-string v2, "ttl"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1596
    iget-wide v2, p1, Lflipboard/objs/CommentaryResult$Item;->j:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    move v0, v1

    .line 1598
    :cond_9
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item;->k:Ljava/util/List;

    if-eqz v1, :cond_a

    .line 1599
    add-int/lit8 v1, v0, 0x1

    const-string v2, "metrics"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1600
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item;->k:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    move v0, v1

    .line 1602
    :cond_a
    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->l:I

    if-eqz v1, :cond_b

    .line 1603
    add-int/lit8 v1, v0, 0x1

    const-string v2, "itemCount"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1604
    iget v0, p1, Lflipboard/objs/CommentaryResult$Item;->l:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 1606
    :cond_b
    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->m:I

    if-eqz v1, :cond_c

    .line 1607
    add-int/lit8 v1, v0, 0x1

    const-string v2, "allItemsLikeCount"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1608
    iget v0, p1, Lflipboard/objs/CommentaryResult$Item;->m:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 1610
    :cond_c
    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->n:I

    if-eqz v1, :cond_d

    .line 1611
    add-int/lit8 v1, v0, 0x1

    const-string v2, "subscribersCountl"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1612
    iget v0, p1, Lflipboard/objs/CommentaryResult$Item;->n:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 1614
    :cond_d
    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->o:I

    if-eqz v1, :cond_e

    .line 1615
    add-int/lit8 v1, v0, 0x1

    const-string v2, "allItemsViewCount"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1616
    iget v0, p1, Lflipboard/objs/CommentaryResult$Item;->o:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 1618
    :cond_e
    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->p:I

    if-eqz v1, :cond_f

    .line 1619
    add-int/lit8 v1, v0, 0x1

    const-string v2, "allItemsFlipCount"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1620
    iget v0, p1, Lflipboard/objs/CommentaryResult$Item;->p:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 1622
    :cond_f
    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->q:I

    if-eqz v1, :cond_10

    .line 1623
    add-int/lit8 v1, v0, 0x1

    const-string v2, "allItemsCommentCount"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1624
    iget v0, p1, Lflipboard/objs/CommentaryResult$Item;->q:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 1626
    :cond_10
    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->r:I

    if-eqz v1, :cond_11

    .line 1627
    add-int/lit8 v1, v0, 0x1

    const-string v2, "allItemsShareCount"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1628
    iget v0, p1, Lflipboard/objs/CommentaryResult$Item;->r:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 1630
    :cond_11
    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->s:I

    if-eqz v1, :cond_12

    .line 1631
    add-int/lit8 v1, v0, 0x1

    const-string v2, "subscribersCount"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1632
    iget v0, p1, Lflipboard/objs/CommentaryResult$Item;->s:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 1634
    :cond_12
    iget v1, p1, Lflipboard/objs/CommentaryResult$Item;->t:I

    if-eqz v1, :cond_13

    .line 1635
    add-int/lit8 v1, v0, 0x1

    const-string v2, "contributorCount"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1636
    iget v0, p1, Lflipboard/objs/CommentaryResult$Item;->t:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 1638
    :cond_13
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item;->u:Ljava/lang/String;

    if-eqz v1, :cond_14

    .line 1639
    add-int/lit8 v1, v0, 0x1

    const-string v2, "likesPageKey"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1640
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item;->u:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1642
    :cond_14
    iget-object v1, p1, Lflipboard/objs/CommentaryResult$Item;->v:Ljava/lang/String;

    if-eqz v1, :cond_15

    .line 1643
    const-string v1, "commentsPageKey"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1644
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item;->v:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 1646
    :cond_15
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    .line 1584
    :cond_16
    invoke-virtual {p0, v7}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v1

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    add-int/lit8 v3, v2, 0x1

    if-lez v2, :cond_17

    invoke-virtual {p0, v6}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_17
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/CommentaryResult$Item$Commentary;)V

    move v2, v3

    goto :goto_4

    :cond_18
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    move v0, v4

    goto/16 :goto_2

    .line 1592
    :cond_19
    invoke-virtual {p0, v7}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;

    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_1a

    invoke-virtual {p0, v6}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_1a
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/CommentaryResult$Item$ProfileMetric;)V

    move v1, v2

    goto :goto_5

    :cond_1b
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    move v0, v3

    goto/16 :goto_3

    :cond_1c
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/ConfigBrick;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1939
    if-nez p1, :cond_0

    .line 1940
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 1990
    :goto_0
    return-void

    .line 1943
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 1944
    iget-object v0, p1, Lflipboard/objs/ConfigBrick;->a:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 1945
    const/4 v0, 0x1

    const-string v2, "imageURL"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1946
    iget-object v1, p1, Lflipboard/objs/ConfigBrick;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 1948
    :goto_1
    iget-object v1, p1, Lflipboard/objs/ConfigBrick;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1949
    add-int/lit8 v1, v0, 0x1

    const-string v2, "largeURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1950
    iget-object v0, p1, Lflipboard/objs/ConfigBrick;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1952
    :cond_1
    iget-object v1, p1, Lflipboard/objs/ConfigBrick;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1953
    add-int/lit8 v1, v0, 0x1

    const-string v2, "mediumURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1954
    iget-object v0, p1, Lflipboard/objs/ConfigBrick;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1956
    :cond_2
    iget-object v1, p1, Lflipboard/objs/ConfigBrick;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1957
    add-int/lit8 v1, v0, 0x1

    const-string v2, "smallURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1958
    iget-object v0, p1, Lflipboard/objs/ConfigBrick;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 1960
    :cond_3
    iget v1, p1, Lflipboard/objs/ConfigBrick;->e:I

    if-eqz v1, :cond_4

    .line 1961
    add-int/lit8 v1, v0, 0x1

    const-string v2, "width"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1962
    iget v0, p1, Lflipboard/objs/ConfigBrick;->e:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 1964
    :cond_4
    iget v1, p1, Lflipboard/objs/ConfigBrick;->f:I

    if-eqz v1, :cond_5

    .line 1965
    add-int/lit8 v1, v0, 0x1

    const-string v2, "height"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1966
    iget v0, p1, Lflipboard/objs/ConfigBrick;->f:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 1968
    :cond_5
    iget v1, p1, Lflipboard/objs/ConfigBrick;->g:I

    if-eqz v1, :cond_6

    .line 1969
    add-int/lit8 v1, v0, 0x1

    const-string v2, "perRow"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1970
    iget v0, p1, Lflipboard/objs/ConfigBrick;->g:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 1972
    :cond_6
    iget-boolean v1, p1, Lflipboard/objs/ConfigBrick;->h:Z

    if-eqz v1, :cond_7

    .line 1973
    add-int/lit8 v1, v0, 0x1

    const-string v2, "showTitle"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1974
    iget-boolean v0, p1, Lflipboard/objs/ConfigBrick;->h:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 1976
    :cond_7
    iget-boolean v1, p1, Lflipboard/objs/ConfigBrick;->i:Z

    if-eqz v1, :cond_8

    .line 1977
    add-int/lit8 v1, v0, 0x1

    const-string v2, "showAuthor"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1978
    iget-boolean v0, p1, Lflipboard/objs/ConfigBrick;->i:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 1980
    :cond_8
    iget-boolean v1, p1, Lflipboard/objs/ConfigBrick;->j:Z

    if-eqz v1, :cond_9

    .line 1981
    add-int/lit8 v1, v0, 0x1

    const-string v2, "showAddButton"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1982
    iget-boolean v0, p1, Lflipboard/objs/ConfigBrick;->j:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 1984
    :cond_9
    iget-object v1, p1, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    if-eqz v1, :cond_a

    .line 1985
    const-string v1, "section"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 1986
    iget-object v0, p1, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigSection;)V

    .line 1988
    :cond_a
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/ConfigEdition;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2139
    if-nez p1, :cond_0

    .line 2140
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 2162
    :goto_0
    return-void

    .line 2143
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 2144
    iget-object v0, p1, Lflipboard/objs/ConfigEdition;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 2145
    const/4 v0, 0x1

    const-string v2, "displayName"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2146
    iget-object v1, p1, Lflipboard/objs/ConfigEdition;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 2148
    :goto_1
    iget-object v1, p1, Lflipboard/objs/ConfigEdition;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2149
    add-int/lit8 v1, v0, 0x1

    const-string v2, "locale"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2150
    iget-object v0, p1, Lflipboard/objs/ConfigEdition;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2152
    :cond_1
    iget-object v1, p1, Lflipboard/objs/ConfigEdition;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2153
    add-int/lit8 v1, v0, 0x1

    const-string v2, "language"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2154
    iget-object v0, p1, Lflipboard/objs/ConfigEdition;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2156
    :cond_2
    iget-boolean v1, p1, Lflipboard/objs/ConfigEdition;->d:Z

    if-eqz v1, :cond_3

    .line 2157
    const-string v1, "currentEdition"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2158
    iget-boolean v0, p1, Lflipboard/objs/ConfigEdition;->d:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    .line 2160
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/ConfigFolder;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2229
    if-nez p1, :cond_0

    .line 2230
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 2280
    :goto_0
    return-void

    .line 2233
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 2234
    iget-object v0, p1, Lflipboard/objs/ConfigFolder;->bP:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 2235
    const/4 v0, 0x1

    const-string v2, "title"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2236
    iget-object v1, p1, Lflipboard/objs/ConfigFolder;->bP:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 2238
    :goto_1
    iget-object v1, p1, Lflipboard/objs/ConfigFolder;->bQ:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2239
    add-int/lit8 v1, v0, 0x1

    const-string v2, "description"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2240
    iget-object v0, p1, Lflipboard/objs/ConfigFolder;->bQ:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2242
    :cond_1
    iget-object v1, p1, Lflipboard/objs/ConfigFolder;->bR:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2243
    add-int/lit8 v1, v0, 0x1

    const-string v2, "icon"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2244
    iget-object v0, p1, Lflipboard/objs/ConfigFolder;->bR:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2246
    :cond_2
    iget-boolean v1, p1, Lflipboard/objs/ConfigFolder;->bS:Z

    if-eqz v1, :cond_3

    .line 2247
    add-int/lit8 v1, v0, 0x1

    const-string v2, "isFavicon"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2248
    iget-boolean v0, p1, Lflipboard/objs/ConfigFolder;->bS:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 2250
    :cond_3
    iget-object v1, p1, Lflipboard/objs/ConfigFolder;->a:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 2251
    add-int/lit8 v1, v0, 0x1

    const-string v2, "groupid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2252
    iget-object v0, p1, Lflipboard/objs/ConfigFolder;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2254
    :cond_4
    iget v1, p1, Lflipboard/objs/ConfigFolder;->b:I

    if-eqz v1, :cond_5

    .line 2255
    add-int/lit8 v1, v0, 0x1

    const-string v2, "minVersion"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2256
    iget v0, p1, Lflipboard/objs/ConfigFolder;->b:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 2258
    :cond_5
    iget-boolean v1, p1, Lflipboard/objs/ConfigFolder;->c:Z

    if-eqz v1, :cond_6

    .line 2259
    add-int/lit8 v1, v0, 0x1

    const-string v2, "showInline"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2260
    iget-boolean v0, p1, Lflipboard/objs/ConfigFolder;->c:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 2262
    :cond_6
    iget-object v1, p1, Lflipboard/objs/ConfigFolder;->d:Ljava/util/List;

    if-eqz v1, :cond_7

    .line 2263
    add-int/lit8 v1, v0, 0x1

    const-string v2, "sections"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2264
    iget-object v0, p1, Lflipboard/objs/ConfigFolder;->d:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->g(Ljava/util/List;)V

    move v0, v1

    .line 2266
    :cond_7
    iget-object v1, p1, Lflipboard/objs/ConfigFolder;->e:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 2267
    add-int/lit8 v1, v0, 0x1

    const-string v2, "language"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2268
    iget-object v0, p1, Lflipboard/objs/ConfigFolder;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2270
    :cond_8
    iget-object v1, p1, Lflipboard/objs/ConfigFolder;->f:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 2271
    add-int/lit8 v1, v0, 0x1

    const-string v2, "remoteid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2272
    iget-object v0, p1, Lflipboard/objs/ConfigFolder;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2274
    :cond_9
    iget-object v1, p1, Lflipboard/objs/ConfigFolder;->g:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 2275
    const-string v1, "categoryTitle"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2276
    iget-object v0, p1, Lflipboard/objs/ConfigFolder;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 2278
    :cond_a
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/ConfigHints$Condition;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2433
    if-nez p1, :cond_0

    .line 2434
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 2460
    :goto_0
    return-void

    .line 2437
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 2438
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Condition;->b:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2439
    const/4 v0, 0x1

    const-string v2, "type"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2440
    iget-object v1, p1, Lflipboard/objs/ConfigHints$Condition;->b:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 2442
    :goto_1
    iget-object v1, p1, Lflipboard/objs/ConfigHints$Condition;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2443
    add-int/lit8 v1, v0, 0x1

    const-string v2, "action"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2444
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Condition;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2446
    :cond_1
    iget-object v1, p1, Lflipboard/objs/ConfigHints$Condition;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2447
    add-int/lit8 v1, v0, 0x1

    const-string v2, "comparison"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2448
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Condition;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2450
    :cond_2
    iget v1, p1, Lflipboard/objs/ConfigHints$Condition;->d:F

    float-to-double v2, v1

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_3

    .line 2451
    add-int/lit8 v1, v0, 0x1

    const-string v2, "value"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2452
    iget v0, p1, Lflipboard/objs/ConfigHints$Condition;->d:F

    float-to-double v2, v0

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v1

    .line 2454
    :cond_3
    iget-object v1, p1, Lflipboard/objs/ConfigHints$Condition;->e:Ljava/util/List;

    if-eqz v1, :cond_4

    .line 2455
    const-string v1, "conditions"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2456
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Condition;->e:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->f(Ljava/util/List;)V

    .line 2458
    :cond_4
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/ConfigHints$Hint;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v4, -0x1

    const/4 v1, 0x1

    const-wide/16 v6, 0x0

    .line 2527
    if-nez p1, :cond_0

    .line 2528
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 2662
    :goto_0
    return-void

    .line 2531
    :cond_0
    const/16 v2, 0x7b

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 2532
    iget-object v2, p1, Lflipboard/objs/ConfigHints$Hint;->l:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 2533
    const-string v2, "type"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2534
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->l:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2536
    :cond_1
    iget-boolean v2, p1, Lflipboard/objs/ConfigHints$Hint;->d:Z

    if-eq v2, v1, :cond_2

    .line 2537
    add-int/lit8 v2, v0, 0x1

    const-string v3, "enabled"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2538
    iget-boolean v0, p1, Lflipboard/objs/ConfigHints$Hint;->d:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 2540
    :cond_2
    iget v2, p1, Lflipboard/objs/ConfigHints$Hint;->e:I

    if-eq v2, v4, :cond_3

    .line 2541
    add-int/lit8 v2, v0, 0x1

    const-string v3, "minVersion"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2542
    iget v0, p1, Lflipboard/objs/ConfigHints$Hint;->e:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 2544
    :cond_3
    iget v2, p1, Lflipboard/objs/ConfigHints$Hint;->f:I

    if-eq v2, v4, :cond_4

    .line 2545
    add-int/lit8 v2, v0, 0x1

    const-string v3, "maxVersion"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2546
    iget v0, p1, Lflipboard/objs/ConfigHints$Hint;->f:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 2548
    :cond_4
    iget-object v2, p1, Lflipboard/objs/ConfigHints$Hint;->g:Ljava/util/List;

    if-eqz v2, :cond_5

    .line 2549
    add-int/lit8 v2, v0, 0x1

    const-string v3, "idioms"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2550
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->g:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    move v0, v2

    .line 2552
    :cond_5
    iget-object v2, p1, Lflipboard/objs/ConfigHints$Hint;->h:Ljava/util/List;

    if-eqz v2, :cond_6

    .line 2553
    add-int/lit8 v2, v0, 0x1

    const-string v3, "languages"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2554
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->h:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    move v0, v2

    .line 2556
    :cond_6
    iget-object v2, p1, Lflipboard/objs/ConfigHints$Hint;->i:Ljava/util/List;

    if-eqz v2, :cond_7

    .line 2557
    add-int/lit8 v2, v0, 0x1

    const-string v3, "locales"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2558
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->i:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    move v0, v2

    .line 2560
    :cond_7
    iget-object v2, p1, Lflipboard/objs/ConfigHints$Hint;->j:Ljava/util/List;

    if-eqz v2, :cond_8

    .line 2561
    add-int/lit8 v2, v0, 0x1

    const-string v3, "inclusionConditions"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2562
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->j:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->f(Ljava/util/List;)V

    move v0, v2

    .line 2564
    :cond_8
    iget-object v2, p1, Lflipboard/objs/ConfigHints$Hint;->k:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 2565
    add-int/lit8 v2, v0, 0x1

    const-string v3, "id"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2566
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->k:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 2568
    :cond_9
    iget-boolean v2, p1, Lflipboard/objs/ConfigHints$Hint;->m:Z

    if-eqz v2, :cond_a

    .line 2569
    add-int/lit8 v2, v0, 0x1

    const-string v3, "modal"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2570
    iget-boolean v0, p1, Lflipboard/objs/ConfigHints$Hint;->m:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 2572
    :cond_a
    iget v2, p1, Lflipboard/objs/ConfigHints$Hint;->n:F

    float-to-double v2, v2

    cmpl-double v2, v2, v6

    if-eqz v2, :cond_b

    .line 2573
    add-int/lit8 v2, v0, 0x1

    const-string v3, "presentationDelay"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2574
    iget v0, p1, Lflipboard/objs/ConfigHints$Hint;->n:F

    float-to-double v4, v0

    invoke-virtual {p0, v4, v5}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v2

    .line 2576
    :cond_b
    iget v2, p1, Lflipboard/objs/ConfigHints$Hint;->o:F

    float-to-double v2, v2

    cmpl-double v2, v2, v6

    if-eqz v2, :cond_c

    .line 2577
    add-int/lit8 v2, v0, 0x1

    const-string v3, "postPresentationDelay"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2578
    iget v0, p1, Lflipboard/objs/ConfigHints$Hint;->o:F

    float-to-double v4, v0

    invoke-virtual {p0, v4, v5}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v2

    .line 2580
    :cond_c
    iget-boolean v2, p1, Lflipboard/objs/ConfigHints$Hint;->p:Z

    if-eqz v2, :cond_d

    .line 2581
    add-int/lit8 v2, v0, 0x1

    const-string v3, "ignoresGlobalCaps"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2582
    iget-boolean v0, p1, Lflipboard/objs/ConfigHints$Hint;->p:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 2584
    :cond_d
    iget-object v2, p1, Lflipboard/objs/ConfigHints$Hint;->q:Ljava/lang/String;

    if-eqz v2, :cond_e

    .line 2585
    add-int/lit8 v2, v0, 0x1

    const-string v3, "url"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2586
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->q:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 2588
    :cond_e
    iget-object v2, p1, Lflipboard/objs/ConfigHints$Hint;->r:Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 2589
    add-int/lit8 v2, v0, 0x1

    const-string v3, "titleKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2590
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->r:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 2592
    :cond_f
    iget-object v2, p1, Lflipboard/objs/ConfigHints$Hint;->s:Ljava/lang/String;

    if-eqz v2, :cond_10

    .line 2593
    add-int/lit8 v2, v0, 0x1

    const-string v3, "subtitleKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2594
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->s:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 2596
    :cond_10
    iget-object v2, p1, Lflipboard/objs/ConfigHints$Hint;->t:Ljava/lang/String;

    if-eqz v2, :cond_11

    .line 2597
    add-int/lit8 v2, v0, 0x1

    const-string v3, "actionTitleKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2598
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->t:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 2600
    :cond_11
    iget-object v2, p1, Lflipboard/objs/ConfigHints$Hint;->u:Ljava/lang/String;

    if-eqz v2, :cond_12

    .line 2601
    add-int/lit8 v2, v0, 0x1

    const-string v3, "cancelTitleKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2602
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->u:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 2604
    :cond_12
    iget v2, p1, Lflipboard/objs/ConfigHints$Hint;->v:F

    float-to-double v2, v2

    const-wide/high16 v4, 0x4020000000000000L    # 8.0

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_13

    .line 2605
    add-int/lit8 v2, v0, 0x1

    const-string v3, "duration"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2606
    iget v0, p1, Lflipboard/objs/ConfigHints$Hint;->v:F

    float-to-double v4, v0

    invoke-virtual {p0, v4, v5}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v2

    .line 2608
    :cond_13
    iget v2, p1, Lflipboard/objs/ConfigHints$Hint;->w:F

    float-to-double v2, v2

    const-wide v4, 0x3fe6666666666666L    # 0.7

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_14

    .line 2609
    add-int/lit8 v2, v0, 0x1

    const-string v3, "pulseOpacity"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2610
    iget v0, p1, Lflipboard/objs/ConfigHints$Hint;->w:F

    float-to-double v4, v0

    invoke-virtual {p0, v4, v5}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v2

    .line 2612
    :cond_14
    iget v2, p1, Lflipboard/objs/ConfigHints$Hint;->x:F

    float-to-double v2, v2

    const-wide v4, 0x4056800000000000L    # 90.0

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_15

    .line 2613
    add-int/lit8 v2, v0, 0x1

    const-string v3, "pulseSize"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2614
    iget v0, p1, Lflipboard/objs/ConfigHints$Hint;->x:F

    float-to-double v4, v0

    invoke-virtual {p0, v4, v5}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v2

    .line 2616
    :cond_15
    iget v2, p1, Lflipboard/objs/ConfigHints$Hint;->y:F

    float-to-double v2, v2

    const-wide/high16 v4, 0x4044000000000000L    # 40.0

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_16

    .line 2617
    add-int/lit8 v2, v0, 0x1

    const-string v3, "bubbleDistance"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2618
    iget v0, p1, Lflipboard/objs/ConfigHints$Hint;->y:F

    float-to-double v4, v0

    invoke-virtual {p0, v4, v5}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v2

    .line 2620
    :cond_16
    iget v2, p1, Lflipboard/objs/ConfigHints$Hint;->z:F

    float-to-double v2, v2

    cmpl-double v2, v2, v6

    if-eqz v2, :cond_17

    .line 2621
    add-int/lit8 v2, v0, 0x1

    const-string v3, "backgroundOpacity"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2622
    iget v0, p1, Lflipboard/objs/ConfigHints$Hint;->z:F

    float-to-double v4, v0

    invoke-virtual {p0, v4, v5}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v2

    .line 2624
    :cond_17
    iget-boolean v2, p1, Lflipboard/objs/ConfigHints$Hint;->A:Z

    if-eqz v2, :cond_18

    .line 2625
    add-int/lit8 v2, v0, 0x1

    const-string v3, "hidePulse"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2626
    iget-boolean v0, p1, Lflipboard/objs/ConfigHints$Hint;->A:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 2628
    :cond_18
    iget-object v2, p1, Lflipboard/objs/ConfigHints$Hint;->B:Ljava/lang/String;

    if-eqz v2, :cond_19

    .line 2629
    add-int/lit8 v2, v0, 0x1

    const-string v3, "tag"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2630
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->B:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 2632
    :cond_19
    iget-object v2, p1, Lflipboard/objs/ConfigHints$Hint;->C:Ljava/util/List;

    if-eqz v2, :cond_1a

    .line 2633
    add-int/lit8 v2, v0, 0x1

    const-string v3, "tags"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2634
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->C:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    move v0, v2

    .line 2636
    :cond_1a
    iget-object v2, p1, Lflipboard/objs/ConfigHints$Hint;->D:Ljava/lang/String;

    if-eqz v2, :cond_1b

    .line 2637
    add-int/lit8 v2, v0, 0x1

    const-string v3, "overlay"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2638
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->D:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 2640
    :cond_1b
    iget-object v2, p1, Lflipboard/objs/ConfigHints$Hint;->E:Ljava/util/List;

    if-eqz v2, :cond_1c

    .line 2641
    add-int/lit8 v2, v0, 0x1

    const-string v3, "conditions"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2642
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->E:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->f(Ljava/util/List;)V

    move v0, v2

    .line 2644
    :cond_1c
    iget-boolean v2, p1, Lflipboard/objs/ConfigHints$Hint;->F:Z

    if-eq v2, v1, :cond_1d

    .line 2645
    add-int/lit8 v1, v0, 0x1

    const-string v2, "touchInitiallyEnabled"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2646
    iget-boolean v0, p1, Lflipboard/objs/ConfigHints$Hint;->F:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 2648
    :cond_1d
    iget-object v1, p1, Lflipboard/objs/ConfigHints$Hint;->G:Ljava/lang/String;

    if-eqz v1, :cond_1e

    .line 2649
    add-int/lit8 v1, v0, 0x1

    const-string v2, "actionTitle"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2650
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->G:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2652
    :cond_1e
    iget-object v1, p1, Lflipboard/objs/ConfigHints$Hint;->H:Ljava/lang/String;

    if-eqz v1, :cond_1f

    .line 2653
    add-int/lit8 v1, v0, 0x1

    const-string v2, "subtitle"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2654
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->H:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2656
    :cond_1f
    iget-object v1, p1, Lflipboard/objs/ConfigHints$Hint;->I:Ljava/lang/String;

    if-eqz v1, :cond_20

    .line 2657
    const-string v1, "title"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2658
    iget-object v0, p1, Lflipboard/objs/ConfigHints$Hint;->I:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 2660
    :cond_20
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0
.end method

.method private a(Lflipboard/objs/ConfigPopularSearches$Category;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2811
    if-nez p1, :cond_0

    .line 2812
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 2826
    :goto_0
    return-void

    .line 2815
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 2816
    iget-object v0, p1, Lflipboard/objs/ConfigPopularSearches$Category;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2817
    const/4 v0, 0x1

    const-string v2, "title"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2818
    iget-object v1, p1, Lflipboard/objs/ConfigPopularSearches$Category;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 2820
    :goto_1
    iget-object v1, p1, Lflipboard/objs/ConfigPopularSearches$Category;->b:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 2821
    const-string v1, "examples"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2822
    iget-object v0, p1, Lflipboard/objs/ConfigPopularSearches$Category;->b:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    .line 2824
    :cond_1
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/ConfigSection;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2893
    if-nez p1, :cond_0

    .line 2894
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 3016
    :goto_0
    return-void

    .line 2897
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 2898
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->bP:Ljava/lang/String;

    if-eqz v0, :cond_1d

    .line 2899
    const/4 v0, 0x1

    const-string v2, "title"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2900
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->bP:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 2902
    :goto_1
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->bQ:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2903
    add-int/lit8 v1, v0, 0x1

    const-string v2, "description"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2904
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->bQ:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2906
    :cond_1
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->bR:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2907
    add-int/lit8 v1, v0, 0x1

    const-string v2, "icon"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2908
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->bR:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2910
    :cond_2
    iget-boolean v1, p1, Lflipboard/objs/ConfigSection;->bS:Z

    if-eqz v1, :cond_3

    .line 2911
    add-int/lit8 v1, v0, 0x1

    const-string v2, "isFavicon"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2912
    iget-boolean v0, p1, Lflipboard/objs/ConfigSection;->bS:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 2914
    :cond_3
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->o:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 2915
    add-int/lit8 v1, v0, 0x1

    const-string v2, "service"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2916
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->o:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2918
    :cond_4
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->p:Ljava/lang/Object;

    if-eqz v1, :cond_5

    .line 2919
    add-int/lit8 v1, v0, 0x1

    const-string v2, "remoteid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2920
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->p:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->b(Ljava/lang/Object;)V

    move v0, v1

    .line 2922
    :cond_5
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->q:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 2923
    add-int/lit8 v1, v0, 0x1

    const-string v2, "titleSuffix"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2924
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->q:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2926
    :cond_6
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->r:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 2927
    add-int/lit8 v1, v0, 0x1

    const-string v2, "sectionTitle"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2928
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->r:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2930
    :cond_7
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->s:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 2931
    add-int/lit8 v1, v0, 0x1

    const-string v2, "unreadRemoteid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2932
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->s:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2934
    :cond_8
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->t:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 2935
    add-int/lit8 v1, v0, 0x1

    const-string v2, "imageURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2936
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->t:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2938
    :cond_9
    iget-boolean v1, p1, Lflipboard/objs/ConfigSection;->u:Z

    if-eqz v1, :cond_a

    .line 2939
    add-int/lit8 v1, v0, 0x1

    const-string v2, "verified"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2940
    iget-boolean v0, p1, Lflipboard/objs/ConfigSection;->u:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 2942
    :cond_a
    iget v1, p1, Lflipboard/objs/ConfigSection;->v:I

    if-eqz v1, :cond_b

    .line 2943
    add-int/lit8 v1, v0, 0x1

    const-string v2, "unreadCount"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2944
    iget v0, p1, Lflipboard/objs/ConfigSection;->v:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 2946
    :cond_b
    iget-boolean v1, p1, Lflipboard/objs/ConfigSection;->w:Z

    if-eqz v1, :cond_c

    .line 2947
    add-int/lit8 v1, v0, 0x1

    const-string v2, "showLogotypeImage"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2948
    iget-boolean v0, p1, Lflipboard/objs/ConfigSection;->w:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 2950
    :cond_c
    iget-boolean v1, p1, Lflipboard/objs/ConfigSection;->x:Z

    if-eqz v1, :cond_d

    .line 2951
    add-int/lit8 v1, v0, 0x1

    const-string v2, "enumerated"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2952
    iget-boolean v0, p1, Lflipboard/objs/ConfigSection;->x:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 2954
    :cond_d
    iget-boolean v1, p1, Lflipboard/objs/ConfigSection;->y:Z

    if-eqz v1, :cond_e

    .line 2955
    add-int/lit8 v1, v0, 0x1

    const-string v2, "private"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2956
    iget-boolean v0, p1, Lflipboard/objs/ConfigSection;->y:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 2958
    :cond_e
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->a:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 2959
    add-int/lit8 v1, v0, 0x1

    const-string v2, "keywords"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2960
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2962
    :cond_f
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    if-eqz v1, :cond_10

    .line 2963
    add-int/lit8 v1, v0, 0x1

    const-string v2, "brick"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2964
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigBrick;)V

    move v0, v1

    .line 2966
    :cond_10
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->c:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 2967
    add-int/lit8 v1, v0, 0x1

    const-string v2, "device"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2968
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2970
    :cond_11
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->d:Ljava/util/List;

    if-eqz v1, :cond_12

    .line 2971
    add-int/lit8 v1, v0, 0x1

    const-string v2, "sections"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2972
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->d:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->g(Ljava/util/List;)V

    move v0, v1

    .line 2974
    :cond_12
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->e:Ljava/lang/String;

    if-eqz v1, :cond_13

    .line 2975
    add-int/lit8 v1, v0, 0x1

    const-string v2, "tileBrickImageSmallURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2976
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2978
    :cond_13
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->f:Ljava/lang/String;

    if-eqz v1, :cond_14

    .line 2979
    add-int/lit8 v1, v0, 0x1

    const-string v2, "tileBrickImageMediumURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2980
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2982
    :cond_14
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->g:Ljava/lang/String;

    if-eqz v1, :cond_15

    .line 2983
    add-int/lit8 v1, v0, 0x1

    const-string v2, "tileBrickImageLargeURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2984
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2986
    :cond_15
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->h:Ljava/lang/String;

    if-eqz v1, :cond_16

    .line 2987
    add-int/lit8 v1, v0, 0x1

    const-string v2, "subhead"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2988
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2990
    :cond_16
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->i:Ljava/lang/String;

    if-eqz v1, :cond_17

    .line 2991
    add-int/lit8 v1, v0, 0x1

    const-string v2, "magazineTarget"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2992
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->i:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2994
    :cond_17
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->j:Ljava/lang/String;

    if-eqz v1, :cond_18

    .line 2995
    add-int/lit8 v1, v0, 0x1

    const-string v2, "socialId"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 2996
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2998
    :cond_18
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->k:Ljava/lang/String;

    if-eqz v1, :cond_19

    .line 2999
    add-int/lit8 v1, v0, 0x1

    const-string v2, "feedType"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3000
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->k:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 3002
    :cond_19
    iget-object v1, p1, Lflipboard/objs/ConfigSection;->l:Lflipboard/objs/Author;

    if-eqz v1, :cond_1a

    .line 3003
    add-int/lit8 v1, v0, 0x1

    const-string v2, "author"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3004
    iget-object v0, p1, Lflipboard/objs/ConfigSection;->l:Lflipboard/objs/Author;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Author;)V

    move v0, v1

    .line 3006
    :cond_1a
    iget-boolean v1, p1, Lflipboard/objs/ConfigSection;->m:Z

    if-eqz v1, :cond_1b

    .line 3007
    add-int/lit8 v1, v0, 0x1

    const-string v2, "imageURLHiddenInList"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3008
    iget-boolean v0, p1, Lflipboard/objs/ConfigSection;->m:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 3010
    :cond_1b
    iget-boolean v1, p1, Lflipboard/objs/ConfigSection;->n:Z

    if-eqz v1, :cond_1c

    .line 3011
    const-string v1, "liveTile"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3012
    iget-boolean v0, p1, Lflipboard/objs/ConfigSection;->n:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    .line 3014
    :cond_1c
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_1d
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/ConfigService;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 3083
    if-nez p1, :cond_0

    .line 3084
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 3678
    :goto_0
    return-void

    .line 3087
    :cond_0
    const/16 v2, 0x7b

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 3088
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bP:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 3089
    const-string v2, "title"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3090
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bP:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 3092
    :cond_1
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bQ:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 3093
    add-int/lit8 v2, v0, 0x1

    const-string v3, "description"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3094
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bQ:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3096
    :cond_2
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bR:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 3097
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3098
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bR:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3100
    :cond_3
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->bS:Z

    if-eqz v2, :cond_4

    .line 3101
    add-int/lit8 v2, v0, 0x1

    const-string v3, "isFavicon"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3102
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->bS:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3104
    :cond_4
    iget-object v2, p1, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 3105
    add-int/lit8 v2, v0, 0x1

    const-string v3, "id"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3106
    iget-object v0, p1, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3108
    :cond_5
    iget-object v2, p1, Lflipboard/objs/ConfigService;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 3109
    add-int/lit8 v2, v0, 0x1

    const-string v3, "displayNameKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3110
    iget-object v0, p1, Lflipboard/objs/ConfigService;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3112
    :cond_6
    iget-object v2, p1, Lflipboard/objs/ConfigService;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 3113
    add-int/lit8 v2, v0, 0x1

    const-string v3, "authenticationMode"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3114
    iget-object v0, p1, Lflipboard/objs/ConfigService;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3116
    :cond_7
    iget-object v2, p1, Lflipboard/objs/ConfigService;->d:Ljava/util/List;

    if-eqz v2, :cond_8

    .line 3117
    add-int/lit8 v2, v0, 0x1

    const-string v3, "authenticationDomainURLs"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3118
    iget-object v0, p1, Lflipboard/objs/ConfigService;->d:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    move v0, v2

    .line 3120
    :cond_8
    iget-object v2, p1, Lflipboard/objs/ConfigService;->e:Ljava/util/List;

    if-eqz v2, :cond_9

    .line 3121
    add-int/lit8 v2, v0, 0x1

    const-string v3, "authenticationExternalURLs"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3122
    iget-object v0, p1, Lflipboard/objs/ConfigService;->e:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    move v0, v2

    .line 3124
    :cond_9
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->f:Z

    if-eq v2, v1, :cond_a

    .line 3125
    add-int/lit8 v2, v0, 0x1

    const-string v3, "canRead"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3126
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->f:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3128
    :cond_a
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->g:Z

    if-eqz v2, :cond_b

    .line 3129
    add-int/lit8 v2, v0, 0x1

    const-string v3, "canCompose"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3130
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->g:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3132
    :cond_b
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->h:Z

    if-eqz v2, :cond_c

    .line 3133
    add-int/lit8 v2, v0, 0x1

    const-string v3, "newestCommentsFirst"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3134
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->h:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3136
    :cond_c
    iget-object v2, p1, Lflipboard/objs/ConfigService;->i:Ljava/lang/String;

    if-eqz v2, :cond_d

    .line 3137
    add-int/lit8 v2, v0, 0x1

    const-string v3, "likeIconStyle"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3138
    iget-object v0, p1, Lflipboard/objs/ConfigService;->i:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3140
    :cond_d
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->j:Z

    if-eqz v2, :cond_e

    .line 3141
    add-int/lit8 v2, v0, 0x1

    const-string v3, "treatSharesAsComments"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3142
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->j:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3144
    :cond_e
    iget-object v2, p1, Lflipboard/objs/ConfigService;->k:Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 3145
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon16URL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3146
    iget-object v0, p1, Lflipboard/objs/ConfigService;->k:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3148
    :cond_f
    iget-object v2, p1, Lflipboard/objs/ConfigService;->l:Ljava/lang/String;

    if-eqz v2, :cond_10

    .line 3149
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon24URL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3150
    iget-object v0, p1, Lflipboard/objs/ConfigService;->l:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3152
    :cond_10
    iget-object v2, p1, Lflipboard/objs/ConfigService;->m:Ljava/lang/String;

    if-eqz v2, :cond_11

    .line 3153
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon32URL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3154
    iget-object v0, p1, Lflipboard/objs/ConfigService;->m:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3156
    :cond_11
    iget-object v2, p1, Lflipboard/objs/ConfigService;->n:Ljava/lang/String;

    if-eqz v2, :cond_12

    .line 3157
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon48URL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3158
    iget-object v0, p1, Lflipboard/objs/ConfigService;->n:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3160
    :cond_12
    iget-object v2, p1, Lflipboard/objs/ConfigService;->o:Ljava/lang/String;

    if-eqz v2, :cond_13

    .line 3161
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon64URL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3162
    iget-object v0, p1, Lflipboard/objs/ConfigService;->o:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3164
    :cond_13
    iget-object v2, p1, Lflipboard/objs/ConfigService;->p:Ljava/lang/String;

    if-eqz v2, :cond_14

    .line 3165
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon96URL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3166
    iget-object v0, p1, Lflipboard/objs/ConfigService;->p:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3168
    :cond_14
    iget-object v2, p1, Lflipboard/objs/ConfigService;->q:Ljava/lang/String;

    if-eqz v2, :cond_15

    .line 3169
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon128URL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3170
    iget-object v0, p1, Lflipboard/objs/ConfigService;->q:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3172
    :cond_15
    iget-object v2, p1, Lflipboard/objs/ConfigService;->r:Ljava/lang/String;

    if-eqz v2, :cond_16

    .line 3173
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon16OpaqueURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3174
    iget-object v0, p1, Lflipboard/objs/ConfigService;->r:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3176
    :cond_16
    iget-object v2, p1, Lflipboard/objs/ConfigService;->s:Ljava/lang/String;

    if-eqz v2, :cond_17

    .line 3177
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon24OpaqueURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3178
    iget-object v0, p1, Lflipboard/objs/ConfigService;->s:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3180
    :cond_17
    iget-object v2, p1, Lflipboard/objs/ConfigService;->t:Ljava/lang/String;

    if-eqz v2, :cond_18

    .line 3181
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon32OpaqueURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3182
    iget-object v0, p1, Lflipboard/objs/ConfigService;->t:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3184
    :cond_18
    iget-object v2, p1, Lflipboard/objs/ConfigService;->u:Ljava/lang/String;

    if-eqz v2, :cond_19

    .line 3185
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon48OpaqueURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3186
    iget-object v0, p1, Lflipboard/objs/ConfigService;->u:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3188
    :cond_19
    iget-object v2, p1, Lflipboard/objs/ConfigService;->v:Ljava/lang/String;

    if-eqz v2, :cond_1a

    .line 3189
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon64OpaqueURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3190
    iget-object v0, p1, Lflipboard/objs/ConfigService;->v:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3192
    :cond_1a
    iget-object v2, p1, Lflipboard/objs/ConfigService;->w:Ljava/lang/String;

    if-eqz v2, :cond_1b

    .line 3193
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon96OpaqueURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3194
    iget-object v0, p1, Lflipboard/objs/ConfigService;->w:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3196
    :cond_1b
    iget-object v2, p1, Lflipboard/objs/ConfigService;->x:Ljava/lang/String;

    if-eqz v2, :cond_1c

    .line 3197
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon128OpaqueURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3198
    iget-object v0, p1, Lflipboard/objs/ConfigService;->x:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3200
    :cond_1c
    iget-object v2, p1, Lflipboard/objs/ConfigService;->y:Ljava/lang/String;

    if-eqz v2, :cond_1d

    .line 3201
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon32GrayURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3202
    iget-object v0, p1, Lflipboard/objs/ConfigService;->y:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3204
    :cond_1d
    iget-object v2, p1, Lflipboard/objs/ConfigService;->z:Ljava/lang/String;

    if-eqz v2, :cond_1e

    .line 3205
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon48GrayURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3206
    iget-object v0, p1, Lflipboard/objs/ConfigService;->z:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3208
    :cond_1e
    iget-object v2, p1, Lflipboard/objs/ConfigService;->A:Ljava/lang/String;

    if-eqz v2, :cond_1f

    .line 3209
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon64GrayURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3210
    iget-object v0, p1, Lflipboard/objs/ConfigService;->A:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3212
    :cond_1f
    iget-object v2, p1, Lflipboard/objs/ConfigService;->B:Ljava/lang/String;

    if-eqz v2, :cond_20

    .line 3213
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon96GrayURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3214
    iget-object v0, p1, Lflipboard/objs/ConfigService;->B:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3216
    :cond_20
    iget-object v2, p1, Lflipboard/objs/ConfigService;->C:Ljava/lang/String;

    if-eqz v2, :cond_21

    .line 3217
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon48ActionURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3218
    iget-object v0, p1, Lflipboard/objs/ConfigService;->C:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3220
    :cond_21
    iget-object v2, p1, Lflipboard/objs/ConfigService;->D:Ljava/lang/String;

    if-eqz v2, :cond_22

    .line 3221
    add-int/lit8 v2, v0, 0x1

    const-string v3, "icon96ActionURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3222
    iget-object v0, p1, Lflipboard/objs/ConfigService;->D:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3224
    :cond_22
    iget-object v2, p1, Lflipboard/objs/ConfigService;->E:Ljava/lang/String;

    if-eqz v2, :cond_23

    .line 3225
    add-int/lit8 v2, v0, 0x1

    const-string v3, "iconAttributionGrayURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3226
    iget-object v0, p1, Lflipboard/objs/ConfigService;->E:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3228
    :cond_23
    iget-object v2, p1, Lflipboard/objs/ConfigService;->F:Ljava/lang/String;

    if-eqz v2, :cond_24

    .line 3229
    add-int/lit8 v2, v0, 0x1

    const-string v3, "subsectionMethodName"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3230
    iget-object v0, p1, Lflipboard/objs/ConfigService;->F:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3232
    :cond_24
    iget-object v2, p1, Lflipboard/objs/ConfigService;->G:Ljava/lang/String;

    if-eqz v2, :cond_25

    .line 3233
    add-int/lit8 v2, v0, 0x1

    const-string v3, "authenticationEndPoint"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3234
    iget-object v0, p1, Lflipboard/objs/ConfigService;->G:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3236
    :cond_25
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->H:Z

    if-eqz v2, :cond_26

    .line 3237
    add-int/lit8 v2, v0, 0x1

    const-string v3, "canShare"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3238
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->H:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3240
    :cond_26
    iget-object v2, p1, Lflipboard/objs/ConfigService;->I:Ljava/lang/String;

    if-eqz v2, :cond_27

    .line 3241
    add-int/lit8 v2, v0, 0x1

    const-string v3, "tocServiceTileColor"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3242
    iget-object v0, p1, Lflipboard/objs/ConfigService;->I:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3244
    :cond_27
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->J:Z

    if-eqz v2, :cond_28

    .line 3245
    add-int/lit8 v2, v0, 0x1

    const-string v3, "supportsUnreadCounts"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3246
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->J:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3248
    :cond_28
    iget v2, p1, Lflipboard/objs/ConfigService;->K:I

    if-eqz v2, :cond_29

    .line 3249
    add-int/lit8 v2, v0, 0x1

    const-string v3, "minVersion"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3250
    iget v0, p1, Lflipboard/objs/ConfigService;->K:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 3252
    :cond_29
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->L:Z

    if-eqz v2, :cond_2a

    .line 3253
    add-int/lit8 v2, v0, 0x1

    const-string v3, "showUserNamesInLikeString"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3254
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->L:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3256
    :cond_2a
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->M:Z

    if-eqz v2, :cond_2b

    .line 3257
    add-int/lit8 v2, v0, 0x1

    const-string v3, "displayUserNameInAvatarPopOver"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3258
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->M:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3260
    :cond_2b
    iget-object v2, p1, Lflipboard/objs/ConfigService;->N:Ljava/lang/String;

    if-eqz v2, :cond_2c

    .line 3261
    add-int/lit8 v2, v0, 0x1

    const-string v3, "profilePopOverUserNameFormat"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3262
    iget-object v0, p1, Lflipboard/objs/ConfigService;->N:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3264
    :cond_2c
    iget-object v2, p1, Lflipboard/objs/ConfigService;->O:Ljava/lang/String;

    if-eqz v2, :cond_2d

    .line 3265
    add-int/lit8 v2, v0, 0x1

    const-string v3, "likeAlertTitleKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3266
    iget-object v0, p1, Lflipboard/objs/ConfigService;->O:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3268
    :cond_2d
    iget-object v2, p1, Lflipboard/objs/ConfigService;->P:Ljava/lang/String;

    if-eqz v2, :cond_2e

    .line 3269
    add-int/lit8 v2, v0, 0x1

    const-string v3, "likeActionType"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3270
    iget-object v0, p1, Lflipboard/objs/ConfigService;->P:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3272
    :cond_2e
    iget-object v2, p1, Lflipboard/objs/ConfigService;->Q:Ljava/lang/String;

    if-eqz v2, :cond_2f

    .line 3273
    add-int/lit8 v2, v0, 0x1

    const-string v3, "followAlertTitleKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3274
    iget-object v0, p1, Lflipboard/objs/ConfigService;->Q:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3276
    :cond_2f
    iget-object v2, p1, Lflipboard/objs/ConfigService;->R:Ljava/lang/String;

    if-eqz v2, :cond_30

    .line 3277
    add-int/lit8 v2, v0, 0x1

    const-string v3, "followActionType"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3278
    iget-object v0, p1, Lflipboard/objs/ConfigService;->R:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3280
    :cond_30
    iget-object v2, p1, Lflipboard/objs/ConfigService;->S:Ljava/lang/String;

    if-eqz v2, :cond_31

    .line 3281
    add-int/lit8 v2, v0, 0x1

    const-string v3, "followAlertTitle"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3282
    iget-object v0, p1, Lflipboard/objs/ConfigService;->S:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3284
    :cond_31
    iget-object v2, p1, Lflipboard/objs/ConfigService;->T:Ljava/lang/String;

    if-eqz v2, :cond_32

    .line 3285
    add-int/lit8 v2, v0, 0x1

    const-string v3, "shareActionType"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3286
    iget-object v0, p1, Lflipboard/objs/ConfigService;->T:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3288
    :cond_32
    iget-object v2, p1, Lflipboard/objs/ConfigService;->U:Ljava/lang/String;

    if-eqz v2, :cond_33

    .line 3289
    add-int/lit8 v2, v0, 0x1

    const-string v3, "showAuthorSectionTitleKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3290
    iget-object v0, p1, Lflipboard/objs/ConfigService;->U:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3292
    :cond_33
    iget-object v2, p1, Lflipboard/objs/ConfigService;->V:Ljava/lang/String;

    if-eqz v2, :cond_34

    .line 3293
    add-int/lit8 v2, v0, 0x1

    const-string v3, "authenticationDomainURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3294
    iget-object v0, p1, Lflipboard/objs/ConfigService;->V:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3296
    :cond_34
    iget-object v2, p1, Lflipboard/objs/ConfigService;->W:Ljava/lang/String;

    if-eqz v2, :cond_35

    .line 3297
    add-int/lit8 v2, v0, 0x1

    const-string v3, "shareItemPlaceholderStringKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3298
    iget-object v0, p1, Lflipboard/objs/ConfigService;->W:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3300
    :cond_35
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->X:Z

    if-eq v2, v1, :cond_36

    .line 3301
    add-int/lit8 v2, v0, 0x1

    const-string v3, "showSignIn"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3302
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->X:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3304
    :cond_36
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->Y:Z

    if-eq v2, v1, :cond_37

    .line 3305
    add-int/lit8 v2, v0, 0x1

    const-string v3, "supportsLike"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3306
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->Y:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3308
    :cond_37
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->Z:Z

    if-eq v2, v1, :cond_38

    .line 3309
    add-int/lit8 v2, v0, 0x1

    const-string v3, "supportsUnlike"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3310
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->Z:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3312
    :cond_38
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->aa:Z

    if-eqz v2, :cond_39

    .line 3313
    add-int/lit8 v2, v0, 0x1

    const-string v3, "supportsFollow"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3314
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->aa:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3316
    :cond_39
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->ab:Z

    if-eqz v2, :cond_3a

    .line 3317
    add-int/lit8 v2, v0, 0x1

    const-string v3, "supportsComposition"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3318
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->ab:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3320
    :cond_3a
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->ac:Z

    if-eqz v2, :cond_3b

    .line 3321
    add-int/lit8 v2, v0, 0x1

    const-string v3, "supportsProfilePopOverShowAuthorSectionAction"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3322
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->ac:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3324
    :cond_3b
    iget-object v2, p1, Lflipboard/objs/ConfigService;->ad:Ljava/lang/String;

    if-eqz v2, :cond_3c

    .line 3325
    add-int/lit8 v2, v0, 0x1

    const-string v3, "authenticationUserNameKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3326
    iget-object v0, p1, Lflipboard/objs/ConfigService;->ad:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3328
    :cond_3c
    iget-object v2, p1, Lflipboard/objs/ConfigService;->ae:Ljava/lang/String;

    if-eqz v2, :cond_3d

    .line 3329
    add-int/lit8 v2, v0, 0x1

    const-string v3, "serviceLoginDescription"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3330
    iget-object v0, p1, Lflipboard/objs/ConfigService;->ae:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3332
    :cond_3d
    iget-object v2, p1, Lflipboard/objs/ConfigService;->af:Ljava/lang/String;

    if-eqz v2, :cond_3e

    .line 3333
    add-int/lit8 v2, v0, 0x1

    const-string v3, "singularShareItemStringKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3334
    iget-object v0, p1, Lflipboard/objs/ConfigService;->af:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3336
    :cond_3e
    iget-object v2, p1, Lflipboard/objs/ConfigService;->ag:Ljava/lang/String;

    if-eqz v2, :cond_3f

    .line 3337
    add-int/lit8 v2, v0, 0x1

    const-string v3, "pluralShareItemStringKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3338
    iget-object v0, p1, Lflipboard/objs/ConfigService;->ag:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3340
    :cond_3f
    iget-object v2, p1, Lflipboard/objs/ConfigService;->ah:Ljava/lang/String;

    if-eqz v2, :cond_40

    .line 3341
    add-int/lit8 v2, v0, 0x1

    const-string v3, "shareIconStyle"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3342
    iget-object v0, p1, Lflipboard/objs/ConfigService;->ah:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3344
    :cond_40
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->ai:Z

    if-eqz v2, :cond_41

    .line 3345
    add-int/lit8 v2, v0, 0x1

    const-string v3, "hideLikesInSocialCard"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3346
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->ai:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3348
    :cond_41
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->aj:Z

    if-eqz v2, :cond_42

    .line 3349
    add-int/lit8 v2, v0, 0x1

    const-string v3, "hasCustomItemTerm"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3350
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->aj:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3352
    :cond_42
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->ak:Z

    if-eqz v2, :cond_43

    .line 3353
    add-int/lit8 v2, v0, 0x1

    const-string v3, "scaleToFitLoginPage"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3354
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->ak:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3356
    :cond_43
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->al:Z

    if-eqz v2, :cond_44

    .line 3357
    add-int/lit8 v2, v0, 0x1

    const-string v3, "hideTableSubTitlesInSocialCard"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3358
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->al:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3360
    :cond_44
    iget-object v2, p1, Lflipboard/objs/ConfigService;->am:Ljava/lang/String;

    if-eqz v2, :cond_45

    .line 3361
    add-int/lit8 v2, v0, 0x1

    const-string v3, "singularGenericItemStringKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3362
    iget-object v0, p1, Lflipboard/objs/ConfigService;->am:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3364
    :cond_45
    iget-object v2, p1, Lflipboard/objs/ConfigService;->an:Ljava/lang/String;

    if-eqz v2, :cond_46

    .line 3365
    add-int/lit8 v2, v0, 0x1

    const-string v3, "pluralGenericItemStringKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3366
    iget-object v0, p1, Lflipboard/objs/ConfigService;->an:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3368
    :cond_46
    iget-object v2, p1, Lflipboard/objs/ConfigService;->ao:Ljava/lang/String;

    if-eqz v2, :cond_47

    .line 3369
    add-int/lit8 v2, v0, 0x1

    const-string v3, "primaryShareButtonTitleKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3370
    iget-object v0, p1, Lflipboard/objs/ConfigService;->ao:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3372
    :cond_47
    iget-object v2, p1, Lflipboard/objs/ConfigService;->ap:Ljava/lang/String;

    if-eqz v2, :cond_48

    .line 3373
    add-int/lit8 v2, v0, 0x1

    const-string v3, "secondaryShareButtonTitleKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3374
    iget-object v0, p1, Lflipboard/objs/ConfigService;->ap:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3376
    :cond_48
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aq:Ljava/lang/String;

    if-eqz v2, :cond_49

    .line 3377
    add-int/lit8 v2, v0, 0x1

    const-string v3, "pastTenseLikeAlertTitleKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3378
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aq:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3380
    :cond_49
    iget-object v2, p1, Lflipboard/objs/ConfigService;->ar:Ljava/lang/String;

    if-eqz v2, :cond_4a

    .line 3381
    add-int/lit8 v2, v0, 0x1

    const-string v3, "verifiedImageURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3382
    iget-object v0, p1, Lflipboard/objs/ConfigService;->ar:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3384
    :cond_4a
    iget-object v2, p1, Lflipboard/objs/ConfigService;->as:Ljava/lang/String;

    if-eqz v2, :cond_4b

    .line 3385
    add-int/lit8 v2, v0, 0x1

    const-string v3, "pastTenseShareAlertTitleKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3386
    iget-object v0, p1, Lflipboard/objs/ConfigService;->as:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3388
    :cond_4b
    iget-object v2, p1, Lflipboard/objs/ConfigService;->at:Ljava/lang/String;

    if-eqz v2, :cond_4c

    .line 3389
    add-int/lit8 v2, v0, 0x1

    const-string v3, "tocSignInTextKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3390
    iget-object v0, p1, Lflipboard/objs/ConfigService;->at:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3392
    :cond_4c
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->au:Z

    if-eqz v2, :cond_4d

    .line 3393
    add-int/lit8 v2, v0, 0x1

    const-string v3, "hideSharedByPrefix"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3394
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->au:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3396
    :cond_4d
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->av:Z

    if-eqz v2, :cond_4e

    .line 3397
    add-int/lit8 v2, v0, 0x1

    const-string v3, "supportsShareTargets"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3398
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->av:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3400
    :cond_4e
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->aw:Z

    if-eqz v2, :cond_4f

    .line 3401
    add-int/lit8 v2, v0, 0x1

    const-string v3, "supportsMultipleSelectedShareTargets"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3402
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->aw:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3404
    :cond_4f
    iget-object v2, p1, Lflipboard/objs/ConfigService;->ax:Ljava/lang/String;

    if-eqz v2, :cond_50

    .line 3405
    add-int/lit8 v2, v0, 0x1

    const-string v3, "pluralShareTargetDisplayNameKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3406
    iget-object v0, p1, Lflipboard/objs/ConfigService;->ax:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3408
    :cond_50
    iget-object v2, p1, Lflipboard/objs/ConfigService;->ay:Ljava/lang/String;

    if-eqz v2, :cond_51

    .line 3409
    add-int/lit8 v2, v0, 0x1

    const-string v3, "shareButtonTitleKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3410
    iget-object v0, p1, Lflipboard/objs/ConfigService;->ay:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3412
    :cond_51
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->az:Z

    if-eqz v2, :cond_52

    .line 3413
    add-int/lit8 v2, v0, 0x1

    const-string v3, "resizeLoginPageToFitWhenKeyboardVisible"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3414
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->az:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3416
    :cond_52
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->aA:Z

    if-eqz v2, :cond_53

    .line 3417
    add-int/lit8 v2, v0, 0x1

    const-string v3, "showMastheadDrillDownInSubsections"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3418
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->aA:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3420
    :cond_53
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aB:Ljava/lang/String;

    if-eqz v2, :cond_54

    .line 3421
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionLeadPromptSignIn"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3422
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aB:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3424
    :cond_54
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aC:Ljava/lang/String;

    if-eqz v2, :cond_55

    .line 3425
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionLeadPromptSignInBoldSubstring"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3426
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aC:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3428
    :cond_55
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aD:Ljava/lang/String;

    if-eqz v2, :cond_56

    .line 3429
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionLeadPromptWantFullAccess"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3430
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aD:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3432
    :cond_56
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aE:Ljava/lang/String;

    if-eqz v2, :cond_57

    .line 3433
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionLeadPromptWantFullAccessBoldSubstring"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3434
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aE:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3436
    :cond_57
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aF:Ljava/lang/String;

    if-eqz v2, :cond_58

    .line 3437
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionLeadPromptUpgrade"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3438
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aF:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3440
    :cond_58
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aG:Ljava/lang/String;

    if-eqz v2, :cond_59

    .line 3441
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionLeadPromptUpgradeBoldSubstring"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3442
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aG:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3444
    :cond_59
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aH:Ljava/lang/String;

    if-eqz v2, :cond_5a

    .line 3445
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionLeadPromptSubscribe"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3446
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3448
    :cond_5a
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aI:Ljava/lang/String;

    if-eqz v2, :cond_5b

    .line 3449
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionLeadPromptSubscribeBoldSubstring"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3450
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aI:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3452
    :cond_5b
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aJ:Ljava/lang/String;

    if-eqz v2, :cond_5c

    .line 3453
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionButtonTitleForNonAuthenticated"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3454
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aJ:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3456
    :cond_5c
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aK:Ljava/lang/String;

    if-eqz v2, :cond_5d

    .line 3457
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionButtonTitleForNonSubscriber"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3458
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aK:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3460
    :cond_5d
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aL:Ljava/lang/String;

    if-eqz v2, :cond_5e

    .line 3461
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionButtonLinkForNonAuthenticated"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3462
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aL:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3464
    :cond_5e
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aM:Ljava/lang/String;

    if-eqz v2, :cond_5f

    .line 3465
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionButtonLinkForNonSubscriber"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3466
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aM:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3468
    :cond_5f
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aN:Ljava/lang/String;

    if-eqz v2, :cond_60

    .line 3469
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionSubscribePromptForTabletNonAuthenticated"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3470
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aN:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3472
    :cond_60
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aO:Ljava/lang/String;

    if-eqz v2, :cond_61

    .line 3473
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionSubscribePromptForSmartphoneNonAuthenticated"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3474
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aO:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3476
    :cond_61
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aP:Ljava/lang/String;

    if-eqz v2, :cond_62

    .line 3477
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionSubscribePromptForTabletAuthenticated"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3478
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aP:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3480
    :cond_62
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aQ:Ljava/lang/String;

    if-eqz v2, :cond_63

    .line 3481
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionSubscribePromptForSmartphoneAuthenticated"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3482
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aQ:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3484
    :cond_63
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aR:Ljava/lang/String;

    if-eqz v2, :cond_64

    .line 3485
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionUpgradePromptForTablet"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3486
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aR:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3488
    :cond_64
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aS:Ljava/lang/String;

    if-eqz v2, :cond_65

    .line 3489
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionUpgradePromptForSmartphone"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3490
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aS:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3492
    :cond_65
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aT:Ljava/lang/String;

    if-eqz v2, :cond_66

    .line 3493
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionLeadPromptSignInNoSub"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3494
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aT:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3496
    :cond_66
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aU:Ljava/lang/String;

    if-eqz v2, :cond_67

    .line 3497
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionLeadPromptSignInBoldSubstringNoSub"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3498
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aU:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3500
    :cond_67
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aV:Ljava/lang/String;

    if-eqz v2, :cond_68

    .line 3501
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionLeadPromptUpgradeNoSub"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3502
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aV:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3504
    :cond_68
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aW:Ljava/lang/String;

    if-eqz v2, :cond_69

    .line 3505
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionLeadPromptUpgradeBoldSubstringNoSub"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3506
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aW:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3508
    :cond_69
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aX:Ljava/lang/String;

    if-eqz v2, :cond_6a

    .line 3509
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionLeadPromptSubscribeNoSub"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3510
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aX:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3512
    :cond_6a
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aY:Ljava/lang/String;

    if-eqz v2, :cond_6b

    .line 3513
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionLeadPromptSubscribeBoldSubstringNoSub"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3514
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aY:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3516
    :cond_6b
    iget-object v2, p1, Lflipboard/objs/ConfigService;->aZ:Ljava/lang/String;

    if-eqz v2, :cond_6c

    .line 3517
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionButtonTitleForNonAuthenticatedNoSub"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3518
    iget-object v0, p1, Lflipboard/objs/ConfigService;->aZ:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3520
    :cond_6c
    iget-object v2, p1, Lflipboard/objs/ConfigService;->ba:Ljava/lang/String;

    if-eqz v2, :cond_6d

    .line 3521
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionButtonTitleForNonSubscriberNoSub"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3522
    iget-object v0, p1, Lflipboard/objs/ConfigService;->ba:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3524
    :cond_6d
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bb:Ljava/lang/String;

    if-eqz v2, :cond_6e

    .line 3525
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionButtonLinkForNonAuthenticatedNoSub"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3526
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bb:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3528
    :cond_6e
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bc:Ljava/lang/String;

    if-eqz v2, :cond_6f

    .line 3529
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionButtonLinkForNonSubscriberNoSub"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3530
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bc:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3532
    :cond_6f
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bd:Ljava/lang/String;

    if-eqz v2, :cond_70

    .line 3533
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionSubscribePrompt"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3534
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bd:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3536
    :cond_70
    iget-object v2, p1, Lflipboard/objs/ConfigService;->be:Ljava/lang/String;

    if-eqz v2, :cond_71

    .line 3537
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionSubscribePromptForTabletNonAuthenticatedNoSub"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3538
    iget-object v0, p1, Lflipboard/objs/ConfigService;->be:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3540
    :cond_71
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bf:Ljava/lang/String;

    if-eqz v2, :cond_72

    .line 3541
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionSubscribePromptForSmartphoneNonAuthenticatedNoSub"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3542
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bf:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3544
    :cond_72
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bg:Ljava/lang/String;

    if-eqz v2, :cond_73

    .line 3545
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionSubscribePromptForTabletAuthenticatedNoSub"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3546
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bg:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3548
    :cond_73
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bh:Ljava/lang/String;

    if-eqz v2, :cond_74

    .line 3549
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionSubscribePromptForSmartphoneAuthenticatedNoSub"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3550
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bh:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3552
    :cond_74
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bi:Ljava/lang/String;

    if-eqz v2, :cond_75

    .line 3553
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionUpgradePromptForTabletNoSub"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3554
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bi:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3556
    :cond_75
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bj:Ljava/lang/String;

    if-eqz v2, :cond_76

    .line 3557
    add-int/lit8 v2, v0, 0x1

    const-string v3, "callToActionUpgradePromptForSmartphoneNoSub"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3558
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bj:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3560
    :cond_76
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bk:Ljava/lang/String;

    if-eqz v2, :cond_77

    .line 3561
    add-int/lit8 v2, v0, 0x1

    const-string v3, "stateNonAuthenticatedTabletHTML"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3562
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bk:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3564
    :cond_77
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bl:Ljava/lang/String;

    if-eqz v2, :cond_78

    .line 3565
    add-int/lit8 v2, v0, 0x1

    const-string v3, "stateNonAuthenticatedSmartphoneHTML"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3566
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bl:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3568
    :cond_78
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bm:Ljava/lang/String;

    if-eqz v2, :cond_79

    .line 3569
    add-int/lit8 v2, v0, 0x1

    const-string v3, "stateAuthenticatedNonSubscriberTabletHTML"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3570
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bm:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3572
    :cond_79
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bn:Ljava/lang/String;

    if-eqz v2, :cond_7a

    .line 3573
    add-int/lit8 v2, v0, 0x1

    const-string v3, "stateAuthenticatedNonSubscriberSmartphoneHTML"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3574
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bn:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3576
    :cond_7a
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bo:Ljava/lang/String;

    if-eqz v2, :cond_7b

    .line 3577
    add-int/lit8 v2, v0, 0x1

    const-string v3, "stateAuthenticatedNonEntitledTabletHTML"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3578
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bo:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3580
    :cond_7b
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bp:Ljava/lang/String;

    if-eqz v2, :cond_7c

    .line 3581
    add-int/lit8 v2, v0, 0x1

    const-string v3, "stateAuthenticatedNonEntitledSmartphoneHTML"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3582
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bp:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3584
    :cond_7c
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bq:Ljava/lang/String;

    if-eqz v2, :cond_7d

    .line 3585
    add-int/lit8 v2, v0, 0x1

    const-string v3, "stateNonAuthenticatedHTML"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3586
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bq:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3588
    :cond_7d
    iget-object v2, p1, Lflipboard/objs/ConfigService;->br:Ljava/lang/String;

    if-eqz v2, :cond_7e

    .line 3589
    add-int/lit8 v2, v0, 0x1

    const-string v3, "stateNonAuthenticatedTabletNoSubHTML"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3590
    iget-object v0, p1, Lflipboard/objs/ConfigService;->br:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3592
    :cond_7e
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bs:Ljava/lang/String;

    if-eqz v2, :cond_7f

    .line 3593
    add-int/lit8 v2, v0, 0x1

    const-string v3, "stateNonAuthenticatedSmartphoneNoSubHTML"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3594
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bs:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3596
    :cond_7f
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bt:Ljava/lang/String;

    if-eqz v2, :cond_80

    .line 3597
    add-int/lit8 v2, v0, 0x1

    const-string v3, "stateAuthenticatedNonSubscriberTabletNoSubHTML"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3598
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bt:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3600
    :cond_80
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bu:Ljava/lang/String;

    if-eqz v2, :cond_81

    .line 3601
    add-int/lit8 v2, v0, 0x1

    const-string v3, "stateAuthenticatedNonSubscriberSmartphoneNoSubHTML"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3602
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bu:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3604
    :cond_81
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bv:Ljava/lang/String;

    if-eqz v2, :cond_82

    .line 3605
    add-int/lit8 v2, v0, 0x1

    const-string v3, "stateAuthenticatedNonEntitledTabletNoSubHTML"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3606
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bv:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3608
    :cond_82
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bw:Ljava/lang/String;

    if-eqz v2, :cond_83

    .line 3609
    add-int/lit8 v2, v0, 0x1

    const-string v3, "stateAuthenticatedNonEntitledSmartphoneNoSubHTML"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3610
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bw:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3612
    :cond_83
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->bx:Z

    if-eqz v2, :cond_84

    .line 3613
    add-int/lit8 v2, v0, 0x1

    const-string v3, "isSubscriptionService"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3614
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->bx:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3616
    :cond_84
    iget-object v2, p1, Lflipboard/objs/ConfigService;->by:Ljava/lang/String;

    if-eqz v2, :cond_85

    .line 3617
    add-int/lit8 v2, v0, 0x1

    const-string v3, "serviceLoginDescriptionKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3618
    iget-object v0, p1, Lflipboard/objs/ConfigService;->by:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3620
    :cond_85
    iget v2, p1, Lflipboard/objs/ConfigService;->bz:I

    if-eqz v2, :cond_86

    .line 3621
    add-int/lit8 v2, v0, 0x1

    const-string v3, "loginPageTabletPortraitWidth"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3622
    iget v0, p1, Lflipboard/objs/ConfigService;->bz:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 3624
    :cond_86
    iget v2, p1, Lflipboard/objs/ConfigService;->bA:I

    if-eqz v2, :cond_87

    .line 3625
    add-int/lit8 v2, v0, 0x1

    const-string v3, "loginPageTabletPortraitHeight"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3626
    iget v0, p1, Lflipboard/objs/ConfigService;->bA:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 3628
    :cond_87
    iget v2, p1, Lflipboard/objs/ConfigService;->bB:I

    if-eqz v2, :cond_88

    .line 3629
    add-int/lit8 v2, v0, 0x1

    const-string v3, "loginPageTabletLandscapeWidth"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3630
    iget v0, p1, Lflipboard/objs/ConfigService;->bB:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 3632
    :cond_88
    iget v2, p1, Lflipboard/objs/ConfigService;->bC:I

    if-eqz v2, :cond_89

    .line 3633
    add-int/lit8 v2, v0, 0x1

    const-string v3, "loginPageTabletLandscapeHeight"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3634
    iget v0, p1, Lflipboard/objs/ConfigService;->bC:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 3636
    :cond_89
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->bD:Z

    if-eqz v2, :cond_8a

    .line 3637
    add-int/lit8 v2, v0, 0x1

    const-string v3, "preserveFSSWhenNavigatingSubsections"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3638
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->bD:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3640
    :cond_8a
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->bE:Z

    if-eqz v2, :cond_8b

    .line 3641
    add-int/lit8 v2, v0, 0x1

    const-string v3, "sharePictureShouldAttributeAuthor"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3642
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->bE:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3644
    :cond_8b
    iget-boolean v2, p1, Lflipboard/objs/ConfigService;->bF:Z

    if-eqz v2, :cond_8c

    .line 3645
    add-int/lit8 v2, v0, 0x1

    const-string v3, "defaultShareTextEnabled"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3646
    iget-boolean v0, p1, Lflipboard/objs/ConfigService;->bF:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 3648
    :cond_8c
    iget-object v2, p1, Lflipboard/objs/ConfigService;->bG:Ljava/lang/String;

    if-eqz v2, :cond_8d

    .line 3649
    add-int/lit8 v2, v0, 0x1

    const-string v3, "meteringTimeUnit"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3650
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bG:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 3652
    :cond_8d
    iget v2, p1, Lflipboard/objs/ConfigService;->bH:I

    if-eq v2, v1, :cond_8e

    .line 3653
    add-int/lit8 v1, v0, 0x1

    const-string v2, "meteringUnitsPerSession"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3654
    iget v0, p1, Lflipboard/objs/ConfigService;->bH:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 3656
    :cond_8e
    iget v1, p1, Lflipboard/objs/ConfigService;->bI:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_8f

    .line 3657
    add-int/lit8 v1, v0, 0x1

    const-string v2, "meteringMaxArticleCountPerSession"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3658
    iget v0, p1, Lflipboard/objs/ConfigService;->bI:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 3660
    :cond_8f
    iget-wide v2, p1, Lflipboard/objs/ConfigService;->bJ:J

    const-wide/32 v4, 0x278d00

    cmp-long v1, v2, v4

    if-eqz v1, :cond_90

    .line 3661
    add-int/lit8 v1, v0, 0x1

    const-string v2, "meteringReadArticleExpirationTime"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3662
    iget-wide v2, p1, Lflipboard/objs/ConfigService;->bJ:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    move v0, v1

    .line 3664
    :cond_90
    iget-object v1, p1, Lflipboard/objs/ConfigService;->bK:Ljava/lang/String;

    if-eqz v1, :cond_91

    .line 3665
    add-int/lit8 v1, v0, 0x1

    const-string v2, "meteringInterstitialBackgroundUrl"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3666
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bK:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 3668
    :cond_91
    iget-object v1, p1, Lflipboard/objs/ConfigService;->bL:Ljava/lang/String;

    if-eqz v1, :cond_92

    .line 3669
    add-int/lit8 v1, v0, 0x1

    const-string v2, "meteringRoadblockBackgroundUrl"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3670
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bL:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 3672
    :cond_92
    iget-object v1, p1, Lflipboard/objs/ConfigService;->bN:Ljava/util/List;

    if-eqz v1, :cond_93

    .line 3673
    const-string v1, "contentDomainURLs"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 3674
    iget-object v0, p1, Lflipboard/objs/ConfigService;->bN:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    .line 3676
    :cond_93
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0
.end method

.method private a(Lflipboard/objs/FeedArticle;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4063
    if-nez p1, :cond_0

    .line 4064
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 4086
    :goto_0
    return-void

    .line 4067
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 4068
    iget-object v0, p1, Lflipboard/objs/FeedArticle;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 4069
    const/4 v0, 0x1

    const-string v2, "url"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4070
    iget-object v1, p1, Lflipboard/objs/FeedArticle;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 4072
    :goto_1
    iget-object v1, p1, Lflipboard/objs/FeedArticle;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 4073
    add-int/lit8 v1, v0, 0x1

    const-string v2, "sectionID"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4074
    iget-object v0, p1, Lflipboard/objs/FeedArticle;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 4076
    :cond_1
    iget-object v1, p1, Lflipboard/objs/FeedArticle;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 4077
    add-int/lit8 v1, v0, 0x1

    const-string v2, "templatePath"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4078
    iget-object v0, p1, Lflipboard/objs/FeedArticle;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 4080
    :cond_2
    iget-object v1, p1, Lflipboard/objs/FeedArticle;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 4081
    const-string v1, "partnerID"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4082
    iget-object v0, p1, Lflipboard/objs/FeedArticle;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 4084
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/FeedItem$Location;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 4915
    if-nez p1, :cond_0

    .line 4916
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 4938
    :goto_0
    return-void

    .line 4919
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 4920
    iget-object v0, p1, Lflipboard/objs/FeedItem$Location;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 4921
    const/4 v0, 0x1

    const-string v2, "name"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4922
    iget-object v1, p1, Lflipboard/objs/FeedItem$Location;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 4924
    :goto_1
    iget-object v1, p1, Lflipboard/objs/FeedItem$Location;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 4925
    add-int/lit8 v1, v0, 0x1

    const-string v2, "locationType"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4926
    iget-object v0, p1, Lflipboard/objs/FeedItem$Location;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 4928
    :cond_1
    iget-wide v2, p1, Lflipboard/objs/FeedItem$Location;->c:D

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_2

    .line 4929
    add-int/lit8 v1, v0, 0x1

    const-string v2, "lat"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4930
    iget-wide v2, p1, Lflipboard/objs/FeedItem$Location;->c:D

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(D)V

    move v0, v1

    .line 4932
    :cond_2
    iget-wide v2, p1, Lflipboard/objs/FeedItem$Location;->d:D

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_3

    .line 4933
    const-string v1, "lon"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4934
    iget-wide v0, p1, Lflipboard/objs/FeedItem$Location;->d:D

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(D)V

    .line 4936
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/FeedItem$Note;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5005
    if-nez p1, :cond_0

    .line 5006
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 5020
    :goto_0
    return-void

    .line 5009
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 5010
    iget-object v0, p1, Lflipboard/objs/FeedItem$Note;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 5011
    const/4 v0, 0x1

    const-string v2, "text"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5012
    iget-object v1, p1, Lflipboard/objs/FeedItem$Note;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 5014
    :goto_1
    iget-object v1, p1, Lflipboard/objs/FeedItem$Note;->b:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 5015
    const-string v1, "sectionLinks"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5016
    iget-object v0, p1, Lflipboard/objs/FeedItem$Note;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->i(Ljava/util/List;)V

    .line 5018
    :cond_1
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/FeedItemRenderHints;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5087
    if-nez p1, :cond_0

    .line 5088
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 5102
    :goto_0
    return-void

    .line 5091
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 5092
    iget-object v0, p1, Lflipboard/objs/FeedItemRenderHints;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 5093
    const/4 v0, 0x1

    const-string v2, "headerBackgroundColor"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5094
    iget-object v1, p1, Lflipboard/objs/FeedItemRenderHints;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 5096
    :goto_1
    iget-object v1, p1, Lflipboard/objs/FeedItemRenderHints;->b:Lflipboard/objs/Image;

    if-eqz v1, :cond_1

    .line 5097
    const-string v1, "headerImage"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5098
    iget-object v0, p1, Lflipboard/objs/FeedItemRenderHints;->b:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    .line 5100
    :cond_1
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/FeedSection;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5169
    if-nez p1, :cond_0

    .line 5170
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 5296
    :goto_0
    return-void

    .line 5173
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 5174
    iget-object v0, p1, Lflipboard/objs/FeedSection;->a:Ljava/lang/String;

    if-eqz v0, :cond_1e

    .line 5175
    const/4 v0, 0x1

    const-string v2, "type"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5176
    iget-object v1, p1, Lflipboard/objs/FeedSection;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 5178
    :goto_1
    iget-object v1, p1, Lflipboard/objs/FeedSection;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5179
    add-int/lit8 v1, v0, 0x1

    const-string v2, "service"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5180
    iget-object v0, p1, Lflipboard/objs/FeedSection;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5182
    :cond_1
    iget-object v1, p1, Lflipboard/objs/FeedSection;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 5183
    add-int/lit8 v1, v0, 0x1

    const-string v2, "remoteid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5184
    iget-object v0, p1, Lflipboard/objs/FeedSection;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5186
    :cond_2
    iget-object v1, p1, Lflipboard/objs/FeedSection;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 5187
    add-int/lit8 v1, v0, 0x1

    const-string v2, "title"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5188
    iget-object v0, p1, Lflipboard/objs/FeedSection;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5190
    :cond_3
    iget-boolean v1, p1, Lflipboard/objs/FeedSection;->e:Z

    if-eqz v1, :cond_4

    .line 5191
    add-int/lit8 v1, v0, 0x1

    const-string v2, "private"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5192
    iget-boolean v0, p1, Lflipboard/objs/FeedSection;->e:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 5194
    :cond_4
    iget-object v1, p1, Lflipboard/objs/FeedSection;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 5195
    add-int/lit8 v1, v0, 0x1

    const-string v2, "unreadRemoteid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5196
    iget-object v0, p1, Lflipboard/objs/FeedSection;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5198
    :cond_5
    iget-object v1, p1, Lflipboard/objs/FeedSection;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 5199
    add-int/lit8 v1, v0, 0x1

    const-string v2, "campaignTarget"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5200
    iget-object v0, p1, Lflipboard/objs/FeedSection;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5202
    :cond_6
    iget-object v1, p1, Lflipboard/objs/FeedSection;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 5203
    add-int/lit8 v1, v0, 0x1

    const-string v2, "partnerId"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5204
    iget-object v0, p1, Lflipboard/objs/FeedSection;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5206
    :cond_7
    iget-object v1, p1, Lflipboard/objs/FeedSection;->i:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 5207
    add-int/lit8 v1, v0, 0x1

    const-string v2, "ecommerceCheckoutURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5208
    iget-object v0, p1, Lflipboard/objs/FeedSection;->i:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5210
    :cond_8
    iget-object v1, p1, Lflipboard/objs/FeedSection;->j:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 5211
    add-int/lit8 v1, v0, 0x1

    const-string v2, "description"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5212
    iget-object v0, p1, Lflipboard/objs/FeedSection;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5214
    :cond_9
    iget-boolean v1, p1, Lflipboard/objs/FeedSection;->k:Z

    if-eqz v1, :cond_a

    .line 5215
    add-int/lit8 v1, v0, 0x1

    const-string v2, "canShare"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5216
    iget-boolean v0, p1, Lflipboard/objs/FeedSection;->k:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 5218
    :cond_a
    iget-object v1, p1, Lflipboard/objs/FeedSection;->l:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 5219
    add-int/lit8 v1, v0, 0x1

    const-string v2, "feedType"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5220
    iget-object v0, p1, Lflipboard/objs/FeedSection;->l:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5222
    :cond_b
    iget-object v1, p1, Lflipboard/objs/FeedSection;->m:Lflipboard/objs/Image;

    if-eqz v1, :cond_c

    .line 5223
    add-int/lit8 v1, v0, 0x1

    const-string v2, "image"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5224
    iget-object v0, p1, Lflipboard/objs/FeedSection;->m:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v1

    .line 5226
    :cond_c
    iget-object v1, p1, Lflipboard/objs/FeedSection;->n:Lflipboard/objs/Image;

    if-eqz v1, :cond_d

    .line 5227
    add-int/lit8 v1, v0, 0x1

    const-string v2, "brick"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5228
    iget-object v0, p1, Lflipboard/objs/FeedSection;->n:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v1

    .line 5230
    :cond_d
    iget-object v1, p1, Lflipboard/objs/FeedSection;->o:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 5231
    add-int/lit8 v1, v0, 0x1

    const-string v2, "socialId"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5232
    iget-object v0, p1, Lflipboard/objs/FeedSection;->o:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5234
    :cond_e
    iget-object v1, p1, Lflipboard/objs/FeedSection;->p:Lflipboard/objs/Author;

    if-eqz v1, :cond_f

    .line 5235
    add-int/lit8 v1, v0, 0x1

    const-string v2, "author"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5236
    iget-object v0, p1, Lflipboard/objs/FeedSection;->p:Lflipboard/objs/Author;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Author;)V

    move v0, v1

    .line 5238
    :cond_f
    iget-object v1, p1, Lflipboard/objs/FeedSection;->q:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 5239
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorDisplayName"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5240
    iget-object v0, p1, Lflipboard/objs/FeedSection;->q:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5242
    :cond_10
    iget-object v1, p1, Lflipboard/objs/FeedSection;->r:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 5243
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorUsername"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5244
    iget-object v0, p1, Lflipboard/objs/FeedSection;->r:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5246
    :cond_11
    iget-object v1, p1, Lflipboard/objs/FeedSection;->s:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 5247
    add-int/lit8 v1, v0, 0x1

    const-string v2, "userid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5248
    iget-object v0, p1, Lflipboard/objs/FeedSection;->s:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5250
    :cond_12
    iget-object v1, p1, Lflipboard/objs/FeedSection;->t:Ljava/lang/String;

    if-eqz v1, :cond_13

    .line 5251
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorDescription"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5252
    iget-object v0, p1, Lflipboard/objs/FeedSection;->t:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5254
    :cond_13
    iget-object v1, p1, Lflipboard/objs/FeedSection;->u:Lflipboard/objs/Image;

    if-eqz v1, :cond_14

    .line 5255
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorImage"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5256
    iget-object v0, p1, Lflipboard/objs/FeedSection;->u:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v1

    .line 5258
    :cond_14
    iget-object v1, p1, Lflipboard/objs/FeedSection;->v:Lflipboard/objs/Image;

    if-eqz v1, :cond_15

    .line 5259
    add-int/lit8 v1, v0, 0x1

    const-string v2, "tileImage"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5260
    iget-object v0, p1, Lflipboard/objs/FeedSection;->v:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v1

    .line 5262
    :cond_15
    iget-object v1, p1, Lflipboard/objs/FeedSection;->w:Ljava/lang/String;

    if-eqz v1, :cond_16

    .line 5263
    add-int/lit8 v1, v0, 0x1

    const-string v2, "remoteidToShare"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5264
    iget-object v0, p1, Lflipboard/objs/FeedSection;->w:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5266
    :cond_16
    iget-object v1, p1, Lflipboard/objs/FeedSection;->x:Ljava/lang/String;

    if-eqz v1, :cond_17

    .line 5267
    add-int/lit8 v1, v0, 0x1

    const-string v2, "magazineTarget"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5268
    iget-object v0, p1, Lflipboard/objs/FeedSection;->x:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5270
    :cond_17
    iget-object v1, p1, Lflipboard/objs/FeedSection;->y:Ljava/lang/String;

    if-eqz v1, :cond_18

    .line 5271
    add-int/lit8 v1, v0, 0x1

    const-string v2, "magazineVisibility"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5272
    iget-object v0, p1, Lflipboard/objs/FeedSection;->y:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5274
    :cond_18
    iget-boolean v1, p1, Lflipboard/objs/FeedSection;->z:Z

    if-eqz v1, :cond_19

    .line 5275
    add-int/lit8 v1, v0, 0x1

    const-string v2, "magazineCanChangeVisibility"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5276
    iget-boolean v0, p1, Lflipboard/objs/FeedSection;->z:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 5278
    :cond_19
    iget-boolean v1, p1, Lflipboard/objs/FeedSection;->A:Z

    if-eqz v1, :cond_1a

    .line 5279
    add-int/lit8 v1, v0, 0x1

    const-string v2, "enumerated"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5280
    iget-boolean v0, p1, Lflipboard/objs/FeedSection;->A:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 5282
    :cond_1a
    iget-boolean v1, p1, Lflipboard/objs/FeedSection;->B:Z

    if-eqz v1, :cond_1b

    .line 5283
    add-int/lit8 v1, v0, 0x1

    const-string v2, "shouldWaitForSidebar"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5284
    iget-boolean v0, p1, Lflipboard/objs/FeedSection;->B:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 5286
    :cond_1b
    iget-boolean v1, p1, Lflipboard/objs/FeedSection;->C:Z

    if-eqz v1, :cond_1c

    .line 5287
    add-int/lit8 v1, v0, 0x1

    const-string v2, "showLogotypeImage"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5288
    iget-boolean v0, p1, Lflipboard/objs/FeedSection;->C:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 5290
    :cond_1c
    iget-object v1, p1, Lflipboard/objs/FeedSection;->D:Ljava/lang/String;

    if-eqz v1, :cond_1d

    .line 5291
    const-string v1, "noContentDisplayStyle"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5292
    iget-object v0, p1, Lflipboard/objs/FeedSection;->D:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 5294
    :cond_1d
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_1e
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/FeedSectionLink;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5363
    if-nez p1, :cond_0

    .line 5364
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 5438
    :goto_0
    return-void

    .line 5367
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 5368
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 5369
    const/4 v0, 0x1

    const-string v2, "type"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5370
    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 5372
    :goto_1
    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5373
    add-int/lit8 v1, v0, 0x1

    const-string v2, "service"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5374
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5376
    :cond_1
    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 5377
    add-int/lit8 v1, v0, 0x1

    const-string v2, "remoteid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5378
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5380
    :cond_2
    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 5381
    add-int/lit8 v1, v0, 0x1

    const-string v2, "title"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5382
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5384
    :cond_3
    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 5385
    add-int/lit8 v1, v0, 0x1

    const-string v2, "referringText"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5386
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5388
    :cond_4
    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 5389
    add-int/lit8 v1, v0, 0x1

    const-string v2, "imageURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5390
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5392
    :cond_5
    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 5393
    add-int/lit8 v1, v0, 0x1

    const-string v2, "sourceURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5394
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5396
    :cond_6
    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 5397
    add-int/lit8 v1, v0, 0x1

    const-string v2, "linkType"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5398
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5400
    :cond_7
    iget-boolean v1, p1, Lflipboard/objs/FeedSectionLink;->i:Z

    if-eqz v1, :cond_8

    .line 5401
    add-int/lit8 v1, v0, 0x1

    const-string v2, "private"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5402
    iget-boolean v0, p1, Lflipboard/objs/FeedSectionLink;->i:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 5404
    :cond_8
    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 5405
    add-int/lit8 v1, v0, 0x1

    const-string v2, "username"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5406
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5408
    :cond_9
    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->k:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 5409
    add-int/lit8 v1, v0, 0x1

    const-string v2, "userID"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5410
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->k:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5412
    :cond_a
    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    if-eqz v1, :cond_b

    .line 5413
    add-int/lit8 v1, v0, 0x1

    const-string v2, "image"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5414
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v1

    .line 5416
    :cond_b
    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 5417
    add-int/lit8 v1, v0, 0x1

    const-string v2, "description"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5418
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5420
    :cond_c
    iget-boolean v1, p1, Lflipboard/objs/FeedSectionLink;->n:Z

    if-eqz v1, :cond_d

    .line 5421
    add-int/lit8 v1, v0, 0x1

    const-string v2, "showFollowAction"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5422
    iget-boolean v0, p1, Lflipboard/objs/FeedSectionLink;->n:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 5424
    :cond_d
    iget-boolean v1, p1, Lflipboard/objs/FeedSectionLink;->o:Z

    if-eqz v1, :cond_e

    .line 5425
    add-int/lit8 v1, v0, 0x1

    const-string v2, "isFollowingAuthor"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5426
    iget-boolean v0, p1, Lflipboard/objs/FeedSectionLink;->o:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 5428
    :cond_e
    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->p:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 5429
    add-int/lit8 v1, v0, 0x1

    const-string v2, "subhead"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5430
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->p:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5432
    :cond_f
    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->s:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 5433
    const-string v1, "feedType"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5434
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->s:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 5436
    :cond_10
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_11
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/FlipResponse$DateResult;)V
    .locals 2

    .prologue
    .line 5635
    if-nez p1, :cond_0

    .line 5636
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 5646
    :goto_0
    return-void

    .line 5638
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 5640
    iget-object v0, p1, Lflipboard/objs/FlipResponse$DateResult;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 5641
    const/4 v0, 0x0

    const-string v1, "$date"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5642
    iget-object v0, p1, Lflipboard/objs/FlipResponse$DateResult;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 5644
    :cond_1
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0
.end method

.method private a(Lflipboard/objs/FlipResponse$FlipImage;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5713
    if-nez p1, :cond_0

    .line 5714
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 5744
    :goto_0
    return-void

    .line 5717
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 5718
    iget-object v0, p1, Lflipboard/objs/FlipResponse$FlipImage;->b:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 5719
    const/4 v0, 0x1

    const-string v2, "type"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5720
    iget-object v1, p1, Lflipboard/objs/FlipResponse$FlipImage;->b:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 5722
    :goto_1
    iget-object v1, p1, Lflipboard/objs/FlipResponse$FlipImage;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5723
    add-int/lit8 v1, v0, 0x1

    const-string v2, "url"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5724
    iget-object v0, p1, Lflipboard/objs/FlipResponse$FlipImage;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5726
    :cond_1
    iget v1, p1, Lflipboard/objs/FlipResponse$FlipImage;->c:I

    if-eqz v1, :cond_2

    .line 5727
    add-int/lit8 v1, v0, 0x1

    const-string v2, "length"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5728
    iget v0, p1, Lflipboard/objs/FlipResponse$FlipImage;->c:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 5730
    :cond_2
    iget v1, p1, Lflipboard/objs/FlipResponse$FlipImage;->d:I

    if-eqz v1, :cond_3

    .line 5731
    add-int/lit8 v1, v0, 0x1

    const-string v2, "width"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5732
    iget v0, p1, Lflipboard/objs/FlipResponse$FlipImage;->d:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 5734
    :cond_3
    iget v1, p1, Lflipboard/objs/FlipResponse$FlipImage;->e:I

    if-eqz v1, :cond_4

    .line 5735
    add-int/lit8 v1, v0, 0x1

    const-string v2, "height"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5736
    iget v0, p1, Lflipboard/objs/FlipResponse$FlipImage;->e:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 5738
    :cond_4
    iget-object v1, p1, Lflipboard/objs/FlipResponse$FlipImage;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 5739
    const-string v1, "hints"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5740
    iget-object v0, p1, Lflipboard/objs/FlipResponse$FlipImage;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 5742
    :cond_5
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/FlipResponse$FlipImagesResult;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5811
    if-nez p1, :cond_0

    .line 5812
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 5854
    :goto_0
    return-void

    .line 5815
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 5816
    iget-object v0, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->a:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 5817
    const/4 v0, 0x1

    const-string v2, "url"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5818
    iget-object v1, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 5820
    :goto_1
    iget-object v1, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5821
    add-int/lit8 v1, v0, 0x1

    const-string v2, "domain"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5822
    iget-object v0, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5824
    :cond_1
    iget-boolean v1, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->c:Z

    if-eqz v1, :cond_2

    .line 5825
    add-int/lit8 v1, v0, 0x1

    const-string v2, "fullscreen"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5826
    iget-boolean v0, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->c:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 5828
    :cond_2
    iget-object v1, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->d:Lflipboard/objs/FlipResponse$FlipImage;

    if-eqz v1, :cond_3

    .line 5829
    add-int/lit8 v1, v0, 0x1

    const-string v2, "original"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5830
    iget-object v0, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->d:Lflipboard/objs/FlipResponse$FlipImage;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FlipResponse$FlipImage;)V

    move v0, v1

    .line 5832
    :cond_3
    iget-object v1, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->e:Lflipboard/objs/FlipResponse$FlipImage;

    if-eqz v1, :cond_4

    .line 5833
    add-int/lit8 v1, v0, 0x1

    const-string v2, "medium"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5834
    iget-object v0, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->e:Lflipboard/objs/FlipResponse$FlipImage;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FlipResponse$FlipImage;)V

    move v0, v1

    .line 5836
    :cond_4
    iget-object v1, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->f:Lflipboard/objs/FlipResponse$FlipImage;

    if-eqz v1, :cond_5

    .line 5837
    add-int/lit8 v1, v0, 0x1

    const-string v2, "small"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5838
    iget-object v0, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->f:Lflipboard/objs/FlipResponse$FlipImage;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FlipResponse$FlipImage;)V

    move v0, v1

    .line 5840
    :cond_5
    iget-object v1, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 5841
    add-int/lit8 v1, v0, 0x1

    const-string v2, "referrer"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5842
    iget-object v0, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 5844
    :cond_6
    iget-object v1, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->h:Lflipboard/objs/FlipResponse$DateResult;

    if-eqz v1, :cond_7

    .line 5845
    add-int/lit8 v1, v0, 0x1

    const-string v2, "created"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5846
    iget-object v0, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->h:Lflipboard/objs/FlipResponse$DateResult;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FlipResponse$DateResult;)V

    move v0, v1

    .line 5848
    :cond_7
    iget-object v1, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->i:Lflipboard/objs/FlipResponse$DateResult;

    if-eqz v1, :cond_8

    .line 5849
    const-string v1, "expires"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5850
    iget-object v0, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;->i:Lflipboard/objs/FlipResponse$DateResult;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FlipResponse$DateResult;)V

    .line 5852
    :cond_8
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/FlipResponse$FlipStats;)V
    .locals 2

    .prologue
    .line 5921
    if-nez p1, :cond_0

    .line 5922
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 5932
    :goto_0
    return-void

    .line 5924
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 5926
    iget v0, p1, Lflipboard/objs/FlipResponse$FlipStats;->a:I

    if-eqz v0, :cond_1

    .line 5927
    const/4 v0, 0x0

    const-string v1, "version"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 5928
    iget v0, p1, Lflipboard/objs/FlipResponse$FlipStats;->a:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    .line 5930
    :cond_1
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0
.end method

.method private a(Lflipboard/objs/Image;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5999
    if-nez p1, :cond_0

    .line 6000
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 6046
    :goto_0
    return-void

    .line 6003
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 6004
    iget-object v0, p1, Lflipboard/objs/Image;->a:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 6005
    const/4 v0, 0x1

    const-string v2, "smallURL"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6006
    iget-object v1, p1, Lflipboard/objs/Image;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 6008
    :goto_1
    iget-object v1, p1, Lflipboard/objs/Image;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 6009
    add-int/lit8 v1, v0, 0x1

    const-string v2, "mediumURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6010
    iget-object v0, p1, Lflipboard/objs/Image;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6012
    :cond_1
    iget-object v1, p1, Lflipboard/objs/Image;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 6013
    add-int/lit8 v1, v0, 0x1

    const-string v2, "largeURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6014
    iget-object v0, p1, Lflipboard/objs/Image;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6016
    :cond_2
    iget-object v1, p1, Lflipboard/objs/Image;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 6017
    add-int/lit8 v1, v0, 0x1

    const-string v2, "xlargeURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6018
    iget-object v0, p1, Lflipboard/objs/Image;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6020
    :cond_3
    iget-object v1, p1, Lflipboard/objs/Image;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 6021
    add-int/lit8 v1, v0, 0x1

    const-string v2, "thumbnailURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6022
    iget-object v0, p1, Lflipboard/objs/Image;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6024
    :cond_4
    iget v1, p1, Lflipboard/objs/Image;->f:I

    if-eqz v1, :cond_5

    .line 6025
    add-int/lit8 v1, v0, 0x1

    const-string v2, "original_width"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6026
    iget v0, p1, Lflipboard/objs/Image;->f:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 6028
    :cond_5
    iget v1, p1, Lflipboard/objs/Image;->g:I

    if-eqz v1, :cond_6

    .line 6029
    add-int/lit8 v1, v0, 0x1

    const-string v2, "original_height"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6030
    iget v0, p1, Lflipboard/objs/Image;->g:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 6032
    :cond_6
    iget-object v1, p1, Lflipboard/objs/Image;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 6033
    add-int/lit8 v1, v0, 0x1

    const-string v2, "original_hints"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6034
    iget-object v0, p1, Lflipboard/objs/Image;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6036
    :cond_7
    iget-object v1, p1, Lflipboard/objs/Image;->i:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 6037
    add-int/lit8 v1, v0, 0x1

    const-string v2, "original_features"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6038
    iget-object v0, p1, Lflipboard/objs/Image;->i:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6040
    :cond_8
    iget-object v1, p1, Lflipboard/objs/Image;->j:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 6041
    const-string v1, "attribution"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6042
    iget-object v0, p1, Lflipboard/objs/Image;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 6044
    :cond_9
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_a
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/Invite;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 6113
    if-nez p1, :cond_0

    .line 6114
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 6156
    :goto_0
    return-void

    .line 6117
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 6118
    iget-object v0, p1, Lflipboard/objs/Invite;->a:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 6119
    const/4 v0, 0x1

    const-string v2, "service"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6120
    iget-object v1, p1, Lflipboard/objs/Invite;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 6122
    :goto_1
    iget-object v1, p1, Lflipboard/objs/Invite;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 6123
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorUsername"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6124
    iget-object v0, p1, Lflipboard/objs/Invite;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6126
    :cond_1
    iget-object v1, p1, Lflipboard/objs/Invite;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 6127
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorDisplayName"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6128
    iget-object v0, p1, Lflipboard/objs/Invite;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6130
    :cond_2
    iget-object v1, p1, Lflipboard/objs/Invite;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 6131
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6132
    iget-object v0, p1, Lflipboard/objs/Invite;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6134
    :cond_3
    iget-object v1, p1, Lflipboard/objs/Invite;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 6135
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorDescription"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6136
    iget-object v0, p1, Lflipboard/objs/Invite;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6138
    :cond_4
    iget-object v1, p1, Lflipboard/objs/Invite;->f:Lflipboard/objs/Image;

    if-eqz v1, :cond_5

    .line 6139
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorImage"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6140
    iget-object v0, p1, Lflipboard/objs/Invite;->f:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v1

    .line 6142
    :cond_5
    iget-object v1, p1, Lflipboard/objs/Invite;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 6143
    add-int/lit8 v1, v0, 0x1

    const-string v2, "magazineTarget"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6144
    iget-object v0, p1, Lflipboard/objs/Invite;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6146
    :cond_6
    iget-object v1, p1, Lflipboard/objs/Invite;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 6147
    add-int/lit8 v1, v0, 0x1

    const-string v2, "inviteToken"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6148
    iget-object v0, p1, Lflipboard/objs/Invite;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6150
    :cond_7
    iget-object v1, p1, Lflipboard/objs/Invite;->i:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 6151
    const-string v1, "title"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6152
    iget-object v0, p1, Lflipboard/objs/Invite;->i:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 6154
    :cond_8
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/LightBoxes$Device;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 6301
    if-nez p1, :cond_0

    .line 6302
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 6316
    :goto_0
    return-void

    .line 6305
    :cond_0
    const/16 v1, 0x7b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 6306
    iget-object v1, p1, Lflipboard/objs/LightBoxes$Device;->a:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 6307
    const/4 v3, 0x1

    const-string v1, "pages"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6308
    iget-object v1, p1, Lflipboard/objs/LightBoxes$Device;->a:Ljava/util/List;

    if-nez v1, :cond_3

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    move v0, v3

    .line 6310
    :cond_1
    :goto_1
    iget-object v1, p1, Lflipboard/objs/LightBoxes$Device;->b:Lflipboard/objs/LightBoxes$Device;

    if-eqz v1, :cond_2

    .line 6311
    const-string v1, "ipad"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6312
    iget-object v0, p1, Lflipboard/objs/LightBoxes$Device;->b:Lflipboard/objs/LightBoxes$Device;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/LightBoxes$Device;)V

    .line 6314
    :cond_2
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    .line 6308
    :cond_3
    const/16 v2, 0x5b

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/LightBoxes$Page;

    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_4

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_4
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/LightBoxes$Page;)V

    move v1, v2

    goto :goto_2

    :cond_5
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    move v0, v3

    goto :goto_1
.end method

.method private a(Lflipboard/objs/LightBoxes$LightBox;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 6383
    if-nez p1, :cond_0

    .line 6384
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 6398
    :goto_0
    return-void

    .line 6387
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 6388
    iget-object v0, p1, Lflipboard/objs/LightBoxes$LightBox;->a:Lflipboard/objs/LightBoxes$Device;

    if-eqz v0, :cond_2

    .line 6389
    const/4 v0, 0x1

    const-string v2, "iphone"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6390
    iget-object v1, p1, Lflipboard/objs/LightBoxes$LightBox;->a:Lflipboard/objs/LightBoxes$Device;

    invoke-direct {p0, v1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/LightBoxes$Device;)V

    .line 6392
    :goto_1
    iget-object v1, p1, Lflipboard/objs/LightBoxes$LightBox;->b:Lflipboard/objs/LightBoxes$Device;

    if-eqz v1, :cond_1

    .line 6393
    const-string v1, "ipad"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6394
    iget-object v0, p1, Lflipboard/objs/LightBoxes$LightBox;->b:Lflipboard/objs/LightBoxes$Device;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/LightBoxes$Device;)V

    .line 6396
    :cond_1
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/LightBoxes$Page;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 6465
    if-nez p1, :cond_0

    .line 6466
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 6496
    :goto_0
    return-void

    .line 6469
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 6470
    iget-object v0, p1, Lflipboard/objs/LightBoxes$Page;->a:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 6471
    const/4 v0, 0x1

    const-string v2, "descriptionKey"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6472
    iget-object v1, p1, Lflipboard/objs/LightBoxes$Page;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 6474
    :goto_1
    iget-object v1, p1, Lflipboard/objs/LightBoxes$Page;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 6475
    add-int/lit8 v1, v0, 0x1

    const-string v2, "titleKey"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6476
    iget-object v0, p1, Lflipboard/objs/LightBoxes$Page;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6478
    :cond_1
    iget-object v1, p1, Lflipboard/objs/LightBoxes$Page;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 6479
    add-int/lit8 v1, v0, 0x1

    const-string v2, "imageFilename"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6480
    iget-object v0, p1, Lflipboard/objs/LightBoxes$Page;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6482
    :cond_2
    iget-object v1, p1, Lflipboard/objs/LightBoxes$Page;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 6483
    add-int/lit8 v1, v0, 0x1

    const-string v2, "rightButtonTitleKey"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6484
    iget-object v0, p1, Lflipboard/objs/LightBoxes$Page;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6486
    :cond_3
    iget-object v1, p1, Lflipboard/objs/LightBoxes$Page;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 6487
    add-int/lit8 v1, v0, 0x1

    const-string v2, "auxiliaryButtonTitleKey"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6488
    iget-object v0, p1, Lflipboard/objs/LightBoxes$Page;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6490
    :cond_4
    iget-object v1, p1, Lflipboard/objs/LightBoxes$Page;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 6491
    const-string v1, "customIdentifier"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6492
    iget-object v0, p1, Lflipboard/objs/LightBoxes$Page;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 6494
    :cond_5
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/SearchResultCategory;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 7075
    if-nez p1, :cond_0

    .line 7076
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 7098
    :goto_0
    return-void

    .line 7079
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 7080
    iget-object v0, p1, Lflipboard/objs/SearchResultCategory;->a:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 7081
    const/4 v0, 0x1

    const-string v2, "categoryTitle"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7082
    iget-object v2, p1, Lflipboard/objs/SearchResultCategory;->a:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 7084
    :goto_1
    iget-object v2, p1, Lflipboard/objs/SearchResultCategory;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 7085
    add-int/lit8 v2, v0, 0x1

    const-string v3, "category"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7086
    iget-object v0, p1, Lflipboard/objs/SearchResultCategory;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 7088
    :cond_1
    iget-object v2, p1, Lflipboard/objs/SearchResultCategory;->c:Ljava/util/List;

    if-eqz v2, :cond_2

    .line 7089
    add-int/lit8 v3, v0, 0x1

    const-string v2, "searchResultItems"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7090
    iget-object v0, p1, Lflipboard/objs/SearchResultCategory;->c:Ljava/util/List;

    if-nez v0, :cond_4

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    move v0, v3

    .line 7092
    :cond_2
    :goto_2
    iget-object v1, p1, Lflipboard/objs/SearchResultCategory;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 7093
    const-string v1, "moreResult"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7094
    iget-object v0, p1, Lflipboard/objs/SearchResultCategory;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 7096
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    .line 7090
    :cond_4
    const/16 v2, 0x5b

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultItem;

    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_5

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_5
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/SearchResultItem;)V

    move v1, v2

    goto :goto_3

    :cond_6
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    move v0, v3

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/SearchResultItem;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 7165
    if-nez p1, :cond_0

    .line 7166
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 7256
    :goto_0
    return-void

    .line 7169
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 7170
    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 7171
    const/4 v0, 0x1

    const-string v2, "title"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7172
    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 7174
    :goto_1
    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->s:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 7175
    add-int/lit8 v1, v0, 0x1

    const-string v2, "description"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7176
    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->s:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7178
    :cond_1
    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->t:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 7179
    add-int/lit8 v1, v0, 0x1

    const-string v2, "imageURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7180
    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->t:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7182
    :cond_2
    iget v1, p1, Lflipboard/objs/SearchResultItem;->u:F

    float-to-double v2, v1

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_3

    .line 7183
    add-int/lit8 v1, v0, 0x1

    const-string v2, "weight"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7184
    iget v0, p1, Lflipboard/objs/SearchResultItem;->u:F

    float-to-double v2, v0

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v1

    .line 7186
    :cond_3
    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->v:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 7187
    add-int/lit8 v1, v0, 0x1

    const-string v2, "service"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7188
    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->v:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7190
    :cond_4
    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    if-eqz v1, :cond_5

    .line 7191
    add-int/lit8 v1, v0, 0x1

    const-string v2, "remoteid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7192
    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->b(Ljava/lang/Object;)V

    move v0, v1

    .line 7194
    :cond_5
    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 7195
    add-int/lit8 v1, v0, 0x1

    const-string v2, "feedType"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7196
    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7198
    :cond_6
    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->y:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 7199
    add-int/lit8 v1, v0, 0x1

    const-string v2, "metricsDisplay"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7200
    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->y:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7202
    :cond_7
    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->z:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 7203
    add-int/lit8 v1, v0, 0x1

    const-string v2, "magazineAuthor"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7204
    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->z:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7206
    :cond_8
    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->A:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 7207
    add-int/lit8 v1, v0, 0x1

    const-string v2, "topic"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7208
    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->A:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7210
    :cond_9
    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->B:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 7211
    add-int/lit8 v1, v0, 0x1

    const-string v2, "partnerId"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7212
    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->B:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7214
    :cond_a
    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->C:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 7215
    add-int/lit8 v1, v0, 0x1

    const-string v2, "userid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7216
    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->C:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7218
    :cond_b
    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->D:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 7219
    add-int/lit8 v1, v0, 0x1

    const-string v2, "categoryList"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7220
    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->D:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7222
    :cond_c
    iget v1, p1, Lflipboard/objs/SearchResultItem;->E:F

    float-to-double v2, v1

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_d

    .line 7223
    add-int/lit8 v1, v0, 0x1

    const-string v2, "categoryListWeight"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7224
    iget v0, p1, Lflipboard/objs/SearchResultItem;->E:F

    float-to-double v2, v0

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v1

    .line 7226
    :cond_d
    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 7227
    add-int/lit8 v1, v0, 0x1

    const-string v2, "category"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7228
    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7230
    :cond_e
    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->G:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 7231
    add-int/lit8 v1, v0, 0x1

    const-string v2, "categoryTitle"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7232
    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->G:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7234
    :cond_f
    iget v1, p1, Lflipboard/objs/SearchResultItem;->H:F

    float-to-double v2, v1

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_10

    .line 7235
    add-int/lit8 v1, v0, 0x1

    const-string v2, "categoryWeight"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7236
    iget v0, p1, Lflipboard/objs/SearchResultItem;->H:F

    float-to-double v2, v0

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v1

    .line 7238
    :cond_10
    iget-boolean v1, p1, Lflipboard/objs/SearchResultItem;->I:Z

    if-eqz v1, :cond_11

    .line 7239
    add-int/lit8 v1, v0, 0x1

    const-string v2, "verified"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7240
    iget-boolean v0, p1, Lflipboard/objs/SearchResultItem;->I:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 7242
    :cond_11
    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->J:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 7243
    add-int/lit8 v1, v0, 0x1

    const-string v2, "titleSuffix"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7244
    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->J:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7246
    :cond_12
    iget-boolean v1, p1, Lflipboard/objs/SearchResultItem;->K:Z

    if-eqz v1, :cond_13

    .line 7247
    add-int/lit8 v1, v0, 0x1

    const-string v2, "isFavicon"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7248
    iget-boolean v0, p1, Lflipboard/objs/SearchResultItem;->K:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 7250
    :cond_13
    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->L:Ljava/lang/String;

    if-eqz v1, :cond_14

    .line 7251
    const-string v1, "bannerColor"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7252
    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->L:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 7254
    :cond_14
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_15
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/SectionListItem;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 7409
    if-nez p1, :cond_0

    .line 7410
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 7520
    :goto_0
    return-void

    .line 7413
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 7414
    iget-object v0, p1, Lflipboard/objs/SectionListItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_1a

    .line 7415
    const/4 v0, 0x1

    const-string v2, "type"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7416
    iget-object v1, p1, Lflipboard/objs/SectionListItem;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 7418
    :goto_1
    iget-object v1, p1, Lflipboard/objs/SectionListItem;->bP:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 7419
    add-int/lit8 v1, v0, 0x1

    const-string v2, "title"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7420
    iget-object v0, p1, Lflipboard/objs/SectionListItem;->bP:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7422
    :cond_1
    iget-object v1, p1, Lflipboard/objs/SectionListItem;->bQ:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 7423
    add-int/lit8 v1, v0, 0x1

    const-string v2, "description"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7424
    iget-object v0, p1, Lflipboard/objs/SectionListItem;->bQ:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7426
    :cond_2
    iget-object v1, p1, Lflipboard/objs/SectionListItem;->bR:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 7427
    add-int/lit8 v1, v0, 0x1

    const-string v2, "icon"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7428
    iget-object v0, p1, Lflipboard/objs/SectionListItem;->bR:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7430
    :cond_3
    iget-boolean v1, p1, Lflipboard/objs/SectionListItem;->bS:Z

    if-eqz v1, :cond_4

    .line 7431
    add-int/lit8 v1, v0, 0x1

    const-string v2, "isFavicon"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7432
    iget-boolean v0, p1, Lflipboard/objs/SectionListItem;->bS:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 7434
    :cond_4
    iget-object v1, p1, Lflipboard/objs/SectionListItem;->o:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 7435
    add-int/lit8 v1, v0, 0x1

    const-string v2, "service"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7436
    iget-object v0, p1, Lflipboard/objs/SectionListItem;->o:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7438
    :cond_5
    iget-object v1, p1, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    if-eqz v1, :cond_6

    .line 7439
    add-int/lit8 v1, v0, 0x1

    const-string v2, "remoteid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7440
    iget-object v0, p1, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->b(Ljava/lang/Object;)V

    move v0, v1

    .line 7442
    :cond_6
    iget-object v1, p1, Lflipboard/objs/SectionListItem;->q:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 7443
    add-int/lit8 v1, v0, 0x1

    const-string v2, "titleSuffix"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7444
    iget-object v0, p1, Lflipboard/objs/SectionListItem;->q:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7446
    :cond_7
    iget-object v1, p1, Lflipboard/objs/SectionListItem;->r:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 7447
    add-int/lit8 v1, v0, 0x1

    const-string v2, "sectionTitle"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7448
    iget-object v0, p1, Lflipboard/objs/SectionListItem;->r:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7450
    :cond_8
    iget-object v1, p1, Lflipboard/objs/SectionListItem;->s:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 7451
    add-int/lit8 v1, v0, 0x1

    const-string v2, "unreadRemoteid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7452
    iget-object v0, p1, Lflipboard/objs/SectionListItem;->s:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7454
    :cond_9
    iget-object v1, p1, Lflipboard/objs/SectionListItem;->t:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 7455
    add-int/lit8 v1, v0, 0x1

    const-string v2, "imageURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7456
    iget-object v0, p1, Lflipboard/objs/SectionListItem;->t:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7458
    :cond_a
    iget-boolean v1, p1, Lflipboard/objs/SectionListItem;->u:Z

    if-eqz v1, :cond_b

    .line 7459
    add-int/lit8 v1, v0, 0x1

    const-string v2, "verified"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7460
    iget-boolean v0, p1, Lflipboard/objs/SectionListItem;->u:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 7462
    :cond_b
    iget v1, p1, Lflipboard/objs/SectionListItem;->v:I

    if-eqz v1, :cond_c

    .line 7463
    add-int/lit8 v1, v0, 0x1

    const-string v2, "unreadCount"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7464
    iget v0, p1, Lflipboard/objs/SectionListItem;->v:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 7466
    :cond_c
    iget-boolean v1, p1, Lflipboard/objs/SectionListItem;->w:Z

    if-eqz v1, :cond_d

    .line 7467
    add-int/lit8 v1, v0, 0x1

    const-string v2, "showLogotypeImage"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7468
    iget-boolean v0, p1, Lflipboard/objs/SectionListItem;->w:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 7470
    :cond_d
    iget-boolean v1, p1, Lflipboard/objs/SectionListItem;->x:Z

    if-eqz v1, :cond_e

    .line 7471
    add-int/lit8 v1, v0, 0x1

    const-string v2, "enumerated"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7472
    iget-boolean v0, p1, Lflipboard/objs/SectionListItem;->x:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 7474
    :cond_e
    iget-boolean v1, p1, Lflipboard/objs/SectionListItem;->y:Z

    if-eqz v1, :cond_f

    .line 7475
    add-int/lit8 v1, v0, 0x1

    const-string v2, "private"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7476
    iget-boolean v0, p1, Lflipboard/objs/SectionListItem;->y:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 7478
    :cond_f
    iget-object v1, p1, Lflipboard/objs/SectionListItem;->b:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 7479
    add-int/lit8 v1, v0, 0x1

    const-string v2, "pageKey"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7480
    iget-object v0, p1, Lflipboard/objs/SectionListItem;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7482
    :cond_10
    iget-boolean v1, p1, Lflipboard/objs/SectionListItem;->c:Z

    if-eqz v1, :cond_11

    .line 7483
    add-int/lit8 v1, v0, 0x1

    const-string v2, "showInline"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7484
    iget-boolean v0, p1, Lflipboard/objs/SectionListItem;->c:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 7486
    :cond_11
    iget-object v1, p1, Lflipboard/objs/SectionListItem;->d:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 7487
    add-int/lit8 v1, v0, 0x1

    const-string v2, "categoryid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7488
    iget-object v0, p1, Lflipboard/objs/SectionListItem;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7490
    :cond_12
    iget-object v1, p1, Lflipboard/objs/SectionListItem;->e:Ljava/util/List;

    if-eqz v1, :cond_13

    .line 7491
    add-int/lit8 v1, v0, 0x1

    const-string v2, "items"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7492
    iget-object v0, p1, Lflipboard/objs/SectionListItem;->e:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->k(Ljava/util/List;)V

    move v0, v1

    .line 7494
    :cond_13
    iget-boolean v1, p1, Lflipboard/objs/SectionListItem;->f:Z

    if-eqz v1, :cond_14

    .line 7495
    add-int/lit8 v1, v0, 0x1

    const-string v2, "disabled"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7496
    iget-boolean v0, p1, Lflipboard/objs/SectionListItem;->f:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 7498
    :cond_14
    iget-boolean v1, p1, Lflipboard/objs/SectionListItem;->g:Z

    if-eqz v1, :cond_15

    .line 7499
    add-int/lit8 v1, v0, 0x1

    const-string v2, "imageURLHiddenInList"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7500
    iget-boolean v0, p1, Lflipboard/objs/SectionListItem;->g:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 7502
    :cond_15
    iget-object v1, p1, Lflipboard/objs/SectionListItem;->h:Lflipboard/objs/Image;

    if-eqz v1, :cond_16

    .line 7503
    add-int/lit8 v1, v0, 0x1

    const-string v2, "image"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7504
    iget-object v0, p1, Lflipboard/objs/SectionListItem;->h:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v1

    .line 7506
    :cond_16
    iget-boolean v1, p1, Lflipboard/objs/SectionListItem;->i:Z

    if-eqz v1, :cond_17

    .line 7507
    add-int/lit8 v1, v0, 0x1

    const-string v2, "default"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7508
    iget-boolean v0, p1, Lflipboard/objs/SectionListItem;->i:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 7510
    :cond_17
    iget-object v1, p1, Lflipboard/objs/SectionListItem;->j:Lflipboard/objs/ConfigBrick;

    if-eqz v1, :cond_18

    .line 7511
    add-int/lit8 v1, v0, 0x1

    const-string v2, "brick"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7512
    iget-object v0, p1, Lflipboard/objs/SectionListItem;->j:Lflipboard/objs/ConfigBrick;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigBrick;)V

    move v0, v1

    .line 7514
    :cond_18
    iget-object v1, p1, Lflipboard/objs/SectionListItem;->k:Lflipboard/objs/Author;

    if-eqz v1, :cond_19

    .line 7515
    const-string v1, "author"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7516
    iget-object v0, p1, Lflipboard/objs/SectionListItem;->k:Lflipboard/objs/Author;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Author;)V

    .line 7518
    :cond_19
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_1a
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/SectionPageTemplate$Area;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 7831
    if-nez p1, :cond_0

    .line 7832
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 7870
    :goto_0
    return-void

    .line 7835
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 7836
    iget v0, p1, Lflipboard/objs/SectionPageTemplate$Area;->a:F

    float-to-double v2, v0

    cmpl-double v0, v2, v4

    if-eqz v0, :cond_8

    .line 7837
    const/4 v0, 0x1

    const-string v2, "x"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7838
    iget v1, p1, Lflipboard/objs/SectionPageTemplate$Area;->a:F

    float-to-double v2, v1

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializerBase;->a(D)V

    .line 7840
    :goto_1
    iget v1, p1, Lflipboard/objs/SectionPageTemplate$Area;->b:F

    float-to-double v2, v1

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_1

    .line 7841
    add-int/lit8 v1, v0, 0x1

    const-string v2, "y"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7842
    iget v0, p1, Lflipboard/objs/SectionPageTemplate$Area;->b:F

    float-to-double v2, v0

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v1

    .line 7844
    :cond_1
    iget v1, p1, Lflipboard/objs/SectionPageTemplate$Area;->c:F

    float-to-double v2, v1

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_2

    .line 7845
    add-int/lit8 v1, v0, 0x1

    const-string v2, "width"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7846
    iget v0, p1, Lflipboard/objs/SectionPageTemplate$Area;->c:F

    float-to-double v2, v0

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v1

    .line 7848
    :cond_2
    iget v1, p1, Lflipboard/objs/SectionPageTemplate$Area;->d:F

    float-to-double v2, v1

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_3

    .line 7849
    add-int/lit8 v1, v0, 0x1

    const-string v2, "height"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7850
    iget v0, p1, Lflipboard/objs/SectionPageTemplate$Area;->d:F

    float-to-double v2, v0

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v1

    .line 7852
    :cond_3
    iget-object v1, p1, Lflipboard/objs/SectionPageTemplate$Area;->e:Ljava/util/List;

    if-eqz v1, :cond_4

    .line 7853
    add-int/lit8 v1, v0, 0x1

    const-string v2, "allowedTypes"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7854
    iget-object v0, p1, Lflipboard/objs/SectionPageTemplate$Area;->e:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    move v0, v1

    .line 7856
    :cond_4
    iget-boolean v1, p1, Lflipboard/objs/SectionPageTemplate$Area;->f:Z

    if-eqz v1, :cond_5

    .line 7857
    add-int/lit8 v1, v0, 0x1

    const-string v2, "fullBleedPortrait"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7858
    iget-boolean v0, p1, Lflipboard/objs/SectionPageTemplate$Area;->f:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 7860
    :cond_5
    iget-boolean v1, p1, Lflipboard/objs/SectionPageTemplate$Area;->g:Z

    if-eqz v1, :cond_6

    .line 7861
    add-int/lit8 v1, v0, 0x1

    const-string v2, "fullBleedLandscape"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7862
    iget-boolean v0, p1, Lflipboard/objs/SectionPageTemplate$Area;->g:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 7864
    :cond_6
    iget-boolean v1, p1, Lflipboard/objs/SectionPageTemplate$Area;->h:Z

    if-eqz v1, :cond_7

    .line 7865
    const-string v1, "landscapeArea"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7866
    iget-boolean v0, p1, Lflipboard/objs/SectionPageTemplate$Area;->h:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    .line 7868
    :cond_7
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/SidebarGroup$Metrics;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 8051
    if-nez p1, :cond_0

    .line 8052
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 8074
    :goto_0
    return-void

    .line 8055
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 8056
    iget-object v0, p1, Lflipboard/objs/SidebarGroup$Metrics;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 8057
    const/4 v0, 0x1

    const-string v2, "type"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8058
    iget-object v1, p1, Lflipboard/objs/SidebarGroup$Metrics;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 8060
    :goto_1
    iget-object v1, p1, Lflipboard/objs/SidebarGroup$Metrics;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 8061
    add-int/lit8 v1, v0, 0x1

    const-string v2, "value"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8062
    iget-object v0, p1, Lflipboard/objs/SidebarGroup$Metrics;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8064
    :cond_1
    iget v1, p1, Lflipboard/objs/SidebarGroup$Metrics;->c:I

    if-eqz v1, :cond_2

    .line 8065
    add-int/lit8 v1, v0, 0x1

    const-string v2, "raw"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8066
    iget v0, p1, Lflipboard/objs/SidebarGroup$Metrics;->c:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 8068
    :cond_2
    iget-object v1, p1, Lflipboard/objs/SidebarGroup$Metrics;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 8069
    const-string v1, "displayName"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8070
    iget-object v0, p1, Lflipboard/objs/SidebarGroup$Metrics;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 8072
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/SidebarGroup$RenderHints;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 8141
    if-nez p1, :cond_0

    .line 8142
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 8208
    :goto_0
    return-void

    .line 8145
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 8146
    iget-object v0, p1, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 8147
    const/4 v0, 0x1

    const-string v2, "type"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8148
    iget-object v1, p1, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 8150
    :goto_1
    iget v1, p1, Lflipboard/objs/SidebarGroup$RenderHints;->c:I

    if-eqz v1, :cond_1

    .line 8151
    add-int/lit8 v1, v0, 0x1

    const-string v2, "pageIndex"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8152
    iget v0, p1, Lflipboard/objs/SidebarGroup$RenderHints;->c:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 8154
    :cond_1
    iget-object v1, p1, Lflipboard/objs/SidebarGroup$RenderHints;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 8155
    add-int/lit8 v1, v0, 0x1

    const-string v2, "backgroundColor"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8156
    iget-object v0, p1, Lflipboard/objs/SidebarGroup$RenderHints;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8158
    :cond_2
    iget-object v1, p1, Lflipboard/objs/SidebarGroup$RenderHints;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 8159
    add-int/lit8 v1, v0, 0x1

    const-string v2, "dividerColor"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8160
    iget-object v0, p1, Lflipboard/objs/SidebarGroup$RenderHints;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8162
    :cond_3
    iget-object v1, p1, Lflipboard/objs/SidebarGroup$RenderHints;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 8163
    add-int/lit8 v1, v0, 0x1

    const-string v2, "textColor"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8164
    iget-object v0, p1, Lflipboard/objs/SidebarGroup$RenderHints;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8166
    :cond_4
    iget-object v1, p1, Lflipboard/objs/SidebarGroup$RenderHints;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 8167
    add-int/lit8 v1, v0, 0x1

    const-string v2, "titleColor"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8168
    iget-object v0, p1, Lflipboard/objs/SidebarGroup$RenderHints;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8170
    :cond_5
    iget-object v1, p1, Lflipboard/objs/SidebarGroup$RenderHints;->h:Lflipboard/objs/Image;

    if-eqz v1, :cond_6

    .line 8171
    add-int/lit8 v1, v0, 0x1

    const-string v2, "logoImage"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8172
    iget-object v0, p1, Lflipboard/objs/SidebarGroup$RenderHints;->h:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v1

    .line 8174
    :cond_6
    iget-object v1, p1, Lflipboard/objs/SidebarGroup$RenderHints;->i:Lflipboard/objs/Image;

    if-eqz v1, :cond_7

    .line 8175
    add-int/lit8 v1, v0, 0x1

    const-string v2, "backgroundImage"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8176
    iget-object v0, p1, Lflipboard/objs/SidebarGroup$RenderHints;->i:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v1

    .line 8178
    :cond_7
    iget-object v1, p1, Lflipboard/objs/SidebarGroup$RenderHints;->j:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 8179
    add-int/lit8 v1, v0, 0x1

    const-string v2, "highlightedColor"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8180
    iget-object v0, p1, Lflipboard/objs/SidebarGroup$RenderHints;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8182
    :cond_8
    iget-boolean v1, p1, Lflipboard/objs/SidebarGroup$RenderHints;->k:Z

    if-eqz v1, :cond_9

    .line 8183
    add-int/lit8 v1, v0, 0x1

    const-string v2, "alwaysShowMore"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8184
    iget-boolean v0, p1, Lflipboard/objs/SidebarGroup$RenderHints;->k:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 8186
    :cond_9
    iget-boolean v1, p1, Lflipboard/objs/SidebarGroup$RenderHints;->l:Z

    if-eqz v1, :cond_a

    .line 8187
    add-int/lit8 v1, v0, 0x1

    const-string v2, "showImages"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8188
    iget-boolean v0, p1, Lflipboard/objs/SidebarGroup$RenderHints;->l:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 8190
    :cond_a
    iget-object v1, p1, Lflipboard/objs/SidebarGroup$RenderHints;->m:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 8191
    add-int/lit8 v1, v0, 0x1

    const-string v2, "style"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8192
    iget-object v0, p1, Lflipboard/objs/SidebarGroup$RenderHints;->m:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8194
    :cond_b
    iget-object v1, p1, Lflipboard/objs/SidebarGroup$RenderHints;->n:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 8195
    add-int/lit8 v1, v0, 0x1

    const-string v2, "expandedGroupTitle"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8196
    iget-object v0, p1, Lflipboard/objs/SidebarGroup$RenderHints;->n:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8198
    :cond_c
    iget-boolean v1, p1, Lflipboard/objs/SidebarGroup$RenderHints;->o:Z

    if-eqz v1, :cond_d

    .line 8199
    add-int/lit8 v1, v0, 0x1

    const-string v2, "showFollowButton"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8200
    iget-boolean v0, p1, Lflipboard/objs/SidebarGroup$RenderHints;->o:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 8202
    :cond_d
    iget-object v1, p1, Lflipboard/objs/SidebarGroup$RenderHints;->p:Lflipboard/objs/SidebarGroup$RenderHints;

    if-eqz v1, :cond_e

    .line 8203
    const-string v1, "expandedGroupRenderHint"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8204
    iget-object v0, p1, Lflipboard/objs/SidebarGroup$RenderHints;->p:Lflipboard/objs/SidebarGroup$RenderHints;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/SidebarGroup$RenderHints;)V

    .line 8206
    :cond_e
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_f
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/SidebarGroup;)V
    .locals 9

    .prologue
    const/16 v8, 0x5d

    const/16 v7, 0x5b

    const/16 v6, 0x2c

    const/4 v1, 0x0

    .line 7937
    if-nez p1, :cond_0

    .line 7938
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 7984
    :goto_0
    return-void

    .line 7941
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 7942
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->d:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 7943
    const/4 v0, 0x1

    const-string v2, "type"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7944
    iget-object v2, p1, Lflipboard/objs/SidebarGroup;->d:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 7946
    :goto_1
    iget-object v2, p1, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 7947
    add-int/lit8 v2, v0, 0x1

    const-string v3, "groupId"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7948
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 7950
    :cond_1
    iget-boolean v2, p1, Lflipboard/objs/SidebarGroup;->b:Z

    if-eqz v2, :cond_2

    .line 7951
    add-int/lit8 v2, v0, 0x1

    const-string v3, "showInline"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7952
    iget-boolean v0, p1, Lflipboard/objs/SidebarGroup;->b:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 7954
    :cond_2
    iget-object v2, p1, Lflipboard/objs/SidebarGroup;->c:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 7955
    add-int/lit8 v2, v0, 0x1

    const-string v3, "title"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7956
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 7958
    :cond_3
    iget-object v2, p1, Lflipboard/objs/SidebarGroup;->e:Ljava/util/List;

    if-eqz v2, :cond_4

    .line 7959
    add-int/lit8 v4, v0, 0x1

    const-string v2, "renderHints"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7960
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->e:Ljava/util/List;

    if-nez v0, :cond_a

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    move v0, v4

    .line 7962
    :cond_4
    :goto_2
    iget-object v2, p1, Lflipboard/objs/SidebarGroup;->f:Ljava/util/List;

    if-eqz v2, :cond_5

    .line 7963
    add-int/lit8 v3, v0, 0x1

    const-string v2, "metrics"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7964
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->f:Ljava/util/List;

    if-nez v0, :cond_d

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    move v0, v3

    .line 7966
    :cond_5
    :goto_3
    iget-object v1, p1, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    if-eqz v1, :cond_6

    .line 7967
    add-int/lit8 v1, v0, 0x1

    const-string v2, "items"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7968
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->c(Ljava/util/List;)V

    move v0, v1

    .line 7970
    :cond_6
    iget-object v1, p1, Lflipboard/objs/SidebarGroup;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 7971
    add-int/lit8 v1, v0, 0x1

    const-string v2, "service"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7972
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7974
    :cond_7
    iget-object v1, p1, Lflipboard/objs/SidebarGroup;->i:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 7975
    add-int/lit8 v1, v0, 0x1

    const-string v2, "impressionValue"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7976
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->i:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 7978
    :cond_8
    iget-object v1, p1, Lflipboard/objs/SidebarGroup;->j:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 7979
    const-string v1, "usageType"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 7980
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 7982
    :cond_9
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    .line 7960
    :cond_a
    invoke-virtual {p0, v7}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v1

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup$RenderHints;

    add-int/lit8 v3, v2, 0x1

    if-lez v2, :cond_b

    invoke-virtual {p0, v6}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_b
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/SidebarGroup$RenderHints;)V

    move v2, v3

    goto :goto_4

    :cond_c
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    move v0, v4

    goto/16 :goto_2

    .line 7964
    :cond_d
    invoke-virtual {p0, v7}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup$Metrics;

    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_e

    invoke-virtual {p0, v6}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_e
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/SidebarGroup$Metrics;)V

    move v1, v2

    goto :goto_5

    :cond_f
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    move v0, v3

    goto/16 :goto_3

    :cond_10
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/UsageEventV2$Properties;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 8721
    if-nez p1, :cond_0

    .line 8722
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 8796
    :goto_0
    return-void

    .line 8725
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 8726
    iget-object v0, p1, Lflipboard/objs/UsageEventV2$Properties;->a:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 8727
    const/4 v0, 0x1

    const-string v2, "unique_id"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8728
    iget-object v1, p1, Lflipboard/objs/UsageEventV2$Properties;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 8730
    :goto_1
    iget-object v1, p1, Lflipboard/objs/UsageEventV2$Properties;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 8731
    add-int/lit8 v1, v0, 0x1

    const-string v2, "session_id"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8732
    iget-object v0, p1, Lflipboard/objs/UsageEventV2$Properties;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8735
    :cond_1
    add-int/lit8 v1, v0, 0x1

    const-string v2, "uid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8736
    iget v0, p1, Lflipboard/objs/UsageEventV2$Properties;->c:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    .line 8738
    iget-wide v2, p1, Lflipboard/objs/UsageEventV2$Properties;->d:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_f

    .line 8739
    add-int/lit8 v0, v1, 0x1

    const-string v2, "time"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8740
    iget-wide v2, p1, Lflipboard/objs/UsageEventV2$Properties;->d:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    .line 8742
    :goto_2
    iget v1, p1, Lflipboard/objs/UsageEventV2$Properties;->e:I

    if-eqz v1, :cond_2

    .line 8743
    add-int/lit8 v1, v0, 0x1

    const-string v2, "time_offset"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8744
    iget v0, p1, Lflipboard/objs/UsageEventV2$Properties;->e:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 8746
    :cond_2
    iget-object v1, p1, Lflipboard/objs/UsageEventV2$Properties;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 8747
    add-int/lit8 v1, v0, 0x1

    const-string v2, "app_version"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8748
    iget-object v0, p1, Lflipboard/objs/UsageEventV2$Properties;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8750
    :cond_3
    iget-object v1, p1, Lflipboard/objs/UsageEventV2$Properties;->g:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 8751
    add-int/lit8 v1, v0, 0x1

    const-string v2, "os"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8752
    iget-object v0, p1, Lflipboard/objs/UsageEventV2$Properties;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8754
    :cond_4
    iget-object v1, p1, Lflipboard/objs/UsageEventV2$Properties;->h:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 8755
    add-int/lit8 v1, v0, 0x1

    const-string v2, "os_version"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8756
    iget-object v0, p1, Lflipboard/objs/UsageEventV2$Properties;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8758
    :cond_5
    iget-object v1, p1, Lflipboard/objs/UsageEventV2$Properties;->i:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 8759
    add-int/lit8 v1, v0, 0x1

    const-string v2, "device_model"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8760
    iget-object v0, p1, Lflipboard/objs/UsageEventV2$Properties;->i:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8762
    :cond_6
    iget-object v1, p1, Lflipboard/objs/UsageEventV2$Properties;->j:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 8763
    add-int/lit8 v1, v0, 0x1

    const-string v2, "device_version"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8764
    iget-object v0, p1, Lflipboard/objs/UsageEventV2$Properties;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8766
    :cond_7
    iget-object v1, p1, Lflipboard/objs/UsageEventV2$Properties;->k:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 8767
    add-int/lit8 v1, v0, 0x1

    const-string v2, "appmode"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8768
    iget-object v0, p1, Lflipboard/objs/UsageEventV2$Properties;->k:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8770
    :cond_8
    iget-object v1, p1, Lflipboard/objs/UsageEventV2$Properties;->l:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 8771
    add-int/lit8 v1, v0, 0x1

    const-string v2, "locale"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8772
    iget-object v0, p1, Lflipboard/objs/UsageEventV2$Properties;->l:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8774
    :cond_9
    iget-object v1, p1, Lflipboard/objs/UsageEventV2$Properties;->m:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 8775
    add-int/lit8 v1, v0, 0x1

    const-string v2, "lang"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8776
    iget-object v0, p1, Lflipboard/objs/UsageEventV2$Properties;->m:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8778
    :cond_a
    iget-object v1, p1, Lflipboard/objs/UsageEventV2$Properties;->n:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 8779
    add-int/lit8 v1, v0, 0x1

    const-string v2, "oem_preload"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8780
    iget-object v0, p1, Lflipboard/objs/UsageEventV2$Properties;->n:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8782
    :cond_b
    iget-object v1, p1, Lflipboard/objs/UsageEventV2$Properties;->o:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 8783
    add-int/lit8 v1, v0, 0x1

    const-string v2, "ab_tests"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8784
    iget-object v0, p1, Lflipboard/objs/UsageEventV2$Properties;->o:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8786
    :cond_c
    iget-object v1, p1, Lflipboard/objs/UsageEventV2$Properties;->p:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 8787
    add-int/lit8 v1, v0, 0x1

    const-string v2, "variant"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8788
    iget-object v0, p1, Lflipboard/objs/UsageEventV2$Properties;->p:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8790
    :cond_d
    iget-object v1, p1, Lflipboard/objs/UsageEventV2$Properties;->q:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 8791
    const-string v1, "user_content_guide"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8792
    iget-object v0, p1, Lflipboard/objs/UsageEventV2$Properties;->q:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 8794
    :cond_e
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_f
    move v0, v1

    goto/16 :goto_2

    :cond_10
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/UserInfo;)V
    .locals 11

    .prologue
    const/16 v10, 0x5d

    const/16 v9, 0x5b

    const/16 v8, 0x2c

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 8863
    if-nez p1, :cond_0

    .line 8864
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 8934
    :goto_0
    return-void

    .line 8867
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 8868
    iget-boolean v0, p1, Lflipboard/objs/UserInfo;->b:Z

    if-eqz v0, :cond_16

    .line 8869
    const-string v0, "success"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8870
    iget-boolean v0, p1, Lflipboard/objs/UserInfo;->b:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 8872
    :goto_1
    iget v3, p1, Lflipboard/objs/UserInfo;->c:I

    if-eqz v3, :cond_1

    .line 8873
    add-int/lit8 v3, v0, 0x1

    const-string v4, "code"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8874
    iget v0, p1, Lflipboard/objs/UserInfo;->c:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v3

    .line 8876
    :cond_1
    iget-object v3, p1, Lflipboard/objs/UserInfo;->d:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 8877
    add-int/lit8 v3, v0, 0x1

    const-string v4, "message"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8878
    iget-object v0, p1, Lflipboard/objs/UserInfo;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v3

    .line 8880
    :cond_2
    iget-wide v4, p1, Lflipboard/objs/UserInfo;->e:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    .line 8881
    add-int/lit8 v3, v0, 0x1

    const-string v4, "time"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8882
    iget-wide v4, p1, Lflipboard/objs/UserInfo;->e:J

    invoke-virtual {p0, v4, v5}, Lflipboard/json/JSONSerializer;->a(J)V

    move v0, v3

    .line 8884
    :cond_3
    iget v3, p1, Lflipboard/objs/UserInfo;->f:I

    if-eqz v3, :cond_4

    .line 8885
    add-int/lit8 v3, v0, 0x1

    const-string v4, "errorcode"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8886
    iget v0, p1, Lflipboard/objs/UserInfo;->f:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v3

    .line 8888
    :cond_4
    iget-object v3, p1, Lflipboard/objs/UserInfo;->g:Ljava/lang/String;

    if-eqz v3, :cond_5

    .line 8889
    add-int/lit8 v3, v0, 0x1

    const-string v4, "errormessage"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8890
    iget-object v0, p1, Lflipboard/objs/UserInfo;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v3

    .line 8892
    :cond_5
    iget-object v3, p1, Lflipboard/objs/UserInfo;->h:Ljava/lang/String;

    if-eqz v3, :cond_6

    .line 8893
    add-int/lit8 v3, v0, 0x1

    const-string v4, "displaymessage"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8894
    iget-object v0, p1, Lflipboard/objs/UserInfo;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v3

    .line 8896
    :cond_6
    iget v3, p1, Lflipboard/objs/UserInfo;->a:I

    if-eqz v3, :cond_7

    .line 8897
    add-int/lit8 v3, v0, 0x1

    const-string v4, "userid"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8898
    iget v0, p1, Lflipboard/objs/UserInfo;->a:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v3

    .line 8900
    :cond_7
    iget-object v3, p1, Lflipboard/objs/UserInfo;->i:Ljava/lang/String;

    if-eqz v3, :cond_8

    .line 8901
    add-int/lit8 v3, v0, 0x1

    const-string v4, "name"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8902
    iget-object v0, p1, Lflipboard/objs/UserInfo;->i:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v3

    .line 8904
    :cond_8
    iget-object v3, p1, Lflipboard/objs/UserInfo;->j:Ljava/util/List;

    if-eqz v3, :cond_9

    .line 8905
    add-int/lit8 v3, v0, 0x1

    const-string v4, "myServices"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8906
    iget-object v0, p1, Lflipboard/objs/UserInfo;->j:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->n(Ljava/util/List;)V

    move v0, v3

    .line 8908
    :cond_9
    iget-object v3, p1, Lflipboard/objs/UserInfo;->k:Ljava/util/List;

    if-eqz v3, :cond_a

    .line 8909
    add-int/lit8 v3, v0, 0x1

    const-string v4, "myReadLaterServices"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8910
    iget-object v0, p1, Lflipboard/objs/UserInfo;->k:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->n(Ljava/util/List;)V

    move v0, v3

    .line 8912
    :cond_a
    iget-object v3, p1, Lflipboard/objs/UserInfo;->l:Ljava/util/List;

    if-eqz v3, :cond_b

    .line 8913
    add-int/lit8 v5, v0, 0x1

    const-string v3, "states"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8914
    iget-object v0, p1, Lflipboard/objs/UserInfo;->l:Ljava/util/List;

    if-nez v0, :cond_10

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    move v0, v5

    .line 8916
    :cond_b
    :goto_2
    iget-object v3, p1, Lflipboard/objs/UserInfo;->m:Lflipboard/objs/UserInfo;

    if-eqz v3, :cond_c

    .line 8917
    add-int/lit8 v3, v0, 0x1

    const-string v4, "userInfo"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8918
    iget-object v0, p1, Lflipboard/objs/UserInfo;->m:Lflipboard/objs/UserInfo;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UserInfo;)V

    move v0, v3

    .line 8920
    :cond_c
    iget-object v3, p1, Lflipboard/objs/UserInfo;->n:Ljava/util/List;

    if-eqz v3, :cond_d

    .line 8921
    add-int/lit8 v4, v0, 0x1

    const-string v3, "magazines"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8922
    iget-object v0, p1, Lflipboard/objs/UserInfo;->n:Ljava/util/List;

    if-nez v0, :cond_13

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    move v0, v4

    .line 8924
    :cond_d
    :goto_3
    iget-boolean v2, p1, Lflipboard/objs/UserInfo;->o:Z

    if-eq v2, v1, :cond_e

    .line 8925
    add-int/lit8 v1, v0, 0x1

    const-string v2, "hasToc"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8926
    iget-boolean v0, p1, Lflipboard/objs/UserInfo;->o:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 8928
    :cond_e
    iget-object v1, p1, Lflipboard/objs/UserInfo;->p:Lflipboard/json/FLObject;

    if-eqz v1, :cond_f

    .line 8929
    const-string v1, "experiments"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8930
    iget-object v0, p1, Lflipboard/objs/UserInfo;->p:Lflipboard/json/FLObject;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/json/FLObject;)V

    .line 8932
    :cond_f
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    .line 8914
    :cond_10
    invoke-virtual {p0, v9}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v2

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserState$State;

    add-int/lit8 v4, v3, 0x1

    if-lez v3, :cond_11

    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_11
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UserState$State;)V

    move v3, v4

    goto :goto_4

    :cond_12
    invoke-virtual {p0, v10}, Lflipboard/json/JSONSerializer;->a(C)V

    move v0, v5

    goto :goto_2

    .line 8922
    :cond_13
    invoke-virtual {p0, v9}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Magazine;

    add-int/lit8 v3, v2, 0x1

    if-lez v2, :cond_14

    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_14
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/Magazine;)V

    move v2, v3

    goto :goto_5

    :cond_15
    invoke-virtual {p0, v10}, Lflipboard/json/JSONSerializer;->a(C)V

    move v0, v4

    goto :goto_3

    :cond_16
    move v0, v2

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/UserService$Cookie;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 9261
    if-nez p1, :cond_0

    .line 9262
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 9280
    :goto_0
    return-void

    .line 9265
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 9266
    iget-object v0, p1, Lflipboard/objs/UserService$Cookie;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 9267
    const/4 v0, 0x1

    const-string v2, "name"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9268
    iget-object v1, p1, Lflipboard/objs/UserService$Cookie;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 9270
    :goto_1
    iget-object v1, p1, Lflipboard/objs/UserService$Cookie;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 9271
    add-int/lit8 v1, v0, 0x1

    const-string v2, "domain"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9272
    iget-object v0, p1, Lflipboard/objs/UserService$Cookie;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 9274
    :cond_1
    iget-object v1, p1, Lflipboard/objs/UserService$Cookie;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 9275
    const-string v1, "value"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9276
    iget-object v0, p1, Lflipboard/objs/UserService$Cookie;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 9278
    :cond_2
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/UserServices$StateRevisions;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 9445
    if-nez p1, :cond_0

    .line 9446
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 9468
    :goto_0
    return-void

    .line 9449
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 9450
    iget-object v0, p1, Lflipboard/objs/UserServices$StateRevisions;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 9451
    const/4 v0, 0x1

    const-string v2, "user"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9452
    iget-object v1, p1, Lflipboard/objs/UserServices$StateRevisions;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 9454
    :goto_1
    iget-object v1, p1, Lflipboard/objs/UserServices$StateRevisions;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 9455
    add-int/lit8 v1, v0, 0x1

    const-string v2, "coverSections"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9456
    iget-object v0, p1, Lflipboard/objs/UserServices$StateRevisions;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 9458
    :cond_1
    iget-object v1, p1, Lflipboard/objs/UserServices$StateRevisions;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 9459
    add-int/lit8 v1, v0, 0x1

    const-string v2, "flap_curation"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9460
    iget-object v0, p1, Lflipboard/objs/UserServices$StateRevisions;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 9462
    :cond_2
    iget-object v1, p1, Lflipboard/objs/UserServices$StateRevisions;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 9463
    const-string v1, "flap_coverSections"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9464
    iget-object v0, p1, Lflipboard/objs/UserServices$StateRevisions;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 9466
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/UserServices;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 9347
    if-nez p1, :cond_0

    .line 9348
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 9378
    :goto_0
    return-void

    .line 9351
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 9352
    iget-object v0, p1, Lflipboard/objs/UserServices;->a:Ljava/util/List;

    if-eqz v0, :cond_6

    .line 9353
    const/4 v0, 0x1

    const-string v2, "myServices"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9354
    iget-object v1, p1, Lflipboard/objs/UserServices;->a:Ljava/util/List;

    invoke-direct {p0, v1}, Lflipboard/json/JSONSerializer;->n(Ljava/util/List;)V

    .line 9356
    :goto_1
    iget-object v1, p1, Lflipboard/objs/UserServices;->b:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 9357
    add-int/lit8 v1, v0, 0x1

    const-string v2, "myReadLaterServices"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9358
    iget-object v0, p1, Lflipboard/objs/UserServices;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->n(Ljava/util/List;)V

    move v0, v1

    .line 9360
    :cond_1
    iget-object v1, p1, Lflipboard/objs/UserServices;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 9361
    add-int/lit8 v1, v0, 0x1

    const-string v2, "name"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9362
    iget-object v0, p1, Lflipboard/objs/UserServices;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 9364
    :cond_2
    iget-object v1, p1, Lflipboard/objs/UserServices;->d:Lflipboard/objs/UserServices$StateRevisions;

    if-eqz v1, :cond_3

    .line 9365
    add-int/lit8 v1, v0, 0x1

    const-string v2, "stateRevisions"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9366
    iget-object v0, p1, Lflipboard/objs/UserServices;->d:Lflipboard/objs/UserServices$StateRevisions;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UserServices$StateRevisions;)V

    move v0, v1

    .line 9368
    :cond_3
    iget v1, p1, Lflipboard/objs/UserServices;->e:I

    if-eqz v1, :cond_4

    .line 9369
    add-int/lit8 v1, v0, 0x1

    const-string v2, "userid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9370
    iget v0, p1, Lflipboard/objs/UserServices;->e:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 9372
    :cond_4
    iget-object v1, p1, Lflipboard/objs/UserServices;->f:Lflipboard/json/FLObject;

    if-eqz v1, :cond_5

    .line 9373
    const-string v1, "experiments"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9374
    iget-object v0, p1, Lflipboard/objs/UserServices;->f:Lflipboard/json/FLObject;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/json/FLObject;)V

    .line 9376
    :cond_5
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/UserState$Data;)V
    .locals 10

    .prologue
    const/16 v9, 0x7d

    const/16 v8, 0x7b

    const/16 v7, 0x3a

    const/16 v6, 0x2c

    const/4 v1, 0x0

    .line 9653
    if-nez p1, :cond_0

    .line 9654
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 9704
    :goto_0
    return-void

    .line 9657
    :cond_0
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 9658
    iget-object v0, p1, Lflipboard/objs/UserState$Data;->a:Ljava/util/List;

    if-eqz v0, :cond_11

    .line 9659
    const/4 v0, 0x1

    const-string v2, "hiddenURLStrings"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9660
    iget-object v2, p1, Lflipboard/objs/UserState$Data;->a:Ljava/util/List;

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    .line 9662
    :goto_1
    iget-object v2, p1, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 9663
    add-int/lit8 v2, v0, 0x1

    const-string v3, "mutedAuthors"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9664
    iget-object v0, p1, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->o(Ljava/util/List;)V

    move v0, v2

    .line 9666
    :cond_1
    iget-object v2, p1, Lflipboard/objs/UserState$Data;->c:Ljava/util/List;

    if-eqz v2, :cond_2

    .line 9667
    add-int/lit8 v4, v0, 0x1

    const-string v2, "tocSections"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9668
    iget-object v0, p1, Lflipboard/objs/UserState$Data;->c:Ljava/util/List;

    if-nez v0, :cond_a

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    move v0, v4

    .line 9670
    :cond_2
    :goto_2
    iget-object v2, p1, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    if-eqz v2, :cond_3

    .line 9671
    add-int/lit8 v3, v0, 0x1

    const-string v2, "mutedAuthorsForSectionIdentifiers"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9672
    iget-object v0, p1, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    if-nez v0, :cond_d

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    move v0, v3

    .line 9674
    :cond_3
    :goto_3
    iget-boolean v1, p1, Lflipboard/objs/UserState$Data;->e:Z

    if-eqz v1, :cond_4

    .line 9675
    add-int/lit8 v1, v0, 0x1

    const-string v2, "coverStoriesHidden"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9676
    iget-boolean v0, p1, Lflipboard/objs/UserState$Data;->e:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 9678
    :cond_4
    iget-object v1, p1, Lflipboard/objs/UserState$Data;->f:Lflipboard/json/FLObject;

    if-eqz v1, :cond_5

    .line 9679
    add-int/lit8 v1, v0, 0x1

    const-string v2, "pushNotificationSettings"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9680
    iget-object v0, p1, Lflipboard/objs/UserState$Data;->f:Lflipboard/json/FLObject;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/json/FLObject;)V

    move v0, v1

    .line 9682
    :cond_5
    iget-object v1, p1, Lflipboard/objs/UserState$Data;->g:Ljava/util/List;

    if-eqz v1, :cond_6

    .line 9683
    add-int/lit8 v1, v0, 0x1

    const-string v2, "selectedShareServices"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9684
    iget-object v0, p1, Lflipboard/objs/UserState$Data;->g:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    move v0, v1

    .line 9686
    :cond_6
    iget-object v1, p1, Lflipboard/objs/UserState$Data;->h:Ljava/util/List;

    if-eqz v1, :cond_7

    .line 9687
    add-int/lit8 v1, v0, 0x1

    const-string v2, "emailsForBugReporting"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9688
    iget-object v0, p1, Lflipboard/objs/UserState$Data;->h:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    move v0, v1

    .line 9690
    :cond_7
    iget-object v1, p1, Lflipboard/objs/UserState$Data;->i:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 9691
    add-int/lit8 v1, v0, 0x1

    const-string v2, "prominenceOverrideType"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9692
    iget-object v0, p1, Lflipboard/objs/UserState$Data;->i:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 9694
    :cond_8
    iget-object v1, p1, Lflipboard/objs/UserState$Data;->j:Lflipboard/json/FLObject;

    if-eqz v1, :cond_10

    .line 9695
    iget-object v1, p1, Lflipboard/objs/UserState$Data;->j:Lflipboard/json/FLObject;

    invoke-virtual {v1}, Lflipboard/json/FLObject;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 9696
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_9

    invoke-virtual {p0, v6}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 9697
    :cond_9
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 9698
    invoke-virtual {p0, v7}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 9699
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->b(Ljava/lang/Object;)V

    move v1, v2

    .line 9700
    goto :goto_4

    .line 9668
    :cond_a
    const/16 v2, 0x5b

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v1

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/TOCSection;

    add-int/lit8 v3, v2, 0x1

    if-lez v2, :cond_b

    invoke-virtual {p0, v6}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_b
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/TOCSection;)V

    move v2, v3

    goto :goto_5

    :cond_c
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    move v0, v4

    goto/16 :goto_2

    .line 9672
    :cond_d
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_e

    invoke-virtual {p0, v6}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_e
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    invoke-virtual {p0, v7}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->o(Ljava/util/List;)V

    move v1, v2

    goto :goto_6

    :cond_f
    invoke-virtual {p0, v9}, Lflipboard/json/JSONSerializer;->a(C)V

    move v0, v3

    goto/16 :goto_3

    .line 9702
    :cond_10
    invoke-virtual {p0, v9}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_11
    move v0, v1

    goto/16 :goto_1
.end method

.method private a(Lflipboard/objs/UserState$MutedAuthor;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9771
    if-nez p1, :cond_0

    .line 9772
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 9802
    :goto_0
    return-void

    .line 9775
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 9776
    iget-object v0, p1, Lflipboard/objs/UserState$MutedAuthor;->b:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 9777
    const/4 v0, 0x1

    const-string v2, "authorID"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9778
    iget-object v1, p1, Lflipboard/objs/UserState$MutedAuthor;->b:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 9780
    :goto_1
    iget-object v1, p1, Lflipboard/objs/UserState$MutedAuthor;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 9781
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorUsername"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9782
    iget-object v0, p1, Lflipboard/objs/UserState$MutedAuthor;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 9784
    :cond_1
    iget-object v1, p1, Lflipboard/objs/UserState$MutedAuthor;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 9785
    add-int/lit8 v1, v0, 0x1

    const-string v2, "serviceName"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9786
    iget-object v0, p1, Lflipboard/objs/UserState$MutedAuthor;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 9788
    :cond_2
    iget-object v1, p1, Lflipboard/objs/UserState$MutedAuthor;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 9789
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorDisplayName"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9790
    iget-object v0, p1, Lflipboard/objs/UserState$MutedAuthor;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 9792
    :cond_3
    iget-object v1, p1, Lflipboard/objs/UserState$MutedAuthor;->a:Lflipboard/json/FLObject;

    if-eqz v1, :cond_5

    .line 9793
    iget-object v1, p1, Lflipboard/objs/UserState$MutedAuthor;->a:Lflipboard/json/FLObject;

    invoke-virtual {v1}, Lflipboard/json/FLObject;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 9794
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_4

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 9795
    :cond_4
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 9796
    const/16 v1, 0x3a

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 9797
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->b(Ljava/lang/Object;)V

    move v1, v2

    .line 9798
    goto :goto_2

    .line 9800
    :cond_5
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method private a(Lflipboard/objs/UserState$State;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 9869
    if-nez p1, :cond_0

    .line 9870
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 9896
    :goto_0
    return-void

    .line 9873
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 9874
    iget-object v0, p1, Lflipboard/objs/UserState$State;->a:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 9875
    const/4 v0, 0x1

    const-string v2, "type"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9876
    iget-object v1, p1, Lflipboard/objs/UserState$State;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 9878
    :goto_1
    iget-wide v2, p1, Lflipboard/objs/UserState$State;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 9879
    add-int/lit8 v1, v0, 0x1

    const-string v2, "dateModified"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9880
    iget-wide v2, p1, Lflipboard/objs/UserState$State;->b:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    move v0, v1

    .line 9882
    :cond_1
    iget-object v1, p1, Lflipboard/objs/UserState$State;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 9883
    add-int/lit8 v1, v0, 0x1

    const-string v2, "revision"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9884
    iget-object v0, p1, Lflipboard/objs/UserState$State;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 9886
    :cond_2
    iget-object v1, p1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    if-eqz v1, :cond_3

    .line 9887
    add-int/lit8 v1, v0, 0x1

    const-string v2, "data"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9888
    iget-object v0, p1, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UserState$Data;)V

    move v0, v1

    .line 9890
    :cond_3
    iget-boolean v1, p1, Lflipboard/objs/UserState$State;->e:Z

    if-eqz v1, :cond_4

    .line 9891
    const-string v1, "unmodified"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9892
    iget-boolean v0, p1, Lflipboard/objs/UserState$State;->e:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    .line 9894
    :cond_4
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public static a(Lflipboard/objs/FeedItem;)[B
    .locals 2

    .prologue
    .line 4139
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x258

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 4140
    new-instance v1, Lflipboard/json/JSONSerializer;

    invoke-direct {v1, v0}, Lflipboard/json/JSONSerializer;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, p0}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/FeedItem;)V

    .line 4141
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static a(Lflipboard/objs/Link;)[B
    .locals 2

    .prologue
    .line 6549
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x258

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 6550
    new-instance v1, Lflipboard/json/JSONSerializer;

    invoke-direct {v1, v0}, Lflipboard/json/JSONSerializer;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, p0}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/Link;)V

    .line 6551
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static a(Lflipboard/objs/Magazine;)[B
    .locals 2

    .prologue
    .line 6635
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x258

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 6636
    new-instance v1, Lflipboard/json/JSONSerializer;

    invoke-direct {v1, v0}, Lflipboard/json/JSONSerializer;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, p0}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/Magazine;)V

    .line 6637
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static a(Lflipboard/objs/TOCSection;)[B
    .locals 2

    .prologue
    .line 8367
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x258

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 8368
    new-instance v1, Lflipboard/json/JSONSerializer;

    invoke-direct {v1, v0}, Lflipboard/json/JSONSerializer;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, p0}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/TOCSection;)V

    .line 8369
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static a(Lflipboard/objs/UserService;)[B
    .locals 2

    .prologue
    .line 9097
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x258

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 9098
    new-instance v1, Lflipboard/json/JSONSerializer;

    invoke-direct {v1, v0}, Lflipboard/json/JSONSerializer;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, p0}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/UserService;)V

    .line 9099
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static a(Lflipboard/objs/UserState;)[B
    .locals 2

    .prologue
    .line 9521
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x258

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 9522
    new-instance v1, Lflipboard/json/JSONSerializer;

    invoke-direct {v1, v0}, Lflipboard/json/JSONSerializer;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, p0}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/UserState;)V

    .line 9523
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static a(Lflipboard/service/Account$Meta;)[B
    .locals 2

    .prologue
    .line 9949
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x258

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 9950
    new-instance v1, Lflipboard/json/JSONSerializer;

    invoke-direct {v1, v0}, Lflipboard/json/JSONSerializer;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, p0}, Lflipboard/json/JSONSerializer;->b(Lflipboard/service/Account$Meta;)V

    .line 9951
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static a(Lflipboard/service/Section$Meta;)[B
    .locals 2

    .prologue
    .line 10039
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x258

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 10040
    new-instance v1, Lflipboard/json/JSONSerializer;

    invoke-direct {v1, v0}, Lflipboard/json/JSONSerializer;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, p0}, Lflipboard/json/JSONSerializer;->b(Lflipboard/service/Section$Meta;)V

    .line 10041
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;)[B
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/io/RequestLogEntry;",
            ">;)[B"
        }
    .end annotation

    .prologue
    .line 346
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit16 v1, v1, 0x258

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 347
    new-instance v1, Lflipboard/json/JSONSerializer;

    invoke-direct {v1, v0}, Lflipboard/json/JSONSerializer;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, p0}, Lflipboard/json/JSONSerializer;->e(Ljava/util/List;)V

    .line 348
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method private b(Lflipboard/objs/FeedItem;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    .line 4153
    if-nez p1, :cond_0

    .line 4154
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 4848
    :goto_0
    return-void

    .line 4157
    :cond_0
    const/16 v2, 0x7b

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 4158
    iget-object v2, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 4159
    const-string v2, "type"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4160
    iget-object v0, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 4162
    :cond_1
    iget-object v2, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 4163
    add-int/lit8 v2, v0, 0x1

    const-string v3, "id"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4164
    iget-object v0, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4166
    :cond_2
    iget-object v2, p1, Lflipboard/objs/FeedItem;->c:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 4167
    add-int/lit8 v2, v0, 0x1

    const-string v3, "groupId"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4168
    iget-object v0, p1, Lflipboard/objs/FeedItem;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4170
    :cond_3
    iget-object v2, p1, Lflipboard/objs/FeedItem;->d:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 4171
    add-int/lit8 v2, v0, 0x1

    const-string v3, "remoteid"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4172
    iget-object v0, p1, Lflipboard/objs/FeedItem;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4174
    :cond_4
    iget-object v2, p1, Lflipboard/objs/FeedItem;->e:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 4175
    add-int/lit8 v2, v0, 0x1

    const-string v3, "franchiseId"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4176
    iget-object v0, p1, Lflipboard/objs/FeedItem;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4178
    :cond_5
    iget-object v2, p1, Lflipboard/objs/FeedItem;->f:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 4179
    add-int/lit8 v2, v0, 0x1

    const-string v3, "nextPageKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4180
    iget-object v0, p1, Lflipboard/objs/FeedItem;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4182
    :cond_6
    iget-object v2, p1, Lflipboard/objs/FeedItem;->g:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 4183
    add-int/lit8 v2, v0, 0x1

    const-string v3, "pageKey"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4184
    iget-object v0, p1, Lflipboard/objs/FeedItem;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4186
    :cond_7
    iget-object v2, p1, Lflipboard/objs/FeedItem;->h:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 4187
    add-int/lit8 v2, v0, 0x1

    const-string v3, "sectionIDType"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4188
    iget-object v0, p1, Lflipboard/objs/FeedItem;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4190
    :cond_8
    iget-object v2, p1, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 4191
    add-int/lit8 v2, v0, 0x1

    const-string v3, "sectionID"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4192
    iget-object v0, p1, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4194
    :cond_9
    iget-object v2, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    if-eqz v2, :cond_a

    .line 4195
    add-int/lit8 v2, v0, 0x1

    const-string v3, "section"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4196
    iget-object v0, p1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FeedSection;)V

    move v0, v2

    .line 4198
    :cond_a
    iget-object v2, p1, Lflipboard/objs/FeedItem;->k:Lflipboard/objs/Invite;

    if-eqz v2, :cond_b

    .line 4199
    add-int/lit8 v2, v0, 0x1

    const-string v3, "invite"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4200
    iget-object v0, p1, Lflipboard/objs/FeedItem;->k:Lflipboard/objs/Invite;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Invite;)V

    move v0, v2

    .line 4202
    :cond_b
    iget-object v2, p1, Lflipboard/objs/FeedItem;->l:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 4203
    add-int/lit8 v2, v0, 0x1

    const-string v3, "version"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4204
    iget-object v0, p1, Lflipboard/objs/FeedItem;->l:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4206
    :cond_c
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->m:Z

    if-eqz v2, :cond_d

    .line 4207
    add-int/lit8 v2, v0, 0x1

    const-string v3, "private"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4208
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->m:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4210
    :cond_d
    iget-object v2, p1, Lflipboard/objs/FeedItem;->n:Ljava/lang/String;

    if-eqz v2, :cond_e

    .line 4211
    add-int/lit8 v2, v0, 0x1

    const-string v3, "imageURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4212
    iget-object v0, p1, Lflipboard/objs/FeedItem;->n:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4214
    :cond_e
    iget-object v2, p1, Lflipboard/objs/FeedItem;->o:Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 4215
    add-int/lit8 v2, v0, 0x1

    const-string v3, "alertMessage"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4216
    iget-object v0, p1, Lflipboard/objs/FeedItem;->o:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4218
    :cond_f
    iget-object v2, p1, Lflipboard/objs/FeedItem;->p:Ljava/lang/String;

    if-eqz v2, :cond_10

    .line 4219
    add-int/lit8 v2, v0, 0x1

    const-string v3, "alertLink"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4220
    iget-object v0, p1, Lflipboard/objs/FeedItem;->p:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4222
    :cond_10
    iget-object v2, p1, Lflipboard/objs/FeedItem;->q:Ljava/lang/String;

    if-eqz v2, :cond_11

    .line 4223
    add-int/lit8 v2, v0, 0x1

    const-string v3, "feedType"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4224
    iget-object v0, p1, Lflipboard/objs/FeedItem;->q:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4226
    :cond_11
    iget v2, p1, Lflipboard/objs/FeedItem;->r:I

    if-eqz v2, :cond_12

    .line 4227
    add-int/lit8 v2, v0, 0x1

    const-string v3, "time"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4228
    iget v0, p1, Lflipboard/objs/FeedItem;->r:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 4230
    :cond_12
    iget v2, p1, Lflipboard/objs/FeedItem;->s:I

    if-eqz v2, :cond_13

    .line 4231
    add-int/lit8 v2, v0, 0x1

    const-string v3, "count"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4232
    iget v0, p1, Lflipboard/objs/FeedItem;->s:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 4234
    :cond_13
    iget v2, p1, Lflipboard/objs/FeedItem;->t:I

    if-eqz v2, :cond_14

    .line 4235
    add-int/lit8 v2, v0, 0x1

    const-string v3, "hashCode"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4236
    iget v0, p1, Lflipboard/objs/FeedItem;->t:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 4238
    :cond_14
    iget-wide v2, p1, Lflipboard/objs/FeedItem;->u:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_15

    .line 4239
    add-int/lit8 v2, v0, 0x1

    const-string v3, "sortValue"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4240
    iget-wide v4, p1, Lflipboard/objs/FeedItem;->u:J

    invoke-virtual {p0, v4, v5}, Lflipboard/json/JSONSerializer;->a(J)V

    move v0, v2

    .line 4242
    :cond_15
    iget v2, p1, Lflipboard/objs/FeedItem;->v:I

    if-eqz v2, :cond_16

    .line 4243
    add-int/lit8 v2, v0, 0x1

    const-string v3, "original_width"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4244
    iget v0, p1, Lflipboard/objs/FeedItem;->v:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 4246
    :cond_16
    iget v2, p1, Lflipboard/objs/FeedItem;->w:I

    if-eqz v2, :cond_17

    .line 4247
    add-int/lit8 v2, v0, 0x1

    const-string v3, "original_height"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4248
    iget v0, p1, Lflipboard/objs/FeedItem;->w:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 4250
    :cond_17
    iget-object v2, p1, Lflipboard/objs/FeedItem;->x:Ljava/lang/String;

    if-eqz v2, :cond_18

    .line 4251
    add-int/lit8 v2, v0, 0x1

    const-string v3, "original_hints"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4252
    iget-object v0, p1, Lflipboard/objs/FeedItem;->x:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4254
    :cond_18
    iget-object v2, p1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v2, :cond_19

    .line 4255
    add-int/lit8 v2, v0, 0x1

    const-string v3, "title"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4256
    iget-object v0, p1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4258
    :cond_19
    iget-object v2, p1, Lflipboard/objs/FeedItem;->z:Ljava/lang/String;

    if-eqz v2, :cond_1a

    .line 4259
    add-int/lit8 v2, v0, 0x1

    const-string v3, "text"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4260
    iget-object v0, p1, Lflipboard/objs/FeedItem;->z:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4262
    :cond_1a
    iget-object v2, p1, Lflipboard/objs/FeedItem;->A:Ljava/lang/String;

    if-eqz v2, :cond_1b

    .line 4263
    add-int/lit8 v2, v0, 0x1

    const-string v3, "rssText"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4264
    iget-object v0, p1, Lflipboard/objs/FeedItem;->A:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4266
    :cond_1b
    iget-object v2, p1, Lflipboard/objs/FeedItem;->B:Ljava/lang/String;

    if-eqz v2, :cond_1c

    .line 4267
    add-int/lit8 v2, v0, 0x1

    const-string v3, "rssBaseURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4268
    iget-object v0, p1, Lflipboard/objs/FeedItem;->B:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4270
    :cond_1c
    iget-object v2, p1, Lflipboard/objs/FeedItem;->C:Ljava/lang/String;

    if-eqz v2, :cond_1d

    .line 4271
    add-int/lit8 v2, v0, 0x1

    const-string v3, "feedTitle"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4272
    iget-object v0, p1, Lflipboard/objs/FeedItem;->C:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4274
    :cond_1d
    iget-object v2, p1, Lflipboard/objs/FeedItem;->D:Ljava/lang/String;

    if-eqz v2, :cond_1e

    .line 4275
    add-int/lit8 v2, v0, 0x1

    const-string v3, "feedUrl"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4276
    iget-object v0, p1, Lflipboard/objs/FeedItem;->D:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4278
    :cond_1e
    iget v2, p1, Lflipboard/objs/FeedItem;->E:F

    float-to-double v2, v2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_1f

    .line 4279
    add-int/lit8 v2, v0, 0x1

    const-string v3, "prominence"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4280
    iget v0, p1, Lflipboard/objs/FeedItem;->E:F

    float-to-double v4, v0

    invoke-virtual {p0, v4, v5}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v2

    .line 4282
    :cond_1f
    iget-object v2, p1, Lflipboard/objs/FeedItem;->F:Ljava/lang/String;

    if-eqz v2, :cond_20

    .line 4283
    add-int/lit8 v2, v0, 0x1

    const-string v3, "excerptText"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4284
    iget-object v0, p1, Lflipboard/objs/FeedItem;->F:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4286
    :cond_20
    iget-object v2, p1, Lflipboard/objs/FeedItem;->G:Ljava/lang/String;

    if-eqz v2, :cond_21

    .line 4287
    add-int/lit8 v2, v0, 0x1

    const-string v3, "fullText"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4288
    iget-object v0, p1, Lflipboard/objs/FeedItem;->G:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4290
    :cond_21
    iget-object v2, p1, Lflipboard/objs/FeedItem;->H:Ljava/lang/String;

    if-eqz v2, :cond_22

    .line 4291
    add-int/lit8 v2, v0, 0x1

    const-string v3, "strippedExcerptText"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4292
    iget-object v0, p1, Lflipboard/objs/FeedItem;->H:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4294
    :cond_22
    iget-object v2, p1, Lflipboard/objs/FeedItem;->I:Lflipboard/objs/Image;

    if-eqz v2, :cond_23

    .line 4295
    add-int/lit8 v2, v0, 0x1

    const-string v3, "inlineImage"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4296
    iget-object v0, p1, Lflipboard/objs/FeedItem;->I:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v2

    .line 4298
    :cond_23
    iget-object v2, p1, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    if-eqz v2, :cond_24

    .line 4299
    add-int/lit8 v2, v0, 0x1

    const-string v3, "image"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4300
    iget-object v0, p1, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v2

    .line 4302
    :cond_24
    iget-object v2, p1, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    if-eqz v2, :cond_25

    .line 4303
    add-int/lit8 v2, v0, 0x1

    const-string v3, "albumArtImage"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4304
    iget-object v0, p1, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v2

    .line 4306
    :cond_25
    iget-object v2, p1, Lflipboard/objs/FeedItem;->L:Lflipboard/objs/Image;

    if-eqz v2, :cond_26

    .line 4307
    add-int/lit8 v2, v0, 0x1

    const-string v3, "tileImage"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4308
    iget-object v0, p1, Lflipboard/objs/FeedItem;->L:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v2

    .line 4310
    :cond_26
    iget-object v2, p1, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v2, :cond_27

    .line 4311
    add-int/lit8 v2, v0, 0x1

    const-string v3, "authorImage"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4312
    iget-object v0, p1, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v2

    .line 4314
    :cond_27
    iget-object v2, p1, Lflipboard/objs/FeedItem;->N:Lflipboard/objs/Image;

    if-eqz v2, :cond_28

    .line 4315
    add-int/lit8 v2, v0, 0x1

    const-string v3, "coverImage"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4316
    iget-object v0, p1, Lflipboard/objs/FeedItem;->N:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v2

    .line 4318
    :cond_28
    iget-object v2, p1, Lflipboard/objs/FeedItem;->O:Lflipboard/objs/Image;

    if-eqz v2, :cond_29

    .line 4319
    add-int/lit8 v2, v0, 0x1

    const-string v3, "profileBackgroundImage"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4320
    iget-object v0, p1, Lflipboard/objs/FeedItem;->O:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v2

    .line 4322
    :cond_29
    iget-object v2, p1, Lflipboard/objs/FeedItem;->P:Ljava/lang/String;

    if-eqz v2, :cond_2a

    .line 4323
    add-int/lit8 v2, v0, 0x1

    const-string v3, "url"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4324
    iget-object v0, p1, Lflipboard/objs/FeedItem;->P:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4326
    :cond_2a
    iget-object v2, p1, Lflipboard/objs/FeedItem;->Q:Ljava/lang/String;

    if-eqz v2, :cond_2b

    .line 4327
    add-int/lit8 v2, v0, 0x1

    const-string v3, "inlineHTML"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4328
    iget-object v0, p1, Lflipboard/objs/FeedItem;->Q:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4330
    :cond_2b
    iget-object v2, p1, Lflipboard/objs/FeedItem;->R:Ljava/lang/String;

    if-eqz v2, :cond_2c

    .line 4331
    add-int/lit8 v2, v0, 0x1

    const-string v3, "baseURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4332
    iget-object v0, p1, Lflipboard/objs/FeedItem;->R:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4334
    :cond_2c
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->S:Z

    if-eqz v2, :cond_2d

    .line 4335
    add-int/lit8 v2, v0, 0x1

    const-string v3, "wantsTranslucentBackground"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4336
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->S:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4338
    :cond_2d
    iget-object v2, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    if-eqz v2, :cond_2e

    .line 4339
    add-int/lit8 v2, v0, 0x1

    const-string v3, "service"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4340
    iget-object v0, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4342
    :cond_2e
    iget-object v2, p1, Lflipboard/objs/FeedItem;->U:Ljava/lang/String;

    if-eqz v2, :cond_2f

    .line 4343
    add-int/lit8 v2, v0, 0x1

    const-string v3, "remoteServiceItemID"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4344
    iget-object v0, p1, Lflipboard/objs/FeedItem;->U:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4346
    :cond_2f
    iget-object v2, p1, Lflipboard/objs/FeedItem;->V:Ljava/lang/String;

    if-eqz v2, :cond_30

    .line 4347
    add-int/lit8 v2, v0, 0x1

    const-string v3, "readButtonCaption"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4348
    iget-object v0, p1, Lflipboard/objs/FeedItem;->V:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4350
    :cond_30
    iget-object v2, p1, Lflipboard/objs/FeedItem;->W:Ljava/lang/String;

    if-eqz v2, :cond_31

    .line 4351
    add-int/lit8 v2, v0, 0x1

    const-string v3, "sourceMagazineURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4352
    iget-object v0, p1, Lflipboard/objs/FeedItem;->W:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4354
    :cond_31
    iget-object v2, p1, Lflipboard/objs/FeedItem;->X:Lflipboard/objs/Image;

    if-eqz v2, :cond_32

    .line 4355
    add-int/lit8 v2, v0, 0x1

    const-string v3, "posterImage"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4356
    iget-object v0, p1, Lflipboard/objs/FeedItem;->X:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v2

    .line 4358
    :cond_32
    iget-object v2, p1, Lflipboard/objs/FeedItem;->Y:Ljava/lang/String;

    if-eqz v2, :cond_33

    .line 4359
    add-int/lit8 v2, v0, 0x1

    const-string v3, "language"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4360
    iget-object v0, p1, Lflipboard/objs/FeedItem;->Y:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4362
    :cond_33
    iget-wide v2, p1, Lflipboard/objs/FeedItem;->Z:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_34

    .line 4363
    add-int/lit8 v2, v0, 0x1

    const-string v3, "dateCreated"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4364
    iget-wide v4, p1, Lflipboard/objs/FeedItem;->Z:J

    invoke-virtual {p0, v4, v5}, Lflipboard/json/JSONSerializer;->a(J)V

    move v0, v2

    .line 4366
    :cond_34
    iget-wide v2, p1, Lflipboard/objs/FeedItem;->aa:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_35

    .line 4367
    add-int/lit8 v2, v0, 0x1

    const-string v3, "ingestionTime"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4368
    iget-wide v4, p1, Lflipboard/objs/FeedItem;->aa:J

    invoke-virtual {p0, v4, v5}, Lflipboard/json/JSONSerializer;->a(J)V

    move v0, v2

    .line 4370
    :cond_35
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->ab:Z

    if-eqz v2, :cond_36

    .line 4371
    add-int/lit8 v2, v0, 0x1

    const-string v3, "topStory"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4372
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->ab:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4374
    :cond_36
    iget-object v2, p1, Lflipboard/objs/FeedItem;->ac:Ljava/lang/String;

    if-eqz v2, :cond_37

    .line 4375
    add-int/lit8 v2, v0, 0x1

    const-string v3, "topStoryText"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4376
    iget-object v0, p1, Lflipboard/objs/FeedItem;->ac:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4378
    :cond_37
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->ad:Z

    if-eq v2, v1, :cond_38

    .line 4379
    add-int/lit8 v2, v0, 0x1

    const-string v3, "canLike"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4380
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->ad:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4382
    :cond_38
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->ae:Z

    if-eq v2, v1, :cond_39

    .line 4383
    add-int/lit8 v2, v0, 0x1

    const-string v3, "canUnlike"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4384
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->ae:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4386
    :cond_39
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->af:Z

    if-eqz v2, :cond_3a

    .line 4387
    add-int/lit8 v2, v0, 0x1

    const-string v3, "isLiked"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4388
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->af:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4390
    :cond_3a
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->ag:Z

    if-eq v2, v1, :cond_3b

    .line 4391
    add-int/lit8 v2, v0, 0x1

    const-string v3, "canReply"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4392
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->ag:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4394
    :cond_3b
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->ah:Z

    if-eq v2, v1, :cond_3c

    .line 4395
    add-int/lit8 v2, v0, 0x1

    const-string v3, "canShare"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4396
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->ah:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4398
    :cond_3c
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->ai:Z

    if-eq v2, v1, :cond_3d

    .line 4399
    add-int/lit8 v2, v0, 0x1

    const-string v3, "canCompose"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4400
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->ai:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4402
    :cond_3d
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->aj:Z

    if-eq v2, v1, :cond_3e

    .line 4403
    add-int/lit8 v2, v0, 0x1

    const-string v3, "canShareLink"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4404
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->aj:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4406
    :cond_3e
    iget v2, p1, Lflipboard/objs/FeedItem;->ak:I

    if-eqz v2, :cond_3f

    .line 4407
    add-int/lit8 v2, v0, 0x1

    const-string v3, "nsfw"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4408
    iget v0, p1, Lflipboard/objs/FeedItem;->ak:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 4410
    :cond_3f
    iget-object v2, p1, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    if-eqz v2, :cond_40

    .line 4411
    add-int/lit8 v2, v0, 0x1

    const-string v3, "sourceURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4412
    iget-object v0, p1, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4414
    :cond_40
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->am:Z

    if-eqz v2, :cond_41

    .line 4415
    add-int/lit8 v2, v0, 0x1

    const-string v3, "allowFullscreenImage"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4416
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->am:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4418
    :cond_41
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->an:Z

    if-eqz v2, :cond_42

    .line 4419
    add-int/lit8 v2, v0, 0x1

    const-string v3, "hideOnCover"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4420
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->an:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4422
    :cond_42
    iget-object v2, p1, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    if-eqz v2, :cond_43

    .line 4423
    add-int/lit8 v2, v0, 0x1

    const-string v3, "userid"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4424
    iget-object v0, p1, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4426
    :cond_43
    iget-object v2, p1, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    if-eqz v2, :cond_44

    .line 4427
    add-int/lit8 v2, v0, 0x1

    const-string v3, "authorUsername"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4428
    iget-object v0, p1, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4430
    :cond_44
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    if-eqz v2, :cond_45

    .line 4431
    add-int/lit8 v2, v0, 0x1

    const-string v3, "authorDisplayName"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4432
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4434
    :cond_45
    iget-object v2, p1, Lflipboard/objs/FeedItem;->ar:Ljava/lang/String;

    if-eqz v2, :cond_46

    .line 4435
    add-int/lit8 v2, v0, 0x1

    const-string v3, "authorURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4436
    iget-object v0, p1, Lflipboard/objs/FeedItem;->ar:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4438
    :cond_46
    iget-object v2, p1, Lflipboard/objs/FeedItem;->as:Ljava/lang/String;

    if-eqz v2, :cond_47

    .line 4439
    add-int/lit8 v2, v0, 0x1

    const-string v3, "authorDescription"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4440
    iget-object v0, p1, Lflipboard/objs/FeedItem;->as:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4442
    :cond_47
    iget-object v2, p1, Lflipboard/objs/FeedItem;->at:Ljava/lang/String;

    if-eqz v2, :cond_48

    .line 4443
    add-int/lit8 v2, v0, 0x1

    const-string v3, "authorLocation"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4444
    iget-object v0, p1, Lflipboard/objs/FeedItem;->at:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4446
    :cond_48
    iget-object v2, p1, Lflipboard/objs/FeedItem;->au:Ljava/lang/String;

    if-eqz v2, :cond_49

    .line 4447
    add-int/lit8 v2, v0, 0x1

    const-string v3, "publisher"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4448
    iget-object v0, p1, Lflipboard/objs/FeedItem;->au:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4450
    :cond_49
    iget-object v2, p1, Lflipboard/objs/FeedItem;->av:Ljava/lang/String;

    if-eqz v2, :cond_4a

    .line 4451
    add-int/lit8 v2, v0, 0x1

    const-string v3, "hostDisplayName"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4452
    iget-object v0, p1, Lflipboard/objs/FeedItem;->av:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4454
    :cond_4a
    iget v2, p1, Lflipboard/objs/FeedItem;->aw:I

    if-eqz v2, :cond_4b

    .line 4455
    add-int/lit8 v2, v0, 0x1

    const-string v3, "totalCount"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4456
    iget v0, p1, Lflipboard/objs/FeedItem;->aw:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 4458
    :cond_4b
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->ax:Z

    if-eqz v2, :cond_4c

    .line 4459
    add-int/lit8 v2, v0, 0x1

    const-string v3, "EOS"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4460
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->ax:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4462
    :cond_4c
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->ay:Z

    if-eqz v2, :cond_4d

    .line 4463
    add-int/lit8 v2, v0, 0x1

    const-string v3, "eof"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4464
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->ay:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4466
    :cond_4d
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->az:Z

    if-eqz v2, :cond_4e

    .line 4467
    add-int/lit8 v2, v0, 0x1

    const-string v3, "neverLoadMore"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4468
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->az:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4470
    :cond_4e
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->aA:Z

    if-eqz v2, :cond_4f

    .line 4471
    add-int/lit8 v2, v0, 0x1

    const-string v3, "success"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4472
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->aA:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4474
    :cond_4f
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aB:Ljava/util/List;

    if-eqz v2, :cond_50

    .line 4475
    add-int/lit8 v2, v0, 0x1

    const-string v3, "urls"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4476
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aB:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    move v0, v2

    .line 4478
    :cond_50
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-eqz v2, :cond_51

    .line 4479
    add-int/lit8 v2, v0, 0x1

    const-string v3, "referredByItems"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4480
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->c(Ljava/util/List;)V

    move v0, v2

    .line 4482
    :cond_51
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    if-eqz v2, :cond_52

    .line 4483
    add-int/lit8 v2, v0, 0x1

    const-string v3, "inlineItems"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4484
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->c(Ljava/util/List;)V

    move v0, v2

    .line 4486
    :cond_52
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v2, :cond_53

    .line 4487
    add-int/lit8 v2, v0, 0x1

    const-string v3, "items"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4488
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->c(Ljava/util/List;)V

    move v0, v2

    .line 4490
    :cond_53
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aF:Lflipboard/objs/FeedArticle;

    if-eqz v2, :cond_54

    .line 4491
    add-int/lit8 v2, v0, 0x1

    const-string v3, "article"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4492
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aF:Lflipboard/objs/FeedArticle;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FeedArticle;)V

    move v0, v2

    .line 4494
    :cond_54
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aG:Ljava/lang/String;

    if-eqz v2, :cond_55

    .line 4495
    add-int/lit8 v2, v0, 0x1

    const-string v3, "articleMarkup"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4496
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aG:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4498
    :cond_55
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aH:Ljava/lang/String;

    if-eqz v2, :cond_56

    .line 4499
    add-int/lit8 v2, v0, 0x1

    const-string v3, "articleMarkupEscaped"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4500
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aH:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4502
    :cond_56
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v2, :cond_57

    .line 4503
    add-int/lit8 v2, v0, 0x1

    const-string v3, "sectionLinks"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4504
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->i(Ljava/util/List;)V

    move v0, v2

    .line 4506
    :cond_57
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aJ:Lflipboard/objs/FeedItem;

    if-eqz v2, :cond_58

    .line 4507
    add-int/lit8 v2, v0, 0x1

    const-string v3, "original"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4508
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aJ:Lflipboard/objs/FeedItem;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/FeedItem;)V

    move v0, v2

    .line 4510
    :cond_58
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aK:Ljava/util/List;

    if-eqz v2, :cond_59

    .line 4511
    add-int/lit8 v2, v0, 0x1

    const-string v3, "categories"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4512
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aK:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    move v0, v2

    .line 4514
    :cond_59
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->aL:Z

    if-eqz v2, :cond_5a

    .line 4515
    add-int/lit8 v2, v0, 0x1

    const-string v3, "displayNative"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4516
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->aL:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4518
    :cond_5a
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->aM:Z

    if-eq v2, v1, :cond_5b

    .line 4519
    add-int/lit8 v2, v0, 0x1

    const-string v3, "canUnread"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4520
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->aM:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4522
    :cond_5b
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aN:Ljava/lang/String;

    if-eqz v2, :cond_5c

    .line 4523
    add-int/lit8 v2, v0, 0x1

    const-string v3, "algorithm"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4524
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aN:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4526
    :cond_5c
    iget v2, p1, Lflipboard/objs/FeedItem;->aO:I

    if-eqz v2, :cond_5d

    .line 4527
    add-int/lit8 v2, v0, 0x1

    const-string v3, "score"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4528
    iget v0, p1, Lflipboard/objs/FeedItem;->aO:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 4530
    :cond_5d
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aP:Ljava/util/List;

    if-eqz v2, :cond_5e

    .line 4531
    add-int/lit8 v2, v0, 0x1

    const-string v3, "crossPosts"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4532
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aP:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->c(Ljava/util/List;)V

    move v0, v2

    .line 4534
    :cond_5e
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->aQ:Z

    if-eqz v2, :cond_5f

    .line 4535
    add-int/lit8 v2, v0, 0x1

    const-string v3, "canRead"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4536
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->aQ:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4538
    :cond_5f
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aR:Ljava/lang/String;

    if-eqz v2, :cond_60

    .line 4539
    add-int/lit8 v2, v0, 0x1

    const-string v3, "rssCustomClasses"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4540
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aR:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4542
    :cond_60
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aS:Lflipboard/objs/FeedItem$Location;

    if-eqz v2, :cond_61

    .line 4543
    add-int/lit8 v2, v0, 0x1

    const-string v3, "location"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4544
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aS:Lflipboard/objs/FeedItem$Location;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FeedItem$Location;)V

    move v0, v2

    .line 4546
    :cond_61
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aT:Ljava/lang/String;

    if-eqz v2, :cond_62

    .line 4547
    add-int/lit8 v2, v0, 0x1

    const-string v3, "noItemsText"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4548
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aT:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4550
    :cond_62
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aU:Lflipboard/json/FLObject;

    if-eqz v2, :cond_63

    .line 4551
    add-int/lit8 v2, v0, 0x1

    const-string v3, "magnetData"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4552
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aU:Lflipboard/json/FLObject;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/json/FLObject;)V

    move v0, v2

    .line 4554
    :cond_63
    iget v2, p1, Lflipboard/objs/FeedItem;->aV:I

    if-eqz v2, :cond_64

    .line 4555
    add-int/lit8 v2, v0, 0x1

    const-string v3, "portrait_width"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4556
    iget v0, p1, Lflipboard/objs/FeedItem;->aV:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 4558
    :cond_64
    iget v2, p1, Lflipboard/objs/FeedItem;->aW:I

    if-eqz v2, :cond_65

    .line 4559
    add-int/lit8 v2, v0, 0x1

    const-string v3, "portrait_height"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4560
    iget v0, p1, Lflipboard/objs/FeedItem;->aW:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 4562
    :cond_65
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aX:Ljava/lang/String;

    if-eqz v2, :cond_66

    .line 4563
    add-int/lit8 v2, v0, 0x1

    const-string v3, "videoSiteURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4564
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aX:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4566
    :cond_66
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aY:Ljava/lang/String;

    if-eqz v2, :cond_67

    .line 4567
    add-int/lit8 v2, v0, 0x1

    const-string v3, "videoEmbedHTML"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4568
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aY:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4570
    :cond_67
    iget-object v2, p1, Lflipboard/objs/FeedItem;->aZ:Ljava/lang/String;

    if-eqz v2, :cond_68

    .line 4571
    add-int/lit8 v2, v0, 0x1

    const-string v3, "inReplyToID"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4572
    iget-object v0, p1, Lflipboard/objs/FeedItem;->aZ:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4574
    :cond_68
    iget-object v2, p1, Lflipboard/objs/FeedItem;->ba:Ljava/lang/String;

    if-eqz v2, :cond_69

    .line 4575
    add-int/lit8 v2, v0, 0x1

    const-string v3, "license"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4576
    iget-object v0, p1, Lflipboard/objs/FeedItem;->ba:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4578
    :cond_69
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->bb:Z

    if-eqz v2, :cond_6a

    .line 4579
    add-int/lit8 v2, v0, 0x1

    const-string v3, "isTextHTML"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4580
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->bb:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4582
    :cond_6a
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bc:Ljava/lang/String;

    if-eqz v2, :cond_6b

    .line 4583
    add-int/lit8 v2, v0, 0x1

    const-string v3, "plainText"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4584
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bc:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4586
    :cond_6b
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bd:Ljava/lang/String;

    if-eqz v2, :cond_6c

    .line 4587
    add-int/lit8 v2, v0, 0x1

    const-string v3, "viaService"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4588
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bd:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4590
    :cond_6c
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->be:Z

    if-eqz v2, :cond_6d

    .line 4591
    add-int/lit8 v2, v0, 0x1

    const-string v3, "isRead"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4592
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->be:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4594
    :cond_6d
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bf:Lflipboard/objs/FeedItem$Note;

    if-eqz v2, :cond_6e

    .line 4595
    add-int/lit8 v2, v0, 0x1

    const-string v3, "reason"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4596
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bf:Lflipboard/objs/FeedItem$Note;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FeedItem$Note;)V

    move v0, v2

    .line 4598
    :cond_6e
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bg:Ljava/lang/String;

    if-eqz v2, :cond_6f

    .line 4599
    add-int/lit8 v2, v0, 0x1

    const-string v3, "action"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4600
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bg:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4602
    :cond_6f
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bh:Ljava/lang/String;

    if-eqz v2, :cond_70

    .line 4603
    add-int/lit8 v2, v0, 0x1

    const-string v3, "errormessage"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4604
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bh:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4606
    :cond_70
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bi:Lflipboard/objs/UserServices;

    if-eqz v2, :cond_71

    .line 4607
    add-int/lit8 v2, v0, 0x1

    const-string v3, "user"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4608
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bi:Lflipboard/objs/UserServices;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UserServices;)V

    move v0, v2

    .line 4610
    :cond_71
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bj:Ljava/lang/String;

    if-eqz v2, :cond_72

    .line 4611
    add-int/lit8 v2, v0, 0x1

    const-string v3, "h264URL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4612
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bj:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4614
    :cond_72
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->bk:Z

    if-eqz v2, :cond_73

    .line 4615
    add-int/lit8 v2, v0, 0x1

    const-string v3, "hideTimelineDate"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4616
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->bk:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4618
    :cond_73
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->bl:Z

    if-eqz v2, :cond_74

    .line 4619
    add-int/lit8 v2, v0, 0x1

    const-string v3, "disableFullBleed"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4620
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->bl:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4622
    :cond_74
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->bm:Z

    if-eqz v2, :cond_75

    .line 4623
    add-int/lit8 v2, v0, 0x1

    const-string v3, "isStarred"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4624
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->bm:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4626
    :cond_75
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->bn:Z

    if-eqz v2, :cond_76

    .line 4627
    add-int/lit8 v2, v0, 0x1

    const-string v3, "canFetchCommentary"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4628
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->bn:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4630
    :cond_76
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bo:Ljava/lang/String;

    if-eqz v2, :cond_77

    .line 4631
    add-int/lit8 v2, v0, 0x1

    const-string v3, "visibilityDisplayName"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4632
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bo:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4634
    :cond_77
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    if-eqz v2, :cond_78

    .line 4635
    add-int/lit8 v2, v0, 0x1

    const-string v3, "contentService"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4636
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4638
    :cond_78
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bq:Ljava/lang/String;

    if-eqz v2, :cond_79

    .line 4639
    add-int/lit8 v2, v0, 0x1

    const-string v3, "imageAttribution"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4640
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bq:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4642
    :cond_79
    iget-object v2, p1, Lflipboard/objs/FeedItem;->br:Ljava/lang/String;

    if-eqz v2, :cond_7a

    .line 4643
    add-int/lit8 v2, v0, 0x1

    const-string v3, "itemPrice"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4644
    iget-object v0, p1, Lflipboard/objs/FeedItem;->br:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4646
    :cond_7a
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bs:Ljava/lang/String;

    if-eqz v2, :cond_7b

    .line 4647
    add-int/lit8 v2, v0, 0x1

    const-string v3, "ecommerceCheckoutURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4648
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bs:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4650
    :cond_7b
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bt:Ljava/lang/String;

    if-eqz v2, :cond_7c

    .line 4651
    add-int/lit8 v2, v0, 0x1

    const-string v3, "album"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4652
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bt:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4654
    :cond_7c
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bu:Ljava/lang/String;

    if-eqz v2, :cond_7d

    .line 4655
    add-int/lit8 v2, v0, 0x1

    const-string v3, "artist"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4656
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bu:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4658
    :cond_7d
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bv:Ljava/lang/String;

    if-eqz v2, :cond_7e

    .line 4659
    add-int/lit8 v2, v0, 0x1

    const-string v3, "audioURL"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4660
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bv:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4662
    :cond_7e
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bw:Ljava/lang/String;

    if-eqz v2, :cond_7f

    .line 4663
    add-int/lit8 v2, v0, 0x1

    const-string v3, "audioType"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4664
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bw:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4666
    :cond_7f
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bx:Ljava/lang/String;

    if-eqz v2, :cond_80

    .line 4667
    add-int/lit8 v2, v0, 0x1

    const-string v3, "postedBy"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4668
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bx:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4670
    :cond_80
    iget-object v2, p1, Lflipboard/objs/FeedItem;->by:Ljava/lang/String;

    if-eqz v2, :cond_81

    .line 4671
    add-int/lit8 v2, v0, 0x1

    const-string v3, "description"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4672
    iget-object v0, p1, Lflipboard/objs/FeedItem;->by:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4674
    :cond_81
    iget v2, p1, Lflipboard/objs/FeedItem;->bz:F

    float-to-double v2, v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_82

    .line 4675
    add-int/lit8 v2, v0, 0x1

    const-string v3, "duration"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4676
    iget v0, p1, Lflipboard/objs/FeedItem;->bz:F

    float-to-double v4, v0

    invoke-virtual {p0, v4, v5}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v2

    .line 4678
    :cond_82
    iget-wide v2, p1, Lflipboard/objs/FeedItem;->bA:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_83

    .line 4679
    add-int/lit8 v2, v0, 0x1

    const-string v3, "releaseDate"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4680
    iget-wide v4, p1, Lflipboard/objs/FeedItem;->bA:J

    invoke-virtual {p0, v4, v5}, Lflipboard/json/JSONSerializer;->a(J)V

    move v0, v2

    .line 4682
    :cond_83
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bB:Ljava/lang/String;

    if-eqz v2, :cond_84

    .line 4683
    add-int/lit8 v2, v0, 0x1

    const-string v3, "releaseDateString"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4684
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bB:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4686
    :cond_84
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->bC:Z

    if-eq v2, v1, :cond_85

    .line 4687
    add-int/lit8 v2, v0, 0x1

    const-string v3, "canAutoplay"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4688
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->bC:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4690
    :cond_85
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bD:Ljava/lang/String;

    if-eqz v2, :cond_86

    .line 4691
    add-int/lit8 v2, v0, 0x1

    const-string v3, "strategy"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4692
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bD:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4694
    :cond_86
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bE:Ljava/lang/String;

    if-eqz v2, :cond_87

    .line 4695
    add-int/lit8 v2, v0, 0x1

    const-string v3, "partnerID"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4696
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bE:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4698
    :cond_87
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bF:Lflipboard/objs/FeedItem;

    if-eqz v2, :cond_88

    .line 4699
    add-int/lit8 v2, v0, 0x1

    const-string v3, "mainItem"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4700
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bF:Lflipboard/objs/FeedItem;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/FeedItem;)V

    move v0, v2

    .line 4702
    :cond_88
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->bG:Z

    if-eqz v2, :cond_89

    .line 4703
    add-int/lit8 v2, v0, 0x1

    const-string v3, "wantsFullPage"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4704
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->bG:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4706
    :cond_89
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bH:Lflipboard/objs/FeedItem;

    if-eqz v2, :cond_8a

    .line 4707
    add-int/lit8 v2, v0, 0x1

    const-string v3, "via"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4708
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bH:Lflipboard/objs/FeedItem;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/FeedItem;)V

    move v0, v2

    .line 4710
    :cond_8a
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bI:Ljava/lang/String;

    if-eqz v2, :cond_8b

    .line 4711
    add-int/lit8 v2, v0, 0x1

    const-string v3, "sectionTitle"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4712
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bI:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4714
    :cond_8b
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bJ:Ljava/lang/String;

    if-eqz v2, :cond_8c

    .line 4715
    add-int/lit8 v2, v0, 0x1

    const-string v3, "textColor"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4716
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bJ:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4718
    :cond_8c
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    if-eqz v2, :cond_8d

    .line 4719
    add-int/lit8 v2, v0, 0x1

    const-string v3, "sidebarType"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4720
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4722
    :cond_8d
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    if-eqz v2, :cond_8e

    .line 4723
    add-int/lit8 v2, v0, 0x1

    const-string v3, "groups"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4724
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->m(Ljava/util/List;)V

    move v0, v2

    .line 4726
    :cond_8e
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bM:Lflipboard/objs/FeedSectionLink;

    if-eqz v2, :cond_8f

    .line 4727
    add-int/lit8 v2, v0, 0x1

    const-string v3, "profileSectionLink"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4728
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bM:Lflipboard/objs/FeedSectionLink;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FeedSectionLink;)V

    move v0, v2

    .line 4730
    :cond_8f
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->bN:Z

    if-eqz v2, :cond_90

    .line 4731
    add-int/lit8 v2, v0, 0x1

    const-string v3, "sponsored"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4732
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->bN:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4734
    :cond_90
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bO:Ljava/lang/String;

    if-eqz v2, :cond_91

    .line 4735
    add-int/lit8 v2, v0, 0x1

    const-string v3, "clickValue"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4736
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bO:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4738
    :cond_91
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bP:Ljava/lang/String;

    if-eqz v2, :cond_92

    .line 4739
    add-int/lit8 v2, v0, 0x1

    const-string v3, "impressionValue"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4740
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bP:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4742
    :cond_92
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bQ:Ljava/lang/String;

    if-eqz v2, :cond_93

    .line 4743
    add-int/lit8 v2, v0, 0x1

    const-string v3, "actionTitle"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4744
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bQ:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4746
    :cond_93
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bR:Ljava/lang/String;

    if-eqz v2, :cond_94

    .line 4747
    add-int/lit8 v2, v0, 0x1

    const-string v3, "subtitle"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4748
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bR:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4750
    :cond_94
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bS:Ljava/util/List;

    if-eqz v2, :cond_95

    .line 4751
    add-int/lit8 v2, v0, 0x1

    const-string v3, "impressionTrackingUrls"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4752
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bS:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    move v0, v2

    .line 4754
    :cond_95
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bT:Ljava/util/List;

    if-eqz v2, :cond_96

    .line 4755
    add-int/lit8 v2, v0, 0x1

    const-string v3, "clickTrackingUrls"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4756
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bT:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    move v0, v2

    .line 4758
    :cond_96
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bU:Ljava/lang/String;

    if-eqz v2, :cond_97

    .line 4759
    add-int/lit8 v2, v0, 0x1

    const-string v3, "sponsoredCampaign"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4760
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bU:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4762
    :cond_97
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->bW:Z

    if-eqz v2, :cond_98

    .line 4763
    add-int/lit8 v2, v0, 0x1

    const-string v3, "disabled"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4764
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->bW:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4766
    :cond_98
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bX:Ljava/lang/String;

    if-eqz v2, :cond_99

    .line 4767
    add-int/lit8 v2, v0, 0x1

    const-string v3, "videoID"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4768
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bX:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4770
    :cond_99
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bY:Ljava/lang/String;

    if-eqz v2, :cond_9a

    .line 4771
    add-int/lit8 v2, v0, 0x1

    const-string v3, "videoService"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4772
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bY:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4774
    :cond_9a
    iget-object v2, p1, Lflipboard/objs/FeedItem;->bZ:Ljava/lang/String;

    if-eqz v2, :cond_9b

    .line 4775
    add-int/lit8 v2, v0, 0x1

    const-string v3, "unreadRemoteID"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4776
    iget-object v0, p1, Lflipboard/objs/FeedItem;->bZ:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4778
    :cond_9b
    iget v2, p1, Lflipboard/objs/FeedItem;->ca:I

    if-eqz v2, :cond_9c

    .line 4779
    add-int/lit8 v2, v0, 0x1

    const-string v3, "unreadCount"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4780
    iget v0, p1, Lflipboard/objs/FeedItem;->ca:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v2

    .line 4782
    :cond_9c
    iget-object v2, p1, Lflipboard/objs/FeedItem;->cb:Ljava/lang/String;

    if-eqz v2, :cond_9d

    .line 4783
    add-int/lit8 v2, v0, 0x1

    const-string v3, "categoryid"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4784
    iget-object v0, p1, Lflipboard/objs/FeedItem;->cb:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4786
    :cond_9d
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->cc:Z

    if-eqz v2, :cond_9e

    .line 4787
    add-int/lit8 v2, v0, 0x1

    const-string v3, "imageURLHiddenInList"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4788
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->cc:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4790
    :cond_9e
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->cd:Z

    if-eqz v2, :cond_9f

    .line 4791
    add-int/lit8 v2, v0, 0x1

    const-string v3, "preselected"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4792
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->cd:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4794
    :cond_9f
    iget-object v2, p1, Lflipboard/objs/FeedItem;->ce:Ljava/lang/String;

    if-eqz v2, :cond_a0

    .line 4795
    add-int/lit8 v2, v0, 0x1

    const-string v3, "notificationType"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4796
    iget-object v0, p1, Lflipboard/objs/FeedItem;->ce:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4798
    :cond_a0
    iget-object v2, p1, Lflipboard/objs/FeedItem;->cf:Ljava/lang/String;

    if-eqz v2, :cond_a1

    .line 4799
    add-int/lit8 v2, v0, 0x1

    const-string v3, "contentTitle"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4800
    iget-object v0, p1, Lflipboard/objs/FeedItem;->cf:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4802
    :cond_a1
    iget-object v2, p1, Lflipboard/objs/FeedItem;->cg:Ljava/lang/String;

    if-eqz v2, :cond_a2

    .line 4803
    add-int/lit8 v2, v0, 0x1

    const-string v3, "magazineTarget"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4804
    iget-object v0, p1, Lflipboard/objs/FeedItem;->cg:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4806
    :cond_a2
    iget-object v2, p1, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    if-eqz v2, :cond_a3

    .line 4807
    add-int/lit8 v2, v0, 0x1

    const-string v3, "originalFlip"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4808
    iget-object v0, p1, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/FeedItem;)V

    move v0, v2

    .line 4810
    :cond_a3
    iget-object v2, p1, Lflipboard/objs/FeedItem;->ci:Ljava/lang/String;

    if-eqz v2, :cond_a4

    .line 4811
    add-int/lit8 v2, v0, 0x1

    const-string v3, "flipboardSocialId"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4812
    iget-object v0, p1, Lflipboard/objs/FeedItem;->ci:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v2

    .line 4814
    :cond_a4
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->cj:Z

    if-eqz v2, :cond_a5

    .line 4815
    add-int/lit8 v2, v0, 0x1

    const-string v3, "hideImageUrl"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4816
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->cj:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4818
    :cond_a5
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->ck:Z

    if-eqz v2, :cond_a6

    .line 4819
    add-int/lit8 v2, v0, 0x1

    const-string v3, "hideContributors"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4820
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->ck:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v2

    .line 4822
    :cond_a6
    iget-boolean v2, p1, Lflipboard/objs/FeedItem;->cl:Z

    if-eq v2, v1, :cond_a7

    .line 4823
    add-int/lit8 v1, v0, 0x1

    const-string v2, "leadWithFullPageItem"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4824
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->cl:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 4826
    :cond_a7
    iget-object v1, p1, Lflipboard/objs/FeedItem;->cm:Ljava/lang/String;

    if-eqz v1, :cond_a8

    .line 4827
    add-int/lit8 v1, v0, 0x1

    const-string v2, "socialId"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4828
    iget-object v0, p1, Lflipboard/objs/FeedItem;->cm:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 4830
    :cond_a8
    iget-object v1, p1, Lflipboard/objs/FeedItem;->cn:Lflipboard/json/FLObject;

    if-eqz v1, :cond_a9

    .line 4831
    add-int/lit8 v1, v0, 0x1

    const-string v2, "additionalUsage"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4832
    iget-object v0, p1, Lflipboard/objs/FeedItem;->cn:Lflipboard/json/FLObject;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/json/FLObject;)V

    move v0, v1

    .line 4834
    :cond_a9
    iget-object v1, p1, Lflipboard/objs/FeedItem;->cp:Lflipboard/objs/FeedItemRenderHints;

    if-eqz v1, :cond_aa

    .line 4835
    add-int/lit8 v1, v0, 0x1

    const-string v2, "groupRenderHints"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4836
    iget-object v0, p1, Lflipboard/objs/FeedItem;->cp:Lflipboard/objs/FeedItemRenderHints;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FeedItemRenderHints;)V

    move v0, v1

    .line 4838
    :cond_aa
    iget-object v1, p1, Lflipboard/objs/FeedItem;->cq:Ljava/lang/String;

    if-eqz v1, :cond_ab

    .line 4839
    add-int/lit8 v1, v0, 0x1

    const-string v2, "subhead"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4840
    iget-object v0, p1, Lflipboard/objs/FeedItem;->cq:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 4842
    :cond_ab
    iget-boolean v1, p1, Lflipboard/objs/FeedItem;->cr:Z

    if-eqz v1, :cond_ac

    .line 4843
    const-string v1, "meteringEnabled"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 4844
    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->cr:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    .line 4846
    :cond_ac
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0
.end method

.method private b(Lflipboard/objs/Link;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 6563
    if-nez p1, :cond_0

    .line 6564
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 6582
    :goto_0
    return-void

    .line 6567
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 6568
    iget-object v0, p1, Lflipboard/objs/Link;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 6569
    const/4 v0, 0x1

    const-string v2, "type"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6570
    iget-object v1, p1, Lflipboard/objs/Link;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 6572
    :goto_1
    iget-object v1, p1, Lflipboard/objs/Link;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 6573
    add-int/lit8 v1, v0, 0x1

    const-string v2, "id"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6574
    iget-object v0, p1, Lflipboard/objs/Link;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6576
    :cond_1
    iget-object v1, p1, Lflipboard/objs/Link;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 6577
    const-string v1, "hints"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6578
    iget-object v0, p1, Lflipboard/objs/Link;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 6580
    :cond_2
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private b(Lflipboard/objs/Magazine;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 6649
    if-nez p1, :cond_0

    .line 6650
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 6736
    :goto_0
    return-void

    .line 6653
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 6654
    iget-object v0, p1, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    if-eqz v0, :cond_14

    .line 6655
    const/4 v0, 0x1

    const-string v2, "title"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6656
    iget-object v1, p1, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 6658
    :goto_1
    iget-object v1, p1, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 6659
    add-int/lit8 v1, v0, 0x1

    const-string v2, "magazineVisibility"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6660
    iget-object v0, p1, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6662
    :cond_1
    iget-object v1, p1, Lflipboard/objs/Magazine;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 6663
    add-int/lit8 v1, v0, 0x1

    const-string v2, "magazineTarget"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6664
    iget-object v0, p1, Lflipboard/objs/Magazine;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6666
    :cond_2
    iget-object v1, p1, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 6667
    add-int/lit8 v1, v0, 0x1

    const-string v2, "remoteid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6668
    iget-object v0, p1, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6670
    :cond_3
    iget-object v1, p1, Lflipboard/objs/Magazine;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 6671
    add-int/lit8 v1, v0, 0x1

    const-string v2, "description"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6672
    iget-object v0, p1, Lflipboard/objs/Magazine;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6674
    :cond_4
    iget-boolean v1, p1, Lflipboard/objs/Magazine;->f:Z

    if-eqz v1, :cond_5

    .line 6675
    add-int/lit8 v1, v0, 0x1

    const-string v2, "magazineCanChangeVisibility"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6676
    iget-boolean v0, p1, Lflipboard/objs/Magazine;->f:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 6678
    :cond_5
    iget-wide v2, p1, Lflipboard/objs/Magazine;->g:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_6

    .line 6679
    add-int/lit8 v1, v0, 0x1

    const-string v2, "magazineDateCreated"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6680
    iget-wide v2, p1, Lflipboard/objs/Magazine;->g:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    move v0, v1

    .line 6682
    :cond_6
    iget-wide v2, p1, Lflipboard/objs/Magazine;->h:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 6683
    add-int/lit8 v1, v0, 0x1

    const-string v2, "magazineDateLastPosted"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6684
    iget-wide v2, p1, Lflipboard/objs/Magazine;->h:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    move v0, v1

    .line 6686
    :cond_7
    iget-object v1, p1, Lflipboard/objs/Magazine;->i:Lflipboard/objs/Author;

    if-eqz v1, :cond_8

    .line 6687
    add-int/lit8 v1, v0, 0x1

    const-string v2, "author"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6688
    iget-object v0, p1, Lflipboard/objs/Magazine;->i:Lflipboard/objs/Author;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Author;)V

    move v0, v1

    .line 6690
    :cond_8
    iget-object v1, p1, Lflipboard/objs/Magazine;->j:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 6691
    add-int/lit8 v1, v0, 0x1

    const-string v2, "magazineCoverItemId"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6692
    iget-object v0, p1, Lflipboard/objs/Magazine;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6694
    :cond_9
    iget-object v1, p1, Lflipboard/objs/Magazine;->k:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 6695
    add-int/lit8 v1, v0, 0x1

    const-string v2, "feedType"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6696
    iget-object v0, p1, Lflipboard/objs/Magazine;->k:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6698
    :cond_a
    iget-object v1, p1, Lflipboard/objs/Magazine;->l:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 6699
    add-int/lit8 v1, v0, 0x1

    const-string v2, "magazineCategory"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6700
    iget-object v0, p1, Lflipboard/objs/Magazine;->l:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6702
    :cond_b
    iget-boolean v1, p1, Lflipboard/objs/Magazine;->m:Z

    if-eqz v1, :cond_c

    .line 6703
    add-int/lit8 v1, v0, 0x1

    const-string v2, "magazineIsDefault"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6704
    iget-boolean v0, p1, Lflipboard/objs/Magazine;->m:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 6706
    :cond_c
    iget-boolean v1, p1, Lflipboard/objs/Magazine;->n:Z

    if-eqz v1, :cond_d

    .line 6707
    add-int/lit8 v1, v0, 0x1

    const-string v2, "isDummyMagazine"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6708
    iget-boolean v0, p1, Lflipboard/objs/Magazine;->n:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 6710
    :cond_d
    iget v1, p1, Lflipboard/objs/Magazine;->o:I

    if-eqz v1, :cond_e

    .line 6711
    add-int/lit8 v1, v0, 0x1

    const-string v2, "defaultMagazineDrawableId"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6712
    iget v0, p1, Lflipboard/objs/Magazine;->o:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 6714
    :cond_e
    iget-object v1, p1, Lflipboard/objs/Magazine;->p:Lflipboard/objs/Link;

    if-eqz v1, :cond_f

    .line 6715
    add-int/lit8 v1, v0, 0x1

    const-string v2, "link"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6716
    iget-object v0, p1, Lflipboard/objs/Magazine;->p:Lflipboard/objs/Link;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/Link;)V

    move v0, v1

    .line 6718
    :cond_f
    iget-object v1, p1, Lflipboard/objs/Magazine;->q:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 6719
    add-int/lit8 v1, v0, 0x1

    const-string v2, "sectionTitle"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6720
    iget-object v0, p1, Lflipboard/objs/Magazine;->q:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6722
    :cond_10
    iget-object v1, p1, Lflipboard/objs/Magazine;->r:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 6723
    add-int/lit8 v1, v0, 0x1

    const-string v2, "service"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6724
    iget-object v0, p1, Lflipboard/objs/Magazine;->r:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6726
    :cond_11
    iget-object v1, p1, Lflipboard/objs/Magazine;->s:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 6727
    add-int/lit8 v1, v0, 0x1

    const-string v2, "imageURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6728
    iget-object v0, p1, Lflipboard/objs/Magazine;->s:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 6730
    :cond_12
    iget-object v1, p1, Lflipboard/objs/Magazine;->t:Lflipboard/objs/Image;

    if-eqz v1, :cond_13

    .line 6731
    const-string v1, "image"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 6732
    iget-object v0, p1, Lflipboard/objs/Magazine;->t:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    .line 6734
    :cond_13
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_14
    move v0, v1

    goto/16 :goto_1
.end method

.method private b(Lflipboard/objs/TOCSection;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 8381
    if-nez p1, :cond_0

    .line 8382
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 8560
    :goto_0
    return-void

    .line 8385
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 8386
    iget-object v0, p1, Lflipboard/objs/TOCSection;->p:Ljava/lang/String;

    if-eqz v0, :cond_29

    .line 8387
    const/4 v0, 0x1

    const-string v2, "type"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8388
    iget-object v1, p1, Lflipboard/objs/TOCSection;->p:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 8390
    :goto_1
    iget-object v1, p1, Lflipboard/objs/TOCSection;->a:Lflipboard/objs/Image;

    if-eqz v1, :cond_1

    .line 8391
    add-int/lit8 v1, v0, 0x1

    const-string v2, "brick"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8392
    iget-object v0, p1, Lflipboard/objs/TOCSection;->a:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v1

    .line 8394
    :cond_1
    iget-object v1, p1, Lflipboard/objs/TOCSection;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 8395
    add-int/lit8 v1, v0, 0x1

    const-string v2, "campaignTarget"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8396
    iget-object v0, p1, Lflipboard/objs/TOCSection;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8398
    :cond_2
    iget-object v1, p1, Lflipboard/objs/TOCSection;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 8399
    add-int/lit8 v1, v0, 0x1

    const-string v2, "category"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8400
    iget-object v0, p1, Lflipboard/objs/TOCSection;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8402
    :cond_3
    iget-object v1, p1, Lflipboard/objs/TOCSection;->d:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 8403
    add-int/lit8 v1, v0, 0x1

    const-string v2, "categoryList"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8404
    iget-object v0, p1, Lflipboard/objs/TOCSection;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8406
    :cond_4
    iget-object v1, p1, Lflipboard/objs/TOCSection;->e:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 8407
    add-int/lit8 v1, v0, 0x1

    const-string v2, "categoryTitle"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8408
    iget-object v0, p1, Lflipboard/objs/TOCSection;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8410
    :cond_5
    iget-object v1, p1, Lflipboard/objs/TOCSection;->f:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 8411
    add-int/lit8 v1, v0, 0x1

    const-string v2, "description"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8412
    iget-object v0, p1, Lflipboard/objs/TOCSection;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8414
    :cond_6
    iget-object v1, p1, Lflipboard/objs/TOCSection;->g:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 8415
    add-int/lit8 v1, v0, 0x1

    const-string v2, "icon"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8416
    iget-object v0, p1, Lflipboard/objs/TOCSection;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8418
    :cond_7
    iget-object v1, p1, Lflipboard/objs/TOCSection;->h:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 8419
    add-int/lit8 v1, v0, 0x1

    const-string v2, "imageURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8420
    iget-object v0, p1, Lflipboard/objs/TOCSection;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8422
    :cond_8
    iget-object v1, p1, Lflipboard/objs/TOCSection;->i:Lflipboard/objs/Image;

    if-eqz v1, :cond_9

    .line 8423
    add-int/lit8 v1, v0, 0x1

    const-string v2, "image"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8424
    iget-object v0, p1, Lflipboard/objs/TOCSection;->i:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v1

    .line 8426
    :cond_9
    iget-object v1, p1, Lflipboard/objs/TOCSection;->j:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 8427
    add-int/lit8 v1, v0, 0x1

    const-string v2, "keywords"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8428
    iget-object v0, p1, Lflipboard/objs/TOCSection;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8430
    :cond_a
    iget-object v1, p1, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 8431
    add-int/lit8 v1, v0, 0x1

    const-string v2, "remoteid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8432
    iget-object v0, p1, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8434
    :cond_b
    iget-object v1, p1, Lflipboard/objs/TOCSection;->l:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 8435
    add-int/lit8 v1, v0, 0x1

    const-string v2, "sectionTitle"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8436
    iget-object v0, p1, Lflipboard/objs/TOCSection;->l:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8438
    :cond_c
    iget-object v1, p1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 8439
    add-int/lit8 v1, v0, 0x1

    const-string v2, "service"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8440
    iget-object v0, p1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8442
    :cond_d
    iget-object v1, p1, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 8443
    add-int/lit8 v1, v0, 0x1

    const-string v2, "title"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8444
    iget-object v0, p1, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8446
    :cond_e
    iget-object v1, p1, Lflipboard/objs/TOCSection;->o:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 8447
    add-int/lit8 v1, v0, 0x1

    const-string v2, "titleSuffix"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8448
    iget-object v0, p1, Lflipboard/objs/TOCSection;->o:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8450
    :cond_f
    iget-object v1, p1, Lflipboard/objs/TOCSection;->q:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 8451
    add-int/lit8 v1, v0, 0x1

    const-string v2, "unreadRemoteid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8452
    iget-object v0, p1, Lflipboard/objs/TOCSection;->q:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8454
    :cond_10
    iget-object v1, p1, Lflipboard/objs/TOCSection;->r:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 8455
    add-int/lit8 v1, v0, 0x1

    const-string v2, "version"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8456
    iget-object v0, p1, Lflipboard/objs/TOCSection;->r:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8458
    :cond_11
    iget-boolean v1, p1, Lflipboard/objs/TOCSection;->s:Z

    if-eqz v1, :cond_12

    .line 8459
    add-int/lit8 v1, v0, 0x1

    const-string v2, "private"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8460
    iget-boolean v0, p1, Lflipboard/objs/TOCSection;->s:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 8462
    :cond_12
    iget-boolean v1, p1, Lflipboard/objs/TOCSection;->t:Z

    if-eqz v1, :cond_13

    .line 8463
    add-int/lit8 v1, v0, 0x1

    const-string v2, "canFavorite"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8464
    iget-boolean v0, p1, Lflipboard/objs/TOCSection;->t:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 8467
    :cond_13
    add-int/lit8 v1, v0, 0x1

    const-string v2, "feedFetchingDisabled"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8468
    iget-boolean v0, p1, Lflipboard/objs/TOCSection;->u:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    .line 8470
    iget-boolean v0, p1, Lflipboard/objs/TOCSection;->v:Z

    if-eqz v0, :cond_28

    .line 8471
    add-int/lit8 v0, v1, 0x1

    const-string v2, "isFavicon"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8472
    iget-boolean v1, p1, Lflipboard/objs/TOCSection;->v:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    .line 8474
    :goto_2
    iget-boolean v1, p1, Lflipboard/objs/TOCSection;->w:Z

    if-eqz v1, :cond_14

    .line 8475
    add-int/lit8 v1, v0, 0x1

    const-string v2, "verified"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8476
    iget-boolean v0, p1, Lflipboard/objs/TOCSection;->w:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 8478
    :cond_14
    iget v1, p1, Lflipboard/objs/TOCSection;->x:F

    float-to-double v2, v1

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_15

    .line 8479
    add-int/lit8 v1, v0, 0x1

    const-string v2, "categoryListWeight"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8480
    iget v0, p1, Lflipboard/objs/TOCSection;->x:F

    float-to-double v2, v0

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v1

    .line 8482
    :cond_15
    iget v1, p1, Lflipboard/objs/TOCSection;->y:F

    float-to-double v2, v1

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_16

    .line 8483
    add-int/lit8 v1, v0, 0x1

    const-string v2, "categoryWeight"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8484
    iget v0, p1, Lflipboard/objs/TOCSection;->y:F

    float-to-double v2, v0

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v1

    .line 8486
    :cond_16
    iget v1, p1, Lflipboard/objs/TOCSection;->z:F

    float-to-double v2, v1

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_17

    .line 8487
    add-int/lit8 v1, v0, 0x1

    const-string v2, "weight"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8488
    iget v0, p1, Lflipboard/objs/TOCSection;->z:F

    float-to-double v2, v0

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v0, v1

    .line 8490
    :cond_17
    iget v1, p1, Lflipboard/objs/TOCSection;->A:I

    if-eqz v1, :cond_18

    .line 8491
    add-int/lit8 v1, v0, 0x1

    const-string v2, "count"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8492
    iget v0, p1, Lflipboard/objs/TOCSection;->A:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 8495
    :cond_18
    add-int/lit8 v1, v0, 0x1

    const-string v2, "unreadCount"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8496
    iget v0, p1, Lflipboard/objs/TOCSection;->B:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    .line 8498
    iget-wide v2, p1, Lflipboard/objs/TOCSection;->C:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_27

    .line 8499
    add-int/lit8 v0, v1, 0x1

    const-string v2, "time"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8500
    iget-wide v2, p1, Lflipboard/objs/TOCSection;->C:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    .line 8502
    :goto_3
    iget-boolean v1, p1, Lflipboard/objs/TOCSection;->D:Z

    if-eqz v1, :cond_19

    .line 8503
    add-int/lit8 v1, v0, 0x1

    const-string v2, "showLogotypeImage"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8504
    iget-boolean v0, p1, Lflipboard/objs/TOCSection;->D:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 8506
    :cond_19
    iget-boolean v1, p1, Lflipboard/objs/TOCSection;->E:Z

    if-eqz v1, :cond_1a

    .line 8507
    add-int/lit8 v1, v0, 0x1

    const-string v2, "enumerated"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8508
    iget-boolean v0, p1, Lflipboard/objs/TOCSection;->E:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 8510
    :cond_1a
    iget-object v1, p1, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    if-eqz v1, :cond_1b

    .line 8511
    add-int/lit8 v1, v0, 0x1

    const-string v2, "feedType"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8512
    iget-object v0, p1, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8514
    :cond_1b
    iget-boolean v1, p1, Lflipboard/objs/TOCSection;->H:Z

    if-eqz v1, :cond_1c

    .line 8515
    add-int/lit8 v1, v0, 0x1

    const-string v2, "isLibrarySection"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8516
    iget-boolean v0, p1, Lflipboard/objs/TOCSection;->H:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 8518
    :cond_1c
    iget-wide v2, p1, Lflipboard/objs/TOCSection;->I:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_1d

    .line 8519
    add-int/lit8 v1, v0, 0x1

    const-string v2, "lastUpdated"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8520
    iget-wide v2, p1, Lflipboard/objs/TOCSection;->I:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    move v0, v1

    .line 8522
    :cond_1d
    iget-object v1, p1, Lflipboard/objs/TOCSection;->J:Ljava/lang/String;

    if-eqz v1, :cond_1e

    .line 8523
    add-int/lit8 v1, v0, 0x1

    const-string v2, "userid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8524
    iget-object v0, p1, Lflipboard/objs/TOCSection;->J:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8526
    :cond_1e
    iget-object v1, p1, Lflipboard/objs/TOCSection;->K:Ljava/lang/String;

    if-eqz v1, :cond_1f

    .line 8527
    add-int/lit8 v1, v0, 0x1

    const-string v2, "magazineTarget"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8528
    iget-object v0, p1, Lflipboard/objs/TOCSection;->K:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8530
    :cond_1f
    iget-object v1, p1, Lflipboard/objs/TOCSection;->L:Ljava/lang/String;

    if-eqz v1, :cond_20

    .line 8531
    add-int/lit8 v1, v0, 0x1

    const-string v2, "magazineVisibility"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8532
    iget-object v0, p1, Lflipboard/objs/TOCSection;->L:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8534
    :cond_20
    iget-object v1, p1, Lflipboard/objs/TOCSection;->M:Ljava/lang/String;

    if-eqz v1, :cond_21

    .line 8535
    add-int/lit8 v1, v0, 0x1

    const-string v2, "authorDisplayName"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8536
    iget-object v0, p1, Lflipboard/objs/TOCSection;->M:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8538
    :cond_21
    iget-boolean v1, p1, Lflipboard/objs/TOCSection;->N:Z

    if-eqz v1, :cond_22

    .line 8539
    add-int/lit8 v1, v0, 0x1

    const-string v2, "isFollowingAuthor"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8540
    iget-boolean v0, p1, Lflipboard/objs/TOCSection;->N:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 8542
    :cond_22
    iget-object v1, p1, Lflipboard/objs/TOCSection;->O:Ljava/lang/String;

    if-eqz v1, :cond_23

    .line 8543
    add-int/lit8 v1, v0, 0x1

    const-string v2, "prominenceOverrideType"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8544
    iget-object v0, p1, Lflipboard/objs/TOCSection;->O:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 8546
    :cond_23
    iget-boolean v1, p1, Lflipboard/objs/TOCSection;->P:Z

    if-eqz v1, :cond_24

    .line 8547
    add-int/lit8 v1, v0, 0x1

    const-string v2, "shouldWaitForSidebar"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 8548
    iget-boolean v0, p1, Lflipboard/objs/TOCSection;->P:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 8550
    :cond_24
    iget-object v1, p1, Lflipboard/objs/TOCSection;->G:Lflipboard/json/FLObject;

    if-eqz v1, :cond_26

    .line 8551
    iget-object v1, p1, Lflipboard/objs/TOCSection;->G:Lflipboard/json/FLObject;

    invoke-virtual {v1}, Lflipboard/json/FLObject;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 8552
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_25

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 8553
    :cond_25
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 8554
    const/16 v1, 0x3a

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 8555
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->b(Ljava/lang/Object;)V

    move v1, v2

    .line 8556
    goto :goto_4

    .line 8558
    :cond_26
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_27
    move v0, v1

    goto/16 :goto_3

    :cond_28
    move v0, v1

    goto/16 :goto_2

    :cond_29
    move v0, v1

    goto/16 :goto_1
.end method

.method private b(Lflipboard/objs/UserService;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 9111
    if-nez p1, :cond_0

    .line 9112
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 9194
    :goto_0
    return-void

    .line 9115
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 9116
    iget-object v0, p1, Lflipboard/objs/UserService;->a:Ljava/lang/String;

    if-eqz v0, :cond_16

    .line 9117
    const-string v0, "email"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9118
    iget-object v0, p1, Lflipboard/objs/UserService;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 9120
    :goto_1
    iget-object v3, p1, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 9121
    add-int/lit8 v3, v0, 0x1

    const-string v4, "name"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9122
    iget-object v0, p1, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v3

    .line 9124
    :cond_1
    iget-object v3, p1, Lflipboard/objs/UserService;->c:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 9125
    add-int/lit8 v3, v0, 0x1

    const-string v4, "profileURL"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9126
    iget-object v0, p1, Lflipboard/objs/UserService;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v3

    .line 9128
    :cond_2
    iget-object v3, p1, Lflipboard/objs/UserService;->d:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 9129
    add-int/lit8 v3, v0, 0x1

    const-string v4, "remoteid"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9130
    iget-object v0, p1, Lflipboard/objs/UserService;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v3

    .line 9132
    :cond_3
    iget-object v3, p1, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 9133
    add-int/lit8 v3, v0, 0x1

    const-string v4, "screenname"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9134
    iget-object v0, p1, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v3

    .line 9136
    :cond_4
    iget-object v3, p1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    if-eqz v3, :cond_5

    .line 9137
    add-int/lit8 v3, v0, 0x1

    const-string v4, "service"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9138
    iget-object v0, p1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v3

    .line 9140
    :cond_5
    iget-object v3, p1, Lflipboard/objs/UserService;->g:Ljava/lang/String;

    if-eqz v3, :cond_6

    .line 9141
    add-int/lit8 v3, v0, 0x1

    const-string v4, "unreadRemoteid"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9142
    iget-object v0, p1, Lflipboard/objs/UserService;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v3

    .line 9144
    :cond_6
    iget-object v3, p1, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    if-eqz v3, :cond_7

    .line 9145
    add-int/lit8 v3, v0, 0x1

    const-string v4, "userid"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9146
    iget-object v0, p1, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v3

    .line 9148
    :cond_7
    iget-boolean v3, p1, Lflipboard/objs/UserService;->i:Z

    if-eqz v3, :cond_8

    .line 9149
    add-int/lit8 v3, v0, 0x1

    const-string v4, "confirmedEmail"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9150
    iget-boolean v0, p1, Lflipboard/objs/UserService;->i:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v3

    .line 9152
    :cond_8
    iget-boolean v3, p1, Lflipboard/objs/UserService;->j:Z

    if-eqz v3, :cond_9

    .line 9153
    add-int/lit8 v3, v0, 0x1

    const-string v4, "isNew"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9154
    iget-boolean v0, p1, Lflipboard/objs/UserService;->j:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v3

    .line 9156
    :cond_9
    iget-object v3, p1, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    if-eqz v3, :cond_a

    .line 9157
    add-int/lit8 v3, v0, 0x1

    const-string v4, "subscriptionLevel"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9158
    iget-object v0, p1, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v3

    .line 9160
    :cond_a
    iget-object v3, p1, Lflipboard/objs/UserService;->l:Ljava/lang/String;

    if-eqz v3, :cond_b

    .line 9161
    add-int/lit8 v3, v0, 0x1

    const-string v4, "subsectionPageKey"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9162
    iget-object v0, p1, Lflipboard/objs/UserService;->l:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v3

    .line 9164
    :cond_b
    iget-object v3, p1, Lflipboard/objs/UserService;->m:Ljava/lang/String;

    if-eqz v3, :cond_c

    .line 9165
    add-int/lit8 v3, v0, 0x1

    const-string v4, "subscriptionLevelDisplayName"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9166
    iget-object v0, p1, Lflipboard/objs/UserService;->m:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v3

    .line 9168
    :cond_c
    iget-object v3, p1, Lflipboard/objs/UserService;->n:Ljava/lang/String;

    if-eqz v3, :cond_d

    .line 9169
    add-int/lit8 v3, v0, 0x1

    const-string v4, "profileRemoteid"

    invoke-virtual {p0, v0, v4}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9170
    iget-object v0, p1, Lflipboard/objs/UserService;->n:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v3

    .line 9172
    :cond_d
    iget-object v3, p1, Lflipboard/objs/UserService;->o:Ljava/util/List;

    if-eqz v3, :cond_e

    .line 9173
    add-int/lit8 v4, v0, 0x1

    const-string v3, "cookies"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9174
    iget-object v0, p1, Lflipboard/objs/UserService;->o:Ljava/util/List;

    if-nez v0, :cond_13

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    move v0, v4

    .line 9176
    :cond_e
    :goto_2
    iget-object v2, p1, Lflipboard/objs/UserService;->p:Lflipboard/objs/Image;

    if-eqz v2, :cond_f

    .line 9177
    add-int/lit8 v2, v0, 0x1

    const-string v3, "profileImage"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9178
    iget-object v0, p1, Lflipboard/objs/UserService;->p:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v2

    .line 9180
    :cond_f
    iget-object v2, p1, Lflipboard/objs/UserService;->q:Lflipboard/objs/FeedSection;

    if-eqz v2, :cond_10

    .line 9181
    add-int/lit8 v2, v0, 0x1

    const-string v3, "profileSection"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9182
    iget-object v0, p1, Lflipboard/objs/UserService;->q:Lflipboard/objs/FeedSection;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FeedSection;)V

    move v0, v2

    .line 9184
    :cond_10
    iget-boolean v2, p1, Lflipboard/objs/UserService;->r:Z

    if-eq v2, v1, :cond_11

    .line 9185
    add-int/lit8 v1, v0, 0x1

    const-string v2, "allowedToSubscribe"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9186
    iget-boolean v0, p1, Lflipboard/objs/UserService;->r:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 9188
    :cond_11
    iget-object v1, p1, Lflipboard/objs/UserService;->s:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 9189
    const-string v1, "description"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9190
    iget-object v0, p1, Lflipboard/objs/UserService;->s:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 9192
    :cond_12
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    .line 9174
    :cond_13
    const/16 v3, 0x5b

    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserService$Cookie;

    add-int/lit8 v3, v2, 0x1

    if-lez v2, :cond_14

    const/16 v2, 0x2c

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_14
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UserService$Cookie;)V

    move v2, v3

    goto :goto_3

    :cond_15
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    move v0, v4

    goto :goto_2

    :cond_16
    move v0, v2

    goto/16 :goto_1
.end method

.method private b(Lflipboard/objs/UserState;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 9535
    if-nez p1, :cond_0

    .line 9536
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 9586
    :goto_0
    return-void

    .line 9539
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 9540
    iget-object v0, p1, Lflipboard/objs/UserState;->k:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 9541
    const/4 v0, 0x1

    const-string v2, "type"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9542
    iget-object v1, p1, Lflipboard/objs/UserState;->k:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 9544
    :goto_1
    iget-boolean v1, p1, Lflipboard/objs/UserState;->b:Z

    if-eqz v1, :cond_1

    .line 9545
    add-int/lit8 v1, v0, 0x1

    const-string v2, "success"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9546
    iget-boolean v0, p1, Lflipboard/objs/UserState;->b:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 9548
    :cond_1
    iget v1, p1, Lflipboard/objs/UserState;->c:I

    if-eqz v1, :cond_2

    .line 9549
    add-int/lit8 v1, v0, 0x1

    const-string v2, "code"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9550
    iget v0, p1, Lflipboard/objs/UserState;->c:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 9552
    :cond_2
    iget-object v1, p1, Lflipboard/objs/UserState;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 9553
    add-int/lit8 v1, v0, 0x1

    const-string v2, "message"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9554
    iget-object v0, p1, Lflipboard/objs/UserState;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 9556
    :cond_3
    iget-wide v2, p1, Lflipboard/objs/UserState;->e:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 9557
    add-int/lit8 v1, v0, 0x1

    const-string v2, "time"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9558
    iget-wide v2, p1, Lflipboard/objs/UserState;->e:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    move v0, v1

    .line 9560
    :cond_4
    iget v1, p1, Lflipboard/objs/UserState;->f:I

    if-eqz v1, :cond_5

    .line 9561
    add-int/lit8 v1, v0, 0x1

    const-string v2, "errorcode"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9562
    iget v0, p1, Lflipboard/objs/UserState;->f:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 9564
    :cond_5
    iget-object v1, p1, Lflipboard/objs/UserState;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 9565
    add-int/lit8 v1, v0, 0x1

    const-string v2, "errormessage"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9566
    iget-object v0, p1, Lflipboard/objs/UserState;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 9568
    :cond_6
    iget-object v1, p1, Lflipboard/objs/UserState;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 9569
    add-int/lit8 v1, v0, 0x1

    const-string v2, "displaymessage"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9570
    iget-object v0, p1, Lflipboard/objs/UserState;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 9572
    :cond_7
    iget v1, p1, Lflipboard/objs/UserState;->i:I

    if-eqz v1, :cond_8

    .line 9573
    add-int/lit8 v1, v0, 0x1

    const-string v2, "userid"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9574
    iget v0, p1, Lflipboard/objs/UserState;->i:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    .line 9576
    :cond_8
    iget-object v1, p1, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    if-eqz v1, :cond_9

    .line 9577
    add-int/lit8 v1, v0, 0x1

    const-string v2, "state"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9578
    iget-object v0, p1, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UserState$State;)V

    move v0, v1

    .line 9580
    :cond_9
    iget-object v1, p1, Lflipboard/objs/UserState;->l:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 9581
    const-string v1, "revision"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9582
    iget-object v0, p1, Lflipboard/objs/UserState;->l:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 9584
    :cond_a
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method private b(Lflipboard/service/Account$Meta;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 9963
    if-nez p1, :cond_0

    .line 9964
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 9986
    :goto_0
    return-void

    .line 9967
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 9968
    iget-boolean v0, p1, Lflipboard/service/Account$Meta;->b:Z

    if-eqz v0, :cond_4

    .line 9969
    const/4 v0, 0x1

    const-string v2, "isReadLaterService"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9970
    iget-boolean v1, p1, Lflipboard/service/Account$Meta;->b:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    .line 9972
    :goto_1
    iget-object v1, p1, Lflipboard/service/Account$Meta;->c:Lflipboard/json/FLObject;

    if-eqz v1, :cond_1

    .line 9973
    add-int/lit8 v1, v0, 0x1

    const-string v2, "selectedShareTargets"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9974
    iget-object v0, p1, Lflipboard/service/Account$Meta;->c:Lflipboard/json/FLObject;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/json/FLObject;)V

    move v0, v1

    .line 9976
    :cond_1
    iget-object v1, p1, Lflipboard/service/Account$Meta;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 9977
    add-int/lit8 v1, v0, 0x1

    const-string v2, "accessToken"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9978
    iget-object v0, p1, Lflipboard/service/Account$Meta;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 9980
    :cond_2
    iget-object v1, p1, Lflipboard/service/Account$Meta;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 9981
    const-string v1, "refreshToken"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 9982
    iget-object v0, p1, Lflipboard/service/Account$Meta;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 9984
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private b(Lflipboard/service/Section$Meta;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 10053
    if-nez p1, :cond_0

    .line 10054
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 10100
    :goto_0
    return-void

    .line 10057
    :cond_0
    const/16 v0, 0x7b

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 10058
    iget-boolean v0, p1, Lflipboard/service/Section$Meta;->b:Z

    if-eqz v0, :cond_a

    .line 10059
    const/4 v0, 0x1

    const-string v2, "isPlaceHolder"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 10060
    iget-boolean v1, p1, Lflipboard/service/Section$Meta;->b:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    .line 10062
    :goto_1
    iget-boolean v1, p1, Lflipboard/service/Section$Meta;->c:Z

    if-eqz v1, :cond_1

    .line 10063
    add-int/lit8 v1, v0, 0x1

    const-string v2, "isUnreadMode"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 10064
    iget-boolean v0, p1, Lflipboard/service/Section$Meta;->c:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v0, v1

    .line 10066
    :cond_1
    iget-object v1, p1, Lflipboard/service/Section$Meta;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 10067
    add-int/lit8 v1, v0, 0x1

    const-string v2, "noItemsText"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 10068
    iget-object v0, p1, Lflipboard/service/Section$Meta;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 10070
    :cond_2
    iget-object v1, p1, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 10071
    add-int/lit8 v1, v0, 0x1

    const-string v2, "partnerId"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 10072
    iget-object v0, p1, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 10074
    :cond_3
    iget-object v1, p1, Lflipboard/service/Section$Meta;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 10075
    add-int/lit8 v1, v0, 0x1

    const-string v2, "ecommerceCheckoutURL"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 10076
    iget-object v0, p1, Lflipboard/service/Section$Meta;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 10078
    :cond_4
    iget-object v1, p1, Lflipboard/service/Section$Meta;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 10079
    add-int/lit8 v1, v0, 0x1

    const-string v2, "campaignTarget"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 10080
    iget-object v0, p1, Lflipboard/service/Section$Meta;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    .line 10082
    :cond_5
    iget-object v1, p1, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    if-eqz v1, :cond_6

    .line 10083
    add-int/lit8 v1, v0, 0x1

    const-string v2, "sidebarGroups"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 10084
    iget-object v0, p1, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->m(Ljava/util/List;)V

    move v0, v1

    .line 10086
    :cond_6
    iget-object v1, p1, Lflipboard/service/Section$Meta;->i:Lflipboard/objs/FeedSectionLink;

    if-eqz v1, :cond_7

    .line 10087
    add-int/lit8 v1, v0, 0x1

    const-string v2, "profileSectionLink"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 10088
    iget-object v0, p1, Lflipboard/service/Section$Meta;->i:Lflipboard/objs/FeedSectionLink;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FeedSectionLink;)V

    move v0, v1

    .line 10090
    :cond_7
    iget-object v1, p1, Lflipboard/service/Section$Meta;->j:Lflipboard/objs/Image;

    if-eqz v1, :cond_8

    .line 10091
    add-int/lit8 v1, v0, 0x1

    const-string v2, "profileBackgroundImage"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 10092
    iget-object v0, p1, Lflipboard/service/Section$Meta;->j:Lflipboard/objs/Image;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    move v0, v1

    .line 10094
    :cond_8
    iget-object v1, p1, Lflipboard/service/Section$Meta;->k:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 10095
    const-string v1, "feedType"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    .line 10096
    iget-object v0, p1, Lflipboard/service/Section$Meta;->k:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    .line 10098
    :cond_9
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_a
    move v0, v1

    goto/16 :goto_1
.end method

.method public static b(Ljava/util/List;)[B
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)[B"
        }
    .end annotation

    .prologue
    .line 4146
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit16 v1, v1, 0x258

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 4147
    new-instance v1, Lflipboard/json/JSONSerializer;

    invoke-direct {v1, v0}, Lflipboard/json/JSONSerializer;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v1, p0}, Lflipboard/json/JSONSerializer;->c(Ljava/util/List;)V

    .line 4148
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method private e(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/io/RequestLogEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 404
    if-nez p1, :cond_0

    .line 405
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 415
    :goto_0
    return-void

    .line 407
    :cond_0
    const/4 v0, 0x0

    .line 408
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 409
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/RequestLogEntry;

    .line 410
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_1

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 411
    :cond_1
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/io/RequestLogEntry;)V

    move v1, v2

    .line 412
    goto :goto_1

    .line 413
    :cond_2
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0
.end method

.method private f(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigHints$Condition;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2464
    if-nez p1, :cond_0

    .line 2465
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 2475
    :goto_0
    return-void

    .line 2467
    :cond_0
    const/4 v0, 0x0

    .line 2468
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 2469
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigHints$Condition;

    .line 2470
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_1

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 2471
    :cond_1
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigHints$Condition;)V

    move v1, v2

    .line 2472
    goto :goto_1

    .line 2473
    :cond_2
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0
.end method

.method private g(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigSection;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3020
    if-nez p1, :cond_0

    .line 3021
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 3031
    :goto_0
    return-void

    .line 3023
    :cond_0
    const/4 v0, 0x0

    .line 3024
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 3025
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigSection;

    .line 3026
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_1

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 3027
    :cond_1
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigSection;)V

    move v1, v2

    .line 3028
    goto :goto_1

    .line 3029
    :cond_2
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0
.end method

.method private h(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigService;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3682
    if-nez p1, :cond_0

    .line 3683
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 3693
    :goto_0
    return-void

    .line 3685
    :cond_0
    const/4 v0, 0x0

    .line 3686
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 3687
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 3688
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_1

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 3689
    :cond_1
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigService;)V

    move v1, v2

    .line 3690
    goto :goto_1

    .line 3691
    :cond_2
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0
.end method

.method private i(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedSectionLink;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 5442
    if-nez p1, :cond_0

    .line 5443
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 5453
    :goto_0
    return-void

    .line 5445
    :cond_0
    const/4 v0, 0x0

    .line 5446
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 5447
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 5448
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_1

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 5449
    :cond_1
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FeedSectionLink;)V

    move v1, v2

    .line 5450
    goto :goto_1

    .line 5451
    :cond_2
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0
.end method

.method private j(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FlipResponse$FlipImagesResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 5858
    if-nez p1, :cond_0

    .line 5859
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 5869
    :goto_0
    return-void

    .line 5861
    :cond_0
    const/4 v0, 0x0

    .line 5862
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 5863
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FlipResponse$FlipImagesResult;

    .line 5864
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_1

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 5865
    :cond_1
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FlipResponse$FlipImagesResult;)V

    move v1, v2

    .line 5866
    goto :goto_1

    .line 5867
    :cond_2
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0
.end method

.method private k(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SectionListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 7524
    if-nez p1, :cond_0

    .line 7525
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 7535
    :goto_0
    return-void

    .line 7527
    :cond_0
    const/4 v0, 0x0

    .line 7528
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 7529
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SectionListItem;

    .line 7530
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_1

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 7531
    :cond_1
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/SectionListItem;)V

    move v1, v2

    .line 7532
    goto :goto_1

    .line 7533
    :cond_2
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0
.end method

.method private l(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SectionPageTemplate$Area;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 7874
    if-nez p1, :cond_0

    .line 7875
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 7885
    :goto_0
    return-void

    .line 7877
    :cond_0
    const/4 v0, 0x0

    .line 7878
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 7879
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SectionPageTemplate$Area;

    .line 7880
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_1

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 7881
    :cond_1
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/SectionPageTemplate$Area;)V

    move v1, v2

    .line 7882
    goto :goto_1

    .line 7883
    :cond_2
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0
.end method

.method private m(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SidebarGroup;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 7988
    if-nez p1, :cond_0

    .line 7989
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 7999
    :goto_0
    return-void

    .line 7991
    :cond_0
    const/4 v0, 0x0

    .line 7992
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 7993
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    .line 7994
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_1

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 7995
    :cond_1
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/SidebarGroup;)V

    move v1, v2

    .line 7996
    goto :goto_1

    .line 7997
    :cond_2
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0
.end method

.method private n(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/UserService;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 9198
    if-nez p1, :cond_0

    .line 9199
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 9209
    :goto_0
    return-void

    .line 9201
    :cond_0
    const/4 v0, 0x0

    .line 9202
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 9203
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserService;

    .line 9204
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_1

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 9205
    :cond_1
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/UserService;)V

    move v1, v2

    .line 9206
    goto :goto_1

    .line 9207
    :cond_2
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0
.end method

.method private o(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/UserState$MutedAuthor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 9806
    if-nez p1, :cond_0

    .line 9807
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 9817
    :goto_0
    return-void

    .line 9809
    :cond_0
    const/4 v0, 0x0

    .line 9810
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 9811
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserState$MutedAuthor;

    .line 9812
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_1

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 9813
    :cond_1
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UserState$MutedAuthor;)V

    move v1, v2

    .line 9814
    goto :goto_1

    .line 9815
    :cond_2
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 10

    .prologue
    const/16 v9, 0x2c

    const/4 v1, 0x1

    const/16 v8, 0x7d

    const/16 v3, 0x7b

    const/4 v2, 0x0

    .line 26
    instance-of v0, p1, Lflipboard/io/RequestLogEntry;

    if-eqz v0, :cond_0

    .line 27
    check-cast p1, Lflipboard/io/RequestLogEntry;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/io/RequestLogEntry;)V

    .line 335
    :goto_0
    return-void

    .line 30
    :cond_0
    instance-of v0, p1, Lflipboard/objs/Ad;

    if-eqz v0, :cond_c

    .line 31
    check-cast p1, Lflipboard/objs/Ad;

    if-nez p1, :cond_1

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-object v0, p1, Lflipboard/objs/Ad;->b:Ljava/lang/String;

    if-eqz v0, :cond_115

    const-string v0, "ad_type"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/Ad;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    :goto_1
    iget-object v1, p1, Lflipboard/objs/Ad;->c:Ljava/util/List;

    if-eqz v1, :cond_2

    add-int/lit8 v1, v0, 0x1

    const-string v3, "click_tracking_urls"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/Ad;->c:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    move v0, v1

    :cond_2
    iget-object v1, p1, Lflipboard/objs/Ad;->d:Ljava/util/List;

    if-eqz v1, :cond_3

    add-int/lit8 v1, v0, 0x1

    const-string v3, "impression_tracking_urls"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/Ad;->d:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    move v0, v1

    :cond_3
    iget-object v1, p1, Lflipboard/objs/Ad;->e:Ljava/util/List;

    if-eqz v1, :cond_4

    add-int/lit8 v3, v0, 0x1

    const-string v1, "assets"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/Ad;->e:Ljava/util/List;

    if-nez v0, :cond_9

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    move v0, v3

    :cond_4
    :goto_2
    iget-object v1, p1, Lflipboard/objs/Ad;->f:Lflipboard/objs/Ad$ButtonInfo;

    if-eqz v1, :cond_5

    add-int/lit8 v1, v0, 0x1

    const-string v2, "button_info"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/Ad;->f:Lflipboard/objs/Ad$ButtonInfo;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Ad$ButtonInfo;)V

    move v0, v1

    :cond_5
    iget-object v1, p1, Lflipboard/objs/Ad;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    add-int/lit8 v1, v0, 0x1

    const-string v2, "impression_value"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/Ad;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v0, v1

    :cond_6
    iget v1, p1, Lflipboard/objs/Ad;->h:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_7

    add-int/lit8 v1, v0, 0x1

    const-string v2, "min_pages_before_shown"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v0, p1, Lflipboard/objs/Ad;->h:I

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(I)V

    move v0, v1

    :cond_7
    iget-object v1, p1, Lflipboard/objs/Ad;->k:Lflipboard/objs/Ad$VideoInfo;

    if-eqz v1, :cond_8

    const-string v1, "video_info"

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/Ad;->k:Lflipboard/objs/Ad$VideoInfo;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Ad$VideoInfo;)V

    :cond_8
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_9
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Ad$Asset;

    add-int/lit8 v1, v2, 0x1

    if-lez v2, :cond_a

    invoke-virtual {p0, v9}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_a
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Ad$Asset;)V

    move v2, v1

    goto :goto_3

    :cond_b
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    move v0, v3

    goto :goto_2

    .line 34
    :cond_c
    instance-of v0, p1, Lflipboard/objs/Ad$Asset;

    if-eqz v0, :cond_d

    .line 35
    check-cast p1, Lflipboard/objs/Ad$Asset;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Ad$Asset;)V

    goto/16 :goto_0

    .line 38
    :cond_d
    instance-of v0, p1, Lflipboard/objs/Ad$Button;

    if-eqz v0, :cond_e

    .line 39
    check-cast p1, Lflipboard/objs/Ad$Button;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Ad$Button;)V

    goto/16 :goto_0

    .line 42
    :cond_e
    instance-of v0, p1, Lflipboard/objs/Ad$ButtonInfo;

    if-eqz v0, :cond_f

    .line 43
    check-cast p1, Lflipboard/objs/Ad$ButtonInfo;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Ad$ButtonInfo;)V

    goto/16 :goto_0

    .line 46
    :cond_f
    instance-of v0, p1, Lflipboard/objs/Ad$HotSpot;

    if-eqz v0, :cond_10

    .line 47
    check-cast p1, Lflipboard/objs/Ad$HotSpot;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Ad$HotSpot;)V

    goto/16 :goto_0

    .line 50
    :cond_10
    instance-of v0, p1, Lflipboard/objs/Ad$MetricValues;

    if-eqz v0, :cond_11

    .line 51
    check-cast p1, Lflipboard/objs/Ad$MetricValues;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Ad$MetricValues;)V

    goto/16 :goto_0

    .line 54
    :cond_11
    instance-of v0, p1, Lflipboard/objs/Ad$VideoInfo;

    if-eqz v0, :cond_12

    .line 55
    check-cast p1, Lflipboard/objs/Ad$VideoInfo;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Ad$VideoInfo;)V

    goto/16 :goto_0

    .line 58
    :cond_12
    instance-of v0, p1, Lflipboard/objs/AlsoFlippedResult;

    if-eqz v0, :cond_1b

    .line 59
    check-cast p1, Lflipboard/objs/AlsoFlippedResult;

    if-nez p1, :cond_13

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_13
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-boolean v0, p1, Lflipboard/objs/AlsoFlippedResult;->b:Z

    if-eqz v0, :cond_114

    const-string v0, "success"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v0, p1, Lflipboard/objs/AlsoFlippedResult;->b:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    :goto_4
    iget v0, p1, Lflipboard/objs/AlsoFlippedResult;->c:I

    if-eqz v0, :cond_14

    add-int/lit8 v0, v1, 0x1

    const-string v2, "code"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/AlsoFlippedResult;->c:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_14
    iget-object v0, p1, Lflipboard/objs/AlsoFlippedResult;->d:Ljava/lang/String;

    if-eqz v0, :cond_15

    add-int/lit8 v0, v1, 0x1

    const-string v2, "message"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/AlsoFlippedResult;->d:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_15
    iget-wide v2, p1, Lflipboard/objs/AlsoFlippedResult;->e:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_16

    add-int/lit8 v0, v1, 0x1

    const-string v2, "time"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-wide v2, p1, Lflipboard/objs/AlsoFlippedResult;->e:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    move v1, v0

    :cond_16
    iget v0, p1, Lflipboard/objs/AlsoFlippedResult;->f:I

    if-eqz v0, :cond_17

    add-int/lit8 v0, v1, 0x1

    const-string v2, "errorcode"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/AlsoFlippedResult;->f:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_17
    iget-object v0, p1, Lflipboard/objs/AlsoFlippedResult;->g:Ljava/lang/String;

    if-eqz v0, :cond_18

    add-int/lit8 v0, v1, 0x1

    const-string v2, "errormessage"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/AlsoFlippedResult;->g:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_18
    iget-object v0, p1, Lflipboard/objs/AlsoFlippedResult;->h:Ljava/lang/String;

    if-eqz v0, :cond_19

    add-int/lit8 v0, v1, 0x1

    const-string v2, "displaymessage"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/AlsoFlippedResult;->h:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_19
    iget-object v0, p1, Lflipboard/objs/AlsoFlippedResult;->a:Ljava/util/List;

    if-eqz v0, :cond_1a

    const-string v0, "results"

    invoke-virtual {p0, v1, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/AlsoFlippedResult;->a:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->c(Ljava/util/List;)V

    :cond_1a
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    .line 62
    :cond_1b
    instance-of v0, p1, Lflipboard/objs/Author;

    if-eqz v0, :cond_1c

    .line 63
    check-cast p1, Lflipboard/objs/Author;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Author;)V

    goto/16 :goto_0

    .line 66
    :cond_1c
    instance-of v0, p1, Lflipboard/objs/Brick;

    if-eqz v0, :cond_24

    .line 67
    check-cast p1, Lflipboard/objs/Brick;

    if-nez p1, :cond_1d

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_1d
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-object v0, p1, Lflipboard/objs/Brick;->a:Ljava/lang/String;

    if-eqz v0, :cond_113

    const-string v0, "imageURL"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/Brick;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    :goto_5
    iget v0, p1, Lflipboard/objs/Brick;->b:I

    if-eqz v0, :cond_1e

    add-int/lit8 v0, v1, 0x1

    const-string v2, "width"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/Brick;->b:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_1e
    iget v0, p1, Lflipboard/objs/Brick;->c:I

    if-eqz v0, :cond_1f

    add-int/lit8 v0, v1, 0x1

    const-string v2, "height"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/Brick;->c:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_1f
    iget v0, p1, Lflipboard/objs/Brick;->d:I

    if-eqz v0, :cond_20

    add-int/lit8 v0, v1, 0x1

    const-string v2, "perRow"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/Brick;->d:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_20
    iget-boolean v0, p1, Lflipboard/objs/Brick;->e:Z

    if-eqz v0, :cond_21

    add-int/lit8 v0, v1, 0x1

    const-string v2, "showAddButton"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v1, p1, Lflipboard/objs/Brick;->e:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v1, v0

    :cond_21
    iget-boolean v0, p1, Lflipboard/objs/Brick;->f:Z

    if-eqz v0, :cond_22

    add-int/lit8 v0, v1, 0x1

    const-string v2, "showAuthor"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v1, p1, Lflipboard/objs/Brick;->f:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v1, v0

    :cond_22
    iget-boolean v0, p1, Lflipboard/objs/Brick;->g:Z

    if-eqz v0, :cond_23

    const-string v0, "showTitle"

    invoke-virtual {p0, v1, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v0, p1, Lflipboard/objs/Brick;->g:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    :cond_23
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    .line 70
    :cond_24
    instance-of v0, p1, Lflipboard/objs/CommentaryResult;

    if-eqz v0, :cond_30

    .line 71
    check-cast p1, Lflipboard/objs/CommentaryResult;

    if-nez p1, :cond_25

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_25
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-boolean v0, p1, Lflipboard/objs/CommentaryResult;->b:Z

    if-eqz v0, :cond_112

    const-string v0, "success"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v0, p1, Lflipboard/objs/CommentaryResult;->b:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    :goto_6
    iget v0, p1, Lflipboard/objs/CommentaryResult;->c:I

    if-eqz v0, :cond_26

    add-int/lit8 v0, v1, 0x1

    const-string v3, "code"

    invoke-virtual {p0, v1, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/CommentaryResult;->c:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_26
    iget-object v0, p1, Lflipboard/objs/CommentaryResult;->d:Ljava/lang/String;

    if-eqz v0, :cond_27

    add-int/lit8 v0, v1, 0x1

    const-string v3, "message"

    invoke-virtual {p0, v1, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/CommentaryResult;->d:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_27
    iget-wide v4, p1, Lflipboard/objs/CommentaryResult;->e:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_28

    add-int/lit8 v0, v1, 0x1

    const-string v3, "time"

    invoke-virtual {p0, v1, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-wide v4, p1, Lflipboard/objs/CommentaryResult;->e:J

    invoke-virtual {p0, v4, v5}, Lflipboard/json/JSONSerializer;->a(J)V

    move v1, v0

    :cond_28
    iget v0, p1, Lflipboard/objs/CommentaryResult;->f:I

    if-eqz v0, :cond_29

    add-int/lit8 v0, v1, 0x1

    const-string v3, "errorcode"

    invoke-virtual {p0, v1, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/CommentaryResult;->f:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_29
    iget-object v0, p1, Lflipboard/objs/CommentaryResult;->g:Ljava/lang/String;

    if-eqz v0, :cond_2a

    add-int/lit8 v0, v1, 0x1

    const-string v3, "errormessage"

    invoke-virtual {p0, v1, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/CommentaryResult;->g:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_2a
    iget-object v0, p1, Lflipboard/objs/CommentaryResult;->h:Ljava/lang/String;

    if-eqz v0, :cond_2b

    add-int/lit8 v0, v1, 0x1

    const-string v3, "displaymessage"

    invoke-virtual {p0, v1, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/CommentaryResult;->h:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_2b
    iget-object v0, p1, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    if-eqz v0, :cond_2c

    const-string v0, "items"

    invoke-virtual {p0, v1, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    if-nez v0, :cond_2d

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    :cond_2c
    :goto_7
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_2d
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item;

    add-int/lit8 v1, v2, 0x1

    if-lez v2, :cond_2e

    invoke-virtual {p0, v9}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_2e
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/CommentaryResult$Item;)V

    move v2, v1

    goto :goto_8

    :cond_2f
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_7

    .line 74
    :cond_30
    instance-of v0, p1, Lflipboard/objs/CommentaryResult$Item;

    if-eqz v0, :cond_31

    .line 75
    check-cast p1, Lflipboard/objs/CommentaryResult$Item;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/CommentaryResult$Item;)V

    goto/16 :goto_0

    .line 78
    :cond_31
    instance-of v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;

    if-eqz v0, :cond_32

    .line 79
    check-cast p1, Lflipboard/objs/CommentaryResult$Item$Commentary;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/CommentaryResult$Item$Commentary;)V

    goto/16 :goto_0

    .line 82
    :cond_32
    instance-of v0, p1, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;

    if-eqz v0, :cond_33

    .line 83
    check-cast p1, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/CommentaryResult$Item$ProfileMetric;)V

    goto/16 :goto_0

    .line 86
    :cond_33
    instance-of v0, p1, Lflipboard/objs/ConfigBrick;

    if-eqz v0, :cond_34

    .line 87
    check-cast p1, Lflipboard/objs/ConfigBrick;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigBrick;)V

    goto/16 :goto_0

    .line 90
    :cond_34
    instance-of v0, p1, Lflipboard/objs/ConfigContentGuide;

    if-eqz v0, :cond_3d

    .line 91
    check-cast p1, Lflipboard/objs/ConfigContentGuide;

    if-nez p1, :cond_35

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_35
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-object v0, p1, Lflipboard/objs/ConfigContentGuide;->a:Ljava/util/List;

    if-eqz v0, :cond_111

    const-string v0, "sections"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/ConfigContentGuide;->a:Ljava/util/List;

    if-nez v0, :cond_37

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    :goto_9
    iget-object v0, p1, Lflipboard/objs/ConfigContentGuide;->b:Ljava/util/List;

    if-eqz v0, :cond_36

    const-string v0, "editions"

    invoke-virtual {p0, v1, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/ConfigContentGuide;->b:Ljava/util/List;

    if-nez v0, :cond_3a

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    :cond_36
    :goto_a
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_37
    const/16 v3, 0x5b

    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v2

    :goto_b
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_39

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigFolder;

    add-int/lit8 v4, v3, 0x1

    if-lez v3, :cond_38

    invoke-virtual {p0, v9}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_38
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigFolder;)V

    move v3, v4

    goto :goto_b

    :cond_39
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_9

    :cond_3a
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigEdition;

    add-int/lit8 v1, v2, 0x1

    if-lez v2, :cond_3b

    invoke-virtual {p0, v9}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_3b
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigEdition;)V

    move v2, v1

    goto :goto_c

    :cond_3c
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_a

    .line 94
    :cond_3d
    instance-of v0, p1, Lflipboard/objs/ConfigEdition;

    if-eqz v0, :cond_3e

    .line 95
    check-cast p1, Lflipboard/objs/ConfigEdition;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigEdition;)V

    goto/16 :goto_0

    .line 98
    :cond_3e
    instance-of v0, p1, Lflipboard/objs/ConfigFolder;

    if-eqz v0, :cond_3f

    .line 99
    check-cast p1, Lflipboard/objs/ConfigFolder;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigFolder;)V

    goto/16 :goto_0

    .line 102
    :cond_3f
    instance-of v0, p1, Lflipboard/objs/ConfigHints;

    if-eqz v0, :cond_46

    .line 103
    check-cast p1, Lflipboard/objs/ConfigHints;

    if-nez p1, :cond_40

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_40
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget v0, p1, Lflipboard/objs/ConfigHints;->a:F

    float-to-double v4, v0

    const-wide/16 v6, 0x0

    cmpl-double v0, v4, v6

    if-eqz v0, :cond_110

    const-string v0, "hintsPerSessionFrequencyCap"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v0, p1, Lflipboard/objs/ConfigHints;->a:F

    float-to-double v4, v0

    invoke-virtual {p0, v4, v5}, Lflipboard/json/JSONSerializerBase;->a(D)V

    :goto_d
    iget v0, p1, Lflipboard/objs/ConfigHints;->b:F

    float-to-double v4, v0

    const-wide/16 v6, 0x0

    cmpl-double v0, v4, v6

    if-eqz v0, :cond_41

    add-int/lit8 v0, v1, 0x1

    const-string v3, "hintSeparationTimeInterval"

    invoke-virtual {p0, v1, v3}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/ConfigHints;->b:F

    float-to-double v4, v1

    invoke-virtual {p0, v4, v5}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v1, v0

    :cond_41
    iget-object v0, p1, Lflipboard/objs/ConfigHints;->c:Ljava/util/List;

    if-eqz v0, :cond_42

    const-string v0, "hints"

    invoke-virtual {p0, v1, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/ConfigHints;->c:Ljava/util/List;

    if-nez v0, :cond_43

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    :cond_42
    :goto_e
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_43
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_45

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigHints$Hint;

    add-int/lit8 v1, v2, 0x1

    if-lez v2, :cond_44

    invoke-virtual {p0, v9}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_44
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigHints$Hint;)V

    move v2, v1

    goto :goto_f

    :cond_45
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_e

    .line 106
    :cond_46
    instance-of v0, p1, Lflipboard/objs/ConfigHints$Condition;

    if-eqz v0, :cond_47

    .line 107
    check-cast p1, Lflipboard/objs/ConfigHints$Condition;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigHints$Condition;)V

    goto/16 :goto_0

    .line 110
    :cond_47
    instance-of v0, p1, Lflipboard/objs/ConfigHints$Hint;

    if-eqz v0, :cond_48

    .line 111
    check-cast p1, Lflipboard/objs/ConfigHints$Hint;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigHints$Hint;)V

    goto/16 :goto_0

    .line 114
    :cond_48
    instance-of v0, p1, Lflipboard/objs/ConfigPopularSearches;

    if-eqz v0, :cond_4e

    .line 115
    check-cast p1, Lflipboard/objs/ConfigPopularSearches;

    if-nez p1, :cond_49

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_49
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-object v0, p1, Lflipboard/objs/ConfigPopularSearches;->a:Ljava/lang/String;

    if-eqz v0, :cond_10f

    const-string v0, "title"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/ConfigPopularSearches;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    :goto_10
    iget-object v0, p1, Lflipboard/objs/ConfigPopularSearches;->b:Ljava/util/List;

    if-eqz v0, :cond_4a

    const-string v0, "categories"

    invoke-virtual {p0, v1, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/ConfigPopularSearches;->b:Ljava/util/List;

    if-nez v0, :cond_4b

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    :cond_4a
    :goto_11
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_4b
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_12
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigPopularSearches$Category;

    add-int/lit8 v1, v2, 0x1

    if-lez v2, :cond_4c

    invoke-virtual {p0, v9}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_4c
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigPopularSearches$Category;)V

    move v2, v1

    goto :goto_12

    :cond_4d
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_11

    .line 118
    :cond_4e
    instance-of v0, p1, Lflipboard/objs/ConfigPopularSearches$Category;

    if-eqz v0, :cond_4f

    .line 119
    check-cast p1, Lflipboard/objs/ConfigPopularSearches$Category;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigPopularSearches$Category;)V

    goto/16 :goto_0

    .line 122
    :cond_4f
    instance-of v0, p1, Lflipboard/objs/ConfigSection;

    if-eqz v0, :cond_50

    .line 123
    check-cast p1, Lflipboard/objs/ConfigSection;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigSection;)V

    goto/16 :goto_0

    .line 126
    :cond_50
    instance-of v0, p1, Lflipboard/objs/ConfigService;

    if-eqz v0, :cond_51

    .line 127
    check-cast p1, Lflipboard/objs/ConfigService;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigService;)V

    goto/16 :goto_0

    .line 130
    :cond_51
    instance-of v0, p1, Lflipboard/objs/ConfigServices;

    if-eqz v0, :cond_59

    .line 131
    check-cast p1, Lflipboard/objs/ConfigServices;

    if-nez p1, :cond_52

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_52
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-object v0, p1, Lflipboard/objs/ConfigServices;->bP:Ljava/lang/String;

    if-eqz v0, :cond_10e

    const-string v0, "title"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/ConfigServices;->bP:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    :goto_13
    iget-object v0, p1, Lflipboard/objs/ConfigServices;->bQ:Ljava/lang/String;

    if-eqz v0, :cond_53

    add-int/lit8 v0, v1, 0x1

    const-string v2, "description"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/ConfigServices;->bQ:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_53
    iget-object v0, p1, Lflipboard/objs/ConfigServices;->bR:Ljava/lang/String;

    if-eqz v0, :cond_54

    add-int/lit8 v0, v1, 0x1

    const-string v2, "icon"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/ConfigServices;->bR:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_54
    iget-boolean v0, p1, Lflipboard/objs/ConfigServices;->bS:Z

    if-eqz v0, :cond_55

    add-int/lit8 v0, v1, 0x1

    const-string v2, "isFavicon"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v1, p1, Lflipboard/objs/ConfigServices;->bS:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v1, v0

    :cond_55
    iget v0, p1, Lflipboard/objs/ConfigServices;->a:I

    if-eqz v0, :cond_56

    add-int/lit8 v0, v1, 0x1

    const-string v2, "version"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/ConfigServices;->a:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_56
    iget-object v0, p1, Lflipboard/objs/ConfigServices;->b:Ljava/util/List;

    if-eqz v0, :cond_57

    add-int/lit8 v0, v1, 0x1

    const-string v2, "services"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/ConfigServices;->b:Ljava/util/List;

    invoke-direct {p0, v1}, Lflipboard/json/JSONSerializer;->h(Ljava/util/List;)V

    move v1, v0

    :cond_57
    iget-object v0, p1, Lflipboard/objs/ConfigServices;->c:Ljava/util/List;

    if-eqz v0, :cond_58

    const-string v0, "readLaterServices"

    invoke-virtual {p0, v1, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/ConfigServices;->c:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->h(Ljava/util/List;)V

    :cond_58
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    .line 134
    :cond_59
    instance-of v0, p1, Lflipboard/objs/CrashInfo;

    if-eqz v0, :cond_5f

    .line 135
    check-cast p1, Lflipboard/objs/CrashInfo;

    if-nez p1, :cond_5a

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_5a
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-object v0, p1, Lflipboard/objs/CrashInfo;->a:Ljava/lang/String;

    if-eqz v0, :cond_10d

    const-string v0, "lastSectionId"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/CrashInfo;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    :goto_14
    iget-object v0, p1, Lflipboard/objs/CrashInfo;->b:Ljava/lang/String;

    if-eqz v0, :cond_5b

    add-int/lit8 v0, v1, 0x1

    const-string v2, "lastItemId"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/CrashInfo;->b:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_5b
    iget-object v0, p1, Lflipboard/objs/CrashInfo;->c:Ljava/lang/String;

    if-eqz v0, :cond_5c

    add-int/lit8 v0, v1, 0x1

    const-string v2, "lastItemSourceUrl"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/CrashInfo;->c:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_5c
    iget-object v0, p1, Lflipboard/objs/CrashInfo;->d:Ljava/lang/String;

    if-eqz v0, :cond_5d

    add-int/lit8 v0, v1, 0x1

    const-string v2, "lastItemSectionId"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/CrashInfo;->d:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_5d
    iget-object v0, p1, Lflipboard/objs/CrashInfo;->e:Ljava/lang/String;

    if-eqz v0, :cond_5e

    const-string v0, "lastEnteredScreen"

    invoke-virtual {p0, v1, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/CrashInfo;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    :cond_5e
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    .line 138
    :cond_5f
    instance-of v0, p1, Lflipboard/objs/DualBrick;

    if-eqz v0, :cond_6c

    .line 139
    check-cast p1, Lflipboard/objs/DualBrick;

    if-nez p1, :cond_60

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_60
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-object v0, p1, Lflipboard/objs/DualBrick;->a:Ljava/lang/String;

    if-eqz v0, :cond_10c

    const-string v0, "imageURL"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/DualBrick;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    :goto_15
    iget-object v0, p1, Lflipboard/objs/DualBrick;->b:Ljava/lang/String;

    if-eqz v0, :cond_61

    add-int/lit8 v0, v1, 0x1

    const-string v2, "largeURL"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/DualBrick;->b:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_61
    iget-object v0, p1, Lflipboard/objs/DualBrick;->c:Ljava/lang/String;

    if-eqz v0, :cond_62

    add-int/lit8 v0, v1, 0x1

    const-string v2, "mediumURL"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/DualBrick;->c:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_62
    iget-object v0, p1, Lflipboard/objs/DualBrick;->d:Ljava/lang/String;

    if-eqz v0, :cond_63

    add-int/lit8 v0, v1, 0x1

    const-string v2, "smallURL"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/DualBrick;->d:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_63
    iget v0, p1, Lflipboard/objs/DualBrick;->e:I

    if-eqz v0, :cond_64

    add-int/lit8 v0, v1, 0x1

    const-string v2, "width"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/DualBrick;->e:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_64
    iget v0, p1, Lflipboard/objs/DualBrick;->f:I

    if-eqz v0, :cond_65

    add-int/lit8 v0, v1, 0x1

    const-string v2, "height"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/DualBrick;->f:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_65
    iget v0, p1, Lflipboard/objs/DualBrick;->g:I

    if-eqz v0, :cond_66

    add-int/lit8 v0, v1, 0x1

    const-string v2, "perRow"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/DualBrick;->g:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_66
    iget-boolean v0, p1, Lflipboard/objs/DualBrick;->h:Z

    if-eqz v0, :cond_67

    add-int/lit8 v0, v1, 0x1

    const-string v2, "showTitle"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v1, p1, Lflipboard/objs/DualBrick;->h:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v1, v0

    :cond_67
    iget-boolean v0, p1, Lflipboard/objs/DualBrick;->i:Z

    if-eqz v0, :cond_68

    add-int/lit8 v0, v1, 0x1

    const-string v2, "showAuthor"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v1, p1, Lflipboard/objs/DualBrick;->i:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v1, v0

    :cond_68
    iget-boolean v0, p1, Lflipboard/objs/DualBrick;->j:Z

    if-eqz v0, :cond_69

    add-int/lit8 v0, v1, 0x1

    const-string v2, "showAddButton"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v1, p1, Lflipboard/objs/DualBrick;->j:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v1, v0

    :cond_69
    iget-object v0, p1, Lflipboard/objs/DualBrick;->k:Lflipboard/objs/ConfigSection;

    if-eqz v0, :cond_6a

    add-int/lit8 v0, v1, 0x1

    const-string v2, "section"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/DualBrick;->k:Lflipboard/objs/ConfigSection;

    invoke-direct {p0, v1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigSection;)V

    move v1, v0

    :cond_6a
    iget-object v0, p1, Lflipboard/objs/DualBrick;->l:Lflipboard/objs/ConfigBrick;

    if-eqz v0, :cond_6b

    const-string v0, "childBrick"

    invoke-virtual {p0, v1, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/DualBrick;->l:Lflipboard/objs/ConfigBrick;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/ConfigBrick;)V

    :cond_6b
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    .line 142
    :cond_6c
    instance-of v0, p1, Lflipboard/objs/FeedArticle;

    if-eqz v0, :cond_6d

    .line 143
    check-cast p1, Lflipboard/objs/FeedArticle;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FeedArticle;)V

    goto/16 :goto_0

    .line 146
    :cond_6d
    instance-of v0, p1, Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_6e

    .line 147
    check-cast p1, Lflipboard/objs/FeedItem;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/FeedItem;)V

    goto/16 :goto_0

    .line 150
    :cond_6e
    instance-of v0, p1, Lflipboard/objs/FeedItem$Location;

    if-eqz v0, :cond_6f

    .line 151
    check-cast p1, Lflipboard/objs/FeedItem$Location;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FeedItem$Location;)V

    goto/16 :goto_0

    .line 154
    :cond_6f
    instance-of v0, p1, Lflipboard/objs/FeedItem$Note;

    if-eqz v0, :cond_70

    .line 155
    check-cast p1, Lflipboard/objs/FeedItem$Note;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FeedItem$Note;)V

    goto/16 :goto_0

    .line 158
    :cond_70
    instance-of v0, p1, Lflipboard/objs/FeedItemRenderHints;

    if-eqz v0, :cond_71

    .line 159
    check-cast p1, Lflipboard/objs/FeedItemRenderHints;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FeedItemRenderHints;)V

    goto/16 :goto_0

    .line 162
    :cond_71
    instance-of v0, p1, Lflipboard/objs/FeedSection;

    if-eqz v0, :cond_72

    .line 163
    check-cast p1, Lflipboard/objs/FeedSection;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FeedSection;)V

    goto/16 :goto_0

    .line 166
    :cond_72
    instance-of v0, p1, Lflipboard/objs/FeedSectionLink;

    if-eqz v0, :cond_73

    .line 167
    check-cast p1, Lflipboard/objs/FeedSectionLink;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FeedSectionLink;)V

    goto/16 :goto_0

    .line 170
    :cond_73
    instance-of v0, p1, Lflipboard/objs/FlipResponse;

    if-eqz v0, :cond_82

    .line 171
    check-cast p1, Lflipboard/objs/FlipResponse;

    if-nez p1, :cond_74

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_74
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-object v0, p1, Lflipboard/objs/FlipResponse;->c:Ljava/lang/String;

    if-eqz v0, :cond_10b

    const-string v0, "type"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/FlipResponse;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    :goto_16
    iget-object v0, p1, Lflipboard/objs/FlipResponse;->a:Ljava/lang/String;

    if-eqz v0, :cond_75

    add-int/lit8 v0, v1, 0x1

    const-string v2, "url"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/FlipResponse;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_75
    iget-object v0, p1, Lflipboard/objs/FlipResponse;->b:Ljava/lang/String;

    if-eqz v0, :cond_76

    add-int/lit8 v0, v1, 0x1

    const-string v2, "domain"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/FlipResponse;->b:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_76
    iget-object v0, p1, Lflipboard/objs/FlipResponse;->d:Ljava/lang/String;

    if-eqz v0, :cond_77

    add-int/lit8 v0, v1, 0x1

    const-string v2, "display"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/FlipResponse;->d:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_77
    iget-object v0, p1, Lflipboard/objs/FlipResponse;->e:Ljava/lang/String;

    if-eqz v0, :cond_78

    add-int/lit8 v0, v1, 0x1

    const-string v2, "author"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/FlipResponse;->e:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_78
    iget-object v0, p1, Lflipboard/objs/FlipResponse;->f:Ljava/lang/String;

    if-eqz v0, :cond_79

    add-int/lit8 v0, v1, 0x1

    const-string v2, "language"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/FlipResponse;->f:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_79
    iget-object v0, p1, Lflipboard/objs/FlipResponse;->g:Ljava/lang/String;

    if-eqz v0, :cond_7a

    add-int/lit8 v0, v1, 0x1

    const-string v2, "title"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/FlipResponse;->g:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_7a
    iget-boolean v0, p1, Lflipboard/objs/FlipResponse;->h:Z

    if-eqz v0, :cond_7b

    add-int/lit8 v0, v1, 0x1

    const-string v2, "fullscreen"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v1, p1, Lflipboard/objs/FlipResponse;->h:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v1, v0

    :cond_7b
    iget-object v0, p1, Lflipboard/objs/FlipResponse;->i:Ljava/util/List;

    if-eqz v0, :cond_7c

    add-int/lit8 v0, v1, 0x1

    const-string v2, "images"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/FlipResponse;->i:Ljava/util/List;

    invoke-direct {p0, v1}, Lflipboard/json/JSONSerializer;->j(Ljava/util/List;)V

    move v1, v0

    :cond_7c
    iget-object v0, p1, Lflipboard/objs/FlipResponse;->j:Ljava/util/List;

    if-eqz v0, :cond_7d

    add-int/lit8 v0, v1, 0x1

    const-string v2, "albumArt"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/FlipResponse;->j:Ljava/util/List;

    invoke-direct {p0, v1}, Lflipboard/json/JSONSerializer;->j(Ljava/util/List;)V

    move v1, v0

    :cond_7d
    iget-object v0, p1, Lflipboard/objs/FlipResponse;->k:Lflipboard/objs/FlipResponse$DateResult;

    if-eqz v0, :cond_7e

    add-int/lit8 v0, v1, 0x1

    const-string v2, "created"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/FlipResponse;->k:Lflipboard/objs/FlipResponse$DateResult;

    invoke-direct {p0, v1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FlipResponse$DateResult;)V

    move v1, v0

    :cond_7e
    iget-object v0, p1, Lflipboard/objs/FlipResponse;->l:Lflipboard/objs/FlipResponse$DateResult;

    if-eqz v0, :cond_7f

    add-int/lit8 v0, v1, 0x1

    const-string v2, "expires"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/FlipResponse;->l:Lflipboard/objs/FlipResponse$DateResult;

    invoke-direct {p0, v1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FlipResponse$DateResult;)V

    move v1, v0

    :cond_7f
    iget-object v0, p1, Lflipboard/objs/FlipResponse;->m:Lflipboard/objs/FlipResponse$DateResult;

    if-eqz v0, :cond_80

    add-int/lit8 v0, v1, 0x1

    const-string v2, "itemcreated"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/FlipResponse;->m:Lflipboard/objs/FlipResponse$DateResult;

    invoke-direct {p0, v1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FlipResponse$DateResult;)V

    move v1, v0

    :cond_80
    iget-object v0, p1, Lflipboard/objs/FlipResponse;->n:Lflipboard/objs/FlipResponse$FlipStats;

    if-eqz v0, :cond_81

    const-string v0, "stats"

    invoke-virtual {p0, v1, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/FlipResponse;->n:Lflipboard/objs/FlipResponse$FlipStats;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FlipResponse$FlipStats;)V

    :cond_81
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    .line 174
    :cond_82
    instance-of v0, p1, Lflipboard/objs/FlipResponse$DateResult;

    if-eqz v0, :cond_83

    .line 175
    check-cast p1, Lflipboard/objs/FlipResponse$DateResult;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FlipResponse$DateResult;)V

    goto/16 :goto_0

    .line 178
    :cond_83
    instance-of v0, p1, Lflipboard/objs/FlipResponse$FlipImage;

    if-eqz v0, :cond_84

    .line 179
    check-cast p1, Lflipboard/objs/FlipResponse$FlipImage;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FlipResponse$FlipImage;)V

    goto/16 :goto_0

    .line 182
    :cond_84
    instance-of v0, p1, Lflipboard/objs/FlipResponse$FlipImagesResult;

    if-eqz v0, :cond_85

    .line 183
    check-cast p1, Lflipboard/objs/FlipResponse$FlipImagesResult;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FlipResponse$FlipImagesResult;)V

    goto/16 :goto_0

    .line 186
    :cond_85
    instance-of v0, p1, Lflipboard/objs/FlipResponse$FlipStats;

    if-eqz v0, :cond_86

    .line 187
    check-cast p1, Lflipboard/objs/FlipResponse$FlipStats;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/FlipResponse$FlipStats;)V

    goto/16 :goto_0

    .line 190
    :cond_86
    instance-of v0, p1, Lflipboard/objs/Image;

    if-eqz v0, :cond_87

    .line 191
    check-cast p1, Lflipboard/objs/Image;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Image;)V

    goto/16 :goto_0

    .line 194
    :cond_87
    instance-of v0, p1, Lflipboard/objs/Invite;

    if-eqz v0, :cond_88

    .line 195
    check-cast p1, Lflipboard/objs/Invite;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/Invite;)V

    goto/16 :goto_0

    .line 198
    :cond_88
    instance-of v0, p1, Lflipboard/objs/LightBoxes;

    if-eqz v0, :cond_8e

    .line 199
    check-cast p1, Lflipboard/objs/LightBoxes;

    if-nez p1, :cond_89

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_89
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-object v0, p1, Lflipboard/objs/LightBoxes;->a:Ljava/util/Map;

    if-eqz v0, :cond_8a

    const-string v0, "lightboxes"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/LightBoxes;->a:Ljava/util/Map;

    if-nez v0, :cond_8b

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    :cond_8a
    :goto_17
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_8b
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_18
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    add-int/lit8 v3, v2, 0x1

    if-lez v2, :cond_8c

    invoke-virtual {p0, v9}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_8c
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    const/16 v1, 0x3a

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/LightBoxes$LightBox;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/LightBoxes$LightBox;)V

    move v2, v3

    goto :goto_18

    :cond_8d
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_17

    .line 202
    :cond_8e
    instance-of v0, p1, Lflipboard/objs/LightBoxes$Device;

    if-eqz v0, :cond_8f

    .line 203
    check-cast p1, Lflipboard/objs/LightBoxes$Device;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/LightBoxes$Device;)V

    goto/16 :goto_0

    .line 206
    :cond_8f
    instance-of v0, p1, Lflipboard/objs/LightBoxes$LightBox;

    if-eqz v0, :cond_90

    .line 207
    check-cast p1, Lflipboard/objs/LightBoxes$LightBox;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/LightBoxes$LightBox;)V

    goto/16 :goto_0

    .line 210
    :cond_90
    instance-of v0, p1, Lflipboard/objs/LightBoxes$Page;

    if-eqz v0, :cond_91

    .line 211
    check-cast p1, Lflipboard/objs/LightBoxes$Page;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/LightBoxes$Page;)V

    goto/16 :goto_0

    .line 214
    :cond_91
    instance-of v0, p1, Lflipboard/objs/Link;

    if-eqz v0, :cond_92

    .line 215
    check-cast p1, Lflipboard/objs/Link;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/Link;)V

    goto/16 :goto_0

    .line 218
    :cond_92
    instance-of v0, p1, Lflipboard/objs/Magazine;

    if-eqz v0, :cond_93

    .line 219
    check-cast p1, Lflipboard/objs/Magazine;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/Magazine;)V

    goto/16 :goto_0

    .line 222
    :cond_93
    instance-of v0, p1, Lflipboard/objs/NonStreaming;

    if-eqz v0, :cond_96

    .line 223
    check-cast p1, Lflipboard/objs/NonStreaming;

    if-nez p1, :cond_94

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_94
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-object v0, p1, Lflipboard/objs/NonStreaming;->a:Ljava/util/List;

    if-eqz v0, :cond_95

    const-string v0, "stream"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/NonStreaming;->a:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->c(Ljava/util/List;)V

    :cond_95
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    .line 226
    :cond_96
    instance-of v0, p1, Lflipboard/objs/SearchResult;

    if-eqz v0, :cond_b5

    .line 227
    check-cast p1, Lflipboard/objs/SearchResult;

    if-nez p1, :cond_97

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_97
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-object v0, p1, Lflipboard/objs/SearchResult;->h:Lflipboard/service/FLSearchManager$ResultType;

    if-eqz v0, :cond_10a

    const-string v0, "type"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/SearchResult;->h:Lflipboard/service/FLSearchManager$ResultType;

    invoke-virtual {v0}, Lflipboard/service/FLSearchManager$ResultType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    :goto_19
    iget-object v0, p1, Lflipboard/objs/SearchResult;->bP:Ljava/lang/String;

    if-eqz v0, :cond_98

    add-int/lit8 v0, v1, 0x1

    const-string v2, "title"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SearchResult;->bP:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_98
    iget-object v0, p1, Lflipboard/objs/SearchResult;->bQ:Ljava/lang/String;

    if-eqz v0, :cond_99

    add-int/lit8 v0, v1, 0x1

    const-string v2, "description"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SearchResult;->bQ:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_99
    iget-object v0, p1, Lflipboard/objs/SearchResult;->bR:Ljava/lang/String;

    if-eqz v0, :cond_9a

    add-int/lit8 v0, v1, 0x1

    const-string v2, "icon"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SearchResult;->bR:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_9a
    iget-boolean v0, p1, Lflipboard/objs/SearchResult;->bS:Z

    if-eqz v0, :cond_9b

    add-int/lit8 v0, v1, 0x1

    const-string v2, "isFavicon"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v1, p1, Lflipboard/objs/SearchResult;->bS:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v1, v0

    :cond_9b
    iget-object v0, p1, Lflipboard/objs/SearchResult;->o:Ljava/lang/String;

    if-eqz v0, :cond_9c

    add-int/lit8 v0, v1, 0x1

    const-string v2, "service"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SearchResult;->o:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_9c
    iget-object v0, p1, Lflipboard/objs/SearchResult;->p:Ljava/lang/Object;

    if-eqz v0, :cond_9d

    add-int/lit8 v0, v1, 0x1

    const-string v2, "remoteid"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SearchResult;->p:Ljava/lang/Object;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->b(Ljava/lang/Object;)V

    move v1, v0

    :cond_9d
    iget-object v0, p1, Lflipboard/objs/SearchResult;->q:Ljava/lang/String;

    if-eqz v0, :cond_9e

    add-int/lit8 v0, v1, 0x1

    const-string v2, "titleSuffix"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SearchResult;->q:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_9e
    iget-object v0, p1, Lflipboard/objs/SearchResult;->r:Ljava/lang/String;

    if-eqz v0, :cond_9f

    add-int/lit8 v0, v1, 0x1

    const-string v2, "sectionTitle"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SearchResult;->r:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_9f
    iget-object v0, p1, Lflipboard/objs/SearchResult;->s:Ljava/lang/String;

    if-eqz v0, :cond_a0

    add-int/lit8 v0, v1, 0x1

    const-string v2, "unreadRemoteid"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SearchResult;->s:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_a0
    iget-object v0, p1, Lflipboard/objs/SearchResult;->t:Ljava/lang/String;

    if-eqz v0, :cond_a1

    add-int/lit8 v0, v1, 0x1

    const-string v2, "imageURL"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SearchResult;->t:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_a1
    iget-boolean v0, p1, Lflipboard/objs/SearchResult;->u:Z

    if-eqz v0, :cond_a2

    add-int/lit8 v0, v1, 0x1

    const-string v2, "verified"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v1, p1, Lflipboard/objs/SearchResult;->u:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v1, v0

    :cond_a2
    iget v0, p1, Lflipboard/objs/SearchResult;->v:I

    if-eqz v0, :cond_a3

    add-int/lit8 v0, v1, 0x1

    const-string v2, "unreadCount"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/SearchResult;->v:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_a3
    iget-boolean v0, p1, Lflipboard/objs/SearchResult;->w:Z

    if-eqz v0, :cond_a4

    add-int/lit8 v0, v1, 0x1

    const-string v2, "showLogotypeImage"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v1, p1, Lflipboard/objs/SearchResult;->w:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v1, v0

    :cond_a4
    iget-boolean v0, p1, Lflipboard/objs/SearchResult;->x:Z

    if-eqz v0, :cond_a5

    add-int/lit8 v0, v1, 0x1

    const-string v2, "enumerated"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v1, p1, Lflipboard/objs/SearchResult;->x:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v1, v0

    :cond_a5
    iget-boolean v0, p1, Lflipboard/objs/SearchResult;->y:Z

    if-eqz v0, :cond_a6

    add-int/lit8 v0, v1, 0x1

    const-string v2, "private"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v1, p1, Lflipboard/objs/SearchResult;->y:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v1, v0

    :cond_a6
    iget-object v0, p1, Lflipboard/objs/SearchResult;->c:Ljava/lang/String;

    if-eqz v0, :cond_a7

    add-int/lit8 v0, v1, 0x1

    const-string v2, "categoryList"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SearchResult;->c:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_a7
    iget v0, p1, Lflipboard/objs/SearchResult;->d:F

    float-to-double v2, v0

    const-wide/16 v4, 0x0

    cmpl-double v0, v2, v4

    if-eqz v0, :cond_a8

    add-int/lit8 v0, v1, 0x1

    const-string v2, "categoryListWeight"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/SearchResult;->d:F

    float-to-double v2, v1

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v1, v0

    :cond_a8
    iget-object v0, p1, Lflipboard/objs/SearchResult;->e:Ljava/lang/String;

    if-eqz v0, :cond_a9

    add-int/lit8 v0, v1, 0x1

    const-string v2, "version"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SearchResult;->e:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_a9
    iget-wide v2, p1, Lflipboard/objs/SearchResult;->f:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_aa

    add-int/lit8 v0, v1, 0x1

    const-string v2, "time"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-wide v2, p1, Lflipboard/objs/SearchResult;->f:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    move v1, v0

    :cond_aa
    iget-wide v2, p1, Lflipboard/objs/SearchResult;->g:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_ab

    add-int/lit8 v0, v1, 0x1

    const-string v2, "count"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-wide v2, p1, Lflipboard/objs/SearchResult;->g:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    move v1, v0

    :cond_ab
    iget-object v0, p1, Lflipboard/objs/SearchResult;->i:Ljava/lang/String;

    if-eqz v0, :cond_ac

    add-int/lit8 v0, v1, 0x1

    const-string v2, "category"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SearchResult;->i:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_ac
    iget-object v0, p1, Lflipboard/objs/SearchResult;->j:Ljava/lang/String;

    if-eqz v0, :cond_ad

    add-int/lit8 v0, v1, 0x1

    const-string v2, "categoryTitle"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SearchResult;->j:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_ad
    iget v0, p1, Lflipboard/objs/SearchResult;->k:F

    float-to-double v2, v0

    const-wide/16 v4, 0x0

    cmpl-double v0, v2, v4

    if-eqz v0, :cond_ae

    add-int/lit8 v0, v1, 0x1

    const-string v2, "categoryWeight"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/SearchResult;->k:F

    float-to-double v2, v1

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v1, v0

    :cond_ae
    iget v0, p1, Lflipboard/objs/SearchResult;->l:F

    float-to-double v2, v0

    const-wide/16 v4, 0x0

    cmpl-double v0, v2, v4

    if-eqz v0, :cond_af

    add-int/lit8 v0, v1, 0x1

    const-string v2, "weight"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/SearchResult;->l:F

    float-to-double v2, v1

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v1, v0

    :cond_af
    iget-boolean v0, p1, Lflipboard/objs/SearchResult;->m:Z

    if-eqz v0, :cond_b0

    add-int/lit8 v0, v1, 0x1

    const-string v2, "flipster"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v1, p1, Lflipboard/objs/SearchResult;->m:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v1, v0

    :cond_b0
    iget-object v0, p1, Lflipboard/objs/SearchResult;->n:Ljava/lang/String;

    if-eqz v0, :cond_b1

    add-int/lit8 v0, v1, 0x1

    const-string v2, "feedType"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SearchResult;->n:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_b1
    iget-object v0, p1, Lflipboard/objs/SearchResult;->z:Ljava/lang/String;

    if-eqz v0, :cond_b2

    add-int/lit8 v0, v1, 0x1

    const-string v2, "userIdentifier"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SearchResult;->z:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_b2
    iget-boolean v0, p1, Lflipboard/objs/SearchResult;->A:Z

    if-eqz v0, :cond_b3

    add-int/lit8 v0, v1, 0x1

    const-string v2, "sponsored"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v1, p1, Lflipboard/objs/SearchResult;->A:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v1, v0

    :cond_b3
    iget-object v0, p1, Lflipboard/objs/SearchResult;->B:Ljava/lang/String;

    if-eqz v0, :cond_b4

    const-string v0, "moreResult"

    invoke-virtual {p0, v1, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/SearchResult;->B:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    :cond_b4
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    .line 230
    :cond_b5
    instance-of v0, p1, Lflipboard/objs/SearchResultCategory;

    if-eqz v0, :cond_b6

    .line 231
    check-cast p1, Lflipboard/objs/SearchResultCategory;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/SearchResultCategory;)V

    goto/16 :goto_0

    .line 234
    :cond_b6
    instance-of v0, p1, Lflipboard/objs/SearchResultItem;

    if-eqz v0, :cond_b7

    .line 235
    check-cast p1, Lflipboard/objs/SearchResultItem;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/SearchResultItem;)V

    goto/16 :goto_0

    .line 238
    :cond_b7
    instance-of v0, p1, Lflipboard/objs/SearchResultStream;

    if-eqz v0, :cond_be

    .line 239
    check-cast p1, Lflipboard/objs/SearchResultStream;

    if-nez p1, :cond_b8

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_b8
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-object v0, p1, Lflipboard/objs/SearchResultStream;->a:Ljava/util/List;

    if-eqz v0, :cond_109

    const-string v0, "stream"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/SearchResultStream;->a:Ljava/util/List;

    if-nez v0, :cond_bb

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    :goto_1a
    iget v0, p1, Lflipboard/objs/SearchResultStream;->b:I

    if-eqz v0, :cond_b9

    add-int/lit8 v0, v1, 0x1

    const-string v2, "code"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/SearchResultStream;->b:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_b9
    iget-wide v2, p1, Lflipboard/objs/SearchResultStream;->c:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_ba

    const-string v0, "time"

    invoke-virtual {p0, v1, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-wide v0, p1, Lflipboard/objs/SearchResultStream;->c:J

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializer;->a(J)V

    :cond_ba
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    :cond_bb
    const/16 v3, 0x5b

    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_bd

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultCategory;

    add-int/lit8 v3, v2, 0x1

    if-lez v2, :cond_bc

    invoke-virtual {p0, v9}, Lflipboard/json/JSONSerializer;->a(C)V

    :cond_bc
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/SearchResultCategory;)V

    move v2, v3

    goto :goto_1b

    :cond_bd
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_1a

    .line 242
    :cond_be
    instance-of v0, p1, Lflipboard/objs/SectionListItem;

    if-eqz v0, :cond_bf

    .line 243
    check-cast p1, Lflipboard/objs/SectionListItem;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/SectionListItem;)V

    goto/16 :goto_0

    .line 246
    :cond_bf
    instance-of v0, p1, Lflipboard/objs/SectionListResult;

    if-eqz v0, :cond_ca

    .line 247
    check-cast p1, Lflipboard/objs/SectionListResult;

    if-nez p1, :cond_c0

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_c0
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-boolean v0, p1, Lflipboard/objs/SectionListResult;->b:Z

    if-eqz v0, :cond_108

    const-string v0, "success"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v0, p1, Lflipboard/objs/SectionListResult;->b:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    :goto_1c
    iget v0, p1, Lflipboard/objs/SectionListResult;->c:I

    if-eqz v0, :cond_c1

    add-int/lit8 v0, v1, 0x1

    const-string v2, "code"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/SectionListResult;->c:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_c1
    iget-object v0, p1, Lflipboard/objs/SectionListResult;->d:Ljava/lang/String;

    if-eqz v0, :cond_c2

    add-int/lit8 v0, v1, 0x1

    const-string v2, "message"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SectionListResult;->d:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_c2
    iget-wide v2, p1, Lflipboard/objs/SectionListResult;->e:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_c3

    add-int/lit8 v0, v1, 0x1

    const-string v2, "time"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-wide v2, p1, Lflipboard/objs/SectionListResult;->e:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    move v1, v0

    :cond_c3
    iget v0, p1, Lflipboard/objs/SectionListResult;->f:I

    if-eqz v0, :cond_c4

    add-int/lit8 v0, v1, 0x1

    const-string v2, "errorcode"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/SectionListResult;->f:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_c4
    iget-object v0, p1, Lflipboard/objs/SectionListResult;->g:Ljava/lang/String;

    if-eqz v0, :cond_c5

    add-int/lit8 v0, v1, 0x1

    const-string v2, "errormessage"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SectionListResult;->g:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_c5
    iget-object v0, p1, Lflipboard/objs/SectionListResult;->h:Ljava/lang/String;

    if-eqz v0, :cond_c6

    add-int/lit8 v0, v1, 0x1

    const-string v2, "displaymessage"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SectionListResult;->h:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_c6
    iget-object v0, p1, Lflipboard/objs/SectionListResult;->a:Ljava/util/List;

    if-eqz v0, :cond_c7

    add-int/lit8 v0, v1, 0x1

    const-string v2, "results"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SectionListResult;->a:Ljava/util/List;

    invoke-direct {p0, v1}, Lflipboard/json/JSONSerializer;->k(Ljava/util/List;)V

    move v1, v0

    :cond_c7
    iget-object v0, p1, Lflipboard/objs/SectionListResult;->i:Ljava/util/List;

    if-eqz v0, :cond_c8

    add-int/lit8 v0, v1, 0x1

    const-string v2, "items"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SectionListResult;->i:Ljava/util/List;

    invoke-direct {p0, v1}, Lflipboard/json/JSONSerializer;->k(Ljava/util/List;)V

    move v1, v0

    :cond_c8
    iget-object v0, p1, Lflipboard/objs/SectionListResult;->j:Ljava/lang/String;

    if-eqz v0, :cond_c9

    const-string v0, "pageKey"

    invoke-virtual {p0, v1, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/SectionListResult;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    :cond_c9
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    .line 250
    :cond_ca
    instance-of v0, p1, Lflipboard/objs/SectionPageTemplate;

    if-eqz v0, :cond_d9

    .line 251
    check-cast p1, Lflipboard/objs/SectionPageTemplate;

    if-nez p1, :cond_cb

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_cb
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-object v0, p1, Lflipboard/objs/SectionPageTemplate;->a:Ljava/lang/String;

    if-eqz v0, :cond_107

    const-string v0, "name"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/SectionPageTemplate;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    :goto_1d
    iget-object v0, p1, Lflipboard/objs/SectionPageTemplate;->b:Ljava/lang/String;

    if-eqz v0, :cond_cc

    add-int/lit8 v0, v1, 0x1

    const-string v2, "description"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SectionPageTemplate;->b:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_cc
    iget-object v0, p1, Lflipboard/objs/SectionPageTemplate;->c:Ljava/util/List;

    if-eqz v0, :cond_cd

    add-int/lit8 v0, v1, 0x1

    const-string v2, "types"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SectionPageTemplate;->c:Ljava/util/List;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->d(Ljava/util/List;)V

    move v1, v0

    :cond_cd
    iget v0, p1, Lflipboard/objs/SectionPageTemplate;->d:I

    if-eqz v0, :cond_ce

    add-int/lit8 v0, v1, 0x1

    const-string v2, "minWidthDp"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/SectionPageTemplate;->d:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_ce
    iget v0, p1, Lflipboard/objs/SectionPageTemplate;->e:I

    if-eqz v0, :cond_cf

    add-int/lit8 v0, v1, 0x1

    const-string v2, "minHeightDp"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/SectionPageTemplate;->e:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_cf
    iget v0, p1, Lflipboard/objs/SectionPageTemplate;->f:I

    const v2, 0x7fffffff

    if-eq v0, v2, :cond_d0

    add-int/lit8 v0, v1, 0x1

    const-string v2, "maxWidthDp"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/SectionPageTemplate;->f:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_d0
    iget v0, p1, Lflipboard/objs/SectionPageTemplate;->g:I

    const v2, 0x7fffffff

    if-eq v0, v2, :cond_d1

    add-int/lit8 v0, v1, 0x1

    const-string v2, "maxHeightDp"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/SectionPageTemplate;->g:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_d1
    iget v0, p1, Lflipboard/objs/SectionPageTemplate;->h:F

    float-to-double v2, v0

    const-wide/16 v4, 0x0

    cmpl-double v0, v2, v4

    if-eqz v0, :cond_d2

    add-int/lit8 v0, v1, 0x1

    const-string v2, "maxFrequency"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/SectionPageTemplate;->h:F

    float-to-double v2, v1

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializerBase;->a(D)V

    move v1, v0

    :cond_d2
    iget-boolean v0, p1, Lflipboard/objs/SectionPageTemplate;->i:Z

    if-eqz v0, :cond_d3

    add-int/lit8 v0, v1, 0x1

    const-string v2, "dontUseNormally"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v1, p1, Lflipboard/objs/SectionPageTemplate;->i:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v1, v0

    :cond_d3
    iget-object v0, p1, Lflipboard/objs/SectionPageTemplate;->j:Ljava/util/List;

    if-eqz v0, :cond_d4

    add-int/lit8 v0, v1, 0x1

    const-string v2, "areas"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SectionPageTemplate;->j:Ljava/util/List;

    invoke-direct {p0, v1}, Lflipboard/json/JSONSerializer;->l(Ljava/util/List;)V

    move v1, v0

    :cond_d4
    iget-object v0, p1, Lflipboard/objs/SectionPageTemplate;->k:Ljava/util/List;

    if-eqz v0, :cond_d5

    add-int/lit8 v0, v1, 0x1

    const-string v2, "areasLandscape"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SectionPageTemplate;->k:Ljava/util/List;

    invoke-direct {p0, v1}, Lflipboard/json/JSONSerializer;->l(Ljava/util/List;)V

    move v1, v0

    :cond_d5
    iget-boolean v0, p1, Lflipboard/objs/SectionPageTemplate;->l:Z

    if-eqz v0, :cond_d6

    add-int/lit8 v0, v1, 0x1

    const-string v2, "allowedAsFirstPage"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v1, p1, Lflipboard/objs/SectionPageTemplate;->l:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v1, v0

    :cond_d6
    iget-boolean v0, p1, Lflipboard/objs/SectionPageTemplate;->m:Z

    if-eqz v0, :cond_d7

    add-int/lit8 v0, v1, 0x1

    const-string v2, "allowedAsFirstPageForPriorityOrdered"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v1, p1, Lflipboard/objs/SectionPageTemplate;->m:Z

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Z)V

    move v1, v0

    :cond_d7
    iget-boolean v0, p1, Lflipboard/objs/SectionPageTemplate;->n:Z

    if-eqz v0, :cond_d8

    const-string v0, "tweetlist"

    invoke-virtual {p0, v1, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v0, p1, Lflipboard/objs/SectionPageTemplate;->n:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    :cond_d8
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    .line 254
    :cond_d9
    instance-of v0, p1, Lflipboard/objs/SectionPageTemplate$Area;

    if-eqz v0, :cond_da

    .line 255
    check-cast p1, Lflipboard/objs/SectionPageTemplate$Area;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/SectionPageTemplate$Area;)V

    goto/16 :goto_0

    .line 258
    :cond_da
    instance-of v0, p1, Lflipboard/objs/SidebarGroup;

    if-eqz v0, :cond_db

    .line 259
    check-cast p1, Lflipboard/objs/SidebarGroup;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/SidebarGroup;)V

    goto/16 :goto_0

    .line 262
    :cond_db
    instance-of v0, p1, Lflipboard/objs/SidebarGroup$Metrics;

    if-eqz v0, :cond_dc

    .line 263
    check-cast p1, Lflipboard/objs/SidebarGroup$Metrics;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/SidebarGroup$Metrics;)V

    goto/16 :goto_0

    .line 266
    :cond_dc
    instance-of v0, p1, Lflipboard/objs/SidebarGroup$RenderHints;

    if-eqz v0, :cond_dd

    .line 267
    check-cast p1, Lflipboard/objs/SidebarGroup$RenderHints;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/SidebarGroup$RenderHints;)V

    goto/16 :goto_0

    .line 270
    :cond_dd
    instance-of v0, p1, Lflipboard/objs/SourceMagazineURLResult;

    if-eqz v0, :cond_e6

    .line 271
    check-cast p1, Lflipboard/objs/SourceMagazineURLResult;

    if-nez p1, :cond_de

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_de
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-boolean v0, p1, Lflipboard/objs/SourceMagazineURLResult;->b:Z

    if-eqz v0, :cond_106

    const-string v0, "success"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v0, p1, Lflipboard/objs/SourceMagazineURLResult;->b:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    :goto_1e
    iget v0, p1, Lflipboard/objs/SourceMagazineURLResult;->c:I

    if-eqz v0, :cond_df

    add-int/lit8 v0, v1, 0x1

    const-string v2, "code"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/SourceMagazineURLResult;->c:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_df
    iget-object v0, p1, Lflipboard/objs/SourceMagazineURLResult;->d:Ljava/lang/String;

    if-eqz v0, :cond_e0

    add-int/lit8 v0, v1, 0x1

    const-string v2, "message"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SourceMagazineURLResult;->d:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_e0
    iget-wide v2, p1, Lflipboard/objs/SourceMagazineURLResult;->e:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_e1

    add-int/lit8 v0, v1, 0x1

    const-string v2, "time"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-wide v2, p1, Lflipboard/objs/SourceMagazineURLResult;->e:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    move v1, v0

    :cond_e1
    iget v0, p1, Lflipboard/objs/SourceMagazineURLResult;->f:I

    if-eqz v0, :cond_e2

    add-int/lit8 v0, v1, 0x1

    const-string v2, "errorcode"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/SourceMagazineURLResult;->f:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_e2
    iget-object v0, p1, Lflipboard/objs/SourceMagazineURLResult;->g:Ljava/lang/String;

    if-eqz v0, :cond_e3

    add-int/lit8 v0, v1, 0x1

    const-string v2, "errormessage"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SourceMagazineURLResult;->g:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_e3
    iget-object v0, p1, Lflipboard/objs/SourceMagazineURLResult;->h:Ljava/lang/String;

    if-eqz v0, :cond_e4

    add-int/lit8 v0, v1, 0x1

    const-string v2, "displaymessage"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/SourceMagazineURLResult;->h:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_e4
    iget-object v0, p1, Lflipboard/objs/SourceMagazineURLResult;->a:Ljava/lang/String;

    if-eqz v0, :cond_e5

    const-string v0, "sourceMagazineURL"

    invoke-virtual {p0, v1, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/SourceMagazineURLResult;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    :cond_e5
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    .line 274
    :cond_e6
    instance-of v0, p1, Lflipboard/objs/TOCSection;

    if-eqz v0, :cond_e7

    .line 275
    check-cast p1, Lflipboard/objs/TOCSection;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/TOCSection;)V

    goto/16 :goto_0

    .line 278
    :cond_e7
    instance-of v0, p1, Lflipboard/objs/UsageEventV2;

    if-eqz v0, :cond_ed

    .line 279
    check-cast p1, Lflipboard/objs/UsageEventV2;

    if-nez p1, :cond_e8

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_e8
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-object v0, p1, Lflipboard/objs/UsageEventV2;->b:Lflipboard/objs/UsageEventV2$EventAction;

    if-eqz v0, :cond_105

    const-string v0, "event_action"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/UsageEventV2;->b:Lflipboard/objs/UsageEventV2$EventAction;

    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2$EventAction;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    :goto_1f
    iget-object v0, p1, Lflipboard/objs/UsageEventV2;->c:Lflipboard/objs/UsageEventV2$EventCategory;

    if-eqz v0, :cond_e9

    add-int/lit8 v0, v1, 0x1

    const-string v2, "event_category"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/UsageEventV2;->c:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-virtual {v1}, Lflipboard/objs/UsageEventV2$EventCategory;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_e9
    iget-object v0, p1, Lflipboard/objs/UsageEventV2;->d:Lflipboard/objs/UsageEventV2$ProductType;

    if-eqz v0, :cond_ea

    add-int/lit8 v0, v1, 0x1

    const-string v2, "prod_type"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/UsageEventV2;->d:Lflipboard/objs/UsageEventV2$ProductType;

    invoke-virtual {v1}, Lflipboard/objs/UsageEventV2$ProductType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_ea
    iget-object v0, p1, Lflipboard/objs/UsageEventV2;->e:Lflipboard/json/FLObject;

    if-eqz v0, :cond_eb

    add-int/lit8 v0, v1, 0x1

    const-string v2, "event_data"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/UsageEventV2;->e:Lflipboard/json/FLObject;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/json/FLObject;)V

    move v1, v0

    :cond_eb
    iget-object v0, p1, Lflipboard/objs/UsageEventV2;->f:Lflipboard/objs/UsageEventV2$Properties;

    if-eqz v0, :cond_ec

    const-string v0, "properties"

    invoke-virtual {p0, v1, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/UsageEventV2;->f:Lflipboard/objs/UsageEventV2$Properties;

    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UsageEventV2$Properties;)V

    :cond_ec
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    .line 282
    :cond_ed
    instance-of v0, p1, Lflipboard/objs/UsageEventV2$Properties;

    if-eqz v0, :cond_ee

    .line 283
    check-cast p1, Lflipboard/objs/UsageEventV2$Properties;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UsageEventV2$Properties;)V

    goto/16 :goto_0

    .line 286
    :cond_ee
    instance-of v0, p1, Lflipboard/objs/UserInfo;

    if-eqz v0, :cond_ef

    .line 287
    check-cast p1, Lflipboard/objs/UserInfo;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UserInfo;)V

    goto/16 :goto_0

    .line 290
    :cond_ef
    instance-of v0, p1, Lflipboard/objs/UserListResult;

    if-eqz v0, :cond_f9

    .line 291
    check-cast p1, Lflipboard/objs/UserListResult;

    if-nez p1, :cond_f0

    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    goto/16 :goto_0

    :cond_f0
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializer;->a(C)V

    iget-boolean v0, p1, Lflipboard/objs/UserListResult;->b:Z

    if-eqz v0, :cond_104

    const-string v0, "success"

    invoke-virtual {p0, v2, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-boolean v0, p1, Lflipboard/objs/UserListResult;->b:Z

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Z)V

    :goto_20
    iget v0, p1, Lflipboard/objs/UserListResult;->c:I

    if-eqz v0, :cond_f1

    add-int/lit8 v0, v1, 0x1

    const-string v2, "code"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/UserListResult;->c:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_f1
    iget-object v0, p1, Lflipboard/objs/UserListResult;->d:Ljava/lang/String;

    if-eqz v0, :cond_f2

    add-int/lit8 v0, v1, 0x1

    const-string v2, "message"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/UserListResult;->d:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_f2
    iget-wide v2, p1, Lflipboard/objs/UserListResult;->e:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_f3

    add-int/lit8 v0, v1, 0x1

    const-string v2, "time"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-wide v2, p1, Lflipboard/objs/UserListResult;->e:J

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONSerializer;->a(J)V

    move v1, v0

    :cond_f3
    iget v0, p1, Lflipboard/objs/UserListResult;->f:I

    if-eqz v0, :cond_f4

    add-int/lit8 v0, v1, 0x1

    const-string v2, "errorcode"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget v1, p1, Lflipboard/objs/UserListResult;->f:I

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(I)V

    move v1, v0

    :cond_f4
    iget-object v0, p1, Lflipboard/objs/UserListResult;->g:Ljava/lang/String;

    if-eqz v0, :cond_f5

    add-int/lit8 v0, v1, 0x1

    const-string v2, "errormessage"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/UserListResult;->g:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_f5
    iget-object v0, p1, Lflipboard/objs/UserListResult;->h:Ljava/lang/String;

    if-eqz v0, :cond_f6

    add-int/lit8 v0, v1, 0x1

    const-string v2, "displaymessage"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/UserListResult;->h:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    move v1, v0

    :cond_f6
    iget-object v0, p1, Lflipboard/objs/UserListResult;->a:Ljava/util/List;

    if-eqz v0, :cond_f7

    add-int/lit8 v0, v1, 0x1

    const-string v2, "items"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v1, p1, Lflipboard/objs/UserListResult;->a:Ljava/util/List;

    invoke-direct {p0, v1}, Lflipboard/json/JSONSerializer;->i(Ljava/util/List;)V

    move v1, v0

    :cond_f7
    iget-object v0, p1, Lflipboard/objs/UserListResult;->i:Ljava/lang/String;

    if-eqz v0, :cond_f8

    const-string v0, "pageKey"

    invoke-virtual {p0, v1, v0}, Lflipboard/json/JSONSerializer;->a(ILjava/lang/String;)V

    iget-object v0, p1, Lflipboard/objs/UserListResult;->i:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(Ljava/lang/String;)V

    :cond_f8
    invoke-virtual {p0, v8}, Lflipboard/json/JSONSerializer;->a(C)V

    goto/16 :goto_0

    .line 294
    :cond_f9
    instance-of v0, p1, Lflipboard/objs/UserService;

    if-eqz v0, :cond_fa

    .line 295
    check-cast p1, Lflipboard/objs/UserService;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/UserService;)V

    goto/16 :goto_0

    .line 298
    :cond_fa
    instance-of v0, p1, Lflipboard/objs/UserService$Cookie;

    if-eqz v0, :cond_fb

    .line 299
    check-cast p1, Lflipboard/objs/UserService$Cookie;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UserService$Cookie;)V

    goto/16 :goto_0

    .line 302
    :cond_fb
    instance-of v0, p1, Lflipboard/objs/UserServices;

    if-eqz v0, :cond_fc

    .line 303
    check-cast p1, Lflipboard/objs/UserServices;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UserServices;)V

    goto/16 :goto_0

    .line 306
    :cond_fc
    instance-of v0, p1, Lflipboard/objs/UserServices$StateRevisions;

    if-eqz v0, :cond_fd

    .line 307
    check-cast p1, Lflipboard/objs/UserServices$StateRevisions;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UserServices$StateRevisions;)V

    goto/16 :goto_0

    .line 310
    :cond_fd
    instance-of v0, p1, Lflipboard/objs/UserState;

    if-eqz v0, :cond_fe

    .line 311
    check-cast p1, Lflipboard/objs/UserState;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/UserState;)V

    goto/16 :goto_0

    .line 314
    :cond_fe
    instance-of v0, p1, Lflipboard/objs/UserState$Data;

    if-eqz v0, :cond_ff

    .line 315
    check-cast p1, Lflipboard/objs/UserState$Data;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UserState$Data;)V

    goto/16 :goto_0

    .line 318
    :cond_ff
    instance-of v0, p1, Lflipboard/objs/UserState$MutedAuthor;

    if-eqz v0, :cond_100

    .line 319
    check-cast p1, Lflipboard/objs/UserState$MutedAuthor;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UserState$MutedAuthor;)V

    goto/16 :goto_0

    .line 322
    :cond_100
    instance-of v0, p1, Lflipboard/objs/UserState$State;

    if-eqz v0, :cond_101

    .line 323
    check-cast p1, Lflipboard/objs/UserState$State;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->a(Lflipboard/objs/UserState$State;)V

    goto/16 :goto_0

    .line 326
    :cond_101
    instance-of v0, p1, Lflipboard/service/Account$Meta;

    if-eqz v0, :cond_102

    .line 327
    check-cast p1, Lflipboard/service/Account$Meta;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->b(Lflipboard/service/Account$Meta;)V

    goto/16 :goto_0

    .line 330
    :cond_102
    instance-of v0, p1, Lflipboard/service/Section$Meta;

    if-eqz v0, :cond_103

    .line 331
    check-cast p1, Lflipboard/service/Section$Meta;

    invoke-direct {p0, p1}, Lflipboard/json/JSONSerializer;->b(Lflipboard/service/Section$Meta;)V

    goto/16 :goto_0

    .line 334
    :cond_103
    invoke-super {p0, p1}, Lflipboard/json/JSONSerializerBase;->a(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_104
    move v1, v2

    goto/16 :goto_20

    :cond_105
    move v1, v2

    goto/16 :goto_1f

    :cond_106
    move v1, v2

    goto/16 :goto_1e

    :cond_107
    move v1, v2

    goto/16 :goto_1d

    :cond_108
    move v1, v2

    goto/16 :goto_1c

    :cond_109
    move v1, v2

    goto/16 :goto_1a

    :cond_10a
    move v1, v2

    goto/16 :goto_19

    :cond_10b
    move v1, v2

    goto/16 :goto_16

    :cond_10c
    move v1, v2

    goto/16 :goto_15

    :cond_10d
    move v1, v2

    goto/16 :goto_14

    :cond_10e
    move v1, v2

    goto/16 :goto_13

    :cond_10f
    move v1, v2

    goto/16 :goto_10

    :cond_110
    move v1, v2

    goto/16 :goto_d

    :cond_111
    move v1, v2

    goto/16 :goto_9

    :cond_112
    move v1, v2

    goto/16 :goto_6

    :cond_113
    move v1, v2

    goto/16 :goto_5

    :cond_114
    move v1, v2

    goto/16 :goto_4

    :cond_115
    move v0, v2

    goto/16 :goto_1
.end method

.method public final c(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4852
    if-nez p1, :cond_0

    .line 4853
    sget-object v0, Lflipboard/json/JSONSerializer;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a([B)V

    .line 4863
    :goto_0
    return-void

    .line 4855
    :cond_0
    const/4 v0, 0x0

    .line 4856
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 4857
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 4858
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_1

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializer;->a(C)V

    .line 4859
    :cond_1
    invoke-direct {p0, v0}, Lflipboard/json/JSONSerializer;->b(Lflipboard/objs/FeedItem;)V

    move v1, v2

    .line 4860
    goto :goto_1

    .line 4861
    :cond_2
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializer;->a(C)V

    goto :goto_0
.end method
