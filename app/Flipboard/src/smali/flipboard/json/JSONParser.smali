.class public final Lflipboard/json/JSONParser;
.super Lflipboard/json/JSONParserBase;
.source "JSONParser.java"


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 16
    invoke-static {p1}, Lflipboard/json/JSONParser;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/json/JSONParser;-><init>([B)V

    .line 17
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 20
    const-string v0, "utf-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/json/JSONParser;-><init>([B)V

    .line 21
    return-void
.end method

.method public constructor <init>([B)V
    .locals 2

    .prologue
    .line 24
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lflipboard/json/JSONParser;-><init>([BII)V

    .line 25
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lflipboard/json/JSONParserBase;-><init>([BII)V

    .line 29
    return-void
.end method

.method private T()Lflipboard/objs/Ad$Asset;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 482
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 483
    new-instance v2, Lflipboard/objs/Ad$Asset;

    invoke-direct {v2}, Lflipboard/objs/Ad$Asset;-><init>()V

    .line 485
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 541
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 487
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 526
    const-string v0, "Ad$Asset"

    const-string v3, ""

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 489
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    .line 503
    const-string v0, "Ad$Asset"

    const-string v3, "h"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 491
    :sswitch_2
    const-string v0, "Ad$Asset"

    const-string v3, "height"

    invoke-virtual {p0, v0, v3, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 492
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v2, Lflipboard/objs/Ad$Asset;->b:I

    goto :goto_0

    .line 497
    :sswitch_3
    const-string v0, "Ad$Asset"

    const-string v3, "hot_spots"

    invoke-virtual {p0, v0, v3, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    iget v3, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v3, :sswitch_data_3

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_4
    invoke-direct {p0}, Lflipboard/json/JSONParser;->W()Lflipboard/objs/Ad$HotSpot;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v2, Lflipboard/objs/Ad$Asset;->e:Ljava/util/List;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_2

    .line 508
    :sswitch_7
    const-string v0, "Ad$Asset"

    const-string v3, "safe_zone"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 509
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v2, Lflipboard/objs/Ad$Asset;->d:I

    goto :goto_0

    .line 514
    :sswitch_8
    const-string v0, "Ad$Asset"

    const-string v3, "url"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 515
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/Ad$Asset;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 520
    :sswitch_9
    const-string v0, "Ad$Asset"

    const-string v3, "width"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v2, Lflipboard/objs/Ad$Asset;->c:I

    goto/16 :goto_0

    .line 531
    :sswitch_a
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v1, v2

    .line 545
    :cond_2
    return-object v1

    .line 538
    :sswitch_b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 485
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_b
        0xa -> :sswitch_b
        0xd -> :sswitch_b
        0x20 -> :sswitch_b
        0x22 -> :sswitch_0
        0x2c -> :sswitch_b
        0x7d -> :sswitch_a
    .end sparse-switch

    .line 487
    :sswitch_data_1
    .sparse-switch
        0x68 -> :sswitch_1
        0x73 -> :sswitch_7
        0x75 -> :sswitch_8
        0x77 -> :sswitch_9
    .end sparse-switch

    .line 489
    :sswitch_data_2
    .sparse-switch
        0x65 -> :sswitch_2
        0x6f -> :sswitch_3
    .end sparse-switch

    .line 498
    :sswitch_data_3
    .sparse-switch
        0x9 -> :sswitch_6
        0xa -> :sswitch_6
        0xd -> :sswitch_6
        0x20 -> :sswitch_6
        0x2c -> :sswitch_6
        0x5d -> :sswitch_5
        0x6e -> :sswitch_4
        0x7b -> :sswitch_4
    .end sparse-switch
.end method

.method private U()Lflipboard/objs/Ad$Button;
    .locals 6

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 648
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 649
    new-instance v0, Lflipboard/objs/Ad$Button;

    invoke-direct {v0}, Lflipboard/objs/Ad$Button;-><init>()V

    .line 651
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 722
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 653
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 707
    const-string v1, "Ad$Button"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 655
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 684
    :pswitch_0
    const-string v1, "Ad$Button"

    const-string v2, "c"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 657
    :pswitch_1
    const-string v1, "Ad$Button"

    const-string v2, "click_"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 658
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 672
    const-string v1, "Ad$Button"

    const-string v2, "click_"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 660
    :pswitch_2
    const-string v1, "Ad$Button"

    const-string v2, "click_url"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 661
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Ad$Button;->a:Ljava/lang/String;

    goto :goto_0

    .line 666
    :pswitch_3
    const-string v1, "Ad$Button"

    const-string v2, "click_value"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 667
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Ad$Button;->b:Ljava/lang/String;

    goto :goto_0

    .line 678
    :pswitch_4
    const-string v1, "Ad$Button"

    const-string v2, "color"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 679
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Ad$Button;->c:Ljava/lang/String;

    goto :goto_0

    .line 689
    :sswitch_2
    const-string v1, "Ad$Button"

    const-string v2, "icon_url"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 690
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Ad$Button;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 695
    :sswitch_3
    const-string v1, "Ad$Button"

    const-string v2, "text"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 696
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Ad$Button;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 701
    :sswitch_4
    const-string v1, "Ad$Button"

    const-string v2, "video_supported"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 702
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/Ad$Button;->f:Z

    goto/16 :goto_0

    .line 712
    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 726
    :goto_1
    return-object v0

    .line 719
    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 726
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 651
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_6
        0xa -> :sswitch_6
        0xd -> :sswitch_6
        0x20 -> :sswitch_6
        0x22 -> :sswitch_0
        0x2c -> :sswitch_6
        0x7d -> :sswitch_5
    .end sparse-switch

    .line 653
    :sswitch_data_1
    .sparse-switch
        0x63 -> :sswitch_1
        0x69 -> :sswitch_2
        0x74 -> :sswitch_3
        0x76 -> :sswitch_4
    .end sparse-switch

    .line 655
    :pswitch_data_0
    .packed-switch 0x6c
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 658
    :pswitch_data_1
    .packed-switch 0x75
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private V()Lflipboard/objs/Ad$ButtonInfo;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 829
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 830
    new-instance v2, Lflipboard/objs/Ad$ButtonInfo;

    invoke-direct {v2}, Lflipboard/objs/Ad$ButtonInfo;-><init>()V

    .line 832
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 863
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 834
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 848
    :pswitch_0
    const-string v0, "Ad$ButtonInfo"

    const-string v3, ""

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 836
    :pswitch_1
    const-string v0, "Ad$ButtonInfo"

    const-string v3, "buttons"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 837
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    iget v3, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v3, :sswitch_data_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_1
    invoke-direct {p0}, Lflipboard/json/JSONParser;->U()Lflipboard/objs/Ad$Button;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v2, Lflipboard/objs/Ad$ButtonInfo;->a:Ljava/util/List;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_2

    .line 842
    :pswitch_2
    const-string v0, "Ad$ButtonInfo"

    const-string v3, "disable_gradient"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 843
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v0

    iput-boolean v0, v2, Lflipboard/objs/Ad$ButtonInfo;->b:Z

    goto :goto_0

    .line 853
    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v1, v2

    .line 867
    :cond_2
    return-object v1

    .line 860
    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 832
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_5
        0xa -> :sswitch_5
        0xd -> :sswitch_5
        0x20 -> :sswitch_5
        0x22 -> :sswitch_0
        0x2c -> :sswitch_5
        0x7d -> :sswitch_4
    .end sparse-switch

    .line 834
    :pswitch_data_0
    .packed-switch 0x62
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 837
    :sswitch_data_1
    .sparse-switch
        0x9 -> :sswitch_3
        0xa -> :sswitch_3
        0xd -> :sswitch_3
        0x20 -> :sswitch_3
        0x2c -> :sswitch_3
        0x5d -> :sswitch_2
        0x6e -> :sswitch_1
        0x7b -> :sswitch_1
    .end sparse-switch
.end method

.method private W()Lflipboard/objs/Ad$HotSpot;
    .locals 6

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x1

    .line 970
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 971
    new-instance v0, Lflipboard/objs/Ad$HotSpot;

    invoke-direct {v0}, Lflipboard/objs/Ad$HotSpot;-><init>()V

    .line 973
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 1064
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 975
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 1049
    const-string v1, "Ad$HotSpot"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 977
    :sswitch_1
    const-string v1, "Ad$HotSpot"

    const-string v2, "click_"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 978
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1007
    const-string v1, "Ad$HotSpot"

    const-string v2, "click_"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 980
    :pswitch_0
    const-string v1, "Ad$HotSpot"

    const-string v2, "click_url"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 981
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 995
    const-string v1, "Ad$HotSpot"

    const-string v2, "click_url"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 983
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 984
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Ad$HotSpot;->f:Ljava/lang/String;

    goto :goto_0

    .line 989
    :sswitch_3
    const-string v1, "Ad$HotSpot"

    const-string v2, "click_url_browser_safe"

    const/16 v3, 0xa

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 990
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Ad$HotSpot;->h:Ljava/lang/String;

    goto :goto_0

    .line 1001
    :pswitch_1
    const-string v1, "Ad$HotSpot"

    const-string v2, "click_value"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1002
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Ad$HotSpot;->g:Ljava/lang/String;

    goto :goto_0

    .line 1013
    :sswitch_4
    const-string v1, "Ad$HotSpot"

    const-string v2, "height"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1014
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/Ad$HotSpot;->e:I

    goto/16 :goto_0

    .line 1019
    :sswitch_5
    const-string v1, "Ad$HotSpot"

    const-string v2, "id"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1020
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Ad$HotSpot;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 1025
    :sswitch_6
    const-string v1, "Ad$HotSpot"

    const-string v2, "video_supported"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1026
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/Ad$HotSpot;->i:Z

    goto/16 :goto_0

    .line 1031
    :sswitch_7
    const-string v1, "Ad$HotSpot"

    const-string v2, "width"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1032
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/Ad$HotSpot;->d:I

    goto/16 :goto_0

    .line 1037
    :sswitch_8
    const-string v1, "Ad$HotSpot"

    const-string v2, "x"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1038
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/Ad$HotSpot;->b:I

    goto/16 :goto_0

    .line 1043
    :sswitch_9
    const-string v1, "Ad$HotSpot"

    const-string v2, "y"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1044
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/Ad$HotSpot;->c:I

    goto/16 :goto_0

    .line 1054
    :sswitch_a
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 1068
    :goto_1
    return-object v0

    .line 1061
    :sswitch_b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 1068
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 973
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_b
        0xa -> :sswitch_b
        0xd -> :sswitch_b
        0x20 -> :sswitch_b
        0x22 -> :sswitch_0
        0x2c -> :sswitch_b
        0x7d -> :sswitch_a
    .end sparse-switch

    .line 975
    :sswitch_data_1
    .sparse-switch
        0x63 -> :sswitch_1
        0x68 -> :sswitch_4
        0x69 -> :sswitch_5
        0x76 -> :sswitch_6
        0x77 -> :sswitch_7
        0x78 -> :sswitch_8
        0x79 -> :sswitch_9
    .end sparse-switch

    .line 978
    :pswitch_data_0
    .packed-switch 0x75
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 981
    :sswitch_data_2
    .sparse-switch
        0x22 -> :sswitch_2
        0x5f -> :sswitch_3
    .end sparse-switch
.end method

.method private X()Lflipboard/objs/Ad$VideoInfo;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0xa

    const/4 v7, 0x1

    const/16 v6, 0x12

    .line 1347
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1348
    new-instance v2, Lflipboard/objs/Ad$VideoInfo;

    invoke-direct {v2}, Lflipboard/objs/Ad$VideoInfo;-><init>()V

    .line 1350
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 1381
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 1352
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 1366
    const-string v0, "Ad$VideoInfo"

    const-string v3, ""

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1354
    :sswitch_1
    const-string v0, "Ad$VideoInfo"

    const-string v3, "metric_values"

    invoke-virtual {p0, v0, v3, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1355
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lflipboard/objs/Ad$MetricValues;

    invoke-direct {v0}, Lflipboard/objs/Ad$MetricValues;-><init>()V

    :cond_1
    :goto_1
    iget v3, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v3, :sswitch_data_2

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_2
    const-string v3, "Ad$MetricValues"

    const-string v4, "playback_"

    const/4 v5, 0x0

    invoke-virtual {p0, v3, v4, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v3

    sparse-switch v3, :sswitch_data_3

    const-string v3, "Ad$MetricValues"

    const-string v4, "playback_"

    invoke-virtual {p0, v3, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :sswitch_3
    const-string v3, "Ad$MetricValues"

    const-string v4, "playback_duration"

    invoke-virtual {p0, v3, v4, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lflipboard/objs/Ad$MetricValues;->f:Ljava/lang/String;

    goto :goto_1

    :sswitch_4
    const-string v3, "Ad$MetricValues"

    const-string v4, "playback_percent_"

    invoke-virtual {p0, v3, v4, v8}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    const-string v3, "Ad$MetricValues"

    const-string v4, "playback_percent_"

    invoke-virtual {p0, v3, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_1
    const-string v3, "Ad$MetricValues"

    const-string v4, "playback_percent_0"

    invoke-virtual {p0, v3, v4, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lflipboard/objs/Ad$MetricValues;->a:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    const-string v3, "Ad$MetricValues"

    const-string v4, "playback_percent_100"

    invoke-virtual {p0, v3, v4, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lflipboard/objs/Ad$MetricValues;->e:Ljava/lang/String;

    goto :goto_1

    :pswitch_3
    const-string v3, "Ad$MetricValues"

    const-string v4, "playback_percent_25"

    invoke-virtual {p0, v3, v4, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lflipboard/objs/Ad$MetricValues;->b:Ljava/lang/String;

    goto :goto_1

    :pswitch_4
    const-string v3, "Ad$MetricValues"

    const-string v4, "playback_percent_50"

    invoke-virtual {p0, v3, v4, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lflipboard/objs/Ad$MetricValues;->c:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_5
    const-string v3, "Ad$MetricValues"

    const-string v4, "playback_percent_75"

    invoke-virtual {p0, v3, v4, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lflipboard/objs/Ad$MetricValues;->d:Ljava/lang/String;

    goto/16 :goto_1

    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v2, Lflipboard/objs/Ad$VideoInfo;->b:Lflipboard/objs/Ad$MetricValues;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_2

    .line 1360
    :sswitch_7
    const-string v0, "Ad$VideoInfo"

    const-string v3, "url"

    invoke-virtual {p0, v0, v3, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1361
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/Ad$VideoInfo;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 1371
    :sswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v1, v2

    .line 1385
    :cond_3
    return-object v1

    .line 1378
    :sswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 1350
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_9
        0xa -> :sswitch_9
        0xd -> :sswitch_9
        0x20 -> :sswitch_9
        0x22 -> :sswitch_0
        0x2c -> :sswitch_9
        0x7d -> :sswitch_8
    .end sparse-switch

    .line 1352
    :sswitch_data_1
    .sparse-switch
        0x6d -> :sswitch_1
        0x75 -> :sswitch_7
    .end sparse-switch

    .line 1355
    :sswitch_data_2
    .sparse-switch
        0x9 -> :sswitch_6
        0xa -> :sswitch_6
        0xd -> :sswitch_6
        0x20 -> :sswitch_6
        0x22 -> :sswitch_2
        0x2c -> :sswitch_6
        0x7d -> :sswitch_5
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x64 -> :sswitch_3
        0x70 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method private Y()Lflipboard/objs/Author;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/16 v4, 0x8

    .line 1674
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1675
    new-instance v0, Lflipboard/objs/Author;

    invoke-direct {v0}, Lflipboard/objs/Author;-><init>()V

    .line 1677
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 1761
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 1679
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 1746
    const-string v1, "Author"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1681
    :sswitch_1
    const-string v1, "Author"

    const-string v2, "author"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1682
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 1728
    const-string v1, "Author"

    const-string v2, "author"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1684
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    .line 1698
    const-string v1, "Author"

    const-string v2, "authorD"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1686
    :sswitch_3
    const-string v1, "Author"

    const-string v2, "authorDescription"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1687
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Author;->f:Ljava/lang/String;

    goto :goto_0

    .line 1692
    :sswitch_4
    const-string v1, "Author"

    const-string v2, "authorDisplayName"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1693
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Author;->c:Ljava/lang/String;

    goto :goto_0

    .line 1703
    :sswitch_5
    const-string v1, "Author"

    const-string v2, "authorImage"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1704
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Author;->g:Lflipboard/objs/Image;

    goto :goto_0

    .line 1709
    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4

    .line 1723
    const-string v1, "Author"

    const-string v2, "authorU"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1711
    :sswitch_7
    const-string v1, "Author"

    const-string v2, "authorURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1712
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Author;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 1717
    :sswitch_8
    const-string v1, "Author"

    const-string v2, "authorUsername"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1718
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Author;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 1734
    :sswitch_9
    const-string v1, "Author"

    const-string v2, "service"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1735
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Author;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 1740
    :sswitch_a
    const-string v1, "Author"

    const-string v2, "userid"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1741
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Author;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 1751
    :sswitch_b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 1765
    :goto_1
    return-object v0

    .line 1758
    :sswitch_c
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 1765
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1677
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_c
        0xa -> :sswitch_c
        0xd -> :sswitch_c
        0x20 -> :sswitch_c
        0x22 -> :sswitch_0
        0x2c -> :sswitch_c
        0x7d -> :sswitch_b
    .end sparse-switch

    .line 1679
    :sswitch_data_1
    .sparse-switch
        0x61 -> :sswitch_1
        0x73 -> :sswitch_9
        0x75 -> :sswitch_a
    .end sparse-switch

    .line 1682
    :sswitch_data_2
    .sparse-switch
        0x44 -> :sswitch_2
        0x49 -> :sswitch_5
        0x55 -> :sswitch_6
    .end sparse-switch

    .line 1684
    :sswitch_data_3
    .sparse-switch
        0x65 -> :sswitch_3
        0x69 -> :sswitch_4
    .end sparse-switch

    .line 1709
    :sswitch_data_4
    .sparse-switch
        0x52 -> :sswitch_7
        0x73 -> :sswitch_8
    .end sparse-switch
.end method

.method private Z()Lflipboard/objs/CommentaryResult$Item$Commentary;
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 2570
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2571
    new-instance v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    invoke-direct {v0}, Lflipboard/objs/CommentaryResult$Item$Commentary;-><init>()V

    .line 2573
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 2746
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 2575
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 2731
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2577
    :sswitch_1
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "author"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2578
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 2624
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "author"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2580
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    .line 2594
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "authorD"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2582
    :sswitch_3
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "authorDescription"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2583
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->f:Ljava/lang/String;

    goto :goto_0

    .line 2588
    :sswitch_4
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "authorDisplayName"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2589
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->c:Ljava/lang/String;

    goto :goto_0

    .line 2599
    :sswitch_5
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "authorImage"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2600
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->g:Lflipboard/objs/Image;

    goto :goto_0

    .line 2605
    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4

    .line 2619
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "authorU"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2607
    :sswitch_7
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "authorURL"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2608
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 2613
    :sswitch_8
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "authorUsername"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2614
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 2630
    :sswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_5

    .line 2657
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "c"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2632
    :sswitch_a
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "canDelete"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2633
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->p:Z

    goto/16 :goto_0

    .line 2638
    :sswitch_b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2652
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "co"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2640
    :pswitch_0
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "commentCount"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2641
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->n:I

    goto/16 :goto_0

    .line 2646
    :pswitch_1
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "connection"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2647
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->l:Z

    goto/16 :goto_0

    .line 2662
    :sswitch_c
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "dateCreated"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2663
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->k:J

    goto/16 :goto_0

    .line 2668
    :sswitch_d
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "id"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2669
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 2674
    :sswitch_e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_6

    .line 2701
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "s"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2676
    :sswitch_f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_7

    .line 2690
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "se"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2678
    :sswitch_10
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "sectionLinks"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2679
    invoke-direct {p0}, Lflipboard/json/JSONParser;->at()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->o:Ljava/util/List;

    goto/16 :goto_0

    .line 2684
    :sswitch_11
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "service"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2685
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 2695
    :sswitch_12
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "sourceURL"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2696
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 2706
    :sswitch_13
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_8

    .line 2720
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "t"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2708
    :sswitch_14
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "text"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2709
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 2714
    :sswitch_15
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "type"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2715
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 2725
    :sswitch_16
    const-string v1, "CommentaryResult$Item$Commentary"

    const-string v2, "userid"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2726
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 2736
    :sswitch_17
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 2750
    :goto_1
    return-object v0

    .line 2743
    :sswitch_18
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 2750
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2573
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_18
        0xa -> :sswitch_18
        0xd -> :sswitch_18
        0x20 -> :sswitch_18
        0x22 -> :sswitch_0
        0x2c -> :sswitch_18
        0x7d -> :sswitch_17
    .end sparse-switch

    .line 2575
    :sswitch_data_1
    .sparse-switch
        0x61 -> :sswitch_1
        0x63 -> :sswitch_9
        0x64 -> :sswitch_c
        0x69 -> :sswitch_d
        0x73 -> :sswitch_e
        0x74 -> :sswitch_13
        0x75 -> :sswitch_16
    .end sparse-switch

    .line 2578
    :sswitch_data_2
    .sparse-switch
        0x44 -> :sswitch_2
        0x49 -> :sswitch_5
        0x55 -> :sswitch_6
    .end sparse-switch

    .line 2580
    :sswitch_data_3
    .sparse-switch
        0x65 -> :sswitch_3
        0x69 -> :sswitch_4
    .end sparse-switch

    .line 2605
    :sswitch_data_4
    .sparse-switch
        0x52 -> :sswitch_7
        0x73 -> :sswitch_8
    .end sparse-switch

    .line 2630
    :sswitch_data_5
    .sparse-switch
        0x61 -> :sswitch_a
        0x6f -> :sswitch_b
    .end sparse-switch

    .line 2638
    :pswitch_data_0
    .packed-switch 0x6d
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 2674
    :sswitch_data_6
    .sparse-switch
        0x65 -> :sswitch_f
        0x6f -> :sswitch_12
    .end sparse-switch

    .line 2676
    :sswitch_data_7
    .sparse-switch
        0x63 -> :sswitch_10
        0x72 -> :sswitch_11
    .end sparse-switch

    .line 2706
    :sswitch_data_8
    .sparse-switch
        0x65 -> :sswitch_14
        0x79 -> :sswitch_15
    .end sparse-switch
.end method

.method private aA()Lflipboard/objs/LightBoxes$LightBox;
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 12204
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 12205
    new-instance v0, Lflipboard/objs/LightBoxes$LightBox;

    invoke-direct {v0}, Lflipboard/objs/LightBoxes$LightBox;-><init>()V

    .line 12207
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 12240
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 12209
    :sswitch_0
    const-string v1, "LightBoxes$LightBox"

    const-string v2, "ip"

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12210
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 12224
    const-string v1, "LightBoxes$LightBox"

    const-string v2, "ip"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 12212
    :sswitch_1
    const-string v1, "LightBoxes$LightBox"

    const-string v2, "ipad"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12213
    invoke-direct {p0}, Lflipboard/json/JSONParser;->az()Lflipboard/objs/LightBoxes$Device;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/LightBoxes$LightBox;->b:Lflipboard/objs/LightBoxes$Device;

    goto :goto_0

    .line 12218
    :sswitch_2
    const-string v1, "LightBoxes$LightBox"

    const-string v2, "iphone"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12219
    invoke-direct {p0}, Lflipboard/json/JSONParser;->az()Lflipboard/objs/LightBoxes$Device;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/LightBoxes$LightBox;->a:Lflipboard/objs/LightBoxes$Device;

    goto :goto_0

    .line 12230
    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 12244
    :goto_1
    return-object v0

    .line 12237
    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 12244
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 12207
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_4
        0xa -> :sswitch_4
        0xd -> :sswitch_4
        0x20 -> :sswitch_4
        0x22 -> :sswitch_0
        0x2c -> :sswitch_4
        0x7d -> :sswitch_3
    .end sparse-switch

    .line 12210
    :sswitch_data_1
    .sparse-switch
        0x61 -> :sswitch_1
        0x68 -> :sswitch_2
    .end sparse-switch
.end method

.method private aB()Lflipboard/objs/LightBoxes$Page;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 12347
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 12348
    new-instance v0, Lflipboard/objs/LightBoxes$Page;

    invoke-direct {v0}, Lflipboard/objs/LightBoxes$Page;-><init>()V

    .line 12350
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 12405
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 12352
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 12390
    const-string v1, "LightBoxes$Page"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 12354
    :sswitch_1
    const-string v1, "LightBoxes$Page"

    const-string v2, "auxiliaryButtonTitleKey"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12355
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/LightBoxes$Page;->e:Ljava/lang/String;

    goto :goto_0

    .line 12360
    :sswitch_2
    const-string v1, "LightBoxes$Page"

    const-string v2, "customIdentifier"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12361
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/LightBoxes$Page;->f:Ljava/lang/String;

    goto :goto_0

    .line 12366
    :sswitch_3
    const-string v1, "LightBoxes$Page"

    const-string v2, "descriptionKey"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12367
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/LightBoxes$Page;->a:Ljava/lang/String;

    goto :goto_0

    .line 12372
    :sswitch_4
    const-string v1, "LightBoxes$Page"

    const-string v2, "imageFilename"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12373
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/LightBoxes$Page;->c:Ljava/lang/String;

    goto :goto_0

    .line 12378
    :sswitch_5
    const-string v1, "LightBoxes$Page"

    const-string v2, "rightButtonTitleKey"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12379
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/LightBoxes$Page;->d:Ljava/lang/String;

    goto :goto_0

    .line 12384
    :sswitch_6
    const-string v1, "LightBoxes$Page"

    const-string v2, "titleKey"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12385
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/LightBoxes$Page;->b:Ljava/lang/String;

    goto :goto_0

    .line 12395
    :sswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 12409
    :goto_1
    return-object v0

    .line 12402
    :sswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 12409
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 12350
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_8
        0xa -> :sswitch_8
        0xd -> :sswitch_8
        0x20 -> :sswitch_8
        0x22 -> :sswitch_0
        0x2c -> :sswitch_8
        0x7d -> :sswitch_7
    .end sparse-switch

    .line 12352
    :sswitch_data_1
    .sparse-switch
        0x61 -> :sswitch_1
        0x63 -> :sswitch_2
        0x64 -> :sswitch_3
        0x69 -> :sswitch_4
        0x72 -> :sswitch_5
        0x74 -> :sswitch_6
    .end sparse-switch
.end method

.method private aC()Lflipboard/objs/SearchResultCategory;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 13501
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 13502
    new-instance v2, Lflipboard/objs/SearchResultCategory;

    invoke-direct {v2}, Lflipboard/objs/SearchResultCategory;-><init>()V

    .line 13504
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 13556
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 13506
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 13541
    const-string v0, "SearchResultCategory"

    const-string v3, ""

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 13508
    :sswitch_1
    const-string v0, "SearchResultCategory"

    const-string v3, "category"

    invoke-virtual {p0, v0, v3, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13509
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    .line 13523
    const-string v0, "SearchResultCategory"

    const-string v3, "category"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 13511
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13512
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/SearchResultCategory;->b:Ljava/lang/String;

    goto :goto_0

    .line 13517
    :sswitch_3
    const-string v0, "SearchResultCategory"

    const-string v3, "categoryTitle"

    const/16 v4, 0x9

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13518
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/SearchResultCategory;->a:Ljava/lang/String;

    goto :goto_0

    .line 13529
    :sswitch_4
    const-string v0, "SearchResultCategory"

    const-string v3, "moreResult"

    invoke-virtual {p0, v0, v3, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13530
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/SearchResultCategory;->d:Ljava/lang/String;

    goto :goto_0

    .line 13535
    :sswitch_5
    const-string v0, "SearchResultCategory"

    const-string v3, "searchResultItems"

    invoke-virtual {p0, v0, v3, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13536
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    iget v3, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v3, :sswitch_data_3

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->r()Lflipboard/objs/SearchResultItem;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v2, Lflipboard/objs/SearchResultCategory;->c:Ljava/util/List;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_2

    .line 13546
    :sswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v1, v2

    .line 13560
    :cond_2
    return-object v1

    .line 13553
    :sswitch_a
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 13504
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_a
        0xa -> :sswitch_a
        0xd -> :sswitch_a
        0x20 -> :sswitch_a
        0x22 -> :sswitch_0
        0x2c -> :sswitch_a
        0x7d -> :sswitch_9
    .end sparse-switch

    .line 13506
    :sswitch_data_1
    .sparse-switch
        0x63 -> :sswitch_1
        0x6d -> :sswitch_4
        0x73 -> :sswitch_5
    .end sparse-switch

    .line 13509
    :sswitch_data_2
    .sparse-switch
        0x22 -> :sswitch_2
        0x54 -> :sswitch_3
    .end sparse-switch

    .line 13536
    :sswitch_data_3
    .sparse-switch
        0x9 -> :sswitch_8
        0xa -> :sswitch_8
        0xd -> :sswitch_8
        0x20 -> :sswitch_8
        0x2c -> :sswitch_8
        0x5d -> :sswitch_7
        0x6e -> :sswitch_6
        0x7b -> :sswitch_6
    .end sparse-switch
.end method

.method private aD()Lflipboard/objs/SectionListItem;
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x5

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 14113
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14114
    new-instance v0, Lflipboard/objs/SectionListItem;

    invoke-direct {v0}, Lflipboard/objs/SectionListItem;-><init>()V

    .line 14116
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 14385
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 14118
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 14370
    :pswitch_0
    const-string v1, "SectionListItem"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14120
    :pswitch_1
    const-string v1, "SectionListItem"

    const-string v2, "author"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14121
    invoke-direct {p0}, Lflipboard/json/JSONParser;->Y()Lflipboard/objs/Author;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListItem;->k:Lflipboard/objs/Author;

    goto :goto_0

    .line 14126
    :pswitch_2
    const-string v1, "SectionListItem"

    const-string v2, "brick"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14127
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ab()Lflipboard/objs/ConfigBrick;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListItem;->j:Lflipboard/objs/ConfigBrick;

    goto :goto_0

    .line 14132
    :pswitch_3
    const-string v1, "SectionListItem"

    const-string v2, "categoryid"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14133
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListItem;->d:Ljava/lang/String;

    goto :goto_0

    .line 14138
    :pswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 14165
    const-string v1, "SectionListItem"

    const-string v2, "d"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14140
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 14154
    const-string v1, "SectionListItem"

    const-string v2, "de"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14142
    :sswitch_2
    const-string v1, "SectionListItem"

    const-string v2, "default"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14143
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SectionListItem;->i:Z

    goto :goto_0

    .line 14148
    :sswitch_3
    const-string v1, "SectionListItem"

    const-string v2, "description"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14149
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListItem;->bQ:Ljava/lang/String;

    goto/16 :goto_0

    .line 14159
    :sswitch_4
    const-string v1, "SectionListItem"

    const-string v2, "disabled"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14160
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SectionListItem;->f:Z

    goto/16 :goto_0

    .line 14170
    :pswitch_5
    const-string v1, "SectionListItem"

    const-string v2, "enumerated"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14171
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SectionListItem;->x:Z

    goto/16 :goto_0

    .line 14176
    :pswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    .line 14232
    const-string v1, "SectionListItem"

    const-string v2, "i"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 14178
    :sswitch_5
    const-string v1, "SectionListItem"

    const-string v2, "icon"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14179
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListItem;->bR:Ljava/lang/String;

    goto/16 :goto_0

    .line 14184
    :sswitch_6
    const-string v1, "SectionListItem"

    const-string v2, "image"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14185
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4

    .line 14214
    const-string v1, "SectionListItem"

    const-string v2, "image"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 14187
    :sswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14188
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListItem;->h:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 14193
    :sswitch_8
    const-string v1, "SectionListItem"

    const-string v2, "imageURL"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14194
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_5

    .line 14208
    const-string v1, "SectionListItem"

    const-string v2, "imageURL"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 14196
    :sswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14197
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListItem;->t:Ljava/lang/String;

    goto/16 :goto_0

    .line 14202
    :sswitch_a
    const-string v1, "SectionListItem"

    const-string v2, "imageURLHiddenInList"

    const/16 v3, 0x9

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14203
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SectionListItem;->g:Z

    goto/16 :goto_0

    .line 14220
    :sswitch_b
    const-string v1, "SectionListItem"

    const-string v2, "isFavicon"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14221
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SectionListItem;->bS:Z

    goto/16 :goto_0

    .line 14226
    :sswitch_c
    const-string v1, "SectionListItem"

    const-string v2, "items"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14227
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aE()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListItem;->e:Ljava/util/List;

    goto/16 :goto_0

    .line 14237
    :pswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_6

    .line 14251
    const-string v1, "SectionListItem"

    const-string v2, "p"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 14239
    :sswitch_d
    const-string v1, "SectionListItem"

    const-string v2, "pageKey"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14240
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListItem;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 14245
    :sswitch_e
    const-string v1, "SectionListItem"

    const-string v2, "private"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14246
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SectionListItem;->y:Z

    goto/16 :goto_0

    .line 14256
    :pswitch_8
    const-string v1, "SectionListItem"

    const-string v2, "remoteid"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14257
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->R()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    goto/16 :goto_0

    .line 14262
    :pswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 14304
    :pswitch_a
    const-string v1, "SectionListItem"

    const-string v2, "s"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 14264
    :pswitch_b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_7

    .line 14278
    const-string v1, "SectionListItem"

    const-string v2, "se"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 14266
    :sswitch_f
    const-string v1, "SectionListItem"

    const-string v2, "sectionTitle"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14267
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListItem;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 14272
    :sswitch_10
    const-string v1, "SectionListItem"

    const-string v2, "service"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14273
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListItem;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 14283
    :pswitch_c
    const-string v1, "SectionListItem"

    const-string v2, "show"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14284
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_2

    .line 14298
    :pswitch_d
    const-string v1, "SectionListItem"

    const-string v2, "show"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 14286
    :pswitch_e
    const-string v1, "SectionListItem"

    const-string v2, "showInline"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14287
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SectionListItem;->c:Z

    goto/16 :goto_0

    .line 14292
    :pswitch_f
    const-string v1, "SectionListItem"

    const-string v2, "showLogotypeImage"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14293
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SectionListItem;->w:Z

    goto/16 :goto_0

    .line 14309
    :pswitch_10
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_8

    .line 14338
    const-string v1, "SectionListItem"

    const-string v2, "t"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 14311
    :sswitch_11
    const-string v1, "SectionListItem"

    const-string v2, "title"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14312
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_9

    .line 14326
    const-string v1, "SectionListItem"

    const-string v2, "title"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 14314
    :sswitch_12
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14315
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListItem;->bP:Ljava/lang/String;

    goto/16 :goto_0

    .line 14320
    :sswitch_13
    const-string v1, "SectionListItem"

    const-string v2, "titleSuffix"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14321
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListItem;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 14332
    :sswitch_14
    const-string v1, "SectionListItem"

    const-string v2, "type"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14333
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListItem;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 14343
    :pswitch_11
    const-string v1, "SectionListItem"

    const-string v2, "unread"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14344
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_a

    .line 14358
    const-string v1, "SectionListItem"

    const-string v2, "unread"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 14346
    :sswitch_15
    const-string v1, "SectionListItem"

    const-string v2, "unreadCount"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14347
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/SectionListItem;->v:I

    goto/16 :goto_0

    .line 14352
    :sswitch_16
    const-string v1, "SectionListItem"

    const-string v2, "unreadRemoteid"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14353
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListItem;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 14364
    :pswitch_12
    const-string v1, "SectionListItem"

    const-string v2, "verified"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14365
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SectionListItem;->u:Z

    goto/16 :goto_0

    .line 14375
    :sswitch_17
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 14389
    :goto_1
    return-object v0

    .line 14382
    :sswitch_18
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 14389
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 14116
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_18
        0xa -> :sswitch_18
        0xd -> :sswitch_18
        0x20 -> :sswitch_18
        0x22 -> :sswitch_0
        0x2c -> :sswitch_18
        0x7d -> :sswitch_17
    .end sparse-switch

    .line 14118
    :pswitch_data_0
    .packed-switch 0x61
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch

    .line 14138
    :sswitch_data_1
    .sparse-switch
        0x65 -> :sswitch_1
        0x69 -> :sswitch_4
    .end sparse-switch

    .line 14140
    :sswitch_data_2
    .sparse-switch
        0x66 -> :sswitch_2
        0x73 -> :sswitch_3
    .end sparse-switch

    .line 14176
    :sswitch_data_3
    .sparse-switch
        0x63 -> :sswitch_5
        0x6d -> :sswitch_6
        0x73 -> :sswitch_b
        0x74 -> :sswitch_c
    .end sparse-switch

    .line 14185
    :sswitch_data_4
    .sparse-switch
        0x22 -> :sswitch_7
        0x55 -> :sswitch_8
    .end sparse-switch

    .line 14194
    :sswitch_data_5
    .sparse-switch
        0x22 -> :sswitch_9
        0x48 -> :sswitch_a
    .end sparse-switch

    .line 14237
    :sswitch_data_6
    .sparse-switch
        0x61 -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch

    .line 14262
    :pswitch_data_1
    .packed-switch 0x65
        :pswitch_b
        :pswitch_a
        :pswitch_a
        :pswitch_c
    .end packed-switch

    .line 14264
    :sswitch_data_7
    .sparse-switch
        0x63 -> :sswitch_f
        0x72 -> :sswitch_10
    .end sparse-switch

    .line 14284
    :pswitch_data_2
    .packed-switch 0x49
        :pswitch_e
        :pswitch_d
        :pswitch_d
        :pswitch_f
    .end packed-switch

    .line 14309
    :sswitch_data_8
    .sparse-switch
        0x69 -> :sswitch_11
        0x79 -> :sswitch_14
    .end sparse-switch

    .line 14312
    :sswitch_data_9
    .sparse-switch
        0x22 -> :sswitch_12
        0x53 -> :sswitch_13
    .end sparse-switch

    .line 14344
    :sswitch_data_a
    .sparse-switch
        0x43 -> :sswitch_15
        0x52 -> :sswitch_16
    .end sparse-switch
.end method

.method private aE()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SectionListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14394
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14395
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 14397
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 14413
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 14400
    :sswitch_0
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aD()Lflipboard/objs/SectionListItem;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 14403
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 14417
    :goto_1
    return-object v0

    .line 14410
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 14417
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 14397
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2c -> :sswitch_2
        0x5d -> :sswitch_1
        0x6e -> :sswitch_0
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method private aF()Lflipboard/objs/SectionPageTemplate;
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    .line 14690
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14691
    new-instance v0, Lflipboard/objs/SectionPageTemplate;

    invoke-direct {v0}, Lflipboard/objs/SectionPageTemplate;-><init>()V

    .line 14693
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 14860
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 14695
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 14845
    const-string v1, "SectionPageTemplate"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14697
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 14741
    const-string v1, "SectionPageTemplate"

    const-string v2, "a"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14699
    :sswitch_2
    const-string v1, "SectionPageTemplate"

    const-string v2, "allowedAsFirstPage"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14700
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    .line 14714
    const-string v1, "SectionPageTemplate"

    const-string v2, "allowedAsFirstPage"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14702
    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14703
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SectionPageTemplate;->l:Z

    goto :goto_0

    .line 14708
    :sswitch_4
    const-string v1, "SectionPageTemplate"

    const-string v2, "allowedAsFirstPageForPriorityOrdered"

    const/16 v3, 0x13

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14709
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SectionPageTemplate;->m:Z

    goto :goto_0

    .line 14720
    :sswitch_5
    const-string v1, "SectionPageTemplate"

    const-string v2, "areas"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14721
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4

    .line 14735
    const-string v1, "SectionPageTemplate"

    const-string v2, "areas"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14723
    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14724
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aH()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionPageTemplate;->j:Ljava/util/List;

    goto/16 :goto_0

    .line 14729
    :sswitch_7
    const-string v1, "SectionPageTemplate"

    const-string v2, "areasLandscape"

    const/4 v3, 0x6

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14730
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aH()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionPageTemplate;->k:Ljava/util/List;

    goto/16 :goto_0

    .line 14746
    :sswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_5

    .line 14760
    const-string v1, "SectionPageTemplate"

    const-string v2, "d"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 14748
    :sswitch_9
    const-string v1, "SectionPageTemplate"

    const-string v2, "description"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14749
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionPageTemplate;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 14754
    :sswitch_a
    const-string v1, "SectionPageTemplate"

    const-string v2, "dontUseNormally"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14755
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SectionPageTemplate;->i:Z

    goto/16 :goto_0

    .line 14765
    :sswitch_b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_6

    .line 14815
    const-string v1, "SectionPageTemplate"

    const-string v2, "m"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 14767
    :sswitch_c
    const-string v1, "SectionPageTemplate"

    const-string v2, "max"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14768
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_7

    .line 14788
    const-string v1, "SectionPageTemplate"

    const-string v2, "max"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 14770
    :sswitch_d
    const-string v1, "SectionPageTemplate"

    const-string v2, "maxFrequency"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14771
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/SectionPageTemplate;->h:F

    goto/16 :goto_0

    .line 14776
    :sswitch_e
    const-string v1, "SectionPageTemplate"

    const-string v2, "maxHeightDp"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14777
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/SectionPageTemplate;->g:I

    goto/16 :goto_0

    .line 14782
    :sswitch_f
    const-string v1, "SectionPageTemplate"

    const-string v2, "maxWidthDp"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14783
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/SectionPageTemplate;->f:I

    goto/16 :goto_0

    .line 14794
    :sswitch_10
    const-string v1, "SectionPageTemplate"

    const-string v2, "min"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14795
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_8

    .line 14809
    const-string v1, "SectionPageTemplate"

    const-string v2, "min"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 14797
    :sswitch_11
    const-string v1, "SectionPageTemplate"

    const-string v2, "minHeightDp"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14798
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/SectionPageTemplate;->e:I

    goto/16 :goto_0

    .line 14803
    :sswitch_12
    const-string v1, "SectionPageTemplate"

    const-string v2, "minWidthDp"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14804
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/SectionPageTemplate;->d:I

    goto/16 :goto_0

    .line 14820
    :sswitch_13
    const-string v1, "SectionPageTemplate"

    const-string v2, "name"

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14821
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionPageTemplate;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 14826
    :sswitch_14
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 14840
    :pswitch_0
    const-string v1, "SectionPageTemplate"

    const-string v2, "t"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 14828
    :pswitch_1
    const-string v1, "SectionPageTemplate"

    const-string v2, "tweetlist"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14829
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SectionPageTemplate;->n:Z

    goto/16 :goto_0

    .line 14834
    :pswitch_2
    const-string v1, "SectionPageTemplate"

    const-string v2, "types"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14835
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionPageTemplate;->c:Ljava/util/List;

    goto/16 :goto_0

    .line 14850
    :sswitch_15
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 14864
    :goto_1
    return-object v0

    .line 14857
    :sswitch_16
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 14864
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 14693
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_16
        0xa -> :sswitch_16
        0xd -> :sswitch_16
        0x20 -> :sswitch_16
        0x22 -> :sswitch_0
        0x2c -> :sswitch_16
        0x7d -> :sswitch_15
    .end sparse-switch

    .line 14695
    :sswitch_data_1
    .sparse-switch
        0x61 -> :sswitch_1
        0x64 -> :sswitch_8
        0x6d -> :sswitch_b
        0x6e -> :sswitch_13
        0x74 -> :sswitch_14
    .end sparse-switch

    .line 14697
    :sswitch_data_2
    .sparse-switch
        0x6c -> :sswitch_2
        0x72 -> :sswitch_5
    .end sparse-switch

    .line 14700
    :sswitch_data_3
    .sparse-switch
        0x22 -> :sswitch_3
        0x46 -> :sswitch_4
    .end sparse-switch

    .line 14721
    :sswitch_data_4
    .sparse-switch
        0x22 -> :sswitch_6
        0x4c -> :sswitch_7
    .end sparse-switch

    .line 14746
    :sswitch_data_5
    .sparse-switch
        0x65 -> :sswitch_9
        0x6f -> :sswitch_a
    .end sparse-switch

    .line 14765
    :sswitch_data_6
    .sparse-switch
        0x61 -> :sswitch_c
        0x69 -> :sswitch_10
    .end sparse-switch

    .line 14768
    :sswitch_data_7
    .sparse-switch
        0x46 -> :sswitch_d
        0x48 -> :sswitch_e
        0x57 -> :sswitch_f
    .end sparse-switch

    .line 14795
    :sswitch_data_8
    .sparse-switch
        0x48 -> :sswitch_11
        0x57 -> :sswitch_12
    .end sparse-switch

    .line 14826
    :pswitch_data_0
    .packed-switch 0x77
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private aG()Lflipboard/objs/SectionPageTemplate$Area;
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/4 v3, 0x1

    .line 14967
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14968
    new-instance v0, Lflipboard/objs/SectionPageTemplate$Area;

    invoke-direct {v0}, Lflipboard/objs/SectionPageTemplate$Area;-><init>()V

    .line 14970
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 15046
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 14972
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 15031
    const-string v1, "SectionPageTemplate$Area"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14974
    :sswitch_1
    const-string v1, "SectionPageTemplate$Area"

    const-string v2, "allowedTypes"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14975
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionPageTemplate$Area;->e:Ljava/util/List;

    goto :goto_0

    .line 14980
    :sswitch_2
    const-string v1, "SectionPageTemplate$Area"

    const-string v2, "fullBleed"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14981
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 14995
    const-string v1, "SectionPageTemplate$Area"

    const-string v2, "fullBleed"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14983
    :sswitch_3
    const-string v1, "SectionPageTemplate$Area"

    const-string v2, "fullBleedLandscape"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14984
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SectionPageTemplate$Area;->g:Z

    goto :goto_0

    .line 14989
    :sswitch_4
    const-string v1, "SectionPageTemplate$Area"

    const-string v2, "fullBleedPortrait"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14990
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SectionPageTemplate$Area;->f:Z

    goto :goto_0

    .line 15001
    :sswitch_5
    const-string v1, "SectionPageTemplate$Area"

    const-string v2, "height"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15002
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/SectionPageTemplate$Area;->d:F

    goto :goto_0

    .line 15007
    :sswitch_6
    const-string v1, "SectionPageTemplate$Area"

    const-string v2, "landscapeArea"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15008
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SectionPageTemplate$Area;->h:Z

    goto/16 :goto_0

    .line 15013
    :sswitch_7
    const-string v1, "SectionPageTemplate$Area"

    const-string v2, "width"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15014
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/SectionPageTemplate$Area;->c:F

    goto/16 :goto_0

    .line 15019
    :sswitch_8
    const-string v1, "SectionPageTemplate$Area"

    const-string v2, "x"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15020
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/SectionPageTemplate$Area;->a:F

    goto/16 :goto_0

    .line 15025
    :sswitch_9
    const-string v1, "SectionPageTemplate$Area"

    const-string v2, "y"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15026
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/SectionPageTemplate$Area;->b:F

    goto/16 :goto_0

    .line 15036
    :sswitch_a
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 15050
    :goto_1
    return-object v0

    .line 15043
    :sswitch_b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 15050
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 14970
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_b
        0xa -> :sswitch_b
        0xd -> :sswitch_b
        0x20 -> :sswitch_b
        0x22 -> :sswitch_0
        0x2c -> :sswitch_b
        0x7d -> :sswitch_a
    .end sparse-switch

    .line 14972
    :sswitch_data_1
    .sparse-switch
        0x61 -> :sswitch_1
        0x66 -> :sswitch_2
        0x68 -> :sswitch_5
        0x6c -> :sswitch_6
        0x77 -> :sswitch_7
        0x78 -> :sswitch_8
        0x79 -> :sswitch_9
    .end sparse-switch

    .line 14981
    :sswitch_data_2
    .sparse-switch
        0x4c -> :sswitch_3
        0x50 -> :sswitch_4
    .end sparse-switch
.end method

.method private aH()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SectionPageTemplate$Area;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15055
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15056
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 15058
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 15074
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 15061
    :sswitch_0
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aG()Lflipboard/objs/SectionPageTemplate$Area;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 15064
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 15078
    :goto_1
    return-object v0

    .line 15071
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 15078
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 15058
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2c -> :sswitch_2
        0x5d -> :sswitch_1
        0x6e -> :sswitch_0
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method private aI()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SidebarGroup;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15265
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15266
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 15268
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 15284
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 15271
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->v()Lflipboard/objs/SidebarGroup;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 15274
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 15288
    :goto_1
    return-object v0

    .line 15281
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 15288
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 15268
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2c -> :sswitch_2
        0x5d -> :sswitch_1
        0x6e -> :sswitch_0
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method private aJ()Lflipboard/objs/SidebarGroup$Metrics;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 15363
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15364
    new-instance v0, Lflipboard/objs/SidebarGroup$Metrics;

    invoke-direct {v0}, Lflipboard/objs/SidebarGroup$Metrics;-><init>()V

    .line 15366
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 15409
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 15368
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 15394
    const-string v1, "SidebarGroup$Metrics"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 15370
    :sswitch_1
    const-string v1, "SidebarGroup$Metrics"

    const-string v2, "displayName"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15371
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SidebarGroup$Metrics;->d:Ljava/lang/String;

    goto :goto_0

    .line 15376
    :sswitch_2
    const-string v1, "SidebarGroup$Metrics"

    const-string v2, "raw"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15377
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/SidebarGroup$Metrics;->c:I

    goto :goto_0

    .line 15382
    :sswitch_3
    const-string v1, "SidebarGroup$Metrics"

    const-string v2, "type"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15383
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SidebarGroup$Metrics;->a:Ljava/lang/String;

    goto :goto_0

    .line 15388
    :sswitch_4
    const-string v1, "SidebarGroup$Metrics"

    const-string v2, "value"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15389
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SidebarGroup$Metrics;->b:Ljava/lang/String;

    goto :goto_0

    .line 15399
    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 15413
    :goto_1
    return-object v0

    .line 15406
    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 15413
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 15366
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_6
        0xa -> :sswitch_6
        0xd -> :sswitch_6
        0x20 -> :sswitch_6
        0x22 -> :sswitch_0
        0x2c -> :sswitch_6
        0x7d -> :sswitch_5
    .end sparse-switch

    .line 15368
    :sswitch_data_1
    .sparse-switch
        0x64 -> :sswitch_1
        0x72 -> :sswitch_2
        0x74 -> :sswitch_3
        0x76 -> :sswitch_4
    .end sparse-switch
.end method

.method private aK()Lflipboard/objs/SidebarGroup$RenderHints;
    .locals 8

    .prologue
    const/16 v7, 0xe

    const/16 v6, 0xb

    const/4 v5, 0x5

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 15516
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15517
    new-instance v0, Lflipboard/objs/SidebarGroup$RenderHints;

    invoke-direct {v0}, Lflipboard/objs/SidebarGroup$RenderHints;-><init>()V

    .line 15519
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 15669
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 15521
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 15654
    :pswitch_0
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 15523
    :pswitch_1
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "alwaysShowMore"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15524
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SidebarGroup$RenderHints;->k:Z

    goto :goto_0

    .line 15529
    :pswitch_2
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "background"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15530
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 15544
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "background"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 15532
    :sswitch_1
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "backgroundColor"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15533
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SidebarGroup$RenderHints;->d:Ljava/lang/String;

    goto :goto_0

    .line 15538
    :sswitch_2
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "backgroundImage"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15539
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SidebarGroup$RenderHints;->i:Lflipboard/objs/Image;

    goto :goto_0

    .line 15550
    :pswitch_3
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "dividerColor"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15551
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SidebarGroup$RenderHints;->e:Ljava/lang/String;

    goto :goto_0

    .line 15556
    :pswitch_4
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "expandedGroup"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15557
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 15571
    :pswitch_5
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "expandedGroup"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 15559
    :pswitch_6
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "expandedGroupRenderHint"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15560
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aK()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SidebarGroup$RenderHints;->p:Lflipboard/objs/SidebarGroup$RenderHints;

    goto/16 :goto_0

    .line 15565
    :pswitch_7
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "expandedGroupTitle"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15566
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SidebarGroup$RenderHints;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 15577
    :pswitch_8
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "highlightedColor"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15578
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SidebarGroup$RenderHints;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 15583
    :pswitch_9
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "logoImage"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15584
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SidebarGroup$RenderHints;->h:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 15589
    :pswitch_a
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "pageIndex"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15590
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/SidebarGroup$RenderHints;->c:I

    goto/16 :goto_0

    .line 15595
    :pswitch_b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 15624
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "s"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 15597
    :sswitch_3
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "show"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15598
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_2

    .line 15612
    :pswitch_c
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "show"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 15600
    :pswitch_d
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "showFollowButton"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15601
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SidebarGroup$RenderHints;->o:Z

    goto/16 :goto_0

    .line 15606
    :pswitch_e
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "showImages"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15607
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SidebarGroup$RenderHints;->l:Z

    goto/16 :goto_0

    .line 15618
    :sswitch_4
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "style"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15619
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SidebarGroup$RenderHints;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 15629
    :pswitch_f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    .line 15649
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "t"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 15631
    :sswitch_5
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "textColor"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15632
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SidebarGroup$RenderHints;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 15637
    :sswitch_6
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "titleColor"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15638
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SidebarGroup$RenderHints;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 15643
    :sswitch_7
    const-string v1, "SidebarGroup$RenderHints"

    const-string v2, "type"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15644
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 15659
    :sswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 15673
    :goto_1
    return-object v0

    .line 15666
    :sswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 15673
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 15519
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_9
        0xa -> :sswitch_9
        0xd -> :sswitch_9
        0x20 -> :sswitch_9
        0x22 -> :sswitch_0
        0x2c -> :sswitch_9
        0x7d -> :sswitch_8
    .end sparse-switch

    .line 15521
    :pswitch_data_0
    .packed-switch 0x61
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_f
    .end packed-switch

    .line 15530
    :sswitch_data_1
    .sparse-switch
        0x43 -> :sswitch_1
        0x49 -> :sswitch_2
    .end sparse-switch

    .line 15557
    :pswitch_data_1
    .packed-switch 0x52
        :pswitch_6
        :pswitch_5
        :pswitch_7
    .end packed-switch

    .line 15595
    :sswitch_data_2
    .sparse-switch
        0x68 -> :sswitch_3
        0x74 -> :sswitch_4
    .end sparse-switch

    .line 15598
    :pswitch_data_2
    .packed-switch 0x46
        :pswitch_d
        :pswitch_c
        :pswitch_c
        :pswitch_e
    .end packed-switch

    .line 15629
    :sswitch_data_3
    .sparse-switch
        0x65 -> :sswitch_5
        0x69 -> :sswitch_6
        0x79 -> :sswitch_7
    .end sparse-switch
.end method

.method private aL()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SidebarGroup$RenderHints;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15678
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15679
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 15681
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 15697
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 15684
    :sswitch_0
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aK()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 15687
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 15701
    :goto_1
    return-object v0

    .line 15694
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 15701
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 15681
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2c -> :sswitch_2
        0x5d -> :sswitch_1
        0x6e -> :sswitch_0
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method private aM()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/UserService;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16840
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16841
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 16843
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 16859
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 16846
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->A()Lflipboard/objs/UserService;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 16849
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 16863
    :goto_1
    return-object v0

    .line 16856
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 16863
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 16843
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2c -> :sswitch_2
        0x5d -> :sswitch_1
        0x6e -> :sswitch_0
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method private aN()Lflipboard/objs/UserService$Cookie;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 16938
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16939
    new-instance v0, Lflipboard/objs/UserService$Cookie;

    invoke-direct {v0}, Lflipboard/objs/UserService$Cookie;-><init>()V

    .line 16941
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 16978
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 16943
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 16963
    const-string v1, "UserService$Cookie"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 16945
    :sswitch_1
    const-string v1, "UserService$Cookie"

    const-string v2, "domain"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16946
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserService$Cookie;->b:Ljava/lang/String;

    goto :goto_0

    .line 16951
    :sswitch_2
    const-string v1, "UserService$Cookie"

    const-string v2, "name"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16952
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserService$Cookie;->a:Ljava/lang/String;

    goto :goto_0

    .line 16957
    :sswitch_3
    const-string v1, "UserService$Cookie"

    const-string v2, "value"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16958
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserService$Cookie;->c:Ljava/lang/String;

    goto :goto_0

    .line 16968
    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 16982
    :goto_1
    return-object v0

    .line 16975
    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 16982
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 16941
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_5
        0xa -> :sswitch_5
        0xd -> :sswitch_5
        0x20 -> :sswitch_5
        0x22 -> :sswitch_0
        0x2c -> :sswitch_5
        0x7d -> :sswitch_4
    .end sparse-switch

    .line 16943
    :sswitch_data_1
    .sparse-switch
        0x64 -> :sswitch_1
        0x6e -> :sswitch_2
        0x76 -> :sswitch_3
    .end sparse-switch
.end method

.method private aO()Lflipboard/objs/UserServices;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x7

    const/4 v6, 0x3

    const/4 v5, 0x1

    .line 17085
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 17086
    new-instance v2, Lflipboard/objs/UserServices;

    invoke-direct {v2}, Lflipboard/objs/UserServices;-><init>()V

    .line 17088
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 17152
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 17090
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 17137
    const-string v0, "UserServices"

    const-string v3, ""

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 17092
    :sswitch_1
    const-string v0, "UserServices"

    const-string v3, "experiments"

    invoke-virtual {p0, v0, v3, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17093
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->Q()Lflipboard/json/FLObject;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/UserServices;->f:Lflipboard/json/FLObject;

    goto :goto_0

    .line 17098
    :sswitch_2
    const-string v0, "UserServices"

    const-string v3, "my"

    invoke-virtual {p0, v0, v3, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17099
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 17113
    const-string v0, "UserServices"

    const-string v3, "my"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 17101
    :pswitch_0
    const-string v0, "UserServices"

    const-string v3, "myReadLaterServices"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17102
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aM()Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/UserServices;->b:Ljava/util/List;

    goto :goto_0

    .line 17107
    :pswitch_1
    const-string v0, "UserServices"

    const-string v3, "myServices"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17108
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aM()Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/UserServices;->a:Ljava/util/List;

    goto :goto_0

    .line 17119
    :sswitch_3
    const-string v0, "UserServices"

    const-string v3, "name"

    invoke-virtual {p0, v0, v3, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17120
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/UserServices;->c:Ljava/lang/String;

    goto :goto_0

    .line 17125
    :sswitch_4
    const-string v0, "UserServices"

    const-string v3, "stateRevisions"

    invoke-virtual {p0, v0, v3, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17126
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lflipboard/objs/UserServices$StateRevisions;

    invoke-direct {v0}, Lflipboard/objs/UserServices$StateRevisions;-><init>()V

    :cond_1
    :goto_1
    iget v3, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v3, :sswitch_data_2

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v3

    sparse-switch v3, :sswitch_data_3

    const-string v3, "UserServices$StateRevisions"

    const-string v4, ""

    invoke-virtual {p0, v3, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :sswitch_6
    const-string v3, "UserServices$StateRevisions"

    const-string v4, "coverSections"

    invoke-virtual {p0, v3, v4, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lflipboard/objs/UserServices$StateRevisions;->b:Ljava/lang/String;

    goto :goto_1

    :sswitch_7
    const-string v3, "UserServices$StateRevisions"

    const-string v4, "flap_c"

    invoke-virtual {p0, v3, v4, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v3

    sparse-switch v3, :sswitch_data_4

    const-string v3, "UserServices$StateRevisions"

    const-string v4, "flap_c"

    invoke-virtual {p0, v3, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :sswitch_8
    const-string v3, "UserServices$StateRevisions"

    const-string v4, "flap_coverSections"

    invoke-virtual {p0, v3, v4, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lflipboard/objs/UserServices$StateRevisions;->d:Ljava/lang/String;

    goto :goto_1

    :sswitch_9
    const-string v3, "UserServices$StateRevisions"

    const-string v4, "flap_curation"

    invoke-virtual {p0, v3, v4, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lflipboard/objs/UserServices$StateRevisions;->c:Ljava/lang/String;

    goto :goto_1

    :sswitch_a
    const-string v3, "UserServices$StateRevisions"

    const-string v4, "user"

    invoke-virtual {p0, v3, v4, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lflipboard/objs/UserServices$StateRevisions;->a:Ljava/lang/String;

    goto :goto_1

    :sswitch_b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v2, Lflipboard/objs/UserServices;->d:Lflipboard/objs/UserServices$StateRevisions;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_2

    .line 17131
    :sswitch_d
    const-string v0, "UserServices"

    const-string v3, "userid"

    invoke-virtual {p0, v0, v3, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17132
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v2, Lflipboard/objs/UserServices;->e:I

    goto/16 :goto_0

    .line 17142
    :sswitch_e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v1, v2

    .line 17156
    :cond_3
    return-object v1

    .line 17149
    :sswitch_f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 17088
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_f
        0xa -> :sswitch_f
        0xd -> :sswitch_f
        0x20 -> :sswitch_f
        0x22 -> :sswitch_0
        0x2c -> :sswitch_f
        0x7d -> :sswitch_e
    .end sparse-switch

    .line 17090
    :sswitch_data_1
    .sparse-switch
        0x65 -> :sswitch_1
        0x6d -> :sswitch_2
        0x6e -> :sswitch_3
        0x73 -> :sswitch_4
        0x75 -> :sswitch_d
    .end sparse-switch

    .line 17099
    :pswitch_data_0
    .packed-switch 0x52
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 17126
    :sswitch_data_2
    .sparse-switch
        0x9 -> :sswitch_c
        0xa -> :sswitch_c
        0xd -> :sswitch_c
        0x20 -> :sswitch_c
        0x22 -> :sswitch_5
        0x2c -> :sswitch_c
        0x7d -> :sswitch_b
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x63 -> :sswitch_6
        0x66 -> :sswitch_7
        0x75 -> :sswitch_a
    .end sparse-switch

    :sswitch_data_4
    .sparse-switch
        0x6f -> :sswitch_8
        0x75 -> :sswitch_9
    .end sparse-switch
.end method

.method private aP()Lflipboard/objs/UserState$MutedAuthor;
    .locals 4

    .prologue
    .line 17788
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 17789
    new-instance v0, Lflipboard/objs/UserState$MutedAuthor;

    invoke-direct {v0}, Lflipboard/objs/UserState$MutedAuthor;-><init>()V

    .line 17791
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 17820
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 17793
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    .line 17794
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->I()Z

    .line 17795
    const-string v2, "authorID"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 17796
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserState$MutedAuthor;->b:Ljava/lang/String;

    goto :goto_0

    .line 17797
    :cond_0
    const-string v2, "authorUsername"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 17798
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserState$MutedAuthor;->c:Ljava/lang/String;

    goto :goto_0

    .line 17799
    :cond_1
    const-string v2, "serviceName"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 17800
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserState$MutedAuthor;->d:Ljava/lang/String;

    goto :goto_0

    .line 17801
    :cond_2
    const-string v2, "authorDisplayName"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 17802
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserState$MutedAuthor;->e:Ljava/lang/String;

    goto :goto_0

    .line 17804
    :cond_3
    iget-object v2, v0, Lflipboard/objs/UserState$MutedAuthor;->a:Lflipboard/json/FLObject;

    if-nez v2, :cond_4

    new-instance v2, Lflipboard/json/FLObject;

    invoke-direct {v2}, Lflipboard/json/FLObject;-><init>()V

    iput-object v2, v0, Lflipboard/objs/UserState$MutedAuthor;->a:Lflipboard/json/FLObject;

    .line 17805
    :cond_4
    iget-object v2, v0, Lflipboard/objs/UserState$MutedAuthor;->a:Lflipboard/json/FLObject;

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->R()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 17810
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 17824
    :goto_1
    return-object v0

    .line 17817
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 17824
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 17791
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x22 -> :sswitch_0
        0x2c -> :sswitch_2
        0x7d -> :sswitch_1
    .end sparse-switch
.end method

.method private aQ()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/UserState$MutedAuthor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17829
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17830
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 17832
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 17848
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 17835
    :sswitch_0
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aP()Lflipboard/objs/UserState$MutedAuthor;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 17838
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 17852
    :goto_1
    return-object v0

    .line 17845
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 17852
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 17832
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2c -> :sswitch_2
        0x5d -> :sswitch_1
        0x6e -> :sswitch_0
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method private aR()Lflipboard/objs/UserState$State;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v1, 0x0

    const/4 v7, 0x1

    .line 17927
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 17928
    new-instance v3, Lflipboard/objs/UserState$State;

    invoke-direct {v3}, Lflipboard/objs/UserState$State;-><init>()V

    .line 17930
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 17988
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 17932
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 17973
    const-string v0, "UserState$State"

    const-string v2, ""

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 17934
    :sswitch_1
    const-string v0, "UserState$State"

    const-string v2, "dat"

    invoke-virtual {p0, v0, v2, v7}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17935
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    .line 17949
    const-string v0, "UserState$State"

    const-string v2, "dat"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 17937
    :sswitch_2
    const-string v0, "UserState$State"

    const-string v2, "data"

    invoke-virtual {p0, v0, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17938
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_e

    new-instance v2, Lflipboard/objs/UserState$Data;

    invoke-direct {v2}, Lflipboard/objs/UserState$Data;-><init>()V

    :goto_1
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_3

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->I()Z

    const-string v4, "hiddenURLStrings"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/UserState$Data;->a:Ljava/util/List;

    goto :goto_1

    :cond_1
    const-string v4, "mutedAuthors"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0}, Lflipboard/json/JSONParser;->aQ()Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    goto :goto_1

    :cond_2
    const-string v4, "tocSections"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->x()Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/UserState$Data;->c:Ljava/util/List;

    goto :goto_1

    :cond_3
    const-string v4, "mutedAuthorsForSectionIdentifiers"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->K()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    :goto_2
    iget v4, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v4, :sswitch_data_4

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_2

    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    iget v4, p0, Lflipboard/json/JSONParser;->e:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p0, v4}, Lflipboard/json/JSONParser;->a(I)Ljava/lang/String;

    move-result-object v4

    :goto_3
    iget v5, p0, Lflipboard/json/JSONParser;->f:I

    const/16 v6, 0x20

    if-eq v5, v6, :cond_4

    iget v5, p0, Lflipboard/json/JSONParser;->f:I

    const/16 v6, 0x9

    if-eq v5, v6, :cond_4

    iget v5, p0, Lflipboard/json/JSONParser;->f:I

    const/16 v6, 0xd

    if-eq v5, v6, :cond_4

    iget v5, p0, Lflipboard/json/JSONParser;->f:I

    const/16 v6, 0xa

    if-eq v5, v6, :cond_4

    iget v5, p0, Lflipboard/json/JSONParser;->f:I

    const/16 v6, 0x3a

    if-ne v5, v6, :cond_5

    :cond_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_3

    :cond_5
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aQ()Ljava/util/List;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_4
    iput-object v0, v2, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    goto/16 :goto_1

    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_2

    :cond_6
    move-object v0, v1

    goto :goto_4

    :cond_7
    const-string v4, "coverStoriesHidden"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v0

    iput-boolean v0, v2, Lflipboard/objs/UserState$Data;->e:Z

    goto/16 :goto_1

    :cond_8
    const-string v4, "pushNotificationSettings"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->Q()Lflipboard/json/FLObject;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/UserState$Data;->f:Lflipboard/json/FLObject;

    goto/16 :goto_1

    :cond_9
    const-string v4, "selectedShareServices"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/UserState$Data;->g:Ljava/util/List;

    goto/16 :goto_1

    :cond_a
    const-string v4, "emailsForBugReporting"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/UserState$Data;->h:Ljava/util/List;

    goto/16 :goto_1

    :cond_b
    const-string v4, "prominenceOverrideType"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/UserState$Data;->i:Ljava/lang/String;

    goto/16 :goto_1

    :cond_c
    iget-object v4, v2, Lflipboard/objs/UserState$Data;->j:Lflipboard/json/FLObject;

    if-nez v4, :cond_d

    new-instance v4, Lflipboard/json/FLObject;

    invoke-direct {v4}, Lflipboard/json/FLObject;-><init>()V

    iput-object v4, v2, Lflipboard/objs/UserState$Data;->j:Lflipboard/json/FLObject;

    :cond_d
    iget-object v4, v2, Lflipboard/objs/UserState$Data;->j:Lflipboard/json/FLObject;

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->R()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :sswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v0, v2

    :goto_5
    iput-object v0, v3, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_1

    :cond_e
    move-object v0, v1

    goto :goto_5

    .line 17943
    :sswitch_9
    const-string v0, "UserState$State"

    const-string v2, "dateModified"

    invoke-virtual {p0, v0, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17944
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v4

    iput-wide v4, v3, Lflipboard/objs/UserState$State;->b:J

    goto/16 :goto_0

    .line 17955
    :sswitch_a
    const-string v0, "UserState$State"

    const-string v2, "revision"

    invoke-virtual {p0, v0, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17956
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lflipboard/objs/UserState$State;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 17961
    :sswitch_b
    const-string v0, "UserState$State"

    const-string v2, "type"

    invoke-virtual {p0, v0, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17962
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lflipboard/objs/UserState$State;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 17967
    :sswitch_c
    const-string v0, "UserState$State"

    const-string v2, "unmodified"

    invoke-virtual {p0, v0, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17968
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v0

    iput-boolean v0, v3, Lflipboard/objs/UserState$State;->e:Z

    goto/16 :goto_0

    .line 17978
    :sswitch_d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v1, v3

    .line 17992
    :cond_f
    return-object v1

    .line 17985
    :sswitch_e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 17930
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_e
        0xa -> :sswitch_e
        0xd -> :sswitch_e
        0x20 -> :sswitch_e
        0x22 -> :sswitch_0
        0x2c -> :sswitch_e
        0x7d -> :sswitch_d
    .end sparse-switch

    .line 17932
    :sswitch_data_1
    .sparse-switch
        0x64 -> :sswitch_1
        0x72 -> :sswitch_a
        0x74 -> :sswitch_b
        0x75 -> :sswitch_c
    .end sparse-switch

    .line 17935
    :sswitch_data_2
    .sparse-switch
        0x61 -> :sswitch_2
        0x65 -> :sswitch_9
    .end sparse-switch

    .line 17938
    :sswitch_data_3
    .sparse-switch
        0x9 -> :sswitch_8
        0xa -> :sswitch_8
        0xd -> :sswitch_8
        0x20 -> :sswitch_8
        0x22 -> :sswitch_3
        0x2c -> :sswitch_8
        0x7d -> :sswitch_7
    .end sparse-switch

    :sswitch_data_4
    .sparse-switch
        0x9 -> :sswitch_6
        0xa -> :sswitch_6
        0xd -> :sswitch_6
        0x20 -> :sswitch_6
        0x22 -> :sswitch_4
        0x2c -> :sswitch_6
        0x7d -> :sswitch_5
    .end sparse-switch
.end method

.method private aa()Lflipboard/objs/CommentaryResult$Item$ProfileMetric;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2853
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2854
    new-instance v0, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;

    invoke-direct {v0}, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;-><init>()V

    .line 2856
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 2893
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 2858
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 2878
    const-string v1, "CommentaryResult$Item$ProfileMetric"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2860
    :sswitch_1
    const-string v1, "CommentaryResult$Item$ProfileMetric"

    const-string v2, "displayName"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2861
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;->a:Ljava/lang/String;

    goto :goto_0

    .line 2866
    :sswitch_2
    const-string v1, "CommentaryResult$Item$ProfileMetric"

    const-string v2, "type"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2867
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;->c:Ljava/lang/String;

    goto :goto_0

    .line 2872
    :sswitch_3
    const-string v1, "CommentaryResult$Item$ProfileMetric"

    const-string v2, "value"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2873
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/CommentaryResult$Item$ProfileMetric;->b:Ljava/lang/String;

    goto :goto_0

    .line 2883
    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 2897
    :goto_1
    return-object v0

    .line 2890
    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 2897
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2856
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_5
        0xa -> :sswitch_5
        0xd -> :sswitch_5
        0x20 -> :sswitch_5
        0x22 -> :sswitch_0
        0x2c -> :sswitch_5
        0x7d -> :sswitch_4
    .end sparse-switch

    .line 2858
    :sswitch_data_1
    .sparse-switch
        0x64 -> :sswitch_1
        0x74 -> :sswitch_2
        0x76 -> :sswitch_3
    .end sparse-switch
.end method

.method private ab()Lflipboard/objs/ConfigBrick;
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 3000
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3001
    new-instance v0, Lflipboard/objs/ConfigBrick;

    invoke-direct {v0}, Lflipboard/objs/ConfigBrick;-><init>()V

    .line 3003
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 3111
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 3005
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 3096
    :pswitch_0
    const-string v1, "ConfigBrick"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3007
    :pswitch_1
    const-string v1, "ConfigBrick"

    const-string v2, "height"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3008
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigBrick;->f:I

    goto :goto_0

    .line 3013
    :pswitch_2
    const-string v1, "ConfigBrick"

    const-string v2, "imageURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3014
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigBrick;->a:Ljava/lang/String;

    goto :goto_0

    .line 3019
    :pswitch_3
    const-string v1, "ConfigBrick"

    const-string v2, "largeURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3020
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigBrick;->b:Ljava/lang/String;

    goto :goto_0

    .line 3025
    :pswitch_4
    const-string v1, "ConfigBrick"

    const-string v2, "mediumURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3026
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigBrick;->c:Ljava/lang/String;

    goto :goto_0

    .line 3031
    :pswitch_5
    const-string v1, "ConfigBrick"

    const-string v2, "perRow"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3032
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigBrick;->g:I

    goto :goto_0

    .line 3037
    :pswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 3085
    const-string v1, "ConfigBrick"

    const-string v2, "s"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3039
    :sswitch_1
    const-string v1, "ConfigBrick"

    const-string v2, "section"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3040
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aj()Lflipboard/objs/ConfigSection;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    goto/16 :goto_0

    .line 3045
    :sswitch_2
    const-string v1, "ConfigBrick"

    const-string v2, "show"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3046
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 3073
    const-string v1, "ConfigBrick"

    const-string v2, "show"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3048
    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    .line 3062
    const-string v1, "ConfigBrick"

    const-string v2, "showA"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3050
    :sswitch_4
    const-string v1, "ConfigBrick"

    const-string v2, "showAddButton"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3051
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigBrick;->j:Z

    goto/16 :goto_0

    .line 3056
    :sswitch_5
    const-string v1, "ConfigBrick"

    const-string v2, "showAuthor"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3057
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigBrick;->i:Z

    goto/16 :goto_0

    .line 3067
    :sswitch_6
    const-string v1, "ConfigBrick"

    const-string v2, "showTitle"

    const/4 v3, 0x5

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3068
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigBrick;->h:Z

    goto/16 :goto_0

    .line 3079
    :sswitch_7
    const-string v1, "ConfigBrick"

    const-string v2, "smallURL"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3080
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigBrick;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 3090
    :pswitch_7
    const-string v1, "ConfigBrick"

    const-string v2, "width"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3091
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigBrick;->e:I

    goto/16 :goto_0

    .line 3101
    :sswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 3115
    :goto_1
    return-object v0

    .line 3108
    :sswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 3115
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 3003
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_9
        0xa -> :sswitch_9
        0xd -> :sswitch_9
        0x20 -> :sswitch_9
        0x22 -> :sswitch_0
        0x2c -> :sswitch_9
        0x7d -> :sswitch_8
    .end sparse-switch

    .line 3005
    :pswitch_data_0
    .packed-switch 0x68
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch

    .line 3037
    :sswitch_data_1
    .sparse-switch
        0x65 -> :sswitch_1
        0x68 -> :sswitch_2
        0x6d -> :sswitch_7
    .end sparse-switch

    .line 3046
    :sswitch_data_2
    .sparse-switch
        0x41 -> :sswitch_3
        0x54 -> :sswitch_6
    .end sparse-switch

    .line 3048
    :sswitch_data_3
    .sparse-switch
        0x64 -> :sswitch_4
        0x75 -> :sswitch_5
    .end sparse-switch
.end method

.method private ac()Lflipboard/objs/ConfigEdition;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 3359
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3360
    new-instance v0, Lflipboard/objs/ConfigEdition;

    invoke-direct {v0}, Lflipboard/objs/ConfigEdition;-><init>()V

    .line 3362
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 3412
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 3364
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 3397
    const-string v1, "ConfigEdition"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3366
    :sswitch_1
    const-string v1, "ConfigEdition"

    const-string v2, "currentEdition"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3367
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigEdition;->d:Z

    goto :goto_0

    .line 3372
    :sswitch_2
    const-string v1, "ConfigEdition"

    const-string v2, "displayName"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3373
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigEdition;->a:Ljava/lang/String;

    goto :goto_0

    .line 3378
    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 3392
    const-string v1, "ConfigEdition"

    const-string v2, "l"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3380
    :sswitch_4
    const-string v1, "ConfigEdition"

    const-string v2, "language"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3381
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigEdition;->c:Ljava/lang/String;

    goto :goto_0

    .line 3386
    :sswitch_5
    const-string v1, "ConfigEdition"

    const-string v2, "locale"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3387
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigEdition;->b:Ljava/lang/String;

    goto :goto_0

    .line 3402
    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 3416
    :goto_1
    return-object v0

    .line 3409
    :sswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 3416
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 3362
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_7
        0xa -> :sswitch_7
        0xd -> :sswitch_7
        0x20 -> :sswitch_7
        0x22 -> :sswitch_0
        0x2c -> :sswitch_7
        0x7d -> :sswitch_6
    .end sparse-switch

    .line 3364
    :sswitch_data_1
    .sparse-switch
        0x63 -> :sswitch_1
        0x64 -> :sswitch_2
        0x6c -> :sswitch_3
    .end sparse-switch

    .line 3378
    :sswitch_data_2
    .sparse-switch
        0x61 -> :sswitch_4
        0x6f -> :sswitch_5
    .end sparse-switch
.end method

.method private ad()Lflipboard/objs/ConfigFolder;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 3519
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3520
    new-instance v0, Lflipboard/objs/ConfigFolder;

    invoke-direct {v0}, Lflipboard/objs/ConfigFolder;-><init>()V

    .line 3522
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 3621
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 3524
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 3606
    :pswitch_0
    const-string v1, "ConfigFolder"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3526
    :pswitch_1
    const-string v1, "ConfigFolder"

    const-string v2, "categoryTitle"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3527
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigFolder;->g:Ljava/lang/String;

    goto :goto_0

    .line 3532
    :pswitch_2
    const-string v1, "ConfigFolder"

    const-string v2, "description"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3533
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigFolder;->bQ:Ljava/lang/String;

    goto :goto_0

    .line 3538
    :pswitch_3
    const-string v1, "ConfigFolder"

    const-string v2, "groupid"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3539
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigFolder;->a:Ljava/lang/String;

    goto :goto_0

    .line 3544
    :pswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 3558
    const-string v1, "ConfigFolder"

    const-string v2, "i"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3546
    :sswitch_1
    const-string v1, "ConfigFolder"

    const-string v2, "icon"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3547
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigFolder;->bR:Ljava/lang/String;

    goto :goto_0

    .line 3552
    :sswitch_2
    const-string v1, "ConfigFolder"

    const-string v2, "isFavicon"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3553
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigFolder;->bS:Z

    goto :goto_0

    .line 3563
    :pswitch_5
    const-string v1, "ConfigFolder"

    const-string v2, "language"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3564
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigFolder;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 3569
    :pswitch_6
    const-string v1, "ConfigFolder"

    const-string v2, "minVersion"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3570
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigFolder;->b:I

    goto/16 :goto_0

    .line 3575
    :pswitch_7
    const-string v1, "ConfigFolder"

    const-string v2, "remoteid"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3576
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigFolder;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 3581
    :pswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 3595
    :pswitch_9
    const-string v1, "ConfigFolder"

    const-string v2, "s"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3583
    :pswitch_a
    const-string v1, "ConfigFolder"

    const-string v2, "sections"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3584
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ak()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigFolder;->d:Ljava/util/List;

    goto/16 :goto_0

    .line 3589
    :pswitch_b
    const-string v1, "ConfigFolder"

    const-string v2, "showInline"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3590
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigFolder;->c:Z

    goto/16 :goto_0

    .line 3600
    :pswitch_c
    const-string v1, "ConfigFolder"

    const-string v2, "title"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3601
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigFolder;->bP:Ljava/lang/String;

    goto/16 :goto_0

    .line 3611
    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 3625
    :goto_1
    return-object v0

    .line 3618
    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 3625
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 3522
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_4
        0xa -> :sswitch_4
        0xd -> :sswitch_4
        0x20 -> :sswitch_4
        0x22 -> :sswitch_0
        0x2c -> :sswitch_4
        0x7d -> :sswitch_3
    .end sparse-switch

    .line 3524
    :pswitch_data_0
    .packed-switch 0x63
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_c
    .end packed-switch

    .line 3544
    :sswitch_data_1
    .sparse-switch
        0x63 -> :sswitch_1
        0x73 -> :sswitch_2
    .end sparse-switch

    .line 3581
    :pswitch_data_1
    .packed-switch 0x65
        :pswitch_a
        :pswitch_9
        :pswitch_9
        :pswitch_b
    .end packed-switch
.end method

.method private ae()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigFolder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3630
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3631
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3633
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 3649
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 3636
    :sswitch_0
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ad()Lflipboard/objs/ConfigFolder;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3639
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 3653
    :goto_1
    return-object v0

    .line 3646
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 3653
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 3633
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2c -> :sswitch_2
        0x5d -> :sswitch_1
        0x6e -> :sswitch_0
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method private af()Lflipboard/objs/ConfigHints$Condition;
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 3884
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3885
    new-instance v0, Lflipboard/objs/ConfigHints$Condition;

    invoke-direct {v0}, Lflipboard/objs/ConfigHints$Condition;-><init>()V

    .line 3887
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 3945
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 3889
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 3930
    const-string v1, "ConfigHints$Condition"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3891
    :sswitch_1
    const-string v1, "ConfigHints$Condition"

    const-string v2, "action"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3892
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Condition;->a:Ljava/lang/String;

    goto :goto_0

    .line 3897
    :sswitch_2
    const-string v1, "ConfigHints$Condition"

    const-string v2, "co"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3898
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 3912
    const-string v1, "ConfigHints$Condition"

    const-string v2, "co"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3900
    :pswitch_0
    const-string v1, "ConfigHints$Condition"

    const-string v2, "comparison"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3901
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Condition;->c:Ljava/lang/String;

    goto :goto_0

    .line 3906
    :pswitch_1
    const-string v1, "ConfigHints$Condition"

    const-string v2, "conditions"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3907
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ag()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Condition;->e:Ljava/util/List;

    goto :goto_0

    .line 3918
    :sswitch_3
    const-string v1, "ConfigHints$Condition"

    const-string v2, "type"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3919
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Condition;->b:Ljava/lang/String;

    goto :goto_0

    .line 3924
    :sswitch_4
    const-string v1, "ConfigHints$Condition"

    const-string v2, "value"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3925
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigHints$Condition;->d:F

    goto/16 :goto_0

    .line 3935
    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 3949
    :goto_1
    return-object v0

    .line 3942
    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 3949
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 3887
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_6
        0xa -> :sswitch_6
        0xd -> :sswitch_6
        0x20 -> :sswitch_6
        0x22 -> :sswitch_0
        0x2c -> :sswitch_6
        0x7d -> :sswitch_5
    .end sparse-switch

    .line 3889
    :sswitch_data_1
    .sparse-switch
        0x61 -> :sswitch_1
        0x63 -> :sswitch_2
        0x74 -> :sswitch_3
        0x76 -> :sswitch_4
    .end sparse-switch

    .line 3898
    :pswitch_data_0
    .packed-switch 0x6d
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private ag()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigHints$Condition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3954
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3955
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3957
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 3973
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 3960
    :sswitch_0
    invoke-direct {p0}, Lflipboard/json/JSONParser;->af()Lflipboard/objs/ConfigHints$Condition;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3963
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 3977
    :goto_1
    return-object v0

    .line 3970
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 3977
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 3957
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2c -> :sswitch_2
        0x5d -> :sswitch_1
        0x6e -> :sswitch_0
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method private ah()Lflipboard/objs/ConfigHints$Hint;
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 4052
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4053
    new-instance v0, Lflipboard/objs/ConfigHints$Hint;

    invoke-direct {v0}, Lflipboard/objs/ConfigHints$Hint;-><init>()V

    .line 4055
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 4367
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 4057
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 4352
    :pswitch_0
    const-string v1, "ConfigHints$Hint"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4059
    :pswitch_1
    const-string v1, "ConfigHints$Hint"

    const-string v2, "actionTitle"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4060
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 4074
    const-string v1, "ConfigHints$Hint"

    const-string v2, "actionTitle"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4062
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4063
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->G:Ljava/lang/String;

    goto :goto_0

    .line 4068
    :sswitch_2
    const-string v1, "ConfigHints$Hint"

    const-string v2, "actionTitleKey"

    const/16 v3, 0xc

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4069
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->t:Ljava/lang/String;

    goto :goto_0

    .line 4080
    :pswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 4094
    const-string v1, "ConfigHints$Hint"

    const-string v2, "b"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4082
    :sswitch_3
    const-string v1, "ConfigHints$Hint"

    const-string v2, "backgroundOpacity"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4083
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigHints$Hint;->z:F

    goto :goto_0

    .line 4088
    :sswitch_4
    const-string v1, "ConfigHints$Hint"

    const-string v2, "bubbleDistance"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4089
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigHints$Hint;->y:F

    goto/16 :goto_0

    .line 4099
    :pswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    .line 4113
    const-string v1, "ConfigHints$Hint"

    const-string v2, "c"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4101
    :sswitch_5
    const-string v1, "ConfigHints$Hint"

    const-string v2, "cancelTitleKey"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4102
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->u:Ljava/lang/String;

    goto/16 :goto_0

    .line 4107
    :sswitch_6
    const-string v1, "ConfigHints$Hint"

    const-string v2, "conditions"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4108
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ag()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->E:Ljava/util/List;

    goto/16 :goto_0

    .line 4118
    :pswitch_4
    const-string v1, "ConfigHints$Hint"

    const-string v2, "duration"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4119
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigHints$Hint;->v:F

    goto/16 :goto_0

    .line 4124
    :pswitch_5
    const-string v1, "ConfigHints$Hint"

    const-string v2, "enabled"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4125
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigHints$Hint;->d:Z

    goto/16 :goto_0

    .line 4130
    :pswitch_6
    const-string v1, "ConfigHints$Hint"

    const-string v2, "hidePulse"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4131
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigHints$Hint;->A:Z

    goto/16 :goto_0

    .line 4136
    :pswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4

    .line 4169
    const-string v1, "ConfigHints$Hint"

    const-string v2, "i"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4138
    :sswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_5

    .line 4152
    const-string v1, "ConfigHints$Hint"

    const-string v2, "id"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4140
    :sswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4141
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 4146
    :sswitch_9
    const-string v1, "ConfigHints$Hint"

    const-string v2, "idioms"

    const/4 v3, 0x3

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4147
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->g:Ljava/util/List;

    goto/16 :goto_0

    .line 4157
    :sswitch_a
    const-string v1, "ConfigHints$Hint"

    const-string v2, "ignoresGlobalCaps"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4158
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigHints$Hint;->p:Z

    goto/16 :goto_0

    .line 4163
    :sswitch_b
    const-string v1, "ConfigHints$Hint"

    const-string v2, "inclusionConditions"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4164
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ag()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->j:Ljava/util/List;

    goto/16 :goto_0

    .line 4174
    :pswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_6

    .line 4188
    const-string v1, "ConfigHints$Hint"

    const-string v2, "l"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4176
    :sswitch_c
    const-string v1, "ConfigHints$Hint"

    const-string v2, "languages"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4177
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->h:Ljava/util/List;

    goto/16 :goto_0

    .line 4182
    :sswitch_d
    const-string v1, "ConfigHints$Hint"

    const-string v2, "locales"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4183
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->i:Ljava/util/List;

    goto/16 :goto_0

    .line 4193
    :pswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_7

    .line 4213
    const-string v1, "ConfigHints$Hint"

    const-string v2, "m"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4195
    :sswitch_e
    const-string v1, "ConfigHints$Hint"

    const-string v2, "maxVersion"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4196
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigHints$Hint;->f:I

    goto/16 :goto_0

    .line 4201
    :sswitch_f
    const-string v1, "ConfigHints$Hint"

    const-string v2, "minVersion"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4202
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigHints$Hint;->e:I

    goto/16 :goto_0

    .line 4207
    :sswitch_10
    const-string v1, "ConfigHints$Hint"

    const-string v2, "modal"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4208
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigHints$Hint;->m:Z

    goto/16 :goto_0

    .line 4218
    :pswitch_a
    const-string v1, "ConfigHints$Hint"

    const-string v2, "overlay"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4219
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->D:Ljava/lang/String;

    goto/16 :goto_0

    .line 4224
    :pswitch_b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_8

    .line 4259
    const-string v1, "ConfigHints$Hint"

    const-string v2, "p"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4226
    :sswitch_11
    const-string v1, "ConfigHints$Hint"

    const-string v2, "postPresentationDelay"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4227
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigHints$Hint;->o:F

    goto/16 :goto_0

    .line 4232
    :sswitch_12
    const-string v1, "ConfigHints$Hint"

    const-string v2, "presentationDelay"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4233
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigHints$Hint;->n:F

    goto/16 :goto_0

    .line 4238
    :sswitch_13
    const-string v1, "ConfigHints$Hint"

    const-string v2, "pulse"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4239
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_9

    .line 4253
    const-string v1, "ConfigHints$Hint"

    const-string v2, "pulse"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4241
    :sswitch_14
    const-string v1, "ConfigHints$Hint"

    const-string v2, "pulseOpacity"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4242
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigHints$Hint;->w:F

    goto/16 :goto_0

    .line 4247
    :sswitch_15
    const-string v1, "ConfigHints$Hint"

    const-string v2, "pulseSize"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4248
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigHints$Hint;->x:F

    goto/16 :goto_0

    .line 4264
    :pswitch_c
    const-string v1, "ConfigHints$Hint"

    const-string v2, "subtitle"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4265
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_a

    .line 4279
    const-string v1, "ConfigHints$Hint"

    const-string v2, "subtitle"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4267
    :sswitch_16
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4268
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->H:Ljava/lang/String;

    goto/16 :goto_0

    .line 4273
    :sswitch_17
    const-string v1, "ConfigHints$Hint"

    const-string v2, "subtitleKey"

    const/16 v3, 0x9

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4274
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 4285
    :pswitch_d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_b

    .line 4341
    const-string v1, "ConfigHints$Hint"

    const-string v2, "t"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4287
    :sswitch_18
    const-string v1, "ConfigHints$Hint"

    const-string v2, "tag"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4288
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_c

    .line 4302
    const-string v1, "ConfigHints$Hint"

    const-string v2, "tag"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4290
    :sswitch_19
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4291
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->B:Ljava/lang/String;

    goto/16 :goto_0

    .line 4296
    :sswitch_1a
    const-string v1, "ConfigHints$Hint"

    const-string v2, "tags"

    const/4 v3, 0x4

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4297
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->C:Ljava/util/List;

    goto/16 :goto_0

    .line 4308
    :sswitch_1b
    const-string v1, "ConfigHints$Hint"

    const-string v2, "title"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4309
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_d

    .line 4323
    const-string v1, "ConfigHints$Hint"

    const-string v2, "title"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4311
    :sswitch_1c
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4312
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->I:Ljava/lang/String;

    goto/16 :goto_0

    .line 4317
    :sswitch_1d
    const-string v1, "ConfigHints$Hint"

    const-string v2, "titleKey"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4318
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 4329
    :sswitch_1e
    const-string v1, "ConfigHints$Hint"

    const-string v2, "touchInitiallyEnabled"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4330
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigHints$Hint;->F:Z

    goto/16 :goto_0

    .line 4335
    :sswitch_1f
    const-string v1, "ConfigHints$Hint"

    const-string v2, "type"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4336
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 4346
    :pswitch_e
    const-string v1, "ConfigHints$Hint"

    const-string v2, "url"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4347
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigHints$Hint;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 4357
    :sswitch_20
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 4371
    :goto_1
    return-object v0

    .line 4364
    :sswitch_21
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 4371
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 4055
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_21
        0xa -> :sswitch_21
        0xd -> :sswitch_21
        0x20 -> :sswitch_21
        0x22 -> :sswitch_0
        0x2c -> :sswitch_21
        0x7d -> :sswitch_20
    .end sparse-switch

    .line 4057
    :pswitch_data_0
    .packed-switch 0x61
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 4060
    :sswitch_data_1
    .sparse-switch
        0x22 -> :sswitch_1
        0x4b -> :sswitch_2
    .end sparse-switch

    .line 4080
    :sswitch_data_2
    .sparse-switch
        0x61 -> :sswitch_3
        0x75 -> :sswitch_4
    .end sparse-switch

    .line 4099
    :sswitch_data_3
    .sparse-switch
        0x61 -> :sswitch_5
        0x6f -> :sswitch_6
    .end sparse-switch

    .line 4136
    :sswitch_data_4
    .sparse-switch
        0x64 -> :sswitch_7
        0x67 -> :sswitch_a
        0x6e -> :sswitch_b
    .end sparse-switch

    .line 4138
    :sswitch_data_5
    .sparse-switch
        0x22 -> :sswitch_8
        0x69 -> :sswitch_9
    .end sparse-switch

    .line 4174
    :sswitch_data_6
    .sparse-switch
        0x61 -> :sswitch_c
        0x6f -> :sswitch_d
    .end sparse-switch

    .line 4193
    :sswitch_data_7
    .sparse-switch
        0x61 -> :sswitch_e
        0x69 -> :sswitch_f
        0x6f -> :sswitch_10
    .end sparse-switch

    .line 4224
    :sswitch_data_8
    .sparse-switch
        0x6f -> :sswitch_11
        0x72 -> :sswitch_12
        0x75 -> :sswitch_13
    .end sparse-switch

    .line 4239
    :sswitch_data_9
    .sparse-switch
        0x4f -> :sswitch_14
        0x53 -> :sswitch_15
    .end sparse-switch

    .line 4265
    :sswitch_data_a
    .sparse-switch
        0x22 -> :sswitch_16
        0x4b -> :sswitch_17
    .end sparse-switch

    .line 4285
    :sswitch_data_b
    .sparse-switch
        0x61 -> :sswitch_18
        0x69 -> :sswitch_1b
        0x6f -> :sswitch_1e
        0x79 -> :sswitch_1f
    .end sparse-switch

    .line 4288
    :sswitch_data_c
    .sparse-switch
        0x22 -> :sswitch_19
        0x73 -> :sswitch_1a
    .end sparse-switch

    .line 4309
    :sswitch_data_d
    .sparse-switch
        0x22 -> :sswitch_1c
        0x4b -> :sswitch_1d
    .end sparse-switch
.end method

.method private ai()Lflipboard/objs/ConfigPopularSearches$Category;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 4615
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4616
    new-instance v0, Lflipboard/objs/ConfigPopularSearches$Category;

    invoke-direct {v0}, Lflipboard/objs/ConfigPopularSearches$Category;-><init>()V

    .line 4618
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 4649
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 4620
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 4634
    const-string v1, "ConfigPopularSearches$Category"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4622
    :sswitch_1
    const-string v1, "ConfigPopularSearches$Category"

    const-string v2, "examples"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4623
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigPopularSearches$Category;->b:Ljava/util/List;

    goto :goto_0

    .line 4628
    :sswitch_2
    const-string v1, "ConfigPopularSearches$Category"

    const-string v2, "title"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4629
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigPopularSearches$Category;->a:Ljava/lang/String;

    goto :goto_0

    .line 4639
    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 4653
    :goto_1
    return-object v0

    .line 4646
    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 4653
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 4618
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_4
        0xa -> :sswitch_4
        0xd -> :sswitch_4
        0x20 -> :sswitch_4
        0x22 -> :sswitch_0
        0x2c -> :sswitch_4
        0x7d -> :sswitch_3
    .end sparse-switch

    .line 4620
    :sswitch_data_1
    .sparse-switch
        0x65 -> :sswitch_1
        0x74 -> :sswitch_2
    .end sparse-switch
.end method

.method private aj()Lflipboard/objs/ConfigSection;
    .locals 9

    .prologue
    const/4 v8, 0x7

    const/16 v7, 0xf

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 4756
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4757
    new-instance v0, Lflipboard/objs/ConfigSection;

    invoke-direct {v0}, Lflipboard/objs/ConfigSection;-><init>()V

    .line 4759
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 5036
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 4761
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 5021
    :pswitch_0
    const-string v1, "ConfigSection"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4763
    :pswitch_1
    const-string v1, "ConfigSection"

    const-string v2, "author"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4764
    invoke-direct {p0}, Lflipboard/json/JSONParser;->Y()Lflipboard/objs/Author;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->l:Lflipboard/objs/Author;

    goto :goto_0

    .line 4769
    :pswitch_2
    const-string v1, "ConfigSection"

    const-string v2, "brick"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4770
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ab()Lflipboard/objs/ConfigBrick;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    goto :goto_0

    .line 4775
    :pswitch_3
    const-string v1, "ConfigSection"

    const-string v2, "de"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4776
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 4790
    :pswitch_4
    const-string v1, "ConfigSection"

    const-string v2, "de"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4778
    :pswitch_5
    const-string v1, "ConfigSection"

    const-string v2, "description"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4779
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->bQ:Ljava/lang/String;

    goto :goto_0

    .line 4784
    :pswitch_6
    const-string v1, "ConfigSection"

    const-string v2, "device"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4785
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->c:Ljava/lang/String;

    goto :goto_0

    .line 4796
    :pswitch_7
    const-string v1, "ConfigSection"

    const-string v2, "enumerated"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4797
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigSection;->x:Z

    goto/16 :goto_0

    .line 4802
    :pswitch_8
    const-string v1, "ConfigSection"

    const-string v2, "feedType"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4803
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 4808
    :pswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 4843
    const-string v1, "ConfigSection"

    const-string v2, "i"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4810
    :sswitch_1
    const-string v1, "ConfigSection"

    const-string v2, "icon"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4811
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->bR:Ljava/lang/String;

    goto/16 :goto_0

    .line 4816
    :sswitch_2
    const-string v1, "ConfigSection"

    const-string v2, "imageURL"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4817
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 4831
    const-string v1, "ConfigSection"

    const-string v2, "imageURL"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4819
    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4820
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->t:Ljava/lang/String;

    goto/16 :goto_0

    .line 4825
    :sswitch_4
    const-string v1, "ConfigSection"

    const-string v2, "imageURLHiddenInList"

    const/16 v3, 0x9

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4826
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigSection;->m:Z

    goto/16 :goto_0

    .line 4837
    :sswitch_5
    const-string v1, "ConfigSection"

    const-string v2, "isFavicon"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4838
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigSection;->bS:Z

    goto/16 :goto_0

    .line 4848
    :pswitch_a
    const-string v1, "ConfigSection"

    const-string v2, "keywords"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4849
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 4854
    :pswitch_b
    const-string v1, "ConfigSection"

    const-string v2, "liveTile"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4855
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigSection;->n:Z

    goto/16 :goto_0

    .line 4860
    :pswitch_c
    const-string v1, "ConfigSection"

    const-string v2, "magazineTarget"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4861
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 4866
    :pswitch_d
    const-string v1, "ConfigSection"

    const-string v2, "private"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4867
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigSection;->y:Z

    goto/16 :goto_0

    .line 4872
    :pswitch_e
    const-string v1, "ConfigSection"

    const-string v2, "remoteid"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4873
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->R()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->p:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4878
    :pswitch_f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    .line 4932
    const-string v1, "ConfigSection"

    const-string v2, "s"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4880
    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4

    .line 4909
    const-string v1, "ConfigSection"

    const-string v2, "se"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4882
    :sswitch_7
    const-string v1, "ConfigSection"

    const-string v2, "section"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4883
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_5

    .line 4897
    const-string v1, "ConfigSection"

    const-string v2, "section"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4885
    :sswitch_8
    const-string v1, "ConfigSection"

    const-string v2, "sectionTitle"

    const/16 v3, 0x8

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4886
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 4891
    :sswitch_9
    const-string v1, "ConfigSection"

    const-string v2, "sections"

    const/16 v3, 0x8

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4892
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ak()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->d:Ljava/util/List;

    goto/16 :goto_0

    .line 4903
    :sswitch_a
    const-string v1, "ConfigSection"

    const-string v2, "service"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4904
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 4914
    :sswitch_b
    const-string v1, "ConfigSection"

    const-string v2, "showLogotypeImage"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4915
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigSection;->w:Z

    goto/16 :goto_0

    .line 4920
    :sswitch_c
    const-string v1, "ConfigSection"

    const-string v2, "socialId"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4921
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 4926
    :sswitch_d
    const-string v1, "ConfigSection"

    const-string v2, "subhead"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4927
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 4937
    :pswitch_10
    const-string v1, "ConfigSection"

    const-string v2, "ti"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4938
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_6

    .line 4988
    const-string v1, "ConfigSection"

    const-string v2, "ti"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4940
    :sswitch_e
    const-string v1, "ConfigSection"

    const-string v2, "tileBrickImage"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4941
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_7

    .line 4961
    const-string v1, "ConfigSection"

    const-string v2, "tileBrickImage"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4943
    :sswitch_f
    const-string v1, "ConfigSection"

    const-string v2, "tileBrickImageLargeURL"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4944
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 4949
    :sswitch_10
    const-string v1, "ConfigSection"

    const-string v2, "tileBrickImageMediumURL"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4950
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 4955
    :sswitch_11
    const-string v1, "ConfigSection"

    const-string v2, "tileBrickImageSmallURL"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4956
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 4967
    :sswitch_12
    const-string v1, "ConfigSection"

    const-string v2, "title"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4968
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_8

    .line 4982
    const-string v1, "ConfigSection"

    const-string v2, "title"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4970
    :sswitch_13
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4971
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->bP:Ljava/lang/String;

    goto/16 :goto_0

    .line 4976
    :sswitch_14
    const-string v1, "ConfigSection"

    const-string v2, "titleSuffix"

    const/4 v3, 0x6

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4977
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 4994
    :pswitch_11
    const-string v1, "ConfigSection"

    const-string v2, "unread"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4995
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_9

    .line 5009
    const-string v1, "ConfigSection"

    const-string v2, "unread"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4997
    :sswitch_15
    const-string v1, "ConfigSection"

    const-string v2, "unreadCount"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4998
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigSection;->v:I

    goto/16 :goto_0

    .line 5003
    :sswitch_16
    const-string v1, "ConfigSection"

    const-string v2, "unreadRemoteid"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5004
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigSection;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 5015
    :pswitch_12
    const-string v1, "ConfigSection"

    const-string v2, "verified"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5016
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigSection;->u:Z

    goto/16 :goto_0

    .line 5026
    :sswitch_17
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 5040
    :goto_1
    return-object v0

    .line 5033
    :sswitch_18
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 5040
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 4759
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_18
        0xa -> :sswitch_18
        0xd -> :sswitch_18
        0x20 -> :sswitch_18
        0x22 -> :sswitch_0
        0x2c -> :sswitch_18
        0x7d -> :sswitch_17
    .end sparse-switch

    .line 4761
    :pswitch_data_0
    .packed-switch 0x61
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch

    .line 4776
    :pswitch_data_1
    .packed-switch 0x73
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_6
    .end packed-switch

    .line 4808
    :sswitch_data_1
    .sparse-switch
        0x63 -> :sswitch_1
        0x6d -> :sswitch_2
        0x73 -> :sswitch_5
    .end sparse-switch

    .line 4817
    :sswitch_data_2
    .sparse-switch
        0x22 -> :sswitch_3
        0x48 -> :sswitch_4
    .end sparse-switch

    .line 4878
    :sswitch_data_3
    .sparse-switch
        0x65 -> :sswitch_6
        0x68 -> :sswitch_b
        0x6f -> :sswitch_c
        0x75 -> :sswitch_d
    .end sparse-switch

    .line 4880
    :sswitch_data_4
    .sparse-switch
        0x63 -> :sswitch_7
        0x72 -> :sswitch_a
    .end sparse-switch

    .line 4883
    :sswitch_data_5
    .sparse-switch
        0x54 -> :sswitch_8
        0x73 -> :sswitch_9
    .end sparse-switch

    .line 4938
    :sswitch_data_6
    .sparse-switch
        0x6c -> :sswitch_e
        0x74 -> :sswitch_12
    .end sparse-switch

    .line 4941
    :sswitch_data_7
    .sparse-switch
        0x4c -> :sswitch_f
        0x4d -> :sswitch_10
        0x53 -> :sswitch_11
    .end sparse-switch

    .line 4968
    :sswitch_data_8
    .sparse-switch
        0x22 -> :sswitch_13
        0x53 -> :sswitch_14
    .end sparse-switch

    .line 4995
    :sswitch_data_9
    .sparse-switch
        0x43 -> :sswitch_15
        0x52 -> :sswitch_16
    .end sparse-switch
.end method

.method private ak()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigSection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5045
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5046
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 5048
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 5064
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 5051
    :sswitch_0
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aj()Lflipboard/objs/ConfigSection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 5054
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 5068
    :goto_1
    return-object v0

    .line 5061
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 5068
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 5048
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2c -> :sswitch_2
        0x5d -> :sswitch_1
        0x6e -> :sswitch_0
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method private al()Lflipboard/objs/ConfigService;
    .locals 9

    .prologue
    const/16 v8, 0x9

    const/4 v7, 0x3

    const/4 v6, 0x5

    const/4 v5, 0x2

    const/4 v4, 0x7

    .line 5143
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5144
    new-instance v0, Lflipboard/objs/ConfigService;

    invoke-direct {v0}, Lflipboard/objs/ConfigService;-><init>()V

    .line 5146
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 6809
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 5148
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 6794
    :pswitch_0
    const-string v1, "ConfigService"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 5150
    :pswitch_1
    const-string v1, "ConfigService"

    const-string v2, "authentication"

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5151
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 5205
    const-string v1, "ConfigService"

    const-string v2, "authentication"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 5153
    :sswitch_1
    const-string v1, "ConfigService"

    const-string v2, "authenticationDomainURL"

    const/16 v3, 0xf

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5154
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 5168
    const-string v1, "ConfigService"

    const-string v2, "authenticationDomainURL"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 5156
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5157
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->V:Ljava/lang/String;

    goto :goto_0

    .line 5162
    :sswitch_3
    const-string v1, "ConfigService"

    const-string v2, "authenticationDomainURLs"

    const/16 v3, 0x18

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5163
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->d:Ljava/util/List;

    goto :goto_0

    .line 5174
    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    .line 5188
    const-string v1, "ConfigService"

    const-string v2, "authenticationE"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 5176
    :sswitch_5
    const-string v1, "ConfigService"

    const-string v2, "authenticationEndPoint"

    const/16 v3, 0x10

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5177
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->G:Ljava/lang/String;

    goto/16 :goto_0

    .line 5182
    :sswitch_6
    const-string v1, "ConfigService"

    const-string v2, "authenticationExternalURLs"

    const/16 v3, 0x10

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5183
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->e:Ljava/util/List;

    goto/16 :goto_0

    .line 5193
    :sswitch_7
    const-string v1, "ConfigService"

    const-string v2, "authenticationMode"

    const/16 v3, 0xf

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5194
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 5199
    :sswitch_8
    const-string v1, "ConfigService"

    const-string v2, "authenticationUserNameKey"

    const/16 v3, 0xf

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5200
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->ad:Ljava/lang/String;

    goto/16 :goto_0

    .line 5211
    :pswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4

    .line 5711
    const-string v1, "ConfigService"

    const-string v2, "c"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5213
    :sswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 5700
    :pswitch_3
    const-string v1, "ConfigService"

    const-string v2, "ca"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5215
    :pswitch_4
    const-string v1, "ConfigService"

    const-string v2, "callToAction"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5216
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_5

    .line 5669
    const-string v1, "ConfigService"

    const-string v2, "callToAction"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5218
    :sswitch_a
    const-string v1, "ConfigService"

    const-string v2, "callToActionButton"

    const/16 v3, 0xd

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5219
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_6

    .line 5323
    const-string v1, "ConfigService"

    const-string v2, "callToActionButton"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5221
    :sswitch_b
    const-string v1, "ConfigService"

    const-string v2, "callToActionButtonLinkForNon"

    const/16 v3, 0x13

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5222
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_7

    .line 5266
    const-string v1, "ConfigService"

    const-string v2, "callToActionButtonLinkForNon"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5224
    :sswitch_c
    const-string v1, "ConfigService"

    const-string v2, "callToActionButtonLinkForNonAuthenticated"

    const/16 v3, 0x1d

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5225
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_8

    .line 5239
    const-string v1, "ConfigService"

    const-string v2, "callToActionButtonLinkForNonAuthenticated"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5227
    :sswitch_d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5228
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aL:Ljava/lang/String;

    goto/16 :goto_0

    .line 5233
    :sswitch_e
    const-string v1, "ConfigService"

    const-string v2, "callToActionButtonLinkForNonAuthenticatedNoSub"

    const/16 v3, 0x2a

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5234
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bb:Ljava/lang/String;

    goto/16 :goto_0

    .line 5245
    :sswitch_f
    const-string v1, "ConfigService"

    const-string v2, "callToActionButtonLinkForNonSubscriber"

    const/16 v3, 0x1d

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5246
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_9

    .line 5260
    const-string v1, "ConfigService"

    const-string v2, "callToActionButtonLinkForNonSubscriber"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5248
    :sswitch_10
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5249
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aM:Ljava/lang/String;

    goto/16 :goto_0

    .line 5254
    :sswitch_11
    const-string v1, "ConfigService"

    const-string v2, "callToActionButtonLinkForNonSubscriberNoSub"

    const/16 v3, 0x27

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5255
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bc:Ljava/lang/String;

    goto/16 :goto_0

    .line 5272
    :sswitch_12
    const-string v1, "ConfigService"

    const-string v2, "callToActionButtonTitleForNon"

    const/16 v3, 0x13

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5273
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_a

    .line 5317
    const-string v1, "ConfigService"

    const-string v2, "callToActionButtonTitleForNon"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5275
    :sswitch_13
    const-string v1, "ConfigService"

    const-string v2, "callToActionButtonTitleForNonAuthenticated"

    const/16 v3, 0x1e

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5276
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_b

    .line 5290
    const-string v1, "ConfigService"

    const-string v2, "callToActionButtonTitleForNonAuthenticated"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5278
    :sswitch_14
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5279
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aJ:Ljava/lang/String;

    goto/16 :goto_0

    .line 5284
    :sswitch_15
    const-string v1, "ConfigService"

    const-string v2, "callToActionButtonTitleForNonAuthenticatedNoSub"

    const/16 v3, 0x2b

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5285
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aZ:Ljava/lang/String;

    goto/16 :goto_0

    .line 5296
    :sswitch_16
    const-string v1, "ConfigService"

    const-string v2, "callToActionButtonTitleForNonSubscriber"

    const/16 v3, 0x1e

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5297
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_c

    .line 5311
    const-string v1, "ConfigService"

    const-string v2, "callToActionButtonTitleForNonSubscriber"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5299
    :sswitch_17
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5300
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aK:Ljava/lang/String;

    goto/16 :goto_0

    .line 5305
    :sswitch_18
    const-string v1, "ConfigService"

    const-string v2, "callToActionButtonTitleForNonSubscriberNoSub"

    const/16 v3, 0x28

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5306
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->ba:Ljava/lang/String;

    goto/16 :goto_0

    .line 5329
    :sswitch_19
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPrompt"

    const/16 v3, 0xd

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5330
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_2

    .line 5486
    :pswitch_5
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPrompt"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5332
    :pswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_d

    .line 5418
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptS"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5334
    :sswitch_1a
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptSignIn"

    const/16 v3, 0x18

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5335
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_e

    .line 5370
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptSignIn"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5337
    :sswitch_1b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5338
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aB:Ljava/lang/String;

    goto/16 :goto_0

    .line 5343
    :sswitch_1c
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptSignInBoldSubstring"

    const/16 v3, 0x1d

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5344
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_f

    .line 5358
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptSignInBoldSubstring"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5346
    :sswitch_1d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5347
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aC:Ljava/lang/String;

    goto/16 :goto_0

    .line 5352
    :sswitch_1e
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptSignInBoldSubstringNoSub"

    const/16 v3, 0x2a

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5353
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aU:Ljava/lang/String;

    goto/16 :goto_0

    .line 5364
    :sswitch_1f
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptSignInNoSub"

    const/16 v3, 0x1d

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5365
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aT:Ljava/lang/String;

    goto/16 :goto_0

    .line 5376
    :sswitch_20
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptSubscribe"

    const/16 v3, 0x18

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5377
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_10

    .line 5412
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptSubscribe"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5379
    :sswitch_21
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5380
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aH:Ljava/lang/String;

    goto/16 :goto_0

    .line 5385
    :sswitch_22
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptSubscribeBoldSubstring"

    const/16 v3, 0x20

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5386
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_11

    .line 5400
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptSubscribeBoldSubstring"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5388
    :sswitch_23
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5389
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aI:Ljava/lang/String;

    goto/16 :goto_0

    .line 5394
    :sswitch_24
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptSubscribeBoldSubstringNoSub"

    const/16 v3, 0x2d

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5395
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aY:Ljava/lang/String;

    goto/16 :goto_0

    .line 5406
    :sswitch_25
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptSubscribeNoSub"

    const/16 v3, 0x20

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5407
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aX:Ljava/lang/String;

    goto/16 :goto_0

    .line 5423
    :pswitch_7
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptUpgrade"

    const/16 v3, 0x17

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5424
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_12

    .line 5459
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptUpgrade"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5426
    :sswitch_26
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5427
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aF:Ljava/lang/String;

    goto/16 :goto_0

    .line 5432
    :sswitch_27
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptUpgradeBoldSubstring"

    const/16 v3, 0x1e

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5433
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_13

    .line 5447
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptUpgradeBoldSubstring"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5435
    :sswitch_28
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5436
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aG:Ljava/lang/String;

    goto/16 :goto_0

    .line 5441
    :sswitch_29
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptUpgradeBoldSubstringNoSub"

    const/16 v3, 0x2b

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5442
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aW:Ljava/lang/String;

    goto/16 :goto_0

    .line 5453
    :sswitch_2a
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptUpgradeNoSub"

    const/16 v3, 0x1e

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5454
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aV:Ljava/lang/String;

    goto/16 :goto_0

    .line 5465
    :pswitch_8
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptWantFullAccess"

    const/16 v3, 0x17

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5466
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_14

    .line 5480
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptWantFullAccess"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5468
    :sswitch_2b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5469
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aD:Ljava/lang/String;

    goto/16 :goto_0

    .line 5474
    :sswitch_2c
    const-string v1, "ConfigService"

    const-string v2, "callToActionLeadPromptWantFullAccessBoldSubstring"

    const/16 v3, 0x25

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5475
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aE:Ljava/lang/String;

    goto/16 :goto_0

    .line 5492
    :sswitch_2d
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePrompt"

    const/16 v3, 0xd

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5493
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_15

    .line 5612
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePrompt"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5495
    :sswitch_2e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5496
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bd:Ljava/lang/String;

    goto/16 :goto_0

    .line 5501
    :sswitch_2f
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptFor"

    const/16 v3, 0x1c

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5502
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_3

    .line 5606
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptFor"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5504
    :pswitch_9
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptForSmartphone"

    const/16 v3, 0x1f

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5505
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_16

    .line 5549
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptForSmartphone"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5507
    :sswitch_30
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptForSmartphoneAuthenticated"

    const/16 v3, 0x29

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5508
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_17

    .line 5522
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptForSmartphoneAuthenticated"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5510
    :sswitch_31
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5511
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aQ:Ljava/lang/String;

    goto/16 :goto_0

    .line 5516
    :sswitch_32
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptForSmartphoneAuthenticatedNoSub"

    const/16 v3, 0x36

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5517
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bh:Ljava/lang/String;

    goto/16 :goto_0

    .line 5528
    :sswitch_33
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptForSmartphoneNonAuthenticated"

    const/16 v3, 0x29

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5529
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_18

    .line 5543
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptForSmartphoneNonAuthenticated"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5531
    :sswitch_34
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5532
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aO:Ljava/lang/String;

    goto/16 :goto_0

    .line 5537
    :sswitch_35
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptForSmartphoneNonAuthenticatedNoSub"

    const/16 v3, 0x39

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5538
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bf:Ljava/lang/String;

    goto/16 :goto_0

    .line 5555
    :pswitch_a
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptForTablet"

    const/16 v3, 0x1f

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5556
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_19

    .line 5600
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptForTablet"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5558
    :sswitch_36
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptForTabletAuthenticated"

    const/16 v3, 0x25

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5559
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1a

    .line 5573
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptForTabletAuthenticated"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5561
    :sswitch_37
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5562
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aP:Ljava/lang/String;

    goto/16 :goto_0

    .line 5567
    :sswitch_38
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptForTabletAuthenticatedNoSub"

    const/16 v3, 0x32

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5568
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bg:Ljava/lang/String;

    goto/16 :goto_0

    .line 5579
    :sswitch_39
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptForTabletNonAuthenticated"

    const/16 v3, 0x25

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5580
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1b

    .line 5594
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptForTabletNonAuthenticated"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5582
    :sswitch_3a
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5583
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aN:Ljava/lang/String;

    goto/16 :goto_0

    .line 5588
    :sswitch_3b
    const-string v1, "ConfigService"

    const-string v2, "callToActionSubscribePromptForTabletNonAuthenticatedNoSub"

    const/16 v3, 0x35

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5589
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->be:Ljava/lang/String;

    goto/16 :goto_0

    .line 5618
    :sswitch_3c
    const-string v1, "ConfigService"

    const-string v2, "callToActionUpgradePromptFor"

    const/16 v3, 0xd

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5619
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_4

    .line 5663
    const-string v1, "ConfigService"

    const-string v2, "callToActionUpgradePromptFor"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5621
    :pswitch_b
    const-string v1, "ConfigService"

    const-string v2, "callToActionUpgradePromptForSmartphone"

    const/16 v3, 0x1d

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5622
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1c

    .line 5636
    const-string v1, "ConfigService"

    const-string v2, "callToActionUpgradePromptForSmartphone"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5624
    :sswitch_3d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5625
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aS:Ljava/lang/String;

    goto/16 :goto_0

    .line 5630
    :sswitch_3e
    const-string v1, "ConfigService"

    const-string v2, "callToActionUpgradePromptForSmartphoneNoSub"

    const/16 v3, 0x27

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5631
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bj:Ljava/lang/String;

    goto/16 :goto_0

    .line 5642
    :pswitch_c
    const-string v1, "ConfigService"

    const-string v2, "callToActionUpgradePromptForTablet"

    const/16 v3, 0x1d

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5643
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1d

    .line 5657
    const-string v1, "ConfigService"

    const-string v2, "callToActionUpgradePromptForTablet"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5645
    :sswitch_3f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5646
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aR:Ljava/lang/String;

    goto/16 :goto_0

    .line 5651
    :sswitch_40
    const-string v1, "ConfigService"

    const-string v2, "callToActionUpgradePromptForTabletNoSub"

    const/16 v3, 0x23

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5652
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bi:Ljava/lang/String;

    goto/16 :goto_0

    .line 5675
    :pswitch_d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1e

    .line 5695
    const-string v1, "ConfigService"

    const-string v2, "can"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5677
    :sswitch_41
    const-string v1, "ConfigService"

    const-string v2, "canCompose"

    const/4 v3, 0x4

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5678
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->g:Z

    goto/16 :goto_0

    .line 5683
    :sswitch_42
    const-string v1, "ConfigService"

    const-string v2, "canRead"

    const/4 v3, 0x4

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5684
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->f:Z

    goto/16 :goto_0

    .line 5689
    :sswitch_43
    const-string v1, "ConfigService"

    const-string v2, "canShare"

    const/4 v3, 0x4

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5690
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->H:Z

    goto/16 :goto_0

    .line 5705
    :sswitch_44
    const-string v1, "ConfigService"

    const-string v2, "contentDomainURLs"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5706
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bN:Ljava/util/List;

    goto/16 :goto_0

    .line 5716
    :pswitch_e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1f

    .line 5758
    const-string v1, "ConfigService"

    const-string v2, "d"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5718
    :sswitch_45
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_20

    .line 5732
    const-string v1, "ConfigService"

    const-string v2, "de"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5720
    :sswitch_46
    const-string v1, "ConfigService"

    const-string v2, "defaultShareTextEnabled"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5721
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->bF:Z

    goto/16 :goto_0

    .line 5726
    :sswitch_47
    const-string v1, "ConfigService"

    const-string v2, "description"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5727
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bQ:Ljava/lang/String;

    goto/16 :goto_0

    .line 5737
    :sswitch_48
    const-string v1, "ConfigService"

    const-string v2, "display"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5738
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_21

    .line 5752
    const-string v1, "ConfigService"

    const-string v2, "display"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5740
    :sswitch_49
    const-string v1, "ConfigService"

    const-string v2, "displayNameKey"

    const/16 v3, 0x8

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5741
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 5746
    :sswitch_4a
    const-string v1, "ConfigService"

    const-string v2, "displayUserNameInAvatarPopOver"

    const/16 v3, 0x8

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5747
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->M:Z

    goto/16 :goto_0

    .line 5763
    :pswitch_f
    const-string v1, "ConfigService"

    const-string v2, "followA"

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5764
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_22

    .line 5793
    const-string v1, "ConfigService"

    const-string v2, "followA"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5766
    :sswitch_4b
    const-string v1, "ConfigService"

    const-string v2, "followActionType"

    const/16 v3, 0x8

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5767
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->R:Ljava/lang/String;

    goto/16 :goto_0

    .line 5772
    :sswitch_4c
    const-string v1, "ConfigService"

    const-string v2, "followAlertTitle"

    const/16 v3, 0x8

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5773
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_23

    .line 5787
    const-string v1, "ConfigService"

    const-string v2, "followAlertTitle"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5775
    :sswitch_4d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5776
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->S:Ljava/lang/String;

    goto/16 :goto_0

    .line 5781
    :sswitch_4e
    const-string v1, "ConfigService"

    const-string v2, "followAlertTitleKey"

    const/16 v3, 0x11

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5782
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->Q:Ljava/lang/String;

    goto/16 :goto_0

    .line 5799
    :pswitch_10
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_24

    .line 5834
    const-string v1, "ConfigService"

    const-string v2, "h"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5801
    :sswitch_4f
    const-string v1, "ConfigService"

    const-string v2, "hasCustomItemTerm"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5802
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->aj:Z

    goto/16 :goto_0

    .line 5807
    :sswitch_50
    const-string v1, "ConfigService"

    const-string v2, "hide"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5808
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_25

    .line 5828
    const-string v1, "ConfigService"

    const-string v2, "hide"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5810
    :sswitch_51
    const-string v1, "ConfigService"

    const-string v2, "hideLikesInSocialCard"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5811
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->ai:Z

    goto/16 :goto_0

    .line 5816
    :sswitch_52
    const-string v1, "ConfigService"

    const-string v2, "hideSharedByPrefix"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5817
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->au:Z

    goto/16 :goto_0

    .line 5822
    :sswitch_53
    const-string v1, "ConfigService"

    const-string v2, "hideTableSubTitlesInSocialCard"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5823
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->al:Z

    goto/16 :goto_0

    .line 5839
    :pswitch_11
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_26

    .line 6075
    const-string v1, "ConfigService"

    const-string v2, "i"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5841
    :sswitch_54
    const-string v1, "ConfigService"

    const-string v2, "icon"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5842
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_27

    .line 6044
    const-string v1, "ConfigService"

    const-string v2, "icon"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5844
    :sswitch_55
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5845
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bR:Ljava/lang/String;

    goto/16 :goto_0

    .line 5850
    :sswitch_56
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_28

    .line 5892
    const-string v1, "ConfigService"

    const-string v2, "icon1"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5852
    :sswitch_57
    const-string v1, "ConfigService"

    const-string v2, "icon128"

    const/4 v3, 0x6

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5853
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_29

    .line 5867
    const-string v1, "ConfigService"

    const-string v2, "icon128"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5855
    :sswitch_58
    const-string v1, "ConfigService"

    const-string v2, "icon128OpaqueURL"

    const/16 v3, 0x8

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5856
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->x:Ljava/lang/String;

    goto/16 :goto_0

    .line 5861
    :sswitch_59
    const-string v1, "ConfigService"

    const-string v2, "icon128URL"

    const/16 v3, 0x8

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5862
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 5873
    :sswitch_5a
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2a

    .line 5887
    const-string v1, "ConfigService"

    const-string v2, "icon16"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5875
    :sswitch_5b
    const-string v1, "ConfigService"

    const-string v2, "icon16OpaqueURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5876
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 5881
    :sswitch_5c
    const-string v1, "ConfigService"

    const-string v2, "icon16URL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5882
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 5897
    :sswitch_5d
    const-string v1, "ConfigService"

    const-string v2, "icon24"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5898
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2b

    .line 5912
    const-string v1, "ConfigService"

    const-string v2, "icon24"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5900
    :sswitch_5e
    const-string v1, "ConfigService"

    const-string v2, "icon24OpaqueURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5901
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 5906
    :sswitch_5f
    const-string v1, "ConfigService"

    const-string v2, "icon24URL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5907
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 5918
    :sswitch_60
    const-string v1, "ConfigService"

    const-string v2, "icon32"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5919
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2c

    .line 5939
    const-string v1, "ConfigService"

    const-string v2, "icon32"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5921
    :sswitch_61
    const-string v1, "ConfigService"

    const-string v2, "icon32GrayURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5922
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->y:Ljava/lang/String;

    goto/16 :goto_0

    .line 5927
    :sswitch_62
    const-string v1, "ConfigService"

    const-string v2, "icon32OpaqueURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5928
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->t:Ljava/lang/String;

    goto/16 :goto_0

    .line 5933
    :sswitch_63
    const-string v1, "ConfigService"

    const-string v2, "icon32URL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5934
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 5945
    :sswitch_64
    const-string v1, "ConfigService"

    const-string v2, "icon48"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5946
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2d

    .line 5972
    const-string v1, "ConfigService"

    const-string v2, "icon48"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5948
    :sswitch_65
    const-string v1, "ConfigService"

    const-string v2, "icon48ActionURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5949
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->C:Ljava/lang/String;

    goto/16 :goto_0

    .line 5954
    :sswitch_66
    const-string v1, "ConfigService"

    const-string v2, "icon48GrayURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5955
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->z:Ljava/lang/String;

    goto/16 :goto_0

    .line 5960
    :sswitch_67
    const-string v1, "ConfigService"

    const-string v2, "icon48OpaqueURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5961
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->u:Ljava/lang/String;

    goto/16 :goto_0

    .line 5966
    :sswitch_68
    const-string v1, "ConfigService"

    const-string v2, "icon48URL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5967
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 5978
    :sswitch_69
    const-string v1, "ConfigService"

    const-string v2, "icon64"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5979
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2e

    .line 5999
    const-string v1, "ConfigService"

    const-string v2, "icon64"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5981
    :sswitch_6a
    const-string v1, "ConfigService"

    const-string v2, "icon64GrayURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5982
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->A:Ljava/lang/String;

    goto/16 :goto_0

    .line 5987
    :sswitch_6b
    const-string v1, "ConfigService"

    const-string v2, "icon64OpaqueURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5988
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->v:Ljava/lang/String;

    goto/16 :goto_0

    .line 5993
    :sswitch_6c
    const-string v1, "ConfigService"

    const-string v2, "icon64URL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5994
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 6005
    :sswitch_6d
    const-string v1, "ConfigService"

    const-string v2, "icon96"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6006
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2f

    .line 6032
    const-string v1, "ConfigService"

    const-string v2, "icon96"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6008
    :sswitch_6e
    const-string v1, "ConfigService"

    const-string v2, "icon96ActionURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6009
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->D:Ljava/lang/String;

    goto/16 :goto_0

    .line 6014
    :sswitch_6f
    const-string v1, "ConfigService"

    const-string v2, "icon96GrayURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6015
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->B:Ljava/lang/String;

    goto/16 :goto_0

    .line 6020
    :sswitch_70
    const-string v1, "ConfigService"

    const-string v2, "icon96OpaqueURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6021
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->w:Ljava/lang/String;

    goto/16 :goto_0

    .line 6026
    :sswitch_71
    const-string v1, "ConfigService"

    const-string v2, "icon96URL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6027
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->p:Ljava/lang/String;

    goto/16 :goto_0

    .line 6038
    :sswitch_72
    const-string v1, "ConfigService"

    const-string v2, "iconAttributionGrayURL"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6039
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->E:Ljava/lang/String;

    goto/16 :goto_0

    .line 6050
    :sswitch_73
    const-string v1, "ConfigService"

    const-string v2, "id"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6051
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 6056
    :sswitch_74
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_30

    .line 6070
    const-string v1, "ConfigService"

    const-string v2, "is"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6058
    :sswitch_75
    const-string v1, "ConfigService"

    const-string v2, "isFavicon"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6059
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->bS:Z

    goto/16 :goto_0

    .line 6064
    :sswitch_76
    const-string v1, "ConfigService"

    const-string v2, "isSubscriptionService"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6065
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->bx:Z

    goto/16 :goto_0

    .line 6080
    :pswitch_12
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_31

    .line 6167
    const-string v1, "ConfigService"

    const-string v2, "l"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6082
    :sswitch_77
    const-string v1, "ConfigService"

    const-string v2, "like"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6083
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_32

    .line 6110
    const-string v1, "ConfigService"

    const-string v2, "like"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6085
    :sswitch_78
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_33

    .line 6099
    const-string v1, "ConfigService"

    const-string v2, "likeA"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6087
    :sswitch_79
    const-string v1, "ConfigService"

    const-string v2, "likeActionType"

    const/4 v3, 0x6

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6088
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->P:Ljava/lang/String;

    goto/16 :goto_0

    .line 6093
    :sswitch_7a
    const-string v1, "ConfigService"

    const-string v2, "likeAlertTitleKey"

    const/4 v3, 0x6

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6094
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->O:Ljava/lang/String;

    goto/16 :goto_0

    .line 6104
    :sswitch_7b
    const-string v1, "ConfigService"

    const-string v2, "likeIconStyle"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6105
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 6116
    :sswitch_7c
    const-string v1, "ConfigService"

    const-string v2, "loginPageTablet"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6117
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_34

    .line 6161
    const-string v1, "ConfigService"

    const-string v2, "loginPageTablet"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6119
    :sswitch_7d
    const-string v1, "ConfigService"

    const-string v2, "loginPageTabletLandscape"

    const/16 v3, 0x10

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6120
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_35

    .line 6134
    const-string v1, "ConfigService"

    const-string v2, "loginPageTabletLandscape"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6122
    :sswitch_7e
    const-string v1, "ConfigService"

    const-string v2, "loginPageTabletLandscapeHeight"

    const/16 v3, 0x19

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6123
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigService;->bC:I

    goto/16 :goto_0

    .line 6128
    :sswitch_7f
    const-string v1, "ConfigService"

    const-string v2, "loginPageTabletLandscapeWidth"

    const/16 v3, 0x19

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6129
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigService;->bB:I

    goto/16 :goto_0

    .line 6140
    :sswitch_80
    const-string v1, "ConfigService"

    const-string v2, "loginPageTabletPortrait"

    const/16 v3, 0x10

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6141
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_36

    .line 6155
    const-string v1, "ConfigService"

    const-string v2, "loginPageTabletPortrait"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6143
    :sswitch_81
    const-string v1, "ConfigService"

    const-string v2, "loginPageTabletPortraitHeight"

    const/16 v3, 0x18

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6144
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigService;->bA:I

    goto/16 :goto_0

    .line 6149
    :sswitch_82
    const-string v1, "ConfigService"

    const-string v2, "loginPageTabletPortraitWidth"

    const/16 v3, 0x18

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6150
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigService;->bz:I

    goto/16 :goto_0

    .line 6172
    :pswitch_13
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_37

    .line 6232
    const-string v1, "ConfigService"

    const-string v2, "m"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6174
    :sswitch_83
    const-string v1, "ConfigService"

    const-string v2, "metering"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6175
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_38

    .line 6220
    const-string v1, "ConfigService"

    const-string v2, "metering"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6177
    :sswitch_84
    const-string v1, "ConfigService"

    const-string v2, "meteringInterstitialBackgroundUrl"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6178
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bK:Ljava/lang/String;

    goto/16 :goto_0

    .line 6183
    :sswitch_85
    const-string v1, "ConfigService"

    const-string v2, "meteringMaxArticleCountPerSession"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6184
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigService;->bI:I

    goto/16 :goto_0

    .line 6189
    :sswitch_86
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_39

    .line 6203
    const-string v1, "ConfigService"

    const-string v2, "meteringR"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6191
    :sswitch_87
    const-string v1, "ConfigService"

    const-string v2, "meteringReadArticleExpirationTime"

    const/16 v3, 0xa

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6192
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/objs/ConfigService;->bJ:J

    goto/16 :goto_0

    .line 6197
    :sswitch_88
    const-string v1, "ConfigService"

    const-string v2, "meteringRoadblockBackgroundUrl"

    const/16 v3, 0xa

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6198
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bL:Ljava/lang/String;

    goto/16 :goto_0

    .line 6208
    :sswitch_89
    const-string v1, "ConfigService"

    const-string v2, "meteringTimeUnit"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6209
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bG:Ljava/lang/String;

    goto/16 :goto_0

    .line 6214
    :sswitch_8a
    const-string v1, "ConfigService"

    const-string v2, "meteringUnitsPerSession"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6215
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigService;->bH:I

    goto/16 :goto_0

    .line 6226
    :sswitch_8b
    const-string v1, "ConfigService"

    const-string v2, "minVersion"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6227
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigService;->K:I

    goto/16 :goto_0

    .line 6237
    :pswitch_14
    const-string v1, "ConfigService"

    const-string v2, "newestCommentsFirst"

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6238
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->h:Z

    goto/16 :goto_0

    .line 6243
    :pswitch_15
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3a

    .line 6327
    const-string v1, "ConfigService"

    const-string v2, "p"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6245
    :sswitch_8c
    const-string v1, "ConfigService"

    const-string v2, "pastTense"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6246
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3b

    .line 6260
    const-string v1, "ConfigService"

    const-string v2, "pastTense"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6248
    :sswitch_8d
    const-string v1, "ConfigService"

    const-string v2, "pastTenseLikeAlertTitleKey"

    const/16 v3, 0xa

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6249
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->aq:Ljava/lang/String;

    goto/16 :goto_0

    .line 6254
    :sswitch_8e
    const-string v1, "ConfigService"

    const-string v2, "pastTenseShareAlertTitleKey"

    const/16 v3, 0xa

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6255
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->as:Ljava/lang/String;

    goto/16 :goto_0

    .line 6266
    :sswitch_8f
    const-string v1, "ConfigService"

    const-string v2, "plural"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6267
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3c

    .line 6296
    const-string v1, "ConfigService"

    const-string v2, "plural"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6269
    :sswitch_90
    const-string v1, "ConfigService"

    const-string v2, "pluralGenericItemStringKey"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6270
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->an:Ljava/lang/String;

    goto/16 :goto_0

    .line 6275
    :sswitch_91
    const-string v1, "ConfigService"

    const-string v2, "pluralShare"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6276
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3d

    .line 6290
    const-string v1, "ConfigService"

    const-string v2, "pluralShare"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6278
    :sswitch_92
    const-string v1, "ConfigService"

    const-string v2, "pluralShareItemStringKey"

    const/16 v3, 0xc

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6279
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->ag:Ljava/lang/String;

    goto/16 :goto_0

    .line 6284
    :sswitch_93
    const-string v1, "ConfigService"

    const-string v2, "pluralShareTargetDisplayNameKey"

    const/16 v3, 0xc

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6285
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->ax:Ljava/lang/String;

    goto/16 :goto_0

    .line 6302
    :sswitch_94
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3e

    .line 6322
    const-string v1, "ConfigService"

    const-string v2, "pr"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6304
    :sswitch_95
    const-string v1, "ConfigService"

    const-string v2, "preserveFSSWhenNavigatingSubsections"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6305
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->bD:Z

    goto/16 :goto_0

    .line 6310
    :sswitch_96
    const-string v1, "ConfigService"

    const-string v2, "primaryShareButtonTitleKey"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6311
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->ao:Ljava/lang/String;

    goto/16 :goto_0

    .line 6316
    :sswitch_97
    const-string v1, "ConfigService"

    const-string v2, "profilePopOverUserNameFormat"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6317
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->N:Ljava/lang/String;

    goto/16 :goto_0

    .line 6332
    :pswitch_16
    const-string v1, "ConfigService"

    const-string v2, "resizeLoginPageToFitWhenKeyboardVisible"

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6333
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->az:Z

    goto/16 :goto_0

    .line 6338
    :pswitch_17
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3f

    .line 6743
    const-string v1, "ConfigService"

    const-string v2, "s"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6340
    :sswitch_98
    const-string v1, "ConfigService"

    const-string v2, "scaleToFitLoginPage"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6341
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->ak:Z

    goto/16 :goto_0

    .line 6346
    :sswitch_99
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_40

    .line 6375
    const-string v1, "ConfigService"

    const-string v2, "se"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6348
    :sswitch_9a
    const-string v1, "ConfigService"

    const-string v2, "secondaryShareButtonTitleKey"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6349
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->ap:Ljava/lang/String;

    goto/16 :goto_0

    .line 6354
    :sswitch_9b
    const-string v1, "ConfigService"

    const-string v2, "serviceLoginDescription"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6355
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_41

    .line 6369
    const-string v1, "ConfigService"

    const-string v2, "serviceLoginDescription"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6357
    :sswitch_9c
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6358
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->ae:Ljava/lang/String;

    goto/16 :goto_0

    .line 6363
    :sswitch_9d
    const-string v1, "ConfigService"

    const-string v2, "serviceLoginDescriptionKey"

    const/16 v3, 0x18

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6364
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->by:Ljava/lang/String;

    goto/16 :goto_0

    .line 6380
    :sswitch_9e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_42

    .line 6461
    const-string v1, "ConfigService"

    const-string v2, "sh"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6382
    :sswitch_9f
    const-string v1, "ConfigService"

    const-string v2, "share"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6383
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_43

    .line 6422
    const-string v1, "ConfigService"

    const-string v2, "share"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6385
    :sswitch_a0
    const-string v1, "ConfigService"

    const-string v2, "shareActionType"

    const/4 v3, 0x6

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6386
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->T:Ljava/lang/String;

    goto/16 :goto_0

    .line 6391
    :sswitch_a1
    const-string v1, "ConfigService"

    const-string v2, "shareButtonTitleKey"

    const/4 v3, 0x6

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6392
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->ay:Ljava/lang/String;

    goto/16 :goto_0

    .line 6397
    :sswitch_a2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_44

    .line 6411
    const-string v1, "ConfigService"

    const-string v2, "shareI"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6399
    :sswitch_a3
    const-string v1, "ConfigService"

    const-string v2, "shareIconStyle"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6400
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->ah:Ljava/lang/String;

    goto/16 :goto_0

    .line 6405
    :sswitch_a4
    const-string v1, "ConfigService"

    const-string v2, "shareItemPlaceholderStringKey"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6406
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->W:Ljava/lang/String;

    goto/16 :goto_0

    .line 6416
    :sswitch_a5
    const-string v1, "ConfigService"

    const-string v2, "sharePictureShouldAttributeAuthor"

    const/4 v3, 0x6

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6417
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->bE:Z

    goto/16 :goto_0

    .line 6428
    :sswitch_a6
    const-string v1, "ConfigService"

    const-string v2, "show"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6429
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_45

    .line 6455
    const-string v1, "ConfigService"

    const-string v2, "show"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6431
    :sswitch_a7
    const-string v1, "ConfigService"

    const-string v2, "showAuthorSectionTitleKey"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6432
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->U:Ljava/lang/String;

    goto/16 :goto_0

    .line 6437
    :sswitch_a8
    const-string v1, "ConfigService"

    const-string v2, "showMastheadDrillDownInSubsections"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6438
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->aA:Z

    goto/16 :goto_0

    .line 6443
    :sswitch_a9
    const-string v1, "ConfigService"

    const-string v2, "showSignIn"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6444
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->X:Z

    goto/16 :goto_0

    .line 6449
    :sswitch_aa
    const-string v1, "ConfigService"

    const-string v2, "showUserNamesInLikeString"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6450
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->L:Z

    goto/16 :goto_0

    .line 6466
    :sswitch_ab
    const-string v1, "ConfigService"

    const-string v2, "singular"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6467
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_46

    .line 6481
    const-string v1, "ConfigService"

    const-string v2, "singular"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6469
    :sswitch_ac
    const-string v1, "ConfigService"

    const-string v2, "singularGenericItemStringKey"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6470
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->am:Ljava/lang/String;

    goto/16 :goto_0

    .line 6475
    :sswitch_ad
    const-string v1, "ConfigService"

    const-string v2, "singularShareItemStringKey"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6476
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->af:Ljava/lang/String;

    goto/16 :goto_0

    .line 6487
    :sswitch_ae
    const-string v1, "ConfigService"

    const-string v2, "state"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6488
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_47

    .line 6658
    const-string v1, "ConfigService"

    const-string v2, "state"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6490
    :sswitch_af
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNon"

    const/4 v3, 0x6

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6491
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_48

    .line 6595
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNon"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6493
    :sswitch_b0
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonEntitled"

    const/16 v3, 0x16

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6494
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_5

    .line 6538
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonEntitled"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6496
    :pswitch_18
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonEntitledSmartphone"

    const/16 v3, 0x1e

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6497
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_49

    .line 6511
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonEntitledSmartphone"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6499
    :sswitch_b1
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonEntitledSmartphoneHTML"

    const/16 v3, 0x28

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6500
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bp:Ljava/lang/String;

    goto/16 :goto_0

    .line 6505
    :sswitch_b2
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonEntitledSmartphoneNoSubHTML"

    const/16 v3, 0x28

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6506
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bw:Ljava/lang/String;

    goto/16 :goto_0

    .line 6517
    :pswitch_19
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonEntitledTablet"

    const/16 v3, 0x1e

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6518
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4a

    .line 6532
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonEntitledTablet"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6520
    :sswitch_b3
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonEntitledTabletHTML"

    const/16 v3, 0x24

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6521
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bo:Ljava/lang/String;

    goto/16 :goto_0

    .line 6526
    :sswitch_b4
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonEntitledTabletNoSubHTML"

    const/16 v3, 0x24

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6527
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bv:Ljava/lang/String;

    goto/16 :goto_0

    .line 6544
    :sswitch_b5
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonSubscriber"

    const/16 v3, 0x16

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6545
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_6

    .line 6589
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonSubscriber"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6547
    :pswitch_1a
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonSubscriberSmartphone"

    const/16 v3, 0x20

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6548
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4b

    .line 6562
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonSubscriberSmartphone"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6550
    :sswitch_b6
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonSubscriberSmartphoneHTML"

    const/16 v3, 0x2a

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6551
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bn:Ljava/lang/String;

    goto/16 :goto_0

    .line 6556
    :sswitch_b7
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonSubscriberSmartphoneNoSubHTML"

    const/16 v3, 0x2a

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6557
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bu:Ljava/lang/String;

    goto/16 :goto_0

    .line 6568
    :pswitch_1b
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonSubscriberTablet"

    const/16 v3, 0x20

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6569
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4c

    .line 6583
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonSubscriberTablet"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6571
    :sswitch_b8
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonSubscriberTabletHTML"

    const/16 v3, 0x26

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6572
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bm:Ljava/lang/String;

    goto/16 :goto_0

    .line 6577
    :sswitch_b9
    const-string v1, "ConfigService"

    const-string v2, "stateAuthenticatedNonSubscriberTabletNoSubHTML"

    const/16 v3, 0x26

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6578
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bt:Ljava/lang/String;

    goto/16 :goto_0

    .line 6601
    :sswitch_ba
    const-string v1, "ConfigService"

    const-string v2, "stateNonAuthenticated"

    const/4 v3, 0x6

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6602
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4d

    .line 6652
    const-string v1, "ConfigService"

    const-string v2, "stateNonAuthenticated"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6604
    :sswitch_bb
    const-string v1, "ConfigService"

    const-string v2, "stateNonAuthenticatedHTML"

    const/16 v3, 0x16

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6605
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bq:Ljava/lang/String;

    goto/16 :goto_0

    .line 6610
    :sswitch_bc
    const-string v1, "ConfigService"

    const-string v2, "stateNonAuthenticatedSmartphone"

    const/16 v3, 0x16

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6611
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4e

    .line 6625
    const-string v1, "ConfigService"

    const-string v2, "stateNonAuthenticatedSmartphone"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6613
    :sswitch_bd
    const-string v1, "ConfigService"

    const-string v2, "stateNonAuthenticatedSmartphoneHTML"

    const/16 v3, 0x20

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6614
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bl:Ljava/lang/String;

    goto/16 :goto_0

    .line 6619
    :sswitch_be
    const-string v1, "ConfigService"

    const-string v2, "stateNonAuthenticatedSmartphoneNoSubHTML"

    const/16 v3, 0x20

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6620
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bs:Ljava/lang/String;

    goto/16 :goto_0

    .line 6631
    :sswitch_bf
    const-string v1, "ConfigService"

    const-string v2, "stateNonAuthenticatedTablet"

    const/16 v3, 0x16

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6632
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4f

    .line 6646
    const-string v1, "ConfigService"

    const-string v2, "stateNonAuthenticatedTablet"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6634
    :sswitch_c0
    const-string v1, "ConfigService"

    const-string v2, "stateNonAuthenticatedTabletHTML"

    const/16 v3, 0x1c

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6635
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bk:Ljava/lang/String;

    goto/16 :goto_0

    .line 6640
    :sswitch_c1
    const-string v1, "ConfigService"

    const-string v2, "stateNonAuthenticatedTabletNoSubHTML"

    const/16 v3, 0x1c

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6641
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->br:Ljava/lang/String;

    goto/16 :goto_0

    .line 6664
    :sswitch_c2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_50

    .line 6738
    const-string v1, "ConfigService"

    const-string v2, "su"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6666
    :sswitch_c3
    const-string v1, "ConfigService"

    const-string v2, "subsectionMethodName"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6667
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->F:Ljava/lang/String;

    goto/16 :goto_0

    .line 6672
    :sswitch_c4
    const-string v1, "ConfigService"

    const-string v2, "supports"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6673
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_51

    .line 6732
    const-string v1, "ConfigService"

    const-string v2, "supports"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6675
    :sswitch_c5
    const-string v1, "ConfigService"

    const-string v2, "supportsComposition"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6676
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->ab:Z

    goto/16 :goto_0

    .line 6681
    :sswitch_c6
    const-string v1, "ConfigService"

    const-string v2, "supportsFollow"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6682
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->aa:Z

    goto/16 :goto_0

    .line 6687
    :sswitch_c7
    const-string v1, "ConfigService"

    const-string v2, "supportsLike"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6688
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->Y:Z

    goto/16 :goto_0

    .line 6693
    :sswitch_c8
    const-string v1, "ConfigService"

    const-string v2, "supportsMultipleSelectedShareTargets"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6694
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->aw:Z

    goto/16 :goto_0

    .line 6699
    :sswitch_c9
    const-string v1, "ConfigService"

    const-string v2, "supportsProfilePopOverShowAuthorSectionAction"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6700
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->ac:Z

    goto/16 :goto_0

    .line 6705
    :sswitch_ca
    const-string v1, "ConfigService"

    const-string v2, "supportsShareTargets"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6706
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->av:Z

    goto/16 :goto_0

    .line 6711
    :sswitch_cb
    const-string v1, "ConfigService"

    const-string v2, "supportsUn"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6712
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_52

    .line 6726
    const-string v1, "ConfigService"

    const-string v2, "supportsUn"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6714
    :sswitch_cc
    const-string v1, "ConfigService"

    const-string v2, "supportsUnlike"

    const/16 v3, 0xb

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6715
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->Z:Z

    goto/16 :goto_0

    .line 6720
    :sswitch_cd
    const-string v1, "ConfigService"

    const-string v2, "supportsUnreadCounts"

    const/16 v3, 0xb

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6721
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->J:Z

    goto/16 :goto_0

    .line 6748
    :pswitch_1c
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_53

    .line 6783
    const-string v1, "ConfigService"

    const-string v2, "t"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6750
    :sswitch_ce
    const-string v1, "ConfigService"

    const-string v2, "title"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6751
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->bP:Ljava/lang/String;

    goto/16 :goto_0

    .line 6756
    :sswitch_cf
    const-string v1, "ConfigService"

    const-string v2, "tocS"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6757
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_54

    .line 6771
    const-string v1, "ConfigService"

    const-string v2, "tocS"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6759
    :sswitch_d0
    const-string v1, "ConfigService"

    const-string v2, "tocServiceTileColor"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6760
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->I:Ljava/lang/String;

    goto/16 :goto_0

    .line 6765
    :sswitch_d1
    const-string v1, "ConfigService"

    const-string v2, "tocSignInTextKey"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6766
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->at:Ljava/lang/String;

    goto/16 :goto_0

    .line 6777
    :sswitch_d2
    const-string v1, "ConfigService"

    const-string v2, "treatSharesAsComments"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6778
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigService;->j:Z

    goto/16 :goto_0

    .line 6788
    :pswitch_1d
    const-string v1, "ConfigService"

    const-string v2, "verifiedImageURL"

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6789
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigService;->ar:Ljava/lang/String;

    goto/16 :goto_0

    .line 6799
    :sswitch_d3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 6813
    :goto_1
    return-object v0

    .line 6806
    :sswitch_d4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 6813
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 5146
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_d4
        0xa -> :sswitch_d4
        0xd -> :sswitch_d4
        0x20 -> :sswitch_d4
        0x22 -> :sswitch_0
        0x2c -> :sswitch_d4
        0x7d -> :sswitch_d3
    .end sparse-switch

    .line 5148
    :pswitch_data_0
    .packed-switch 0x61
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_0
        :pswitch_15
        :pswitch_0
        :pswitch_16
        :pswitch_17
        :pswitch_1c
        :pswitch_0
        :pswitch_1d
    .end packed-switch

    .line 5151
    :sswitch_data_1
    .sparse-switch
        0x44 -> :sswitch_1
        0x45 -> :sswitch_4
        0x4d -> :sswitch_7
        0x55 -> :sswitch_8
    .end sparse-switch

    .line 5154
    :sswitch_data_2
    .sparse-switch
        0x22 -> :sswitch_2
        0x73 -> :sswitch_3
    .end sparse-switch

    .line 5174
    :sswitch_data_3
    .sparse-switch
        0x6e -> :sswitch_5
        0x78 -> :sswitch_6
    .end sparse-switch

    .line 5211
    :sswitch_data_4
    .sparse-switch
        0x61 -> :sswitch_9
        0x6f -> :sswitch_44
    .end sparse-switch

    .line 5213
    :pswitch_data_1
    .packed-switch 0x6c
        :pswitch_4
        :pswitch_3
        :pswitch_d
    .end packed-switch

    .line 5216
    :sswitch_data_5
    .sparse-switch
        0x42 -> :sswitch_a
        0x4c -> :sswitch_19
        0x53 -> :sswitch_2d
        0x55 -> :sswitch_3c
    .end sparse-switch

    .line 5219
    :sswitch_data_6
    .sparse-switch
        0x4c -> :sswitch_b
        0x54 -> :sswitch_12
    .end sparse-switch

    .line 5222
    :sswitch_data_7
    .sparse-switch
        0x41 -> :sswitch_c
        0x53 -> :sswitch_f
    .end sparse-switch

    .line 5225
    :sswitch_data_8
    .sparse-switch
        0x22 -> :sswitch_d
        0x4e -> :sswitch_e
    .end sparse-switch

    .line 5246
    :sswitch_data_9
    .sparse-switch
        0x22 -> :sswitch_10
        0x4e -> :sswitch_11
    .end sparse-switch

    .line 5273
    :sswitch_data_a
    .sparse-switch
        0x41 -> :sswitch_13
        0x53 -> :sswitch_16
    .end sparse-switch

    .line 5276
    :sswitch_data_b
    .sparse-switch
        0x22 -> :sswitch_14
        0x4e -> :sswitch_15
    .end sparse-switch

    .line 5297
    :sswitch_data_c
    .sparse-switch
        0x22 -> :sswitch_17
        0x4e -> :sswitch_18
    .end sparse-switch

    .line 5330
    :pswitch_data_2
    .packed-switch 0x53
        :pswitch_6
        :pswitch_5
        :pswitch_7
        :pswitch_5
        :pswitch_8
    .end packed-switch

    .line 5332
    :sswitch_data_d
    .sparse-switch
        0x69 -> :sswitch_1a
        0x75 -> :sswitch_20
    .end sparse-switch

    .line 5335
    :sswitch_data_e
    .sparse-switch
        0x22 -> :sswitch_1b
        0x42 -> :sswitch_1c
        0x4e -> :sswitch_1f
    .end sparse-switch

    .line 5344
    :sswitch_data_f
    .sparse-switch
        0x22 -> :sswitch_1d
        0x4e -> :sswitch_1e
    .end sparse-switch

    .line 5377
    :sswitch_data_10
    .sparse-switch
        0x22 -> :sswitch_21
        0x42 -> :sswitch_22
        0x4e -> :sswitch_25
    .end sparse-switch

    .line 5386
    :sswitch_data_11
    .sparse-switch
        0x22 -> :sswitch_23
        0x4e -> :sswitch_24
    .end sparse-switch

    .line 5424
    :sswitch_data_12
    .sparse-switch
        0x22 -> :sswitch_26
        0x42 -> :sswitch_27
        0x4e -> :sswitch_2a
    .end sparse-switch

    .line 5433
    :sswitch_data_13
    .sparse-switch
        0x22 -> :sswitch_28
        0x4e -> :sswitch_29
    .end sparse-switch

    .line 5466
    :sswitch_data_14
    .sparse-switch
        0x22 -> :sswitch_2b
        0x42 -> :sswitch_2c
    .end sparse-switch

    .line 5493
    :sswitch_data_15
    .sparse-switch
        0x22 -> :sswitch_2e
        0x46 -> :sswitch_2f
    .end sparse-switch

    .line 5502
    :pswitch_data_3
    .packed-switch 0x53
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 5505
    :sswitch_data_16
    .sparse-switch
        0x41 -> :sswitch_30
        0x4e -> :sswitch_33
    .end sparse-switch

    .line 5508
    :sswitch_data_17
    .sparse-switch
        0x22 -> :sswitch_31
        0x4e -> :sswitch_32
    .end sparse-switch

    .line 5529
    :sswitch_data_18
    .sparse-switch
        0x22 -> :sswitch_34
        0x4e -> :sswitch_35
    .end sparse-switch

    .line 5556
    :sswitch_data_19
    .sparse-switch
        0x41 -> :sswitch_36
        0x4e -> :sswitch_39
    .end sparse-switch

    .line 5559
    :sswitch_data_1a
    .sparse-switch
        0x22 -> :sswitch_37
        0x4e -> :sswitch_38
    .end sparse-switch

    .line 5580
    :sswitch_data_1b
    .sparse-switch
        0x22 -> :sswitch_3a
        0x4e -> :sswitch_3b
    .end sparse-switch

    .line 5619
    :pswitch_data_4
    .packed-switch 0x53
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 5622
    :sswitch_data_1c
    .sparse-switch
        0x22 -> :sswitch_3d
        0x4e -> :sswitch_3e
    .end sparse-switch

    .line 5643
    :sswitch_data_1d
    .sparse-switch
        0x22 -> :sswitch_3f
        0x4e -> :sswitch_40
    .end sparse-switch

    .line 5675
    :sswitch_data_1e
    .sparse-switch
        0x43 -> :sswitch_41
        0x52 -> :sswitch_42
        0x53 -> :sswitch_43
    .end sparse-switch

    .line 5716
    :sswitch_data_1f
    .sparse-switch
        0x65 -> :sswitch_45
        0x69 -> :sswitch_48
    .end sparse-switch

    .line 5718
    :sswitch_data_20
    .sparse-switch
        0x66 -> :sswitch_46
        0x73 -> :sswitch_47
    .end sparse-switch

    .line 5738
    :sswitch_data_21
    .sparse-switch
        0x4e -> :sswitch_49
        0x55 -> :sswitch_4a
    .end sparse-switch

    .line 5764
    :sswitch_data_22
    .sparse-switch
        0x63 -> :sswitch_4b
        0x6c -> :sswitch_4c
    .end sparse-switch

    .line 5773
    :sswitch_data_23
    .sparse-switch
        0x22 -> :sswitch_4d
        0x4b -> :sswitch_4e
    .end sparse-switch

    .line 5799
    :sswitch_data_24
    .sparse-switch
        0x61 -> :sswitch_4f
        0x69 -> :sswitch_50
    .end sparse-switch

    .line 5808
    :sswitch_data_25
    .sparse-switch
        0x4c -> :sswitch_51
        0x53 -> :sswitch_52
        0x54 -> :sswitch_53
    .end sparse-switch

    .line 5839
    :sswitch_data_26
    .sparse-switch
        0x63 -> :sswitch_54
        0x64 -> :sswitch_73
        0x73 -> :sswitch_74
    .end sparse-switch

    .line 5842
    :sswitch_data_27
    .sparse-switch
        0x22 -> :sswitch_55
        0x31 -> :sswitch_56
        0x32 -> :sswitch_5d
        0x33 -> :sswitch_60
        0x34 -> :sswitch_64
        0x36 -> :sswitch_69
        0x39 -> :sswitch_6d
        0x41 -> :sswitch_72
    .end sparse-switch

    .line 5850
    :sswitch_data_28
    .sparse-switch
        0x32 -> :sswitch_57
        0x36 -> :sswitch_5a
    .end sparse-switch

    .line 5853
    :sswitch_data_29
    .sparse-switch
        0x4f -> :sswitch_58
        0x55 -> :sswitch_59
    .end sparse-switch

    .line 5873
    :sswitch_data_2a
    .sparse-switch
        0x4f -> :sswitch_5b
        0x55 -> :sswitch_5c
    .end sparse-switch

    .line 5898
    :sswitch_data_2b
    .sparse-switch
        0x4f -> :sswitch_5e
        0x55 -> :sswitch_5f
    .end sparse-switch

    .line 5919
    :sswitch_data_2c
    .sparse-switch
        0x47 -> :sswitch_61
        0x4f -> :sswitch_62
        0x55 -> :sswitch_63
    .end sparse-switch

    .line 5946
    :sswitch_data_2d
    .sparse-switch
        0x41 -> :sswitch_65
        0x47 -> :sswitch_66
        0x4f -> :sswitch_67
        0x55 -> :sswitch_68
    .end sparse-switch

    .line 5979
    :sswitch_data_2e
    .sparse-switch
        0x47 -> :sswitch_6a
        0x4f -> :sswitch_6b
        0x55 -> :sswitch_6c
    .end sparse-switch

    .line 6006
    :sswitch_data_2f
    .sparse-switch
        0x41 -> :sswitch_6e
        0x47 -> :sswitch_6f
        0x4f -> :sswitch_70
        0x55 -> :sswitch_71
    .end sparse-switch

    .line 6056
    :sswitch_data_30
    .sparse-switch
        0x46 -> :sswitch_75
        0x53 -> :sswitch_76
    .end sparse-switch

    .line 6080
    :sswitch_data_31
    .sparse-switch
        0x69 -> :sswitch_77
        0x6f -> :sswitch_7c
    .end sparse-switch

    .line 6083
    :sswitch_data_32
    .sparse-switch
        0x41 -> :sswitch_78
        0x49 -> :sswitch_7b
    .end sparse-switch

    .line 6085
    :sswitch_data_33
    .sparse-switch
        0x63 -> :sswitch_79
        0x6c -> :sswitch_7a
    .end sparse-switch

    .line 6117
    :sswitch_data_34
    .sparse-switch
        0x4c -> :sswitch_7d
        0x50 -> :sswitch_80
    .end sparse-switch

    .line 6120
    :sswitch_data_35
    .sparse-switch
        0x48 -> :sswitch_7e
        0x57 -> :sswitch_7f
    .end sparse-switch

    .line 6141
    :sswitch_data_36
    .sparse-switch
        0x48 -> :sswitch_81
        0x57 -> :sswitch_82
    .end sparse-switch

    .line 6172
    :sswitch_data_37
    .sparse-switch
        0x65 -> :sswitch_83
        0x69 -> :sswitch_8b
    .end sparse-switch

    .line 6175
    :sswitch_data_38
    .sparse-switch
        0x49 -> :sswitch_84
        0x4d -> :sswitch_85
        0x52 -> :sswitch_86
        0x54 -> :sswitch_89
        0x55 -> :sswitch_8a
    .end sparse-switch

    .line 6189
    :sswitch_data_39
    .sparse-switch
        0x65 -> :sswitch_87
        0x6f -> :sswitch_88
    .end sparse-switch

    .line 6243
    :sswitch_data_3a
    .sparse-switch
        0x61 -> :sswitch_8c
        0x6c -> :sswitch_8f
        0x72 -> :sswitch_94
    .end sparse-switch

    .line 6246
    :sswitch_data_3b
    .sparse-switch
        0x4c -> :sswitch_8d
        0x53 -> :sswitch_8e
    .end sparse-switch

    .line 6267
    :sswitch_data_3c
    .sparse-switch
        0x47 -> :sswitch_90
        0x53 -> :sswitch_91
    .end sparse-switch

    .line 6276
    :sswitch_data_3d
    .sparse-switch
        0x49 -> :sswitch_92
        0x54 -> :sswitch_93
    .end sparse-switch

    .line 6302
    :sswitch_data_3e
    .sparse-switch
        0x65 -> :sswitch_95
        0x69 -> :sswitch_96
        0x6f -> :sswitch_97
    .end sparse-switch

    .line 6338
    :sswitch_data_3f
    .sparse-switch
        0x63 -> :sswitch_98
        0x65 -> :sswitch_99
        0x68 -> :sswitch_9e
        0x69 -> :sswitch_ab
        0x74 -> :sswitch_ae
        0x75 -> :sswitch_c2
    .end sparse-switch

    .line 6346
    :sswitch_data_40
    .sparse-switch
        0x63 -> :sswitch_9a
        0x72 -> :sswitch_9b
    .end sparse-switch

    .line 6355
    :sswitch_data_41
    .sparse-switch
        0x22 -> :sswitch_9c
        0x4b -> :sswitch_9d
    .end sparse-switch

    .line 6380
    :sswitch_data_42
    .sparse-switch
        0x61 -> :sswitch_9f
        0x6f -> :sswitch_a6
    .end sparse-switch

    .line 6383
    :sswitch_data_43
    .sparse-switch
        0x41 -> :sswitch_a0
        0x42 -> :sswitch_a1
        0x49 -> :sswitch_a2
        0x50 -> :sswitch_a5
    .end sparse-switch

    .line 6397
    :sswitch_data_44
    .sparse-switch
        0x63 -> :sswitch_a3
        0x74 -> :sswitch_a4
    .end sparse-switch

    .line 6429
    :sswitch_data_45
    .sparse-switch
        0x41 -> :sswitch_a7
        0x4d -> :sswitch_a8
        0x53 -> :sswitch_a9
        0x55 -> :sswitch_aa
    .end sparse-switch

    .line 6467
    :sswitch_data_46
    .sparse-switch
        0x47 -> :sswitch_ac
        0x53 -> :sswitch_ad
    .end sparse-switch

    .line 6488
    :sswitch_data_47
    .sparse-switch
        0x41 -> :sswitch_af
        0x4e -> :sswitch_ba
    .end sparse-switch

    .line 6491
    :sswitch_data_48
    .sparse-switch
        0x45 -> :sswitch_b0
        0x53 -> :sswitch_b5
    .end sparse-switch

    .line 6494
    :pswitch_data_5
    .packed-switch 0x53
        :pswitch_18
        :pswitch_19
    .end packed-switch

    .line 6497
    :sswitch_data_49
    .sparse-switch
        0x48 -> :sswitch_b1
        0x4e -> :sswitch_b2
    .end sparse-switch

    .line 6518
    :sswitch_data_4a
    .sparse-switch
        0x48 -> :sswitch_b3
        0x4e -> :sswitch_b4
    .end sparse-switch

    .line 6545
    :pswitch_data_6
    .packed-switch 0x53
        :pswitch_1a
        :pswitch_1b
    .end packed-switch

    .line 6548
    :sswitch_data_4b
    .sparse-switch
        0x48 -> :sswitch_b6
        0x4e -> :sswitch_b7
    .end sparse-switch

    .line 6569
    :sswitch_data_4c
    .sparse-switch
        0x48 -> :sswitch_b8
        0x4e -> :sswitch_b9
    .end sparse-switch

    .line 6602
    :sswitch_data_4d
    .sparse-switch
        0x48 -> :sswitch_bb
        0x53 -> :sswitch_bc
        0x54 -> :sswitch_bf
    .end sparse-switch

    .line 6611
    :sswitch_data_4e
    .sparse-switch
        0x48 -> :sswitch_bd
        0x4e -> :sswitch_be
    .end sparse-switch

    .line 6632
    :sswitch_data_4f
    .sparse-switch
        0x48 -> :sswitch_c0
        0x4e -> :sswitch_c1
    .end sparse-switch

    .line 6664
    :sswitch_data_50
    .sparse-switch
        0x62 -> :sswitch_c3
        0x70 -> :sswitch_c4
    .end sparse-switch

    .line 6673
    :sswitch_data_51
    .sparse-switch
        0x43 -> :sswitch_c5
        0x46 -> :sswitch_c6
        0x4c -> :sswitch_c7
        0x4d -> :sswitch_c8
        0x50 -> :sswitch_c9
        0x53 -> :sswitch_ca
        0x55 -> :sswitch_cb
    .end sparse-switch

    .line 6712
    :sswitch_data_52
    .sparse-switch
        0x6c -> :sswitch_cc
        0x72 -> :sswitch_cd
    .end sparse-switch

    .line 6748
    :sswitch_data_53
    .sparse-switch
        0x69 -> :sswitch_ce
        0x6f -> :sswitch_cf
        0x72 -> :sswitch_d2
    .end sparse-switch

    .line 6757
    :sswitch_data_54
    .sparse-switch
        0x65 -> :sswitch_d0
        0x69 -> :sswitch_d1
    .end sparse-switch
.end method

.method private am()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigService;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6818
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6819
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 6821
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 6837
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 6824
    :sswitch_0
    invoke-direct {p0}, Lflipboard/json/JSONParser;->al()Lflipboard/objs/ConfigService;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 6827
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 6841
    :goto_1
    return-object v0

    .line 6834
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 6841
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 6821
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2c -> :sswitch_2
        0x5d -> :sswitch_1
        0x6e -> :sswitch_0
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method private an()Lflipboard/objs/FeedArticle;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 7495
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7496
    new-instance v0, Lflipboard/objs/FeedArticle;

    invoke-direct {v0}, Lflipboard/objs/FeedArticle;-><init>()V

    .line 7498
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 7541
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 7500
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 7526
    :pswitch_0
    const-string v1, "FeedArticle"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 7502
    :pswitch_1
    const-string v1, "FeedArticle"

    const-string v2, "partnerID"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7503
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedArticle;->d:Ljava/lang/String;

    goto :goto_0

    .line 7508
    :pswitch_2
    const-string v1, "FeedArticle"

    const-string v2, "sectionID"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7509
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedArticle;->b:Ljava/lang/String;

    goto :goto_0

    .line 7514
    :pswitch_3
    const-string v1, "FeedArticle"

    const-string v2, "templatePath"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7515
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedArticle;->c:Ljava/lang/String;

    goto :goto_0

    .line 7520
    :pswitch_4
    const-string v1, "FeedArticle"

    const-string v2, "url"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7521
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedArticle;->a:Ljava/lang/String;

    goto :goto_0

    .line 7531
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 7545
    :goto_1
    return-object v0

    .line 7538
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 7545
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 7498
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x22 -> :sswitch_0
        0x2c -> :sswitch_2
        0x7d -> :sswitch_1
    .end sparse-switch

    .line 7500
    :pswitch_data_0
    .packed-switch 0x70
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private ao()Lflipboard/objs/FeedItem$Location;
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 9539
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9540
    new-instance v0, Lflipboard/objs/FeedItem$Location;

    invoke-direct {v0}, Lflipboard/objs/FeedItem$Location;-><init>()V

    .line 9542
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 9599
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 9544
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 9584
    :pswitch_0
    const-string v1, "FeedItem$Location"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 9546
    :pswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 9573
    const-string v1, "FeedItem$Location"

    const-string v2, "l"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 9548
    :sswitch_1
    const-string v1, "FeedItem$Location"

    const-string v2, "lat"

    const/4 v3, 0x2

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9549
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->P()D

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/objs/FeedItem$Location;->c:D

    goto :goto_0

    .line 9554
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 9568
    const-string v1, "FeedItem$Location"

    const-string v2, "lo"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 9556
    :sswitch_3
    const-string v1, "FeedItem$Location"

    const-string v2, "locationType"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9557
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem$Location;->b:Ljava/lang/String;

    goto :goto_0

    .line 9562
    :sswitch_4
    const-string v1, "FeedItem$Location"

    const-string v2, "lon"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9563
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->P()D

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/objs/FeedItem$Location;->d:D

    goto :goto_0

    .line 9578
    :pswitch_2
    const-string v1, "FeedItem$Location"

    const-string v2, "name"

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9579
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem$Location;->a:Ljava/lang/String;

    goto :goto_0

    .line 9589
    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 9603
    :goto_1
    return-object v0

    .line 9596
    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 9603
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 9542
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_6
        0xa -> :sswitch_6
        0xd -> :sswitch_6
        0x20 -> :sswitch_6
        0x22 -> :sswitch_0
        0x2c -> :sswitch_6
        0x7d -> :sswitch_5
    .end sparse-switch

    .line 9544
    :pswitch_data_0
    .packed-switch 0x6c
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 9546
    :sswitch_data_1
    .sparse-switch
        0x61 -> :sswitch_1
        0x6f -> :sswitch_2
    .end sparse-switch

    .line 9554
    :sswitch_data_2
    .sparse-switch
        0x63 -> :sswitch_3
        0x6e -> :sswitch_4
    .end sparse-switch
.end method

.method private ap()Lflipboard/objs/FeedItem$Note;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 9706
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9707
    new-instance v0, Lflipboard/objs/FeedItem$Note;

    invoke-direct {v0}, Lflipboard/objs/FeedItem$Note;-><init>()V

    .line 9709
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 9740
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 9711
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 9725
    const-string v1, "FeedItem$Note"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 9713
    :pswitch_0
    const-string v1, "FeedItem$Note"

    const-string v2, "sectionLinks"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9714
    invoke-direct {p0}, Lflipboard/json/JSONParser;->at()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem$Note;->b:Ljava/util/List;

    goto :goto_0

    .line 9719
    :pswitch_1
    const-string v1, "FeedItem$Note"

    const-string v2, "text"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9720
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem$Note;->a:Ljava/lang/String;

    goto :goto_0

    .line 9730
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 9744
    :goto_1
    return-object v0

    .line 9737
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 9744
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 9709
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x22 -> :sswitch_0
        0x2c -> :sswitch_2
        0x7d -> :sswitch_1
    .end sparse-switch

    .line 9711
    :pswitch_data_0
    .packed-switch 0x73
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private aq()Lflipboard/objs/FeedItemRenderHints;
    .locals 5

    .prologue
    const/4 v4, 0x7

    .line 9847
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9848
    new-instance v0, Lflipboard/objs/FeedItemRenderHints;

    invoke-direct {v0}, Lflipboard/objs/FeedItemRenderHints;-><init>()V

    .line 9850
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 9883
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 9852
    :sswitch_0
    const-string v1, "FeedItemRenderHints"

    const-string v2, "header"

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9853
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 9867
    const-string v1, "FeedItemRenderHints"

    const-string v2, "header"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 9855
    :sswitch_1
    const-string v1, "FeedItemRenderHints"

    const-string v2, "headerBackgroundColor"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9856
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItemRenderHints;->a:Ljava/lang/String;

    goto :goto_0

    .line 9861
    :sswitch_2
    const-string v1, "FeedItemRenderHints"

    const-string v2, "headerImage"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9862
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItemRenderHints;->b:Lflipboard/objs/Image;

    goto :goto_0

    .line 9873
    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 9887
    :goto_1
    return-object v0

    .line 9880
    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 9887
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 9850
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_4
        0xa -> :sswitch_4
        0xd -> :sswitch_4
        0x20 -> :sswitch_4
        0x22 -> :sswitch_0
        0x2c -> :sswitch_4
        0x7d -> :sswitch_3
    .end sparse-switch

    .line 9853
    :sswitch_data_1
    .sparse-switch
        0x42 -> :sswitch_1
        0x49 -> :sswitch_2
    .end sparse-switch
.end method

.method private ar()Lflipboard/objs/FeedSection;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/16 v7, 0x9

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 9990
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9991
    new-instance v0, Lflipboard/objs/FeedSection;

    invoke-direct {v0}, Lflipboard/objs/FeedSection;-><init>()V

    .line 9993
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 10286
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 9995
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 10271
    :pswitch_0
    const-string v1, "FeedSection"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 9997
    :pswitch_1
    const-string v1, "FeedSection"

    const-string v2, "author"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9998
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 10037
    const-string v1, "FeedSection"

    const-string v2, "author"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 10000
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10001
    invoke-direct {p0}, Lflipboard/json/JSONParser;->Y()Lflipboard/objs/Author;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->p:Lflipboard/objs/Author;

    goto :goto_0

    .line 10006
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 10020
    const-string v1, "FeedSection"

    const-string v2, "authorD"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 10008
    :sswitch_3
    const-string v1, "FeedSection"

    const-string v2, "authorDescription"

    const/16 v3, 0x8

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10009
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->t:Ljava/lang/String;

    goto :goto_0

    .line 10014
    :sswitch_4
    const-string v1, "FeedSection"

    const-string v2, "authorDisplayName"

    const/16 v3, 0x8

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10015
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->q:Ljava/lang/String;

    goto :goto_0

    .line 10025
    :sswitch_5
    const-string v1, "FeedSection"

    const-string v2, "authorImage"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10026
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->u:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 10031
    :sswitch_6
    const-string v1, "FeedSection"

    const-string v2, "authorUsername"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10032
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 10043
    :pswitch_2
    const-string v1, "FeedSection"

    const-string v2, "brick"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10044
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->n:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 10049
    :pswitch_3
    const-string v1, "FeedSection"

    const-string v2, "ca"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10050
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 10064
    const-string v1, "FeedSection"

    const-string v2, "ca"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10052
    :pswitch_4
    const-string v1, "FeedSection"

    const-string v2, "campaignTarget"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10053
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 10058
    :pswitch_5
    const-string v1, "FeedSection"

    const-string v2, "canShare"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10059
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedSection;->k:Z

    goto/16 :goto_0

    .line 10070
    :pswitch_6
    const-string v1, "FeedSection"

    const-string v2, "description"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10071
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 10076
    :pswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    .line 10090
    const-string v1, "FeedSection"

    const-string v2, "e"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10078
    :sswitch_7
    const-string v1, "FeedSection"

    const-string v2, "ecommerceCheckoutURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10079
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 10084
    :sswitch_8
    const-string v1, "FeedSection"

    const-string v2, "enumerated"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10085
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedSection;->A:Z

    goto/16 :goto_0

    .line 10095
    :pswitch_8
    const-string v1, "FeedSection"

    const-string v2, "feedType"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10096
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 10101
    :pswitch_9
    const-string v1, "FeedSection"

    const-string v2, "image"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10102
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->m:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 10107
    :pswitch_a
    const-string v1, "FeedSection"

    const-string v2, "magazine"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10108
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4

    .line 10128
    const-string v1, "FeedSection"

    const-string v2, "magazine"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10110
    :sswitch_9
    const-string v1, "FeedSection"

    const-string v2, "magazineCanChangeVisibility"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10111
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedSection;->z:Z

    goto/16 :goto_0

    .line 10116
    :sswitch_a
    const-string v1, "FeedSection"

    const-string v2, "magazineTarget"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10117
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->x:Ljava/lang/String;

    goto/16 :goto_0

    .line 10122
    :sswitch_b
    const-string v1, "FeedSection"

    const-string v2, "magazineVisibility"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10123
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->y:Ljava/lang/String;

    goto/16 :goto_0

    .line 10134
    :pswitch_b
    const-string v1, "FeedSection"

    const-string v2, "noContentDisplayStyle"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10135
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->D:Ljava/lang/String;

    goto/16 :goto_0

    .line 10140
    :pswitch_c
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_5

    .line 10154
    const-string v1, "FeedSection"

    const-string v2, "p"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10142
    :sswitch_c
    const-string v1, "FeedSection"

    const-string v2, "partnerId"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10143
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 10148
    :sswitch_d
    const-string v1, "FeedSection"

    const-string v2, "private"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10149
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedSection;->e:Z

    goto/16 :goto_0

    .line 10159
    :pswitch_d
    const-string v1, "FeedSection"

    const-string v2, "remoteid"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10160
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_6

    .line 10174
    const-string v1, "FeedSection"

    const-string v2, "remoteid"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10162
    :sswitch_e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10163
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 10168
    :sswitch_f
    const-string v1, "FeedSection"

    const-string v2, "remoteidToShare"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10169
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->w:Ljava/lang/String;

    goto/16 :goto_0

    .line 10180
    :pswitch_e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_7

    .line 10215
    const-string v1, "FeedSection"

    const-string v2, "s"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10182
    :sswitch_10
    const-string v1, "FeedSection"

    const-string v2, "service"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10183
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 10188
    :sswitch_11
    const-string v1, "FeedSection"

    const-string v2, "sho"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10189
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_2

    .line 10203
    :pswitch_f
    const-string v1, "FeedSection"

    const-string v2, "sho"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10191
    :pswitch_10
    const-string v1, "FeedSection"

    const-string v2, "shouldWaitForSidebar"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10192
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedSection;->B:Z

    goto/16 :goto_0

    .line 10197
    :pswitch_11
    const-string v1, "FeedSection"

    const-string v2, "showLogotypeImage"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10198
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedSection;->C:Z

    goto/16 :goto_0

    .line 10209
    :sswitch_12
    const-string v1, "FeedSection"

    const-string v2, "socialId"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10210
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 10220
    :pswitch_12
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_8

    .line 10247
    const-string v1, "FeedSection"

    const-string v2, "t"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10222
    :sswitch_13
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_9

    .line 10236
    const-string v1, "FeedSection"

    const-string v2, "ti"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10224
    :sswitch_14
    const-string v1, "FeedSection"

    const-string v2, "tileImage"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10225
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->v:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 10230
    :sswitch_15
    const-string v1, "FeedSection"

    const-string v2, "title"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10231
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 10241
    :sswitch_16
    const-string v1, "FeedSection"

    const-string v2, "type"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10242
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 10252
    :pswitch_13
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_a

    .line 10266
    const-string v1, "FeedSection"

    const-string v2, "u"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10254
    :sswitch_17
    const-string v1, "FeedSection"

    const-string v2, "unreadRemoteid"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10255
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 10260
    :sswitch_18
    const-string v1, "FeedSection"

    const-string v2, "userid"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10261
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSection;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 10276
    :sswitch_19
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 10290
    :goto_1
    return-object v0

    .line 10283
    :sswitch_1a
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 10290
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 9993
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_1a
        0xa -> :sswitch_1a
        0xd -> :sswitch_1a
        0x20 -> :sswitch_1a
        0x22 -> :sswitch_0
        0x2c -> :sswitch_1a
        0x7d -> :sswitch_19
    .end sparse-switch

    .line 9995
    :pswitch_data_0
    .packed-switch 0x61
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_12
        :pswitch_13
    .end packed-switch

    .line 9998
    :sswitch_data_1
    .sparse-switch
        0x22 -> :sswitch_1
        0x44 -> :sswitch_2
        0x49 -> :sswitch_5
        0x55 -> :sswitch_6
    .end sparse-switch

    .line 10006
    :sswitch_data_2
    .sparse-switch
        0x65 -> :sswitch_3
        0x69 -> :sswitch_4
    .end sparse-switch

    .line 10050
    :pswitch_data_1
    .packed-switch 0x6d
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 10076
    :sswitch_data_3
    .sparse-switch
        0x63 -> :sswitch_7
        0x6e -> :sswitch_8
    .end sparse-switch

    .line 10108
    :sswitch_data_4
    .sparse-switch
        0x43 -> :sswitch_9
        0x54 -> :sswitch_a
        0x56 -> :sswitch_b
    .end sparse-switch

    .line 10140
    :sswitch_data_5
    .sparse-switch
        0x61 -> :sswitch_c
        0x72 -> :sswitch_d
    .end sparse-switch

    .line 10160
    :sswitch_data_6
    .sparse-switch
        0x22 -> :sswitch_e
        0x54 -> :sswitch_f
    .end sparse-switch

    .line 10180
    :sswitch_data_7
    .sparse-switch
        0x65 -> :sswitch_10
        0x68 -> :sswitch_11
        0x6f -> :sswitch_12
    .end sparse-switch

    .line 10189
    :pswitch_data_2
    .packed-switch 0x75
        :pswitch_10
        :pswitch_f
        :pswitch_11
    .end packed-switch

    .line 10220
    :sswitch_data_8
    .sparse-switch
        0x69 -> :sswitch_13
        0x79 -> :sswitch_16
    .end sparse-switch

    .line 10222
    :sswitch_data_9
    .sparse-switch
        0x6c -> :sswitch_14
        0x74 -> :sswitch_15
    .end sparse-switch

    .line 10252
    :sswitch_data_a
    .sparse-switch
        0x6e -> :sswitch_17
        0x73 -> :sswitch_18
    .end sparse-switch
.end method

.method private as()Lflipboard/objs/FeedSectionLink;
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 10393
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10394
    new-instance v0, Lflipboard/objs/FeedSectionLink;

    invoke-direct {v0}, Lflipboard/objs/FeedSectionLink;-><init>()V

    .line 10396
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 10565
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 10398
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 10550
    :pswitch_0
    const-string v1, "FeedSectionLink"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 10400
    :pswitch_1
    const-string v1, "FeedSectionLink"

    const-string v2, "description"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10401
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    goto :goto_0

    .line 10406
    :pswitch_2
    const-string v1, "FeedSectionLink"

    const-string v2, "feedType"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10407
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->s:Ljava/lang/String;

    goto :goto_0

    .line 10412
    :pswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 10441
    const-string v1, "FeedSectionLink"

    const-string v2, "i"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 10414
    :sswitch_1
    const-string v1, "FeedSectionLink"

    const-string v2, "image"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10415
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 10429
    const-string v1, "FeedSectionLink"

    const-string v2, "image"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 10417
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10418
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    goto :goto_0

    .line 10423
    :sswitch_3
    const-string v1, "FeedSectionLink"

    const-string v2, "imageURL"

    const/4 v3, 0x6

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10424
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->f:Ljava/lang/String;

    goto :goto_0

    .line 10435
    :sswitch_4
    const-string v1, "FeedSectionLink"

    const-string v2, "isFollowingAuthor"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10436
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedSectionLink;->o:Z

    goto/16 :goto_0

    .line 10446
    :pswitch_4
    const-string v1, "FeedSectionLink"

    const-string v2, "linkType"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10447
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 10452
    :pswitch_5
    const-string v1, "FeedSectionLink"

    const-string v2, "private"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10453
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedSectionLink;->i:Z

    goto/16 :goto_0

    .line 10458
    :pswitch_6
    const-string v1, "FeedSectionLink"

    const-string v2, "re"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10459
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    .line 10473
    const-string v1, "FeedSectionLink"

    const-string v2, "re"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10461
    :sswitch_5
    const-string v1, "FeedSectionLink"

    const-string v2, "referringText"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10462
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 10467
    :sswitch_6
    const-string v1, "FeedSectionLink"

    const-string v2, "remoteid"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10468
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 10479
    :pswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4

    .line 10505
    const-string v1, "FeedSectionLink"

    const-string v2, "s"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10481
    :sswitch_7
    const-string v1, "FeedSectionLink"

    const-string v2, "service"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10482
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 10487
    :sswitch_8
    const-string v1, "FeedSectionLink"

    const-string v2, "showFollowAction"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10488
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedSectionLink;->n:Z

    goto/16 :goto_0

    .line 10493
    :sswitch_9
    const-string v1, "FeedSectionLink"

    const-string v2, "sourceURL"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10494
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 10499
    :sswitch_a
    const-string v1, "FeedSectionLink"

    const-string v2, "subhead"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10500
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->p:Ljava/lang/String;

    goto/16 :goto_0

    .line 10510
    :pswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_5

    .line 10524
    const-string v1, "FeedSectionLink"

    const-string v2, "t"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10512
    :sswitch_b
    const-string v1, "FeedSectionLink"

    const-string v2, "title"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10513
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 10518
    :sswitch_c
    const-string v1, "FeedSectionLink"

    const-string v2, "type"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10519
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 10529
    :pswitch_9
    const-string v1, "FeedSectionLink"

    const-string v2, "user"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10530
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_6

    .line 10544
    const-string v1, "FeedSectionLink"

    const-string v2, "user"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10532
    :sswitch_d
    const-string v1, "FeedSectionLink"

    const-string v2, "userID"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10533
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 10538
    :sswitch_e
    const-string v1, "FeedSectionLink"

    const-string v2, "username"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10539
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 10555
    :sswitch_f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 10569
    :goto_1
    return-object v0

    .line 10562
    :sswitch_10
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 10569
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 10396
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_10
        0xa -> :sswitch_10
        0xd -> :sswitch_10
        0x20 -> :sswitch_10
        0x22 -> :sswitch_0
        0x2c -> :sswitch_10
        0x7d -> :sswitch_f
    .end sparse-switch

    .line 10398
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 10412
    :sswitch_data_1
    .sparse-switch
        0x6d -> :sswitch_1
        0x73 -> :sswitch_4
    .end sparse-switch

    .line 10415
    :sswitch_data_2
    .sparse-switch
        0x22 -> :sswitch_2
        0x55 -> :sswitch_3
    .end sparse-switch

    .line 10459
    :sswitch_data_3
    .sparse-switch
        0x66 -> :sswitch_5
        0x6d -> :sswitch_6
    .end sparse-switch

    .line 10479
    :sswitch_data_4
    .sparse-switch
        0x65 -> :sswitch_7
        0x68 -> :sswitch_8
        0x6f -> :sswitch_9
        0x75 -> :sswitch_a
    .end sparse-switch

    .line 10510
    :sswitch_data_5
    .sparse-switch
        0x69 -> :sswitch_b
        0x79 -> :sswitch_c
    .end sparse-switch

    .line 10530
    :sswitch_data_6
    .sparse-switch
        0x49 -> :sswitch_d
        0x6e -> :sswitch_e
    .end sparse-switch
.end method

.method private at()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedSectionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 10574
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10575
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 10577
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 10593
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 10580
    :sswitch_0
    invoke-direct {p0}, Lflipboard/json/JSONParser;->as()Lflipboard/objs/FeedSectionLink;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 10583
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 10597
    :goto_1
    return-object v0

    .line 10590
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 10597
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 10577
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2c -> :sswitch_2
        0x5d -> :sswitch_1
        0x6e -> :sswitch_0
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method private au()Lflipboard/objs/FlipResponse$DateResult;
    .locals 4

    .prologue
    .line 10913
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10914
    new-instance v0, Lflipboard/objs/FlipResponse$DateResult;

    invoke-direct {v0}, Lflipboard/objs/FlipResponse$DateResult;-><init>()V

    .line 10916
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 10934
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 10918
    :sswitch_0
    const-string v1, "FlipResponse$DateResult"

    const-string v2, "$date"

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10919
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FlipResponse$DateResult;->a:Ljava/lang/String;

    goto :goto_0

    .line 10924
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 10938
    :goto_1
    return-object v0

    .line 10931
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 10938
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 10916
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x22 -> :sswitch_0
        0x2c -> :sswitch_2
        0x7d -> :sswitch_1
    .end sparse-switch
.end method

.method private av()Lflipboard/objs/FlipResponse$FlipImage;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 11041
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 11042
    new-instance v0, Lflipboard/objs/FlipResponse$FlipImage;

    invoke-direct {v0}, Lflipboard/objs/FlipResponse$FlipImage;-><init>()V

    .line 11044
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 11106
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 11046
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 11091
    const-string v1, "FlipResponse$FlipImage"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 11048
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 11062
    const-string v1, "FlipResponse$FlipImage"

    const-string v2, "h"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 11050
    :sswitch_2
    const-string v1, "FlipResponse$FlipImage"

    const-string v2, "height"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11051
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/FlipResponse$FlipImage;->e:I

    goto :goto_0

    .line 11056
    :sswitch_3
    const-string v1, "FlipResponse$FlipImage"

    const-string v2, "hints"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11057
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FlipResponse$FlipImage;->f:Ljava/lang/String;

    goto :goto_0

    .line 11067
    :sswitch_4
    const-string v1, "FlipResponse$FlipImage"

    const-string v2, "length"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11068
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/FlipResponse$FlipImage;->c:I

    goto :goto_0

    .line 11073
    :sswitch_5
    const-string v1, "FlipResponse$FlipImage"

    const-string v2, "type"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11074
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FlipResponse$FlipImage;->b:Ljava/lang/String;

    goto :goto_0

    .line 11079
    :sswitch_6
    const-string v1, "FlipResponse$FlipImage"

    const-string v2, "url"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11080
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FlipResponse$FlipImage;->a:Ljava/lang/String;

    goto :goto_0

    .line 11085
    :sswitch_7
    const-string v1, "FlipResponse$FlipImage"

    const-string v2, "width"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11086
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/FlipResponse$FlipImage;->d:I

    goto/16 :goto_0

    .line 11096
    :sswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 11110
    :goto_1
    return-object v0

    .line 11103
    :sswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 11110
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 11044
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_9
        0xa -> :sswitch_9
        0xd -> :sswitch_9
        0x20 -> :sswitch_9
        0x22 -> :sswitch_0
        0x2c -> :sswitch_9
        0x7d -> :sswitch_8
    .end sparse-switch

    .line 11046
    :sswitch_data_1
    .sparse-switch
        0x68 -> :sswitch_1
        0x6c -> :sswitch_4
        0x74 -> :sswitch_5
        0x75 -> :sswitch_6
        0x77 -> :sswitch_7
    .end sparse-switch

    .line 11048
    :sswitch_data_2
    .sparse-switch
        0x65 -> :sswitch_2
        0x69 -> :sswitch_3
    .end sparse-switch
.end method

.method private aw()Lflipboard/objs/FlipResponse$FlipImagesResult;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 11213
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 11214
    new-instance v0, Lflipboard/objs/FlipResponse$FlipImagesResult;

    invoke-direct {v0}, Lflipboard/objs/FlipResponse$FlipImagesResult;-><init>()V

    .line 11216
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 11289
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 11218
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 11274
    :pswitch_0
    const-string v1, "FlipResponse$FlipImagesResult"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 11220
    :pswitch_1
    const-string v1, "FlipResponse$FlipImagesResult"

    const-string v2, "created"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11221
    invoke-direct {p0}, Lflipboard/json/JSONParser;->au()Lflipboard/objs/FlipResponse$DateResult;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FlipResponse$FlipImagesResult;->h:Lflipboard/objs/FlipResponse$DateResult;

    goto :goto_0

    .line 11226
    :pswitch_2
    const-string v1, "FlipResponse$FlipImagesResult"

    const-string v2, "domain"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11227
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FlipResponse$FlipImagesResult;->b:Ljava/lang/String;

    goto :goto_0

    .line 11232
    :pswitch_3
    const-string v1, "FlipResponse$FlipImagesResult"

    const-string v2, "expires"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11233
    invoke-direct {p0}, Lflipboard/json/JSONParser;->au()Lflipboard/objs/FlipResponse$DateResult;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FlipResponse$FlipImagesResult;->i:Lflipboard/objs/FlipResponse$DateResult;

    goto :goto_0

    .line 11238
    :pswitch_4
    const-string v1, "FlipResponse$FlipImagesResult"

    const-string v2, "fullscreen"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11239
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FlipResponse$FlipImagesResult;->c:Z

    goto :goto_0

    .line 11244
    :pswitch_5
    const-string v1, "FlipResponse$FlipImagesResult"

    const-string v2, "medium"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11245
    invoke-direct {p0}, Lflipboard/json/JSONParser;->av()Lflipboard/objs/FlipResponse$FlipImage;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FlipResponse$FlipImagesResult;->e:Lflipboard/objs/FlipResponse$FlipImage;

    goto :goto_0

    .line 11250
    :pswitch_6
    const-string v1, "FlipResponse$FlipImagesResult"

    const-string v2, "original"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11251
    invoke-direct {p0}, Lflipboard/json/JSONParser;->av()Lflipboard/objs/FlipResponse$FlipImage;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FlipResponse$FlipImagesResult;->d:Lflipboard/objs/FlipResponse$FlipImage;

    goto :goto_0

    .line 11256
    :pswitch_7
    const-string v1, "FlipResponse$FlipImagesResult"

    const-string v2, "referrer"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11257
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FlipResponse$FlipImagesResult;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 11262
    :pswitch_8
    const-string v1, "FlipResponse$FlipImagesResult"

    const-string v2, "small"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11263
    invoke-direct {p0}, Lflipboard/json/JSONParser;->av()Lflipboard/objs/FlipResponse$FlipImage;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FlipResponse$FlipImagesResult;->f:Lflipboard/objs/FlipResponse$FlipImage;

    goto/16 :goto_0

    .line 11268
    :pswitch_9
    const-string v1, "FlipResponse$FlipImagesResult"

    const-string v2, "url"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11269
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FlipResponse$FlipImagesResult;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 11279
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 11293
    :goto_1
    return-object v0

    .line 11286
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 11293
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 11216
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x22 -> :sswitch_0
        0x2c -> :sswitch_2
        0x7d -> :sswitch_1
    .end sparse-switch

    .line 11218
    :pswitch_data_0
    .packed-switch 0x63
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method private ax()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FlipResponse$FlipImagesResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 11298
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11299
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 11301
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 11317
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 11304
    :sswitch_0
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aw()Lflipboard/objs/FlipResponse$FlipImagesResult;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 11307
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 11321
    :goto_1
    return-object v0

    .line 11314
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 11321
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 11301
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2c -> :sswitch_2
        0x5d -> :sswitch_1
        0x6e -> :sswitch_0
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method private ay()Lflipboard/objs/Image;
    .locals 6

    .prologue
    const/16 v5, 0xb

    const/16 v4, 0xa

    const/4 v3, 0x1

    .line 11524
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 11525
    new-instance v0, Lflipboard/objs/Image;

    invoke-direct {v0}, Lflipboard/objs/Image;-><init>()V

    .line 11527
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 11622
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 11529
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 11607
    const-string v1, "Image"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 11531
    :sswitch_1
    const-string v1, "Image"

    const-string v2, "attribution"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11532
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Image;->j:Ljava/lang/String;

    goto :goto_0

    .line 11537
    :sswitch_2
    const-string v1, "Image"

    const-string v2, "largeURL"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11538
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Image;->c:Ljava/lang/String;

    goto :goto_0

    .line 11543
    :sswitch_3
    const-string v1, "Image"

    const-string v2, "mediumURL"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11544
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Image;->b:Ljava/lang/String;

    goto :goto_0

    .line 11549
    :sswitch_4
    const-string v1, "Image"

    const-string v2, "original_"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11550
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 11583
    const-string v1, "Image"

    const-string v2, "original_"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 11552
    :sswitch_5
    const-string v1, "Image"

    const-string v2, "original_features"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11553
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Image;->i:Ljava/lang/String;

    goto :goto_0

    .line 11558
    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    .line 11572
    const-string v1, "Image"

    const-string v2, "original_h"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 11560
    :sswitch_7
    const-string v1, "Image"

    const-string v2, "original_height"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11561
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/Image;->g:I

    goto/16 :goto_0

    .line 11566
    :sswitch_8
    const-string v1, "Image"

    const-string v2, "original_hints"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11567
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 11577
    :sswitch_9
    const-string v1, "Image"

    const-string v2, "original_width"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11578
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/Image;->f:I

    goto/16 :goto_0

    .line 11589
    :sswitch_a
    const-string v1, "Image"

    const-string v2, "smallURL"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11590
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Image;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 11595
    :sswitch_b
    const-string v1, "Image"

    const-string v2, "thumbnailURL"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11596
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Image;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 11601
    :sswitch_c
    const-string v1, "Image"

    const-string v2, "xlargeURL"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11602
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Image;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 11612
    :sswitch_d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 11626
    :goto_1
    return-object v0

    .line 11619
    :sswitch_e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 11626
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 11527
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_e
        0xa -> :sswitch_e
        0xd -> :sswitch_e
        0x20 -> :sswitch_e
        0x22 -> :sswitch_0
        0x2c -> :sswitch_e
        0x7d -> :sswitch_d
    .end sparse-switch

    .line 11529
    :sswitch_data_1
    .sparse-switch
        0x61 -> :sswitch_1
        0x6c -> :sswitch_2
        0x6d -> :sswitch_3
        0x6f -> :sswitch_4
        0x73 -> :sswitch_a
        0x74 -> :sswitch_b
        0x78 -> :sswitch_c
    .end sparse-switch

    .line 11550
    :sswitch_data_2
    .sparse-switch
        0x66 -> :sswitch_5
        0x68 -> :sswitch_6
        0x77 -> :sswitch_9
    .end sparse-switch

    .line 11558
    :sswitch_data_3
    .sparse-switch
        0x65 -> :sswitch_7
        0x69 -> :sswitch_8
    .end sparse-switch
.end method

.method private az()Lflipboard/objs/LightBoxes$Device;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 12063
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 12064
    new-instance v2, Lflipboard/objs/LightBoxes$Device;

    invoke-direct {v2}, Lflipboard/objs/LightBoxes$Device;-><init>()V

    .line 12066
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 12097
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 12068
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 12082
    const-string v0, "LightBoxes$Device"

    const-string v3, ""

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 12070
    :sswitch_1
    const-string v0, "LightBoxes$Device"

    const-string v3, "ipad"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12071
    invoke-direct {p0}, Lflipboard/json/JSONParser;->az()Lflipboard/objs/LightBoxes$Device;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/LightBoxes$Device;->b:Lflipboard/objs/LightBoxes$Device;

    goto :goto_0

    .line 12076
    :sswitch_2
    const-string v0, "LightBoxes$Device"

    const-string v3, "pages"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12077
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    iget v3, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v3, :sswitch_data_2

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_3
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aB()Lflipboard/objs/LightBoxes$Page;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v2, Lflipboard/objs/LightBoxes$Device;->a:Ljava/util/List;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_2

    .line 12087
    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v1, v2

    .line 12101
    :cond_2
    return-object v1

    .line 12094
    :sswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 12066
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_7
        0xa -> :sswitch_7
        0xd -> :sswitch_7
        0x20 -> :sswitch_7
        0x22 -> :sswitch_0
        0x2c -> :sswitch_7
        0x7d -> :sswitch_6
    .end sparse-switch

    .line 12068
    :sswitch_data_1
    .sparse-switch
        0x69 -> :sswitch_1
        0x70 -> :sswitch_2
    .end sparse-switch

    .line 12077
    :sswitch_data_2
    .sparse-switch
        0x9 -> :sswitch_5
        0xa -> :sswitch_5
        0xd -> :sswitch_5
        0x20 -> :sswitch_5
        0x2c -> :sswitch_5
        0x5d -> :sswitch_4
        0x6e -> :sswitch_3
        0x7b -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method public final A()Lflipboard/objs/UserService;
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x3

    const/16 v6, 0x8

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 16645
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 16646
    new-instance v1, Lflipboard/objs/UserService;

    invoke-direct {v1}, Lflipboard/objs/UserService;-><init>()V

    .line 16648
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 16831
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 16650
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 16816
    :pswitch_0
    const-string v0, "UserService"

    const-string v2, ""

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 16652
    :pswitch_1
    const-string v0, "UserService"

    const-string v2, "allowedToSubscribe"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16653
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v0

    iput-boolean v0, v1, Lflipboard/objs/UserService;->r:Z

    goto :goto_0

    .line 16658
    :pswitch_2
    const-string v0, "UserService"

    const-string v2, "co"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16659
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 16673
    const-string v0, "UserService"

    const-string v2, "co"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 16661
    :pswitch_3
    const-string v0, "UserService"

    const-string v2, "confirmedEmail"

    invoke-virtual {p0, v0, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16662
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v0

    iput-boolean v0, v1, Lflipboard/objs/UserService;->i:Z

    goto :goto_0

    .line 16667
    :pswitch_4
    const-string v0, "UserService"

    const-string v2, "cookies"

    invoke-virtual {p0, v0, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16668
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    iget v2, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v2, :sswitch_data_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_1
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aN()Lflipboard/objs/UserService$Cookie;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v1, Lflipboard/objs/UserService;->o:Ljava/util/List;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 16679
    :pswitch_5
    const-string v0, "UserService"

    const-string v2, "description"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16680
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserService;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 16685
    :pswitch_6
    const-string v0, "UserService"

    const-string v2, "email"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16686
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserService;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 16691
    :pswitch_7
    const-string v0, "UserService"

    const-string v2, "isNew"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16692
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v0

    iput-boolean v0, v1, Lflipboard/objs/UserService;->j:Z

    goto/16 :goto_0

    .line 16697
    :pswitch_8
    const-string v0, "UserService"

    const-string v2, "name"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16698
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 16703
    :pswitch_9
    const-string v0, "UserService"

    const-string v2, "profile"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16704
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    .line 16730
    const-string v0, "UserService"

    const-string v2, "profile"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 16706
    :sswitch_4
    const-string v0, "UserService"

    const-string v2, "profileImage"

    invoke-virtual {p0, v0, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16707
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserService;->p:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 16712
    :sswitch_5
    const-string v0, "UserService"

    const-string v2, "profileRemoteid"

    invoke-virtual {p0, v0, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16713
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserService;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 16718
    :sswitch_6
    const-string v0, "UserService"

    const-string v2, "profileSection"

    invoke-virtual {p0, v0, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16719
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ar()Lflipboard/objs/FeedSection;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserService;->q:Lflipboard/objs/FeedSection;

    goto/16 :goto_0

    .line 16724
    :sswitch_7
    const-string v0, "UserService"

    const-string v2, "profileURL"

    invoke-virtual {p0, v0, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16725
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserService;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 16736
    :pswitch_a
    const-string v0, "UserService"

    const-string v2, "remoteid"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16737
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserService;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 16742
    :pswitch_b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_3

    .line 16792
    const-string v0, "UserService"

    const-string v2, "s"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 16744
    :sswitch_8
    const-string v0, "UserService"

    const-string v2, "screenname"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16745
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 16750
    :sswitch_9
    const-string v0, "UserService"

    const-string v2, "service"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16751
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 16756
    :sswitch_a
    const-string v0, "UserService"

    const-string v2, "subs"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16757
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    .line 16786
    :pswitch_c
    const-string v0, "UserService"

    const-string v2, "subs"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 16759
    :pswitch_d
    const-string v0, "UserService"

    const-string v2, "subscriptionLevel"

    invoke-virtual {p0, v0, v2, v8}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16760
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_4

    .line 16774
    const-string v0, "UserService"

    const-string v2, "subscriptionLevel"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 16762
    :sswitch_b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16763
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 16768
    :sswitch_c
    const-string v0, "UserService"

    const-string v2, "subscriptionLevelDisplayName"

    const/16 v3, 0x12

    invoke-virtual {p0, v0, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16769
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserService;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 16780
    :pswitch_e
    const-string v0, "UserService"

    const-string v2, "subsectionPageKey"

    invoke-virtual {p0, v0, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16781
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserService;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 16797
    :pswitch_f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_5

    .line 16811
    const-string v0, "UserService"

    const-string v2, "u"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 16799
    :sswitch_d
    const-string v0, "UserService"

    const-string v2, "unreadRemoteid"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16800
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserService;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 16805
    :sswitch_e
    const-string v0, "UserService"

    const-string v2, "userid"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16806
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 16821
    :sswitch_f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v0, v1

    .line 16835
    :goto_3
    return-object v0

    .line 16828
    :sswitch_10
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 16835
    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    .line 16648
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_10
        0xa -> :sswitch_10
        0xd -> :sswitch_10
        0x20 -> :sswitch_10
        0x22 -> :sswitch_0
        0x2c -> :sswitch_10
        0x7d -> :sswitch_f
    .end sparse-switch

    .line 16650
    :pswitch_data_0
    .packed-switch 0x61
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_f
    .end packed-switch

    .line 16659
    :pswitch_data_1
    .packed-switch 0x6e
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 16668
    :sswitch_data_1
    .sparse-switch
        0x9 -> :sswitch_3
        0xa -> :sswitch_3
        0xd -> :sswitch_3
        0x20 -> :sswitch_3
        0x2c -> :sswitch_3
        0x5d -> :sswitch_2
        0x6e -> :sswitch_1
        0x7b -> :sswitch_1
    .end sparse-switch

    .line 16704
    :sswitch_data_2
    .sparse-switch
        0x49 -> :sswitch_4
        0x52 -> :sswitch_5
        0x53 -> :sswitch_6
        0x55 -> :sswitch_7
    .end sparse-switch

    .line 16742
    :sswitch_data_3
    .sparse-switch
        0x63 -> :sswitch_8
        0x65 -> :sswitch_9
        0x75 -> :sswitch_a
    .end sparse-switch

    .line 16757
    :pswitch_data_2
    .packed-switch 0x63
        :pswitch_d
        :pswitch_c
        :pswitch_e
    .end packed-switch

    .line 16760
    :sswitch_data_4
    .sparse-switch
        0x22 -> :sswitch_b
        0x44 -> :sswitch_c
    .end sparse-switch

    .line 16797
    :sswitch_data_5
    .sparse-switch
        0x6e -> :sswitch_d
        0x73 -> :sswitch_e
    .end sparse-switch
.end method

.method public final B()Lflipboard/objs/UserState;
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 17421
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 17422
    new-instance v0, Lflipboard/objs/UserState;

    invoke-direct {v0}, Lflipboard/objs/UserState;-><init>()V

    .line 17424
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 17532
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 17426
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 17517
    :pswitch_0
    const-string v1, "UserState"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 17428
    :pswitch_1
    const-string v1, "UserState"

    const-string v2, "code"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17429
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/UserState;->c:I

    goto :goto_0

    .line 17434
    :pswitch_2
    const-string v1, "UserState"

    const-string v2, "displaymessage"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17435
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserState;->h:Ljava/lang/String;

    goto :goto_0

    .line 17440
    :pswitch_3
    const-string v1, "UserState"

    const-string v2, "error"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17441
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 17455
    const-string v1, "UserState"

    const-string v2, "error"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 17443
    :sswitch_1
    const-string v1, "UserState"

    const-string v2, "errorcode"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17444
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/UserState;->f:I

    goto :goto_0

    .line 17449
    :sswitch_2
    const-string v1, "UserState"

    const-string v2, "errormessage"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17450
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserState;->g:Ljava/lang/String;

    goto :goto_0

    .line 17461
    :pswitch_4
    const-string v1, "UserState"

    const-string v2, "message"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17462
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserState;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 17467
    :pswitch_5
    const-string v1, "UserState"

    const-string v2, "revision"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17468
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserState;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 17473
    :pswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 17487
    const-string v1, "UserState"

    const-string v2, "s"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 17475
    :pswitch_7
    const-string v1, "UserState"

    const-string v2, "state"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17476
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aR()Lflipboard/objs/UserState$State;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    goto/16 :goto_0

    .line 17481
    :pswitch_8
    const-string v1, "UserState"

    const-string v2, "success"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17482
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/UserState;->b:Z

    goto/16 :goto_0

    .line 17492
    :pswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 17506
    const-string v1, "UserState"

    const-string v2, "t"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 17494
    :sswitch_3
    const-string v1, "UserState"

    const-string v2, "time"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17495
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/objs/UserState;->e:J

    goto/16 :goto_0

    .line 17500
    :sswitch_4
    const-string v1, "UserState"

    const-string v2, "type"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17501
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserState;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 17511
    :pswitch_a
    const-string v1, "UserState"

    const-string v2, "userid"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17512
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/UserState;->i:I

    goto/16 :goto_0

    .line 17522
    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 17536
    :goto_1
    return-object v0

    .line 17529
    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 17536
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 17424
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_6
        0xa -> :sswitch_6
        0xd -> :sswitch_6
        0x20 -> :sswitch_6
        0x22 -> :sswitch_0
        0x2c -> :sswitch_6
        0x7d -> :sswitch_5
    .end sparse-switch

    .line 17426
    :pswitch_data_0
    .packed-switch 0x63
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 17441
    :sswitch_data_1
    .sparse-switch
        0x63 -> :sswitch_1
        0x6d -> :sswitch_2
    .end sparse-switch

    .line 17473
    :pswitch_data_1
    .packed-switch 0x74
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 17492
    :sswitch_data_2
    .sparse-switch
        0x69 -> :sswitch_3
        0x79 -> :sswitch_4
    .end sparse-switch
.end method

.method public final C()Lflipboard/service/Account$Meta;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 18095
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 18096
    new-instance v0, Lflipboard/service/Account$Meta;

    invoke-direct {v0}, Lflipboard/service/Account$Meta;-><init>()V

    .line 18098
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 18141
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 18100
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 18126
    const-string v1, "Account$Meta"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 18102
    :sswitch_1
    const-string v1, "Account$Meta"

    const-string v2, "accessToken"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18103
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/Account$Meta;->d:Ljava/lang/String;

    goto :goto_0

    .line 18108
    :sswitch_2
    const-string v1, "Account$Meta"

    const-string v2, "isReadLaterService"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18109
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/service/Account$Meta;->b:Z

    goto :goto_0

    .line 18114
    :sswitch_3
    const-string v1, "Account$Meta"

    const-string v2, "refreshToken"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18115
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/Account$Meta;->e:Ljava/lang/String;

    goto :goto_0

    .line 18120
    :sswitch_4
    const-string v1, "Account$Meta"

    const-string v2, "selectedShareTargets"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18121
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->Q()Lflipboard/json/FLObject;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/Account$Meta;->c:Lflipboard/json/FLObject;

    goto :goto_0

    .line 18131
    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 18145
    :goto_1
    return-object v0

    .line 18138
    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 18145
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 18098
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_6
        0xa -> :sswitch_6
        0xd -> :sswitch_6
        0x20 -> :sswitch_6
        0x22 -> :sswitch_0
        0x2c -> :sswitch_6
        0x7d -> :sswitch_5
    .end sparse-switch

    .line 18100
    :sswitch_data_1
    .sparse-switch
        0x61 -> :sswitch_1
        0x69 -> :sswitch_2
        0x72 -> :sswitch_3
        0x73 -> :sswitch_4
    .end sparse-switch
.end method

.method public final D()Lflipboard/service/Section$Meta;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 18248
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 18249
    new-instance v0, Lflipboard/service/Section$Meta;

    invoke-direct {v0}, Lflipboard/service/Section$Meta;-><init>()V

    .line 18251
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 18355
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 18253
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 18340
    const-string v1, "Section$Meta"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 18255
    :sswitch_1
    const-string v1, "Section$Meta"

    const-string v2, "campaignTarget"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18256
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/Section$Meta;->g:Ljava/lang/String;

    goto :goto_0

    .line 18261
    :sswitch_2
    const-string v1, "Section$Meta"

    const-string v2, "ecommerceCheckoutURL"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18262
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/Section$Meta;->f:Ljava/lang/String;

    goto :goto_0

    .line 18267
    :sswitch_3
    const-string v1, "Section$Meta"

    const-string v2, "feedType"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18268
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/Section$Meta;->k:Ljava/lang/String;

    goto :goto_0

    .line 18273
    :sswitch_4
    const-string v1, "Section$Meta"

    const-string v2, "is"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18274
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 18288
    const-string v1, "Section$Meta"

    const-string v2, "is"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 18276
    :sswitch_5
    const-string v1, "Section$Meta"

    const-string v2, "isPlaceHolder"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18277
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/service/Section$Meta;->b:Z

    goto :goto_0

    .line 18282
    :sswitch_6
    const-string v1, "Section$Meta"

    const-string v2, "isUnreadMode"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18283
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/service/Section$Meta;->c:Z

    goto/16 :goto_0

    .line 18294
    :sswitch_7
    const-string v1, "Section$Meta"

    const-string v2, "noItemsText"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18295
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/Section$Meta;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 18300
    :sswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    .line 18329
    const-string v1, "Section$Meta"

    const-string v2, "p"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 18302
    :sswitch_9
    const-string v1, "Section$Meta"

    const-string v2, "partnerId"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18303
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 18308
    :sswitch_a
    const-string v1, "Section$Meta"

    const-string v2, "profile"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18309
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4

    .line 18323
    const-string v1, "Section$Meta"

    const-string v2, "profile"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 18311
    :sswitch_b
    const-string v1, "Section$Meta"

    const-string v2, "profileBackgroundImage"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18312
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/Section$Meta;->j:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 18317
    :sswitch_c
    const-string v1, "Section$Meta"

    const-string v2, "profileSectionLink"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18318
    invoke-direct {p0}, Lflipboard/json/JSONParser;->as()Lflipboard/objs/FeedSectionLink;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/Section$Meta;->i:Lflipboard/objs/FeedSectionLink;

    goto/16 :goto_0

    .line 18334
    :sswitch_d
    const-string v1, "Section$Meta"

    const-string v2, "sidebarGroups"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18335
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aI()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    goto/16 :goto_0

    .line 18345
    :sswitch_e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 18359
    :goto_1
    return-object v0

    .line 18352
    :sswitch_f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 18359
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 18251
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_f
        0xa -> :sswitch_f
        0xd -> :sswitch_f
        0x20 -> :sswitch_f
        0x22 -> :sswitch_0
        0x2c -> :sswitch_f
        0x7d -> :sswitch_e
    .end sparse-switch

    .line 18253
    :sswitch_data_1
    .sparse-switch
        0x63 -> :sswitch_1
        0x65 -> :sswitch_2
        0x66 -> :sswitch_3
        0x69 -> :sswitch_4
        0x6e -> :sswitch_7
        0x70 -> :sswitch_8
        0x73 -> :sswitch_d
    .end sparse-switch

    .line 18274
    :sswitch_data_2
    .sparse-switch
        0x50 -> :sswitch_5
        0x55 -> :sswitch_6
    .end sparse-switch

    .line 18300
    :sswitch_data_3
    .sparse-switch
        0x61 -> :sswitch_9
        0x72 -> :sswitch_a
    .end sparse-switch

    .line 18309
    :sswitch_data_4
    .sparse-switch
        0x42 -> :sswitch_b
        0x53 -> :sswitch_c
    .end sparse-switch
.end method

.method public final a()Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0x6f

    const/16 v6, 0x74

    const/16 v5, 0x61

    const/16 v4, 0x65

    const/16 v3, 0x22

    .line 33
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    const/16 v1, 0x6e

    if-ne v0, v1, :cond_0

    .line 34
    const-string v0, "null"

    invoke-super {p0, v0}, Lflipboard/json/JSONParserBase;->a(Ljava/lang/String;)V

    .line 35
    const/4 v0, 0x0

    .line 284
    :goto_0
    return-object v0

    .line 37
    :cond_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    if-eq v0, v3, :cond_1

    .line 38
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    .line 40
    :cond_1
    iget v0, p0, Lflipboard/json/JSONParser;->e:I

    .line 41
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 284
    :cond_2
    :goto_1
    invoke-super {p0, v0}, Lflipboard/json/JSONParserBase;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 43
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 44
    const-string v0, ""

    goto :goto_0

    .line 47
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    goto :goto_1

    .line 49
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x62

    if-ne v1, v2, :cond_2

    .line 50
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x75

    if-ne v1, v2, :cond_2

    .line 51
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x6d

    if-ne v1, v2, :cond_2

    .line 52
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 53
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 54
    const-string v0, "album"

    goto :goto_0

    .line 59
    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v6, :cond_2

    .line 60
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x69

    if-ne v1, v2, :cond_2

    .line 61
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x63

    if-ne v1, v2, :cond_2

    .line 62
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x6c

    if-ne v1, v2, :cond_2

    .line 63
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 64
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 65
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 66
    const-string v0, "article"

    goto :goto_0

    .line 71
    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v6, :cond_2

    .line 72
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x68

    if-ne v1, v2, :cond_2

    .line 73
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v7, :cond_2

    .line 74
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x72

    if-ne v1, v2, :cond_2

    .line 75
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 76
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 77
    const-string v0, "author"

    goto/16 :goto_0

    .line 85
    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x6e

    if-ne v1, v2, :cond_2

    .line 86
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 87
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 88
    const-string v0, "en"

    goto/16 :goto_0

    .line 93
    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    goto/16 :goto_1

    .line 95
    :sswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x63

    if-ne v1, v2, :cond_2

    .line 96
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 97
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x62

    if-ne v1, v2, :cond_2

    .line 98
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v7, :cond_2

    .line 99
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v7, :cond_2

    .line 100
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x6b

    if-ne v1, v2, :cond_2

    .line 101
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 102
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 103
    const-string v0, "facebook"

    goto/16 :goto_0

    .line 108
    :sswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x69

    if-ne v1, v2, :cond_2

    .line 109
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    goto/16 :goto_1

    .line 111
    :sswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x6b

    if-ne v1, v2, :cond_2

    .line 112
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x72

    if-ne v1, v2, :cond_2

    .line 113
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 114
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 115
    const-string v0, "flickr"

    goto/16 :goto_0

    .line 120
    :sswitch_a
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x62

    if-ne v1, v2, :cond_2

    .line 121
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v7, :cond_2

    .line 122
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 123
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x72

    if-ne v1, v2, :cond_2

    .line 124
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x64

    if-ne v1, v2, :cond_2

    .line 125
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 126
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 127
    const-string v0, "flipboard"

    goto/16 :goto_0

    .line 138
    :sswitch_b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v7, :cond_2

    .line 139
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v7, :cond_2

    .line 140
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x67

    if-ne v1, v2, :cond_2

    .line 141
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x6c

    if-ne v1, v2, :cond_2

    .line 142
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 143
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x72

    if-ne v1, v2, :cond_2

    .line 144
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 145
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 146
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x64

    if-ne v1, v2, :cond_2

    .line 147
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 148
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x72

    if-ne v1, v2, :cond_2

    .line 149
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 150
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 151
    const-string v0, "googlereader"

    goto/16 :goto_0

    .line 156
    :sswitch_c
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4

    goto/16 :goto_1

    .line 158
    :sswitch_d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 159
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 160
    const-string v0, "id"

    goto/16 :goto_0

    .line 165
    :sswitch_e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 166
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x67

    if-ne v1, v2, :cond_2

    .line 167
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 168
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 169
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 170
    const-string v0, "image"

    goto/16 :goto_0

    .line 175
    :sswitch_f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x73

    if-ne v1, v2, :cond_2

    .line 176
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v6, :cond_2

    .line 177
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 178
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x67

    if-ne v1, v2, :cond_2

    .line 179
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x72

    if-ne v1, v2, :cond_2

    .line 180
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 181
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x6d

    if-ne v1, v2, :cond_2

    .line 182
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 183
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 184
    const-string v0, "instagram"

    goto/16 :goto_0

    .line 192
    :sswitch_10
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 193
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v6, :cond_2

    .line 194
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 195
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x64

    if-ne v1, v2, :cond_2

    .line 196
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 197
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v6, :cond_2

    .line 198
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 199
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 200
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 201
    const-string v0, "metadata"

    goto/16 :goto_0

    .line 206
    :sswitch_11
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 207
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x6d

    if-ne v1, v2, :cond_2

    .line 208
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 209
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 210
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 211
    const-string v0, "name"

    goto/16 :goto_0

    .line 216
    :sswitch_12
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v7, :cond_2

    .line 217
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x73

    if-ne v1, v2, :cond_2

    .line 218
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v6, :cond_2

    .line 219
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 220
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 221
    const-string v0, "post"

    goto/16 :goto_0

    .line 226
    :sswitch_13
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v6, :cond_2

    .line 227
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 228
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v6, :cond_2

    .line 229
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x75

    if-ne v1, v2, :cond_2

    .line 230
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x73

    if-ne v1, v2, :cond_2

    .line 231
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 232
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 233
    const-string v0, "status"

    goto/16 :goto_0

    .line 238
    :sswitch_14
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x77

    if-ne v1, v2, :cond_2

    .line 239
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x69

    if-ne v1, v2, :cond_2

    .line 240
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v6, :cond_2

    .line 241
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v6, :cond_2

    .line 242
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 243
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x72

    if-ne v1, v2, :cond_2

    .line 244
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 245
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 246
    const-string v0, "twitter"

    goto/16 :goto_0

    .line 251
    :sswitch_15
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_1

    .line 253
    :pswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x6c

    if-ne v1, v2, :cond_2

    .line 254
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 255
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 256
    const-string v0, "url"

    goto/16 :goto_0

    .line 261
    :pswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 262
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x72

    if-ne v1, v2, :cond_2

    .line 263
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 264
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 265
    const-string v0, "user"

    goto/16 :goto_0

    .line 273
    :sswitch_16
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x69

    if-ne v1, v2, :cond_2

    .line 274
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    const/16 v2, 0x64

    if-ne v1, v2, :cond_2

    .line 275
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 276
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v7, :cond_2

    .line 277
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 278
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 279
    const-string v0, "video"

    goto/16 :goto_0

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_0
        0x61 -> :sswitch_1
        0x65 -> :sswitch_5
        0x66 -> :sswitch_6
        0x67 -> :sswitch_b
        0x69 -> :sswitch_c
        0x6d -> :sswitch_10
        0x6e -> :sswitch_11
        0x70 -> :sswitch_12
        0x73 -> :sswitch_13
        0x74 -> :sswitch_14
        0x75 -> :sswitch_15
        0x76 -> :sswitch_16
    .end sparse-switch

    .line 47
    :sswitch_data_1
    .sparse-switch
        0x6c -> :sswitch_2
        0x72 -> :sswitch_3
        0x75 -> :sswitch_4
    .end sparse-switch

    .line 93
    :sswitch_data_2
    .sparse-switch
        0x61 -> :sswitch_7
        0x6c -> :sswitch_8
    .end sparse-switch

    .line 109
    :sswitch_data_3
    .sparse-switch
        0x63 -> :sswitch_9
        0x70 -> :sswitch_a
    .end sparse-switch

    .line 156
    :sswitch_data_4
    .sparse-switch
        0x64 -> :sswitch_d
        0x6d -> :sswitch_e
        0x6e -> :sswitch_f
    .end sparse-switch

    .line 251
    :pswitch_data_0
    .packed-switch 0x72
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Lflipboard/objs/Ad;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/16 v6, 0xc

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 289
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 290
    new-instance v2, Lflipboard/objs/Ad;

    invoke-direct {v2}, Lflipboard/objs/Ad;-><init>()V

    .line 292
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 375
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 294
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 360
    const-string v0, "Ad"

    const-string v3, ""

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 296
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    .line 310
    const-string v0, "Ad"

    const-string v3, "a"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 298
    :sswitch_2
    const-string v0, "Ad"

    const-string v3, "ad_type"

    invoke-virtual {p0, v0, v3, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/Ad;->b:Ljava/lang/String;

    goto :goto_0

    .line 304
    :sswitch_3
    const-string v0, "Ad"

    const-string v3, "assets"

    invoke-virtual {p0, v0, v3, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    iget v3, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v3, :sswitch_data_3

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_4
    invoke-direct {p0}, Lflipboard/json/JSONParser;->T()Lflipboard/objs/Ad$Asset;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v2, Lflipboard/objs/Ad;->e:Ljava/util/List;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_2

    .line 315
    :sswitch_7
    const-string v0, "Ad"

    const-string v3, "button_info"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 316
    invoke-direct {p0}, Lflipboard/json/JSONParser;->V()Lflipboard/objs/Ad$ButtonInfo;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/Ad;->f:Lflipboard/objs/Ad$ButtonInfo;

    goto :goto_0

    .line 321
    :sswitch_8
    const-string v0, "Ad"

    const-string v3, "click_tracking_urls"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/Ad;->c:Ljava/util/List;

    goto/16 :goto_0

    .line 327
    :sswitch_9
    const-string v0, "Ad"

    const-string v3, "impression_"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 342
    :pswitch_0
    const-string v0, "Ad"

    const-string v3, "impression_"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 330
    :pswitch_1
    const-string v0, "Ad"

    const-string v3, "impression_tracking_urls"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/Ad;->d:Ljava/util/List;

    goto/16 :goto_0

    .line 336
    :pswitch_2
    const-string v0, "Ad"

    const-string v3, "impression_value"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/Ad;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 348
    :sswitch_a
    const-string v0, "Ad"

    const-string v3, "min_pages_before_shown"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v2, Lflipboard/objs/Ad;->h:I

    goto/16 :goto_0

    .line 354
    :sswitch_b
    const-string v0, "Ad"

    const-string v3, "video_info"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    invoke-direct {p0}, Lflipboard/json/JSONParser;->X()Lflipboard/objs/Ad$VideoInfo;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/Ad;->k:Lflipboard/objs/Ad$VideoInfo;

    goto/16 :goto_0

    .line 365
    :sswitch_c
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v1, v2

    .line 379
    :cond_2
    return-object v1

    .line 372
    :sswitch_d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 292
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_d
        0xa -> :sswitch_d
        0xd -> :sswitch_d
        0x20 -> :sswitch_d
        0x22 -> :sswitch_0
        0x2c -> :sswitch_d
        0x7d -> :sswitch_c
    .end sparse-switch

    .line 294
    :sswitch_data_1
    .sparse-switch
        0x61 -> :sswitch_1
        0x62 -> :sswitch_7
        0x63 -> :sswitch_8
        0x69 -> :sswitch_9
        0x6d -> :sswitch_a
        0x76 -> :sswitch_b
    .end sparse-switch

    .line 296
    :sswitch_data_2
    .sparse-switch
        0x64 -> :sswitch_2
        0x73 -> :sswitch_3
    .end sparse-switch

    .line 305
    :sswitch_data_3
    .sparse-switch
        0x9 -> :sswitch_6
        0xa -> :sswitch_6
        0xd -> :sswitch_6
        0x20 -> :sswitch_6
        0x2c -> :sswitch_6
        0x5d -> :sswitch_5
        0x6e -> :sswitch_4
        0x7b -> :sswitch_4
    .end sparse-switch

    .line 328
    :pswitch_data_0
    .packed-switch 0x74
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final c()Lflipboard/objs/AlsoFlippedResult;
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x1

    .line 1488
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1489
    new-instance v0, Lflipboard/objs/AlsoFlippedResult;

    invoke-direct {v0}, Lflipboard/objs/AlsoFlippedResult;-><init>()V

    .line 1491
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 1567
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 1493
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 1552
    const-string v1, "AlsoFlippedResult"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1495
    :sswitch_1
    const-string v1, "AlsoFlippedResult"

    const-string v2, "code"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1496
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/AlsoFlippedResult;->c:I

    goto :goto_0

    .line 1501
    :sswitch_2
    const-string v1, "AlsoFlippedResult"

    const-string v2, "displaymessage"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1502
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/AlsoFlippedResult;->h:Ljava/lang/String;

    goto :goto_0

    .line 1507
    :sswitch_3
    const-string v1, "AlsoFlippedResult"

    const-string v2, "error"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1508
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 1522
    const-string v1, "AlsoFlippedResult"

    const-string v2, "error"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1510
    :sswitch_4
    const-string v1, "AlsoFlippedResult"

    const-string v2, "errorcode"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1511
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/AlsoFlippedResult;->f:I

    goto :goto_0

    .line 1516
    :sswitch_5
    const-string v1, "AlsoFlippedResult"

    const-string v2, "errormessage"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1517
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/AlsoFlippedResult;->g:Ljava/lang/String;

    goto :goto_0

    .line 1528
    :sswitch_6
    const-string v1, "AlsoFlippedResult"

    const-string v2, "message"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1529
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/AlsoFlippedResult;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 1534
    :sswitch_7
    const-string v1, "AlsoFlippedResult"

    const-string v2, "results"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1535
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->l()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/AlsoFlippedResult;->a:Ljava/util/List;

    goto/16 :goto_0

    .line 1540
    :sswitch_8
    const-string v1, "AlsoFlippedResult"

    const-string v2, "success"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1541
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/AlsoFlippedResult;->b:Z

    goto/16 :goto_0

    .line 1546
    :sswitch_9
    const-string v1, "AlsoFlippedResult"

    const-string v2, "time"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1547
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/objs/AlsoFlippedResult;->e:J

    goto/16 :goto_0

    .line 1557
    :sswitch_a
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 1571
    :goto_1
    return-object v0

    .line 1564
    :sswitch_b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 1571
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1491
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_b
        0xa -> :sswitch_b
        0xd -> :sswitch_b
        0x20 -> :sswitch_b
        0x22 -> :sswitch_0
        0x2c -> :sswitch_b
        0x7d -> :sswitch_a
    .end sparse-switch

    .line 1493
    :sswitch_data_1
    .sparse-switch
        0x63 -> :sswitch_1
        0x64 -> :sswitch_2
        0x65 -> :sswitch_3
        0x6d -> :sswitch_6
        0x72 -> :sswitch_7
        0x73 -> :sswitch_8
        0x74 -> :sswitch_9
    .end sparse-switch

    .line 1508
    :sswitch_data_2
    .sparse-switch
        0x63 -> :sswitch_4
        0x6d -> :sswitch_5
    .end sparse-switch
.end method

.method public final d()Lflipboard/objs/CommentaryResult;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x6

    const/4 v6, 0x1

    .line 2055
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2056
    new-instance v2, Lflipboard/objs/CommentaryResult;

    invoke-direct {v2}, Lflipboard/objs/CommentaryResult;-><init>()V

    .line 2058
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 2134
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 2060
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 2119
    const-string v0, "CommentaryResult"

    const-string v3, ""

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2062
    :sswitch_1
    const-string v0, "CommentaryResult"

    const-string v3, "code"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2063
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v2, Lflipboard/objs/CommentaryResult;->c:I

    goto :goto_0

    .line 2068
    :sswitch_2
    const-string v0, "CommentaryResult"

    const-string v3, "displaymessage"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2069
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/CommentaryResult;->h:Ljava/lang/String;

    goto :goto_0

    .line 2074
    :sswitch_3
    const-string v0, "CommentaryResult"

    const-string v3, "error"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2075
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    .line 2089
    const-string v0, "CommentaryResult"

    const-string v3, "error"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2077
    :sswitch_4
    const-string v0, "CommentaryResult"

    const-string v3, "errorcode"

    invoke-virtual {p0, v0, v3, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2078
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v2, Lflipboard/objs/CommentaryResult;->f:I

    goto :goto_0

    .line 2083
    :sswitch_5
    const-string v0, "CommentaryResult"

    const-string v3, "errormessage"

    invoke-virtual {p0, v0, v3, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2084
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/CommentaryResult;->g:Ljava/lang/String;

    goto :goto_0

    .line 2095
    :sswitch_6
    const-string v0, "CommentaryResult"

    const-string v3, "items"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2096
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    iget v3, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v3, :sswitch_data_3

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->e()Lflipboard/objs/CommentaryResult$Item;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v2, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_2

    .line 2101
    :sswitch_a
    const-string v0, "CommentaryResult"

    const-string v3, "message"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2102
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/CommentaryResult;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 2107
    :sswitch_b
    const-string v0, "CommentaryResult"

    const-string v3, "success"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2108
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v0

    iput-boolean v0, v2, Lflipboard/objs/CommentaryResult;->b:Z

    goto/16 :goto_0

    .line 2113
    :sswitch_c
    const-string v0, "CommentaryResult"

    const-string v3, "time"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2114
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v4

    iput-wide v4, v2, Lflipboard/objs/CommentaryResult;->e:J

    goto/16 :goto_0

    .line 2124
    :sswitch_d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v1, v2

    .line 2138
    :cond_2
    return-object v1

    .line 2131
    :sswitch_e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 2058
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_e
        0xa -> :sswitch_e
        0xd -> :sswitch_e
        0x20 -> :sswitch_e
        0x22 -> :sswitch_0
        0x2c -> :sswitch_e
        0x7d -> :sswitch_d
    .end sparse-switch

    .line 2060
    :sswitch_data_1
    .sparse-switch
        0x63 -> :sswitch_1
        0x64 -> :sswitch_2
        0x65 -> :sswitch_3
        0x69 -> :sswitch_6
        0x6d -> :sswitch_a
        0x73 -> :sswitch_b
        0x74 -> :sswitch_c
    .end sparse-switch

    .line 2075
    :sswitch_data_2
    .sparse-switch
        0x63 -> :sswitch_4
        0x6d -> :sswitch_5
    .end sparse-switch

    .line 2096
    :sswitch_data_3
    .sparse-switch
        0x9 -> :sswitch_9
        0xa -> :sswitch_9
        0xd -> :sswitch_9
        0x20 -> :sswitch_9
        0x2c -> :sswitch_9
        0x5d -> :sswitch_8
        0x6e -> :sswitch_7
        0x7b -> :sswitch_7
    .end sparse-switch
.end method

.method public final e()Lflipboard/objs/CommentaryResult$Item;
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/16 v7, 0x8

    const/16 v6, 0x9

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 2241
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2242
    new-instance v1, Lflipboard/objs/CommentaryResult$Item;

    invoke-direct {v1}, Lflipboard/objs/CommentaryResult$Item;-><init>()V

    .line 2244
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 2463
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 2246
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2448
    :pswitch_0
    const-string v0, "CommentaryResult$Item"

    const-string v2, ""

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2248
    :pswitch_1
    const-string v0, "CommentaryResult$Item"

    const-string v2, "allItems"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2249
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 2281
    const-string v0, "CommentaryResult$Item"

    const-string v2, "allItems"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2251
    :sswitch_1
    const-string v0, "CommentaryResult$Item"

    const-string v2, "allItemsCommentCount"

    invoke-virtual {p0, v0, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2252
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/CommentaryResult$Item;->q:I

    goto :goto_0

    .line 2257
    :sswitch_2
    const-string v0, "CommentaryResult$Item"

    const-string v2, "allItemsFlipCount"

    invoke-virtual {p0, v0, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2258
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/CommentaryResult$Item;->p:I

    goto :goto_0

    .line 2263
    :sswitch_3
    const-string v0, "CommentaryResult$Item"

    const-string v2, "allItemsLikeCount"

    invoke-virtual {p0, v0, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2264
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/CommentaryResult$Item;->m:I

    goto :goto_0

    .line 2269
    :sswitch_4
    const-string v0, "CommentaryResult$Item"

    const-string v2, "allItemsShareCount"

    invoke-virtual {p0, v0, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2270
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/CommentaryResult$Item;->r:I

    goto :goto_0

    .line 2275
    :sswitch_5
    const-string v0, "CommentaryResult$Item"

    const-string v2, "allItemsViewCount"

    invoke-virtual {p0, v0, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2276
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/CommentaryResult$Item;->o:I

    goto/16 :goto_0

    .line 2287
    :pswitch_2
    const-string v0, "CommentaryResult$Item"

    const-string v2, "co"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2288
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 2323
    const-string v0, "CommentaryResult$Item"

    const-string v2, "co"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2290
    :pswitch_3
    const-string v0, "CommentaryResult$Item"

    const-string v2, "comment"

    invoke-virtual {p0, v0, v2, v8}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2291
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    .line 2311
    const-string v0, "CommentaryResult$Item"

    const-string v2, "comment"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2293
    :sswitch_6
    const-string v0, "CommentaryResult$Item"

    const-string v2, "commentCount"

    invoke-virtual {p0, v0, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2294
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/CommentaryResult$Item;->b:I

    goto/16 :goto_0

    .line 2299
    :sswitch_7
    const-string v0, "CommentaryResult$Item"

    const-string v2, "commentary"

    invoke-virtual {p0, v0, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2300
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->f()Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    goto/16 :goto_0

    .line 2305
    :sswitch_8
    const-string v0, "CommentaryResult$Item"

    const-string v2, "commentsPageKey"

    invoke-virtual {p0, v0, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2306
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/CommentaryResult$Item;->v:Ljava/lang/String;

    goto/16 :goto_0

    .line 2317
    :pswitch_4
    const-string v0, "CommentaryResult$Item"

    const-string v2, "contributorCount"

    invoke-virtual {p0, v0, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2318
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/CommentaryResult$Item;->t:I

    goto/16 :goto_0

    .line 2329
    :pswitch_5
    const-string v0, "CommentaryResult$Item"

    const-string v2, "genericCount"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2330
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/CommentaryResult$Item;->f:I

    goto/16 :goto_0

    .line 2335
    :pswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_3

    .line 2355
    const-string v0, "CommentaryResult$Item"

    const-string v2, "i"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2337
    :sswitch_9
    const-string v0, "CommentaryResult$Item"

    const-string v2, "id"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2338
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/CommentaryResult$Item;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 2343
    :sswitch_a
    const-string v0, "CommentaryResult$Item"

    const-string v2, "inReplyToChain"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2344
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->l()Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/CommentaryResult$Item;->h:Ljava/util/List;

    goto/16 :goto_0

    .line 2349
    :sswitch_b
    const-string v0, "CommentaryResult$Item"

    const-string v2, "itemCount"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2350
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/CommentaryResult$Item;->l:I

    goto/16 :goto_0

    .line 2360
    :pswitch_7
    const-string v0, "CommentaryResult$Item"

    const-string v2, "like"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2361
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_4

    .line 2375
    const-string v0, "CommentaryResult$Item"

    const-string v2, "like"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2363
    :sswitch_c
    const-string v0, "CommentaryResult$Item"

    const-string v2, "likeCount"

    const/4 v3, 0x5

    invoke-virtual {p0, v0, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2364
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/CommentaryResult$Item;->c:I

    goto/16 :goto_0

    .line 2369
    :sswitch_d
    const-string v0, "CommentaryResult$Item"

    const-string v2, "likesPageKey"

    const/4 v3, 0x5

    invoke-virtual {p0, v0, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2370
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/CommentaryResult$Item;->u:Ljava/lang/String;

    goto/16 :goto_0

    .line 2381
    :pswitch_8
    const-string v0, "CommentaryResult$Item"

    const-string v2, "metrics"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2382
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/CommentaryResult$Item;->k:Ljava/util/List;

    goto/16 :goto_0

    .line 2387
    :pswitch_9
    const-string v0, "CommentaryResult$Item"

    const-string v2, "profileMetrics"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2388
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    iget v2, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v2, :sswitch_data_5

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_e
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aa()Lflipboard/objs/CommentaryResult$Item$ProfileMetric;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v1, Lflipboard/objs/CommentaryResult$Item;->i:Ljava/util/List;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 2393
    :pswitch_a
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_6

    .line 2437
    const-string v0, "CommentaryResult$Item"

    const-string v2, "s"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2395
    :sswitch_11
    const-string v0, "CommentaryResult$Item"

    const-string v2, "share"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2396
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_7

    .line 2410
    const-string v0, "CommentaryResult$Item"

    const-string v2, "share"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2398
    :sswitch_12
    const-string v0, "CommentaryResult$Item"

    const-string v2, "shareCount"

    const/4 v3, 0x6

    invoke-virtual {p0, v0, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2399
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/CommentaryResult$Item;->d:I

    goto/16 :goto_0

    .line 2404
    :sswitch_13
    const-string v0, "CommentaryResult$Item"

    const-string v2, "sharedwithCount"

    const/4 v3, 0x6

    invoke-virtual {p0, v0, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2405
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/CommentaryResult$Item;->e:I

    goto/16 :goto_0

    .line 2416
    :sswitch_14
    const-string v0, "CommentaryResult$Item"

    const-string v2, "subscribersCount"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2417
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_8

    .line 2431
    const-string v0, "CommentaryResult$Item"

    const-string v2, "subscribersCount"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2419
    :sswitch_15
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2420
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/CommentaryResult$Item;->s:I

    goto/16 :goto_0

    .line 2425
    :sswitch_16
    const-string v0, "CommentaryResult$Item"

    const-string v2, "subscribersCountl"

    const/16 v3, 0x11

    invoke-virtual {p0, v0, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2426
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/CommentaryResult$Item;->n:I

    goto/16 :goto_0

    .line 2442
    :pswitch_b
    const-string v0, "CommentaryResult$Item"

    const-string v2, "ttl"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2443
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v2

    iput-wide v2, v1, Lflipboard/objs/CommentaryResult$Item;->j:J

    goto/16 :goto_0

    .line 2453
    :sswitch_17
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v0, v1

    .line 2467
    :goto_3
    return-object v0

    .line 2460
    :sswitch_18
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 2467
    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    .line 2244
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_18
        0xa -> :sswitch_18
        0xd -> :sswitch_18
        0x20 -> :sswitch_18
        0x22 -> :sswitch_0
        0x2c -> :sswitch_18
        0x7d -> :sswitch_17
    .end sparse-switch

    .line 2246
    :pswitch_data_0
    .packed-switch 0x61
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 2249
    :sswitch_data_1
    .sparse-switch
        0x43 -> :sswitch_1
        0x46 -> :sswitch_2
        0x4c -> :sswitch_3
        0x53 -> :sswitch_4
        0x56 -> :sswitch_5
    .end sparse-switch

    .line 2288
    :pswitch_data_1
    .packed-switch 0x6d
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 2291
    :sswitch_data_2
    .sparse-switch
        0x43 -> :sswitch_6
        0x61 -> :sswitch_7
        0x73 -> :sswitch_8
    .end sparse-switch

    .line 2335
    :sswitch_data_3
    .sparse-switch
        0x64 -> :sswitch_9
        0x6e -> :sswitch_a
        0x74 -> :sswitch_b
    .end sparse-switch

    .line 2361
    :sswitch_data_4
    .sparse-switch
        0x43 -> :sswitch_c
        0x73 -> :sswitch_d
    .end sparse-switch

    .line 2388
    :sswitch_data_5
    .sparse-switch
        0x9 -> :sswitch_10
        0xa -> :sswitch_10
        0xd -> :sswitch_10
        0x20 -> :sswitch_10
        0x2c -> :sswitch_10
        0x5d -> :sswitch_f
        0x6e -> :sswitch_e
        0x7b -> :sswitch_e
    .end sparse-switch

    .line 2393
    :sswitch_data_6
    .sparse-switch
        0x68 -> :sswitch_11
        0x75 -> :sswitch_14
    .end sparse-switch

    .line 2396
    :sswitch_data_7
    .sparse-switch
        0x43 -> :sswitch_12
        0x64 -> :sswitch_13
    .end sparse-switch

    .line 2417
    :sswitch_data_8
    .sparse-switch
        0x22 -> :sswitch_15
        0x6c -> :sswitch_16
    .end sparse-switch
.end method

.method public final f()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/CommentaryResult$Item$Commentary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2755
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2756
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2758
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 2774
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 2761
    :sswitch_0
    invoke-direct {p0}, Lflipboard/json/JSONParser;->Z()Lflipboard/objs/CommentaryResult$Item$Commentary;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2764
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 2778
    :goto_1
    return-object v0

    .line 2771
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 2778
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 2758
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2c -> :sswitch_2
        0x5d -> :sswitch_1
        0x6e -> :sswitch_0
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method public final g()Lflipboard/objs/ConfigContentGuide;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 3218
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3219
    new-instance v2, Lflipboard/objs/ConfigContentGuide;

    invoke-direct {v2}, Lflipboard/objs/ConfigContentGuide;-><init>()V

    .line 3221
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 3252
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 3223
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 3237
    const-string v0, "ConfigContentGuide"

    const-string v3, ""

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3225
    :sswitch_1
    const-string v0, "ConfigContentGuide"

    const-string v3, "editions"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3226
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    iget v3, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v3, :sswitch_data_2

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_2
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ac()Lflipboard/objs/ConfigEdition;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v2, Lflipboard/objs/ConfigContentGuide;->b:Ljava/util/List;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_2

    .line 3231
    :sswitch_5
    const-string v0, "ConfigContentGuide"

    const-string v3, "sections"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3232
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ae()Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/ConfigContentGuide;->a:Ljava/util/List;

    goto :goto_0

    .line 3242
    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v1, v2

    .line 3256
    :cond_2
    return-object v1

    .line 3249
    :sswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 3221
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_7
        0xa -> :sswitch_7
        0xd -> :sswitch_7
        0x20 -> :sswitch_7
        0x22 -> :sswitch_0
        0x2c -> :sswitch_7
        0x7d -> :sswitch_6
    .end sparse-switch

    .line 3223
    :sswitch_data_1
    .sparse-switch
        0x65 -> :sswitch_1
        0x73 -> :sswitch_5
    .end sparse-switch

    .line 3226
    :sswitch_data_2
    .sparse-switch
        0x9 -> :sswitch_4
        0xa -> :sswitch_4
        0xd -> :sswitch_4
        0x20 -> :sswitch_4
        0x2c -> :sswitch_4
        0x5d -> :sswitch_3
        0x6e -> :sswitch_2
        0x7b -> :sswitch_2
    .end sparse-switch
.end method

.method public final h()Lflipboard/objs/ConfigHints;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 3728
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3729
    new-instance v2, Lflipboard/objs/ConfigHints;

    invoke-direct {v2}, Lflipboard/objs/ConfigHints;-><init>()V

    .line 3731
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 3777
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 3733
    :sswitch_0
    const-string v0, "ConfigHints"

    const-string v3, "hint"

    const/4 v4, 0x0

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3734
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 3761
    const-string v0, "ConfigHints"

    const-string v3, "hint"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3736
    :sswitch_1
    const-string v0, "ConfigHints"

    const-string v3, "hintSeparationTimeInterval"

    const/4 v4, 0x5

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3737
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v0

    iput v0, v2, Lflipboard/objs/ConfigHints;->b:F

    goto :goto_0

    .line 3742
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    .line 3756
    const-string v0, "ConfigHints"

    const-string v3, "hints"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3744
    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3745
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    iget v3, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v3, :sswitch_data_3

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_4
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ah()Lflipboard/objs/ConfigHints$Hint;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v2, Lflipboard/objs/ConfigHints;->c:Ljava/util/List;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_2

    .line 3750
    :sswitch_7
    const-string v0, "ConfigHints"

    const-string v3, "hintsPerSessionFrequencyCap"

    const/4 v4, 0x6

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3751
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v0

    iput v0, v2, Lflipboard/objs/ConfigHints;->a:F

    goto/16 :goto_0

    .line 3767
    :sswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v1, v2

    .line 3781
    :cond_2
    return-object v1

    .line 3774
    :sswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 3731
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_9
        0xa -> :sswitch_9
        0xd -> :sswitch_9
        0x20 -> :sswitch_9
        0x22 -> :sswitch_0
        0x2c -> :sswitch_9
        0x7d -> :sswitch_8
    .end sparse-switch

    .line 3734
    :sswitch_data_1
    .sparse-switch
        0x53 -> :sswitch_1
        0x73 -> :sswitch_2
    .end sparse-switch

    .line 3742
    :sswitch_data_2
    .sparse-switch
        0x22 -> :sswitch_3
        0x50 -> :sswitch_7
    .end sparse-switch

    .line 3745
    :sswitch_data_3
    .sparse-switch
        0x9 -> :sswitch_6
        0xa -> :sswitch_6
        0xd -> :sswitch_6
        0x20 -> :sswitch_6
        0x2c -> :sswitch_6
        0x5d -> :sswitch_5
        0x6e -> :sswitch_4
        0x7b -> :sswitch_4
    .end sparse-switch
.end method

.method public final i()Lflipboard/objs/ConfigPopularSearches;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 4474
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4475
    new-instance v2, Lflipboard/objs/ConfigPopularSearches;

    invoke-direct {v2}, Lflipboard/objs/ConfigPopularSearches;-><init>()V

    .line 4477
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 4508
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 4479
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 4493
    const-string v0, "ConfigPopularSearches"

    const-string v3, ""

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4481
    :sswitch_1
    const-string v0, "ConfigPopularSearches"

    const-string v3, "categories"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4482
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    iget v3, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v3, :sswitch_data_2

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_2
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ai()Lflipboard/objs/ConfigPopularSearches$Category;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v2, Lflipboard/objs/ConfigPopularSearches;->b:Ljava/util/List;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_2

    .line 4487
    :sswitch_5
    const-string v0, "ConfigPopularSearches"

    const-string v3, "title"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4488
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/ConfigPopularSearches;->a:Ljava/lang/String;

    goto :goto_0

    .line 4498
    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v1, v2

    .line 4512
    :cond_2
    return-object v1

    .line 4505
    :sswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 4477
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_7
        0xa -> :sswitch_7
        0xd -> :sswitch_7
        0x20 -> :sswitch_7
        0x22 -> :sswitch_0
        0x2c -> :sswitch_7
        0x7d -> :sswitch_6
    .end sparse-switch

    .line 4479
    :sswitch_data_1
    .sparse-switch
        0x63 -> :sswitch_1
        0x74 -> :sswitch_5
    .end sparse-switch

    .line 4482
    :sswitch_data_2
    .sparse-switch
        0x9 -> :sswitch_4
        0xa -> :sswitch_4
        0xd -> :sswitch_4
        0x20 -> :sswitch_4
        0x2c -> :sswitch_4
        0x5d -> :sswitch_3
        0x6e -> :sswitch_2
        0x7b -> :sswitch_2
    .end sparse-switch
.end method

.method public final j()Lflipboard/objs/ConfigServices;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 6916
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6917
    new-instance v0, Lflipboard/objs/ConfigServices;

    invoke-direct {v0}, Lflipboard/objs/ConfigServices;-><init>()V

    .line 6919
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 6987
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 6921
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 6972
    const-string v1, "ConfigServices"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 6923
    :sswitch_1
    const-string v1, "ConfigServices"

    const-string v2, "description"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6924
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigServices;->bQ:Ljava/lang/String;

    goto :goto_0

    .line 6929
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 6943
    const-string v1, "ConfigServices"

    const-string v2, "i"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 6931
    :sswitch_3
    const-string v1, "ConfigServices"

    const-string v2, "icon"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6932
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigServices;->bR:Ljava/lang/String;

    goto :goto_0

    .line 6937
    :sswitch_4
    const-string v1, "ConfigServices"

    const-string v2, "isFavicon"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6938
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/ConfigServices;->bS:Z

    goto :goto_0

    .line 6948
    :sswitch_5
    const-string v1, "ConfigServices"

    const-string v2, "readLaterServices"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6949
    invoke-direct {p0}, Lflipboard/json/JSONParser;->am()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigServices;->c:Ljava/util/List;

    goto :goto_0

    .line 6954
    :sswitch_6
    const-string v1, "ConfigServices"

    const-string v2, "services"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6955
    invoke-direct {p0}, Lflipboard/json/JSONParser;->am()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigServices;->b:Ljava/util/List;

    goto :goto_0

    .line 6960
    :sswitch_7
    const-string v1, "ConfigServices"

    const-string v2, "title"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6961
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/ConfigServices;->bP:Ljava/lang/String;

    goto/16 :goto_0

    .line 6966
    :sswitch_8
    const-string v1, "ConfigServices"

    const-string v2, "version"

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6967
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/ConfigServices;->a:I

    goto/16 :goto_0

    .line 6977
    :sswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 6991
    :goto_1
    return-object v0

    .line 6984
    :sswitch_a
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 6991
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 6919
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_a
        0xa -> :sswitch_a
        0xd -> :sswitch_a
        0x20 -> :sswitch_a
        0x22 -> :sswitch_0
        0x2c -> :sswitch_a
        0x7d -> :sswitch_9
    .end sparse-switch

    .line 6921
    :sswitch_data_1
    .sparse-switch
        0x64 -> :sswitch_1
        0x69 -> :sswitch_2
        0x72 -> :sswitch_5
        0x73 -> :sswitch_6
        0x74 -> :sswitch_7
        0x76 -> :sswitch_8
    .end sparse-switch

    .line 6929
    :sswitch_data_2
    .sparse-switch
        0x63 -> :sswitch_3
        0x73 -> :sswitch_4
    .end sparse-switch
.end method

.method public final k()Lflipboard/objs/FeedItem;
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x6

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x3

    .line 7648
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7649
    new-instance v0, Lflipboard/objs/FeedItem;

    invoke-direct {v0}, Lflipboard/objs/FeedItem;-><init>()V

    .line 7651
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 9432
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 7653
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 9417
    :pswitch_0
    const-string v1, "FeedItem"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 7655
    :pswitch_1
    const-string v1, "FeedItem"

    const-string v2, "EOS"

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7656
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->ax:Z

    goto :goto_0

    .line 7661
    :pswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 7889
    const-string v1, "FeedItem"

    const-string v2, "a"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 7663
    :sswitch_1
    const-string v1, "FeedItem"

    const-string v2, "action"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7664
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 7678
    const-string v1, "FeedItem"

    const-string v2, "action"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 7666
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7667
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bg:Ljava/lang/String;

    goto :goto_0

    .line 7672
    :sswitch_3
    const-string v1, "FeedItem"

    const-string v2, "actionTitle"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7673
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bQ:Ljava/lang/String;

    goto :goto_0

    .line 7684
    :sswitch_4
    const-string v1, "FeedItem"

    const-string v2, "additionalUsage"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7685
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->Q()Lflipboard/json/FLObject;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->cn:Lflipboard/json/FLObject;

    goto/16 :goto_0

    .line 7690
    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    .line 7746
    const-string v1, "FeedItem"

    const-string v2, "al"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7692
    :sswitch_6
    const-string v1, "FeedItem"

    const-string v2, "album"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7693
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4

    .line 7707
    const-string v1, "FeedItem"

    const-string v2, "album"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7695
    :sswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7696
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bt:Ljava/lang/String;

    goto/16 :goto_0

    .line 7701
    :sswitch_8
    const-string v1, "FeedItem"

    const-string v2, "albumArtImage"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7702
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 7713
    :sswitch_9
    const-string v1, "FeedItem"

    const-string v2, "alert"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7714
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 7728
    const-string v1, "FeedItem"

    const-string v2, "alert"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7716
    :pswitch_3
    const-string v1, "FeedItem"

    const-string v2, "alertLink"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7717
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->p:Ljava/lang/String;

    goto/16 :goto_0

    .line 7722
    :pswitch_4
    const-string v1, "FeedItem"

    const-string v2, "alertMessage"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7723
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 7734
    :sswitch_a
    const-string v1, "FeedItem"

    const-string v2, "algorithm"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7735
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aN:Ljava/lang/String;

    goto/16 :goto_0

    .line 7740
    :sswitch_b
    const-string v1, "FeedItem"

    const-string v2, "allowFullscreenImage"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7741
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->am:Z

    goto/16 :goto_0

    .line 7751
    :sswitch_c
    const-string v1, "FeedItem"

    const-string v2, "arti"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7752
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_5

    .line 7796
    const-string v1, "FeedItem"

    const-string v2, "arti"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7754
    :sswitch_d
    const-string v1, "FeedItem"

    const-string v2, "article"

    const/4 v3, 0x5

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7755
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_6

    .line 7784
    const-string v1, "FeedItem"

    const-string v2, "article"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7757
    :sswitch_e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7758
    invoke-direct {p0}, Lflipboard/json/JSONParser;->an()Lflipboard/objs/FeedArticle;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aF:Lflipboard/objs/FeedArticle;

    goto/16 :goto_0

    .line 7763
    :sswitch_f
    const-string v1, "FeedItem"

    const-string v2, "articleMarkup"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7764
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_7

    .line 7778
    const-string v1, "FeedItem"

    const-string v2, "articleMarkup"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7766
    :sswitch_10
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7767
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aG:Ljava/lang/String;

    goto/16 :goto_0

    .line 7772
    :sswitch_11
    const-string v1, "FeedItem"

    const-string v2, "articleMarkupEscaped"

    const/16 v3, 0xe

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7773
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aH:Ljava/lang/String;

    goto/16 :goto_0

    .line 7790
    :sswitch_12
    const-string v1, "FeedItem"

    const-string v2, "artist"

    const/4 v3, 0x5

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7791
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bu:Ljava/lang/String;

    goto/16 :goto_0

    .line 7802
    :sswitch_13
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_8

    .line 7884
    const-string v1, "FeedItem"

    const-string v2, "au"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7804
    :sswitch_14
    const-string v1, "FeedItem"

    const-string v2, "audio"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7805
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_2

    .line 7819
    const-string v1, "FeedItem"

    const-string v2, "audio"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7807
    :pswitch_5
    const-string v1, "FeedItem"

    const-string v2, "audioType"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7808
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bw:Ljava/lang/String;

    goto/16 :goto_0

    .line 7813
    :pswitch_6
    const-string v1, "FeedItem"

    const-string v2, "audioURL"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7814
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bv:Ljava/lang/String;

    goto/16 :goto_0

    .line 7825
    :sswitch_15
    const-string v1, "FeedItem"

    const-string v2, "author"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7826
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_9

    .line 7878
    const-string v1, "FeedItem"

    const-string v2, "author"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7828
    :sswitch_16
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_a

    .line 7842
    const-string v1, "FeedItem"

    const-string v2, "authorD"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7830
    :sswitch_17
    const-string v1, "FeedItem"

    const-string v2, "authorDescription"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7831
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->as:Ljava/lang/String;

    goto/16 :goto_0

    .line 7836
    :sswitch_18
    const-string v1, "FeedItem"

    const-string v2, "authorDisplayName"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7837
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    goto/16 :goto_0

    .line 7847
    :sswitch_19
    const-string v1, "FeedItem"

    const-string v2, "authorImage"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7848
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 7853
    :sswitch_1a
    const-string v1, "FeedItem"

    const-string v2, "authorLocation"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7854
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->at:Ljava/lang/String;

    goto/16 :goto_0

    .line 7859
    :sswitch_1b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_b

    .line 7873
    const-string v1, "FeedItem"

    const-string v2, "authorU"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7861
    :sswitch_1c
    const-string v1, "FeedItem"

    const-string v2, "authorURL"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7862
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->ar:Ljava/lang/String;

    goto/16 :goto_0

    .line 7867
    :sswitch_1d
    const-string v1, "FeedItem"

    const-string v2, "authorUsername"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7868
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->ap:Ljava/lang/String;

    goto/16 :goto_0

    .line 7894
    :pswitch_7
    const-string v1, "FeedItem"

    const-string v2, "baseURL"

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7895
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->R:Ljava/lang/String;

    goto/16 :goto_0

    .line 7900
    :pswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_c

    .line 8091
    const-string v1, "FeedItem"

    const-string v2, "c"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7902
    :sswitch_1e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_d

    .line 8019
    const-string v1, "FeedItem"

    const-string v2, "ca"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7904
    :sswitch_1f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_e

    .line 7993
    const-string v1, "FeedItem"

    const-string v2, "can"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7906
    :sswitch_20
    const-string v1, "FeedItem"

    const-string v2, "canAutoplay"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7907
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->bC:Z

    goto/16 :goto_0

    .line 7912
    :sswitch_21
    const-string v1, "FeedItem"

    const-string v2, "canCompose"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7913
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->ai:Z

    goto/16 :goto_0

    .line 7918
    :sswitch_22
    const-string v1, "FeedItem"

    const-string v2, "canFetchCommentary"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7919
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->bn:Z

    goto/16 :goto_0

    .line 7924
    :sswitch_23
    const-string v1, "FeedItem"

    const-string v2, "canLike"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7925
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->ad:Z

    goto/16 :goto_0

    .line 7930
    :sswitch_24
    const-string v1, "FeedItem"

    const-string v2, "canRe"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7931
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_f

    .line 7945
    const-string v1, "FeedItem"

    const-string v2, "canRe"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7933
    :sswitch_25
    const-string v1, "FeedItem"

    const-string v2, "canRead"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7934
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->aQ:Z

    goto/16 :goto_0

    .line 7939
    :sswitch_26
    const-string v1, "FeedItem"

    const-string v2, "canReply"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7940
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->ag:Z

    goto/16 :goto_0

    .line 7951
    :sswitch_27
    const-string v1, "FeedItem"

    const-string v2, "canShare"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7952
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_10

    .line 7966
    const-string v1, "FeedItem"

    const-string v2, "canShare"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7954
    :sswitch_28
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7955
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->ah:Z

    goto/16 :goto_0

    .line 7960
    :sswitch_29
    const-string v1, "FeedItem"

    const-string v2, "canShareLink"

    const/16 v3, 0x9

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7961
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->aj:Z

    goto/16 :goto_0

    .line 7972
    :sswitch_2a
    const-string v1, "FeedItem"

    const-string v2, "canUn"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7973
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_11

    .line 7987
    const-string v1, "FeedItem"

    const-string v2, "canUn"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7975
    :sswitch_2b
    const-string v1, "FeedItem"

    const-string v2, "canUnlike"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7976
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->ae:Z

    goto/16 :goto_0

    .line 7981
    :sswitch_2c
    const-string v1, "FeedItem"

    const-string v2, "canUnread"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7982
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->aM:Z

    goto/16 :goto_0

    .line 7998
    :sswitch_2d
    const-string v1, "FeedItem"

    const-string v2, "categor"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7999
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_12

    .line 8013
    const-string v1, "FeedItem"

    const-string v2, "categor"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8001
    :sswitch_2e
    const-string v1, "FeedItem"

    const-string v2, "categories"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8002
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aK:Ljava/util/List;

    goto/16 :goto_0

    .line 8007
    :sswitch_2f
    const-string v1, "FeedItem"

    const-string v2, "categoryid"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8008
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->cb:Ljava/lang/String;

    goto/16 :goto_0

    .line 8024
    :sswitch_30
    const-string v1, "FeedItem"

    const-string v2, "click"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8025
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_3

    .line 8039
    :pswitch_9
    const-string v1, "FeedItem"

    const-string v2, "click"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8027
    :pswitch_a
    const-string v1, "FeedItem"

    const-string v2, "clickTrackingUrls"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8028
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bT:Ljava/util/List;

    goto/16 :goto_0

    .line 8033
    :pswitch_b
    const-string v1, "FeedItem"

    const-string v2, "clickValue"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8034
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bO:Ljava/lang/String;

    goto/16 :goto_0

    .line 8045
    :sswitch_31
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_13

    .line 8080
    const-string v1, "FeedItem"

    const-string v2, "co"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8047
    :sswitch_32
    const-string v1, "FeedItem"

    const-string v2, "content"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8048
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_4

    .line 8062
    const-string v1, "FeedItem"

    const-string v2, "content"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8050
    :pswitch_c
    const-string v1, "FeedItem"

    const-string v2, "contentService"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8051
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    goto/16 :goto_0

    .line 8056
    :pswitch_d
    const-string v1, "FeedItem"

    const-string v2, "contentTitle"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8057
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->cf:Ljava/lang/String;

    goto/16 :goto_0

    .line 8068
    :sswitch_33
    const-string v1, "FeedItem"

    const-string v2, "count"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8069
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/FeedItem;->s:I

    goto/16 :goto_0

    .line 8074
    :sswitch_34
    const-string v1, "FeedItem"

    const-string v2, "coverImage"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8075
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->N:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 8085
    :sswitch_35
    const-string v1, "FeedItem"

    const-string v2, "crossPosts"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8086
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->l()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aP:Ljava/util/List;

    goto/16 :goto_0

    .line 8096
    :pswitch_e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_14

    .line 8152
    const-string v1, "FeedItem"

    const-string v2, "d"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8098
    :sswitch_36
    const-string v1, "FeedItem"

    const-string v2, "dateCreated"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8099
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/objs/FeedItem;->Z:J

    goto/16 :goto_0

    .line 8104
    :sswitch_37
    const-string v1, "FeedItem"

    const-string v2, "description"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8105
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->by:Ljava/lang/String;

    goto/16 :goto_0

    .line 8110
    :sswitch_38
    const-string v1, "FeedItem"

    const-string v2, "dis"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8111
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_15

    .line 8140
    const-string v1, "FeedItem"

    const-string v2, "dis"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8113
    :sswitch_39
    const-string v1, "FeedItem"

    const-string v2, "disable"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8114
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_16

    .line 8128
    const-string v1, "FeedItem"

    const-string v2, "disable"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8116
    :sswitch_3a
    const-string v1, "FeedItem"

    const-string v2, "disableFullBleed"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8117
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->bl:Z

    goto/16 :goto_0

    .line 8122
    :sswitch_3b
    const-string v1, "FeedItem"

    const-string v2, "disabled"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8123
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->bW:Z

    goto/16 :goto_0

    .line 8134
    :sswitch_3c
    const-string v1, "FeedItem"

    const-string v2, "displayNative"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8135
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->aL:Z

    goto/16 :goto_0

    .line 8146
    :sswitch_3d
    const-string v1, "FeedItem"

    const-string v2, "duration"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8147
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/FeedItem;->bz:F

    goto/16 :goto_0

    .line 8157
    :pswitch_f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_17

    .line 8183
    const-string v1, "FeedItem"

    const-string v2, "e"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8159
    :sswitch_3e
    const-string v1, "FeedItem"

    const-string v2, "ecommerceCheckoutURL"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8160
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bs:Ljava/lang/String;

    goto/16 :goto_0

    .line 8165
    :sswitch_3f
    const-string v1, "FeedItem"

    const-string v2, "eof"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8166
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->ay:Z

    goto/16 :goto_0

    .line 8171
    :sswitch_40
    const-string v1, "FeedItem"

    const-string v2, "errormessage"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8172
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bh:Ljava/lang/String;

    goto/16 :goto_0

    .line 8177
    :sswitch_41
    const-string v1, "FeedItem"

    const-string v2, "excerptText"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8178
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->F:Ljava/lang/String;

    goto/16 :goto_0

    .line 8188
    :pswitch_10
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_18

    .line 8242
    const-string v1, "FeedItem"

    const-string v2, "f"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8190
    :sswitch_42
    const-string v1, "FeedItem"

    const-string v2, "feed"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8191
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_5

    .line 8218
    const-string v1, "FeedItem"

    const-string v2, "feed"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8193
    :pswitch_11
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_19

    .line 8207
    const-string v1, "FeedItem"

    const-string v2, "feedT"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8195
    :sswitch_43
    const-string v1, "FeedItem"

    const-string v2, "feedTitle"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8196
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->C:Ljava/lang/String;

    goto/16 :goto_0

    .line 8201
    :sswitch_44
    const-string v1, "FeedItem"

    const-string v2, "feedType"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8202
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 8212
    :pswitch_12
    const-string v1, "FeedItem"

    const-string v2, "feedUrl"

    const/4 v3, 0x5

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8213
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->D:Ljava/lang/String;

    goto/16 :goto_0

    .line 8224
    :sswitch_45
    const-string v1, "FeedItem"

    const-string v2, "flipboardSocialId"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8225
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->ci:Ljava/lang/String;

    goto/16 :goto_0

    .line 8230
    :sswitch_46
    const-string v1, "FeedItem"

    const-string v2, "franchiseId"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8231
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 8236
    :sswitch_47
    const-string v1, "FeedItem"

    const-string v2, "fullText"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8237
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->G:Ljava/lang/String;

    goto/16 :goto_0

    .line 8247
    :pswitch_13
    const-string v1, "FeedItem"

    const-string v2, "group"

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8248
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1a

    .line 8268
    const-string v1, "FeedItem"

    const-string v2, "group"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8250
    :sswitch_48
    const-string v1, "FeedItem"

    const-string v2, "groupId"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8251
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 8256
    :sswitch_49
    const-string v1, "FeedItem"

    const-string v2, "groupRenderHints"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8257
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aq()Lflipboard/objs/FeedItemRenderHints;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->cp:Lflipboard/objs/FeedItemRenderHints;

    goto/16 :goto_0

    .line 8262
    :sswitch_4a
    const-string v1, "FeedItem"

    const-string v2, "groups"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8263
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aI()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    goto/16 :goto_0

    .line 8274
    :pswitch_14
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1b

    .line 8327
    const-string v1, "FeedItem"

    const-string v2, "h"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8276
    :sswitch_4b
    const-string v1, "FeedItem"

    const-string v2, "h264URL"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8277
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bj:Ljava/lang/String;

    goto/16 :goto_0

    .line 8282
    :sswitch_4c
    const-string v1, "FeedItem"

    const-string v2, "hashCode"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8283
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/FeedItem;->t:I

    goto/16 :goto_0

    .line 8288
    :sswitch_4d
    const-string v1, "FeedItem"

    const-string v2, "hide"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8289
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1c

    .line 8315
    const-string v1, "FeedItem"

    const-string v2, "hide"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8291
    :sswitch_4e
    const-string v1, "FeedItem"

    const-string v2, "hideContributors"

    const/4 v3, 0x5

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8292
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->ck:Z

    goto/16 :goto_0

    .line 8297
    :sswitch_4f
    const-string v1, "FeedItem"

    const-string v2, "hideImageUrl"

    const/4 v3, 0x5

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8298
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->cj:Z

    goto/16 :goto_0

    .line 8303
    :sswitch_50
    const-string v1, "FeedItem"

    const-string v2, "hideOnCover"

    const/4 v3, 0x5

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8304
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->an:Z

    goto/16 :goto_0

    .line 8309
    :sswitch_51
    const-string v1, "FeedItem"

    const-string v2, "hideTimelineDate"

    const/4 v3, 0x5

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8310
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->bk:Z

    goto/16 :goto_0

    .line 8321
    :sswitch_52
    const-string v1, "FeedItem"

    const-string v2, "hostDisplayName"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8322
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->av:Ljava/lang/String;

    goto/16 :goto_0

    .line 8332
    :pswitch_15
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1d

    .line 8521
    const-string v1, "FeedItem"

    const-string v2, "i"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8334
    :sswitch_53
    const-string v1, "FeedItem"

    const-string v2, "id"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8335
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 8340
    :sswitch_54
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1e

    .line 8405
    const-string v1, "FeedItem"

    const-string v2, "im"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8342
    :sswitch_55
    const-string v1, "FeedItem"

    const-string v2, "image"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8343
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1f

    .line 8378
    const-string v1, "FeedItem"

    const-string v2, "image"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8345
    :sswitch_56
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8346
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 8351
    :sswitch_57
    const-string v1, "FeedItem"

    const-string v2, "imageAttribution"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8352
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bq:Ljava/lang/String;

    goto/16 :goto_0

    .line 8357
    :sswitch_58
    const-string v1, "FeedItem"

    const-string v2, "imageURL"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8358
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_20

    .line 8372
    const-string v1, "FeedItem"

    const-string v2, "imageURL"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8360
    :sswitch_59
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8361
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 8366
    :sswitch_5a
    const-string v1, "FeedItem"

    const-string v2, "imageURLHiddenInList"

    const/16 v3, 0x9

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8367
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->cc:Z

    goto/16 :goto_0

    .line 8384
    :sswitch_5b
    const-string v1, "FeedItem"

    const-string v2, "impression"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8385
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_6

    .line 8399
    :pswitch_16
    const-string v1, "FeedItem"

    const-string v2, "impression"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8387
    :pswitch_17
    const-string v1, "FeedItem"

    const-string v2, "impressionTrackingUrls"

    const/16 v3, 0xb

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8388
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bS:Ljava/util/List;

    goto/16 :goto_0

    .line 8393
    :pswitch_18
    const-string v1, "FeedItem"

    const-string v2, "impressionValue"

    const/16 v3, 0xb

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8394
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bP:Ljava/lang/String;

    goto/16 :goto_0

    .line 8410
    :sswitch_5c
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_21

    .line 8464
    const-string v1, "FeedItem"

    const-string v2, "in"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8412
    :sswitch_5d
    const-string v1, "FeedItem"

    const-string v2, "inReplyToID"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8413
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aZ:Ljava/lang/String;

    goto/16 :goto_0

    .line 8418
    :sswitch_5e
    const-string v1, "FeedItem"

    const-string v2, "ingestionTime"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8419
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/objs/FeedItem;->aa:J

    goto/16 :goto_0

    .line 8424
    :sswitch_5f
    const-string v1, "FeedItem"

    const-string v2, "inline"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8425
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_7

    .line 8452
    const-string v1, "FeedItem"

    const-string v2, "inline"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8427
    :pswitch_19
    const-string v1, "FeedItem"

    const-string v2, "inlineHTML"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8428
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->Q:Ljava/lang/String;

    goto/16 :goto_0

    .line 8433
    :pswitch_1a
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_22

    .line 8447
    const-string v1, "FeedItem"

    const-string v2, "inlineI"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8435
    :sswitch_60
    const-string v1, "FeedItem"

    const-string v2, "inlineImage"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8436
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->I:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 8441
    :sswitch_61
    const-string v1, "FeedItem"

    const-string v2, "inlineItems"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8442
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->l()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    goto/16 :goto_0

    .line 8458
    :sswitch_62
    const-string v1, "FeedItem"

    const-string v2, "invite"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8459
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->n()Lflipboard/objs/Invite;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->k:Lflipboard/objs/Invite;

    goto/16 :goto_0

    .line 8469
    :sswitch_63
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_8

    .line 8495
    :pswitch_1b
    const-string v1, "FeedItem"

    const-string v2, "is"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8471
    :pswitch_1c
    const-string v1, "FeedItem"

    const-string v2, "isLiked"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8472
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->af:Z

    goto/16 :goto_0

    .line 8477
    :pswitch_1d
    const-string v1, "FeedItem"

    const-string v2, "isRead"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8478
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->be:Z

    goto/16 :goto_0

    .line 8483
    :pswitch_1e
    const-string v1, "FeedItem"

    const-string v2, "isStarred"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8484
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->bm:Z

    goto/16 :goto_0

    .line 8489
    :pswitch_1f
    const-string v1, "FeedItem"

    const-string v2, "isTextHTML"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8490
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->bb:Z

    goto/16 :goto_0

    .line 8500
    :sswitch_64
    const-string v1, "FeedItem"

    const-string v2, "item"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8501
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_23

    .line 8515
    const-string v1, "FeedItem"

    const-string v2, "item"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8503
    :sswitch_65
    const-string v1, "FeedItem"

    const-string v2, "itemPrice"

    const/4 v3, 0x5

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8504
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->br:Ljava/lang/String;

    goto/16 :goto_0

    .line 8509
    :sswitch_66
    const-string v1, "FeedItem"

    const-string v2, "items"

    const/4 v3, 0x5

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8510
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->l()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    goto/16 :goto_0

    .line 8526
    :pswitch_20
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_24

    .line 8552
    const-string v1, "FeedItem"

    const-string v2, "l"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8528
    :sswitch_67
    const-string v1, "FeedItem"

    const-string v2, "language"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8529
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->Y:Ljava/lang/String;

    goto/16 :goto_0

    .line 8534
    :sswitch_68
    const-string v1, "FeedItem"

    const-string v2, "leadWithFullPageItem"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8535
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->cl:Z

    goto/16 :goto_0

    .line 8540
    :sswitch_69
    const-string v1, "FeedItem"

    const-string v2, "license"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8541
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->ba:Ljava/lang/String;

    goto/16 :goto_0

    .line 8546
    :sswitch_6a
    const-string v1, "FeedItem"

    const-string v2, "location"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8547
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ao()Lflipboard/objs/FeedItem$Location;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aS:Lflipboard/objs/FeedItem$Location;

    goto/16 :goto_0

    .line 8557
    :pswitch_21
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_25

    .line 8597
    const-string v1, "FeedItem"

    const-string v2, "m"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8559
    :sswitch_6b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_9

    .line 8586
    :pswitch_22
    const-string v1, "FeedItem"

    const-string v2, "ma"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8561
    :pswitch_23
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_26

    .line 8575
    const-string v1, "FeedItem"

    const-string v2, "mag"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8563
    :sswitch_6c
    const-string v1, "FeedItem"

    const-string v2, "magazineTarget"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8564
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->cg:Ljava/lang/String;

    goto/16 :goto_0

    .line 8569
    :sswitch_6d
    const-string v1, "FeedItem"

    const-string v2, "magnetData"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8570
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->Q()Lflipboard/json/FLObject;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aU:Lflipboard/json/FLObject;

    goto/16 :goto_0

    .line 8580
    :pswitch_24
    const-string v1, "FeedItem"

    const-string v2, "mainItem"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8581
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->k()Lflipboard/objs/FeedItem;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bF:Lflipboard/objs/FeedItem;

    goto/16 :goto_0

    .line 8591
    :sswitch_6e
    const-string v1, "FeedItem"

    const-string v2, "meteringEnabled"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8592
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->cr:Z

    goto/16 :goto_0

    .line 8602
    :pswitch_25
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_27

    .line 8648
    const-string v1, "FeedItem"

    const-string v2, "n"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8604
    :sswitch_6f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_a

    .line 8618
    :pswitch_26
    const-string v1, "FeedItem"

    const-string v2, "ne"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8606
    :pswitch_27
    const-string v1, "FeedItem"

    const-string v2, "neverLoadMore"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8607
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->az:Z

    goto/16 :goto_0

    .line 8612
    :pswitch_28
    const-string v1, "FeedItem"

    const-string v2, "nextPageKey"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8613
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 8623
    :sswitch_70
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_28

    .line 8637
    const-string v1, "FeedItem"

    const-string v2, "no"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8625
    :sswitch_71
    const-string v1, "FeedItem"

    const-string v2, "noItemsText"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8626
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aT:Ljava/lang/String;

    goto/16 :goto_0

    .line 8631
    :sswitch_72
    const-string v1, "FeedItem"

    const-string v2, "notificationType"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8632
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->ce:Ljava/lang/String;

    goto/16 :goto_0

    .line 8642
    :sswitch_73
    const-string v1, "FeedItem"

    const-string v2, "nsfw"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8643
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/FeedItem;->ak:I

    goto/16 :goto_0

    .line 8653
    :pswitch_29
    const-string v1, "FeedItem"

    const-string v2, "original"

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8654
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_29

    .line 8700
    const-string v1, "FeedItem"

    const-string v2, "original"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8656
    :sswitch_74
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8657
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->k()Lflipboard/objs/FeedItem;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aJ:Lflipboard/objs/FeedItem;

    goto/16 :goto_0

    .line 8662
    :sswitch_75
    const-string v1, "FeedItem"

    const-string v2, "originalFlip"

    const/16 v3, 0x9

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8663
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->k()Lflipboard/objs/FeedItem;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    goto/16 :goto_0

    .line 8668
    :sswitch_76
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2a

    .line 8695
    const-string v1, "FeedItem"

    const-string v2, "original_"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8670
    :sswitch_77
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2b

    .line 8684
    const-string v1, "FeedItem"

    const-string v2, "original_h"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8672
    :sswitch_78
    const-string v1, "FeedItem"

    const-string v2, "original_height"

    const/16 v3, 0xb

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8673
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/FeedItem;->w:I

    goto/16 :goto_0

    .line 8678
    :sswitch_79
    const-string v1, "FeedItem"

    const-string v2, "original_hints"

    const/16 v3, 0xb

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8679
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->x:Ljava/lang/String;

    goto/16 :goto_0

    .line 8689
    :sswitch_7a
    const-string v1, "FeedItem"

    const-string v2, "original_width"

    const/16 v3, 0xa

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8690
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/FeedItem;->v:I

    goto/16 :goto_0

    .line 8706
    :pswitch_2a
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2c

    .line 8841
    const-string v1, "FeedItem"

    const-string v2, "p"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8708
    :sswitch_7b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2d

    .line 8722
    const-string v1, "FeedItem"

    const-string v2, "pa"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8710
    :sswitch_7c
    const-string v1, "FeedItem"

    const-string v2, "pageKey"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8711
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 8716
    :sswitch_7d
    const-string v1, "FeedItem"

    const-string v2, "partnerID"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8717
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bE:Ljava/lang/String;

    goto/16 :goto_0

    .line 8727
    :sswitch_7e
    const-string v1, "FeedItem"

    const-string v2, "plainText"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8728
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bc:Ljava/lang/String;

    goto/16 :goto_0

    .line 8733
    :sswitch_7f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_b

    .line 8777
    const-string v1, "FeedItem"

    const-string v2, "po"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8735
    :pswitch_2b
    const-string v1, "FeedItem"

    const-string v2, "portrait_"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8736
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2e

    .line 8750
    const-string v1, "FeedItem"

    const-string v2, "portrait_"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8738
    :sswitch_80
    const-string v1, "FeedItem"

    const-string v2, "portrait_height"

    const/16 v3, 0xa

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8739
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/FeedItem;->aW:I

    goto/16 :goto_0

    .line 8744
    :sswitch_81
    const-string v1, "FeedItem"

    const-string v2, "portrait_width"

    const/16 v3, 0xa

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8745
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/FeedItem;->aV:I

    goto/16 :goto_0

    .line 8756
    :pswitch_2c
    const-string v1, "FeedItem"

    const-string v2, "poste"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8757
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2f

    .line 8771
    const-string v1, "FeedItem"

    const-string v2, "poste"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8759
    :sswitch_82
    const-string v1, "FeedItem"

    const-string v2, "postedBy"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8760
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bx:Ljava/lang/String;

    goto/16 :goto_0

    .line 8765
    :sswitch_83
    const-string v1, "FeedItem"

    const-string v2, "posterImage"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8766
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->X:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 8782
    :sswitch_84
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_30

    .line 8830
    const-string v1, "FeedItem"

    const-string v2, "pr"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8784
    :sswitch_85
    const-string v1, "FeedItem"

    const-string v2, "preselected"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8785
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->cd:Z

    goto/16 :goto_0

    .line 8790
    :sswitch_86
    const-string v1, "FeedItem"

    const-string v2, "private"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8791
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->m:Z

    goto/16 :goto_0

    .line 8796
    :sswitch_87
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_31

    .line 8825
    const-string v1, "FeedItem"

    const-string v2, "pro"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8798
    :sswitch_88
    const-string v1, "FeedItem"

    const-string v2, "profile"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8799
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_32

    .line 8813
    const-string v1, "FeedItem"

    const-string v2, "profile"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8801
    :sswitch_89
    const-string v1, "FeedItem"

    const-string v2, "profileBackgroundImage"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8802
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->O:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 8807
    :sswitch_8a
    const-string v1, "FeedItem"

    const-string v2, "profileSectionLink"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8808
    invoke-direct {p0}, Lflipboard/json/JSONParser;->as()Lflipboard/objs/FeedSectionLink;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bM:Lflipboard/objs/FeedSectionLink;

    goto/16 :goto_0

    .line 8819
    :sswitch_8b
    const-string v1, "FeedItem"

    const-string v2, "prominence"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8820
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/FeedItem;->E:F

    goto/16 :goto_0

    .line 8835
    :sswitch_8c
    const-string v1, "FeedItem"

    const-string v2, "publisher"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8836
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->au:Ljava/lang/String;

    goto/16 :goto_0

    .line 8846
    :pswitch_2d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_33

    .line 8949
    const-string v1, "FeedItem"

    const-string v2, "r"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8848
    :sswitch_8d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_34

    .line 8917
    const-string v1, "FeedItem"

    const-string v2, "re"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8850
    :sswitch_8e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_35

    .line 8864
    const-string v1, "FeedItem"

    const-string v2, "rea"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8852
    :sswitch_8f
    const-string v1, "FeedItem"

    const-string v2, "readButtonCaption"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8853
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->V:Ljava/lang/String;

    goto/16 :goto_0

    .line 8858
    :sswitch_90
    const-string v1, "FeedItem"

    const-string v2, "reason"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8859
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ap()Lflipboard/objs/FeedItem$Note;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bf:Lflipboard/objs/FeedItem$Note;

    goto/16 :goto_0

    .line 8869
    :sswitch_91
    const-string v1, "FeedItem"

    const-string v2, "referredByItems"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8870
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->l()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    goto/16 :goto_0

    .line 8875
    :sswitch_92
    const-string v1, "FeedItem"

    const-string v2, "releaseDate"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8876
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_36

    .line 8890
    const-string v1, "FeedItem"

    const-string v2, "releaseDate"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8878
    :sswitch_93
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8879
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/objs/FeedItem;->bA:J

    goto/16 :goto_0

    .line 8884
    :sswitch_94
    const-string v1, "FeedItem"

    const-string v2, "releaseDateString"

    const/16 v3, 0xc

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8885
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bB:Ljava/lang/String;

    goto/16 :goto_0

    .line 8896
    :sswitch_95
    const-string v1, "FeedItem"

    const-string v2, "remote"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8897
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_37

    .line 8911
    const-string v1, "FeedItem"

    const-string v2, "remote"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8899
    :sswitch_96
    const-string v1, "FeedItem"

    const-string v2, "remoteServiceItemID"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8900
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->U:Ljava/lang/String;

    goto/16 :goto_0

    .line 8905
    :sswitch_97
    const-string v1, "FeedItem"

    const-string v2, "remoteid"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8906
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 8922
    :sswitch_98
    const-string v1, "FeedItem"

    const-string v2, "rss"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8923
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_38

    .line 8943
    const-string v1, "FeedItem"

    const-string v2, "rss"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8925
    :sswitch_99
    const-string v1, "FeedItem"

    const-string v2, "rssBaseURL"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8926
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->B:Ljava/lang/String;

    goto/16 :goto_0

    .line 8931
    :sswitch_9a
    const-string v1, "FeedItem"

    const-string v2, "rssCustomClasses"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8932
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aR:Ljava/lang/String;

    goto/16 :goto_0

    .line 8937
    :sswitch_9b
    const-string v1, "FeedItem"

    const-string v2, "rssText"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8938
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->A:Ljava/lang/String;

    goto/16 :goto_0

    .line 8954
    :pswitch_2e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_39

    .line 9143
    const-string v1, "FeedItem"

    const-string v2, "s"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8956
    :sswitch_9c
    const-string v1, "FeedItem"

    const-string v2, "score"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8957
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/FeedItem;->aO:I

    goto/16 :goto_0

    .line 8962
    :sswitch_9d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3a

    .line 9018
    const-string v1, "FeedItem"

    const-string v2, "se"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8964
    :sswitch_9e
    const-string v1, "FeedItem"

    const-string v2, "section"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8965
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3b

    .line 9006
    const-string v1, "FeedItem"

    const-string v2, "section"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8967
    :sswitch_9f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8968
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ar()Lflipboard/objs/FeedSection;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    goto/16 :goto_0

    .line 8973
    :sswitch_a0
    const-string v1, "FeedItem"

    const-string v2, "sectionID"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8974
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3c

    .line 8988
    const-string v1, "FeedItem"

    const-string v2, "sectionID"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8976
    :sswitch_a1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8977
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 8982
    :sswitch_a2
    const-string v1, "FeedItem"

    const-string v2, "sectionIDType"

    const/16 v3, 0xa

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8983
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 8994
    :sswitch_a3
    const-string v1, "FeedItem"

    const-string v2, "sectionLinks"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8995
    invoke-direct {p0}, Lflipboard/json/JSONParser;->at()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    goto/16 :goto_0

    .line 9000
    :sswitch_a4
    const-string v1, "FeedItem"

    const-string v2, "sectionTitle"

    invoke-virtual {p0, v1, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9001
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bI:Ljava/lang/String;

    goto/16 :goto_0

    .line 9012
    :sswitch_a5
    const-string v1, "FeedItem"

    const-string v2, "service"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9013
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    goto/16 :goto_0

    .line 9023
    :sswitch_a6
    const-string v1, "FeedItem"

    const-string v2, "sidebarType"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9024
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    goto/16 :goto_0

    .line 9029
    :sswitch_a7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3d

    .line 9064
    const-string v1, "FeedItem"

    const-string v2, "so"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9031
    :sswitch_a8
    const-string v1, "FeedItem"

    const-string v2, "socialId"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9032
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->cm:Ljava/lang/String;

    goto/16 :goto_0

    .line 9037
    :sswitch_a9
    const-string v1, "FeedItem"

    const-string v2, "sortValue"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9038
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/objs/FeedItem;->u:J

    goto/16 :goto_0

    .line 9043
    :sswitch_aa
    const-string v1, "FeedItem"

    const-string v2, "source"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9044
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3e

    .line 9058
    const-string v1, "FeedItem"

    const-string v2, "source"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9046
    :sswitch_ab
    const-string v1, "FeedItem"

    const-string v2, "sourceMagazineURL"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9047
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->W:Ljava/lang/String;

    goto/16 :goto_0

    .line 9052
    :sswitch_ac
    const-string v1, "FeedItem"

    const-string v2, "sourceURL"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9053
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    goto/16 :goto_0

    .line 9069
    :sswitch_ad
    const-string v1, "FeedItem"

    const-string v2, "sponsored"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9070
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3f

    .line 9084
    const-string v1, "FeedItem"

    const-string v2, "sponsored"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9072
    :sswitch_ae
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9073
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->bN:Z

    goto/16 :goto_0

    .line 9078
    :sswitch_af
    const-string v1, "FeedItem"

    const-string v2, "sponsoredCampaign"

    const/16 v3, 0xa

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9079
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bU:Ljava/lang/String;

    goto/16 :goto_0

    .line 9090
    :sswitch_b0
    const-string v1, "FeedItem"

    const-string v2, "str"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9091
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_40

    .line 9105
    const-string v1, "FeedItem"

    const-string v2, "str"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9093
    :sswitch_b1
    const-string v1, "FeedItem"

    const-string v2, "strategy"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9094
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bD:Ljava/lang/String;

    goto/16 :goto_0

    .line 9099
    :sswitch_b2
    const-string v1, "FeedItem"

    const-string v2, "strippedExcerptText"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9100
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->H:Ljava/lang/String;

    goto/16 :goto_0

    .line 9111
    :sswitch_b3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_c

    .line 9138
    const-string v1, "FeedItem"

    const-string v2, "su"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9113
    :pswitch_2f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_41

    .line 9127
    const-string v1, "FeedItem"

    const-string v2, "sub"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9115
    :sswitch_b4
    const-string v1, "FeedItem"

    const-string v2, "subhead"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9116
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->cq:Ljava/lang/String;

    goto/16 :goto_0

    .line 9121
    :sswitch_b5
    const-string v1, "FeedItem"

    const-string v2, "subtitle"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9122
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bR:Ljava/lang/String;

    goto/16 :goto_0

    .line 9132
    :pswitch_30
    const-string v1, "FeedItem"

    const-string v2, "success"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9133
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->aA:Z

    goto/16 :goto_0

    .line 9148
    :pswitch_31
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_42

    .line 9236
    const-string v1, "FeedItem"

    const-string v2, "t"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9150
    :sswitch_b6
    const-string v1, "FeedItem"

    const-string v2, "text"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9151
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_43

    .line 9165
    const-string v1, "FeedItem"

    const-string v2, "text"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9153
    :sswitch_b7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9154
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->z:Ljava/lang/String;

    goto/16 :goto_0

    .line 9159
    :sswitch_b8
    const-string v1, "FeedItem"

    const-string v2, "textColor"

    const/4 v3, 0x5

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9160
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bJ:Ljava/lang/String;

    goto/16 :goto_0

    .line 9171
    :sswitch_b9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_44

    .line 9191
    const-string v1, "FeedItem"

    const-string v2, "ti"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9173
    :sswitch_ba
    const-string v1, "FeedItem"

    const-string v2, "tileImage"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9174
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->L:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 9179
    :sswitch_bb
    const-string v1, "FeedItem"

    const-string v2, "time"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9180
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/FeedItem;->r:I

    goto/16 :goto_0

    .line 9185
    :sswitch_bc
    const-string v1, "FeedItem"

    const-string v2, "title"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9186
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    goto/16 :goto_0

    .line 9196
    :sswitch_bd
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_45

    .line 9225
    const-string v1, "FeedItem"

    const-string v2, "to"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9198
    :sswitch_be
    const-string v1, "FeedItem"

    const-string v2, "topStory"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9199
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_46

    .line 9213
    const-string v1, "FeedItem"

    const-string v2, "topStory"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9201
    :sswitch_bf
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9202
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->ab:Z

    goto/16 :goto_0

    .line 9207
    :sswitch_c0
    const-string v1, "FeedItem"

    const-string v2, "topStoryText"

    const/16 v3, 0x9

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9208
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->ac:Ljava/lang/String;

    goto/16 :goto_0

    .line 9219
    :sswitch_c1
    const-string v1, "FeedItem"

    const-string v2, "totalCount"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9220
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/FeedItem;->aw:I

    goto/16 :goto_0

    .line 9230
    :sswitch_c2
    const-string v1, "FeedItem"

    const-string v2, "type"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9231
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 9241
    :pswitch_32
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_d

    .line 9306
    :pswitch_33
    const-string v1, "FeedItem"

    const-string v2, "u"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9243
    :pswitch_34
    const-string v1, "FeedItem"

    const-string v2, "unread"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9244
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_47

    .line 9258
    const-string v1, "FeedItem"

    const-string v2, "unread"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9246
    :sswitch_c3
    const-string v1, "FeedItem"

    const-string v2, "unreadCount"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9247
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/FeedItem;->ca:I

    goto/16 :goto_0

    .line 9252
    :sswitch_c4
    const-string v1, "FeedItem"

    const-string v2, "unreadRemoteID"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9253
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bZ:Ljava/lang/String;

    goto/16 :goto_0

    .line 9264
    :pswitch_35
    const-string v1, "FeedItem"

    const-string v2, "url"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9265
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_48

    .line 9279
    const-string v1, "FeedItem"

    const-string v2, "url"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9267
    :sswitch_c5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9268
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->P:Ljava/lang/String;

    goto/16 :goto_0

    .line 9273
    :sswitch_c6
    const-string v1, "FeedItem"

    const-string v2, "urls"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9274
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->S()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aB:Ljava/util/List;

    goto/16 :goto_0

    .line 9285
    :pswitch_36
    const-string v1, "FeedItem"

    const-string v2, "user"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9286
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_49

    .line 9300
    const-string v1, "FeedItem"

    const-string v2, "user"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9288
    :sswitch_c7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9289
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aO()Lflipboard/objs/UserServices;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bi:Lflipboard/objs/UserServices;

    goto/16 :goto_0

    .line 9294
    :sswitch_c8
    const-string v1, "FeedItem"

    const-string v2, "userid"

    const/4 v3, 0x5

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9295
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->ao:Ljava/lang/String;

    goto/16 :goto_0

    .line 9311
    :pswitch_37
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4a

    .line 9391
    const-string v1, "FeedItem"

    const-string v2, "v"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9313
    :sswitch_c9
    const-string v1, "FeedItem"

    const-string v2, "version"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9314
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 9319
    :sswitch_ca
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4b

    .line 9386
    const-string v1, "FeedItem"

    const-string v2, "vi"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9321
    :sswitch_cb
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4c

    .line 9335
    const-string v1, "FeedItem"

    const-string v2, "via"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9323
    :sswitch_cc
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9324
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->k()Lflipboard/objs/FeedItem;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bH:Lflipboard/objs/FeedItem;

    goto/16 :goto_0

    .line 9329
    :sswitch_cd
    const-string v1, "FeedItem"

    const-string v2, "viaService"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9330
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bd:Ljava/lang/String;

    goto/16 :goto_0

    .line 9340
    :sswitch_ce
    const-string v1, "FeedItem"

    const-string v2, "video"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9341
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4d

    .line 9374
    const-string v1, "FeedItem"

    const-string v2, "video"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9343
    :sswitch_cf
    const-string v1, "FeedItem"

    const-string v2, "videoEmbedHTML"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9344
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aY:Ljava/lang/String;

    goto/16 :goto_0

    .line 9349
    :sswitch_d0
    const-string v1, "FeedItem"

    const-string v2, "videoID"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9350
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bX:Ljava/lang/String;

    goto/16 :goto_0

    .line 9355
    :sswitch_d1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4e

    .line 9369
    const-string v1, "FeedItem"

    const-string v2, "videoS"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9357
    :sswitch_d2
    const-string v1, "FeedItem"

    const-string v2, "videoService"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9358
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bY:Ljava/lang/String;

    goto/16 :goto_0

    .line 9363
    :sswitch_d3
    const-string v1, "FeedItem"

    const-string v2, "videoSiteURL"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9364
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->aX:Ljava/lang/String;

    goto/16 :goto_0

    .line 9380
    :sswitch_d4
    const-string v1, "FeedItem"

    const-string v2, "visibilityDisplayName"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9381
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bo:Ljava/lang/String;

    goto/16 :goto_0

    .line 9396
    :pswitch_38
    const-string v1, "FeedItem"

    const-string v2, "wants"

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9397
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4f

    .line 9411
    const-string v1, "FeedItem"

    const-string v2, "wants"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 9399
    :sswitch_d5
    const-string v1, "FeedItem"

    const-string v2, "wantsFullPage"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9400
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->bG:Z

    goto/16 :goto_0

    .line 9405
    :sswitch_d6
    const-string v1, "FeedItem"

    const-string v2, "wantsTranslucentBackground"

    invoke-virtual {p0, v1, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9406
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/FeedItem;->S:Z

    goto/16 :goto_0

    .line 9422
    :sswitch_d7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 9436
    :goto_1
    return-object v0

    .line 9429
    :sswitch_d8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 9436
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 7651
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_d8
        0xa -> :sswitch_d8
        0xd -> :sswitch_d8
        0x20 -> :sswitch_d8
        0x22 -> :sswitch_0
        0x2c -> :sswitch_d8
        0x7d -> :sswitch_d7
    .end sparse-switch

    .line 7653
    :pswitch_data_0
    .packed-switch 0x45
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_7
        :pswitch_8
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_20
        :pswitch_21
        :pswitch_25
        :pswitch_29
        :pswitch_2a
        :pswitch_0
        :pswitch_2d
        :pswitch_2e
        :pswitch_31
        :pswitch_32
        :pswitch_37
        :pswitch_38
    .end packed-switch

    .line 7661
    :sswitch_data_1
    .sparse-switch
        0x63 -> :sswitch_1
        0x64 -> :sswitch_4
        0x6c -> :sswitch_5
        0x72 -> :sswitch_c
        0x75 -> :sswitch_13
    .end sparse-switch

    .line 7664
    :sswitch_data_2
    .sparse-switch
        0x22 -> :sswitch_2
        0x54 -> :sswitch_3
    .end sparse-switch

    .line 7690
    :sswitch_data_3
    .sparse-switch
        0x62 -> :sswitch_6
        0x65 -> :sswitch_9
        0x67 -> :sswitch_a
        0x6c -> :sswitch_b
    .end sparse-switch

    .line 7693
    :sswitch_data_4
    .sparse-switch
        0x22 -> :sswitch_7
        0x41 -> :sswitch_8
    .end sparse-switch

    .line 7714
    :pswitch_data_1
    .packed-switch 0x4c
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 7752
    :sswitch_data_5
    .sparse-switch
        0x63 -> :sswitch_d
        0x73 -> :sswitch_12
    .end sparse-switch

    .line 7755
    :sswitch_data_6
    .sparse-switch
        0x22 -> :sswitch_e
        0x4d -> :sswitch_f
    .end sparse-switch

    .line 7764
    :sswitch_data_7
    .sparse-switch
        0x22 -> :sswitch_10
        0x45 -> :sswitch_11
    .end sparse-switch

    .line 7802
    :sswitch_data_8
    .sparse-switch
        0x64 -> :sswitch_14
        0x74 -> :sswitch_15
    .end sparse-switch

    .line 7805
    :pswitch_data_2
    .packed-switch 0x54
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 7826
    :sswitch_data_9
    .sparse-switch
        0x44 -> :sswitch_16
        0x49 -> :sswitch_19
        0x4c -> :sswitch_1a
        0x55 -> :sswitch_1b
    .end sparse-switch

    .line 7828
    :sswitch_data_a
    .sparse-switch
        0x65 -> :sswitch_17
        0x69 -> :sswitch_18
    .end sparse-switch

    .line 7859
    :sswitch_data_b
    .sparse-switch
        0x52 -> :sswitch_1c
        0x73 -> :sswitch_1d
    .end sparse-switch

    .line 7900
    :sswitch_data_c
    .sparse-switch
        0x61 -> :sswitch_1e
        0x6c -> :sswitch_30
        0x6f -> :sswitch_31
        0x72 -> :sswitch_35
    .end sparse-switch

    .line 7902
    :sswitch_data_d
    .sparse-switch
        0x6e -> :sswitch_1f
        0x74 -> :sswitch_2d
    .end sparse-switch

    .line 7904
    :sswitch_data_e
    .sparse-switch
        0x41 -> :sswitch_20
        0x43 -> :sswitch_21
        0x46 -> :sswitch_22
        0x4c -> :sswitch_23
        0x52 -> :sswitch_24
        0x53 -> :sswitch_27
        0x55 -> :sswitch_2a
    .end sparse-switch

    .line 7931
    :sswitch_data_f
    .sparse-switch
        0x61 -> :sswitch_25
        0x70 -> :sswitch_26
    .end sparse-switch

    .line 7952
    :sswitch_data_10
    .sparse-switch
        0x22 -> :sswitch_28
        0x4c -> :sswitch_29
    .end sparse-switch

    .line 7973
    :sswitch_data_11
    .sparse-switch
        0x6c -> :sswitch_2b
        0x72 -> :sswitch_2c
    .end sparse-switch

    .line 7999
    :sswitch_data_12
    .sparse-switch
        0x69 -> :sswitch_2e
        0x79 -> :sswitch_2f
    .end sparse-switch

    .line 8025
    :pswitch_data_3
    .packed-switch 0x54
        :pswitch_a
        :pswitch_9
        :pswitch_b
    .end packed-switch

    .line 8045
    :sswitch_data_13
    .sparse-switch
        0x6e -> :sswitch_32
        0x75 -> :sswitch_33
        0x76 -> :sswitch_34
    .end sparse-switch

    .line 8048
    :pswitch_data_4
    .packed-switch 0x53
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 8096
    :sswitch_data_14
    .sparse-switch
        0x61 -> :sswitch_36
        0x65 -> :sswitch_37
        0x69 -> :sswitch_38
        0x75 -> :sswitch_3d
    .end sparse-switch

    .line 8111
    :sswitch_data_15
    .sparse-switch
        0x61 -> :sswitch_39
        0x70 -> :sswitch_3c
    .end sparse-switch

    .line 8114
    :sswitch_data_16
    .sparse-switch
        0x46 -> :sswitch_3a
        0x64 -> :sswitch_3b
    .end sparse-switch

    .line 8157
    :sswitch_data_17
    .sparse-switch
        0x63 -> :sswitch_3e
        0x6f -> :sswitch_3f
        0x72 -> :sswitch_40
        0x78 -> :sswitch_41
    .end sparse-switch

    .line 8188
    :sswitch_data_18
    .sparse-switch
        0x65 -> :sswitch_42
        0x6c -> :sswitch_45
        0x72 -> :sswitch_46
        0x75 -> :sswitch_47
    .end sparse-switch

    .line 8191
    :pswitch_data_5
    .packed-switch 0x54
        :pswitch_11
        :pswitch_12
    .end packed-switch

    .line 8193
    :sswitch_data_19
    .sparse-switch
        0x69 -> :sswitch_43
        0x79 -> :sswitch_44
    .end sparse-switch

    .line 8248
    :sswitch_data_1a
    .sparse-switch
        0x49 -> :sswitch_48
        0x52 -> :sswitch_49
        0x73 -> :sswitch_4a
    .end sparse-switch

    .line 8274
    :sswitch_data_1b
    .sparse-switch
        0x32 -> :sswitch_4b
        0x61 -> :sswitch_4c
        0x69 -> :sswitch_4d
        0x6f -> :sswitch_52
    .end sparse-switch

    .line 8289
    :sswitch_data_1c
    .sparse-switch
        0x43 -> :sswitch_4e
        0x49 -> :sswitch_4f
        0x4f -> :sswitch_50
        0x54 -> :sswitch_51
    .end sparse-switch

    .line 8332
    :sswitch_data_1d
    .sparse-switch
        0x64 -> :sswitch_53
        0x6d -> :sswitch_54
        0x6e -> :sswitch_5c
        0x73 -> :sswitch_63
        0x74 -> :sswitch_64
    .end sparse-switch

    .line 8340
    :sswitch_data_1e
    .sparse-switch
        0x61 -> :sswitch_55
        0x70 -> :sswitch_5b
    .end sparse-switch

    .line 8343
    :sswitch_data_1f
    .sparse-switch
        0x22 -> :sswitch_56
        0x41 -> :sswitch_57
        0x55 -> :sswitch_58
    .end sparse-switch

    .line 8358
    :sswitch_data_20
    .sparse-switch
        0x22 -> :sswitch_59
        0x48 -> :sswitch_5a
    .end sparse-switch

    .line 8385
    :pswitch_data_6
    .packed-switch 0x54
        :pswitch_17
        :pswitch_16
        :pswitch_18
    .end packed-switch

    .line 8410
    :sswitch_data_21
    .sparse-switch
        0x52 -> :sswitch_5d
        0x67 -> :sswitch_5e
        0x6c -> :sswitch_5f
        0x76 -> :sswitch_62
    .end sparse-switch

    .line 8425
    :pswitch_data_7
    .packed-switch 0x48
        :pswitch_19
        :pswitch_1a
    .end packed-switch

    .line 8433
    :sswitch_data_22
    .sparse-switch
        0x6d -> :sswitch_60
        0x74 -> :sswitch_61
    .end sparse-switch

    .line 8469
    :pswitch_data_8
    .packed-switch 0x4c
        :pswitch_1c
        :pswitch_1b
        :pswitch_1b
        :pswitch_1b
        :pswitch_1b
        :pswitch_1b
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
    .end packed-switch

    .line 8501
    :sswitch_data_23
    .sparse-switch
        0x50 -> :sswitch_65
        0x73 -> :sswitch_66
    .end sparse-switch

    .line 8526
    :sswitch_data_24
    .sparse-switch
        0x61 -> :sswitch_67
        0x65 -> :sswitch_68
        0x69 -> :sswitch_69
        0x6f -> :sswitch_6a
    .end sparse-switch

    .line 8557
    :sswitch_data_25
    .sparse-switch
        0x61 -> :sswitch_6b
        0x65 -> :sswitch_6e
    .end sparse-switch

    .line 8559
    :pswitch_data_9
    .packed-switch 0x67
        :pswitch_23
        :pswitch_22
        :pswitch_24
    .end packed-switch

    .line 8561
    :sswitch_data_26
    .sparse-switch
        0x61 -> :sswitch_6c
        0x6e -> :sswitch_6d
    .end sparse-switch

    .line 8602
    :sswitch_data_27
    .sparse-switch
        0x65 -> :sswitch_6f
        0x6f -> :sswitch_70
        0x73 -> :sswitch_73
    .end sparse-switch

    .line 8604
    :pswitch_data_a
    .packed-switch 0x76
        :pswitch_27
        :pswitch_26
        :pswitch_28
    .end packed-switch

    .line 8623
    :sswitch_data_28
    .sparse-switch
        0x49 -> :sswitch_71
        0x74 -> :sswitch_72
    .end sparse-switch

    .line 8654
    :sswitch_data_29
    .sparse-switch
        0x22 -> :sswitch_74
        0x46 -> :sswitch_75
        0x5f -> :sswitch_76
    .end sparse-switch

    .line 8668
    :sswitch_data_2a
    .sparse-switch
        0x68 -> :sswitch_77
        0x77 -> :sswitch_7a
    .end sparse-switch

    .line 8670
    :sswitch_data_2b
    .sparse-switch
        0x65 -> :sswitch_78
        0x69 -> :sswitch_79
    .end sparse-switch

    .line 8706
    :sswitch_data_2c
    .sparse-switch
        0x61 -> :sswitch_7b
        0x6c -> :sswitch_7e
        0x6f -> :sswitch_7f
        0x72 -> :sswitch_84
        0x75 -> :sswitch_8c
    .end sparse-switch

    .line 8708
    :sswitch_data_2d
    .sparse-switch
        0x67 -> :sswitch_7c
        0x72 -> :sswitch_7d
    .end sparse-switch

    .line 8733
    :pswitch_data_b
    .packed-switch 0x72
        :pswitch_2b
        :pswitch_2c
    .end packed-switch

    .line 8736
    :sswitch_data_2e
    .sparse-switch
        0x68 -> :sswitch_80
        0x77 -> :sswitch_81
    .end sparse-switch

    .line 8757
    :sswitch_data_2f
    .sparse-switch
        0x64 -> :sswitch_82
        0x72 -> :sswitch_83
    .end sparse-switch

    .line 8782
    :sswitch_data_30
    .sparse-switch
        0x65 -> :sswitch_85
        0x69 -> :sswitch_86
        0x6f -> :sswitch_87
    .end sparse-switch

    .line 8796
    :sswitch_data_31
    .sparse-switch
        0x66 -> :sswitch_88
        0x6d -> :sswitch_8b
    .end sparse-switch

    .line 8799
    :sswitch_data_32
    .sparse-switch
        0x42 -> :sswitch_89
        0x53 -> :sswitch_8a
    .end sparse-switch

    .line 8846
    :sswitch_data_33
    .sparse-switch
        0x65 -> :sswitch_8d
        0x73 -> :sswitch_98
    .end sparse-switch

    .line 8848
    :sswitch_data_34
    .sparse-switch
        0x61 -> :sswitch_8e
        0x66 -> :sswitch_91
        0x6c -> :sswitch_92
        0x6d -> :sswitch_95
    .end sparse-switch

    .line 8850
    :sswitch_data_35
    .sparse-switch
        0x64 -> :sswitch_8f
        0x73 -> :sswitch_90
    .end sparse-switch

    .line 8876
    :sswitch_data_36
    .sparse-switch
        0x22 -> :sswitch_93
        0x53 -> :sswitch_94
    .end sparse-switch

    .line 8897
    :sswitch_data_37
    .sparse-switch
        0x53 -> :sswitch_96
        0x69 -> :sswitch_97
    .end sparse-switch

    .line 8923
    :sswitch_data_38
    .sparse-switch
        0x42 -> :sswitch_99
        0x43 -> :sswitch_9a
        0x54 -> :sswitch_9b
    .end sparse-switch

    .line 8954
    :sswitch_data_39
    .sparse-switch
        0x63 -> :sswitch_9c
        0x65 -> :sswitch_9d
        0x69 -> :sswitch_a6
        0x6f -> :sswitch_a7
        0x70 -> :sswitch_ad
        0x74 -> :sswitch_b0
        0x75 -> :sswitch_b3
    .end sparse-switch

    .line 8962
    :sswitch_data_3a
    .sparse-switch
        0x63 -> :sswitch_9e
        0x72 -> :sswitch_a5
    .end sparse-switch

    .line 8965
    :sswitch_data_3b
    .sparse-switch
        0x22 -> :sswitch_9f
        0x49 -> :sswitch_a0
        0x4c -> :sswitch_a3
        0x54 -> :sswitch_a4
    .end sparse-switch

    .line 8974
    :sswitch_data_3c
    .sparse-switch
        0x22 -> :sswitch_a1
        0x54 -> :sswitch_a2
    .end sparse-switch

    .line 9029
    :sswitch_data_3d
    .sparse-switch
        0x63 -> :sswitch_a8
        0x72 -> :sswitch_a9
        0x75 -> :sswitch_aa
    .end sparse-switch

    .line 9044
    :sswitch_data_3e
    .sparse-switch
        0x4d -> :sswitch_ab
        0x55 -> :sswitch_ac
    .end sparse-switch

    .line 9070
    :sswitch_data_3f
    .sparse-switch
        0x22 -> :sswitch_ae
        0x43 -> :sswitch_af
    .end sparse-switch

    .line 9091
    :sswitch_data_40
    .sparse-switch
        0x61 -> :sswitch_b1
        0x69 -> :sswitch_b2
    .end sparse-switch

    .line 9111
    :pswitch_data_c
    .packed-switch 0x62
        :pswitch_2f
        :pswitch_30
    .end packed-switch

    .line 9113
    :sswitch_data_41
    .sparse-switch
        0x68 -> :sswitch_b4
        0x74 -> :sswitch_b5
    .end sparse-switch

    .line 9148
    :sswitch_data_42
    .sparse-switch
        0x65 -> :sswitch_b6
        0x69 -> :sswitch_b9
        0x6f -> :sswitch_bd
        0x79 -> :sswitch_c2
    .end sparse-switch

    .line 9151
    :sswitch_data_43
    .sparse-switch
        0x22 -> :sswitch_b7
        0x43 -> :sswitch_b8
    .end sparse-switch

    .line 9171
    :sswitch_data_44
    .sparse-switch
        0x6c -> :sswitch_ba
        0x6d -> :sswitch_bb
        0x74 -> :sswitch_bc
    .end sparse-switch

    .line 9196
    :sswitch_data_45
    .sparse-switch
        0x70 -> :sswitch_be
        0x74 -> :sswitch_c1
    .end sparse-switch

    .line 9199
    :sswitch_data_46
    .sparse-switch
        0x22 -> :sswitch_bf
        0x54 -> :sswitch_c0
    .end sparse-switch

    .line 9241
    :pswitch_data_d
    .packed-switch 0x6e
        :pswitch_34
        :pswitch_33
        :pswitch_33
        :pswitch_33
        :pswitch_35
        :pswitch_36
    .end packed-switch

    .line 9244
    :sswitch_data_47
    .sparse-switch
        0x43 -> :sswitch_c3
        0x52 -> :sswitch_c4
    .end sparse-switch

    .line 9265
    :sswitch_data_48
    .sparse-switch
        0x22 -> :sswitch_c5
        0x73 -> :sswitch_c6
    .end sparse-switch

    .line 9286
    :sswitch_data_49
    .sparse-switch
        0x22 -> :sswitch_c7
        0x69 -> :sswitch_c8
    .end sparse-switch

    .line 9311
    :sswitch_data_4a
    .sparse-switch
        0x65 -> :sswitch_c9
        0x69 -> :sswitch_ca
    .end sparse-switch

    .line 9319
    :sswitch_data_4b
    .sparse-switch
        0x61 -> :sswitch_cb
        0x64 -> :sswitch_ce
        0x73 -> :sswitch_d4
    .end sparse-switch

    .line 9321
    :sswitch_data_4c
    .sparse-switch
        0x22 -> :sswitch_cc
        0x53 -> :sswitch_cd
    .end sparse-switch

    .line 9341
    :sswitch_data_4d
    .sparse-switch
        0x45 -> :sswitch_cf
        0x49 -> :sswitch_d0
        0x53 -> :sswitch_d1
    .end sparse-switch

    .line 9355
    :sswitch_data_4e
    .sparse-switch
        0x65 -> :sswitch_d2
        0x69 -> :sswitch_d3
    .end sparse-switch

    .line 9397
    :sswitch_data_4f
    .sparse-switch
        0x46 -> :sswitch_d5
        0x54 -> :sswitch_d6
    .end sparse-switch
.end method

.method public final l()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 9441
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9442
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 9444
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 9460
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 9447
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->k()Lflipboard/objs/FeedItem;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 9450
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 9464
    :goto_1
    return-object v0

    .line 9457
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 9464
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 9444
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2c -> :sswitch_2
        0x5d -> :sswitch_1
        0x6e -> :sswitch_0
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method public final m()Lflipboard/objs/FlipResponse;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 10672
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 10673
    new-instance v2, Lflipboard/objs/FlipResponse;

    invoke-direct {v2}, Lflipboard/objs/FlipResponse;-><init>()V

    .line 10675
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 10806
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 10677
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 10791
    :pswitch_0
    const-string v0, "FlipResponse"

    const-string v3, ""

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 10679
    :pswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 10693
    const-string v0, "FlipResponse"

    const-string v3, "a"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 10681
    :sswitch_1
    const-string v0, "FlipResponse"

    const-string v3, "albumArt"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10682
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ax()Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/FlipResponse;->j:Ljava/util/List;

    goto :goto_0

    .line 10687
    :sswitch_2
    const-string v0, "FlipResponse"

    const-string v3, "author"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10688
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/FlipResponse;->e:Ljava/lang/String;

    goto :goto_0

    .line 10698
    :pswitch_2
    const-string v0, "FlipResponse"

    const-string v3, "created"

    invoke-virtual {p0, v0, v3, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10699
    invoke-direct {p0}, Lflipboard/json/JSONParser;->au()Lflipboard/objs/FlipResponse$DateResult;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/FlipResponse;->k:Lflipboard/objs/FlipResponse$DateResult;

    goto :goto_0

    .line 10704
    :pswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    .line 10718
    const-string v0, "FlipResponse"

    const-string v3, "d"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 10706
    :sswitch_3
    const-string v0, "FlipResponse"

    const-string v3, "display"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10707
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/FlipResponse;->d:Ljava/lang/String;

    goto :goto_0

    .line 10712
    :sswitch_4
    const-string v0, "FlipResponse"

    const-string v3, "domain"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10713
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/FlipResponse;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 10723
    :pswitch_4
    const-string v0, "FlipResponse"

    const-string v3, "expires"

    invoke-virtual {p0, v0, v3, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10724
    invoke-direct {p0}, Lflipboard/json/JSONParser;->au()Lflipboard/objs/FlipResponse$DateResult;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/FlipResponse;->l:Lflipboard/objs/FlipResponse$DateResult;

    goto/16 :goto_0

    .line 10729
    :pswitch_5
    const-string v0, "FlipResponse"

    const-string v3, "fullscreen"

    invoke-virtual {p0, v0, v3, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10730
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v0

    iput-boolean v0, v2, Lflipboard/objs/FlipResponse;->h:Z

    goto/16 :goto_0

    .line 10735
    :pswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_3

    .line 10749
    const-string v0, "FlipResponse"

    const-string v3, "i"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10737
    :sswitch_5
    const-string v0, "FlipResponse"

    const-string v3, "images"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10738
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ax()Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/FlipResponse;->i:Ljava/util/List;

    goto/16 :goto_0

    .line 10743
    :sswitch_6
    const-string v0, "FlipResponse"

    const-string v3, "itemcreated"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10744
    invoke-direct {p0}, Lflipboard/json/JSONParser;->au()Lflipboard/objs/FlipResponse$DateResult;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/FlipResponse;->m:Lflipboard/objs/FlipResponse$DateResult;

    goto/16 :goto_0

    .line 10754
    :pswitch_7
    const-string v0, "FlipResponse"

    const-string v3, "language"

    invoke-virtual {p0, v0, v3, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10755
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/FlipResponse;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 10760
    :pswitch_8
    const-string v0, "FlipResponse"

    const-string v3, "stats"

    invoke-virtual {p0, v0, v3, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10761
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lflipboard/objs/FlipResponse$FlipStats;

    invoke-direct {v0}, Lflipboard/objs/FlipResponse$FlipStats;-><init>()V

    :cond_1
    :goto_1
    iget v3, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v3, :sswitch_data_4

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_7
    const-string v3, "FlipResponse$FlipStats"

    const-string v4, "version"

    const/4 v5, 0x0

    invoke-virtual {p0, v3, v4, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v3

    iput v3, v0, Lflipboard/objs/FlipResponse$FlipStats;->a:I

    goto :goto_1

    :sswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v2, Lflipboard/objs/FlipResponse;->n:Lflipboard/objs/FlipResponse$FlipStats;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_2

    .line 10766
    :pswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_5

    .line 10780
    const-string v0, "FlipResponse"

    const-string v3, "t"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10768
    :sswitch_a
    const-string v0, "FlipResponse"

    const-string v3, "title"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10769
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/FlipResponse;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 10774
    :sswitch_b
    const-string v0, "FlipResponse"

    const-string v3, "type"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10775
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/FlipResponse;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 10785
    :pswitch_a
    const-string v0, "FlipResponse"

    const-string v3, "url"

    invoke-virtual {p0, v0, v3, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10786
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/FlipResponse;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 10796
    :sswitch_c
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v1, v2

    .line 10810
    :cond_3
    return-object v1

    .line 10803
    :sswitch_d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 10675
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_d
        0xa -> :sswitch_d
        0xd -> :sswitch_d
        0x20 -> :sswitch_d
        0x22 -> :sswitch_0
        0x2c -> :sswitch_d
        0x7d -> :sswitch_c
    .end sparse-switch

    .line 10677
    :pswitch_data_0
    .packed-switch 0x61
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 10679
    :sswitch_data_1
    .sparse-switch
        0x6c -> :sswitch_1
        0x75 -> :sswitch_2
    .end sparse-switch

    .line 10704
    :sswitch_data_2
    .sparse-switch
        0x69 -> :sswitch_3
        0x6f -> :sswitch_4
    .end sparse-switch

    .line 10735
    :sswitch_data_3
    .sparse-switch
        0x6d -> :sswitch_5
        0x74 -> :sswitch_6
    .end sparse-switch

    .line 10761
    :sswitch_data_4
    .sparse-switch
        0x9 -> :sswitch_9
        0xa -> :sswitch_9
        0xd -> :sswitch_9
        0x20 -> :sswitch_9
        0x22 -> :sswitch_7
        0x2c -> :sswitch_9
        0x7d -> :sswitch_8
    .end sparse-switch

    .line 10766
    :sswitch_data_5
    .sparse-switch
        0x69 -> :sswitch_a
        0x79 -> :sswitch_b
    .end sparse-switch
.end method

.method public final n()Lflipboard/objs/Invite;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    .line 11729
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 11730
    new-instance v0, Lflipboard/objs/Invite;

    invoke-direct {v0}, Lflipboard/objs/Invite;-><init>()V

    .line 11732
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 11828
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 11734
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 11813
    const-string v1, "Invite"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 11736
    :sswitch_1
    const-string v1, "Invite"

    const-string v2, "author"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11737
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 11783
    const-string v1, "Invite"

    const-string v2, "author"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 11739
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    .line 11753
    const-string v1, "Invite"

    const-string v2, "authorD"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 11741
    :sswitch_3
    const-string v1, "Invite"

    const-string v2, "authorDescription"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11742
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Invite;->e:Ljava/lang/String;

    goto :goto_0

    .line 11747
    :sswitch_4
    const-string v1, "Invite"

    const-string v2, "authorDisplayName"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11748
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Invite;->c:Ljava/lang/String;

    goto :goto_0

    .line 11758
    :sswitch_5
    const-string v1, "Invite"

    const-string v2, "authorImage"

    const/4 v3, 0x7

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11759
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Invite;->f:Lflipboard/objs/Image;

    goto :goto_0

    .line 11764
    :sswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4

    .line 11778
    const-string v1, "Invite"

    const-string v2, "authorU"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 11766
    :sswitch_7
    const-string v1, "Invite"

    const-string v2, "authorURL"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11767
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Invite;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 11772
    :sswitch_8
    const-string v1, "Invite"

    const-string v2, "authorUsername"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11773
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Invite;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 11789
    :sswitch_9
    const-string v1, "Invite"

    const-string v2, "inviteToken"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11790
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Invite;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 11795
    :sswitch_a
    const-string v1, "Invite"

    const-string v2, "magazineTarget"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11796
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Invite;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 11801
    :sswitch_b
    const-string v1, "Invite"

    const-string v2, "service"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11802
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Invite;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 11807
    :sswitch_c
    const-string v1, "Invite"

    const-string v2, "title"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11808
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/Invite;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 11818
    :sswitch_d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 11832
    :goto_1
    return-object v0

    .line 11825
    :sswitch_e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 11832
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 11732
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_e
        0xa -> :sswitch_e
        0xd -> :sswitch_e
        0x20 -> :sswitch_e
        0x22 -> :sswitch_0
        0x2c -> :sswitch_e
        0x7d -> :sswitch_d
    .end sparse-switch

    .line 11734
    :sswitch_data_1
    .sparse-switch
        0x61 -> :sswitch_1
        0x69 -> :sswitch_9
        0x6d -> :sswitch_a
        0x73 -> :sswitch_b
        0x74 -> :sswitch_c
    .end sparse-switch

    .line 11737
    :sswitch_data_2
    .sparse-switch
        0x44 -> :sswitch_2
        0x49 -> :sswitch_5
        0x55 -> :sswitch_6
    .end sparse-switch

    .line 11739
    :sswitch_data_3
    .sparse-switch
        0x65 -> :sswitch_3
        0x69 -> :sswitch_4
    .end sparse-switch

    .line 11764
    :sswitch_data_4
    .sparse-switch
        0x52 -> :sswitch_7
        0x73 -> :sswitch_8
    .end sparse-switch
.end method

.method public final o()Lflipboard/objs/LightBoxes;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 11935
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 11936
    new-instance v2, Lflipboard/objs/LightBoxes;

    invoke-direct {v2}, Lflipboard/objs/LightBoxes;-><init>()V

    .line 11938
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 11956
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 11940
    :sswitch_0
    const-string v0, "LightBoxes"

    const-string v3, "lightboxes"

    const/4 v4, 0x0

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11941
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->K()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    :goto_1
    iget v3, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v3, :sswitch_data_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    iget v3, p0, Lflipboard/json/JSONParser;->e:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v3}, Lflipboard/json/JSONParser;->a(I)Ljava/lang/String;

    move-result-object v3

    :goto_2
    iget v4, p0, Lflipboard/json/JSONParser;->f:I

    const/16 v5, 0x20

    if-eq v4, v5, :cond_1

    iget v4, p0, Lflipboard/json/JSONParser;->f:I

    const/16 v5, 0x9

    if-eq v4, v5, :cond_1

    iget v4, p0, Lflipboard/json/JSONParser;->f:I

    const/16 v5, 0xd

    if-eq v4, v5, :cond_1

    iget v4, p0, Lflipboard/json/JSONParser;->f:I

    const/16 v5, 0xa

    if-eq v4, v5, :cond_1

    iget v4, p0, Lflipboard/json/JSONParser;->f:I

    const/16 v5, 0x3a

    if-ne v4, v5, :cond_2

    :cond_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_2

    :cond_2
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aA()Lflipboard/objs/LightBoxes$LightBox;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_3
    iput-object v0, v2, Lflipboard/objs/LightBoxes;->a:Ljava/util/Map;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_3

    .line 11946
    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v1, v2

    .line 11960
    :cond_4
    return-object v1

    .line 11953
    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 11938
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_5
        0xa -> :sswitch_5
        0xd -> :sswitch_5
        0x20 -> :sswitch_5
        0x22 -> :sswitch_0
        0x2c -> :sswitch_5
        0x7d -> :sswitch_4
    .end sparse-switch

    .line 11941
    :sswitch_data_1
    .sparse-switch
        0x9 -> :sswitch_3
        0xa -> :sswitch_3
        0xd -> :sswitch_3
        0x20 -> :sswitch_3
        0x22 -> :sswitch_1
        0x2c -> :sswitch_3
        0x7d -> :sswitch_2
    .end sparse-switch
.end method

.method public final p()Lflipboard/objs/Magazine;
    .locals 9

    .prologue
    const/16 v8, 0xb

    const/4 v7, 0x2

    const/16 v6, 0x9

    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 12659
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 12660
    new-instance v1, Lflipboard/objs/Magazine;

    invoke-direct {v1}, Lflipboard/objs/Magazine;-><init>()V

    .line 12662
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 12867
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 12664
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 12852
    :pswitch_0
    const-string v0, "Magazine"

    const-string v2, ""

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 12666
    :pswitch_1
    const-string v0, "Magazine"

    const-string v2, "author"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12667
    invoke-direct {p0}, Lflipboard/json/JSONParser;->Y()Lflipboard/objs/Author;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/Magazine;->i:Lflipboard/objs/Author;

    goto :goto_0

    .line 12672
    :pswitch_2
    const-string v0, "Magazine"

    const-string v2, "de"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12673
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 12687
    const-string v0, "Magazine"

    const-string v2, "de"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 12675
    :sswitch_1
    const-string v0, "Magazine"

    const-string v2, "defaultMagazineDrawableId"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12676
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/Magazine;->o:I

    goto :goto_0

    .line 12681
    :sswitch_2
    const-string v0, "Magazine"

    const-string v2, "description"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12682
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/Magazine;->e:Ljava/lang/String;

    goto :goto_0

    .line 12693
    :pswitch_3
    const-string v0, "Magazine"

    const-string v2, "feedType"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12694
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/Magazine;->k:Ljava/lang/String;

    goto :goto_0

    .line 12699
    :pswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    .line 12728
    const-string v0, "Magazine"

    const-string v2, "i"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 12701
    :sswitch_3
    const-string v0, "Magazine"

    const-string v2, "image"

    invoke-virtual {p0, v0, v2, v7}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12702
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_3

    .line 12716
    const-string v0, "Magazine"

    const-string v2, "image"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 12704
    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12705
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/Magazine;->t:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 12710
    :sswitch_5
    const-string v0, "Magazine"

    const-string v2, "imageURL"

    const/4 v3, 0x6

    invoke-virtual {p0, v0, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12711
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/Magazine;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 12722
    :sswitch_6
    const-string v0, "Magazine"

    const-string v2, "isDummyMagazine"

    invoke-virtual {p0, v0, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12723
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v0

    iput-boolean v0, v1, Lflipboard/objs/Magazine;->n:Z

    goto/16 :goto_0

    .line 12733
    :pswitch_5
    const-string v0, "Magazine"

    const-string v2, "link"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12734
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lflipboard/objs/Link;

    invoke-direct {v0}, Lflipboard/objs/Link;-><init>()V

    :cond_1
    :goto_1
    iget v2, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v2, :sswitch_data_4

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v2

    sparse-switch v2, :sswitch_data_5

    const-string v2, "Link"

    const-string v3, ""

    invoke-virtual {p0, v2, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :sswitch_8
    const-string v2, "Link"

    const-string v3, "hints"

    invoke-virtual {p0, v2, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lflipboard/objs/Link;->c:Ljava/lang/String;

    goto :goto_1

    :sswitch_9
    const-string v2, "Link"

    const-string v3, "id"

    invoke-virtual {p0, v2, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lflipboard/objs/Link;->b:Ljava/lang/String;

    goto :goto_1

    :sswitch_a
    const-string v2, "Link"

    const-string v3, "type"

    invoke-virtual {p0, v2, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lflipboard/objs/Link;->a:Ljava/lang/String;

    goto :goto_1

    :sswitch_b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v1, Lflipboard/objs/Magazine;->p:Lflipboard/objs/Link;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 12739
    :pswitch_6
    const-string v0, "Magazine"

    const-string v2, "magazine"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12740
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_6

    .line 12813
    const-string v0, "Magazine"

    const-string v2, "magazine"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 12742
    :sswitch_d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_7

    .line 12769
    const-string v0, "Magazine"

    const-string v2, "magazineC"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 12744
    :sswitch_e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_8

    .line 12758
    const-string v0, "Magazine"

    const-string v2, "magazineCa"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 12746
    :sswitch_f
    const-string v0, "Magazine"

    const-string v2, "magazineCanChangeVisibility"

    invoke-virtual {p0, v0, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12747
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v0

    iput-boolean v0, v1, Lflipboard/objs/Magazine;->f:Z

    goto/16 :goto_0

    .line 12752
    :sswitch_10
    const-string v0, "Magazine"

    const-string v2, "magazineCategory"

    invoke-virtual {p0, v0, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12753
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/Magazine;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 12763
    :sswitch_11
    const-string v0, "Magazine"

    const-string v2, "magazineCoverItemId"

    const/16 v3, 0xa

    invoke-virtual {p0, v0, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12764
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/Magazine;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 12774
    :sswitch_12
    const-string v0, "Magazine"

    const-string v2, "magazineDate"

    invoke-virtual {p0, v0, v2, v6}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12775
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_9

    .line 12789
    const-string v0, "Magazine"

    const-string v2, "magazineDate"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 12777
    :sswitch_13
    const-string v0, "Magazine"

    const-string v2, "magazineDateCreated"

    const/16 v3, 0xd

    invoke-virtual {p0, v0, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12778
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v2

    iput-wide v2, v1, Lflipboard/objs/Magazine;->g:J

    goto/16 :goto_0

    .line 12783
    :sswitch_14
    const-string v0, "Magazine"

    const-string v2, "magazineDateLastPosted"

    const/16 v3, 0xd

    invoke-virtual {p0, v0, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12784
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v2

    iput-wide v2, v1, Lflipboard/objs/Magazine;->h:J

    goto/16 :goto_0

    .line 12795
    :sswitch_15
    const-string v0, "Magazine"

    const-string v2, "magazineIsDefault"

    invoke-virtual {p0, v0, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12796
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v0

    iput-boolean v0, v1, Lflipboard/objs/Magazine;->m:Z

    goto/16 :goto_0

    .line 12801
    :sswitch_16
    const-string v0, "Magazine"

    const-string v2, "magazineTarget"

    invoke-virtual {p0, v0, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12802
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/Magazine;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 12807
    :sswitch_17
    const-string v0, "Magazine"

    const-string v2, "magazineVisibility"

    invoke-virtual {p0, v0, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12808
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 12819
    :pswitch_7
    const-string v0, "Magazine"

    const-string v2, "remoteid"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12820
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 12825
    :pswitch_8
    const-string v0, "Magazine"

    const-string v2, "se"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12826
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_a

    .line 12840
    const-string v0, "Magazine"

    const-string v2, "se"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 12828
    :sswitch_18
    const-string v0, "Magazine"

    const-string v2, "sectionTitle"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12829
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/Magazine;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 12834
    :sswitch_19
    const-string v0, "Magazine"

    const-string v2, "service"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12835
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/Magazine;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 12846
    :pswitch_9
    const-string v0, "Magazine"

    const-string v2, "title"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12847
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 12857
    :sswitch_1a
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v0, v1

    .line 12871
    :goto_3
    return-object v0

    .line 12864
    :sswitch_1b
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 12871
    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    .line 12662
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_1b
        0xa -> :sswitch_1b
        0xd -> :sswitch_1b
        0x20 -> :sswitch_1b
        0x22 -> :sswitch_0
        0x2c -> :sswitch_1b
        0x7d -> :sswitch_1a
    .end sparse-switch

    .line 12664
    :pswitch_data_0
    .packed-switch 0x61
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 12673
    :sswitch_data_1
    .sparse-switch
        0x66 -> :sswitch_1
        0x73 -> :sswitch_2
    .end sparse-switch

    .line 12699
    :sswitch_data_2
    .sparse-switch
        0x6d -> :sswitch_3
        0x73 -> :sswitch_6
    .end sparse-switch

    .line 12702
    :sswitch_data_3
    .sparse-switch
        0x22 -> :sswitch_4
        0x55 -> :sswitch_5
    .end sparse-switch

    .line 12734
    :sswitch_data_4
    .sparse-switch
        0x9 -> :sswitch_c
        0xa -> :sswitch_c
        0xd -> :sswitch_c
        0x20 -> :sswitch_c
        0x22 -> :sswitch_7
        0x2c -> :sswitch_c
        0x7d -> :sswitch_b
    .end sparse-switch

    :sswitch_data_5
    .sparse-switch
        0x68 -> :sswitch_8
        0x69 -> :sswitch_9
        0x74 -> :sswitch_a
    .end sparse-switch

    .line 12740
    :sswitch_data_6
    .sparse-switch
        0x43 -> :sswitch_d
        0x44 -> :sswitch_12
        0x49 -> :sswitch_15
        0x54 -> :sswitch_16
        0x56 -> :sswitch_17
    .end sparse-switch

    .line 12742
    :sswitch_data_7
    .sparse-switch
        0x61 -> :sswitch_e
        0x6f -> :sswitch_11
    .end sparse-switch

    .line 12744
    :sswitch_data_8
    .sparse-switch
        0x6e -> :sswitch_f
        0x74 -> :sswitch_10
    .end sparse-switch

    .line 12775
    :sswitch_data_9
    .sparse-switch
        0x43 -> :sswitch_13
        0x4c -> :sswitch_14
    .end sparse-switch

    .line 12826
    :sswitch_data_a
    .sparse-switch
        0x63 -> :sswitch_18
        0x72 -> :sswitch_19
    .end sparse-switch
.end method

.method public final q()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Magazine;",
            ">;"
        }
    .end annotation

    .prologue
    .line 12876
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12877
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 12879
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 12895
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 12882
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->p()Lflipboard/objs/Magazine;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 12885
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 12899
    :goto_1
    return-object v0

    .line 12892
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 12899
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 12879
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2c -> :sswitch_2
        0x5d -> :sswitch_1
        0x6e -> :sswitch_0
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method public final r()Lflipboard/objs/SearchResultItem;
    .locals 7

    .prologue
    const/16 v6, 0x9

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 13663
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13664
    new-instance v0, Lflipboard/objs/SearchResultItem;

    invoke-direct {v0}, Lflipboard/objs/SearchResultItem;-><init>()V

    .line 13666
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 13859
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 13668
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 13844
    :pswitch_0
    const-string v1, "SearchResultItem"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 13670
    :pswitch_1
    const-string v1, "SearchResultItem"

    const-string v2, "bannerColor"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13671
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->L:Ljava/lang/String;

    goto :goto_0

    .line 13676
    :pswitch_2
    const-string v1, "SearchResultItem"

    const-string v2, "category"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13677
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 13718
    const-string v1, "SearchResultItem"

    const-string v2, "category"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 13679
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13680
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    goto :goto_0

    .line 13685
    :sswitch_2
    const-string v1, "SearchResultItem"

    const-string v2, "categoryList"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13686
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    .line 13700
    const-string v1, "SearchResultItem"

    const-string v2, "categoryList"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 13688
    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13689
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->D:Ljava/lang/String;

    goto :goto_0

    .line 13694
    :sswitch_4
    const-string v1, "SearchResultItem"

    const-string v2, "categoryListWeight"

    const/16 v3, 0xd

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13695
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/SearchResultItem;->E:F

    goto/16 :goto_0

    .line 13706
    :sswitch_5
    const-string v1, "SearchResultItem"

    const-string v2, "categoryTitle"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13707
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->G:Ljava/lang/String;

    goto/16 :goto_0

    .line 13712
    :sswitch_6
    const-string v1, "SearchResultItem"

    const-string v2, "categoryWeight"

    invoke-virtual {p0, v1, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13713
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/SearchResultItem;->H:F

    goto/16 :goto_0

    .line 13724
    :pswitch_3
    const-string v1, "SearchResultItem"

    const-string v2, "description"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13725
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 13730
    :pswitch_4
    const-string v1, "SearchResultItem"

    const-string v2, "feedType"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13731
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    goto/16 :goto_0

    .line 13736
    :pswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    .line 13750
    const-string v1, "SearchResultItem"

    const-string v2, "i"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 13738
    :sswitch_7
    const-string v1, "SearchResultItem"

    const-string v2, "imageURL"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13739
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->t:Ljava/lang/String;

    goto/16 :goto_0

    .line 13744
    :sswitch_8
    const-string v1, "SearchResultItem"

    const-string v2, "isFavicon"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13745
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SearchResultItem;->K:Z

    goto/16 :goto_0

    .line 13755
    :pswitch_6
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_4

    .line 13769
    const-string v1, "SearchResultItem"

    const-string v2, "m"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 13757
    :sswitch_9
    const-string v1, "SearchResultItem"

    const-string v2, "magazineAuthor"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13758
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->z:Ljava/lang/String;

    goto/16 :goto_0

    .line 13763
    :sswitch_a
    const-string v1, "SearchResultItem"

    const-string v2, "metricsDisplay"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13764
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->y:Ljava/lang/String;

    goto/16 :goto_0

    .line 13774
    :pswitch_7
    const-string v1, "SearchResultItem"

    const-string v2, "partnerId"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13775
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->B:Ljava/lang/String;

    goto/16 :goto_0

    .line 13780
    :pswitch_8
    const-string v1, "SearchResultItem"

    const-string v2, "remoteid"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13781
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->R()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    goto/16 :goto_0

    .line 13786
    :pswitch_9
    const-string v1, "SearchResultItem"

    const-string v2, "service"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13787
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->v:Ljava/lang/String;

    goto/16 :goto_0

    .line 13792
    :pswitch_a
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_5

    .line 13821
    const-string v1, "SearchResultItem"

    const-string v2, "t"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 13794
    :sswitch_b
    const-string v1, "SearchResultItem"

    const-string v2, "title"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13795
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_6

    .line 13809
    const-string v1, "SearchResultItem"

    const-string v2, "title"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 13797
    :sswitch_c
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13798
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 13803
    :sswitch_d
    const-string v1, "SearchResultItem"

    const-string v2, "titleSuffix"

    const/4 v3, 0x6

    invoke-virtual {p0, v1, v2, v3}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13804
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->J:Ljava/lang/String;

    goto/16 :goto_0

    .line 13815
    :sswitch_e
    const-string v1, "SearchResultItem"

    const-string v2, "topic"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13816
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->A:Ljava/lang/String;

    goto/16 :goto_0

    .line 13826
    :pswitch_b
    const-string v1, "SearchResultItem"

    const-string v2, "userid"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13827
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->C:Ljava/lang/String;

    goto/16 :goto_0

    .line 13832
    :pswitch_c
    const-string v1, "SearchResultItem"

    const-string v2, "verified"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13833
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SearchResultItem;->I:Z

    goto/16 :goto_0

    .line 13838
    :pswitch_d
    const-string v1, "SearchResultItem"

    const-string v2, "weight"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13839
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/SearchResultItem;->u:F

    goto/16 :goto_0

    .line 13849
    :sswitch_f
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 13863
    :goto_1
    return-object v0

    .line 13856
    :sswitch_10
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 13863
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 13666
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_10
        0xa -> :sswitch_10
        0xd -> :sswitch_10
        0x20 -> :sswitch_10
        0x22 -> :sswitch_0
        0x2c -> :sswitch_10
        0x7d -> :sswitch_f
    .end sparse-switch

    .line 13668
    :pswitch_data_0
    .packed-switch 0x62
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 13677
    :sswitch_data_1
    .sparse-switch
        0x22 -> :sswitch_1
        0x4c -> :sswitch_2
        0x54 -> :sswitch_5
        0x57 -> :sswitch_6
    .end sparse-switch

    .line 13686
    :sswitch_data_2
    .sparse-switch
        0x22 -> :sswitch_3
        0x57 -> :sswitch_4
    .end sparse-switch

    .line 13736
    :sswitch_data_3
    .sparse-switch
        0x6d -> :sswitch_7
        0x73 -> :sswitch_8
    .end sparse-switch

    .line 13755
    :sswitch_data_4
    .sparse-switch
        0x61 -> :sswitch_9
        0x65 -> :sswitch_a
    .end sparse-switch

    .line 13792
    :sswitch_data_5
    .sparse-switch
        0x69 -> :sswitch_b
        0x6f -> :sswitch_e
    .end sparse-switch

    .line 13795
    :sswitch_data_6
    .sparse-switch
        0x22 -> :sswitch_c
        0x53 -> :sswitch_d
    .end sparse-switch
.end method

.method public final s()Lflipboard/objs/SearchResultStream;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 13966
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 13967
    new-instance v2, Lflipboard/objs/SearchResultStream;

    invoke-direct {v2}, Lflipboard/objs/SearchResultStream;-><init>()V

    .line 13969
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 14006
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 13971
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 13991
    const-string v0, "SearchResultStream"

    const-string v3, ""

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 13973
    :sswitch_1
    const-string v0, "SearchResultStream"

    const-string v3, "code"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13974
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v2, Lflipboard/objs/SearchResultStream;->b:I

    goto :goto_0

    .line 13979
    :sswitch_2
    const-string v0, "SearchResultStream"

    const-string v3, "stream"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13980
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    iget v3, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v3, :sswitch_data_2

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_3
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aC()Lflipboard/objs/SearchResultCategory;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v2, Lflipboard/objs/SearchResultStream;->a:Ljava/util/List;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_2

    .line 13985
    :sswitch_6
    const-string v0, "SearchResultStream"

    const-string v3, "time"

    invoke-virtual {p0, v0, v3, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13986
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v4

    iput-wide v4, v2, Lflipboard/objs/SearchResultStream;->c:J

    goto :goto_0

    .line 13996
    :sswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v1, v2

    .line 14010
    :cond_2
    return-object v1

    .line 14003
    :sswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 13969
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_8
        0xa -> :sswitch_8
        0xd -> :sswitch_8
        0x20 -> :sswitch_8
        0x22 -> :sswitch_0
        0x2c -> :sswitch_8
        0x7d -> :sswitch_7
    .end sparse-switch

    .line 13971
    :sswitch_data_1
    .sparse-switch
        0x63 -> :sswitch_1
        0x73 -> :sswitch_2
        0x74 -> :sswitch_6
    .end sparse-switch

    .line 13980
    :sswitch_data_2
    .sparse-switch
        0x9 -> :sswitch_5
        0xa -> :sswitch_5
        0xd -> :sswitch_5
        0x20 -> :sswitch_5
        0x2c -> :sswitch_5
        0x5d -> :sswitch_4
        0x6e -> :sswitch_3
        0x7b -> :sswitch_3
    .end sparse-switch
.end method

.method public final t()Lflipboard/objs/SectionListResult;
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x1

    .line 14492
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14493
    new-instance v0, Lflipboard/objs/SectionListResult;

    invoke-direct {v0}, Lflipboard/objs/SectionListResult;-><init>()V

    .line 14495
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 14583
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 14497
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 14568
    :pswitch_0
    const-string v1, "SectionListResult"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14499
    :pswitch_1
    const-string v1, "SectionListResult"

    const-string v2, "code"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14500
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/SectionListResult;->c:I

    goto :goto_0

    .line 14505
    :pswitch_2
    const-string v1, "SectionListResult"

    const-string v2, "displaymessage"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14506
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListResult;->h:Ljava/lang/String;

    goto :goto_0

    .line 14511
    :pswitch_3
    const-string v1, "SectionListResult"

    const-string v2, "error"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14512
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 14526
    const-string v1, "SectionListResult"

    const-string v2, "error"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14514
    :sswitch_1
    const-string v1, "SectionListResult"

    const-string v2, "errorcode"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14515
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/SectionListResult;->f:I

    goto :goto_0

    .line 14520
    :sswitch_2
    const-string v1, "SectionListResult"

    const-string v2, "errormessage"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14521
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListResult;->g:Ljava/lang/String;

    goto :goto_0

    .line 14532
    :pswitch_4
    const-string v1, "SectionListResult"

    const-string v2, "items"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14533
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aE()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListResult;->i:Ljava/util/List;

    goto/16 :goto_0

    .line 14538
    :pswitch_5
    const-string v1, "SectionListResult"

    const-string v2, "message"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14539
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListResult;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 14544
    :pswitch_6
    const-string v1, "SectionListResult"

    const-string v2, "pageKey"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14545
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListResult;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 14550
    :pswitch_7
    const-string v1, "SectionListResult"

    const-string v2, "results"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14551
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aE()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SectionListResult;->a:Ljava/util/List;

    goto/16 :goto_0

    .line 14556
    :pswitch_8
    const-string v1, "SectionListResult"

    const-string v2, "success"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14557
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/SectionListResult;->b:Z

    goto/16 :goto_0

    .line 14562
    :pswitch_9
    const-string v1, "SectionListResult"

    const-string v2, "time"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14563
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/objs/SectionListResult;->e:J

    goto/16 :goto_0

    .line 14573
    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 14587
    :goto_1
    return-object v0

    .line 14580
    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 14587
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 14495
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_4
        0xa -> :sswitch_4
        0xd -> :sswitch_4
        0x20 -> :sswitch_4
        0x22 -> :sswitch_0
        0x2c -> :sswitch_4
        0x7d -> :sswitch_3
    .end sparse-switch

    .line 14497
    :pswitch_data_0
    .packed-switch 0x63
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 14512
    :sswitch_data_1
    .sparse-switch
        0x63 -> :sswitch_1
        0x6d -> :sswitch_2
    .end sparse-switch
.end method

.method public final u()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SectionPageTemplate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14869
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14870
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 14872
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 14888
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 14875
    :sswitch_0
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aF()Lflipboard/objs/SectionPageTemplate;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 14878
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 14892
    :goto_1
    return-object v0

    .line 14885
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 14892
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 14872
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2c -> :sswitch_2
        0x5d -> :sswitch_1
        0x6e -> :sswitch_0
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method public final v()Lflipboard/objs/SidebarGroup;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 15153
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 15154
    new-instance v2, Lflipboard/objs/SidebarGroup;

    invoke-direct {v2}, Lflipboard/objs/SidebarGroup;-><init>()V

    .line 15156
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 15256
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 15158
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 15241
    :pswitch_0
    const-string v0, "SidebarGroup"

    const-string v3, ""

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 15160
    :pswitch_1
    const-string v0, "SidebarGroup"

    const-string v3, "groupId"

    invoke-virtual {p0, v0, v3, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15161
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    goto :goto_0

    .line 15166
    :pswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 15180
    const-string v0, "SidebarGroup"

    const-string v3, "i"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 15168
    :sswitch_1
    const-string v0, "SidebarGroup"

    const-string v3, "impressionValue"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15169
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/SidebarGroup;->i:Ljava/lang/String;

    goto :goto_0

    .line 15174
    :sswitch_2
    const-string v0, "SidebarGroup"

    const-string v3, "items"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15175
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->l()Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    goto :goto_0

    .line 15185
    :pswitch_3
    const-string v0, "SidebarGroup"

    const-string v3, "metrics"

    invoke-virtual {p0, v0, v3, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15186
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    iget v3, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v3, :sswitch_data_2

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_3
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aJ()Lflipboard/objs/SidebarGroup$Metrics;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v2, Lflipboard/objs/SidebarGroup;->f:Ljava/util/List;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_2

    .line 15191
    :pswitch_4
    const-string v0, "SidebarGroup"

    const-string v3, "renderHints"

    invoke-virtual {p0, v0, v3, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15192
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aL()Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/SidebarGroup;->e:Ljava/util/List;

    goto/16 :goto_0

    .line 15197
    :pswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 15211
    :pswitch_6
    const-string v0, "SidebarGroup"

    const-string v3, "s"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 15199
    :pswitch_7
    const-string v0, "SidebarGroup"

    const-string v3, "service"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15200
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/SidebarGroup;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 15205
    :pswitch_8
    const-string v0, "SidebarGroup"

    const-string v3, "showInline"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15206
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v0

    iput-boolean v0, v2, Lflipboard/objs/SidebarGroup;->b:Z

    goto/16 :goto_0

    .line 15216
    :pswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_3

    .line 15230
    const-string v0, "SidebarGroup"

    const-string v3, "t"

    invoke-virtual {p0, v0, v3}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 15218
    :sswitch_6
    const-string v0, "SidebarGroup"

    const-string v3, "title"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15219
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/SidebarGroup;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 15224
    :sswitch_7
    const-string v0, "SidebarGroup"

    const-string v3, "type"

    invoke-virtual {p0, v0, v3, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15225
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/SidebarGroup;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 15235
    :pswitch_a
    const-string v0, "SidebarGroup"

    const-string v3, "usageType"

    invoke-virtual {p0, v0, v3, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15236
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lflipboard/objs/SidebarGroup;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 15246
    :sswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v1, v2

    .line 15260
    :cond_2
    return-object v1

    .line 15253
    :sswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 15156
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_9
        0xa -> :sswitch_9
        0xd -> :sswitch_9
        0x20 -> :sswitch_9
        0x22 -> :sswitch_0
        0x2c -> :sswitch_9
        0x7d -> :sswitch_8
    .end sparse-switch

    .line 15158
    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 15166
    :sswitch_data_1
    .sparse-switch
        0x6d -> :sswitch_1
        0x74 -> :sswitch_2
    .end sparse-switch

    .line 15186
    :sswitch_data_2
    .sparse-switch
        0x9 -> :sswitch_5
        0xa -> :sswitch_5
        0xd -> :sswitch_5
        0x20 -> :sswitch_5
        0x2c -> :sswitch_5
        0x5d -> :sswitch_4
        0x6e -> :sswitch_3
        0x7b -> :sswitch_3
    .end sparse-switch

    .line 15197
    :pswitch_data_1
    .packed-switch 0x65
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_8
    .end packed-switch

    .line 15216
    :sswitch_data_3
    .sparse-switch
        0x69 -> :sswitch_6
        0x79 -> :sswitch_7
    .end sparse-switch
.end method

.method public final w()Lflipboard/objs/TOCSection;
    .locals 4

    .prologue
    .line 15969
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 15970
    new-instance v0, Lflipboard/objs/TOCSection;

    invoke-direct {v0}, Lflipboard/objs/TOCSection;-><init>()V

    .line 15972
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 16075
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 15974
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    .line 15975
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->I()Z

    .line 15976
    const-string v2, "brick"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 15977
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->a:Lflipboard/objs/Image;

    goto :goto_0

    .line 15978
    :cond_0
    const-string v2, "campaignTarget"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 15979
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->b:Ljava/lang/String;

    goto :goto_0

    .line 15980
    :cond_1
    const-string v2, "category"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 15981
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->c:Ljava/lang/String;

    goto :goto_0

    .line 15982
    :cond_2
    const-string v2, "categoryList"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 15983
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->d:Ljava/lang/String;

    goto :goto_0

    .line 15984
    :cond_3
    const-string v2, "categoryTitle"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 15985
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->e:Ljava/lang/String;

    goto :goto_0

    .line 15986
    :cond_4
    const-string v2, "description"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 15987
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->f:Ljava/lang/String;

    goto :goto_0

    .line 15988
    :cond_5
    const-string v2, "icon"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 15989
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->g:Ljava/lang/String;

    goto :goto_0

    .line 15990
    :cond_6
    const-string v2, "imageURL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 15991
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 15992
    :cond_7
    const-string v2, "image"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 15993
    invoke-direct {p0}, Lflipboard/json/JSONParser;->ay()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->i:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 15994
    :cond_8
    const-string v2, "keywords"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 15995
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 15996
    :cond_9
    const-string v2, "remoteid"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 15997
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 15998
    :cond_a
    const-string v2, "sectionTitle"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 15999
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 16000
    :cond_b
    const-string v2, "service"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 16001
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 16002
    :cond_c
    const-string v2, "title"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 16003
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 16004
    :cond_d
    const-string v2, "titleSuffix"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 16005
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 16006
    :cond_e
    const-string v2, "type"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 16007
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->p:Ljava/lang/String;

    goto/16 :goto_0

    .line 16008
    :cond_f
    const-string v2, "unreadRemoteid"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 16009
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 16010
    :cond_10
    const-string v2, "version"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 16011
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 16012
    :cond_11
    const-string v2, "private"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 16013
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->s:Z

    goto/16 :goto_0

    .line 16014
    :cond_12
    const-string v2, "canFavorite"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 16015
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->t:Z

    goto/16 :goto_0

    .line 16016
    :cond_13
    const-string v2, "feedFetchingDisabled"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 16017
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->u:Z

    goto/16 :goto_0

    .line 16018
    :cond_14
    const-string v2, "isFavicon"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 16019
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->v:Z

    goto/16 :goto_0

    .line 16020
    :cond_15
    const-string v2, "verified"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 16021
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->w:Z

    goto/16 :goto_0

    .line 16022
    :cond_16
    const-string v2, "categoryListWeight"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 16023
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/TOCSection;->x:F

    goto/16 :goto_0

    .line 16024
    :cond_17
    const-string v2, "categoryWeight"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 16025
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/TOCSection;->y:F

    goto/16 :goto_0

    .line 16026
    :cond_18
    const-string v2, "weight"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 16027
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->O()F

    move-result v1

    iput v1, v0, Lflipboard/objs/TOCSection;->z:F

    goto/16 :goto_0

    .line 16028
    :cond_19
    const-string v2, "count"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 16029
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/TOCSection;->A:I

    goto/16 :goto_0

    .line 16030
    :cond_1a
    const-string v2, "unreadCount"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 16031
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/TOCSection;->B:I

    goto/16 :goto_0

    .line 16032
    :cond_1b
    const-string v2, "time"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 16033
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/objs/TOCSection;->C:J

    goto/16 :goto_0

    .line 16034
    :cond_1c
    const-string v2, "showLogotypeImage"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 16035
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->D:Z

    goto/16 :goto_0

    .line 16036
    :cond_1d
    const-string v2, "enumerated"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 16037
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->E:Z

    goto/16 :goto_0

    .line 16038
    :cond_1e
    const-string v2, "feedType"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 16039
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    goto/16 :goto_0

    .line 16040
    :cond_1f
    const-string v2, "isLibrarySection"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 16041
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->H:Z

    goto/16 :goto_0

    .line 16042
    :cond_20
    const-string v2, "lastUpdated"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 16043
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/objs/TOCSection;->I:J

    goto/16 :goto_0

    .line 16044
    :cond_21
    const-string v2, "userid"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 16045
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->J:Ljava/lang/String;

    goto/16 :goto_0

    .line 16046
    :cond_22
    const-string v2, "magazineTarget"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 16047
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->K:Ljava/lang/String;

    goto/16 :goto_0

    .line 16048
    :cond_23
    const-string v2, "magazineVisibility"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 16049
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->L:Ljava/lang/String;

    goto/16 :goto_0

    .line 16050
    :cond_24
    const-string v2, "authorDisplayName"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 16051
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->M:Ljava/lang/String;

    goto/16 :goto_0

    .line 16052
    :cond_25
    const-string v2, "isFollowingAuthor"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_26

    .line 16053
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->N:Z

    goto/16 :goto_0

    .line 16054
    :cond_26
    const-string v2, "prominenceOverrideType"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 16055
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/TOCSection;->O:Ljava/lang/String;

    goto/16 :goto_0

    .line 16056
    :cond_27
    const-string v2, "shouldWaitForSidebar"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_28

    .line 16057
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/TOCSection;->P:Z

    goto/16 :goto_0

    .line 16059
    :cond_28
    iget-object v2, v0, Lflipboard/objs/TOCSection;->G:Lflipboard/json/FLObject;

    if-nez v2, :cond_29

    new-instance v2, Lflipboard/json/FLObject;

    invoke-direct {v2}, Lflipboard/json/FLObject;-><init>()V

    iput-object v2, v0, Lflipboard/objs/TOCSection;->G:Lflipboard/json/FLObject;

    .line 16060
    :cond_29
    iget-object v2, v0, Lflipboard/objs/TOCSection;->G:Lflipboard/json/FLObject;

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->R()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 16065
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 16079
    :goto_1
    return-object v0

    .line 16072
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 16079
    :cond_2a
    const/4 v0, 0x0

    goto :goto_1

    .line 15972
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x22 -> :sswitch_0
        0x2c -> :sswitch_2
        0x7d -> :sswitch_1
    .end sparse-switch
.end method

.method public final x()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/TOCSection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16084
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16085
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 16087
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 16103
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 16090
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->w()Lflipboard/objs/TOCSection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 16093
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 16107
    :goto_1
    return-object v0

    .line 16100
    :sswitch_2
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_0

    .line 16107
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 16087
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2c -> :sswitch_2
        0x5d -> :sswitch_1
        0x6e -> :sswitch_0
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method public final y()Lflipboard/objs/UserInfo;
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x5

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 16182
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 16183
    new-instance v1, Lflipboard/objs/UserInfo;

    invoke-direct {v1}, Lflipboard/objs/UserInfo;-><init>()V

    .line 16185
    :cond_0
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 16346
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 16187
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 16331
    :pswitch_0
    const-string v0, "UserInfo"

    const-string v2, ""

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 16189
    :pswitch_1
    const-string v0, "UserInfo"

    const-string v2, "code"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16190
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/UserInfo;->c:I

    goto :goto_0

    .line 16195
    :pswitch_2
    const-string v0, "UserInfo"

    const-string v2, "displaymessage"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16196
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserInfo;->h:Ljava/lang/String;

    goto :goto_0

    .line 16201
    :pswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 16230
    const-string v0, "UserInfo"

    const-string v2, "e"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 16203
    :sswitch_1
    const-string v0, "UserInfo"

    const-string v2, "error"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16204
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    .line 16218
    const-string v0, "UserInfo"

    const-string v2, "error"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 16206
    :sswitch_2
    const-string v0, "UserInfo"

    const-string v2, "errorcode"

    invoke-virtual {p0, v0, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16207
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/UserInfo;->f:I

    goto :goto_0

    .line 16212
    :sswitch_3
    const-string v0, "UserInfo"

    const-string v2, "errormessage"

    invoke-virtual {p0, v0, v2, v8}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16213
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserInfo;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 16224
    :sswitch_4
    const-string v0, "UserInfo"

    const-string v2, "experiments"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16225
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->Q()Lflipboard/json/FLObject;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserInfo;->p:Lflipboard/json/FLObject;

    goto/16 :goto_0

    .line 16235
    :pswitch_4
    const-string v0, "UserInfo"

    const-string v2, "hasToc"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16236
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v0

    iput-boolean v0, v1, Lflipboard/objs/UserInfo;->o:Z

    goto/16 :goto_0

    .line 16241
    :pswitch_5
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_3

    .line 16274
    const-string v0, "UserInfo"

    const-string v2, "m"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 16243
    :sswitch_5
    const-string v0, "UserInfo"

    const-string v2, "magazines"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16244
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->q()Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserInfo;->n:Ljava/util/List;

    goto/16 :goto_0

    .line 16249
    :sswitch_6
    const-string v0, "UserInfo"

    const-string v2, "message"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16250
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserInfo;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 16255
    :sswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 16269
    const-string v0, "UserInfo"

    const-string v2, "my"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 16257
    :pswitch_6
    const-string v0, "UserInfo"

    const-string v2, "myReadLaterServices"

    invoke-virtual {p0, v0, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16258
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aM()Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserInfo;->k:Ljava/util/List;

    goto/16 :goto_0

    .line 16263
    :pswitch_7
    const-string v0, "UserInfo"

    const-string v2, "myServices"

    invoke-virtual {p0, v0, v2, v6}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16264
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aM()Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserInfo;->j:Ljava/util/List;

    goto/16 :goto_0

    .line 16279
    :pswitch_8
    const-string v0, "UserInfo"

    const-string v2, "name"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16280
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserInfo;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 16285
    :pswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    .line 16299
    const-string v0, "UserInfo"

    const-string v2, "s"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 16287
    :pswitch_a
    const-string v0, "UserInfo"

    const-string v2, "states"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16288
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    iget v2, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v2, :sswitch_data_4

    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_1

    :sswitch_8
    invoke-direct {p0}, Lflipboard/json/JSONParser;->aR()Lflipboard/objs/UserState$State;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    :goto_2
    iput-object v0, v1, Lflipboard/objs/UserInfo;->l:Ljava/util/List;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 16293
    :pswitch_b
    const-string v0, "UserInfo"

    const-string v2, "success"

    invoke-virtual {p0, v0, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16294
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v0

    iput-boolean v0, v1, Lflipboard/objs/UserInfo;->b:Z

    goto/16 :goto_0

    .line 16304
    :pswitch_c
    const-string v0, "UserInfo"

    const-string v2, "time"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16305
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v2

    iput-wide v2, v1, Lflipboard/objs/UserInfo;->e:J

    goto/16 :goto_0

    .line 16310
    :pswitch_d
    const-string v0, "UserInfo"

    const-string v2, "user"

    invoke-virtual {p0, v0, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16311
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_5

    .line 16325
    const-string v0, "UserInfo"

    const-string v2, "user"

    invoke-virtual {p0, v0, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 16313
    :sswitch_b
    const-string v0, "UserInfo"

    const-string v2, "userInfo"

    invoke-virtual {p0, v0, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16314
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->y()Lflipboard/objs/UserInfo;

    move-result-object v0

    iput-object v0, v1, Lflipboard/objs/UserInfo;->m:Lflipboard/objs/UserInfo;

    goto/16 :goto_0

    .line 16319
    :sswitch_c
    const-string v0, "UserInfo"

    const-string v2, "userid"

    invoke-virtual {p0, v0, v2, v7}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16320
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v0

    iput v0, v1, Lflipboard/objs/UserInfo;->a:I

    goto/16 :goto_0

    .line 16336
    :sswitch_d
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-object v0, v1

    .line 16350
    :goto_3
    return-object v0

    .line 16343
    :sswitch_e
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 16350
    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    .line 16185
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_e
        0xa -> :sswitch_e
        0xd -> :sswitch_e
        0x20 -> :sswitch_e
        0x22 -> :sswitch_0
        0x2c -> :sswitch_e
        0x7d -> :sswitch_d
    .end sparse-switch

    .line 16187
    :pswitch_data_0
    .packed-switch 0x63
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 16201
    :sswitch_data_1
    .sparse-switch
        0x72 -> :sswitch_1
        0x78 -> :sswitch_4
    .end sparse-switch

    .line 16204
    :sswitch_data_2
    .sparse-switch
        0x63 -> :sswitch_2
        0x6d -> :sswitch_3
    .end sparse-switch

    .line 16241
    :sswitch_data_3
    .sparse-switch
        0x61 -> :sswitch_5
        0x65 -> :sswitch_6
        0x79 -> :sswitch_7
    .end sparse-switch

    .line 16255
    :pswitch_data_1
    .packed-switch 0x52
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 16285
    :pswitch_data_2
    .packed-switch 0x74
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 16288
    :sswitch_data_4
    .sparse-switch
        0x9 -> :sswitch_a
        0xa -> :sswitch_a
        0xd -> :sswitch_a
        0x20 -> :sswitch_a
        0x2c -> :sswitch_a
        0x5d -> :sswitch_9
        0x6e -> :sswitch_8
        0x7b -> :sswitch_8
    .end sparse-switch

    .line 16311
    :sswitch_data_5
    .sparse-switch
        0x49 -> :sswitch_b
        0x69 -> :sswitch_c
    .end sparse-switch
.end method

.method public final z()Lflipboard/objs/UserListResult;
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x1

    .line 16453
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16454
    new-instance v0, Lflipboard/objs/UserListResult;

    invoke-direct {v0}, Lflipboard/objs/UserListResult;-><init>()V

    .line 16456
    :cond_0
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParser;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 16538
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->F()V

    goto :goto_0

    .line 16458
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 16523
    :pswitch_0
    const-string v1, "UserListResult"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 16460
    :pswitch_1
    const-string v1, "UserListResult"

    const-string v2, "code"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16461
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/UserListResult;->c:I

    goto :goto_0

    .line 16466
    :pswitch_2
    const-string v1, "UserListResult"

    const-string v2, "displaymessage"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16467
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserListResult;->h:Ljava/lang/String;

    goto :goto_0

    .line 16472
    :pswitch_3
    const-string v1, "UserListResult"

    const-string v2, "error"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16473
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 16487
    const-string v1, "UserListResult"

    const-string v2, "error"

    invoke-virtual {p0, v1, v2}, Lflipboard/json/JSONParser;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 16475
    :sswitch_1
    const-string v1, "UserListResult"

    const-string v2, "errorcode"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16476
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->M()I

    move-result v1

    iput v1, v0, Lflipboard/objs/UserListResult;->f:I

    goto :goto_0

    .line 16481
    :sswitch_2
    const-string v1, "UserListResult"

    const-string v2, "errormessage"

    invoke-virtual {p0, v1, v2, v5}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16482
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserListResult;->g:Ljava/lang/String;

    goto :goto_0

    .line 16493
    :pswitch_4
    const-string v1, "UserListResult"

    const-string v2, "items"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16494
    invoke-direct {p0}, Lflipboard/json/JSONParser;->at()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserListResult;->a:Ljava/util/List;

    goto/16 :goto_0

    .line 16499
    :pswitch_5
    const-string v1, "UserListResult"

    const-string v2, "message"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16500
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserListResult;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 16505
    :pswitch_6
    const-string v1, "UserListResult"

    const-string v2, "pageKey"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16506
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/UserListResult;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 16511
    :pswitch_7
    const-string v1, "UserListResult"

    const-string v2, "success"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16512
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->L()Z

    move-result v1

    iput-boolean v1, v0, Lflipboard/objs/UserListResult;->b:Z

    goto/16 :goto_0

    .line 16517
    :pswitch_8
    const-string v1, "UserListResult"

    const-string v2, "time"

    invoke-virtual {p0, v1, v2, v4}, Lflipboard/json/JSONParser;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16518
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->N()J

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/objs/UserListResult;->e:J

    goto/16 :goto_0

    .line 16528
    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    .line 16542
    :goto_1
    return-object v0

    .line 16535
    :sswitch_4
    invoke-virtual {p0}, Lflipboard/json/JSONParser;->E()I

    goto/16 :goto_0

    .line 16542
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 16456
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_4
        0xa -> :sswitch_4
        0xd -> :sswitch_4
        0x20 -> :sswitch_4
        0x22 -> :sswitch_0
        0x2c -> :sswitch_4
        0x7d -> :sswitch_3
    .end sparse-switch

    .line 16458
    :pswitch_data_0
    .packed-switch 0x63
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 16473
    :sswitch_data_1
    .sparse-switch
        0x63 -> :sswitch_1
        0x6d -> :sswitch_2
    .end sparse-switch
.end method
