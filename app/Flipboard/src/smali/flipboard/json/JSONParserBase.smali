.class public abstract Lflipboard/json/JSONParserBase;
.super Ljava/lang/Object;
.source "JSONParserBase.java"


# static fields
.field public static a:Z

.field static g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static h:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static i:I

.field private static final j:Ljava/lang/Integer;


# instance fields
.field protected b:[B

.field protected c:I

.field protected d:I

.field protected e:I

.field protected f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    const/4 v0, 0x1

    sput-boolean v0, Lflipboard/json/JSONParserBase;->a:Z

    .line 20
    const v0, -0x35fdc00

    sput v0, Lflipboard/json/JSONParserBase;->i:I

    .line 21
    new-instance v0, Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    sput-object v0, Lflipboard/json/JSONParserBase;->j:Ljava/lang/Integer;

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lflipboard/json/JSONParserBase;->g:Ljava/util/Map;

    .line 31
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lflipboard/json/JSONParserBase;->h:Ljava/util/HashSet;

    return-void
.end method

.method protected constructor <init>([BII)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-virtual {p0, p1, p2, p3}, Lflipboard/json/JSONParserBase;->a([BII)V

    .line 37
    return-void
.end method

.method static a(Ljava/io/InputStream;)[B
    .locals 5

    .prologue
    const/16 v4, 0x1000

    const/4 v3, 0x0

    .line 41
    new-array v0, v4, [B

    .line 42
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 44
    :goto_0
    invoke-virtual {p0, v0, v3, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 45
    if-ltz v2, :cond_0

    .line 46
    invoke-virtual {v1, v0, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 50
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .locals 6

    .prologue
    .line 71
    iget v0, p0, Lflipboard/json/JSONParserBase;->c:I

    iget v1, p0, Lflipboard/json/JSONParserBase;->e:I

    add-int/lit8 v1, v1, -0x14

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 72
    iget v0, p0, Lflipboard/json/JSONParserBase;->e:I

    add-int/lit8 v0, v0, 0x1e

    iget v2, p0, Lflipboard/json/JSONParserBase;->d:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 73
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    if-lez v1, :cond_0

    const-string v0, "..."

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Lflipboard/json/JSONParserBase;->b:[B

    iget v5, p0, Lflipboard/json/JSONParserBase;->e:I

    sub-int/2addr v5, v1

    invoke-direct {v3, v4, v1, v5}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "^"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    iget-object v3, p0, Lflipboard/json/JSONParserBase;->b:[B

    iget v4, p0, Lflipboard/json/JSONParserBase;->e:I

    iget v5, p0, Lflipboard/json/JSONParserBase;->e:I

    sub-int v5, v2, v5

    invoke-direct {v1, v3, v4, v5}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, p0, Lflipboard/json/JSONParserBase;->e:I

    if-le v2, v0, :cond_1

    const-string v0, "..."

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method private b(I)Ljava/lang/String;
    .locals 13

    .prologue
    const v2, 0xfffd

    const/16 v12, 0xbd

    const/16 v11, -0x11

    const/16 v10, -0x41

    const/16 v9, 0x80

    .line 369
    iget v0, p0, Lflipboard/json/JSONParserBase;->e:I

    add-int/lit8 v0, v0, -0x1

    .line 371
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParserBase;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 450
    iget v1, p0, Lflipboard/json/JSONParserBase;->f:I

    and-int/lit16 v1, v1, 0xf0

    sparse-switch v1, :sswitch_data_1

    .line 544
    const/16 v1, 0x3f

    iput v1, p0, Lflipboard/json/JSONParserBase;->f:I

    move v1, v0

    .line 552
    :goto_1
    iget-object v3, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v0, v1, 0x1

    iget v4, p0, Lflipboard/json/JSONParserBase;->f:I

    int-to-byte v4, v4

    aput-byte v4, v3, v1

    .line 553
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    goto :goto_0

    .line 373
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    :goto_2
    move v1, v0

    .line 440
    goto :goto_1

    .line 376
    :sswitch_1
    invoke-direct {p0}, Lflipboard/json/JSONParserBase;->c()I

    move-result v3

    .line 378
    if-ge v3, v9, :cond_0

    .line 379
    iget-object v4, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v1, v0, 0x1

    int-to-byte v3, v3

    aput-byte v3, v4, v0

    move v0, v1

    goto :goto_0

    .line 382
    :cond_0
    const/16 v1, 0x800

    if-ge v3, v1, :cond_1

    .line 383
    iget-object v1, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v4, v0, 0x1

    shr-int/lit8 v5, v3, 0x6

    add-int/lit16 v5, v5, 0xc0

    int-to-byte v5, v5

    aput-byte v5, v1, v0

    .line 384
    iget-object v1, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v0, v4, 0x1

    and-int/lit8 v3, v3, 0x3f

    add-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    aput-byte v3, v1, v4

    goto :goto_0

    .line 387
    :cond_1
    const v1, 0xd800

    if-lt v3, v1, :cond_f

    const v1, 0xdbff

    if-gt v3, v1, :cond_f

    .line 388
    iget v1, p0, Lflipboard/json/JSONParserBase;->f:I

    const/16 v4, 0x5c

    if-eq v1, v4, :cond_3

    move v1, v2

    .line 417
    :goto_3
    invoke-static {v1}, Lflipboard/util/JavaUtil;->a(I)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 422
    :cond_2
    :goto_4
    const/high16 v3, 0x10000

    if-ge v1, v3, :cond_7

    .line 423
    iget-object v3, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v4, v0, 0x1

    shr-int/lit8 v5, v1, 0xc

    add-int/lit16 v5, v5, 0xe0

    int-to-byte v5, v5

    aput-byte v5, v3, v0

    .line 424
    iget-object v0, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v3, v4, 0x1

    shr-int/lit8 v5, v1, 0x6

    and-int/lit8 v5, v5, 0x3f

    add-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    aput-byte v5, v0, v4

    .line 425
    iget-object v4, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v0, v3, 0x1

    and-int/lit8 v1, v1, 0x3f

    add-int/lit16 v1, v1, 0x80

    int-to-byte v1, v1

    aput-byte v1, v4, v3

    goto/16 :goto_0

    .line 393
    :cond_3
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    .line 394
    iget v1, p0, Lflipboard/json/JSONParserBase;->f:I

    const/16 v4, 0x75

    if-eq v1, v4, :cond_4

    move v1, v2

    .line 396
    goto :goto_3

    .line 399
    :cond_4
    invoke-direct {p0}, Lflipboard/json/JSONParserBase;->c()I

    move-result v1

    .line 403
    const v4, 0xdc00

    if-lt v1, v4, :cond_5

    const v4, 0xdfff

    if-le v1, v4, :cond_6

    :cond_5
    move v1, v2

    .line 406
    goto :goto_3

    .line 412
    :cond_6
    shl-int/lit8 v3, v3, 0xa

    add-int/2addr v1, v3

    sget v3, Lflipboard/json/JSONParserBase;->i:I

    add-int/2addr v1, v3

    goto :goto_3

    .line 427
    :cond_7
    iget-object v3, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v4, v0, 0x1

    shr-int/lit8 v5, v1, 0x12

    add-int/lit16 v5, v5, 0xf0

    int-to-byte v5, v5

    aput-byte v5, v3, v0

    .line 428
    iget-object v0, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v3, v4, 0x1

    shr-int/lit8 v5, v1, 0xc

    and-int/lit8 v5, v5, 0x3f

    add-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    aput-byte v5, v0, v4

    .line 429
    iget-object v0, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v4, v3, 0x1

    shr-int/lit8 v5, v1, 0x6

    and-int/lit8 v5, v5, 0x3f

    add-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    aput-byte v5, v0, v3

    .line 430
    iget-object v3, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v0, v4, 0x1

    and-int/lit8 v1, v1, 0x3f

    add-int/lit16 v1, v1, 0x80

    int-to-byte v1, v1

    aput-byte v1, v3, v4

    goto/16 :goto_0

    .line 435
    :sswitch_2
    const/16 v1, 0xd

    iput v1, p0, Lflipboard/json/JSONParserBase;->f:I

    move v1, v0

    goto/16 :goto_1

    .line 436
    :sswitch_3
    const/16 v1, 0xa

    iput v1, p0, Lflipboard/json/JSONParserBase;->f:I

    move v1, v0

    goto/16 :goto_1

    .line 437
    :sswitch_4
    const/16 v1, 0x9

    iput v1, p0, Lflipboard/json/JSONParserBase;->f:I

    move v1, v0

    goto/16 :goto_1

    .line 438
    :sswitch_5
    const/16 v1, 0x8

    iput v1, p0, Lflipboard/json/JSONParserBase;->f:I

    move v1, v0

    goto/16 :goto_1

    .line 439
    :sswitch_6
    const/16 v1, 0xc

    iput v1, p0, Lflipboard/json/JSONParserBase;->f:I

    goto/16 :goto_2

    .line 446
    :sswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    .line 447
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lflipboard/json/JSONParserBase;->b:[B

    sub-int/2addr v0, p1

    const-string v3, "UTF-8"

    invoke-direct {v1, v2, p1, v0, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    return-object v1

    :sswitch_8
    move v1, v0

    .line 452
    goto/16 :goto_1

    :sswitch_9
    move v1, v0

    .line 461
    goto/16 :goto_1

    .line 467
    :sswitch_a
    const/16 v1, 0x3f

    iput v1, p0, Lflipboard/json/JSONParserBase;->f:I

    move v1, v0

    .line 468
    goto/16 :goto_1

    .line 472
    :sswitch_b
    iget v3, p0, Lflipboard/json/JSONParserBase;->f:I

    .line 473
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    move-result v1

    .line 474
    and-int/lit16 v1, v1, 0xc0

    if-eq v1, v9, :cond_8

    .line 476
    const/16 v1, 0x3f

    iput v1, p0, Lflipboard/json/JSONParserBase;->f:I

    move v1, v0

    goto/16 :goto_1

    .line 478
    :cond_8
    iget-object v4, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v1, v0, 0x1

    int-to-byte v3, v3

    aput-byte v3, v4, v0

    goto/16 :goto_1

    .line 485
    :sswitch_c
    iget v1, p0, Lflipboard/json/JSONParserBase;->f:I

    .line 486
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    move-result v3

    .line 487
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    move-result v4

    .line 488
    and-int/lit16 v5, v3, 0xc0

    if-ne v5, v9, :cond_9

    and-int/lit16 v5, v4, 0xc0

    if-eq v5, v9, :cond_a

    .line 491
    :cond_9
    iget-object v1, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v3, v0, 0x1

    aput-byte v11, v1, v0

    .line 492
    iget-object v1, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v0, v3, 0x1

    aput-byte v10, v1, v3

    .line 493
    iput v12, p0, Lflipboard/json/JSONParserBase;->f:I

    move v1, v0

    goto/16 :goto_1

    .line 497
    :cond_a
    shl-int/lit8 v5, v1, 0xc

    const v6, 0xffff

    and-int/2addr v5, v6

    shl-int/lit8 v6, v3, 0x6

    and-int/lit16 v6, v6, 0xfff

    add-int/2addr v5, v6

    and-int/lit8 v4, v4, 0x3f

    add-int/2addr v4, v5

    .line 499
    invoke-static {v4}, Lflipboard/util/JavaUtil;->a(I)Z

    move-result v4

    if-nez v4, :cond_b

    .line 501
    iget-object v1, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v3, v0, 0x1

    aput-byte v11, v1, v0

    .line 502
    iget-object v1, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v0, v3, 0x1

    aput-byte v10, v1, v3

    .line 503
    iput v12, p0, Lflipboard/json/JSONParserBase;->f:I

    move v1, v0

    goto/16 :goto_1

    .line 505
    :cond_b
    iget-object v4, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v5, v0, 0x1

    int-to-byte v1, v1

    aput-byte v1, v4, v0

    .line 506
    iget-object v1, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v0, v5, 0x1

    int-to-byte v3, v3

    aput-byte v3, v1, v5

    move v1, v0

    .line 510
    goto/16 :goto_1

    .line 514
    :sswitch_d
    iget v1, p0, Lflipboard/json/JSONParserBase;->f:I

    .line 515
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    move-result v3

    .line 516
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    move-result v4

    .line 517
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    move-result v5

    .line 518
    and-int/lit16 v6, v1, 0xf8

    const/16 v7, 0xf0

    if-ne v6, v7, :cond_c

    and-int/lit16 v6, v3, 0xc0

    if-ne v6, v9, :cond_c

    and-int/lit16 v6, v4, 0xc0

    if-ne v6, v9, :cond_c

    and-int/lit16 v6, v5, 0xc0

    if-eq v6, v9, :cond_d

    .line 521
    :cond_c
    iget-object v1, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v3, v0, 0x1

    aput-byte v11, v1, v0

    .line 522
    iget-object v1, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v0, v3, 0x1

    aput-byte v10, v1, v3

    .line 523
    iput v12, p0, Lflipboard/json/JSONParserBase;->f:I

    move v1, v0

    goto/16 :goto_1

    .line 526
    :cond_d
    shl-int/lit8 v6, v1, 0x12

    const v7, 0x1fffff

    and-int/2addr v6, v7

    shl-int/lit8 v7, v3, 0xc

    const v8, 0x3ffff

    and-int/2addr v7, v8

    add-int/2addr v6, v7

    shl-int/lit8 v7, v4, 0x6

    and-int/lit16 v7, v7, 0xfff

    add-int/2addr v6, v7

    and-int/lit8 v5, v5, 0x3f

    add-int/2addr v5, v6

    .line 528
    invoke-static {v5}, Lflipboard/util/JavaUtil;->a(I)Z

    move-result v5

    if-nez v5, :cond_e

    .line 530
    iget-object v1, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v3, v0, 0x1

    aput-byte v11, v1, v0

    .line 531
    iget-object v1, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v0, v3, 0x1

    aput-byte v10, v1, v3

    .line 532
    iput v12, p0, Lflipboard/json/JSONParserBase;->f:I

    move v1, v0

    goto/16 :goto_1

    .line 534
    :cond_e
    iget-object v5, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v6, v0, 0x1

    int-to-byte v1, v1

    aput-byte v1, v5, v0

    .line 535
    iget-object v0, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v1, v6, 0x1

    int-to-byte v3, v3

    aput-byte v3, v0, v6

    .line 536
    iget-object v3, p0, Lflipboard/json/JSONParserBase;->b:[B

    add-int/lit8 v0, v1, 0x1

    int-to-byte v4, v4

    aput-byte v4, v3, v1

    move v1, v0

    .line 540
    goto/16 :goto_1

    .line 549
    :sswitch_e
    new-instance v0, Ljava/io/IOException;

    const-string v1, "invalid JSON, unexpected EOF in string"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    move v1, v3

    goto/16 :goto_4

    .line 371
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_e
        0x22 -> :sswitch_7
        0x5c -> :sswitch_0
    .end sparse-switch

    .line 450
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_8
        0x10 -> :sswitch_9
        0x20 -> :sswitch_9
        0x30 -> :sswitch_9
        0x40 -> :sswitch_9
        0x50 -> :sswitch_9
        0x60 -> :sswitch_9
        0x70 -> :sswitch_9
        0x80 -> :sswitch_a
        0x90 -> :sswitch_a
        0xa0 -> :sswitch_a
        0xb0 -> :sswitch_a
        0xc0 -> :sswitch_b
        0xd0 -> :sswitch_b
        0xe0 -> :sswitch_c
        0xf0 -> :sswitch_d
    .end sparse-switch

    .line 373
    :sswitch_data_2
    .sparse-switch
        0x62 -> :sswitch_5
        0x66 -> :sswitch_6
        0x6e -> :sswitch_3
        0x72 -> :sswitch_2
        0x74 -> :sswitch_4
        0x75 -> :sswitch_1
    .end sparse-switch
.end method

.method private c()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 348
    move v1, v0

    .line 349
    :goto_0
    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    .line 350
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 349
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 353
    :sswitch_0
    shl-int/lit8 v0, v0, 0x4

    iget v2, p0, Lflipboard/json/JSONParserBase;->f:I

    add-int/lit8 v2, v2, -0x30

    add-int/2addr v0, v2

    .line 354
    goto :goto_1

    .line 356
    :sswitch_1
    shl-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0xa

    iget v2, p0, Lflipboard/json/JSONParserBase;->f:I

    add-int/lit8 v2, v2, -0x61

    add-int/2addr v0, v2

    .line 357
    goto :goto_1

    .line 359
    :sswitch_2
    shl-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0xa

    iget v2, p0, Lflipboard/json/JSONParserBase;->f:I

    add-int/lit8 v2, v2, -0x41

    add-int/2addr v0, v2

    goto :goto_1

    .line 363
    :cond_0
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    .line 364
    return v0

    .line 350
    nop

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x31 -> :sswitch_0
        0x32 -> :sswitch_0
        0x33 -> :sswitch_0
        0x34 -> :sswitch_0
        0x35 -> :sswitch_0
        0x36 -> :sswitch_0
        0x37 -> :sswitch_0
        0x38 -> :sswitch_0
        0x39 -> :sswitch_0
        0x41 -> :sswitch_2
        0x42 -> :sswitch_2
        0x43 -> :sswitch_2
        0x44 -> :sswitch_2
        0x45 -> :sswitch_2
        0x46 -> :sswitch_2
        0x61 -> :sswitch_1
        0x62 -> :sswitch_1
        0x63 -> :sswitch_1
        0x64 -> :sswitch_1
        0x65 -> :sswitch_1
        0x66 -> :sswitch_1
    .end sparse-switch
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 679
    move v0, v1

    move v2, v1

    .line 682
    :goto_0
    iget v3, p0, Lflipboard/json/JSONParserBase;->f:I

    sparse-switch v3, :sswitch_data_0

    .line 714
    :cond_0
    :goto_1
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    goto :goto_0

    .line 685
    :sswitch_1
    add-int/lit8 v2, v2, 0x1

    .line 686
    goto :goto_1

    .line 689
    :sswitch_2
    if-nez v2, :cond_1

    if-nez v0, :cond_1

    .line 709
    :goto_2
    return-void

    .line 692
    :cond_1
    add-int/lit8 v2, v2, -0x1

    .line 693
    goto :goto_1

    .line 695
    :sswitch_3
    if-eqz v0, :cond_0

    .line 696
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    goto :goto_1

    .line 700
    :sswitch_4
    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1

    .line 708
    :sswitch_5
    if-nez v2, :cond_0

    if-nez v0, :cond_0

    goto :goto_2

    .line 682
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x22 -> :sswitch_4
        0x2c -> :sswitch_5
        0x5b -> :sswitch_1
        0x5c -> :sswitch_3
        0x5d -> :sswitch_2
        0x7b -> :sswitch_1
        0x7d -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method protected final E()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 55
    iget v1, p0, Lflipboard/json/JSONParserBase;->f:I

    if-ne v1, v0, :cond_0

    .line 56
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 58
    :cond_0
    iget v1, p0, Lflipboard/json/JSONParserBase;->e:I

    iget v2, p0, Lflipboard/json/JSONParserBase;->d:I

    if-lt v1, v2, :cond_1

    :goto_0
    iput v0, p0, Lflipboard/json/JSONParserBase;->f:I

    .line 66
    iget v0, p0, Lflipboard/json/JSONParserBase;->f:I

    return v0

    .line 58
    :cond_1
    iget-object v0, p0, Lflipboard/json/JSONParserBase;->b:[B

    iget v1, p0, Lflipboard/json/JSONParserBase;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lflipboard/json/JSONParserBase;->e:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method protected final F()V
    .locals 3

    .prologue
    .line 78
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid input near: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lflipboard/json/JSONParserBase;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final G()Z
    .locals 1

    .prologue
    .line 116
    :goto_0
    :sswitch_0
    iget v0, p0, Lflipboard/json/JSONParserBase;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 139
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->F()V

    goto :goto_0

    .line 123
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    .line 124
    const/4 v0, 0x1

    .line 132
    :goto_1
    return v0

    .line 131
    :sswitch_2
    const-string v0, "null"

    invoke-virtual {p0, v0}, Lflipboard/json/JSONParserBase;->a(Ljava/lang/String;)V

    .line 132
    const/4 v0, 0x0

    goto :goto_1

    .line 116
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x6e -> :sswitch_2
        0x7b -> :sswitch_1
    .end sparse-switch
.end method

.method protected final H()Z
    .locals 2

    .prologue
    .line 170
    iget v0, p0, Lflipboard/json/JSONParserBase;->f:I

    const/16 v1, 0x22

    if-eq v0, v1, :cond_0

    .line 171
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->F()V

    .line 174
    :cond_0
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->I()Z

    move-result v0

    return v0
.end method

.method protected final I()Z
    .locals 1

    .prologue
    .line 179
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 187
    const/4 v0, 0x1

    return v0

    .line 179
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x3a -> :sswitch_0
    .end sparse-switch
.end method

.method protected final J()Z
    .locals 1

    .prologue
    .line 195
    :goto_0
    :sswitch_0
    iget v0, p0, Lflipboard/json/JSONParserBase;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 208
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->F()V

    goto :goto_0

    .line 197
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    .line 198
    const/4 v0, 0x1

    .line 206
    :goto_1
    return v0

    .line 205
    :sswitch_2
    const-string v0, "null"

    invoke-virtual {p0, v0}, Lflipboard/json/JSONParserBase;->a(Ljava/lang/String;)V

    .line 206
    const/4 v0, 0x0

    goto :goto_1

    .line 195
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x5b -> :sswitch_1
        0x6e -> :sswitch_2
    .end sparse-switch
.end method

.method protected final K()Z
    .locals 1

    .prologue
    .line 216
    :goto_0
    :sswitch_0
    iget v0, p0, Lflipboard/json/JSONParserBase;->f:I

    sparse-switch v0, :sswitch_data_0

    .line 229
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->F()V

    goto :goto_0

    .line 218
    :sswitch_1
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    .line 219
    const/4 v0, 0x1

    .line 227
    :goto_1
    return v0

    .line 226
    :sswitch_2
    const-string v0, "null"

    invoke-virtual {p0, v0}, Lflipboard/json/JSONParserBase;->a(Ljava/lang/String;)V

    .line 227
    const/4 v0, 0x0

    goto :goto_1

    .line 216
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x6e -> :sswitch_2
        0x7b -> :sswitch_1
    .end sparse-switch
.end method

.method public final L()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 261
    iget v2, p0, Lflipboard/json/JSONParserBase;->f:I

    sparse-switch v2, :sswitch_data_0

    .line 269
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->N()J

    move-result-wide v2

    long-to-int v2, v2

    .line 270
    if-eqz v2, :cond_0

    :goto_0
    return v0

    .line 263
    :sswitch_0
    const-string v1, "true"

    invoke-virtual {p0, v1}, Lflipboard/json/JSONParserBase;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 266
    :sswitch_1
    const-string v0, "false"

    invoke-virtual {p0, v0}, Lflipboard/json/JSONParserBase;->a(Ljava/lang/String;)V

    move v0, v1

    .line 267
    goto :goto_0

    :cond_0
    move v0, v1

    .line 270
    goto :goto_0

    .line 261
    :sswitch_data_0
    .sparse-switch
        0x66 -> :sswitch_1
        0x74 -> :sswitch_0
    .end sparse-switch
.end method

.method public final M()I
    .locals 2

    .prologue
    .line 275
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->N()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final N()J
    .locals 6

    .prologue
    .line 279
    const-wide/16 v2, 0x0

    .line 280
    const/4 v0, 0x0

    .line 282
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParserBase;->f:I

    packed-switch v1, :pswitch_data_0

    .line 291
    :pswitch_0
    if-eqz v0, :cond_0

    neg-long v2, v2

    :cond_0
    return-wide v2

    .line 284
    :pswitch_1
    const/4 v0, 0x1

    .line 293
    :goto_1
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    goto :goto_0

    .line 288
    :pswitch_2
    const-wide/16 v4, 0xa

    mul-long/2addr v2, v4

    iget v1, p0, Lflipboard/json/JSONParserBase;->f:I

    add-int/lit8 v1, v1, -0x30

    int-to-long v4, v1

    add-long/2addr v2, v4

    .line 289
    goto :goto_1

    .line 282
    nop

    :pswitch_data_0
    .packed-switch 0x2d
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final O()F
    .locals 2

    .prologue
    .line 298
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->P()D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public final P()D
    .locals 4

    .prologue
    .line 302
    iget v0, p0, Lflipboard/json/JSONParserBase;->e:I

    add-int/lit8 v0, v0, -0x1

    .line 304
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParserBase;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 310
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lflipboard/json/JSONParserBase;->b:[B

    iget v3, p0, Lflipboard/json/JSONParserBase;->e:I

    add-int/lit8 v3, v3, -0x1

    sub-int/2addr v3, v0

    invoke-direct {v1, v2, v0, v3}, Ljava/lang/String;-><init>([BII)V

    invoke-static {v1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    return-wide v0

    .line 312
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    goto :goto_0

    .line 304
    :sswitch_data_0
    .sparse-switch
        0x2b -> :sswitch_0
        0x2d -> :sswitch_0
        0x2e -> :sswitch_0
        0x30 -> :sswitch_0
        0x31 -> :sswitch_0
        0x32 -> :sswitch_0
        0x33 -> :sswitch_0
        0x34 -> :sswitch_0
        0x35 -> :sswitch_0
        0x36 -> :sswitch_0
        0x37 -> :sswitch_0
        0x38 -> :sswitch_0
        0x39 -> :sswitch_0
        0x45 -> :sswitch_0
        0x65 -> :sswitch_0
    .end sparse-switch
.end method

.method public final Q()Lflipboard/json/FLObject;
    .locals 1

    .prologue
    .line 559
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->R()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/json/FLObject;

    return-object v0
.end method

.method public final R()Ljava/lang/Object;
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    const/16 v7, 0x2d

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 564
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParserBase;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 666
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid JSON, near \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lflipboard/json/JSONParserBase;->f:I

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lflipboard/json/JSONParserBase;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 566
    :sswitch_0
    const-string v1, "null"

    invoke-virtual {p0, v1}, Lflipboard/json/JSONParserBase;->a(Ljava/lang/String;)V

    .line 650
    :goto_1
    return-object v0

    .line 570
    :sswitch_1
    const-string v0, "true"

    invoke-virtual {p0, v0}, Lflipboard/json/JSONParserBase;->a(Ljava/lang/String;)V

    .line 571
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 574
    :sswitch_2
    const-string v0, "false"

    invoke-virtual {p0, v0}, Lflipboard/json/JSONParserBase;->a(Ljava/lang/String;)V

    .line 575
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 578
    :sswitch_3
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 583
    :sswitch_4
    iget v0, p0, Lflipboard/json/JSONParserBase;->f:I

    if-eq v0, v7, :cond_0

    iget v0, p0, Lflipboard/json/JSONParserBase;->f:I

    const/16 v1, 0x2b

    if-ne v0, v1, :cond_1

    :cond_0
    move-wide v0, v2

    .line 584
    :goto_2
    iget v4, p0, Lflipboard/json/JSONParserBase;->f:I

    if-ne v4, v7, :cond_2

    move v4, v5

    .line 585
    :goto_3
    iget v7, p0, Lflipboard/json/JSONParserBase;->e:I

    add-int/lit8 v7, v7, -0x1

    .line 588
    :goto_4
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    move-result v8

    sparse-switch v8, :sswitch_data_1

    .line 597
    iget v5, p0, Lflipboard/json/JSONParserBase;->e:I

    add-int/lit8 v5, v5, -0x1

    sub-int v8, v5, v7

    .line 598
    if-eqz v6, :cond_3

    .line 599
    new-instance v0, Ljava/lang/Double;

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lflipboard/json/JSONParserBase;->b:[B

    const-string v3, "ASCII"

    invoke-direct {v1, v2, v7, v8, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Double;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 583
    :cond_1
    iget v0, p0, Lflipboard/json/JSONParserBase;->f:I

    add-int/lit8 v0, v0, -0x30

    int-to-long v0, v0

    goto :goto_2

    :cond_2
    move v4, v6

    .line 584
    goto :goto_3

    :sswitch_5
    move v6, v5

    .line 591
    goto :goto_4

    .line 594
    :sswitch_6
    const-wide/16 v8, 0xa

    mul-long/2addr v0, v8

    iget v8, p0, Lflipboard/json/JSONParserBase;->f:I

    int-to-long v8, v8

    add-long/2addr v0, v8

    const-wide/16 v8, 0x30

    sub-long/2addr v0, v8

    .line 595
    goto :goto_4

    .line 601
    :cond_3
    if-eqz v4, :cond_6

    .line 602
    neg-long v0, v0

    move-wide v4, v0

    .line 604
    :goto_5
    const/16 v0, 0xa

    if-lt v8, v0, :cond_4

    .line 605
    new-instance v0, Ljava/lang/Long;

    invoke-direct {v0, v4, v5}, Ljava/lang/Long;-><init>(J)V

    goto :goto_1

    .line 607
    :cond_4
    cmp-long v0, v4, v2

    if-nez v0, :cond_5

    sget-object v0, Lflipboard/json/JSONParserBase;->j:Ljava/lang/Integer;

    goto :goto_1

    :cond_5
    new-instance v0, Ljava/lang/Integer;

    long-to-int v1, v4

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    goto :goto_1

    .line 613
    :sswitch_7
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    .line 614
    new-instance v1, Lflipboard/json/FLObject;

    invoke-direct {v1}, Lflipboard/json/FLObject;-><init>()V

    .line 616
    :goto_6
    iget v2, p0, Lflipboard/json/JSONParserBase;->f:I

    sparse-switch v2, :sswitch_data_2

    .line 633
    new-instance v0, Ljava/io/IOException;

    const-string v1, "invalid JSON"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 618
    :sswitch_8
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 621
    :sswitch_9
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    goto :goto_6

    .line 624
    :sswitch_a
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    .line 625
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->R()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    .line 628
    :sswitch_b
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    move-object v0, v1

    .line 629
    goto/16 :goto_1

    .line 631
    :sswitch_c
    new-instance v0, Ljava/io/IOException;

    const-string v1, "invalid JSON, unexpected EOF in string"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 640
    :sswitch_d
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    .line 642
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 644
    :goto_7
    iget v1, p0, Lflipboard/json/JSONParserBase;->f:I

    sparse-switch v1, :sswitch_data_3

    .line 654
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->R()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 646
    :sswitch_e
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    goto :goto_7

    .line 649
    :sswitch_f
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    goto/16 :goto_1

    .line 652
    :sswitch_10
    new-instance v0, Ljava/io/IOException;

    const-string v1, "invalid JSON, unexpected EOF in array"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 661
    :sswitch_11
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    goto/16 :goto_0

    .line 664
    :sswitch_12
    new-instance v0, Ljava/io/IOException;

    const-string v1, "invalid JSON, unexpected EOF"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move-wide v4, v0

    goto/16 :goto_5

    .line 564
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_12
        0x9 -> :sswitch_11
        0xa -> :sswitch_11
        0xc -> :sswitch_11
        0xd -> :sswitch_11
        0x20 -> :sswitch_11
        0x22 -> :sswitch_3
        0x2b -> :sswitch_4
        0x2d -> :sswitch_4
        0x30 -> :sswitch_4
        0x31 -> :sswitch_4
        0x32 -> :sswitch_4
        0x33 -> :sswitch_4
        0x34 -> :sswitch_4
        0x35 -> :sswitch_4
        0x36 -> :sswitch_4
        0x37 -> :sswitch_4
        0x38 -> :sswitch_4
        0x39 -> :sswitch_4
        0x5b -> :sswitch_d
        0x66 -> :sswitch_2
        0x6e -> :sswitch_0
        0x74 -> :sswitch_1
        0x7b -> :sswitch_7
    .end sparse-switch

    .line 588
    :sswitch_data_1
    .sparse-switch
        0x2b -> :sswitch_5
        0x2d -> :sswitch_5
        0x2e -> :sswitch_5
        0x30 -> :sswitch_6
        0x31 -> :sswitch_6
        0x32 -> :sswitch_6
        0x33 -> :sswitch_6
        0x34 -> :sswitch_6
        0x35 -> :sswitch_6
        0x36 -> :sswitch_6
        0x37 -> :sswitch_6
        0x38 -> :sswitch_6
        0x39 -> :sswitch_6
        0x45 -> :sswitch_5
        0x65 -> :sswitch_5
    .end sparse-switch

    .line 616
    :sswitch_data_2
    .sparse-switch
        -0x1 -> :sswitch_c
        0x9 -> :sswitch_9
        0xa -> :sswitch_9
        0xc -> :sswitch_9
        0xd -> :sswitch_9
        0x20 -> :sswitch_9
        0x22 -> :sswitch_8
        0x2c -> :sswitch_9
        0x3a -> :sswitch_a
        0x7d -> :sswitch_b
    .end sparse-switch

    .line 644
    :sswitch_data_3
    .sparse-switch
        -0x1 -> :sswitch_10
        0x9 -> :sswitch_e
        0xa -> :sswitch_e
        0xc -> :sswitch_e
        0xd -> :sswitch_e
        0x20 -> :sswitch_e
        0x2c -> :sswitch_e
        0x5d -> :sswitch_f
    .end sparse-switch
.end method

.method protected final S()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 674
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->R()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 318
    iget v0, p0, Lflipboard/json/JSONParserBase;->f:I

    const/16 v1, 0x6e

    if-ne v0, v1, :cond_0

    .line 319
    const-string v0, "null"

    invoke-virtual {p0, v0}, Lflipboard/json/JSONParserBase;->a(Ljava/lang/String;)V

    .line 320
    const/4 v0, 0x0

    .line 326
    :goto_0
    return-object v0

    .line 322
    :cond_0
    iget v0, p0, Lflipboard/json/JSONParserBase;->f:I

    const/16 v1, 0x22

    if-eq v0, v1, :cond_1

    .line 323
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->F()V

    .line 325
    :cond_1
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    .line 326
    iget v0, p0, Lflipboard/json/JSONParserBase;->e:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lflipboard/json/JSONParserBase;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 331
    :goto_0
    iget v0, p0, Lflipboard/json/JSONParserBase;->f:I

    const/16 v1, 0x22

    if-ne v0, v1, :cond_0

    .line 332
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    .line 333
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lflipboard/json/JSONParserBase;->b:[B

    iget v2, p0, Lflipboard/json/JSONParserBase;->e:I

    sub-int/2addr v2, p1

    add-int/lit8 v2, v2, -0x2

    const-string v3, "ascii"

    invoke-direct {v0, v1, p1, v2, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 340
    :goto_1
    return-object v0

    .line 337
    :cond_0
    iget v0, p0, Lflipboard/json/JSONParserBase;->f:I

    and-int/lit16 v0, v0, 0x80

    if-nez v0, :cond_1

    iget v0, p0, Lflipboard/json/JSONParserBase;->f:I

    const/16 v1, 0x5c

    if-ne v0, v1, :cond_2

    .line 338
    :cond_1
    invoke-direct {p0, p1}, Lflipboard/json/JSONParserBase;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 342
    :cond_2
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    goto :goto_0
.end method

.method final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 236
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 237
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    move-result v1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v1, v2, :cond_0

    .line 238
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->F()V

    .line 236
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 241
    :cond_1
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    .line 242
    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 83
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    :goto_0
    iget v1, p0, Lflipboard/json/JSONParserBase;->f:I

    const/16 v2, 0x22

    if-eq v1, v2, :cond_0

    .line 85
    iget v1, p0, Lflipboard/json/JSONParserBase;->f:I

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 86
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    goto :goto_0

    .line 88
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 89
    sget-boolean v1, Lflipboard/json/JSONParserBase;->a:Z

    if-eqz v1, :cond_2

    .line 90
    sget-object v1, Lflipboard/json/JSONParserBase;->h:Ljava/util/HashSet;

    monitor-enter v1

    .line 91
    :try_start_0
    sget-object v2, Lflipboard/json/JSONParserBase;->h:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 93
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v3, 0x2

    invoke-direct {p0}, Lflipboard/json/JSONParserBase;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 95
    :cond_1
    sget-object v2, Lflipboard/json/JSONParserBase;->h:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 96
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    :cond_2
    :sswitch_0
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 107
    invoke-direct {p0}, Lflipboard/json/JSONParserBase;->d()V

    .line 108
    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 99
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x3a -> :sswitch_0
    .end sparse-switch
.end method

.method public final a([BII)V
    .locals 1

    .prologue
    .line 246
    iput-object p1, p0, Lflipboard/json/JSONParserBase;->b:[B

    .line 247
    iput p2, p0, Lflipboard/json/JSONParserBase;->c:I

    .line 248
    add-int v0, p2, p3

    iput v0, p0, Lflipboard/json/JSONParserBase;->d:I

    .line 249
    iput p2, p0, Lflipboard/json/JSONParserBase;->e:I

    .line 250
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/json/JSONParserBase;->f:I

    .line 251
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    move-result v0

    iput v0, p0, Lflipboard/json/JSONParserBase;->f:I

    .line 252
    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 145
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    :goto_0
    if-ge p3, v1, :cond_1

    .line 146
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    move-result v2

    invoke-virtual {p2, p3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v2, v3, :cond_0

    .line 147
    invoke-virtual {p2, v0, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lflipboard/json/JSONParserBase;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :goto_1
    return v0

    .line 145
    :cond_0
    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    .line 151
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method protected final b(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 155
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    :goto_0
    if-ge p3, v1, :cond_1

    .line 156
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    move-result v2

    invoke-virtual {p2, p3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v2, v3, :cond_0

    .line 157
    invoke-virtual {p2, v0, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lflipboard/json/JSONParserBase;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :goto_1
    return v0

    .line 155
    :cond_0
    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    .line 162
    :cond_1
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->E()I

    move-result v1

    const/16 v2, 0x22

    if-eq v1, v2, :cond_2

    .line 163
    invoke-virtual {p0, p1, p2}, Lflipboard/json/JSONParserBase;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 166
    :cond_2
    invoke-virtual {p0}, Lflipboard/json/JSONParserBase;->H()Z

    move-result v0

    goto :goto_1
.end method
