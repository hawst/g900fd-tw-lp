.class public abstract Lflipboard/json/JSONSerializerBase;
.super Ljava/lang/Object;
.source "JSONSerializerBase.java"


# static fields
.field protected static final a:[B

.field protected static final b:[B

.field protected static final c:[B

.field protected static final d:[C


# instance fields
.field final e:Lflipboard/io/UTF8StreamWriter;

.field final f:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-string v0, "null"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lflipboard/json/JSONSerializerBase;->a:[B

    .line 20
    const-string v0, "true"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lflipboard/json/JSONSerializerBase;->b:[B

    .line 21
    const-string v0, "false"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lflipboard/json/JSONSerializerBase;->c:[B

    .line 22
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lflipboard/json/JSONSerializerBase;->d:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method protected constructor <init>(Lflipboard/io/UTF8StreamWriter;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/16 v0, 0x40

    new-array v0, v0, [B

    iput-object v0, p0, Lflipboard/json/JSONSerializerBase;->f:[B

    .line 29
    iput-object p1, p0, Lflipboard/json/JSONSerializerBase;->e:Lflipboard/io/UTF8StreamWriter;

    .line 30
    return-void
.end method

.method public static c(Ljava/lang/Object;)[B
    .locals 2

    .prologue
    .line 243
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 244
    new-instance v1, Lflipboard/json/JSONSerializer;

    invoke-direct {v1, v0}, Lflipboard/json/JSONSerializer;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v1, p0}, Lflipboard/json/JSONSerializer;->b(Ljava/lang/Object;)V

    .line 245
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 248
    :goto_0
    return-object v0

    .line 246
    :catch_0
    move-exception v0

    .line 247
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 248
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 253
    new-instance v0, Ljava/lang/String;

    invoke-static {p0}, Lflipboard/json/JSONSerializerBase;->c(Ljava/lang/Object;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    return-object v0
.end method


# virtual methods
.method final a(C)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lflipboard/json/JSONSerializerBase;->e:Lflipboard/io/UTF8StreamWriter;

    iget-object v0, v0, Lflipboard/io/UTF8StreamWriter;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 36
    return-void
.end method

.method final a(D)V
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lflipboard/json/JSONSerializerBase;->e:Lflipboard/io/UTF8StreamWriter;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/io/UTF8StreamWriter;->write(Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method final a(I)V
    .locals 2

    .prologue
    .line 63
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializerBase;->a(J)V

    .line 64
    return-void
.end method

.method final a(ILjava/lang/String;)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 46
    if-lez p1, :cond_0

    .line 47
    const/16 v0, 0x2c

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 49
    :cond_0
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 50
    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    .line 51
    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_1
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 54
    const/16 v0, 0x3a

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 55
    return-void
.end method

.method final a(J)V
    .locals 11

    .prologue
    const-wide/16 v8, 0xa

    const-wide/16 v6, 0x0

    .line 67
    cmp-long v0, p1, v6

    if-nez v0, :cond_0

    .line 68
    const/16 v0, 0x30

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 80
    :goto_0
    return-void

    .line 71
    :cond_0
    cmp-long v0, p1, v6

    if-gez v0, :cond_1

    .line 72
    const/16 v0, 0x2d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 73
    neg-long p1, p1

    .line 75
    :cond_1
    iget-object v0, p0, Lflipboard/json/JSONSerializerBase;->f:[B

    array-length v0, v0

    .line 76
    :goto_1
    cmp-long v1, p1, v6

    if-lez v1, :cond_2

    .line 77
    iget-object v1, p0, Lflipboard/json/JSONSerializerBase;->f:[B

    add-int/lit8 v0, v0, -0x1

    sget-object v2, Lflipboard/json/JSONSerializerBase;->d:[C

    rem-long v4, p1, v8

    long-to-int v3, v4

    aget-char v2, v2, v3

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 76
    div-long/2addr p1, v8

    goto :goto_1

    .line 79
    :cond_2
    iget-object v1, p0, Lflipboard/json/JSONSerializerBase;->e:Lflipboard/io/UTF8StreamWriter;

    iget-object v2, p0, Lflipboard/json/JSONSerializerBase;->f:[B

    iget-object v3, p0, Lflipboard/json/JSONSerializerBase;->f:[B

    array-length v3, v3

    sub-int/2addr v3, v0

    invoke-virtual {v1, v2, v0, v3}, Lflipboard/io/UTF8StreamWriter;->a([BII)V

    goto :goto_0
.end method

.method public final a(Lflipboard/json/FLObject;)V
    .locals 4

    .prologue
    .line 96
    if-nez p1, :cond_0

    .line 97
    sget-object v0, Lflipboard/json/JSONSerializerBase;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializerBase;->a([B)V

    .line 114
    :goto_0
    return-void

    .line 99
    :cond_0
    const/4 v0, 0x0

    .line 100
    const/16 v1, 0x7b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 101
    invoke-virtual {p1}, Lflipboard/json/FLObject;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 102
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_1

    .line 103
    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 108
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializerBase;->a(Ljava/lang/String;)V

    .line 109
    const/16 v1, 0x3a

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 110
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializerBase;->b(Ljava/lang/Object;)V

    move v1, v2

    .line 111
    goto :goto_1

    .line 112
    :cond_2
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializerBase;->a(C)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 237
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "can\'t output as json: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/16 v5, 0x22

    const/16 v4, 0x5c

    .line 121
    if-nez p1, :cond_0

    .line 122
    sget-object v0, Lflipboard/json/JSONSerializerBase;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializerBase;->a([B)V

    .line 152
    :goto_0
    return-void

    .line 125
    :cond_0
    invoke-virtual {p0, v5}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 126
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 127
    invoke-virtual {p1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v1

    .line 128
    sparse-switch v1, :sswitch_data_0

    .line 137
    const/16 v2, 0x20

    if-ge v1, v2, :cond_1

    .line 138
    invoke-virtual {p0, v4}, Lflipboard/json/JSONSerializerBase;->a(C)V

    const/16 v2, 0x75

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 139
    sget-object v2, Lflipboard/json/JSONSerializerBase;->d:[C

    shr-int/lit8 v3, v1, 0xc

    and-int/lit8 v3, v3, 0xf

    aget-char v2, v2, v3

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 140
    sget-object v2, Lflipboard/json/JSONSerializerBase;->d:[C

    shr-int/lit8 v3, v1, 0x8

    and-int/lit8 v3, v3, 0xf

    aget-char v2, v2, v3

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 141
    sget-object v2, Lflipboard/json/JSONSerializerBase;->d:[C

    shr-int/lit8 v3, v1, 0x4

    and-int/lit8 v3, v3, 0xf

    aget-char v2, v2, v3

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 142
    sget-object v2, Lflipboard/json/JSONSerializerBase;->d:[C

    and-int/lit8 v3, v1, 0xf

    aget-char v2, v2, v3

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 148
    :goto_2
    invoke-static {v1}, Ljava/lang/Character;->charCount(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    goto :goto_1

    .line 129
    :sswitch_0
    invoke-virtual {p0, v4}, Lflipboard/json/JSONSerializerBase;->a(C)V

    invoke-virtual {p0, v4}, Lflipboard/json/JSONSerializerBase;->a(C)V

    goto :goto_2

    .line 130
    :sswitch_1
    invoke-virtual {p0, v4}, Lflipboard/json/JSONSerializerBase;->a(C)V

    invoke-virtual {p0, v5}, Lflipboard/json/JSONSerializerBase;->a(C)V

    goto :goto_2

    .line 131
    :sswitch_2
    invoke-virtual {p0, v4}, Lflipboard/json/JSONSerializerBase;->a(C)V

    const/16 v2, 0x6e

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializerBase;->a(C)V

    goto :goto_2

    .line 132
    :sswitch_3
    invoke-virtual {p0, v4}, Lflipboard/json/JSONSerializerBase;->a(C)V

    const/16 v2, 0x72

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializerBase;->a(C)V

    goto :goto_2

    .line 133
    :sswitch_4
    invoke-virtual {p0, v4}, Lflipboard/json/JSONSerializerBase;->a(C)V

    const/16 v2, 0x74

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializerBase;->a(C)V

    goto :goto_2

    .line 134
    :sswitch_5
    invoke-virtual {p0, v4}, Lflipboard/json/JSONSerializerBase;->a(C)V

    const/16 v2, 0x62

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializerBase;->a(C)V

    goto :goto_2

    .line 135
    :sswitch_6
    invoke-virtual {p0, v4}, Lflipboard/json/JSONSerializerBase;->a(C)V

    const/16 v2, 0x66

    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializerBase;->a(C)V

    goto :goto_2

    .line 144
    :cond_1
    iget-object v2, p0, Lflipboard/json/JSONSerializerBase;->e:Lflipboard/io/UTF8StreamWriter;

    invoke-virtual {v2, v1}, Lflipboard/io/UTF8StreamWriter;->write(I)V

    goto :goto_2

    .line 150
    :cond_2
    invoke-virtual {p0, v5}, Lflipboard/json/JSONSerializerBase;->a(C)V

    goto/16 :goto_0

    .line 128
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_5
        0x9 -> :sswitch_4
        0xa -> :sswitch_2
        0xc -> :sswitch_6
        0xd -> :sswitch_3
        0x22 -> :sswitch_1
        0x5c -> :sswitch_0
    .end sparse-switch
.end method

.method final a(Z)V
    .locals 1

    .prologue
    .line 59
    if-eqz p1, :cond_0

    sget-object v0, Lflipboard/json/JSONSerializerBase;->b:[B

    :goto_0
    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializerBase;->a([B)V

    .line 60
    return-void

    .line 59
    :cond_0
    sget-object v0, Lflipboard/json/JSONSerializerBase;->c:[B

    goto :goto_0
.end method

.method final a([B)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lflipboard/json/JSONSerializerBase;->e:Lflipboard/io/UTF8StreamWriter;

    invoke-virtual {v0, p1}, Lflipboard/io/UTF8StreamWriter;->a([B)V

    .line 42
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/16 v6, 0x5d

    const/16 v2, 0x5b

    const/16 v5, 0x2c

    const/4 v0, 0x0

    .line 155
    if-nez p1, :cond_0

    .line 156
    sget-object v0, Lflipboard/json/JSONSerializerBase;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializerBase;->a([B)V

    .line 218
    :goto_0
    return-void

    .line 159
    :cond_0
    instance-of v1, p1, Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 160
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lflipboard/json/JSONSerializerBase;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 163
    :cond_1
    instance-of v1, p1, Ljava/lang/Number;

    if-eqz v1, :cond_4

    .line 164
    instance-of v0, p1, Ljava/lang/Float;

    if-nez v0, :cond_2

    instance-of v0, p1, Ljava/lang/Double;

    if-eqz v0, :cond_3

    .line 165
    :cond_2
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializerBase;->a(D)V

    goto :goto_0

    .line 168
    :cond_3
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lflipboard/json/JSONSerializerBase;->a(J)V

    goto :goto_0

    .line 171
    :cond_4
    instance-of v1, p1, Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 172
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializerBase;->a(Z)V

    goto :goto_0

    .line 175
    :cond_5
    instance-of v1, p1, Ljava/lang/Iterable;

    if-eqz v1, :cond_8

    .line 177
    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 178
    check-cast p1, Ljava/lang/Iterable;

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 179
    add-int/lit8 v1, v0, 0x1

    if-lez v0, :cond_6

    .line 180
    invoke-virtual {p0, v5}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 182
    :cond_6
    invoke-virtual {p0, v3}, Lflipboard/json/JSONSerializerBase;->b(Ljava/lang/Object;)V

    move v0, v1

    .line 183
    goto :goto_1

    .line 184
    :cond_7
    invoke-virtual {p0, v6}, Lflipboard/json/JSONSerializerBase;->a(C)V

    goto :goto_0

    .line 187
    :cond_8
    instance-of v1, p1, Ljava/util/Map;

    if-eqz v1, :cond_b

    .line 189
    const/16 v1, 0x7b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 190
    check-cast p1, Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 191
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_9

    .line 192
    invoke-virtual {p0, v5}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 194
    :cond_9
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializerBase;->a(Ljava/lang/String;)V

    .line 195
    const/16 v1, 0x3a

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 196
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializerBase;->b(Ljava/lang/Object;)V

    move v1, v2

    .line 197
    goto :goto_2

    .line 198
    :cond_a
    const/16 v0, 0x7d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializerBase;->a(C)V

    goto/16 :goto_0

    .line 201
    :cond_b
    instance-of v1, p1, [Ljava/lang/Object;

    if-eqz v1, :cond_e

    .line 203
    invoke-virtual {p0, v2}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 204
    check-cast p1, [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Object;

    array-length v3, p1

    move v1, v0

    :goto_3
    if-ge v0, v3, :cond_d

    aget-object v4, p1, v0

    .line 205
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_c

    .line 206
    invoke-virtual {p0, v5}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 208
    :cond_c
    invoke-virtual {p0, v4}, Lflipboard/json/JSONSerializerBase;->b(Ljava/lang/Object;)V

    .line 204
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_3

    .line 210
    :cond_d
    invoke-virtual {p0, v6}, Lflipboard/json/JSONSerializerBase;->a(C)V

    goto/16 :goto_0

    .line 213
    :cond_e
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isEnum()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 214
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializerBase;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 217
    :cond_f
    invoke-virtual {p0, p1}, Lflipboard/json/JSONSerializerBase;->a(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public final d(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 221
    if-nez p1, :cond_0

    .line 222
    sget-object v0, Lflipboard/json/JSONSerializerBase;->a:[B

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializerBase;->a([B)V

    .line 234
    :goto_0
    return-void

    .line 224
    :cond_0
    const/4 v0, 0x0

    .line 225
    const/16 v1, 0x5b

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 226
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 227
    add-int/lit8 v2, v1, 0x1

    if-lez v1, :cond_1

    .line 228
    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lflipboard/json/JSONSerializerBase;->a(C)V

    .line 230
    :cond_1
    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializerBase;->a(Ljava/lang/String;)V

    move v1, v2

    .line 231
    goto :goto_1

    .line 232
    :cond_2
    const/16 v0, 0x5d

    invoke-virtual {p0, v0}, Lflipboard/json/JSONSerializerBase;->a(C)V

    goto :goto_0
.end method
