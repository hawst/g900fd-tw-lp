.class public final Lflipboard/usage/UsageManagerV2;
.super Ljava/lang/Object;
.source "UsageManagerV2.java"


# static fields
.field public static final a:Lflipboard/util/Log;

.field public static b:Lflipboard/usage/UsageManagerV2;

.field static final c:Ljava/text/SimpleDateFormat;


# instance fields
.field public final d:Ljava/io/File;

.field final e:Ljava/lang/String;

.field final f:Ljava/lang/String;

.field final g:Ljava/lang/String;

.field public final h:I

.field public final i:I

.field final j:Landroid/content/SharedPreferences;

.field final k:Landroid/content/SharedPreferences;

.field public final l:Ljava/util/concurrent/ThreadPoolExecutor;

.field m:Ljava/io/RandomAccessFile;

.field n:I

.field o:I

.field public final p:Lflipboard/usage/UsageManagerV2$Uploader;

.field public q:Ljava/lang/String;

.field r:Ljava/io/File;

.field s:I

.field t:J

.field u:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 76
    const-string v0, "usagev2"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    .line 86
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MM-dd-yy HH:mm:ss +0000"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 92
    sput-object v0, Lflipboard/usage/UsageManagerV2;->c:Ljava/text/SimpleDateFormat;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 13

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    sput-object p0, Lflipboard/usage/UsageManagerV2;->b:Lflipboard/usage/UsageManagerV2;

    .line 137
    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v0, 0x2710

    invoke-direct {v7, v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    .line 142
    new-instance v8, Lflipboard/usage/UsageManagerV2$1;

    invoke-direct {v8, p0}, Lflipboard/usage/UsageManagerV2$1;-><init>(Lflipboard/usage/UsageManagerV2;)V

    .line 149
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x4

    const/4 v3, 0x4

    const-wide/16 v4, 0x1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lflipboard/usage/UsageManagerV2;->l:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 150
    const-string v0, "usagev2"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lflipboard/usage/UsageManagerV2;->d:Ljava/io/File;

    .line 151
    new-instance v0, Lflipboard/usage/UsageManagerV2$Uploader;

    invoke-direct {v0, p0}, Lflipboard/usage/UsageManagerV2$Uploader;-><init>(Lflipboard/usage/UsageManagerV2;)V

    iput-object v0, p0, Lflipboard/usage/UsageManagerV2;->p:Lflipboard/usage/UsageManagerV2$Uploader;

    .line 152
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    iput-object v0, p0, Lflipboard/usage/UsageManagerV2;->j:Landroid/content/SharedPreferences;

    .line 153
    const-string v0, "flipboard_local_usage"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lflipboard/usage/UsageManagerV2;->k:Landroid/content/SharedPreferences;

    .line 155
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 157
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 158
    iget-object v1, v0, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    iput-object v1, p0, Lflipboard/usage/UsageManagerV2;->e:Ljava/lang/String;

    .line 159
    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/usage/UsageManagerV2;->f:Ljava/lang/String;

    .line 160
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/usage/UsageManagerV2;->g:Ljava/lang/String;

    .line 161
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 162
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/16 v4, 0x10

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/2addr v0, v1

    int-to-long v0, v0

    const-wide/32 v4, 0xea60

    div-long/2addr v0, v4

    long-to-int v0, v0

    iput v0, p0, Lflipboard/usage/UsageManagerV2;->i:I

    .line 163
    iget v0, p0, Lflipboard/usage/UsageManagerV2;->i:I

    div-int/lit8 v0, v0, 0x3c

    iput v0, p0, Lflipboard/usage/UsageManagerV2;->h:I

    .line 164
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2;->j:Landroid/content/SharedPreferences;

    const-string v1, "uploadTime"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/usage/UsageManagerV2;->u:J

    .line 166
    iget-wide v0, p0, Lflipboard/usage/UsageManagerV2;->u:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 167
    sget-object v0, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    .line 172
    :goto_0
    invoke-virtual {p0}, Lflipboard/usage/UsageManagerV2;->b()V

    .line 175
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    array-length v4, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_5

    aget-object v5, v1, v0

    .line 176
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lflipboard/usage/UsageManagerV2;->d:Ljava/io/File;

    invoke-direct {v6, v7, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 177
    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v8

    .line 178
    const-string v7, "current-"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 179
    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-nez v7, :cond_1

    .line 180
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 196
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 169
    :cond_0
    sget-object v0, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lflipboard/usage/UsageManagerV2;->u:J

    sub-long/2addr v4, v6

    const-wide/32 v6, 0xea60

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v0, v1

    goto :goto_0

    .line 183
    :cond_1
    iget-object v6, p0, Lflipboard/usage/UsageManagerV2;->r:Ljava/io/File;

    if-eqz v6, :cond_2

    .line 184
    sget-object v6, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v7, "multiple current usage files: %s, %s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lflipboard/usage/UsageManagerV2;->r:Ljava/io/File;

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v5, v10, v11

    invoke-virtual {v6, v7, v10}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 185
    iget-object v6, p0, Lflipboard/usage/UsageManagerV2;->r:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 187
    :cond_2
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lflipboard/usage/UsageManagerV2;->d:Ljava/io/File;

    invoke-direct {v6, v7, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v6, p0, Lflipboard/usage/UsageManagerV2;->r:Ljava/io/File;

    .line 188
    const/16 v6, 0x8

    const/16 v7, 0x2e

    const/16 v10, 0x8

    invoke-virtual {v5, v7, v10}, Ljava/lang/String;->indexOf(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lflipboard/usage/UsageManagerV2;->s:I

    .line 189
    const-wide/16 v6, 0x64

    div-long v6, v8, v6

    long-to-int v5, v6

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lflipboard/usage/UsageManagerV2;->o:I

    goto :goto_2

    .line 190
    :cond_3
    const-string v7, "pending-"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 191
    iget v6, p0, Lflipboard/usage/UsageManagerV2;->n:I

    const/16 v7, 0x8

    const/16 v10, 0x2e

    const/16 v11, 0x8

    invoke-virtual {v5, v10, v11}, Ljava/lang/String;->indexOf(II)I

    move-result v10

    invoke-virtual {v5, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lflipboard/usage/UsageManagerV2;->n:I

    .line 192
    iget-wide v6, p0, Lflipboard/usage/UsageManagerV2;->t:J

    add-long/2addr v6, v8

    iput-wide v6, p0, Lflipboard/usage/UsageManagerV2;->t:J

    goto/16 :goto_2

    .line 194
    :cond_4
    sget-object v5, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    const-string v7, "invalid usage file: %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v6, v8, v9

    invoke-virtual {v5, v7, v8}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 195
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    goto/16 :goto_2

    .line 200
    :cond_5
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2;->r:Ljava/io/File;

    if-eqz v0, :cond_6

    .line 201
    sget-object v0, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v4, p0, Lflipboard/usage/UsageManagerV2;->o:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x1

    iget v4, p0, Lflipboard/usage/UsageManagerV2;->s:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x2

    iget-object v4, p0, Lflipboard/usage/UsageManagerV2;->r:Ljava/io/File;

    aput-object v4, v0, v1

    .line 205
    :cond_6
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2;->r:Ljava/io/File;

    if-eqz v0, :cond_8

    iget v0, p0, Lflipboard/usage/UsageManagerV2;->o:I

    const/16 v1, 0x64

    if-gt v0, v1, :cond_7

    iget-object v0, p0, Lflipboard/usage/UsageManagerV2;->r:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    const-wide/32 v4, 0x36ee80

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_8

    .line 206
    :cond_7
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2;->p:Lflipboard/usage/UsageManagerV2$Uploader;

    invoke-virtual {v0}, Lflipboard/usage/UsageManagerV2$Uploader;->a()V

    .line 208
    :cond_8
    return-void
.end method

.method public static a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 212
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    .line 213
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v1, :cond_0

    .line 214
    iget-object v0, v0, Lflipboard/model/ConfigSetting;->BetaUsageV2Host:Ljava/lang/String;

    .line 216
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lflipboard/model/ConfigSetting;->UsageV2Host:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 722
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    invoke-direct {v0, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 724
    :try_start_0
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v0, v2

    const/4 v2, 0x1

    aput-object p1, v0, v2

    .line 725
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    .line 726
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 725
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 728
    :cond_0
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 730
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 731
    return-void

    .line 730
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    throw v0
.end method

.method static a(Ljava/io/File;)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 743
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 744
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, p0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 747
    :try_start_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_0

    .line 748
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 747
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 751
    :cond_0
    :try_start_1
    new-instance v2, Lorg/json/JSONArray;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x5

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 752
    sget-object v5, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 763
    :goto_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 765
    return v0

    .line 754
    :catch_0
    move-exception v0

    .line 755
    :try_start_2
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v5, "Error parsing: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v2, v5, v6}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 756
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v5}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 757
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 759
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->J()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    .line 760
    goto :goto_1

    .line 763
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    throw v0
.end method

.method public static c()Z
    .locals 3

    .prologue
    .line 770
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "usage_redirect_for_monitoring"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final b()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 222
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 223
    iget-object v1, p0, Lflipboard/usage/UsageManagerV2;->j:Landroid/content/SharedPreferences;

    const-string v2, "launchCount"

    invoke-interface {v1, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 224
    iget-object v2, p0, Lflipboard/usage/UsageManagerV2;->j:Landroid/content/SharedPreferences;

    const-string v3, "currentVersionLaunchCount"

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 225
    sget-object v3, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lflipboard/usage/UsageManagerV2;->q:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    .line 226
    add-int/lit8 v1, v1, 0x1

    .line 227
    add-int/lit8 v2, v2, 0x1

    .line 228
    iget-object v3, p0, Lflipboard/usage/UsageManagerV2;->j:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "launchCount"

    .line 229
    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "currentVersionLaunchCount"

    .line 230
    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 228
    invoke-static {v3}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 231
    const-string v3, "%02d-%03d-%02d"

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    rem-int/lit8 v5, v5, 0x64

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x6

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v7

    rem-int/lit8 v0, v1, 0x64

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v8

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/usage/UsageManagerV2;->q:Ljava/lang/String;

    .line 232
    sget-object v0, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    new-array v0, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lflipboard/usage/UsageManagerV2;->q:Ljava/lang/String;

    aput-object v3, v0, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v8

    .line 233
    return-void
.end method
