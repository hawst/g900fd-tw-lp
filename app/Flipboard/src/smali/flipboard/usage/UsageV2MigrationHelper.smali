.class public Lflipboard/usage/UsageV2MigrationHelper;
.super Ljava/lang/Object;
.source "UsageV2MigrationHelper.java"


# direct methods
.method public static a(Ljava/lang/String;)Lflipboard/objs/UsageEventV2$SectionNavFrom;
    .locals 5

    .prologue
    .line 10
    invoke-static {}, Lflipboard/objs/UsageEventV2$SectionNavFrom;->values()[Lflipboard/objs/UsageEventV2$SectionNavFrom;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 11
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2$SectionNavFrom;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 23
    :goto_1
    return-object v0

    .line 10
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 16
    :cond_1
    const-string v0, "masthead"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 17
    sget-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->g:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    goto :goto_1

    .line 18
    :cond_2
    const-string v0, "contentGuide"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 19
    sget-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->h:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    goto :goto_1

    .line 20
    :cond_3
    const-string v0, "sectionItem"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 21
    sget-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->i:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    goto :goto_1

    .line 23
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method
