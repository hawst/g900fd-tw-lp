.class public final Lflipboard/usage/UsageManagerV2$Uploader;
.super Ljava/lang/Thread;
.source "UsageManagerV2.java"


# instance fields
.field a:Z

.field b:Z

.field c:Lflipboard/objs/UsageEventV2;

.field d:Lflipboard/objs/UsageEventV2;

.field public final synthetic e:Lflipboard/usage/UsageManagerV2;


# direct methods
.method constructor <init>(Lflipboard/usage/UsageManagerV2;)V
    .locals 1

    .prologue
    .line 328
    iput-object p1, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    .line 329
    const-string v0, "usage uploader"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 330
    return-void
.end method

.method public static a(Ljava/io/RandomAccessFile;)V
    .locals 4

    .prologue
    .line 643
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x2800

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 644
    new-instance v1, Lflipboard/io/UTF8StreamWriter;

    invoke-direct {v1, v0}, Lflipboard/io/UTF8StreamWriter;-><init>(Ljava/io/OutputStream;)V

    .line 645
    const-string v2, "data=["

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 646
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-static {p0, v0}, Lflipboard/usage/UsageManagerV2$Uploader;->a(Ljava/io/RandomAccessFile;[B)V

    .line 647
    return-void
.end method

.method public static a(Ljava/io/RandomAccessFile;[B)V
    .locals 5

    .prologue
    const/16 v4, 0x25

    const/4 v0, 0x0

    .line 623
    move v1, v0

    .line 624
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_3

    .line 625
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    .line 626
    if-eq v2, v4, :cond_0

    const/16 v3, 0x26

    if-eq v2, v3, :cond_0

    const/16 v3, 0x20

    if-lt v2, v3, :cond_0

    const/16 v3, 0x7f

    if-lt v2, v3, :cond_2

    .line 627
    :cond_0
    if-ge v1, v0, :cond_1

    .line 628
    sub-int v3, v0, v1

    invoke-virtual {p0, p1, v1, v3}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 630
    :cond_1
    invoke-virtual {p0, v4}, Ljava/io/RandomAccessFile;->write(I)V

    .line 631
    const-string v1, "0123456789abcdef"

    shr-int/lit8 v3, v2, 0x4

    and-int/lit8 v3, v3, 0xf

    invoke-virtual {v1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, Ljava/io/RandomAccessFile;->write(I)V

    .line 632
    const-string v1, "0123456789abcdef"

    and-int/lit8 v2, v2, 0xf

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, Ljava/io/RandomAccessFile;->write(I)V

    .line 633
    add-int/lit8 v1, v0, 0x1

    .line 624
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 636
    :cond_3
    array-length v0, p1

    if-ge v1, v0, :cond_4

    .line 637
    array-length v0, p1

    sub-int/2addr v0, v1

    invoke-virtual {p0, p1, v1, v0}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 639
    :cond_4
    return-void
.end method

.method private declared-synchronized b()V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 438
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-wide v2, v1, Lflipboard/usage/UsageManagerV2;->t:J

    const-wide/32 v4, 0x500000

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 439
    iget-object v1, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-wide v2, v1, Lflipboard/usage/UsageManagerV2;->t:J

    .line 441
    :goto_0
    iget-object v1, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget v1, v1, Lflipboard/usage/UsageManagerV2;->n:I

    if-gt v0, v1, :cond_1

    iget-object v1, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-wide v4, v1, Lflipboard/usage/UsageManagerV2;->t:J

    const-wide/32 v6, 0x3c0000

    cmp-long v1, v4, v6

    if-lez v1, :cond_1

    .line 442
    new-instance v1, Ljava/io/File;

    iget-object v4, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v4, v4, Lflipboard/usage/UsageManagerV2;->d:Ljava/io/File;

    const-string v5, "pending-%d.json"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 443
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 444
    sget-object v4, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    const-string v5, "trimming: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-virtual {v4, v5, v6}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 445
    iget-object v4, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-wide v6, v4, Lflipboard/usage/UsageManagerV2;->t:J

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v8

    sub-long/2addr v6, v8

    iput-wide v6, v4, Lflipboard/usage/UsageManagerV2;->t:J

    .line 446
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 441
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 449
    :cond_1
    sget-object v0, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    const-string v1, "trimmed %,d bytes"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-wide v6, v6, Lflipboard/usage/UsageManagerV2;->t:J

    sub-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-virtual {v0, v1, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 451
    :cond_2
    monitor-exit p0

    return-void

    .line 438
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static b(Ljava/io/RandomAccessFile;)V
    .locals 4

    .prologue
    .line 662
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 663
    new-instance v1, Lflipboard/io/UTF8StreamWriter;

    invoke-direct {v1, v0}, Lflipboard/io/UTF8StreamWriter;-><init>(Ljava/io/OutputStream;)V

    .line 664
    const-string v2, "]"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lflipboard/io/UTF8StreamWriter;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 665
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-static {p0, v0}, Lflipboard/usage/UsageManagerV2$Uploader;->a(Ljava/io/RandomAccessFile;[B)V

    .line 666
    return-void
.end method


# virtual methods
.method final declared-synchronized a()V
    .locals 9

    .prologue
    .line 418
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget v0, v0, Lflipboard/usage/UsageManagerV2;->o:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_1

    .line 420
    :try_start_1
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v0, v0, Lflipboard/usage/UsageManagerV2;->m:Ljava/io/RandomAccessFile;

    if-nez v0, :cond_0

    .line 421
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    new-instance v1, Ljava/io/RandomAccessFile;

    iget-object v2, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v2, v2, Lflipboard/usage/UsageManagerV2;->r:Ljava/io/File;

    const-string v3, "rw"

    invoke-direct {v1, v2, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, v0, Lflipboard/usage/UsageManagerV2;->m:Ljava/io/RandomAccessFile;

    .line 422
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v0, v0, Lflipboard/usage/UsageManagerV2;->m:Ljava/io/RandomAccessFile;

    iget-object v1, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v1, v1, Lflipboard/usage/UsageManagerV2;->m:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 424
    :cond_0
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v0, v0, Lflipboard/usage/UsageManagerV2;->m:Ljava/io/RandomAccessFile;

    invoke-static {v0}, Lflipboard/usage/UsageManagerV2$Uploader;->b(Ljava/io/RandomAccessFile;)V

    .line 425
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v0, v0, Lflipboard/usage/UsageManagerV2;->m:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 426
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    const/4 v1, 0x0

    iput-object v1, v0, Lflipboard/usage/UsageManagerV2;->m:Ljava/io/RandomAccessFile;

    .line 427
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-wide v2, v0, Lflipboard/usage/UsageManagerV2;->t:J

    iget-object v1, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v1, v1, Lflipboard/usage/UsageManagerV2;->r:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, v0, Lflipboard/usage/UsageManagerV2;->t:J

    .line 428
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v0, v0, Lflipboard/usage/UsageManagerV2;->r:Ljava/io/File;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v2, v2, Lflipboard/usage/UsageManagerV2;->d:Ljava/io/File;

    const-string v3, "pending-%d.json"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget v7, v6, Lflipboard/usage/UsageManagerV2;->n:I

    add-int/lit8 v8, v7, 0x1

    iput v8, v6, Lflipboard/usage/UsageManagerV2;->n:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 429
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    const/4 v1, 0x0

    iput v1, v0, Lflipboard/usage/UsageManagerV2;->o:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 434
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 430
    :catch_0
    move-exception v0

    .line 431
    :try_start_2
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 418
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lflipboard/objs/UsageEventV2;)V
    .locals 2

    .prologue
    .line 335
    iget-object v1, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    monitor-enter v1

    .line 336
    :try_start_0
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->d:Lflipboard/objs/UsageEventV2;

    if-nez v0, :cond_0

    .line 337
    iput-object p1, p0, Lflipboard/usage/UsageManagerV2$Uploader;->d:Lflipboard/objs/UsageEventV2;

    iput-object p1, p0, Lflipboard/usage/UsageManagerV2$Uploader;->c:Lflipboard/objs/UsageEventV2;

    .line 342
    :goto_0
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 343
    monitor-exit v1

    return-void

    .line 339
    :cond_0
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->d:Lflipboard/objs/UsageEventV2;

    iput-object p1, v0, Lflipboard/objs/UsageEventV2;->g:Lflipboard/objs/UsageEventV2;

    .line 340
    iput-object p1, p0, Lflipboard/usage/UsageManagerV2$Uploader;->d:Lflipboard/objs/UsageEventV2;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 343
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/io/File;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 556
    invoke-static {}, Lflipboard/usage/UsageManagerV2;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 557
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->e()Ljava/lang/String;

    move-result-object v0

    .line 561
    :goto_0
    sget-object v1, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    iget-boolean v1, v1, Lflipboard/util/Log;->f:Z

    if-eqz v1, :cond_0

    .line 562
    sget-object v1, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    const/4 v3, 0x1

    aput-object v0, v1, v3

    .line 563
    iget-object v1, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    const-string v1, "upload"

    invoke-static {v1, p1}, Lflipboard/usage/UsageManagerV2;->a(Ljava/lang/String;Ljava/io/File;)V

    .line 565
    :cond_0
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v1, :cond_1

    .line 567
    iget-object v1, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    invoke-static {p1}, Lflipboard/usage/UsageManagerV2;->a(Ljava/io/File;)Z

    .line 571
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-int v1, v4

    new-array v1, v1, [B

    .line 572
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 574
    :goto_1
    :try_start_0
    array-length v4, v1

    if-ge v2, v4, :cond_3

    .line 575
    array-length v4, v1

    sub-int/2addr v4, v2

    invoke-virtual {v3, v1, v2, v4}, Ljava/io/FileInputStream;->read([BII)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1

    .line 559
    :cond_2
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    invoke-static {}, Lflipboard/usage/UsageManagerV2;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 578
    :cond_3
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 582
    invoke-static {}, Lflipboard/usage/UsageManagerV2;->c()Z

    move-result v2

    if-nez v2, :cond_4

    .line 583
    invoke-static {v1}, Lflipboard/util/JavaUtil;->b([B)[B

    move-result-object v1

    .line 587
    :cond_4
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v2, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 588
    const-string v3, "Content-Type"

    const-string v4, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    const-string v3, "Content-Encoding"

    const-string v4, "deflate"

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    new-instance v3, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v3, v1}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 593
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1, v2}, Lflipboard/io/NetworkManager;->a(Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 594
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    .line 595
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Response: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 596
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Statusline: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 597
    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    .line 598
    const/16 v4, 0xc8

    if-eq v3, v4, :cond_5

    .line 599
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "unexpected response: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 578
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    throw v0

    .line 603
    :cond_5
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0, v2, v1}, Lflipboard/io/NetworkManager;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    .line 605
    :cond_6
    :try_start_1
    invoke-virtual {v0}, Ljava/io/InputStream;->read()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-gez v1, :cond_6

    .line 607
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 608
    return-void

    .line 607
    :catchall_1
    move-exception v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v1
.end method

.method public final run()V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 455
    const/16 v0, 0x13

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 457
    sget-object v5, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    .line 460
    :goto_0
    :try_start_0
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-wide v6, v0, Lflipboard/usage/UsageManagerV2;->t:J

    const-wide/32 v8, 0x500000

    cmp-long v0, v6, v8

    if-lez v0, :cond_0

    .line 461
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v0, v0, Lflipboard/usage/UsageManagerV2;->p:Lflipboard/usage/UsageManagerV2$Uploader;

    invoke-direct {v0}, Lflipboard/usage/UsageManagerV2$Uploader;->b()V

    .line 466
    :cond_0
    iget-object v6, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 467
    :try_start_1
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 468
    :goto_1
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->c:Lflipboard/objs/UsageEventV2;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->a:Z

    if-nez v0, :cond_1

    .line 469
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 481
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v6

    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    .line 542
    :catch_0
    move-exception v0

    return-void

    .line 471
    :cond_1
    :try_start_3
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->c:Lflipboard/objs/UsageEventV2;

    if-eqz v0, :cond_9

    .line 472
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->c:Lflipboard/objs/UsageEventV2;

    .line 473
    iget-object v3, p0, Lflipboard/usage/UsageManagerV2$Uploader;->c:Lflipboard/objs/UsageEventV2;

    iget-object v3, v3, Lflipboard/objs/UsageEventV2;->g:Lflipboard/objs/UsageEventV2;

    iput-object v3, p0, Lflipboard/usage/UsageManagerV2$Uploader;->c:Lflipboard/objs/UsageEventV2;

    .line 474
    iget-object v3, p0, Lflipboard/usage/UsageManagerV2$Uploader;->c:Lflipboard/objs/UsageEventV2;

    if-nez v3, :cond_10

    .line 475
    const/4 v3, 0x0

    iput-object v3, p0, Lflipboard/usage/UsageManagerV2$Uploader;->d:Lflipboard/objs/UsageEventV2;

    move-object v3, v0

    move v0, v1

    .line 481
    :goto_2
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 482
    if-eqz v3, :cond_6

    .line 483
    :try_start_4
    monitor-enter p0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    .line 484
    :try_start_5
    sget-object v6, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    iget-boolean v6, v6, Lflipboard/util/Log;->f:Z

    if-eqz v6, :cond_2

    .line 485
    sget-object v6, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v3}, Lflipboard/objs/UsageEventV2;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    .line 487
    :cond_2
    iget-object v6, v3, Lflipboard/objs/UsageEventV2;->f:Lflipboard/objs/UsageEventV2$Properties;

    iget v6, v6, Lflipboard/objs/UsageEventV2$Properties;->c:I

    iget-object v7, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget v7, v7, Lflipboard/usage/UsageManagerV2;->s:I

    if-eq v6, v7, :cond_3

    .line 488
    invoke-virtual {p0}, Lflipboard/usage/UsageManagerV2$Uploader;->a()V

    .line 489
    iget-object v6, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v7, v3, Lflipboard/objs/UsageEventV2;->f:Lflipboard/objs/UsageEventV2$Properties;

    iget v7, v7, Lflipboard/objs/UsageEventV2$Properties;->c:I

    iput v7, v6, Lflipboard/usage/UsageManagerV2;->s:I

    .line 490
    iget-object v6, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    new-instance v7, Ljava/io/File;

    iget-object v8, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v8, v8, Lflipboard/usage/UsageManagerV2;->d:Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "current-"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget v10, v10, Lflipboard/usage/UsageManagerV2;->s:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".json"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v7, v6, Lflipboard/usage/UsageManagerV2;->r:Ljava/io/File;

    .line 492
    :cond_3
    iget-object v6, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v6, v6, Lflipboard/usage/UsageManagerV2;->m:Ljava/io/RandomAccessFile;

    if-nez v6, :cond_4

    .line 493
    iget-object v6, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    new-instance v7, Ljava/io/RandomAccessFile;

    iget-object v8, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v8, v8, Lflipboard/usage/UsageManagerV2;->r:Ljava/io/File;

    const-string v9, "rw"

    invoke-direct {v7, v8, v9}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v7, v6, Lflipboard/usage/UsageManagerV2;->m:Ljava/io/RandomAccessFile;

    .line 494
    iget-object v6, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v6, v6, Lflipboard/usage/UsageManagerV2;->m:Ljava/io/RandomAccessFile;

    iget-object v7, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v7, v7, Lflipboard/usage/UsageManagerV2;->m:Ljava/io/RandomAccessFile;

    invoke-virtual {v7}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 496
    :cond_4
    iget-object v6, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v6, v6, Lflipboard/usage/UsageManagerV2;->m:Ljava/io/RandomAccessFile;

    iget-object v7, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget v7, v7, Lflipboard/usage/UsageManagerV2;->o:I

    if-nez v7, :cond_a

    invoke-static {v6}, Lflipboard/usage/UsageManagerV2$Uploader;->a(Ljava/io/RandomAccessFile;)V

    :goto_3
    iget-object v7, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget v8, v7, Lflipboard/usage/UsageManagerV2;->o:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v7, Lflipboard/usage/UsageManagerV2;->o:I

    invoke-virtual {v3}, Lflipboard/objs/UsageEventV2;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-static {v6, v3}, Lflipboard/usage/UsageManagerV2$Uploader;->a(Ljava/io/RandomAccessFile;[B)V

    .line 497
    iget-object v3, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget v3, v3, Lflipboard/usage/UsageManagerV2;->o:I

    const/16 v6, 0x64

    if-le v3, v6, :cond_5

    .line 498
    invoke-virtual {p0}, Lflipboard/usage/UsageManagerV2$Uploader;->a()V

    .line 500
    :cond_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 502
    :cond_6
    if-eqz v0, :cond_8

    move v0, v1

    .line 503
    :goto_4
    :try_start_6
    iget-object v3, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget v3, v3, Lflipboard/usage/UsageManagerV2;->n:I

    if-gt v0, v3, :cond_7

    .line 504
    monitor-enter p0
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    .line 505
    :try_start_7
    iget-object v3, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget v3, v3, Lflipboard/usage/UsageManagerV2;->n:I

    if-ne v0, v3, :cond_b

    .line 506
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    const/4 v3, 0x0

    iput v3, v0, Lflipboard/usage/UsageManagerV2;->n:I

    .line 507
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 536
    :cond_7
    :goto_5
    const/4 v0, 0x0

    :try_start_8
    iput-boolean v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->b:Z

    .line 538
    :cond_8
    iget-object v3, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    monitor-enter v3
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1

    .line 539
    :try_start_9
    iget-object v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 540
    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    :try_start_a
    monitor-exit v3

    throw v0
    :try_end_a
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_1

    .line 543
    :catch_1
    move-exception v0

    .line 544
    sget-object v3, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    invoke-virtual {v3, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 477
    :cond_9
    :try_start_b
    iget-boolean v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->a:Z

    if-eqz v0, :cond_f

    .line 479
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/usage/UsageManagerV2$Uploader;->a:Z
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move v0, v2

    move-object v3, v4

    goto/16 :goto_2

    .line 496
    :cond_a
    const/16 v7, 0x2c

    :try_start_c
    invoke-virtual {v6, v7}, Ljava/io/RandomAccessFile;->write(I)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto :goto_3

    .line 500
    :catchall_2
    move-exception v0

    :try_start_d
    monitor-exit p0

    throw v0

    .line 509
    :cond_b
    monitor-exit p0

    .line 510
    new-instance v3, Ljava/io/File;

    iget-object v6, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v6, v6, Lflipboard/usage/UsageManagerV2;->d:Ljava/io/File;

    const-string v7, "pending-%d.json"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 511
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_e

    .line 512
    invoke-virtual {v5}, Lflipboard/io/NetworkManager;->a()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 513
    iget-object v6, v5, Lflipboard/io/NetworkManager;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v6

    if-eqz v6, :cond_c

    iget-boolean v6, p0, Lflipboard/usage/UsageManagerV2$Uploader;->b:Z

    if-eqz v6, :cond_7

    .line 517
    :cond_c
    iget-boolean v6, p0, Lflipboard/usage/UsageManagerV2$Uploader;->b:Z

    if-nez v6, :cond_d

    invoke-virtual {v5}, Lflipboard/io/NetworkManager;->e()Z
    :try_end_d
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_1

    move-result v6

    if-nez v6, :cond_7

    .line 521
    :cond_d
    :try_start_e
    invoke-virtual {p0, v3}, Lflipboard/usage/UsageManagerV2$Uploader;->a(Ljava/io/File;)V

    .line 525
    iget-object v6, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-wide v8, v6, Lflipboard/usage/UsageManagerV2;->t:J

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v10

    sub-long/2addr v8, v10

    iput-wide v8, v6, Lflipboard/usage/UsageManagerV2;->t:J

    .line 526
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 528
    iget-object v6, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iput-wide v8, v6, Lflipboard/usage/UsageManagerV2;->u:J

    .line 529
    iget-object v6, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-object v6, v6, Lflipboard/usage/UsageManagerV2;->j:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "uploadTime"

    iget-object v8, p0, Lflipboard/usage/UsageManagerV2$Uploader;->e:Lflipboard/usage/UsageManagerV2;

    iget-wide v8, v8, Lflipboard/usage/UsageManagerV2;->u:J

    invoke-interface {v6, v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-static {v6}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_1

    .line 503
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_4

    .line 509
    :catchall_3
    move-exception v0

    :try_start_f
    monitor-exit p0

    throw v0

    .line 531
    :catch_2
    move-exception v0

    sget-object v0, Lflipboard/usage/UsageManagerV2;->a:Lflipboard/util/Log;

    const-string v6, "usage: upload failed: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    invoke-virtual {v0, v6, v7}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_1

    goto/16 :goto_5

    :cond_f
    move v0, v1

    move-object v3, v4

    goto/16 :goto_2

    :cond_10
    move-object v3, v0

    move v0, v1

    goto/16 :goto_2
.end method
