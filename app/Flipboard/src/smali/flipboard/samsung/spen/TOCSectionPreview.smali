.class public Lflipboard/samsung/spen/TOCSectionPreview;
.super Lflipboard/gui/FLRelativeLayout;
.source "TOCSectionPreview.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static j:Lflipboard/util/Log;


# instance fields
.field public a:Lflipboard/gui/FLImageView;

.field public b:Lflipboard/gui/FLStaticTextView;

.field public c:Landroid/widget/LinearLayout;

.field public d:Landroid/view/View;

.field public e:Lflipboard/service/Section;

.field public f:Z

.field public g:Z

.field public h:Lflipboard/samsung/spen/OnSamsungViewListener;

.field public i:Lflipboard/gui/toc/TOCView;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Lflipboard/gui/FLStaticTextView;

.field private o:Landroid/view/View;

.field private p:F

.field private q:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-string v0, "sectionpreview"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/samsung/spen/TOCSectionPreview;->j:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->g:Z

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->g:Z

    .line 73
    const v0, 0x7f0a036d

    invoke-virtual {p0, v0}, Lflipboard/samsung/spen/TOCSectionPreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->a:Lflipboard/gui/FLImageView;

    .line 74
    const v0, 0x7f0a0370

    invoke-virtual {p0, v0}, Lflipboard/samsung/spen/TOCSectionPreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->c:Landroid/widget/LinearLayout;

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->g:Z

    .line 80
    return-void
.end method

.method static a(Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 341
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 342
    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 343
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    .line 344
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 345
    return-void
.end method

.method private a(Landroid/view/View;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 244
    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    .line 245
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 246
    iget v3, p0, Lflipboard/samsung/spen/TOCSectionPreview;->p:F

    aget v4, v2, v1

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_0

    iget v3, p0, Lflipboard/samsung/spen/TOCSectionPreview;->p:F

    aget v4, v2, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_0

    iget v3, p0, Lflipboard/samsung/spen/TOCSectionPreview;->q:F

    aget v4, v2, v0

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_0

    iget v3, p0, Lflipboard/samsung/spen/TOCSectionPreview;->q:F

    aget v2, v2, v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v2, v4

    int-to-float v2, v2

    cmpg-float v2, v3, v2

    if-gtz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    .line 244
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method static b(Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 349
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 350
    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 351
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 352
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 353
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->k:Landroid/view/View;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreview;->a(Landroid/view/View;I)V

    .line 317
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->c:Landroid/widget/LinearLayout;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreview;->a(Landroid/view/View;I)V

    .line 318
    iget-boolean v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->f:Z

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->l:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 320
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->m:Lflipboard/gui/FLStaticTextView;

    div-int/lit8 v1, p1, 0x2

    invoke-static {v0, v1}, Lflipboard/samsung/spen/TOCSectionPreview;->b(Landroid/view/View;I)V

    .line 321
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->o:Landroid/view/View;

    div-int/lit8 v1, p1, 0x2

    invoke-static {v0, v1}, Lflipboard/samsung/spen/TOCSectionPreview;->b(Landroid/view/View;I)V

    .line 325
    :goto_0
    return-void

    .line 323
    :cond_0
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->l:Landroid/view/View;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreview;->b(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->k:Landroid/view/View;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreview;->b(Landroid/view/View;I)V

    .line 330
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->c:Landroid/widget/LinearLayout;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreview;->b(Landroid/view/View;I)V

    .line 331
    iget-boolean v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->f:Z

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->m:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreview;->a(Landroid/view/View;I)V

    .line 333
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->o:Landroid/view/View;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreview;->a(Landroid/view/View;I)V

    .line 337
    :goto_0
    return-void

    .line 335
    :cond_0
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->l:Landroid/view/View;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreview;->a(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public getAnchor()Landroid/view/View;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->d:Landroid/view/View;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 133
    invoke-direct {p0, p0}, Lflipboard/samsung/spen/TOCSectionPreview;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 134
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 135
    const-string v0, "source"

    const-string v3, "toc"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const-string v0, "pageIndex"

    iget-object v3, p0, Lflipboard/samsung/spen/TOCSectionPreview;->i:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v3}, Lflipboard/gui/toc/TOCView;->getCurrentViewIndex()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 137
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->c:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lflipboard/samsung/spen/TOCSectionPreview;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 139
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->e:Lflipboard/service/Section;

    iget-object v3, v0, Lflipboard/service/Section;->u:Ljava/util/List;

    move v0, v1

    .line 140
    :goto_0
    iget-object v4, p0, Lflipboard/samsung/spen/TOCSectionPreview;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-ge v0, v4, :cond_5

    .line 141
    iget-object v4, p0, Lflipboard/samsung/spen/TOCSectionPreview;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-direct {p0, v4}, Lflipboard/samsung/spen/TOCSectionPreview;->a(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 142
    if-eqz v3, :cond_5

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v0, :cond_5

    .line 143
    sget-object v4, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v4, v4, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v4, :cond_6

    .line 144
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 145
    const/4 v2, 0x0

    .line 146
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->P()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 147
    iget-object v4, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v4, :cond_1

    iget-object v4, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 148
    iget-object v0, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    move-object v1, v0

    .line 155
    :goto_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 156
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 157
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->P()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 159
    iget-object v4, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 160
    iget-object v0, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 161
    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 150
    :cond_1
    sget-object v0, Lflipboard/samsung/spen/TOCSectionPreview;->j:Lflipboard/util/Log;

    const-string v4, "Clicked a section preview item of type album but has no sub items"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v4, v1}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v2

    goto :goto_1

    .line 153
    :cond_2
    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    move-object v1, v0

    goto :goto_1

    .line 165
    :cond_3
    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 168
    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    if-eqz v1, :cond_5

    .line 169
    invoke-virtual {p0}, Lflipboard/samsung/spen/TOCSectionPreview;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 170
    new-instance v4, Landroid/content/Intent;

    const-class v0, Lflipboard/activities/DetailActivity;

    invoke-direct {v4, v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 171
    const-string v0, "sid"

    iget-object v5, p0, Lflipboard/samsung/spen/TOCSectionPreview;->e:Lflipboard/service/Section;

    invoke-virtual {v5}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    const-string v5, "extra_item_ids"

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 173
    const-string v0, "extra_current_item"

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 174
    invoke-virtual {v3, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 192
    :cond_5
    :goto_4
    return-void

    .line 178
    :cond_6
    invoke-virtual {p0}, Lflipboard/samsung/spen/TOCSectionPreview;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 179
    iget-object v4, p0, Lflipboard/samsung/spen/TOCSectionPreview;->e:Lflipboard/service/Section;

    invoke-virtual {v4, v1, v2}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v2

    .line 180
    const-string v4, "item"

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_4

    .line 140
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 188
    :cond_8
    invoke-virtual {p0}, Lflipboard/samsung/spen/TOCSectionPreview;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 189
    iget-object v1, p0, Lflipboard/samsung/spen/TOCSectionPreview;->e:Lflipboard/service/Section;

    invoke-virtual {v1, v0, v2}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_4
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    .line 100
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onFinishInflate()V

    .line 101
    const v0, 0x7f0a036d

    invoke-virtual {p0, v0}, Lflipboard/samsung/spen/TOCSectionPreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->a:Lflipboard/gui/FLImageView;

    .line 102
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->a:Lflipboard/gui/FLImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setAllowPreloadOnUIThread(Z)V

    .line 103
    const v0, 0x7f0a0370

    invoke-virtual {p0, v0}, Lflipboard/samsung/spen/TOCSectionPreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->c:Landroid/widget/LinearLayout;

    .line 104
    const v0, 0x7f0a01fc

    invoke-virtual {p0, v0}, Lflipboard/samsung/spen/TOCSectionPreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->b:Lflipboard/gui/FLStaticTextView;

    .line 105
    invoke-virtual {p0}, Lflipboard/samsung/spen/TOCSectionPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090138

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 106
    iget-object v1, p0, Lflipboard/samsung/spen/TOCSectionPreview;->b:Lflipboard/gui/FLStaticTextView;

    const/4 v2, 0x0

    int-to-float v0, v0

    const/high16 v3, 0x3fc00000    # 1.5f

    mul-float/2addr v0, v3

    float-to-int v0, v0

    invoke-virtual {v1, v2, v0}, Lflipboard/gui/FLStaticTextView;->a(II)V

    .line 107
    const v0, 0x7f0a01f8

    invoke-virtual {p0, v0}, Lflipboard/samsung/spen/TOCSectionPreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->k:Landroid/view/View;

    .line 108
    const v0, 0x7f0a01f9

    invoke-virtual {p0, v0}, Lflipboard/samsung/spen/TOCSectionPreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->l:Landroid/view/View;

    .line 109
    const v0, 0x7f0a035d

    invoke-virtual {p0, v0}, Lflipboard/samsung/spen/TOCSectionPreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->m:Lflipboard/gui/FLStaticTextView;

    .line 110
    const v0, 0x7f0a036f

    invoke-virtual {p0, v0}, Lflipboard/samsung/spen/TOCSectionPreview;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->o:Landroid/view/View;

    .line 112
    invoke-virtual {p0, p0}, Lflipboard/samsung/spen/TOCSectionPreview;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 205
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x7

    if-ne v2, v3, :cond_6

    .line 206
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    cmpl-float v2, v2, v6

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    cmpl-float v2, v2, v6

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-virtual {p0, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    aget v4, v2, v0

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    aget v4, v2, v0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    aget v4, v2, v1

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    aget v2, v2, v1

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v2, v4

    int-to-float v2, v2

    cmpg-float v2, v3, v2

    if-gtz v2, :cond_3

    move v2, v1

    :goto_0
    if-eqz v2, :cond_4

    .line 207
    :cond_1
    sget-object v0, Lflipboard/samsung/spen/TOCSectionPreview;->j:Lflipboard/util/Log;

    .line 208
    iput-boolean v1, p0, Lflipboard/samsung/spen/TOCSectionPreview;->g:Z

    move v0, v1

    .line 232
    :cond_2
    :goto_1
    return v0

    :cond_3
    move v2, v0

    .line 206
    goto :goto_0

    .line 211
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    cmpl-float v1, v1, v6

    if-nez v1, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    cmpl-float v1, v1, v6

    if-nez v1, :cond_5

    .line 212
    sget-object v1, Lflipboard/samsung/spen/TOCSectionPreview;->j:Lflipboard/util/Log;

    .line 214
    :cond_5
    iget-object v1, p0, Lflipboard/samsung/spen/TOCSectionPreview;->h:Lflipboard/samsung/spen/OnSamsungViewListener;

    if-eqz v1, :cond_2

    .line 215
    sget-object v1, Lflipboard/samsung/spen/TOCSectionPreview;->j:Lflipboard/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Section Preview send calculated hover exit event at Section "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lflipboard/samsung/spen/TOCSectionPreview;->e:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    iput-boolean v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->g:Z

    .line 217
    iget-object v1, p0, Lflipboard/samsung/spen/TOCSectionPreview;->h:Lflipboard/samsung/spen/OnSamsungViewListener;

    invoke-interface {v1, p0}, Lflipboard/samsung/spen/OnSamsungViewListener;->c(Landroid/view/View;)V

    goto :goto_1

    .line 221
    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_8

    .line 222
    iget-object v2, p0, Lflipboard/samsung/spen/TOCSectionPreview;->h:Lflipboard/samsung/spen/OnSamsungViewListener;

    if-eqz v2, :cond_7

    .line 223
    sget-object v2, Lflipboard/samsung/spen/TOCSectionPreview;->j:Lflipboard/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " Section Preview send hover exit event at Section "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lflipboard/samsung/spen/TOCSectionPreview;->e:Lflipboard/service/Section;

    invoke-virtual {v3}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    iput-boolean v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->g:Z

    .line 225
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->h:Lflipboard/samsung/spen/OnSamsungViewListener;

    invoke-interface {v0, p0}, Lflipboard/samsung/spen/OnSamsungViewListener;->c(Landroid/view/View;)V

    :cond_7
    move v0, v1

    .line 227
    goto :goto_1

    .line 228
    :cond_8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v2, 0x9

    if-ne v1, v2, :cond_2

    .line 229
    sget-object v1, Lflipboard/samsung/spen/TOCSectionPreview;->j:Lflipboard/util/Log;

    goto :goto_1

    .line 206
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 196
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->p:F

    .line 197
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->q:F

    .line 199
    const/4 v0, 0x0

    return v0
.end method

.method public setIsCoverStories(Z)V
    .locals 5

    .prologue
    const/high16 v4, 0x3fc00000    # 1.5f

    const/4 v3, 0x0

    .line 357
    iput-boolean p1, p0, Lflipboard/samsung/spen/TOCSectionPreview;->f:Z

    .line 358
    if-eqz p1, :cond_0

    .line 359
    invoke-virtual {p0}, Lflipboard/samsung/spen/TOCSectionPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09004a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 360
    iget-object v1, p0, Lflipboard/samsung/spen/TOCSectionPreview;->b:Lflipboard/gui/FLStaticTextView;

    int-to-float v0, v0

    mul-float/2addr v0, v4

    float-to-int v0, v0

    invoke-virtual {v1, v3, v0}, Lflipboard/gui/FLStaticTextView;->a(II)V

    .line 361
    const/4 v0, 0x1

    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {p0}, Lflipboard/samsung/spen/TOCSectionPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 362
    iget-object v1, p0, Lflipboard/samsung/spen/TOCSectionPreview;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1, v0, v0, v0, v0}, Lflipboard/gui/FLStaticTextView;->setPadding(IIII)V

    .line 363
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->d:Landroid/view/View;

    instance-of v0, v0, Lflipboard/gui/toc/CoverStoryTileContainerTablet;

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->d:Landroid/view/View;

    check-cast v0, Lflipboard/gui/toc/CoverStoryTileContainerTablet;

    .line 365
    iget-object v1, p0, Lflipboard/samsung/spen/TOCSectionPreview;->m:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/toc/CoverStoryTileContainerTablet;->getCoverStoryProvenanceView()Lflipboard/gui/FLStaticTextView;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 366
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->m:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    .line 367
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->m:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {p0}, Lflipboard/samsung/spen/TOCSectionPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090049

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v4

    float-to-int v1, v1

    invoke-virtual {v0, v3, v1}, Lflipboard/gui/FLStaticTextView;->a(II)V

    .line 368
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreview;->o:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 371
    :cond_0
    return-void
.end method

.method public setSPenHoverListener(Lflipboard/samsung/spen/OnSamsungViewListener;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lflipboard/samsung/spen/TOCSectionPreview;->h:Lflipboard/samsung/spen/OnSamsungViewListener;

    .line 118
    return-void
.end method

.method public setSection(Lflipboard/service/Section;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lflipboard/samsung/spen/TOCSectionPreview;->e:Lflipboard/service/Section;

    .line 96
    return-void
.end method
