.class public Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;
.super Lflipboard/samsung/spen/TOCSectionPreview;
.source "TOCSectionPreviewCoverStory.java"


# instance fields
.field j:Landroid/widget/ImageView;

.field k:Landroid/widget/ImageView;

.field l:Lflipboard/gui/FLLabelTextView;

.field m:Lflipboard/gui/FLStaticTextView;

.field o:Lflipboard/gui/FLStaticTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lflipboard/samsung/spen/TOCSectionPreview;-><init>(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lflipboard/samsung/spen/TOCSectionPreview;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lflipboard/samsung/spen/TOCSectionPreview;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0, p1}, Lflipboard/samsung/spen/TOCSectionPreview;->a(I)V

    .line 48
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->o:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->a(Landroid/view/View;I)V

    .line 49
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->j:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->b(Landroid/view/View;I)V

    .line 50
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->k:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->b(Landroid/view/View;I)V

    .line 51
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->l:Lflipboard/gui/FLLabelTextView;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->b(Landroid/view/View;I)V

    .line 52
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->m:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->b(Landroid/view/View;I)V

    .line 53
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0, p1}, Lflipboard/samsung/spen/TOCSectionPreview;->b(I)V

    .line 57
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->o:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->b(Landroid/view/View;I)V

    .line 58
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->j:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->a(Landroid/view/View;I)V

    .line 59
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->k:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->a(Landroid/view/View;I)V

    .line 60
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->l:Lflipboard/gui/FLLabelTextView;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->a(Landroid/view/View;I)V

    .line 61
    iget-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->m:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0, p1}, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->a(Landroid/view/View;I)V

    .line 62
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Lflipboard/samsung/spen/TOCSectionPreview;->onFinishInflate()V

    .line 39
    const v0, 0x7f0a035b

    invoke-virtual {p0, v0}, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->j:Landroid/widget/ImageView;

    .line 40
    const v0, 0x7f0a0354

    invoke-virtual {p0, v0}, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->k:Landroid/widget/ImageView;

    .line 41
    const v0, 0x7f0a035c

    invoke-virtual {p0, v0}, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->l:Lflipboard/gui/FLLabelTextView;

    .line 42
    const v0, 0x7f0a035d

    invoke-virtual {p0, v0}, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->m:Lflipboard/gui/FLStaticTextView;

    .line 43
    const v0, 0x7f0a01fc

    invoke-virtual {p0, v0}, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/samsung/spen/TOCSectionPreviewCoverStory;->o:Lflipboard/gui/FLStaticTextView;

    .line 44
    return-void
.end method
