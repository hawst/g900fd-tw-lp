.class public Lflipboard/activities/LoginActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "LoginActivity.java"

# interfaces
.implements Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;


# instance fields
.field private A:Lflipboard/activities/LoginActivity$LoginInitFrom;

.field n:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FlipboardManager;",
            "Lflipboard/service/FlipboardManager$LoginMessage;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field o:Z

.field private p:Landroid/widget/Button;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;

.field private s:Lflipboard/gui/dialog/FLProgressDialogFragment;

.field private t:Lflipboard/util/Log;

.field private u:Lflipboard/activities/FacebookAuthenticateFragment;

.field private v:Lflipboard/util/GooglePlusSignInClient;

.field private w:Z

.field private x:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

.field private y:Z

.field private z:Lflipboard/objs/UsageEventV2$EventCategory;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 61
    const-string v0, "login"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/LoginActivity;->t:Lflipboard/util/Log;

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/LoginActivity;->w:Z

    .line 74
    sget-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->c:Lflipboard/objs/UsageEventV2$EventCategory;

    iput-object v0, p0, Lflipboard/activities/LoginActivity;->z:Lflipboard/objs/UsageEventV2$EventCategory;

    .line 81
    return-void
.end method

.method static synthetic a(Lflipboard/activities/LoginActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->q:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/LoginActivity;Lflipboard/util/GooglePlusSignInClient;)Lflipboard/util/GooglePlusSignInClient;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lflipboard/activities/LoginActivity;->v:Lflipboard/util/GooglePlusSignInClient;

    return-object p1
.end method

.method public static a(ILflipboard/activities/LoginActivity$SignInMethod;Lflipboard/objs/UsageEventV2$EventCategory;Lflipboard/activities/LoginActivity$LoginInitFrom;)V
    .locals 3

    .prologue
    .line 792
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->M:Lflipboard/objs/UsageEventV2$EventAction;

    invoke-direct {v0, v1, p2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 793
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->B:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 794
    sget-object v1, Lflipboard/activities/LoginActivity$LoginInitFrom;->b:Lflipboard/activities/LoginActivity$LoginInitFrom;

    if-ne p3, v1, :cond_0

    .line 795
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v0, v1, p3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 797
    :cond_0
    if-eqz p1, :cond_1

    .line 798
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->u:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v0, v1, p1}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 800
    :cond_1
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 801
    return-void
.end method

.method static synthetic a(Lflipboard/activities/LoginActivity;Lflipboard/gui/dialog/FLProgressDialogFragment;)V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/LoginActivity$13;

    invoke-direct {v1, p0, p1}, Lflipboard/activities/LoginActivity$13;-><init>(Lflipboard/activities/LoginActivity;Lflipboard/gui/dialog/FLProgressDialogFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 633
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/LoginActivity$11;

    invoke-direct {v1, p0}, Lflipboard/activities/LoginActivity$11;-><init>(Lflipboard/activities/LoginActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    .line 645
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v5, Lflipboard/activities/LoginActivity$12;

    invoke-direct {v5, p0, p1, p2}, Lflipboard/activities/LoginActivity$12;-><init>(Lflipboard/activities/LoginActivity;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;

    iget-object v2, v1, Lflipboard/service/Flap;->i:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-direct {v0, v1, v2}, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    const-string v4, "/v1/flipboard/connectWithSSOWithToken"

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;->login(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;

    .line 731
    return-void
.end method

.method static synthetic b(Lflipboard/activities/LoginActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->r:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(Lflipboard/activities/LoginActivity;Lflipboard/gui/dialog/FLProgressDialogFragment;)Lflipboard/gui/dialog/FLProgressDialogFragment;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lflipboard/activities/LoginActivity;->s:Lflipboard/gui/dialog/FLProgressDialogFragment;

    return-object p1
.end method

.method static synthetic c(Lflipboard/activities/LoginActivity;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->p:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic d(Lflipboard/activities/LoginActivity;)V
    .locals 6

    .prologue
    .line 46
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->q:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/LoginActivity;->r:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflipboard/activities/LoginActivity;->q:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->clearFocus()V

    iget-object v2, p0, Lflipboard/activities/LoginActivity;->r:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->clearFocus()V

    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    new-instance v3, Lflipboard/gui/dialog/FLProgressDialogFragment;

    invoke-direct {v3}, Lflipboard/gui/dialog/FLProgressDialogFragment;-><init>()V

    iput-object v3, p0, Lflipboard/activities/LoginActivity;->s:Lflipboard/gui/dialog/FLProgressDialogFragment;

    iget-object v3, p0, Lflipboard/activities/LoginActivity;->s:Lflipboard/gui/dialog/FLProgressDialogFragment;

    const v4, 0x7f0d02f5

    invoke-virtual {v3, v4}, Lflipboard/gui/dialog/FLProgressDialogFragment;->f(I)V

    iget-object v3, p0, Lflipboard/activities/LoginActivity;->s:Lflipboard/gui/dialog/FLProgressDialogFragment;

    new-instance v4, Lflipboard/activities/LoginActivity$8;

    invoke-direct {v4, p0, v2}, Lflipboard/activities/LoginActivity$8;-><init>(Lflipboard/activities/LoginActivity;Ljava/util/concurrent/atomic/AtomicReference;)V

    iput-object v4, v3, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    iget-object v3, p0, Lflipboard/activities/LoginActivity;->s:Lflipboard/gui/dialog/FLProgressDialogFragment;

    iget-object v4, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v5, "progress"

    invoke-virtual {v3, v4, v5}, Lflipboard/gui/dialog/FLProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    new-instance v3, Lflipboard/activities/LoginActivity$9;

    invoke-direct {v3, p0, v2, v0, v1}, Lflipboard/activities/LoginActivity$9;-><init>(Lflipboard/activities/LoginActivity;Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lflipboard/activities/LoginActivity;->n:Lflipboard/util/Observer;

    const-string v3, "FlipLogin Submit"

    const-string v4, "service"

    const-string v5, "flipboard"

    invoke-static {v3, v4, v5}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v3, p0, Lflipboard/activities/LoginActivity;->M:Lflipboard/service/FlipboardManager;

    const/4 v4, 0x0

    iget-object v5, p0, Lflipboard/activities/LoginActivity;->n:Lflipboard/util/Observer;

    invoke-virtual {v3, v0, v1, v4, v5}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/String;ZLflipboard/util/Observer;)Lflipboard/service/Flap$Request;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic e(Lflipboard/activities/LoginActivity;)Lflipboard/util/GooglePlusSignInClient;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->v:Lflipboard/util/GooglePlusSignInClient;

    return-object v0
.end method

.method static synthetic f(Lflipboard/activities/LoginActivity;)Lflipboard/activities/FacebookAuthenticateFragment;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->u:Lflipboard/activities/FacebookAuthenticateFragment;

    return-object v0
.end method

.method static synthetic g(Lflipboard/activities/LoginActivity;)Lflipboard/objs/UsageEventV2$EventCategory;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->z:Lflipboard/objs/UsageEventV2$EventCategory;

    return-object v0
.end method

.method static synthetic h(Lflipboard/activities/LoginActivity;)Lflipboard/activities/LoginActivity$LoginInitFrom;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->A:Lflipboard/activities/LoginActivity$LoginInitFrom;

    return-object v0
.end method

.method static synthetic i(Lflipboard/activities/LoginActivity;)Lflipboard/gui/dialog/FLProgressDialogFragment;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->s:Lflipboard/gui/dialog/FLProgressDialogFragment;

    return-object v0
.end method

.method static synthetic j(Lflipboard/activities/LoginActivity;)V
    .locals 2

    .prologue
    .line 46
    :try_start_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->A()Lflipboard/model/ConfigFirstLaunch;

    move-result-object v0

    new-instance v1, Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    invoke-direct {v1, p0}, Lflipboard/util/TOCBuilder$TOCBuilderObserver;-><init>(Lflipboard/activities/FlipboardActivity;)V

    iput-object v1, p0, Lflipboard/activities/LoginActivity;->x:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    iget-object v1, p0, Lflipboard/activities/LoginActivity;->x:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    invoke-static {p0, v0, v1}, Lflipboard/util/TOCBuilder;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/model/ConfigFirstLaunch;Lflipboard/util/TOCBuilder$TOCBuilderObserver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {p0}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;)V

    goto :goto_0
.end method

.method static synthetic k(Lflipboard/activities/LoginActivity;)Lflipboard/util/Log;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->t:Lflipboard/util/Log;

    return-object v0
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 759
    const-string v0, "Google signin succeeded from Login"

    invoke-static {v0}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;)V

    .line 760
    const-string v0, "googleplus"

    const/4 v1, 0x0

    invoke-direct {p0, v0, p2, v1}, Lflipboard/activities/LoginActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 774
    .line 775
    const v0, 0x7f0d011e

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 776
    if-eqz p0, :cond_0

    iget-boolean v1, p0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_0

    .line 777
    const/4 v1, 0x1

    invoke-static {p0, v0, p1, v1}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 779
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/LoginActivity;->o:Z

    .line 780
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 627
    const-string v0, "login"

    return-object v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 783
    const-string v0, "connecting_google"

    invoke-static {p0, v0}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 784
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const v5, 0x7f0201cf

    const/16 v4, 0x2328

    const/16 v2, 0x1e39

    const/4 v3, 0x1

    const/4 v1, -0x1

    .line 566
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FlipboardActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 567
    const/16 v0, 0x1e3a

    if-ne p1, v0, :cond_2

    .line 568
    if-ne p2, v1, :cond_1

    .line 569
    sget-object v0, Lflipboard/activities/LoginActivity$SignInMethod;->c:Lflipboard/activities/LoginActivity$SignInMethod;

    iget-object v1, p0, Lflipboard/activities/LoginActivity;->z:Lflipboard/objs/UsageEventV2$EventCategory;

    iget-object v2, p0, Lflipboard/activities/LoginActivity;->A:Lflipboard/activities/LoginActivity$LoginInitFrom;

    invoke-static {v3, v0, v1, v2}, Lflipboard/activities/LoginActivity;->a(ILflipboard/activities/LoginActivity$SignInMethod;Lflipboard/objs/UsageEventV2$EventCategory;Lflipboard/activities/LoginActivity$LoginInitFrom;)V

    .line 571
    invoke-static {p0}, Lflipboard/activities/LaunchActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 572
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 573
    invoke-virtual {p0, v0}, Lflipboard/activities/LoginActivity;->startActivity(Landroid/content/Intent;)V

    .line 574
    invoke-virtual {p0}, Lflipboard/activities/LoginActivity;->finish()V

    .line 623
    :cond_0
    :goto_0
    return-void

    .line 576
    :cond_1
    iput-boolean v3, p0, Lflipboard/activities/LoginActivity;->y:Z

    goto :goto_0

    .line 578
    :cond_2
    if-ne p1, v2, :cond_3

    if-ne p2, v1, :cond_3

    .line 579
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->v:Lflipboard/util/GooglePlusSignInClient;

    if-eqz v0, :cond_0

    .line 580
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "LoginActivity:onActivityResult"

    new-instance v2, Lflipboard/activities/LoginActivity$10;

    invoke-direct {v2, p0}, Lflipboard/activities/LoginActivity$10;-><init>(Lflipboard/activities/LoginActivity;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 587
    :cond_3
    if-ne p1, v4, :cond_4

    if-ne p2, v1, :cond_4

    .line 588
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->v:Lflipboard/util/GooglePlusSignInClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/LoginActivity;->v:Lflipboard/util/GooglePlusSignInClient;

    invoke-virtual {v0}, Lflipboard/util/GooglePlusSignInClient;->d()V

    goto :goto_0

    .line 589
    :cond_4
    if-ne p1, v4, :cond_5

    if-nez p2, :cond_5

    .line 590
    iput-boolean v3, p0, Lflipboard/activities/LoginActivity;->w:Z

    goto :goto_0

    .line 591
    :cond_5
    if-ne p1, v2, :cond_6

    if-nez p2, :cond_6

    .line 592
    iput-boolean v3, p0, Lflipboard/activities/LoginActivity;->w:Z

    goto :goto_0

    .line 593
    :cond_6
    const/16 v0, 0x64

    if-ne p1, v0, :cond_9

    .line 594
    if-ne p2, v1, :cond_8

    .line 595
    if-eqz p3, :cond_7

    const-string v0, "is_agree_to_disclaimer"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 596
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->t:Lflipboard/util/Log;

    .line 598
    invoke-static {p0}, Lflipboard/service/SamsungHelper;->a(Landroid/app/Activity;)V

    goto :goto_0

    .line 601
    :cond_7
    invoke-virtual {p0}, Lflipboard/activities/LoginActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v0

    invoke-virtual {p0}, Lflipboard/activities/LoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0282

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 604
    :cond_8
    invoke-virtual {p0}, Lflipboard/activities/LoginActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v0

    invoke-virtual {p0}, Lflipboard/activities/LoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0283

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 606
    :cond_9
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_0

    .line 607
    if-ne p2, v1, :cond_0

    .line 608
    const-string v0, "access_token"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 610
    const-string v0, "api_server_url"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 611
    if-eqz v0, :cond_a

    const-string v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 613
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https://"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 615
    :cond_a
    const-string v2, "auth_server_url"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 617
    const-string v2, "login_id"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 618
    const-string v2, "login_id_type"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 620
    const-string v2, "samsung"

    invoke-direct {p0, v2, v1, v0}, Lflipboard/activities/LoginActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v10, 0x5

    const/16 v9, 0x21

    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 86
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 87
    invoke-virtual {p0}, Lflipboard/activities/LoginActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 315
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    invoke-virtual {p0}, Lflipboard/activities/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 92
    const-string v0, "in_first_launch"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 93
    if-eqz v0, :cond_a

    sget-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    :goto_1
    iput-object v0, p0, Lflipboard/activities/LoginActivity;->z:Lflipboard/objs/UsageEventV2$EventCategory;

    .line 95
    const-string v0, "extra_initialized_from_briefing"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 96
    sget-object v0, Lflipboard/activities/LoginActivity$LoginInitFrom;->b:Lflipboard/activities/LoginActivity$LoginInitFrom;

    iput-object v0, p0, Lflipboard/activities/LoginActivity;->A:Lflipboard/activities/LoginActivity$LoginInitFrom;

    .line 101
    :goto_2
    const v0, 0x7f0300cb

    invoke-virtual {p0, v0}, Lflipboard/activities/LoginActivity;->setContentView(I)V

    .line 103
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/activities/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 104
    sget-object v2, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v2}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 106
    const-string v0, "extra_flipboard_login_screen_title"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 107
    if-eqz v1, :cond_2

    .line 108
    const v0, 0x7f0a0258

    invoke-virtual {p0, v0}, Lflipboard/activities/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 109
    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    :cond_2
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lflipboard/activities/LoginActivity;->A:Lflipboard/activities/LoginActivity$LoginInitFrom;

    sget-object v1, Lflipboard/activities/LoginActivity$LoginInitFrom;->b:Lflipboard/activities/LoginActivity$LoginInitFrom;

    if-ne v0, v1, :cond_4

    .line 114
    :cond_3
    const v0, 0x7f0a0150

    invoke-virtual {p0, v0}, Lflipboard/activities/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 117
    :cond_4
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->A:Lflipboard/activities/LoginActivity$LoginInitFrom;

    sget-object v1, Lflipboard/activities/LoginActivity$LoginInitFrom;->b:Lflipboard/activities/LoginActivity$LoginInitFrom;

    if-ne v0, v1, :cond_5

    .line 119
    sget-boolean v0, Lflipboard/service/SamsungHelper;->a:Z

    if-eqz v0, :cond_5

    const-string v0, "com.osp.app.signin"

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 120
    const v0, 0x7f0a0151

    invoke-virtual {p0, v0}, Lflipboard/activities/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 121
    new-instance v1, Landroid/text/SpannableString;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "   "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v3, 0x7f0d030d

    invoke-virtual {p0, v3}, Lflipboard/activities/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 122
    new-instance v2, Landroid/text/style/ImageSpan;

    invoke-virtual {p0}, Lflipboard/activities/LoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f020154

    invoke-direct {v2, v3, v4, v6}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    invoke-interface {v1, v2, v6, v7, v9}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 123
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 124
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 125
    new-instance v1, Lflipboard/activities/LoginActivity$1;

    invoke-direct {v1, p0}, Lflipboard/activities/LoginActivity$1;-><init>(Lflipboard/activities/LoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    :cond_5
    const v0, 0x7f0a00f2

    invoke-virtual {p0, v0}, Lflipboard/activities/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lflipboard/activities/LoginActivity;->p:Landroid/widget/Button;

    .line 136
    new-instance v1, Lflipboard/activities/LoginActivity$2;

    invoke-direct {v1, p0, p0}, Lflipboard/activities/LoginActivity$2;-><init>(Lflipboard/activities/LoginActivity;Landroid/content/Context;)V

    .line 157
    const v0, 0x7f0a0259

    invoke-virtual {p0, v0}, Lflipboard/activities/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflipboard/activities/LoginActivity;->q:Landroid/widget/TextView;

    .line 158
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 159
    const v0, 0x7f0a025a

    invoke-virtual {p0, v0}, Lflipboard/activities/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflipboard/activities/LoginActivity;->r:Landroid/widget/TextView;

    .line 160
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 161
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->r:Landroid/widget/TextView;

    new-instance v1, Lflipboard/activities/LoginActivity$3;

    invoke-direct {v1, p0}, Lflipboard/activities/LoginActivity$3;-><init>(Lflipboard/activities/LoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 174
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->p:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 178
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v1

    .line 179
    iget-object v0, v1, Lflipboard/model/ConfigSetting;->AccountHelpURLString:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 180
    const v0, 0x7f0a025b

    invoke-virtual {p0, v0}, Lflipboard/activities/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 181
    iget-object v2, p0, Lflipboard/activities/LoginActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/model/ConfigSetting;->AccountHelpURLString:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lflipboard/service/FlipboardManager;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 182
    const-string v2, "<a href=%s>%s</a>"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v6

    .line 184
    invoke-virtual {p0}, Lflipboard/activities/LoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0d013b

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v7

    .line 182
    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 185
    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 187
    :cond_6
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->p:Landroid/widget/Button;

    new-instance v1, Lflipboard/activities/LoginActivity$4;

    invoke-direct {v1, p0}, Lflipboard/activities/LoginActivity$4;-><init>(Lflipboard/activities/LoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    const v0, 0x7f0a014f

    invoke-virtual {p0, v0}, Lflipboard/activities/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    .line 205
    invoke-virtual {v0, v7}, Lflipboard/gui/FLButton;->setEnabled(Z)V

    .line 208
    const v1, 0x7f0d0168

    invoke-virtual {p0, v1}, Lflipboard/activities/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 210
    const v2, 0x7f0d030e

    invoke-virtual {p0, v2}, Lflipboard/activities/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 211
    new-instance v2, Landroid/text/SpannableString;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "   "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 212
    new-instance v1, Landroid/text/style/ImageSpan;

    invoke-virtual {p0}, Lflipboard/activities/LoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f020153

    invoke-direct {v1, v3, v4, v6}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    invoke-interface {v2, v1, v6, v7, v9}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 213
    invoke-virtual {v0, v2}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 219
    sget-boolean v1, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v1, :cond_7

    .line 220
    invoke-virtual {v0, v8}, Lflipboard/gui/FLButton;->setVisibility(I)V

    .line 223
    :cond_7
    const v1, 0x7f0a0150

    invoke-virtual {p0, v1}, Lflipboard/activities/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLButton;

    .line 224
    iget-object v2, p0, Lflipboard/activities/LoginActivity;->M:Lflipboard/service/FlipboardManager;

    const-string v3, "facebook"

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v2

    .line 226
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->d()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_c

    .line 227
    :cond_8
    const-string v2, "Facebook"

    .line 233
    :goto_3
    const v3, 0x7f0d030e

    invoke-virtual {p0, v3}, Lflipboard/activities/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v2, v4, v6

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 234
    new-instance v3, Landroid/text/SpannableString;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "   "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 235
    new-instance v2, Landroid/text/style/ImageSpan;

    invoke-virtual {p0}, Lflipboard/activities/LoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f020152

    invoke-direct {v2, v4, v5, v6}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    invoke-interface {v3, v2, v6, v7, v9}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 236
    invoke-virtual {v1, v3}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 238
    invoke-virtual {v1}, Lflipboard/gui/FLButton;->getVisibility()I

    move-result v2

    if-ne v2, v8, :cond_9

    invoke-virtual {v0}, Lflipboard/gui/FLButton;->getVisibility()I

    move-result v2

    if-ne v2, v8, :cond_9

    .line 239
    const v2, 0x7f0a0152

    invoke-virtual {p0, v2}, Lflipboard/activities/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 242
    :cond_9
    invoke-virtual {p0}, Lflipboard/activities/LoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->a(Landroid/content/Context;)I

    move-result v2

    .line 243
    if-nez v2, :cond_d

    .line 246
    new-instance v2, Lflipboard/activities/LoginActivity$5;

    invoke-direct {v2, p0}, Lflipboard/activities/LoginActivity$5;-><init>(Lflipboard/activities/LoginActivity;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 265
    :goto_4
    invoke-virtual {p0}, Lflipboard/activities/LoginActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 267
    if-nez p1, :cond_e

    .line 269
    new-instance v0, Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-direct {v0}, Lflipboard/activities/FacebookAuthenticateFragment;-><init>()V

    iput-object v0, p0, Lflipboard/activities/LoginActivity;->u:Lflipboard/activities/FacebookAuthenticateFragment;

    .line 270
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->u:Lflipboard/activities/FacebookAuthenticateFragment;

    iget-object v2, p0, Lflipboard/activities/LoginActivity;->z:Lflipboard/objs/UsageEventV2$EventCategory;

    iput-object v2, v0, Lflipboard/activities/FacebookAuthenticateFragment;->h:Lflipboard/objs/UsageEventV2$EventCategory;

    .line 271
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 272
    const-string v2, "fragmentAction"

    sget-object v3, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->b:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 273
    iget-object v2, p0, Lflipboard/activities/LoginActivity;->u:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-virtual {v2, v0}, Lflipboard/activities/FacebookAuthenticateFragment;->setArguments(Landroid/os/Bundle;)V

    .line 274
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->u:Lflipboard/activities/FacebookAuthenticateFragment;

    iget-object v2, p0, Lflipboard/activities/LoginActivity;->A:Lflipboard/activities/LoginActivity$LoginInitFrom;

    iput-object v2, v0, Lflipboard/activities/FacebookAuthenticateFragment;->d:Lflipboard/activities/LoginActivity$LoginInitFrom;

    .line 275
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    .line 276
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v2, 0x1020002

    iget-object v3, p0, Lflipboard/activities/LoginActivity;->u:Lflipboard/activities/FacebookAuthenticateFragment;

    .line 277
    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 278
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 285
    :goto_5
    new-instance v0, Lflipboard/activities/LoginActivity$6;

    invoke-direct {v0, p0}, Lflipboard/activities/LoginActivity$6;-><init>(Lflipboard/activities/LoginActivity;)V

    invoke-virtual {v1, v0}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 304
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->u:Lflipboard/activities/FacebookAuthenticateFragment;

    new-instance v1, Lflipboard/activities/LoginActivity$7;

    invoke-direct {v1, p0}, Lflipboard/activities/LoginActivity$7;-><init>(Lflipboard/activities/LoginActivity;)V

    iput-object v1, v0, Lflipboard/activities/FacebookAuthenticateFragment;->c:Lflipboard/activities/FacebookAuthenticateFragment$CancelListener;

    .line 311
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setGravity(I)V

    .line 313
    iget-object v0, p0, Lflipboard/activities/LoginActivity;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setGravity(I)V

    goto/16 :goto_0

    .line 93
    :cond_a
    sget-object v0, Lflipboard/objs/UsageEventV2$EventCategory;->c:Lflipboard/objs/UsageEventV2$EventCategory;

    goto/16 :goto_1

    .line 98
    :cond_b
    sget-object v0, Lflipboard/activities/LoginActivity$LoginInitFrom;->a:Lflipboard/activities/LoginActivity$LoginInitFrom;

    iput-object v0, p0, Lflipboard/activities/LoginActivity;->A:Lflipboard/activities/LoginActivity$LoginInitFrom;

    goto/16 :goto_2

    .line 229
    :cond_c
    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->d()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 261
    :cond_d
    invoke-virtual {v0, v8}, Lflipboard/gui/FLButton;->setVisibility(I)V

    goto :goto_4

    .line 282
    :cond_e
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const v2, 0x1020002

    .line 283
    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FacebookAuthenticateFragment;

    iput-object v0, p0, Lflipboard/activities/LoginActivity;->u:Lflipboard/activities/FacebookAuthenticateFragment;

    goto :goto_5
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 546
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onPause()V

    .line 547
    iget-boolean v0, p0, Lflipboard/activities/LoginActivity;->o:Z

    if-nez v0, :cond_0

    .line 548
    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/activities/LoginActivity;->z:Lflipboard/objs/UsageEventV2$EventCategory;

    iget-object v3, p0, Lflipboard/activities/LoginActivity;->A:Lflipboard/activities/LoginActivity$LoginInitFrom;

    invoke-static {v0, v1, v2, v3}, Lflipboard/activities/LoginActivity;->a(ILflipboard/activities/LoginActivity$SignInMethod;Lflipboard/objs/UsageEventV2$EventCategory;Lflipboard/activities/LoginActivity$LoginInitFrom;)V

    .line 550
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 519
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onResume()V

    .line 521
    iget-boolean v0, p0, Lflipboard/activities/LoginActivity;->o:Z

    if-nez v0, :cond_1

    .line 522
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->N:Lflipboard/objs/UsageEventV2$EventAction;

    iget-object v2, p0, Lflipboard/activities/LoginActivity;->z:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 523
    iget-object v1, p0, Lflipboard/activities/LoginActivity;->A:Lflipboard/activities/LoginActivity$LoginInitFrom;

    sget-object v2, Lflipboard/activities/LoginActivity$LoginInitFrom;->b:Lflipboard/activities/LoginActivity$LoginInitFrom;

    if-ne v1, v2, :cond_0

    .line 524
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/activities/LoginActivity;->A:Lflipboard/activities/LoginActivity$LoginInitFrom;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 526
    :cond_0
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 528
    :cond_1
    iget-boolean v0, p0, Lflipboard/activities/LoginActivity;->y:Z

    if-eqz v0, :cond_2

    .line 529
    iput-boolean v3, p0, Lflipboard/activities/LoginActivity;->o:Z

    .line 530
    iput-boolean v3, p0, Lflipboard/activities/LoginActivity;->y:Z

    .line 537
    :cond_2
    iget-boolean v0, p0, Lflipboard/activities/LoginActivity;->w:Z

    if-eqz v0, :cond_3

    .line 538
    const-string v0, "connecting_google"

    invoke-static {p0, v0}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 539
    iput-boolean v3, p0, Lflipboard/activities/LoginActivity;->w:Z

    .line 540
    iput-boolean v3, p0, Lflipboard/activities/LoginActivity;->o:Z

    .line 542
    :cond_3
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 554
    const/4 v0, 0x1

    return v0
.end method
