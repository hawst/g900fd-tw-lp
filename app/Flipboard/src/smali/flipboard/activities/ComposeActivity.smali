.class public Lflipboard/activities/ComposeActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "ComposeActivity.java"


# instance fields
.field n:Lflipboard/activities/ComposeFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lflipboard/activities/ComposeActivity;)V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onBackPressed()V

    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 126
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 127
    const v1, 0x7f0d008e

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 128
    const v1, 0x7f0d008d

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 129
    const v1, 0x7f0d004a

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 130
    const v1, 0x7f0d023f

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 131
    new-instance v1, Lflipboard/activities/ComposeActivity$3;

    invoke-direct {v1, p0, p1}, Lflipboard/activities/ComposeActivity$3;-><init>(Lflipboard/activities/ComposeActivity;Ljava/lang/Runnable;)V

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 140
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "data_loss"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 141
    return-void
.end method

.method static synthetic b(Lflipboard/activities/ComposeActivity;)V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onBackPressed()V

    return-void
.end method


# virtual methods
.method public final e()V
    .locals 2

    .prologue
    .line 104
    new-instance v0, Lflipboard/activities/ComposeActivity$2;

    invoke-direct {v0, p0}, Lflipboard/activities/ComposeActivity$2;-><init>(Lflipboard/activities/ComposeActivity;)V

    .line 117
    iget-object v1, p0, Lflipboard/activities/ComposeActivity;->n:Lflipboard/activities/ComposeFragment;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/activities/ComposeActivity;->n:Lflipboard/activities/ComposeFragment;

    invoke-virtual {v1}, Lflipboard/activities/ComposeFragment;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    invoke-direct {p0, v0}, Lflipboard/activities/ComposeActivity;->a(Ljava/lang/Runnable;)V

    .line 122
    :goto_0
    return-void

    .line 120
    :cond_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public finish()V
    .locals 8

    .prologue
    .line 146
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 148
    iget-wide v0, p0, Lflipboard/activities/ComposeActivity;->V:J

    .line 149
    iget-wide v4, p0, Lflipboard/activities/ComposeActivity;->R:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 150
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lflipboard/activities/ComposeActivity;->R:J

    sub-long/2addr v4, v6

    add-long/2addr v0, v4

    .line 152
    :cond_0
    const-string v3, "extra_result_active_time"

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 153
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v2}, Lflipboard/activities/ComposeActivity;->setResult(ILandroid/content/Intent;)V

    .line 154
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->finish()V

    .line 155
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    const-string v0, "compose"

    return-object v0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lflipboard/activities/ComposeActivity;->n:Lflipboard/activities/ComposeFragment;

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lflipboard/activities/ComposeActivity;->n:Lflipboard/activities/ComposeFragment;

    invoke-virtual {v0}, Lflipboard/activities/ComposeFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0a004c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lflipboard/activities/ComposeActivity;->n:Lflipboard/activities/ComposeFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/ComposeActivity;->n:Lflipboard/activities/ComposeFragment;

    invoke-virtual {v0}, Lflipboard/activities/ComposeFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    new-instance v0, Lflipboard/activities/ComposeActivity$1;

    invoke-direct {v0, p0}, Lflipboard/activities/ComposeActivity$1;-><init>(Lflipboard/activities/ComposeActivity;)V

    invoke-direct {p0, v0}, Lflipboard/activities/ComposeActivity;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 96
    :cond_1
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 34
    invoke-virtual {p0}, Lflipboard/activities/ComposeActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lflipboard/activities/ComposeActivity;->S:Z

    if-eqz v0, :cond_1

    .line 35
    :cond_0
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    :goto_0
    return-void

    .line 38
    :cond_1
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lflipboard/activities/ComposeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f080004

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 41
    const v0, 0x7f030074

    invoke-virtual {p0, v0}, Lflipboard/activities/ComposeActivity;->setContentView(I)V

    .line 42
    if-nez p1, :cond_2

    .line 43
    new-instance v0, Lflipboard/activities/ComposeFragment;

    invoke-direct {v0}, Lflipboard/activities/ComposeFragment;-><init>()V

    iput-object v0, p0, Lflipboard/activities/ComposeActivity;->n:Lflipboard/activities/ComposeFragment;

    .line 44
    iget-object v0, p0, Lflipboard/activities/ComposeActivity;->n:Lflipboard/activities/ComposeFragment;

    invoke-virtual {p0}, Lflipboard/activities/ComposeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/activities/ComposeFragment;->setArguments(Landroid/os/Bundle;)V

    .line 45
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0a004e

    iget-object v2, p0, Lflipboard/activities/ComposeActivity;->n:Lflipboard/activities/ComposeFragment;

    const-string v3, "compose"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    goto :goto_0

    .line 47
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "compose"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lflipboard/activities/ComposeFragment;

    iput-object v0, p0, Lflipboard/activities/ComposeActivity;->n:Lflipboard/activities/ComposeFragment;

    goto :goto_0
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 8

    .prologue
    const/high16 v5, 0x80000

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 60
    .line 61
    iget-object v2, p0, Lflipboard/activities/ComposeActivity;->n:Lflipboard/activities/ComposeFragment;

    if-eqz v2, :cond_2

    .line 62
    iget-object v2, p0, Lflipboard/activities/ComposeActivity;->n:Lflipboard/activities/ComposeFragment;

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x7f0a039a

    if-ne v3, v4, :cond_4

    invoke-virtual {v2}, Lflipboard/activities/ComposeFragment;->a()V

    move v0, v1

    :cond_0
    :goto_0
    move v1, v0

    :cond_1
    :goto_1
    move v0, v1

    .line 64
    :cond_2
    if-nez v0, :cond_3

    .line 65
    invoke-super {p0, p1, p2}, Lflipboard/activities/FlipboardActivity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    .line 67
    :cond_3
    return v0

    .line 62
    :cond_4
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x7f0a039b

    if-ne v3, v4, :cond_7

    iget-object v3, v2, Lflipboard/activities/ComposeFragment;->c:Ljava/lang/String;

    if-nez v3, :cond_5

    invoke-virtual {v2}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    if-eqz v0, :cond_1

    const v2, 0x7f0d0092

    invoke-virtual {v0, v2}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    sget-object v4, Lflipboard/activities/ComposeFragment;->a:Lflipboard/util/Log;

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, v2, Lflipboard/activities/ComposeFragment;->c:Ljava/lang/String;

    aput-object v5, v4, v0

    new-instance v0, Ljava/io/File;

    iget-object v4, v2, Lflipboard/activities/ComposeFragment;->c:Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_6
    const-string v4, "output"

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/16 v0, 0x1e7a

    invoke-virtual {v2, v3, v0}, Lflipboard/activities/ComposeFragment;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v1

    goto :goto_0

    :cond_7
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x7f0a039c

    if-ne v3, v4, :cond_8

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v3, "image/*"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/16 v3, 0x1e7b

    invoke-virtual {v2, v0, v3}, Lflipboard/activities/ComposeFragment;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v1

    goto :goto_0

    :cond_8
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x7f0a039d

    if-ne v3, v4, :cond_b

    invoke-virtual {v2}, Lflipboard/activities/ComposeFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f0a00e6

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x15

    invoke-static {v3, v4}, Lflipboard/util/JavaUtil;->b(Ljava/lang/String;I)Ljava/util/Set;

    move-result-object v3

    iget-object v4, v2, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    if-eqz v4, :cond_9

    iget-object v4, v2, Lflipboard/activities/ComposeFragment;->b:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_9
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_a

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v4

    new-instance v5, Lflipboard/activities/ComposeFragment$3;

    invoke-direct {v5, v2, v0, v4}, Lflipboard/activities/ComposeFragment$3;-><init>(Lflipboard/activities/ComposeFragment;Landroid/widget/TextView;I)V

    iget-object v0, v2, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    new-instance v4, Lflipboard/activities/ComposeFragment$4;

    invoke-direct {v4, v2, v5}, Lflipboard/activities/ComposeFragment$4;-><init>(Lflipboard/activities/ComposeFragment;Lflipboard/service/Flap$CancellableJSONResultObserver;)V

    invoke-virtual {v0, v4}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, v2, Lflipboard/activities/ComposeFragment;->d:Lflipboard/service/FlipboardManager;

    iget-object v6, v4, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v7, Lflipboard/service/Flap$ShortenUrlRequest;

    invoke-direct {v7, v4, v6}, Lflipboard/service/Flap$ShortenUrlRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v7, v0, v5}, Lflipboard/service/Flap$ShortenUrlRequest;->a(Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$ShortenUrlRequest;

    goto :goto_2

    :cond_a
    move v0, v1

    goto/16 :goto_0

    :cond_b
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x7f0a039e

    if-ne v3, v4, :cond_0

    invoke-virtual {v2}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/ComposeActivity;

    new-instance v3, Lflipboard/activities/ComposeFragment$5;

    invoke-direct {v3, v2}, Lflipboard/activities/ComposeFragment$5;-><init>(Lflipboard/activities/ComposeFragment;)V

    invoke-direct {v0, v3}, Lflipboard/activities/ComposeActivity;->a(Ljava/lang/Runnable;)V

    move v0, v1

    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onResume()V

    .line 54
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v1, "composeViewShown"

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method public send(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lflipboard/activities/ComposeActivity;->n:Lflipboard/activities/ComposeFragment;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lflipboard/activities/ComposeActivity;->n:Lflipboard/activities/ComposeFragment;

    invoke-virtual {v0}, Lflipboard/activities/ComposeFragment;->send()V

    .line 75
    :cond_0
    return-void
.end method
