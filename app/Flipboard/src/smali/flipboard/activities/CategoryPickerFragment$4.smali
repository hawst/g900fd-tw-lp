.class Lflipboard/activities/CategoryPickerFragment$4;
.super Ljava/lang/Object;
.source "CategoryPickerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/activities/CategoryPickerFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/CategoryPickerFragment;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lflipboard/activities/CategoryPickerFragment$4;->a:Lflipboard/activities/CategoryPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 124
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 128
    iget-object v1, p0, Lflipboard/activities/CategoryPickerFragment$4;->a:Lflipboard/activities/CategoryPickerFragment;

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v3}, Lflipboard/service/User;->t()Z

    move-result v3

    iput-boolean v3, v1, Lflipboard/activities/CategoryPickerFragment;->b:Z

    .line 129
    iget-object v1, p0, Lflipboard/activities/CategoryPickerFragment$4;->a:Lflipboard/activities/CategoryPickerFragment;

    invoke-static {v1}, Lflipboard/activities/CategoryPickerFragment;->b(Lflipboard/activities/CategoryPickerFragment;)Lflipboard/gui/firstrun/CategoryPickerCloud;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getSelectedTopics()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    .line 130
    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v1

    iget-object v5, p0, Lflipboard/activities/CategoryPickerFragment$4;->a:Lflipboard/activities/CategoryPickerFragment;

    invoke-static {v5}, Lflipboard/activities/CategoryPickerFragment;->b(Lflipboard/activities/CategoryPickerFragment;)Lflipboard/gui/firstrun/CategoryPickerCloud;

    move-result-object v5

    iget v5, v5, Lflipboard/gui/firstrun/CategoryPickerCloud;->h:I

    if-ge v1, v5, :cond_2

    .line 131
    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->t()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v4

    .line 132
    :goto_0
    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment$4;->a:Lflipboard/activities/CategoryPickerFragment;

    invoke-virtual {v0}, Lflipboard/activities/CategoryPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0106

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lflipboard/activities/CategoryPickerFragment$4;->a:Lflipboard/activities/CategoryPickerFragment;

    invoke-static {v4}, Lflipboard/activities/CategoryPickerFragment;->b(Lflipboard/activities/CategoryPickerFragment;)Lflipboard/gui/firstrun/CategoryPickerCloud;

    move-result-object v4

    iget v4, v4, Lflipboard/gui/firstrun/CategoryPickerCloud;->h:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 134
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment$4;->a:Lflipboard/activities/CategoryPickerFragment;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->c(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 178
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 131
    goto :goto_0

    .line 140
    :cond_2
    iget-object v1, p0, Lflipboard/activities/CategoryPickerFragment$4;->a:Lflipboard/activities/CategoryPickerFragment;

    invoke-static {v1}, Lflipboard/activities/CategoryPickerFragment;->c(Lflipboard/activities/CategoryPickerFragment;)Z

    .line 142
    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->t()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 143
    const v0, 0x7f0d015a

    move v1, v0

    .line 147
    :goto_2
    if-eqz v1, :cond_3

    .line 148
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment$4;->a:Lflipboard/activities/CategoryPickerFragment;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-object v5, p0, Lflipboard/activities/CategoryPickerFragment$4;->a:Lflipboard/activities/CategoryPickerFragment;

    invoke-virtual {v5}, Lflipboard/activities/CategoryPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)Lflipboard/gui/dialog/FLProgressDialogFragment;

    .line 152
    :cond_3
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 153
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;

    .line 154
    new-instance v7, Lflipboard/model/FirstRunSection;

    invoke-direct {v7}, Lflipboard/model/FirstRunSection;-><init>()V

    .line 155
    iget-object v3, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->sectionTitle:Ljava/lang/String;

    iput-object v3, v7, Lflipboard/model/FirstRunSection;->title:Ljava/lang/String;

    .line 156
    iget-object v3, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->remoteid:Ljava/lang/String;

    iput-object v3, v7, Lflipboard/model/FirstRunSection;->remoteid:Ljava/lang/String;

    .line 158
    add-int/lit8 v3, v1, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lflipboard/model/FirstRunSection;->position:Ljava/lang/String;

    .line 159
    iget-object v0, v0, Lflipboard/model/FirstLaunchTopicInfo$TopicInfo;->feedType:Ljava/lang/String;

    iput-object v0, v7, Lflipboard/model/FirstRunSection;->feedType:Ljava/lang/String;

    .line 160
    invoke-interface {v5, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move v1, v3

    .line 161
    goto :goto_3

    .line 144
    :cond_4
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->af:Z

    if-eqz v0, :cond_9

    .line 145
    const v0, 0x7f0d0048

    move v1, v0

    goto :goto_2

    .line 162
    :cond_5
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment$4;->a:Lflipboard/activities/CategoryPickerFragment;

    invoke-static {v0}, Lflipboard/activities/CategoryPickerFragment;->d(Lflipboard/activities/CategoryPickerFragment;)Lflipboard/activities/CategoryPickerFragment$CategoryPickerListener;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 163
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment$4;->a:Lflipboard/activities/CategoryPickerFragment;

    invoke-static {v0}, Lflipboard/activities/CategoryPickerFragment;->d(Lflipboard/activities/CategoryPickerFragment;)Lflipboard/activities/CategoryPickerFragment$CategoryPickerListener;

    move-result-object v0

    invoke-interface {v0, v5, p1}, Lflipboard/activities/CategoryPickerFragment$CategoryPickerListener;->a(Ljava/util/Set;Landroid/view/View;)V

    goto :goto_1

    .line 165
    :cond_6
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 167
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/FirstRunSection;

    .line 168
    new-instance v7, Lflipboard/service/Section;

    iget-object v8, v0, Lflipboard/model/FirstRunSection;->title:Ljava/lang/String;

    invoke-direct {v7, v0, v8}, Lflipboard/service/Section;-><init>(Lflipboard/model/FirstRunSection;Ljava/lang/String;)V

    .line 169
    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v1, v0, :cond_7

    move v0, v4

    .line 170
    :goto_5
    const-string v8, "topic_picker"

    invoke-virtual {v3, v7, v4, v0, v8}, Lflipboard/service/User;->a(Lflipboard/service/Section;ZZLjava/lang/String;)Lflipboard/service/Section;

    .line 171
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 172
    goto :goto_4

    :cond_7
    move v0, v2

    .line 169
    goto :goto_5

    .line 173
    :cond_8
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment$4;->a:Lflipboard/activities/CategoryPickerFragment;

    invoke-virtual {v0}, Lflipboard/activities/CategoryPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 174
    if-eqz v0, :cond_0

    .line 175
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_1

    :cond_9
    move v1, v2

    goto/16 :goto_2
.end method
