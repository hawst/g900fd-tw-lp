.class public Lflipboard/activities/ReadLaterActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "ReadLaterActivity.java"


# instance fields
.field n:Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;

.field o:Lflipboard/service/User;

.field p:Z

.field q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 71
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 158
    const/4 v0, 0x1

    if-ne p1, v0, :cond_4

    if-ne p2, v7, :cond_4

    .line 159
    iget-object v0, p0, Lflipboard/activities/ReadLaterActivity;->q:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 161
    iget-object v2, p0, Lflipboard/activities/ReadLaterActivity;->o:Lflipboard/service/User;

    iget-object v3, p0, Lflipboard/activities/ReadLaterActivity;->q:Ljava/lang/String;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v2, Lflipboard/service/User;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/service/Account;

    iget-object v6, v1, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iget-boolean v6, v6, Lflipboard/service/Account$Meta;->b:Z

    if-eqz v6, :cond_0

    iget-object v1, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Account;

    iget-object v3, v2, Lflipboard/service/User;->a:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lflipboard/service/FlipboardManager;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 162
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/ReadLaterActivity;->q:Ljava/lang/String;

    .line 164
    iget-object v0, p0, Lflipboard/activities/ReadLaterActivity;->n:Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;

    invoke-virtual {v0}, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;->notifyDataSetChanged()V

    .line 166
    iget-boolean v0, p0, Lflipboard/activities/ReadLaterActivity;->p:Z

    if-eqz v0, :cond_3

    .line 168
    invoke-virtual {p0, v7}, Lflipboard/activities/ReadLaterActivity;->setResult(I)V

    .line 169
    invoke-virtual {p0}, Lflipboard/activities/ReadLaterActivity;->finish()V

    .line 176
    :cond_3
    :goto_2
    return-void

    .line 172
    :cond_4
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 174
    iget-object v0, p0, Lflipboard/activities/ReadLaterActivity;->n:Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;

    invoke-virtual {v0}, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;->notifyDataSetChanged()V

    goto :goto_2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 40
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-virtual {p0}, Lflipboard/activities/ReadLaterActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    :goto_0
    return-void

    .line 45
    :cond_0
    iget-object v0, p0, Lflipboard/activities/ReadLaterActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iput-object v0, p0, Lflipboard/activities/ReadLaterActivity;->o:Lflipboard/service/User;

    .line 49
    invoke-virtual {p0}, Lflipboard/activities/ReadLaterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "finishOnSuccessfulLogin"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/activities/ReadLaterActivity;->p:Z

    .line 51
    const v0, 0x7f03011a

    invoke-virtual {p0, v0}, Lflipboard/activities/ReadLaterActivity;->setContentView(I)V

    .line 53
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/activities/ReadLaterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 54
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 56
    const v0, 0x7f0a0310

    invoke-virtual {p0, v0}, Lflipboard/activities/ReadLaterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    .line 57
    const v1, 0x7f0d02d1

    invoke-virtual {p0, v1}, Lflipboard/activities/ReadLaterActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    const v0, 0x7f0a0144

    invoke-virtual {p0, v0}, Lflipboard/activities/ReadLaterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 60
    new-instance v1, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;

    invoke-direct {v1, p0}, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;-><init>(Lflipboard/activities/ReadLaterActivity;)V

    iput-object v1, p0, Lflipboard/activities/ReadLaterActivity;->n:Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;

    .line 61
    iget-object v1, p0, Lflipboard/activities/ReadLaterActivity;->n:Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 62
    iget-object v1, p0, Lflipboard/activities/ReadLaterActivity;->n:Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method
