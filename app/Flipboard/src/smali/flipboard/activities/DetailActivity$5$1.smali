.class Lflipboard/activities/DetailActivity$5$1;
.super Ljava/lang/Object;
.source "DetailActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field static final synthetic a:Z


# instance fields
.field final synthetic b:Lflipboard/gui/flipping/FlipTransitionViews$Message;

.field final synthetic c:Lflipboard/gui/item/FlipmagDetailViewTablet;

.field final synthetic d:Lflipboard/activities/DetailActivity$5;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 412
    const-class v0, Lflipboard/activities/DetailActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lflipboard/activities/DetailActivity$5$1;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lflipboard/activities/DetailActivity$5;Lflipboard/gui/flipping/FlipTransitionViews$Message;Lflipboard/gui/item/FlipmagDetailViewTablet;)V
    .locals 0

    .prologue
    .line 412
    iput-object p1, p0, Lflipboard/activities/DetailActivity$5$1;->d:Lflipboard/activities/DetailActivity$5;

    iput-object p2, p0, Lflipboard/activities/DetailActivity$5$1;->b:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    iput-object p3, p0, Lflipboard/activities/DetailActivity$5$1;->c:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 415
    iget-object v0, p0, Lflipboard/activities/DetailActivity$5$1;->b:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionViews$Message;->d:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    if-ne v0, v1, :cond_0

    .line 416
    iget-object v0, p0, Lflipboard/activities/DetailActivity$5$1;->c:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getCurrentViewIndex()I

    move-result v1

    .line 417
    iget-object v0, p0, Lflipboard/activities/DetailActivity$5$1;->d:Lflipboard/activities/DetailActivity$5;

    iget-object v0, v0, Lflipboard/activities/DetailActivity$5;->a:Lflipboard/activities/DetailActivity;

    invoke-static {v0}, Lflipboard/activities/DetailActivity;->b(Lflipboard/activities/DetailActivity;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Ad;

    .line 418
    iget-object v2, p0, Lflipboard/activities/DetailActivity$5$1;->d:Lflipboard/activities/DetailActivity$5;

    iget-object v2, v2, Lflipboard/activities/DetailActivity$5;->a:Lflipboard/activities/DetailActivity;

    invoke-static {v2}, Lflipboard/activities/DetailActivity;->c(Lflipboard/activities/DetailActivity;)Lflipboard/service/FLAdManager;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lflipboard/service/FLAdManager;->a(ILflipboard/objs/Ad;)V

    .line 419
    iget-object v0, p0, Lflipboard/activities/DetailActivity$5$1;->d:Lflipboard/activities/DetailActivity$5;

    iget-object v0, v0, Lflipboard/activities/DetailActivity$5;->a:Lflipboard/activities/DetailActivity;

    iget-object v1, p0, Lflipboard/activities/DetailActivity$5$1;->d:Lflipboard/activities/DetailActivity$5;

    iget-object v1, v1, Lflipboard/activities/DetailActivity$5;->a:Lflipboard/activities/DetailActivity;

    invoke-static {v1}, Lflipboard/activities/DetailActivity;->c(Lflipboard/activities/DetailActivity;)Lflipboard/service/FLAdManager;

    move-result-object v1

    iget v1, v1, Lflipboard/service/FLAdManager;->d:I

    invoke-static {v0, v1}, Lflipboard/activities/DetailActivity;->a(Lflipboard/activities/DetailActivity;I)I

    .line 420
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/DetailActivity$5$1$1;

    invoke-direct {v1, p0}, Lflipboard/activities/DetailActivity$5$1$1;-><init>(Lflipboard/activities/DetailActivity$5$1;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 431
    :goto_0
    return-void

    .line 426
    :cond_0
    sget-boolean v0, Lflipboard/activities/DetailActivity$5$1;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/DetailActivity$5$1;->b:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    sget-object v1, Lflipboard/gui/flipping/FlipTransitionViews$Message;->c:Lflipboard/gui/flipping/FlipTransitionViews$Message;

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 428
    :cond_1
    iget-object v0, p0, Lflipboard/activities/DetailActivity$5$1;->c:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getNextViewIndex()I

    move-result v0

    .line 429
    iget-object v1, p0, Lflipboard/activities/DetailActivity$5$1;->d:Lflipboard/activities/DetailActivity$5;

    iget-object v1, v1, Lflipboard/activities/DetailActivity$5;->a:Lflipboard/activities/DetailActivity;

    invoke-static {v1}, Lflipboard/activities/DetailActivity;->c(Lflipboard/activities/DetailActivity;)Lflipboard/service/FLAdManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lflipboard/service/FLAdManager;->a(I)V

    goto :goto_0
.end method
