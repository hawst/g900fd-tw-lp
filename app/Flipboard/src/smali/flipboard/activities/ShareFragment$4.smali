.class Lflipboard/activities/ShareFragment$4;
.super Ljava/lang/Object;
.source "ShareFragment.java"

# interfaces
.implements Lflipboard/service/Flap$CancellableJSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/activities/ShareFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/ShareFragment;)V
    .locals 0

    .prologue
    .line 558
    iput-object p1, p0, Lflipboard/activities/ShareFragment$4;->a:Lflipboard/activities/ShareFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 561
    sget-object v0, Lflipboard/activities/ShareFragment;->a:Lflipboard/util/Log;

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 564
    :try_start_0
    const-string v0, "magazine"

    invoke-virtual {p1, v0}, Lflipboard/json/FLObject;->f(Ljava/lang/String;)Lflipboard/json/FLObject;

    move-result-object v0

    .line 565
    if-eqz v0, :cond_2

    .line 566
    new-instance v1, Lflipboard/json/JSONParser;

    invoke-virtual {v0}, Lflipboard/json/FLObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    .line 567
    invoke-virtual {v1}, Lflipboard/json/JSONParser;->p()Lflipboard/objs/Magazine;

    move-result-object v1

    .line 568
    if-eqz v1, :cond_2

    .line 570
    const/4 v0, 0x0

    .line 571
    iget-object v2, p0, Lflipboard/activities/ShareFragment$4;->a:Lflipboard/activities/ShareFragment;

    invoke-static {v2}, Lflipboard/activities/ShareFragment;->g(Lflipboard/activities/ShareFragment;)Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 572
    iget-object v0, p0, Lflipboard/activities/ShareFragment$4;->a:Lflipboard/activities/ShareFragment;

    invoke-static {v0}, Lflipboard/activities/ShareFragment;->g(Lflipboard/activities/ShareFragment;)Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "flipboard.extra.navigating.from"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 574
    :cond_0
    invoke-static {v1, v0}, Lflipboard/util/AndroidUtil;->a(Lflipboard/objs/Magazine;Ljava/lang/String;)V

    .line 576
    iget-object v0, p0, Lflipboard/activities/ShareFragment$4;->a:Lflipboard/activities/ShareFragment;

    invoke-static {v0, v1}, Lflipboard/activities/ShareFragment;->a(Lflipboard/activities/ShareFragment;Lflipboard/objs/Magazine;)Lflipboard/objs/Magazine;

    .line 577
    iget-object v0, p0, Lflipboard/activities/ShareFragment$4;->a:Lflipboard/activities/ShareFragment;

    invoke-static {v0}, Lflipboard/activities/ShareFragment;->d(Lflipboard/activities/ShareFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Lflipboard/objs/Magazine;)V

    .line 578
    iget-object v0, v1, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    const-string v2, "Read Later"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, v1, Lflipboard/objs/Magazine;->m:Z

    if-eqz v0, :cond_3

    .line 579
    iget-object v0, p0, Lflipboard/activities/ShareFragment$4;->a:Lflipboard/activities/ShareFragment;

    invoke-static {v0}, Lflipboard/activities/ShareFragment;->d(Lflipboard/activities/ShareFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "created.read.later.default.magazine"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 584
    :cond_1
    :goto_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/ShareFragment$4$1;

    invoke-direct {v1, p0}, Lflipboard/activities/ShareFragment$4$1;-><init>(Lflipboard/activities/ShareFragment$4;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 597
    :cond_2
    :goto_1
    return-void

    .line 580
    :cond_3
    iget-object v0, v1, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    const-string v2, "Picks"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, v1, Lflipboard/objs/Magazine;->m:Z

    if-eqz v0, :cond_1

    .line 581
    iget-object v0, p0, Lflipboard/activities/ShareFragment$4;->a:Lflipboard/activities/ShareFragment;

    invoke-static {v0}, Lflipboard/activities/ShareFragment;->d(Lflipboard/activities/ShareFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "created.picks.default.magazine"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 594
    :catch_0
    move-exception v0

    .line 595
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 601
    sget-object v0, Lflipboard/activities/ShareFragment;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 603
    iget-object v0, p0, Lflipboard/activities/ShareFragment$4;->a:Lflipboard/activities/ShareFragment;

    invoke-virtual {v0}, Lflipboard/activities/ShareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 604
    if-eqz v0, :cond_0

    .line 605
    iget-object v1, p0, Lflipboard/activities/ShareFragment$4;->a:Lflipboard/activities/ShareFragment;

    invoke-static {v1}, Lflipboard/activities/ShareFragment;->d(Lflipboard/activities/ShareFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v1

    new-instance v2, Lflipboard/activities/ShareFragment$4$2;

    invoke-direct {v2, p0, v0}, Lflipboard/activities/ShareFragment$4$2;-><init>(Lflipboard/activities/ShareFragment$4;Lflipboard/activities/FlipboardActivity;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 620
    :cond_0
    return-void
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 625
    sget-object v0, Lflipboard/activities/ShareFragment;->a:Lflipboard/util/Log;

    .line 626
    return-void
.end method
