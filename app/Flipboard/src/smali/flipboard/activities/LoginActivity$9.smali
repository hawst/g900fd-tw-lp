.class Lflipboard/activities/LoginActivity$9;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/FlipboardManager;",
        "Lflipboard/service/FlipboardManager$LoginMessage;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/concurrent/atomic/AtomicReference;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lflipboard/activities/LoginActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/LoginActivity;Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    iput-object p2, p0, Lflipboard/activities/LoginActivity$9;->a:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p3, p0, Lflipboard/activities/LoginActivity$9;->b:Ljava/lang/String;

    iput-object p4, p0, Lflipboard/activities/LoginActivity$9;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 344
    check-cast p1, Lflipboard/service/FlipboardManager;

    check-cast p2, Lflipboard/service/FlipboardManager$LoginMessage;

    sget-object v0, Lflipboard/activities/LoginActivity$14;->a:[I

    invoke-virtual {p2}, Lflipboard/service/FlipboardManager$LoginMessage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    new-instance v0, Lflipboard/activities/LoginActivity$9$1;

    invoke-direct {v0, p0, p1}, Lflipboard/activities/LoginActivity$9$1;-><init>(Lflipboard/activities/LoginActivity$9;Lflipboard/service/FlipboardManager;)V

    invoke-virtual {p1, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    :pswitch_1
    check-cast p3, Ljava/lang/String;

    iget-object v0, p0, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    iget-object v1, p0, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-static {v1}, Lflipboard/activities/LoginActivity;->i(Lflipboard/activities/LoginActivity;)Lflipboard/gui/dialog/FLProgressDialogFragment;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/activities/LoginActivity;->a(Lflipboard/activities/LoginActivity;Lflipboard/gui/dialog/FLProgressDialogFragment;)V

    new-instance v0, Lflipboard/activities/LoginActivity$9$2;

    invoke-direct {v0, p0, p1, p3}, Lflipboard/activities/LoginActivity$9$2;-><init>(Lflipboard/activities/LoginActivity$9;Lflipboard/service/FlipboardManager;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    const-string v1, "service"

    const-string v2, "flipboard"

    invoke-virtual {v0, v1, v2}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "errorMsg"

    invoke-virtual {v0, v1, p3}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "FlipLogin Failed"

    invoke-static {v1, v0}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    iget-object v1, p0, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-static {v1}, Lflipboard/activities/LoginActivity;->i(Lflipboard/activities/LoginActivity;)Lflipboard/gui/dialog/FLProgressDialogFragment;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/activities/LoginActivity;->a(Lflipboard/activities/LoginActivity;Lflipboard/gui/dialog/FLProgressDialogFragment;)V

    new-instance v0, Lflipboard/activities/LoginActivity$9$3;

    invoke-direct {v0, p0, p1}, Lflipboard/activities/LoginActivity$9$3;-><init>(Lflipboard/activities/LoginActivity$9;Lflipboard/service/FlipboardManager;)V

    invoke-virtual {p1, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    new-instance v1, Lflipboard/json/FLObject;

    invoke-direct {v1}, Lflipboard/json/FLObject;-><init>()V

    const-string v0, "service"

    const-string v2, "flipboard"

    invoke-virtual {v1, v0, v2}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "errorMsg"

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v2, v0}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "FlipLogin Failed"

    invoke-static {v0, v1}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    :cond_0
    const-string v0, "switcharoo"

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    iget-object v1, p0, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-static {v1}, Lflipboard/activities/LoginActivity;->i(Lflipboard/activities/LoginActivity;)Lflipboard/gui/dialog/FLProgressDialogFragment;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/activities/LoginActivity;->a(Lflipboard/activities/LoginActivity;Lflipboard/gui/dialog/FLProgressDialogFragment;)V

    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    const-string v1, "service"

    const-string v2, "flipboard"

    invoke-virtual {v0, v1, v2}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "FlipLogin Cancelled"

    invoke-static {v1, v0}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;Ljava/util/Map;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
