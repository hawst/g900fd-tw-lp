.class Lflipboard/activities/ComposeFragment$6;
.super Ljava/lang/Object;
.source "ComposeFragment.java"

# interfaces
.implements Lflipboard/service/Flap$CancellableJSONResultObserver;


# instance fields
.field a:Z

.field final synthetic b:Lflipboard/activities/ComposeFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/ComposeFragment;)V
    .locals 0

    .prologue
    .line 615
    iput-object p1, p0, Lflipboard/activities/ComposeFragment$6;->b:Lflipboard/activities/ComposeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 2

    .prologue
    .line 625
    iget-boolean v0, p0, Lflipboard/activities/ComposeFragment$6;->a:Z

    if-nez v0, :cond_0

    .line 626
    invoke-static {}, Lflipboard/activities/ComposeFragment;->c()Lflipboard/util/Log;

    .line 627
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$6;->b:Lflipboard/activities/ComposeFragment;

    invoke-static {v0}, Lflipboard/activities/ComposeFragment;->c(Lflipboard/activities/ComposeFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v0

    new-instance v1, Lflipboard/activities/ComposeFragment$6$1;

    invoke-direct {v1, p0}, Lflipboard/activities/ComposeFragment$6$1;-><init>(Lflipboard/activities/ComposeFragment$6;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 650
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 654
    iget-boolean v0, p0, Lflipboard/activities/ComposeFragment$6;->a:Z

    if-nez v0, :cond_0

    .line 655
    invoke-static {}, Lflipboard/activities/ComposeFragment;->c()Lflipboard/util/Log;

    move-result-object v0

    const-string v1, "Sending message failed: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 656
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$6;->b:Lflipboard/activities/ComposeFragment;

    invoke-static {v0}, Lflipboard/activities/ComposeFragment;->c(Lflipboard/activities/ComposeFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v0

    new-instance v1, Lflipboard/activities/ComposeFragment$6$2;

    invoke-direct {v1, p0}, Lflipboard/activities/ComposeFragment$6$2;-><init>(Lflipboard/activities/ComposeFragment$6;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 683
    :cond_0
    return-void
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 620
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/ComposeFragment$6;->a:Z

    .line 621
    return-void
.end method
