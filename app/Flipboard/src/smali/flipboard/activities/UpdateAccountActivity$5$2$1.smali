.class Lflipboard/activities/UpdateAccountActivity$5$2$1;
.super Ljava/lang/Object;
.source "UpdateAccountActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager$UpdateAccountMessage;

.field final synthetic b:Ljava/lang/Object;

.field final synthetic c:Lflipboard/activities/UpdateAccountActivity$5$2;


# direct methods
.method constructor <init>(Lflipboard/activities/UpdateAccountActivity$5$2;Lflipboard/service/FlipboardManager$UpdateAccountMessage;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iput-object p2, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->a:Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    iput-object p3, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->b:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const v3, 0x7f0201cf

    .line 176
    sget-object v0, Lflipboard/activities/UpdateAccountActivity$8;->a:[I

    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->a:Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager$UpdateAccountMessage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 211
    :goto_0
    return-void

    .line 179
    :pswitch_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 180
    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iget-object v1, v1, Lflipboard/activities/UpdateAccountActivity$5$2;->a:Ljava/lang/String;

    iget-object v2, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iput-object v1, v2, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    .line 181
    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iget-object v1, v1, Lflipboard/activities/UpdateAccountActivity$5$2;->b:Ljava/lang/String;

    iget-object v2, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iput-object v1, v2, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    .line 182
    iget-object v1, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iget-object v2, v2, Lflipboard/activities/UpdateAccountActivity$5$2;->c:Ljava/lang/String;

    iput-object v2, v1, Lflipboard/objs/UserService;->s:Ljava/lang/String;

    .line 183
    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iget-object v1, v1, Lflipboard/activities/UpdateAccountActivity$5$2;->d:Ljava/lang/String;

    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v0, v1}, Lflipboard/objs/UserService;->a(Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iget-object v0, v0, Lflipboard/activities/UpdateAccountActivity$5$2;->e:Lflipboard/activities/UpdateAccountActivity$5;

    iget-object v0, v0, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iget-object v1, v1, Lflipboard/activities/UpdateAccountActivity$5$2;->e:Lflipboard/activities/UpdateAccountActivity$5;

    invoke-static {v1}, Lflipboard/activities/UpdateAccountActivity$5;->a(Lflipboard/activities/UpdateAccountActivity$5;)Lflipboard/gui/dialog/FLProgressDialog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/activities/UpdateAccountActivity;->a(Landroid/content/DialogInterface;)V

    .line 187
    new-instance v0, Landroid/content/Intent;

    const-string v1, "flipboard.app.broadcast.SYNC_USER_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 188
    const-string v1, "name"

    iget-object v2, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iget-object v2, v2, Lflipboard/activities/UpdateAccountActivity$5$2;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    const-string v1, "profile"

    iget-object v2, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iget-object v2, v2, Lflipboard/activities/UpdateAccountActivity$5$2;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const-string v2, "sstream.app.broadcast.SYNC_USER"

    invoke-virtual {v1, v0, v2}, Lflipboard/app/FlipboardApplication;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 191
    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iget-object v1, v1, Lflipboard/activities/UpdateAccountActivity$5$2;->e:Lflipboard/activities/UpdateAccountActivity$5;

    iget-object v1, v1, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lflipboard/activities/UpdateAccountActivity;->setResult(ILandroid/content/Intent;)V

    .line 192
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iget-object v0, v0, Lflipboard/activities/UpdateAccountActivity$5$2;->e:Lflipboard/activities/UpdateAccountActivity$5;

    iget-object v0, v0, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    invoke-virtual {v0}, Lflipboard/activities/UpdateAccountActivity;->finish()V

    .line 193
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iget-object v0, v0, Lflipboard/activities/UpdateAccountActivity$5$2;->e:Lflipboard/activities/UpdateAccountActivity$5;

    iget-object v0, v0, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    invoke-virtual {v0}, Lflipboard/activities/UpdateAccountActivity;->f()V

    goto :goto_0

    .line 197
    :pswitch_1
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 198
    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iget-object v1, v1, Lflipboard/activities/UpdateAccountActivity$5$2;->e:Lflipboard/activities/UpdateAccountActivity$5;

    iget-object v1, v1, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    iget-object v2, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iget-object v2, v2, Lflipboard/activities/UpdateAccountActivity$5$2;->e:Lflipboard/activities/UpdateAccountActivity$5;

    invoke-static {v2}, Lflipboard/activities/UpdateAccountActivity$5;->a(Lflipboard/activities/UpdateAccountActivity$5;)Lflipboard/gui/dialog/FLProgressDialog;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/activities/UpdateAccountActivity;->a(Landroid/content/DialogInterface;)V

    .line 201
    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iget-object v1, v1, Lflipboard/activities/UpdateAccountActivity$5$2;->e:Lflipboard/activities/UpdateAccountActivity$5;

    iget-object v1, v1, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    invoke-static {v1}, Lflipboard/activities/UpdateAccountActivity;->g(Lflipboard/activities/UpdateAccountActivity;)Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 202
    if-eqz v0, :cond_0

    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->a:Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    sget-object v2, Lflipboard/service/FlipboardManager$UpdateAccountMessage;->c:Lflipboard/service/FlipboardManager$UpdateAccountMessage;

    if-ne v1, v2, :cond_0

    .line 204
    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iget-object v1, v1, Lflipboard/activities/UpdateAccountActivity$5$2;->e:Lflipboard/activities/UpdateAccountActivity$5;

    iget-object v1, v1, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    invoke-virtual {v1}, Lflipboard/activities/UpdateAccountActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 207
    :cond_0
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iget-object v0, v0, Lflipboard/activities/UpdateAccountActivity$5$2;->e:Lflipboard/activities/UpdateAccountActivity$5;

    iget-object v0, v0, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    invoke-virtual {v0}, Lflipboard/activities/UpdateAccountActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity$5$2$1;->c:Lflipboard/activities/UpdateAccountActivity$5$2;

    iget-object v1, v1, Lflipboard/activities/UpdateAccountActivity$5$2;->e:Lflipboard/activities/UpdateAccountActivity$5;

    iget-object v1, v1, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    invoke-virtual {v1}, Lflipboard/activities/UpdateAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d033b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 176
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
