.class Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$1;
.super Ljava/lang/Object;
.source "ContentDrawerTabletActivity.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/ContentDrawerHandler;",
        "Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/activities/ContentDrawerTabletActivity;

.field final synthetic b:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;


# direct methods
.method constructor <init>(Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;Lflipboard/activities/ContentDrawerTabletActivity;)V
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$1;->b:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iput-object p2, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$1;->a:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 354
    check-cast p2, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    sget-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->a:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    if-ne p2, v0, :cond_2

    check-cast p3, Ljava/util/List;

    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$1;->b:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget-object v0, v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    if-eqz p3, :cond_0

    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$1;->b:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget-object v0, v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$1;->b:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget-object v0, v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    iget-object v0, v0, Lflipboard/activities/ContentDrawerTabletActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$1$1;

    invoke-direct {v1, p0}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$1$1;-><init>(Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$1;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v0, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->i:Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$1;->b:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget-object v0, v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    iget-object v0, v0, Lflipboard/activities/ContentDrawerTabletActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$1$2;

    invoke-direct {v1, p0}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$1$2;-><init>(Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$1;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
