.class Lflipboard/activities/FlipboardActivity$8$1;
.super Ljava/lang/Object;
.source "FlipboardActivity.java"

# interfaces
.implements Lflipboard/service/Flap$SearchObserver;


# instance fields
.field final synthetic a:Lflipboard/activities/FlipboardActivity$8;


# direct methods
.method constructor <init>(Lflipboard/activities/FlipboardActivity$8;)V
    .locals 0

    .prologue
    .line 1095
    iput-object p1, p0, Lflipboard/activities/FlipboardActivity$8$1;->a:Lflipboard/activities/FlipboardActivity$8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 1107
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    .line 1108
    return-void
.end method

.method public final a(Ljava/lang/String;Lflipboard/objs/SearchResultItem;)V
    .locals 2

    .prologue
    .line 1098
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 1099
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SearchResultCategory;",
            ">;IJ)V"
        }
    .end annotation

    .prologue
    .line 1103
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;J)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SearchResultCategory;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 1101
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    return-void
.end method

.method public final a(Ljava/lang/Throwable;Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 1112
    sget-object v0, Lflipboard/activities/FlipboardActivity;->K:Lflipboard/util/Log;

    const-string v1, "Unexpected exception: %E"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1113
    return-void
.end method
