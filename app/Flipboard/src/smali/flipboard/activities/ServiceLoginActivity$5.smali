.class Lflipboard/activities/ServiceLoginActivity$5;
.super Ljava/lang/Object;
.source "ServiceLoginActivity.java"

# interfaces
.implements Lflipboard/service/Flap$TypedResultObserver;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/service/Flap$TypedResultObserver",
        "<",
        "Lflipboard/objs/UserInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic c:Lflipboard/activities/ServiceLoginActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/ServiceLoginActivity;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 604
    iput-object p1, p0, Lflipboard/activities/ServiceLoginActivity$5;->c:Lflipboard/activities/ServiceLoginActivity;

    iput-object p2, p0, Lflipboard/activities/ServiceLoginActivity$5;->a:Ljava/lang/String;

    iput-object p3, p0, Lflipboard/activities/ServiceLoginActivity$5;->b:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 604
    check-cast p1, Lflipboard/objs/UserInfo;

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$5;->c:Lflipboard/activities/ServiceLoginActivity;

    invoke-static {v0}, Lflipboard/activities/ServiceLoginActivity;->b(Lflipboard/activities/ServiceLoginActivity;)Lflipboard/util/GooglePlusSignInClient;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity$5;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/util/GooglePlusSignInClient;->a(Ljava/lang/String;)V

    iget-boolean v0, p1, Lflipboard/objs/UserInfo;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$5;->c:Lflipboard/activities/ServiceLoginActivity;

    invoke-virtual {v0, p1}, Lflipboard/activities/ServiceLoginActivity;->a(Lflipboard/objs/UserInfo;)V

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$5;->b:Landroid/os/Bundle;

    const-string v1, "from"

    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity$5;->c:Lflipboard/activities/ServiceLoginActivity;

    invoke-static {v2}, Lflipboard/activities/ServiceLoginActivity;->a(Lflipboard/activities/ServiceLoginActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lflipboard/activities/ServiceLoginActivity;->n:Lflipboard/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Login to flipboard with Google token error "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lflipboard/objs/UserInfo;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity$5;->c:Lflipboard/activities/ServiceLoginActivity;

    iget-object v0, p1, Lflipboard/objs/UserInfo;->g:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v2, :cond_0

    if-nez v0, :cond_2

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$5;->c:Lflipboard/activities/ServiceLoginActivity;

    const v2, 0x7f0d0164

    invoke-virtual {v0, v2}, Lflipboard/activities/ServiceLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_2
    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity$5;->c:Lflipboard/activities/ServiceLoginActivity;

    const v3, 0x7f0d011e

    invoke-virtual {v2, v3}, Lflipboard/activities/ServiceLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v0, v3}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 633
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$5;->c:Lflipboard/activities/ServiceLoginActivity;

    invoke-static {v0}, Lflipboard/activities/ServiceLoginActivity;->b(Lflipboard/activities/ServiceLoginActivity;)Lflipboard/util/GooglePlusSignInClient;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity$5;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/util/GooglePlusSignInClient;->a(Ljava/lang/String;)V

    .line 635
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$5;->c:Lflipboard/activities/ServiceLoginActivity;

    const-string v1, "loading"

    invoke-static {v0, v1}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 636
    const-string v0, "418"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 637
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$5;->c:Lflipboard/activities/ServiceLoginActivity;

    invoke-static {v0}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;)V

    .line 646
    :cond_0
    :goto_0
    return-void

    .line 639
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$5;->c:Lflipboard/activities/ServiceLoginActivity;

    .line 640
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_0

    .line 641
    const v1, 0x7f0d0164

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 642
    const v2, 0x7f0d0165

    invoke-virtual {v0, v2}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 643
    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method
