.class Lflipboard/activities/SettingsFragment$52;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SettingsFragment.java"


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Lflipboard/activities/SettingsFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1970
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$52;->b:Lflipboard/activities/SettingsFragment;

    iput-object p2, p0, Lflipboard/activities/SettingsFragment$52;->a:Landroid/widget/EditText;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 4

    .prologue
    .line 1973
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$52;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1974
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 1976
    const-string v0, "http://flint01.beta.live.flipboard.com:35468"

    .line 1978
    :cond_0
    iget-object v1, p0, Lflipboard/activities/SettingsFragment$52;->a:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1979
    iget-object v1, p0, Lflipboard/activities/SettingsFragment$52;->b:Lflipboard/activities/SettingsFragment;

    invoke-static {v1}, Lflipboard/activities/SettingsFragment;->d(Lflipboard/activities/SettingsFragment;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "adserver_internal_baseurl"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1980
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$52;->b:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->i(Lflipboard/activities/SettingsFragment;)Lflipboard/activities/SettingsFragment$SettingsAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->notifyDataSetChanged()V

    .line 1983
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$52;->b:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->a(Lflipboard/activities/SettingsFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/SettingsFragment$52;->b:Lflipboard/activities/SettingsFragment;

    invoke-static {v1}, Lflipboard/activities/SettingsFragment;->a(Lflipboard/activities/SettingsFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v1

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v2, Lflipboard/activities/SettingsFragment$52$1;

    invoke-direct {v2, p0}, Lflipboard/activities/SettingsFragment$52$1;-><init>(Lflipboard/activities/SettingsFragment$52;)V

    new-instance v3, Lflipboard/service/Flap$AdDeleteAllRequest;

    invoke-direct {v3, v0, v1}, Lflipboard/service/Flap$AdDeleteAllRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v3, v2}, Lflipboard/service/Flap$AdDeleteAllRequest;->a(Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$AdDeleteAllRequest;

    .line 1994
    return-void
.end method

.method public final e(Landroid/support/v4/app/DialogFragment;)V
    .locals 3

    .prologue
    .line 1998
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$52;->a:Landroid/widget/EditText;

    const-string v1, "http://flint01.beta.live.flipboard.com:35468"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1999
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$52;->b:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->d(Lflipboard/activities/SettingsFragment;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "adserver_internal_baseurl"

    const-string v2, "http://flint01.beta.live.flipboard.com:35468"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2000
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$52;->b:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->i(Lflipboard/activities/SettingsFragment;)Lflipboard/activities/SettingsFragment$SettingsAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->notifyDataSetChanged()V

    .line 2001
    return-void
.end method
