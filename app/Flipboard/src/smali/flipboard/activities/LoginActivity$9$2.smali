.class Lflipboard/activities/LoginActivity$9$2;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lflipboard/activities/LoginActivity$9;


# direct methods
.method constructor <init>(Lflipboard/activities/LoginActivity$9;Lflipboard/service/FlipboardManager;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 416
    iput-object p1, p0, Lflipboard/activities/LoginActivity$9$2;->c:Lflipboard/activities/LoginActivity$9;

    iput-object p2, p0, Lflipboard/activities/LoginActivity$9$2;->a:Lflipboard/service/FlipboardManager;

    iput-object p3, p0, Lflipboard/activities/LoginActivity$9$2;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 421
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialog$Builder;

    iget-object v1, p0, Lflipboard/activities/LoginActivity$9$2;->c:Lflipboard/activities/LoginActivity$9;

    iget-object v1, v1, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-direct {v0, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflipboard/activities/LoginActivity$9$2;->c:Lflipboard/activities/LoginActivity$9;

    iget-object v1, v1, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    const v2, 0x7f0d011e

    .line 422
    invoke-virtual {v1, v2}, Lflipboard/activities/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0d023f

    const/4 v2, 0x0

    .line 423
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 425
    iget-object v1, p0, Lflipboard/activities/LoginActivity$9$2;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v1

    iget-object v1, v1, Lflipboard/model/ConfigSetting;->AccountHelpURLString:Ljava/lang/String;

    .line 426
    if-eqz v1, :cond_0

    .line 427
    const v2, 0x7f0d0173

    new-instance v3, Lflipboard/activities/LoginActivity$9$2$1;

    invoke-direct {v3, p0, v1}, Lflipboard/activities/LoginActivity$9$2$1;-><init>(Lflipboard/activities/LoginActivity$9$2;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 435
    :cond_0
    iget-object v1, p0, Lflipboard/activities/LoginActivity$9$2;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 436
    iget-object v1, p0, Lflipboard/activities/LoginActivity$9$2;->c:Lflipboard/activities/LoginActivity$9;

    iget-object v1, v1, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-virtual {v1, v0}, Lflipboard/activities/LoginActivity;->a(Landroid/app/AlertDialog$Builder;)Landroid/app/AlertDialog;

    .line 437
    return-void
.end method
