.class final enum Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;
.super Ljava/lang/Enum;
.source "ChooseAccountActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

.field public static final enum b:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

.field public static final enum c:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

.field public static final enum d:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

.field private static final synthetic e:[Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 363
    new-instance v0, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    const-string v1, "account"

    invoke-direct {v0, v1, v2}, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->a:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    new-instance v0, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    const-string v1, "item"

    invoke-direct {v0, v1, v3}, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->b:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    new-instance v0, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    const-string v1, "header"

    invoke-direct {v0, v1, v4}, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->c:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    new-instance v0, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    const-string v1, "nonaccountservice"

    invoke-direct {v0, v1, v5}, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->d:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    const/4 v0, 0x4

    new-array v0, v0, [Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    sget-object v1, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->a:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->b:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->c:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->d:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    aput-object v1, v0, v5

    sput-object v0, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->e:[Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 363
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;
    .locals 1

    .prologue
    .line 363
    const-class v0, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    return-object v0
.end method

.method public static values()[Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;
    .locals 1

    .prologue
    .line 363
    sget-object v0, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->e:[Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    invoke-virtual {v0}, [Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    return-object v0
.end method
