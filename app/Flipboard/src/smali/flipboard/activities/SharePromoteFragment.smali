.class public Lflipboard/activities/SharePromoteFragment;
.super Lflipboard/gui/dialog/FLDialogFragment;
.source "SharePromoteFragment.java"


# static fields
.field public static final j:Lflipboard/util/Log;


# instance fields
.field private k:Lflipboard/objs/Magazine;

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Account;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lflipboard/activities/ShareActivity;->n:Lflipboard/util/Log;

    sput-object v0, Lflipboard/activities/SharePromoteFragment;->j:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogFragment;-><init>()V

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lflipboard/activities/SharePromoteFragment;->n:Ljava/util/List;

    .line 64
    iput-object p1, p0, Lflipboard/activities/SharePromoteFragment;->o:Landroid/os/Bundle;

    .line 65
    return-void
.end method

.method static synthetic a(Lflipboard/activities/SharePromoteFragment;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 50
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lflipboard/activities/SharePromoteFragment;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Account;

    iget-object v2, v0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iget-object v2, v2, Lflipboard/service/Account$Meta;->c:Lflipboard/json/FLObject;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iget-object v2, v2, Lflipboard/service/Account$Meta;->c:Lflipboard/json/FLObject;

    invoke-virtual {v2}, Lflipboard/json/FLObject;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lflipboard/activities/SharePromoteFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d004b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lflipboard/activities/SharePromoteFragment;->k:Lflipboard/objs/Magazine;

    iget-object v6, v6, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    aput-object v6, v2, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lflipboard/activities/SharePromoteFragment;->m:Ljava/lang/String;

    aput-object v6, v2, v5

    invoke-static {v0, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lflipboard/activities/SharePromoteFragment$4;

    invoke-direct {v0, p0, v4}, Lflipboard/activities/SharePromoteFragment$4;-><init>(Lflipboard/activities/SharePromoteFragment;Ljava/util/Set;)V

    iget-object v4, p0, Lflipboard/activities/SharePromoteFragment;->k:Lflipboard/objs/Magazine;

    iget-object v4, v4, Lflipboard/objs/Magazine;->r:Ljava/lang/String;

    iget-object v5, p0, Lflipboard/activities/SharePromoteFragment;->m:Ljava/lang/String;

    move-object v6, v3

    invoke-static/range {v0 .. v6}, Lflipboard/util/ShareHelper;->a(Lflipboard/service/Flap$JSONResultObserver;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Section;)V

    return-void
.end method

.method static synthetic b(Lflipboard/activities/SharePromoteFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lflipboard/activities/SharePromoteFragment;->n:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lflipboard/activities/SharePromoteFragment;)V
    .locals 4

    .prologue
    .line 50
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lflipboard/activities/SharePromoteFragment;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lflipboard/activities/SharePromoteFragment;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Account;

    iget-object v3, v0, Lflipboard/service/Account;->c:Lflipboard/service/Account$Meta;

    iget-object v3, v3, Lflipboard/service/Account$Meta;->c:Lflipboard/json/FLObject;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lflipboard/json/FLObject;->size()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->c(Ljava/util/List;)V

    return-void
.end method

.method static synthetic d(Lflipboard/activities/SharePromoteFragment;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lflipboard/activities/SharePromoteFragment;->l:I

    return v0
.end method

.method static synthetic e(Lflipboard/activities/SharePromoteFragment;)Lflipboard/objs/Magazine;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lflipboard/activities/SharePromoteFragment;->k:Lflipboard/objs/Magazine;

    return-object v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 318
    invoke-virtual {p0}, Lflipboard/activities/SharePromoteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 321
    if-eqz v0, :cond_0

    .line 322
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 324
    :cond_0
    return-void
.end method

.method static synthetic f(Lflipboard/activities/SharePromoteFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lflipboard/activities/SharePromoteFragment;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lflipboard/activities/SharePromoteFragment;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lflipboard/activities/SharePromoteFragment;->e()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 69
    invoke-super {p0, p1}, Lflipboard/gui/dialog/FLDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 70
    const v0, 0x7f0e0023

    invoke-virtual {p0, v0}, Lflipboard/activities/SharePromoteFragment;->a(I)V

    .line 72
    iget-object v0, p0, Lflipboard/activities/SharePromoteFragment;->o:Landroid/os/Bundle;

    .line 74
    const-string v1, "flipboard.extra.magazine.target"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 75
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 76
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 77
    if-eqz v2, :cond_0

    .line 78
    invoke-virtual {v2, v1}, Lflipboard/service/User;->l(Ljava/lang/String;)Lflipboard/objs/Magazine;

    move-result-object v1

    iput-object v1, p0, Lflipboard/activities/SharePromoteFragment;->k:Lflipboard/objs/Magazine;

    .line 81
    :cond_0
    const-string v1, "flipboard.extra.magazine.num.items"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lflipboard/activities/SharePromoteFragment;->l:I

    .line 82
    const-string v1, "flipboard.extra.reference.link"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/SharePromoteFragment;->m:Ljava/lang/String;

    .line 85
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "promote_mag_prompt"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 86
    const-string v1, "sectionIdentifier"

    iget-object v2, p0, Lflipboard/activities/SharePromoteFragment;->k:Lflipboard/objs/Magazine;

    iget-object v2, v2, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 87
    const-string v1, "count"

    iget v2, p0, Lflipboard/activities/SharePromoteFragment;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 88
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 89
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 93
    const v0, 0x7f03011f

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 96
    const v0, 0x7f0a0315

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 97
    const v2, 0x7f030121

    invoke-virtual {v0, v2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 98
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 100
    return-object v1
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 312
    invoke-super {p0}, Lflipboard/gui/dialog/FLDialogFragment;->onDestroy()V

    .line 313
    invoke-direct {p0}, Lflipboard/activities/SharePromoteFragment;->e()V

    .line 314
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 105
    invoke-super/range {p0 .. p2}, Lflipboard/gui/dialog/FLDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 107
    invoke-virtual {p0}, Lflipboard/activities/SharePromoteFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 110
    const v0, 0x7f0a0057

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 112
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_0

    const v1, 0x7f090109

    :goto_0
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 113
    const/4 v1, -0x2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 114
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 117
    const v0, 0x7f0a0058

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 118
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 119
    invoke-virtual {p0}, Lflipboard/activities/SharePromoteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0, v1, p0}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/gui/dialog/FLDialogFragment;)V

    .line 122
    const v0, 0x7f0a004f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    .line 123
    const v1, 0x7f0d0257

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    const v0, 0x7f0a0318

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 127
    iget-object v1, p0, Lflipboard/activities/SharePromoteFragment;->k:Lflipboard/objs/Magazine;

    iget-object v1, v1, Lflipboard/objs/Magazine;->t:Lflipboard/objs/Image;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 128
    const v0, 0x7f0a0319

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    .line 129
    iget-object v1, p0, Lflipboard/activities/SharePromoteFragment;->k:Lflipboard/objs/Magazine;

    iget-object v1, v1, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    const v0, 0x7f0a0050

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 133
    const v1, 0x7f0d0256

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lflipboard/activities/SharePromoteFragment;->l:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 134
    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    const v0, 0x7f0a0317

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lflipboard/gui/FLButton;

    .line 138
    new-instance v0, Lflipboard/activities/SharePromoteFragment$1;

    invoke-direct {v0, p0}, Lflipboard/activities/SharePromoteFragment$1;-><init>(Lflipboard/activities/SharePromoteFragment;)V

    invoke-virtual {v8, v0}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    const v0, 0x7f0a0316

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/IconBar;

    iget-object v0, p0, Lflipboard/activities/SharePromoteFragment;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    sget-object v10, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v10}, Lflipboard/service/FlipboardManager;->y()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_1

    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Lflipboard/gui/IconBar;->setVisibility(I)V

    .line 148
    :goto_1
    return-void

    .line 112
    :cond_0
    const v1, 0x7f090084

    goto/16 :goto_0

    .line 147
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lflipboard/gui/IconBar;->setVisibility(I)V

    iget-object v2, v10, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2}, Lflipboard/service/User;->x()Ljava/util/List;

    move-result-object v11

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lflipboard/service/Account;

    iget-object v0, v9, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v10, v0}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v13

    if-eqz v11, :cond_7

    iget-object v0, v13, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-interface {v11, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v4, 0x1

    :goto_3
    if-eqz v4, :cond_2

    iget-object v0, p0, Lflipboard/activities/SharePromoteFragment;->n:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v3, v13, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    iget-object v5, v13, Lflipboard/objs/ConfigService;->o:Ljava/lang/String;

    iget-object v6, v13, Lflipboard/objs/ConfigService;->A:Ljava/lang/String;

    new-instance v0, Lflipboard/gui/IconBar$ToggleImageButton;

    invoke-virtual {v1}, Lflipboard/gui/IconBar;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lflipboard/gui/IconBar$ToggleImageButton;-><init>(Lflipboard/gui/IconBar;Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;B)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    iget v3, v1, Lflipboard/gui/IconBar;->a:I

    iget v5, v1, Lflipboard/gui/IconBar;->b:I

    invoke-direct {v2, v3, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1}, Lflipboard/gui/IconBar;->getChildCount()I

    move-result v3

    if-lez v3, :cond_3

    invoke-virtual {v1}, Lflipboard/gui/IconBar;->getOrientation()I

    move-result v3

    if-nez v3, :cond_8

    iget v3, v1, Lflipboard/gui/IconBar;->c:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    :cond_3
    :goto_4
    invoke-virtual {v0, v2}, Lflipboard/gui/IconBar$ToggleImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    if-eqz v4, :cond_4

    iget v3, v1, Lflipboard/gui/IconBar;->d:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, Lflipboard/gui/IconBar;->d:I

    :cond_4
    invoke-virtual {v1, v0, v2}, Lflipboard/gui/IconBar;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1}, Lflipboard/gui/IconBar;->getNumSelectedIcons()I

    move-result v0

    if-lez v0, :cond_9

    const/4 v0, 0x1

    :goto_5
    invoke-virtual {v8, v0}, Lflipboard/gui/FLButton;->setEnabled(Z)V

    iget-object v0, v9, Lflipboard/service/Account;->d:Lflipboard/io/Download;

    if-eqz v0, :cond_5

    iget-object v0, v9, Lflipboard/service/Account;->e:Lflipboard/io/Download;

    if-nez v0, :cond_6

    :cond_5
    const-string v0, "SharePromoteFragment:initServicesBar"

    new-instance v2, Lflipboard/activities/SharePromoteFragment$2;

    invoke-direct {v2, p0, v13, v9, v10}, Lflipboard/activities/SharePromoteFragment$2;-><init>(Lflipboard/activities/SharePromoteFragment;Lflipboard/objs/ConfigService;Lflipboard/service/Account;Lflipboard/service/FlipboardManager;)V

    invoke-virtual {v10, v0, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    :cond_6
    invoke-static {v13, v9}, Lflipboard/util/ShareHelper;->a(Lflipboard/objs/ConfigService;Lflipboard/service/Account;)V

    goto/16 :goto_2

    :cond_7
    const/4 v4, 0x0

    goto :goto_3

    :cond_8
    const/4 v3, 0x0

    iget v5, v1, Lflipboard/gui/IconBar;->c:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_4

    :cond_9
    const/4 v0, 0x0

    goto :goto_5

    :cond_a
    new-instance v0, Lflipboard/activities/SharePromoteFragment$3;

    invoke-direct {v0, p0, v8}, Lflipboard/activities/SharePromoteFragment$3;-><init>(Lflipboard/activities/SharePromoteFragment;Lflipboard/gui/FLButton;)V

    invoke-virtual {v1, v0}, Lflipboard/gui/IconBar;->setObserver(Lflipboard/gui/IconBar$OnChangeObserver;)V

    goto/16 :goto_1
.end method
