.class Lflipboard/activities/ComposeFragment$6$2;
.super Ljava/lang/Object;
.source "ComposeFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/activities/ComposeFragment$6;


# direct methods
.method constructor <init>(Lflipboard/activities/ComposeFragment$6;)V
    .locals 0

    .prologue
    .line 657
    iput-object p1, p0, Lflipboard/activities/ComposeFragment$6$2;->a:Lflipboard/activities/ComposeFragment$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 661
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$6$2;->a:Lflipboard/activities/ComposeFragment$6;

    iget-object v0, v0, Lflipboard/activities/ComposeFragment$6;->b:Lflipboard/activities/ComposeFragment;

    invoke-virtual {v0}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 662
    if-eqz v0, :cond_1

    iget-boolean v1, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_1

    .line 663
    iget-object v1, p0, Lflipboard/activities/ComposeFragment$6$2;->a:Lflipboard/activities/ComposeFragment$6;

    iget-object v1, v1, Lflipboard/activities/ComposeFragment$6;->b:Lflipboard/activities/ComposeFragment;

    invoke-virtual {v1}, Lflipboard/activities/ComposeFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "uploading"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/DialogFragment;

    .line 664
    if-eqz v1, :cond_0

    .line 665
    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->a()V

    .line 669
    :cond_0
    iget-object v1, p0, Lflipboard/activities/ComposeFragment$6$2;->a:Lflipboard/activities/ComposeFragment$6;

    iget-object v1, v1, Lflipboard/activities/ComposeFragment$6;->b:Lflipboard/activities/ComposeFragment;

    invoke-static {v1}, Lflipboard/activities/ComposeFragment;->f(Lflipboard/activities/ComposeFragment;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 670
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f0d02e8

    :goto_0
    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 674
    :goto_1
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 675
    iget-object v1, p0, Lflipboard/activities/ComposeFragment$6$2;->a:Lflipboard/activities/ComposeFragment$6;

    iget-object v1, v1, Lflipboard/activities/ComposeFragment$6;->b:Lflipboard/activities/ComposeFragment;

    invoke-static {v1}, Lflipboard/activities/ComposeFragment;->f(Lflipboard/activities/ComposeFragment;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    const v1, 0x7f0d02ea

    :goto_2
    invoke-virtual {v2, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 676
    iput-object v0, v2, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 677
    const v0, 0x7f0d023f

    invoke-virtual {v2, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->e(I)V

    .line 678
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$6$2;->a:Lflipboard/activities/ComposeFragment$6;

    iget-object v0, v0, Lflipboard/activities/ComposeFragment$6;->b:Lflipboard/activities/ComposeFragment;

    invoke-virtual {v0}, Lflipboard/activities/ComposeFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "error"

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 680
    :cond_1
    return-void

    .line 670
    :cond_2
    const v1, 0x7f0d02e9

    goto :goto_0

    .line 672
    :cond_3
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    const v1, 0x7f0d008f

    :goto_3
    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    const v1, 0x7f0d0090

    goto :goto_3

    .line 675
    :cond_5
    const v1, 0x7f0d0091

    goto :goto_2
.end method
