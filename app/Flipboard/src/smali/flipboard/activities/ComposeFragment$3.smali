.class Lflipboard/activities/ComposeFragment$3;
.super Ljava/lang/Object;
.source "ComposeFragment.java"

# interfaces
.implements Lflipboard/service/Flap$CancellableJSONResultObserver;


# instance fields
.field final synthetic a:Landroid/widget/TextView;

.field final synthetic b:I

.field final synthetic c:Lflipboard/activities/ComposeFragment;

.field private d:Z

.field private e:I

.field private f:I


# direct methods
.method constructor <init>(Lflipboard/activities/ComposeFragment;Landroid/widget/TextView;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 436
    iput-object p1, p0, Lflipboard/activities/ComposeFragment$3;->c:Lflipboard/activities/ComposeFragment;

    iput-object p2, p0, Lflipboard/activities/ComposeFragment$3;->a:Landroid/widget/TextView;

    iput p3, p0, Lflipboard/activities/ComposeFragment$3;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 437
    iput-boolean v0, p0, Lflipboard/activities/ComposeFragment$3;->d:Z

    .line 438
    iput v0, p0, Lflipboard/activities/ComposeFragment$3;->e:I

    .line 439
    iput v0, p0, Lflipboard/activities/ComposeFragment$3;->f:I

    return-void
.end method

.method static synthetic a(Lflipboard/activities/ComposeFragment$3;)I
    .locals 1

    .prologue
    .line 436
    iget v0, p0, Lflipboard/activities/ComposeFragment$3;->e:I

    return v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 493
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$3;->c:Lflipboard/activities/ComposeFragment;

    invoke-static {v0}, Lflipboard/activities/ComposeFragment;->c(Lflipboard/activities/ComposeFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v0

    new-instance v1, Lflipboard/activities/ComposeFragment$3$3;

    invoke-direct {v1, p0}, Lflipboard/activities/ComposeFragment$3$3;-><init>(Lflipboard/activities/ComposeFragment$3;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 509
    return-void
.end method

.method static synthetic b(Lflipboard/activities/ComposeFragment$3;)I
    .locals 1

    .prologue
    .line 436
    iget v0, p0, Lflipboard/activities/ComposeFragment$3;->f:I

    return v0
.end method


# virtual methods
.method public final declared-synchronized a(Lflipboard/json/FLObject;)V
    .locals 4

    .prologue
    .line 448
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lflipboard/activities/ComposeFragment$3;->d:Z

    if-nez v0, :cond_0

    .line 449
    iget v0, p0, Lflipboard/activities/ComposeFragment$3;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/activities/ComposeFragment$3;->e:I

    .line 450
    const-string v0, "meta"

    invoke-virtual {p1, v0}, Lflipboard/json/FLObject;->f(Ljava/lang/String;)Lflipboard/json/FLObject;

    move-result-object v0

    .line 451
    const-string v1, "url"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 452
    const-string v1, "result"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 453
    invoke-static {}, Lflipboard/activities/ComposeFragment;->c()Lflipboard/util/Log;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v3, 0x1

    aput-object v1, v2, v3

    .line 454
    if-nez v0, :cond_1

    .line 455
    invoke-static {}, Lflipboard/activities/ComposeFragment;->c()Lflipboard/util/Log;

    move-result-object v0

    const-string v1, "Server gave us a null long url in the result, object: "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 456
    iget v0, p0, Lflipboard/activities/ComposeFragment$3;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/activities/ComposeFragment$3;->f:I

    .line 457
    invoke-direct {p0}, Lflipboard/activities/ComposeFragment$3;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 481
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 460
    :cond_1
    :try_start_1
    iget-object v2, p0, Lflipboard/activities/ComposeFragment$3;->c:Lflipboard/activities/ComposeFragment;

    invoke-static {v2}, Lflipboard/activities/ComposeFragment;->c(Lflipboard/activities/ComposeFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v2

    new-instance v3, Lflipboard/activities/ComposeFragment$3$1;

    invoke-direct {v3, p0, v0, v1}, Lflipboard/activities/ComposeFragment$3$1;-><init>(Lflipboard/activities/ComposeFragment$3;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 469
    iget-object v2, p0, Lflipboard/activities/ComposeFragment$3;->c:Lflipboard/activities/ComposeFragment;

    invoke-static {v2}, Lflipboard/activities/ComposeFragment;->d(Lflipboard/activities/ComposeFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 470
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$3;->c:Lflipboard/activities/ComposeFragment;

    invoke-static {v0, v1}, Lflipboard/activities/ComposeFragment;->a(Lflipboard/activities/ComposeFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 471
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$3;->c:Lflipboard/activities/ComposeFragment;

    invoke-static {v0}, Lflipboard/activities/ComposeFragment;->c(Lflipboard/activities/ComposeFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v0

    new-instance v1, Lflipboard/activities/ComposeFragment$3$2;

    invoke-direct {v1, p0}, Lflipboard/activities/ComposeFragment$3$2;-><init>(Lflipboard/activities/ComposeFragment$3;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 479
    :cond_2
    invoke-direct {p0}, Lflipboard/activities/ComposeFragment$3;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 448
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 485
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lflipboard/activities/ComposeFragment$3;->d:Z

    if-nez v0, :cond_0

    .line 486
    iget v0, p0, Lflipboard/activities/ComposeFragment$3;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/activities/ComposeFragment$3;->f:I

    .line 487
    invoke-direct {p0}, Lflipboard/activities/ComposeFragment$3;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 489
    :cond_0
    monitor-exit p0

    return-void

    .line 485
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 443
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/ComposeFragment$3;->d:Z

    .line 444
    return-void
.end method
