.class public Lflipboard/activities/SearchTabletActivity;
.super Lflipboard/activities/ContentDrawerActivity;
.source "SearchTabletActivity.java"


# instance fields
.field protected s:Lflipboard/gui/FLSearchView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lflipboard/activities/ContentDrawerActivity;-><init>()V

    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 115
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_0

    .line 117
    const-string v1, "search_requested"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    const-string v1, "search_term"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 119
    if-eqz v0, :cond_1

    .line 120
    iget-object v1, p0, Lflipboard/activities/SearchTabletActivity;->s:Lflipboard/gui/FLSearchView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLSearchView;->setSearchQuery(Ljava/lang/String;)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    iget-object v0, p0, Lflipboard/activities/SearchTabletActivity;->s:Lflipboard/gui/FLSearchView;

    invoke-virtual {v0}, Lflipboard/gui/FLSearchView;->c()V

    goto :goto_0
.end method


# virtual methods
.method protected final f()V
    .locals 2

    .prologue
    .line 77
    const v0, 0x7f040012

    const v1, 0x7f04001b

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/SearchTabletActivity;->overridePendingTransition(II)V

    .line 78
    return-void
.end method

.method protected final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    return-object v0
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 97
    invoke-virtual {p0}, Lflipboard/activities/SearchTabletActivity;->finish()V

    .line 98
    const v0, 0x7f040012

    const v1, 0x7f04001b

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/SearchTabletActivity;->overridePendingTransition(II)V

    .line 99
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 59
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/ContentDrawerActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 72
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lflipboard/activities/SearchTabletActivity;->s:Lflipboard/gui/FLSearchView;

    invoke-virtual {v0}, Lflipboard/gui/FLSearchView;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    invoke-virtual {p0}, Lflipboard/activities/SearchTabletActivity;->m()V

    .line 87
    :cond_0
    return-void
.end method

.method public onClickOutside(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 91
    invoke-virtual {p0}, Lflipboard/activities/SearchTabletActivity;->m()V

    .line 92
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 27
    const v0, 0x7f030101

    invoke-virtual {p0, v0}, Lflipboard/activities/SearchTabletActivity;->setContentView(I)V

    .line 29
    invoke-super {p0, p1}, Lflipboard/activities/ContentDrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    const v0, 0x7f0a010b

    invoke-virtual {p0, v0}, Lflipboard/activities/SearchTabletActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLSearchView;

    iput-object v0, p0, Lflipboard/activities/SearchTabletActivity;->s:Lflipboard/gui/FLSearchView;

    .line 32
    invoke-virtual {p0}, Lflipboard/activities/SearchTabletActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 33
    if-eqz v0, :cond_0

    .line 34
    const-string v1, "extra_origin_section_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 35
    if-eqz v1, :cond_0

    .line 36
    iget-object v2, p0, Lflipboard/activities/SearchTabletActivity;->s:Lflipboard/gui/FLSearchView;

    invoke-virtual {v2, v1}, Lflipboard/gui/FLSearchView;->setOriginSectionId(Ljava/lang/String;)V

    .line 40
    :cond_0
    invoke-virtual {p0, p1}, Lflipboard/activities/SearchTabletActivity;->a(Landroid/os/Bundle;)V

    .line 41
    invoke-direct {p0, v0}, Lflipboard/activities/SearchTabletActivity;->b(Landroid/content/Intent;)V

    .line 42
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 130
    invoke-super {p0}, Lflipboard/activities/ContentDrawerActivity;->onDestroy()V

    .line 131
    iget-object v0, p0, Lflipboard/activities/SearchTabletActivity;->s:Lflipboard/gui/FLSearchView;

    invoke-virtual {v0}, Lflipboard/gui/FLSearchView;->b()V

    .line 132
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 52
    invoke-super {p0, p1}, Lflipboard/activities/ContentDrawerActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 53
    invoke-direct {p0, p1}, Lflipboard/activities/SearchTabletActivity;->b(Landroid/content/Intent;)V

    .line 54
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 109
    invoke-super {p0, p1}, Lflipboard/activities/ContentDrawerActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 110
    iget-object v0, p0, Lflipboard/activities/SearchTabletActivity;->s:Lflipboard/gui/FLSearchView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLSearchView;->b(Landroid/os/Bundle;)V

    .line 111
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 46
    invoke-super {p0}, Lflipboard/activities/ContentDrawerActivity;->onResume()V

    .line 47
    iget-object v0, p0, Lflipboard/activities/SearchTabletActivity;->s:Lflipboard/gui/FLSearchView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lflipboard/gui/FLSearchView;->setActiveTimeInMills(J)V

    .line 48
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0, p1}, Lflipboard/activities/ContentDrawerActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 104
    iget-object v0, p0, Lflipboard/activities/SearchTabletActivity;->s:Lflipboard/gui/FLSearchView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLSearchView;->a(Landroid/os/Bundle;)V

    .line 105
    return-void
.end method

.method protected final r_()Z
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    return v0
.end method
