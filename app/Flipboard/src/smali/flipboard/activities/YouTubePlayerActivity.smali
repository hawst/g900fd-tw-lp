.class public Lflipboard/activities/YouTubePlayerActivity;
.super Lflipboard/activities/FeedActivity;
.source "YouTubePlayerActivity.java"

# interfaces
.implements Lcom/google/android/youtube/player/YouTubePlayer$OnFullscreenListener;
.implements Lcom/google/android/youtube/player/YouTubePlayer$OnInitializedListener;


# static fields
.field private static n:Lflipboard/util/Log;


# instance fields
.field private o:Lcom/google/android/youtube/player/YouTubePlayer;

.field private p:Ljava/lang/String;

.field private q:Lflipboard/gui/actionbar/FLActionBar;

.field private r:Lflipboard/objs/ConfigService;

.field private s:Lcom/google/android/youtube/player/YouTubePlayerSupportFragment;

.field private t:D

.field private u:D

.field private v:Lflipboard/io/UsageEvent;

.field private w:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lflipboard/util/YouTubeHelper;->a:Lflipboard/util/Log;

    sput-object v0, Lflipboard/activities/YouTubePlayerActivity;->n:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lflipboard/activities/FeedActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/player/YouTubeInitializationResult;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 112
    sget-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult$1;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/player/YouTubeInitializationResult;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_0

    .line 113
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult$1;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/player/YouTubeInitializationResult;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    const/4 v0, 0x0

    :goto_1
    new-instance v2, Lcom/google/android/youtube/player/YouTubeInitializationResult$a;

    invoke-direct {v2, p0, v0}, Lcom/google/android/youtube/player/YouTubeInitializationResult$a;-><init>(Landroid/app/Activity;Landroid/content/Intent;)V

    new-instance v0, Lcom/google/android/youtube/player/internal/m;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/player/internal/m;-><init>(Landroid/content/Context;)V

    sget-object v3, Lcom/google/android/youtube/player/YouTubeInitializationResult$1;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/player/YouTubeInitializationResult;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected errorReason: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/youtube/player/YouTubeInitializationResult;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    move v0, v1

    .line 112
    goto :goto_0

    .line 113
    :pswitch_1
    invoke-static {p0}, Lcom/google/android/youtube/player/internal/z;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/player/internal/z;->b(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    invoke-static {p0}, Lcom/google/android/youtube/player/internal/z;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/player/internal/z;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :pswitch_3
    iget-object v3, v0, Lcom/google/android/youtube/player/internal/m;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v3, v0, Lcom/google/android/youtube/player/internal/m;->c:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/youtube/player/internal/m;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 118
    :goto_3
    return-void

    .line 113
    :pswitch_4
    iget-object v3, v0, Lcom/google/android/youtube/player/internal/m;->e:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v3, v0, Lcom/google/android/youtube/player/internal/m;->f:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/youtube/player/internal/m;->g:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_2

    :pswitch_5
    iget-object v3, v0, Lcom/google/android/youtube/player/internal/m;->h:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v3, v0, Lcom/google/android/youtube/player/internal/m;->i:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/youtube/player/internal/m;->j:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_2

    .line 115
    :cond_0
    const v0, 0x7f0d00dd

    invoke-virtual {p0, v0}, Lflipboard/activities/YouTubePlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/youtube/player/YouTubeInitializationResult;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_3

    .line 112
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 113
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lcom/google/android/youtube/player/YouTubePlayer;Z)V
    .locals 1

    .prologue
    .line 130
    iput-object p1, p0, Lflipboard/activities/YouTubePlayerActivity;->o:Lcom/google/android/youtube/player/YouTubePlayer;

    .line 132
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Lcom/google/android/youtube/player/YouTubePlayer;->a(I)V

    .line 133
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Lcom/google/android/youtube/player/YouTubePlayer;->a(I)V

    .line 134
    invoke-interface {p1, p0}, Lcom/google/android/youtube/player/YouTubePlayer;->a(Lcom/google/android/youtube/player/YouTubePlayer$OnFullscreenListener;)V

    .line 135
    if-nez p2, :cond_0

    .line 136
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->p:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/google/android/youtube/player/YouTubePlayer;->a(Ljava/lang/String;)V

    .line 138
    :cond_0
    return-void
.end method

.method public final a_(Z)V
    .locals 2

    .prologue
    .line 142
    if-eqz p1, :cond_1

    .line 143
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->q:Lflipboard/gui/actionbar/FLActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->q:Lflipboard/gui/actionbar/FLActionBar;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->q:Lflipboard/gui/actionbar/FLActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->q:Lflipboard/gui/actionbar/FLActionBar;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    const/4 v12, 0x2

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    const-wide/16 v8, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 297
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v2, v2, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v2, :cond_8

    .line 299
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 300
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    if-lt v3, v12, :cond_0

    .line 301
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 302
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 303
    float-to-double v6, v3

    invoke-static {v6, v7, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    float-to-double v4, v4

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    iput-wide v4, p0, Lflipboard/activities/YouTubePlayerActivity;->u:D

    .line 306
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    if-ne v3, v12, :cond_1

    if-eqz v2, :cond_2

    :cond_1
    const/16 v3, 0x105

    if-ne v2, v3, :cond_3

    .line 307
    :cond_2
    iget-wide v4, p0, Lflipboard/activities/YouTubePlayerActivity;->u:D

    iput-wide v4, p0, Lflipboard/activities/YouTubePlayerActivity;->t:D

    .line 309
    :cond_3
    if-eq v2, v0, :cond_4

    and-int/lit8 v2, v2, 0x6

    const/4 v3, 0x6

    if-ne v2, v3, :cond_6

    .line 310
    :cond_4
    iget-wide v2, p0, Lflipboard/activities/YouTubePlayerActivity;->t:D

    cmpl-double v2, v2, v8

    if-lez v2, :cond_5

    iget-wide v2, p0, Lflipboard/activities/YouTubePlayerActivity;->u:D

    iget-wide v4, p0, Lflipboard/activities/YouTubePlayerActivity;->t:D

    const-wide v6, 0x3ff199999999999aL    # 1.1

    mul-double/2addr v4, v6

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_5

    .line 311
    invoke-virtual {p0}, Lflipboard/activities/YouTubePlayerActivity;->finish()V

    .line 329
    :goto_0
    return v0

    .line 314
    :cond_5
    iput-wide v8, p0, Lflipboard/activities/YouTubePlayerActivity;->t:D

    .line 318
    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-le v2, v0, :cond_7

    move v0, v1

    .line 319
    goto :goto_0

    .line 322
    :cond_7
    :try_start_0
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 324
    :catch_0
    move-exception v0

    .line 325
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v2, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    move v0, v1

    .line 326
    goto :goto_0

    .line 329
    :cond_8
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final f()V
    .locals 2

    .prologue
    .line 292
    const/4 v0, 0x0

    const v1, 0x7f040004

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/YouTubePlayerActivity;->overridePendingTransition(II)V

    .line 293
    return-void
.end method

.method public finish()V
    .locals 8

    .prologue
    .line 335
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 337
    iget-wide v0, p0, Lflipboard/activities/YouTubePlayerActivity;->V:J

    .line 338
    iget-wide v4, p0, Lflipboard/activities/YouTubePlayerActivity;->R:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 339
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lflipboard/activities/YouTubePlayerActivity;->R:J

    sub-long/2addr v4, v6

    add-long/2addr v0, v4

    .line 341
    :cond_0
    const-string v3, "extra_result_active_time"

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 343
    const-string v0, "usage_intent_extra_flipcount"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 344
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v2}, Lflipboard/activities/YouTubePlayerActivity;->setResult(ILandroid/content/Intent;)V

    .line 345
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->finish()V

    .line 346
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 350
    const-string v0, "item"

    return-object v0
.end method

.method public final l()Lflipboard/gui/actionbar/FLActionBar;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->q:Lflipboard/gui/actionbar/FLActionBar;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 124
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 126
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const v3, 0x7f0a004c

    const/4 v10, 0x3

    const v9, 0x7f0d02f7

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 44
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    sget-object v0, Lflipboard/activities/YouTubePlayerActivity;->n:Lflipboard/util/Log;

    .line 48
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->C:Lflipboard/service/Section;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->D:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 50
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->D:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->r:Lflipboard/objs/ConfigService;

    .line 53
    :cond_0
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->D:Lflipboard/objs/FeedItem;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lflipboard/activities/YouTubePlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    .line 55
    invoke-virtual {p0}, Lflipboard/activities/YouTubePlayerActivity;->finish()V

    .line 108
    :goto_0
    return-void

    .line 59
    :cond_1
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "viewed"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->v:Lflipboard/io/UsageEvent;

    .line 60
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->v:Lflipboard/io/UsageEvent;

    const-string v1, "itemType"

    const-string v2, "video"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 61
    iget-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->v:Lflipboard/io/UsageEvent;

    const-string v2, "sourceURL"

    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->D:Lflipboard/objs/FeedItem;

    if-nez v0, :cond_b

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v2, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 62
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->v:Lflipboard/io/UsageEvent;

    const-string v1, "videoType"

    sget-object v2, Lflipboard/util/VideoUtil$VideoType;->c:Lflipboard/util/VideoUtil$VideoType;

    invoke-static {v2}, Lflipboard/util/VideoUtil;->a(Lflipboard/util/VideoUtil$VideoType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 63
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->v:Lflipboard/io/UsageEvent;

    const-string v1, "itemFlipCount"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 64
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->v:Lflipboard/io/UsageEvent;

    const-string v1, "deprecated"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 65
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->D:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_2

    .line 66
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->v:Lflipboard/io/UsageEvent;

    const-string v1, "partnerID"

    iget-object v2, p0, Lflipboard/activities/YouTubePlayerActivity;->D:Lflipboard/objs/FeedItem;

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->aa()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 69
    :cond_2
    const v0, 0x7f030155

    invoke-virtual {p0, v0}, Lflipboard/activities/YouTubePlayerActivity;->setContentView(I)V

    .line 71
    invoke-virtual {p0, v3}, Lflipboard/activities/YouTubePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    iput-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->q:Lflipboard/gui/actionbar/FLActionBar;

    .line 72
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->q:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0, v5}, Lflipboard/gui/actionbar/FLActionBar;->setInverted(Z)V

    .line 73
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->q:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->f()Landroid/view/View;

    .line 74
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->q:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->g()V

    .line 76
    invoke-virtual {p0, v3}, Lflipboard/activities/YouTubePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    iput-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->q:Lflipboard/gui/actionbar/FLActionBar;

    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->q:Lflipboard/gui/actionbar/FLActionBar;

    if-eqz v0, :cond_5

    new-instance v0, Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-direct {v0, p0}, Lflipboard/gui/actionbar/FLActionBarMenu;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->D:Lflipboard/objs/FeedItem;

    if-eqz v1, :cond_4

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_c

    iget-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->D:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v3

    iget-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {}, Lflipboard/service/User;->u()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v1

    invoke-static {v1}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0, v10, v9}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(II)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    iget-object v2, p0, Lflipboard/activities/YouTubePlayerActivity;->r:Lflipboard/objs/ConfigService;

    invoke-static {v2}, Lflipboard/util/AndroidUtil;->c(Lflipboard/objs/ConfigService;)I

    move-result v2

    invoke-virtual {v1, v2}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    sget-object v2, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    invoke-virtual {v1, v2}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    new-instance v2, Lflipboard/activities/YouTubePlayerActivity$1;

    invoke-direct {v2, p0, v3}, Lflipboard/activities/YouTubePlayerActivity$1;-><init>(Lflipboard/activities/YouTubePlayerActivity;Lflipboard/objs/FeedItem;)V

    iput-object v2, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    iput-boolean v5, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    invoke-virtual {v1, v5}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v9}, Lflipboard/activities/YouTubePlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_3
    iget-object v2, p0, Lflipboard/activities/YouTubePlayerActivity;->C:Lflipboard/service/Section;

    sget-object v4, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    iget-object v6, p0, Lflipboard/activities/YouTubePlayerActivity;->D:Lflipboard/objs/FeedItem;

    move-object v1, p0

    invoke-virtual/range {v0 .. v6}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;ZLflipboard/objs/FeedItem;)V

    iget-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->C:Lflipboard/service/Section;

    invoke-virtual {p0, v0, v3, v1, p0}, Lflipboard/activities/YouTubePlayerActivity;->a(Lflipboard/gui/actionbar/FLActionBarMenu;Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;)V

    :cond_4
    :goto_2
    iget-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->q:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v1, v0}, Lflipboard/gui/actionbar/FLActionBar;->setMenu(Lflipboard/gui/actionbar/FLActionBarMenu;)V

    .line 78
    :cond_5
    invoke-virtual {p0}, Lflipboard/activities/YouTubePlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 79
    if-eqz v0, :cond_6

    const-string v1, "youtube_video_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 80
    const-string v1, "youtube_video_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->p:Ljava/lang/String;

    .line 82
    :cond_6
    iget-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->p:Ljava/lang/String;

    if-nez v1, :cond_7

    iget-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->D:Lflipboard/objs/FeedItem;

    if-nez v1, :cond_7

    .line 84
    invoke-virtual {p0}, Lflipboard/activities/YouTubePlayerActivity;->finish()V

    .line 87
    :cond_7
    iget-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->s:Lcom/google/android/youtube/player/YouTubePlayerSupportFragment;

    if-nez v1, :cond_8

    .line 88
    invoke-static {p0}, Lflipboard/util/YouTubeHelper;->a(Lcom/google/android/youtube/player/YouTubePlayer$OnInitializedListener;)Lcom/google/android/youtube/player/YouTubePlayerSupportFragment;

    move-result-object v1

    iput-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->s:Lcom/google/android/youtube/player/YouTubePlayerSupportFragment;

    .line 91
    :cond_8
    if-eqz v0, :cond_a

    .line 92
    const-string v1, "fromSection"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lflipboard/activities/YouTubePlayerActivity;->w:Z

    .line 93
    iget-boolean v1, p0, Lflipboard/activities/YouTubePlayerActivity;->w:Z

    if-eqz v1, :cond_a

    .line 94
    const-string v1, "extra_content_discovery_from_source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 95
    const/4 v0, 0x0

    .line 96
    if-eqz v1, :cond_9

    .line 97
    const-string v0, "source"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 100
    :cond_9
    iget-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->D:Lflipboard/objs/FeedItem;

    iget-object v2, p0, Lflipboard/activities/YouTubePlayerActivity;->C:Lflipboard/service/Section;

    invoke-static {v1, v2, v0}, Lflipboard/activities/DetailActivity;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Ljava/lang/String;)V

    .line 104
    :cond_a
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    .line 105
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 106
    const v1, 0x7f0a0390

    iget-object v2, p0, Lflipboard/activities/YouTubePlayerActivity;->s:Lcom/google/android/youtube/player/YouTubePlayerSupportFragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 107
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    goto/16 :goto_0

    .line 61
    :cond_b
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->D:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    goto/16 :goto_1

    .line 76
    :cond_c
    iget-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->D:Lflipboard/objs/FeedItem;

    if-eqz v1, :cond_f

    iget-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->D:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v1

    iget-object v2, p0, Lflipboard/activities/YouTubePlayerActivity;->q:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v2, v5}, Lflipboard/gui/actionbar/FLActionBar;->setInverted(Z)V

    iget-object v2, p0, Lflipboard/activities/YouTubePlayerActivity;->q:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v2}, Lflipboard/gui/actionbar/FLActionBar;->g()V

    iget-object v2, p0, Lflipboard/activities/YouTubePlayerActivity;->r:Lflipboard/objs/ConfigService;

    invoke-virtual {v1, v2}, Lflipboard/objs/FeedItem;->a(Lflipboard/objs/ConfigService;)Z

    move-result v2

    if-eqz v2, :cond_d

    iget-object v2, p0, Lflipboard/activities/YouTubePlayerActivity;->r:Lflipboard/objs/ConfigService;

    invoke-static {p0, v2}, Lflipboard/gui/SocialFormatter;->d(Landroid/content/Context;Lflipboard/objs/ConfigService;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lflipboard/util/JavaUtil;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v3, v0

    move v6, v4

    move v8, v4

    invoke-virtual/range {v3 .. v8}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIILjava/lang/CharSequence;I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v2

    iget-object v3, p0, Lflipboard/activities/YouTubePlayerActivity;->r:Lflipboard/objs/ConfigService;

    invoke-static {v3}, Lflipboard/util/AndroidUtil;->a(Lflipboard/objs/ConfigService;)I

    move-result v3

    invoke-virtual {v2, v3}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    sget-object v3, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    invoke-virtual {v2, v3}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    iput-boolean v5, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->p:Z

    iget-boolean v3, v1, Lflipboard/objs/FeedItem;->af:Z

    invoke-virtual {v2, v3}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Z)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    iput-boolean v5, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    invoke-virtual {v2, v5}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    new-instance v3, Lflipboard/activities/YouTubePlayerActivity$2;

    invoke-direct {v3, p0, v1, v0}, Lflipboard/activities/YouTubePlayerActivity$2;-><init>(Lflipboard/activities/YouTubePlayerActivity;Lflipboard/objs/FeedItem;Lflipboard/gui/actionbar/FLActionBarMenu;)V

    iput-object v3, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    :cond_d
    iget-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->D:Lflipboard/objs/FeedItem;

    invoke-static {v1}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;)Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-virtual {v0, v10, v9}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(II)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    iget-object v2, p0, Lflipboard/activities/YouTubePlayerActivity;->r:Lflipboard/objs/ConfigService;

    invoke-static {v2}, Lflipboard/util/AndroidUtil;->c(Lflipboard/objs/ConfigService;)I

    move-result v2

    invoke-virtual {v1, v2}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    sget-object v2, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    invoke-virtual {v1, v2}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    new-instance v2, Lflipboard/activities/YouTubePlayerActivity$3;

    invoke-direct {v2, p0}, Lflipboard/activities/YouTubePlayerActivity$3;-><init>(Lflipboard/activities/YouTubePlayerActivity;)V

    iput-object v2, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    iput-boolean v5, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    invoke-virtual {v1, v5}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    :cond_e
    iget-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->D:Lflipboard/objs/FeedItem;

    iget-object v2, p0, Lflipboard/activities/YouTubePlayerActivity;->r:Lflipboard/objs/ConfigService;

    invoke-virtual {p0, v0, v1, v2, v5}, Lflipboard/activities/YouTubePlayerActivity;->a(Lflipboard/gui/actionbar/FLActionBarMenu;Lflipboard/objs/FeedItem;Lflipboard/objs/ConfigService;Z)V

    :cond_f
    invoke-super {p0, v0}, Lflipboard/activities/FeedActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    goto/16 :goto_2
.end method

.method protected onDestroy()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    .line 163
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->o:Lcom/google/android/youtube/player/YouTubePlayer;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->o:Lcom/google/android/youtube/player/YouTubePlayer;

    invoke-interface {v0}, Lcom/google/android/youtube/player/YouTubePlayer;->a()V

    .line 165
    iput-object v6, p0, Lflipboard/activities/YouTubePlayerActivity;->o:Lcom/google/android/youtube/player/YouTubePlayer;

    .line 167
    :cond_0
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->v:Lflipboard/io/UsageEvent;

    if-eqz v0, :cond_2

    .line 168
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->p:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->v:Lflipboard/io/UsageEvent;

    const-string v1, "youtubeId"

    iget-object v3, p0, Lflipboard/activities/YouTubePlayerActivity;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 171
    :cond_1
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->v:Lflipboard/io/UsageEvent;

    iget-wide v4, p0, Lflipboard/activities/YouTubePlayerActivity;->V:J

    iput-wide v4, v0, Lflipboard/io/UsageEvent;->g:J

    .line 173
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->v:Lflipboard/io/UsageEvent;

    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 176
    :cond_2
    iget-boolean v0, p0, Lflipboard/activities/YouTubePlayerActivity;->w:Z

    if-eqz v0, :cond_3

    .line 177
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->D:Lflipboard/objs/FeedItem;

    iget-object v1, p0, Lflipboard/activities/YouTubePlayerActivity;->C:Lflipboard/service/Section;

    iget-wide v4, p0, Lflipboard/activities/YouTubePlayerActivity;->V:J

    move v3, v2

    invoke-static/range {v0 .. v6}, Lflipboard/activities/DetailActivity;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;IIJLjava/lang/String;)V

    .line 179
    :cond_3
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onDestroy()V

    .line 180
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 152
    :try_start_0
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->o:Lcom/google/android/youtube/player/YouTubePlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->o:Lcom/google/android/youtube/player/YouTubePlayer;

    invoke-interface {v0}, Lcom/google/android/youtube/player/YouTubePlayer;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    iget-object v0, p0, Lflipboard/activities/YouTubePlayerActivity;->o:Lcom/google/android/youtube/player/YouTubePlayer;

    invoke-interface {v0}, Lcom/google/android/youtube/player/YouTubePlayer;->b()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    :cond_0
    :goto_0
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onPause()V

    .line 159
    return-void

    .line 155
    :catch_0
    move-exception v0

    .line 156
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
