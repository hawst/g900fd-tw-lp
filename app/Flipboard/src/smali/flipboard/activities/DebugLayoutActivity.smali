.class public Lflipboard/activities/DebugLayoutActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "DebugLayoutActivity.java"


# instance fields
.field private n:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 127
    return-void
.end method

.method static synthetic a(Lflipboard/activities/DebugLayoutActivity;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lflipboard/activities/DebugLayoutActivity;->n:I

    return v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const v3, 0x7f0a0144

    const v2, 0x7f03011a

    .line 39
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lflipboard/activities/DebugLayoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/activities/DebugLayoutActivity;->n:I

    .line 41
    invoke-virtual {p0, v2}, Lflipboard/activities/DebugLayoutActivity;->setContentView(I)V

    .line 42
    const/4 v0, 0x0

    invoke-static {p0, v2, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 43
    new-instance v2, Lflipboard/activities/DebugLayoutActivity$Adapter;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lflipboard/activities/DebugLayoutActivity$Adapter;-><init>(Lflipboard/activities/DebugLayoutActivity;B)V

    .line 44
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_0

    .line 45
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 46
    new-instance v1, Landroid/widget/GridView;

    invoke-direct {v1, p0}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    .line 47
    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 48
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 49
    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 50
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 56
    :goto_0
    const v1, 0x7f0a0310

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLLabelTextView;

    const-string v2, "Layout templates"

    invoke-virtual {v1, v2}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    invoke-virtual {p0, v0}, Lflipboard/activities/DebugLayoutActivity;->setContentView(Landroid/view/View;)V

    .line 58
    invoke-virtual {p0}, Lflipboard/activities/DebugLayoutActivity;->l()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 60
    return-void

    .line 52
    :cond_0
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 53
    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 54
    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method
