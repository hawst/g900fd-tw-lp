.class Lflipboard/activities/FirstRunActivity$5;
.super Ljava/lang/Object;
.source "FirstRunActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/activities/FirstRunActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/FirstRunActivity;)V
    .locals 0

    .prologue
    .line 417
    iput-object p1, p0, Lflipboard/activities/FirstRunActivity$5;->a:Lflipboard/activities/FirstRunActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;JJ)V
    .locals 7

    .prologue
    const/4 v1, 0x3

    .line 436
    new-instance v0, Landroid/content/Intent;

    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const-class v3, Lflipboard/activities/FirstRunActivity$AlarmReceiver;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 437
    const-string v2, "extra_alarm_action"

    invoke-virtual {v0, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 438
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v3, 0x41f15

    const/high16 v4, 0x10000000

    invoke-static {v2, v3, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 440
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const-string v2, "alarm"

    invoke-virtual {v0, v2}, Lflipboard/app/FlipboardApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 441
    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-lez v2, :cond_0

    .line 442
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 446
    :goto_0
    return-void

    .line 444
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v2, p1

    invoke-virtual {v0, v1, v2, v3, v6}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    .line 421
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->v()Lflipboard/model/ConfigSetting;

    move-result-object v0

    .line 423
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-object v1, v1, Lflipboard/app/FlipboardApplication;->o:Ljava/util/Locale;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 424
    if-eqz v1, :cond_1

    iget-boolean v1, v0, Lflipboard/model/ConfigSetting;->FirstRunSurveyNotificationEnabled:Z

    if-eqz v1, :cond_1

    .line 425
    const-string v0, "alarm_action_survey"

    const-wide/32 v2, 0xea60

    const-wide/16 v4, -0x1

    invoke-static {v0, v2, v3, v4, v5}, Lflipboard/activities/FirstRunActivity$5;->a(Ljava/lang/String;JJ)V

    .line 433
    :cond_0
    :goto_0
    return-void

    .line 426
    :cond_1
    iget-boolean v1, v0, Lflipboard/model/ConfigSetting;->FirstRunNotificationEnabled:Z

    if-eqz v1, :cond_0

    .line 428
    iget-wide v2, v0, Lflipboard/model/ConfigSetting;->FirstRunNotificationInitialDelay:J

    mul-long/2addr v2, v4

    .line 429
    iget-wide v0, v0, Lflipboard/model/ConfigSetting;->FirstRunNotificationRepeatDelay:J

    mul-long/2addr v0, v4

    .line 430
    const-string v4, "alarm_action_reminder"

    invoke-static {v4, v2, v3, v0, v1}, Lflipboard/activities/FirstRunActivity$5;->a(Ljava/lang/String;JJ)V

    .line 431
    sget-object v0, Lflipboard/objs/UsageEventV2$EventAction;->u:Lflipboard/objs/UsageEventV2$EventAction;

    invoke-static {v0}, Lflipboard/activities/FirstRunActivity;->a(Lflipboard/objs/UsageEventV2$EventAction;)V

    goto :goto_0
.end method
