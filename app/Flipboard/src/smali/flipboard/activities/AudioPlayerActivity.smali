.class public Lflipboard/activities/AudioPlayerActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "AudioPlayerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/activities/FlipboardActivity;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/SeekBar$OnSeekBarChangeListener;",
        "Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/audio/FLAudioManager;",
        "Lflipboard/service/audio/FLAudioManager$AudioMessage;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static n:Lflipboard/util/Log;


# instance fields
.field private A:Lflipboard/gui/FLCameleonImageView;

.field private B:Landroid/view/View;

.field private C:Z

.field private D:Ljava/util/Timer;

.field private o:Lflipboard/service/audio/FLAudioManager;

.field private p:Lflipboard/gui/FLStaticTextView;

.field private q:Lflipboard/gui/FLStaticTextView;

.field private r:Lflipboard/gui/FLImageView;

.field private s:Lflipboard/gui/FLToggleImageButton;

.field private t:Landroid/widget/ImageButton;

.field private u:Landroid/widget/ImageButton;

.field private v:Lflipboard/gui/FLLabelTextView;

.field private w:Lflipboard/gui/FLLabelTextView;

.field private x:Lflipboard/gui/FLBusyView;

.field private y:Landroid/widget/SeekBar;

.field private z:Lflipboard/objs/FeedItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->b:Lflipboard/util/Log;

    sput-object v0, Lflipboard/activities/AudioPlayerActivity;->n:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 36
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->z()Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    .line 690
    return-void
.end method

.method private I()V
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->D:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->D:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 457
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->D:Ljava/util/Timer;

    .line 459
    :cond_0
    return-void
.end method

.method private J()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 466
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->y:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 468
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->w:Lflipboard/gui/FLLabelTextView;

    invoke-static {v6, v7}, Lflipboard/activities/AudioPlayerActivity$AudioPlayerUtility;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 470
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->v:Lflipboard/gui/FLLabelTextView;

    const-string v1, "-%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v6, v7}, Lflipboard/activities/AudioPlayerActivity$AudioPlayerUtility;->a(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 471
    return-void
.end method

.method private K()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 513
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 514
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/AudioPlayerActivity$6;

    invoke-direct {v1, p0}, Lflipboard/activities/AudioPlayerActivity$6;-><init>(Lflipboard/activities/AudioPlayerActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 532
    :goto_0
    return-void

    .line 526
    :cond_0
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->I()V

    .line 527
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->J()V

    .line 528
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->y:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 529
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->h()Lflipboard/service/audio/Song;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->a(Lflipboard/service/audio/Song;)V

    .line 530
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->x:Lflipboard/gui/FLBusyView;

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 531
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->s:Lflipboard/gui/FLToggleImageButton;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto :goto_0
.end method

.method private L()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 641
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->s:Lflipboard/gui/FLToggleImageButton;

    iget-object v3, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v3}, Lflipboard/service/audio/FLAudioManager;->d()Z

    move-result v3

    invoke-virtual {v0, v3}, Lflipboard/gui/FLToggleImageButton;->setChecked(Z)V

    .line 644
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->l()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v0, v0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    iget-object v3, v0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-eqz v3, :cond_2

    iget-object v0, v0, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v0}, Lflipboard/service/audio/FLMediaPlayer;->a()Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 645
    :cond_0
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->r()V

    .line 646
    iput-boolean v1, p0, Lflipboard/activities/AudioPlayerActivity;->C:Z

    .line 662
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v2

    .line 644
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    .line 649
    :cond_4
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 651
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->y:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 652
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->x:Lflipboard/gui/FLBusyView;

    invoke-static {v0, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 653
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->s:Lflipboard/gui/FLToggleImageButton;

    invoke-static {v0, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 654
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->J()V

    .line 657
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 658
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->x:Lflipboard/gui/FLBusyView;

    invoke-static {v0, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 659
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->s:Lflipboard/gui/FLToggleImageButton;

    invoke-static {v0, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto :goto_2
.end method

.method static synthetic a(Lflipboard/activities/AudioPlayerActivity;)Lflipboard/gui/FLBusyView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->x:Lflipboard/gui/FLBusyView;

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/AudioPlayerActivity;Lflipboard/service/audio/FLMediaPlayer$PlayerState;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lflipboard/activities/AudioPlayerActivity;->a(Lflipboard/service/audio/FLMediaPlayer$PlayerState;)V

    return-void
.end method

.method private a(Lflipboard/objs/FeedItem;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 304
    const v0, 0x7f0a00ab

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 307
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->Y()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 308
    invoke-static {p0, p1}, Lflipboard/gui/SocialFormatter;->a(Landroid/content/Context;Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v1

    .line 309
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 310
    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 311
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 321
    :goto_0
    return-void

    .line 313
    :cond_0
    invoke-interface {v0, v3}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 314
    invoke-interface {v0, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 317
    :cond_1
    invoke-interface {v0, v3}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 318
    invoke-interface {v0, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(Lflipboard/service/audio/FLMediaPlayer$PlayerState;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 536
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 537
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/AudioPlayerActivity$7;

    invoke-direct {v1, p0, p1}, Lflipboard/activities/AudioPlayerActivity$7;-><init>(Lflipboard/activities/AudioPlayerActivity;Lflipboard/service/audio/FLMediaPlayer$PlayerState;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 574
    :goto_0
    :pswitch_0
    return-void

    .line 548
    :cond_0
    sget-object v0, Lflipboard/activities/AudioPlayerActivity$9;->b:[I

    invoke-virtual {p1}, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 571
    invoke-virtual {p0}, Lflipboard/activities/AudioPlayerActivity;->m_()V

    goto :goto_0

    .line 553
    :pswitch_1
    iput-boolean v3, p0, Lflipboard/activities/AudioPlayerActivity;->C:Z

    .line 554
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->r()V

    .line 555
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->x:Lflipboard/gui/FLBusyView;

    invoke-static {v0, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 556
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->s:Lflipboard/gui/FLToggleImageButton;

    invoke-static {v0, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 557
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->s:Lflipboard/gui/FLToggleImageButton;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLToggleImageButton;->setChecked(Z)V

    goto :goto_0

    .line 560
    :pswitch_2
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->x:Lflipboard/gui/FLBusyView;

    invoke-static {v0, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 561
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->s:Lflipboard/gui/FLToggleImageButton;

    invoke-static {v0, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 562
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->s:Lflipboard/gui/FLToggleImageButton;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLToggleImageButton;->setChecked(Z)V

    goto :goto_0

    .line 565
    :pswitch_3
    invoke-virtual {p0}, Lflipboard/activities/AudioPlayerActivity;->m_()V

    goto :goto_0

    .line 568
    :pswitch_4
    invoke-virtual {p0}, Lflipboard/activities/AudioPlayerActivity;->m_()V

    goto :goto_0

    .line 548
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(Lflipboard/service/audio/Song;)V
    .locals 10

    .prologue
    .line 169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/AudioPlayerActivity;->C:Z

    .line 172
    iget-object v0, p1, Lflipboard/service/audio/Song;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 173
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->r:Lflipboard/gui/FLImageView;

    iget-object v1, p1, Lflipboard/service/audio/Song;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 178
    :goto_0
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->p:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p1, Lflipboard/service/audio/Song;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->q:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p1, Lflipboard/service/audio/Song;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedItem;->b(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 185
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    .line 188
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->f()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 191
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->f()Lflipboard/objs/FeedItem;

    move-result-object v2

    .line 192
    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    .line 195
    const/4 v0, 0x0

    .line 196
    iget-object v1, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 197
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    move-object v1, v0

    .line 201
    :goto_1
    iget-object v0, v2, Lflipboard/objs/FeedItem;->bv:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, v2, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, v2, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, v2, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bv:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 202
    iget-object v0, v2, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    move-object v2, v0

    .line 206
    :cond_1
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 207
    :goto_2
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v5

    .line 208
    if-eqz v1, :cond_6

    iget-object v0, v1, Lflipboard/objs/ConfigService;->m:Ljava/lang/String;

    move-object v4, v0

    .line 209
    :goto_3
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    iget-wide v6, v0, Lflipboard/objs/FeedItem;->Z:J

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-static {p0, v6, v7}, Lflipboard/util/JavaUtil;->b(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v6

    .line 212
    sget-object v0, Lflipboard/activities/AudioPlayerActivity;->n:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v0, v7

    .line 213
    sget-object v0, Lflipboard/activities/AudioPlayerActivity;->n:Lflipboard/util/Log;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v0, v7

    const/4 v7, 0x1

    aput-object v5, v0, v7

    const/4 v7, 0x2

    aput-object v4, v0, v7

    const/4 v7, 0x3

    const/4 v8, 0x0

    aput-object v8, v0, v7

    const/4 v7, 0x4

    aput-object v6, v0, v7

    .line 215
    const v0, 0x7f0a00a3

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 216
    invoke-virtual {v0, v3}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 218
    const v0, 0x7f0a00a6

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    .line 219
    invoke-virtual {v0, v5}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    invoke-virtual {p0}, Lflipboard/activities/AudioPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f080003

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lflipboard/gui/FLLabelTextView;->setTextColor(I)V

    .line 221
    const v3, 0x3c23d70a    # 0.01f

    const/4 v5, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v8, 0x66000000

    invoke-virtual {v0, v3, v5, v7, v8}, Lflipboard/gui/FLLabelTextView;->setShadowLayer(FFFI)V

    .line 223
    const v0, 0x7f0a00a7

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 224
    invoke-virtual {v0, v4}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 226
    const v0, 0x7f0a00a8

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 227
    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_7

    .line 228
    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 229
    invoke-virtual {p0}, Lflipboard/activities/AudioPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080085

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-interface {v0, v3}, Lflipboard/gui/FLTextIntf;->setTextColor(I)V

    .line 230
    const v3, 0x3c23d70a    # 0.01f

    const/4 v4, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v6, 0x66000000

    invoke-interface {v0, v3, v4, v5, v6}, Lflipboard/gui/FLTextIntf;->setShadowLayer(FFFI)V

    .line 236
    :goto_4
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->A:Lflipboard/gui/FLCameleonImageView;

    if-eqz v0, :cond_2

    .line 237
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/objs/FeedItem;->a(Lflipboard/objs/ConfigService;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 238
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->A:Lflipboard/gui/FLCameleonImageView;

    invoke-static {v1}, Lflipboard/util/AndroidUtil;->a(Lflipboard/objs/ConfigService;)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setImageResource(I)V

    .line 239
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->j()V

    .line 240
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->A:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v0, p0}, Lflipboard/gui/FLCameleonImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->A:Lflipboard/gui/FLCameleonImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setVisibility(I)V

    .line 248
    :cond_2
    :goto_5
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->B:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 249
    iget-boolean v0, v2, Lflipboard/objs/FeedItem;->aj:Z

    if-eqz v0, :cond_9

    .line 250
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->B:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 251
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->B:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 257
    :cond_3
    :goto_6
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    invoke-direct {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->a(Lflipboard/objs/FeedItem;)V

    .line 260
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedItem;->a(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 262
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->Y()Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->g()Lflipboard/service/Section;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 265
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 266
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->g()Lflipboard/service/Section;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/Section;->u:Ljava/util/List;

    .line 267
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 268
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 175
    :cond_4
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->r:Lflipboard/gui/FLImageView;

    const v1, 0x7f02003c

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    goto/16 :goto_0

    .line 206
    :cond_5
    const/4 v0, 0x0

    move-object v3, v0

    goto/16 :goto_2

    .line 208
    :cond_6
    const/4 v0, 0x0

    move-object v4, v0

    goto/16 :goto_3

    .line 232
    :cond_7
    const/4 v3, 0x4

    invoke-interface {v0, v3}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    goto/16 :goto_4

    .line 243
    :cond_8
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->A:Lflipboard/gui/FLCameleonImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setVisibility(I)V

    goto :goto_5

    .line 253
    :cond_9
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->B:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_6

    .line 271
    :cond_a
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/util/List;)V

    .line 274
    :cond_b
    return-void

    :cond_c
    move-object v1, v0

    goto/16 :goto_1
.end method

.method static synthetic b(Lflipboard/activities/AudioPlayerActivity;)Lflipboard/service/audio/FLAudioManager;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    return-object v0
.end method

.method static synthetic c(Lflipboard/activities/AudioPlayerActivity;)Lflipboard/gui/FLToggleImageButton;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->s:Lflipboard/gui/FLToggleImageButton;

    return-object v0
.end method

.method static synthetic d(Lflipboard/activities/AudioPlayerActivity;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->I()V

    return-void
.end method

.method static synthetic e(Lflipboard/activities/AudioPlayerActivity;)Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method static synthetic f(Lflipboard/activities/AudioPlayerActivity;)Lflipboard/gui/FLCameleonImageView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->A:Lflipboard/gui/FLCameleonImageView;

    return-object v0
.end method

.method static synthetic g(Lflipboard/activities/AudioPlayerActivity;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lflipboard/activities/AudioPlayerActivity;->C:Z

    return v0
.end method

.method static synthetic h(Lflipboard/activities/AudioPlayerActivity;)Lflipboard/gui/FLLabelTextView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->v:Lflipboard/gui/FLLabelTextView;

    return-object v0
.end method

.method static synthetic i(Lflipboard/activities/AudioPlayerActivity;)Lflipboard/gui/FLLabelTextView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->w:Lflipboard/gui/FLLabelTextView;

    return-object v0
.end method

.method static synthetic j(Lflipboard/activities/AudioPlayerActivity;)Landroid/widget/SeekBar;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->y:Landroid/widget/SeekBar;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    if-nez v0, :cond_0

    .line 354
    :goto_0
    return-void

    .line 341
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/AudioPlayerActivity$4;

    invoke-direct {v1, p0}, Lflipboard/activities/AudioPlayerActivity$4;-><init>(Lflipboard/activities/AudioPlayerActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic k(Lflipboard/activities/AudioPlayerActivity;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->K()V

    return-void
.end method

.method private r()V
    .locals 6

    .prologue
    .line 361
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->y:Landroid/widget/SeekBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 362
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->I()V

    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->D:Ljava/util/Timer;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Timer;

    const-string v1, "audio-progress-bar"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->D:Ljava/util/Timer;

    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->D:Ljava/util/Timer;

    new-instance v1, Lflipboard/activities/AudioPlayerActivity$5;

    invoke-direct {v1, p0}, Lflipboard/activities/AudioPlayerActivity$5;-><init>(Lflipboard/activities/AudioPlayerActivity;)V

    const-wide/16 v2, 0x64

    const-wide/16 v4, 0xfa

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 363
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/objs/HasCommentaryItem;)V
    .locals 2

    .prologue
    .line 291
    const/4 v0, 0x0

    .line 292
    instance-of v1, p1, Lflipboard/objs/FeedItem;

    if-eqz v1, :cond_1

    .line 293
    check-cast p1, Lflipboard/objs/FeedItem;

    .line 295
    :goto_0
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->j()V

    .line 298
    invoke-direct {p0, p1}, Lflipboard/activities/AudioPlayerActivity;->a(Lflipboard/objs/FeedItem;)V

    .line 300
    :cond_0
    return-void

    :cond_1
    move-object p1, v0

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 33
    check-cast p2, Lflipboard/service/audio/FLAudioManager$AudioMessage;

    sget-object v0, Lflipboard/activities/AudioPlayerActivity;->n:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    sget-object v0, Lflipboard/activities/AudioPlayerActivity$9;->a:[I

    invoke-virtual {p2}, Lflipboard/service/audio/FLAudioManager$AudioMessage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    check-cast p3, Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    invoke-direct {p0, p3}, Lflipboard/activities/AudioPlayerActivity;->a(Lflipboard/service/audio/FLMediaPlayer$PlayerState;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->i()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lflipboard/activities/AudioPlayerActivity;->finish()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->h()Lflipboard/service/audio/Song;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->a(Lflipboard/service/audio/Song;)V

    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->L()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->K()V

    goto :goto_0

    :pswitch_3
    iget-boolean v0, p0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_2

    const v0, 0x7f0d0041

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const v0, 0x7f0d0042

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0}, Lflipboard/activities/AudioPlayerActivity;->m_()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public clickedOutside(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 603
    invoke-virtual {p0}, Lflipboard/activities/AudioPlayerActivity;->finish()V

    .line 604
    const/4 v0, 0x0

    const v1, 0x7f040004

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/AudioPlayerActivity;->overridePendingTransition(II)V

    .line 605
    return-void
.end method

.method protected final f()V
    .locals 2

    .prologue
    .line 609
    const/4 v0, 0x0

    const v1, 0x7f040004

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/AudioPlayerActivity;->overridePendingTransition(II)V

    .line 610
    return-void
.end method

.method public finish()V
    .locals 8

    .prologue
    .line 755
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 757
    iget-wide v0, p0, Lflipboard/activities/AudioPlayerActivity;->V:J

    .line 758
    iget-wide v4, p0, Lflipboard/activities/AudioPlayerActivity;->R:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 759
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lflipboard/activities/AudioPlayerActivity;->R:J

    sub-long/2addr v4, v6

    add-long/2addr v0, v4

    .line 761
    :cond_0
    const-string v3, "extra_result_active_time"

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 762
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v2}, Lflipboard/activities/AudioPlayerActivity;->setResult(ILandroid/content/Intent;)V

    .line 763
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->finish()V

    .line 764
    return-void
.end method

.method public final g()Lflipboard/gui/flipping/FlippingBitmap;
    .locals 1

    .prologue
    .line 683
    const/4 v0, 0x0

    return-object v0
.end method

.method public final m_()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 578
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 579
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/AudioPlayerActivity$8;

    invoke-direct {v1, p0}, Lflipboard/activities/AudioPlayerActivity$8;-><init>(Lflipboard/activities/AudioPlayerActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 599
    :goto_0
    return-void

    .line 591
    :cond_0
    iput-boolean v2, p0, Lflipboard/activities/AudioPlayerActivity;->C:Z

    .line 592
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->I()V

    .line 593
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->J()V

    .line 594
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->s:Lflipboard/gui/FLToggleImageButton;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLToggleImageButton;->setChecked(Z)V

    .line 595
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->y:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 596
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->x:Lflipboard/gui/FLBusyView;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 597
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->s:Lflipboard/gui/FLToggleImageButton;

    invoke-static {v0, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 278
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->g()Lflipboard/service/Section;

    move-result-object v0

    .line 279
    :goto_0
    iget-object v2, p0, Lflipboard/activities/AudioPlayerActivity;->A:Lflipboard/gui/FLCameleonImageView;

    if-ne p1, v2, :cond_2

    .line 280
    iget-object v1, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    invoke-static {v1, p0, v0}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V

    .line 287
    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object v0, v1

    .line 278
    goto :goto_0

    .line 281
    :cond_2
    iget-object v2, p0, Lflipboard/activities/AudioPlayerActivity;->B:Landroid/view/View;

    if-ne p1, v2, :cond_0

    .line 283
    iget-object v2, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v2}, Lflipboard/service/audio/FLAudioManager;->f()Lflipboard/objs/FeedItem;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 284
    iget-object v2, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v2}, Lflipboard/service/audio/FLAudioManager;->f()Lflipboard/objs/FeedItem;

    move-result-object v2

    invoke-static {v2, v0, p0, v1}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 65
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 68
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    if-nez v0, :cond_0

    .line 69
    sget-object v0, Lflipboard/activities/AudioPlayerActivity;->n:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    aput-object v1, v0, v2

    .line 70
    invoke-virtual {p0}, Lflipboard/activities/AudioPlayerActivity;->finish()V

    .line 105
    :goto_0
    return-void

    .line 75
    :cond_0
    iput-boolean v2, p0, Lflipboard/activities/AudioPlayerActivity;->W:Z

    .line 77
    const v0, 0x7f030027

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->setContentView(I)V

    .line 79
    const v0, 0x7f0a0097

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->p:Lflipboard/gui/FLStaticTextView;

    .line 80
    const v0, 0x7f0a0098

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->q:Lflipboard/gui/FLStaticTextView;

    .line 81
    const v0, 0x7f0a0096

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->r:Lflipboard/gui/FLImageView;

    .line 82
    const v0, 0x7f0a009e

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLToggleImageButton;

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->s:Lflipboard/gui/FLToggleImageButton;

    .line 83
    const v0, 0x7f0a009d

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->t:Landroid/widget/ImageButton;

    .line 84
    const v0, 0x7f0a009f

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->u:Landroid/widget/ImageButton;

    .line 85
    const v0, 0x7f0a009b

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->v:Lflipboard/gui/FLLabelTextView;

    .line 86
    const v0, 0x7f0a0099

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->w:Lflipboard/gui/FLLabelTextView;

    .line 87
    const v0, 0x7f0a009a

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->y:Landroid/widget/SeekBar;

    .line 88
    const v0, 0x7f0a0090

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLBusyView;

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->x:Lflipboard/gui/FLBusyView;

    .line 89
    const v0, 0x7f0a00ac

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->B:Landroid/view/View;

    .line 90
    const v0, 0x7f0a00a5

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLCameleonImageView;

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->A:Lflipboard/gui/FLCameleonImageView;

    .line 92
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->y:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 95
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->y:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 96
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0, p0}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/util/Observer;)V

    .line 99
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->s:Lflipboard/gui/FLToggleImageButton;

    new-instance v1, Lflipboard/activities/AudioPlayerActivity$1;

    invoke-direct {v1, p0}, Lflipboard/activities/AudioPlayerActivity$1;-><init>(Lflipboard/activities/AudioPlayerActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLToggleImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->u:Landroid/widget/ImageButton;

    new-instance v1, Lflipboard/activities/AudioPlayerActivity$2;

    invoke-direct {v1, p0}, Lflipboard/activities/AudioPlayerActivity$2;-><init>(Lflipboard/activities/AudioPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->t:Landroid/widget/ImageButton;

    new-instance v1, Lflipboard/activities/AudioPlayerActivity$3;

    invoke-direct {v1, p0}, Lflipboard/activities/AudioPlayerActivity$3;-><init>(Lflipboard/activities/AudioPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->setVolumeControlStream(I)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 666
    sget-object v0, Lflipboard/activities/AudioPlayerActivity;->n:Lflipboard/util/Log;

    .line 668
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onDestroy()V

    .line 671
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->I()V

    .line 672
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0, p0}, Lflipboard/service/audio/FLAudioManager;->c(Lflipboard/util/Observer;)V

    .line 675
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 676
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedItem;->b(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 678
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->z:Lflipboard/objs/FeedItem;

    .line 679
    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 6

    .prologue
    .line 370
    iget-boolean v0, p0, Lflipboard/activities/AudioPlayerActivity;->C:Z

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->b()I

    move-result v0

    .line 372
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1, v0}, Lflipboard/activities/AudioPlayerActivity$AudioPlayerUtility;->a(II)I

    move-result v1

    .line 375
    iget-object v2, p0, Lflipboard/activities/AudioPlayerActivity;->w:Lflipboard/gui/FLLabelTextView;

    int-to-long v4, v1

    invoke-static {v4, v5}, Lflipboard/activities/AudioPlayerActivity$AudioPlayerUtility;->a(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 378
    iget-object v2, p0, Lflipboard/activities/AudioPlayerActivity;->v:Lflipboard/gui/FLLabelTextView;

    const-string v3, "-%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sub-int/2addr v0, v1

    int-to-long v0, v0

    invoke-static {v0, v1}, Lflipboard/activities/AudioPlayerActivity$AudioPlayerUtility;->a(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 380
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 614
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onResume()V

    .line 616
    sget-object v0, Lflipboard/activities/AudioPlayerActivity;->n:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    aput-object v2, v0, v1

    .line 619
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    if-nez v0, :cond_0

    .line 620
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->z()Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    .line 623
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    if-nez v0, :cond_0

    .line 624
    invoke-virtual {p0}, Lflipboard/activities/AudioPlayerActivity;->finish()V

    .line 636
    :goto_0
    return-void

    .line 630
    :cond_0
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 631
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->h()Lflipboard/service/audio/Song;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/activities/AudioPlayerActivity;->a(Lflipboard/service/audio/Song;)V

    .line 632
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->L()V

    .line 635
    :cond_1
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v1, "usedAudioControls"

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 388
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->I()V

    .line 389
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3

    .prologue
    .line 396
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->I()V

    .line 397
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->b()I

    move-result v0

    .line 398
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1, v0}, Lflipboard/activities/AudioPlayerActivity$AudioPlayerUtility;->a(II)I

    move-result v0

    .line 401
    iget-object v1, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v1}, Lflipboard/service/audio/FLAudioManager;->m()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, v1, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    iget-object v2, v1, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    if-eqz v2, :cond_0

    iget-object v1, v1, Lflipboard/service/audio/MediaPlayerService;->c:Lflipboard/service/audio/FLMediaPlayer;

    invoke-virtual {v1, v0}, Lflipboard/service/audio/FLMediaPlayer;->seekTo(I)V

    .line 404
    :cond_0
    invoke-direct {p0}, Lflipboard/activities/AudioPlayerActivity;->r()V

    .line 405
    return-void
.end method

.method public openSocialCard(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 325
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->f()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->g()Lflipboard/service/Section;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->f()Lflipboard/objs/FeedItem;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/AudioPlayerActivity;->o:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v1}, Lflipboard/service/audio/FLAudioManager;->g()Lflipboard/service/Section;

    move-result-object v1

    sget-object v2, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->c:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    invoke-static {v0, v1, p0, v2}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/UsageEventV2$SocialCardNavFrom;)V

    .line 328
    :cond_0
    return-void
.end method
