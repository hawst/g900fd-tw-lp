.class Lflipboard/activities/ComposeFragment$12$1;
.super Ljava/lang/Object;
.source "ComposeFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/json/FLObject;

.field final synthetic b:Lflipboard/activities/ComposeFragment$12;


# direct methods
.method constructor <init>(Lflipboard/activities/ComposeFragment$12;Lflipboard/json/FLObject;)V
    .locals 0

    .prologue
    .line 1267
    iput-object p1, p0, Lflipboard/activities/ComposeFragment$12$1;->b:Lflipboard/activities/ComposeFragment$12;

    iput-object p2, p0, Lflipboard/activities/ComposeFragment$12$1;->a:Lflipboard/json/FLObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1270
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$12$1;->b:Lflipboard/activities/ComposeFragment$12;

    iget-object v0, v0, Lflipboard/activities/ComposeFragment$12;->b:Lflipboard/activities/ComposeFragment;

    iget-object v1, p0, Lflipboard/activities/ComposeFragment$12$1;->a:Lflipboard/json/FLObject;

    const-string v2, "result"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/activities/ComposeFragment;->a(Lflipboard/activities/ComposeFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1271
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$12$1;->b:Lflipboard/activities/ComposeFragment$12;

    iget-object v0, v0, Lflipboard/activities/ComposeFragment$12;->b:Lflipboard/activities/ComposeFragment;

    invoke-static {v0}, Lflipboard/activities/ComposeFragment;->g(Lflipboard/activities/ComposeFragment;)Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1272
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$12$1;->b:Lflipboard/activities/ComposeFragment$12;

    iget-object v0, v0, Lflipboard/activities/ComposeFragment$12;->b:Lflipboard/activities/ComposeFragment;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0, v1}, Lflipboard/activities/ComposeFragment;->a(Lflipboard/activities/ComposeFragment;Ljava/util/Map;)Ljava/util/Map;

    .line 1274
    :cond_0
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$12$1;->b:Lflipboard/activities/ComposeFragment$12;

    iget-object v0, v0, Lflipboard/activities/ComposeFragment$12;->b:Lflipboard/activities/ComposeFragment;

    invoke-static {v0}, Lflipboard/activities/ComposeFragment;->g(Lflipboard/activities/ComposeFragment;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/ComposeFragment$12$1;->b:Lflipboard/activities/ComposeFragment$12;

    iget-object v1, v1, Lflipboard/activities/ComposeFragment$12;->b:Lflipboard/activities/ComposeFragment;

    invoke-static {v1}, Lflipboard/activities/ComposeFragment;->d(Lflipboard/activities/ComposeFragment;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflipboard/activities/ComposeFragment$12$1;->b:Lflipboard/activities/ComposeFragment$12;

    iget-object v2, v2, Lflipboard/activities/ComposeFragment$12;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1276
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$12$1;->b:Lflipboard/activities/ComposeFragment$12;

    iget-object v0, v0, Lflipboard/activities/ComposeFragment$12;->b:Lflipboard/activities/ComposeFragment;

    invoke-virtual {v0}, Lflipboard/activities/ComposeFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 1277
    if-eqz v0, :cond_1

    .line 1278
    iget-object v1, p0, Lflipboard/activities/ComposeFragment$12$1;->b:Lflipboard/activities/ComposeFragment$12;

    iget-object v1, v1, Lflipboard/activities/ComposeFragment$12;->b:Lflipboard/activities/ComposeFragment;

    invoke-virtual {v1, v0}, Lflipboard/activities/ComposeFragment;->b(Landroid/view/View;)V

    .line 1279
    const v1, 0x7f0a00ea

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1280
    const v1, 0x7f0a00e6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1281
    const-string v1, "twitter"

    iget-object v2, p0, Lflipboard/activities/ComposeFragment$12$1;->b:Lflipboard/activities/ComposeFragment$12;

    iget-object v2, v2, Lflipboard/activities/ComposeFragment$12;->b:Lflipboard/activities/ComposeFragment;

    invoke-static {v2}, Lflipboard/activities/ComposeFragment;->h(Lflipboard/activities/ComposeFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1282
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lflipboard/activities/ComposeFragment$12$1;->b:Lflipboard/activities/ComposeFragment$12;

    iget-object v2, v2, Lflipboard/activities/ComposeFragment$12;->b:Lflipboard/activities/ComposeFragment;

    invoke-static {v2}, Lflipboard/activities/ComposeFragment;->d(Lflipboard/activities/ComposeFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 1285
    :cond_1
    return-void
.end method
