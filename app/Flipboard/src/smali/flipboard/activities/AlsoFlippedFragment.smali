.class public Lflipboard/activities/AlsoFlippedFragment;
.super Lflipboard/gui/dialog/FLDialogFragment;
.source "AlsoFlippedFragment.java"


# static fields
.field public static final j:Lflipboard/util/Log;


# instance fields
.field k:Ljava/lang/String;

.field private l:Landroid/os/Bundle;

.field private m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lflipboard/activities/ShareActivity;->n:Lflipboard/util/Log;

    sput-object v0, Lflipboard/activities/AlsoFlippedFragment;->j:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lflipboard/activities/AlsoFlippedFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lflipboard/activities/AlsoFlippedFragment;->n:Ljava/lang/String;

    return-object v0
.end method

.method static d()Lflipboard/activities/AlsoFlippedFragment;
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lflipboard/activities/AlsoFlippedFragment;

    invoke-direct {v0}, Lflipboard/activities/AlsoFlippedFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 54
    invoke-super {p0, p1}, Lflipboard/gui/dialog/FLDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 55
    const v0, 0x7f0e0023

    invoke-virtual {p0, v0}, Lflipboard/activities/AlsoFlippedFragment;->a(I)V

    .line 56
    invoke-virtual {p0}, Lflipboard/activities/AlsoFlippedFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/AlsoFlippedFragment;->l:Landroid/os/Bundle;

    .line 57
    iget-object v0, p0, Lflipboard/activities/AlsoFlippedFragment;->l:Landroid/os/Bundle;

    const-string v1, "flipboard.extra.from.section.remote.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/AlsoFlippedFragment;->k:Ljava/lang/String;

    .line 58
    iget-object v0, p0, Lflipboard/activities/AlsoFlippedFragment;->l:Landroid/os/Bundle;

    const-string v1, "flipboard.extra.from.item.remote.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/AlsoFlippedFragment;->n:Ljava/lang/String;

    .line 59
    iget-object v0, p0, Lflipboard/activities/AlsoFlippedFragment;->l:Landroid/os/Bundle;

    const-string v1, "flipboard.extra.feeditem.result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, v2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lflipboard/activities/AlsoFlippedFragment;->m:Ljava/util/ArrayList;

    .line 61
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 63
    :try_start_0
    new-instance v4, Lflipboard/json/JSONParser;

    invoke-direct {v4, v0}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lflipboard/json/JSONParser;->k()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 64
    iget-object v4, p0, Lflipboard/activities/AlsoFlippedFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 69
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 73
    const v0, 0x7f030013

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 186
    invoke-super {p0}, Lflipboard/gui/dialog/FLDialogFragment;->onDestroy()V

    .line 188
    invoke-virtual {p0}, Lflipboard/activities/AlsoFlippedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 189
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 78
    invoke-super {p0, p1, p2}, Lflipboard/gui/dialog/FLDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 79
    const v0, 0x7f0a005b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 82
    const v1, 0x7f0a0057

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 83
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 84
    iget-object v2, p0, Lflipboard/activities/AlsoFlippedFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v10, :cond_3

    .line 86
    invoke-virtual {p0}, Lflipboard/activities/AlsoFlippedFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090017

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 87
    invoke-virtual {p0}, Lflipboard/activities/AlsoFlippedFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09007d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 88
    mul-int/lit8 v4, v3, 0x2

    add-int/2addr v2, v4

    .line 89
    iget-object v4, p0, Lflipboard/activities/AlsoFlippedFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    mul-int/2addr v2, v4

    mul-int/lit8 v4, v3, 0x2

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 91
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v2

    iget v3, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    if-ge v2, v3, :cond_0

    .line 92
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 97
    :cond_0
    :goto_0
    const/4 v2, -0x2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 98
    const/16 v2, 0x11

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 101
    const v1, 0x7f0a0058

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/actionbar/FLActionBar;

    .line 102
    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBar;->e()V

    .line 103
    sget-object v2, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v1, v2}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 104
    invoke-virtual {p0}, Lflipboard/gui/dialog/FLDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v1, v2, p0}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/gui/dialog/FLDialogFragment;)V

    .line 106
    iget-object v1, p0, Lflipboard/activities/AlsoFlippedFragment;->m:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lflipboard/activities/AlsoFlippedFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 107
    iget-object v1, p0, Lflipboard/activities/AlsoFlippedFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    .line 108
    invoke-virtual {p0, p2}, Lflipboard/activities/AlsoFlippedFragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030121

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 109
    if-eqz v1, :cond_1

    .line 110
    const v3, 0x7f0a0318

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lflipboard/gui/FLImageView;

    .line 111
    invoke-virtual {v3, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 112
    const v3, 0x7f0a0319

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lflipboard/gui/FLStaticTextView;

    .line 113
    iget-object v5, v1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget v3, v1, Lflipboard/objs/FeedItem;->s:I

    if-le v3, v10, :cond_2

    .line 116
    const v3, 0x7f0a02ac

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lflipboard/gui/FLStaticTextView;

    .line 117
    invoke-virtual {p0}, Lflipboard/activities/AlsoFlippedFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d0298

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 118
    iget v6, v1, Lflipboard/objs/FeedItem;->s:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 119
    new-instance v8, Ljava/text/DecimalFormat;

    const-string v9, "#,###,###"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 120
    invoke-virtual {v8, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    .line 121
    new-array v7, v10, [Ljava/lang/Object;

    aput-object v6, v7, v11

    invoke-static {v5, v7}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    invoke-virtual {v3, v11}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    .line 124
    :cond_2
    invoke-virtual {p0}, Lflipboard/activities/AlsoFlippedFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f080004

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 125
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 126
    new-instance v1, Lflipboard/activities/AlsoFlippedFragment$1;

    invoke-direct {v1, p0}, Lflipboard/activities/AlsoFlippedFragment$1;-><init>(Lflipboard/activities/AlsoFlippedFragment;)V

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 95
    :cond_3
    invoke-virtual {p0}, Lflipboard/activities/AlsoFlippedFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090084

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    goto/16 :goto_0

    .line 153
    :cond_4
    return-void
.end method
