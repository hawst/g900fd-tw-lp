.class Lflipboard/activities/TOCActivity$3;
.super Ljava/lang/Object;
.source "TOCActivity.java"

# interfaces
.implements Lflipboard/service/Flap$TypedResultObserver;


# instance fields
.field final synthetic a:Lflipboard/service/User;

.field final synthetic b:J

.field final synthetic c:Lflipboard/activities/TOCActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/TOCActivity;Lflipboard/service/User;J)V
    .locals 1

    .prologue
    .line 251
    iput-object p1, p0, Lflipboard/activities/TOCActivity$3;->c:Lflipboard/activities/TOCActivity;

    iput-object p2, p0, Lflipboard/activities/TOCActivity$3;->a:Lflipboard/service/User;

    iput-wide p3, p0, Lflipboard/activities/TOCActivity$3;->b:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 256
    iget-object v0, p0, Lflipboard/activities/TOCActivity$3;->a:Lflipboard/service/User;

    iget-wide v2, p0, Lflipboard/activities/TOCActivity$3;->b:J

    invoke-virtual {v0}, Lflipboard/service/User;->f()Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v4}, Lflipboard/io/NetworkManager;->e()Z

    move-result v4

    if-nez v4, :cond_1

    iget-wide v4, v0, Lflipboard/service/User;->l:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x927c0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 257
    iget-object v0, p0, Lflipboard/activities/TOCActivity$3;->a:Lflipboard/service/User;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->a(Z)V

    .line 265
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 256
    goto :goto_0

    .line 261
    :cond_2
    iget-object v0, p0, Lflipboard/activities/TOCActivity$3;->c:Lflipboard/activities/TOCActivity;

    invoke-static {v0}, Lflipboard/activities/TOCActivity;->a(Lflipboard/activities/TOCActivity;)Lflipboard/gui/toc/TOCView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lflipboard/activities/TOCActivity$3;->c:Lflipboard/activities/TOCActivity;

    invoke-static {v0}, Lflipboard/activities/TOCActivity;->a(Lflipboard/activities/TOCActivity;)Lflipboard/gui/toc/TOCView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/TOCView;->a(Z)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 270
    return-void
.end method
