.class Lflipboard/activities/FeedActivity$9;
.super Ljava/lang/Object;
.source "FeedActivity.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Lflipboard/service/Section;

.field final synthetic b:Lflipboard/objs/FeedItem;

.field final synthetic c:Lflipboard/activities/FlipboardActivity;

.field final synthetic d:Lflipboard/activities/FeedActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/FeedActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;)V
    .locals 0

    .prologue
    .line 363
    iput-object p1, p0, Lflipboard/activities/FeedActivity$9;->d:Lflipboard/activities/FeedActivity;

    iput-object p2, p0, Lflipboard/activities/FeedActivity$9;->a:Lflipboard/service/Section;

    iput-object p3, p0, Lflipboard/activities/FeedActivity$9;->b:Lflipboard/objs/FeedItem;

    iput-object p4, p0, Lflipboard/activities/FeedActivity$9;->c:Lflipboard/activities/FlipboardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 367
    iget-object v0, p0, Lflipboard/activities/FeedActivity$9;->d:Lflipboard/activities/FeedActivity;

    const/4 v1, 0x0

    iput-object v1, v0, Lflipboard/activities/FeedActivity;->O:Lflipboard/objs/FeedItem;

    .line 369
    iget-object v0, p0, Lflipboard/activities/FeedActivity$9;->d:Lflipboard/activities/FeedActivity;

    iget-object v0, v0, Lflipboard/activities/FeedActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 370
    invoke-virtual {v0}, Lflipboard/service/User;->l()Ljava/lang/String;

    move-result-object v0

    .line 371
    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lflipboard/activities/FeedActivity$9;->a:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/activities/FeedActivity$9;->b:Lflipboard/objs/FeedItem;

    iget-object v2, p0, Lflipboard/activities/FeedActivity$9;->d:Lflipboard/activities/FeedActivity;

    invoke-static {v0, v1, v2}, Lflipboard/util/SocialHelper;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;)V

    .line 395
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 375
    :cond_0
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 376
    const v1, 0x7f0d01f0

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 377
    const v1, 0x7f0d01ed

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 378
    const v1, 0x7f0d004a

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 379
    const v1, 0x7f0d01f1

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 380
    new-instance v1, Lflipboard/activities/FeedActivity$9$1;

    invoke-direct {v1, p0}, Lflipboard/activities/FeedActivity$9$1;-><init>(Lflipboard/activities/FeedActivity$9;)V

    iput-object v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 393
    iget-object v1, p0, Lflipboard/activities/FeedActivity$9;->c:Lflipboard/activities/FlipboardActivity;

    iget-object v1, v1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "read_later"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
