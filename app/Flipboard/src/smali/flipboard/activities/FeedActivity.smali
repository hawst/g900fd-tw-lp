.class public abstract Lflipboard/activities/FeedActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "FeedActivity.java"

# interfaces
.implements Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/activities/FlipboardActivity;",
        "Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/Section;",
        "Lflipboard/service/Section$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final B:Lflipboard/util/Log;

.field protected C:Lflipboard/service/Section;

.field protected D:Lflipboard/objs/FeedItem;

.field protected E:Ljava/lang/String;

.field protected F:Z

.field G:Z

.field private final n:Lflipboard/service/User;

.field private o:Lflipboard/objs/FeedItem;

.field private p:Z

.field private q:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 44
    const-string v0, "activities"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/FeedActivity;->B:Lflipboard/util/Log;

    .line 49
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iput-object v0, p0, Lflipboard/activities/FeedActivity;->n:Lflipboard/service/User;

    return-void
.end method

.method private static a(Landroid/view/View;Lflipboard/util/MeteringHelper$ExitPath;)V
    .locals 2

    .prologue
    .line 635
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 636
    instance-of v1, v0, Lflipboard/util/MeterView;

    if-eqz v1, :cond_0

    .line 637
    check-cast v0, Lflipboard/util/MeterView;

    .line 638
    invoke-interface {v0, p1}, Lflipboard/util/MeterView;->setExitPath(Lflipboard/util/MeteringHelper$ExitPath;)V

    .line 642
    :cond_0
    return-void
.end method

.method private a(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 613
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lflipboard/activities/ServiceLoginActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 614
    const-string v0, "service"

    const-string v2, "nytimes"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 615
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Lflipboard/util/MeterView;

    if-eqz v2, :cond_0

    check-cast v0, Lflipboard/util/MeterView;

    const-string v2, "extra_content_discovery_from_source"

    invoke-interface {v0}, Lflipboard/util/MeterView;->getViewType()Lflipboard/util/MeteringHelper$MeteringViewUsageType;

    move-result-object v0

    iget-object v0, v0, Lflipboard/util/MeteringHelper$MeteringViewUsageType;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 616
    :cond_0
    if-eqz p2, :cond_1

    .line 617
    const-string v0, "subscribe"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 618
    sget-object v0, Lflipboard/util/MeteringHelper$ExitPath;->d:Lflipboard/util/MeteringHelper$ExitPath;

    invoke-static {p1, v0}, Lflipboard/activities/FeedActivity;->a(Landroid/view/View;Lflipboard/util/MeteringHelper$ExitPath;)V

    .line 623
    :goto_0
    invoke-virtual {p0, v1}, Lflipboard/activities/FeedActivity;->startActivity(Landroid/content/Intent;)V

    .line 624
    invoke-virtual {p0}, Lflipboard/activities/FeedActivity;->finish()V

    .line 625
    return-void

    .line 620
    :cond_1
    sget-object v0, Lflipboard/util/MeteringHelper$ExitPath;->a:Lflipboard/util/MeteringHelper$ExitPath;

    invoke-static {p1, v0}, Lflipboard/activities/FeedActivity;->a(Landroid/view/View;Lflipboard/util/MeteringHelper$ExitPath;)V

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/activities/FeedActivity;)Z
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/FeedActivity;->G:Z

    return v0
.end method


# virtual methods
.method public final a(Lflipboard/gui/actionbar/FLActionBarMenu;Lflipboard/objs/FeedItem;Lflipboard/objs/ConfigService;Z)V
    .locals 10

    .prologue
    const v9, 0x7f0d02e7

    const v8, 0x7f0d001f

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 138
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v6

    .line 141
    iput-object v6, p0, Lflipboard/activities/FeedActivity;->o:Lflipboard/objs/FeedItem;

    .line 142
    iput-boolean v7, p0, Lflipboard/activities/FeedActivity;->q:Z

    .line 143
    iget-object v0, p0, Lflipboard/activities/FeedActivity;->o:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedItem;->a(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 145
    iget-boolean v0, p2, Lflipboard/objs/FeedItem;->aj:Z

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    const/4 v2, 0x6

    invoke-virtual {p0}, Lflipboard/activities/FeedActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/JavaUtil;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v0, p1

    move v3, v1

    move v5, v1

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIILjava/lang/CharSequence;I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v2

    .line 149
    const v0, 0x7f020006

    invoke-virtual {v2, v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    .line 150
    if-eqz p4, :cond_4

    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    :goto_0
    invoke-virtual {v2, v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    .line 151
    new-instance v0, Lflipboard/activities/FeedActivity$1;

    invoke-direct {v0, p0, p2}, Lflipboard/activities/FeedActivity$1;-><init>(Lflipboard/activities/FeedActivity;Lflipboard/objs/FeedItem;)V

    iput-object v0, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 158
    iput-boolean v7, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    invoke-virtual {v2, v7}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 159
    invoke-virtual {v2}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v8}, Lflipboard/activities/FeedActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 163
    :cond_0
    invoke-virtual {v6, p3}, Lflipboard/objs/FeedItem;->b(Lflipboard/objs/ConfigService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    invoke-virtual {p3}, Lflipboard/objs/ConfigService;->f()Ljava/lang/String;

    move-result-object v0

    .line 166
    const/4 v2, 0x2

    invoke-static {v0}, Lflipboard/util/JavaUtil;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v0, p1

    move v3, v1

    move v5, v1

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIILjava/lang/CharSequence;I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v2

    .line 167
    invoke-static {p3}, Lflipboard/util/AndroidUtil;->b(Lflipboard/objs/ConfigService;)I

    move-result v0

    invoke-virtual {v2, v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    .line 168
    if-eqz p4, :cond_5

    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    :goto_1
    invoke-virtual {v2, v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    .line 169
    new-instance v0, Lflipboard/activities/FeedActivity$2;

    invoke-direct {v0, p0, p2}, Lflipboard/activities/FeedActivity$2;-><init>(Lflipboard/activities/FeedActivity;Lflipboard/objs/FeedItem;)V

    iput-object v0, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 176
    iput-boolean v7, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    invoke-virtual {v2, v7}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 178
    :cond_1
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->ak()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 179
    const v0, 0x7f0d0288

    invoke-virtual {p1, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 180
    new-instance v2, Lflipboard/activities/FeedActivity$3;

    invoke-direct {v2, p0, p2}, Lflipboard/activities/FeedActivity$3;-><init>(Lflipboard/activities/FeedActivity;Lflipboard/objs/FeedItem;)V

    iput-object v2, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 191
    :cond_2
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->aj:Z

    if-eqz v0, :cond_3

    invoke-virtual {p1, v1, v9}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(II)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    new-instance v0, Lflipboard/activities/FeedActivity$4;

    invoke-direct {v0, p0, p2}, Lflipboard/activities/FeedActivity$4;-><init>(Lflipboard/activities/FeedActivity;Lflipboard/objs/FeedItem;)V

    iput-object v0, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    const v0, 0x7f020016

    invoke-virtual {v1, v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    iput-boolean v7, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    invoke-virtual {v0, v7}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    if-eqz p4, :cond_6

    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    :goto_2
    invoke-virtual {v1, v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v9}, Lflipboard/activities/FeedActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_3
    iget-object v0, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    invoke-virtual {p0, p1, p2, v0, p0}, Lflipboard/activities/FeedActivity;->a(Lflipboard/gui/actionbar/FLActionBarMenu;Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;)V

    .line 192
    return-void

    .line 150
    :cond_4
    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    goto/16 :goto_0

    .line 168
    :cond_5
    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    goto :goto_1

    .line 191
    :cond_6
    sget-object v0, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    goto :goto_2
.end method

.method public final a(Lflipboard/gui/actionbar/FLActionBarMenu;Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 355
    iget-object v0, p2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 528
    :cond_0
    :goto_0
    return-void

    .line 359
    :cond_1
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 362
    const v1, 0x7f0d0261

    invoke-virtual {p1, v3, v3, v3, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIII)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    new-instance v2, Lflipboard/activities/FeedActivity$9;

    invoke-direct {v2, p0, p3, p2, p4}, Lflipboard/activities/FeedActivity$9;-><init>(Lflipboard/activities/FeedActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;)V

    iput-object v2, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 397
    iput-boolean v4, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    invoke-virtual {v1, v3}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 400
    const v1, 0x7f0d0345

    invoke-virtual {p1, v3, v3, v3, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIII)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    new-instance v2, Lflipboard/activities/FeedActivity$10;

    invoke-direct {v2, p0, p2, p3, p4}, Lflipboard/activities/FeedActivity$10;-><init>(Lflipboard/activities/FeedActivity;Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;)V

    iput-object v2, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 433
    iput-boolean v4, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    invoke-virtual {v1, v3}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 436
    const v1, 0x7f0d012d

    invoke-virtual {p1, v3, v3, v3, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIII)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    new-instance v2, Lflipboard/activities/FeedActivity$11;

    invoke-direct {v2, p0, p4, p2, p3}, Lflipboard/activities/FeedActivity$11;-><init>(Lflipboard/activities/FeedActivity;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V

    iput-object v2, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 484
    iput-boolean v4, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    invoke-virtual {v1, v3}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 487
    iget-object v1, p0, Lflipboard/activities/FeedActivity;->M:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ah:Z

    if-eqz v1, :cond_2

    .line 488
    const v1, 0x7f0d0275

    invoke-virtual {p1, v3, v3, v3, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIII)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    new-instance v2, Lflipboard/activities/FeedActivity$12;

    invoke-direct {v2, p0, p3, v0}, Lflipboard/activities/FeedActivity$12;-><init>(Lflipboard/activities/FeedActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    iput-object v2, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 496
    iput-boolean v4, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    invoke-virtual {v1, v3}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 501
    :cond_2
    iget-boolean v0, p2, Lflipboard/objs/FeedItem;->aQ:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p2, Lflipboard/objs/FeedItem;->aM:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p2, Lflipboard/objs/FeedItem;->be:Z

    if-eqz v0, :cond_0

    .line 502
    const v0, 0x7f0d0206

    invoke-virtual {p1, v3, v3, v3, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIII)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    new-instance v1, Lflipboard/activities/FeedActivity$13;

    invoke-direct {v1, p0, p2, p3}, Lflipboard/activities/FeedActivity$13;-><init>(Lflipboard/activities/FeedActivity;Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V

    iput-object v1, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 526
    iput-boolean v4, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    invoke-virtual {v0, v3}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    goto :goto_0
.end method

.method public a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V
    .locals 0

    .prologue
    .line 565
    invoke-static {p1, p2}, Lflipboard/util/SocialHelper;->b(Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V

    .line 566
    return-void
.end method

.method public final a(Lflipboard/objs/HasCommentaryItem;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 197
    const/4 v0, 0x0

    .line 198
    instance-of v2, p1, Lflipboard/objs/FeedItem;

    if-eqz v2, :cond_2

    .line 199
    check-cast p1, Lflipboard/objs/FeedItem;

    .line 203
    :goto_0
    if-eqz p1, :cond_1

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, p1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v0, p1, Lflipboard/objs/FeedItem;->af:Z

    if-eqz v0, :cond_1

    move v0, v1

    .line 206
    :goto_1
    invoke-virtual {p0}, Lflipboard/activities/FeedActivity;->l()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v2

    .line 207
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lflipboard/gui/actionbar/FLActionBar;->getMenu()Lflipboard/gui/actionbar/FLActionBarMenu;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 208
    invoke-virtual {v2}, Lflipboard/gui/actionbar/FLActionBar;->getMenu()Lflipboard/gui/actionbar/FLActionBarMenu;

    move-result-object v2

    invoke-virtual {v2, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    .line 209
    if-eqz v1, :cond_0

    .line 210
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 213
    :cond_0
    return-void

    .line 203
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move-object p1, v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method public b(Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 560
    invoke-static {p1}, Lflipboard/util/SocialHelper;->b(Lflipboard/objs/FeedItem;)V

    .line 561
    return-void
.end method

.method public c(Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 571
    return-void
.end method

.method public g()Lflipboard/gui/flipping/FlippingBitmap;
    .locals 1

    .prologue
    .line 532
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final n()V
    .locals 1

    .prologue
    .line 128
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->n()V

    .line 129
    invoke-virtual {p0}, Lflipboard/activities/FeedActivity;->l()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    .line 130
    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->i()V

    .line 133
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 537
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 538
    iget-object v0, p0, Lflipboard/activities/FeedActivity;->O:Lflipboard/objs/FeedItem;

    .line 539
    const/4 v1, 0x0

    iput-object v1, p0, Lflipboard/activities/FeedActivity;->O:Lflipboard/objs/FeedItem;

    .line 540
    if-ne p2, v2, :cond_0

    if-eqz v0, :cond_0

    .line 542
    iget-object v1, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    invoke-static {v1, v0, p0}, Lflipboard/util/SocialHelper;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;)V

    .line 545
    :cond_0
    const/16 v0, 0x8e

    if-ne p1, v0, :cond_2

    .line 547
    if-ne p2, v2, :cond_1

    .line 548
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 551
    :cond_1
    const-string v0, "edit_magazine_object"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 552
    invoke-virtual {p0}, Lflipboard/activities/FeedActivity;->r()V

    .line 555
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FlipboardActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 556
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    .prologue
    .line 122
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onAttachedToWindow()V

    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/FeedActivity;->F:Z

    .line 124
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x1

    .line 58
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-virtual {p0}, Lflipboard/activities/FeedActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    invoke-virtual {p0}, Lflipboard/activities/FeedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    .line 63
    const-string v0, "sid"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 64
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v0, v7, [Ljava/lang/Object;

    aput-object v1, v0, v5

    .line 65
    if-eqz v1, :cond_3

    .line 67
    iget-object v0, p0, Lflipboard/activities/FeedActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    .line 68
    iget-object v0, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    if-nez v0, :cond_2

    .line 70
    invoke-static {v1}, Lflipboard/remoteservice/RemoteServiceUtil;->a(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    .line 73
    :cond_2
    iget-object v0, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    if-nez v0, :cond_3

    .line 74
    new-instance v0, Lflipboard/service/Section;

    const-string v3, "twitter"

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    .line 75
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v0, v2}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 81
    :cond_3
    iget-object v0, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    if-nez v0, :cond_6

    .line 83
    const-string v0, "sid"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v0

    if-eq v0, v7, :cond_5

    :cond_4
    const-string v0, "sid"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 84
    :cond_5
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "section not found: %s, %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v5

    aput-object v6, v3, v7

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    const-string v0, "unwanted.FeedActivity_section_null"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    .line 86
    invoke-virtual {p0}, Lflipboard/activities/FeedActivity;->finish()V

    goto :goto_0

    .line 89
    :cond_6
    const-string v0, "extra_current_item"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 90
    if-eqz v0, :cond_7

    .line 91
    iput-object v0, p0, Lflipboard/activities/FeedActivity;->E:Ljava/lang/String;

    .line 92
    iget-object v1, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v1, v0}, Lflipboard/service/Section;->a(Ljava/lang/String;)Lflipboard/objs/FeedItem;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/FeedActivity;->D:Lflipboard/objs/FeedItem;

    .line 94
    :cond_7
    iput-boolean v7, p0, Lflipboard/activities/FeedActivity;->p:Z

    .line 95
    iget-object v0, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v0, p0}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 102
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onDestroy()V

    .line 103
    iget-object v0, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/activities/FeedActivity;->p:Z

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v0, p0}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    .line 106
    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 3

    .prologue
    .line 110
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onDetachedFromWindow()V

    .line 111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/FeedActivity;->F:Z

    .line 113
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 115
    iget-object v0, p0, Lflipboard/activities/FeedActivity;->o:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/activities/FeedActivity;->q:Z

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lflipboard/activities/FeedActivity;->o:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedItem;->b(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 118
    :cond_0
    return-void
.end method

.method public onNYTLogoClicked(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 657
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_0

    .line 658
    invoke-virtual {p0}, Lflipboard/activities/FeedActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v0

    const-string v1, "All metering data cleared"

    const v2, 0x7f0201ce

    invoke-virtual {v0, v2, v1}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    .line 659
    const-string v0, "nytimes"

    invoke-static {p0, v0}, Lflipboard/util/MeteringHelper;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 660
    invoke-virtual {p0}, Lflipboard/activities/FeedActivity;->finish()V

    .line 662
    :cond_0
    return-void
.end method

.method public onPriceTagButtonClicked(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 605
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 606
    if-eqz v0, :cond_0

    .line 607
    iget-object v4, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->a()Z

    move-result v1

    if-nez v1, :cond_1

    const v0, 0x7f0d0218

    invoke-virtual {p0, v0}, Lflipboard/activities/FeedActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 609
    :cond_0
    :goto_0
    return-void

    .line 607
    :cond_1
    iget-boolean v1, v0, Lflipboard/objs/FeedItem;->aQ:Z

    if-eqz v1, :cond_2

    iget-boolean v1, v0, Lflipboard/objs/FeedItem;->be:Z

    if-nez v1, :cond_2

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1, v4, v0}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    :cond_2
    invoke-static {v0}, Lflipboard/usage/UsageTracker;->a(Lflipboard/objs/FeedItem;)V

    invoke-static {p0, v0}, Lflipboard/util/MeteringHelper;->b(Landroid/content/Context;Lflipboard/objs/FeedItem;)Lflipboard/util/MeteringHelper$AccessType;

    move-result-object v1

    iget-object v5, v0, Lflipboard/objs/FeedItem;->W:Ljava/lang/String;

    if-eqz v5, :cond_3

    sget-object v5, Lflipboard/util/MeteringHelper$AccessType;->c:Lflipboard/util/MeteringHelper$AccessType;

    if-eq v1, v5, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {v4}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->Z()Ljava/lang/String;

    move-result-object v5

    if-nez v1, :cond_4

    :goto_2
    new-instance v1, Landroid/content/Intent;

    if-eqz v2, :cond_5

    const-class v0, Lflipboard/activities/DetailActivityStayOnRotation;

    :goto_3
    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "sid"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "extra_current_item"

    invoke-virtual {v1, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v0, 0x65

    invoke-virtual {p0, v1, v0}, Lflipboard/activities/FeedActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    move v2, v3

    goto :goto_2

    :cond_5
    const-class v0, Lflipboard/activities/DetailActivity;

    goto :goto_3
.end method

.method public onSignInNYTClicked(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 585
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/activities/FeedActivity;->a(Landroid/view/View;Z)V

    .line 586
    return-void
.end method

.method public onSubscribeNYTClicked(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 590
    sget-boolean v0, Lflipboard/service/FlipboardManager;->q:Z

    if-eqz v0, :cond_0

    .line 591
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    .line 592
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    iget-object v0, v0, Lflipboard/model/ConfigSetting;->NYTSubscribeLinkKindle:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 593
    invoke-virtual {p0, v1}, Lflipboard/activities/FeedActivity;->startActivity(Landroid/content/Intent;)V

    .line 594
    invoke-virtual {p0}, Lflipboard/activities/FeedActivity;->finish()V

    .line 598
    :goto_0
    return-void

    .line 596
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lflipboard/activities/FeedActivity;->a(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method protected p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    .line 268
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->p()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final q()Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    return-object v0
.end method

.method public r()V
    .locals 0

    .prologue
    .line 576
    return-void
.end method
