.class public Lflipboard/activities/CreateMagazineFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "CreateMagazineFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# static fields
.field public static final a:Lflipboard/util/Log;


# instance fields
.field final b:Lflipboard/service/FlipboardManager;

.field c:Lflipboard/gui/FLEditText;

.field d:Lflipboard/gui/FLEditText;

.field e:Landroid/widget/CheckBox;

.field f:Lflipboard/model/ConfigSetting$MagazineCategory;

.field g:Z

.field h:Lflipboard/objs/Magazine;

.field i:Lflipboard/activities/CreateMagazineFragment$CreateMagazineFragmentActionListener;

.field private m:Lflipboard/gui/FLButton;

.field private n:Landroid/widget/Spinner;

.field private o:Landroid/view/View;

.field private p:Z

.field private q:Lflipboard/service/Section;

.field private r:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lflipboard/model/ConfigSetting$MagazineCategory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lflipboard/activities/ShareActivity;->n:Lflipboard/util/Log;

    sput-object v0, Lflipboard/activities/CreateMagazineFragment;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 57
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->b:Lflipboard/service/FlipboardManager;

    .line 592
    return-void
.end method

.method static synthetic a(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/activities/CreateMagazineFragment$CreateMagazineFragmentActionListener;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->i:Lflipboard/activities/CreateMagazineFragment$CreateMagazineFragmentActionListener;

    return-object v0
.end method

.method private a(Ljava/lang/String;)Lflipboard/model/ConfigSetting$MagazineCategory;
    .locals 3

    .prologue
    .line 248
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->b:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object v0, v0, Lflipboard/model/ConfigSetting;->MagazineCategories:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/ConfigSetting$MagazineCategory;

    .line 249
    iget-object v2, v0, Lflipboard/model/ConfigSetting$MagazineCategory;->identifier:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 253
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/gui/FLEditText;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->c:Lflipboard/gui/FLEditText;

    return-object v0
.end method

.method static synthetic c(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/gui/FLButton;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->m:Lflipboard/gui/FLButton;

    return-object v0
.end method

.method static synthetic d(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/model/ConfigSetting$MagazineCategory;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->f:Lflipboard/model/ConfigSetting$MagazineCategory;

    return-object v0
.end method

.method static synthetic e(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/service/FlipboardManager;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->b:Lflipboard/service/FlipboardManager;

    return-object v0
.end method

.method static synthetic f(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/objs/Magazine;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->h:Lflipboard/objs/Magazine;

    return-object v0
.end method

.method static synthetic g(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->q:Lflipboard/service/Section;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 85
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 87
    invoke-virtual {p0}, Lflipboard/activities/CreateMagazineFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 88
    if-eqz v0, :cond_0

    .line 89
    const-string v1, "edit_magazine_object"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    :try_start_0
    new-instance v1, Lflipboard/json/JSONParser;

    const-string v2, "edit_magazine_object"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lflipboard/json/JSONParser;->p()Lflipboard/objs/Magazine;

    move-result-object v1

    iput-object v1, p0, Lflipboard/activities/CreateMagazineFragment;->h:Lflipboard/objs/Magazine;

    .line 92
    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment;->b:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v2, "edit_magazine_sectionid"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->q:Lflipboard/service/Section;

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/CreateMagazineFragment;->g:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 95
    :catch_0
    move-exception v0

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/CreateMagazineFragment;->g:Z

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 104
    const v0, 0x7f030058

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 106
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_1

    .line 109
    invoke-virtual {p0}, Lflipboard/activities/CreateMagazineFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f090109

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 110
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 115
    :goto_0
    const/16 v2, 0x11

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 116
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 118
    iget-boolean v0, p0, Lflipboard/activities/CreateMagazineFragment;->g:Z

    if-eqz v0, :cond_0

    .line 119
    const v0, 0x7f0a00e2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00cf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 122
    :cond_0
    return-object v1

    .line 112
    :cond_1
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v0, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 221
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onDestroy()V

    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->i:Lflipboard/activities/CreateMagazineFragment$CreateMagazineFragmentActionListener;

    .line 223
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 579
    iget-boolean v0, p0, Lflipboard/activities/CreateMagazineFragment;->p:Z

    if-eqz v0, :cond_0

    .line 580
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->r:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/ConfigSetting$MagazineCategory;

    iput-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->f:Lflipboard/model/ConfigSetting$MagazineCategory;

    .line 581
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->r:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 585
    :goto_0
    return-void

    .line 583
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/CreateMagazineFragment;->p:Z

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 590
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 127
    invoke-super {p0, p1, p2}, Lflipboard/activities/FlipboardFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 129
    const v0, 0x7f0a0147

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLEditText;

    iput-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->c:Lflipboard/gui/FLEditText;

    .line 130
    const v0, 0x7f0a0148

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLEditText;

    iput-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->d:Lflipboard/gui/FLEditText;

    .line 131
    const v0, 0x7f0a014b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->e:Landroid/widget/CheckBox;

    .line 132
    const v0, 0x7f0a0146

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    iput-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->m:Lflipboard/gui/FLButton;

    .line 133
    const v0, 0x7f0a0149

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->n:Landroid/widget/Spinner;

    .line 134
    const v0, 0x7f0a014a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->o:Landroid/view/View;

    .line 135
    const v0, 0x7f0a0145

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    .line 136
    new-instance v1, Lflipboard/activities/CreateMagazineFragment$1;

    invoke-direct {v1, p0}, Lflipboard/activities/CreateMagazineFragment$1;-><init>(Lflipboard/activities/CreateMagazineFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->m:Lflipboard/gui/FLButton;

    new-instance v1, Lflipboard/activities/CreateMagazineFragment$2;

    invoke-direct {v1, p0}, Lflipboard/activities/CreateMagazineFragment$2;-><init>(Lflipboard/activities/CreateMagazineFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->c:Lflipboard/gui/FLEditText;

    new-instance v1, Lflipboard/activities/CreateMagazineFragment$3;

    invoke-direct {v1, p0}, Lflipboard/activities/CreateMagazineFragment$3;-><init>(Lflipboard/activities/CreateMagazineFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 179
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->d:Lflipboard/gui/FLEditText;

    new-instance v1, Lflipboard/activities/CreateMagazineFragment$4;

    invoke-direct {v1, p0}, Lflipboard/activities/CreateMagazineFragment$4;-><init>(Lflipboard/activities/CreateMagazineFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 196
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 198
    new-instance v1, Lflipboard/model/ConfigSetting$MagazineCategory;

    invoke-direct {v1}, Lflipboard/model/ConfigSetting$MagazineCategory;-><init>()V

    .line 199
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    const-string v2, "no_category_selected"

    iput-object v2, v1, Lflipboard/model/ConfigSetting$MagazineCategory;->identifier:Ljava/lang/String;

    .line 201
    invoke-virtual {p0}, Lflipboard/activities/CreateMagazineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d021f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lflipboard/model/ConfigSetting$MagazineCategory;->displayNameKey:Ljava/lang/String;

    .line 202
    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment;->b:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v1

    iget-object v1, v1, Lflipboard/model/ConfigSetting;->MagazineCategories:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 203
    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment;->b:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v1

    iget-object v1, v1, Lflipboard/model/ConfigSetting;->MagazineCategories:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 206
    :cond_0
    new-instance v1, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;

    invoke-virtual {p0}, Lflipboard/activities/CreateMagazineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lflipboard/activities/CreateMagazineFragment$MagazineCategoryAdapter;-><init>(Lflipboard/activities/CreateMagazineFragment;Landroid/content/Context;Ljava/util/List;)V

    iput-object v1, p0, Lflipboard/activities/CreateMagazineFragment;->r:Landroid/widget/ArrayAdapter;

    .line 208
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->r:Landroid/widget/ArrayAdapter;

    const v1, 0x7f03012c

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 210
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->n:Landroid/widget/Spinner;

    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment;->r:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 211
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->n:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 213
    iget-boolean v0, p0, Lflipboard/activities/CreateMagazineFragment;->g:Z

    if-eqz v0, :cond_2

    .line 214
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->m:Lflipboard/gui/FLButton;

    invoke-virtual {p0}, Lflipboard/activities/CreateMagazineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d00c9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 215
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->c:Lflipboard/gui/FLEditText;

    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment;->h:Lflipboard/objs/Magazine;

    iget-object v1, v1, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLEditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->d:Lflipboard/gui/FLEditText;

    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment;->h:Lflipboard/objs/Magazine;

    iget-object v1, v1, Lflipboard/objs/Magazine;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLEditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->h:Lflipboard/objs/Magazine;

    iget-boolean v0, v0, Lflipboard/objs/Magazine;->f:Z

    if-eqz v0, :cond_4

    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment;->e:Landroid/widget/CheckBox;

    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->h:Lflipboard/objs/Magazine;

    iget-object v0, v0, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->h:Lflipboard/objs/Magazine;

    iget-object v0, v0, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    const-string v2, "public"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    :goto_1
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->h:Lflipboard/objs/Magazine;

    iget-object v0, v0, Lflipboard/objs/Magazine;->l:Ljava/lang/String;

    invoke-direct {p0, v0}, Lflipboard/activities/CreateMagazineFragment;->a(Ljava/lang/String;)Lflipboard/model/ConfigSetting$MagazineCategory;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->f:Lflipboard/model/ConfigSetting$MagazineCategory;

    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->f:Lflipboard/model/ConfigSetting$MagazineCategory;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->r:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 217
    :cond_2
    return-void

    .line 215
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment;->o:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
