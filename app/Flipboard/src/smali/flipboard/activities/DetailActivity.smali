.class public Lflipboard/activities/DetailActivity;
.super Lflipboard/activities/FeedActivity;
.source "DetailActivity.java"


# static fields
.field public static A:Landroid/graphics/Bitmap;

.field public static final n:Lflipboard/util/Log;


# instance fields
.field private aA:Lflipboard/service/FLAdManager;

.field private aB:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FLAdManager;",
            "Lflipboard/service/FLAdManager$Message;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private aC:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lflipboard/objs/Ad;",
            ">;"
        }
    .end annotation
.end field

.field private aD:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Ljava/lang/Object;",
            "Lflipboard/gui/flipping/FlipTransitionViews$Message;",
            "Lflipboard/gui/flipping/FlipTransitionBase$Direction;",
            ">;"
        }
    .end annotation
.end field

.field private aE:I

.field private aF:Z

.field private aG:Lflipboard/gui/item/WebDetailView;

.field private aH:Z

.field private aI:Lcom/amazon/motiongestures/GestureManager;

.field private aJ:Lcom/amazon/motiongestures/GestureListener;

.field private ac:Landroid/view/View;

.field private ad:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ae:D

.field private af:D

.field private ag:Z

.field private ah:Lflipboard/objs/FeedItem;

.field private ai:J

.field private aj:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Ljava/lang/Object;",
            "Lflipboard/gui/flipping/FlipTransitionViews$Message;",
            "Lflipboard/gui/flipping/FlipTransitionBase$Direction;",
            ">;"
        }
    .end annotation
.end field

.field private ak:Z

.field private al:Lflipboard/activities/DetailActivity$LoadingMorePage;

.field private am:Z

.field private an:Z

.field private ao:Z

.field private ap:Z

.field private aq:Landroid/os/Bundle;

.field private ar:Ljava/lang/String;

.field private as:Lflipboard/service/Section;

.field private at:Ljava/lang/String;

.field private au:I

.field private av:Lflipboard/gui/actionbar/FLActionBar;

.field private aw:Ljava/lang/String;

.field private ax:Z

.field private ay:Z

.field private final az:Ljava/lang/Object;

.field o:I

.field p:I

.field public q:Lflipboard/io/UsageEvent;

.field r:Lflipboard/gui/flipping/FlipTransitionViews;

.field s:I

.field t:I

.field u:J

.field v:J

.field w:J

.field x:J

.field y:Z

.field z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    const-string v0, "detail-tab"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/activities/DetailActivity;->n:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 96
    invoke-direct {p0}, Lflipboard/activities/FeedActivity;-><init>()V

    .line 105
    iput v1, p0, Lflipboard/activities/DetailActivity;->o:I

    .line 109
    const/4 v0, 0x3

    iput v0, p0, Lflipboard/activities/DetailActivity;->p:I

    .line 129
    iput v1, p0, Lflipboard/activities/DetailActivity;->s:I

    .line 130
    iput v1, p0, Lflipboard/activities/DetailActivity;->t:I

    .line 164
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->az:Ljava/lang/Object;

    .line 167
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->aC:Landroid/util/SparseArray;

    .line 177
    new-instance v0, Lflipboard/activities/DetailActivity$1;

    invoke-direct {v0, p0}, Lflipboard/activities/DetailActivity$1;-><init>(Lflipboard/activities/DetailActivity;)V

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->aJ:Lcom/amazon/motiongestures/GestureListener;

    .line 1744
    return-void
.end method

.method private I()V
    .locals 3

    .prologue
    .line 450
    new-instance v0, Lflipboard/gui/section/SectionFragment$DetailAcititySnapshotMessage;

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lflipboard/gui/section/SectionFragment$DetailAcititySnapshotMessage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    sget-object v1, Lflipboard/gui/section/SectionFragment;->G:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, v0}, Lcom/squareup/otto/Bus;->c(Ljava/lang/Object;)V

    .line 452
    return-void
.end method

.method private J()Landroid/view/View;
    .locals 2

    .prologue
    .line 1055
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1056
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v0

    .line 1059
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private K()V
    .locals 3

    .prologue
    .line 1819
    invoke-static {p0}, Lflipboard/activities/LaunchActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 1820
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1821
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1822
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_0

    .line 1823
    const-string v1, "page"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1825
    :cond_0
    invoke-virtual {p0, v0}, Lflipboard/activities/DetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 1826
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1827
    const v0, 0x7f040011

    const v1, 0x7f04001b

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/DetailActivity;->overridePendingTransition(II)V

    .line 1829
    :cond_1
    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->finish()V

    .line 1830
    return-void
.end method

.method static synthetic a(Lflipboard/activities/DetailActivity;I)I
    .locals 0

    .prologue
    .line 96
    iput p1, p0, Lflipboard/activities/DetailActivity;->aE:I

    return p1
.end method

.method private a(Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 486
    .line 487
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    if-eqz v0, :cond_0

    .line 488
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/io/UsageEvent;->d(Ljava/lang/String;)V

    .line 490
    :cond_0
    const-string v0, "detail_image_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 491
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    const-string v1, "itemType"

    const-string v4, "image-from-article"

    invoke-virtual {v0, v1, v4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 492
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/DetailActivity;->W:Z

    .line 493
    new-instance v1, Lflipboard/gui/item/ImageDetailView;

    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "detail_image_url"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lflipboard/gui/item/ImageDetailView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 494
    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "detail_image_url"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->aw:Ljava/lang/String;

    .line 496
    iput-boolean v2, p0, Lflipboard/activities/DetailActivity;->ay:Z

    .line 513
    :goto_0
    invoke-direct {p0, v1, v3}, Lflipboard/activities/DetailActivity;->b(Landroid/view/View;Lflipboard/objs/FeedItem;)V

    .line 515
    return-object v1

    .line 497
    :cond_1
    const-string v0, "flipmag_show_html"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 498
    new-instance v0, Lflipboard/gui/item/WebDetailView;

    invoke-direct {v0, p0}, Lflipboard/gui/item/WebDetailView;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    .line 499
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    move-object v0, v1

    .line 500
    check-cast v0, Lflipboard/gui/item/WebDetailView;

    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "flipmag_show_html"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v4}, Lflipboard/io/NetworkManager;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v0, v0, Lflipboard/gui/item/WebDetailView;->b:Lflipboard/gui/FLWebView;

    const-string v4, "text/html"

    const-string v5, "utf-8"

    invoke-virtual {v0, v2, v4, v5}, Lflipboard/gui/FLWebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lflipboard/gui/item/WebDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0}, Lflipboard/gui/item/WebDetailView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0218

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    invoke-virtual {v0}, Lflipboard/gui/item/WebDetailView;->e()V

    goto :goto_0

    .line 502
    :cond_3
    const-string v0, "detail_open_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 503
    new-instance v0, Lflipboard/gui/item/WebDetailView;

    invoke-direct {v0, p0}, Lflipboard/gui/item/WebDetailView;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    .line 504
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    move-object v0, v1

    .line 506
    check-cast v0, Lflipboard/gui/item/WebDetailView;

    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "detail_open_url"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "use_wide_viewport"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v5, "use_wide_viewport"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    :cond_4
    iget-object v5, v0, Lflipboard/gui/item/WebDetailView;->b:Lflipboard/gui/FLWebView;

    invoke-virtual {v5}, Lflipboard/gui/FLWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    iget-object v5, v0, Lflipboard/gui/item/WebDetailView;->b:Lflipboard/gui/FLWebView;

    invoke-virtual {v5}, Lflipboard/gui/FLWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    invoke-virtual {v0, v4}, Lflipboard/gui/item/WebDetailView;->a(Ljava/lang/String;)V

    .line 508
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    const-string v2, "itemType"

    const-string v4, "web"

    invoke-virtual {v0, v2, v4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 509
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    const-string v2, "sourceURL"

    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "detail_open_url"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 510
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    iget-object v2, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v2}, Lflipboard/io/UsageEvent;->a(Lflipboard/objs/FeedItem;)V

    goto/16 :goto_0

    :cond_5
    move-object v1, v3

    goto/16 :goto_0
.end method

.method static synthetic a(Lflipboard/activities/DetailActivity;)Lflipboard/gui/flipping/FlipTransitionViews;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1607
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    if-eqz v0, :cond_2

    .line 1608
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    const-string v1, "itemFlipCount"

    iget v2, p0, Lflipboard/activities/DetailActivity;->s:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1609
    iput v6, p0, Lflipboard/activities/DetailActivity;->s:I

    .line 1610
    iget-wide v0, p0, Lflipboard/activities/DetailActivity;->w:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lflipboard/activities/DetailActivity;->u:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lflipboard/activities/DetailActivity;->w:J

    .line 1611
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    iget-wide v2, p0, Lflipboard/activities/DetailActivity;->w:J

    iput-wide v2, v0, Lflipboard/io/UsageEvent;->g:J

    .line 1612
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lflipboard/activities/DetailActivity;->w:J

    .line 1613
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/activities/DetailActivity;->u:J

    .line 1615
    instance-of v0, p1, Lflipboard/gui/item/FlipmagDetailViewTablet;

    if-eqz v0, :cond_0

    .line 1616
    check-cast p1, Lflipboard/gui/item/FlipmagDetailViewTablet;

    .line 1617
    invoke-virtual {p1}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getNumberOfPages()I

    move-result v0

    .line 1618
    if-lez v0, :cond_0

    .line 1619
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    const-string v2, "itemLayoutPageCount"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1622
    :cond_0
    invoke-static {}, Lflipboard/util/HappyUser;->c()V

    .line 1623
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "enable_new_usage_v2_events"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1624
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    const-string v1, "deprecated"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1626
    :cond_1
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 1628
    :cond_2
    return-void
.end method

.method private a(Landroid/view/View;Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V
    .locals 5

    .prologue
    .line 669
    invoke-direct {p0, p1}, Lflipboard/activities/DetailActivity;->a(Landroid/view/View;)V

    .line 670
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "viewed"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    .line 671
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    const-string v1, "sectionType"

    const-string v2, "feed"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 673
    if-eqz p2, :cond_b

    .line 674
    iget-object v0, p2, Lflipboard/objs/FeedItem;->co:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lflipboard/objs/FeedItem;->co:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->cn:Lflipboard/json/FLObject;

    if-eqz v0, :cond_0

    .line 675
    iget-object v0, p2, Lflipboard/objs/FeedItem;->co:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->cn:Lflipboard/json/FLObject;

    invoke-virtual {v0}, Lflipboard/json/FLObject;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 676
    iget-object v2, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    iget-object v3, p2, Lflipboard/objs/FeedItem;->co:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->cn:Lflipboard/json/FLObject;

    invoke-virtual {v3, v0}, Lflipboard/json/FLObject;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 679
    :cond_0
    iget-object v0, p2, Lflipboard/objs/FeedItem;->cn:Lflipboard/json/FLObject;

    if-eqz v0, :cond_1

    .line 680
    iget-object v0, p2, Lflipboard/objs/FeedItem;->cn:Lflipboard/json/FLObject;

    invoke-virtual {v0}, Lflipboard/json/FLObject;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 681
    iget-object v2, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    iget-object v3, p2, Lflipboard/objs/FeedItem;->cn:Lflipboard/json/FLObject;

    invoke-virtual {v3, v0}, Lflipboard/json/FLObject;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 686
    :cond_1
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/io/UsageEvent;->d(Ljava/lang/String;)V

    .line 687
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->ab:Lflipboard/objs/CrashInfo;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/CrashInfo;->b:Ljava/lang/String;

    .line 688
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->ab:Lflipboard/objs/CrashInfo;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/CrashInfo;->d:Ljava/lang/String;

    .line 689
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->ab:Lflipboard/objs/CrashInfo;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/CrashInfo;->c:Ljava/lang/String;

    .line 691
    iget-object v0, p2, Lflipboard/objs/FeedItem;->bs:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 692
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    const-string v1, "canAddToCart"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 695
    :cond_2
    iget-object v0, p2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 698
    iget-object v1, p2, Lflipboard/objs/FeedItem;->W:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 699
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-magazine"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 710
    :goto_2
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    const-string v2, "itemType"

    invoke-virtual {v1, v2, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 711
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    const-string v1, "sourceURL"

    iget-object v2, p2, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 712
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    const-string v1, "partnerID"

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->aa()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 714
    iget-object v0, p2, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-eqz v0, :cond_a

    .line 715
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 716
    iget-object v0, p2, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 717
    iget-object v3, v0, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v3, :cond_3

    .line 718
    iget-object v0, v0, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 719
    invoke-virtual {v0}, Lflipboard/objs/FeedSectionLink;->b()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 720
    iget-object v0, v0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 700
    :cond_5
    const-string v1, "image"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 701
    const-string v0, "image"

    goto :goto_2

    .line 702
    :cond_6
    iget-object v1, p2, Lflipboard/objs/FeedItem;->A:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p2, Lflipboard/objs/FeedItem;->B:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 703
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-rss"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 704
    :cond_7
    const-string v1, "video"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 705
    invoke-static {p2}, Lflipboard/util/VideoUtil;->a(Lflipboard/objs/FeedItem;)Lflipboard/util/VideoUtil$VideoType;

    move-result-object v1

    .line 706
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Lflipboard/util/VideoUtil;->a(Lflipboard/util/VideoUtil$VideoType;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 708
    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-embeddedWebview"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 725
    :cond_9
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 726
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    const-string v2, "magId"

    const-string v3, ","

    invoke-static {v3, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 729
    :cond_a
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    invoke-virtual {v0, p2}, Lflipboard/io/UsageEvent;->a(Lflipboard/objs/FeedItem;)V

    .line 731
    iget-boolean v0, p2, Lflipboard/objs/FeedItem;->aQ:Z

    if-eqz v0, :cond_b

    iget-boolean v0, p2, Lflipboard/objs/FeedItem;->be:Z

    if-nez v0, :cond_b

    .line 732
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, p3, p2}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 735
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p3, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 738
    :cond_b
    if-eqz p3, :cond_c

    .line 739
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    const-string v1, "sectionIdentifier"

    invoke-virtual {p3}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 740
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    const-string v1, "sectionPartnerID"

    iget-object v2, p3, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v2, v2, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 742
    :cond_c
    return-void
.end method

.method private a(Lflipboard/objs/FeedItem;Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const-wide/16 v8, 0x0

    .line 842
    instance-of v1, p2, Lflipboard/gui/item/FlipmagDetailViewTablet;

    if-eqz v1, :cond_2

    check-cast p2, Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {p2}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getNumberOfPages()I

    move-result v3

    .line 843
    :goto_0
    iget-wide v4, p0, Lflipboard/activities/DetailActivity;->x:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    add-long/2addr v4, v6

    iget-wide v6, p0, Lflipboard/activities/DetailActivity;->v:J

    sub-long/2addr v4, v6

    .line 844
    const-wide/32 v6, 0x2932e00

    cmp-long v1, v4, v6

    if-gtz v1, :cond_0

    cmp-long v1, v4, v8

    if-gez v1, :cond_1

    .line 846
    :cond_0
    const-string v1, "time_spent on item_viewed event is too high/low to be accurate"

    .line 850
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 851
    const-string v0, "unwanted.invalid_time_spent_on_item_viewed"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    move-wide v4, v8

    .line 855
    :cond_1
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    iget v2, p0, Lflipboard/activities/DetailActivity;->t:I

    iget-object v6, p0, Lflipboard/activities/DetailActivity;->ar:Ljava/lang/String;

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lflipboard/activities/DetailActivity;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;IIJLjava/lang/String;)V

    .line 856
    const/4 v0, 0x1

    iput v0, p0, Lflipboard/activities/DetailActivity;->t:I

    .line 857
    iput-wide v8, p0, Lflipboard/activities/DetailActivity;->x:J

    .line 858
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/activities/DetailActivity;->v:J

    .line 859
    return-void

    :cond_2
    move v3, v0

    .line 842
    goto :goto_0
.end method

.method public static a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;IIJLjava/lang/String;)V
    .locals 6

    .prologue
    .line 790
    if-eqz p0, :cond_8

    .line 792
    new-instance v1, Lflipboard/objs/UsageEventV2;

    sget-object v0, Lflipboard/objs/UsageEventV2$EventAction;->c:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->f:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v1, v0, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 793
    if-eqz p1, :cond_0

    .line 794
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 795
    invoke-virtual {p1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->k:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 796
    iget-object v3, p1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v3, v3, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 797
    iget-object v3, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 799
    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 800
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 801
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 802
    iget-object v4, v0, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    if-eqz v4, :cond_1

    iget-object v4, v0, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v4, :cond_1

    .line 803
    iget-object v0, v0, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 804
    invoke-virtual {v0}, Lflipboard/objs/FeedSectionLink;->b()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 805
    iget-object v0, v0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 810
    :cond_3
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 811
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->d:Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v3, ","

    invoke-static {v3, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 814
    :cond_4
    if-lez p3, :cond_5

    .line 815
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->v:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 817
    :cond_5
    iget-object v0, p0, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/service/Account;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 818
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 819
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->m:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, p0, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    .line 820
    invoke-virtual {v0, v3}, Lflipboard/service/User;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v2

    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->n:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v4, p0, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    .line 821
    invoke-virtual {v0, v4}, Lflipboard/service/User;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 823
    :cond_6
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->b:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 824
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->Z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->c:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 825
    invoke-virtual {v0, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->l:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 826
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->aa()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->p:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, p0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    .line 827
    invoke-virtual {v0, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->s:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 828
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->al()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->y:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 829
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->D:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 830
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->y()Lflipboard/json/FLObject;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->i:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, p0, Lflipboard/objs/FeedItem;->bU:Ljava/lang/String;

    .line 831
    invoke-virtual {v0, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 832
    invoke-virtual {v0, v2, p6}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 834
    const-wide/16 v2, 0x0

    cmp-long v0, p4, v2

    if-lez v0, :cond_7

    .line 835
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->z:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 837
    :cond_7
    invoke-virtual {v1}, Lflipboard/objs/UsageEventV2;->c()V

    .line 839
    :cond_8
    return-void
.end method

.method public static a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 745
    if-eqz p0, :cond_6

    .line 747
    new-instance v1, Lflipboard/objs/UsageEventV2;

    sget-object v0, Lflipboard/objs/UsageEventV2$EventAction;->b:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->f:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v1, v0, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 748
    if-eqz p1, :cond_0

    .line 749
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 750
    invoke-virtual {p1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->k:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 751
    iget-object v3, p1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v3, v3, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 752
    iget-object v3, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 754
    :cond_0
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 755
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 756
    iget-object v0, p0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 757
    iget-object v4, v0, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    if-eqz v4, :cond_1

    iget-object v4, v0, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v4, :cond_1

    .line 758
    iget-object v0, v0, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 759
    invoke-virtual {v0}, Lflipboard/objs/FeedSectionLink;->b()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 760
    iget-object v0, v0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 765
    :cond_3
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 766
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->d:Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v3, ","

    invoke-static {v3, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 769
    :cond_4
    iget-object v0, p0, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/service/Account;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 770
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 771
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->m:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, p0, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    .line 772
    invoke-virtual {v0, v3}, Lflipboard/service/User;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v2

    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->n:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v4, p0, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    .line 773
    invoke-virtual {v0, v4}, Lflipboard/service/User;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 775
    :cond_5
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->b:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 776
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->Z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->c:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 777
    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->l:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 778
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->aa()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->p:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    .line 779
    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->s:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 780
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->al()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->q:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 781
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->z()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->D:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 782
    invoke-virtual {p0}, Lflipboard/objs/FeedItem;->y()Lflipboard/json/FLObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->i:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/objs/FeedItem;->bU:Ljava/lang/String;

    .line 783
    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 784
    invoke-virtual {v0, v1, p2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    .line 785
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 787
    :cond_6
    return-void
.end method

.method static synthetic a(Lflipboard/activities/DetailActivity;Z)Z
    .locals 0

    .prologue
    .line 96
    iput-boolean p1, p0, Lflipboard/activities/DetailActivity;->ak:Z

    return p1
.end method

.method static synthetic b(Lflipboard/activities/DetailActivity;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aC:Landroid/util/SparseArray;

    return-object v0
.end method

.method private b(Landroid/view/View;Lflipboard/objs/FeedItem;)V
    .locals 13

    .prologue
    const v12, 0x7f0d02f7

    const v11, 0x7f0a016b

    const v8, 0x7f0a004c

    const/4 v9, 0x0

    const/4 v5, 0x1

    .line 863
    if-eqz p2, :cond_a

    iget-object v0, p2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v0, :cond_a

    invoke-virtual {p1, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 864
    invoke-virtual {p1, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lflipboard/gui/SocialBarTablet;

    .line 865
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    iget-object v2, p2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lflipboard/service/Section;->m()Ljava/util/List;

    iget-object v0, v0, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v1, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    iget-object v6, v1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v6, :cond_1

    iget-object v1, v1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 866
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->P()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object p2, v0

    :cond_2
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    iput-object p2, v7, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iput-object v0, v7, Lflipboard/gui/SocialBarTablet;->c:Lflipboard/objs/FeedItem;

    iget-object v0, v7, Lflipboard/gui/SocialBarTablet;->c:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aJ:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_6

    iget-object v0, v7, Lflipboard/gui/SocialBarTablet;->c:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aJ:Lflipboard/objs/FeedItem;

    :goto_1
    iput-object v0, v7, Lflipboard/gui/SocialBarTablet;->c:Lflipboard/objs/FeedItem;

    iput-object v1, v7, Lflipboard/gui/SocialBarTablet;->e:Lflipboard/service/Section;

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->ag()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->ah()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_3
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v7, Lflipboard/gui/SocialBarTablet;->c:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    iput-object v0, v7, Lflipboard/gui/SocialBarTablet;->d:Lflipboard/objs/ConfigService;

    :goto_2
    iget-object v0, v7, Lflipboard/gui/SocialBarTablet;->c:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v7}, Lflipboard/objs/FeedItem;->a(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    iget-object v0, v7, Lflipboard/gui/SocialBarTablet;->c:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v0

    iput-object v0, v7, Lflipboard/gui/SocialBarTablet;->h:Lflipboard/objs/FeedItem;

    iget-object v0, v7, Lflipboard/gui/SocialBarTablet;->h:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v7}, Lflipboard/objs/FeedItem;->a(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    invoke-virtual {v7, v7}, Lflipboard/gui/SocialBarTablet;->a(Landroid/view/View;)V

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    iput-object v0, v7, Lflipboard/gui/SocialBarTablet;->f:Lflipboard/gui/actionbar/FLActionBar;

    iget-object v0, v7, Lflipboard/gui/SocialBarTablet;->f:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0, v9, v9}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    iget-object v0, v7, Lflipboard/gui/SocialBarTablet;->f:Lflipboard/gui/actionbar/FLActionBar;

    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->c:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    invoke-virtual {v7}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lflipboard/activities/FlipboardActivity;

    new-instance v0, Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-virtual {v7}, Lflipboard/gui/SocialBarTablet;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lflipboard/gui/actionbar/FLActionBarMenu;-><init>(Landroid/content/Context;)V

    iget-object v2, v7, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v2

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v3

    iget-object v2, v7, Lflipboard/gui/SocialBarTablet;->e:Lflipboard/service/Section;

    sget-object v4, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->c:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    iget-object v6, v7, Lflipboard/gui/SocialBarTablet;->b:Lflipboard/objs/FeedItem;

    invoke-virtual/range {v0 .. v6}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;ZLflipboard/objs/FeedItem;)V

    invoke-virtual {v7}, Lflipboard/gui/SocialBarTablet;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lflipboard/activities/FeedActivity;

    check-cast v1, Lflipboard/activities/FeedActivity;

    iget-object v4, v7, Lflipboard/gui/SocialBarTablet;->e:Lflipboard/service/Section;

    invoke-virtual {v1, v0, v3, v4, v2}, Lflipboard/activities/FeedActivity;->a(Lflipboard/gui/actionbar/FLActionBarMenu;Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;)V

    iget-object v1, v7, Lflipboard/gui/SocialBarTablet;->f:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v1, v0}, Lflipboard/gui/actionbar/FLActionBar;->setMenu(Lflipboard/gui/actionbar/FLActionBarMenu;)V

    .line 867
    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->ap:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->as:Lflipboard/service/Section;

    if-eqz v0, :cond_4

    .line 868
    iget-object v2, p0, Lflipboard/activities/DetailActivity;->as:Lflipboard/service/Section;

    iget-object v3, p0, Lflipboard/activities/DetailActivity;->aq:Landroid/os/Bundle;

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->at:Ljava/lang/String;

    const v0, 0x7f0a0170

    invoke-virtual {v7, v0}, Lflipboard/gui/SocialBarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setVisibility(I)V

    if-eqz v1, :cond_8

    :goto_3
    if-eqz v1, :cond_9

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    new-instance v1, Lflipboard/gui/SocialBarTablet$1;

    invoke-direct {v1, v7, v2, v3}, Lflipboard/gui/SocialBarTablet$1;-><init>(Lflipboard/gui/SocialBarTablet;Lflipboard/service/Section;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 907
    :cond_4
    :goto_5
    return-void

    .line 865
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 866
    :cond_6
    iget-object v0, v7, Lflipboard/gui/SocialBarTablet;->c:Lflipboard/objs/FeedItem;

    goto/16 :goto_1

    :cond_7
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    iput-object v0, v7, Lflipboard/gui/SocialBarTablet;->d:Lflipboard/objs/ConfigService;

    goto/16 :goto_2

    .line 868
    :cond_8
    invoke-virtual {v2}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_9
    invoke-virtual {v2}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 870
    :cond_a
    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 871
    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->av:Lflipboard/gui/actionbar/FLActionBar;

    .line 872
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->av:Lflipboard/gui/actionbar/FLActionBar;

    if-eqz v0, :cond_4

    .line 876
    new-instance v3, Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-direct {v3, p0}, Lflipboard/gui/actionbar/FLActionBarMenu;-><init>(Landroid/content/Context;)V

    .line 877
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_15

    .line 878
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "image"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->av:Lflipboard/gui/actionbar/FLActionBar;

    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    :cond_b
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v1

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->ag()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->ah()Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_c
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    :goto_6
    invoke-virtual {v1, v0}, Lflipboard/objs/FeedItem;->a(Lflipboard/objs/ConfigService;)Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-static {p0, v0}, Lflipboard/gui/SocialFormatter;->d(Landroid/content/Context;Lflipboard/objs/ConfigService;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lflipboard/util/JavaUtil;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move v4, v9

    move v6, v9

    move v8, v9

    invoke-virtual/range {v3 .. v8}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIILjava/lang/CharSequence;I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v4

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Lflipboard/objs/ConfigService;)I

    move-result v2

    invoke-virtual {v4, v2}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    if-eqz v10, :cond_13

    sget-object v2, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    :goto_7
    invoke-virtual {v4, v2}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    iput-boolean v5, v4, Lflipboard/gui/actionbar/FLActionBarMenuItem;->p:Z

    iget-boolean v2, v1, Lflipboard/objs/FeedItem;->af:Z

    invoke-virtual {v4, v2}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Z)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    iput-boolean v5, v4, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    invoke-virtual {v4, v5}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    new-instance v2, Lflipboard/activities/DetailActivity$10;

    invoke-direct {v2, p0, v1, v3}, Lflipboard/activities/DetailActivity$10;-><init>(Lflipboard/activities/DetailActivity;Lflipboard/objs/FeedItem;Lflipboard/gui/actionbar/FLActionBarMenu;)V

    iput-object v2, v4, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-virtual {v4}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_d
    const/4 v2, 0x3

    invoke-virtual {v3, v2, v12}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(II)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v2

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->c(Lflipboard/objs/ConfigService;)I

    move-result v4

    invoke-virtual {v2, v4}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    new-instance v4, Lflipboard/activities/DetailActivity$11;

    invoke-direct {v4, p0, v1}, Lflipboard/activities/DetailActivity$11;-><init>(Lflipboard/activities/DetailActivity;Lflipboard/objs/FeedItem;)V

    iput-object v4, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    iput-boolean v5, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    invoke-virtual {v2, v5}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    invoke-virtual {v2}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v12}, Lflipboard/activities/DetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    if-eqz v10, :cond_14

    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    :goto_8
    invoke-virtual {v2, v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    invoke-virtual {p0, v3, v1, v0, v10}, Lflipboard/activities/DetailActivity;->a(Lflipboard/gui/actionbar/FLActionBarMenu;Lflipboard/objs/FeedItem;Lflipboard/objs/ConfigService;Z)V

    :cond_e
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_f

    const v0, 0x7f0d0046

    invoke-virtual {v3, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    new-instance v1, Lflipboard/activities/DetailActivity$12;

    invoke-direct {v1, p0}, Lflipboard/activities/DetailActivity$12;-><init>(Lflipboard/activities/DetailActivity;)V

    iput-object v1, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    iput-boolean v5, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    invoke-virtual {v0, v9}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    :cond_f
    invoke-super {p0, v3}, Lflipboard/activities/FeedActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 894
    :cond_10
    :goto_9
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->av:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0, v3}, Lflipboard/gui/actionbar/FLActionBar;->setMenu(Lflipboard/gui/actionbar/FLActionBarMenu;)V

    .line 895
    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->ap:Z

    if-eqz v0, :cond_11

    .line 896
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->av:Lflipboard/gui/actionbar/FLActionBar;

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->as:Lflipboard/service/Section;

    iget-object v2, p0, Lflipboard/activities/DetailActivity;->aq:Landroid/os/Bundle;

    iget-object v3, p0, Lflipboard/activities/DetailActivity;->at:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/service/Section;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 899
    :cond_11
    if-nez p2, :cond_16

    .line 900
    :goto_a
    invoke-virtual {p1, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/SocialBarTablet;

    .line 901
    if-eqz v0, :cond_4

    if-eqz v5, :cond_4

    .line 902
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/SocialBarTablet;->setVisibility(I)V

    goto/16 :goto_5

    .line 878
    :cond_12
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    goto/16 :goto_6

    :cond_13
    sget-object v2, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    goto/16 :goto_7

    :cond_14
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    goto :goto_8

    .line 879
    :cond_15
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aw:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 880
    const v0, 0x7f0d0288

    invoke-virtual {v3, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 881
    new-instance v1, Lflipboard/activities/DetailActivity$9;

    invoke-direct {v1, p0}, Lflipboard/activities/DetailActivity$9;-><init>(Lflipboard/activities/DetailActivity;)V

    iput-object v1, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    goto :goto_9

    :cond_16
    move v5, v9

    .line 899
    goto :goto_a
.end method

.method static synthetic c(Lflipboard/activities/DetailActivity;)Lflipboard/service/FLAdManager;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aA:Lflipboard/service/FLAdManager;

    return-object v0
.end method

.method static synthetic d(Lflipboard/activities/DetailActivity;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aq:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic e(Lflipboard/activities/DetailActivity;)Lflipboard/activities/DetailActivity$LoadingMorePage;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->al:Lflipboard/activities/DetailActivity$LoadingMorePage;

    return-object v0
.end method

.method static synthetic f(Lflipboard/activities/DetailActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aw:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method final a(Lflipboard/objs/FeedItem;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1006
    if-nez p1, :cond_0

    .line 1039
    :goto_0
    return-object v0

    .line 1010
    :cond_0
    iget-object v1, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 1011
    invoke-static {p0, p1}, Lflipboard/util/MeteringHelper;->b(Landroid/content/Context;Lflipboard/objs/FeedItem;)Lflipboard/util/MeteringHelper$AccessType;

    move-result-object v2

    .line 1013
    iget-object v3, p1, Lflipboard/objs/FeedItem;->W:Ljava/lang/String;

    if-eqz v3, :cond_1

    sget-object v3, Lflipboard/util/MeteringHelper$AccessType;->c:Lflipboard/util/MeteringHelper$AccessType;

    if-eq v2, v3, :cond_1

    .line 1015
    new-instance v0, Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/item/FlipmagDetailViewTablet;-><init>(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/FeedItem;)V

    .line 1016
    iput-boolean v4, p0, Lflipboard/activities/DetailActivity;->ax:Z

    .line 1038
    :goto_1
    invoke-direct {p0, v0, p1}, Lflipboard/activities/DetailActivity;->b(Landroid/view/View;Lflipboard/objs/FeedItem;)V

    goto :goto_0

    .line 1017
    :cond_1
    const-string v2, "image"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1018
    const/4 v1, 0x0

    iput-boolean v1, p0, Lflipboard/activities/DetailActivity;->W:Z

    .line 1019
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->ad:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->ad:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1020
    const v1, 0x7f030061

    invoke-static {p0, v1, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/ImageDetailTabletView;

    .line 1021
    invoke-virtual {v0, p1}, Lflipboard/gui/item/ImageDetailTabletView;->setItem(Lflipboard/objs/FeedItem;)V

    goto :goto_1

    .line 1024
    :cond_2
    new-instance v0, Lflipboard/gui/item/ImageDetailView;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/item/ImageDetailView;-><init>(Landroid/content/Context;Lflipboard/objs/FeedItem;)V

    .line 1025
    iput-boolean v4, p0, Lflipboard/activities/DetailActivity;->ay:Z

    goto :goto_1

    .line 1027
    :cond_3
    iget-object v1, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v2, "video"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1028
    const v1, 0x7f030062

    invoke-static {p0, v1, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/VideoDetailTabletView;

    .line 1029
    invoke-virtual {v0, p1}, Lflipboard/gui/item/VideoDetailTabletView;->setItem(Lflipboard/objs/FeedItem;)V

    goto :goto_1

    .line 1030
    :cond_4
    iget-object v0, p1, Lflipboard/objs/FeedItem;->A:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lflipboard/objs/FeedItem;->B:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1032
    new-instance v0, Lflipboard/gui/item/RssDetailView;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/item/RssDetailView;-><init>(Landroid/content/Context;Lflipboard/objs/FeedItem;)V

    goto :goto_1

    .line 1034
    :cond_5
    new-instance v0, Lflipboard/gui/item/WebDetailView;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/item/WebDetailView;-><init>(Landroid/app/Activity;Lflipboard/objs/FeedItem;)V

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    .line 1035
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    goto :goto_1
.end method

.method final a(IZ)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1673
    iget-object v3, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    .line 1674
    if-eqz v3, :cond_3

    iget-object v4, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v4, :cond_3

    .line 1676
    iget-object v4, p0, Lflipboard/activities/DetailActivity;->aA:Lflipboard/service/FLAdManager;

    if-eqz v4, :cond_4

    .line 1677
    if-eqz p2, :cond_8

    .line 1679
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->az:Ljava/lang/Object;

    monitor-enter v1

    .line 1680
    :try_start_0
    iget-object v4, p0, Lflipboard/activities/DetailActivity;->aB:Lflipboard/util/Observer;

    if-eqz v4, :cond_0

    .line 1681
    iget-object v4, p0, Lflipboard/activities/DetailActivity;->aA:Lflipboard/service/FLAdManager;

    iget-object v5, p0, Lflipboard/activities/DetailActivity;->aB:Lflipboard/util/Observer;

    invoke-virtual {v4, v5}, Lflipboard/service/FLAdManager;->c(Lflipboard/util/Observer;)V

    .line 1682
    const/4 v4, 0x0

    iput-object v4, p0, Lflipboard/activities/DetailActivity;->aB:Lflipboard/util/Observer;

    .line 1684
    :cond_0
    iget-object v4, p0, Lflipboard/activities/DetailActivity;->aA:Lflipboard/service/FLAdManager;

    const/4 v5, -0x1

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lflipboard/service/FLAdManager;->a(IZ)V

    .line 1685
    iget-object v4, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v4}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;Lflipboard/objs/FeedItem;)Lflipboard/service/FLAdManager;

    move-result-object v3

    iput-object v3, p0, Lflipboard/activities/DetailActivity;->aA:Lflipboard/service/FLAdManager;

    .line 1687
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1700
    :goto_0
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->aB:Lflipboard/util/Observer;

    if-nez v1, :cond_6

    .line 1701
    iget-object v3, p0, Lflipboard/activities/DetailActivity;->az:Ljava/lang/Object;

    monitor-enter v3

    .line 1702
    :try_start_1
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->aB:Lflipboard/util/Observer;

    if-nez v1, :cond_5

    .line 1703
    new-instance v1, Lflipboard/activities/DetailActivity$16;

    invoke-direct {v1, p0}, Lflipboard/activities/DetailActivity$16;-><init>(Lflipboard/activities/DetailActivity;)V

    .line 1719
    iput-object v1, p0, Lflipboard/activities/DetailActivity;->aB:Lflipboard/util/Observer;

    .line 1721
    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1724
    :goto_2
    if-eqz v1, :cond_1

    .line 1725
    iget-object v2, p0, Lflipboard/activities/DetailActivity;->aA:Lflipboard/service/FLAdManager;

    invoke-virtual {v2, v1}, Lflipboard/service/FLAdManager;->b(Lflipboard/util/Observer;)V

    .line 1728
    :cond_1
    if-eqz v0, :cond_2

    .line 1729
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aA:Lflipboard/service/FLAdManager;

    iget v1, p0, Lflipboard/activities/DetailActivity;->aE:I

    invoke-virtual {v0, p1, v1}, Lflipboard/service/FLAdManager;->a(II)V

    .line 1733
    :cond_2
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentView()Lflipboard/gui/flipping/FlippingContainer;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlippingContainer;->getChild()Landroid/view/View;

    move-result-object v0

    .line 1734
    instance-of v1, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;

    if-eqz v1, :cond_3

    .line 1735
    check-cast v0, Lflipboard/gui/item/FlipmagDetailViewTablet;

    .line 1736
    invoke-virtual {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getCurrentViewIndex()I

    move-result v1

    .line 1737
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aC:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Ad;

    .line 1738
    iget-object v2, p0, Lflipboard/activities/DetailActivity;->aA:Lflipboard/service/FLAdManager;

    invoke-virtual {v2, v1, v0}, Lflipboard/service/FLAdManager;->a(ILflipboard/objs/Ad;)V

    .line 1739
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aA:Lflipboard/service/FLAdManager;

    iget v0, v0, Lflipboard/service/FLAdManager;->d:I

    iput v0, p0, Lflipboard/activities/DetailActivity;->aE:I

    .line 1742
    :cond_3
    return-void

    .line 1687
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1691
    :cond_4
    iget-object v4, p0, Lflipboard/activities/DetailActivity;->az:Ljava/lang/Object;

    monitor-enter v4

    .line 1692
    :try_start_2
    iget-object v5, p0, Lflipboard/activities/DetailActivity;->aA:Lflipboard/service/FLAdManager;

    if-nez v5, :cond_7

    .line 1693
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v3}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;Lflipboard/objs/FeedItem;)Lflipboard/service/FLAdManager;

    move-result-object v1

    iput-object v1, p0, Lflipboard/activities/DetailActivity;->aA:Lflipboard/service/FLAdManager;

    .line 1696
    :goto_3
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v4

    throw v0

    .line 1721
    :catchall_2
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_5
    move-object v1, v2

    goto :goto_1

    :cond_6
    move-object v1, v2

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_3

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method final a(Landroid/view/View;Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 619
    invoke-direct {p0, p1, p2}, Lflipboard/activities/DetailActivity;->b(Landroid/view/View;Lflipboard/objs/FeedItem;)V

    .line 621
    instance-of v0, p1, Lflipboard/gui/item/FlipmagDetailViewTablet;

    if-eqz v0, :cond_0

    .line 622
    new-instance v0, Lflipboard/activities/DetailActivity$7;

    invoke-direct {v0, p0, p0}, Lflipboard/activities/DetailActivity$7;-><init>(Lflipboard/activities/DetailActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    .line 646
    new-instance v0, Lflipboard/activities/DetailActivity$8;

    invoke-direct {v0, p0}, Lflipboard/activities/DetailActivity$8;-><init>(Lflipboard/activities/DetailActivity;)V

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->aj:Lflipboard/util/Observer;

    .line 656
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->aj:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->a(Lflipboard/util/Observer;)V

    .line 657
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->setShouldHaveFlipToRefresh(Z)V

    .line 658
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, p1}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    .line 659
    iget v0, p0, Lflipboard/activities/DetailActivity;->z:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/activities/DetailActivity;->z:I

    .line 661
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {p0, v0}, Lflipboard/activities/DetailActivity;->setContentView(Landroid/view/View;)V

    .line 665
    :goto_0
    return-void

    .line 663
    :cond_0
    invoke-virtual {p0, p1}, Lflipboard/activities/DetailActivity;->setContentView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(Lflipboard/gui/flipping/FlipTransitionBase$Direction;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1297
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    if-nez v0, :cond_1

    .line 1376
    :cond_0
    :goto_0
    return-void

    .line 1301
    :cond_1
    invoke-direct {p0}, Lflipboard/activities/DetailActivity;->J()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_b

    instance-of v4, v0, Lflipboard/gui/item/DetailView;

    if-eqz v4, :cond_b

    check-cast v0, Lflipboard/gui/item/DetailView;

    invoke-interface {v0}, Lflipboard/gui/item/DetailView;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v0

    move-object v4, v0

    .line 1303
    :goto_1
    if-eqz v4, :cond_5

    .line 1304
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    invoke-virtual {v4, v0}, Lflipboard/objs/FeedItem;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1305
    iget v0, p0, Lflipboard/activities/DetailActivity;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/activities/DetailActivity;->s:I

    .line 1306
    iget v0, p0, Lflipboard/activities/DetailActivity;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/activities/DetailActivity;->t:I

    move v0, v2

    .line 1338
    :goto_2
    if-nez v0, :cond_0

    .line 1339
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    .line 1341
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v1

    .line 1342
    sget-object v4, Lflipboard/activities/DetailActivity;->n:Lflipboard/util/Log;

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    .line 1343
    sget-object v4, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne p1, v4, :cond_2

    .line 1344
    sub-int v0, v1, v0

    add-int/lit8 v0, v0, -0x1

    .line 1350
    :cond_2
    sget-object v1, Lflipboard/activities/DetailActivity;->n:Lflipboard/util/Log;

    new-array v1, v8, [Ljava/lang/Object;

    aput-object p1, v1, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    .line 1351
    sget-object v1, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne p1, v1, :cond_7

    iget v1, p0, Lflipboard/activities/DetailActivity;->o:I

    if-ge v0, v1, :cond_7

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v1

    if-lez v1, :cond_7

    move v0, v2

    .line 1361
    :goto_3
    if-eqz v0, :cond_9

    iget v0, p0, Lflipboard/activities/DetailActivity;->z:I

    iget v1, p0, Lflipboard/activities/DetailActivity;->p:I

    if-gt v0, v1, :cond_9

    .line 1362
    new-instance v0, Lflipboard/activities/DetailActivity$14;

    invoke-direct {v0, p0, p1}, Lflipboard/activities/DetailActivity$14;-><init>(Lflipboard/activities/DetailActivity;Lflipboard/gui/flipping/FlipTransitionBase$Direction;)V

    .line 1371
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1309
    :cond_3
    iget-object v5, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    .line 1310
    iput-object v4, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    .line 1312
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v6

    .line 1314
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne p1, v0, :cond_6

    .line 1315
    add-int/lit8 v0, v6, -0x1

    .line 1319
    :goto_4
    iget-object v7, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v7, :cond_a

    iget-object v7, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v7}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v7

    if-le v7, v0, :cond_a

    if-ltz v0, :cond_a

    .line 1320
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v0

    .line 1322
    :goto_5
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    iget-object v7, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-direct {p0, v0, v1, v7}, Lflipboard/activities/DetailActivity;->a(Landroid/view/View;Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V

    .line 1323
    invoke-direct {p0, v5, v0}, Lflipboard/activities/DetailActivity;->a(Lflipboard/objs/FeedItem;Landroid/view/View;)V

    .line 1324
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->ar:Ljava/lang/String;

    invoke-static {v4, v0, v1}, Lflipboard/activities/DetailActivity;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Ljava/lang/String;)V

    .line 1325
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    const-string v1, "nytimes"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1326
    invoke-static {p0, v4}, Lflipboard/util/MeteringHelper;->c(Landroid/content/Context;Lflipboard/objs/FeedItem;)V

    .line 1330
    :cond_4
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "DetailActivity:onFlipWillComplete"

    new-instance v4, Lflipboard/activities/DetailActivity$13;

    invoke-direct {v4, p0, v6}, Lflipboard/activities/DetailActivity$13;-><init>(Lflipboard/activities/DetailActivity;I)V

    invoke-virtual {v0, v1, v4}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    :cond_5
    move v0, v3

    goto/16 :goto_2

    .line 1317
    :cond_6
    add-int/lit8 v0, v6, 0x1

    goto :goto_4

    .line 1354
    :cond_7
    sget-object v1, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->b:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne p1, v1, :cond_8

    if-gtz v0, :cond_8

    move v0, v2

    .line 1356
    goto :goto_3

    :cond_8
    move v0, v3

    .line 1358
    goto :goto_3

    .line 1373
    :cond_9
    sget-object v0, Lflipboard/activities/DetailActivity;->n:Lflipboard/util/Log;

    goto/16 :goto_0

    :cond_a
    move-object v0, v1

    goto :goto_5

    :cond_b
    move-object v4, v1

    goto/16 :goto_1
.end method

.method final b(Lflipboard/gui/flipping/FlipTransitionBase$Direction;)V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, -0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 1380
    .line 1382
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v8

    .line 1383
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->b:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    move v6, v0

    move v7, v3

    move v0, v1

    .line 1387
    :goto_1
    if-nez v0, :cond_4

    if-ltz v6, :cond_4

    if-ge v6, v8, :cond_4

    .line 1388
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0, v6}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v0

    .line 1389
    instance-of v4, v0, Lflipboard/gui/RoadBlock;

    if-nez v4, :cond_4

    .line 1390
    instance-of v4, v0, Lflipboard/gui/item/DetailView;

    .line 1393
    if-eqz v4, :cond_2

    .line 1394
    check-cast v0, Lflipboard/gui/item/DetailView;

    .line 1395
    invoke-interface {v0}, Lflipboard/gui/item/DetailView;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 1396
    iget-object v7, p0, Lflipboard/activities/DetailActivity;->ad:Ljava/util/List;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-interface {v7, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v7

    .line 1397
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne p1, v0, :cond_1

    move v0, v2

    :goto_2
    add-int/2addr v0, v7

    move v7, v0

    move v0, v4

    .line 1398
    goto :goto_1

    .line 1383
    :cond_0
    add-int/lit8 v0, v8, -0x1

    goto :goto_0

    :cond_1
    move v0, v3

    .line 1397
    goto :goto_2

    .line 1399
    :cond_2
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne p1, v0, :cond_3

    .line 1400
    add-int/lit8 v0, v6, -0x1

    move v6, v0

    move v0, v4

    goto :goto_1

    .line 1402
    :cond_3
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v0, v4

    .line 1405
    goto :goto_1

    .line 1407
    :cond_4
    if-ltz v7, :cond_b

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ad:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_b

    .line 1408
    iget-object v4, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ad:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Lflipboard/service/Section;->a(Ljava/lang/String;)Lflipboard/objs/FeedItem;

    move-result-object v0

    move-object v4, v0

    .line 1412
    :goto_3
    sget-object v0, Lflipboard/gui/flipping/FlipTransitionBase$Direction;->a:Lflipboard/gui/flipping/FlipTransitionBase$Direction;

    if-ne p1, v0, :cond_7

    .line 1413
    :goto_4
    if-eqz v4, :cond_5

    .line 1414
    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->y:Z

    if-eqz v0, :cond_5

    .line 1415
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v6, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v6}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v6

    invoke-virtual {v0, v6}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v0

    .line 1416
    instance-of v6, v0, Lflipboard/gui/Interstitial;

    if-nez v6, :cond_5

    instance-of v0, v0, Lflipboard/gui/RoadBlock;

    if-nez v0, :cond_5

    .line 1417
    invoke-static {p0, v4}, Lflipboard/util/MeteringHelper;->b(Landroid/content/Context;Lflipboard/objs/FeedItem;)Lflipboard/util/MeteringHelper$AccessType;

    move-result-object v0

    .line 1418
    sget-object v6, Lflipboard/util/MeteringHelper$AccessType;->b:Lflipboard/util/MeteringHelper$AccessType;

    if-ne v0, v6, :cond_9

    .line 1420
    const v0, 0x7f0300e2

    invoke-static {p0, v0, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/Interstitial;

    .line 1421
    iget-object v6, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v6}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lflipboard/gui/Interstitial;->a:Lflipboard/gui/FLStaticTextView;

    iget-object v8, v4, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, v0, Lflipboard/gui/Interstitial;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v7, v6}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lflipboard/gui/Interstitial;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-wide v8, v4, Lflipboard/objs/FeedItem;->Z:J

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    invoke-static {v6, v8, v9}, Lflipboard/util/JavaUtil;->a(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_8

    iget-object v7, v0, Lflipboard/gui/Interstitial;->c:Lflipboard/gui/FLStaticTextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " | "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1422
    :goto_5
    iget-object v4, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v4, v3, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    move-object v4, v5

    .line 1432
    :cond_5
    :goto_6
    if-eqz v4, :cond_a

    .line 1433
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {p0, v4}, Lflipboard/activities/DetailActivity;->a(Lflipboard/objs/FeedItem;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    .line 1434
    iget v0, p0, Lflipboard/activities/DetailActivity;->z:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/activities/DetailActivity;->z:I

    .line 1435
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v0

    .line 1436
    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v3, v3, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v3, :cond_6

    iget-boolean v3, p0, Lflipboard/activities/DetailActivity;->aH:Z

    if-eqz v3, :cond_6

    .line 1437
    invoke-direct {p0}, Lflipboard/activities/DetailActivity;->I()V

    .line 1439
    :cond_6
    sget-object v3, Lflipboard/activities/DetailActivity;->n:Lflipboard/util/Log;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v2

    .line 1443
    :goto_7
    return-void

    :cond_7
    move v3, v1

    .line 1412
    goto/16 :goto_4

    .line 1421
    :cond_8
    iget-object v4, v0, Lflipboard/gui/Interstitial;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 1424
    :cond_9
    sget-object v6, Lflipboard/util/MeteringHelper$AccessType;->c:Lflipboard/util/MeteringHelper$AccessType;

    if-ne v0, v6, :cond_5

    .line 1426
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    const v4, 0x7f030158

    invoke-static {p0, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    move-object v4, v5

    .line 1427
    goto :goto_6

    .line 1441
    :cond_a
    sget-object v0, Lflipboard/activities/DetailActivity;->n:Lflipboard/util/Log;

    goto :goto_7

    :cond_b
    move-object v4, v5

    goto/16 :goto_3
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    const/4 v12, 0x2

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    const-wide/16 v10, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1224
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->ag:Z

    if-nez v0, :cond_c

    .line 1226
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 1227
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-lt v0, v12, :cond_3

    .line 1228
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    sub-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 1229
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 1230
    float-to-double v6, v0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    float-to-double v4, v4

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    iput-wide v4, p0, Lflipboard/activities/DetailActivity;->af:D

    .line 1231
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1232
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-nez v4, :cond_1

    .line 1280
    :cond_0
    :goto_0
    return v1

    .line 1236
    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1237
    iget-boolean v5, p0, Lflipboard/activities/DetailActivity;->aH:Z

    if-eqz v5, :cond_2

    sget-object v5, Lflipboard/activities/DetailActivity;->A:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_2

    .line 1238
    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget-object v7, Lflipboard/activities/DetailActivity;->A:Landroid/graphics/Bitmap;

    invoke-direct {v5, v6, v7}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1242
    :cond_2
    iget-wide v6, p0, Lflipboard/activities/DetailActivity;->ae:D

    cmpl-double v0, v6, v10

    if-lez v0, :cond_3

    .line 1243
    const/high16 v0, 0x3f800000    # 1.0f

    iget-wide v6, p0, Lflipboard/activities/DetailActivity;->af:D

    iget-wide v8, p0, Lflipboard/activities/DetailActivity;->ae:D

    div-double/2addr v6, v8

    double-to-float v5, v6

    invoke-static {v0, v5}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1244
    invoke-virtual {v4, v0}, Landroid/view/View;->setScaleX(F)V

    .line 1245
    invoke-virtual {v4, v0}, Landroid/view/View;->setScaleY(F)V

    .line 1248
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-ne v0, v12, :cond_4

    if-eqz v3, :cond_5

    :cond_4
    const/16 v0, 0x105

    if-ne v3, v0, :cond_6

    .line 1249
    :cond_5
    iget-wide v4, p0, Lflipboard/activities/DetailActivity;->af:D

    iput-wide v4, p0, Lflipboard/activities/DetailActivity;->ae:D

    .line 1251
    :cond_6
    if-eq v3, v2, :cond_7

    and-int/lit8 v0, v3, 0x6

    const/4 v3, 0x6

    if-ne v0, v3, :cond_a

    .line 1252
    :cond_7
    iget-wide v4, p0, Lflipboard/activities/DetailActivity;->ae:D

    cmpl-double v0, v4, v10

    if-lez v0, :cond_9

    iget-wide v4, p0, Lflipboard/activities/DetailActivity;->af:D

    iget-wide v6, p0, Lflipboard/activities/DetailActivity;->ae:D

    const-wide v8, 0x3ff199999999999aL    # 1.1

    mul-double/2addr v6, v8

    cmpg-double v0, v4, v6

    if-gtz v0, :cond_9

    .line 1255
    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->aF:Z

    if-eqz v0, :cond_8

    .line 1256
    invoke-direct {p0}, Lflipboard/activities/DetailActivity;->K()V

    :goto_1
    move v1, v2

    .line 1261
    goto :goto_0

    .line 1258
    :cond_8
    iput-boolean v2, p0, Lflipboard/activities/DetailActivity;->ay:Z

    .line 1259
    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->finish()V

    goto :goto_1

    .line 1263
    :cond_9
    iput-wide v10, p0, Lflipboard/activities/DetailActivity;->ae:D

    .line 1267
    :cond_a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-gt v0, v2, :cond_0

    .line 1276
    :cond_b
    :goto_2
    :try_start_0
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1270
    :cond_c
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    if-eqz v0, :cond_b

    .line 1272
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    invoke-virtual {v0}, Lflipboard/gui/item/WebDetailView;->getWebView()Lflipboard/gui/FLWebView;

    move-result-object v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    invoke-virtual {v0}, Lflipboard/gui/item/WebDetailView;->getWebView()Lflipboard/gui/FLWebView;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/FLWebView;->getScrollX()I

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-le v0, v2, :cond_e

    :cond_d
    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lflipboard/activities/DetailActivity;->W:Z

    goto :goto_2

    :cond_e
    move v0, v1

    goto :goto_3

    .line 1278
    :catch_0
    move-exception v0

    .line 1279
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v2, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method public dispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1213
    const/4 v0, 0x1

    return v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1810
    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->aF:Z

    if-eqz v0, :cond_0

    .line 1811
    invoke-direct {p0}, Lflipboard/activities/DetailActivity;->K()V

    .line 1815
    :goto_0
    return-void

    .line 1813
    :cond_0
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->e()V

    goto :goto_0
.end method

.method public finish()V
    .locals 8

    .prologue
    .line 1141
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1142
    iget v0, p0, Lflipboard/activities/DetailActivity;->au:I

    add-int/lit8 v0, v0, 0x1

    .line 1143
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v1, :cond_0

    .line 1144
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    iget v1, v1, Lflipboard/gui/flipping/FlipTransitionViews;->y:I

    add-int/2addr v0, v1

    .line 1146
    :cond_0
    const-string v1, "usage_intent_extra_flipcount"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1147
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ad:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ad:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1149
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1150
    const-string v0, "extra_result_item_id"

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1154
    :cond_1
    iget-wide v0, p0, Lflipboard/activities/DetailActivity;->V:J

    .line 1155
    iget-wide v4, p0, Lflipboard/activities/DetailActivity;->R:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_2

    .line 1156
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lflipboard/activities/DetailActivity;->R:J

    sub-long/2addr v4, v6

    add-long/2addr v0, v4

    .line 1158
    :cond_2
    iget-boolean v3, p0, Lflipboard/activities/FeedActivity;->G:Z

    if-eqz v3, :cond_3

    .line 1159
    const-string v3, "intent_extra_flag_inappropriate"

    iget-boolean v4, p0, Lflipboard/activities/FeedActivity;->G:Z

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1160
    const-string v3, "intent_extra_flag_inappropriate_feed_id"

    iget-object v4, p0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1162
    :cond_3
    const-string v3, "extra_result_active_time"

    iget-wide v4, p0, Lflipboard/activities/DetailActivity;->ai:J

    add-long/2addr v0, v4

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1163
    invoke-direct {p0}, Lflipboard/activities/DetailActivity;->J()Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;

    if-eqz v0, :cond_4

    .line 1164
    const-string v0, "extra_result_is_flipmag"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1166
    :cond_4
    const-string v0, "pages_since_last_ad"

    iget v1, p0, Lflipboard/activities/DetailActivity;->aE:I

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1167
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v2}, Lflipboard/activities/DetailActivity;->setResult(ILandroid/content/Intent;)V

    .line 1169
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->finish()V

    .line 1170
    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->ay:Z

    if-eqz v0, :cond_5

    .line 1171
    const/4 v0, 0x0

    const v1, 0x7f040004

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/DetailActivity;->overridePendingTransition(II)V

    .line 1173
    :cond_5
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1793
    const-string v0, "item"

    return-object v0
.end method

.method protected final i()V
    .locals 2

    .prologue
    .line 1481
    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->ax:Z

    if-eqz v0, :cond_1

    .line 1484
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "amazon"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "kftt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1485
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflipboard/activities/DetailActivity;->setRequestedOrientation(I)V

    .line 1492
    :goto_0
    return-void

    .line 1487
    :cond_0
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->i()V

    goto :goto_0

    .line 1490
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lflipboard/activities/DetailActivity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method final j()V
    .locals 5

    .prologue
    .line 460
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->M:Lflipboard/service/FlipboardManager;

    const-string v0, "tryInsertAdPage"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 461
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getRunningFlips()I

    move-result v0

    if-eqz v0, :cond_1

    .line 482
    :cond_0
    :goto_0
    return-void

    .line 464
    :cond_1
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentView()Lflipboard/gui/flipping/FlippingContainer;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlippingContainer;->getChild()Landroid/view/View;

    move-result-object v0

    .line 465
    instance-of v1, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;

    if-eqz v1, :cond_0

    .line 466
    check-cast v0, Lflipboard/gui/item/FlipmagDetailViewTablet;

    .line 467
    invoke-virtual {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getCurrentViewIndex()I

    move-result v1

    .line 468
    invoke-virtual {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getNumberOfPages()I

    move-result v2

    .line 469
    iget-object v3, p0, Lflipboard/activities/DetailActivity;->aA:Lflipboard/service/FLAdManager;

    invoke-virtual {v3, v1, v2}, Lflipboard/service/FLAdManager;->b(II)Lflipboard/service/FLAdManager$AdAsset;

    move-result-object v2

    .line 470
    if-eqz v2, :cond_0

    .line 471
    iget-object v1, v2, Lflipboard/service/FLAdManager$AdAsset;->b:Lflipboard/objs/Ad$Asset;

    if-eqz v1, :cond_0

    .line 472
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->aC:Landroid/util/SparseArray;

    iget-object v3, v2, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget v3, v3, Lflipboard/objs/Ad;->i:I

    iget-object v4, v2, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    invoke-virtual {v1, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 473
    const v1, 0x7f03009b

    const/4 v3, 0x0

    invoke-static {p0, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/item/AdItem;

    .line 474
    iget-object v3, p0, Lflipboard/activities/DetailActivity;->aA:Lflipboard/service/FLAdManager;

    invoke-static {}, Lflipboard/service/FLAdManager;->a()Landroid/graphics/Point;

    move-result-object v3

    .line 475
    iget v4, v3, Landroid/graphics/Point;->x:I

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2, v4, v3}, Lflipboard/gui/item/AdItem;->a(Lflipboard/service/FLAdManager$AdAsset;II)V

    .line 476
    iget-object v3, v2, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget-object v3, v3, Lflipboard/objs/Ad;->k:Lflipboard/objs/Ad$VideoInfo;

    invoke-virtual {v1, v3}, Lflipboard/gui/item/AdItem;->setVideoInfo(Lflipboard/objs/Ad$VideoInfo;)V

    .line 477
    iget-object v3, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    iget-object v4, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    invoke-virtual {v1, v3, v4}, Lflipboard/gui/item/AdItem;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 478
    iget-object v3, v2, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "FlipmagDetailView:addAdPage"

    invoke-static {v2}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    iget v4, v3, Lflipboard/objs/Ad;->i:I

    iget-object v2, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->o:Landroid/util/SparseArray;

    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->o:Landroid/util/SparseArray;

    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v0, v2}, Lflipboard/gui/item/FlipmagDetailViewTablet;->removeView(Landroid/view/View;)V

    iget-object v2, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->o:Landroid/util/SparseArray;

    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->remove(I)V

    :cond_2
    invoke-virtual {v0, v1}, Lflipboard/gui/item/FlipmagDetailViewTablet;->addView(Landroid/view/View;)V

    iget-object v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->o:Landroid/util/SparseArray;

    iget v2, v3, Lflipboard/objs/Ad;->i:I

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public final l()Lflipboard/gui/actionbar/FLActionBar;
    .locals 3

    .prologue
    const v2, 0x7f0a004c

    .line 1076
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_0

    .line 1077
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1078
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 1080
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v2}, Lflipboard/activities/DetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 1177
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FeedActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1179
    if-eqz p3, :cond_0

    .line 1180
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1181
    if-eqz v0, :cond_0

    .line 1182
    iget-wide v2, p0, Lflipboard/activities/DetailActivity;->ai:J

    const-string v1, "extra_result_active_time"

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lflipboard/activities/DetailActivity;->ai:J

    .line 1184
    const-string v1, "usage_intent_extra_flipcount"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1185
    iget v1, p0, Lflipboard/activities/DetailActivity;->au:I

    add-int/2addr v0, v1

    iput v0, p0, Lflipboard/activities/DetailActivity;->au:I

    .line 1188
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 1632
    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->ag:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ac:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ac:Landroid/view/View;

    instance-of v0, v0, Lflipboard/gui/item/WebDetailView;

    if-eqz v0, :cond_1

    .line 1633
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ac:Landroid/view/View;

    check-cast v0, Lflipboard/gui/item/WebDetailView;

    invoke-virtual {v0}, Lflipboard/gui/item/WebDetailView;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1653
    :cond_0
    :goto_0
    return-void

    .line 1636
    :cond_1
    invoke-direct {p0}, Lflipboard/activities/DetailActivity;->J()Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lflipboard/gui/item/WebDetailView;

    if-eqz v0, :cond_2

    .line 1637
    invoke-direct {p0}, Lflipboard/activities/DetailActivity;->J()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/WebDetailView;

    invoke-virtual {v0}, Lflipboard/gui/item/WebDetailView;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1642
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->d()I

    move-result v0

    if-lez v0, :cond_3

    .line 1643
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->d()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->c(I)Landroid/support/v4/app/FragmentManager$BackStackEntry;

    move-result-object v0

    .line 1644
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-interface {v0}, Landroid/support/v4/app/FragmentManager$BackStackEntry;->a()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentManager;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1645
    if-eqz v0, :cond_3

    .line 1646
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 1647
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->c()V

    goto :goto_0

    .line 1652
    :cond_3
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCloseCornerPressed(Landroid/view/View;)V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1497
    const v6, 0x102002c

    new-instance v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;

    const/4 v5, 0x0

    move-object v1, p0

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lflipboard/gui/actionbar/FLActionBarMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    invoke-virtual {p0, v6, v0}, Lflipboard/activities/DetailActivity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    .line 1498
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 1070
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1071
    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->z()V

    .line 1072
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 189
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 190
    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 447
    :goto_0
    return-void

    .line 194
    :cond_0
    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    .line 195
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    if-nez v0, :cond_1

    invoke-virtual {v7}, Landroid/os/Bundle;->size()I

    move-result v0

    if-gt v0, v6, :cond_1

    .line 196
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 197
    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->finish()V

    goto :goto_0

    .line 201
    :cond_1
    const-string v0, "pages_since_last_ad"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflipboard/activities/DetailActivity;->aE:I

    .line 202
    const-string v0, "extra_opened_from_widget"

    invoke-virtual {v7, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/activities/DetailActivity;->aF:Z

    .line 203
    const-string v0, "extra_opened_from_section_fragment"

    invoke-virtual {v7, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/activities/DetailActivity;->aH:Z

    .line 205
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    const-string v1, "nytimes"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v6

    :goto_1
    iput-boolean v0, p0, Lflipboard/activities/DetailActivity;->y:Z

    .line 206
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v0

    const-wide/32 v8, 0x12c00000

    cmp-long v0, v0, v8

    if-lez v0, :cond_2

    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->y:Z

    if-nez v0, :cond_2

    .line 207
    const/4 v0, 0x2

    iput v0, p0, Lflipboard/activities/DetailActivity;->o:I

    .line 208
    const/4 v0, 0x5

    iput v0, p0, Lflipboard/activities/DetailActivity;->p:I

    .line 211
    :cond_2
    const-string v0, "extra_item_ids"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 212
    if-eqz v0, :cond_8

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lflipboard/activities/DetailActivity;->ad:Ljava/util/List;

    .line 214
    if-eqz p1, :cond_9

    const-string v0, "extra_current_item"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 215
    const-string v0, "extra_current_item"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 219
    :goto_3
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    if-eqz v1, :cond_4

    .line 220
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v1, v0}, Lflipboard/service/Section;->a(Ljava/lang/String;)Lflipboard/objs/FeedItem;

    move-result-object v1

    iput-object v1, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    .line 221
    iget-boolean v1, p0, Lflipboard/activities/DetailActivity;->aF:Z

    if-eqz v1, :cond_4

    .line 222
    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v3, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v3}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Lflipboard/widget/FlipboardWidgetManager;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/model/ConfigSetting;->WidgetLogoHintEnabled:Z

    if-eqz v0, :cond_3

    .line 224
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->K()V

    .line 226
    :cond_3
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->m()Ljava/util/List;

    .line 230
    :cond_4
    const-string v0, "extra_content_discovery_from_source"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->aq:Landroid/os/Bundle;

    .line 231
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aq:Landroid/os/Bundle;

    if-eqz v0, :cond_5

    .line 232
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aq:Landroid/os/Bundle;

    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->ar:Ljava/lang/String;

    .line 236
    :cond_5
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-direct {p0, v2, v0, v1}, Lflipboard/activities/DetailActivity;->a(Landroid/view/View;Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V

    .line 237
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    iget-object v3, p0, Lflipboard/activities/DetailActivity;->ar:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lflipboard/activities/DetailActivity;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Ljava/lang/String;)V

    .line 239
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    invoke-static {p0, v0}, Lflipboard/util/MeteringHelper;->b(Landroid/content/Context;Lflipboard/objs/FeedItem;)Lflipboard/util/MeteringHelper$AccessType;

    move-result-object v0

    .line 240
    iget-boolean v1, p0, Lflipboard/activities/DetailActivity;->y:Z

    if-eqz v1, :cond_a

    sget-object v1, Lflipboard/util/MeteringHelper$AccessType;->c:Lflipboard/util/MeteringHelper$AccessType;

    if-ne v0, v1, :cond_a

    .line 241
    const v0, 0x7f030158

    invoke-static {p0, v0, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 242
    iput-boolean v6, p0, Lflipboard/activities/DetailActivity;->ag:Z

    .line 244
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "viewed"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    .line 245
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/io/UsageEvent;->d(Ljava/lang/String;)V

    .line 246
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    const-string v1, "itemType"

    const-string v3, "roadblock"

    invoke-virtual {v0, v1, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 319
    :cond_6
    :goto_4
    if-nez v2, :cond_19

    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->ak:Z

    if-nez v0, :cond_19

    .line 321
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 322
    const-string v0, "unwanted.DetailActivity_item_null"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    .line 323
    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->aF:Z

    if-eqz v0, :cond_18

    .line 324
    invoke-direct {p0}, Lflipboard/activities/DetailActivity;->K()V

    goto/16 :goto_0

    :cond_7
    move v0, v5

    .line 205
    goto/16 :goto_1

    .line 212
    :cond_8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    goto/16 :goto_2

    .line 217
    :cond_9
    const-string v0, "extra_current_item"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 249
    :cond_a
    const-string v0, "section_fetch_new"

    invoke-virtual {v7, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 250
    iput-boolean v6, p0, Lflipboard/activities/DetailActivity;->ak:Z

    .line 253
    :cond_b
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aq:Landroid/os/Bundle;

    if-eqz v0, :cond_c

    .line 254
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->aq:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lflipboard/io/UsageEvent;->a(Landroid/os/Bundle;)V

    .line 257
    :cond_c
    const-string v0, "launched_by_sstream"

    invoke-virtual {v7, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/activities/DetailActivity;->ap:Z

    .line 258
    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->ap:Z

    if-eqz v0, :cond_d

    .line 259
    const-string v0, "extra_flipboard_button_title"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->at:Ljava/lang/String;

    .line 260
    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_origin_section_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 261
    if-eqz v1, :cond_e

    .line 262
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->as:Lflipboard/service/Section;

    .line 263
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->as:Lflipboard/service/Section;

    if-nez v0, :cond_d

    .line 264
    new-instance v0, Lflipboard/service/Section;

    move-object v3, v2

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->as:Lflipboard/service/Section;

    .line 265
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->as:Lflipboard/service/Section;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 272
    :cond_d
    :goto_5
    const-string v0, "detail_image_url"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 273
    invoke-direct {p0, v7}, Lflipboard/activities/DetailActivity;->a(Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 274
    iput-boolean v6, p0, Lflipboard/activities/DetailActivity;->ag:Z

    goto/16 :goto_4

    .line 268
    :cond_e
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->as:Lflipboard/service/Section;

    goto :goto_5

    .line 275
    :cond_f
    const-string v0, "detail_open_url"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 276
    invoke-direct {p0, v7}, Lflipboard/activities/DetailActivity;->a(Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 280
    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "detail_open_url"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 281
    if-eqz v1, :cond_11

    .line 283
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v3}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v3

    iget-object v3, v3, Lflipboard/model/ConfigSetting;->TermsOfUseURLString:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 284
    iput-boolean v6, p0, Lflipboard/activities/DetailActivity;->an:Z

    .line 285
    const-string v2, "tos"

    .line 293
    :cond_10
    :goto_6
    if-eqz v2, :cond_11

    .line 294
    new-instance v1, Lflipboard/objs/UsageEventV2;

    sget-object v3, Lflipboard/objs/UsageEventV2$EventAction;->b:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v4, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v1, v3, v4}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 295
    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v1, v3, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 296
    invoke-virtual {v1}, Lflipboard/objs/UsageEventV2;->c()V

    .line 300
    :cond_11
    iput-boolean v6, p0, Lflipboard/activities/DetailActivity;->ag:Z

    move-object v2, v0

    .line 301
    goto/16 :goto_4

    .line 286
    :cond_12
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v3}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v3

    iget-object v3, v3, Lflipboard/model/ConfigSetting;->PrivacyPolicyURLString:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 287
    iput-boolean v6, p0, Lflipboard/activities/DetailActivity;->am:Z

    .line 288
    const-string v2, "privacy_policy"

    goto :goto_6

    .line 289
    :cond_13
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v3}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v3

    iget-object v3, v3, Lflipboard/model/ConfigSetting;->AccountHelpURLString:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 290
    iput-boolean v6, p0, Lflipboard/activities/DetailActivity;->ao:Z

    .line 291
    const-string v2, "forget_username_pwd"

    goto :goto_6

    .line 301
    :cond_14
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    if-nez v0, :cond_16

    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->ak:Z

    if-eqz v0, :cond_16

    .line 302
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    if-eqz v0, :cond_15

    .line 303
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    new-instance v1, Lflipboard/activities/DetailActivity$6;

    invoke-direct {v1, p0}, Lflipboard/activities/DetailActivity$6;-><init>(Lflipboard/activities/DetailActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v0, v6}, Lflipboard/service/Section;->d(Z)Z

    new-instance v0, Lflipboard/activities/DetailActivity$LoadingMorePage;

    invoke-direct {v0, p0, p0}, Lflipboard/activities/DetailActivity$LoadingMorePage;-><init>(Lflipboard/activities/DetailActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->al:Lflipboard/activities/DetailActivity$LoadingMorePage;

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->al:Lflipboard/activities/DetailActivity$LoadingMorePage;

    invoke-virtual {v0}, Lflipboard/activities/DetailActivity$LoadingMorePage;->a()V

    iget-object v2, p0, Lflipboard/activities/DetailActivity;->al:Lflipboard/activities/DetailActivity$LoadingMorePage;

    .line 304
    iput-boolean v6, p0, Lflipboard/activities/DetailActivity;->ag:Z

    goto/16 :goto_4

    .line 306
    :cond_15
    const-string v0, "unwanted.DetailActivity_section_null_from_sstream"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    .line 307
    iput-boolean v5, p0, Lflipboard/activities/DetailActivity;->ak:Z

    goto/16 :goto_4

    .line 309
    :cond_16
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_6

    .line 310
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    invoke-virtual {p0, v0}, Lflipboard/activities/DetailActivity;->a(Lflipboard/objs/FeedItem;)Landroid/view/View;

    move-result-object v2

    .line 311
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    invoke-static {p0, v0}, Lflipboard/util/MeteringHelper;->c(Landroid/content/Context;Lflipboard/objs/FeedItem;)V

    .line 312
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ad:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_17

    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->ax:Z

    if-nez v0, :cond_17

    move v0, v6

    :goto_7
    iput-boolean v0, p0, Lflipboard/activities/DetailActivity;->ag:Z

    .line 313
    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->y:Z

    if-eqz v0, :cond_6

    .line 314
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    invoke-static {p0, v0}, Lflipboard/util/MeteringHelper;->c(Landroid/content/Context;Lflipboard/objs/FeedItem;)V

    goto/16 :goto_4

    :cond_17
    move v0, v5

    .line 312
    goto :goto_7

    .line 326
    :cond_18
    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->finish()V

    goto/16 :goto_0

    .line 331
    :cond_19
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    const-string v1, "itemFlipCount"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 332
    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->ag:Z

    if-nez v0, :cond_1d

    .line 333
    new-instance v0, Lflipboard/activities/DetailActivity$2;

    invoke-direct {v0, p0, p0}, Lflipboard/activities/DetailActivity$2;-><init>(Lflipboard/activities/DetailActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    .line 357
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0, v5}, Lflipboard/gui/flipping/FlipTransitionViews;->setShouldHaveFlipToRefresh(Z)V

    .line 358
    new-instance v0, Lflipboard/activities/DetailActivity$3;

    invoke-direct {v0, p0}, Lflipboard/activities/DetailActivity$3;-><init>(Lflipboard/activities/DetailActivity;)V

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->aj:Lflipboard/util/Observer;

    .line 376
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->aj:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->a(Lflipboard/util/Observer;)V

    .line 377
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/flipping/FlipTransitionViews;->a(ILandroid/view/View;)Lflipboard/gui/flipping/FlippingContainer;

    .line 378
    iget v0, p0, Lflipboard/activities/DetailActivity;->z:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/activities/DetailActivity;->z:I

    .line 380
    new-instance v0, Lflipboard/activities/DetailActivity$4;

    invoke-direct {v0, p0}, Lflipboard/activities/DetailActivity$4;-><init>(Lflipboard/activities/DetailActivity;)V

    .line 390
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3, v0}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    .line 392
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {p0, v0}, Lflipboard/activities/DetailActivity;->setContentView(Landroid/view/View;)V

    .line 400
    :cond_1a
    :goto_8
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 402
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->D:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_1b

    .line 403
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    .line 404
    invoke-virtual {p0, v0, v6}, Lflipboard/activities/DetailActivity;->a(IZ)V

    .line 406
    new-instance v0, Lflipboard/activities/DetailActivity$5;

    invoke-direct {v0, p0}, Lflipboard/activities/DetailActivity$5;-><init>(Lflipboard/activities/DetailActivity;)V

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->aD:Lflipboard/util/Observer;

    .line 437
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->aD:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->a(Lflipboard/util/Observer;)V

    .line 440
    :cond_1b
    invoke-static {}, Lflipboard/util/AndroidUtil;->m()Z

    move-result v0

    if-nez v0, :cond_1c

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->b()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 443
    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->y()V

    .line 446
    :cond_1c
    invoke-static {p0}, Lflipboard/service/FlipboardManager;->a(Landroid/app/Activity;)Lcom/amazon/motiongestures/GestureManager;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/DetailActivity;->aI:Lcom/amazon/motiongestures/GestureManager;

    goto/16 :goto_0

    .line 394
    :cond_1d
    iput-object v2, p0, Lflipboard/activities/DetailActivity;->ac:Landroid/view/View;

    .line 395
    if-eqz v2, :cond_1a

    .line 396
    invoke-virtual {p0, v2}, Lflipboard/activities/DetailActivity;->setContentView(Landroid/view/View;)V

    goto :goto_8
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1086
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onDestroy()V

    .line 1088
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aA:Lflipboard/service/FLAdManager;

    if-eqz v0, :cond_1

    .line 1089
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aB:Lflipboard/util/Observer;

    if-eqz v0, :cond_0

    .line 1090
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aA:Lflipboard/service/FLAdManager;

    iget-object v2, p0, Lflipboard/activities/DetailActivity;->aB:Lflipboard/util/Observer;

    invoke-virtual {v0, v2}, Lflipboard/service/FLAdManager;->c(Lflipboard/util/Observer;)V

    .line 1091
    iput-object v1, p0, Lflipboard/activities/DetailActivity;->aB:Lflipboard/util/Observer;

    .line 1093
    :cond_0
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aA:Lflipboard/service/FLAdManager;

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lflipboard/service/FLAdManager;->a(IZ)V

    .line 1097
    :cond_1
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_a

    .line 1098
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    .line 1099
    iget-object v2, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v2}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v2

    if-le v2, v0, :cond_a

    .line 1100
    iget-object v2, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v2, v0}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v0

    .line 1103
    :goto_0
    invoke-direct {p0, v0}, Lflipboard/activities/DetailActivity;->a(Landroid/view/View;)V

    .line 1104
    iget-object v2, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    invoke-direct {p0, v2, v0}, Lflipboard/activities/DetailActivity;->a(Lflipboard/objs/FeedItem;Landroid/view/View;)V

    .line 1105
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_2

    .line 1106
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v2, p0, Lflipboard/activities/DetailActivity;->aj:Lflipboard/util/Observer;

    invoke-virtual {v0, v2}, Lflipboard/gui/flipping/FlipTransitionViews;->b(Lflipboard/util/Observer;)V

    .line 1108
    :cond_2
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aD:Lflipboard/util/Observer;

    if-eqz v0, :cond_3

    .line 1109
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v2, p0, Lflipboard/activities/DetailActivity;->aD:Lflipboard/util/Observer;

    invoke-virtual {v0, v2}, Lflipboard/gui/flipping/FlipTransitionViews;->b(Lflipboard/util/Observer;)V

    .line 1110
    iput-object v1, p0, Lflipboard/activities/DetailActivity;->aD:Lflipboard/util/Observer;

    .line 1115
    :cond_3
    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->an:Z

    if-eqz v0, :cond_7

    .line 1116
    const-string v0, "tos"

    .line 1122
    :goto_1
    if-eqz v0, :cond_4

    .line 1123
    new-instance v2, Lflipboard/objs/UsageEventV2;

    sget-object v3, Lflipboard/objs/UsageEventV2$EventAction;->d:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v4, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v2, v3, v4}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 1124
    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v2, v3, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 1125
    invoke-virtual {v2}, Lflipboard/objs/UsageEventV2;->c()V

    .line 1129
    :cond_4
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    invoke-virtual {v0}, Lflipboard/gui/item/WebDetailView;->getWebView()Lflipboard/gui/FLWebView;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1130
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    invoke-virtual {v0}, Lflipboard/gui/item/WebDetailView;->getWebView()Lflipboard/gui/FLWebView;

    move-result-object v0

    const-string v2, ""

    const-string v3, "text/html"

    const-string v4, "utf-8"

    invoke-virtual {v0, v2, v3, v4}, Lflipboard/gui/FLWebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1134
    :cond_5
    sget-object v0, Lflipboard/activities/DetailActivity;->A:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_6

    .line 1135
    sput-object v1, Lflipboard/activities/DetailActivity;->A:Landroid/graphics/Bitmap;

    .line 1137
    :cond_6
    return-void

    .line 1117
    :cond_7
    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->am:Z

    if-eqz v0, :cond_8

    .line 1118
    const-string v0, "privacy_policy"

    goto :goto_1

    .line 1119
    :cond_8
    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->ao:Z

    if-eqz v0, :cond_9

    .line 1120
    const-string v0, "forget_username_pwd"

    goto :goto_1

    :cond_9
    move-object v0, v1

    goto :goto_1

    :cond_a
    move-object v0, v1

    goto :goto_0
.end method

.method public onDetailImageClicked(Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v4, 0x65

    .line 1502
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1539
    :cond_0
    :goto_0
    return-void

    .line 1506
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 1507
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1508
    iget-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v2, "image"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1509
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflipboard/activities/DetailActivityStayOnRotation;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1510
    const-string v2, "extra_current_item"

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->Z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1511
    const-string v0, "sid"

    iget-object v2, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1512
    invoke-virtual {p0, v1, v4}, Lflipboard/activities/DetailActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1513
    :cond_2
    iget-object v1, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v2, "video"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1514
    iget-object v1, v0, Lflipboard/objs/FeedItem;->bY:Ljava/lang/String;

    .line 1515
    invoke-static {}, Lflipboard/util/YouTubeHelper;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    if-eqz v1, :cond_4

    const-string v2, "youtube"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1519
    iget-object v1, v0, Lflipboard/objs/FeedItem;->aX:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1520
    iget-object v1, v0, Lflipboard/objs/FeedItem;->aX:Ljava/lang/String;

    .line 1525
    :goto_1
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1526
    const-string v2, "v"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1527
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lflipboard/activities/YouTubePlayerActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1528
    const-string v3, "youtube_video_id"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1529
    const-string v1, "extra_current_item"

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->Z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1530
    const-string v0, "sid"

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1531
    const-string v0, "fromInsideitem"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1532
    sget-object v0, Lflipboard/util/YouTubeHelper;->a:Lflipboard/util/Log;

    .line 1533
    invoke-virtual {p0, v2, v4}, Lflipboard/activities/DetailActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1522
    :cond_3
    iget-object v1, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    goto :goto_1

    .line 1535
    :cond_4
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lflipboard/util/VideoUtil;->a(Landroid/app/Activity;Lflipboard/objs/FeedItem;Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method

.method public onLikeClicked(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1657
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 1658
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1659
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-static {v0, p0, v1}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V

    .line 1661
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 6

    .prologue
    .line 1543
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onPause()V

    .line 1544
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 1545
    iget-wide v2, p0, Lflipboard/activities/DetailActivity;->w:J

    iget-wide v4, p0, Lflipboard/activities/DetailActivity;->u:J

    sub-long v4, v0, v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lflipboard/activities/DetailActivity;->w:J

    .line 1546
    iget-wide v2, p0, Lflipboard/activities/DetailActivity;->x:J

    iget-wide v4, p0, Lflipboard/activities/DetailActivity;->v:J

    sub-long/2addr v0, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lflipboard/activities/DetailActivity;->x:J

    .line 1549
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getNumberOfPages()I

    move-result v0

    if-lez v0, :cond_1

    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->y:Z

    if-eqz v0, :cond_1

    .line 1550
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v1}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/flipping/FlipTransitionViews;->d(I)Landroid/view/View;

    move-result-object v0

    .line 1551
    instance-of v1, v0, Lflipboard/gui/RoadBlock;

    if-nez v1, :cond_0

    instance-of v0, v0, Lflipboard/gui/Interstitial;

    if-eqz v0, :cond_1

    .line 1552
    :cond_0
    invoke-virtual {p0}, Lflipboard/activities/DetailActivity;->finish()V

    .line 1555
    :cond_1
    sget-boolean v0, Lflipboard/service/FlipboardManager;->n:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    invoke-virtual {v0}, Lflipboard/gui/item/WebDetailView;->getWebView()Lflipboard/gui/FLWebView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1559
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    invoke-virtual {v0}, Lflipboard/gui/item/WebDetailView;->getWebView()Lflipboard/gui/FLWebView;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/FLWebView;->onPause()V

    .line 1562
    :cond_2
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aI:Lcom/amazon/motiongestures/GestureManager;

    if-eqz v0, :cond_3

    .line 1563
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aI:Lcom/amazon/motiongestures/GestureManager;

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aJ:Lcom/amazon/motiongestures/GestureListener;

    invoke-static {}, Lcom/amazon/motiongestures/GestureManager;->c()V

    .line 1565
    :cond_3
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1597
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 1598
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_0

    .line 1599
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    const-string v1, "extra_flipcount"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lflipboard/gui/flipping/FlipTransitionViews;->y:I

    .line 1603
    :goto_0
    return-void

    .line 1601
    :cond_0
    const-string v0, "extra_flipcount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/activities/DetailActivity;->au:I

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 1569
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onResume()V

    .line 1570
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 1571
    iput-wide v0, p0, Lflipboard/activities/DetailActivity;->u:J

    .line 1572
    iput-wide v0, p0, Lflipboard/activities/DetailActivity;->v:J

    .line 1573
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    invoke-virtual {v0}, Lflipboard/gui/flipping/FlipTransitionViews;->getCurrentViewIndex()I

    move-result v0

    .line 1574
    :goto_0
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "DetailActivity:onResume"

    new-instance v3, Lflipboard/activities/DetailActivity$15;

    invoke-direct {v3, p0, v0}, Lflipboard/activities/DetailActivity$15;-><init>(Lflipboard/activities/DetailActivity;I)V

    invoke-virtual {v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 1579
    sget-boolean v0, Lflipboard/service/FlipboardManager;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    invoke-virtual {v0}, Lflipboard/gui/item/WebDetailView;->getWebView()Lflipboard/gui/FLWebView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1582
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aG:Lflipboard/gui/item/WebDetailView;

    invoke-virtual {v0}, Lflipboard/gui/item/WebDetailView;->getWebView()Lflipboard/gui/FLWebView;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/FLWebView;->onResume()V

    .line 1585
    :cond_0
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aI:Lcom/amazon/motiongestures/GestureManager;

    if-eqz v0, :cond_1

    .line 1586
    invoke-static {}, Lcom/amazon/motiongestures/Gesture;->a()Lcom/amazon/motiongestures/Gesture;

    .line 1587
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aI:Lcom/amazon/motiongestures/GestureManager;

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->aJ:Lcom/amazon/motiongestures/GestureListener;

    invoke-static {}, Lcom/amazon/motiongestures/GestureManager;->b()V

    .line 1590
    :cond_1
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->aH:Z

    if-eqz v0, :cond_2

    .line 1591
    invoke-direct {p0}, Lflipboard/activities/DetailActivity;->I()V

    .line 1593
    :cond_2
    return-void

    .line 1573
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 991
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 992
    iget v0, p0, Lflipboard/activities/DetailActivity;->au:I

    .line 993
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    if-eqz v1, :cond_1

    .line 994
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 995
    const-string v1, "extra_current_item"

    iget-object v2, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    :cond_0
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->r:Lflipboard/gui/flipping/FlipTransitionViews;

    iget v1, v1, Lflipboard/gui/flipping/FlipTransitionViews;->y:I

    add-int/2addr v0, v1

    .line 999
    :cond_1
    const-string v1, "extra_flipcount"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1000
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 1201
    const/4 v0, 0x1

    return v0
.end method

.method public onShareClicked(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1665
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 1666
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 1667
    iget-object v1, p0, Lflipboard/activities/DetailActivity;->C:Lflipboard/service/Section;

    invoke-static {p0, v1, v0}, Lflipboard/util/SocialHelper;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 1669
    :cond_0
    return-void
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1207
    const/4 v0, 0x1

    return v0
.end method

.method protected final s_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1065
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aF:Lflipboard/objs/FeedArticle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aF:Lflipboard/objs/FeedArticle;

    iget-object v0, v0, Lflipboard/objs/FeedArticle;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aF:Lflipboard/objs/FeedArticle;

    iget-object v0, v0, Lflipboard/objs/FeedArticle;->d:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->s_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1288
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.VIEW"

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v0

    const-string v1, "android.intent.category.BROWSABLE"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "flipmag://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1293
    :goto_0
    return-void

    .line 1292
    :cond_0
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected final t_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1192
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aF:Lflipboard/objs/FeedArticle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aF:Lflipboard/objs/FeedArticle;

    iget-object v0, v0, Lflipboard/objs/FeedArticle;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1193
    iget-object v0, p0, Lflipboard/activities/DetailActivity;->ah:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aF:Lflipboard/objs/FeedArticle;

    iget-object v0, v0, Lflipboard/objs/FeedArticle;->a:Ljava/lang/String;

    .line 1195
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->t_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final u_()Z
    .locals 1

    .prologue
    .line 1799
    iget-boolean v0, p0, Lflipboard/activities/DetailActivity;->aF:Z

    if-eqz v0, :cond_0

    .line 1800
    invoke-direct {p0}, Lflipboard/activities/DetailActivity;->K()V

    .line 1801
    const/4 v0, 0x1

    .line 1805
    :goto_0
    return v0

    .line 1803
    :cond_0
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->u_()Z

    move-result v0

    goto :goto_0
.end method
