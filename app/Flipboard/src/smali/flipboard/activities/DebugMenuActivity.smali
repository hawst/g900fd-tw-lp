.class public Lflipboard/activities/DebugMenuActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "DebugMenuActivity.java"


# instance fields
.field private A:I

.field private final n:Ljava/lang/String;

.field private final o:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private final q:I

.field private final r:I

.field private final s:I

.field private final t:I

.field private final u:I

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Lflipboard/objs/FeedItem;

.field private y:Lflipboard/service/Section;

.field private z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 29
    const-string v0, "sectionId"

    iput-object v0, p0, Lflipboard/activities/DebugMenuActivity;->n:Ljava/lang/String;

    .line 30
    const-string v0, "feedItemId"

    iput-object v0, p0, Lflipboard/activities/DebugMenuActivity;->o:Ljava/lang/String;

    .line 31
    const-string v0, "activityType"

    iput-object v0, p0, Lflipboard/activities/DebugMenuActivity;->p:Ljava/lang/String;

    .line 32
    const/4 v0, 0x1

    iput v0, p0, Lflipboard/activities/DebugMenuActivity;->q:I

    .line 33
    const/4 v0, 0x2

    iput v0, p0, Lflipboard/activities/DebugMenuActivity;->r:I

    .line 34
    const/4 v0, 0x3

    iput v0, p0, Lflipboard/activities/DebugMenuActivity;->s:I

    .line 35
    const/4 v0, 0x4

    iput v0, p0, Lflipboard/activities/DebugMenuActivity;->t:I

    .line 36
    const/4 v0, 0x5

    iput v0, p0, Lflipboard/activities/DebugMenuActivity;->u:I

    .line 326
    return-void
.end method

.method static synthetic a(Lflipboard/activities/DebugMenuActivity;)Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity;->x:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method static synthetic b(Lflipboard/activities/DebugMenuActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity;->v:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lflipboard/activities/DebugMenuActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity;->w:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lflipboard/activities/DebugMenuActivity;)Ljava/util/List;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity;->z:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method protected final i()V
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lflipboard/activities/DebugMenuActivity;->setRequestedOrientation(I)V

    .line 223
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/16 v7, 0x8

    const/4 v3, 0x0

    .line 47
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-virtual {p0}, Lflipboard/activities/DebugMenuActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    :goto_0
    return-void

    .line 52
    :cond_0
    const v0, 0x7f03005d

    invoke-virtual {p0, v0}, Lflipboard/activities/DebugMenuActivity;->setContentView(I)V

    .line 54
    invoke-virtual {p0}, Lflipboard/activities/DebugMenuActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 55
    const-string v1, "activityType"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lflipboard/activities/DebugMenuActivity;->A:I

    .line 56
    const-string v1, "sectionId"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lflipboard/activities/DebugMenuActivity;->v:Ljava/lang/String;

    .line 57
    const-string v1, "feedItemId"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/DebugMenuActivity;->w:Ljava/lang/String;

    .line 59
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/activities/DebugMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 60
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 62
    const v0, 0x7f0a0163

    invoke-virtual {p0, v0}, Lflipboard/activities/DebugMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    .line 63
    const v1, 0x7f0a0164

    invoke-virtual {p0, v1}, Lflipboard/activities/DebugMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 64
    new-instance v4, Lflipboard/activities/DebugMenuActivity$DebugAdapter;

    invoke-direct {v4, p0, v3}, Lflipboard/activities/DebugMenuActivity$DebugAdapter;-><init>(Lflipboard/activities/DebugMenuActivity;B)V

    .line 65
    const v2, 0x7f0a0166

    invoke-virtual {p0, v2}, Lflipboard/activities/DebugMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 68
    iget v5, p0, Lflipboard/activities/DebugMenuActivity;->A:I

    if-ne v5, v8, :cond_3

    .line 69
    invoke-virtual {v1, v7}, Landroid/widget/ListView;->setVisibility(I)V

    .line 70
    iput-boolean v3, p0, Lflipboard/activities/DebugMenuActivity;->W:Z

    .line 71
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v4, p0, Lflipboard/activities/DebugMenuActivity;->v:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v1

    iget-object v4, p0, Lflipboard/activities/DebugMenuActivity;->w:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lflipboard/service/Section;->a(Ljava/lang/String;)Lflipboard/objs/FeedItem;

    move-result-object v1

    iput-object v1, p0, Lflipboard/activities/DebugMenuActivity;->x:Lflipboard/objs/FeedItem;

    .line 72
    new-instance v1, Lflipboard/activities/DebugMenuActivity$1;

    invoke-direct {v1, p0, v2}, Lflipboard/activities/DebugMenuActivity$1;-><init>(Lflipboard/activities/DebugMenuActivity;Landroid/widget/TextView;)V

    new-array v2, v8, [Ljava/lang/Void;

    const/4 v4, 0x0

    aput-object v4, v2, v3

    .line 87
    invoke-virtual {v1, v2}, Lflipboard/activities/DebugMenuActivity$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 88
    iget-object v1, p0, Lflipboard/activities/DebugMenuActivity;->x:Lflipboard/objs/FeedItem;

    if-eqz v1, :cond_2

    .line 89
    iget-object v1, p0, Lflipboard/activities/DebugMenuActivity;->x:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lflipboard/activities/DebugMenuActivity;->x:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    .line 90
    iget-object v1, p0, Lflipboard/activities/DebugMenuActivity;->x:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 92
    :cond_1
    const-string v1, "-No Title Given-"

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 95
    :cond_2
    const-string v1, "Item"

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 97
    :cond_3
    iget v5, p0, Lflipboard/activities/DebugMenuActivity;->A:I

    if-eq v5, v9, :cond_4

    iget v5, p0, Lflipboard/activities/DebugMenuActivity;->A:I

    const/4 v6, 0x5

    if-ne v5, v6, :cond_7

    .line 98
    :cond_4
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 100
    invoke-virtual {p0}, Lflipboard/activities/DebugMenuActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03005b

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 101
    new-instance v5, Landroid/content/Intent;

    const-class v2, Lflipboard/activities/DebugMenuActivity;

    invoke-direct {v5, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 102
    const-string v2, "sectionId"

    iget-object v3, p0, Lflipboard/activities/DebugMenuActivity;->v:Ljava/lang/String;

    invoke-virtual {v5, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 103
    new-instance v2, Lflipboard/activities/DebugMenuActivity$2;

    invoke-direct {v2, p0, v5}, Lflipboard/activities/DebugMenuActivity$2;-><init>(Lflipboard/activities/DebugMenuActivity;Landroid/content/Intent;)V

    invoke-virtual {v4, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    const v2, 0x7f0a004f

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLTextIntf;

    .line 113
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v6, p0, Lflipboard/activities/DebugMenuActivity;->v:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v3

    iput-object v3, p0, Lflipboard/activities/DebugMenuActivity;->y:Lflipboard/service/Section;

    .line 115
    iget v3, p0, Lflipboard/activities/DebugMenuActivity;->A:I

    if-ne v3, v9, :cond_5

    .line 116
    iget-object v3, p0, Lflipboard/activities/DebugMenuActivity;->y:Lflipboard/service/Section;

    invoke-virtual {v3}, Lflipboard/service/Section;->m()Ljava/util/List;

    move-result-object v3

    .line 117
    const-string v6, "Section dump info"

    invoke-interface {v2, v6}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 118
    const-string v2, "activityType"

    invoke-virtual {v5, v2, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object v2, v3

    .line 128
    :goto_1
    iget-object v3, p0, Lflipboard/activities/DebugMenuActivity;->y:Lflipboard/service/Section;

    invoke-virtual {v3}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 129
    iget-object v3, p0, Lflipboard/activities/DebugMenuActivity;->y:Lflipboard/service/Section;

    invoke-virtual {v3}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    :goto_2
    invoke-virtual {v1, v4}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 136
    new-instance v0, Lflipboard/activities/DebugMenuActivity$DebugImageListAdapter;

    invoke-direct {v0, p0, v2}, Lflipboard/activities/DebugMenuActivity$DebugImageListAdapter;-><init>(Lflipboard/activities/DebugMenuActivity;Ljava/util/List;)V

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 137
    new-instance v0, Lflipboard/activities/DebugMenuActivity$3;

    invoke-direct {v0, p0}, Lflipboard/activities/DebugMenuActivity$3;-><init>(Lflipboard/activities/DebugMenuActivity;)V

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto/16 :goto_0

    .line 120
    :cond_5
    iget-object v3, p0, Lflipboard/activities/DebugMenuActivity;->y:Lflipboard/service/Section;

    iget-object v6, p0, Lflipboard/activities/DebugMenuActivity;->w:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lflipboard/service/Section;->a(Ljava/lang/String;)Lflipboard/objs/FeedItem;

    move-result-object v3

    iput-object v3, p0, Lflipboard/activities/DebugMenuActivity;->x:Lflipboard/objs/FeedItem;

    .line 121
    new-instance v3, Ljava/util/ArrayList;

    iget-object v6, p0, Lflipboard/activities/DebugMenuActivity;->x:Lflipboard/objs/FeedItem;

    iget-object v6, v6, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 122
    const-string v6, "Group item info"

    invoke-interface {v2, v6}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 123
    const-string v2, "activityType"

    invoke-virtual {v5, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 124
    const-string v2, "feedItemId"

    iget-object v6, p0, Lflipboard/activities/DebugMenuActivity;->w:Ljava/lang/String;

    invoke-virtual {v5, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object v2, v3

    goto :goto_1

    .line 131
    :cond_6
    const-string v3, "Section"

    invoke-virtual {v0, v3}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 152
    :cond_7
    iget v5, p0, Lflipboard/activities/DebugMenuActivity;->A:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_8

    .line 153
    const-string v3, "All Sections"

    invoke-virtual {v0, v3}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 156
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->e:Ljava/util/List;

    .line 158
    new-instance v2, Lflipboard/activities/DebugMenuActivity$DebugSectionListAdapter;

    invoke-direct {v2, p0, v0}, Lflipboard/activities/DebugMenuActivity$DebugSectionListAdapter;-><init>(Lflipboard/activities/DebugMenuActivity;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 159
    new-instance v0, Lflipboard/activities/DebugMenuActivity$4;

    invoke-direct {v0, p0}, Lflipboard/activities/DebugMenuActivity$4;-><init>(Lflipboard/activities/DebugMenuActivity;)V

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto/16 :goto_0

    .line 174
    :cond_8
    iget v5, p0, Lflipboard/activities/DebugMenuActivity;->A:I

    if-ne v5, v10, :cond_10

    .line 175
    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v4, v4, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v5, p0, Lflipboard/activities/DebugMenuActivity;->v:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v4

    iput-object v4, p0, Lflipboard/activities/DebugMenuActivity;->y:Lflipboard/service/Section;

    .line 176
    invoke-virtual {v1, v7}, Landroid/widget/ListView;->setVisibility(I)V

    .line 177
    iput-boolean v3, p0, Lflipboard/activities/DebugMenuActivity;->W:Z

    .line 179
    iget-object v1, p0, Lflipboard/activities/DebugMenuActivity;->y:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v1, p0, Lflipboard/activities/DebugMenuActivity;->y:Lflipboard/service/Section;

    const-string v4, "Section Id = %s\n\n pos = %d: service = %s\n\n getSection() = %s\n\n remoteId = %s\n\n unreadRemoteId = %s\n\n title = %s\n\n nitems = %d\n\n meta = %s\n\n observers = %s\n\n islocal = %s\n\n isChanged = %s\n\n User = %s\n\n isGoogleFeed = %s\n\n needsUpdating = %s\n\n isOverflowSection = %s\n\n isSinleSource = %s\n\n strategy = %s\n\n"

    const/16 v0, 0x12

    new-array v5, v0, [Ljava/lang/Object;

    iget-object v0, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    aput-object v0, v5, v3

    iget v0, v1, Lflipboard/service/Section;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v8

    iget-object v0, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    aput-object v0, v5, v9

    const/4 v0, 0x3

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    iget-object v0, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    aput-object v0, v5, v10

    const/4 v0, 0x5

    iget-object v6, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v6, v6, Lflipboard/objs/TOCSection;->q:Ljava/lang/String;

    aput-object v6, v5, v0

    const/4 v0, 0x6

    invoke-virtual {v1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v6, 0x7

    iget-object v0, v1, Lflipboard/service/Section;->u:Ljava/util/List;

    if-eqz v0, :cond_9

    iget-object v0, v1, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    iget-object v0, v1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    aput-object v0, v5, v7

    const/16 v0, 0x9

    iget-object v3, v1, Lflipboard/util/Observable;->I:[Ljava/lang/Object;

    array-length v3, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v0

    const/16 v3, 0xa

    iget-boolean v0, v1, Lflipboard/service/Section;->v:Z

    if-eqz v0, :cond_a

    const-string v0, "True"

    :goto_4
    aput-object v0, v5, v3

    const/16 v3, 0xb

    iget-boolean v0, v1, Lflipboard/service/Section;->y:Z

    if-eqz v0, :cond_b

    const-string v0, "True"

    :goto_5
    aput-object v0, v5, v3

    const/16 v0, 0xc

    iget-object v3, v1, Lflipboard/service/Section;->z:Lflipboard/service/User;

    invoke-virtual {v3}, Lflipboard/service/User;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v0

    const/16 v3, 0xd

    iget-boolean v0, v1, Lflipboard/service/Section;->A:Z

    if-eqz v0, :cond_c

    const-string v0, "True"

    :goto_6
    aput-object v0, v5, v3

    const/16 v3, 0xe

    iget-boolean v0, v1, Lflipboard/service/Section;->B:Z

    if-eqz v0, :cond_d

    const-string v0, "True"

    :goto_7
    aput-object v0, v5, v3

    const/16 v3, 0xf

    iget-boolean v0, v1, Lflipboard/service/Section;->C:Z

    if-eqz v0, :cond_e

    const-string v0, "True"

    :goto_8
    aput-object v0, v5, v3

    const/16 v3, 0x10

    iget-boolean v0, v1, Lflipboard/service/Section;->D:Z

    if-eqz v0, :cond_f

    const-string v0, "True"

    :goto_9
    aput-object v0, v5, v3

    const/16 v0, 0x11

    iget-object v1, v1, Lflipboard/service/Section;->E:Ljava/lang/String;

    aput-object v1, v5, v0

    invoke-static {v4, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_9
    move v0, v3

    goto :goto_3

    :cond_a
    const-string v0, "False"

    goto :goto_4

    :cond_b
    const-string v0, "False"

    goto :goto_5

    :cond_c
    const-string v0, "False"

    goto :goto_6

    :cond_d
    const-string v0, "False"

    goto :goto_7

    :cond_e
    const-string v0, "False"

    goto :goto_8

    :cond_f
    const-string v0, "False"

    goto :goto_9

    .line 182
    :cond_10
    const-string v3, "Debug Menu"

    invoke-virtual {v0, v3}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 184
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/activities/DebugMenuActivity;->z:Ljava/util/List;

    .line 185
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity;->z:Ljava/util/List;

    const-string v2, "Item JSON"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity;->z:Ljava/util/List;

    const-string v2, "Section"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity;->z:Ljava/util/List;

    const-string v2, "All Sections"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 190
    new-instance v0, Lflipboard/activities/DebugMenuActivity$5;

    invoke-direct {v0, p0}, Lflipboard/activities/DebugMenuActivity$5;-><init>(Lflipboard/activities/DebugMenuActivity;)V

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto/16 :goto_0
.end method
