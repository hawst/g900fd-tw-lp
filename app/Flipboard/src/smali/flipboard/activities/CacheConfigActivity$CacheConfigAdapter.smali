.class Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;
.super Landroid/widget/BaseAdapter;
.source "CacheConfigActivity.java"


# instance fields
.field final synthetic a:Lflipboard/activities/CacheConfigActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/CacheConfigActivity;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;->a:Lflipboard/activities/CacheConfigActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x4

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 203
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 211
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 215
    if-nez p1, :cond_1

    .line 217
    if-nez p2, :cond_0

    .line 218
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;->a:Lflipboard/activities/CacheConfigActivity;

    invoke-virtual {v0}, Lflipboard/activities/CacheConfigActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03004f

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 221
    :cond_0
    sget-object v0, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    iget-object v0, v0, Lflipboard/io/DownloadManager;->e:Ljava/io/File;

    .line 222
    sget-object v1, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    iget-wide v2, v1, Lflipboard/io/DownloadManager;->o:J

    .line 223
    invoke-static {v2, v3}, Lflipboard/util/JavaUtil;->b(J)Ljava/lang/String;

    move-result-object v1

    .line 224
    sget-object v4, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    iget v4, v4, Lflipboard/io/DownloadManager;->n:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 225
    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Ljava/io/File;)J

    move-result-wide v6

    .line 226
    add-long/2addr v2, v6

    invoke-static {v2, v3}, Lflipboard/util/JavaUtil;->b(J)Ljava/lang/String;

    move-result-object v2

    .line 227
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;->a:Lflipboard/activities/CacheConfigActivity;

    invoke-virtual {v0}, Lflipboard/activities/CacheConfigActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0d02ab

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 229
    const v0, 0x7f0a004f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 230
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v8

    const/4 v1, 0x1

    aput-object v4, v5, v1

    const/4 v1, 0x2

    aput-object v2, v5, v1

    invoke-static {v3, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 258
    :goto_0
    return-object p2

    .line 234
    :cond_1
    if-nez p2, :cond_2

    .line 235
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;->a:Lflipboard/activities/CacheConfigActivity;

    invoke-virtual {v0}, Lflipboard/activities/CacheConfigActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030117

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 237
    :cond_2
    const v0, 0x7f0a0308

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 238
    const v1, 0x7f0a030b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    .line 240
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 242
    :pswitch_0
    iget-object v2, p0, Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;->a:Lflipboard/activities/CacheConfigActivity;

    invoke-virtual {v2}, Lflipboard/activities/CacheConfigActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d02ad

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 243
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;->a:Lflipboard/activities/CacheConfigActivity;

    invoke-virtual {v0}, Lflipboard/activities/CacheConfigActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;->a:Lflipboard/activities/CacheConfigActivity;

    invoke-static {v0}, Lflipboard/activities/CacheConfigActivity;->b(Lflipboard/activities/CacheConfigActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "internal"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0d02ac

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 244
    invoke-interface {v1, v8}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    goto :goto_0

    .line 243
    :cond_3
    const v0, 0x7f0d02aa

    goto :goto_1

    .line 248
    :pswitch_1
    iget-object v2, p0, Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;->a:Lflipboard/activities/CacheConfigActivity;

    invoke-virtual {v2}, Lflipboard/activities/CacheConfigActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d02ae

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 249
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;->a:Lflipboard/activities/CacheConfigActivity;

    invoke-static {v0}, Lflipboard/activities/CacheConfigActivity;->c(Lflipboard/activities/CacheConfigActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 250
    invoke-interface {v1, v8}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    goto :goto_0

    .line 254
    :pswitch_2
    iget-object v1, p0, Lflipboard/activities/CacheConfigActivity$CacheConfigAdapter;->a:Lflipboard/activities/CacheConfigActivity;

    invoke-virtual {v1}, Lflipboard/activities/CacheConfigActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d02a8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 240
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x2

    return v0
.end method
