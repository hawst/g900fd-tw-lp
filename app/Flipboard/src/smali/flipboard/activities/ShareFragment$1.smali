.class Lflipboard/activities/ShareFragment$1;
.super Ljava/lang/Object;
.source "ShareFragment.java"

# interfaces
.implements Lflipboard/service/Flap$TypedResultObserver;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/service/Flap$TypedResultObserver",
        "<",
        "Lflipboard/objs/FlipResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/activities/ShareFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/ShareFragment;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lflipboard/activities/ShareFragment$1;->a:Lflipboard/activities/ShareFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 158
    check-cast p1, Lflipboard/objs/FlipResponse;

    iget-object v0, p0, Lflipboard/activities/ShareFragment$1;->a:Lflipboard/activities/ShareFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflipboard/activities/ShareFragment;->a(Lflipboard/activities/ShareFragment;Ljava/lang/String;)Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lflipboard/objs/FlipResponse;->i:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lflipboard/objs/FlipResponse;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v1, p0, Lflipboard/activities/ShareFragment$1;->a:Lflipboard/activities/ShareFragment;

    iget-object v0, p1, Lflipboard/objs/FlipResponse;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FlipResponse$FlipImagesResult;

    invoke-virtual {v0}, Lflipboard/objs/FlipResponse$FlipImagesResult;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lflipboard/activities/ShareFragment;->a(Lflipboard/activities/ShareFragment;Ljava/lang/String;)Ljava/lang/String;

    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/activities/ShareFragment$1;->a:Lflipboard/activities/ShareFragment;

    invoke-static {v0}, Lflipboard/activities/ShareFragment;->a(Lflipboard/activities/ShareFragment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/ShareFragment$1;->a:Lflipboard/activities/ShareFragment;

    invoke-static {v0}, Lflipboard/activities/ShareFragment;->b(Lflipboard/activities/ShareFragment;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Lflipboard/activities/ShareFragment;->a:Lflipboard/util/Log;

    iget-object v0, p0, Lflipboard/activities/ShareFragment$1;->a:Lflipboard/activities/ShareFragment;

    invoke-static {v0}, Lflipboard/activities/ShareFragment;->a(Lflipboard/activities/ShareFragment;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/ShareFragment$1;->a:Lflipboard/activities/ShareFragment;

    invoke-static {v1}, Lflipboard/activities/ShareFragment;->d(Lflipboard/activities/ShareFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v1

    new-instance v2, Lflipboard/activities/ShareFragment$1$1;

    invoke-direct {v2, p0, v0}, Lflipboard/activities/ShareFragment$1$1;-><init>(Lflipboard/activities/ShareFragment$1;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p1, Lflipboard/objs/FlipResponse;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lflipboard/objs/FlipResponse;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v1, p0, Lflipboard/activities/ShareFragment$1;->a:Lflipboard/activities/ShareFragment;

    iget-object v0, p1, Lflipboard/objs/FlipResponse;->j:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FlipResponse$FlipImagesResult;

    invoke-virtual {v0}, Lflipboard/objs/FlipResponse$FlipImagesResult;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lflipboard/activities/ShareFragment;->a(Lflipboard/activities/ShareFragment;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 206
    sget-object v0, Lflipboard/activities/ShareFragment;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 207
    return-void
.end method
