.class Lflipboard/activities/CategoryPickerFragment$2;
.super Ljava/lang/Object;
.source "CategoryPickerFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lflipboard/gui/FLTextView;

.field final synthetic b:Lflipboard/gui/FLTextView;

.field final synthetic c:Lflipboard/gui/FLTextView;

.field final synthetic d:Lflipboard/activities/CategoryPickerFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/CategoryPickerFragment;Lflipboard/gui/FLTextView;Lflipboard/gui/FLTextView;Lflipboard/gui/FLTextView;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lflipboard/activities/CategoryPickerFragment$2;->d:Lflipboard/activities/CategoryPickerFragment;

    iput-object p2, p0, Lflipboard/activities/CategoryPickerFragment$2;->a:Lflipboard/gui/FLTextView;

    iput-object p3, p0, Lflipboard/activities/CategoryPickerFragment$2;->b:Lflipboard/gui/FLTextView;

    iput-object p4, p0, Lflipboard/activities/CategoryPickerFragment$2;->c:Lflipboard/gui/FLTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment$2;->d:Lflipboard/activities/CategoryPickerFragment;

    invoke-virtual {v0}, Lflipboard/activities/CategoryPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment$2;->d:Lflipboard/activities/CategoryPickerFragment;

    invoke-virtual {v0}, Lflipboard/activities/CategoryPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 87
    iget-object v2, p0, Lflipboard/activities/CategoryPickerFragment$2;->d:Lflipboard/activities/CategoryPickerFragment;

    invoke-static {v2}, Lflipboard/activities/CategoryPickerFragment;->a(Lflipboard/activities/CategoryPickerFragment;)Lflipboard/gui/FLSearchBar;

    move-result-object v2

    invoke-virtual {v2}, Lflipboard/gui/FLSearchBar;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 89
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment$2;->d:Lflipboard/activities/CategoryPickerFragment;

    invoke-static {v0}, Lflipboard/activities/CategoryPickerFragment;->a(Lflipboard/activities/CategoryPickerFragment;)Lflipboard/gui/FLSearchBar;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/FLSearchBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment$2;->d:Lflipboard/activities/CategoryPickerFragment;

    invoke-static {v0}, Lflipboard/activities/CategoryPickerFragment;->b(Lflipboard/activities/CategoryPickerFragment;)Lflipboard/gui/firstrun/CategoryPickerCloud;

    move-result-object v0

    iget-object v2, v0, Lflipboard/gui/firstrun/CategoryPickerCloud;->i:Lflipboard/gui/FLTextView;

    if-eqz v2, :cond_1

    iget-object v0, v0, Lflipboard/gui/firstrun/CategoryPickerCloud;->i:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment$2;->d:Lflipboard/activities/CategoryPickerFragment;

    invoke-static {v0}, Lflipboard/activities/CategoryPickerFragment;->b(Lflipboard/activities/CategoryPickerFragment;)Lflipboard/gui/firstrun/CategoryPickerCloud;

    move-result-object v0

    iget-object v2, p0, Lflipboard/activities/CategoryPickerFragment$2;->d:Lflipboard/activities/CategoryPickerFragment;

    invoke-static {v2}, Lflipboard/activities/CategoryPickerFragment;->b(Lflipboard/activities/CategoryPickerFragment;)Lflipboard/gui/firstrun/CategoryPickerCloud;

    move-result-object v2

    invoke-virtual {v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->getSelectedTopics()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/gui/firstrun/CategoryPickerCloud;->a(Ljava/util/Map;)V

    .line 91
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment$2;->a:Lflipboard/gui/FLTextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 92
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment$2;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lflipboard/activities/CategoryPickerFragment$2;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 97
    :cond_0
    return v1

    :cond_1
    move v0, v1

    .line 89
    goto :goto_0
.end method
