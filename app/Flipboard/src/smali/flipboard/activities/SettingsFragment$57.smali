.class Lflipboard/activities/SettingsFragment$57;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SettingsFragment.java"


# instance fields
.field final synthetic a:Landroid/widget/AdapterView$OnItemClickListener;

.field final synthetic b:Lflipboard/activities/SettingsFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment;Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    .prologue
    .line 2134
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$57;->b:Lflipboard/activities/SettingsFragment;

    iput-object p2, p0, Lflipboard/activities/SettingsFragment$57;->a:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;I)V
    .locals 4

    .prologue
    .line 2137
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$57;->b:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->d(Lflipboard/activities/SettingsFragment;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "font_size"

    iget-object v2, p0, Lflipboard/activities/SettingsFragment$57;->b:Lflipboard/activities/SettingsFragment;

    iget-object v2, v2, Lflipboard/activities/SettingsFragment;->h:[I

    aget v2, v2, p2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2138
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->a()V

    .line 2139
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$57;->b:Lflipboard/activities/SettingsFragment;

    new-instance v1, Lflipboard/activities/SettingsFragment$SettingsAdapter;

    iget-object v2, p0, Lflipboard/activities/SettingsFragment$57;->b:Lflipboard/activities/SettingsFragment;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lflipboard/activities/SettingsFragment$SettingsAdapter;-><init>(Lflipboard/activities/SettingsFragment;B)V

    invoke-static {v0, v1}, Lflipboard/activities/SettingsFragment;->a(Lflipboard/activities/SettingsFragment;Lflipboard/activities/SettingsFragment$SettingsAdapter;)Lflipboard/activities/SettingsFragment$SettingsAdapter;

    .line 2140
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$57;->b:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0a0144

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 2141
    iget-object v1, p0, Lflipboard/activities/SettingsFragment$57;->b:Lflipboard/activities/SettingsFragment;

    invoke-static {v1}, Lflipboard/activities/SettingsFragment;->i(Lflipboard/activities/SettingsFragment;)Lflipboard/activities/SettingsFragment$SettingsAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2142
    iget-object v1, p0, Lflipboard/activities/SettingsFragment$57;->a:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2143
    return-void
.end method
