.class Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;
.super Landroid/widget/BaseAdapter;
.source "NotificationSettingsActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic b:Lflipboard/activities/NotificationSettingsActivity;


# direct methods
.method public constructor <init>(Lflipboard/activities/NotificationSettingsActivity;)V
    .locals 5

    .prologue
    .line 61
    iput-object p1, p0, Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;->b:Lflipboard/activities/NotificationSettingsActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 62
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object v1, v0, Lflipboard/model/ConfigSetting;->PushNotificationSettings:Ljava/util/List;

    .line 63
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 64
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;->a:Ljava/util/List;

    .line 65
    if-eqz v1, :cond_1

    .line 66
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 67
    iget-object v3, p0, Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;->a:Ljava/util/List;

    new-instance v4, Landroid/util/Pair;

    iget-object v0, p1, Lflipboard/activities/NotificationSettingsActivity;->n:Lflipboard/json/FLObject;

    invoke-virtual {v0, v1}, Lflipboard/json/FLObject;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-direct {v4, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 70
    :cond_1
    return-void
.end method

.method private a(I)Landroid/util/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;->a(I)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 84
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 89
    invoke-direct {p0, p1}, Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;->a(I)Landroid/util/Pair;

    move-result-object v3

    .line 92
    if-eqz p2, :cond_0

    .line 94
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/NotificationSettingsActivity$ViewHolder;

    move-object v2, v0

    .line 104
    :goto_0
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 105
    iget-object v1, v2, Lflipboard/activities/NotificationSettingsActivity$ViewHolder;->a:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v0, p0, Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;->b:Lflipboard/activities/NotificationSettingsActivity;

    iget-object v4, v0, Lflipboard/activities/NotificationSettingsActivity;->n:Lflipboard/json/FLObject;

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v1, :cond_1

    iget-object v1, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :goto_1
    invoke-virtual {v4, v0, v1}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 107
    iget-object v1, v2, Lflipboard/activities/NotificationSettingsActivity$ViewHolder;->b:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 108
    return-object p2

    .line 96
    :cond_0
    iget-object v0, p0, Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;->b:Lflipboard/activities/NotificationSettingsActivity;

    const v1, 0x7f030117

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 97
    new-instance v1, Lflipboard/activities/NotificationSettingsActivity$ViewHolder;

    iget-object v0, p0, Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;->b:Lflipboard/activities/NotificationSettingsActivity;

    invoke-direct {v1, v0, v4}, Lflipboard/activities/NotificationSettingsActivity$ViewHolder;-><init>(Lflipboard/activities/NotificationSettingsActivity;B)V

    .line 98
    const v0, 0x7f0a0308

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, v1, Lflipboard/activities/NotificationSettingsActivity$ViewHolder;->a:Lflipboard/gui/FLLabelTextView;

    .line 99
    const v0, 0x7f0a030a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v1, Lflipboard/activities/NotificationSettingsActivity$ViewHolder;->b:Landroid/widget/CheckBox;

    .line 100
    iget-object v0, v1, Lflipboard/activities/NotificationSettingsActivity$ViewHolder;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 101
    iget-object v0, v1, Lflipboard/activities/NotificationSettingsActivity$ViewHolder;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 102
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v2, v1

    goto :goto_0

    .line 106
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 114
    invoke-direct {p0, p3}, Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;->a(I)Landroid/util/Pair;

    move-result-object v3

    .line 115
    iget-object v0, p0, Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;->b:Lflipboard/activities/NotificationSettingsActivity;

    iget-object v4, v0, Lflipboard/activities/NotificationSettingsActivity;->n:Lflipboard/json/FLObject;

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :goto_0
    invoke-virtual {v4, v0, v1}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 116
    :goto_1
    iget-object v0, p0, Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;->b:Lflipboard/activities/NotificationSettingsActivity;

    iget-object v1, v0, Lflipboard/activities/NotificationSettingsActivity;->n:Lflipboard/json/FLObject;

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    .line 117
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/NotificationSettingsActivity$ViewHolder;

    .line 118
    iget-object v0, v0, Lflipboard/activities/NotificationSettingsActivity$ViewHolder;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 119
    iget-object v0, p0, Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;->b:Lflipboard/activities/NotificationSettingsActivity;

    invoke-static {v0}, Lflipboard/activities/NotificationSettingsActivity;->a(Lflipboard/activities/NotificationSettingsActivity;)Z

    .line 120
    return-void

    :cond_0
    move v1, v2

    .line 115
    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method
