.class Lflipboard/activities/SubsectionActivity$ListItemAdapter;
.super Landroid/widget/BaseAdapter;
.source "SubsectionActivity.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lflipboard/activities/SubsectionActivity;

.field private c:Landroid/view/LayoutInflater;

.field private final d:Ljava/lang/String;

.field private e:Lflipboard/objs/SectionListItem;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lflipboard/activities/SubsectionActivity;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 222
    iput-object p1, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 223
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Lflipboard/activities/SubsectionActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->c:Landroid/view/LayoutInflater;

    .line 224
    iput-object p2, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a:Ljava/util/List;

    .line 225
    if-nez p2, :cond_0

    .line 226
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a:Ljava/util/List;

    .line 228
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->d:Ljava/lang/String;

    .line 229
    return-void
.end method

.method private a(I)Lflipboard/objs/ContentDrawerListItem;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/SubsectionActivity$ListItemAdapter;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 212
    iput-object p1, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->f:Ljava/lang/String;

    return-object p1
.end method

.method static a()V
    .locals 2

    .prologue
    .line 508
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 509
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Only the UI thread is allowed to do this"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 511
    :cond_0
    return-void
.end method

.method static synthetic a(Lflipboard/activities/SubsectionActivity$ListItemAdapter;)V
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->e:Lflipboard/objs/SectionListItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a:Ljava/util/List;

    iget-object v1, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->e:Lflipboard/objs/SectionListItem;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 497
    invoke-static {}, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a()V

    .line 498
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 499
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 212
    invoke-direct {p0, p1}, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a(I)Lflipboard/objs/ContentDrawerListItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 245
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 343
    invoke-direct {p0, p1}, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a(I)Lflipboard/objs/ContentDrawerListItem;

    move-result-object v1

    invoke-interface {v1}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    const v11, 0x7f09010f

    const v10, 0x7f0900b7

    const/16 v9, 0x8

    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 377
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 492
    :cond_0
    :goto_0
    return-object p2

    .line 384
    :cond_1
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    .line 386
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 390
    if-nez p2, :cond_2

    .line 391
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f03004f

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 392
    const v1, 0x7f0a004f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    .line 393
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 397
    :goto_1
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 395
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    goto :goto_1

    .line 400
    :cond_3
    if-eqz p2, :cond_7

    .line 402
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/activities/SubsectionActivity$ListItemAdapter$ViewHolder;

    .line 403
    iget-object v2, v1, Lflipboard/activities/SubsectionActivity$ListItemAdapter$ViewHolder;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->a()V

    move-object v2, v1

    .line 414
    :goto_2
    instance-of v1, v0, Lflipboard/objs/FeedItem;

    if-eqz v1, :cond_8

    move-object v1, v0

    .line 415
    check-cast v1, Lflipboard/objs/FeedItem;

    .line 416
    iget-object v1, v1, Lflipboard/objs/FeedItem;->d:Ljava/lang/String;

    .line 421
    :goto_3
    if-eqz v1, :cond_a

    iget-object v4, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-static {v4}, Lflipboard/activities/SubsectionActivity;->d(Lflipboard/activities/SubsectionActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 422
    const v1, 0x7f080005

    invoke-virtual {p2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 426
    :goto_4
    iget-object v4, v2, Lflipboard/activities/SubsectionActivity$ListItemAdapter$ViewHolder;->b:Lflipboard/gui/FLTextIntf;

    .line 427
    iget-object v5, v2, Lflipboard/activities/SubsectionActivity$ListItemAdapter$ViewHolder;->d:Lflipboard/gui/FLTextIntf;

    .line 428
    iget-object v6, v2, Lflipboard/activities/SubsectionActivity$ListItemAdapter$ViewHolder;->a:Lflipboard/gui/FLImageView;

    .line 429
    sget-object v1, Lflipboard/gui/FLImageView$Align;->a:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v6, v1}, Lflipboard/gui/FLImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 430
    if-eqz v4, :cond_4

    .line 431
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 432
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-virtual {v1}, Lflipboard/activities/SubsectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->t()Z

    move-result v1

    if-eqz v1, :cond_b

    const v1, 0x7f080022

    :goto_5
    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-interface {v4, v1}, Lflipboard/gui/FLTextIntf;->setTextColor(I)V

    .line 434
    :cond_4
    if-eqz v5, :cond_5

    .line 435
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->o()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_c

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->o()Ljava/lang/String;

    move-result-object v1

    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 436
    invoke-static {v5, v8}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLTextIntf;I)V

    .line 437
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->o()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 442
    :cond_5
    :goto_6
    if-eqz v6, :cond_f

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_f

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->u()Z

    move-result v1

    if-nez v1, :cond_f

    .line 443
    invoke-static {v6, v8}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 446
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-virtual {v1}, Lflipboard/activities/SubsectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->j()Ljava/lang/String;

    move-result-object v4

    const-string v5, "drawable"

    iget-object v7, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->d:Ljava/lang/String;

    invoke-virtual {v1, v4, v5, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 448
    if-eqz v1, :cond_d

    .line 449
    invoke-virtual {v6, v1}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    .line 454
    :goto_7
    invoke-virtual {v6}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 455
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->s()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 456
    iget-object v4, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-virtual {v4}, Lflipboard/activities/SubsectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 457
    iget-object v4, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-virtual {v4}, Lflipboard/activities/SubsectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 468
    :goto_8
    iget-object v1, v2, Lflipboard/activities/SubsectionActivity$ListItemAdapter$ViewHolder;->c:Lflipboard/gui/FLTextIntf;

    .line 469
    if-eqz v1, :cond_6

    .line 470
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->p()Ljava/lang/String;

    move-result-object v4

    .line 471
    if-eqz v4, :cond_10

    .line 472
    invoke-interface {v1, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 478
    :cond_6
    :goto_9
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->k()Z

    move-result v1

    if-nez v1, :cond_0

    .line 481
    iget-object v1, v2, Lflipboard/activities/SubsectionActivity$ListItemAdapter$ViewHolder;->e:Lflipboard/gui/FLTextIntf;

    .line 482
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v2

    if-ne v2, v9, :cond_12

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->w()I

    move-result v2

    if-lez v2, :cond_12

    .line 483
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->w()I

    move-result v2

    const/16 v3, 0x3e8

    if-ne v2, v3, :cond_11

    const-string v0, "999+"

    :goto_a
    invoke-interface {v1, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 484
    invoke-static {v1, v8}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLTextIntf;I)V

    goto/16 :goto_0

    .line 405
    :cond_7
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f03004d

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 406
    new-instance v2, Lflipboard/activities/SubsectionActivity$ListItemAdapter$ViewHolder;

    invoke-direct {v2, p0, v8}, Lflipboard/activities/SubsectionActivity$ListItemAdapter$ViewHolder;-><init>(Lflipboard/activities/SubsectionActivity$ListItemAdapter;B)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 407
    const v1, 0x7f0a00c0

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/activities/SubsectionActivity$ListItemAdapter$ViewHolder;->b:Lflipboard/gui/FLTextIntf;

    .line 408
    const v1, 0x7f0a00c1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/activities/SubsectionActivity$ListItemAdapter$ViewHolder;->c:Lflipboard/gui/FLTextIntf;

    .line 409
    const v1, 0x7f0a00c3

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/activities/SubsectionActivity$ListItemAdapter$ViewHolder;->d:Lflipboard/gui/FLTextIntf;

    .line 410
    const v1, 0x7f0a00be

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/activities/SubsectionActivity$ListItemAdapter$ViewHolder;->a:Lflipboard/gui/FLImageView;

    .line 411
    const v1, 0x7f0a0128

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/activities/SubsectionActivity$ListItemAdapter$ViewHolder;->e:Lflipboard/gui/FLTextIntf;

    goto/16 :goto_2

    :cond_8
    move-object v1, v0

    .line 418
    check-cast v1, Lflipboard/objs/SectionListItem;

    .line 419
    iget-object v4, v1, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    if-eqz v4, :cond_9

    iget-object v1, v1, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3

    :cond_9
    move-object v1, v3

    goto/16 :goto_3

    .line 424
    :cond_a
    invoke-virtual {p2, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 432
    :cond_b
    const v1, 0x7f080023

    goto/16 :goto_5

    .line 439
    :cond_c
    invoke-static {v5, v9}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLTextIntf;I)V

    goto/16 :goto_6

    .line 451
    :cond_d
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 459
    :cond_e
    iget-object v4, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-virtual {v4}, Lflipboard/activities/SubsectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 460
    iget-object v4, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-virtual {v4}, Lflipboard/activities/SubsectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_8

    .line 464
    :cond_f
    invoke-static {v6, v9}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_8

    .line 474
    :cond_10
    invoke-interface {v1, v3}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 483
    :cond_11
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->w()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_a

    .line 486
    :cond_12
    invoke-static {v1, v9}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLTextIntf;I)V

    goto/16 :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x2

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 325
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 331
    :goto_0
    return v0

    .line 329
    :cond_1
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    .line 331
    iget-object v3, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->e:Lflipboard/objs/SectionListItem;

    if-eq v0, v3, :cond_2

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v0

    if-eq v0, v1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const v7, 0x7f0a00ba

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 261
    invoke-virtual {p0, p3}, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->isEnabled(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 319
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    invoke-direct {p0, p3}, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a(I)Lflipboard/objs/ContentDrawerListItem;

    move-result-object v4

    .line 268
    instance-of v0, v4, Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_2

    move-object v0, v4

    .line 269
    check-cast v0, Lflipboard/objs/FeedItem;

    .line 270
    iget-object v3, v0, Lflipboard/objs/FeedItem;->d:Ljava/lang/String;

    .line 271
    iget-object v1, v0, Lflipboard/objs/FeedItem;->g:Ljava/lang/String;

    .line 272
    iget-object v0, v0, Lflipboard/objs/FeedItem;->c:Ljava/lang/String;

    move-object v8, v1

    move-object v1, v3

    move-object v3, v8

    .line 280
    :goto_1
    invoke-interface {v4}, Lflipboard/objs/ContentDrawerListItem;->t()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 281
    invoke-interface {v4}, Lflipboard/objs/ContentDrawerListItem;->q()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 282
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-interface {v4}, Lflipboard/objs/ContentDrawerListItem;->q()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v3, v4, v2}, Lflipboard/service/FlipboardManager;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;ZLflipboard/gui/dialog/FLDialogResponse;)Z

    goto :goto_0

    :cond_2
    move-object v0, v4

    .line 274
    check-cast v0, Lflipboard/objs/SectionListItem;

    .line 275
    iget-object v1, v0, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    if-eqz v1, :cond_9

    .line 276
    iget-object v1, v0, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 278
    :goto_2
    iget-object v0, v0, Lflipboard/objs/SectionListItem;->b:Ljava/lang/String;

    move-object v3, v0

    move-object v0, v2

    goto :goto_1

    .line 287
    :cond_3
    invoke-interface {v4}, Lflipboard/objs/ContentDrawerListItem;->k()Z

    move-result v6

    if-nez v6, :cond_6

    if-eqz v1, :cond_6

    .line 288
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 289
    const-string v2, "sid"

    invoke-virtual {v6, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 291
    if-eqz v0, :cond_4

    .line 292
    const-string v2, "extra_section_group_id"

    invoke-virtual {v6, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 294
    :cond_4
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    iget-object v0, v0, Lflipboard/activities/SubsectionActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    if-nez v0, :cond_5

    .line 295
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    iget-object v0, v0, Lflipboard/activities/SubsectionActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v7, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v0, Lflipboard/service/Section;

    invoke-interface {v4}, Lflipboard/objs/ContentDrawerListItem;->n()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-static {v3}, Lflipboard/activities/SubsectionActivity;->b(Lflipboard/activities/SubsectionActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4}, Lflipboard/objs/ContentDrawerListItem;->j()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v7, v0}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 297
    :cond_5
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v6}, Lflipboard/activities/SubsectionActivity;->setResult(ILandroid/content/Intent;)V

    .line 298
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-virtual {v0}, Lflipboard/activities/SubsectionActivity;->finish()V

    .line 299
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    const v1, 0x7f040004

    invoke-virtual {v0, v5, v1}, Lflipboard/activities/SubsectionActivity;->overridePendingTransition(II)V

    goto/16 :goto_0

    .line 301
    :cond_6
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    const v1, 0x7f03002e

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 302
    new-instance v2, Lflipboard/activities/SubsectionActivity$ListItemAdapter;

    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-interface {v4}, Lflipboard/objs/ContentDrawerListItem;->c()Ljava/util/List;

    move-result-object v5

    invoke-direct {v2, v0, v5}, Lflipboard/activities/SubsectionActivity$ListItemAdapter;-><init>(Lflipboard/activities/SubsectionActivity;Ljava/util/List;)V

    .line 303
    const v0, 0x7f0a00bd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 304
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 305
    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 306
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 307
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 309
    invoke-interface {v4}, Lflipboard/objs/ContentDrawerListItem;->c()Ljava/util/List;

    move-result-object v0

    .line 310
    if-eqz v0, :cond_7

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 311
    :cond_7
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v0, v2, v3, v4}, Lflipboard/activities/SubsectionActivity;->a(Lflipboard/activities/SubsectionActivity;Lflipboard/activities/SubsectionActivity$ListItemAdapter;Ljava/lang/String;Landroid/view/View;)V

    .line 314
    :cond_8
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-static {v0}, Lflipboard/activities/SubsectionActivity;->c(Lflipboard/activities/SubsectionActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->addView(Landroid/view/View;)V

    .line 315
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-static {v0}, Lflipboard/activities/SubsectionActivity;->c(Lflipboard/activities/SubsectionActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    const v2, 0x7f040019

    invoke-virtual {v0, v1, v2}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/content/Context;I)V

    .line 316
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-static {v0}, Lflipboard/activities/SubsectionActivity;->c(Lflipboard/activities/SubsectionActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    const v2, 0x7f04001c

    invoke-virtual {v0, v1, v2}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/content/Context;I)V

    .line 317
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-static {v0}, Lflipboard/activities/SubsectionActivity;->c(Lflipboard/activities/SubsectionActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showNext()V

    goto/16 :goto_0

    :cond_9
    move-object v1, v2

    goto/16 :goto_2
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 5

    .prologue
    .line 518
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-static {v0}, Lflipboard/activities/SubsectionActivity;->a(Lflipboard/activities/SubsectionActivity;)Lflipboard/objs/ConfigService;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 519
    if-lez p4, :cond_2

    add-int v0, p2, p3

    if-lt v0, p4, :cond_2

    const/4 v0, 0x1

    .line 520
    :goto_0
    if-eqz v0, :cond_1

    .line 523
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->f:Ljava/lang/String;

    .line 524
    const/4 v1, 0x0

    iput-object v1, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->f:Ljava/lang/String;

    .line 527
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->e:Lflipboard/objs/SectionListItem;

    if-nez v1, :cond_0

    new-instance v1, Lflipboard/objs/SectionListItem;

    invoke-direct {v1}, Lflipboard/objs/SectionListItem;-><init>()V

    iput-object v1, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->e:Lflipboard/objs/SectionListItem;

    iget-object v1, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->e:Lflipboard/objs/SectionListItem;

    iget-object v2, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    const v3, 0x7f0d01e4

    invoke-virtual {v2, v3}, Lflipboard/activities/SubsectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/objs/SectionListItem;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a:Ljava/util/List;

    iget-object v2, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->e:Lflipboard/objs/SectionListItem;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->notifyDataSetChanged()V

    .line 530
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v1

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v3, p0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->b:Lflipboard/activities/SubsectionActivity;

    invoke-static {v3}, Lflipboard/activities/SubsectionActivity;->a(Lflipboard/activities/SubsectionActivity;)Lflipboard/objs/ConfigService;

    move-result-object v3

    new-instance v4, Lflipboard/activities/SubsectionActivity$ListItemAdapter$1;

    invoke-direct {v4, p0, v0}, Lflipboard/activities/SubsectionActivity$ListItemAdapter$1;-><init>(Lflipboard/activities/SubsectionActivity$ListItemAdapter;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3, v0, v4}, Lflipboard/service/Flap;->a(Lflipboard/service/User;Lflipboard/objs/ConfigService;Ljava/lang/String;Lflipboard/service/Flap$SectionListObserver;)V

    .line 569
    :cond_1
    return-void

    .line 519
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 513
    return-void
.end method
