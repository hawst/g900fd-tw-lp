.class Lflipboard/activities/FlipboardActivity$5;
.super Ljava/lang/Object;
.source "FlipboardActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/activities/FlipboardActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/FlipboardActivity;)V
    .locals 0

    .prologue
    .line 520
    iput-object p1, p0, Lflipboard/activities/FlipboardActivity$5;->a:Lflipboard/activities/FlipboardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v0, 0x1

    const v9, 0x7f0d023f

    const/4 v1, 0x0

    .line 524
    iget-object v2, p0, Lflipboard/activities/FlipboardActivity$5;->a:Lflipboard/activities/FlipboardActivity;

    iget-boolean v2, v2, Lflipboard/activities/FlipboardActivity;->P:Z

    if-nez v2, :cond_1

    .line 525
    invoke-static {}, Lflipboard/activities/FlipboardActivity;->G()Z

    .line 595
    :cond_0
    :goto_0
    return-void

    .line 528
    :cond_1
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v2, v2, Lflipboard/app/FlipboardApplication;->g:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lflipboard/activities/FlipboardActivity$5;->a:Lflipboard/activities/FlipboardActivity;

    const-string v3, "tabletUpgrade"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, v2, Lflipboard/activities/FlipboardActivity;->N:Landroid/content/SharedPreferences;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "warned_"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    cmp-long v6, v6, v10

    if-lez v6, :cond_2

    move v2, v1

    :goto_1
    if-eqz v2, :cond_3

    .line 529
    invoke-static {}, Lflipboard/activities/FlipboardActivity;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 532
    const v2, 0x7f0d031d

    invoke-virtual {v0, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 533
    invoke-virtual {v0, v9}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 534
    const v2, 0x7f0d031c

    invoke-virtual {v0, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 535
    iput-boolean v1, v0, Lflipboard/gui/dialog/FLDialogFragment;->C:Z

    .line 536
    iget-object v1, p0, Lflipboard/activities/FlipboardActivity$5;->a:Lflipboard/activities/FlipboardActivity;

    iget-object v1, v1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "upgrade"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 537
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "tabletupgradenotice"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    goto :goto_0

    .line 528
    :cond_2
    iget-object v2, v2, Lflipboard/activities/FlipboardActivity;->N:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v6, "warned_last"

    invoke-interface {v2, v6, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "warned_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-static {v2}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    move v2, v0

    goto :goto_1

    .line 539
    :cond_3
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v2}, Lflipboard/app/FlipboardApplication;->l()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lflipboard/activities/FlipboardActivity$5;->a:Lflipboard/activities/FlipboardActivity;

    const-string v3, "date"

    invoke-virtual {v2, v3}, Lflipboard/activities/FlipboardActivity;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 541
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 542
    invoke-virtual {v0, v9}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 543
    const v1, 0x7f0d00bf

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 544
    iget-object v1, p0, Lflipboard/activities/FlipboardActivity$5;->a:Lflipboard/activities/FlipboardActivity;

    iget-object v1, v1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "date"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 545
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "incompatible"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 546
    const-string v1, "reason"

    const-string v2, "date not set"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 547
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    goto/16 :goto_0

    .line 548
    :cond_4
    sget-boolean v2, Lflipboard/service/FlipboardManager;->o:Z

    if-nez v2, :cond_7

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v2, Lflipboard/service/FlipboardManager;->j:Ljava/lang/String;

    if-eqz v3, :cond_5

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->j:Ljava/lang/String;

    const-string v3, "cn"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :goto_2
    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/activities/FlipboardActivity$5;->a:Lflipboard/activities/FlipboardActivity;

    const-string v2, "china"

    invoke-virtual {v0, v2}, Lflipboard/activities/FlipboardActivity;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 550
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    .line 551
    const-string v2, "state_not_optimized_for_china_displayed"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 552
    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    .line 554
    add-int/lit8 v1, v1, 0x1

    .line 555
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "state_not_optimized_for_china_displayed"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 558
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 560
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity$5;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0227

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 561
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v2

    iget-object v2, v2, Lflipboard/model/ConfigSetting;->AppDownloadURLChina:Ljava/lang/String;

    .line 562
    if-eqz v2, :cond_6

    .line 563
    iget-object v3, p0, Lflipboard/activities/FlipboardActivity$5;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v3}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0229

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 564
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 565
    iget-object v3, p0, Lflipboard/activities/FlipboardActivity$5;->a:Lflipboard/activities/FlipboardActivity;

    const v4, 0x7f0d0228

    invoke-virtual {v3, v4}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lflipboard/gui/dialog/FLAlertDialogFragment;->k:Ljava/lang/String;

    .line 566
    new-instance v3, Lflipboard/activities/FlipboardActivity$5$1;

    invoke-direct {v3, p0, v2}, Lflipboard/activities/FlipboardActivity$5$1;-><init>(Lflipboard/activities/FlipboardActivity$5;Ljava/lang/String;)V

    iput-object v3, v1, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 575
    const v2, 0x7f0d004a

    invoke-virtual {v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 579
    :goto_3
    iput-object v0, v1, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 580
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity$5;->a:Lflipboard/activities/FlipboardActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "china"

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 581
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "incompatible"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 582
    const-string v1, "reason"

    const-string v2, "not optimized for china"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 583
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 548
    goto/16 :goto_2

    .line 577
    :cond_6
    invoke-virtual {v1, v9}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    goto :goto_3

    .line 585
    :cond_7
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/FlipboardActivity$5;->a:Lflipboard/activities/FlipboardActivity;

    const-string v1, "compat"

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 588
    invoke-virtual {v0, v9}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 589
    const v1, 0x7f0d01cf

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 590
    iget-object v1, p0, Lflipboard/activities/FlipboardActivity$5;->a:Lflipboard/activities/FlipboardActivity;

    iget-object v1, v1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "incompatible"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 591
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "incompatible"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 592
    const-string v1, "reason"

    const-string v2, "not compatible"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 593
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    goto/16 :goto_0
.end method
