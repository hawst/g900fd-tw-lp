.class Lflipboard/activities/SwitchContentGuideActivity$2;
.super Ljava/lang/Object;
.source "SwitchContentGuideActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Z

.field final synthetic c:Lflipboard/activities/SwitchContentGuideActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/SwitchContentGuideActivity;Ljava/util/List;Z)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lflipboard/activities/SwitchContentGuideActivity$2;->c:Lflipboard/activities/SwitchContentGuideActivity;

    iput-object p2, p0, Lflipboard/activities/SwitchContentGuideActivity$2;->a:Ljava/util/List;

    iput-boolean p3, p0, Lflipboard/activities/SwitchContentGuideActivity$2;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 278
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity$2;->c:Lflipboard/activities/SwitchContentGuideActivity;

    iget-object v1, v0, Lflipboard/activities/SwitchContentGuideActivity;->n:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;

    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity$2;->c:Lflipboard/activities/SwitchContentGuideActivity;

    iget-object v2, v0, Lflipboard/activities/SwitchContentGuideActivity;->n:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;

    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity$2;->a:Ljava/util/List;

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v4, Lflipboard/objs/ConfigEdition;

    invoke-direct {v4}, Lflipboard/objs/ConfigEdition;-><init>()V

    iget-object v5, v2, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->b:Lflipboard/activities/SwitchContentGuideActivity;

    const v6, 0x7f0d02b0

    invoke-virtual {v5, v6}, Lflipboard/activities/SwitchContentGuideActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lflipboard/objs/ConfigEdition;->a:Ljava/lang/String;

    const-string v5, "language_system"

    iput-object v5, v4, Lflipboard/objs/ConfigEdition;->c:Ljava/lang/String;

    const-string v5, "locale_system"

    iput-object v5, v4, Lflipboard/objs/ConfigEdition;->b:Ljava/lang/String;

    invoke-virtual {v2}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->clear()V

    new-instance v5, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    invoke-direct {v5, v2, v4}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;-><init>(Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;Lflipboard/objs/ConfigEdition;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v5, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    invoke-direct {v5, v2, v4}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;-><init>(Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;Lflipboard/objs/ConfigEdition;)V

    invoke-virtual {v2, v5}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->add(Ljava/lang/Object;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigEdition;

    new-instance v5, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    invoke-direct {v5, v2, v0}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;-><init>(Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;Lflipboard/objs/ConfigEdition;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v5, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;

    invoke-direct {v5, v2, v0}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter$Edition;-><init>(Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;Lflipboard/objs/ConfigEdition;)V

    invoke-virtual {v2, v5}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iput-object v3, v1, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->a:Ljava/util/List;

    .line 280
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity$2;->c:Lflipboard/activities/SwitchContentGuideActivity;

    iget-object v0, v0, Lflipboard/activities/SwitchContentGuideActivity;->n:Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;

    invoke-virtual {v0}, Lflipboard/activities/SwitchContentGuideActivity$EditionsAdapter;->notifyDataSetChanged()V

    .line 281
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/activities/SwitchContentGuideActivity$2;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->d(Ljava/util/List;)V

    .line 283
    iget-boolean v0, p0, Lflipboard/activities/SwitchContentGuideActivity$2;->b:Z

    if-eqz v0, :cond_2

    .line 284
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity$2;->c:Lflipboard/activities/SwitchContentGuideActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/activities/SwitchContentGuideActivity;->setResult(I)V

    .line 285
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity$2;->c:Lflipboard/activities/SwitchContentGuideActivity;

    invoke-virtual {v0}, Lflipboard/activities/SwitchContentGuideActivity;->finish()V

    .line 292
    :cond_1
    :goto_1
    return-void

    .line 287
    :cond_2
    iget-object v0, p0, Lflipboard/activities/SwitchContentGuideActivity$2;->c:Lflipboard/activities/SwitchContentGuideActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "progress"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lflipboard/gui/dialog/FLProgressDialogFragment;

    .line 288
    if-eqz v0, :cond_1

    .line 289
    invoke-virtual {v0}, Lflipboard/gui/dialog/FLProgressDialogFragment;->b()V

    goto :goto_1
.end method
