.class Lflipboard/activities/CreateMagazineFragment$5$1;
.super Ljava/lang/Object;
.source "CreateMagazineFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/activities/CreateMagazineFragment$5;


# direct methods
.method constructor <init>(Lflipboard/activities/CreateMagazineFragment$5;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lflipboard/activities/CreateMagazineFragment$5$1;->a:Lflipboard/activities/CreateMagazineFragment$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 373
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment$5$1;->a:Lflipboard/activities/CreateMagazineFragment$5;

    iget-object v0, v0, Lflipboard/activities/CreateMagazineFragment$5;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-virtual {v0}, Lflipboard/activities/CreateMagazineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 375
    iget-boolean v1, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_0

    .line 376
    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment$5$1;->a:Lflipboard/activities/CreateMagazineFragment$5;

    iget-object v1, v1, Lflipboard/activities/CreateMagazineFragment$5;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-virtual {v1}, Lflipboard/activities/CreateMagazineFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "editing_magazine"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/DialogFragment;

    .line 377
    if-eqz v1, :cond_0

    .line 378
    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->a()V

    .line 382
    :cond_0
    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v1

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00c9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0201ce

    invoke-virtual {v1, v3, v2}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    .line 383
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 384
    const-string v2, "edit_magazine_object"

    iget-object v3, p0, Lflipboard/activities/CreateMagazineFragment$5$1;->a:Lflipboard/activities/CreateMagazineFragment$5;

    iget-object v3, v3, Lflipboard/activities/CreateMagazineFragment$5;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v3}, Lflipboard/activities/CreateMagazineFragment;->f(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/objs/Magazine;

    move-result-object v3

    invoke-virtual {v3}, Lflipboard/objs/Magazine;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 385
    iget-object v2, p0, Lflipboard/activities/CreateMagazineFragment$5$1;->a:Lflipboard/activities/CreateMagazineFragment$5;

    iget-object v2, v2, Lflipboard/activities/CreateMagazineFragment$5;->a:Lflipboard/activities/CreateMagazineFragment;

    iput-object v1, v2, Lflipboard/activities/FlipboardFragment;->k:Landroid/content/Intent;

    const/4 v1, -0x1

    iput v1, v2, Lflipboard/activities/FlipboardFragment;->l:I

    .line 387
    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment$5$1;->a:Lflipboard/activities/CreateMagazineFragment$5;

    iget-object v1, v1, Lflipboard/activities/CreateMagazineFragment$5;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v1}, Lflipboard/activities/CreateMagazineFragment;->g(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/service/Section;

    move-result-object v1

    iget-object v2, p0, Lflipboard/activities/CreateMagazineFragment$5$1;->a:Lflipboard/activities/CreateMagazineFragment$5;

    iget-object v2, v2, Lflipboard/activities/CreateMagazineFragment$5;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v2}, Lflipboard/activities/CreateMagazineFragment;->f(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/objs/Magazine;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/service/Section;->a(Lflipboard/objs/Magazine;)V

    .line 388
    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment$5$1;->a:Lflipboard/activities/CreateMagazineFragment$5;

    iget-object v1, v1, Lflipboard/activities/CreateMagazineFragment$5;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v1}, Lflipboard/activities/CreateMagazineFragment;->e(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v1

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, p0, Lflipboard/activities/CreateMagazineFragment$5$1;->a:Lflipboard/activities/CreateMagazineFragment$5;

    iget-object v2, v2, Lflipboard/activities/CreateMagazineFragment$5;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v2}, Lflipboard/activities/CreateMagazineFragment;->f(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/objs/Magazine;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/service/User;->a(Lflipboard/objs/Magazine;)V

    .line 389
    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->finish()V

    .line 390
    return-void
.end method
