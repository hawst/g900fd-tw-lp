.class Lflipboard/activities/SettingsFragment$23;
.super Lflipboard/activities/SettingsFragment$RowItem;
.source "SettingsFragment.java"


# instance fields
.field final synthetic a:Lflipboard/activities/SettingsFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 835
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$23;->a:Lflipboard/activities/SettingsFragment;

    invoke-direct {p0, p2}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 838
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 839
    const-string v0, "Recent messages: "

    iput-object v0, v2, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 840
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->Z:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    .line 841
    const/4 v0, 0x0

    .line 842
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->Z:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 843
    aput-object v0, v3, v1

    .line 844
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 845
    goto :goto_0

    .line 846
    :cond_0
    iput-object v3, v2, Lflipboard/gui/dialog/FLAlertDialogFragment;->q:[Ljava/lang/CharSequence;

    .line 847
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$23;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "network_log"

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 848
    return-void
.end method
