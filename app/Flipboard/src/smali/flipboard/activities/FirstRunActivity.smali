.class public Lflipboard/activities/FirstRunActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "FirstRunActivity.java"


# static fields
.field private static n:I

.field private static u:Z


# instance fields
.field private o:Lflipboard/gui/firstrun/FirstRunView;

.field private p:Lfr/castorflex/android/verticalviewpager/VerticalViewPager;

.field private q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lflipboard/model/FirstRunSection;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lflipboard/model/ConfigFirstLaunch;

.field private s:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

.field private t:Z

.field private v:Z

.field private w:J

.field private x:I

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x1

    sput v0, Lflipboard/activities/FirstRunActivity;->n:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 112
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lflipboard/activities/FirstRunActivity;->w:J

    .line 113
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/activities/FirstRunActivity;->x:I

    .line 828
    return-void
.end method

.method private I()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x1f4

    .line 649
    new-instance v0, Lflipboard/activities/FirstRunActivity$7;

    invoke-direct {v0, p0, p0}, Lflipboard/activities/FirstRunActivity$7;-><init>(Lflipboard/activities/FirstRunActivity;Lflipboard/activities/FlipboardActivity;)V

    iput-object v0, p0, Lflipboard/activities/FirstRunActivity;->s:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    .line 672
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 673
    const v0, 0x7f0d0103

    invoke-virtual {p0, v0}, Lflipboard/activities/FirstRunActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 674
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 675
    const v2, 0x7f0d0105

    invoke-virtual {v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 676
    iput-object v0, v1, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 677
    const v0, 0x7f0d023f

    invoke-virtual {v1, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->e(I)V

    .line 678
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "error"

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 719
    :goto_0
    return-void

    .line 683
    :cond_0
    const v0, 0x7f0d0048

    invoke-virtual {p0, v0}, Lflipboard/activities/FirstRunActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lflipboard/util/AndroidUtil;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)Lflipboard/gui/dialog/FLProgressDialogFragment;

    .line 689
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->w()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->az:Z

    if-eqz v0, :cond_1

    .line 690
    invoke-static {p0}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;)V

    goto :goto_0

    .line 695
    :cond_1
    new-instance v4, Lflipboard/activities/FirstRunActivity$8;

    invoke-direct {v4, p0}, Lflipboard/activities/FirstRunActivity$8;-><init>(Lflipboard/activities/FirstRunActivity;)V

    .line 701
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "fresh_user"

    const/4 v5, 0x1

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 703
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->w()Z

    move-result v0

    if-nez v0, :cond_2

    .line 704
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 705
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    new-instance v1, Lflipboard/activities/FirstRunActivity$9;

    invoke-direct {v1, p0, v4}, Lflipboard/activities/FirstRunActivity$9;-><init>(Lflipboard/activities/FirstRunActivity;Ljava/lang/Runnable;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto :goto_0

    .line 717
    :cond_2
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    const-string v1, "FirstRunActivity:tryBeginBuildingFlipboard:configSettingsLoaded"

    invoke-virtual {v0, v1, v4}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private J()Lflipboard/gui/firstrun/FirstRunView$PageType;
    .locals 3

    .prologue
    .line 811
    const/4 v0, 0x0

    .line 812
    iget-boolean v1, p0, Lflipboard/activities/FirstRunActivity;->y:Z

    if-eqz v1, :cond_1

    .line 813
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->o:Lflipboard/gui/firstrun/FirstRunView;

    invoke-virtual {v0}, Lflipboard/gui/firstrun/FirstRunView;->getCurrentPageType()Lflipboard/gui/firstrun/FirstRunView$PageType;

    move-result-object v0

    .line 825
    :cond_0
    :goto_0
    return-object v0

    .line 814
    :cond_1
    iget-boolean v1, p0, Lflipboard/activities/FirstRunActivity;->z:Z

    if-eqz v1, :cond_2

    .line 815
    sget-object v0, Lflipboard/gui/firstrun/FirstRunView$PageType;->a:Lflipboard/gui/firstrun/FirstRunView$PageType;

    goto :goto_0

    .line 817
    :cond_2
    iget-object v1, p0, Lflipboard/activities/FirstRunActivity;->p:Lfr/castorflex/android/verticalviewpager/VerticalViewPager;

    invoke-virtual {v1}, Lfr/castorflex/android/verticalviewpager/VerticalViewPager;->getCurrentItem()I

    move-result v1

    .line 818
    if-nez v1, :cond_3

    .line 819
    sget-object v0, Lflipboard/gui/firstrun/FirstRunView$PageType;->a:Lflipboard/gui/firstrun/FirstRunView$PageType;

    goto :goto_0

    .line 820
    :cond_3
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 821
    sget-object v0, Lflipboard/gui/firstrun/FirstRunView$PageType;->b:Lflipboard/gui/firstrun/FirstRunView$PageType;

    goto :goto_0
.end method

.method private a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 4

    .prologue
    .line 259
    const v0, 0x7f030014

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 260
    const v0, 0x7f0a0063

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    .line 261
    invoke-static {v2}, Lflipboard/activities/FirstRunActivity;->setTOSLinks(Landroid/view/View;)V

    .line 262
    iget-boolean v1, p0, Lflipboard/activities/FirstRunActivity;->z:Z

    if-eqz v1, :cond_0

    .line 263
    const v1, 0x7f0d0107

    invoke-virtual {p0, v1}, Lflipboard/activities/FirstRunActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 264
    const v1, 0x7f0a0060

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextView;

    const v3, 0x7f0d0266

    invoke-virtual {p0, v3}, Lflipboard/activities/FirstRunActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    const v1, 0x7f0a0062

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 269
    :cond_0
    new-instance v1, Lflipboard/activities/FirstRunActivity$3;

    invoke-direct {v1, p0}, Lflipboard/activities/FirstRunActivity$3;-><init>(Lflipboard/activities/FirstRunActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 284
    invoke-virtual {p0}, Lflipboard/activities/FirstRunActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f04000a

    invoke-static {v1, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 285
    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->startAnimation(Landroid/view/animation/Animation;)V

    .line 286
    return-object v2
.end method

.method static synthetic a(Lflipboard/activities/FirstRunActivity;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lflipboard/activities/FirstRunActivity;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/FirstRunActivity;Ljava/util/Set;)Ljava/util/Set;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lflipboard/activities/FirstRunActivity;->q:Ljava/util/Set;

    return-object p1
.end method

.method static a(Landroid/content/Context;Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 502
    const-string v0, "power"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 504
    const/4 v1, 0x1

    const-string v2, "FLIPBOARD"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 505
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 506
    new-instance v1, Lflipboard/activities/FirstRunActivity$6;

    invoke-direct {v1, p1, v0}, Lflipboard/activities/FirstRunActivity$6;-><init>(Ljava/lang/Runnable;Landroid/os/PowerManager$WakeLock;)V

    .line 516
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "FirstRunActivity:stayAwakeAndRun"

    invoke-virtual {v0, v2, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 517
    return-void
.end method

.method static a(Lflipboard/objs/UsageEventV2$EventAction;)V
    .locals 3

    .prologue
    .line 522
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, p0, v1}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 523
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v2, "reminder"

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 524
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->b()V

    .line 525
    return-void
.end method

.method static synthetic a(Lflipboard/activities/FirstRunActivity;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lflipboard/activities/FirstRunActivity;->z:Z

    return v0
.end method

.method static synthetic b(Lflipboard/activities/FirstRunActivity;)Lfr/castorflex/android/verticalviewpager/VerticalViewPager;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->p:Lfr/castorflex/android/verticalviewpager/VerticalViewPager;

    return-object v0
.end method

.method static synthetic c(Lflipboard/activities/FirstRunActivity;)Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/FirstRunActivity;->v:Z

    return v0
.end method

.method static synthetic d(Lflipboard/activities/FirstRunActivity;)Lflipboard/model/ConfigFirstLaunch;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->r:Lflipboard/model/ConfigFirstLaunch;

    return-object v0
.end method

.method static synthetic e(Lflipboard/activities/FirstRunActivity;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->q:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic f(Lflipboard/activities/FirstRunActivity;)Lflipboard/util/TOCBuilder$TOCBuilderObserver;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->s:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    return-object v0
.end method

.method static synthetic g(Lflipboard/activities/FirstRunActivity;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lflipboard/activities/FirstRunActivity;->I()V

    return-void
.end method

.method static synthetic r()Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    sput-boolean v0, Lflipboard/activities/FirstRunActivity;->u:Z

    return v0
.end method

.method public static setTOSLinks(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 291
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object v1, v0, Lflipboard/model/ConfigSetting;->TermsOfUseURLString:Ljava/lang/String;

    .line 292
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object v3, v0, Lflipboard/model/ConfigSetting;->PrivacyPolicyURLString:Ljava/lang/String;

    .line 293
    const v0, 0x7f0a0061

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 296
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d00fd

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v2

    const/4 v1, 0x1

    aput-object v3, v5, v1

    invoke-static {v4, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 297
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    .line 298
    invoke-interface {v3}, Landroid/text/Spanned;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 299
    invoke-interface {v3}, Landroid/text/Spanned;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const-class v4, Landroid/text/style/URLSpan;

    invoke-interface {v3, v2, v1, v4}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/URLSpan;

    .line 300
    array-length v4, v1

    if-lez v4, :cond_1

    .line 301
    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 302
    array-length v5, v1

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v1, v2

    .line 303
    invoke-interface {v3, v6}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    .line 304
    invoke-interface {v3, v6}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v8

    .line 305
    invoke-interface {v3, v6}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    move-result v9

    .line 306
    new-instance v10, Lflipboard/activities/FirstRunActivity$4;

    invoke-virtual {v6}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lflipboard/activities/FirstRunActivity$4;-><init>(Ljava/lang/String;)V

    .line 313
    invoke-virtual {v4, v6}, Landroid/text/SpannableString;->removeSpan(Ljava/lang/Object;)V

    .line 314
    invoke-virtual {v4, v10, v7, v8, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 302
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 316
    :cond_0
    invoke-virtual {v0, v4}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 320
    :cond_1
    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 538
    new-instance v0, Lflipboard/activities/FirstRunActivity$DataHolder;

    iget-object v1, p0, Lflipboard/activities/FirstRunActivity;->q:Ljava/util/Set;

    iget-object v2, p0, Lflipboard/activities/FirstRunActivity;->r:Lflipboard/model/ConfigFirstLaunch;

    iget-object v3, p0, Lflipboard/activities/FirstRunActivity;->s:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    invoke-direct {v0, p0, v1, v2, v3}, Lflipboard/activities/FirstRunActivity$DataHolder;-><init>(Lflipboard/activities/FirstRunActivity;Ljava/util/Set;Lflipboard/model/ConfigFirstLaunch;Lflipboard/util/TOCBuilder$TOCBuilderObserver;)V

    return-object v0
.end method

.method public doneSelecting(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 592
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 593
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 594
    const v1, 0x7f0d023f

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 595
    const v1, 0x7f0d00bf

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 596
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "date"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 632
    :goto_0
    return-void

    .line 598
    :cond_0
    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    .line 599
    iget-object v0, v4, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->t()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 600
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->q:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 601
    iget-object v5, v4, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 602
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 604
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->q:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v2

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/FirstRunSection;

    .line 605
    new-instance v8, Lflipboard/service/Section;

    iget-object v9, v0, Lflipboard/model/FirstRunSection;->title:Ljava/lang/String;

    invoke-direct {v8, v0, v9}, Lflipboard/service/Section;-><init>(Lflipboard/model/FirstRunSection;Ljava/lang/String;)V

    .line 607
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->q:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v1, v0, :cond_1

    move v0, v3

    .line 608
    :goto_2
    sget-object v9, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-virtual {v9}, Lflipboard/objs/UsageEventV2$EventCategory;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v3, v0, v9}, Lflipboard/service/User;->a(Lflipboard/service/Section;ZZLjava/lang/String;)Lflipboard/service/Section;

    .line 609
    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 610
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 611
    goto :goto_1

    :cond_1
    move v0, v2

    .line 607
    goto :goto_2

    .line 612
    :cond_2
    const/4 v0, 0x0

    invoke-static {p0, v6, v0, v3}, Lflipboard/util/TOCBuilder;->a(Lflipboard/activities/FlipboardActivity;Ljava/util/List;Lflipboard/util/TOCBuilder$TOCBuilderObserver;Z)V

    .line 615
    :cond_3
    invoke-virtual {v4}, Lflipboard/service/FlipboardManager;->n()V

    .line 617
    sget-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->d:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2$SectionNavFrom;->toString()Ljava/lang/String;

    invoke-static {p0}, Lflipboard/activities/LaunchActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/activities/FirstRunActivity;->startActivity(Landroid/content/Intent;)V

    .line 618
    invoke-virtual {p0}, Lflipboard/activities/FirstRunActivity;->finish()V

    goto :goto_0

    .line 619
    :cond_4
    iget-boolean v0, v4, Lflipboard/service/FlipboardManager;->af:Z

    if-eqz v0, :cond_5

    .line 621
    invoke-direct {p0}, Lflipboard/activities/FirstRunActivity;->I()V

    .line 623
    invoke-virtual {v4}, Lflipboard/service/FlipboardManager;->n()V

    .line 624
    iput-boolean v2, v4, Lflipboard/service/FlipboardManager;->af:Z

    goto :goto_0

    .line 627
    :cond_5
    invoke-virtual {p0}, Lflipboard/activities/FirstRunActivity;->j()V

    .line 629
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/CreateAccountActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "in_first_launch"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1e39

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/FirstRunActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method

.method final j()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 637
    iget-boolean v0, p0, Lflipboard/activities/FirstRunActivity;->y:Z

    if-eqz v0, :cond_0

    .line 638
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->d:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 639
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    sget-object v2, Lflipboard/gui/firstrun/FirstRunView$PageType;->b:Lflipboard/gui/firstrun/FirstRunView$PageType;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 640
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->B:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 641
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 642
    iput-boolean v3, p0, Lflipboard/activities/FirstRunActivity;->v:Z

    .line 644
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 732
    const/16 v2, 0x1e39

    if-ne p1, v2, :cond_4

    .line 733
    if-ne p2, v3, :cond_3

    .line 734
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2}, Lflipboard/service/User;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 735
    :goto_0
    if-eqz v0, :cond_2

    .line 737
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/FirstRunActivity$10;

    invoke-direct {v1, p0}, Lflipboard/activities/FirstRunActivity$10;-><init>(Lflipboard/activities/FirstRunActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    .line 761
    :cond_0
    :goto_1
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FlipboardActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 762
    return-void

    :cond_1
    move v0, v1

    .line 734
    goto :goto_0

    .line 744
    :cond_2
    iput-boolean v1, p0, Lflipboard/activities/FirstRunActivity;->v:Z

    goto :goto_1

    .line 747
    :cond_3
    if-eqz p3, :cond_0

    .line 748
    const-string v2, "result"

    invoke-virtual {p3, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 749
    const/16 v2, 0x65

    if-ne v1, v2, :cond_0

    .line 751
    sput-boolean v0, Lflipboard/activities/FirstRunActivity;->u:Z

    .line 752
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const/4 v2, 0x0

    iput-object v2, v1, Lflipboard/service/FlipboardManager;->W:Landroid/os/Bundle;

    .line 753
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflipboard/activities/LoginActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1, v0}, Lflipboard/activities/FirstRunActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 757
    :cond_4
    if-ne p2, v3, :cond_0

    .line 759
    invoke-virtual {p0}, Lflipboard/activities/FirstRunActivity;->finish()V

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 121
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 122
    invoke-static {}, Lflipboard/activities/LaunchActivity;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    invoke-virtual {p0}, Lflipboard/activities/FirstRunActivity;->finish()V

    .line 125
    :cond_0
    invoke-virtual {p0}, Lflipboard/activities/FirstRunActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 256
    :cond_1
    :goto_0
    return-void

    .line 128
    :cond_2
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {}, Lflipboard/service/User;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->t()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    .line 129
    :goto_1
    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v3, v3, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v3, :cond_3

    if-eqz v0, :cond_9

    :cond_3
    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lflipboard/activities/FirstRunActivity;->y:Z

    .line 130
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {}, Lflipboard/service/User;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->t()Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    .line 131
    :goto_3
    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v3}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v3

    if-eqz v3, :cond_b

    if-eqz v0, :cond_b

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lflipboard/activities/FirstRunActivity;->z:Z

    .line 134
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {}, Lflipboard/service/User;->a()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->t()Z

    move-result v0

    if-eqz v0, :cond_c

    move v0, v1

    .line 135
    :goto_5
    invoke-static {}, Lflipboard/abtest/testcase/SplashScreenTest;->a()Z

    move-result v3

    if-nez v3, :cond_4

    if-eqz v0, :cond_4

    .line 136
    iput-boolean v1, p0, Lflipboard/activities/FirstRunActivity;->z:Z

    .line 139
    :cond_4
    iput-boolean v2, p0, Lflipboard/activities/FirstRunActivity;->W:Z

    .line 141
    invoke-virtual {p0}, Lflipboard/activities/FirstRunActivity;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FirstRunActivity$DataHolder;

    .line 142
    if-eqz v0, :cond_d

    .line 143
    iget-object v3, v0, Lflipboard/activities/FirstRunActivity$DataHolder;->a:Ljava/util/Set;

    iput-object v3, p0, Lflipboard/activities/FirstRunActivity;->q:Ljava/util/Set;

    .line 144
    iget-object v3, v0, Lflipboard/activities/FirstRunActivity$DataHolder;->b:Lflipboard/model/ConfigFirstLaunch;

    iput-object v3, p0, Lflipboard/activities/FirstRunActivity;->r:Lflipboard/model/ConfigFirstLaunch;

    .line 145
    iget-object v0, v0, Lflipboard/activities/FirstRunActivity$DataHolder;->c:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    iput-object v0, p0, Lflipboard/activities/FirstRunActivity;->s:Lflipboard/util/TOCBuilder$TOCBuilderObserver;

    .line 191
    :cond_5
    :goto_6
    invoke-virtual {p0}, Lflipboard/activities/FirstRunActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 192
    iget-boolean v0, p0, Lflipboard/activities/FirstRunActivity;->y:Z

    if-eqz v0, :cond_17

    .line 193
    new-instance v0, Lflipboard/gui/firstrun/FirstRunView;

    invoke-direct {v0, p0}, Lflipboard/gui/firstrun/FirstRunView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/activities/FirstRunActivity;->o:Lflipboard/gui/firstrun/FirstRunView;

    .line 194
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->o:Lflipboard/gui/firstrun/FirstRunView;

    iget-object v4, p0, Lflipboard/activities/FirstRunActivity;->r:Lflipboard/model/ConfigFirstLaunch;

    iget-object v5, p0, Lflipboard/activities/FirstRunActivity;->q:Ljava/util/Set;

    invoke-virtual {v0, v4, v5}, Lflipboard/gui/firstrun/FirstRunView;->a(Lflipboard/model/ConfigFirstLaunch;Ljava/util/Set;)V

    .line 195
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->o:Lflipboard/gui/firstrun/FirstRunView;

    const-string v4, "extra_first_run_start_page"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v0, v4}, Lflipboard/gui/firstrun/FirstRunView;->setCurrentViewIndex(I)V

    .line 196
    if-eqz p1, :cond_6

    .line 197
    const-string v0, "flip_position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 198
    iget-object v4, p0, Lflipboard/activities/FirstRunActivity;->o:Lflipboard/gui/firstrun/FirstRunView;

    invoke-virtual {v4, v0}, Lflipboard/gui/firstrun/FirstRunView;->setCurrentViewIndex(I)V

    .line 201
    :cond_6
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->o:Lflipboard/gui/firstrun/FirstRunView;

    invoke-virtual {p0, v0}, Lflipboard/activities/FirstRunActivity;->setContentView(Landroid/view/View;)V

    .line 202
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->o:Lflipboard/gui/firstrun/FirstRunView;

    iget-object v4, p0, Lflipboard/activities/FirstRunActivity;->q:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    sget v5, Lflipboard/activities/FirstRunActivity;->n:I

    if-lt v4, v5, :cond_16

    :goto_7
    invoke-virtual {v0, v1}, Lflipboard/gui/firstrun/FirstRunView;->a(Z)V

    .line 236
    :goto_8
    if-nez p1, :cond_7

    invoke-virtual {p0}, Lflipboard/activities/FirstRunActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_show_invite_dialog"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 237
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 238
    const v1, 0x7f0d010c

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 239
    const v1, 0x7f0d010d

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 240
    const v1, 0x7f0d023f

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 241
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v4, "account_required"

    invoke-virtual {v0, v1, v4}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 245
    :cond_7
    const-string v0, "extra_submit_reminder_notification_usage"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 247
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v1, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->g:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->aD:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    .line 248
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "FirstRunActivity:onCreate:EXTRA_SUBMIT_REMINDER_NOTIFICATION_USAGE"

    new-instance v2, Lflipboard/activities/FirstRunActivity$2;

    invoke-direct {v2, p0}, Lflipboard/activities/FirstRunActivity$2;-><init>(Lflipboard/activities/FirstRunActivity;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 254
    const-string v0, "extra_submit_reminder_notification_usage"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 128
    goto/16 :goto_1

    :cond_9
    move v0, v2

    .line 129
    goto/16 :goto_2

    :cond_a
    move v0, v2

    .line 130
    goto/16 :goto_3

    :cond_b
    move v0, v2

    .line 131
    goto/16 :goto_4

    :cond_c
    move v0, v2

    .line 134
    goto/16 :goto_5

    .line 148
    :cond_d
    :try_start_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->A()Lflipboard/model/ConfigFirstLaunch;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/FirstRunActivity;->r:Lflipboard/model/ConfigFirstLaunch;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->o()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 156
    invoke-static {}, Lflipboard/abtest/testcase/ForcedAccountCreation;->a()Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    move-result-object v0

    .line 157
    if-eqz v0, :cond_e

    iget-boolean v0, v0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;->h:Z

    if-eqz v0, :cond_11

    .line 160
    :cond_e
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->j()V

    .line 161
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    iput-boolean v2, v0, Lflipboard/service/FlipboardManager;->af:Z

    .line 172
    :cond_f
    :goto_9
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lflipboard/activities/FirstRunActivity;->q:Ljava/util/Set;

    .line 174
    iget-boolean v0, p0, Lflipboard/activities/FirstRunActivity;->y:Z

    if-eqz v0, :cond_14

    .line 176
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->r:Lflipboard/model/ConfigFirstLaunch;

    iget-object v0, v0, Lflipboard/model/ConfigFirstLaunch;->SectionsToChooseFrom:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_10
    :goto_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/FirstRunSection;

    .line 177
    iget-boolean v4, v0, Lflipboard/model/FirstRunSection;->preselected:Z

    if-eqz v4, :cond_10

    .line 178
    iget-object v4, p0, Lflipboard/activities/FirstRunActivity;->q:Ljava/util/Set;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 150
    :catch_0
    move-exception v0

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Exception loading first launch config, can\'t create a new flipboard now"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    invoke-virtual {p0}, Lflipboard/activities/FirstRunActivity;->finish()V

    goto/16 :goto_0

    .line 164
    :cond_11
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->m()Z

    move-result v0

    if-nez v0, :cond_13

    move v0, v1

    .line 165
    :goto_b
    if-eqz v0, :cond_12

    .line 166
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->s()V

    .line 168
    :cond_12
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    iput-boolean v1, v0, Lflipboard/service/FlipboardManager;->af:Z

    goto :goto_9

    :cond_13
    move v0, v2

    .line 164
    goto :goto_b

    .line 185
    :cond_14
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->m()Z

    move-result v0

    if-nez v0, :cond_15

    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->p()Z

    move-result v0

    if-nez v0, :cond_15

    move v0, v1

    .line 186
    :goto_c
    if-eqz v0, :cond_5

    .line 187
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->s()V

    goto/16 :goto_6

    :cond_15
    move v0, v2

    .line 185
    goto :goto_c

    :cond_16
    move v1, v2

    .line 202
    goto/16 :goto_7

    .line 204
    :cond_17
    iget-boolean v0, p0, Lflipboard/activities/FirstRunActivity;->z:Z

    if-eqz v0, :cond_18

    .line 205
    invoke-virtual {p0}, Lflipboard/activities/FirstRunActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/activities/FirstRunActivity;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    .line 206
    invoke-virtual {p0, v0}, Lflipboard/activities/FirstRunActivity;->setContentView(Landroid/view/View;)V

    goto/16 :goto_8

    .line 208
    :cond_18
    const v0, 0x7f030070

    invoke-virtual {p0, v0}, Lflipboard/activities/FirstRunActivity;->setContentView(I)V

    .line 210
    const v0, 0x7f0a019e

    invoke-virtual {p0, v0}, Lflipboard/activities/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lfr/castorflex/android/verticalviewpager/VerticalViewPager;

    iput-object v0, p0, Lflipboard/activities/FirstRunActivity;->p:Lfr/castorflex/android/verticalviewpager/VerticalViewPager;

    .line 211
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->p:Lfr/castorflex/android/verticalviewpager/VerticalViewPager;

    new-instance v1, Lflipboard/activities/FirstRunActivity$PhoneFirstRunPagerAdapter;

    iget-object v4, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-direct {v1, p0, v4}, Lflipboard/activities/FirstRunActivity$PhoneFirstRunPagerAdapter;-><init>(Lflipboard/activities/FirstRunActivity;Landroid/support/v4/app/FragmentManager;)V

    invoke-virtual {v0, v1}, Lfr/castorflex/android/verticalviewpager/VerticalViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 213
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->p:Lfr/castorflex/android/verticalviewpager/VerticalViewPager;

    new-instance v1, Lflipboard/activities/FirstRunActivity$1;

    invoke-direct {v1, p0}, Lflipboard/activities/FirstRunActivity$1;-><init>(Lflipboard/activities/FirstRunActivity;)V

    invoke-virtual {v0, v1}, Lfr/castorflex/android/verticalviewpager/VerticalViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    goto/16 :goto_8
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 393
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onDestroy()V

    .line 394
    iget-boolean v0, p0, Lflipboard/activities/FirstRunActivity;->t:Z

    if-nez v0, :cond_0

    invoke-static {}, Lflipboard/activities/LaunchActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/FirstRunActivity;->t:Z

    new-instance v0, Lflipboard/activities/FirstRunActivity$5;

    invoke-direct {v0, p0}, Lflipboard/activities/FirstRunActivity$5;-><init>(Lflipboard/activities/FirstRunActivity;)V

    invoke-static {p0, v0}, Lflipboard/activities/FirstRunActivity;->a(Landroid/content/Context;Ljava/lang/Runnable;)V

    .line 395
    :cond_0
    return-void
.end method

.method public onFirstLaunchClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 564
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_1

    .line 565
    iget-wide v0, p0, Lflipboard/activities/FirstRunActivity;->w:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x1b58

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 566
    iget v0, p0, Lflipboard/activities/FirstRunActivity;->x:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 567
    const-string v0, "Build Flipboard Enabled"

    invoke-static {p0, v0}, Lflipboard/gui/FLToast;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 568
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/service/FlipboardManager;->af:Z

    .line 577
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/activities/FirstRunActivity;->x:I

    .line 580
    :cond_1
    :goto_0
    return-void

    .line 571
    :cond_2
    iget v0, p0, Lflipboard/activities/FirstRunActivity;->x:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/activities/FirstRunActivity;->x:I

    goto :goto_0

    .line 573
    :cond_3
    iget v0, p0, Lflipboard/activities/FirstRunActivity;->x:I

    if-nez v0, :cond_0

    .line 574
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/activities/FirstRunActivity;->w:J

    .line 575
    iget v0, p0, Lflipboard/activities/FirstRunActivity;->x:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/activities/FirstRunActivity;->x:I

    goto :goto_0
.end method

.method protected onPause()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 372
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onPause()V

    .line 373
    invoke-direct {p0}, Lflipboard/activities/FirstRunActivity;->J()Lflipboard/gui/firstrun/FirstRunView$PageType;

    move-result-object v3

    .line 374
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->t()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 377
    :goto_0
    iget-boolean v4, p0, Lflipboard/activities/FirstRunActivity;->v:Z

    if-nez v4, :cond_1

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lflipboard/activities/FirstRunActivity;->y:Z

    if-nez v0, :cond_0

    sget-object v0, Lflipboard/gui/firstrun/FirstRunView$PageType;->a:Lflipboard/gui/firstrun/FirstRunView$PageType;

    if-ne v3, v0, :cond_1

    .line 378
    :cond_0
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v4, Lflipboard/objs/UsageEventV2$EventAction;->d:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v5, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v4, v5}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 379
    sget-object v4, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v0, v4, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 381
    sget-boolean v3, Lflipboard/activities/FirstRunActivity;->u:Z

    if-nez v3, :cond_3

    .line 382
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->B:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 387
    :goto_1
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 389
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 374
    goto :goto_0

    .line 385
    :cond_3
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->B:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    goto :goto_1
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 325
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onResume()V

    .line 327
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "feed_first_guide_new_user_key"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 329
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v3, "show_firstlaunch_smartlink_message"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/activities/FirstRunActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "show_firstlaunch_smartlink_message"

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    new-instance v0, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    const v3, 0x7f0d0107

    invoke-virtual {v0, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    const v3, 0x7f0d00fb

    invoke-virtual {v0, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    const v3, 0x7f0d023f

    invoke-virtual {v0, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    iput-boolean v2, v0, Lflipboard/gui/dialog/FLDialogFragment;->C:Z

    new-instance v3, Lflipboard/activities/FirstRunActivity$11;

    invoke-direct {v3, p0}, Lflipboard/activities/FirstRunActivity$11;-><init>(Lflipboard/activities/FirstRunActivity;)V

    iput-object v3, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    iget-object v3, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v4, "smartlink_message"

    invoke-virtual {v0, v3, v4}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    .line 330
    invoke-virtual {p0}, Lflipboard/activities/FirstRunActivity;->x()V

    .line 335
    :cond_0
    new-instance v0, Landroid/content/Intent;

    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const-class v4, Lflipboard/activities/FirstRunActivity$AlarmReceiver;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 336
    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v4, 0x41f15

    const/high16 v5, 0x10000000

    invoke-static {v3, v4, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 337
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const-string v4, "alarm"

    invoke-virtual {v0, v4}, Lflipboard/app/FlipboardApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 338
    invoke-virtual {v0, v3}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 339
    iput-boolean v2, p0, Lflipboard/activities/FirstRunActivity;->t:Z

    .line 340
    sput-boolean v2, Lflipboard/activities/FirstRunActivity;->u:Z

    .line 343
    const/4 v0, 0x4

    invoke-static {p0, v0}, Lflipboard/notifications/FLNotification;->a(Landroid/content/Context;I)V

    .line 344
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->t()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 345
    :goto_1
    iget-boolean v1, p0, Lflipboard/activities/FirstRunActivity;->v:Z

    if-nez v1, :cond_2

    .line 346
    new-instance v1, Lflipboard/objs/UsageEventV2;

    sget-object v3, Lflipboard/objs/UsageEventV2$EventAction;->b:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v4, Lflipboard/objs/UsageEventV2$EventCategory;->b:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v1, v3, v4}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 347
    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-direct {p0}, Lflipboard/activities/FirstRunActivity;->J()Lflipboard/gui/firstrun/FirstRunView$PageType;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 348
    if-eqz v0, :cond_1

    sget-object v3, Lflipboard/gui/firstrun/FirstRunView$PageType;->a:Lflipboard/gui/firstrun/FirstRunView$PageType;

    invoke-direct {p0}, Lflipboard/activities/FirstRunActivity;->J()Lflipboard/gui/firstrun/FirstRunView$PageType;

    move-result-object v4

    if-ne v3, v4, :cond_1

    .line 350
    iget-object v3, p0, Lflipboard/activities/FirstRunActivity;->N:Landroid/content/SharedPreferences;

    const-string v4, "app_view_count"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 351
    iget-object v3, p0, Lflipboard/activities/FirstRunActivity;->N:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "app_view_count"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 352
    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->E:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 355
    :cond_1
    if-eqz v0, :cond_6

    .line 356
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->u:Lflipboard/objs/UsageEventV2$CommonEventData;

    sget-object v2, Lflipboard/objs/UsageEventV2$MethodEventData;->a:Lflipboard/objs/UsageEventV2$MethodEventData;

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 360
    :goto_2
    invoke-virtual {v1}, Lflipboard/objs/UsageEventV2;->c()V

    .line 363
    :cond_2
    invoke-static {}, Lflipboard/activities/LaunchActivity;->e()Z

    move-result v0

    if-nez v0, :cond_3

    .line 365
    sget-object v0, Lflipboard/objs/UsageEventV2$SectionNavFrom;->d:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2$SectionNavFrom;->toString()Ljava/lang/String;

    invoke-static {p0}, Lflipboard/activities/LaunchActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/activities/FirstRunActivity;->startActivity(Landroid/content/Intent;)V

    .line 366
    invoke-virtual {p0}, Lflipboard/activities/FirstRunActivity;->finish()V

    .line 368
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 329
    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 344
    goto :goto_1

    .line 358
    :cond_6
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->u:Lflipboard/objs/UsageEventV2$CommonEventData;

    sget-object v2, Lflipboard/objs/UsageEventV2$MethodEventData;->b:Lflipboard/objs/UsageEventV2$MethodEventData;

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    goto :goto_2
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 530
    iget-boolean v0, p0, Lflipboard/activities/FirstRunActivity;->y:Z

    if-eqz v0, :cond_0

    .line 531
    const-string v0, "flip_position"

    iget-object v1, p0, Lflipboard/activities/FirstRunActivity;->o:Lflipboard/gui/firstrun/FirstRunView;

    invoke-virtual {v1}, Lflipboard/gui/firstrun/FirstRunView;->getCurrentViewIndex()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 533
    :cond_0
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 767
    const/4 v0, 0x1

    return v0
.end method

.method public onSignInClicked(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 584
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "feed_first_guide_new_user_key"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 585
    sput-boolean v3, Lflipboard/activities/FirstRunActivity;->u:Z

    .line 586
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const/4 v1, 0x0

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->W:Landroid/os/Bundle;

    .line 587
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "in_first_launch"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lflipboard/activities/FirstRunActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 588
    return-void
.end method

.method public onTileClicked(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 723
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/FirstRunSection;

    .line 725
    iget-object v2, p0, Lflipboard/activities/FirstRunActivity;->q:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v1

    :goto_0
    iget-object v4, p0, Lflipboard/activities/FirstRunActivity;->o:Lflipboard/gui/firstrun/FirstRunView;

    iget-object v5, p0, Lflipboard/activities/FirstRunActivity;->q:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    sget v6, Lflipboard/activities/FirstRunActivity;->n:I

    if-lt v5, v6, :cond_1

    :goto_1
    invoke-virtual {v4, v3}, Lflipboard/gui/firstrun/FirstRunView;->a(Z)V

    .line 726
    const v1, 0x7f0a018e

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLStaticTextView;

    invoke-static {p0, p1, v1, v0, v2}, Lflipboard/gui/firstrun/SelectCategoriesGrid;->a(Landroid/content/Context;Landroid/view/View;Lflipboard/gui/FLStaticTextView;Lflipboard/model/FirstRunSection;Z)V

    .line 727
    return-void

    .line 725
    :cond_0
    iget-object v2, p0, Lflipboard/activities/FirstRunActivity;->q:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move v2, v3

    goto :goto_0

    :cond_1
    move v3, v1

    goto :goto_1
.end method
