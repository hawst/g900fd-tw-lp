.class Lflipboard/activities/ChooseAccountActivity$AccountItem;
.super Ljava/lang/Object;
.source "ChooseAccountActivity.java"


# instance fields
.field a:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

.field b:Lflipboard/objs/ConfigService;

.field c:Lflipboard/service/Account;

.field d:Lflipboard/objs/SectionListItem;

.field e:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;)V
    .locals 0

    .prologue
    .line 377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 378
    iput-object p1, p0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->a:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    .line 379
    return-void
.end method

.method constructor <init>(Lflipboard/objs/ConfigService;)V
    .locals 1

    .prologue
    .line 394
    sget-object v0, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->d:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    invoke-direct {p0, v0}, Lflipboard/activities/ChooseAccountActivity$AccountItem;-><init>(Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;)V

    .line 395
    iput-object p1, p0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->b:Lflipboard/objs/ConfigService;

    .line 396
    return-void
.end method

.method constructor <init>(Lflipboard/objs/SectionListItem;Lflipboard/objs/ConfigService;)V
    .locals 1

    .prologue
    .line 388
    sget-object v0, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->b:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    invoke-direct {p0, v0}, Lflipboard/activities/ChooseAccountActivity$AccountItem;-><init>(Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;)V

    .line 389
    iput-object p1, p0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->d:Lflipboard/objs/SectionListItem;

    .line 390
    iput-object p2, p0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->b:Lflipboard/objs/ConfigService;

    .line 391
    return-void
.end method

.method constructor <init>(Lflipboard/service/Account;Lflipboard/objs/ConfigService;)V
    .locals 1

    .prologue
    .line 382
    sget-object v0, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->a:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    invoke-direct {p0, v0}, Lflipboard/activities/ChooseAccountActivity$AccountItem;-><init>(Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;)V

    .line 383
    iput-object p1, p0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->c:Lflipboard/service/Account;

    .line 384
    iput-object p2, p0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->b:Lflipboard/objs/ConfigService;

    .line 385
    return-void
.end method

.method static a(Ljava/lang/String;)Lflipboard/activities/ChooseAccountActivity$AccountItem;
    .locals 2

    .prologue
    .line 400
    new-instance v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;

    sget-object v1, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->c:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    invoke-direct {v0, v1}, Lflipboard/activities/ChooseAccountActivity$AccountItem;-><init>(Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;)V

    .line 401
    iput-object p0, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->e:Ljava/lang/String;

    .line 402
    return-object v0
.end method


# virtual methods
.method final a()Z
    .locals 2

    .prologue
    .line 407
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->a:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    sget-object v1, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->c:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
