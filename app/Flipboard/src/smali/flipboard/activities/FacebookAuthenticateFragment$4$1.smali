.class Lflipboard/activities/FacebookAuthenticateFragment$4$1;
.super Ljava/lang/Object;
.source "FacebookAuthenticateFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/activities/FacebookAuthenticateFragment$4;


# direct methods
.method constructor <init>(Lflipboard/activities/FacebookAuthenticateFragment$4;)V
    .locals 0

    .prologue
    .line 435
    iput-object p1, p0, Lflipboard/activities/FacebookAuthenticateFragment$4$1;->a:Lflipboard/activities/FacebookAuthenticateFragment$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 439
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment$4$1;->a:Lflipboard/activities/FacebookAuthenticateFragment$4;

    iget-object v0, v0, Lflipboard/activities/FacebookAuthenticateFragment$4;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-virtual {v0}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 440
    if-eqz v0, :cond_0

    .line 443
    iget-object v1, p0, Lflipboard/activities/FacebookAuthenticateFragment$4$1;->a:Lflipboard/activities/FacebookAuthenticateFragment$4;

    iget-object v1, v1, Lflipboard/activities/FacebookAuthenticateFragment$4;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    iget-object v1, v1, Lflipboard/activities/FacebookAuthenticateFragment;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->n()V

    .line 444
    const/4 v1, 0x1

    sget-object v2, Lflipboard/activities/LoginActivity$SignInMethod;->c:Lflipboard/activities/LoginActivity$SignInMethod;

    iget-object v3, p0, Lflipboard/activities/FacebookAuthenticateFragment$4$1;->a:Lflipboard/activities/FacebookAuthenticateFragment$4;

    iget-object v3, v3, Lflipboard/activities/FacebookAuthenticateFragment$4;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    iget-object v3, v3, Lflipboard/activities/FacebookAuthenticateFragment;->h:Lflipboard/objs/UsageEventV2$EventCategory;

    iget-object v4, p0, Lflipboard/activities/FacebookAuthenticateFragment$4$1;->a:Lflipboard/activities/FacebookAuthenticateFragment$4;

    iget-object v4, v4, Lflipboard/activities/FacebookAuthenticateFragment$4;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    iget-object v4, v4, Lflipboard/activities/FacebookAuthenticateFragment;->d:Lflipboard/activities/LoginActivity$LoginInitFrom;

    invoke-static {v1, v2, v3, v4}, Lflipboard/activities/LoginActivity;->a(ILflipboard/activities/LoginActivity$SignInMethod;Lflipboard/objs/UsageEventV2$EventCategory;Lflipboard/activities/LoginActivity$LoginInitFrom;)V

    .line 446
    invoke-static {v0}, Lflipboard/activities/LaunchActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 447
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 448
    iget-object v1, p0, Lflipboard/activities/FacebookAuthenticateFragment$4$1;->a:Lflipboard/activities/FacebookAuthenticateFragment$4;

    iget-object v1, v1, Lflipboard/activities/FacebookAuthenticateFragment$4;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-virtual {v1, v0}, Lflipboard/activities/FacebookAuthenticateFragment;->startActivity(Landroid/content/Intent;)V

    .line 449
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment$4$1;->a:Lflipboard/activities/FacebookAuthenticateFragment$4;

    iget-object v0, v0, Lflipboard/activities/FacebookAuthenticateFragment$4;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-virtual {v0}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 450
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment$4$1;->a:Lflipboard/activities/FacebookAuthenticateFragment$4;

    iget-object v0, v0, Lflipboard/activities/FacebookAuthenticateFragment$4;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-virtual {v0}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 451
    const-string v0, "FlipSSOLoginWithToken Succeeded"

    const-string v1, "service"

    const-string v2, "flipboard"

    invoke-static {v0, v1, v2}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 453
    :cond_0
    return-void
.end method
