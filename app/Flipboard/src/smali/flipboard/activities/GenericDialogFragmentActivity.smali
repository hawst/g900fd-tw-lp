.class public Lflipboard/activities/GenericDialogFragmentActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "GenericDialogFragmentActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 21
    invoke-virtual {p0}, Lflipboard/activities/GenericDialogFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "fragment_type"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 22
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    const v0, 0x7f03011b

    invoke-virtual {p0, v0}, Lflipboard/activities/GenericDialogFragmentActivity;->setContentView(I)V

    .line 24
    const/4 v0, 0x0

    .line 25
    packed-switch v1, :pswitch_data_0

    .line 32
    :goto_0
    if-eqz v0, :cond_0

    .line 33
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 34
    const v2, 0x7f040016

    const v3, 0x7f040018

    const v4, 0x7f040015

    const v5, 0x7f040017

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/support/v4/app/FragmentTransaction;->a(IIII)Landroid/support/v4/app/FragmentTransaction;

    .line 35
    const v2, 0x7f0a004e

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 36
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 38
    :cond_0
    return-void

    .line 27
    :pswitch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/GenericDialogFragmentActivity;->W:Z

    .line 28
    new-instance v0, Lflipboard/activities/CreateMagazineFragment;

    invoke-direct {v0}, Lflipboard/activities/CreateMagazineFragment;-><init>()V

    new-instance v1, Lflipboard/activities/GenericDialogFragmentActivity$1;

    invoke-direct {v1, p0}, Lflipboard/activities/GenericDialogFragmentActivity$1;-><init>(Lflipboard/activities/GenericDialogFragmentActivity;)V

    iput-object v1, v0, Lflipboard/activities/CreateMagazineFragment;->i:Lflipboard/activities/CreateMagazineFragment$CreateMagazineFragmentActionListener;

    goto :goto_0

    .line 25
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
