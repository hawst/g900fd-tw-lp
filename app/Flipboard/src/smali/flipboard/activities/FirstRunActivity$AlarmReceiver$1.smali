.class Lflipboard/activities/FirstRunActivity$AlarmReceiver$1;
.super Ljava/lang/Object;
.source "FirstRunActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Lflipboard/activities/FirstRunActivity$AlarmReceiver;

.field private final d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lflipboard/activities/FirstRunActivity$AlarmReceiver;Landroid/content/Intent;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 455
    iput-object p1, p0, Lflipboard/activities/FirstRunActivity$AlarmReceiver$1;->c:Lflipboard/activities/FirstRunActivity$AlarmReceiver;

    iput-object p2, p0, Lflipboard/activities/FirstRunActivity$AlarmReceiver$1;->a:Landroid/content/Intent;

    iput-object p3, p0, Lflipboard/activities/FirstRunActivity$AlarmReceiver$1;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456
    const-string v0, "pref_times_first_launch_reminder_shown"

    iput-object v0, p0, Lflipboard/activities/FirstRunActivity$AlarmReceiver$1;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 460
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v0, v3, :cond_1

    .line 488
    :cond_0
    :goto_0
    return-void

    .line 464
    :cond_1
    iget-object v0, p0, Lflipboard/activities/FirstRunActivity$AlarmReceiver$1;->a:Landroid/content/Intent;

    const-string v3, "extra_alarm_action"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 465
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->v()Lflipboard/model/ConfigSetting;

    move-result-object v4

    .line 466
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget v0, v0, Lflipboard/service/FlipboardManager;->aB:I

    if-lez v0, :cond_2

    move v0, v1

    .line 467
    :goto_1
    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v5, v5, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v5}, Lflipboard/service/User;->c()Z

    move-result v5

    .line 468
    const-string v6, "alarm_action_reminder"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 469
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v6, "pref_times_first_launch_reminder_shown"

    invoke-interface {v3, v6, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 470
    iget-boolean v6, v4, Lflipboard/model/ConfigSetting;->FirstRunNotificationEnabled:Z

    if-eqz v6, :cond_3

    iget v4, v4, Lflipboard/model/ConfigSetting;->FirstRunNotificationMaxTimes:I

    if-ge v3, v4, :cond_3

    .line 471
    :goto_2
    if-eqz v1, :cond_4

    if-nez v5, :cond_4

    if-nez v0, :cond_4

    .line 472
    sget-object v0, Lflipboard/objs/UsageEventV2$EventAction;->x:Lflipboard/objs/UsageEventV2$EventAction;

    invoke-static {v0}, Lflipboard/activities/FirstRunActivity;->a(Lflipboard/objs/UsageEventV2$EventAction;)V

    .line 473
    new-instance v0, Lflipboard/notifications/BuildFlipboardNotification;

    invoke-direct {v0}, Lflipboard/notifications/BuildFlipboardNotification;-><init>()V

    iget-object v1, p0, Lflipboard/activities/FirstRunActivity$AlarmReceiver$1;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lflipboard/notifications/BuildFlipboardNotification;->b(Landroid/content/Context;)V

    .line 474
    add-int/lit8 v0, v3, 0x1

    .line 475
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "pref_times_first_launch_reminder_shown"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 466
    goto :goto_1

    :cond_3
    move v1, v2

    .line 470
    goto :goto_2

    .line 477
    :cond_4
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v1, 0x41f15

    iget-object v2, p0, Lflipboard/activities/FirstRunActivity$AlarmReceiver$1;->a:Landroid/content/Intent;

    const/high16 v3, 0x10000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 478
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const-string v2, "alarm"

    invoke-virtual {v0, v2}, Lflipboard/app/FlipboardApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 479
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 481
    :cond_5
    const-string v1, "alarm_action_survey"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 483
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-object v1, v1, Lflipboard/app/FlipboardApplication;->o:Ljava/util/Locale;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 484
    if-eqz v1, :cond_0

    iget-boolean v1, v4, Lflipboard/model/ConfigSetting;->FirstRunSurveyNotificationEnabled:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    if-nez v5, :cond_0

    .line 485
    new-instance v0, Lflipboard/notifications/ExpireNotification;

    new-instance v1, Lflipboard/notifications/SurveyNotification;

    invoke-direct {v1}, Lflipboard/notifications/SurveyNotification;-><init>()V

    const-wide/32 v2, 0x1b7740

    invoke-direct {v0, v1, v2, v3}, Lflipboard/notifications/ExpireNotification;-><init>(Lflipboard/notifications/FLNotification;J)V

    iget-object v1, p0, Lflipboard/activities/FirstRunActivity$AlarmReceiver$1;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lflipboard/notifications/ExpireNotification;->b(Landroid/content/Context;)V

    goto/16 :goto_0
.end method
