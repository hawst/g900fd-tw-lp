.class public Lflipboard/activities/NotificationSettingsActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "NotificationSettingsActivity.java"


# instance fields
.field n:Lflipboard/json/FLObject;

.field private o:Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;

.field private p:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 54
    return-void
.end method

.method static synthetic a(Lflipboard/activities/NotificationSettingsActivity;)Z
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/NotificationSettingsActivity;->p:Z

    return v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 28
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    const v0, 0x7f03011a

    invoke-virtual {p0, v0}, Lflipboard/activities/NotificationSettingsActivity;->setContentView(I)V

    .line 31
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->s()Lflipboard/json/FLObject;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/NotificationSettingsActivity;->n:Lflipboard/json/FLObject;

    .line 32
    iget-object v0, p0, Lflipboard/activities/NotificationSettingsActivity;->n:Lflipboard/json/FLObject;

    if-nez v0, :cond_0

    .line 33
    new-instance v0, Lflipboard/json/FLObject;

    invoke-direct {v0}, Lflipboard/json/FLObject;-><init>()V

    iput-object v0, p0, Lflipboard/activities/NotificationSettingsActivity;->n:Lflipboard/json/FLObject;

    .line 36
    :cond_0
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/activities/NotificationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 37
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 38
    const v0, 0x7f0a0310

    invoke-virtual {p0, v0}, Lflipboard/activities/NotificationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    .line 39
    const v1, 0x7f0d02cf

    invoke-virtual {p0, v1}, Lflipboard/activities/NotificationSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    const v0, 0x7f0a0144

    invoke-virtual {p0, v0}, Lflipboard/activities/NotificationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 41
    new-instance v1, Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;

    invoke-direct {v1, p0}, Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;-><init>(Lflipboard/activities/NotificationSettingsActivity;)V

    iput-object v1, p0, Lflipboard/activities/NotificationSettingsActivity;->o:Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;

    .line 42
    iget-object v1, p0, Lflipboard/activities/NotificationSettingsActivity;->o:Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 43
    iget-object v1, p0, Lflipboard/activities/NotificationSettingsActivity;->o:Lflipboard/activities/NotificationSettingsActivity$NotificationAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 44
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 126
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onDestroy()V

    .line 127
    iget-boolean v0, p0, Lflipboard/activities/NotificationSettingsActivity;->p:Z

    if-eqz v0, :cond_0

    .line 128
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/activities/NotificationSettingsActivity;->n:Lflipboard/json/FLObject;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->a(Lflipboard/json/FLObject;)V

    .line 130
    :cond_0
    return-void
.end method
