.class Lflipboard/activities/LoginActivity$12$1$1;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/activities/LoginActivity$12$1;


# direct methods
.method constructor <init>(Lflipboard/activities/LoginActivity$12$1;)V
    .locals 0

    .prologue
    .line 658
    iput-object p1, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 661
    iget-object v0, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12$1;->b:Lflipboard/activities/LoginActivity$12;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    if-eqz v0, :cond_0

    .line 663
    iget-object v0, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12$1;->b:Lflipboard/activities/LoginActivity$12;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Lflipboard/activities/LoginActivity;->setResult(I)V

    .line 664
    iget-object v0, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12$1;->a:Lflipboard/objs/UserInfo;

    iget-boolean v0, v0, Lflipboard/objs/UserInfo;->o:Z

    if-nez v0, :cond_1

    move v0, v1

    .line 665
    :goto_0
    const-string v2, "GoogleSSOLogin succeeded"

    const-string v3, "service"

    const-string v4, "flipboard"

    invoke-static {v2, v3, v4}, Lflipboard/io/FunnelUsageEvent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 667
    iget-object v2, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    iget-object v2, v2, Lflipboard/activities/LoginActivity$12$1;->b:Lflipboard/activities/LoginActivity$12;

    iget-object v2, v2, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    invoke-static {v2}, Lflipboard/activities/LoginActivity;->h(Lflipboard/activities/LoginActivity;)Lflipboard/activities/LoginActivity$LoginInitFrom;

    move-result-object v2

    sget-object v3, Lflipboard/activities/LoginActivity$LoginInitFrom;->b:Lflipboard/activities/LoginActivity$LoginInitFrom;

    if-ne v2, v3, :cond_2

    .line 669
    iget-object v0, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12$1;->b:Lflipboard/activities/LoginActivity$12;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    iget-object v0, v0, Lflipboard/activities/LoginActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->n()V

    .line 671
    sget-object v0, Lflipboard/activities/LoginActivity$SignInMethod;->b:Lflipboard/activities/LoginActivity$SignInMethod;

    iget-object v2, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    iget-object v2, v2, Lflipboard/activities/LoginActivity$12$1;->b:Lflipboard/activities/LoginActivity$12;

    iget-object v2, v2, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    invoke-static {v2}, Lflipboard/activities/LoginActivity;->g(Lflipboard/activities/LoginActivity;)Lflipboard/objs/UsageEventV2$EventCategory;

    move-result-object v2

    iget-object v3, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    iget-object v3, v3, Lflipboard/activities/LoginActivity$12$1;->b:Lflipboard/activities/LoginActivity$12;

    iget-object v3, v3, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    invoke-static {v3}, Lflipboard/activities/LoginActivity;->h(Lflipboard/activities/LoginActivity;)Lflipboard/activities/LoginActivity$LoginInitFrom;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lflipboard/activities/LoginActivity;->a(ILflipboard/activities/LoginActivity$SignInMethod;Lflipboard/objs/UsageEventV2$EventCategory;Lflipboard/activities/LoginActivity$LoginInitFrom;)V

    .line 672
    iget-object v0, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12$1;->b:Lflipboard/activities/LoginActivity$12;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    iput-boolean v1, v0, Lflipboard/activities/LoginActivity;->o:Z

    .line 673
    iget-object v0, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12$1;->b:Lflipboard/activities/LoginActivity$12;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    invoke-virtual {v0}, Lflipboard/activities/LoginActivity;->finish()V

    .line 691
    :cond_0
    :goto_1
    return-void

    .line 664
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 674
    :cond_2
    if-eqz v0, :cond_3

    .line 677
    iget-object v0, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12$1;->b:Lflipboard/activities/LoginActivity$12;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LoginActivity;->j(Lflipboard/activities/LoginActivity;)V

    goto :goto_1

    .line 681
    :cond_3
    iget-object v0, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12$1;->b:Lflipboard/activities/LoginActivity$12;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    iget-object v0, v0, Lflipboard/activities/LoginActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->n()V

    .line 682
    iget-object v0, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12$1;->b:Lflipboard/activities/LoginActivity$12;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LaunchActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 683
    const/high16 v2, 0x4000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 684
    iget-object v2, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    iget-object v2, v2, Lflipboard/activities/LoginActivity$12$1;->b:Lflipboard/activities/LoginActivity$12;

    iget-object v2, v2, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    invoke-virtual {v2, v0}, Lflipboard/activities/LoginActivity;->startActivity(Landroid/content/Intent;)V

    .line 686
    sget-object v0, Lflipboard/activities/LoginActivity$SignInMethod;->b:Lflipboard/activities/LoginActivity$SignInMethod;

    iget-object v2, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    iget-object v2, v2, Lflipboard/activities/LoginActivity$12$1;->b:Lflipboard/activities/LoginActivity$12;

    iget-object v2, v2, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    invoke-static {v2}, Lflipboard/activities/LoginActivity;->g(Lflipboard/activities/LoginActivity;)Lflipboard/objs/UsageEventV2$EventCategory;

    move-result-object v2

    iget-object v3, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    iget-object v3, v3, Lflipboard/activities/LoginActivity$12$1;->b:Lflipboard/activities/LoginActivity$12;

    iget-object v3, v3, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    invoke-static {v3}, Lflipboard/activities/LoginActivity;->h(Lflipboard/activities/LoginActivity;)Lflipboard/activities/LoginActivity$LoginInitFrom;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lflipboard/activities/LoginActivity;->a(ILflipboard/activities/LoginActivity$SignInMethod;Lflipboard/objs/UsageEventV2$EventCategory;Lflipboard/activities/LoginActivity$LoginInitFrom;)V

    .line 687
    iget-object v0, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12$1;->b:Lflipboard/activities/LoginActivity$12;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    iput-boolean v1, v0, Lflipboard/activities/LoginActivity;->o:Z

    .line 688
    iget-object v0, p0, Lflipboard/activities/LoginActivity$12$1$1;->a:Lflipboard/activities/LoginActivity$12$1;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12$1;->b:Lflipboard/activities/LoginActivity$12;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$12;->c:Lflipboard/activities/LoginActivity;

    invoke-virtual {v0}, Lflipboard/activities/LoginActivity;->finish()V

    goto :goto_1
.end method
