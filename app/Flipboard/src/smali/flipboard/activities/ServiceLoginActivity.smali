.class public Lflipboard/activities/ServiceLoginActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "ServiceLoginActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Lflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;


# static fields
.field public static n:Lflipboard/util/Log;


# instance fields
.field o:Ljava/lang/String;

.field p:Z

.field q:Z

.field private r:Lflipboard/service/Section;

.field private s:Landroid/widget/EditText;

.field private t:Landroid/widget/EditText;

.field private u:Landroid/widget/Button;

.field private v:Lflipboard/gui/FLWebViewNoScale;

.field private w:J

.field private x:Lflipboard/objs/ConfigService;

.field private y:Ljava/lang/String;

.field private z:Lflipboard/util/GooglePlusSignInClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-string v0, "servicelogin"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/activities/ServiceLoginActivity;->n:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lflipboard/activities/ServiceLoginActivity;Lflipboard/service/Section;)Lflipboard/service/Section;
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lflipboard/activities/ServiceLoginActivity;->r:Lflipboard/service/Section;

    return-object p1
.end method

.method static synthetic a(Lflipboard/activities/ServiceLoginActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->y:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lflipboard/activities/ServiceLoginActivity;)Lflipboard/util/GooglePlusSignInClient;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->z:Lflipboard/util/GooglePlusSignInClient;

    return-object v0
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 582
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->z:Lflipboard/util/GooglePlusSignInClient;

    if-nez v0, :cond_0

    .line 583
    new-instance v0, Lflipboard/util/GooglePlusSignInClient;

    invoke-direct {v0, p0, p1, p0}, Lflipboard/util/GooglePlusSignInClient;-><init>(Landroid/content/Context;ILflipboard/util/GooglePlusSignInClient$GooglePlusSignInListener;)V

    iput-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->z:Lflipboard/util/GooglePlusSignInClient;

    .line 585
    :cond_0
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->z:Lflipboard/util/GooglePlusSignInClient;

    invoke-virtual {v0}, Lflipboard/util/GooglePlusSignInClient;->c()V

    .line 586
    return-void
.end method

.method static synthetic c(Lflipboard/activities/ServiceLoginActivity;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->r:Lflipboard/service/Section;

    return-object v0
.end method

.method private r()V
    .locals 7

    .prologue
    const/4 v6, -0x2

    const/4 v5, 0x1

    .line 392
    invoke-virtual {p0}, Lflipboard/activities/ServiceLoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 398
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->x:Lflipboard/objs/ConfigService;

    iget v0, v0, Lflipboard/objs/ConfigService;->bz:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->x:Lflipboard/objs/ConfigService;

    iget v0, v0, Lflipboard/objs/ConfigService;->bA:I

    if-lez v0, :cond_1

    .line 399
    :cond_0
    invoke-virtual {p0}, Lflipboard/activities/ServiceLoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_2

    .line 400
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->x:Lflipboard/objs/ConfigService;

    iget v1, v0, Lflipboard/objs/ConfigService;->bz:I

    .line 401
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->x:Lflipboard/objs/ConfigService;

    iget v0, v0, Lflipboard/objs/ConfigService;->bA:I

    move v2, v1

    move v1, v0

    .line 407
    :goto_0
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 408
    if-eqz v0, :cond_1

    .line 409
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 410
    if-eqz v0, :cond_1

    .line 411
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 412
    if-nez v2, :cond_3

    .line 413
    iput v6, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 417
    :goto_1
    if-nez v1, :cond_4

    .line 418
    iput v6, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 425
    :cond_1
    :goto_2
    return-void

    .line 403
    :cond_2
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->x:Lflipboard/objs/ConfigService;

    iget v1, v0, Lflipboard/objs/ConfigService;->bB:I

    .line 404
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->x:Lflipboard/objs/ConfigService;

    iget v0, v0, Lflipboard/objs/ConfigService;->bC:I

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 415
    :cond_3
    int-to-float v2, v2

    invoke-static {v5, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_1

    .line 420
    :cond_4
    int-to-float v1, v1

    invoke-static {v5, v1, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_2
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 590
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 593
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 594
    const-string v0, "googleplus"

    .line 602
    :goto_0
    const-string v2, "googleplussso"

    invoke-static {p0, v2, v0}, Lflipboard/service/DialogHandler;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v3, Lflipboard/activities/ServiceLoginActivity$5;

    invoke-direct {v3, p0, p2, v1}, Lflipboard/activities/ServiceLoginActivity$5;-><init>(Lflipboard/activities/ServiceLoginActivity;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v2, v0, p2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$TypedResultObserver;)Lflipboard/service/Flap$LoginOrCreateRequestWithServiceWithTokenRequest;

    .line 650
    return-void

    .line 595
    :cond_0
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 596
    const-string v0, "youtube"

    goto :goto_0

    .line 598
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Google plus sign in succeeds with a service that we don\'t know. authScopeType is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final a(Lflipboard/objs/UserInfo;)V
    .locals 4

    .prologue
    .line 487
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceLoginActivity;->setResult(I)V

    .line 489
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 492
    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 493
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lflipboard/service/Section;->d(Z)Z

    goto :goto_0

    .line 497
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    const-string v2, "usageSocialLogin"

    new-instance v3, Lflipboard/activities/ServiceLoginActivity$4;

    invoke-direct {v3, p0}, Lflipboard/activities/ServiceLoginActivity$4;-><init>(Lflipboard/activities/ServiceLoginActivity;)V

    invoke-virtual {v0, v1, p1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/objs/UserInfo;Ljava/lang/String;Lflipboard/util/Observer;)Lflipboard/service/Section;

    .line 538
    return-void
.end method

.method protected final a(Ljava/lang/String;Lflipboard/service/User;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 464
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 465
    invoke-virtual {v2}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "success.html"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v3

    const-string v4, "NYT-S="

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 469
    iget-object v3, p0, Lflipboard/activities/ServiceLoginActivity;->M:Lflipboard/service/FlipboardManager;

    const-string v4, "/v1/users/nytimesCallback"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "url"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-virtual {v2}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, p2, v5}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 474
    iget-object v3, p0, Lflipboard/activities/ServiceLoginActivity;->v:Lflipboard/gui/FLWebViewNoScale;

    invoke-virtual {v3, v2}, Lflipboard/gui/FLWebViewNoScale;->loadUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 481
    :goto_0
    return v0

    .line 477
    :catch_0
    move-exception v0

    .line 478
    sget-object v2, Lflipboard/activities/ServiceLoginActivity;->n:Lflipboard/util/Log;

    invoke-virtual {v2, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    :cond_0
    move v0, v1

    .line 481
    goto :goto_0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 173
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->s:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 174
    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity;->t:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 175
    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity;->u:Landroid/widget/Button;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 176
    return-void

    .line 175
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 165
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 665
    const-string v0, "authenticating"

    invoke-static {p0, v0}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 667
    const v0, 0x7f0d011e

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 668
    if-eqz p0, :cond_0

    iget-boolean v1, p0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_0

    .line 669
    const/4 v1, 0x1

    invoke-static {p0, v0, p1, v1}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 671
    :cond_0
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 577
    const-string v0, "service_login"

    return-object v0
.end method

.method public final j()V
    .locals 0

    .prologue
    .line 676
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const v4, 0x7f04001b

    const v3, 0x7f040011

    const/16 v2, 0x2328

    const/16 v1, 0x1e39

    const/4 v0, -0x1

    .line 437
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FlipboardActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 439
    if-ne p1, v1, :cond_1

    if-ne p2, v0, :cond_1

    .line 440
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->z:Lflipboard/util/GooglePlusSignInClient;

    if-eqz v0, :cond_0

    .line 441
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "ServiceLoginActivity:onActivityResult:REQUEST_GOOGLE_SSO:RESULT_OK"

    new-instance v2, Lflipboard/activities/ServiceLoginActivity$3;

    invoke-direct {v2, p0}, Lflipboard/activities/ServiceLoginActivity$3;-><init>(Lflipboard/activities/ServiceLoginActivity;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 458
    :cond_0
    :goto_0
    return-void

    .line 448
    :cond_1
    if-ne p1, v2, :cond_2

    if-ne p2, v0, :cond_2

    .line 449
    const-string v0, "authenticating"

    invoke-static {p0, v0}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->z:Lflipboard/util/GooglePlusSignInClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->z:Lflipboard/util/GooglePlusSignInClient;

    invoke-virtual {v0}, Lflipboard/util/GooglePlusSignInClient;->d()V

    goto :goto_0

    .line 450
    :cond_2
    if-ne p1, v2, :cond_3

    if-nez p2, :cond_3

    .line 451
    invoke-virtual {p0, v3, v4}, Lflipboard/activities/ServiceLoginActivity;->overridePendingTransition(II)V

    .line 452
    invoke-virtual {p0}, Lflipboard/activities/ServiceLoginActivity;->finish()V

    goto :goto_0

    .line 453
    :cond_3
    if-ne p1, v1, :cond_0

    if-nez p2, :cond_0

    .line 454
    invoke-virtual {p0, v3, v4}, Lflipboard/activities/ServiceLoginActivity;->overridePendingTransition(II)V

    .line 455
    invoke-virtual {p0}, Lflipboard/activities/ServiceLoginActivity;->finish()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 6

    .prologue
    .line 542
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->v:Lflipboard/gui/FLWebViewNoScale;

    if-eqz v0, :cond_0

    .line 543
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 544
    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->i()Z

    move-result v2

    if-nez v2, :cond_0

    iget-wide v2, p0, Lflipboard/activities/ServiceLoginActivity;->w:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x190

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 549
    iput-wide v0, p0, Lflipboard/activities/ServiceLoginActivity;->w:J

    .line 550
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->v:Lflipboard/gui/FLWebViewNoScale;

    invoke-virtual {v0}, Lflipboard/gui/FLWebViewNoScale;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->v:Lflipboard/gui/FLWebViewNoScale;

    invoke-virtual {v0}, Lflipboard/gui/FLWebViewNoScale;->goBack()V

    .line 557
    :goto_0
    return-void

    .line 556
    :cond_0
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 430
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 431
    invoke-direct {p0}, Lflipboard/activities/ServiceLoginActivity;->r()V

    .line 432
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v6, -0x1

    const/4 v4, -0x2

    const/4 v5, 0x2

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 85
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    invoke-virtual {p0}, Lflipboard/activities/ServiceLoginActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    invoke-virtual {p0}, Lflipboard/activities/ServiceLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 91
    const-string v1, "service"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    .line 92
    const-string v1, "subscribe"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lflipboard/activities/ServiceLoginActivity;->p:Z

    .line 93
    const-string v1, "viewSectionAfterSuccess"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lflipboard/activities/ServiceLoginActivity;->q:Z

    .line 94
    const-string v1, "extra_content_discovery_from_source"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->y:Ljava/lang/String;

    .line 96
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 97
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "missing service for LoginActvity"

    new-array v2, v9, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    invoke-virtual {p0}, Lflipboard/activities/ServiceLoginActivity;->finish()V

    goto :goto_0

    .line 102
    :cond_2
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->x:Lflipboard/objs/ConfigService;

    .line 103
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->x:Lflipboard/objs/ConfigService;

    if-nez v0, :cond_3

    .line 104
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "No service config in LoginActvity for service=%s"

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    aput-object v3, v2, v9

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    invoke-virtual {p0}, Lflipboard/activities/ServiceLoginActivity;->finish()V

    goto :goto_0

    .line 110
    :cond_3
    const-string v0, "facebook"

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    const-string v2, "facebook"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "com.facebook.katana"

    invoke-static {v1}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v0, Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-direct {v0}, Lflipboard/activities/FacebookAuthenticateFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "fragmentAction"

    sget-object v3, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->c:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v2, "openNewSessionOnCreate"

    invoke-virtual {v1, v2, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "startSectionAfterSuccess"

    iget-boolean v3, p0, Lflipboard/activities/ServiceLoginActivity;->q:Z

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    :cond_4
    if-eqz v0, :cond_5

    .line 112
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x1020002

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 113
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto/16 :goto_0

    .line 114
    :cond_5
    const-string v0, "googleplus"

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "youtube"

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_6
    invoke-virtual {p0}, Lflipboard/activities/ServiceLoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->a(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_8

    .line 116
    const-string v0, "googleplus"

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 117
    invoke-direct {p0, v5}, Lflipboard/activities/ServiceLoginActivity;->b(I)V

    goto/16 :goto_0

    .line 118
    :cond_7
    const-string v0, "youtube"

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lflipboard/activities/ServiceLoginActivity;->b(I)V

    goto/16 :goto_0

    .line 125
    :cond_8
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->x:Lflipboard/objs/ConfigService;

    iget-object v0, v0, Lflipboard/objs/ConfigService;->c:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->x:Lflipboard/objs/ConfigService;

    iget-object v0, v0, Lflipboard/objs/ConfigService;->c:Ljava/lang/String;

    const-string v1, "native"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v8

    .line 126
    :goto_1
    if-eqz v0, :cond_a

    .line 127
    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity;->x:Lflipboard/objs/ConfigService;

    const v0, 0x7f030112

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceLoginActivity;->setContentView(I)V

    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a02c3

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->s:Landroid/widget/EditText;

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->s:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const v0, 0x7f0a02fa

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->t:Landroid/widget/EditText;

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->t:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {p0}, Lflipboard/activities/ServiceLoginActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    const v0, 0x7f0a00f2

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->u:Landroid/widget/Button;

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->u:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_9
    move v0, v9

    .line 125
    goto :goto_1

    .line 129
    :cond_a
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v0, :cond_c

    const v0, 0x7f030113

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceLoginActivity;->setContentView(I)V

    invoke-virtual {p0}, Lflipboard/activities/ServiceLoginActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v6, v6}, Landroid/view/Window;->setLayout(II)V

    :goto_2
    iput-boolean v9, p0, Lflipboard/activities/ServiceLoginActivity;->W:Z

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v6, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-boolean v0, p0, Lflipboard/activities/ServiceLoginActivity;->p:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    const-string v2, "/v1/users/services"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "loginService"

    aput-object v4, v3, v9

    aput-object v1, v3, v8

    const-string v1, "subscribe"

    aput-object v1, v3, v5

    const/4 v1, 0x3

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v6, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    :goto_3
    sget-object v0, Lflipboard/activities/ServiceLoginActivity;->n:Lflipboard/util/Log;

    new-array v0, v5, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    aput-object v1, v0, v9

    aput-object v7, v0, v8

    const v0, 0x7f0a02fb

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLWebViewNoScale;

    iput-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->v:Lflipboard/gui/FLWebViewNoScale;

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->v:Lflipboard/gui/FLWebViewNoScale;

    invoke-virtual {v0}, Lflipboard/gui/FLWebViewNoScale;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    invoke-virtual {v0, v9}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    invoke-virtual {v0, v9}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v1

    iget-boolean v1, v1, Lflipboard/model/ConfigSetting;->ModifyUserAgentForTabletServiceLogin:Z

    if-eqz v1, :cond_b

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    const-string v2, "(?<!Mobile )Safari"

    const-string v3, "Mobile Safari"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    :cond_b
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    invoke-virtual {v0, v8}, Landroid/webkit/CookieManager;->setAcceptCookie(Z)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v3

    iget-object v10, p0, Lflipboard/activities/ServiceLoginActivity;->v:Lflipboard/gui/FLWebViewNoScale;

    new-instance v0, Lflipboard/activities/ServiceLoginActivity$2;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lflipboard/activities/ServiceLoginActivity$2;-><init>(Lflipboard/activities/ServiceLoginActivity;Lflipboard/activities/FlipboardActivity;Lflipboard/service/Account;JLflipboard/service/User;)V

    invoke-virtual {v10, v0}, Lflipboard/gui/FLWebViewNoScale;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    const-string v1, "web"

    const-string v2, "Login Cancelled"

    invoke-static {p0, v0, v1, v2}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lflipboard/activities/ServiceLoginActivity;->n:Lflipboard/util/Log;

    new-array v0, v8, [Ljava/lang/Object;

    aput-object v7, v0, v9

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->v:Lflipboard/gui/FLWebViewNoScale;

    invoke-virtual {v0, v7}, Lflipboard/gui/FLWebViewNoScale;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    const v0, 0x7f030114

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceLoginActivity;->setContentView(I)V

    invoke-virtual {p0}, Lflipboard/activities/ServiceLoginActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/view/Window;->setLayout(II)V

    invoke-direct {p0}, Lflipboard/activities/ServiceLoginActivity;->r()V

    goto/16 :goto_2

    :cond_d
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    const-string v2, "/v1/users/services"

    new-array v3, v5, [Ljava/lang/Object;

    const-string v4, "loginService"

    aput-object v4, v3, v9

    aput-object v1, v3, v8

    invoke-virtual {v0, v2, v6, v3}, Lflipboard/service/Flap;->a(Ljava/lang/String;Lflipboard/service/User;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    goto/16 :goto_3
.end method

.method public onHelpClicked(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 238
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lflipboard/util/HelpshiftHelper;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 239
    return-void
.end method

.method public onNativeSubmitClicked(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 180
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 182
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceLoginActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 183
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v4, 0x2

    invoke-virtual {v0, v1, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 185
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->s:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 186
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity;->t:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 189
    const-string v0, "native"

    iget-object v5, p0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    invoke-static {p0, v0, v5}, Lflipboard/service/DialogHandler;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 194
    iget-object v5, p0, Lflipboard/activities/ServiceLoginActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v5}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v5

    iget-object v6, p0, Lflipboard/activities/ServiceLoginActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v6, v6, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v7, Lflipboard/activities/ServiceLoginActivity$1;

    invoke-direct {v7, p0, p0, v2, v3}, Lflipboard/activities/ServiceLoginActivity$1;-><init>(Lflipboard/activities/ServiceLoginActivity;Lflipboard/activities/FlipboardActivity;J)V

    new-instance v2, Lflipboard/service/Flap$ServiceLoginXauthRequest;

    invoke-direct {v2, v5, v6, v0}, Lflipboard/service/Flap$ServiceLoginXauthRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;Lflipboard/objs/ConfigService;)V

    invoke-virtual {v2, v1, v4, v7}, Lflipboard/service/Flap$ServiceLoginXauthRequest;->login(Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Flap$AccountRequestObserver;)Lflipboard/service/Flap$ServiceLoginXauthRequest;

    .line 234
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 135
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onPause()V

    .line 137
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lflipboard/activities/ServiceLoginActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 138
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    .line 139
    if-nez v0, :cond_0

    .line 140
    invoke-virtual {p0}, Lflipboard/activities/ServiceLoginActivity;->finish()V

    .line 142
    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 167
    return-void
.end method
