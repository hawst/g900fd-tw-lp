.class Lflipboard/activities/DebugMenuActivity$4;
.super Ljava/lang/Object;
.source "DebugMenuActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lflipboard/activities/DebugMenuActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/DebugMenuActivity;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lflipboard/activities/DebugMenuActivity$4;->a:Lflipboard/activities/DebugMenuActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 164
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$4;->a:Lflipboard/activities/DebugMenuActivity;

    const-class v2, Lflipboard/activities/DebugMenuActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 166
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 168
    const-string v2, "sectionId"

    iget-object v0, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 170
    const-string v0, "activityType"

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 171
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$4;->a:Lflipboard/activities/DebugMenuActivity;

    invoke-virtual {v0, v1}, Lflipboard/activities/DebugMenuActivity;->startActivity(Landroid/content/Intent;)V

    .line 172
    return-void
.end method
