.class public Lflipboard/activities/SettingsFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "SettingsFragment.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field public static final a:Z

.field public static final b:[Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static f:Lflipboard/objs/SectionPageTemplate;


# instance fields
.field final g:[I

.field final h:[I

.field i:I

.field m:I

.field n:Z

.field o:Z

.field p:Z

.field private q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/activities/SettingsFragment$Category;",
            ">;"
        }
    .end annotation
.end field

.field private r:Landroid/content/SharedPreferences;

.field private s:[Ljava/lang/String;

.field private t:[Ljava/lang/String;

.field private u:[Ljava/lang/String;

.field private v:[Ljava/lang/String;

.field private w:Lflipboard/activities/SettingsFragment$SettingsAdapter;

.field private x:I

.field private y:Lflipboard/service/FlipboardManager;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 83
    sget-boolean v0, Lflipboard/app/FlipboardApplication;->h:Z

    sput-boolean v0, Lflipboard/activities/SettingsFragment;->a:Z

    .line 112
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "32MB"

    aput-object v1, v0, v4

    const-string v1, "64MB"

    aput-object v1, v0, v5

    const-string v1, "128MB"

    aput-object v1, v0, v6

    const-string v1, "256MB"

    aput-object v1, v0, v3

    const-string v1, "512MB"

    aput-object v1, v0, v7

    sput-object v0, Lflipboard/activities/SettingsFragment;->b:[Ljava/lang/String;

    .line 148
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_0

    .line 149
    const-string v0, "https://fbchina.flipboard.com"

    sput-object v0, Lflipboard/activities/SettingsFragment;->c:Ljava/lang/String;

    .line 277
    :goto_0
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "https://fbprod.flipboard.com"

    aput-object v2, v1, v4

    const-string v2, "https://laika.flipboard.com"

    aput-object v2, v1, v5

    const-string v2, "https://bogus.flipboard.com"

    aput-object v2, v1, v6

    const-string v2, "https://crystal.flipboard.com"

    aput-object v2, v1, v3

    const-string v2, "https://fbchina.flipboard.com"

    aput-object v2, v1, v7

    const/4 v2, 0x5

    const-string v3, "https://staging.flipboard.com"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "https://beta.flipboard.com"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "https://prodfosho.flipboard.com"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lflipboard/activities/SettingsFragment;->d:Ljava/util/HashSet;

    .line 287
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "https://ad-beta.flipboard.com"

    aput-object v2, v1, v4

    const-string v2, "https://ad.flipboard.com"

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lflipboard/activities/SettingsFragment;->e:Ljava/util/HashSet;

    return-void

    .line 150
    :cond_0
    const-string v0, "samsung"

    const-string v1, "internal"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 151
    const-string v0, "https://fbprod.flipboard.com"

    sput-object v0, Lflipboard/activities/SettingsFragment;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 297
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lflipboard/activities/SettingsFragment;->g:[I

    .line 299
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lflipboard/activities/SettingsFragment;->h:[I

    .line 1503
    return-void

    .line 297
    :array_0
    .array-data 4
        0x64
        0x1f4
        0x5dc
    .end array-data

    .line 299
    :array_1
    .array-data 4
        0x50
        0x64
        0x7d
        0x96
    .end array-data
.end method

.method private static a([II)I
    .locals 3

    .prologue
    .line 1481
    const/4 v0, -0x1

    .line 1482
    const/4 v1, 0x0

    .line 1483
    :goto_0
    if-gez v0, :cond_1

    array-length v2, p0

    if-ge v1, v2, :cond_1

    .line 1484
    aget v2, p0, v1

    if-ne v2, p1, :cond_0

    move v0, v1

    .line 1487
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1489
    :cond_1
    return v0
.end method

.method static synthetic a(Lflipboard/activities/SettingsFragment;Lflipboard/activities/SettingsFragment$SettingsAdapter;)Lflipboard/activities/SettingsFragment$SettingsAdapter;
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lflipboard/activities/SettingsFragment;->w:Lflipboard/activities/SettingsFragment$SettingsAdapter;

    return-object p1
.end method

.method public static a()Lflipboard/activities/SettingsFragment;
    .locals 4

    .prologue
    .line 314
    new-instance v0, Lflipboard/activities/SettingsFragment;

    invoke-direct {v0}, Lflipboard/activities/SettingsFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "hide_action_bar"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lflipboard/activities/SettingsFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/SettingsFragment;)Lflipboard/service/FlipboardManager;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->y:Lflipboard/service/FlipboardManager;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Account;)V
    .locals 3

    .prologue
    .line 1565
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/ServiceSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1566
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1567
    const-string v1, "account_image"

    iget-object v2, p3, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v2}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1568
    const-string v1, "account_username"

    iget-object v2, p3, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1569
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1570
    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1572
    return-void
.end method

.method private a(Lflipboard/activities/SettingsFragment$Category;)V
    .locals 2

    .prologue
    .line 1300
    new-instance v0, Lflipboard/activities/SettingsFragment$41;

    const v1, 0x7f0d02a3

    invoke-virtual {p0, v1}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lflipboard/activities/SettingsFragment$41;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 1331
    iget-object v1, p1, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1332
    return-void
.end method

.method static synthetic b(Lflipboard/activities/SettingsFragment;)I
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Lflipboard/activities/SettingsFragment;->f()I

    move-result v0

    return v0
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1611
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_0

    const-string v0, "https://ad-beta.flipboard.com"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "https://ad.flipboard.com"

    goto :goto_0
.end method

.method private c()V
    .locals 10

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 364
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/activities/SettingsFragment;->q:Ljava/util/ArrayList;

    .line 366
    new-array v0, v2, [Ljava/lang/String;

    const v1, 0x7f0d02e2

    invoke-virtual {p0, v1}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v8

    const v1, 0x7f0d02e3

    invoke-virtual {p0, v1}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v9

    const v1, 0x7f0d02e4

    invoke-virtual {p0, v1}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    iput-object v0, p0, Lflipboard/activities/SettingsFragment;->s:[Ljava/lang/String;

    .line 368
    new-array v0, v3, [Ljava/lang/String;

    const v1, 0x7f0d02da

    invoke-virtual {p0, v1}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v8

    const v1, 0x7f0d02d9

    invoke-virtual {p0, v1}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v9

    const v1, 0x7f0d02d8

    invoke-virtual {p0, v1}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    const v1, 0x7f0d02dc

    invoke-virtual {p0, v1}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    iput-object v0, p0, Lflipboard/activities/SettingsFragment;->t:[Ljava/lang/String;

    .line 370
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "off"

    aput-object v1, v0, v8

    const-string v1, "short text"

    aput-object v1, v0, v9

    const-string v1, "long text"

    aput-object v1, v0, v6

    const-string v1, "long german word"

    aput-object v1, v0, v2

    const-string v1, "hyphenated words"

    aput-object v1, v0, v3

    const/4 v1, 0x5

    const-string v2, "newlines"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "weird"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "spacing"

    aput-object v2, v0, v1

    iput-object v0, p0, Lflipboard/activities/SettingsFragment;->u:[Ljava/lang/String;

    .line 371
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "Google Analytics"

    aput-object v1, v0, v8

    const-string v1, "Off - no logging"

    aput-object v1, v0, v9

    iput-object v0, p0, Lflipboard/activities/SettingsFragment;->v:[Ljava/lang/String;

    .line 373
    new-instance v1, Lflipboard/activities/SettingsFragment$Category;

    const v0, 0x7f0d02c0

    invoke-virtual {p0, v0}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lflipboard/activities/SettingsFragment$Category;-><init>(Ljava/lang/String;)V

    .line 374
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 377
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->y:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 379
    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 381
    new-instance v2, Lflipboard/activities/SettingsFragment$1;

    const v3, 0x7f0d00d1

    invoke-virtual {p0, v3}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3, v0}, Lflipboard/activities/SettingsFragment$1;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;Landroid/content/Context;)V

    .line 386
    iget-object v0, v1, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 389
    :cond_0
    new-instance v0, Lflipboard/activities/SettingsFragment$2;

    const v2, 0x7f0d02a0

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lflipboard/activities/SettingsFragment$2;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 411
    iget-object v2, v1, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 413
    new-instance v0, Lflipboard/activities/SettingsFragment$3;

    const v2, 0x7f0d02c3

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lflipboard/activities/SettingsFragment$3;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 420
    iget-object v2, v1, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 423
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->y:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 424
    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 426
    new-instance v2, Lflipboard/activities/SettingsFragment$4;

    const v3, 0x7f0d02f4

    invoke-virtual {p0, v3}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3, v0}, Lflipboard/activities/SettingsFragment$4;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;Lflipboard/activities/FlipboardActivity;)V

    .line 447
    iget-object v0, v1, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 465
    :goto_0
    new-instance v0, Lflipboard/activities/SettingsFragment$Category;

    const v1, 0x7f0d02c2

    invoke-virtual {p0, v1}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lflipboard/activities/SettingsFragment$Category;-><init>(Ljava/lang/String;)V

    .line 466
    iget-object v1, p0, Lflipboard/activities/SettingsFragment;->q:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 467
    new-instance v1, Lflipboard/activities/SettingsFragment$7;

    const v2, 0x7f0d035a

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$7;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 474
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 475
    new-instance v1, Lflipboard/activities/SettingsFragment$8;

    const v2, 0x7f0d02d1

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$8;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 502
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 504
    sget-boolean v1, Lflipboard/service/FlipboardManager;->o:Z

    if-nez v1, :cond_1

    .line 506
    new-instance v1, Lflipboard/activities/SettingsFragment$9;

    const v2, 0x7f0d02b1

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$9;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 522
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 525
    :cond_1
    iget-object v1, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    new-instance v2, Lflipboard/activities/SettingsFragment$10;

    const v3, 0x7f0d02db

    invoke-virtual {p0, v3}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lflipboard/activities/SettingsFragment$10;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 543
    iget-object v1, p0, Lflipboard/activities/SettingsFragment;->y:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1}, Lflipboard/service/User;->s()Lflipboard/json/FLObject;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 544
    new-instance v1, Lflipboard/activities/SettingsFragment$11;

    const v2, 0x7f0d02cf

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$11;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 553
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 556
    :cond_2
    new-instance v1, Lflipboard/activities/SettingsFragment$12;

    const v2, 0x7f0d02cd

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$12;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 586
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 588
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const v2, 0x7f0d02d5

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "show_status_updates"

    invoke-direct {v1, v2, v3, v9}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 589
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 591
    sget-boolean v1, Lflipboard/app/FlipboardApplication;->h:Z

    if-eqz v1, :cond_3

    .line 592
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const v2, 0x7f0d02a2

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "animate_gifs"

    sget-boolean v4, Lflipboard/activities/SettingsFragment;->a:Z

    invoke-direct {v1, v2, v3, v4}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 593
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 596
    :cond_3
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const v2, 0x7f0d02d4

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "show_hints"

    invoke-direct {v1, v2, v3, v9}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 597
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 600
    iget-object v1, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v2, "use_legacy_cover_stories"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 601
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const v2, 0x7f0d02de

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "use_legacy_cover_stories"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 602
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 605
    :cond_4
    new-instance v1, Lflipboard/activities/SettingsFragment$13;

    const v2, 0x7f0d02a7

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$13;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 625
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 627
    new-instance v1, Lflipboard/activities/SettingsFragment$14;

    const v2, 0x7f0d02e6

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$14;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 646
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 648
    new-instance v1, Lflipboard/activities/SettingsFragment$15;

    const v2, 0x7f0d02c5

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$15;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 674
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 676
    new-instance v1, Lflipboard/activities/SettingsFragment$16;

    const v2, 0x7f0d02d7

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$16;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 684
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 687
    invoke-direct {p0}, Lflipboard/activities/SettingsFragment;->f()I

    move-result v1

    iput v1, p0, Lflipboard/activities/SettingsFragment;->i:I

    .line 689
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const v2, 0x7f0d02be

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "fullscreen"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 690
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 694
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v2, "showAppMode"

    invoke-interface {v1, v2, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 695
    invoke-direct {p0, v0}, Lflipboard/activities/SettingsFragment;->a(Lflipboard/activities/SettingsFragment$Category;)V

    .line 698
    :cond_5
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->y:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->x()Ljava/util/List;

    move-result-object v1

    .line 701
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_a

    .line 702
    const/4 v0, 0x0

    .line 703
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 704
    const-string v2, "flipboard.settings.%s%s"

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    iget-object v5, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v5, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-static {v2, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 706
    :try_start_0
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 707
    if-nez v1, :cond_6

    .line 708
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v2

    .line 710
    :cond_6
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 714
    :catch_0
    move-exception v0

    move-object v0, v1

    move-object v1, v0

    goto :goto_1

    .line 449
    :cond_7
    new-instance v0, Lflipboard/activities/SettingsFragment$5;

    const v2, 0x7f0d01de

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0d013d

    invoke-virtual {p0, v3}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p0, v2, v3}, Lflipboard/activities/SettingsFragment$5;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    iget-object v2, v1, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 456
    new-instance v0, Lflipboard/activities/SettingsFragment$6;

    const v2, 0x7f0d01f1

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lflipboard/activities/SettingsFragment$6;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 462
    iget-object v1, v1, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 715
    :cond_8
    if-eqz v1, :cond_a

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_a

    .line 716
    new-instance v6, Lflipboard/activities/SettingsFragment$Category;

    const v0, 0x7f0d02ce

    invoke-virtual {p0, v0}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Lflipboard/activities/SettingsFragment$Category;-><init>(Ljava/lang/String;)V

    .line 717
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 718
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_9
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lflipboard/objs/ConfigService;

    .line 719
    iget-object v0, v4, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 720
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->y:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v4, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v5

    .line 722
    new-instance v0, Lflipboard/activities/SettingsFragment$17;

    invoke-virtual {v4}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v1, v5, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v1, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lflipboard/activities/SettingsFragment$17;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;Ljava/lang/String;Lflipboard/objs/ConfigService;Lflipboard/service/Account;)V

    .line 730
    invoke-virtual {v4}, Lflipboard/objs/ConfigService;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/activities/SettingsFragment$RowItem;->g:Ljava/lang/String;

    .line 731
    iget-object v1, v6, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 737
    :cond_a
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->y:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_e

    .line 738
    new-instance v0, Lflipboard/activities/SettingsFragment$Category;

    const v1, 0x7f0d02bf

    invoke-virtual {p0, v1}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lflipboard/activities/SettingsFragment$Category;-><init>(Ljava/lang/String;)V

    .line 739
    iget-object v1, p0, Lflipboard/activities/SettingsFragment;->q:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 741
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const v2, 0x7f0d02b8

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "enable_scrolling"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 742
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 744
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const v2, 0x7f0d02b9

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "enable_volume_up_to_settings"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 745
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 747
    new-instance v1, Lflipboard/activities/SettingsFragment$18;

    const-string v2, "Show request log"

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$18;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 753
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 755
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const v2, 0x7f0d02d6

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "snap_scrolling"

    invoke-direct {v1, v2, v3, v9}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 756
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 758
    sget-boolean v1, Lflipboard/app/FlipboardApplication;->h:Z

    if-nez v1, :cond_b

    .line 759
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const v2, 0x7f0d02a2

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "animate_gifs"

    sget-boolean v4, Lflipboard/activities/SettingsFragment;->a:Z

    invoke-direct {v1, v2, v3, v4}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 760
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 764
    :cond_b
    new-instance v1, Lflipboard/activities/SettingsFragment$19;

    const-string v2, "Tab UI Home Screen"

    const-string v3, "settings_tab_homescreen"

    invoke-direct {v1, p0, v2, v3}, Lflipboard/activities/SettingsFragment$19;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 772
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v2, "Display scrolling item\'s position in feed"

    const-string v3, "enable_scrolling_item_position"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 773
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 775
    new-instance v1, Lflipboard/activities/SettingsFragment$20;

    const-string v2, "Replace Cover Stories with Topic Feed"

    const-string v3, "settings_topic_feed"

    invoke-direct {v1, p0, v2, v3}, Lflipboard/activities/SettingsFragment$20;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 784
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v2, "Show Following as rows"

    const-string v3, "settings_use_grid_for_following"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 785
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 787
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const v2, 0x7f0d02a1

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "always_allow_rotation"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 788
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 790
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v2, "Flip from item to item in detail view"

    const-string v3, "detail_to_detail_on_phone"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 791
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 793
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const v2, 0x7f0d02df

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "volume_button_flip"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 794
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 796
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const v2, 0x7f0d02c4

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "limit_toc_pages"

    invoke-direct {v1, v2, v3, v9}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 797
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 799
    iget-object v1, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    new-instance v2, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v3, "Enable ads"

    const-string v4, "enable_ads"

    invoke-direct {v2, v3, v4, v9}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 801
    iget-object v1, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    new-instance v2, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v3, "Show clickrect for ads"

    const-string v4, "show_hitrects_for_ads"

    invoke-direct {v2, v3, v4, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 803
    iget-object v1, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    new-instance v2, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v3, "Disable ad frequency cap"

    const-string v4, "disable_ad_frequency_cap"

    invoke-direct {v2, v3, v4, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 805
    new-instance v1, Lflipboard/activities/SettingsFragment$21;

    const-string v2, "Clear ad frequency cap user state"

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$21;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 817
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 819
    iget-object v1, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    new-instance v2, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v3, "Local ad manager"

    const-string v4, "use_local_admanager"

    invoke-direct {v2, v3, v4, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 821
    iget-object v1, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    new-instance v2, Lflipboard/activities/SettingsFragment$22;

    const-string v3, "Ad manager location"

    invoke-direct {v2, p0, v3}, Lflipboard/activities/SettingsFragment$22;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 834
    iget-object v1, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    new-instance v2, Lflipboard/activities/SettingsFragment$23;

    const-string v3, "Show connection log"

    invoke-direct {v2, p0, v3}, Lflipboard/activities/SettingsFragment$23;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 851
    iget-object v1, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    new-instance v2, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v3, "Use Flipmag Proxy"

    const-string v4, "use_flipmag_proxy"

    invoke-direct {v2, v3, v4, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 853
    iget-object v1, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    new-instance v2, Lflipboard/activities/SettingsFragment$24;

    const-string v3, "Flipmag proxy host"

    invoke-direct {v2, p0, v3}, Lflipboard/activities/SettingsFragment$24;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 866
    iget-object v1, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    new-instance v2, Lflipboard/activities/SettingsFragment$25;

    const-string v3, "RSS HTML"

    invoke-direct {v2, p0, v3}, Lflipboard/activities/SettingsFragment$25;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 883
    new-instance v1, Lflipboard/activities/SettingsFragment$26;

    const v2, 0x7f0d02e1

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$26;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 900
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 902
    new-instance v1, Lflipboard/activities/SettingsFragment$27;

    const v2, 0x7f0d02ba

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$27;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 929
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 931
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_c

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v2, "showAppMode"

    .line 932
    invoke-interface {v1, v2, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_d

    .line 933
    :cond_c
    invoke-direct {p0, v0}, Lflipboard/activities/SettingsFragment;->a(Lflipboard/activities/SettingsFragment$Category;)V

    .line 936
    :cond_d
    new-instance v1, Lflipboard/activities/SettingsFragment$28;

    const-string v2, "Locale Override"

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$28;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 952
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 954
    new-instance v1, Lflipboard/activities/SettingsFragment$29;

    const-string v2, "Location Override"

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$29;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 967
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 969
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v2, "Force first launch"

    const-string v3, "do_first_launch"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 970
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 972
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v2, "Force Picker Launch"

    const-string v3, "do_first_launch_category"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 973
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 975
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v2, "Always show overflow button"

    const-string v3, "always_show_overflow"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 976
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 978
    new-instance v1, Lflipboard/activities/SettingsFragment$30;

    const v2, 0x7f0d02d2

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$30;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 990
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 992
    new-instance v1, Lflipboard/activities/SettingsFragment$31;

    const-string v2, "Ad Server URL"

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$31;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 1004
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1024
    new-instance v1, Lflipboard/activities/SettingsFragment$33;

    const-string v2, "Force section template"

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$33;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 1059
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1061
    new-instance v1, Lflipboard/activities/SettingsFragment$34;

    const-string v2, "Analytics"

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$34;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 1074
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1076
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v2, "Redirect usage for monitoring"

    const-string v3, "usage_redirect_for_monitoring"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1077
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1079
    new-instance v1, Lflipboard/activities/SettingsFragment$35;

    const v2, 0x7f0d02dd

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$35;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 1091
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1094
    new-instance v1, Lflipboard/activities/SettingsFragment$36;

    const-string v2, "Crash"

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$36;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 1101
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1104
    new-instance v1, Lflipboard/activities/SettingsFragment$37;

    const-string v2, "Show rate me prompt now"

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$37;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 1111
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1113
    new-instance v1, Lflipboard/activities/SettingsFragment$38;

    const-string v2, "Show update prompt now"

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$38;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 1120
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1126
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v2, "freeze feeds"

    const-string v3, "freeze_feeds"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1127
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1134
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v2, "Ignore Hint Time Frequency Cap"

    const-string v3, "ignore_hint_time_freq_cap"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1135
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1137
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v2, "Ignore Session Frequency Cap"

    const-string v3, "ignore_hint_session_freq_cap"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1138
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1140
    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v2, "Enable All Hints"

    const-string v3, "enable_all_hints"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1141
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1143
    new-instance v1, Lflipboard/activities/SettingsFragment$39;

    const-string v2, "Wipe Hints Usage"

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsFragment$39;-><init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V

    .line 1151
    iget-object v2, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1155
    iget-object v1, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    new-instance v2, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v3, "Enable audio"

    const-string v4, "enable_audio"

    invoke-direct {v2, v3, v4, v9}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1158
    iget-object v1, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    new-instance v2, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v3, "Enable image selection info"

    const-string v4, "enable_image_selection_overlay"

    invoke-direct {v2, v3, v4, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1161
    iget-object v1, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    new-instance v2, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v3, "Enable new usage v2 events"

    const-string v4, "enable_new_usage_v2_events"

    invoke-direct {v2, v3, v4, v9}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1164
    iget-object v1, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    new-instance v2, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v3, "Enable tablet profiles"

    const-string v4, "enable_tablet_profiles"

    invoke-direct {v2, v3, v4, v9}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1167
    iget-object v0, v0, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    new-instance v1, Lflipboard/activities/SettingsFragment$RowItem;

    const-string v2, "Enable flip without attribution"

    const-string v3, "enable_flip_no_attrib"

    invoke-direct {v1, v2, v3, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1169
    new-instance v1, Lflipboard/activities/SettingsFragment$Category;

    const v0, 0x7f0d02c1

    invoke-virtual {p0, v0}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lflipboard/activities/SettingsFragment$Category;-><init>(Ljava/lang/String;)V

    .line 1170
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1172
    invoke-static {}, Lflipboard/util/Log;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1173
    new-instance v3, Lflipboard/activities/SettingsFragment$RowItem;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "log_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4, v8}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1174
    iget-object v0, v1, Lflipboard/activities/SettingsFragment$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1177
    :cond_e
    return-void
.end method

.method static synthetic c(Lflipboard/activities/SettingsFragment;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->t:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lflipboard/activities/SettingsFragment;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private e()I
    .locals 4

    .prologue
    .line 1466
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->g:[I

    iget-object v1, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v2, "volume_flip_delay"

    const/16 v3, 0x1f4

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lflipboard/activities/SettingsFragment;->a([II)I

    move-result v0

    return v0
.end method

.method static synthetic e(Lflipboard/activities/SettingsFragment;)I
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Lflipboard/activities/SettingsFragment;->e()I

    move-result v0

    return v0
.end method

.method private f()I
    .locals 4

    .prologue
    .line 1471
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->h:[I

    iget-object v1, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v2, "font_size"

    const/16 v3, 0x64

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lflipboard/activities/SettingsFragment;->a([II)I

    move-result v0

    return v0
.end method

.method static synthetic f(Lflipboard/activities/SettingsFragment;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->s:[Ljava/lang/String;

    return-object v0
.end method

.method private g()I
    .locals 3

    .prologue
    .line 1476
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v1, "text_override_mode"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method static synthetic g(Lflipboard/activities/SettingsFragment;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->v:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lflipboard/activities/SettingsFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->q:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic i(Lflipboard/activities/SettingsFragment;)Lflipboard/activities/SettingsFragment$SettingsAdapter;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->w:Lflipboard/activities/SettingsFragment$SettingsAdapter;

    return-object v0
.end method


# virtual methods
.method final a(I)V
    .locals 8

    .prologue
    const/4 v2, -0x1

    const/4 v6, 0x3

    const/4 v1, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1616
    new-instance v5, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v5}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 1617
    packed-switch p1, :pswitch_data_0

    .line 2191
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    const-string v1, "alertDialog"

    invoke-virtual {v5, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 2196
    return-void

    .line 1619
    :pswitch_1
    const v0, 0x7f0d02e5

    invoke-virtual {v5, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 1620
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->s:[Ljava/lang/String;

    invoke-direct {p0}, Lflipboard/activities/SettingsFragment;->e()I

    move-result v1

    invoke-virtual {v5, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a([Ljava/lang/String;I)V

    .line 1621
    new-instance v0, Lflipboard/activities/SettingsFragment$42;

    invoke-direct {v0, p0}, Lflipboard/activities/SettingsFragment$42;-><init>(Lflipboard/activities/SettingsFragment;)V

    iput-object v0, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto :goto_0

    .line 1631
    :pswitch_2
    const v0, 0x7f0d02ba

    invoke-virtual {v5, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 1632
    new-array v0, v6, [Ljava/lang/String;

    const v2, 0x7f0d02bd

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    const v2, 0x7f0d02bc

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    const v2, 0x7f0d02bb

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1633
    iget-object v2, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v3, "flip_orientation"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v5, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a([Ljava/lang/String;I)V

    .line 1634
    new-instance v0, Lflipboard/activities/SettingsFragment$43;

    invoke-direct {v0, p0}, Lflipboard/activities/SettingsFragment$43;-><init>(Lflipboard/activities/SettingsFragment;)V

    iput-object v0, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto :goto_0

    .line 1644
    :pswitch_3
    const v0, 0x7f0d02a3

    invoke-virtual {v5, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 1645
    new-array v0, v1, [Ljava/lang/String;

    const v2, 0x7f0d02a4

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    const v2, 0x7f0d02a5

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    .line 1646
    iget-object v2, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v3, "application_mode"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v4, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1647
    invoke-virtual {v5, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a([Ljava/lang/String;I)V

    .line 1648
    new-instance v0, Lflipboard/activities/SettingsFragment$44;

    invoke-direct {v0, p0}, Lflipboard/activities/SettingsFragment$44;-><init>(Lflipboard/activities/SettingsFragment;)V

    iput-object v0, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto/16 :goto_0

    .line 1658
    :pswitch_4
    const-string v0, "Locale Override"

    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 1659
    const/16 v0, 0x12

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "System Provided"

    aput-object v0, v2, v3

    const-string v0, "en_US"

    aput-object v0, v2, v4

    const-string v0, "en_UK"

    aput-object v0, v2, v1

    const-string v0, "en_AU"

    aput-object v0, v2, v6

    const/4 v0, 0x4

    const-string v1, "en_CA"

    aput-object v1, v2, v0

    const/4 v0, 0x5

    const-string v1, "fr_FR"

    aput-object v1, v2, v0

    const/4 v0, 0x6

    const-string v1, "de_DE"

    aput-object v1, v2, v0

    const/4 v0, 0x7

    const-string v1, "es_ES"

    aput-object v1, v2, v0

    const/16 v0, 0x8

    const-string v1, "it_IT"

    aput-object v1, v2, v0

    const/16 v0, 0x9

    const-string v1, "nl_NL"

    aput-object v1, v2, v0

    const/16 v0, 0xa

    const-string v1, "ja_JP"

    aput-object v1, v2, v0

    const/16 v0, 0xb

    const-string v1, "zh_CN"

    aput-object v1, v2, v0

    const/16 v0, 0xc

    const-string v1, "zh_TW"

    aput-object v1, v2, v0

    const/16 v0, 0xd

    const-string v1, "zh_HK"

    aput-object v1, v2, v0

    const/16 v0, 0xe

    const-string v1, "ko_KR"

    aput-object v1, v2, v0

    const/16 v0, 0xf

    const-string v1, "pt_BR"

    aput-object v1, v2, v0

    const/16 v0, 0x10

    const-string v1, "pt_PT"

    aput-object v1, v2, v0

    const/16 v0, 0x11

    const-string v1, "Custom..."

    aput-object v1, v2, v0

    .line 1661
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v1, "locale_override"

    const/4 v6, 0x0

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move v1, v4

    move v0, v3

    .line 1662
    :goto_1
    const/16 v7, 0x12

    if-ge v1, v7, :cond_1

    .line 1663
    aget-object v7, v2, v1

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    move v0, v1

    .line 1662
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1669
    :cond_1
    if-nez v0, :cond_2

    if-eqz v6, :cond_2

    aget-object v1, v2, v3

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1671
    const/16 v1, 0x13

    new-array v1, v1, [Ljava/lang/String;

    .line 1672
    aput-object v6, v1, v3

    .line 1673
    const/16 v7, 0x12

    invoke-static {v2, v3, v1, v4, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1677
    :goto_2
    invoke-virtual {v5, v1, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a([Ljava/lang/String;I)V

    .line 1678
    new-instance v0, Lflipboard/activities/SettingsFragment$45;

    invoke-direct {v0, p0, v1, v6}, Lflipboard/activities/SettingsFragment$45;-><init>(Lflipboard/activities/SettingsFragment;[Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto/16 :goto_0

    :cond_2
    move-object v1, v2

    .line 1675
    goto :goto_2

    .line 1725
    :pswitch_5
    const-string v0, "Location Override"

    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 1726
    new-array v1, v1, [Ljava/lang/String;

    const-string v0, "No Override"

    aput-object v0, v1, v3

    const-string v0, "China"

    aput-object v0, v1, v4

    .line 1727
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v0, v0, Lflipboard/io/NetworkManager;->l:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v3

    .line 1728
    :goto_3
    invoke-virtual {v5, v1, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a([Ljava/lang/String;I)V

    .line 1729
    new-instance v0, Lflipboard/activities/SettingsFragment$46;

    invoke-direct {v0, p0}, Lflipboard/activities/SettingsFragment$46;-><init>(Lflipboard/activities/SettingsFragment;)V

    iput-object v0, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto/16 :goto_0

    :cond_3
    move v0, v4

    .line 1727
    goto :goto_3

    .line 1747
    :pswitch_6
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v1, "server_base_urls"

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 1750
    sget-object v1, Lflipboard/activities/SettingsFragment;->d:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 1752
    iget-object v1, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v2, "server_baseurl"

    sget-object v4, Lflipboard/activities/SettingsFragment;->c:Ljava/lang/String;

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1755
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 1760
    :goto_4
    array-length v4, v1

    if-ge v3, v4, :cond_4

    .line 1761
    aget-object v4, v1, v3

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1762
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 1766
    :cond_4
    invoke-virtual {v5, v1, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a([Ljava/lang/String;I)V

    .line 1767
    const-string v2, "Add"

    iput-object v2, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->k:Ljava/lang/String;

    .line 1768
    const-string v2, "Reset"

    iput-object v2, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->m:Ljava/lang/String;

    .line 1769
    const-string v2, "Cancel"

    iput-object v2, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->l:Ljava/lang/String;

    .line 1770
    new-instance v2, Lflipboard/activities/SettingsFragment$47;

    invoke-direct {v2, p0, v1, v0}, Lflipboard/activities/SettingsFragment$47;-><init>(Lflipboard/activities/SettingsFragment;[Ljava/lang/String;Ljava/util/HashSet;)V

    iput-object v2, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto/16 :goto_0

    .line 1807
    :pswitch_7
    const v0, 0x7f0d02d3

    invoke-virtual {v5, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 1808
    new-instance v0, Landroid/widget/EditText;

    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1809
    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1810
    const/16 v1, 0xd0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 1811
    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 1812
    const-string v1, "https://.flipboard.com"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1813
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 1814
    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->s:Landroid/view/View;

    .line 1815
    const-string v1, "Add"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->k:Ljava/lang/String;

    .line 1816
    const-string v1, "Cancel"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->l:Ljava/lang/String;

    .line 1817
    new-instance v1, Lflipboard/activities/SettingsFragment$48;

    invoke-direct {v1, p0, v0}, Lflipboard/activities/SettingsFragment$48;-><init>(Lflipboard/activities/SettingsFragment;Landroid/widget/EditText;)V

    iput-object v1, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto/16 :goto_0

    .line 1839
    :pswitch_8
    const v0, 0x7f0d02dd

    invoke-virtual {v5, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 1840
    new-instance v0, Landroid/widget/EditText;

    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1841
    iget-object v1, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v2, "usage_redirect_monitor_baseurl"

    const-string v3, "http://jing.eng.live.flipboard.com:8080"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1842
    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->s:Landroid/view/View;

    .line 1843
    const-string v1, "OK"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->k:Ljava/lang/String;

    .line 1844
    const-string v1, "Default"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->m:Ljava/lang/String;

    .line 1845
    const-string v1, "Cancel"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->l:Ljava/lang/String;

    .line 1846
    new-instance v1, Lflipboard/activities/SettingsFragment$49;

    invoke-direct {v1, p0, v0}, Lflipboard/activities/SettingsFragment$49;-><init>(Lflipboard/activities/SettingsFragment;Landroid/widget/EditText;)V

    iput-object v1, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto/16 :goto_0

    .line 1871
    :pswitch_9
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v1, "ad_server_base_urls"

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 1874
    sget-object v1, Lflipboard/activities/SettingsFragment;->e:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 1876
    iget-object v1, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v2, "adserver_baseurl"

    invoke-static {}, Lflipboard/activities/SettingsFragment;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1879
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 1884
    :goto_5
    array-length v4, v1

    if-ge v3, v4, :cond_5

    .line 1885
    aget-object v4, v1, v3

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 1886
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 1890
    :cond_5
    invoke-virtual {v5, v1, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a([Ljava/lang/String;I)V

    .line 1891
    const-string v2, "Add"

    iput-object v2, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->k:Ljava/lang/String;

    .line 1892
    const-string v2, "Reset"

    iput-object v2, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->m:Ljava/lang/String;

    .line 1893
    const-string v2, "Cancel"

    iput-object v2, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->l:Ljava/lang/String;

    .line 1894
    new-instance v2, Lflipboard/activities/SettingsFragment$50;

    invoke-direct {v2, p0, v1, v0}, Lflipboard/activities/SettingsFragment$50;-><init>(Lflipboard/activities/SettingsFragment;[Ljava/lang/String;Ljava/util/HashSet;)V

    iput-object v2, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto/16 :goto_0

    .line 1931
    :pswitch_a
    const v0, 0x7f0d02d3

    invoke-virtual {v5, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 1932
    new-instance v0, Landroid/widget/EditText;

    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1933
    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1934
    const/16 v1, 0xd0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 1935
    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 1936
    const-string v1, "http://.flipboard.com"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1937
    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 1938
    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->s:Landroid/view/View;

    .line 1939
    const-string v1, "Add"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->k:Ljava/lang/String;

    .line 1940
    const-string v1, "Cancel"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->l:Ljava/lang/String;

    .line 1941
    new-instance v1, Lflipboard/activities/SettingsFragment$51;

    invoke-direct {v1, p0, v0}, Lflipboard/activities/SettingsFragment$51;-><init>(Lflipboard/activities/SettingsFragment;Landroid/widget/EditText;)V

    iput-object v1, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto/16 :goto_0

    .line 1963
    :pswitch_b
    const-string v0, "Ad Server Internal URL"

    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 1964
    new-instance v0, Landroid/widget/EditText;

    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1965
    iget-object v1, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v2, "adserver_internal_baseurl"

    const-string v3, "http://flint01.beta.live.flipboard.com:35468"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1966
    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->s:Landroid/view/View;

    .line 1967
    const-string v1, "Clear"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->k:Ljava/lang/String;

    .line 1968
    const-string v1, "Default"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->m:Ljava/lang/String;

    .line 1969
    const-string v1, "Cancel"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->l:Ljava/lang/String;

    .line 1970
    new-instance v1, Lflipboard/activities/SettingsFragment$52;

    invoke-direct {v1, p0, v0}, Lflipboard/activities/SettingsFragment$52;-><init>(Lflipboard/activities/SettingsFragment;Landroid/widget/EditText;)V

    iput-object v1, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto/16 :goto_0

    .line 2005
    :pswitch_c
    const-string v0, "Enter host url"

    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 2006
    new-instance v0, Landroid/widget/EditText;

    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 2007
    iget-object v1, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v2, "flipmag_proxy_server"

    const-string v3, "http://cdn.flipboard.com/flipmag-beta/flipmag"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2008
    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->s:Landroid/view/View;

    .line 2009
    const-string v1, "OK"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->k:Ljava/lang/String;

    .line 2010
    const-string v1, "Default"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->m:Ljava/lang/String;

    .line 2011
    const-string v1, "Cancel"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->l:Ljava/lang/String;

    .line 2012
    new-instance v1, Lflipboard/activities/SettingsFragment$53;

    invoke-direct {v1, p0, v0}, Lflipboard/activities/SettingsFragment$53;-><init>(Lflipboard/activities/SettingsFragment;Landroid/widget/EditText;)V

    iput-object v1, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto/16 :goto_0

    .line 2034
    :pswitch_d
    const-string v0, "Ad manager location"

    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 2035
    new-instance v0, Landroid/widget/EditText;

    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 2036
    iget-object v1, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v2, "admanager_location"

    const-string v3, "http://cdn.flipboard.com/flipmag-beta/flipmag/adManager.js"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2037
    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->s:Landroid/view/View;

    .line 2038
    const-string v1, "OK"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->k:Ljava/lang/String;

    .line 2039
    const-string v1, "Default"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->m:Ljava/lang/String;

    .line 2040
    const-string v1, "Cancel"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->l:Ljava/lang/String;

    .line 2041
    new-instance v1, Lflipboard/activities/SettingsFragment$54;

    invoke-direct {v1, p0, v0}, Lflipboard/activities/SettingsFragment$54;-><init>(Lflipboard/activities/SettingsFragment;Landroid/widget/EditText;)V

    iput-object v1, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto/16 :goto_0

    .line 2063
    :pswitch_e
    const-string v0, "RSS HTML location"

    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 2064
    new-instance v0, Landroid/widget/EditText;

    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 2065
    iget-object v1, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v2, "override_rss_html"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2066
    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->s:Landroid/view/View;

    .line 2067
    const-string v1, "OK"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->k:Ljava/lang/String;

    .line 2068
    const-string v1, "Default"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->m:Ljava/lang/String;

    .line 2069
    const-string v1, "Cancel"

    iput-object v1, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->l:Ljava/lang/String;

    .line 2070
    new-instance v1, Lflipboard/activities/SettingsFragment$55;

    invoke-direct {v1, p0, v0}, Lflipboard/activities/SettingsFragment$55;-><init>(Lflipboard/activities/SettingsFragment;Landroid/widget/EditText;)V

    iput-object v1, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto/16 :goto_0

    .line 2094
    :pswitch_f
    const v0, 0x7f0d02c5

    invoke-virtual {v5, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 2096
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_6

    .line 2097
    new-array v0, v6, [Ljava/lang/String;

    const v6, 0x7f0d02c8

    invoke-virtual {p0, v6}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v3

    const v6, 0x7f0d02c9

    invoke-virtual {p0, v6}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v4

    const v6, 0x7f0d02c7

    invoke-virtual {p0, v6}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v1

    .line 2101
    :goto_6
    sget-object v6, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v6, v6, Lflipboard/io/NetworkManager;->k:Ljava/lang/String;

    .line 2103
    const-string v7, "enabled"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2110
    :goto_7
    invoke-virtual {v5, v0, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a([Ljava/lang/String;I)V

    .line 2111
    new-instance v0, Lflipboard/activities/SettingsFragment$56;

    invoke-direct {v0, p0}, Lflipboard/activities/SettingsFragment$56;-><init>(Lflipboard/activities/SettingsFragment;)V

    iput-object v0, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto/16 :goto_0

    .line 2099
    :cond_6
    new-array v0, v6, [Ljava/lang/String;

    const v6, 0x7f0d02c8

    invoke-virtual {p0, v6}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v3

    const v6, 0x7f0d02c9

    invoke-virtual {p0, v6}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v4

    const v6, 0x7f0d02c6

    invoke-virtual {p0, v6}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v1

    goto :goto_6

    .line 2105
    :cond_7
    const-string v3, "ondemand"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    move v3, v4

    .line 2106
    goto :goto_7

    .line 2107
    :cond_8
    const-string v3, "disabled"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    move v3, v1

    .line 2108
    goto :goto_7

    .line 2131
    :pswitch_10
    const v0, 0x7f0d02db

    invoke-virtual {v5, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 2132
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->t:[Ljava/lang/String;

    invoke-direct {p0}, Lflipboard/activities/SettingsFragment;->f()I

    move-result v1

    invoke-virtual {v5, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a([Ljava/lang/String;I)V

    .line 2134
    new-instance v0, Lflipboard/activities/SettingsFragment$57;

    invoke-direct {v0, p0, p0}, Lflipboard/activities/SettingsFragment$57;-><init>(Lflipboard/activities/SettingsFragment;Landroid/widget/AdapterView$OnItemClickListener;)V

    iput-object v0, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto/16 :goto_0

    .line 2147
    :pswitch_11
    const-string v0, "Text Override Mode"

    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 2148
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->u:[Ljava/lang/String;

    invoke-direct {p0}, Lflipboard/activities/SettingsFragment;->g()I

    move-result v1

    invoke-virtual {v5, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a([Ljava/lang/String;I)V

    .line 2149
    new-instance v0, Lflipboard/activities/SettingsFragment$58;

    invoke-direct {v0, p0}, Lflipboard/activities/SettingsFragment$58;-><init>(Lflipboard/activities/SettingsFragment;)V

    iput-object v0, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto/16 :goto_0

    .line 2159
    :pswitch_12
    const-string v0, "Analytics"

    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 2160
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->v:[Ljava/lang/String;

    iget-object v1, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v2, "analytics_tracker"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v5, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a([Ljava/lang/String;I)V

    .line 2161
    new-instance v0, Lflipboard/activities/SettingsFragment$59;

    invoke-direct {v0, p0}, Lflipboard/activities/SettingsFragment$59;-><init>(Lflipboard/activities/SettingsFragment;)V

    iput-object v0, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto/16 :goto_0

    .line 2171
    :pswitch_13
    const-string v0, "Relaunch Required"

    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 2172
    const-string v0, "Relaunching Flipboard is required for this change to take effect."

    iput-object v0, v5, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 2173
    const-string v0, "Now Now"

    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->l:Ljava/lang/String;

    .line 2174
    const-string v0, "Relaunch"

    iput-object v0, v5, Lflipboard/gui/dialog/FLAlertDialogFragment;->k:Ljava/lang/String;

    .line 2175
    new-instance v0, Lflipboard/activities/SettingsFragment$60;

    invoke-direct {v0, p0}, Lflipboard/activities/SettingsFragment$60;-><init>(Lflipboard/activities/SettingsFragment;)V

    iput-object v0, v5, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto/16 :goto_0

    :cond_9
    move v3, v2

    goto/16 :goto_7

    .line 1617
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_c
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_4
        :pswitch_5
        :pswitch_e
        :pswitch_d
        :pswitch_0
        :pswitch_f
        :pswitch_8
        :pswitch_9
        :pswitch_b
        :pswitch_7
        :pswitch_13
        :pswitch_a
    .end packed-switch
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1576
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FlipboardFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1577
    const/16 v0, 0xa

    if-ne p2, v0, :cond_0

    .line 1578
    invoke-direct {p0}, Lflipboard/activities/SettingsFragment;->c()V

    .line 1579
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->w:Lflipboard/activities/SettingsFragment$SettingsAdapter;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->notifyDataSetChanged()V

    .line 1581
    :cond_0
    const/16 v0, 0x3039

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 1582
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->w:Lflipboard/activities/SettingsFragment$SettingsAdapter;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->notifyDataSetChanged()V

    .line 1584
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 327
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 328
    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 329
    const-string v1, "hide_action_bar"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/activities/SettingsFragment;->p:Z

    .line 330
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 334
    const v0, 0x7f03011a

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 335
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/activities/SettingsFragment;->y:Lflipboard/service/FlipboardManager;

    .line 336
    const v0, 0x7f0a004c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 337
    iget-boolean v2, p0, Lflipboard/activities/SettingsFragment;->p:Z

    if-eqz v2, :cond_3

    .line 338
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lflipboard/gui/actionbar/FLActionBar;->setVisibility(I)V

    .line 345
    :goto_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ad:Z

    if-nez v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "contentGuide.json"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/RemoteWatchedFile$Observer;)Lflipboard/service/RemoteWatchedFile;

    .line 346
    :cond_0
    const v0, 0x7f0a0144

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 347
    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "flipboard_settings"

    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    .line 348
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v2, v2, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v2, :cond_1

    .line 349
    iget-object v2, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "application_mode"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 351
    :cond_1
    invoke-direct {p0}, Lflipboard/activities/SettingsFragment;->c()V

    .line 352
    new-instance v2, Lflipboard/activities/SettingsFragment$SettingsAdapter;

    invoke-direct {v2, p0, v4}, Lflipboard/activities/SettingsFragment$SettingsAdapter;-><init>(Lflipboard/activities/SettingsFragment;B)V

    iput-object v2, p0, Lflipboard/activities/SettingsFragment;->w:Lflipboard/activities/SettingsFragment$SettingsAdapter;

    .line 353
    iget-object v2, p0, Lflipboard/activities/SettingsFragment;->w:Lflipboard/activities/SettingsFragment$SettingsAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 354
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 356
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-boolean v0, v0, Lflipboard/activities/FlipboardActivity;->Z:Z

    if-nez v0, :cond_2

    .line 357
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v2, Lflipboard/objs/UsageEventV2$AppEnterNavFrom;->f:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    iput-object v2, v0, Lflipboard/service/FlipboardManager;->aD:Lflipboard/objs/UsageEventV2$AppEnterNavFrom;

    .line 359
    :cond_2
    return-object v1

    .line 340
    :cond_3
    sget-object v2, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v2}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 341
    const v0, 0x7f0a0310

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    .line 342
    const v2, 0x7f0d02e0

    invoke-virtual {p0, v2}, Lflipboard/activities/SettingsFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 1206
    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->v()V

    .line 1207
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onDestroy()V

    .line 1208
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1212
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->y:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1237
    :goto_0
    return-void

    .line 1216
    :cond_0
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->w:Lflipboard/activities/SettingsFragment$SettingsAdapter;

    invoke-virtual {v0, p3}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_1

    .line 1217
    iget v0, p0, Lflipboard/activities/SettingsFragment;->x:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/activities/SettingsFragment;->x:I

    .line 1219
    iget v0, p0, Lflipboard/activities/SettingsFragment;->x:I

    const/4 v3, 0x5

    if-lt v0, v3, :cond_1

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_1

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v3, "showAppMode"

    .line 1220
    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1221
    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v0

    const v3, 0x7f0d00c9

    invoke-virtual {p0, v3}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0201ce

    invoke-virtual {v0, v4, v3}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    .line 1222
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "showAppMode"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1223
    invoke-direct {p0}, Lflipboard/activities/SettingsFragment;->c()V

    .line 1224
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->w:Lflipboard/activities/SettingsFragment$SettingsAdapter;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->notifyDataSetChanged()V

    .line 1228
    :cond_1
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->w:Lflipboard/activities/SettingsFragment$SettingsAdapter;

    invoke-virtual {v0, p3}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->a(I)Lflipboard/activities/SettingsFragment$RowItem;

    move-result-object v3

    .line 1230
    iget-boolean v0, v3, Lflipboard/activities/SettingsFragment$RowItem;->h:Z

    if-eqz v0, :cond_2

    .line 1231
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    iget-object v5, v3, Lflipboard/activities/SettingsFragment$RowItem;->i:Ljava/lang/String;

    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    iget-object v6, v3, Lflipboard/activities/SettingsFragment$RowItem;->i:Ljava/lang/String;

    iget-boolean v7, v3, Lflipboard/activities/SettingsFragment$RowItem;->j:Z

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1232
    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0a030a

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1233
    iget-object v1, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    iget-object v2, v3, Lflipboard/activities/SettingsFragment$RowItem;->i:Ljava/lang/String;

    iget-boolean v4, v3, Lflipboard/activities/SettingsFragment$RowItem;->j:Z

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1234
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->w:Lflipboard/activities/SettingsFragment$SettingsAdapter;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->notifyDataSetChanged()V

    .line 1236
    :cond_2
    invoke-virtual {v3}, Lflipboard/activities/SettingsFragment$RowItem;->a()V

    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 1231
    goto :goto_1
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 1194
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onPause()V

    .line 1195
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1198
    iget-boolean v0, p0, Lflipboard/activities/SettingsFragment;->n:Z

    if-nez v0, :cond_0

    iget v0, p0, Lflipboard/activities/SettingsFragment;->m:I

    invoke-direct {p0}, Lflipboard/activities/SettingsFragment;->g()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lflipboard/activities/SettingsFragment;->o:Z

    if-eqz v0, :cond_1

    .line 1199
    :cond_0
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-static {}, Lflipboard/app/FlipboardApplication;->o()V

    .line 1201
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 1182
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onResume()V

    .line 1184
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v1, "fullscreen"

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/SettingsFragment;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 1185
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v1, "application_mode"

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/SettingsFragment;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 1186
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1188
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->w:Lflipboard/activities/SettingsFragment$SettingsAdapter;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->notifyDataSetInvalidated()V

    .line 1189
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/16 v1, 0x400

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1247
    const-string v0, "fullscreen"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1248
    const-string v0, "fullscreen"

    invoke-interface {p1, v0, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1249
    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 1293
    :cond_0
    :goto_0
    const-string v0, "log"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1294
    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    invoke-interface {p1, p2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/util/Log;->a(Z)Lflipboard/util/Log;

    .line 1296
    :cond_1
    return-void

    .line 1251
    :cond_2
    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0

    .line 1253
    :cond_3
    const-string v0, "application_mode"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1255
    iget-object v0, p0, Lflipboard/activities/SettingsFragment;->r:Landroid/content/SharedPreferences;

    const-string v1, "application_mode"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1257
    if-nez v0, :cond_4

    .line 1258
    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    .line 1265
    :goto_1
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    .line 1266
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1, p1}, Lflipboard/app/FlipboardApplication;->a(Landroid/content/SharedPreferences;)V

    .line 1268
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eq v0, v1, :cond_0

    .line 1269
    iput-boolean v4, p0, Lflipboard/activities/SettingsFragment;->o:Z

    goto :goto_0

    .line 1262
    :cond_4
    invoke-virtual {p0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    goto :goto_1

    .line 1271
    :cond_5
    const-string v0, "enable_ads"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "enable_audio"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1272
    :cond_6
    iput-boolean v4, p0, Lflipboard/activities/SettingsFragment;->o:Z

    goto :goto_0

    .line 1273
    :cond_7
    const-string v0, "enable_all_hints"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1274
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    iget-object v1, v0, Lflipboard/service/HintManager;->c:Lflipboard/objs/ConfigHints;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lflipboard/service/HintManager;->c:Lflipboard/objs/ConfigHints;

    invoke-virtual {v0, v1}, Lflipboard/service/HintManager;->a(Lflipboard/objs/ConfigHints;)V

    goto :goto_0

    .line 1275
    :cond_8
    const-string v0, "use_legacy_cover_stories"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1276
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->h:Lflipboard/service/Section;

    .line 1278
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "SettingsActivity:USE_LEGACY_COVER_STORIES"

    new-instance v3, Lflipboard/activities/SettingsFragment$40;

    invoke-direct {v3, p0, v0}, Lflipboard/activities/SettingsFragment$40;-><init>(Lflipboard/activities/SettingsFragment;Lflipboard/service/Section;)V

    invoke-virtual {v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 1284
    invoke-virtual {v0, v4}, Lflipboard/service/Section;->d(Z)Z

    .line 1287
    const-string v0, "use_legacy_cover_stories"

    invoke-interface {p1, v0, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1288
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "event"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 1289
    const-string v1, "id"

    const-string v2, "UserEnabledLegacyCoverStories"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1290
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    goto/16 :goto_0
.end method
