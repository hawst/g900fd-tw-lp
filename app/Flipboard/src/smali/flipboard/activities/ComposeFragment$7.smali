.class Lflipboard/activities/ComposeFragment$7;
.super Ljava/lang/Object;
.source "ComposeFragment.java"

# interfaces
.implements Lflipboard/service/Flap$CancellableJSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/service/Flap$CancellableJSONResultObserver;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lflipboard/activities/ComposeFragment;

.field private e:Z


# direct methods
.method constructor <init>(Lflipboard/activities/ComposeFragment;Lflipboard/service/Flap$CancellableJSONResultObserver;Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 688
    iput-object p1, p0, Lflipboard/activities/ComposeFragment$7;->d:Lflipboard/activities/ComposeFragment;

    iput-object p2, p0, Lflipboard/activities/ComposeFragment$7;->a:Lflipboard/service/Flap$CancellableJSONResultObserver;

    iput-object p3, p0, Lflipboard/activities/ComposeFragment$7;->b:Ljava/util/List;

    iput-object p4, p0, Lflipboard/activities/ComposeFragment$7;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 2

    .prologue
    .line 698
    iget-boolean v0, p0, Lflipboard/activities/ComposeFragment$7;->e:Z

    if-nez v0, :cond_0

    .line 699
    invoke-static {}, Lflipboard/activities/ComposeFragment;->c()Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 702
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/ComposeFragment$7$1;

    invoke-direct {v1, p0, p1, p0}, Lflipboard/activities/ComposeFragment$7$1;-><init>(Lflipboard/activities/ComposeFragment$7;Lflipboard/json/FLObject;Lflipboard/service/Flap$CancellableJSONResultObserver;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 719
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 723
    iget-boolean v0, p0, Lflipboard/activities/ComposeFragment$7;->e:Z

    if-nez v0, :cond_0

    .line 724
    invoke-static {}, Lflipboard/activities/ComposeFragment;->c()Lflipboard/util/Log;

    move-result-object v0

    const-string v1, "Uploading image failed: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 726
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/ComposeFragment$7$2;

    invoke-direct {v1, p0}, Lflipboard/activities/ComposeFragment$7$2;-><init>(Lflipboard/activities/ComposeFragment$7;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 744
    :cond_0
    return-void
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 693
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/ComposeFragment$7;->e:Z

    .line 694
    return-void
.end method
