.class Lflipboard/activities/DebugLayoutActivity$Adapter;
.super Landroid/widget/BaseAdapter;
.source "DebugLayoutActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SectionPageTemplate;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lflipboard/activities/DebugLayoutActivity;


# direct methods
.method private constructor <init>(Lflipboard/activities/DebugLayoutActivity;)V
    .locals 1

    .prologue
    .line 63
    iput-object p1, p0, Lflipboard/activities/DebugLayoutActivity$Adapter;->b:Lflipboard/activities/DebugLayoutActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 65
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-object v0, v0, Lflipboard/app/FlipboardApplication;->k:Ljava/util/List;

    iput-object v0, p0, Lflipboard/activities/DebugLayoutActivity$Adapter;->a:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lflipboard/activities/DebugLayoutActivity;B)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lflipboard/activities/DebugLayoutActivity$Adapter;-><init>(Lflipboard/activities/DebugLayoutActivity;)V

    return-void
.end method

.method private a(I)Lflipboard/objs/SectionPageTemplate;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lflipboard/activities/DebugLayoutActivity$Adapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SectionPageTemplate;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lflipboard/activities/DebugLayoutActivity$Adapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lflipboard/activities/DebugLayoutActivity$Adapter;->a(I)Lflipboard/objs/SectionPageTemplate;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 75
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, -0x2

    const/4 v7, 0x0

    .line 80
    invoke-direct {p0, p1}, Lflipboard/activities/DebugLayoutActivity$Adapter;->a(I)Lflipboard/objs/SectionPageTemplate;

    move-result-object v0

    .line 81
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lflipboard/activities/DebugLayoutActivity$Adapter;->b:Lflipboard/activities/DebugLayoutActivity;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 82
    invoke-virtual {v1, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 83
    iget-object v2, p0, Lflipboard/activities/DebugLayoutActivity$Adapter;->b:Lflipboard/activities/DebugLayoutActivity;

    invoke-virtual {v2}, Lflipboard/activities/DebugLayoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09007c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 84
    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 86
    new-instance v2, Lflipboard/gui/FLStaticTextView;

    iget-object v3, p0, Lflipboard/activities/DebugLayoutActivity$Adapter;->b:Lflipboard/activities/DebugLayoutActivity;

    invoke-direct {v2, v3}, Lflipboard/gui/FLStaticTextView;-><init>(Landroid/content/Context;)V

    .line 87
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Name: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lflipboard/objs/SectionPageTemplate;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 90
    new-instance v2, Lflipboard/gui/FLStaticTextView;

    iget-object v3, p0, Lflipboard/activities/DebugLayoutActivity$Adapter;->b:Lflipboard/activities/DebugLayoutActivity;

    invoke-direct {v2, v3}, Lflipboard/gui/FLStaticTextView;-><init>(Landroid/content/Context;)V

    .line 91
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Description: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lflipboard/objs/SectionPageTemplate;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 94
    new-instance v2, Landroid/widget/LinearLayout;

    iget-object v3, p0, Lflipboard/activities/DebugLayoutActivity$Adapter;->b:Lflipboard/activities/DebugLayoutActivity;

    invoke-direct {v2, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 95
    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 96
    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 98
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v4, 0x41200000    # 10.0f

    invoke-direct {v3, v7, v8, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 99
    new-instance v4, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;

    iget-object v5, p0, Lflipboard/activities/DebugLayoutActivity$Adapter;->b:Lflipboard/activities/DebugLayoutActivity;

    iget-object v6, p0, Lflipboard/activities/DebugLayoutActivity$Adapter;->b:Lflipboard/activities/DebugLayoutActivity;

    invoke-direct {v4, v5, v6, v0, v9}, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;-><init>(Lflipboard/activities/DebugLayoutActivity;Landroid/content/Context;Lflipboard/objs/SectionPageTemplate;Z)V

    invoke-virtual {v2, v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 100
    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v3, v3, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v3, :cond_0

    .line 101
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v4, 0x41700000    # 15.0f

    invoke-direct {v3, v7, v8, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 102
    iget-object v4, p0, Lflipboard/activities/DebugLayoutActivity$Adapter;->b:Lflipboard/activities/DebugLayoutActivity;

    invoke-static {v4}, Lflipboard/activities/DebugLayoutActivity;->a(Lflipboard/activities/DebugLayoutActivity;)I

    move-result v4

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 103
    new-instance v4, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;

    iget-object v5, p0, Lflipboard/activities/DebugLayoutActivity$Adapter;->b:Lflipboard/activities/DebugLayoutActivity;

    iget-object v6, p0, Lflipboard/activities/DebugLayoutActivity$Adapter;->b:Lflipboard/activities/DebugLayoutActivity;

    invoke-direct {v4, v5, v6, v0, v7}, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;-><init>(Lflipboard/activities/DebugLayoutActivity;Landroid/content/Context;Lflipboard/objs/SectionPageTemplate;Z)V

    invoke-virtual {v2, v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 105
    :cond_0
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 106
    return-object v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 110
    invoke-direct {p0, p3}, Lflipboard/activities/DebugLayoutActivity$Adapter;->a(I)Lflipboard/objs/SectionPageTemplate;

    move-result-object v0

    .line 111
    new-instance v1, Lflipboard/activities/DebugLayoutActivity$Adapter$1;

    invoke-direct {v1, p0, v0}, Lflipboard/activities/DebugLayoutActivity$Adapter$1;-><init>(Lflipboard/activities/DebugLayoutActivity$Adapter;Lflipboard/objs/SectionPageTemplate;)V

    .line 123
    iget-object v0, p0, Lflipboard/activities/DebugLayoutActivity$Adapter;->b:Lflipboard/activities/DebugLayoutActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 124
    return-void
.end method
