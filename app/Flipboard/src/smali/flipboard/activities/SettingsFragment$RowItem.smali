.class Lflipboard/activities/SettingsFragment$RowItem;
.super Ljava/lang/Object;
.source "SettingsFragment.java"


# instance fields
.field final d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:I

.field g:Ljava/lang/String;

.field final h:Z

.field i:Ljava/lang/String;

.field j:Z

.field k:Z


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 1516
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 1517
    return-void
.end method

.method constructor <init>(Ljava/lang/String;B)V
    .locals 1

    .prologue
    .line 1525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1507
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/activities/SettingsFragment$RowItem;->f:I

    .line 1508
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/SettingsFragment$RowItem;->g:Ljava/lang/String;

    .line 1526
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$RowItem;->d:Ljava/lang/String;

    .line 1527
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/SettingsFragment$RowItem;->h:Z

    .line 1528
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/SettingsFragment$RowItem;->k:Z

    .line 1529
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1521
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 1522
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1

    .prologue
    .line 1537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1507
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/activities/SettingsFragment$RowItem;->f:I

    .line 1508
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/SettingsFragment$RowItem;->g:Ljava/lang/String;

    .line 1538
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$RowItem;->d:Ljava/lang/String;

    .line 1539
    iput-object p2, p0, Lflipboard/activities/SettingsFragment$RowItem;->e:Ljava/lang/String;

    .line 1540
    iput-object p3, p0, Lflipboard/activities/SettingsFragment$RowItem;->i:Ljava/lang/String;

    .line 1541
    iput-boolean p4, p0, Lflipboard/activities/SettingsFragment$RowItem;->h:Z

    .line 1542
    iput-boolean p5, p0, Lflipboard/activities/SettingsFragment$RowItem;->j:Z

    .line 1543
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 1533
    const/4 v2, 0x0

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 1534
    return-void
.end method


# virtual methods
.method a()V
    .locals 0

    .prologue
    .line 1557
    return-void
.end method

.method b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1552
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$RowItem;->e:Ljava/lang/String;

    return-object v0
.end method
