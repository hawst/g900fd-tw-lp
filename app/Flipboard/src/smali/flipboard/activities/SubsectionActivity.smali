.class public Lflipboard/activities/SubsectionActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "SubsectionActivity.java"


# static fields
.field public static n:Ljava/lang/String;

.field public static o:Ljava/lang/String;


# instance fields
.field p:Lflipboard/gui/ServicePromptView;

.field private q:Landroid/widget/ViewFlipper;

.field private r:Lflipboard/service/User;

.field private s:Lflipboard/objs/ConfigService;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Lflipboard/activities/SubsectionActivity$ListItemAdapter;

.field private x:Lflipboard/service/Section;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-string v0, "service_name"

    sput-object v0, Lflipboard/activities/SubsectionActivity;->n:Ljava/lang/String;

    .line 23
    const-string v0, "section_id"

    sput-object v0, Lflipboard/activities/SubsectionActivity;->o:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lflipboard/activities/SubsectionActivity;->t:Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lflipboard/activities/SubsectionActivity;->u:Ljava/lang/String;

    .line 30
    iput-object v1, p0, Lflipboard/activities/SubsectionActivity;->v:Ljava/lang/String;

    .line 212
    return-void
.end method

.method static synthetic a(Lflipboard/activities/SubsectionActivity;)Lflipboard/objs/ConfigService;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity;->s:Lflipboard/objs/ConfigService;

    return-object v0
.end method

.method private a(Lflipboard/activities/SubsectionActivity$ListItemAdapter;Ljava/lang/String;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 101
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/SubsectionActivity;->r:Lflipboard/service/User;

    iget-object v2, p0, Lflipboard/activities/SubsectionActivity;->s:Lflipboard/objs/ConfigService;

    new-instance v3, Lflipboard/activities/SubsectionActivity$1;

    invoke-direct {v3, p0, p3, p1}, Lflipboard/activities/SubsectionActivity$1;-><init>(Lflipboard/activities/SubsectionActivity;Landroid/view/View;Lflipboard/activities/SubsectionActivity$ListItemAdapter;)V

    invoke-virtual {v0, v1, v2, p2, v3}, Lflipboard/service/Flap;->a(Lflipboard/service/User;Lflipboard/objs/ConfigService;Ljava/lang/String;Lflipboard/service/Flap$SectionListObserver;)V

    .line 153
    return-void
.end method

.method static synthetic a(Lflipboard/activities/SubsectionActivity;Lflipboard/activities/SubsectionActivity$ListItemAdapter;Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lflipboard/activities/SubsectionActivity;->a(Lflipboard/activities/SubsectionActivity$ListItemAdapter;Ljava/lang/String;Landroid/view/View;)V

    return-void
.end method

.method static synthetic b(Lflipboard/activities/SubsectionActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity;->u:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lflipboard/activities/SubsectionActivity;)Landroid/widget/ViewFlipper;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity;->q:Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method static synthetic d(Lflipboard/activities/SubsectionActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity;->t:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public clickedOutside(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 189
    invoke-virtual {p0}, Lflipboard/activities/SubsectionActivity;->finish()V

    .line 190
    const/4 v0, 0x0

    const v1, 0x7f040004

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/SubsectionActivity;->overridePendingTransition(II)V

    .line 191
    return-void
.end method

.method protected final f()V
    .locals 2

    .prologue
    .line 209
    const/4 v0, 0x0

    const v1, 0x7f040004

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/SubsectionActivity;->overridePendingTransition(II)V

    .line 210
    return-void
.end method

.method public onAddSectionClicked(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 204
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 176
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity;->q:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/SubsectionActivity;->q:Landroid/widget/ViewFlipper;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 177
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onBackPressed()V

    .line 185
    :goto_0
    return-void

    .line 179
    :cond_0
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity;->q:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    .line 180
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity;->q:Landroid/widget/ViewFlipper;

    const v2, 0x7f04001a

    invoke-virtual {v1, p0, v2}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/content/Context;I)V

    .line 181
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity;->q:Landroid/widget/ViewFlipper;

    const v2, 0x7f04001b

    invoke-virtual {v1, p0, v2}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/content/Context;I)V

    .line 182
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity;->q:Landroid/widget/ViewFlipper;

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->showPrevious()V

    .line 183
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity;->q:Landroid/widget/ViewFlipper;

    invoke-virtual {v1, v0}, Landroid/widget/ViewFlipper;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/16 v3, 0x14

    const/4 v4, 0x1

    const/4 v9, 0x0

    .line 40
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-virtual {p0}, Lflipboard/activities/SubsectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 42
    if-nez v0, :cond_0

    .line 43
    invoke-virtual {p0}, Lflipboard/activities/SubsectionActivity;->finish()V

    .line 97
    :goto_0
    return-void

    .line 46
    :cond_0
    sget-object v1, Lflipboard/activities/SubsectionActivity;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lflipboard/activities/SubsectionActivity;->u:Ljava/lang/String;

    .line 47
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity;->u:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 48
    invoke-virtual {p0}, Lflipboard/activities/SubsectionActivity;->finish()V

    goto :goto_0

    .line 52
    :cond_1
    sget-object v1, Lflipboard/activities/SubsectionActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/SubsectionActivity;->t:Ljava/lang/String;

    .line 53
    const v0, 0x7f03002d

    invoke-static {p0, v0, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 54
    invoke-virtual {p0, v0}, Lflipboard/activities/SubsectionActivity;->setContentView(Landroid/view/View;)V

    .line 55
    const v0, 0x7f0a02fc

    invoke-virtual {p0, v0}, Lflipboard/activities/SubsectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ServicePromptView;

    iput-object v0, p0, Lflipboard/activities/SubsectionActivity;->p:Lflipboard/gui/ServicePromptView;

    .line 56
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity;->p:Lflipboard/gui/ServicePromptView;

    iget-object v1, p0, Lflipboard/activities/SubsectionActivity;->u:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Lflipboard/gui/ServicePromptView;->a(Ljava/lang/String;Z)Z

    .line 57
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/activities/SubsectionActivity;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 58
    if-eqz v0, :cond_2

    .line 59
    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/activities/SubsectionActivity;->v:Ljava/lang/String;

    .line 61
    :cond_2
    const v0, 0x7f0a00b9

    invoke-virtual {p0, v0}, Lflipboard/activities/SubsectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lflipboard/activities/SubsectionActivity;->q:Landroid/widget/ViewFlipper;

    .line 62
    const v0, 0x7f03002e

    invoke-static {p0, v0, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 63
    new-instance v0, Lflipboard/activities/SubsectionActivity$ListItemAdapter;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-direct {v0, p0, v2}, Lflipboard/activities/SubsectionActivity$ListItemAdapter;-><init>(Lflipboard/activities/SubsectionActivity;Ljava/util/List;)V

    iput-object v0, p0, Lflipboard/activities/SubsectionActivity;->w:Lflipboard/activities/SubsectionActivity$ListItemAdapter;

    .line 64
    const v0, 0x7f0a00bd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 65
    iget-object v2, p0, Lflipboard/activities/SubsectionActivity;->w:Lflipboard/activities/SubsectionActivity$ListItemAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 66
    const v2, 0x7f0a00ba

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 67
    iget-object v2, p0, Lflipboard/activities/SubsectionActivity;->w:Lflipboard/activities/SubsectionActivity$ListItemAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 69
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity;->q:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->addView(Landroid/view/View;)V

    .line 71
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iput-object v0, p0, Lflipboard/activities/SubsectionActivity;->r:Lflipboard/service/User;

    .line 72
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity;->r:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/activities/SubsectionActivity;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/SubsectionActivity;->x:Lflipboard/service/Section;

    .line 73
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity;->x:Lflipboard/service/Section;

    if-nez v0, :cond_3

    .line 74
    invoke-virtual {p0}, Lflipboard/activities/SubsectionActivity;->finish()V

    goto/16 :goto_0

    .line 76
    :cond_3
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/activities/SubsectionActivity;->u:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/SubsectionActivity;->s:Lflipboard/objs/ConfigService;

    .line 77
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 78
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity;->x:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    .line 79
    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/SidebarGroup$RenderHints;

    .line 80
    iget-object v2, v1, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    const-string v3, "sidebar"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 81
    iget-object v2, v0, Lflipboard/objs/SidebarGroup;->c:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, v0, Lflipboard/objs/SidebarGroup;->c:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_6

    .line 82
    new-instance v2, Lflipboard/objs/ContentDrawerListItemHeader;

    iget-object v3, v0, Lflipboard/objs/SidebarGroup;->c:Ljava/lang/String;

    invoke-direct {v2, v3, v9}, Lflipboard/objs/ContentDrawerListItemHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    :cond_6
    iget-object v2, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/FeedItem;

    .line 86
    iget-object v3, v2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v3, :cond_7

    .line 87
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    :cond_7
    iget-boolean v3, v1, Lflipboard/objs/SidebarGroup$RenderHints;->l:Z

    if-nez v3, :cond_8

    move v3, v4

    :goto_2
    iput-boolean v3, v2, Lflipboard/objs/FeedItem;->cj:Z

    goto :goto_1

    :cond_8
    const/4 v3, 0x0

    goto :goto_2

    .line 94
    :cond_9
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity;->w:Lflipboard/activities/SubsectionActivity$ListItemAdapter;

    invoke-virtual {v0, v5}, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a(Ljava/util/List;)V

    .line 95
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity;->w:Lflipboard/activities/SubsectionActivity$ListItemAdapter;

    invoke-virtual {v0}, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 157
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onResume()V

    .line 159
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/activities/SubsectionActivity;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    .line 160
    iget-object v0, p0, Lflipboard/activities/SubsectionActivity;->v:Ljava/lang/String;

    .line 161
    if-eqz v1, :cond_0

    .line 162
    iget-object v0, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->k:Ljava/lang/String;

    .line 164
    :cond_0
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity;->v:Ljava/lang/String;

    if-nez v1, :cond_3

    if-nez v0, :cond_2

    move v1, v2

    :goto_0
    if-nez v1, :cond_1

    .line 165
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity;->p:Lflipboard/gui/ServicePromptView;

    iget-object v3, p0, Lflipboard/activities/SubsectionActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Lflipboard/gui/ServicePromptView;->a(Ljava/lang/String;Z)Z

    .line 166
    iget-object v1, p0, Lflipboard/activities/SubsectionActivity;->w:Lflipboard/activities/SubsectionActivity$ListItemAdapter;

    invoke-static {}, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a()V

    iget-object v1, v1, Lflipboard/activities/SubsectionActivity$ListItemAdapter;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 167
    const v1, 0x7f03002e

    invoke-static {p0, v1, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 168
    iget-object v2, p0, Lflipboard/activities/SubsectionActivity;->w:Lflipboard/activities/SubsectionActivity$ListItemAdapter;

    const v3, 0x7f0a00ba

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v2, v4, v1}, Lflipboard/activities/SubsectionActivity;->a(Lflipboard/activities/SubsectionActivity$ListItemAdapter;Ljava/lang/String;Landroid/view/View;)V

    .line 170
    :cond_1
    iput-object v0, p0, Lflipboard/activities/SubsectionActivity;->v:Ljava/lang/String;

    .line 171
    return-void

    .line 164
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method
