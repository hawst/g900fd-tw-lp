.class public Lflipboard/activities/ContentDrawerPhoneActivity;
.super Lflipboard/activities/ContentDrawerActivity;
.source "ContentDrawerPhoneActivity.java"


# instance fields
.field private final A:Ljava/lang/String;

.field private final B:Ljava/lang/String;

.field private final C:Ljava/lang/String;

.field private final D:Ljava/lang/String;

.field private final E:Ljava/lang/String;

.field private F:Lflipboard/gui/ContentDrawerView;

.field private G:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/ContentDrawerHandler;",
            "Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private ac:Landroid/view/View;

.field protected s:Lflipboard/gui/FLEditText;

.field protected t:Landroid/view/View;

.field protected u:Lflipboard/gui/FLSearchView;

.field v:Landroid/view/View;

.field w:Landroid/view/View;

.field private final x:Ljava/lang/String;

.field private final y:Ljava/lang/String;

.field private final z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lflipboard/activities/ContentDrawerActivity;-><init>()V

    .line 45
    const-string v0, "state_title"

    iput-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->x:Ljava/lang/String;

    .line 46
    const-string v0, "state_description"

    iput-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->y:Ljava/lang/String;

    .line 47
    const-string v0, "state_edit"

    iput-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->z:Ljava/lang/String;

    .line 48
    const-string v0, "state_logout"

    iput-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->A:Ljava/lang/String;

    .line 49
    const-string v0, "state_service"

    iput-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->B:Ljava/lang/String;

    .line 50
    const-string v0, "state_has_content"

    iput-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->C:Ljava/lang/String;

    .line 51
    const-string v0, "state_no_content_text"

    iput-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->D:Ljava/lang/String;

    .line 52
    const-string v0, "state_pagekey"

    iput-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->E:Ljava/lang/String;

    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x0

    .line 193
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 194
    if-eqz v0, :cond_0

    .line 195
    const-string v1, "opened_from_discovery"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 197
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->w:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->u:Lflipboard/gui/FLSearchView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLSearchView;->setVisibility(I)V

    .line 199
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->F:Lflipboard/gui/ContentDrawerView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/gui/ContentDrawerListItemAdapter;->f:Z

    .line 200
    const v0, 0x7f0a0106

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerPhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 201
    if-eqz v0, :cond_0

    .line 202
    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    const-string v1, "open_in_favorites"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 205
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->removeAllViews()V

    .line 206
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->ac:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    new-instance v2, Lflipboard/gui/personal/TocGridFragment;

    invoke-direct {v2}, Lflipboard/gui/personal/TocGridFragment;-><init>()V

    const-string v3, "FOLLOWING"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 207
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->ac:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 208
    :cond_2
    const-string v1, "open_in_notifications"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 209
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->removeAllViews()V

    .line 210
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->F()Lflipboard/service/ContentDrawerHandler;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/service/ContentDrawerHandler;->c()V

    iget-object v0, v0, Lflipboard/service/ContentDrawerHandler;->c:Lflipboard/objs/ConfigFolder;

    invoke-virtual {p0, v0, v4}, Lflipboard/activities/ContentDrawerPhoneActivity;->a(Lflipboard/objs/ContentDrawerListItem;Z)Lflipboard/gui/ContentDrawerView;

    goto :goto_0

    .line 211
    :cond_3
    const-string v1, "open_in_account"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 212
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->removeAllViews()V

    .line 213
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->ac:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {}, Lflipboard/util/ActivityUtil;->a()Lflipboard/activities/FlipboardFragment;

    move-result-object v2

    const-string v3, "PROFILE"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 214
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->ac:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 215
    :cond_4
    const-string v1, "search_requested"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 216
    const-string v1, "search_term"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 217
    if-eqz v0, :cond_0

    .line 218
    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerPhoneActivity;->c(Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 176
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 177
    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 179
    sget-object v1, Lflipboard/activities/ContentDrawerPhoneActivity;->o:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 181
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->F:Lflipboard/gui/ContentDrawerView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v0, p1}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(Ljava/util/List;)V

    .line 182
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->F:Lflipboard/gui/ContentDrawerView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    invoke-virtual {v0, p0}, Lflipboard/gui/EditableListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 183
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 481
    invoke-super {p0, p1}, Lflipboard/activities/ContentDrawerActivity;->b(Ljava/lang/String;)V

    .line 482
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ContentDrawerView;

    .line 484
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->F()Lflipboard/service/ContentDrawerHandler;

    move-result-object v1

    iget-object v1, v1, Lflipboard/service/ContentDrawerHandler;->g:Lflipboard/objs/ConfigServices;

    .line 485
    if-eqz v1, :cond_1

    .line 486
    invoke-virtual {v1}, Lflipboard/objs/ConfigServices;->c()Ljava/util/List;

    move-result-object v1

    .line 490
    :goto_0
    if-eqz v0, :cond_0

    iget-object v2, v0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    if-eqz v2, :cond_0

    .line 491
    iget-object v2, v0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v2, v1}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(Ljava/util/List;)V

    .line 492
    iget-object v1, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    new-instance v2, Lflipboard/activities/ContentDrawerPhoneActivity$5;

    invoke-direct {v2, p0, v0}, Lflipboard/activities/ContentDrawerPhoneActivity$5;-><init>(Lflipboard/activities/ContentDrawerPhoneActivity;Lflipboard/gui/ContentDrawerView;)V

    invoke-virtual {v1, v2}, Lflipboard/gui/EditableListView;->post(Ljava/lang/Runnable;)Z

    .line 500
    :cond_0
    return-void

    .line 488
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const-wide/16 v6, 0x12c

    const/4 v4, 0x0

    .line 376
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->v:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 378
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->F:Lflipboard/gui/ContentDrawerView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/EditableListView;->setVisibility(I)V

    .line 380
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 381
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    iget-object v2, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->v:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-direct {v1, v4, v4, v4, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 382
    invoke-virtual {v1, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 383
    invoke-virtual {v1, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 384
    invoke-virtual {v1, v5}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    .line 385
    iget-object v2, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->v:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 388
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    iget-object v3, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->v:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    invoke-direct {v2, v4, v4, v4, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 389
    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 390
    invoke-virtual {v2, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 391
    iget-object v3, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->w:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 394
    iget-object v2, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->s:Lflipboard/gui/FLEditText;

    invoke-virtual {v2}, Lflipboard/gui/FLEditText;->clearFocus()V

    .line 396
    new-instance v2, Lflipboard/activities/ContentDrawerPhoneActivity$4;

    invoke-direct {v2, p0, p1}, Lflipboard/activities/ContentDrawerPhoneActivity$4;-><init>(Lflipboard/activities/ContentDrawerPhoneActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 422
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    iget-object v2, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->v:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v1, v4, v4, v2, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 423
    invoke-virtual {v1, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 424
    invoke-virtual {v1, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 425
    invoke-virtual {v1, v5}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    .line 426
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 430
    :cond_0
    return-void
.end method

.method public compose(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 236
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/ComposeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerPhoneActivity;->startActivity(Landroid/content/Intent;)V

    .line 237
    return-void
.end method

.method protected final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ContentDrawerView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()V
    .locals 0

    .prologue
    .line 341
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerPhoneActivity;->finish()V

    .line 342
    return-void
.end method

.method protected final n()V
    .locals 2

    .prologue
    .line 226
    invoke-super {p0}, Lflipboard/activities/ContentDrawerActivity;->n()V

    .line 228
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    .line 229
    instance-of v1, v0, Lflipboard/gui/ContentDrawerView;

    if-eqz v1, :cond_0

    .line 230
    check-cast v0, Lflipboard/gui/ContentDrawerView;

    invoke-virtual {v0}, Lflipboard/gui/ContentDrawerView;->d()V

    .line 232
    :cond_0
    return-void
.end method

.method public final o()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x12c

    const/16 v5, 0x8

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 437
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->v:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v5, :cond_0

    .line 438
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 439
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    iget-object v2, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->v:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-direct {v1, v3, v3, v2, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 440
    invoke-virtual {v1, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 441
    invoke-virtual {v1, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 442
    iget-object v2, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->v:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 443
    iget-object v1, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->v:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 445
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    iget-object v2, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->v:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-direct {v1, v3, v3, v2, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 446
    invoke-virtual {v1, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 447
    invoke-virtual {v1, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 448
    iget-object v2, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->w:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 449
    iget-object v1, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->w:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 450
    iget-object v1, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->s:Lflipboard/gui/FLEditText;

    invoke-virtual {v1}, Lflipboard/gui/FLEditText;->clearFocus()V

    .line 451
    iget-object v1, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->t:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 453
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    iget-object v2, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->v:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-direct {v1, v3, v3, v2, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 454
    invoke-virtual {v1, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 455
    invoke-virtual {v1, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 456
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->F:Lflipboard/gui/ContentDrawerView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    invoke-virtual {v0, v1}, Lflipboard/gui/EditableListView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 457
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->F:Lflipboard/gui/ContentDrawerView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    invoke-virtual {v0, v4}, Lflipboard/gui/EditableListView;->setVisibility(I)V

    .line 459
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->t:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 461
    :cond_0
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->F:Lflipboard/gui/ContentDrawerView;

    iget-object v1, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    invoke-virtual {v1}, Lflipboard/gui/EditableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    iget-object v2, v0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    if-eq v1, v2, :cond_1

    iget-object v1, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v1, v0}, Lflipboard/gui/EditableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 462
    :cond_1
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 317
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/ContentDrawerActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 319
    const/16 v0, 0x3039

    if-ne p1, v0, :cond_1

    if-ne p2, v1, :cond_1

    .line 321
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ContentDrawerView;

    .line 323
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->F()Lflipboard/service/ContentDrawerHandler;

    move-result-object v1

    iget-object v1, v1, Lflipboard/service/ContentDrawerHandler;->f:Ljava/util/List;

    .line 325
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/ContentDrawerListItem;

    .line 326
    invoke-interface {v2}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    const/4 v4, 0x3

    if-ne v1, v4, :cond_0

    move-object v1, v2

    .line 327
    check-cast v1, Lflipboard/objs/ConfigFolder;

    .line 328
    iget-object v4, v1, Lflipboard/objs/ConfigFolder;->a:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v1, v1, Lflipboard/objs/ConfigFolder;->a:Ljava/lang/String;

    const-string v4, "featured"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 329
    iget-object v1, v0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-interface {v2}, Lflipboard/objs/ContentDrawerListItem;->c()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1, v3}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(Ljava/util/List;)V

    .line 330
    invoke-interface {v2}, Lflipboard/objs/ContentDrawerListItem;->n()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2}, Lflipboard/objs/ContentDrawerListItem;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/ContentDrawerView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :cond_1
    return-void
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 504
    const/4 v3, 0x0

    .line 506
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 507
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->u:Lflipboard/gui/FLSearchView;

    invoke-virtual {v0}, Lflipboard/gui/FLSearchView;->a()Z

    move-result v0

    .line 508
    if-nez v0, :cond_0

    .line 509
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerPhoneActivity;->o()V

    move v0, v2

    .line 529
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 530
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerPhoneActivity;->finish()V

    .line 532
    :cond_1
    return-void

    .line 513
    :cond_2
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    iget-object v1, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v1}, Lflipboard/gui/FLViewFlipper;->getDisplayedChild()I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 514
    instance-of v0, v1, Lflipboard/gui/ContentDrawerView;

    if-eqz v0, :cond_4

    move-object v0, v1

    check-cast v0, Lflipboard/gui/ContentDrawerView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    if-eqz v0, :cond_4

    move-object v0, v1

    check-cast v0, Lflipboard/gui/ContentDrawerView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    iget-boolean v0, v0, Lflipboard/gui/EditableListView;->a:Z

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 515
    check-cast v0, Lflipboard/gui/ContentDrawerView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->k:Landroid/widget/Button;

    .line 516
    if-eqz v0, :cond_3

    .line 517
    check-cast v1, Lflipboard/gui/ContentDrawerView;

    iget-object v0, v1, Lflipboard/gui/ContentDrawerView;->k:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerPhoneActivity;->onEditClicked(Landroid/view/View;)V

    move v0, v2

    .line 518
    goto :goto_0

    .line 520
    :cond_3
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->b()V

    move v0, v2

    .line 523
    goto :goto_0

    :cond_4
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->a()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->getChildCount()I

    move-result v0

    if-lez v0, :cond_5

    .line 524
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->b()V

    move v0, v2

    .line 525
    goto :goto_0

    :cond_5
    move v0, v3

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 65
    const v0, 0x7f030042

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerPhoneActivity;->setContentView(I)V

    .line 66
    invoke-super {p0, p1}, Lflipboard/activities/ContentDrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 67
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerPhoneActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    :goto_0
    return-void

    .line 71
    :cond_0
    const v0, 0x7f0a004e

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerPhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->ac:Landroid/view/View;

    .line 72
    const v0, 0x7f0a00fc

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerPhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ContentDrawerView;

    iput-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->F:Lflipboard/gui/ContentDrawerView;

    .line 73
    const v0, 0x7f0a00fd

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerPhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->v:Landroid/view/View;

    .line 74
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->v:Landroid/view/View;

    const v1, 0x7f0a004c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 75
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    .line 76
    const v0, 0x7f0a0107

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerPhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->w:Landroid/view/View;

    .line 78
    const v0, 0x7f0a010b

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerPhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLSearchView;

    iput-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->u:Lflipboard/gui/FLSearchView;

    .line 79
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerPhoneActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 80
    if-eqz v0, :cond_1

    .line 81
    const-string v1, "extra_origin_section_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_1

    .line 83
    iget-object v1, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->u:Lflipboard/gui/FLSearchView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLSearchView;->setOriginSectionId(Ljava/lang/String;)V

    .line 86
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->u:Lflipboard/gui/FLSearchView;

    new-instance v1, Lflipboard/activities/ContentDrawerPhoneActivity$1;

    invoke-direct {v1, p0}, Lflipboard/activities/ContentDrawerPhoneActivity$1;-><init>(Lflipboard/activities/ContentDrawerPhoneActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLSearchView;->setSearchViewListener(Lflipboard/gui/FLSearchView$FLSearchViewListener;)V

    .line 100
    const v0, 0x7f0a010a

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerPhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->t:Landroid/view/View;

    .line 101
    const v0, 0x7f0a0108

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerPhoneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLEditText;

    iput-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->s:Lflipboard/gui/FLEditText;

    .line 102
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->s:Lflipboard/gui/FLEditText;

    invoke-virtual {v0}, Lflipboard/gui/FLEditText;->clearFocus()V

    .line 103
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->s:Lflipboard/gui/FLEditText;

    new-instance v1, Lflipboard/activities/ContentDrawerPhoneActivity$2;

    invoke-direct {v1, p0}, Lflipboard/activities/ContentDrawerPhoneActivity$2;-><init>(Lflipboard/activities/ContentDrawerPhoneActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    invoke-virtual {p0, p1}, Lflipboard/activities/ContentDrawerPhoneActivity;->a(Landroid/os/Bundle;)V

    .line 113
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerPhoneActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/activities/ContentDrawerPhoneActivity;->b(Landroid/content/Intent;)V

    .line 115
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->F()Lflipboard/service/ContentDrawerHandler;

    move-result-object v0

    new-instance v1, Lflipboard/activities/ContentDrawerPhoneActivity$3;

    invoke-direct {v1, p0}, Lflipboard/activities/ContentDrawerPhoneActivity$3;-><init>(Lflipboard/activities/ContentDrawerPhoneActivity;)V

    iput-object v1, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->G:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/util/Observer;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 536
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->G:Lflipboard/util/Observer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->F()Lflipboard/service/ContentDrawerHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 537
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->F()Lflipboard/service/ContentDrawerHandler;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->G:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/util/Observer;)V

    .line 539
    :cond_0
    invoke-super {p0}, Lflipboard/activities/ContentDrawerActivity;->onDestroy()V

    .line 540
    return-void
.end method

.method public onEditClicked(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 346
    check-cast p1, Lflipboard/gui/FLButton;

    .line 347
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ContentDrawerView;

    .line 348
    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    .line 349
    iget-boolean v1, v0, Lflipboard/gui/EditableListView;->a:Z

    if-eqz v1, :cond_0

    .line 350
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/EditableListView;->setEditing(Z)V

    .line 351
    const v0, 0x7f0d00cb

    invoke-virtual {p1, v0}, Lflipboard/gui/FLButton;->setText(I)V

    .line 356
    :goto_0
    return-void

    .line 353
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/EditableListView;->setEditing(Z)V

    .line 354
    const v0, 0x7f0d00c9

    invoke-virtual {p1, v0}, Lflipboard/gui/FLButton;->setText(I)V

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 187
    invoke-super {p0, p1}, Lflipboard/activities/ContentDrawerActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 188
    invoke-direct {p0, p1}, Lflipboard/activities/ContentDrawerPhoneActivity;->b(Landroid/content/Intent;)V

    .line 189
    return-void
.end method

.method public onRemoveButtonClicked(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 361
    invoke-super {p0, p1}, Lflipboard/activities/ContentDrawerActivity;->onRemoveButtonClicked(Landroid/view/View;)V

    .line 362
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 363
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigSection;

    iget-object v0, v0, Lflipboard/objs/ConfigSection;->i:Ljava/lang/String;

    .line 364
    if-nez v0, :cond_0

    .line 365
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    .line 366
    iget-object v1, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v1}, Lflipboard/gui/FLViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/ContentDrawerView;

    .line 367
    iget-object v1, v1, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v1, v0}, Lflipboard/gui/ContentDrawerListItemAdapter;->b(Lflipboard/objs/ContentDrawerListItem;)I

    .line 370
    :cond_0
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 267
    const-string v0, "state_title"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 268
    const v0, 0x7f030047

    invoke-static {p0, v0, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ContentDrawerView;

    .line 269
    const v1, 0x7f0a004c

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/actionbar/FLActionBar;

    .line 270
    invoke-virtual {v1, v6, v5}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    .line 272
    const-string v1, "state_title"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "state_description"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/ContentDrawerView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    iget-object v1, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    invoke-virtual {v1, p0}, Lflipboard/gui/EditableListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 274
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerPhoneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0021

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "state_title"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->F()Lflipboard/service/ContentDrawerHandler;

    move-result-object v1

    iget-object v1, v1, Lflipboard/service/ContentDrawerHandler;->g:Lflipboard/objs/ConfigServices;

    if-eqz v1, :cond_3

    .line 276
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->F()Lflipboard/service/ContentDrawerHandler;

    move-result-object v1

    iget-object v1, v1, Lflipboard/service/ContentDrawerHandler;->g:Lflipboard/objs/ConfigServices;

    invoke-virtual {v1}, Lflipboard/objs/ConfigServices;->c()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->setItems(Ljava/util/List;)V

    .line 289
    :goto_0
    const-string v1, "state_service"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->setServiceId(Ljava/lang/String;)V

    .line 290
    const-string v1, "state_pagekey"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->setPageKey(Ljava/lang/String;)V

    .line 292
    const-string v1, "state_edit"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "state_edit"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 293
    invoke-virtual {v0}, Lflipboard/gui/ContentDrawerView;->a()V

    .line 297
    :cond_0
    :goto_1
    const-string v1, "state_has_content"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->setHasContent(Z)V

    .line 298
    iget-boolean v1, v0, Lflipboard/gui/ContentDrawerView;->f:Z

    if-nez v1, :cond_1

    .line 299
    const v1, 0x7f0a0102

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLStaticTextView;

    .line 300
    const-string v2, "state_no_content_text"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 301
    const v1, 0x7f0a0101

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 302
    const v1, 0x7f0a00ba

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 304
    :cond_1
    iget-object v1, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v1}, Lflipboard/gui/FLViewFlipper;->removeAllViews()V

    .line 305
    iget-object v1, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLViewFlipper;->addView(Landroid/view/View;)V

    .line 306
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0, v4}, Lflipboard/gui/FLViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 307
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0, v4}, Lflipboard/gui/FLViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 308
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->showNext()V

    .line 311
    :cond_2
    invoke-super {p0, p1}, Lflipboard/activities/ContentDrawerActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 312
    :goto_2
    return-void

    .line 277
    :cond_3
    const-string v1, "state_title"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    const-string v1, "state_title"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerPhoneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0022

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 278
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerPhoneActivity;->q_()V

    .line 279
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerPhoneActivity;->j()V

    .line 280
    invoke-virtual {v0, v7}, Lflipboard/gui/ContentDrawerView;->setVisibility(I)V

    .line 281
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->q:Lflipboard/gui/ContentDrawerView;

    const-string v1, "state_title"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "state_description"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/ContentDrawerView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->q:Lflipboard/gui/ContentDrawerView;

    const-string v1, "state_service"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->setServiceId(Ljava/lang/String;)V

    .line 283
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->q:Lflipboard/gui/ContentDrawerView;

    invoke-virtual {v0, v4}, Lflipboard/gui/ContentDrawerView;->setEmptyMessage$505cbf4b(Ljava/lang/String;)V

    .line 284
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->q:Lflipboard/gui/ContentDrawerView;

    invoke-virtual {p0, v0, v5, v6}, Lflipboard/activities/ContentDrawerPhoneActivity;->a(Lflipboard/gui/ContentDrawerView;ZZ)V

    goto :goto_2

    .line 287
    :cond_4
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->X:Ljava/util/List;

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->setItems(Ljava/util/List;)V

    goto/16 :goto_0

    .line 294
    :cond_5
    const-string v1, "state_logout"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "state_logout"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 295
    invoke-virtual {v0}, Lflipboard/gui/ContentDrawerView;->b()V

    goto/16 :goto_1
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 170
    invoke-super {p0}, Lflipboard/activities/ContentDrawerActivity;->onResume()V

    .line 171
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->u:Lflipboard/gui/FLSearchView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lflipboard/gui/FLSearchView;->setActiveTimeInMills(J)V

    .line 172
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 241
    invoke-super {p0, p1}, Lflipboard/activities/ContentDrawerActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 244
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->a()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lflipboard/gui/ContentDrawerView;

    if-eqz v0, :cond_2

    .line 245
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ContentDrawerView;

    .line 246
    const-string v1, "state_title"

    iget-object v2, v0, Lflipboard/gui/ContentDrawerView;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const-string v1, "state_description"

    iget-object v2, v0, Lflipboard/gui/ContentDrawerView;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const-string v1, "state_edit"

    iget-boolean v2, v0, Lflipboard/gui/ContentDrawerView;->l:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 249
    const-string v1, "state_logout"

    iget-boolean v2, v0, Lflipboard/gui/ContentDrawerView;->m:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 250
    const-string v1, "state_has_content"

    iget-boolean v2, v0, Lflipboard/gui/ContentDrawerView;->f:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 251
    iget-boolean v1, v0, Lflipboard/gui/ContentDrawerView;->f:Z

    if-nez v1, :cond_0

    .line 252
    const v1, 0x7f0a0102

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLStaticTextView;

    .line 253
    const-string v2, "state_no_content_text"

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    :cond_0
    const-string v1, "state_service"

    iget-object v2, v0, Lflipboard/gui/ContentDrawerView;->i:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    iget-object v1, v0, Lflipboard/gui/ContentDrawerView;->j:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 257
    const-string v1, "state_pagekey"

    iget-object v2, v0, Lflipboard/gui/ContentDrawerView;->j:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    :cond_1
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v0}, Lflipboard/gui/ContentDrawerListItemAdapter;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, Lflipboard/service/FlipboardManager;->X:Ljava/util/List;

    .line 261
    :cond_2
    return-void
.end method

.method protected final r_()Z
    .locals 1

    .prologue
    .line 475
    const/4 v0, 0x0

    return v0
.end method
