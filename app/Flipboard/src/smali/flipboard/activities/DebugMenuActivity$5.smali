.class Lflipboard/activities/DebugMenuActivity$5;
.super Ljava/lang/Object;
.source "DebugMenuActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lflipboard/activities/DebugMenuActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/DebugMenuActivity;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lflipboard/activities/DebugMenuActivity$5;->a:Lflipboard/activities/DebugMenuActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 195
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lflipboard/activities/DebugMenuActivity$5;->a:Lflipboard/activities/DebugMenuActivity;

    const-class v2, Lflipboard/activities/DebugMenuActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 196
    const-string v1, "sectionId"

    iget-object v2, p0, Lflipboard/activities/DebugMenuActivity$5;->a:Lflipboard/activities/DebugMenuActivity;

    invoke-static {v2}, Lflipboard/activities/DebugMenuActivity;->b(Lflipboard/activities/DebugMenuActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    const-string v1, "feedItemId"

    iget-object v2, p0, Lflipboard/activities/DebugMenuActivity$5;->a:Lflipboard/activities/DebugMenuActivity;

    invoke-static {v2}, Lflipboard/activities/DebugMenuActivity;->c(Lflipboard/activities/DebugMenuActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    packed-switch p3, :pswitch_data_0

    .line 207
    :goto_0
    iget-object v1, p0, Lflipboard/activities/DebugMenuActivity$5;->a:Lflipboard/activities/DebugMenuActivity;

    invoke-virtual {v1, v0}, Lflipboard/activities/DebugMenuActivity;->startActivity(Landroid/content/Intent;)V

    .line 212
    return-void

    .line 200
    :pswitch_0
    const-string v1, "activityType"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 203
    :pswitch_1
    const-string v1, "activityType"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 206
    :pswitch_2
    const-string v1, "activityType"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 198
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
