.class Lflipboard/activities/SettingsFragment$39;
.super Lflipboard/activities/SettingsFragment$RowItem;
.source "SettingsFragment.java"


# instance fields
.field final synthetic a:Lflipboard/activities/SettingsFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1144
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$39;->a:Lflipboard/activities/SettingsFragment;

    invoke-direct {p0, p2}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1147
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    iget-object v1, v0, Lflipboard/io/UsageManager;->j:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    iget-object v0, v0, Lflipboard/io/UsageManager;->j:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    sget-object v4, Lflipboard/io/UsageManager;->a:Lflipboard/util/Log;

    const-string v5, "removed cached key: %s"

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-static {v2}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    .line 1148
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$39;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v0

    const-string v2, "Removed %d cached events"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v7

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0201ce

    invoke-virtual {v0, v2, v1}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    .line 1149
    return-void
.end method
