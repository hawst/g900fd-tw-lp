.class Lflipboard/activities/CacheConfigActivity$3;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "CacheConfigActivity.java"


# instance fields
.field final synthetic a:Lflipboard/activities/CacheConfigActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/CacheConfigActivity;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lflipboard/activities/CacheConfigActivity$3;->a:Lflipboard/activities/CacheConfigActivity;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;)V
    .locals 0

    .prologue
    .line 143
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->a()V

    .line 144
    return-void
.end method

.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 4

    .prologue
    .line 149
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->a()V

    .line 151
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity$3;->a:Lflipboard/activities/CacheConfigActivity;

    invoke-static {v0}, Lflipboard/activities/CacheConfigActivity;->b(Lflipboard/activities/CacheConfigActivity;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/CacheConfigActivity$3;->a:Lflipboard/activities/CacheConfigActivity;

    iget-object v1, v1, Lflipboard/activities/CacheConfigActivity;->N:Landroid/content/SharedPreferences;

    const-string v2, "cache_location"

    const-string v3, "external"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity$3;->a:Lflipboard/activities/CacheConfigActivity;

    iget-object v0, v0, Lflipboard/activities/CacheConfigActivity;->N:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "cache_location"

    iget-object v2, p0, Lflipboard/activities/CacheConfigActivity$3;->a:Lflipboard/activities/CacheConfigActivity;

    invoke-static {v2}, Lflipboard/activities/CacheConfigActivity;->b(Lflipboard/activities/CacheConfigActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 153
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity$3;->a:Lflipboard/activities/CacheConfigActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lflipboard/activities/CacheConfigActivity;->a(Lflipboard/activities/CacheConfigActivity;Z)Z

    .line 155
    :cond_0
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity$3;->a:Lflipboard/activities/CacheConfigActivity;

    invoke-static {v0}, Lflipboard/activities/CacheConfigActivity;->c(Lflipboard/activities/CacheConfigActivity;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/CacheConfigActivity$3;->a:Lflipboard/activities/CacheConfigActivity;

    iget-object v1, v1, Lflipboard/activities/CacheConfigActivity;->N:Landroid/content/SharedPreferences;

    const-string v2, "cache_size"

    const-string v3, "128MB"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 156
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity$3;->a:Lflipboard/activities/CacheConfigActivity;

    iget-object v0, v0, Lflipboard/activities/CacheConfigActivity;->N:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "cache_size"

    iget-object v2, p0, Lflipboard/activities/CacheConfigActivity$3;->a:Lflipboard/activities/CacheConfigActivity;

    invoke-static {v2}, Lflipboard/activities/CacheConfigActivity;->c(Lflipboard/activities/CacheConfigActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 160
    :cond_1
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity$3;->a:Lflipboard/activities/CacheConfigActivity;

    invoke-static {v0}, Lflipboard/activities/CacheConfigActivity;->d(Lflipboard/activities/CacheConfigActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 161
    sget-object v0, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/io/DownloadManager;->a(Z)V

    .line 162
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-static {}, Lflipboard/app/FlipboardApplication;->o()V

    .line 166
    :goto_0
    return-void

    .line 164
    :cond_2
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity$3;->a:Lflipboard/activities/CacheConfigActivity;

    invoke-virtual {v0}, Lflipboard/activities/CacheConfigActivity;->finish()V

    goto :goto_0
.end method

.method public final c(Landroid/support/v4/app/DialogFragment;)V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lflipboard/activities/CacheConfigActivity$3;->a:Lflipboard/activities/CacheConfigActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflipboard/activities/CacheConfigActivity;->a(Lflipboard/activities/CacheConfigActivity;Z)Z

    .line 172
    return-void
.end method
