.class Lflipboard/activities/FlipboardActivity$9;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "FlipboardActivity.java"


# instance fields
.field final synthetic a:Lflipboard/activities/FlipboardActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/FlipboardActivity;)V
    .locals 0

    .prologue
    .line 1629
    iput-object p1, p0, Lflipboard/activities/FlipboardActivity$9;->a:Lflipboard/activities/FlipboardActivity;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1633
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-boolean v3, v0, Lflipboard/service/FlipboardManager;->ac:Z

    .line 1634
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->g()V

    .line 1635
    iget-object v0, p1, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    .line 1636
    const v1, 0x7f0a017a

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1637
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1638
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "should_show_data_use_dialog"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v1}, Lflipboard/util/AndroidUtil;->a(Landroid/content/SharedPreferences$Editor;)V

    iput-boolean v3, v0, Lflipboard/service/FlipboardManager;->ac:Z

    .line 1640
    :cond_0
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity$9;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->F()V

    .line 1641
    return-void
.end method

.method public final d(Landroid/support/v4/app/DialogFragment;)V
    .locals 1

    .prologue
    .line 1647
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity$9;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->finish()V

    .line 1648
    return-void
.end method
