.class Lflipboard/activities/ComposeFragment$10;
.super Ljava/lang/Object;
.source "ComposeFragment.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Lflipboard/gui/FLTextView;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lflipboard/activities/ComposeFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/ComposeFragment;Lflipboard/gui/FLTextView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1143
    iput-object p1, p0, Lflipboard/activities/ComposeFragment$10;->c:Lflipboard/activities/ComposeFragment;

    iput-object p2, p0, Lflipboard/activities/ComposeFragment$10;->a:Lflipboard/gui/FLTextView;

    iput-object p3, p0, Lflipboard/activities/ComposeFragment$10;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4

    .prologue
    .line 1151
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1152
    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1153
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1154
    iget-object v1, p0, Lflipboard/activities/ComposeFragment$10;->a:Lflipboard/gui/FLTextView;

    iget-object v2, p0, Lflipboard/activities/ComposeFragment$10;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1155
    iget-object v1, p0, Lflipboard/activities/ComposeFragment$10;->a:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1156
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1160
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1147
    return-void
.end method
