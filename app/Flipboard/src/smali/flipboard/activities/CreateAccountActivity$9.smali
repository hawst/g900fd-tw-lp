.class Lflipboard/activities/CreateAccountActivity$9;
.super Ljava/lang/Object;
.source "CreateAccountActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/activities/CreateAccountActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/CreateAccountActivity;)V
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 279
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    :goto_0
    return-void

    .line 283
    :cond_0
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 284
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity;->s:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 285
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity;->t:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 287
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    move-object v3, v1

    .line 290
    :cond_1
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    move-object v4, v1

    .line 294
    :cond_2
    new-instance v8, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v8}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 296
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    new-instance v5, Lflipboard/gui/dialog/FLProgressDialogFragment;

    invoke-direct {v5}, Lflipboard/gui/dialog/FLProgressDialogFragment;-><init>()V

    iput-object v5, v0, Lflipboard/activities/CreateAccountActivity;->C:Lflipboard/gui/dialog/FLProgressDialogFragment;

    .line 297
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity;->C:Lflipboard/gui/dialog/FLProgressDialogFragment;

    const v5, 0x7f0d0120

    invoke-virtual {v0, v5}, Lflipboard/gui/dialog/FLProgressDialogFragment;->f(I)V

    .line 298
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity;->C:Lflipboard/gui/dialog/FLProgressDialogFragment;

    new-instance v5, Lflipboard/activities/CreateAccountActivity$9$1;

    invoke-direct {v5, p0, v8}, Lflipboard/activities/CreateAccountActivity$9$1;-><init>(Lflipboard/activities/CreateAccountActivity$9;Ljava/util/concurrent/atomic/AtomicReference;)V

    iput-object v5, v0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 310
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity;->C:Lflipboard/gui/dialog/FLProgressDialogFragment;

    iget-object v5, p0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    iget-object v5, v5, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v6, "progress"

    invoke-virtual {v0, v5, v6}, Lflipboard/gui/dialog/FLProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 312
    new-instance v7, Lflipboard/activities/CreateAccountActivity$9$2;

    invoke-direct {v7, p0}, Lflipboard/activities/CreateAccountActivity$9$2;-><init>(Lflipboard/activities/CreateAccountActivity$9;)V

    .line 379
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity;->n:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v5, v0, v5

    .line 380
    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    iget-object v0, v0, Lflipboard/activities/CreateAccountActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v6, p0, Lflipboard/activities/CreateAccountActivity$9;->a:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v6}, Lflipboard/activities/CreateAccountActivity;->c(Lflipboard/activities/CreateAccountActivity;)Lflipboard/activities/LoginActivity$LoginInitFrom;

    move-result-object v6

    invoke-virtual {v6}, Lflipboard/activities/LoginActivity$LoginInitFrom;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v7}, Lflipboard/service/FlipboardManager;->createAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/util/Observer;)Lflipboard/service/Flap$Request;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
