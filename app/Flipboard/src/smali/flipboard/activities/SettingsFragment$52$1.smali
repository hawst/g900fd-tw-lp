.class Lflipboard/activities/SettingsFragment$52$1;
.super Ljava/lang/Object;
.source "SettingsFragment.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/activities/SettingsFragment$52;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment$52;)V
    .locals 0

    .prologue
    .line 1983
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$52$1;->a:Lflipboard/activities/SettingsFragment$52;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 2

    .prologue
    .line 1986
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$52$1;->a:Lflipboard/activities/SettingsFragment$52;

    iget-object v0, v0, Lflipboard/activities/SettingsFragment$52;->b:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    const-string v1, "Cleared frequency cap state for user"

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 1987
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1990
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$52$1;->a:Lflipboard/activities/SettingsFragment$52;

    iget-object v0, v0, Lflipboard/activities/SettingsFragment$52;->b:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    const-string v1, "Error clearing frequency cap state for user: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 1991
    return-void
.end method
