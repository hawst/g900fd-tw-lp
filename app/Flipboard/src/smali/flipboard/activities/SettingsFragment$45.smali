.class Lflipboard/activities/SettingsFragment$45;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SettingsFragment.java"


# instance fields
.field final synthetic a:[Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lflipboard/activities/SettingsFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1678
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$45;->c:Lflipboard/activities/SettingsFragment;

    iput-object p2, p0, Lflipboard/activities/SettingsFragment$45;->a:[Ljava/lang/String;

    iput-object p3, p0, Lflipboard/activities/SettingsFragment$45;->b:Ljava/lang/String;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;I)V
    .locals 4

    .prologue
    .line 1681
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$45;->a:[Ljava/lang/String;

    aget-object v0, v0, p2

    .line 1682
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->a()V

    .line 1683
    const-string v1, "Custom..."

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1685
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialog$Builder;

    iget-object v0, p0, Lflipboard/activities/SettingsFragment$45;->c:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v1, v0}, Lflipboard/gui/dialog/FLAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1686
    const-string v0, "Enter a locale:"

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1687
    new-instance v0, Landroid/widget/EditText;

    iget-object v2, p0, Lflipboard/activities/SettingsFragment$45;->c:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v2}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1688
    const-string v2, "en_US"

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1689
    iget-object v2, p0, Lflipboard/activities/SettingsFragment$45;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1690
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1691
    const-string v2, "OK"

    new-instance v3, Lflipboard/activities/SettingsFragment$45$1;

    invoke-direct {v3, p0, v0}, Lflipboard/activities/SettingsFragment$45$1;-><init>(Lflipboard/activities/SettingsFragment$45;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1708
    const-string v0, "Cancel"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1709
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$45;->c:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->a(Landroid/app/Dialog;)V

    .line 1720
    :goto_0
    return-void

    .line 1712
    :cond_0
    const-string v1, "System Provided"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1713
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$45;->c:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->d(Lflipboard/activities/SettingsFragment;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "locale_override"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1717
    :goto_1
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$45;->c:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->i(Lflipboard/activities/SettingsFragment;)Lflipboard/activities/SettingsFragment$SettingsAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->notifyDataSetChanged()V

    .line 1718
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$45;->c:Lflipboard/activities/SettingsFragment;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/activities/SettingsFragment;->o:Z

    goto :goto_0

    .line 1715
    :cond_1
    iget-object v1, p0, Lflipboard/activities/SettingsFragment$45;->c:Lflipboard/activities/SettingsFragment;

    invoke-static {v1}, Lflipboard/activities/SettingsFragment;->d(Lflipboard/activities/SettingsFragment;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "locale_override"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1
.end method
