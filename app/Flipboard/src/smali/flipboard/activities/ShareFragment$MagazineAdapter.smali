.class public Lflipboard/activities/ShareFragment$MagazineAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "ShareFragment.java"


# instance fields
.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Magazine;",
            ">;"
        }
    .end annotation
.end field

.field c:Lflipboard/objs/Magazine;

.field private d:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 875
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 876
    iput-object p1, p0, Lflipboard/activities/ShareFragment$MagazineAdapter;->d:Landroid/content/Context;

    .line 877
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 906
    mul-int/lit8 v1, p2, 0x6

    mul-int/lit8 v0, p2, 0x6

    add-int/lit8 v0, v0, 0x6

    iget-object v2, p0, Lflipboard/activities/ShareFragment$MagazineAdapter;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v0, v2, :cond_2

    iget-object v0, p0, Lflipboard/activities/ShareFragment$MagazineAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    iget-object v2, p0, Lflipboard/activities/ShareFragment$MagazineAdapter;->b:Ljava/util/List;

    mul-int/lit8 v3, p2, 0x6

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    iget-object v2, p0, Lflipboard/activities/ShareFragment$MagazineAdapter;->b:Ljava/util/List;

    invoke-interface {v2, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v7

    iget-object v0, p0, Lflipboard/activities/ShareFragment$MagazineAdapter;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030124

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    move v6, v1

    :goto_1
    const/4 v1, 0x2

    if-ge v6, v1, :cond_8

    if-nez v6, :cond_3

    const v1, 0x7f0a0323

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object v4, v1

    :goto_2
    const/4 v1, 0x0

    move v5, v1

    :goto_3
    const/4 v1, 0x3

    if-ge v5, v1, :cond_7

    mul-int/lit8 v1, v6, 0x3

    add-int/2addr v1, v5

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/Magazine;

    instance-of v2, v1, Lflipboard/activities/ShareFragment$CreateMagazine;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lflipboard/activities/ShareFragment$MagazineAdapter;->d:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030059

    const/4 v9, 0x0

    invoke-virtual {v2, v3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v2, v1}, Lflipboard/gui/MagazineThumbView;->setMagazine(Lflipboard/objs/Magazine;)V

    :cond_0
    :goto_4
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v9, -0x1

    invoke-direct {v1, v3, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v3, 0xf

    const/4 v9, -0x1

    invoke-virtual {v1, v3, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/16 v3, 0xb

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v3, p0, Lflipboard/activities/ShareFragment$MagazineAdapter;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v9, 0x7f09007d

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v2, v1}, Lflipboard/gui/MagazineThumbView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lflipboard/activities/ShareFragment$MagazineAdapter;->d:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v9, 0x0

    const/4 v10, -0x1

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-direct {v3, v9, v10, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_3

    :cond_2
    mul-int/lit8 v0, p2, 0x6

    add-int/lit8 v0, v0, 0x6

    goto/16 :goto_0

    :cond_3
    const/4 v1, 0x1

    if-ne v6, v1, :cond_4

    const v1, 0x7f0a0324

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object v4, v1

    goto/16 :goto_2

    :cond_4
    iget-object v1, p0, Lflipboard/activities/ShareFragment$MagazineAdapter;->d:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030123

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    add-int/lit8 v2, v6, 0x1

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    move-object v4, v1

    goto/16 :goto_2

    :cond_5
    iget-object v2, p0, Lflipboard/activities/ShareFragment$MagazineAdapter;->d:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03011d

    const/4 v9, 0x0

    invoke-virtual {v2, v3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/MagazineThumbView;

    new-instance v3, Lflipboard/activities/ShareFragment$MagazineAdapter$1;

    invoke-direct {v3, p0}, Lflipboard/activities/ShareFragment$MagazineAdapter$1;-><init>(Lflipboard/activities/ShareFragment$MagazineAdapter;)V

    invoke-virtual {v2, v3}, Lflipboard/gui/MagazineThumbView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v1}, Lflipboard/gui/MagazineThumbView;->setMagazine(Lflipboard/objs/Magazine;)V

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v3}, Lflipboard/service/User;->p()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v3}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v9, 0x7f0d0246

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v9, v1, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v2}, Lflipboard/gui/MagazineThumbView;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lflipboard/activities/ShareActivity;

    invoke-virtual {v3, v2}, Lflipboard/activities/ShareActivity;->selectMagazine(Landroid/view/View;)V

    :cond_6
    iget-object v3, p0, Lflipboard/activities/ShareFragment$MagazineAdapter;->c:Lflipboard/objs/Magazine;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lflipboard/activities/ShareFragment$MagazineAdapter;->c:Lflipboard/objs/Magazine;

    iget-object v3, v3, Lflipboard/objs/Magazine;->c:Ljava/lang/String;

    iget-object v1, v1, Lflipboard/objs/Magazine;->c:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Lflipboard/gui/MagazineThumbView;->setSelected(Z)V

    const/4 v1, 0x0

    iput-object v1, p0, Lflipboard/activities/ShareFragment$MagazineAdapter;->c:Lflipboard/objs/Magazine;

    iget-object v1, p0, Lflipboard/activities/ShareFragment$MagazineAdapter;->d:Landroid/content/Context;

    check-cast v1, Lflipboard/activities/ShareActivity;

    invoke-virtual {v1, v2}, Lflipboard/activities/ShareActivity;->selectMagazine(Landroid/view/View;)V

    goto/16 :goto_4

    :cond_7
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto/16 :goto_1

    .line 907
    :cond_8
    check-cast p1, Landroid/support/v4/view/ViewPager;

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    .line 908
    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 913
    check-cast p3, Landroid/view/View;

    .line 914
    check-cast p1, Landroid/support/v4/view/ViewPager;

    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 915
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 901
    check-cast p2, Landroid/view/View;

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 891
    const/4 v0, -0x2

    return v0
.end method

.method public final b(I)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 920
    iget-object v0, p0, Lflipboard/activities/ShareFragment$MagazineAdapter;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    add-int/lit8 v3, p1, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 896
    iget-object v0, p0, Lflipboard/activities/ShareFragment$MagazineAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40c00000    # 6.0f

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method
