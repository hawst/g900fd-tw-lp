.class Lflipboard/activities/ComposeFragment$9$1;
.super Ljava/lang/Object;
.source "ComposeFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/objs/SectionListResult;

.field final synthetic b:Lflipboard/activities/ComposeFragment$9;


# direct methods
.method constructor <init>(Lflipboard/activities/ComposeFragment$9;Lflipboard/objs/SectionListResult;)V
    .locals 0

    .prologue
    .line 783
    iput-object p1, p0, Lflipboard/activities/ComposeFragment$9$1;->b:Lflipboard/activities/ComposeFragment$9;

    iput-object p2, p0, Lflipboard/activities/ComposeFragment$9$1;->a:Lflipboard/objs/SectionListResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 788
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$9$1;->b:Lflipboard/activities/ComposeFragment$9;

    iget-object v0, v0, Lflipboard/activities/ComposeFragment$9;->a:Lflipboard/service/Account;

    iget-object v1, p0, Lflipboard/activities/ComposeFragment$9$1;->b:Lflipboard/activities/ComposeFragment$9;

    iget-object v1, v1, Lflipboard/activities/ComposeFragment$9;->c:Lflipboard/activities/ComposeFragment;

    invoke-static {v1}, Lflipboard/activities/ComposeFragment;->a(Lflipboard/activities/ComposeFragment;)Lflipboard/service/Account;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 831
    :cond_0
    :goto_0
    return-void

    .line 791
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$9$1;->b:Lflipboard/activities/ComposeFragment$9;

    iget-object v0, v0, Lflipboard/activities/ComposeFragment$9;->c:Lflipboard/activities/ComposeFragment;

    invoke-static {v0}, Lflipboard/activities/ComposeFragment;->j(Lflipboard/activities/ComposeFragment;)Lflipboard/service/Flap$ShareListRequest;

    .line 793
    new-instance v1, Lflipboard/json/FLObject;

    invoke-direct {v1}, Lflipboard/json/FLObject;-><init>()V

    .line 794
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$9$1;->a:Lflipboard/objs/SectionListResult;

    iget-object v0, v0, Lflipboard/objs/SectionListResult;->a:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/activities/ComposeFragment$9$1;->a:Lflipboard/objs/SectionListResult;

    iget-object v0, v0, Lflipboard/objs/SectionListResult;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 796
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$9$1;->b:Lflipboard/activities/ComposeFragment$9;

    iget-object v0, v0, Lflipboard/activities/ComposeFragment$9;->c:Lflipboard/activities/ComposeFragment;

    iget-object v2, p0, Lflipboard/activities/ComposeFragment$9$1;->b:Lflipboard/activities/ComposeFragment$9;

    iget-object v2, v2, Lflipboard/activities/ComposeFragment$9;->b:Lflipboard/objs/ConfigService;

    invoke-static {v0, v2}, Lflipboard/activities/ComposeFragment;->a(Lflipboard/activities/ComposeFragment;Lflipboard/objs/ConfigService;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 797
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$9$1;->a:Lflipboard/objs/SectionListResult;

    iget-object v0, v0, Lflipboard/objs/SectionListResult;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SectionListItem;

    .line 800
    iget-boolean v3, v0, Lflipboard/objs/SectionListItem;->i:Z

    if-eqz v3, :cond_2

    .line 801
    iget-object v3, v0, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    invoke-static {v3}, Lflipboard/util/JavaUtil;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lflipboard/objs/SectionListItem;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 802
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$9$1;->b:Lflipboard/activities/ComposeFragment$9;

    iget-object v0, v0, Lflipboard/activities/ComposeFragment$9;->b:Lflipboard/objs/ConfigService;

    iget-boolean v0, v0, Lflipboard/objs/ConfigService;->aw:Z

    if-nez v0, :cond_2

    .line 812
    :cond_3
    invoke-virtual {v1}, Lflipboard/json/FLObject;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 813
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$9$1;->a:Lflipboard/objs/SectionListResult;

    iget-object v0, v0, Lflipboard/objs/SectionListResult;->a:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SectionListItem;

    .line 814
    invoke-virtual {v0}, Lflipboard/objs/SectionListItem;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 815
    iget-object v2, v0, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    invoke-static {v2}, Lflipboard/util/JavaUtil;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lflipboard/objs/SectionListItem;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    :cond_4
    invoke-virtual {v1}, Lflipboard/json/FLObject;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 822
    invoke-static {}, Lflipboard/activities/ComposeFragment;->c()Lflipboard/util/Log;

    move-result-object v0

    const-string v2, "unable to fetch share targets for %s: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lflipboard/activities/ComposeFragment$9$1;->b:Lflipboard/activities/ComposeFragment$9;

    iget-object v4, v4, Lflipboard/activities/ComposeFragment$9;->b:Lflipboard/objs/ConfigService;

    invoke-virtual {v4}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x1

    iget-object v5, p0, Lflipboard/activities/ComposeFragment$9$1;->a:Lflipboard/objs/SectionListResult;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 823
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$9$1;->b:Lflipboard/activities/ComposeFragment$9;

    iget-object v0, v0, Lflipboard/activities/ComposeFragment$9;->c:Lflipboard/activities/ComposeFragment;

    invoke-static {v0}, Lflipboard/activities/ComposeFragment;->k(Lflipboard/activities/ComposeFragment;)Z

    .line 827
    :cond_5
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$9$1;->b:Lflipboard/activities/ComposeFragment$9;

    iget-object v0, v0, Lflipboard/activities/ComposeFragment$9;->c:Lflipboard/activities/ComposeFragment;

    invoke-virtual {v0}, Lflipboard/activities/ComposeFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 828
    if-eqz v0, :cond_0

    .line 829
    iget-object v2, p0, Lflipboard/activities/ComposeFragment$9$1;->b:Lflipboard/activities/ComposeFragment$9;

    iget-object v2, v2, Lflipboard/activities/ComposeFragment$9;->c:Lflipboard/activities/ComposeFragment;

    iget-object v3, p0, Lflipboard/activities/ComposeFragment$9$1;->b:Lflipboard/activities/ComposeFragment$9;

    iget-object v3, v3, Lflipboard/activities/ComposeFragment$9;->c:Lflipboard/activities/ComposeFragment;

    invoke-static {v3}, Lflipboard/activities/ComposeFragment;->a(Lflipboard/activities/ComposeFragment;)Lflipboard/service/Account;

    move-result-object v3

    invoke-virtual {v2, v0, v3, v1}, Lflipboard/activities/ComposeFragment;->a(Landroid/view/View;Lflipboard/service/Account;Lflipboard/json/FLObject;)V

    goto/16 :goto_0
.end method
