.class public Lflipboard/activities/SettingsDensityActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "SettingsDensityActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/activities/SettingsDensityActivity$Category;",
            ">;"
        }
    .end annotation
.end field

.field private o:Landroid/content/SharedPreferences;

.field private p:Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 303
    return-void
.end method

.method public static a(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 5

    .prologue
    .line 65
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0253

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 66
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0254

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 67
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d024f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 68
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0d0255

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 69
    if-nez p0, :cond_2

    .line 70
    if-eqz p1, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    .line 70
    goto :goto_0

    .line 71
    :cond_2
    sget-object v1, Lflipboard/service/Section;->c:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v0, v2

    .line 72
    goto :goto_0

    .line 73
    :cond_3
    sget-object v1, Lflipboard/service/Section;->d:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v0, v3

    .line 74
    goto :goto_0
.end method

.method static synthetic a(Lflipboard/activities/SettingsDensityActivity;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lflipboard/activities/SettingsDensityActivity;->j()V

    return-void
.end method

.method static synthetic b(Lflipboard/activities/SettingsDensityActivity;)Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity;->p:Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;

    return-object v0
.end method

.method static synthetic c(Lflipboard/activities/SettingsDensityActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity;->n:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lflipboard/activities/SettingsDensityActivity;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity;->o:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private j()V
    .locals 8

    .prologue
    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/activities/SettingsDensityActivity;->n:Ljava/util/ArrayList;

    .line 84
    new-instance v1, Lflipboard/activities/SettingsDensityActivity$Category;

    const v0, 0x7f0d0250

    invoke-virtual {p0, v0}, Lflipboard/activities/SettingsDensityActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lflipboard/activities/SettingsDensityActivity$Category;-><init>(Lflipboard/activities/SettingsDensityActivity;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->k()Lflipboard/objs/UserState;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    iget-object v0, v0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->i:Ljava/lang/String;

    .line 87
    new-instance v2, Lflipboard/activities/SettingsDensityActivity$1;

    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v3}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0251

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3, v0}, Lflipboard/activities/SettingsDensityActivity$1;-><init>(Lflipboard/activities/SettingsDensityActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    iget-object v0, v1, Lflipboard/activities/SettingsDensityActivity$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    invoke-virtual {p0}, Lflipboard/activities/SettingsDensityActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "sid"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    if-nez v0, :cond_0

    .line 103
    const-string v0, "auth/flipboard/coverstories"

    .line 105
    :cond_0
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2, v0}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    .line 106
    if-eqz v0, :cond_1

    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    if-eqz v2, :cond_1

    .line 107
    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->O:Ljava/lang/String;

    .line 108
    new-instance v3, Lflipboard/activities/SettingsDensityActivity$2;

    sget-object v4, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v4}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0252

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v0}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p0, v4, v2, v0}, Lflipboard/activities/SettingsDensityActivity$2;-><init>(Lflipboard/activities/SettingsDensityActivity;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Section;)V

    .line 120
    iget-object v0, v1, Lflipboard/activities/SettingsDensityActivity$Category;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    :cond_1
    return-void
.end method


# virtual methods
.method final a(Lflipboard/service/Section;Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 126
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 129
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0253

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 130
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0d0254

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 131
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0d024f

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 132
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v1}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0d0255

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 133
    if-eqz p1, :cond_0

    .line 134
    new-array v1, v10, [Ljava/lang/String;

    aput-object v5, v1, v6

    aput-object v4, v1, v7

    aput-object v0, v1, v8

    aput-object v3, v1, v9

    .line 135
    new-array v0, v10, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v3, v0, v6

    sget-object v3, Lflipboard/service/Section;->e:Ljava/lang/String;

    aput-object v3, v0, v7

    sget-object v3, Lflipboard/service/Section;->c:Ljava/lang/String;

    aput-object v3, v0, v8

    sget-object v3, Lflipboard/service/Section;->d:Ljava/lang/String;

    aput-object v3, v0, v9

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 140
    :goto_0
    invoke-interface {v0, p2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 141
    invoke-virtual {v2, v1, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a([Ljava/lang/String;I)V

    .line 142
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v4, 0x7f0d0250

    invoke-virtual {v1, v4}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 143
    new-instance v1, Lflipboard/activities/SettingsDensityActivity$3;

    invoke-direct {v1, p0, v3, p1, v0}, Lflipboard/activities/SettingsDensityActivity$3;-><init>(Lflipboard/activities/SettingsDensityActivity;ILflipboard/service/Section;Ljava/util/List;)V

    iput-object v1, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 163
    const-string v0, "display_options"

    invoke-virtual {v2, p0, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 164
    return-void

    .line 137
    :cond_0
    new-array v1, v9, [Ljava/lang/String;

    aput-object v4, v1, v6

    aput-object v0, v1, v7

    aput-object v3, v1, v8

    .line 138
    new-array v0, v9, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v3, v0, v6

    sget-object v3, Lflipboard/service/Section;->c:Ljava/lang/String;

    aput-object v3, v0, v7

    sget-object v3, Lflipboard/service/Section;->d:Ljava/lang/String;

    aput-object v3, v0, v8

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 371
    const-string v0, "settings_density"

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 362
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FlipboardActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 363
    const/16 v0, 0xa

    if-ne p2, v0, :cond_0

    .line 364
    invoke-direct {p0}, Lflipboard/activities/SettingsDensityActivity;->j()V

    .line 365
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity;->p:Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;

    invoke-virtual {v0}, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;->notifyDataSetChanged()V

    .line 367
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 45
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-virtual {p0}, Lflipboard/activities/SettingsDensityActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    :goto_0
    return-void

    .line 50
    :cond_0
    const v0, 0x7f03011a

    invoke-virtual {p0, v0}, Lflipboard/activities/SettingsDensityActivity;->setContentView(I)V

    .line 51
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/activities/SettingsDensityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 52
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->b:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 53
    const v0, 0x7f0a0310

    invoke-virtual {p0, v0}, Lflipboard/activities/SettingsDensityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    .line 54
    const v1, 0x7f0d02e0

    invoke-virtual {p0, v1}, Lflipboard/activities/SettingsDensityActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    const v0, 0x7f0a0144

    invoke-virtual {p0, v0}, Lflipboard/activities/SettingsDensityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 56
    const-string v1, "flipboard_settings"

    invoke-virtual {p0, v1, v2}, Lflipboard/activities/SettingsDensityActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lflipboard/activities/SettingsDensityActivity;->o:Landroid/content/SharedPreferences;

    .line 57
    invoke-direct {p0}, Lflipboard/activities/SettingsDensityActivity;->j()V

    .line 58
    new-instance v1, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;

    invoke-direct {v1, p0, v2}, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;-><init>(Lflipboard/activities/SettingsDensityActivity;B)V

    iput-object v1, p0, Lflipboard/activities/SettingsDensityActivity;->p:Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;

    .line 59
    iget-object v1, p0, Lflipboard/activities/SettingsDensityActivity;->p:Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 60
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity;->p:Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;

    invoke-virtual {v0, p3}, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;->a(I)Lflipboard/activities/SettingsDensityActivity$RowItem;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Lflipboard/activities/SettingsDensityActivity$RowItem;->b()V

    .line 169
    return-void
.end method
