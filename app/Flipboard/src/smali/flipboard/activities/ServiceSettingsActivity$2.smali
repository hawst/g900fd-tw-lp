.class Lflipboard/activities/ServiceSettingsActivity$2;
.super Ljava/lang/Object;
.source "ServiceSettingsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Ljava/lang/reflect/Field;

.field final synthetic b:Ljava/lang/Class;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lflipboard/gui/FLTextIntf;

.field final synthetic e:Lflipboard/activities/ServiceSettingsActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/ServiceSettingsActivity;Ljava/lang/reflect/Field;Ljava/lang/Class;Ljava/lang/String;Lflipboard/gui/FLTextIntf;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lflipboard/activities/ServiceSettingsActivity$2;->e:Lflipboard/activities/ServiceSettingsActivity;

    iput-object p2, p0, Lflipboard/activities/ServiceSettingsActivity$2;->a:Ljava/lang/reflect/Field;

    iput-object p3, p0, Lflipboard/activities/ServiceSettingsActivity$2;->b:Ljava/lang/Class;

    iput-object p4, p0, Lflipboard/activities/ServiceSettingsActivity$2;->c:Ljava/lang/String;

    iput-object p5, p0, Lflipboard/activities/ServiceSettingsActivity$2;->d:Lflipboard/gui/FLTextIntf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 141
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialog$Builder;

    iget-object v1, p0, Lflipboard/activities/ServiceSettingsActivity$2;->e:Lflipboard/activities/ServiceSettingsActivity;

    invoke-direct {v2, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 144
    iget-object v1, p0, Lflipboard/activities/ServiceSettingsActivity$2;->e:Lflipboard/activities/ServiceSettingsActivity;

    iget-object v3, p0, Lflipboard/activities/ServiceSettingsActivity$2;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lflipboard/activities/ServiceSettingsActivity;->a(Lflipboard/activities/ServiceSettingsActivity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 146
    iget-object v1, p0, Lflipboard/activities/ServiceSettingsActivity$2;->e:Lflipboard/activities/ServiceSettingsActivity;

    iget-object v1, p0, Lflipboard/activities/ServiceSettingsActivity$2;->a:Ljava/lang/reflect/Field;

    invoke-static {v1}, Lflipboard/activities/ServiceSettingsActivity;->a(Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v3

    .line 149
    iget-object v1, p0, Lflipboard/activities/ServiceSettingsActivity$2;->b:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v4

    .line 152
    array-length v1, v4

    new-array v5, v1, [Ljava/lang/String;

    move v1, v0

    .line 154
    :goto_0
    array-length v6, v4

    if-ge v0, v6, :cond_1

    .line 155
    aget-object v6, v4, v0

    if-ne v6, v3, :cond_0

    move v1, v0

    .line 158
    :cond_0
    iget-object v6, p0, Lflipboard/activities/ServiceSettingsActivity$2;->e:Lflipboard/activities/ServiceSettingsActivity;

    iget-object v7, p0, Lflipboard/activities/ServiceSettingsActivity$2;->b:Ljava/lang/Class;

    aget-object v8, v4, v0

    invoke-static {v6, v7, v8}, Lflipboard/activities/ServiceSettingsActivity;->a(Lflipboard/activities/ServiceSettingsActivity;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    .line 154
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 161
    :cond_1
    new-instance v0, Lflipboard/activities/ServiceSettingsActivity$2$1;

    invoke-direct {v0, p0, v4}, Lflipboard/activities/ServiceSettingsActivity$2$1;-><init>(Lflipboard/activities/ServiceSettingsActivity$2;[Ljava/lang/Object;)V

    invoke-virtual {v2, v5, v1, v0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 172
    iget-object v0, p0, Lflipboard/activities/ServiceSettingsActivity$2;->e:Lflipboard/activities/ServiceSettingsActivity;

    invoke-virtual {v0, v2}, Lflipboard/activities/ServiceSettingsActivity;->a(Landroid/app/AlertDialog$Builder;)Landroid/app/AlertDialog;

    .line 173
    return-void
.end method
