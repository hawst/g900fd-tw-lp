.class Lflipboard/activities/FacebookAuthenticateFragment$5;
.super Ljava/lang/Object;
.source "FacebookAuthenticateFragment.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/FlipboardManager;",
        "Lflipboard/service/Section;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/activities/FacebookAuthenticateFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/FacebookAuthenticateFragment;)V
    .locals 0

    .prologue
    .line 469
    iput-object p1, p0, Lflipboard/activities/FacebookAuthenticateFragment$5;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lflipboard/activities/FacebookAuthenticateFragment$5;Lflipboard/activities/FlipboardActivity;)V
    .locals 3

    .prologue
    .line 469
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment$5;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-static {v0}, Lflipboard/activities/FacebookAuthenticateFragment;->e(Lflipboard/activities/FacebookAuthenticateFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "source"

    const-string v2, "serviceSignIn"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "serviceIdentifier"

    const-string v2, "facebook"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/activities/FacebookAuthenticateFragment$5;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-static {v1}, Lflipboard/activities/FacebookAuthenticateFragment;->f(Lflipboard/activities/FacebookAuthenticateFragment;)Lflipboard/service/Section;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment$5;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-static {v0}, Lflipboard/activities/FacebookAuthenticateFragment;->d(Lflipboard/activities/FacebookAuthenticateFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lflipboard/activities/FlipboardActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    const-string v0, "loading"

    invoke-static {p1, v0}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 469
    check-cast p1, Lflipboard/service/FlipboardManager;

    check-cast p2, Lflipboard/service/Section;

    new-instance v0, Lflipboard/activities/FacebookAuthenticateFragment$5$1;

    invoke-direct {v0, p0, p2, p1}, Lflipboard/activities/FacebookAuthenticateFragment$5$1;-><init>(Lflipboard/activities/FacebookAuthenticateFragment$5;Lflipboard/service/Section;Lflipboard/service/FlipboardManager;)V

    invoke-virtual {p1, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    return-void
.end method
