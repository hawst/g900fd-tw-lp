.class Lflipboard/activities/ChooseAccountActivity$2$1;
.super Ljava/lang/Object;
.source "ChooseAccountActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/objs/SectionListResult;

.field final synthetic b:Lflipboard/activities/ChooseAccountActivity$2;


# direct methods
.method constructor <init>(Lflipboard/activities/ChooseAccountActivity$2;Lflipboard/objs/SectionListResult;)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->b:Lflipboard/activities/ChooseAccountActivity$2;

    iput-object p2, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->a:Lflipboard/objs/SectionListResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 218
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->a:Lflipboard/objs/SectionListResult;

    iget-object v0, v0, Lflipboard/objs/SectionListResult;->a:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 221
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->b:Lflipboard/activities/ChooseAccountActivity$2;

    iget-object v0, v0, Lflipboard/activities/ChooseAccountActivity$2;->e:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v0}, Lflipboard/activities/ChooseAccountActivity;->b(Lflipboard/activities/ChooseAccountActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->b:Lflipboard/activities/ChooseAccountActivity$2;

    iget-object v2, v2, Lflipboard/activities/ChooseAccountActivity$2;->e:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v2}, Lflipboard/activities/ChooseAccountActivity;->c(Lflipboard/activities/ChooseAccountActivity;)Lflipboard/json/FLObject;

    move-result-object v2

    invoke-virtual {v2}, Lflipboard/json/FLObject;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 223
    iget-object v2, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->b:Lflipboard/activities/ChooseAccountActivity$2;

    iget-object v2, v2, Lflipboard/activities/ChooseAccountActivity$2;->e:Lflipboard/activities/ChooseAccountActivity;

    iget-object v3, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->a:Lflipboard/objs/SectionListResult;

    iget-object v3, v3, Lflipboard/objs/SectionListResult;->a:Ljava/util/List;

    invoke-static {v2, v3, v0}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/activities/ChooseAccountActivity;Ljava/util/List;Ljava/util/Set;)V

    .line 226
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 227
    iget-object v3, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->b:Lflipboard/activities/ChooseAccountActivity$2;

    iget-object v3, v3, Lflipboard/activities/ChooseAccountActivity$2;->e:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v3}, Lflipboard/activities/ChooseAccountActivity;->c(Lflipboard/activities/ChooseAccountActivity;)Lflipboard/json/FLObject;

    move-result-object v3

    invoke-virtual {v3, v0}, Lflipboard/json/FLObject;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 232
    :cond_0
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->b:Lflipboard/activities/ChooseAccountActivity$2;

    iget-object v2, v0, Lflipboard/activities/ChooseAccountActivity$2;->e:Lflipboard/activities/ChooseAccountActivity;

    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->a:Lflipboard/objs/SectionListResult;

    iget-object v3, v0, Lflipboard/objs/SectionListResult;->a:Ljava/util/List;

    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->b:Lflipboard/activities/ChooseAccountActivity$2;

    iget-object v4, v0, Lflipboard/activities/ChooseAccountActivity$2;->a:Lflipboard/activities/ChooseAccountActivity$Adapter;

    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->b:Lflipboard/activities/ChooseAccountActivity$2;

    iget-object v5, v0, Lflipboard/activities/ChooseAccountActivity$2;->b:Lflipboard/objs/ConfigService;

    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->b:Lflipboard/activities/ChooseAccountActivity$2;

    iget-object v0, v0, Lflipboard/activities/ChooseAccountActivity$2;->e:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v0}, Lflipboard/activities/ChooseAccountActivity;->b(Lflipboard/activities/ChooseAccountActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v2, v3, v4, v5, v0}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/activities/ChooseAccountActivity;Ljava/util/List;Lflipboard/activities/ChooseAccountActivity$Adapter;Lflipboard/objs/ConfigService;Z)V

    .line 236
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->b:Lflipboard/activities/ChooseAccountActivity$2;

    iget-object v0, v0, Lflipboard/activities/ChooseAccountActivity$2;->e:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v0}, Lflipboard/activities/ChooseAccountActivity;->b(Lflipboard/activities/ChooseAccountActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 237
    :goto_2
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->b:Lflipboard/activities/ChooseAccountActivity$2;

    iget-object v0, v0, Lflipboard/activities/ChooseAccountActivity$2;->a:Lflipboard/activities/ChooseAccountActivity$Adapter;

    invoke-virtual {v0}, Lflipboard/activities/ChooseAccountActivity$Adapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 238
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->b:Lflipboard/activities/ChooseAccountActivity$2;

    iget-object v0, v0, Lflipboard/activities/ChooseAccountActivity$2;->a:Lflipboard/activities/ChooseAccountActivity$Adapter;

    invoke-virtual {v0, v1}, Lflipboard/activities/ChooseAccountActivity$Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;

    .line 239
    iget-object v2, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->d:Lflipboard/objs/SectionListItem;

    if-eqz v2, :cond_3

    .line 240
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->b:Lflipboard/activities/ChooseAccountActivity$2;

    iget-object v1, v1, Lflipboard/activities/ChooseAccountActivity$2;->e:Lflipboard/activities/ChooseAccountActivity;

    iget-object v0, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->d:Lflipboard/objs/SectionListItem;

    iget-object v2, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->b:Lflipboard/activities/ChooseAccountActivity$2;

    iget-object v2, v2, Lflipboard/activities/ChooseAccountActivity$2;->b:Lflipboard/objs/ConfigService;

    invoke-static {v1, v0, v2}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/activities/ChooseAccountActivity;Lflipboard/objs/SectionListItem;Lflipboard/objs/ConfigService;)Z

    .line 248
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->b:Lflipboard/activities/ChooseAccountActivity$2;

    iget-object v0, v0, Lflipboard/activities/ChooseAccountActivity$2;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 249
    iget-object v0, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->b:Lflipboard/activities/ChooseAccountActivity$2;

    iget-object v0, v0, Lflipboard/activities/ChooseAccountActivity$2;->d:Lflipboard/gui/FLTextIntf;

    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity$2$1;->b:Lflipboard/activities/ChooseAccountActivity$2;

    iget-object v1, v1, Lflipboard/activities/ChooseAccountActivity$2;->e:Lflipboard/activities/ChooseAccountActivity;

    invoke-virtual {v1}, Lflipboard/activities/ChooseAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0223

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 250
    return-void

    :cond_2
    move v0, v1

    .line 232
    goto :goto_1

    .line 237
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method
