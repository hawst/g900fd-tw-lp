.class Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$4;
.super Ljava/lang/Object;
.source "ContentDrawerTabletActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;


# direct methods
.method constructor <init>(Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;I)V
    .locals 0

    .prologue
    .line 640
    iput-object p1, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$4;->b:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iput p2, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$4;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 644
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$4;->b:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget-object v0, v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->i(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    move-result-object v0

    iget v0, v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->b:I

    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$4;->b:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget-object v1, v1, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v1}, Lflipboard/activities/ContentDrawerTabletActivity;->j(Lflipboard/activities/ContentDrawerTabletActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$4;->b:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget-object v0, v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->i(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    move-result-object v0

    iget v0, v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->b:I

    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$4;->b:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget-object v1, v1, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v1}, Lflipboard/activities/ContentDrawerTabletActivity;->j(Lflipboard/activities/ContentDrawerTabletActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 645
    :cond_0
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$4;->b:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget-object v0, v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->j(Lflipboard/activities/ContentDrawerTabletActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$4;->b:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget-object v1, v1, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v1}, Lflipboard/activities/ContentDrawerTabletActivity;->i(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    move-result-object v1

    iget v1, v1, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 647
    :cond_1
    iget v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$4;->a:I

    if-lez v0, :cond_2

    .line 648
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$4;->b:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget-object v0, v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->a(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/EditableListView;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$4;->b:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget-object v1, v1, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v1}, Lflipboard/activities/ContentDrawerTabletActivity;->a(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/EditableListView;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/gui/EditableListView;->getCount()I

    move-result v1

    iget v2, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$4;->a:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lflipboard/gui/EditableListView;->setSelection(I)V

    .line 650
    :cond_2
    return-void
.end method
