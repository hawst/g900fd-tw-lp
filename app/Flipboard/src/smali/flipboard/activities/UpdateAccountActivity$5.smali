.class Lflipboard/activities/UpdateAccountActivity$5;
.super Ljava/lang/Object;
.source "UpdateAccountActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/activities/UpdateAccountActivity;

.field private b:Lflipboard/gui/dialog/FLProgressDialog;


# direct methods
.method constructor <init>(Lflipboard/activities/UpdateAccountActivity;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lflipboard/activities/UpdateAccountActivity$5;)Lflipboard/gui/dialog/FLProgressDialog;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity$5;->b:Lflipboard/gui/dialog/FLProgressDialog;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    .line 145
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    iget-object v0, v0, Lflipboard/activities/UpdateAccountActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    invoke-static {v0}, Lflipboard/activities/UpdateAccountActivity;->d(Lflipboard/activities/UpdateAccountActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 149
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    invoke-static {v0}, Lflipboard/activities/UpdateAccountActivity;->e(Lflipboard/activities/UpdateAccountActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 150
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    invoke-static {v0}, Lflipboard/activities/UpdateAccountActivity;->f(Lflipboard/activities/UpdateAccountActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 153
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    iget-object v0, v0, Lflipboard/activities/UpdateAccountActivity;->n:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v5, v0, v1

    .line 155
    new-instance v6, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v6}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 157
    new-instance v0, Lflipboard/gui/dialog/FLProgressDialog;

    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    const v7, 0x7f0d033c

    invoke-direct {v0, v1, v7}, Lflipboard/gui/dialog/FLProgressDialog;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lflipboard/activities/UpdateAccountActivity$5;->b:Lflipboard/gui/dialog/FLProgressDialog;

    .line 158
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity$5;->b:Lflipboard/gui/dialog/FLProgressDialog;

    new-instance v1, Lflipboard/activities/UpdateAccountActivity$5$1;

    invoke-direct {v1, p0, v6}, Lflipboard/activities/UpdateAccountActivity$5$1;-><init>(Lflipboard/activities/UpdateAccountActivity$5;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 168
    iget-object v0, p0, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity$5;->b:Lflipboard/gui/dialog/FLProgressDialog;

    invoke-virtual {v0, v1}, Lflipboard/activities/UpdateAccountActivity;->a(Landroid/app/Dialog;)V

    .line 170
    new-instance v0, Lflipboard/activities/UpdateAccountActivity$5$2;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lflipboard/activities/UpdateAccountActivity$5$2;-><init>(Lflipboard/activities/UpdateAccountActivity$5;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    iget-object v1, p0, Lflipboard/activities/UpdateAccountActivity$5;->a:Lflipboard/activities/UpdateAccountActivity;

    iget-object v1, v1, Lflipboard/activities/UpdateAccountActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v7, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v8, Lflipboard/service/FlipboardManager$16;

    invoke-direct {v8, v1, v0}, Lflipboard/service/FlipboardManager$16;-><init>(Lflipboard/service/FlipboardManager;Lflipboard/util/Observer;)V

    new-instance v0, Lflipboard/service/Flap$UpdateAccountRequest;

    invoke-direct {v0, v1, v7}, Lflipboard/service/Flap$UpdateAccountRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    iput-object v8, v0, Lflipboard/service/Flap$AccountRequest;->a:Lflipboard/service/Flap$AccountRequestObserver;

    iput-object v3, v0, Lflipboard/service/Flap$UpdateAccountRequest;->g:Ljava/lang/String;

    iput-object v5, v0, Lflipboard/service/Flap$UpdateAccountRequest;->h:Ljava/lang/String;

    iput-object v4, v0, Lflipboard/service/Flap$UpdateAccountRequest;->i:Ljava/lang/String;

    iput-object v2, v0, Lflipboard/service/Flap$UpdateAccountRequest;->j:Ljava/lang/String;

    invoke-virtual {v0}, Lflipboard/service/Flap$UpdateAccountRequest;->c()V

    invoke-virtual {v6, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
