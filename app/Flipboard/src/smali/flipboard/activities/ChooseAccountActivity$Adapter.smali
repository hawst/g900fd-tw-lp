.class Lflipboard/activities/ChooseAccountActivity$Adapter;
.super Landroid/widget/ArrayAdapter;
.source "ChooseAccountActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lflipboard/activities/ChooseAccountActivity$AccountItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/activities/ChooseAccountActivity;

.field private b:Lflipboard/activities/ChooseAccountActivity$AccountItem;


# direct methods
.method constructor <init>(Lflipboard/activities/ChooseAccountActivity;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lflipboard/activities/ChooseAccountActivity$AccountItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 416
    iput-object p1, p0, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    .line 417
    invoke-direct {p0, p2, v0, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    .line 418
    return-void
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 423
    invoke-virtual {p0, p1}, Lflipboard/activities/ChooseAccountActivity$Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;

    .line 424
    invoke-virtual {v0}, Lflipboard/activities/ChooseAccountActivity$AccountItem;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    .line 432
    invoke-virtual {p0}, Lflipboard/activities/ChooseAccountActivity$Adapter;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    invoke-virtual {p0}, Lflipboard/activities/ChooseAccountActivity$Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 619
    :cond_0
    :goto_0
    return-object p2

    .line 436
    :cond_1
    invoke-virtual {p0}, Lflipboard/activities/ChooseAccountActivity$Adapter;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 437
    invoke-virtual {p0, p1}, Lflipboard/activities/ChooseAccountActivity$Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;

    .line 439
    if-nez p2, :cond_2

    .line 440
    invoke-virtual {v0}, Lflipboard/activities/ChooseAccountActivity$AccountItem;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 441
    const v1, 0x7f030119

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 447
    :cond_2
    :goto_1
    invoke-virtual {v0}, Lflipboard/activities/ChooseAccountActivity$AccountItem;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 448
    const v1, 0x7f0a004f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iget-object v0, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->e:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/JavaUtil;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 443
    :cond_3
    const v1, 0x7f03002f

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_1

    .line 452
    :cond_4
    const v1, 0x7f0a00be

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    .line 453
    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 454
    iget-object v4, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->d:Lflipboard/objs/SectionListItem;

    if-eqz v4, :cond_6

    iget-object v4, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->d:Lflipboard/objs/SectionListItem;

    iget-boolean v4, v4, Lflipboard/objs/ContentDrawerListItemBase;->bS:Z

    if-eqz v4, :cond_6

    .line 455
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09010f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 456
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f09010f

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 462
    :goto_2
    const v2, 0x7f0a00c0

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLTextIntf;

    .line 463
    const v3, 0x7f0a00c1

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lflipboard/gui/FLTextIntf;

    .line 464
    const v4, 0x7f0a00c3

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lflipboard/gui/FLTextIntf;

    .line 465
    const v5, 0x7f0a00c4

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lflipboard/gui/FLTextIntf;

    .line 466
    const v6, 0x7f0a00c2

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lflipboard/gui/FLImageView;

    .line 467
    const v7, 0x7f0a0056

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CompoundButton;

    .line 470
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 472
    iget-object v8, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->d:Lflipboard/objs/SectionListItem;

    if-eqz v8, :cond_9

    .line 474
    iget-object v8, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->d:Lflipboard/objs/SectionListItem;

    iget-object v8, v8, Lflipboard/objs/SectionListItem;->t:Ljava/lang/String;

    invoke-virtual {v1, v8}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 475
    iget-object v1, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->d:Lflipboard/objs/SectionListItem;

    invoke-virtual {v1}, Lflipboard/objs/SectionListItem;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 476
    iget-object v1, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->d:Lflipboard/objs/SectionListItem;

    invoke-virtual {v1}, Lflipboard/objs/SectionListItem;->p()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 477
    iget-object v1, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->d:Lflipboard/objs/SectionListItem;

    invoke-virtual {v1}, Lflipboard/objs/SectionListItem;->o()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 478
    iget-object v1, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->d:Lflipboard/objs/SectionListItem;

    iget-boolean v1, v1, Lflipboard/objs/ContentDrawerListItemSection;->u:Z

    if-eqz v1, :cond_7

    iget-object v1, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->d:Lflipboard/objs/SectionListItem;

    iget-object v1, v1, Lflipboard/objs/ContentDrawerListItemSection;->o:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 479
    iget-object v1, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->b:Lflipboard/objs/ConfigService;

    invoke-virtual {v1}, Lflipboard/objs/ConfigService;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 480
    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 485
    :goto_3
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Landroid/widget/CompoundButton;->setVisibility(I)V

    .line 487
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    iget-object v2, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->d:Lflipboard/objs/SectionListItem;

    iget-object v2, v2, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    invoke-static {v2}, Lflipboard/util/JavaUtil;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/activities/ChooseAccountActivity;Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v7, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 488
    iget-object v1, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->b:Lflipboard/objs/ConfigService;

    iget-boolean v1, v1, Lflipboard/objs/ConfigService;->aw:Z

    if-eqz v1, :cond_8

    const v1, 0x7f020076

    :goto_4
    invoke-virtual {v7, v1}, Landroid/widget/CompoundButton;->setButtonDrawable(I)V

    .line 489
    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Landroid/widget/CompoundButton;->setFocusable(Z)V

    .line 490
    const/4 v1, 0x0

    invoke-interface {v5, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 491
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 534
    :cond_5
    :goto_5
    invoke-interface {v4}, Lflipboard/gui/FLTextIntf;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 535
    const/4 v1, 0x0

    invoke-interface {v4, v1}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 539
    :goto_6
    invoke-interface {v5}, Lflipboard/gui/FLTextIntf;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 540
    const/4 v1, 0x0

    invoke-interface {v5, v1}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 544
    :goto_7
    invoke-interface {v3}, Lflipboard/gui/FLTextIntf;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 545
    const/4 v1, 0x0

    invoke-interface {v3, v1}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 551
    :goto_8
    new-instance v1, Lflipboard/activities/ChooseAccountActivity$Adapter$1;

    invoke-direct {v1, p0, v0}, Lflipboard/activities/ChooseAccountActivity$Adapter$1;-><init>(Lflipboard/activities/ChooseAccountActivity$Adapter;Lflipboard/activities/ChooseAccountActivity$AccountItem;)V

    invoke-virtual {v7, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 458
    :cond_6
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900b7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 459
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0900b7

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_2

    .line 482
    :cond_7
    const/16 v1, 0x8

    invoke-virtual {v6, v1}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 488
    :cond_8
    const v1, 0x7f0201da

    goto :goto_4

    .line 493
    :cond_9
    iget-object v8, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->c:Lflipboard/service/Account;

    if-eqz v8, :cond_10

    .line 495
    iget-object v8, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->c:Lflipboard/service/Account;

    iget-object v8, v8, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v8}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 496
    iget-object v1, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->c:Lflipboard/service/Account;

    iget-object v1, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    if-eqz v1, :cond_b

    iget-object v1, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->c:Lflipboard/service/Account;

    iget-object v1, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    :goto_9
    invoke-interface {v2, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 497
    const/4 v1, 0x0

    invoke-interface {v3, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 498
    iget-object v1, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->b:Lflipboard/objs/ConfigService;

    invoke-virtual {v1}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 499
    const/16 v1, 0x8

    invoke-virtual {v6, v1}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 501
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v1}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/activities/ChooseAccountActivity;)Lflipboard/service/Account;

    move-result-object v1

    iget-object v2, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->c:Lflipboard/service/Account;

    if-ne v1, v2, :cond_d

    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v1}, Lflipboard/activities/ChooseAccountActivity;->b(Lflipboard/activities/ChooseAccountActivity;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 503
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v1}, Lflipboard/activities/ChooseAccountActivity;->c(Lflipboard/activities/ChooseAccountActivity;)Lflipboard/json/FLObject;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/json/FLObject;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_c

    .line 505
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v1}, Lflipboard/activities/ChooseAccountActivity;->c(Lflipboard/activities/ChooseAccountActivity;)Lflipboard/json/FLObject;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/json/FLObject;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 506
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 517
    :cond_a
    :goto_a
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Landroid/widget/CompoundButton;->setVisibility(I)V

    .line 519
    iget-object v1, p0, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v1}, Lflipboard/activities/ChooseAccountActivity;->a(Lflipboard/activities/ChooseAccountActivity;)Lflipboard/service/Account;

    move-result-object v1

    iget-object v2, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->c:Lflipboard/service/Account;

    if-ne v1, v2, :cond_e

    const/4 v1, 0x1

    :goto_b
    invoke-virtual {v7, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 520
    const v1, 0x7f0201da

    invoke-virtual {v7, v1}, Landroid/widget/CompoundButton;->setButtonDrawable(I)V

    .line 522
    iget-object v1, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->b:Lflipboard/objs/ConfigService;

    iget-boolean v1, v1, Lflipboard/objs/ConfigService;->av:Z

    if-nez v1, :cond_f

    const/4 v1, 0x1

    :goto_c
    invoke-virtual {v7, v1}, Landroid/widget/CompoundButton;->setFocusable(Z)V

    .line 523
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_5

    .line 496
    :cond_b
    iget-object v1, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->c:Lflipboard/service/Account;

    iget-object v1, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    goto/16 :goto_9

    .line 511
    :cond_c
    const-string v1, "%d %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v8, p0, Lflipboard/activities/ChooseAccountActivity$Adapter;->a:Lflipboard/activities/ChooseAccountActivity;

    invoke-static {v8}, Lflipboard/activities/ChooseAccountActivity;->c(Lflipboard/activities/ChooseAccountActivity;)Lflipboard/json/FLObject;

    move-result-object v8

    invoke-virtual {v8}, Lflipboard/json/FLObject;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v2, v6

    const/4 v6, 0x1

    iget-object v8, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->b:Lflipboard/objs/ConfigService;

    invoke-virtual {v8}, Lflipboard/objs/ConfigService;->i()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v6

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto :goto_a

    .line 514
    :cond_d
    const/4 v1, 0x0

    invoke-interface {v5, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto :goto_a

    .line 519
    :cond_e
    const/4 v1, 0x0

    goto :goto_b

    .line 522
    :cond_f
    const/4 v1, 0x0

    goto :goto_c

    .line 524
    :cond_10
    iget-object v6, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->a:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    sget-object v8, Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;->d:Lflipboard/activities/ChooseAccountActivity$AccountItem$Type;

    if-ne v6, v8, :cond_5

    .line 526
    iget-object v6, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->b:Lflipboard/objs/ConfigService;

    invoke-virtual {v6}, Lflipboard/objs/ConfigService;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 527
    iget-object v1, v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;->b:Lflipboard/objs/ConfigService;

    invoke-virtual {v1}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 528
    const/4 v1, 0x0

    invoke-interface {v4, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 529
    const/4 v1, 0x0

    invoke-interface {v5, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 530
    const/16 v1, 0x8

    invoke-virtual {v7, v1}, Landroid/widget/CompoundButton;->setVisibility(I)V

    .line 531
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_5

    .line 537
    :cond_11
    const/16 v1, 0x8

    invoke-interface {v4, v1}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    goto/16 :goto_6

    .line 542
    :cond_12
    const/16 v1, 0x8

    invoke-interface {v5, v1}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    goto/16 :goto_7

    .line 547
    :cond_13
    const/16 v1, 0x8

    invoke-interface {v3, v1}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    goto/16 :goto_8
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 420
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 624
    invoke-virtual {p0}, Lflipboard/activities/ChooseAccountActivity$Adapter;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    invoke-virtual {p0}, Lflipboard/activities/ChooseAccountActivity$Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 633
    :goto_0
    return v0

    .line 628
    :cond_1
    invoke-virtual {p0, p1}, Lflipboard/activities/ChooseAccountActivity$Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/ChooseAccountActivity$AccountItem;

    .line 629
    iget-object v2, p0, Lflipboard/activities/ChooseAccountActivity$Adapter;->b:Lflipboard/activities/ChooseAccountActivity$AccountItem;

    if-eq v0, v2, :cond_2

    invoke-virtual {v0}, Lflipboard/activities/ChooseAccountActivity$AccountItem;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v1

    .line 631
    goto :goto_0

    .line 633
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
