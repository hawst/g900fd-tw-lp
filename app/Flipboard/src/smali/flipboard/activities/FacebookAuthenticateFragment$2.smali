.class Lflipboard/activities/FacebookAuthenticateFragment$2;
.super Ljava/lang/Object;
.source "FacebookAuthenticateFragment.java"

# interfaces
.implements Lflipboard/service/Flap$TypedResultObserver;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/service/Flap$TypedResultObserver",
        "<",
        "Lflipboard/objs/UserInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/activities/FacebookAuthenticateFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/FacebookAuthenticateFragment;)V
    .locals 0

    .prologue
    .line 333
    iput-object p1, p0, Lflipboard/activities/FacebookAuthenticateFragment$2;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 333
    check-cast p1, Lflipboard/objs/UserInfo;

    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment$2;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-static {v0}, Lflipboard/activities/FacebookAuthenticateFragment;->a(Lflipboard/activities/FacebookAuthenticateFragment;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-boolean v0, p1, Lflipboard/objs/UserInfo;->b:Z

    if-eqz v0, :cond_5

    iget-object v1, p0, Lflipboard/activities/FacebookAuthenticateFragment$2;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-virtual {v1}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setResult(I)V

    iget-object v0, v1, Lflipboard/activities/FacebookAuthenticateFragment;->e:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    sget-object v2, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->c:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    if-eq v0, v2, :cond_0

    iget-object v0, v1, Lflipboard/activities/FacebookAuthenticateFragment;->e:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    sget-object v2, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->d:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    if-ne v0, v2, :cond_4

    :cond_0
    iget-object v0, v1, Lflipboard/activities/FacebookAuthenticateFragment;->a:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    iget-object v3, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    const-string v4, "facebook"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lflipboard/service/Section;->d(Z)Z

    goto :goto_0

    :cond_2
    iget-object v0, v1, Lflipboard/activities/FacebookAuthenticateFragment;->a:Lflipboard/service/FlipboardManager;

    const-string v2, "facebook"

    const-string v3, "usageSocialLogin"

    new-instance v4, Lflipboard/activities/FacebookAuthenticateFragment$5;

    invoke-direct {v4, v1}, Lflipboard/activities/FacebookAuthenticateFragment$5;-><init>(Lflipboard/activities/FacebookAuthenticateFragment;)V

    invoke-virtual {v0, v2, p1, v3, v4}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/objs/UserInfo;Ljava/lang/String;Lflipboard/util/Observer;)Lflipboard/service/Section;

    :cond_3
    :goto_1
    return-void

    :cond_4
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/activities/FacebookAuthenticateFragment$4;

    invoke-direct {v2, v1}, Lflipboard/activities/FacebookAuthenticateFragment$4;-><init>(Lflipboard/activities/FacebookAuthenticateFragment;)V

    invoke-virtual {v0, p1, v2}, Lflipboard/service/FlipboardManager;->a(Lflipboard/objs/UserInfo;Lflipboard/util/Observer;)V

    goto :goto_1

    :cond_5
    invoke-static {}, Lcom/facebook/Session;->getActiveSession()Lcom/facebook/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/Session;->close()V

    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment$2;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-virtual {v0}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    if-eqz v0, :cond_3

    iget-boolean v1, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_3

    const-string v1, "loading"

    invoke-static {v0, v1}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/activities/FacebookAuthenticateFragment$2;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-static {v1}, Lflipboard/activities/FacebookAuthenticateFragment;->b(Lflipboard/activities/FacebookAuthenticateFragment;)Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    move-result-object v1

    sget-object v2, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->b:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    invoke-virtual {v1, v2}, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const v1, 0x7f0d0165

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_2
    iget-object v2, p1, Lflipboard/objs/UserInfo;->g:Ljava/lang/String;

    if-nez v2, :cond_6

    const v2, 0x7f0d0164

    invoke-virtual {v0, v2}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_6
    invoke-static {v0, v1, v2, v3}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    :cond_7
    iget-object v1, p0, Lflipboard/activities/FacebookAuthenticateFragment$2;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-static {v1}, Lflipboard/activities/FacebookAuthenticateFragment;->b(Lflipboard/activities/FacebookAuthenticateFragment;)Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    move-result-object v1

    sget-object v2, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->a:Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;

    invoke-virtual {v1, v2}, Lflipboard/activities/FacebookAuthenticateFragment$FragmentAction;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const v1, 0x7f0d00be

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_8
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 365
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment$2;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-static {v0}, Lflipboard/activities/FacebookAuthenticateFragment;->a(Lflipboard/activities/FacebookAuthenticateFragment;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 366
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment$2;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-virtual {v0}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    const-string v1, "loading"

    invoke-static {v0, v1}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 367
    const-string v0, "418"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 368
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment$2;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-virtual {v0}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-static {v0}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;)V

    .line 378
    :cond_0
    :goto_0
    return-void

    .line 371
    :cond_1
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment$2;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-virtual {v0}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 372
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_0

    .line 373
    const v1, 0x7f0d0164

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 374
    const v2, 0x7f0d0165

    invoke-virtual {v0, v2}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 375
    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method
