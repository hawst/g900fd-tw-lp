.class public Lflipboard/activities/ContentDrawerTabletActivity;
.super Lflipboard/activities/ContentDrawerActivity;
.source "ContentDrawerTabletActivity.java"


# instance fields
.field private final A:Lflipboard/activities/FlipboardFragment;

.field private final B:Lflipboard/activities/FlipboardFragment;

.field private C:Landroid/view/View;

.field private s:Lflipboard/gui/EditableListView;

.field private t:Landroid/widget/FrameLayout;

.field private u:Lflipboard/gui/ContentDrawerListItemAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/gui/ContentDrawerListItemAdapter",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

.field private w:Landroid/widget/ListView;

.field private x:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/ContentDrawerHandler;",
            "Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private y:Lflipboard/gui/FLBusyView;

.field private z:Lflipboard/gui/FLTextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lflipboard/activities/ContentDrawerActivity;-><init>()V

    .line 68
    invoke-static {}, Lflipboard/util/ActivityUtil;->a()Lflipboard/activities/FlipboardFragment;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->A:Lflipboard/activities/FlipboardFragment;

    .line 69
    new-instance v0, Lflipboard/gui/personal/TocGridFragment;

    invoke-direct {v0}, Lflipboard/gui/personal/TocGridFragment;-><init>()V

    iput-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->B:Lflipboard/activities/FlipboardFragment;

    .line 339
    return-void
.end method

.method static synthetic a(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/EditableListView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->s:Lflipboard/gui/EditableListView;

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/ContentDrawerTabletActivity;Lflipboard/util/Observer;)Lflipboard/util/Observer;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lflipboard/activities/ContentDrawerTabletActivity;->x:Lflipboard/util/Observer;

    return-object p1
.end method

.method static synthetic a(Lflipboard/activities/ContentDrawerTabletActivity;Lflipboard/service/Section;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerTabletActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity;->u:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-static {v0, p1, v1, p2}, Lflipboard/gui/ContentDrawerView;->a(Landroid/content/res/Resources;Lflipboard/service/Section;Lflipboard/gui/ContentDrawerListItemAdapter;Ljava/util/List;)V

    return-void
.end method

.method private static a(Lflipboard/gui/EditableListView;Lflipboard/gui/FLButton;)V
    .locals 1

    .prologue
    .line 197
    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 198
    iget-boolean v0, p0, Lflipboard/gui/EditableListView;->a:Z

    if-eqz v0, :cond_1

    .line 199
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/EditableListView;->setEditing(Z)V

    .line 200
    const v0, 0x7f0d00cb

    invoke-virtual {p1, v0}, Lflipboard/gui/FLButton;->setText(I)V

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflipboard/gui/EditableListView;->setEditing(Z)V

    .line 203
    const v0, 0x7f0d00c9

    invoke-virtual {p1, v0}, Lflipboard/gui/FLButton;->setText(I)V

    goto :goto_0
.end method

.method static synthetic b(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/ContentDrawerListItemAdapter;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->u:Lflipboard/gui/ContentDrawerListItemAdapter;

    return-object v0
.end method

.method private b(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 237
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 238
    if-eqz v0, :cond_0

    .line 239
    const-string v1, "open_in_account"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 241
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    invoke-virtual {v0, v3, v2}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a(II)V

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    const-string v1, "open_in_favorites"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 243
    const-string v1, "favorites_position"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 244
    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity;->v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    const-string v2, "favorites_position"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v1, v3, v0}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a(II)V

    goto :goto_0

    .line 246
    :cond_2
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    invoke-virtual {v0, v3, v2}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a(II)V

    goto :goto_0

    .line 248
    :cond_3
    const-string v1, "open_in_notifications"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a(II)V

    goto :goto_0
.end method

.method static synthetic c(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/util/Observer;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->x:Lflipboard/util/Observer;

    return-object v0
.end method

.method static synthetic d(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/FLBusyView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->y:Lflipboard/gui/FLBusyView;

    return-object v0
.end method

.method static synthetic e(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/FLTextView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->z:Lflipboard/gui/FLTextView;

    return-object v0
.end method

.method static synthetic f(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/activities/FlipboardFragment;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->A:Lflipboard/activities/FlipboardFragment;

    return-object v0
.end method

.method static synthetic g(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/activities/FlipboardFragment;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->B:Lflipboard/activities/FlipboardFragment;

    return-object v0
.end method

.method static synthetic h(Lflipboard/activities/ContentDrawerTabletActivity;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->t:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic i(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    return-object v0
.end method

.method static synthetic j(Lflipboard/activities/ContentDrawerTabletActivity;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->w:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic k(Lflipboard/activities/ContentDrawerTabletActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->C:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 294
    invoke-super {p0, p1}, Lflipboard/activities/ContentDrawerActivity;->b(Ljava/lang/String;)V

    .line 295
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->u:Lflipboard/gui/ContentDrawerListItemAdapter;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->F()Lflipboard/service/ContentDrawerHandler;

    move-result-object v1

    iget-object v1, v1, Lflipboard/service/ContentDrawerHandler;->g:Lflipboard/objs/ConfigServices;

    invoke-virtual {v1}, Lflipboard/objs/ConfigServices;->c()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(Ljava/util/List;)V

    .line 296
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->s:Lflipboard/gui/EditableListView;

    new-instance v1, Lflipboard/activities/ContentDrawerTabletActivity$2;

    invoke-direct {v1, p0}, Lflipboard/activities/ContentDrawerTabletActivity$2;-><init>(Lflipboard/activities/ContentDrawerTabletActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/EditableListView;->post(Ljava/lang/Runnable;)Z

    .line 303
    return-void
.end method

.method protected final f()V
    .locals 2

    .prologue
    .line 174
    const v0, 0x7f040012

    const v1, 0x7f04001b

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/ContentDrawerTabletActivity;->overridePendingTransition(II)V

    .line 175
    return-void
.end method

.method protected final k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity;->v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget v1, v1, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->b:I

    invoke-virtual {v0, v1}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 219
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerTabletActivity;->finish()V

    .line 220
    const v0, 0x7f040012

    const v1, 0x7f04001b

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/ContentDrawerTabletActivity;->overridePendingTransition(II)V

    .line 221
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 164
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/ContentDrawerActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 166
    const/16 v0, 0x3039

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 167
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerTabletActivity;->w()V

    .line 169
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 307
    .line 308
    iget-object v2, p0, Lflipboard/activities/ContentDrawerTabletActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v2}, Lflipboard/gui/FLViewFlipper;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 309
    iget-object v2, p0, Lflipboard/activities/ContentDrawerTabletActivity;->s:Lflipboard/gui/EditableListView;

    iget-boolean v2, v2, Lflipboard/gui/EditableListView;->a:Z

    if-eqz v2, :cond_3

    .line 310
    iget-object v2, p0, Lflipboard/activities/ContentDrawerTabletActivity;->s:Lflipboard/gui/EditableListView;

    invoke-virtual {v2, v1}, Lflipboard/gui/EditableListView;->setEditing(Z)V

    .line 321
    :goto_0
    if-nez v0, :cond_0

    .line 322
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerTabletActivity;->m()V

    .line 324
    :cond_0
    return-void

    .line 315
    :cond_1
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "social_card"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 316
    if-eqz v1, :cond_2

    .line 317
    iget-object v2, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/FragmentTransaction;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 319
    :cond_2
    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v1}, Lflipboard/gui/FLViewFlipper;->b()V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public onClickOutside(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 179
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerTabletActivity;->m()V

    .line 180
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 75
    const v0, 0x7f030056

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerTabletActivity;->setContentView(I)V

    .line 77
    invoke-super {p0, p1}, Lflipboard/activities/ContentDrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 79
    const v0, 0x7f0a013f

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerTabletActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/EditableListView;

    iput-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->s:Lflipboard/gui/EditableListView;

    .line 80
    new-instance v0, Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-direct {v0, p0}, Lflipboard/gui/ContentDrawerListItemAdapter;-><init>(Lflipboard/activities/FlipboardActivity;)V

    iput-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->u:Lflipboard/gui/ContentDrawerListItemAdapter;

    .line 81
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->s:Lflipboard/gui/EditableListView;

    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity;->u:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v0, v1}, Lflipboard/gui/EditableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 82
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->s:Lflipboard/gui/EditableListView;

    invoke-virtual {v0, p0}, Lflipboard/gui/EditableListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 83
    const v0, 0x7f0a00ba

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerTabletActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->C:Landroid/view/View;

    .line 84
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->s:Lflipboard/gui/EditableListView;

    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity;->C:Landroid/view/View;

    invoke-virtual {v0, v1}, Lflipboard/gui/EditableListView;->setEmptyView(Landroid/view/View;)V

    .line 85
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->C:Landroid/view/View;

    const v1, 0x7f0a00bb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLBusyView;

    iput-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->y:Lflipboard/gui/FLBusyView;

    .line 86
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->C:Landroid/view/View;

    const v1, 0x7f0a00bc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->z:Lflipboard/gui/FLTextView;

    .line 87
    const v0, 0x7f0a0140

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerTabletActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->t:Landroid/widget/FrameLayout;

    .line 89
    const v0, 0x7f0a013b

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerTabletActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->w:Landroid/widget/ListView;

    .line 90
    new-instance v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    invoke-direct {v0, p0}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;-><init>(Lflipboard/activities/ContentDrawerTabletActivity;)V

    iput-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    .line 91
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->w:Landroid/widget/ListView;

    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity;->v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 92
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->w:Landroid/widget/ListView;

    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity;->v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 94
    const v0, 0x7f0a00ed

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerTabletActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLViewFlipper;

    iput-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->p:Lflipboard/gui/FLViewFlipper;

    .line 96
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->s:Lflipboard/gui/EditableListView;

    new-instance v1, Lflipboard/activities/ContentDrawerTabletActivity$1;

    invoke-direct {v1, p0}, Lflipboard/activities/ContentDrawerTabletActivity$1;-><init>(Lflipboard/activities/ContentDrawerTabletActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/EditableListView;->setDragListener(Lflipboard/gui/EditableListView$DragListener;)V

    .line 131
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "social_card"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 132
    const v0, 0x7f030074

    invoke-static {p0, v0, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 133
    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 134
    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 135
    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLViewFlipper;->addView(Landroid/view/View;)V

    .line 136
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->showNext()V

    .line 138
    :cond_0
    invoke-virtual {p0, p1}, Lflipboard/activities/ContentDrawerTabletActivity;->a(Landroid/os/Bundle;)V

    .line 139
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerTabletActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/activities/ContentDrawerTabletActivity;->b(Landroid/content/Intent;)V

    .line 140
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 691
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->x:Lflipboard/util/Observer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->F()Lflipboard/service/ContentDrawerHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 692
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->F()Lflipboard/service/ContentDrawerHandler;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity;->x:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/ContentDrawerHandler;->a(Lflipboard/util/Observer;)V

    .line 694
    :cond_0
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    if-eqz v0, :cond_1

    .line 695
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget-object v1, v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->d:Lflipboard/util/Observer;

    if-eqz v1, :cond_1

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->d:Lflipboard/util/Observer;

    invoke-virtual {v1, v2}, Lflipboard/service/User;->c(Lflipboard/util/Observer;)V

    const/4 v1, 0x0

    iput-object v1, v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->d:Lflipboard/util/Observer;

    .line 697
    :cond_1
    invoke-super {p0}, Lflipboard/activities/ContentDrawerActivity;->onDestroy()V

    .line 698
    return-void
.end method

.method public onEditClicked(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 184
    check-cast p1, Lflipboard/gui/FLButton;

    .line 185
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->s:Lflipboard/gui/EditableListView;

    invoke-static {v0, p1}, Lflipboard/activities/ContentDrawerTabletActivity;->a(Lflipboard/gui/EditableListView;Lflipboard/gui/FLButton;)V

    .line 186
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ContentDrawerView;

    .line 187
    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    .line 188
    invoke-static {v0, p1}, Lflipboard/activities/ContentDrawerTabletActivity;->a(Lflipboard/gui/EditableListView;Lflipboard/gui/FLButton;)V

    .line 189
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 231
    invoke-super {p0, p1}, Lflipboard/activities/ContentDrawerActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 232
    invoke-direct {p0, p1}, Lflipboard/activities/ContentDrawerTabletActivity;->b(Landroid/content/Intent;)V

    .line 233
    return-void
.end method

.method public onRemoveButtonClicked(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 211
    invoke-super {p0, p1}, Lflipboard/activities/ContentDrawerActivity;->onRemoveButtonClicked(Landroid/view/View;)V

    .line 212
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    .line 213
    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity;->u:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v1, v0}, Lflipboard/gui/ContentDrawerListItemAdapter;->b(Lflipboard/objs/ContentDrawerListItem;)I

    .line 214
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 155
    invoke-super {p0, p1}, Lflipboard/activities/ContentDrawerActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 156
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    const-string v1, "state_selected_category_regular"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->c:I

    .line 157
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->s:Lflipboard/gui/EditableListView;

    const-string v1, "state_editing"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/EditableListView;->setEditing(Z)V

    .line 158
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    const-string v1, "state_selected_category"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a(II)V

    .line 159
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 146
    invoke-super {p0, p1}, Lflipboard/activities/ContentDrawerActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 147
    const-string v0, "state_selected_category"

    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity;->v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget v1, v1, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 148
    const-string v0, "state_selected_category_regular"

    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity;->v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget v1, v1, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 149
    const-string v0, "state_editing"

    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity;->s:Lflipboard/gui/EditableListView;

    iget-boolean v1, v1, Lflipboard/gui/EditableListView;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 150
    return-void
.end method

.method protected final r_()Z
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity;->v:Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    iget v0, v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
