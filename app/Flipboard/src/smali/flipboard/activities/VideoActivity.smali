.class public Lflipboard/activities/VideoActivity;
.super Lflipboard/activities/FeedActivity;
.source "VideoActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final n:Lflipboard/util/Log;

.field public static final o:Lflipboard/util/Log;

.field static v:Landroid/util/DisplayMetrics;


# instance fields
.field private A:Z

.field private ac:Z

.field private ad:J

.field private ae:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private af:Z

.field p:Landroid/net/Uri;

.field q:Landroid/widget/RelativeLayout;

.field r:I

.field s:I

.field t:J

.field u:Landroid/webkit/WebView;

.field w:Z

.field private x:Lflipboard/io/UsageEvent;

.field private y:Lflipboard/gui/FLVideoView;

.field private z:Landroid/widget/MediaController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-string v0, "video"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/activities/VideoActivity;->n:Lflipboard/util/Log;

    .line 51
    const-string v0, "usage"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/activities/VideoActivity;->o:Lflipboard/util/Log;

    .line 61
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    sput-object v0, Lflipboard/activities/VideoActivity;->v:Landroid/util/DisplayMetrics;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lflipboard/activities/FeedActivity;-><init>()V

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/VideoActivity;->ac:Z

    .line 606
    return-void
.end method

.method private I()I
    .locals 3

    .prologue
    .line 307
    invoke-virtual {p0}, Lflipboard/activities/VideoActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 308
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 310
    const/4 v0, 0x3

    .line 318
    :goto_0
    return v0

    .line 312
    :cond_0
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 313
    const/4 v0, 0x1

    goto :goto_0

    .line 315
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/activities/VideoActivity;)Landroid/widget/MediaController;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->z:Landroid/widget/MediaController;

    return-object v0
.end method

.method static synthetic b(Lflipboard/activities/VideoActivity;)Lflipboard/gui/FLVideoView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    return-object v0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 162
    sget-object v0, Lflipboard/activities/VideoActivity;->o:Lflipboard/util/Log;

    .line 163
    invoke-virtual {p0}, Lflipboard/activities/VideoActivity;->finish()V

    .line 164
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 8

    .prologue
    .line 168
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 170
    iget-wide v0, p0, Lflipboard/activities/VideoActivity;->V:J

    .line 171
    iget-wide v4, p0, Lflipboard/activities/VideoActivity;->R:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 172
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lflipboard/activities/VideoActivity;->R:J

    sub-long/2addr v4, v6

    add-long/2addr v0, v4

    .line 174
    :cond_0
    const-string v3, "extra_result_active_time"

    iget-wide v4, p0, Lflipboard/activities/VideoActivity;->ad:J

    add-long/2addr v0, v4

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 175
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v2}, Lflipboard/activities/VideoActivity;->setResult(ILandroid/content/Intent;)V

    .line 176
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->finish()V

    .line 177
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 656
    const-string v0, "item"

    return-object v0
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 564
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FeedActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 567
    if-eqz p3, :cond_0

    .line 568
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 569
    if-eqz v0, :cond_0

    .line 570
    iget-wide v2, p0, Lflipboard/activities/VideoActivity;->ad:J

    const-string v1, "extra_result_active_time"

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lflipboard/activities/VideoActivity;->ad:J

    .line 573
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 525
    sget-object v0, Lflipboard/activities/VideoActivity;->o:Lflipboard/util/Log;

    .line 526
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onBackPressed()V

    .line 527
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->z:Landroid/widget/MediaController;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/activities/VideoActivity;->P:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflipboard/activities/VideoActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->z:Landroid/widget/MediaController;

    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, Landroid/widget/MediaController;->show(I)V

    goto :goto_0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2

    .prologue
    .line 119
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 120
    if-eqz v0, :cond_0

    iget-object v1, p0, Lflipboard/activities/VideoActivity;->ae:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    if-eqz v1, :cond_0

    .line 121
    iget-object v1, p0, Lflipboard/activities/VideoActivity;->ae:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 123
    :cond_0
    invoke-direct {p0}, Lflipboard/activities/VideoActivity;->j()V

    .line 124
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    .line 382
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 383
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lflipboard/activities/FlipboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 384
    if-eqz v0, :cond_0

    .line 385
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 386
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 388
    :cond_0
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->u:Landroid/webkit/WebView;

    if-eqz v0, :cond_1

    .line 389
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->u:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->requestLayout()V

    .line 390
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->u:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->invalidate()V

    .line 395
    :cond_1
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    if-eqz v0, :cond_2

    .line 396
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    iget v1, p0, Lflipboard/activities/VideoActivity;->r:I

    iget v2, p0, Lflipboard/activities/VideoActivity;->s:I

    invoke-direct {p0}, Lflipboard/activities/VideoActivity;->I()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/gui/FLVideoView;->a(III)V

    .line 397
    :cond_2
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 73
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "viewed"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/activities/VideoActivity;->x:Lflipboard/io/UsageEvent;

    .line 74
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->x:Lflipboard/io/UsageEvent;

    const-string v1, "itemType"

    const-string v2, "video"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 75
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->x:Lflipboard/io/UsageEvent;

    const-string v1, "itemFlipCount"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 76
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->x:Lflipboard/io/UsageEvent;

    const-string v1, "deprecated"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 78
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/activities/VideoActivity;->t:J

    .line 80
    invoke-virtual {p0, v3}, Lflipboard/activities/VideoActivity;->requestWindowFeature(I)Z

    .line 81
    invoke-super {p0, p1}, Lflipboard/activities/FeedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 82
    iput-boolean v3, p0, Lflipboard/activities/VideoActivity;->w:Z

    .line 84
    invoke-virtual {p0}, Lflipboard/activities/VideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 85
    if-eqz v0, :cond_1

    .line 86
    const-string v1, "fromSection"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lflipboard/activities/VideoActivity;->af:Z

    .line 87
    iget-boolean v1, p0, Lflipboard/activities/VideoActivity;->af:Z

    if-eqz v1, :cond_1

    .line 88
    const-string v1, "extra_content_discovery_from_source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 89
    const/4 v0, 0x0

    .line 90
    if-eqz v1, :cond_0

    .line 91
    const-string v0, "source"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 94
    :cond_0
    iget-object v1, p0, Lflipboard/activities/VideoActivity;->D:Lflipboard/objs/FeedItem;

    iget-object v2, p0, Lflipboard/activities/VideoActivity;->C:Lflipboard/service/Section;

    invoke-static {v1, v2, v0}, Lflipboard/activities/DetailActivity;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Ljava/lang/String;)V

    .line 99
    :cond_1
    return-void
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 323
    packed-switch p1, :pswitch_data_0

    .line 336
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 325
    :pswitch_0
    new-instance v0, Lflipboard/gui/dialog/FLProgressDialog;

    const v1, 0x7f0d01e6

    invoke-direct {v0, p0, v1}, Lflipboard/gui/dialog/FLProgressDialog;-><init>(Landroid/app/Activity;I)V

    .line 326
    new-instance v1, Lflipboard/activities/VideoActivity$1;

    invoke-direct {v1, p0}, Lflipboard/activities/VideoActivity$1;-><init>(Lflipboard/activities/VideoActivity;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    .line 323
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 12

    .prologue
    const-wide v10, 0x408f400000000000L    # 1000.0

    const/4 v8, 0x0

    const/4 v2, 0x1

    .line 531
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->z:Landroid/widget/MediaController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->z:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 532
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->z:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    .line 536
    :cond_0
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->u:Landroid/webkit/WebView;

    if-eqz v0, :cond_1

    .line 537
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->u:Landroid/webkit/WebView;

    const-string v1, ""

    const-string v3, "text/html"

    const-string v4, "utf-8"

    invoke-virtual {v0, v1, v3, v4}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v4, p0, Lflipboard/activities/VideoActivity;->t:J

    sub-long/2addr v0, v4

    long-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 541
    sget-object v1, Lflipboard/activities/VideoActivity;->o:Lflipboard/util/Log;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v8

    .line 542
    sget-object v1, Lflipboard/activities/VideoActivity;->o:Lflipboard/util/Log;

    new-array v1, v2, [Ljava/lang/Object;

    iget-wide v4, p0, Lflipboard/activities/VideoActivity;->V:J

    long-to-double v4, v4

    div-double/2addr v4, v10

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v8

    .line 543
    sget-object v1, Lflipboard/activities/VideoActivity;->o:Lflipboard/util/Log;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lflipboard/activities/VideoActivity;->R:J

    sub-long/2addr v4, v6

    long-to-double v4, v4

    div-double/2addr v4, v10

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v8

    .line 545
    iget-object v1, p0, Lflipboard/activities/VideoActivity;->x:Lflipboard/io/UsageEvent;

    if-eqz v1, :cond_3

    .line 546
    iget-object v1, p0, Lflipboard/activities/VideoActivity;->p:Landroid/net/Uri;

    if-eqz v1, :cond_2

    .line 547
    iget-object v1, p0, Lflipboard/activities/VideoActivity;->x:Lflipboard/io/UsageEvent;

    const-string v3, "sourceURL"

    iget-object v4, p0, Lflipboard/activities/VideoActivity;->p:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 549
    :cond_2
    iget-object v1, p0, Lflipboard/activities/VideoActivity;->x:Lflipboard/io/UsageEvent;

    iget-wide v4, p0, Lflipboard/activities/VideoActivity;->V:J

    iput-wide v4, v1, Lflipboard/io/UsageEvent;->g:J

    .line 550
    iget-object v1, p0, Lflipboard/activities/VideoActivity;->x:Lflipboard/io/UsageEvent;

    const-string v3, "totalDuration"

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 552
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->x:Lflipboard/io/UsageEvent;

    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 555
    :cond_3
    iget-boolean v0, p0, Lflipboard/activities/VideoActivity;->af:Z

    if-eqz v0, :cond_4

    .line 556
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->D:Lflipboard/objs/FeedItem;

    iget-object v1, p0, Lflipboard/activities/VideoActivity;->C:Lflipboard/service/Section;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lflipboard/activities/VideoActivity;->t:J

    sub-long/2addr v4, v6

    const/4 v6, 0x0

    move v3, v2

    invoke-static/range {v0 .. v6}, Lflipboard/activities/DetailActivity;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;IIJLjava/lang/String;)V

    .line 558
    :cond_4
    iput-boolean v8, p0, Lflipboard/activities/VideoActivity;->w:Z

    .line 559
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onDestroy()V

    .line 560
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 128
    sget-object v0, Lflipboard/activities/VideoActivity;->n:Lflipboard/util/Log;

    const-string v1, "Error playing video: %d, %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    invoke-direct {p0}, Lflipboard/activities/VideoActivity;->j()V

    .line 130
    return v5
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 499
    sget-object v0, Lflipboard/activities/VideoActivity;->o:Lflipboard/util/Log;

    .line 501
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 502
    if-eqz v0, :cond_0

    iget-object v1, p0, Lflipboard/activities/VideoActivity;->ae:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    if-eqz v1, :cond_0

    .line 503
    iget-object v1, p0, Lflipboard/activities/VideoActivity;->ae:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 506
    :cond_0
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->M:Lflipboard/service/FlipboardManager;

    const-string v1, "VideoActivity:onPause"

    new-instance v2, Lflipboard/activities/VideoActivity$5;

    invoke-direct {v2, p0}, Lflipboard/activities/VideoActivity$5;-><init>(Lflipboard/activities/VideoActivity;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 515
    sget-boolean v0, Lflipboard/service/FlipboardManager;->n:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->u:Landroid/webkit/WebView;

    if-eqz v0, :cond_1

    .line 518
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->u:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onPause()V

    .line 520
    :cond_1
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onPause()V

    .line 521
    return-void
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 135
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v0

    iput v0, p0, Lflipboard/activities/VideoActivity;->s:I

    .line 136
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v0

    iput v0, p0, Lflipboard/activities/VideoActivity;->r:I

    .line 137
    iget v0, p0, Lflipboard/activities/VideoActivity;->s:I

    if-lez v0, :cond_0

    iget v0, p0, Lflipboard/activities/VideoActivity;->r:I

    if-lez v0, :cond_0

    .line 138
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoActivity;->removeDialog(I)V

    .line 139
    :cond_0
    invoke-virtual {p1, p0}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 140
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->z:Landroid/widget/MediaController;

    invoke-virtual {v0, v2}, Landroid/widget/MediaController;->setEnabled(Z)V

    .line 141
    invoke-virtual {p1, v2}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 142
    invoke-virtual {p0}, Lflipboard/activities/VideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "should_loop"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    invoke-virtual {p1, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 145
    :cond_1
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    if-eqz v0, :cond_2

    .line 146
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    iget v1, p0, Lflipboard/activities/VideoActivity;->r:I

    iget v2, p0, Lflipboard/activities/VideoActivity;->s:I

    invoke-direct {p0}, Lflipboard/activities/VideoActivity;->I()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/gui/FLVideoView;->a(III)V

    .line 147
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    invoke-virtual {v0}, Lflipboard/gui/FLVideoView;->start()V

    .line 149
    :cond_2
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/16 v2, 0x400

    .line 216
    sget-object v0, Lflipboard/activities/VideoActivity;->o:Lflipboard/util/Log;

    .line 217
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onResume()V

    .line 219
    iget-boolean v0, p0, Lflipboard/activities/VideoActivity;->ac:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lflipboard/activities/VideoActivity;->A:Z

    if-nez v0, :cond_0

    .line 220
    invoke-virtual {p0}, Lflipboard/activities/VideoActivity;->finish()V

    .line 222
    :cond_0
    sget-boolean v0, Lflipboard/service/FlipboardManager;->n:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->u:Landroid/webkit/WebView;

    if-eqz v0, :cond_1

    .line 224
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->u:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onResume()V

    .line 226
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/VideoActivity;->ac:Z

    .line 227
    invoke-virtual {p0}, Lflipboard/activities/VideoActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 228
    invoke-virtual {p0}, Lflipboard/activities/VideoActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 230
    return-void
.end method

.method public onStart()V
    .locals 11

    .prologue
    const/16 v10, 0xa

    const/4 v2, 0x0

    .line 182
    invoke-super {p0}, Lflipboard/activities/FeedActivity;->onStart()V

    .line 184
    invoke-virtual {p0}, Lflipboard/activities/VideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 185
    if-eqz v1, :cond_3

    .line 186
    const-string v0, "uri"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 187
    const-string v3, "videoEmbedHTML"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 188
    const-string v3, "usageVideoType"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 189
    const-string v3, "partnerID"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 190
    const-string v3, "sid"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 193
    const-string v3, "width"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    const-string v3, "height"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 194
    const-string v3, "width"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 195
    const-string v8, "height"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 198
    :goto_0
    iget-object v8, p0, Lflipboard/activities/VideoActivity;->x:Lflipboard/io/UsageEvent;

    const-string v9, "partnerID"

    invoke-virtual {v8, v9, v6}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 199
    iget-object v6, p0, Lflipboard/activities/VideoActivity;->x:Lflipboard/io/UsageEvent;

    const-string v8, "videoType"

    invoke-virtual {v6, v8, v5}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 200
    iget-object v5, p0, Lflipboard/activities/VideoActivity;->x:Lflipboard/io/UsageEvent;

    const-string v6, "sectionIdentifier"

    invoke-virtual {v5, v6, v7}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 202
    if-eqz v0, :cond_3

    .line 203
    const-string v5, "url"

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 204
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 206
    iget-object v6, p0, Lflipboard/activities/VideoActivity;->p:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 207
    iput-object v5, p0, Lflipboard/activities/VideoActivity;->p:Landroid/net/Uri;

    .line 208
    iget-object v5, p0, Lflipboard/activities/VideoActivity;->p:Landroid/net/Uri;

    new-instance v6, Lflipboard/json/FLObject;

    invoke-direct {v6}, Lflipboard/json/FLObject;-><init>()V

    const-string v7, "sourceURL"

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v4, :cond_4

    invoke-virtual {v5}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v7

    const-string v8, "youku.com"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string v0, "Video Play"

    invoke-static {v0, v6}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;Ljava/util/Map;)V

    :try_start_0
    invoke-virtual {p0}, Lflipboard/activities/VideoActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    sget-object v2, Lflipboard/activities/VideoActivity;->v:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoActivity;->showDialog(I)V

    new-instance v0, Lflipboard/gui/FLWebView;

    invoke-direct {v0, p0}, Lflipboard/gui/FLWebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/activities/VideoActivity;->u:Landroid/webkit/WebView;

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->u:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->u:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    sget-object v2, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->u:Landroid/webkit/WebView;

    new-instance v2, Lflipboard/activities/VideoActivity$WClient;

    invoke-direct {v2, p0}, Lflipboard/activities/VideoActivity$WClient;-><init>(Lflipboard/activities/VideoActivity;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->u:Landroid/webkit/WebView;

    new-instance v2, Lflipboard/activities/VideoActivity$WChromeClient;

    invoke-direct {v2, p0}, Lflipboard/activities/VideoActivity$WChromeClient;-><init>(Lflipboard/activities/VideoActivity;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    const v0, 0x7f030154

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoActivity;->setContentView(I)V

    const v0, 0x7f0a0393

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lflipboard/activities/VideoActivity;->q:Landroid/widget/RelativeLayout;

    sget-object v0, Lflipboard/activities/VideoActivity;->v:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    sget-object v2, Lflipboard/activities/VideoActivity;->v:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    const/high16 v2, 0x43200000    # 160.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    if-eqz v3, :cond_0

    if-nez v1, :cond_1

    :cond_0
    const/16 v3, 0x280

    const/16 v1, 0x1e0

    :cond_1
    mul-int/2addr v0, v1

    div-int v2, v0, v3

    iget-boolean v0, p0, Lflipboard/activities/VideoActivity;->w:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->u:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->q:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lflipboard/activities/VideoActivity;->u:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    :cond_2
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->u:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "{width}px"

    const-string v5, "100%"

    invoke-virtual {v4, v3, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "{height}px"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "px"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "text/html"

    const-string v4, "utf-8"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/VideoActivity;->A:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 212
    :cond_3
    :goto_1
    return-void

    .line 208
    :catch_0
    move-exception v0

    sget-object v1, Lflipboard/activities/VideoActivity;->n:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_4
    const-string v1, "h264"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "video type"

    const-string v1, "h264"

    invoke-virtual {v6, v0, v1}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Video Play"

    invoke-static {v0, v6}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {p0, v10}, Lflipboard/activities/VideoActivity;->showDialog(I)V

    if-eqz v5, :cond_5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".m3u8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "video/mp4"

    invoke-virtual {v0, v5, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lflipboard/activities/VideoActivity;->finish()V

    :cond_5
    const v0, 0x7f030154

    :try_start_1
    invoke-virtual {p0, v0}, Lflipboard/activities/VideoActivity;->setContentView(I)V

    const v0, 0x7f0a0393

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lflipboard/activities/VideoActivity;->q:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->q:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lflipboard/activities/VideoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08009b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0x30

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    new-instance v1, Lflipboard/gui/FLVideoView;

    new-instance v2, Lflipboard/activities/VideoActivity$2;

    invoke-direct {v2, p0}, Lflipboard/activities/VideoActivity$2;-><init>(Lflipboard/activities/VideoActivity;)V

    invoke-direct {v1, p0, v2}, Lflipboard/gui/FLVideoView;-><init>(Landroid/content/Context;Lflipboard/gui/FLVideoView$TouchEventCallback;)V

    iput-object v1, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    iget-object v1, p0, Lflipboard/activities/VideoActivity;->q:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLVideoView;->setZOrderOnTop(Z)V

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    invoke-virtual {v0, p0}, Lflipboard/gui/FLVideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    invoke-virtual {v0, p0}, Lflipboard/gui/FLVideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    invoke-virtual {v0, p0}, Lflipboard/gui/FLVideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    invoke-virtual {v0, p0}, Lflipboard/gui/FLVideoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lflipboard/activities/VideoActivity$3;

    invoke-direct {v0, p0, p0}, Lflipboard/activities/VideoActivity$3;-><init>(Lflipboard/activities/VideoActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/activities/VideoActivity;->z:Landroid/widget/MediaController;

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->z:Landroid/widget/MediaController;

    iget-object v1, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    invoke-virtual {v0, v1}, Landroid/widget/MediaController;->setAnchorView(Landroid/view/View;)V

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->z:Landroid/widget/MediaController;

    iget-object v1, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    invoke-virtual {v0, v1}, Landroid/widget/MediaController;->setMediaPlayer(Landroid/widget/MediaController$MediaPlayerControl;)V

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    iget-object v1, p0, Lflipboard/activities/VideoActivity;->z:Landroid/widget/MediaController;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLVideoView;->setMediaController(Landroid/widget/MediaController;)V

    iget-object v0, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLVideoView;->setVideoURI(Landroid/net/Uri;)V

    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    if-eqz v0, :cond_6

    new-instance v1, Lflipboard/activities/VideoActivity$4;

    invoke-direct {v1, p0}, Lflipboard/activities/VideoActivity$4;-><init>(Lflipboard/activities/VideoActivity;)V

    iput-object v1, p0, Lflipboard/activities/VideoActivity;->ae:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    iget-object v1, p0, Lflipboard/activities/VideoActivity;->ae:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    :cond_6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/VideoActivity;->A:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    :catch_1
    move-exception v0

    sget-object v1, Lflipboard/activities/VideoActivity;->n:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :cond_7
    invoke-virtual {v5}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "youtube.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_a

    const-string v0, "video type"

    const-string v1, "youtube"

    invoke-virtual {v6, v0, v1}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Video Play"

    invoke-static {v0, v6}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;Ljava/util/Map;)V

    const/4 v0, 0x0

    const-string v1, "v"

    invoke-virtual {v5, v1}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_8
    if-nez v0, :cond_9

    invoke-virtual {p0}, Lflipboard/activities/VideoActivity;->finish()V

    goto/16 :goto_1

    :cond_9
    :try_start_2
    invoke-static {}, Lflipboard/util/YouTubeHelper;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_1

    :catch_2
    move-exception v0

    sget-object v1, Lflipboard/activities/VideoActivity;->n:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :pswitch_0
    :try_start_3
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflipboard/activities/YouTubePlayerActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "youtube_video_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v0, Lflipboard/util/YouTubeHelper;->a:Lflipboard/util/Log;

    const/16 v0, 0x65

    invoke-virtual {p0, v1, v0}, Lflipboard/activities/VideoActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_1

    :pswitch_1
    :try_start_4
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "vnd.youtube://"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v1}, Lflipboard/activities/VideoActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_4
    .catch Landroid/content/ActivityNotFoundException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_1

    :catch_3
    move-exception v0

    goto :goto_2

    :cond_a
    const-string v0, "video type"

    const-string v1, "web view"

    invoke-virtual {v6, v0, v1}, Lflipboard/json/FLObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Video Play"

    invoke-static {v0, v6}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;Ljava/util/Map;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_b
    move v1, v2

    move v3, v2

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 2

    .prologue
    .line 153
    iput p3, p0, Lflipboard/activities/VideoActivity;->s:I

    .line 154
    iput p2, p0, Lflipboard/activities/VideoActivity;->r:I

    .line 155
    iget v0, p0, Lflipboard/activities/VideoActivity;->s:I

    if-lez v0, :cond_0

    iget v0, p0, Lflipboard/activities/VideoActivity;->r:I

    if-lez v0, :cond_0

    .line 156
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lflipboard/activities/VideoActivity;->removeDialog(I)V

    .line 157
    :cond_0
    iget-object v0, p0, Lflipboard/activities/VideoActivity;->y:Lflipboard/gui/FLVideoView;

    invoke-direct {p0}, Lflipboard/activities/VideoActivity;->I()I

    move-result v1

    invoke-virtual {v0, p2, p3, v1}, Lflipboard/gui/FLVideoView;->a(III)V

    .line 158
    return-void
.end method
