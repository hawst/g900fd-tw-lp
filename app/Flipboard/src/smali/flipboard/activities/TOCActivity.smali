.class public Lflipboard/activities/TOCActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "TOCActivity.java"


# instance fields
.field private n:Lflipboard/io/UsageEvent;

.field private o:Lflipboard/gui/toc/TOCView;

.field private p:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/User;",
            "Lflipboard/service/User$Message;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private q:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/TOCActivity;->q:Z

    return-void
.end method

.method static synthetic a(Lflipboard/activities/TOCActivity;)Lflipboard/gui/toc/TOCView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    return-object v0
.end method

.method static synthetic a(Lflipboard/activities/TOCActivity;Lflipboard/service/User$Message;)V
    .locals 2

    .prologue
    .line 42
    sget-object v0, Lflipboard/activities/TOCActivity$7;->a:[I

    invoke-virtual {p1}, Lflipboard/service/User$Message;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lflipboard/activities/TOCActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0219

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private b(Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 155
    const-string v0, "selection_path"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 156
    const-string v0, "selection_path"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 157
    invoke-static {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 159
    :cond_0
    const-string v0, "launch_from"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_1

    const-string v1, "widget"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 161
    invoke-virtual {p0}, Lflipboard/activities/TOCActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0, v2, v2}, Lflipboard/widget/FlipboardWidgetManager;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->K()V

    .line 164
    :cond_1
    return-void
.end method

.method private r()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 244
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 279
    :cond_0
    :goto_0
    return-void

    .line 248
    :cond_1
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v2, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 249
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 250
    invoke-virtual {v2}, Lflipboard/service/User;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->e()Z

    move-result v0

    if-nez v0, :cond_3

    iget-wide v6, v2, Lflipboard/service/User;->m:J

    sub-long v6, v4, v6

    const-wide/32 v8, 0x1d4c0

    cmp-long v0, v6, v8

    if-ltz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_4

    .line 251
    new-instance v0, Lflipboard/activities/TOCActivity$3;

    invoke-direct {v0, p0, v2, v4, v5}, Lflipboard/activities/TOCActivity$3;-><init>(Lflipboard/activities/TOCActivity;Lflipboard/service/User;J)V

    invoke-virtual {v2, v0}, Lflipboard/service/User;->a(Lflipboard/service/Flap$TypedResultObserver;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 250
    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1

    .line 274
    :cond_4
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/TOCView;->a(Z)V

    goto :goto_0
.end method


# virtual methods
.method protected final C()V
    .locals 0

    .prologue
    .line 432
    invoke-direct {p0}, Lflipboard/activities/TOCActivity;->r()V

    .line 433
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 459
    const-string v0, "toc"

    return-object v0
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 339
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 340
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/service/User;->a(Z)V

    .line 345
    :goto_0
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->w()V

    .line 348
    :cond_0
    return-void

    .line 342
    :cond_1
    invoke-virtual {p0}, Lflipboard/activities/TOCActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0218

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onAccountClicked(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 447
    invoke-static {p0}, Lflipboard/activities/ContentDrawerActivity;->openContentDrawerOnAccount(Landroid/app/Activity;)V

    .line 448
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->w()V

    .line 421
    :goto_0
    return-void

    .line 420
    :cond_0
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 63
    iput-boolean v4, p0, Lflipboard/activities/TOCActivity;->W:Z

    .line 65
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 67
    invoke-virtual {p0}, Lflipboard/activities/TOCActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 74
    iget-object v1, v0, Lflipboard/service/User;->e:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v5, :cond_3

    .line 77
    :cond_2
    :try_start_0
    invoke-static {}, Lflipboard/service/User;->y()Z

    move-result v1

    if-nez v1, :cond_4

    .line 78
    const-string v1, "unwanted.TOC_no_sections"

    invoke-static {v1}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    .line 79
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "Have to load default TOC, because it\'s empty."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->A()Lflipboard/model/ConfigFirstLaunch;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "sstreamFallbackDefault"

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/User;->a(Lflipboard/model/ConfigFirstLaunch;ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lflipboard/activities/TOCActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 92
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v3, "do_first_launch"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 95
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/FirstRunActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 96
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 97
    invoke-virtual {p0, v0}, Lflipboard/activities/TOCActivity;->startActivity(Landroid/content/Intent;)V

    .line 98
    invoke-virtual {p0}, Lflipboard/activities/TOCActivity;->finish()V

    goto :goto_0

    .line 82
    :cond_4
    :try_start_1
    const-string v1, "unwanted.TOC_user_removed_all"

    invoke-static {v1}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    .line 83
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "User removed all TOC tiles"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 86
    :catch_0
    move-exception v1

    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "unable to load first launch file to add default sections to empty TOC"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 102
    :cond_5
    invoke-direct {p0, v1}, Lflipboard/activities/TOCActivity;->b(Landroid/content/Intent;)V

    .line 103
    const-string v2, "page"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 106
    iget-object v2, v0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 108
    new-instance v3, Lflipboard/gui/toc/TOCView;

    invoke-direct {v3, p0}, Lflipboard/gui/toc/TOCView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    .line 109
    iget-object v3, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v3, v1}, Lflipboard/gui/toc/TOCView;->setCurrentViewIndex(I)V

    .line 110
    iget-object v1, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {p0, v1}, Lflipboard/activities/TOCActivity;->setContentView(Landroid/view/View;)V

    .line 112
    sget-object v1, Lflipboard/gui/toc/TOCView;->a:Lflipboard/util/Log;

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 113
    invoke-virtual {v0}, Lflipboard/service/User;->c()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lflipboard/service/User;->f()Z

    move-result v1

    if-nez v1, :cond_6

    .line 115
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/service/User;->a(Lflipboard/util/Observer;)V

    .line 119
    :cond_6
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->a()Lflipboard/widget/FlipboardWidgetManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Lflipboard/widget/FlipboardWidgetManager;->a(Z)I

    .line 122
    iget-object v0, p0, Lflipboard/activities/FlipboardActivity;->Y:Lflipboard/util/Observer;

    if-nez v0, :cond_7

    new-instance v0, Lflipboard/activities/FlipboardActivity$2;

    invoke-direct {v0, p0}, Lflipboard/activities/FlipboardActivity$2;-><init>(Lflipboard/activities/FlipboardActivity;)V

    iput-object v0, p0, Lflipboard/activities/FlipboardActivity;->Y:Lflipboard/util/Observer;

    .line 124
    invoke-static {}, Lflipboard/util/AndroidUtil;->m()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    invoke-virtual {p0}, Lflipboard/activities/TOCActivity;->y()V

    goto/16 :goto_0

    .line 122
    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Network observer already created for: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onDetachedFromWindow()V

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    .line 55
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lflipboard/activities/TOCActivity;->setContentView(Landroid/view/View;)V

    .line 56
    return-void
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 371
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0a0398

    if-ne v1, v2, :cond_0

    .line 372
    new-instance v1, Lflipboard/service/SyncJob;

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, v2, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-direct {v1, p0, v2}, Lflipboard/service/SyncJob;-><init>(Lflipboard/activities/FlipboardActivity;Ljava/util/List;)V

    .line 373
    invoke-virtual {v1}, Lflipboard/service/SyncJob;->a()V

    .line 374
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 408
    :goto_0
    return v0

    .line 377
    :cond_0
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0a0399

    if-ne v1, v2, :cond_1

    .line 378
    new-instance v1, Lflipboard/gui/dialog/FLProgressDialog;

    const v2, 0x7f0d004f

    invoke-direct {v1, p0, v2}, Lflipboard/gui/dialog/FLProgressDialog;-><init>(Landroid/app/Activity;I)V

    .line 379
    new-instance v2, Lflipboard/activities/TOCActivity$5;

    invoke-direct {v2, p0}, Lflipboard/activities/TOCActivity$5;-><init>(Lflipboard/activities/TOCActivity;)V

    invoke-virtual {v1, v2}, Lflipboard/gui/dialog/FLProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 386
    invoke-virtual {v1}, Lflipboard/gui/dialog/FLProgressDialog;->show()V

    .line 387
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v3, "TOCActivity:onMenuItemSelected"

    new-instance v4, Lflipboard/activities/TOCActivity$6;

    invoke-direct {v4, p0, v1}, Lflipboard/activities/TOCActivity$6;-><init>(Lflipboard/activities/TOCActivity;Lflipboard/gui/dialog/FLProgressDialog;)V

    invoke-virtual {v2, v3, v4}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 399
    :cond_1
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0a03b6

    if-ne v1, v2, :cond_3

    .line 400
    iget-object v1, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v1}, Lflipboard/gui/toc/TOCView;->v()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v1}, Lflipboard/gui/toc/TOCView;->w()V

    iget-object v1, p0, Lflipboard/activities/TOCActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    new-instance v2, Lflipboard/activities/TOCActivity$4;

    invoke-direct {v2, p0}, Lflipboard/activities/TOCActivity$4;-><init>(Lflipboard/activities/TOCActivity;)V

    const-wide/16 v4, 0x15e

    invoke-virtual {v1, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 401
    :goto_1
    sget-object v1, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v2, "tocSettingsButtonPressed"

    invoke-virtual {v1, v2}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 400
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflipboard/activities/SettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lflipboard/activities/TOCActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 404
    :cond_3
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0a03b7

    if-ne v1, v2, :cond_4

    .line 405
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflipboard/activities/ComposeActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lflipboard/activities/TOCActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 408
    :cond_4
    invoke-super {p0, p1, p2}, Lflipboard/activities/FlipboardActivity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 149
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 150
    invoke-direct {p0, p1}, Lflipboard/activities/TOCActivity;->b(Landroid/content/Intent;)V

    .line 151
    return-void
.end method

.method public onPageClicked(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->w()V

    .line 428
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 6

    .prologue
    .line 284
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->w()V

    .line 288
    :cond_0
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->p:Lflipboard/util/Observer;

    if-eqz v0, :cond_1

    .line 289
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/activities/TOCActivity;->p:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->c(Lflipboard/util/Observer;)V

    .line 290
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/TOCActivity;->p:Lflipboard/util/Observer;

    .line 294
    :cond_1
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->n:Lflipboard/io/UsageEvent;

    if-eqz v0, :cond_2

    .line 295
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->n:Lflipboard/io/UsageEvent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, Lflipboard/activities/TOCActivity;->n:Lflipboard/io/UsageEvent;

    iget-wide v4, v1, Lflipboard/io/UsageEvent;->e:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lflipboard/io/UsageEvent;->g:J

    .line 296
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->n:Lflipboard/io/UsageEvent;

    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 298
    :cond_2
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onPause()V

    .line 299
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 320
    if-eqz p1, :cond_0

    const-string v0, "flip_position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    const-string v1, "flip_position"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/toc/TOCView;->setCurrentViewIndex(I)V

    .line 323
    :cond_0
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 324
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 168
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onResume()V

    .line 169
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "navigation"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/activities/TOCActivity;->n:Lflipboard/io/UsageEvent;

    .line 170
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->n:Lflipboard/io/UsageEvent;

    const-string v1, "type"

    const-string v2, "toc"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 172
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 173
    invoke-virtual {v0}, Lflipboard/service/User;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 174
    new-instance v1, Lflipboard/activities/TOCActivity$1;

    invoke-direct {v1, p0}, Lflipboard/activities/TOCActivity$1;-><init>(Lflipboard/activities/TOCActivity;)V

    iput-object v1, p0, Lflipboard/activities/TOCActivity;->p:Lflipboard/util/Observer;

    .line 180
    iget-object v1, p0, Lflipboard/activities/TOCActivity;->p:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Lflipboard/util/Observer;)V

    .line 183
    :cond_0
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->i()V

    .line 184
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->M:Lflipboard/service/FlipboardManager;

    const-string v1, "TOCActivity:onResume"

    new-instance v2, Lflipboard/activities/TOCActivity$2;

    invoke-direct {v2, p0}, Lflipboard/activities/TOCActivity$2;-><init>(Lflipboard/activities/TOCActivity;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 221
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, p0}, Lflipboard/service/FlipboardManager;->b(Landroid/app/Activity;)V

    .line 223
    invoke-virtual {p0}, Lflipboard/activities/TOCActivity;->x()V

    .line 226
    iget-boolean v0, p0, Lflipboard/activities/TOCActivity;->q:Z

    if-eqz v0, :cond_1

    .line 227
    invoke-direct {p0}, Lflipboard/activities/TOCActivity;->r()V

    .line 228
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/TOCActivity;->q:Z

    .line 232
    :goto_0
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v1

    .line 233
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->getPages()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/toc/TOCPage;

    .line 234
    invoke-virtual {v0, v1}, Lflipboard/gui/toc/TOCPage;->a(Z)V

    goto :goto_1

    .line 230
    :cond_1
    invoke-direct {p0}, Lflipboard/activities/TOCActivity;->r()V

    goto :goto_0

    .line 236
    :cond_2
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 313
    const-string v0, "flip_position"

    iget-object v1, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v1}, Lflipboard/gui/toc/TOCView;->getCurrentViewIndex()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 314
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 315
    return-void
.end method

.method public onShareItem(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 452
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    invoke-virtual {v0}, Lflipboard/gui/toc/TOCView;->getCurrentViewIndex()I

    move-result v0

    if-nez v0, :cond_0

    .line 453
    iget-object v0, p0, Lflipboard/activities/TOCActivity;->o:Lflipboard/gui/toc/TOCView;

    iget-object v1, v0, Lflipboard/gui/toc/TOCView;->M:Lflipboard/gui/toc/CoverPage;

    iget v0, v1, Lflipboard/gui/toc/CoverPage;->l:I

    iget-object v2, v1, Lflipboard/gui/toc/CoverPage;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, v1, Lflipboard/gui/toc/CoverPage;->t:Z

    iget-object v2, v1, Lflipboard/gui/toc/CoverPage;->p:Lflipboard/objs/FeedItem;

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lflipboard/gui/toc/CoverPage;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-object v1, v1, Lflipboard/gui/toc/CoverPage;->c:Lflipboard/service/Section;

    sget-object v3, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->g:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    invoke-static {v0, v1, v2, v3}, Lflipboard/util/SocialHelper;->b(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UsageEventV2$FlipItemNavFrom;)V

    .line 455
    :cond_0
    return-void
.end method

.method public openContentDrawer(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 437
    invoke-static {p0}, Lflipboard/activities/ContentDrawerActivity;->openContentDrawer(Landroid/app/Activity;)V

    .line 438
    return-void
.end method

.method public openContentDrawerOnAccount(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 442
    invoke-static {p0}, Lflipboard/activities/ContentDrawerActivity;->openContentDrawerOnAccount(Landroid/app/Activity;)V

    .line 443
    return-void
.end method

.method public refreshClicked(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 331
    invoke-virtual {p0}, Lflipboard/activities/TOCActivity;->j()V

    .line 332
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v1, "tocRefreshButtonPressed"

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    .line 333
    return-void
.end method
