.class Lflipboard/activities/SettingsFragment$15;
.super Lflipboard/activities/SettingsFragment$RowItem;
.source "SettingsFragment.java"


# instance fields
.field final synthetic a:Lflipboard/activities/SettingsFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 649
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$15;->a:Lflipboard/activities/SettingsFragment;

    invoke-direct {p0, p2}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 671
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$15;->a:Lflipboard/activities/SettingsFragment;

    const/16 v1, 0x71

    invoke-virtual {v0, v1}, Lflipboard/activities/SettingsFragment;->a(I)V

    .line 672
    return-void
.end method

.method final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 652
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v0, v0, Lflipboard/io/NetworkManager;->k:Ljava/lang/String;

    .line 653
    const-string v1, "enabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 654
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$15;->a:Lflipboard/activities/SettingsFragment;

    const v1, 0x7f0d02c8

    invoke-virtual {v0, v1}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 666
    :goto_0
    return-object v0

    .line 656
    :cond_0
    const-string v1, "ondemand"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 657
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$15;->a:Lflipboard/activities/SettingsFragment;

    const v1, 0x7f0d02c9

    invoke-virtual {v0, v1}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 659
    :cond_1
    const-string v1, "disabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 660
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_2

    .line 661
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$15;->a:Lflipboard/activities/SettingsFragment;

    const v1, 0x7f0d02c7

    invoke-virtual {v0, v1}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 663
    :cond_2
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$15;->a:Lflipboard/activities/SettingsFragment;

    const v1, 0x7f0d02c6

    invoke-virtual {v0, v1}, Lflipboard/activities/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 666
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
