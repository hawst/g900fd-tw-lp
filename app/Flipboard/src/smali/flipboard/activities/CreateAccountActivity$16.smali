.class Lflipboard/activities/CreateAccountActivity$16;
.super Ljava/lang/Object;
.source "CreateAccountActivity.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/FlipboardManager;",
        "Lflipboard/service/FlipboardManager$CreateAccountMessage;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lflipboard/activities/CreateAccountActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/CreateAccountActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 833
    iput-object p1, p0, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    iput-object p2, p0, Lflipboard/activities/CreateAccountActivity$16;->a:Ljava/lang/String;

    iput-object p3, p0, Lflipboard/activities/CreateAccountActivity$16;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 833
    check-cast p2, Lflipboard/service/FlipboardManager$CreateAccountMessage;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/CreateAccountActivity$16$1;

    invoke-direct {v1, p0}, Lflipboard/activities/CreateAccountActivity$16$1;-><init>(Lflipboard/activities/CreateAccountActivity$16;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$16;->a:Ljava/lang/String;

    const-string v1, "googleplus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/CreateAccountActivity$16;->c:Lflipboard/activities/CreateAccountActivity;

    invoke-static {v0}, Lflipboard/activities/CreateAccountActivity;->f(Lflipboard/activities/CreateAccountActivity;)Lflipboard/util/GooglePlusSignInClient;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/CreateAccountActivity$16;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/util/GooglePlusSignInClient;->a(Ljava/lang/String;)V

    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/CreateAccountActivity$16$2;

    invoke-direct {v1, p0, p2, p3}, Lflipboard/activities/CreateAccountActivity$16$2;-><init>(Lflipboard/activities/CreateAccountActivity$16;Lflipboard/service/FlipboardManager$CreateAccountMessage;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    return-void
.end method
