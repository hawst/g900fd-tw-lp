.class public Lflipboard/activities/SocialCardFragment;
.super Lflipboard/gui/dialog/FLDialogFragment;
.source "SocialCardFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private E:Lflipboard/util/Log;

.field private F:Lflipboard/objs/ConfigService;

.field private G:Lflipboard/gui/CommentaryList;

.field public j:Lflipboard/objs/FeedItem;

.field public k:Lflipboard/objs/FeedItem;

.field public l:Lflipboard/objs/FeedItem;

.field public m:Lflipboard/service/Section;

.field n:Lflipboard/service/FlipboardManager;

.field public o:Z

.field public p:Z

.field q:Z

.field r:Lflipboard/gui/FLTextIntf;

.field s:Landroid/widget/EditText;

.field t:Landroid/widget/Button;

.field public u:Z

.field v:Z

.field w:Lflipboard/service/Section;

.field x:Landroid/os/Bundle;

.field y:Ljava/lang/String;

.field public z:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogFragment;-><init>()V

    .line 60
    const-string v0, "social_card"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/SocialCardFragment;->E:Lflipboard/util/Log;

    .line 64
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/activities/SocialCardFragment;->n:Lflipboard/service/FlipboardManager;

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/activities/SocialCardFragment;->o:Z

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/activities/SocialCardFragment;->u:Z

    return-void
.end method

.method static synthetic a(Lflipboard/activities/SocialCardFragment;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lflipboard/activities/SocialCardFragment;->m:Lflipboard/service/Section;

    return-object v0
.end method

.method static synthetic b(Lflipboard/activities/SocialCardFragment;)Lflipboard/util/Log;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lflipboard/activities/SocialCardFragment;->E:Lflipboard/util/Log;

    return-object v0
.end method

.method static synthetic c(Lflipboard/activities/SocialCardFragment;)Lflipboard/objs/ConfigService;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lflipboard/activities/SocialCardFragment;->F:Lflipboard/objs/ConfigService;

    return-object v0
.end method

.method static synthetic d(Lflipboard/activities/SocialCardFragment;)Lflipboard/gui/CommentaryList;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lflipboard/activities/SocialCardFragment;->G:Lflipboard/gui/CommentaryList;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 294
    const-string v0, ""

    .line 295
    iget-object v1, p0, Lflipboard/activities/SocialCardFragment;->F:Lflipboard/objs/ConfigService;

    iget-boolean v1, v1, Lflipboard/objs/ConfigService;->bF:Z

    if-eqz v1, :cond_0

    .line 297
    iget-object v0, p0, Lflipboard/activities/SocialCardFragment;->k:Lflipboard/objs/FeedItem;

    invoke-static {v0}, Lflipboard/util/SocialCardHelper;->a(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v0

    .line 299
    :cond_0
    iget-object v1, p0, Lflipboard/activities/SocialCardFragment;->s:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 301
    iget-object v1, p0, Lflipboard/activities/SocialCardFragment;->s:Landroid/widget/EditText;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 302
    return-void
.end method

.method static synthetic e(Lflipboard/activities/SocialCardFragment;)Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lflipboard/activities/SocialCardFragment;->D:Z

    return v0
.end method

.method static synthetic f(Lflipboard/activities/SocialCardFragment;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lflipboard/activities/SocialCardFragment;->e()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/objs/FeedItem;)V
    .locals 1

    .prologue
    .line 408
    if-nez p1, :cond_0

    .line 414
    :goto_0
    return-void

    .line 411
    :cond_0
    iput-object p1, p0, Lflipboard/activities/SocialCardFragment;->j:Lflipboard/objs/FeedItem;

    .line 412
    iput-object p1, p0, Lflipboard/activities/SocialCardFragment;->l:Lflipboard/objs/FeedItem;

    .line 413
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/SocialCardFragment;->k:Lflipboard/objs/FeedItem;

    goto :goto_0
.end method

.method final a(Lflipboard/service/Section;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 264
    iget-boolean v0, p0, Lflipboard/activities/SocialCardFragment;->D:Z

    if-nez v0, :cond_0

    .line 291
    :goto_0
    return-void

    .line 267
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lflipboard/service/Section;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 268
    :goto_1
    new-instance v3, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 269
    const v4, 0x7f0d00a0

    invoke-virtual {v3, v4}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 270
    iget-object v4, p0, Lflipboard/activities/SocialCardFragment;->j:Lflipboard/objs/FeedItem;

    invoke-static {v4}, Lflipboard/gui/section/ItemDisplayUtil;->f(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v4

    .line 271
    if-eqz v0, :cond_2

    .line 272
    invoke-virtual {p0}, Lflipboard/activities/SocialCardFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d009f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v4, v6, v2

    iget-object v2, p0, Lflipboard/activities/SocialCardFragment;->F:Lflipboard/objs/ConfigService;

    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v1

    invoke-virtual {p1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v7

    invoke-static {v5, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 276
    :goto_2
    const v1, 0x7f0d004a

    invoke-virtual {v3, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 277
    const v1, 0x7f0d0178

    invoke-virtual {v3, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 278
    new-instance v1, Lflipboard/activities/SocialCardFragment$6;

    invoke-direct {v1, p0, v0, p1}, Lflipboard/activities/SocialCardFragment$6;-><init>(Lflipboard/activities/SocialCardFragment;ZLflipboard/service/Section;)V

    iput-object v1, v3, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 290
    invoke-virtual {p0}, Lflipboard/activities/SocialCardFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "mute"

    invoke-virtual {v3, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 267
    goto :goto_1

    .line 274
    :cond_2
    invoke-virtual {p0}, Lflipboard/activities/SocialCardFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d009e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v4, v6, v2

    iget-object v2, p0, Lflipboard/activities/SocialCardFragment;->F:Lflipboard/objs/ConfigService;

    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v1

    invoke-static {v5, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    goto :goto_2
.end method

.method public final l_()Z
    .locals 1

    .prologue
    .line 447
    iget-boolean v0, p0, Lflipboard/activities/SocialCardFragment;->o:Z

    if-eqz v0, :cond_0

    .line 448
    invoke-virtual {p0}, Lflipboard/activities/SocialCardFragment;->a()V

    .line 450
    :cond_0
    iget-boolean v0, p0, Lflipboard/activities/SocialCardFragment;->o:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 439
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a026d

    if-ne v0, v1, :cond_0

    .line 440
    invoke-virtual {p0, p1}, Lflipboard/activities/SocialCardFragment;->send(Landroid/view/View;)V

    .line 442
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/16 v10, 0x8

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 84
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v2, "event"

    invoke-direct {v0, v2}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    .line 85
    const-string v2, "type"

    const-string v3, "enter"

    invoke-virtual {v0, v2, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 86
    const-string v2, "id"

    const-string v3, "social_card"

    invoke-virtual {v0, v2, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 87
    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->b()V

    .line 90
    invoke-virtual {p0}, Lflipboard/activities/SocialCardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 91
    const v2, 0x7f030129

    invoke-static {v0, v2, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 93
    iget-object v2, p0, Lflipboard/activities/SocialCardFragment;->m:Lflipboard/service/Section;

    if-nez v2, :cond_0

    .line 94
    iget-object v0, p0, Lflipboard/activities/SocialCardFragment;->E:Lflipboard/util/Log;

    const-string v2, "section is null"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 260
    :goto_0
    return-object v0

    .line 100
    :cond_0
    iget-object v2, p0, Lflipboard/activities/SocialCardFragment;->j:Lflipboard/objs/FeedItem;

    if-nez v2, :cond_1

    .line 101
    iget-object v0, p0, Lflipboard/activities/SocialCardFragment;->E:Lflipboard/util/Log;

    const-string v2, "secondary item is null: %s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lflipboard/activities/SocialCardFragment;->m:Lflipboard/service/Section;

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 102
    goto :goto_0

    .line 104
    :cond_1
    iget-object v2, p0, Lflipboard/activities/SocialCardFragment;->k:Lflipboard/objs/FeedItem;

    if-nez v2, :cond_2

    .line 105
    iget-object v0, p0, Lflipboard/activities/SocialCardFragment;->E:Lflipboard/util/Log;

    const-string v2, "primary item is null: %s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lflipboard/activities/SocialCardFragment;->m:Lflipboard/service/Section;

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 106
    goto :goto_0

    .line 109
    :cond_2
    new-instance v1, Lflipboard/objs/UsageEventV2;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventAction;->o:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v3, Lflipboard/objs/UsageEventV2$EventCategory;->f:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v1, v2, v3}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 110
    iget-object v2, p0, Lflipboard/activities/SocialCardFragment;->k:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-static {v2}, Lflipboard/service/Account;->e(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 111
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 112
    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->m:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v4, p0, Lflipboard/activities/SocialCardFragment;->k:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    .line 113
    invoke-virtual {v2, v4}, Lflipboard/service/User;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v3

    sget-object v4, Lflipboard/objs/UsageEventV2$CommonEventData;->n:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v6, p0, Lflipboard/activities/SocialCardFragment;->k:Lflipboard/objs/FeedItem;

    iget-object v6, v6, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    .line 114
    invoke-virtual {v2, v6}, Lflipboard/service/User;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 116
    :cond_3
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->b:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, p0, Lflipboard/activities/SocialCardFragment;->k:Lflipboard/objs/FeedItem;

    .line 117
    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->Z()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v1

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->c:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, p0, Lflipboard/activities/SocialCardFragment;->k:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 118
    invoke-virtual {v1, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v1

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->l:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, p0, Lflipboard/activities/SocialCardFragment;->k:Lflipboard/objs/FeedItem;

    .line 119
    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->aa()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v1

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->p:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, p0, Lflipboard/activities/SocialCardFragment;->k:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    .line 120
    invoke-virtual {v1, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v1

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, p0, Lflipboard/activities/SocialCardFragment;->m:Lflipboard/service/Section;

    .line 121
    invoke-virtual {v3}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v1

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->k:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, p0, Lflipboard/activities/SocialCardFragment;->m:Lflipboard/service/Section;

    .line 122
    iget-object v3, v3, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v3, v3, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v1

    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, p0, Lflipboard/activities/SocialCardFragment;->z:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    .line 123
    invoke-virtual {v1, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v1

    .line 124
    invoke-virtual {v1}, Lflipboard/objs/UsageEventV2;->c()V

    .line 126
    iget-object v1, p0, Lflipboard/activities/SocialCardFragment;->j:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->ag()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lflipboard/activities/SocialCardFragment;->j:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->ah()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 127
    :cond_4
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, p0, Lflipboard/activities/SocialCardFragment;->k:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    iput-object v1, p0, Lflipboard/activities/SocialCardFragment;->F:Lflipboard/objs/ConfigService;

    .line 133
    :goto_1
    const v1, 0x7f0a032b

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/CommentaryList;

    iput-object v1, p0, Lflipboard/activities/SocialCardFragment;->G:Lflipboard/gui/CommentaryList;

    .line 134
    iget-object v1, p0, Lflipboard/activities/SocialCardFragment;->G:Lflipboard/gui/CommentaryList;

    invoke-virtual {v1, v0}, Lflipboard/gui/CommentaryList;->setActivity(Lflipboard/activities/FlipboardActivity;)V

    .line 135
    iget-object v1, p0, Lflipboard/activities/SocialCardFragment;->G:Lflipboard/gui/CommentaryList;

    iget-object v2, p0, Lflipboard/activities/SocialCardFragment;->m:Lflipboard/service/Section;

    iget-object v3, p0, Lflipboard/activities/SocialCardFragment;->F:Lflipboard/objs/ConfigService;

    iget-object v4, p0, Lflipboard/activities/SocialCardFragment;->k:Lflipboard/objs/FeedItem;

    invoke-virtual {v1, v2, v3, v4}, Lflipboard/gui/CommentaryList;->a(Lflipboard/service/Section;Lflipboard/objs/ConfigService;Lflipboard/objs/FeedItem;)V

    .line 138
    iget-boolean v1, p0, Lflipboard/activities/SocialCardFragment;->p:Z

    if-eqz v1, :cond_5

    .line 139
    const v1, 0x7f0a032a

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/SocialCardMainItem;

    .line 140
    invoke-virtual {v1, v5}, Lflipboard/gui/SocialCardMainItem;->setVisibility(I)V

    .line 141
    iget-object v2, p0, Lflipboard/activities/SocialCardFragment;->m:Lflipboard/service/Section;

    iget-object v3, p0, Lflipboard/activities/SocialCardFragment;->j:Lflipboard/objs/FeedItem;

    if-eqz v2, :cond_5

    if-nez v3, :cond_b

    .line 144
    :cond_5
    :goto_2
    const v1, 0x7f0a032c

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/section/AttributionServiceInfo;

    .line 145
    iput-boolean v8, v1, Lflipboard/gui/section/AttributionServiceInfo;->t:Z

    .line 146
    iget-object v2, p0, Lflipboard/activities/SocialCardFragment;->m:Lflipboard/service/Section;

    iget-object v3, p0, Lflipboard/activities/SocialCardFragment;->j:Lflipboard/objs/FeedItem;

    invoke-virtual {v1, v2, v3}, Lflipboard/gui/section/AttributionServiceInfo;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 148
    iget-object v1, p0, Lflipboard/activities/SocialCardFragment;->n:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, p0, Lflipboard/activities/SocialCardFragment;->F:Lflipboard/objs/ConfigService;

    iget-object v2, v2, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v2

    .line 149
    if-eqz v2, :cond_11

    iget-object v1, p0, Lflipboard/activities/SocialCardFragment;->k:Lflipboard/objs/FeedItem;

    iget-boolean v1, v1, Lflipboard/objs/FeedItem;->ag:Z

    if-eqz v1, :cond_11

    .line 150
    const v1, 0x7f0a026e

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lflipboard/activities/SocialCardFragment;->s:Landroid/widget/EditText;

    .line 152
    const v1, 0x7f0a026c

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    .line 153
    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setTag(Ljava/lang/Object;)V

    .line 154
    iget-object v3, p0, Lflipboard/activities/SocialCardFragment;->G:Lflipboard/gui/CommentaryList;

    invoke-virtual {v1, v3}, Lflipboard/gui/FLImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    iget-object v3, v2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v3}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_f

    .line 156
    iget-object v2, v2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v2}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 161
    :goto_3
    const v1, 0x7f0a0330

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    .line 162
    iget-object v2, p0, Lflipboard/activities/SocialCardFragment;->F:Lflipboard/objs/ConfigService;

    iget-object v2, v2, Lflipboard/objs/ConfigService;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 167
    const-string v1, "twitter"

    iget-object v2, p0, Lflipboard/activities/SocialCardFragment;->F:Lflipboard/objs/ConfigService;

    iget-object v2, v2, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "weibo"

    iget-object v2, p0, Lflipboard/activities/SocialCardFragment;->F:Lflipboard/objs/ConfigService;

    iget-object v2, v2, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    :cond_6
    move v1, v8

    :goto_4
    iput-boolean v1, p0, Lflipboard/activities/SocialCardFragment;->q:Z

    .line 169
    invoke-direct {p0}, Lflipboard/activities/SocialCardFragment;->e()V

    .line 171
    const v1, 0x7f0a0331

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, p0, Lflipboard/activities/SocialCardFragment;->r:Lflipboard/gui/FLTextIntf;

    .line 172
    const v1, 0x7f0a026d

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lflipboard/activities/SocialCardFragment;->t:Landroid/widget/Button;

    .line 173
    iget-object v1, p0, Lflipboard/activities/SocialCardFragment;->t:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    iget-object v1, p0, Lflipboard/activities/SocialCardFragment;->s:Landroid/widget/EditText;

    new-instance v2, Lflipboard/activities/SocialCardFragment$1;

    invoke-direct {v2, p0}, Lflipboard/activities/SocialCardFragment$1;-><init>(Lflipboard/activities/SocialCardFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    iget-object v1, p0, Lflipboard/activities/SocialCardFragment;->s:Landroid/widget/EditText;

    new-instance v2, Lflipboard/activities/SocialCardFragment$2;

    invoke-direct {v2, p0}, Lflipboard/activities/SocialCardFragment$2;-><init>(Lflipboard/activities/SocialCardFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 193
    iget-object v1, p0, Lflipboard/activities/SocialCardFragment;->s:Landroid/widget/EditText;

    new-instance v2, Lflipboard/activities/SocialCardFragment$3;

    invoke-direct {v2, p0}, Lflipboard/activities/SocialCardFragment$3;-><init>(Lflipboard/activities/SocialCardFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 218
    :goto_5
    const v1, 0x7f0a004c

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lflipboard/gui/actionbar/FLActionBar;

    .line 219
    iget-boolean v1, p0, Lflipboard/activities/SocialCardFragment;->u:Z

    if-eqz v1, :cond_12

    .line 220
    invoke-virtual {v7, v5}, Lflipboard/gui/actionbar/FLActionBar;->setVisibility(I)V

    .line 221
    invoke-virtual {v7}, Lflipboard/gui/actionbar/FLActionBar;->f()Landroid/view/View;

    .line 222
    invoke-virtual {v7}, Lflipboard/gui/actionbar/FLActionBar;->e()V

    .line 223
    invoke-virtual {v7, v0, p0}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/gui/dialog/FLDialogFragment;)V

    .line 225
    iget-boolean v0, p0, Lflipboard/activities/SocialCardFragment;->v:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/activities/SocialCardFragment;->w:Lflipboard/service/Section;

    if-eqz v0, :cond_7

    .line 226
    iget-object v0, p0, Lflipboard/activities/SocialCardFragment;->w:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/activities/SocialCardFragment;->x:Landroid/os/Bundle;

    iget-object v2, p0, Lflipboard/activities/SocialCardFragment;->y:Ljava/lang/String;

    invoke-virtual {v7, v0, v1, v2}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/service/Section;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 229
    :cond_7
    new-instance v0, Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;-><init>(Landroid/content/Context;)V

    .line 230
    iget-object v1, p0, Lflipboard/activities/SocialCardFragment;->m:Lflipboard/service/Section;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lflipboard/activities/SocialCardFragment;->j:Lflipboard/objs/FeedItem;

    if-eqz v1, :cond_8

    .line 231
    invoke-virtual {p0}, Lflipboard/activities/SocialCardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lflipboard/activities/FlipboardActivity;

    iget-object v2, p0, Lflipboard/activities/SocialCardFragment;->m:Lflipboard/service/Section;

    iget-object v3, p0, Lflipboard/activities/SocialCardFragment;->j:Lflipboard/objs/FeedItem;

    sget-object v4, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    iget-object v6, p0, Lflipboard/activities/SocialCardFragment;->l:Lflipboard/objs/FeedItem;

    invoke-virtual/range {v0 .. v6}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;ZLflipboard/objs/FeedItem;)V

    .line 232
    const v1, 0x7f0d017a

    invoke-virtual {p0, v1}, Lflipboard/activities/SocialCardFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(Ljava/lang/CharSequence;)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    new-instance v2, Lflipboard/activities/SocialCardFragment$4;

    invoke-direct {v2, p0}, Lflipboard/activities/SocialCardFragment$4;-><init>(Lflipboard/activities/SocialCardFragment;)V

    iput-object v2, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 239
    iget-object v1, p0, Lflipboard/activities/SocialCardFragment;->m:Lflipboard/service/Section;

    invoke-virtual {v1}, Lflipboard/service/Section;->t()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 240
    const v1, 0x7f0d017b

    invoke-virtual {p0, v1}, Lflipboard/activities/SocialCardFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/Object;

    const v3, 0x7f0d00b7

    invoke-virtual {p0, v3}, Lflipboard/activities/SocialCardFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 241
    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(Ljava/lang/CharSequence;)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    new-instance v2, Lflipboard/activities/SocialCardFragment$5;

    invoke-direct {v2, p0}, Lflipboard/activities/SocialCardFragment$5;-><init>(Lflipboard/activities/SocialCardFragment;)V

    iput-object v2, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 251
    :cond_8
    invoke-virtual {v7, v0}, Lflipboard/gui/actionbar/FLActionBar;->setMenu(Lflipboard/gui/actionbar/FLActionBarMenu;)V

    .line 256
    :goto_6
    iget-boolean v0, p0, Lflipboard/activities/SocialCardFragment;->o:Z

    if-eqz v0, :cond_9

    .line 257
    invoke-virtual {p0}, Lflipboard/activities/SocialCardFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_9
    move-object v0, v9

    .line 260
    goto/16 :goto_0

    .line 129
    :cond_a
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "flipboard"

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    iput-object v1, p0, Lflipboard/activities/SocialCardFragment;->F:Lflipboard/objs/ConfigService;

    goto/16 :goto_1

    .line 141
    :cond_b
    iput-object v2, v1, Lflipboard/gui/SocialCardMainItem;->a:Lflipboard/service/Section;

    iput-object v3, v1, Lflipboard/gui/SocialCardMainItem;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->f()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c

    iget-object v2, v1, Lflipboard/gui/SocialCardMainItem;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    iget-object v2, v1, Lflipboard/gui/SocialCardMainItem;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v2, v5}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    :goto_7
    iget-object v2, v3, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, v1, Lflipboard/gui/SocialCardMainItem;->d:Lflipboard/gui/FLStaticTextView;

    iget-object v4, v3, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lflipboard/gui/SocialCardMainItem;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2, v5}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    :goto_8
    invoke-static {v3}, Lflipboard/gui/section/ItemDisplayUtil;->c(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_e

    iget-object v3, v1, Lflipboard/gui/SocialCardMainItem;->e:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v3, v2}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v1, Lflipboard/gui/SocialCardMainItem;->e:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1, v5}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_c
    iget-object v2, v1, Lflipboard/gui/SocialCardMainItem;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v2, v10}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto :goto_7

    :cond_d
    iget-object v2, v1, Lflipboard/gui/SocialCardMainItem;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2, v10}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    goto :goto_8

    :cond_e
    iget-object v1, v1, Lflipboard/gui/SocialCardMainItem;->e:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1, v10}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 158
    :cond_f
    const v2, 0x7f02005a

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    goto/16 :goto_3

    :cond_10
    move v1, v5

    .line 167
    goto/16 :goto_4

    .line 215
    :cond_11
    const v1, 0x7f0a026b

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5

    .line 253
    :cond_12
    invoke-virtual {v7, v10}, Lflipboard/gui/actionbar/FLActionBar;->setVisibility(I)V

    goto/16 :goto_6
.end method

.method public send(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 314
    iget-object v0, p0, Lflipboard/activities/SocialCardFragment;->n:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/SocialCardFragment$7;

    invoke-direct {v1, p0}, Lflipboard/activities/SocialCardFragment$7;-><init>(Lflipboard/activities/SocialCardFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 396
    invoke-virtual {p0}, Lflipboard/activities/SocialCardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 397
    if-eqz v1, :cond_0

    .line 399
    const-string v0, "input_method"

    invoke-virtual {v1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 400
    invoke-virtual {v1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 401
    invoke-virtual {v1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 404
    :cond_0
    return-void
.end method
