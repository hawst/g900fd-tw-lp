.class Lflipboard/activities/SettingsFragment$51;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SettingsFragment.java"


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Lflipboard/activities/SettingsFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1941
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$51;->b:Lflipboard/activities/SettingsFragment;

    iput-object p2, p0, Lflipboard/activities/SettingsFragment$51;->a:Landroid/widget/EditText;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 4

    .prologue
    .line 1945
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$51;->b:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->d(Lflipboard/activities/SettingsFragment;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ad_server_base_urls"

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 1946
    iget-object v1, p0, Lflipboard/activities/SettingsFragment$51;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, " "

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 1947
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1948
    iget-object v1, p0, Lflipboard/activities/SettingsFragment$51;->b:Lflipboard/activities/SettingsFragment;

    invoke-static {v1}, Lflipboard/activities/SettingsFragment;->d(Lflipboard/activities/SettingsFragment;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "ad_server_base_urls"

    .line 1949
    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "ad_server_base_urls"

    .line 1950
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "ad_server_base_urls_size"

    .line 1951
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1952
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1953
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$51;->b:Lflipboard/activities/SettingsFragment;

    const/16 v1, 0x73

    invoke-virtual {v0, v1}, Lflipboard/activities/SettingsFragment;->a(I)V

    .line 1954
    return-void
.end method

.method public final d(Landroid/support/v4/app/DialogFragment;)V
    .locals 2

    .prologue
    .line 1958
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$51;->b:Lflipboard/activities/SettingsFragment;

    const/16 v1, 0x73

    invoke-virtual {v0, v1}, Lflipboard/activities/SettingsFragment;->a(I)V

    .line 1959
    return-void
.end method
