.class Lflipboard/activities/SettingsFragment$14;
.super Lflipboard/activities/SettingsFragment$RowItem;
.source "SettingsFragment.java"


# instance fields
.field final synthetic a:Lflipboard/activities/SettingsFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 628
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$14;->a:Lflipboard/activities/SettingsFragment;

    invoke-direct {p0, p2}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 642
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$14;->a:Lflipboard/activities/SettingsFragment;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lflipboard/activities/SettingsFragment$14;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v2}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lflipboard/activities/WidgetConfigActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lflipboard/activities/SettingsFragment;->startActivity(Landroid/content/Intent;)V

    .line 644
    return-void
.end method

.method final b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 631
    invoke-static {}, Lflipboard/widget/FlipboardWidgetManager;->a()Lflipboard/widget/FlipboardWidgetManager;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/widget/FlipboardWidgetManager;->g()J

    move-result-wide v0

    .line 632
    const-wide/32 v2, 0x2255100

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 633
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$14;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0353

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 637
    :goto_0
    return-object v0

    .line 634
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 635
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$14;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0354

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 637
    :cond_1
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$14;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0355

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
