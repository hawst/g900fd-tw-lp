.class public Lflipboard/activities/MediaButtonReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MediaButtonReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 13
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 15
    const-string v0, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/service/audio/MediaPlayerService;->b:Lflipboard/service/audio/MediaPlayerService;

    if-eqz v0, :cond_0

    .line 16
    const-string v0, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    .line 17
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 39
    :cond_0
    :goto_0
    return-void

    .line 19
    :sswitch_0
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    goto :goto_0

    .line 22
    :sswitch_1
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    goto :goto_0

    .line 25
    :sswitch_2
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    goto :goto_0

    .line 28
    :sswitch_3
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    goto :goto_0

    .line 31
    :sswitch_4
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    goto :goto_0

    .line 34
    :sswitch_5
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    goto :goto_0

    .line 17
    :sswitch_data_0
    .sparse-switch
        0x55 -> :sswitch_3
        0x56 -> :sswitch_1
        0x57 -> :sswitch_5
        0x58 -> :sswitch_4
        0x7e -> :sswitch_0
        0x7f -> :sswitch_2
    .end sparse-switch
.end method
