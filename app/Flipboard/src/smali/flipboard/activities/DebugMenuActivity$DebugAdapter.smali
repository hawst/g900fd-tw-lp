.class Lflipboard/activities/DebugMenuActivity$DebugAdapter;
.super Landroid/widget/BaseAdapter;
.source "DebugMenuActivity.java"


# instance fields
.field final synthetic a:Lflipboard/activities/DebugMenuActivity;


# direct methods
.method private constructor <init>(Lflipboard/activities/DebugMenuActivity;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lflipboard/activities/DebugMenuActivity$DebugAdapter;->a:Lflipboard/activities/DebugMenuActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflipboard/activities/DebugMenuActivity;B)V
    .locals 0

    .prologue
    .line 225
    invoke-direct {p0, p1}, Lflipboard/activities/DebugMenuActivity$DebugAdapter;-><init>(Lflipboard/activities/DebugMenuActivity;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$DebugAdapter;->a:Lflipboard/activities/DebugMenuActivity;

    invoke-static {v0}, Lflipboard/activities/DebugMenuActivity;->d(Lflipboard/activities/DebugMenuActivity;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$DebugAdapter;->a:Lflipboard/activities/DebugMenuActivity;

    invoke-static {v0}, Lflipboard/activities/DebugMenuActivity;->d(Lflipboard/activities/DebugMenuActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 233
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$DebugAdapter;->a:Lflipboard/activities/DebugMenuActivity;

    invoke-static {v0}, Lflipboard/activities/DebugMenuActivity;->d(Lflipboard/activities/DebugMenuActivity;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$DebugAdapter;->a:Lflipboard/activities/DebugMenuActivity;

    invoke-static {v0}, Lflipboard/activities/DebugMenuActivity;->d(Lflipboard/activities/DebugMenuActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 246
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 251
    .line 252
    if-nez p2, :cond_0

    .line 253
    iget-object v0, p0, Lflipboard/activities/DebugMenuActivity$DebugAdapter;->a:Lflipboard/activities/DebugMenuActivity;

    invoke-virtual {v0}, Lflipboard/activities/DebugMenuActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030117

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 256
    :cond_0
    const v0, 0x7f0a0308

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 257
    if-nez v0, :cond_1

    .line 261
    :goto_0
    return-object p2

    .line 260
    :cond_1
    iget-object v1, p0, Lflipboard/activities/DebugMenuActivity$DebugAdapter;->a:Lflipboard/activities/DebugMenuActivity;

    invoke-static {v1}, Lflipboard/activities/DebugMenuActivity;->d(Lflipboard/activities/DebugMenuActivity;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
