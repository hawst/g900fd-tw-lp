.class Lflipboard/activities/SettingsFragment$12;
.super Lflipboard/activities/SettingsFragment$RowItem;
.source "SettingsFragment.java"


# instance fields
.field final synthetic a:Lflipboard/activities/SettingsFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 557
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$12;->a:Lflipboard/activities/SettingsFragment;

    invoke-direct {p0, p2}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 582
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$12;->a:Lflipboard/activities/SettingsFragment;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lflipboard/activities/SettingsFragment$12;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v2}, Lflipboard/activities/SettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lflipboard/activities/MuteActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lflipboard/activities/SettingsFragment;->startActivity(Landroid/content/Intent;)V

    .line 584
    return-void
.end method

.method final b()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 560
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$12;->a:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->a(Lflipboard/activities/SettingsFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->k()Lflipboard/objs/UserState;

    move-result-object v0

    iget-object v3, v0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    .line 562
    iget-object v0, v3, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    if-eqz v0, :cond_4

    .line 563
    iget-object v0, v3, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 564
    iget-object v0, v3, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 566
    :goto_0
    iget-object v0, v3, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 567
    iget-object v0, v3, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    sget-object v3, Lflipboard/service/Section;->a:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 568
    if-eqz v0, :cond_0

    .line 569
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v1, v0

    .line 573
    :cond_0
    :goto_1
    if-lez v1, :cond_2

    .line 574
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$12;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-ne v1, v4, :cond_1

    const v0, 0x7f0d02cc

    :goto_2
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 575
    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v2

    invoke-static {v0, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 577
    :goto_3
    return-object v0

    .line 574
    :cond_1
    const v0, 0x7f0d02cb

    goto :goto_2

    .line 577
    :cond_2
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$12;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d02ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1
.end method
