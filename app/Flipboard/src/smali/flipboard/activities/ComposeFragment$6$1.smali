.class Lflipboard/activities/ComposeFragment$6$1;
.super Ljava/lang/Object;
.source "ComposeFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/activities/ComposeFragment$6;


# direct methods
.method constructor <init>(Lflipboard/activities/ComposeFragment$6;)V
    .locals 0

    .prologue
    .line 628
    iput-object p1, p0, Lflipboard/activities/ComposeFragment$6$1;->a:Lflipboard/activities/ComposeFragment$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 632
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    .line 633
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "pref_compose_account_id"

    iget-object v2, p0, Lflipboard/activities/ComposeFragment$6$1;->a:Lflipboard/activities/ComposeFragment$6;

    iget-object v2, v2, Lflipboard/activities/ComposeFragment$6;->b:Lflipboard/activities/ComposeFragment;

    invoke-static {v2}, Lflipboard/activities/ComposeFragment;->a(Lflipboard/activities/ComposeFragment;)Lflipboard/service/Account;

    move-result-object v2

    iget-object v2, v2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 635
    iget-object v0, p0, Lflipboard/activities/ComposeFragment$6$1;->a:Lflipboard/activities/ComposeFragment$6;

    iget-object v0, v0, Lflipboard/activities/ComposeFragment$6;->b:Lflipboard/activities/ComposeFragment;

    invoke-virtual {v0}, Lflipboard/activities/ComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 636
    if-eqz v0, :cond_1

    .line 637
    iget-boolean v1, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_0

    .line 638
    iget-object v1, p0, Lflipboard/activities/ComposeFragment$6$1;->a:Lflipboard/activities/ComposeFragment$6;

    iget-object v1, v1, Lflipboard/activities/ComposeFragment$6;->b:Lflipboard/activities/ComposeFragment;

    invoke-virtual {v1}, Lflipboard/activities/ComposeFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "uploading"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/DialogFragment;

    .line 639
    if-eqz v1, :cond_0

    .line 640
    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->a()V

    .line 644
    :cond_0
    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v1

    const v2, 0x7f0d0121

    invoke-virtual {v0, v2}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0201ce

    invoke-virtual {v1, v3, v2}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    .line 645
    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->finish()V

    .line 647
    :cond_1
    return-void
.end method
