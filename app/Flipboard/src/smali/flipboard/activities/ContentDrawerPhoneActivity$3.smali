.class Lflipboard/activities/ContentDrawerPhoneActivity$3;
.super Ljava/lang/Object;
.source "ContentDrawerPhoneActivity.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/ContentDrawerHandler;",
        "Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/activities/ContentDrawerPhoneActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/ContentDrawerPhoneActivity;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lflipboard/activities/ContentDrawerPhoneActivity$3;->a:Lflipboard/activities/ContentDrawerPhoneActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 116
    check-cast p2, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;

    sget-object v0, Lflipboard/activities/ContentDrawerPhoneActivity$6;->a:[I

    invoke-virtual {p2}, Lflipboard/service/ContentDrawerHandler$ContentDrawerHandlerMessage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity$3;->a:Lflipboard/activities/ContentDrawerPhoneActivity;

    iget-object v0, v0, Lflipboard/activities/ContentDrawerPhoneActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/ContentDrawerPhoneActivity$3$1;

    invoke-direct {v1, p0, p3}, Lflipboard/activities/ContentDrawerPhoneActivity$3$1;-><init>(Lflipboard/activities/ContentDrawerPhoneActivity$3;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity$3;->a:Lflipboard/activities/ContentDrawerPhoneActivity;

    iget-object v0, v0, Lflipboard/activities/ContentDrawerPhoneActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    if-eqz p3, :cond_0

    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity$3;->a:Lflipboard/activities/ContentDrawerPhoneActivity;

    iget-object v0, v0, Lflipboard/activities/ContentDrawerPhoneActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/ContentDrawerPhoneActivity$3$2;

    invoke-direct {v1, p0, p3}, Lflipboard/activities/ContentDrawerPhoneActivity$3$2;-><init>(Lflipboard/activities/ContentDrawerPhoneActivity$3;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lflipboard/activities/ContentDrawerPhoneActivity$3;->a:Lflipboard/activities/ContentDrawerPhoneActivity;

    iget-object v0, v0, Lflipboard/activities/ContentDrawerPhoneActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->F()Lflipboard/service/ContentDrawerHandler;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/service/ContentDrawerHandler;->a()V

    goto :goto_0

    :pswitch_4
    sget-object v0, Lflipboard/service/ContentDrawerHandler;->j:Lflipboard/util/Log;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
