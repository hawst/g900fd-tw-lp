.class Lflipboard/activities/FacebookAuthenticateFragment$3$1;
.super Ljava/lang/Object;
.source "FacebookAuthenticateFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager$CreateAccountMessage;

.field final synthetic b:Ljava/lang/Object;

.field final synthetic c:Lflipboard/activities/FacebookAuthenticateFragment$3;


# direct methods
.method constructor <init>(Lflipboard/activities/FacebookAuthenticateFragment$3;Lflipboard/service/FlipboardManager$CreateAccountMessage;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 393
    iput-object p1, p0, Lflipboard/activities/FacebookAuthenticateFragment$3$1;->c:Lflipboard/activities/FacebookAuthenticateFragment$3;

    iput-object p2, p0, Lflipboard/activities/FacebookAuthenticateFragment$3$1;->a:Lflipboard/service/FlipboardManager$CreateAccountMessage;

    iput-object p3, p0, Lflipboard/activities/FacebookAuthenticateFragment$3$1;->b:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const v3, 0x7f0201cf

    .line 397
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment$3$1;->c:Lflipboard/activities/FacebookAuthenticateFragment$3;

    iget-object v0, v0, Lflipboard/activities/FacebookAuthenticateFragment$3;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-static {v0}, Lflipboard/activities/FacebookAuthenticateFragment;->a(Lflipboard/activities/FacebookAuthenticateFragment;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 398
    iget-object v0, p0, Lflipboard/activities/FacebookAuthenticateFragment$3$1;->c:Lflipboard/activities/FacebookAuthenticateFragment$3;

    iget-object v0, v0, Lflipboard/activities/FacebookAuthenticateFragment$3;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-virtual {v0}, Lflipboard/activities/FacebookAuthenticateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 399
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_0

    .line 400
    iget-object v1, p0, Lflipboard/activities/FacebookAuthenticateFragment$3$1;->a:Lflipboard/service/FlipboardManager$CreateAccountMessage;

    sget-object v2, Lflipboard/service/FlipboardManager$CreateAccountMessage;->a:Lflipboard/service/FlipboardManager$CreateAccountMessage;

    if-ne v1, v2, :cond_1

    .line 401
    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "extra_invite"

    iget-object v4, p0, Lflipboard/activities/FacebookAuthenticateFragment$3$1;->c:Lflipboard/activities/FacebookAuthenticateFragment$3;

    iget-object v4, v4, Lflipboard/activities/FacebookAuthenticateFragment$3;->a:Lflipboard/activities/FacebookAuthenticateFragment;

    invoke-static {v4}, Lflipboard/activities/FacebookAuthenticateFragment;->c(Lflipboard/activities/FacebookAuthenticateFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/activities/FlipboardActivity;->setResult(ILandroid/content/Intent;)V

    .line 402
    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->finish()V

    .line 412
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    iget-object v1, p0, Lflipboard/activities/FacebookAuthenticateFragment$3$1;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 405
    if-eqz v1, :cond_2

    .line 406
    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v0

    invoke-virtual {v0, v3, v1}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 408
    :cond_2
    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v1

    const v2, 0x7f0d00be

    invoke-virtual {v0, v2}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    goto :goto_0
.end method
