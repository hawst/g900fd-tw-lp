.class Lflipboard/activities/SettingsFragment$33;
.super Lflipboard/activities/SettingsFragment$RowItem;
.source "SettingsFragment.java"


# instance fields
.field final synthetic a:Lflipboard/activities/SettingsFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1025
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$33;->a:Lflipboard/activities/SettingsFragment;

    invoke-direct {p0, p2}, Lflipboard/activities/SettingsFragment$RowItem;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1028
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 1029
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-object v0, v0, Lflipboard/app/FlipboardApplication;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    .line 1030
    const/4 v0, 0x0

    const-string v1, "Not forced"

    aput-object v1, v3, v0

    .line 1031
    const/4 v0, 0x1

    .line 1032
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-object v1, v1, Lflipboard/app/FlipboardApplication;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SectionPageTemplate;

    .line 1033
    iget-object v0, v0, Lflipboard/objs/SectionPageTemplate;->a:Ljava/lang/String;

    aput-object v0, v3, v1

    .line 1034
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1035
    goto :goto_0

    .line 1036
    :cond_0
    iput-object v3, v2, Lflipboard/gui/dialog/FLAlertDialogFragment;->q:[Ljava/lang/CharSequence;

    .line 1037
    new-instance v0, Lflipboard/activities/SettingsFragment$33$1;

    invoke-direct {v0, p0}, Lflipboard/activities/SettingsFragment$33$1;-><init>(Lflipboard/activities/SettingsFragment$33;)V

    iput-object v0, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 1051
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$33;->a:Lflipboard/activities/SettingsFragment;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "pick_layout_force"

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1052
    return-void
.end method

.method final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1056
    sget-object v0, Lflipboard/activities/SettingsFragment;->f:Lflipboard/objs/SectionPageTemplate;

    if-nez v0, :cond_0

    const-string v0, "Not forced"

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lflipboard/activities/SettingsFragment;->f:Lflipboard/objs/SectionPageTemplate;

    iget-object v0, v0, Lflipboard/objs/SectionPageTemplate;->a:Ljava/lang/String;

    goto :goto_0
.end method
