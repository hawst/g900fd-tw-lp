.class Lflipboard/activities/SharePromoteFragment$4;
.super Ljava/lang/Object;
.source "SharePromoteFragment.java"

# interfaces
.implements Lflipboard/service/Flap$CancellableJSONResultObserver;


# instance fields
.field final synthetic a:Ljava/util/Set;

.field final synthetic b:Lflipboard/activities/SharePromoteFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SharePromoteFragment;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lflipboard/activities/SharePromoteFragment$4;->b:Lflipboard/activities/SharePromoteFragment;

    iput-object p2, p0, Lflipboard/activities/SharePromoteFragment$4;->a:Ljava/util/Set;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 244
    sget-object v0, Lflipboard/activities/SharePromoteFragment;->j:Lflipboard/util/Log;

    new-array v0, v5, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 249
    iget-object v0, p0, Lflipboard/activities/SharePromoteFragment$4;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 250
    iget-object v2, p0, Lflipboard/activities/SharePromoteFragment$4;->b:Lflipboard/activities/SharePromoteFragment;

    invoke-static {v2}, Lflipboard/activities/SharePromoteFragment;->d(Lflipboard/activities/SharePromoteFragment;)I

    move-result v2

    iget-object v3, p0, Lflipboard/activities/SharePromoteFragment$4;->b:Lflipboard/activities/SharePromoteFragment;

    invoke-static {v3}, Lflipboard/activities/SharePromoteFragment;->e(Lflipboard/activities/SharePromoteFragment;)Lflipboard/objs/Magazine;

    move-result-object v3

    iget-object v3, v3, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    iget-object v4, p0, Lflipboard/activities/SharePromoteFragment$4;->b:Lflipboard/activities/SharePromoteFragment;

    invoke-static {v4}, Lflipboard/activities/SharePromoteFragment;->f(Lflipboard/activities/SharePromoteFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v2, v3, v4, v0}, Lflipboard/io/UsageEvent;->a(ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 253
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/SharePromoteFragment$4$1;

    invoke-direct {v1, p0}, Lflipboard/activities/SharePromoteFragment$4$1;-><init>(Lflipboard/activities/SharePromoteFragment$4;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 260
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 264
    sget-object v0, Lflipboard/activities/SharePromoteFragment;->j:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v5

    .line 269
    iget-object v0, p0, Lflipboard/activities/SharePromoteFragment$4;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 270
    iget-object v2, p0, Lflipboard/activities/SharePromoteFragment$4;->b:Lflipboard/activities/SharePromoteFragment;

    invoke-static {v2}, Lflipboard/activities/SharePromoteFragment;->d(Lflipboard/activities/SharePromoteFragment;)I

    move-result v2

    iget-object v3, p0, Lflipboard/activities/SharePromoteFragment$4;->b:Lflipboard/activities/SharePromoteFragment;

    invoke-static {v3}, Lflipboard/activities/SharePromoteFragment;->e(Lflipboard/activities/SharePromoteFragment;)Lflipboard/objs/Magazine;

    move-result-object v3

    iget-object v3, v3, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    iget-object v4, p0, Lflipboard/activities/SharePromoteFragment$4;->b:Lflipboard/activities/SharePromoteFragment;

    invoke-static {v4}, Lflipboard/activities/SharePromoteFragment;->f(Lflipboard/activities/SharePromoteFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v2, v3, v4, v0}, Lflipboard/io/UsageEvent;->a(ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 273
    :cond_0
    iget-object v0, p0, Lflipboard/activities/SharePromoteFragment$4;->b:Lflipboard/activities/SharePromoteFragment;

    invoke-virtual {v0}, Lflipboard/activities/SharePromoteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 274
    if-nez v0, :cond_1

    .line 291
    :goto_1
    return-void

    .line 277
    :cond_1
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/activities/SharePromoteFragment$4$2;

    invoke-direct {v2, p0, v0}, Lflipboard/activities/SharePromoteFragment$4$2;-><init>(Lflipboard/activities/SharePromoteFragment$4;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public cancel()V
    .locals 2

    .prologue
    .line 295
    sget-object v0, Lflipboard/activities/SharePromoteFragment;->j:Lflipboard/util/Log;

    .line 296
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/SharePromoteFragment$4$3;

    invoke-direct {v1, p0}, Lflipboard/activities/SharePromoteFragment$4$3;-><init>(Lflipboard/activities/SharePromoteFragment$4;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 303
    return-void
.end method
