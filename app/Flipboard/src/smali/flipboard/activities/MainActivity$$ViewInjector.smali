.class public Lflipboard/activities/MainActivity$$ViewInjector;
.super Ljava/lang/Object;
.source "MainActivity$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/activities/MainActivity;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a027c

    const-string v1, "field \'pager\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p1, Lflipboard/activities/MainActivity;->n:Landroid/support/v4/view/ViewPager;

    .line 12
    const v0, 0x7f0a027b

    const-string v1, "field \'slidingTabLayout\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/tabs/SlidingTabLayout;

    iput-object v0, p1, Lflipboard/activities/MainActivity;->o:Lflipboard/gui/tabs/SlidingTabLayout;

    .line 14
    return-void
.end method

.method public static reset(Lflipboard/activities/MainActivity;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lflipboard/activities/MainActivity;->n:Landroid/support/v4/view/ViewPager;

    .line 18
    iput-object v0, p0, Lflipboard/activities/MainActivity;->o:Lflipboard/gui/tabs/SlidingTabLayout;

    .line 19
    return-void
.end method
