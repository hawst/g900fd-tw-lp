.class public Lflipboard/activities/ProfileActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "ProfileActivity.java"


# instance fields
.field n:Lflipboard/gui/actionbar/FLActionBar;

.field o:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 32
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    const v0, 0x7f03000d

    invoke-virtual {p0, v0}, Lflipboard/activities/ProfileActivity;->setContentView(I)V

    .line 34
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/app/Activity;)V

    .line 35
    iget-object v0, p0, Lflipboard/activities/ProfileActivity;->n:Lflipboard/gui/actionbar/FLActionBar;

    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 36
    iget-object v0, p0, Lflipboard/activities/ProfileActivity;->n:Lflipboard/gui/actionbar/FLActionBar;

    const v1, 0x7f0a004d

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    const v1, 0x7f0d024c

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(I)V

    .line 38
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    .line 39
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/ProfileActivity;->o:Landroid/view/View;

    .line 40
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {}, Lflipboard/gui/section/ProfileFragmentScrolling;->a()Lflipboard/gui/section/ProfileFragmentScrolling;

    move-result-object v2

    const-string v3, "PROFILE"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 42
    return-void
.end method
