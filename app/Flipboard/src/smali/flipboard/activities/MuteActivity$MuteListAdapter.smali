.class Lflipboard/activities/MuteActivity$MuteListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "MuteActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/activities/MuteActivity$MuteListAdapter$Author;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/activities/MuteActivity$MuteListAdapter$Author;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic c:Lflipboard/activities/MuteActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/MuteActivity;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 90
    iput-object p1, p0, Lflipboard/activities/MuteActivity$MuteListAdapter;->c:Lflipboard/activities/MuteActivity;

    .line 91
    invoke-direct {p0, p1, v5, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 94
    iget-object v0, p1, Lflipboard/activities/MuteActivity;->o:Lflipboard/objs/UserState$State;

    iget-object v0, v0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 95
    new-instance v0, Lflipboard/activities/MuteActivity$MuteListAdapter$Header;

    const v1, 0x7f0d0174

    invoke-virtual {p1, v1}, Lflipboard/activities/MuteActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lflipboard/activities/MuteActivity$MuteListAdapter$Header;-><init>(Lflipboard/activities/MuteActivity$MuteListAdapter;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lflipboard/activities/MuteActivity$MuteListAdapter;->add(Ljava/lang/Object;)V

    .line 96
    iget-object v0, p1, Lflipboard/activities/MuteActivity;->o:Lflipboard/objs/UserState$State;

    iget-object v0, v0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lflipboard/activities/MuteActivity$MuteListAdapter;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/MuteActivity$MuteListAdapter;->a:Ljava/util/List;

    .line 100
    :cond_0
    iget-object v0, p1, Lflipboard/activities/MuteActivity;->o:Lflipboard/objs/UserState$State;

    iget-object v0, v0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->d:Ljava/util/Map;

    sget-object v1, Lflipboard/service/Section;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 101
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 102
    new-instance v1, Lflipboard/activities/MuteActivity$MuteListAdapter$Header;

    const v2, 0x7f0d0175

    invoke-virtual {p1, v2}, Lflipboard/activities/MuteActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const v4, 0x7f0d00b7

    invoke-virtual {p1, v4}, Lflipboard/activities/MuteActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lflipboard/activities/MuteActivity$MuteListAdapter$Header;-><init>(Lflipboard/activities/MuteActivity$MuteListAdapter;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lflipboard/activities/MuteActivity$MuteListAdapter;->add(Ljava/lang/Object;)V

    .line 103
    invoke-direct {p0, v0}, Lflipboard/activities/MuteActivity$MuteListAdapter;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/activities/MuteActivity$MuteListAdapter;->b:Ljava/util/List;

    .line 105
    :cond_1
    return-void
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/UserState$MutedAuthor;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lflipboard/activities/MuteActivity$MuteListAdapter$Author;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 116
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserState$MutedAuthor;

    .line 117
    new-instance v3, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;

    invoke-direct {v3, p0, v0}, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;-><init>(Lflipboard/activities/MuteActivity$MuteListAdapter;Lflipboard/objs/UserState$MutedAuthor;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 121
    :cond_0
    new-instance v0, Lflipboard/activities/MuteActivity$MuteListAdapter$1;

    invoke-direct {v0, p0}, Lflipboard/activities/MuteActivity$MuteListAdapter$1;-><init>(Lflipboard/activities/MuteActivity$MuteListAdapter;)V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 129
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;

    .line 130
    invoke-virtual {p0, v0}, Lflipboard/activities/MuteActivity$MuteListAdapter;->add(Ljava/lang/Object;)V

    goto :goto_1

    .line 133
    :cond_1
    return-object v1
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 138
    iget-object v0, p0, Lflipboard/activities/MuteActivity$MuteListAdapter;->c:Lflipboard/activities/MuteActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lflipboard/activities/MuteActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 141
    invoke-virtual {p0, p1}, Lflipboard/activities/MuteActivity$MuteListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 142
    instance-of v2, v1, Lflipboard/activities/MuteActivity$MuteListAdapter$Header;

    if-eqz v2, :cond_0

    .line 144
    const v2, 0x7f03004f

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 145
    const v0, 0x7f0a004f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 146
    check-cast v1, Lflipboard/activities/MuteActivity$MuteListAdapter$Header;

    iget-object v1, v1, Lflipboard/activities/MuteActivity$MuteListAdapter$Header;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v2

    .line 176
    :goto_0
    return-object v0

    .line 147
    :cond_0
    instance-of v2, v1, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;

    if-eqz v2, :cond_2

    move-object v2, v1

    .line 148
    check-cast v2, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;

    .line 149
    const v4, 0x7f0300dc

    invoke-virtual {v0, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 152
    iget-object v0, p0, Lflipboard/activities/MuteActivity$MuteListAdapter;->c:Lflipboard/activities/MuteActivity;

    iget-object v0, v0, Lflipboard/activities/MuteActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v4, v2, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;->a:Lflipboard/objs/UserState$MutedAuthor;

    iget-object v4, v4, Lflipboard/objs/UserState$MutedAuthor;->d:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v4

    .line 153
    const v0, 0x7f0a00a7

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 154
    if-eqz v0, :cond_1

    iget-object v5, v4, Lflipboard/objs/ConfigService;->o:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 155
    iget-object v4, v4, Lflipboard/objs/ConfigService;->o:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 159
    :cond_1
    const v0, 0x7f0a0274

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 160
    invoke-virtual {v2}, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 163
    const v0, 0x7f0a027f

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 164
    check-cast v1, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;

    iget-boolean v1, v1, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;->b:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 166
    new-instance v1, Lflipboard/activities/MuteActivity$MuteListAdapter$2;

    invoke-direct {v1, p0, v2, v0}, Lflipboard/activities/MuteActivity$MuteListAdapter$2;-><init>(Lflipboard/activities/MuteActivity$MuteListAdapter;Lflipboard/activities/MuteActivity$MuteListAdapter$Author;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, v3

    goto :goto_0

    :cond_2
    move-object v0, v3

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 182
    invoke-virtual {p0, p1}, Lflipboard/activities/MuteActivity$MuteListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 183
    instance-of v0, v0, Lflipboard/activities/MuteActivity$MuteListAdapter$Author;

    return v0
.end method
