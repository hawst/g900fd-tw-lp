.class Lflipboard/activities/SettingsDensityActivity$SettingsAdapter$1;
.super Ljava/lang/Object;
.source "SettingsDensityActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/activities/SettingsDensityActivity$RowItem;

.field final synthetic b:Landroid/widget/CheckBox;

.field final synthetic c:Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;Lflipboard/activities/SettingsDensityActivity$RowItem;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter$1;->c:Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;

    iput-object p2, p0, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter$1;->a:Lflipboard/activities/SettingsDensityActivity$RowItem;

    iput-object p3, p0, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter$1;->b:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 262
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter$1;->c:Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;

    iget-object v0, v0, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;->a:Lflipboard/activities/SettingsDensityActivity;

    iget-object v0, v0, Lflipboard/activities/SettingsDensityActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    :goto_0
    return-void

    .line 266
    :cond_0
    iget-object v0, p0, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter$1;->c:Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;

    iget-object v0, v0, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter;->a:Lflipboard/activities/SettingsDensityActivity;

    invoke-static {v0}, Lflipboard/activities/SettingsDensityActivity;->d(Lflipboard/activities/SettingsDensityActivity;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter$1;->a:Lflipboard/activities/SettingsDensityActivity$RowItem;

    iget-object v1, v1, Lflipboard/activities/SettingsDensityActivity$RowItem;->i:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/activities/SettingsDensityActivity$SettingsAdapter$1;->b:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
