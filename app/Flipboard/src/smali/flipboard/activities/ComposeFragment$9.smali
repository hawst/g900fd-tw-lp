.class Lflipboard/activities/ComposeFragment$9;
.super Ljava/lang/Object;
.source "ComposeFragment.java"

# interfaces
.implements Lflipboard/service/Flap$SectionListObserver;


# instance fields
.field final synthetic a:Lflipboard/service/Account;

.field final synthetic b:Lflipboard/objs/ConfigService;

.field final synthetic c:Lflipboard/activities/ComposeFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/ComposeFragment;Lflipboard/service/Account;Lflipboard/objs/ConfigService;)V
    .locals 0

    .prologue
    .line 778
    iput-object p1, p0, Lflipboard/activities/ComposeFragment$9;->c:Lflipboard/activities/ComposeFragment;

    iput-object p2, p0, Lflipboard/activities/ComposeFragment$9;->a:Lflipboard/service/Account;

    iput-object p3, p0, Lflipboard/activities/ComposeFragment$9;->b:Lflipboard/objs/ConfigService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 778
    check-cast p1, Lflipboard/objs/SectionListResult;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/ComposeFragment$9$1;

    invoke-direct {v1, p0, p1}, Lflipboard/activities/ComposeFragment$9$1;-><init>(Lflipboard/activities/ComposeFragment$9;Lflipboard/objs/SectionListResult;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 837
    invoke-static {}, Lflipboard/activities/ComposeFragment;->c()Lflipboard/util/Log;

    move-result-object v0

    const-string v1, "failed to fetch share targets for %s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/activities/ComposeFragment$9;->b:Lflipboard/objs/ConfigService;

    invoke-virtual {v4}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 838
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/ComposeFragment$9$2;

    invoke-direct {v1, p0}, Lflipboard/activities/ComposeFragment$9$2;-><init>(Lflipboard/activities/ComposeFragment$9;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 859
    return-void
.end method
