.class Lflipboard/activities/FlipboardDreamSettings$Adapter;
.super Landroid/widget/BaseAdapter;
.source "FlipboardDreamSettings.java"


# instance fields
.field final synthetic a:Lflipboard/activities/FlipboardDreamSettings;


# direct methods
.method constructor <init>(Lflipboard/activities/FlipboardDreamSettings;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lflipboard/activities/FlipboardDreamSettings$Adapter;->a:Lflipboard/activities/FlipboardDreamSettings;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x2

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 115
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 123
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 127
    if-nez p1, :cond_2

    .line 129
    if-nez p2, :cond_0

    .line 130
    iget-object v0, p0, Lflipboard/activities/FlipboardDreamSettings$Adapter;->a:Lflipboard/activities/FlipboardDreamSettings;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardDreamSettings;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030119

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 133
    :cond_0
    const v0, 0x7f0a004f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 134
    const-string v1, ""

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 145
    :cond_1
    :goto_0
    return-object p2

    .line 135
    :cond_2
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 136
    iget-object v0, p0, Lflipboard/activities/FlipboardDreamSettings$Adapter;->a:Lflipboard/activities/FlipboardDreamSettings;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardDreamSettings;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030117

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 137
    const v0, 0x7f0a0308

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 138
    const v1, 0x7f0a0309

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    .line 140
    iget-object v2, p0, Lflipboard/activities/FlipboardDreamSettings$Adapter;->a:Lflipboard/activities/FlipboardDreamSettings;

    invoke-virtual {v2}, Lflipboard/activities/FlipboardDreamSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d02b3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v0, p0, Lflipboard/activities/FlipboardDreamSettings$Adapter;->a:Lflipboard/activities/FlipboardDreamSettings;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardDreamSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v2, p0, Lflipboard/activities/FlipboardDreamSettings$Adapter;->a:Lflipboard/activities/FlipboardDreamSettings;

    invoke-static {v2}, Lflipboard/activities/FlipboardDreamSettings;->c(Lflipboard/activities/FlipboardDreamSettings;)[I

    move-result-object v2

    iget-object v3, p0, Lflipboard/activities/FlipboardDreamSettings$Adapter;->a:Lflipboard/activities/FlipboardDreamSettings;

    invoke-static {v3}, Lflipboard/activities/FlipboardDreamSettings;->a(Lflipboard/activities/FlipboardDreamSettings;)Lflipboard/activities/FlipboardDreamSettings$UpdateType;

    move-result-object v3

    invoke-virtual {v3}, Lflipboard/activities/FlipboardDreamSettings$UpdateType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 142
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x2

    return v0
.end method
