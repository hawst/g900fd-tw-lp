.class public Lflipboard/activities/GenericFragmentActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "GenericFragmentActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 132
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/GenericFragmentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 133
    const-string v1, "fragment_title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    const-string v1, "fragment_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 135
    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 51
    invoke-virtual {p0}, Lflipboard/activities/GenericFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "fragment_type"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 52
    const/4 v0, 0x6

    if-ne v2, v0, :cond_0

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_0

    .line 53
    const v0, 0x7f0e0018

    invoke-virtual {p0, v0}, Lflipboard/activities/GenericFragmentActivity;->setTheme(I)V

    .line 55
    :cond_0
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    const/16 v0, 0x9

    if-eq v2, v0, :cond_1

    const/4 v0, 0x7

    if-eq v2, v0, :cond_1

    const/16 v0, 0xb

    if-ne v2, v0, :cond_5

    .line 58
    :cond_1
    const v0, 0x7f030074

    invoke-virtual {p0, v0}, Lflipboard/activities/GenericFragmentActivity;->setContentView(I)V

    .line 67
    :cond_2
    :goto_0
    const/4 v1, 0x0

    .line 68
    invoke-virtual {p0}, Lflipboard/activities/GenericFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "fragment_title"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 70
    packed-switch v2, :pswitch_data_0

    :pswitch_0
    move-object v2, v1

    move-object v1, v0

    .line 121
    :goto_1
    if-eqz v1, :cond_3

    .line 122
    const v0, 0x7f0a004d

    invoke-virtual {p0, v0}, Lflipboard/activities/GenericFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 123
    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    :cond_3
    if-eqz v2, :cond_4

    .line 127
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0a004e

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 129
    :cond_4
    return-void

    .line 60
    :cond_5
    const v0, 0x7f030075

    invoke-virtual {p0, v0}, Lflipboard/activities/GenericFragmentActivity;->setContentView(I)V

    .line 61
    invoke-virtual {p0}, Lflipboard/activities/GenericFragmentActivity;->l()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    .line 62
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 63
    const/4 v1, 0x4

    if-eq v2, v1, :cond_6

    const/16 v1, 0xc

    if-ne v2, v1, :cond_2

    .line 64
    :cond_6
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->e()V

    goto :goto_0

    .line 72
    :pswitch_1
    new-instance v1, Lflipboard/gui/personal/NotificationsFragment;

    invoke-direct {v1}, Lflipboard/gui/personal/NotificationsFragment;-><init>()V

    move-object v2, v1

    move-object v1, v0

    .line 73
    goto :goto_1

    .line 75
    :pswitch_2
    new-instance v1, Lflipboard/gui/personal/SocialNetworksFragment;

    invoke-direct {v1}, Lflipboard/gui/personal/SocialNetworksFragment;-><init>()V

    move-object v2, v1

    move-object v1, v0

    .line 76
    goto :goto_1

    .line 78
    :pswitch_3
    new-instance v1, Lflipboard/gui/debug/NetworkRequestListFragment;

    invoke-direct {v1}, Lflipboard/gui/debug/NetworkRequestListFragment;-><init>()V

    move-object v2, v1

    move-object v1, v0

    .line 79
    goto :goto_1

    .line 81
    :pswitch_4
    new-instance v1, Lflipboard/gui/section/UserListFragment;

    invoke-direct {v1}, Lflipboard/gui/section/UserListFragment;-><init>()V

    .line 82
    invoke-virtual {p0}, Lflipboard/activities/GenericFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 83
    invoke-virtual {p0}, Lflipboard/activities/GenericFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "listType"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/service/Flap$FollowListType;->valueOf(Ljava/lang/String;)Lflipboard/service/Flap$FollowListType;

    move-result-object v0

    .line 84
    invoke-static {v0}, Lflipboard/gui/section/UserListFragment;->a(Lflipboard/service/Flap$FollowListType;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v1

    move-object v1, v0

    .line 85
    goto :goto_1

    .line 87
    :pswitch_5
    new-instance v1, Lflipboard/gui/section/SuggestedUsersFragment;

    invoke-direct {v1}, Lflipboard/gui/section/SuggestedUsersFragment;-><init>()V

    .line 88
    invoke-virtual {p0}, Lflipboard/activities/GenericFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 89
    const v0, 0x7f0d00ee

    invoke-virtual {p0, v0}, Lflipboard/activities/GenericFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 91
    iput-boolean v4, p0, Lflipboard/activities/GenericFragmentActivity;->W:Z

    move-object v2, v1

    move-object v1, v0

    .line 92
    goto/16 :goto_1

    .line 94
    :pswitch_6
    new-instance v1, Lflipboard/gui/section/MagazineConversationThreadsFragment;

    invoke-direct {v1}, Lflipboard/gui/section/MagazineConversationThreadsFragment;-><init>()V

    .line 96
    invoke-virtual {p0}, Lflipboard/activities/GenericFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "feedItemId"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 97
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 98
    const-string v4, "feedItemId"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-virtual {v1, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    move-object v2, v1

    move-object v1, v0

    .line 100
    goto/16 :goto_1

    .line 102
    :pswitch_7
    invoke-virtual {p0}, Lflipboard/activities/GenericFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_section_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 103
    invoke-static {v1}, Lflipboard/gui/section/SectionFragmentScrolling;->a(Ljava/lang/String;)Lflipboard/gui/section/SectionFragmentScrolling;

    move-result-object v1

    move-object v2, v1

    move-object v1, v0

    .line 104
    goto/16 :goto_1

    .line 106
    :pswitch_8
    invoke-virtual {p0}, Lflipboard/activities/GenericFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_section_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 107
    invoke-static {v1}, Lflipboard/gui/section/SectionFragmentScrolling;->a(Ljava/lang/String;)Lflipboard/gui/section/SectionFragmentScrolling;

    move-result-object v1

    move-object v2, v1

    move-object v1, v0

    .line 108
    goto/16 :goto_1

    .line 110
    :pswitch_9
    invoke-virtual {p0}, Lflipboard/activities/GenericFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_section_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 111
    invoke-static {v1}, Lflipboard/gui/section/MagazineGridFragment;->a(Ljava/lang/String;)Lflipboard/gui/section/MagazineGridFragment;

    move-result-object v1

    move-object v2, v1

    move-object v1, v0

    .line 112
    goto/16 :goto_1

    .line 114
    :pswitch_a
    invoke-static {}, Lflipboard/activities/CategoryPickerFragment;->a()Lflipboard/activities/CategoryPickerFragment;

    move-result-object v1

    move-object v2, v1

    move-object v1, v0

    .line 115
    goto/16 :goto_1

    .line 117
    :pswitch_b
    new-instance v1, Lflipboard/gui/section/EmptyMagazineFragment;

    invoke-direct {v1}, Lflipboard/gui/section/EmptyMagazineFragment;-><init>()V

    move-object v2, v1

    move-object v1, v0

    goto/16 :goto_1

    .line 70
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public onDisableSwipeBack(Lflipboard/activities/FlipboardActivity$ToggleSwipeBackGestureEvent;)V
    .locals 1
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 161
    iget-boolean v0, p1, Lflipboard/activities/FlipboardActivity$ToggleSwipeBackGestureEvent;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lflipboard/activities/GenericFragmentActivity;->W:Z

    .line 162
    return-void

    .line 161
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
