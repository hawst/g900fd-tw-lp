.class Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;
.super Landroid/view/View;
.source "DebugLayoutActivity.java"


# instance fields
.field a:Landroid/graphics/Paint;

.field final synthetic b:Lflipboard/activities/DebugLayoutActivity;

.field private final c:Lflipboard/objs/SectionPageTemplate;

.field private final d:Z


# direct methods
.method public constructor <init>(Lflipboard/activities/DebugLayoutActivity;Landroid/content/Context;Lflipboard/objs/SectionPageTemplate;Z)V
    .locals 3

    .prologue
    .line 133
    iput-object p1, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->b:Lflipboard/activities/DebugLayoutActivity;

    .line 134
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 131
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->a:Landroid/graphics/Paint;

    .line 135
    iput-object p3, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->c:Lflipboard/objs/SectionPageTemplate;

    .line 136
    iput-boolean p4, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->d:Z

    .line 137
    iget-object v0, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->a:Landroid/graphics/Paint;

    const/high16 v1, 0x42000000    # 32.0f

    invoke-virtual {p0}, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 138
    iget-object v0, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->a:Landroid/graphics/Paint;

    iget-object v1, p1, Lflipboard/activities/DebugLayoutActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->z:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 139
    iget-object v0, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->a:Landroid/graphics/Paint;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/graphics/Paint;)V

    .line 140
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    .line 157
    invoke-virtual {p0}, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->getWidth()I

    move-result v7

    .line 158
    invoke-virtual {p0}, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->getHeight()I

    move-result v8

    .line 159
    iget-object v0, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->a:Landroid/graphics/Paint;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 160
    const/4 v1, 0x0

    const/4 v2, 0x0

    int-to-float v3, v7

    int-to-float v4, v8

    iget-object v5, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 161
    const/4 v0, 0x0

    .line 162
    iget-object v1, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->c:Lflipboard/objs/SectionPageTemplate;

    iget-boolean v2, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->d:Z

    invoke-virtual {v1, v2}, Lflipboard/objs/SectionPageTemplate;->a(Z)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v6, v0

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SectionPageTemplate$Area;

    .line 163
    iget-boolean v1, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->d:Z

    invoke-virtual {v0, v1}, Lflipboard/objs/SectionPageTemplate$Area;->c(Z)F

    move-result v1

    int-to-float v2, v7

    mul-float v10, v1, v2

    .line 164
    iget-boolean v1, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->d:Z

    invoke-virtual {v0, v1}, Lflipboard/objs/SectionPageTemplate$Area;->d(Z)F

    move-result v1

    int-to-float v2, v8

    mul-float v11, v1, v2

    .line 165
    iget-boolean v1, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->d:Z

    invoke-virtual {v0, v1}, Lflipboard/objs/SectionPageTemplate$Area;->a(Z)F

    move-result v1

    int-to-float v2, v7

    mul-float/2addr v1, v2

    add-float v12, v10, v1

    .line 166
    iget-boolean v1, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->d:Z

    invoke-virtual {v0, v1}, Lflipboard/objs/SectionPageTemplate$Area;->b(Z)F

    move-result v0

    int-to-float v1, v8

    mul-float/2addr v0, v1

    add-float v13, v11, v0

    .line 167
    div-int/lit8 v0, v6, 0x3

    mul-int/lit8 v0, v0, 0x3c

    rem-int/lit16 v0, v0, 0x80

    add-int/lit16 v0, v0, 0x80

    rem-int/lit8 v1, v6, 0x3

    if-nez v1, :cond_0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    .line 168
    :goto_1
    iget-object v1, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 169
    const/high16 v0, 0x40000000    # 2.0f

    add-float v1, v10, v0

    const/high16 v0, 0x40000000    # 2.0f

    add-float v2, v11, v0

    const/high16 v0, 0x40000000    # 2.0f

    sub-float v3, v12, v0

    const/high16 v0, 0x40000000    # 2.0f

    sub-float v4, v13, v0

    iget-object v5, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 171
    iget-object v0, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->a:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 172
    add-int/lit8 v0, v6, 0x1

    .line 174
    sub-float v1, v12, v10

    .line 175
    sub-float v2, v13, v11

    .line 176
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 177
    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v1, v4

    add-float/2addr v1, v10

    iget-object v4, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->a:Landroid/graphics/Paint;

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    sub-float/2addr v1, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    add-float/2addr v2, v11

    iget-object v4, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->a:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getTextSize()F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v2, v4

    iget-object v4, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v1, v2, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move v6, v0

    .line 178
    goto/16 :goto_0

    .line 167
    :cond_0
    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_1

    .line 179
    :cond_2
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 180
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    .line 144
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 145
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v1

    .line 146
    invoke-static {}, Lflipboard/util/AndroidUtil;->d()I

    move-result v2

    .line 147
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    int-to-float v3, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    div-float v1, v3, v1

    .line 148
    iget-boolean v2, p0, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->d:Z

    if-eqz v2, :cond_0

    .line 149
    int-to-float v2, v0

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->setMeasuredDimension(II)V

    .line 153
    :goto_0
    return-void

    .line 151
    :cond_0
    int-to-float v2, v0

    div-float v1, v2, v1

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/DebugLayoutActivity$DebugTemplateView;->setMeasuredDimension(II)V

    goto :goto_0
.end method
