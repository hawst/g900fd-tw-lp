.class public abstract Lflipboard/activities/ContentDrawerActivity;
.super Lflipboard/activities/FlipboardActivity;
.source "ContentDrawerActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field public static final n:Ljava/lang/String;

.field public static o:Lflipboard/util/Log;


# instance fields
.field protected p:Lflipboard/gui/FLViewFlipper;

.field protected q:Lflipboard/gui/ContentDrawerView;

.field r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;

.field private t:Lflipboard/io/UsageEvent;

.field private u:I

.field private v:Lflipboard/gui/FLPopoverWindow;

.field private w:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/User;",
            "Lflipboard/service/User$Message;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    const-string v0, "_favorites_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lflipboard/activities/ContentDrawerActivity;->n:Ljava/lang/String;

    .line 102
    const-string v0, "contentdrawer"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/activities/ContentDrawerActivity;->o:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lflipboard/activities/FlipboardActivity;-><init>()V

    return-void
.end method

.method private I()V
    .locals 6

    .prologue
    .line 617
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    if-eqz v0, :cond_0

    .line 618
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    iget-wide v4, v1, Lflipboard/io/UsageEvent;->e:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lflipboard/io/UsageEvent;->g:J

    .line 619
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    invoke-virtual {v0}, Lflipboard/io/UsageEvent;->a()V

    .line 621
    :cond_0
    return-void
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 1043
    invoke-static {p0}, Lflipboard/activities/ContentDrawerActivity;->d(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    .line 1044
    const-string v1, "open_in_notifications"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1045
    invoke-static {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 1046
    return-void
.end method

.method public static a(Landroid/app/Activity;I)V
    .locals 3

    .prologue
    .line 1035
    invoke-static {p0}, Lflipboard/activities/ContentDrawerActivity;->d(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    .line 1036
    const-string v1, "open_in_favorites"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1037
    const-string v1, "favorites_position"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1038
    invoke-static {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 1039
    return-void
.end method

.method private static a(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1084
    instance-of v0, p0, Lflipboard/activities/FeedActivity;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 1085
    check-cast v0, Lflipboard/activities/FeedActivity;

    .line 1086
    iget-object v1, v0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    if-eqz v1, :cond_0

    .line 1087
    const-string v1, "extra_origin_section_id"

    iget-object v0, v0, Lflipboard/activities/FeedActivity;->C:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1090
    :cond_0
    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 1091
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_1

    .line 1092
    const v0, 0x7f040019

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 1094
    :cond_1
    return-void
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1108
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v1, "openedContentGuide"

    invoke-virtual {v0, v1}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    .line 1109
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    .line 1110
    new-instance v1, Landroid/content/Intent;

    if-eqz v0, :cond_1

    const-class v0, Lflipboard/activities/ContentDrawerTabletActivity;

    :goto_0
    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1111
    const-string v0, "profile"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1112
    const-string v0, "open_in_favorites"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1114
    :cond_0
    const-string v0, "selection_path"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1115
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 1116
    return-void

    .line 1110
    :cond_1
    const-class v0, Lflipboard/activities/ContentDrawerPhoneActivity;

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/activities/ContentDrawerActivity;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lflipboard/activities/ContentDrawerActivity;->r()V

    return-void
.end method

.method private a(Lflipboard/gui/ContentDrawerView;Lflipboard/objs/ConfigService;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 974
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 975
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v1

    new-instance v2, Lflipboard/activities/ContentDrawerActivity$6;

    invoke-direct {v2, p0, p1, p2}, Lflipboard/activities/ContentDrawerActivity$6;-><init>(Lflipboard/activities/ContentDrawerActivity;Lflipboard/gui/ContentDrawerView;Lflipboard/objs/ConfigService;)V

    invoke-virtual {v1, v0, p2, p3, v2}, Lflipboard/service/Flap;->a(Lflipboard/service/User;Lflipboard/objs/ConfigService;Ljava/lang/String;Lflipboard/service/Flap$SectionListObserver;)V

    .line 1015
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 692
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    if-eqz v0, :cond_0

    .line 693
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    .line 694
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    const-string v2, "selectionPath"

    invoke-virtual {v1, v2, v0}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 696
    if-eqz p4, :cond_0

    .line 697
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    const-string v1, "service"

    invoke-virtual {v0, v1, p4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 701
    :cond_0
    return-void
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 603
    new-instance v0, Lflipboard/io/UsageEvent;

    const-string v1, "navigation"

    invoke-direct {v0, v1}, Lflipboard/io/UsageEvent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    .line 604
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    const-string v1, "type"

    const-string v2, "drawerSelected"

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 605
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    const-string v1, "tap"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 606
    return-void
.end method

.method public static b(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 1050
    invoke-static {p0}, Lflipboard/activities/ContentDrawerActivity;->d(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    .line 1051
    const-string v1, "search_requested"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1052
    invoke-static {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 1053
    return-void
.end method

.method public static c(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 1057
    invoke-static {p0}, Lflipboard/activities/ContentDrawerActivity;->d(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    .line 1058
    sget-object v1, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v2, "openedContentGuide"

    invoke-virtual {v1, v2}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    .line 1059
    const-string v1, "open_in_account"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1062
    const/high16 v1, 0x30000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1063
    invoke-static {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 1064
    return-void
.end method

.method private static d(Landroid/app/Activity;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1078
    new-instance v1, Landroid/content/Intent;

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_0

    const-class v0, Lflipboard/activities/ContentDrawerTabletActivity;

    :goto_0
    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1079
    return-object v1

    .line 1078
    :cond_0
    const-class v0, Lflipboard/activities/ContentDrawerPhoneActivity;

    goto :goto_0
.end method

.method public static openContentDrawer(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 1025
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->openContentDrawer(Landroid/app/Activity;Z)V

    .line 1026
    return-void
.end method

.method public static openContentDrawer(Landroid/app/Activity;Z)V
    .locals 3

    .prologue
    .line 1068
    invoke-static {p0}, Lflipboard/activities/ContentDrawerActivity;->d(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    .line 1069
    sget-object v1, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v2, "openedContentGuide"

    invoke-virtual {v1, v2}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    .line 1070
    if-eqz p1, :cond_0

    .line 1071
    const-string v1, "open_in_account"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1073
    :cond_0
    invoke-static {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 1074
    return-void
.end method

.method public static openContentDrawerOnAccount(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 1030
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->openContentDrawer(Landroid/app/Activity;Z)V

    .line 1031
    return-void
.end method

.method private r()V
    .locals 15

    .prologue
    const v14, 0x7f080021

    const v13, 0x7f030049

    const/4 v12, 0x5

    const/4 v11, 0x0

    const/4 v4, 0x0

    .line 296
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    :goto_0
    return-void

    .line 301
    :cond_0
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 304
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 307
    iget v2, p0, Lflipboard/activities/ContentDrawerActivity;->u:I

    add-int/lit8 v3, v1, 0x1

    mul-int/2addr v3, v0

    sub-int/2addr v2, v3

    div-int v1, v2, v1

    .line 309
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2}, Lflipboard/service/User;->q()Ljava/util/List;

    move-result-object v5

    .line 310
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    .line 313
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    add-int/lit8 v2, v1, 0xc

    invoke-direct {v7, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 314
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 315
    iput v0, v7, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 319
    :goto_1
    iput v0, v7, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    move v3, v4

    .line 320
    :goto_2
    if-ge v3, v6, :cond_5

    .line 321
    invoke-static {p0, v13, v11}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 324
    invoke-virtual {v8, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 325
    invoke-virtual {v8}, Landroid/view/View;->requestLayout()V

    .line 328
    const v0, 0x7f0a010d

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 329
    const v1, 0x7f0a010e

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLStaticTextView;

    .line 330
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/Magazine;

    .line 331
    iget-object v9, v2, Lflipboard/objs/Magazine;->t:Lflipboard/objs/Image;

    if-eqz v9, :cond_3

    .line 332
    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f02022d

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v0, v9}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 333
    iget-object v9, v2, Lflipboard/objs/Magazine;->t:Lflipboard/objs/Image;

    invoke-virtual {v0, v9}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 340
    :goto_3
    iget-object v0, v2, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    .line 342
    iget-object v1, v2, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    const-string v9, "private"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 343
    const v0, 0x7f0a0110

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 344
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 350
    :cond_1
    :goto_4
    new-instance v0, Lflipboard/gui/grid/GridItemHolder;

    sget-object v1, Lflipboard/gui/grid/GridItemHolder$GridItemType;->b:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    invoke-direct {v0, v1, v3, v3}, Lflipboard/gui/grid/GridItemHolder;-><init>(Lflipboard/gui/grid/GridItemHolder$GridItemType;II)V

    invoke-virtual {v8, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 320
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 317
    :cond_2
    iput v0, v7, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    goto :goto_1

    .line 336
    :cond_3
    invoke-virtual {v0, v11}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 337
    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->a()V

    .line 338
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v0, v9}, Lflipboard/gui/FLImageView;->setBackgroundColor(I)V

    goto :goto_3

    .line 345
    :cond_4
    iget-object v1, v2, Lflipboard/objs/Magazine;->i:Lflipboard/objs/Author;

    iget-object v1, v1, Lflipboard/objs/Author;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 346
    const v0, 0x7f0a0110

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 347
    const v1, 0x7f02015a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 348
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    .line 352
    :cond_5
    invoke-static {p0, v13, v11}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 353
    const v0, 0x7f0a010d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 354
    invoke-virtual {v0, v11}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 355
    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->a()V

    .line 356
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setBackgroundColor(I)V

    .line 357
    invoke-virtual {v1, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 358
    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->requestLayout()V

    .line 359
    const v0, 0x7f0a010f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    .line 360
    invoke-virtual {v0, v4}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    .line 361
    new-instance v0, Lflipboard/gui/grid/GridItemHolder;

    sget-object v2, Lflipboard/gui/grid/GridItemHolder$GridItemType;->d:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    invoke-direct {v0, v2, v12, v12}, Lflipboard/gui/grid/GridItemHolder;-><init>(Lflipboard/gui/grid/GridItemHolder$GridItemType;II)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method


# virtual methods
.method protected final a(Lflipboard/objs/ContentDrawerListItem;Z)Lflipboard/gui/ContentDrawerView;
    .locals 8

    .prologue
    const v7, 0x7f0d0022

    const/16 v6, 0x8

    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 866
    const v0, 0x7f030047

    invoke-static {p0, v0, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ContentDrawerView;

    .line 867
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lflipboard/objs/ContentDrawerListItem;->n()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Lflipboard/objs/ContentDrawerListItem;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 868
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerActivity;->q_()V

    .line 869
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerActivity;->j()V

    .line 870
    invoke-virtual {v0, v6}, Lflipboard/gui/ContentDrawerView;->setVisibility(I)V

    .line 871
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->q:Lflipboard/gui/ContentDrawerView;

    invoke-interface {p1}, Lflipboard/objs/ContentDrawerListItem;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lflipboard/objs/ContentDrawerListItem;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lflipboard/gui/ContentDrawerView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 872
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->q:Lflipboard/gui/ContentDrawerView;

    invoke-interface {p1}, Lflipboard/objs/ContentDrawerListItem;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->setServiceId(Ljava/lang/String;)V

    .line 873
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->q:Lflipboard/gui/ContentDrawerView;

    invoke-virtual {v0, v2}, Lflipboard/gui/ContentDrawerView;->setEmptyMessage$505cbf4b(Ljava/lang/String;)V

    .line 874
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->q:Lflipboard/gui/ContentDrawerView;

    invoke-virtual {p0, v0, p2, v4}, Lflipboard/activities/ContentDrawerActivity;->a(Lflipboard/gui/ContentDrawerView;ZZ)V

    .line 875
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->q:Lflipboard/gui/ContentDrawerView;

    .line 909
    :goto_0
    return-object v0

    .line 878
    :cond_0
    invoke-interface {p1}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    if-ne v1, v5, :cond_5

    move-object v1, p1

    .line 879
    check-cast v1, Lflipboard/objs/ConfigFolder;

    .line 880
    iget-object v1, v1, Lflipboard/objs/ConfigFolder;->g:Ljava/lang/String;

    .line 882
    :goto_1
    if-nez v1, :cond_1

    .line 883
    invoke-interface {p1}, Lflipboard/objs/ContentDrawerListItem;->b()Ljava/lang/String;

    move-result-object v1

    .line 885
    :cond_1
    invoke-interface {p1}, Lflipboard/objs/ContentDrawerListItem;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lflipboard/gui/ContentDrawerView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 886
    invoke-interface {p1}, Lflipboard/objs/ContentDrawerListItem;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->setServiceId(Ljava/lang/String;)V

    .line 887
    const v1, 0x7f0a004c

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/actionbar/FLActionBar;

    .line 888
    const/4 v3, 0x1

    invoke-virtual {v1, v3, v4}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    .line 889
    iget-object v1, v0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-interface {p1}, Lflipboard/objs/ContentDrawerListItem;->c()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1, v3}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(Ljava/util/List;)V

    .line 890
    iget-object v1, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    invoke-virtual {v1, p0}, Lflipboard/gui/EditableListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 891
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->q:Lflipboard/gui/ContentDrawerView;

    if-eqz v1, :cond_2

    .line 892
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->q:Lflipboard/gui/ContentDrawerView;

    invoke-virtual {v1, v6}, Lflipboard/gui/ContentDrawerView;->setVisibility(I)V

    .line 894
    :cond_2
    invoke-interface {p1}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    if-ne v1, v5, :cond_3

    move-object v1, p1

    .line 895
    check-cast v1, Lflipboard/objs/ConfigFolder;

    .line 896
    iget-object v3, v1, Lflipboard/objs/ConfigFolder;->a:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v1, v1, Lflipboard/objs/ConfigFolder;->a:Ljava/lang/String;

    sget-object v3, Lflipboard/activities/ContentDrawerActivity;->n:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 897
    invoke-virtual {v0, v2}, Lflipboard/gui/ContentDrawerView;->setEmptyMessage$505cbf4b(Ljava/lang/String;)V

    .line 898
    invoke-interface {p1}, Lflipboard/objs/ContentDrawerListItem;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 899
    invoke-virtual {v0}, Lflipboard/gui/ContentDrawerView;->a()V

    .line 903
    :cond_3
    if-eqz p2, :cond_4

    .line 904
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLViewFlipper;->a(Landroid/view/View;)V

    goto :goto_0

    .line 906
    :cond_4
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLViewFlipper;->addView(Landroid/view/View;)V

    .line 907
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    iget-object v2, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v2, v0}, Lflipboard/gui/FLViewFlipper;->indexOfChild(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v1, v2}, Lflipboard/gui/FLViewFlipper;->setDisplayedChild(I)V

    goto/16 :goto_0

    :cond_5
    move-object v1, v2

    goto/16 :goto_1
.end method

.method protected final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 368
    if-nez p1, :cond_0

    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->F()Lflipboard/service/ContentDrawerHandler;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/service/ContentDrawerHandler;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->W:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 369
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->W:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 370
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->W:Landroid/os/Bundle;

    .line 371
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->X:Ljava/util/List;

    .line 373
    :cond_0
    return-void
.end method

.method public final a(Lflipboard/gui/ContentDrawerView;ZZ)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 914
    const v0, 0x7f0a004c

    invoke-virtual {p1, v0}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 915
    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    .line 916
    if-eqz p3, :cond_1

    .line 917
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->removeAllViews()V

    .line 921
    :cond_0
    :goto_0
    if-eqz p2, :cond_2

    .line 922
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLViewFlipper;->a(Landroid/view/View;)V

    .line 927
    :goto_1
    return-void

    .line 918
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->getChildCount()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 919
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v1}, Lflipboard/gui/FLViewFlipper;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Lflipboard/gui/FLViewFlipper;->removeViews(II)V

    goto :goto_0

    .line 924
    :cond_2
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLViewFlipper;->addView(Landroid/view/View;)V

    .line 925
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v1, p1}, Lflipboard/gui/FLViewFlipper;->indexOfChild(Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLViewFlipper;->setDisplayedChild(I)V

    goto :goto_1
.end method

.method public final a(Lflipboard/service/Section;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 634
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 635
    const v0, 0x7f0d0218

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 652
    :goto_0
    return-void

    .line 639
    :cond_0
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerActivity;->k()Ljava/lang/String;

    move-result-object v0

    .line 640
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerActivity;->r_()Z

    move-result v1

    .line 641
    if-eqz v1, :cond_1

    const-string v0, "Accounts"

    :cond_1
    invoke-virtual {p1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_4

    iget-object v1, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    :goto_1
    invoke-direct {p0, v0, v2, v3, v1}, Lflipboard/activities/ContentDrawerActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    invoke-virtual {p1, p0, p2}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 644
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_2

    .line 645
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerActivity;->m()V

    .line 646
    const-string v1, "should_finish_other_section_activites"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 648
    :cond_2
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v1

    if-nez v1, :cond_3

    .line 649
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, p1}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 651
    :cond_3
    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 641
    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1019
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, p1}, Lflipboard/service/FlipboardManager;->c(Ljava/lang/String;)V

    .line 1020
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->b()V

    .line 1021
    return-void
.end method

.method public consume(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1121
    return-void
.end method

.method public createAccount(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 723
    sget-object v0, Lflipboard/activities/ContentDrawerActivity;->o:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 724
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/CreateAccountActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x1e39

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/ContentDrawerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 725
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1133
    const-string v0, "content_guide"

    return-object v0
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 253
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 254
    const v0, 0x7f0d0041

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 292
    :cond_0
    :goto_0
    return-void

    .line 257
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 258
    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 259
    new-instance v1, Lflipboard/activities/ContentDrawerActivity$1;

    invoke-direct {v1, p0}, Lflipboard/activities/ContentDrawerActivity$1;-><init>(Lflipboard/activities/ContentDrawerActivity;)V

    iput-object v1, v0, Lflipboard/service/User;->c:Lflipboard/service/User$MagazineChangeListener;

    .line 269
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->w:Lflipboard/util/Observer;

    if-nez v1, :cond_2

    .line 270
    new-instance v1, Lflipboard/activities/ContentDrawerActivity$2;

    invoke-direct {v1, p0}, Lflipboard/activities/ContentDrawerActivity$2;-><init>(Lflipboard/activities/ContentDrawerActivity;)V

    iput-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->w:Lflipboard/util/Observer;

    .line 281
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->w:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Lflipboard/util/Observer;)V

    .line 283
    :cond_2
    invoke-virtual {v0}, Lflipboard/service/User;->r()V

    .line 286
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->q()Ljava/util/List;

    move-result-object v0

    .line 287
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 288
    invoke-direct {p0}, Lflipboard/activities/ContentDrawerActivity;->r()V

    goto :goto_0
.end method

.method protected abstract k()Ljava/lang/String;
.end method

.method public login(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 717
    sget-object v0, Lflipboard/activities/ContentDrawerActivity;->o:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 718
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->startActivity(Landroid/content/Intent;)V

    .line 719
    return-void
.end method

.method public abstract m()V
.end method

.method public onAccountHeaderClicked(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 377
    sget-object v0, Lflipboard/activities/ContentDrawerActivity;->o:Lflipboard/util/Log;

    .line 380
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 381
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    .line 382
    const v0, 0x7f030047

    invoke-static {p0, v0, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ContentDrawerView;

    .line 383
    iget-object v2, v1, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflipboard/gui/ContentDrawerView;->setServiceId(Ljava/lang/String;)V

    .line 384
    invoke-virtual {v1}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v3}, Lflipboard/gui/ContentDrawerView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    invoke-direct {p0, v0, v1, v3}, Lflipboard/activities/ContentDrawerActivity;->a(Lflipboard/gui/ContentDrawerView;Lflipboard/objs/ConfigService;Ljava/lang/String;)V

    .line 387
    invoke-virtual {v0}, Lflipboard/gui/ContentDrawerView;->b()V

    move-object v1, v0

    .line 391
    :goto_0
    const v0, 0x7f0a004c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 392
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    .line 393
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLViewFlipper;->a(Landroid/view/View;)V

    .line 394
    return-void

    .line 389
    :cond_0
    const v0, 0x7f030043

    invoke-static {p0, v0, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 732
    const/16 v0, 0x1e39

    if-ne p1, v0, :cond_0

    .line 733
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 734
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerActivity;->finish()V

    .line 736
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lflipboard/activities/FlipboardActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 745
    return-void
.end method

.method public onAddSectionClicked(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 404
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    .line 405
    if-eqz v0, :cond_0

    .line 407
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 408
    const-wide/16 v2, 0x190

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 409
    invoke-virtual {p1, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 410
    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 411
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 413
    invoke-static {v0}, Lflipboard/service/Section;->a(Lflipboard/objs/ContentDrawerListItem;)Lflipboard/service/Section;

    move-result-object v0

    .line 414
    if-eqz v0, :cond_0

    .line 415
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0029

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lflipboard/gui/FLToast;->c(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 416
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "ContentDrawerActivity:onAddSectionClicked"

    new-instance v3, Lflipboard/activities/ContentDrawerActivity$3;

    invoke-direct {v3, p0, v0}, Lflipboard/activities/ContentDrawerActivity$3;-><init>(Lflipboard/activities/ContentDrawerActivity;Lflipboard/service/Section;)V

    invoke-virtual {v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 427
    :cond_0
    return-void
.end method

.method public onBrickClicked(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 436
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    invoke-static {v0}, Lflipboard/service/Section;->a(Lflipboard/objs/ContentDrawerListItem;)Lflipboard/service/Section;

    move-result-object v0

    .line 437
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 438
    const-string v2, "source"

    const-string v3, "contentGuide"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    const-string v2, "rootGroupIdentifier"

    const-string v3, "contentBrick"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    invoke-virtual {p0, v0, v1}, Lflipboard/activities/ContentDrawerActivity;->a(Lflipboard/service/Section;Landroid/os/Bundle;)V

    .line 441
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 117
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardActivity;->onCreate(Landroid/os/Bundle;)V

    .line 118
    const v0, 0x7f0a00ed

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLViewFlipper;

    iput-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    .line 126
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->F()Lflipboard/service/ContentDrawerHandler;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/service/ContentDrawerHandler;->b:Z

    .line 127
    return-void
.end method

.method protected onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 543
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onDestroy()V

    .line 544
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 548
    iput-object v4, v0, Lflipboard/service/User;->c:Lflipboard/service/User$MagazineChangeListener;

    .line 549
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->w:Lflipboard/util/Observer;

    if-eqz v1, :cond_0

    .line 550
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->w:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->c(Lflipboard/util/Observer;)V

    .line 553
    :cond_0
    invoke-virtual {v0}, Lflipboard/service/User;->c()Z

    move-result v1

    if-nez v1, :cond_2

    .line 580
    :cond_1
    :goto_0
    return-void

    .line 558
    :cond_2
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->r:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 559
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 563
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->q()Z

    move-result v1

    if-nez v1, :cond_1

    .line 570
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v1

    .line 571
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lflipboard/service/Flap;->a(Lflipboard/service/User;ZZ)Lflipboard/service/Flap$UpdateRequest;

    move-result-object v1

    .line 572
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 573
    invoke-virtual {v0}, Lflipboard/service/Section;->i()Z

    move-result v3

    if-nez v3, :cond_3

    .line 574
    invoke-virtual {v1, v0, v4, v4}, Lflipboard/service/Flap$UpdateRequest;->a(Lflipboard/service/Section;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto :goto_1

    .line 577
    :cond_4
    invoke-virtual {v1}, Lflipboard/service/Flap$UpdateRequest;->c()V

    .line 578
    iput-object v4, p0, Lflipboard/activities/ContentDrawerActivity;->r:Ljava/util/List;

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 769
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 770
    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    .line 772
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->t()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 773
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->q()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 774
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->q()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, p0, v0, v2, v3}, Lflipboard/service/FlipboardManager;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;ZLflipboard/gui/dialog/FLDialogResponse;)Z

    .line 856
    :cond_0
    :goto_0
    return-void

    .line 779
    :cond_1
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    const-string v2, "tap"

    invoke-virtual {v1, v2}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    const-string v2, "tap"

    invoke-virtual {v1, v2}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gtz v1, :cond_4

    :cond_2
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    const-string v2, "tap"

    new-instance v3, Ljava/lang/Integer;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v2, v3}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 780
    :cond_3
    :goto_1
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->c()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 781
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lflipboard/activities/ContentDrawerActivity;->a(Lflipboard/objs/ContentDrawerListItem;Z)Lflipboard/gui/ContentDrawerView;

    goto :goto_0

    .line 779
    :cond_4
    iget-object v2, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    const-string v3, "tap"

    new-instance v4, Ljava/lang/Integer;

    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    const-string v5, "tap"

    invoke-virtual {v1, v5}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v4, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v3, v4}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 783
    :cond_5
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 786
    :pswitch_1
    invoke-static {v0}, Lflipboard/service/Section;->a(Lflipboard/objs/ContentDrawerListItem;)Lflipboard/service/Section;

    move-result-object v0

    .line 787
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 788
    const-string v2, "source"

    const-string v3, "contentGuide"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    const-string v2, "rootGroupIdentifier"

    const-string v3, "contentItem"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    invoke-virtual {p0, v0, v1}, Lflipboard/activities/ContentDrawerActivity;->a(Lflipboard/service/Section;Landroid/os/Bundle;)V

    goto :goto_0

    .line 795
    :pswitch_2
    check-cast v0, Lflipboard/objs/FeedItem;

    .line 796
    iget-object v1, v0, Lflipboard/objs/FeedItem;->ce:Ljava/lang/String;

    const-string v2, "follow"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-eqz v1, :cond_6

    iget-object v1, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 797
    iget-object v1, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedSectionLink;

    .line 798
    iget-object v0, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 799
    new-instance v2, Lflipboard/service/Section;

    invoke-direct {v2, v1}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    .line 800
    new-instance v1, Lflipboard/activities/SocialCardFragment;

    invoke-direct {v1}, Lflipboard/activities/SocialCardFragment;-><init>()V

    .line 801
    invoke-virtual {v1, v0}, Lflipboard/activities/SocialCardFragment;->a(Lflipboard/objs/FeedItem;)V

    .line 802
    iput-object v2, v1, Lflipboard/activities/SocialCardFragment;->m:Lflipboard/service/Section;

    .line 803
    const/4 v0, 0x1

    iput-boolean v0, v1, Lflipboard/activities/SocialCardFragment;->p:Z

    .line 807
    const v0, 0x7f030074

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 808
    iget-object v2, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v2, v0}, Lflipboard/gui/FLViewFlipper;->a(Landroid/view/View;)V

    .line 809
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v2, 0x7f0a004e

    const-string v3, "social_card"

    invoke-virtual {v0, v2, v1, v3}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    goto/16 :goto_0

    .line 810
    :cond_6
    iget-object v1, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 811
    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 812
    new-instance v1, Lflipboard/service/Section;

    invoke-direct {v1, v0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    .line 813
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 814
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 815
    const-string v2, "source"

    const-string v3, "contentGuide"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    const-string v2, "rootGroupIdentifier"

    const-string v3, "notifications"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    invoke-virtual {v1, p0, v0}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 818
    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 825
    :pswitch_3
    check-cast v0, Lflipboard/objs/ConfigService;

    .line 826
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    if-nez v1, :cond_7

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lflipboard/activities/ContentDrawerActivity;->b(I)V

    :cond_7
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    const-string v2, "tap"

    invoke-virtual {v1, v2}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->t:Lflipboard/io/UsageEvent;

    const-string v2, "tap"

    invoke-virtual {v1, v2}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_2
    const-string v2, "Accounts"

    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v2, v3, v4, v5}, Lflipboard/activities/ContentDrawerActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lflipboard/activities/ContentDrawerActivity;->I()V

    invoke-direct {p0, v1}, Lflipboard/activities/ContentDrawerActivity;->b(I)V

    const v1, 0x7f030047

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/ContentDrawerView;

    iget-object v2, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/gui/ContentDrawerView;->setServiceId(Ljava/lang/String;)V

    invoke-virtual {v1}, Lflipboard/gui/ContentDrawerView;->b()V

    iget-object v2, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v2, v1}, Lflipboard/gui/FLViewFlipper;->a(Landroid/view/View;)V

    const v2, 0x7f0a004c

    invoke-virtual {v1, v2}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/actionbar/FLActionBar;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    iget-boolean v2, v0, Lflipboard/objs/ConfigService;->f:Z

    if-eqz v2, :cond_9

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lflipboard/activities/ContentDrawerActivity;->a(Lflipboard/gui/ContentDrawerView;Lflipboard/objs/ConfigService;Ljava/lang/String;)V

    :goto_3
    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lflipboard/gui/ContentDrawerView;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v1, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    invoke-virtual {v0, p0}, Lflipboard/gui/EditableListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    iget-object v2, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v2, v1}, Lflipboard/gui/FLViewFlipper;->indexOfChild(Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLViewFlipper;->setDisplayedChild(I)V

    goto/16 :goto_0

    :cond_8
    const/4 v1, 0x0

    goto :goto_2

    :cond_9
    const v2, 0x7f0a0102

    invoke-virtual {v1, v2}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLStaticTextView;

    const v3, 0x7f0d0012

    invoke-virtual {p0, v3}, Lflipboard/activities/ContentDrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    iput-boolean v2, v1, Lflipboard/gui/ContentDrawerView;->f:Z

    const v2, 0x7f0a0101

    invoke-virtual {v1, v2}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x7f0a00ba

    invoke-virtual {v1, v2}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_a
    iget-object v1, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    const-string v2, "flipboard"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    sget-object v0, Lflipboard/service/ContentDrawerHandler;->j:Lflipboard/util/Log;

    const v0, 0x7f030043

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a004c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLViewFlipper;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    iget-object v2, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v2, v1}, Lflipboard/gui/FLViewFlipper;->indexOfChild(Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLViewFlipper;->setDisplayedChild(I)V

    goto/16 :goto_0

    :cond_b
    sget-object v1, Lflipboard/service/ContentDrawerHandler;->j:Lflipboard/util/Log;

    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->a()Z

    move-result v1

    if-nez v1, :cond_c

    const v0, 0x7f0d0218

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflipboard/activities/ServiceLoginActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "service"

    iget-object v3, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "extra_content_discovery_from_source"

    const-string v3, "usageSocialLoginOriginSocialPane"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "Accounts"

    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v2, v3, v4, v0}, Lflipboard/activities/ContentDrawerActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerActivity;->m()V

    invoke-virtual {p0, v1}, Lflipboard/activities/ContentDrawerActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_4
    move-object v1, v0

    .line 831
    check-cast v1, Lflipboard/objs/SectionListItem;

    .line 832
    iget-object v2, v1, Lflipboard/objs/SectionListItem;->a:Ljava/lang/String;

    const-string v3, "feed"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 833
    invoke-static {v0}, Lflipboard/service/Section;->a(Lflipboard/objs/ContentDrawerListItem;)Lflipboard/service/Section;

    move-result-object v0

    .line 834
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 835
    const-string v2, "source"

    const-string v3, "contentGuide"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    const-string v2, "rootGroupIdentifier"

    const-string v3, "contentItem"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    invoke-virtual {p0, v0, v1}, Lflipboard/activities/ContentDrawerActivity;->a(Lflipboard/service/Section;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 839
    :cond_d
    iget-object v0, v1, Lflipboard/objs/SectionListItem;->a:Ljava/lang/String;

    const-string v2, "folder"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v1, Lflipboard/objs/SectionListItem;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 841
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v1, Lflipboard/objs/SectionListItem;->o:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v3

    .line 842
    const v0, 0x7f030047

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ContentDrawerView;

    .line 843
    iget-object v2, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    invoke-virtual {v2, p0}, Lflipboard/gui/EditableListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 844
    const v2, 0x7f0a004c

    invoke-virtual {v0, v2}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/actionbar/FLActionBar;

    .line 845
    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    .line 846
    iget-object v2, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v2, v0}, Lflipboard/gui/FLViewFlipper;->a(Landroid/view/View;)V

    .line 847
    iget-object v1, v1, Lflipboard/objs/SectionListItem;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v3, v1}, Lflipboard/activities/ContentDrawerActivity;->a(Lflipboard/gui/ContentDrawerView;Lflipboard/objs/ConfigService;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 852
    :pswitch_5
    check-cast v0, Lflipboard/gui/FLSearchView$CollapsedItemList;

    invoke-virtual {v0}, Lflipboard/gui/FLSearchView$CollapsedItemList;->a()V

    goto/16 :goto_0

    .line 783
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 174
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->v:Lflipboard/gui/FLPopoverWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->v:Lflipboard/gui/FLPopoverWindow;

    invoke-virtual {v0}, Lflipboard/gui/FLPopoverWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->v:Lflipboard/gui/FLPopoverWindow;

    invoke-virtual {v0}, Lflipboard/gui/FLPopoverWindow;->dismiss()V

    .line 177
    :cond_0
    invoke-super {p0, p1, p2}, Lflipboard/activities/FlipboardActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onLearnMoreButtonClicked(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 243
    new-instance v0, Lflipboard/gui/hints/LightBoxFragment;

    invoke-direct {v0}, Lflipboard/gui/hints/LightBoxFragment;-><init>()V

    .line 244
    sget-object v1, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;->b:Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    invoke-virtual {v0, v1}, Lflipboard/gui/hints/LightBoxFragment;->a(Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;)V

    .line 245
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "light_box"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/hints/LightBoxFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 246
    return-void
.end method

.method public onMyFlipboardItemClicked(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 187
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/grid/GridItemHolder;

    .line 188
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 189
    const-string v1, "source"

    const-string v2, "contentGuide"

    invoke-virtual {v6, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v1, "rootGroupIdentifier"

    const-string v2, "profile"

    invoke-virtual {v6, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    iget-object v1, v0, Lflipboard/gui/grid/GridItemHolder;->a:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    sget-object v2, Lflipboard/gui/grid/GridItemHolder$GridItemType;->a:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    if-ne v1, v2, :cond_1

    .line 193
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->s:Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;

    iget v2, v0, Lflipboard/gui/grid/GridItemHolder;->b:I

    iget v0, v0, Lflipboard/gui/grid/GridItemHolder;->c:I

    invoke-virtual {v1, v2, v0}, Lflipboard/gui/grid/ContentDrawerDragDropGridAdapter;->b(II)Lflipboard/service/Section;

    move-result-object v0

    .line 194
    invoke-virtual {p0, v0, v6}, Lflipboard/activities/ContentDrawerActivity;->a(Lflipboard/service/Section;Landroid/os/Bundle;)V

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    iget-object v1, v0, Lflipboard/gui/grid/GridItemHolder;->a:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    sget-object v2, Lflipboard/gui/grid/GridItemHolder$GridItemType;->b:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    if-ne v1, v2, :cond_4

    .line 197
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1}, Lflipboard/service/User;->q()Ljava/util/List;

    move-result-object v1

    .line 199
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    iget v3, v0, Lflipboard/gui/grid/GridItemHolder;->c:I

    if-le v2, v3, :cond_3

    .line 200
    iget v0, v0, Lflipboard/gui/grid/GridItemHolder;->c:I

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lflipboard/objs/Magazine;

    .line 201
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v4, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    .line 202
    if-nez v0, :cond_2

    .line 203
    new-instance v0, Lflipboard/service/Section;

    iget-object v1, v4, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    iget-object v2, v4, Lflipboard/objs/Magazine;->q:Ljava/lang/String;

    iget-object v3, v4, Lflipboard/objs/Magazine;->r:Ljava/lang/String;

    iget-object v4, v4, Lflipboard/objs/Magazine;->s:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 205
    :cond_2
    invoke-virtual {p0, v0, v6}, Lflipboard/activities/ContentDrawerActivity;->a(Lflipboard/service/Section;Landroid/os/Bundle;)V

    goto :goto_0

    .line 207
    :cond_3
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v1, v4, [Ljava/lang/Object;

    iget v0, v0, Lflipboard/gui/grid/GridItemHolder;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v5

    goto :goto_0

    .line 209
    :cond_4
    iget-object v1, v0, Lflipboard/gui/grid/GridItemHolder;->a:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    sget-object v2, Lflipboard/gui/grid/GridItemHolder$GridItemType;->c:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    if-ne v1, v2, :cond_5

    .line 211
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 212
    if-eqz v0, :cond_0

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v1

    if-nez v1, :cond_0

    .line 214
    new-instance v1, Lflipboard/service/Section;

    invoke-direct {v1, v0}, Lflipboard/service/Section;-><init>(Lflipboard/service/Account;)V

    .line 215
    invoke-virtual {p0, v1, v6}, Lflipboard/activities/ContentDrawerActivity;->a(Lflipboard/service/Section;Landroid/os/Bundle;)V

    goto :goto_0

    .line 217
    :cond_5
    iget-object v0, v0, Lflipboard/gui/grid/GridItemHolder;->a:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    sget-object v1, Lflipboard/gui/grid/GridItemHolder$GridItemType;->d:Lflipboard/gui/grid/GridItemHolder$GridItemType;

    if-ne v0, v1, :cond_0

    .line 218
    const v0, 0x7f030047

    invoke-static {p0, v0, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ContentDrawerView;

    .line 220
    const v1, 0x7f0a004c

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/actionbar/FLActionBar;

    .line 221
    invoke-virtual {v1, v4, v5}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    .line 222
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->F()Lflipboard/service/ContentDrawerHandler;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/service/ContentDrawerHandler;->b()Ljava/util/List;

    move-result-object v1

    .line 223
    if-eqz v1, :cond_6

    .line 224
    iget-object v2, v0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v2, v1}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(Ljava/util/List;)V

    .line 226
    :cond_6
    iget-object v1, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    invoke-virtual {v1, p0}, Lflipboard/gui/EditableListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 227
    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1}, Lflipboard/service/User;->p()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->M:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1}, Lflipboard/service/User;->p()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 228
    invoke-virtual {v0}, Lflipboard/gui/ContentDrawerView;->a()V

    .line 230
    :cond_7
    invoke-virtual {v0, v5}, Lflipboard/gui/ContentDrawerView;->setVisibility(I)V

    .line 231
    iget-object v1, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    invoke-virtual {v1, v5}, Lflipboard/gui/EditableListView;->setVisibility(I)V

    .line 232
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d00ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lflipboard/gui/ContentDrawerView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    invoke-virtual {p0, v0, v4, v5}, Lflipboard/activities/ContentDrawerActivity;->a(Lflipboard/gui/ContentDrawerView;ZZ)V

    goto/16 :goto_0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 610
    invoke-direct {p0}, Lflipboard/activities/ContentDrawerActivity;->I()V

    .line 611
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onPause()V

    .line 612
    return-void
.end method

.method public onRemoveButtonClicked(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 445
    sget-object v0, Lflipboard/activities/ContentDrawerActivity;->o:Lflipboard/util/Log;

    .line 446
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    .line 447
    instance-of v1, v0, Lflipboard/objs/ConfigSection;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 448
    check-cast v1, Lflipboard/objs/ConfigSection;

    .line 449
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->i:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 450
    iget-object v1, v1, Lflipboard/objs/ConfigSection;->i:Ljava/lang/String;

    .line 451
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2, v1}, Lflipboard/service/User;->h(Ljava/lang/String;)Lflipboard/objs/Magazine;

    move-result-object v2

    .line 452
    if-eqz v2, :cond_0

    .line 454
    new-instance v3, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 455
    const v4, 0x7f0d00c5

    invoke-virtual {p0, v4}, Lflipboard/activities/ContentDrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 456
    const v4, 0x7f0d00c4

    invoke-virtual {p0, v4}, Lflipboard/activities/ContentDrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v2, v2, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    aput-object v2, v5, v6

    invoke-static {v4, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 457
    const v2, 0x7f0d004a

    invoke-virtual {v3, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 458
    const v2, 0x7f0d00c3

    invoke-virtual {v3, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 459
    new-instance v2, Lflipboard/activities/ContentDrawerActivity$4;

    invoke-direct {v2, p0, v1, v0}, Lflipboard/activities/ContentDrawerActivity$4;-><init>(Lflipboard/activities/ContentDrawerActivity;Ljava/lang/String;Lflipboard/objs/ContentDrawerListItem;)V

    iput-object v2, v3, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 482
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "remove_account"

    invoke-virtual {v3, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 490
    :cond_0
    :goto_0
    return-void

    .line 486
    :cond_1
    const v0, 0x7f0d0305

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 487
    new-array v1, v5, [Ljava/lang/Object;

    const v2, 0x7f0d026a

    invoke-virtual {p0, v2}, Lflipboard/activities/ContentDrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 488
    invoke-static {p0, v0}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 592
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onResume()V

    .line 593
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->b(I)V

    .line 594
    return-void
.end method

.method public onSearchRequested()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 749
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 750
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->getChildCount()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_2

    .line 751
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    iget-object v1, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v1}, Lflipboard/gui/FLViewFlipper;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v3, v1}, Lflipboard/gui/FLViewFlipper;->removeViews(II)V

    .line 752
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f04001a

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLViewFlipper;->setInAnimation(Landroid/content/Context;I)V

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f04001b

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLViewFlipper;->setOutAnimation(Landroid/content/Context;I)V

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v3, v1}, Lflipboard/gui/FLViewFlipper;->removeViews(II)V

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->showPrevious()V

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v3, v1}, Lflipboard/gui/FLViewFlipper;->removeViews(II)V

    .line 757
    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    .line 758
    if-eqz v0, :cond_1

    .line 759
    const v1, 0x7f0a0138

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 760
    if-eqz v0, :cond_1

    .line 761
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 764
    :cond_1
    return v3

    .line 754
    :cond_2
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->p:Lflipboard/gui/FLViewFlipper;

    invoke-virtual {v0}, Lflipboard/gui/FLViewFlipper;->b()V

    goto :goto_0
.end method

.method public onSettingsClicked(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 168
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lflipboard/activities/SettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->startActivity(Landroid/content/Intent;)V

    .line 169
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 584
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 585
    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 586
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, v1, Lflipboard/service/FlipboardManager;->W:Landroid/os/Bundle;

    .line 587
    invoke-super {p0}, Lflipboard/activities/FlipboardActivity;->onStop()V

    .line 588
    return-void
.end method

.method public final q_()V
    .locals 4

    .prologue
    .line 135
    new-instance v0, Lflipboard/gui/grid/Page;

    invoke-direct {v0}, Lflipboard/gui/grid/Page;-><init>()V

    .line 137
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1}, Lflipboard/service/User;->h()Ljava/util/List;

    move-result-object v1

    .line 150
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, v0, Lflipboard/gui/grid/Page;->a:Ljava/util/List;

    iput-object v1, v0, Lflipboard/gui/grid/Page;->a:Ljava/util/List;

    .line 151
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_0

    .line 152
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/activities/ContentDrawerActivity;->u:I

    .line 160
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->q:Lflipboard/gui/ContentDrawerView;

    if-nez v0, :cond_1

    .line 155
    const v0, 0x7f030047

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ContentDrawerView;

    iput-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->q:Lflipboard/gui/ContentDrawerView;

    .line 157
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ContentDrawerActivity;->q:Lflipboard/gui/ContentDrawerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerView;->setVisibility(I)V

    .line 158
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iput v0, p0, Lflipboard/activities/ContentDrawerActivity;->u:I

    goto :goto_0
.end method

.method protected abstract r_()Z
.end method

.method public send(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1125
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "social_card"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lflipboard/activities/SocialCardFragment;

    .line 1126
    if-eqz v0, :cond_0

    .line 1127
    invoke-virtual {v0, p1}, Lflipboard/activities/SocialCardFragment;->send(Landroid/view/View;)V

    .line 1129
    :cond_0
    return-void
.end method
