.class Lflipboard/activities/ServiceLoginActivity$1$1;
.super Ljava/lang/Object;
.source "ServiceLoginActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/objs/UserInfo;

.field final synthetic b:Lflipboard/activities/ServiceLoginActivity$1;


# direct methods
.method constructor <init>(Lflipboard/activities/ServiceLoginActivity$1;Lflipboard/objs/UserInfo;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lflipboard/activities/ServiceLoginActivity$1$1;->b:Lflipboard/activities/ServiceLoginActivity$1;

    iput-object p2, p0, Lflipboard/activities/ServiceLoginActivity$1$1;->a:Lflipboard/objs/UserInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 202
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$1$1;->b:Lflipboard/activities/ServiceLoginActivity$1;

    iget-object v0, v0, Lflipboard/activities/ServiceLoginActivity$1;->a:Lflipboard/activities/FlipboardActivity;

    const-string v1, "authenticating"

    invoke-static {v0, v1}, Lflipboard/service/DialogHandler;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 204
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$1$1;->b:Lflipboard/activities/ServiceLoginActivity$1;

    iget-object v0, v0, Lflipboard/activities/ServiceLoginActivity$1;->c:Lflipboard/activities/ServiceLoginActivity;

    invoke-virtual {v0}, Lflipboard/activities/ServiceLoginActivity;->B()Lflipboard/gui/FLToast;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity$1$1;->b:Lflipboard/activities/ServiceLoginActivity$1;

    iget-object v1, v1, Lflipboard/activities/ServiceLoginActivity$1;->c:Lflipboard/activities/ServiceLoginActivity;

    const v2, 0x7f0d0217

    invoke-virtual {v1, v2}, Lflipboard/activities/ServiceLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0201ce

    invoke-virtual {v0, v2, v1}, Lflipboard/gui/FLToast;->a(ILjava/lang/String;)V

    .line 206
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 208
    const-string v0, "from"

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity$1$1;->b:Lflipboard/activities/ServiceLoginActivity$1;

    iget-object v1, v1, Lflipboard/activities/ServiceLoginActivity$1;->c:Lflipboard/activities/ServiceLoginActivity;

    invoke-static {v1}, Lflipboard/activities/ServiceLoginActivity;->a(Lflipboard/activities/ServiceLoginActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const-string v1, "login"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$1$1;->b:Lflipboard/activities/ServiceLoginActivity$1;

    iget-wide v4, v0, Lflipboard/activities/ServiceLoginActivity$1;->b:J

    sub-long/2addr v2, v4

    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$1$1;->b:Lflipboard/activities/ServiceLoginActivity$1;

    iget-object v0, v0, Lflipboard/activities/ServiceLoginActivity$1;->c:Lflipboard/activities/ServiceLoginActivity;

    iget-object v4, v0, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    const-string v5, "xauth"

    invoke-static/range {v1 .. v6}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 214
    iget-object v0, p0, Lflipboard/activities/ServiceLoginActivity$1$1;->b:Lflipboard/activities/ServiceLoginActivity$1;

    iget-object v0, v0, Lflipboard/activities/ServiceLoginActivity$1;->c:Lflipboard/activities/ServiceLoginActivity;

    iget-object v1, p0, Lflipboard/activities/ServiceLoginActivity$1$1;->a:Lflipboard/objs/UserInfo;

    invoke-virtual {v1}, Lflipboard/objs/UserInfo;->a()Lflipboard/objs/UserInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/activities/ServiceLoginActivity;->a(Lflipboard/objs/UserInfo;)V

    .line 215
    sget-object v0, Lflipboard/activities/ServiceLoginActivity;->n:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/activities/ServiceLoginActivity$1$1;->b:Lflipboard/activities/ServiceLoginActivity$1;

    iget-object v2, v2, Lflipboard/activities/ServiceLoginActivity$1;->c:Lflipboard/activities/ServiceLoginActivity;

    iget-object v2, v2, Lflipboard/activities/ServiceLoginActivity;->o:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 216
    return-void
.end method
