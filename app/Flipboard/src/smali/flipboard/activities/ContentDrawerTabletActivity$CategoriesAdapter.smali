.class Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;
.super Landroid/widget/BaseAdapter;
.source "ContentDrawerTabletActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation
.end field

.field b:I

.field c:I

.field d:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/User;",
            "Lflipboard/service/User$Message;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic e:Lflipboard/activities/ContentDrawerTabletActivity;

.field private f:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/Section;",
            "Lflipboard/service/Section$Message;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lflipboard/activities/ContentDrawerTabletActivity;)V
    .locals 2

    .prologue
    .line 352
    iput-object p1, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 342
    const/4 v0, 0x1

    iput v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->b:I

    .line 353
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a:Ljava/util/List;

    .line 354
    new-instance v0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$1;

    invoke-direct {v0, p0, p1}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$1;-><init>(Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;Lflipboard/activities/ContentDrawerTabletActivity;)V

    invoke-static {p1, v0}, Lflipboard/activities/ContentDrawerTabletActivity;->a(Lflipboard/activities/ContentDrawerTabletActivity;Lflipboard/util/Observer;)Lflipboard/util/Observer;

    .line 387
    iget-object v0, p1, Lflipboard/activities/ContentDrawerTabletActivity;->M:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->F()Lflipboard/service/ContentDrawerHandler;

    move-result-object v0

    invoke-static {p1}, Lflipboard/activities/ContentDrawerTabletActivity;->c(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/util/Observer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/service/ContentDrawerHandler;->b(Lflipboard/util/Observer;)V

    .line 388
    return-void
.end method

.method static synthetic a(Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;)Lflipboard/util/Observer;
    .locals 1

    .prologue
    .line 339
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->f:Lflipboard/util/Observer;

    return-object v0
.end method

.method private a(Lflipboard/activities/FlipboardFragment;Lflipboard/gui/FLLabelTextView;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 679
    iget v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->b:I

    invoke-virtual {p0, v0}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 680
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->a(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/EditableListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lflipboard/gui/EditableListView;->setVisibility(I)V

    .line 681
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->k(Lflipboard/activities/ContentDrawerTabletActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 682
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    .line 683
    invoke-static {v1}, Lflipboard/activities/ContentDrawerTabletActivity;->h(Lflipboard/activities/ContentDrawerTabletActivity;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getId()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/FragmentTransaction;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 684
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->d()I

    .line 685
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->h(Lflipboard/activities/ContentDrawerTabletActivity;)Landroid/widget/FrameLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 686
    return-void
.end method

.method private b(I)Lflipboard/objs/ContentDrawerListItem;
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    return-object v0
.end method


# virtual methods
.method public final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 402
    invoke-direct {p0, p1}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->b(I)Lflipboard/objs/ContentDrawerListItem;

    move-result-object v0

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(II)V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v5, 0x0

    const/4 v9, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 538
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "ContentDrawerTabletActivity:setSelectedPosition"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 539
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 540
    invoke-virtual {v1}, Lflipboard/service/User;->d()Lflipboard/service/Section;

    move-result-object v2

    .line 541
    if-eqz v2, :cond_0

    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->f:Lflipboard/util/Observer;

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->f:Lflipboard/util/Observer;

    invoke-virtual {v2, v0}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    .line 543
    iput-object v5, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->f:Lflipboard/util/Observer;

    .line 545
    :cond_0
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->d:Lflipboard/util/Observer;

    if-eqz v0, :cond_1

    .line 546
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->d:Lflipboard/util/Observer;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->c(Lflipboard/util/Observer;)V

    .line 547
    iput-object v5, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->d:Lflipboard/util/Observer;

    .line 550
    :cond_1
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v0, v9, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v3

    .line 552
    iput p1, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->b:I

    .line 553
    invoke-virtual {p0}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->notifyDataSetChanged()V

    .line 554
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_3

    .line 555
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    .line 556
    iget-object v6, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v6}, Lflipboard/activities/ContentDrawerTabletActivity;->b(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/ContentDrawerListItemAdapter;

    move-result-object v6

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->c()Ljava/util/List;

    move-result-object v7

    invoke-virtual {v6, v7}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(Ljava/util/List;)V

    .line 557
    iget-object v6, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    const v7, 0x7f0d00ac

    invoke-virtual {v6, v7}, Lflipboard/activities/ContentDrawerTabletActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 558
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->n()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 559
    if-eqz v2, :cond_2

    .line 560
    iget-object v6, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v6}, Lflipboard/activities/ContentDrawerTabletActivity;->d(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/FLBusyView;

    move-result-object v6

    invoke-virtual {v6, v4}, Lflipboard/gui/FLBusyView;->setVisibility(I)V

    .line 561
    iget-object v6, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v6}, Lflipboard/activities/ContentDrawerTabletActivity;->e(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/FLTextView;

    move-result-object v6

    const v7, 0x7f0d01e5

    invoke-virtual {v6, v7}, Lflipboard/gui/FLTextView;->setText(I)V

    .line 562
    new-instance v6, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$2;

    invoke-direct {v6, p0}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$2;-><init>(Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;)V

    iput-object v6, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->f:Lflipboard/util/Observer;

    .line 594
    iget-object v6, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->f:Lflipboard/util/Observer;

    invoke-virtual {v2, v6}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    .line 596
    new-instance v6, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$3;

    invoke-direct {v6, p0, v2}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$3;-><init>(Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;Lflipboard/service/Section;)V

    iput-object v6, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->d:Lflipboard/util/Observer;

    .line 607
    iget-object v6, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->d:Lflipboard/util/Observer;

    invoke-virtual {v1, v6}, Lflipboard/service/User;->b(Lflipboard/util/Observer;)V

    .line 609
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 610
    invoke-virtual {v2, v3}, Lflipboard/service/Section;->d(Z)Z

    .line 614
    :cond_2
    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    const v2, 0x7f0a013d

    invoke-virtual {v1, v2}, Lflipboard/activities/ContentDrawerTabletActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLLabelTextView;

    .line 615
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v2

    if-ne v2, v3, :cond_4

    move v6, v3

    .line 616
    :goto_0
    if-le p1, v9, :cond_5

    move v2, v3

    .line 617
    :goto_1
    iget-object v7, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    const v8, 0x7f0d00a9

    invoke-virtual {v7, v8}, Lflipboard/activities/ContentDrawerTabletActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->n()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 619
    if-ne p1, v3, :cond_6

    .line 620
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->f(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/activities/FlipboardFragment;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a(Lflipboard/activities/FlipboardFragment;Lflipboard/gui/FLLabelTextView;)V

    .line 640
    :cond_3
    :goto_2
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    iget-object v0, v0, Lflipboard/activities/ContentDrawerTabletActivity;->M:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$4;

    invoke-direct {v1, p0, p2}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$4;-><init>(Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;I)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    .line 652
    return-void

    :cond_4
    move v6, v4

    .line 615
    goto :goto_0

    :cond_5
    move v2, v4

    .line 616
    goto :goto_1

    .line 621
    :cond_6
    if-ne p1, v9, :cond_7

    .line 622
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->g(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/activities/FlipboardFragment;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a(Lflipboard/activities/FlipboardFragment;Lflipboard/gui/FLLabelTextView;)V

    goto :goto_2

    .line 623
    :cond_7
    if-eqz v2, :cond_3

    if-nez v6, :cond_3

    if-nez v7, :cond_3

    .line 624
    iget-object v2, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v2}, Lflipboard/activities/ContentDrawerTabletActivity;->h(Lflipboard/activities/ContentDrawerTabletActivity;)Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 626
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v2

    const/4 v6, 0x3

    if-ne v2, v6, :cond_9

    .line 627
    check-cast v0, Lflipboard/objs/ConfigFolder;

    .line 628
    iget-object v0, v0, Lflipboard/objs/ConfigFolder;->g:Ljava/lang/String;

    .line 630
    :goto_3
    if-nez v0, :cond_8

    .line 631
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->i(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 633
    :cond_8
    invoke-virtual {v1, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 634
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    const v1, 0x7f0a013e

    invoke-virtual {v0, v1}, Lflipboard/activities/ContentDrawerTabletActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 635
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->a(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/EditableListView;

    move-result-object v0

    invoke-virtual {v0, v4}, Lflipboard/gui/EditableListView;->setVisibility(I)V

    .line 636
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->a(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/EditableListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Lflipboard/gui/EditableListView;->setClickable(Z)V

    goto :goto_2

    :cond_9
    move-object v0, v5

    goto :goto_3
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 339
    invoke-direct {p0, p1}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->b(I)Lflipboard/objs/ContentDrawerListItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 412
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 423
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 437
    :goto_0
    return v0

    .line 427
    :cond_1
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    .line 428
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v0

    .line 430
    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 431
    goto :goto_0

    .line 432
    :cond_2
    const/4 v3, 0x6

    if-ne v0, v3, :cond_3

    move v0, v1

    .line 433
    goto :goto_0

    .line 434
    :cond_3
    if-ne v0, v2, :cond_4

    move v0, v2

    .line 435
    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/16 v5, 0xff

    const/4 v3, 0x4

    const/4 v4, 0x0

    .line 443
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    .line 446
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 448
    if-nez p2, :cond_2

    .line 449
    iget-object v1, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    const v2, 0x7f030046

    invoke-static {v1, v2, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 450
    new-instance v2, Lflipboard/activities/ContentDrawerTabletActivity$HeaderHolder;

    invoke-direct {v2}, Lflipboard/activities/ContentDrawerTabletActivity$HeaderHolder;-><init>()V

    .line 451
    const v1, 0x7f0a004f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/activities/ContentDrawerTabletActivity$HeaderHolder;->a:Lflipboard/gui/FLTextIntf;

    .line 452
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    move-object v1, v0

    .line 457
    check-cast v1, Lflipboard/objs/ContentDrawerListItemHeader;

    iget-object v1, v1, Lflipboard/objs/ContentDrawerListItemHeader;->a:Ljava/lang/String;

    if-eqz v1, :cond_4

    move-object v1, v0

    check-cast v1, Lflipboard/objs/ContentDrawerListItemHeader;

    iget-object v1, v1, Lflipboard/objs/ContentDrawerListItemHeader;->a:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-virtual {v3}, Lflipboard/activities/ContentDrawerTabletActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0024

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 458
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 459
    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 460
    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    .line 461
    iget-object v0, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    .line 462
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 463
    :cond_0
    iget-object v0, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    .line 465
    :cond_1
    iget-object v1, v2, Lflipboard/activities/ContentDrawerTabletActivity$HeaderHolder;->a:Lflipboard/gui/FLTextIntf;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 516
    :goto_1
    return-object p2

    .line 454
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/activities/ContentDrawerTabletActivity$HeaderHolder;

    move-object v2, v1

    goto :goto_0

    .line 467
    :cond_3
    iget-object v0, v2, Lflipboard/activities/ContentDrawerTabletActivity$HeaderHolder;->a:Lflipboard/gui/FLTextIntf;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    goto :goto_1

    .line 470
    :cond_4
    iget-object v1, v2, Lflipboard/activities/ContentDrawerTabletActivity$HeaderHolder;->a:Lflipboard/gui/FLTextIntf;

    check-cast v0, Lflipboard/objs/ContentDrawerListItemHeader;

    iget-object v0, v0, Lflipboard/objs/ContentDrawerListItemHeader;->bP:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 480
    :cond_5
    if-nez p2, :cond_6

    .line 481
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    const v1, 0x7f030045

    invoke-static {v0, v1, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 482
    new-instance v1, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;

    invoke-direct {v1}, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;-><init>()V

    .line 483
    const v0, 0x7f0a00f8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, v1, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;->a:Lflipboard/gui/FLTextView;

    .line 484
    const v0, 0x7f0a00fb

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;->b:Landroid/view/View;

    .line 485
    const v0, 0x7f0a00f9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;->c:Landroid/view/View;

    .line 486
    const v0, 0x7f0a00fa

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;->d:Landroid/view/View;

    .line 487
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 492
    :goto_2
    iget-object v1, v0, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;->a:Lflipboard/gui/FLTextView;

    invoke-virtual {p0, p1}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 494
    iget v1, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->b:I

    if-eq p1, v1, :cond_8

    .line 495
    iget-object v1, v0, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;->b:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 496
    iget v1, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->b:I

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_7

    .line 497
    iget-object v1, v0, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;->c:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 498
    iget-object v1, v0, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;->d:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 503
    :goto_3
    invoke-virtual {p2, v6}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 504
    iget-object v1, v0, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;->a:Lflipboard/gui/FLTextView;

    iget-object v2, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-virtual {v2}, Lflipboard/activities/ContentDrawerTabletActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08009b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    .line 505
    iget-object v0, v0, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;->a:Lflipboard/gui/FLTextView;

    const v1, 0x3c23d70a    # 0.01f

    const/high16 v2, 0x40000000    # 2.0f

    const/16 v3, 0x66

    invoke-static {v3, v5, v5, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v0, v1, v7, v2, v3}, Lflipboard/gui/FLTextView;->setShadowLayer(FFFI)V

    goto/16 :goto_1

    .line 489
    :cond_6
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;

    goto :goto_2

    .line 500
    :cond_7
    iget-object v1, v0, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;->c:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 501
    iget-object v1, v0, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;->d:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 507
    :cond_8
    iget-object v1, v0, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;->b:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 508
    iget-object v1, v0, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;->c:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 509
    iget-object v1, v0, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;->d:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 510
    const v1, 0x7f0200c4

    invoke-virtual {p2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 511
    iget-object v1, v0, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;->a:Lflipboard/gui/FLTextView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    .line 512
    iget-object v0, v0, Lflipboard/activities/ContentDrawerTabletActivity$CatHolder;->a:Lflipboard/gui/FLTextView;

    const v1, 0x3c23d70a    # 0.01f

    const/high16 v2, -0x40000000    # -2.0f

    const/16 v3, 0x66

    invoke-static {v3, v4, v4, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v0, v1, v7, v2, v3}, Lflipboard/gui/FLTextView;->setShadowLayer(FFFI)V

    goto/16 :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 417
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 526
    invoke-direct {p0, p1}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->b(I)Lflipboard/objs/ContentDrawerListItem;

    move-result-object v1

    invoke-interface {v1}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 657
    iget v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->b:I

    if-eq p3, v0, :cond_0

    .line 658
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->a(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/EditableListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lflipboard/gui/EditableListView;->setEditing(Z)V

    .line 659
    iget-object v0, p0, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->e:Lflipboard/activities/ContentDrawerTabletActivity;

    invoke-static {v0}, Lflipboard/activities/ContentDrawerTabletActivity;->a(Lflipboard/activities/ContentDrawerTabletActivity;)Lflipboard/gui/EditableListView;

    move-result-object v0

    new-instance v1, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$5;

    invoke-direct {v1, p0}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter$5;-><init>(Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/EditableListView;->post(Ljava/lang/Runnable;)Z

    .line 666
    invoke-virtual {p0, p3, v2}, Lflipboard/activities/ContentDrawerTabletActivity$CategoriesAdapter;->a(II)V

    .line 668
    :cond_0
    return-void
.end method
