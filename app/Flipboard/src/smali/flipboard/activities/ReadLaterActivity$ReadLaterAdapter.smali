.class Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ReadLaterActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lflipboard/activities/ReadLaterActivity$Service;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/activities/ReadLaterActivity;


# direct methods
.method constructor <init>(Lflipboard/activities/ReadLaterActivity;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 74
    iput-object p1, p0, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;->a:Lflipboard/activities/ReadLaterActivity;

    .line 75
    invoke-direct {p0, p1, v0, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 77
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->S:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 78
    new-instance v2, Lflipboard/activities/ReadLaterActivity$Service;

    invoke-direct {v2, p1}, Lflipboard/activities/ReadLaterActivity$Service;-><init>(Lflipboard/activities/ReadLaterActivity;)V

    .line 79
    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lflipboard/activities/ReadLaterActivity$Service;->a:Ljava/lang/String;

    .line 80
    iget-object v0, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    iput-object v0, v2, Lflipboard/activities/ReadLaterActivity$Service;->b:Ljava/lang/String;

    .line 81
    invoke-virtual {p0, v2}, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 83
    :cond_0
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x0

    .line 87
    invoke-virtual {p0, p1}, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/ReadLaterActivity$Service;

    .line 90
    if-nez p2, :cond_0

    .line 91
    iget-object v1, p0, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;->a:Lflipboard/activities/ReadLaterActivity;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Lflipboard/activities/ReadLaterActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 92
    const v2, 0x7f0300f5

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 96
    :cond_0
    const v1, 0x7f0a02c2

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 97
    iget-object v2, v0, Lflipboard/activities/ReadLaterActivity$Service;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v1, p0, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;->a:Lflipboard/activities/ReadLaterActivity;

    iget-object v1, v1, Lflipboard/activities/ReadLaterActivity;->o:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/activities/ReadLaterActivity$Service;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v2

    .line 102
    iget-object v0, p0, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;->a:Lflipboard/activities/ReadLaterActivity;

    iget-object v0, v0, Lflipboard/activities/ReadLaterActivity;->o:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->l()Ljava/lang/String;

    move-result-object v3

    .line 103
    const v0, 0x7f0a02c1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 104
    const v1, 0x7f0a02c3

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 105
    if-eqz v2, :cond_4

    iget-object v4, v2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v4, v4, Lflipboard/objs/UserService;->f:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 106
    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_1

    .line 107
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 116
    :cond_1
    :goto_0
    if-eqz v2, :cond_5

    .line 117
    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    .line 118
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 120
    :cond_2
    iget-object v0, v2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    :cond_3
    :goto_1
    return-object p2

    .line 110
    :cond_4
    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    if-eq v3, v6, :cond_1

    .line 111
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 121
    :cond_5
    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eq v0, v7, :cond_3

    .line 122
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 130
    invoke-virtual {p0}, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;->getCount()I

    move-result v0

    if-lt p3, v0, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 134
    :cond_1
    invoke-virtual {p0, p3}, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/ReadLaterActivity$Service;

    .line 136
    iget-object v1, p0, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;->a:Lflipboard/activities/ReadLaterActivity;

    iget-object v1, v1, Lflipboard/activities/ReadLaterActivity;->o:Lflipboard/service/User;

    iget-object v2, v0, Lflipboard/activities/ReadLaterActivity$Service;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    .line 137
    if-eqz v1, :cond_2

    .line 139
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;->a:Lflipboard/activities/ReadLaterActivity;

    const-class v4, Lflipboard/activities/ReadLaterSignOutActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 140
    const-string v3, "account_name"

    iget-object v4, v0, Lflipboard/activities/ReadLaterActivity$Service;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    const-string v3, "account_username"

    iget-object v1, v1, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    const-string v1, "account_id"

    iget-object v0, v0, Lflipboard/activities/ReadLaterActivity$Service;->b:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    iget-object v0, p0, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;->a:Lflipboard/activities/ReadLaterActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v2, v1}, Lflipboard/activities/ReadLaterActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 144
    :cond_2
    iget-object v1, v0, Lflipboard/activities/ReadLaterActivity$Service;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 146
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;->a:Lflipboard/activities/ReadLaterActivity;

    const-class v3, Lflipboard/activities/ServiceLoginActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 147
    const-string v2, "service"

    iget-object v3, v0, Lflipboard/activities/ReadLaterActivity$Service;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    const-string v2, "viewSectionAfterSuccess"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 149
    const-string v2, "extra_content_discovery_from_source"

    const-string v3, "usageSocialLoginOriginSettings"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 150
    iget-object v2, p0, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;->a:Lflipboard/activities/ReadLaterActivity;

    iget-object v0, v0, Lflipboard/activities/ReadLaterActivity$Service;->b:Ljava/lang/String;

    iput-object v0, v2, Lflipboard/activities/ReadLaterActivity;->q:Ljava/lang/String;

    .line 151
    iget-object v0, p0, Lflipboard/activities/ReadLaterActivity$ReadLaterAdapter;->a:Lflipboard/activities/ReadLaterActivity;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lflipboard/activities/ReadLaterActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
