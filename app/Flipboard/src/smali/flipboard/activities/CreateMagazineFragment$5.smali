.class Lflipboard/activities/CreateMagazineFragment$5;
.super Ljava/lang/Object;
.source "CreateMagazineFragment.java"

# interfaces
.implements Lflipboard/service/Flap$CancellableJSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/activities/CreateMagazineFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/CreateMagazineFragment;)V
    .locals 0

    .prologue
    .line 363
    iput-object p1, p0, Lflipboard/activities/CreateMagazineFragment$5;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 2

    .prologue
    .line 367
    sget-object v0, Lflipboard/activities/CreateMagazineFragment;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 369
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment$5;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v0}, Lflipboard/activities/CreateMagazineFragment;->e(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v0

    new-instance v1, Lflipboard/activities/CreateMagazineFragment$5$1;

    invoke-direct {v1, p0}, Lflipboard/activities/CreateMagazineFragment$5$1;-><init>(Lflipboard/activities/CreateMagazineFragment$5;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 393
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 397
    sget-object v0, Lflipboard/activities/CreateMagazineFragment;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 399
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment$5;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-virtual {v0}, Lflipboard/activities/CreateMagazineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 400
    if-eqz v0, :cond_0

    .line 401
    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment$5;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v1}, Lflipboard/activities/CreateMagazineFragment;->e(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v1

    new-instance v2, Lflipboard/activities/CreateMagazineFragment$5$2;

    invoke-direct {v2, p0, v0}, Lflipboard/activities/CreateMagazineFragment$5$2;-><init>(Lflipboard/activities/CreateMagazineFragment$5;Lflipboard/activities/FlipboardActivity;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 417
    :cond_0
    return-void
.end method

.method public cancel()V
    .locals 3

    .prologue
    .line 421
    iget-object v0, p0, Lflipboard/activities/CreateMagazineFragment$5;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-virtual {v0}, Lflipboard/activities/CreateMagazineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 422
    if-eqz v0, :cond_0

    .line 423
    iget-object v1, p0, Lflipboard/activities/CreateMagazineFragment$5;->a:Lflipboard/activities/CreateMagazineFragment;

    invoke-static {v1}, Lflipboard/activities/CreateMagazineFragment;->e(Lflipboard/activities/CreateMagazineFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v1

    new-instance v2, Lflipboard/activities/CreateMagazineFragment$5$3;

    invoke-direct {v2, p0, v0}, Lflipboard/activities/CreateMagazineFragment$5$3;-><init>(Lflipboard/activities/CreateMagazineFragment$5;Lflipboard/activities/FlipboardActivity;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 438
    :cond_0
    return-void
.end method
