.class Lflipboard/activities/SettingsFragment$SettingsAdapter$1;
.super Ljava/lang/Object;
.source "SettingsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/activities/SettingsFragment$RowItem;

.field final synthetic b:Landroid/widget/CheckBox;

.field final synthetic c:Lflipboard/activities/SettingsFragment$SettingsAdapter;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment$SettingsAdapter;Lflipboard/activities/SettingsFragment$RowItem;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 1429
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$SettingsAdapter$1;->c:Lflipboard/activities/SettingsFragment$SettingsAdapter;

    iput-object p2, p0, Lflipboard/activities/SettingsFragment$SettingsAdapter$1;->a:Lflipboard/activities/SettingsFragment$RowItem;

    iput-object p3, p0, Lflipboard/activities/SettingsFragment$SettingsAdapter$1;->b:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1432
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$SettingsAdapter$1;->c:Lflipboard/activities/SettingsFragment$SettingsAdapter;

    iget-object v0, v0, Lflipboard/activities/SettingsFragment$SettingsAdapter;->a:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->a(Lflipboard/activities/SettingsFragment;)Lflipboard/service/FlipboardManager;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1437
    :goto_0
    return-void

    .line 1435
    :cond_0
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$SettingsAdapter$1;->c:Lflipboard/activities/SettingsFragment$SettingsAdapter;

    iget-object v0, v0, Lflipboard/activities/SettingsFragment$SettingsAdapter;->a:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->d(Lflipboard/activities/SettingsFragment;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/SettingsFragment$SettingsAdapter$1;->a:Lflipboard/activities/SettingsFragment$RowItem;

    iget-object v1, v1, Lflipboard/activities/SettingsFragment$RowItem;->i:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/activities/SettingsFragment$SettingsAdapter$1;->b:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1436
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$SettingsAdapter$1;->a:Lflipboard/activities/SettingsFragment$RowItem;

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment$RowItem;->a()V

    goto :goto_0
.end method
