.class Lflipboard/activities/LoginActivity$9$3;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/service/FlipboardManager;

.field final synthetic b:Lflipboard/activities/LoginActivity$9;


# direct methods
.method constructor <init>(Lflipboard/activities/LoginActivity$9;Lflipboard/service/FlipboardManager;)V
    .locals 0

    .prologue
    .line 450
    iput-object p1, p0, Lflipboard/activities/LoginActivity$9$3;->b:Lflipboard/activities/LoginActivity$9;

    iput-object p2, p0, Lflipboard/activities/LoginActivity$9$3;->a:Lflipboard/service/FlipboardManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 455
    iget-object v0, p0, Lflipboard/activities/LoginActivity$9$3;->a:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_0

    .line 456
    iget-object v0, p0, Lflipboard/activities/LoginActivity$9$3;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$9;->a:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v1, p0, Lflipboard/activities/LoginActivity$9$3;->a:Lflipboard/service/FlipboardManager;

    iget-object v2, p0, Lflipboard/activities/LoginActivity$9$3;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v2, v2, Lflipboard/activities/LoginActivity$9;->b:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/activities/LoginActivity$9$3;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v3, v3, Lflipboard/activities/LoginActivity$9;->c:Ljava/lang/String;

    iget-object v4, p0, Lflipboard/activities/LoginActivity$9$3;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v4, v4, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    iget-object v4, v4, Lflipboard/activities/LoginActivity;->n:Lflipboard/util/Observer;

    invoke-virtual {v1, v2, v3, v5, v4}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/String;ZLflipboard/util/Observer;)Lflipboard/service/Flap$Request;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 457
    iget-object v0, p0, Lflipboard/activities/LoginActivity$9$3;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v0, v0, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-static {v0}, Lflipboard/activities/LoginActivity;->i(Lflipboard/activities/LoginActivity;)Lflipboard/gui/dialog/FLProgressDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/LoginActivity$9$3;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v1, v1, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    iget-object v1, v1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "progress"

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 489
    :goto_0
    return-void

    .line 462
    :cond_0
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialog$Builder;

    iget-object v1, p0, Lflipboard/activities/LoginActivity$9$3;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v1, v1, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-direct {v0, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lflipboard/activities/LoginActivity$9$3;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v1, v1, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    const v2, 0x7f0d011b

    .line 463
    invoke-virtual {v1, v2}, Lflipboard/activities/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lflipboard/activities/LoginActivity$9$3;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v1, v1, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    const v2, 0x7f0d011a

    .line 464
    invoke-virtual {v1, v2}, Lflipboard/activities/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 467
    const v1, 0x7f0d0128

    new-instance v2, Lflipboard/activities/LoginActivity$9$3$1;

    invoke-direct {v2, p0}, Lflipboard/activities/LoginActivity$9$3$1;-><init>(Lflipboard/activities/LoginActivity$9$3;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 477
    iget-object v1, p0, Lflipboard/activities/LoginActivity$9$3;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v1

    iget-object v1, v1, Lflipboard/model/ConfigSetting;->AccountHelpURLString:Ljava/lang/String;

    .line 478
    if-eqz v1, :cond_1

    .line 479
    const v2, 0x7f0d0173

    new-instance v3, Lflipboard/activities/LoginActivity$9$3$2;

    invoke-direct {v3, p0, v1}, Lflipboard/activities/LoginActivity$9$3$2;-><init>(Lflipboard/activities/LoginActivity$9$3;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 488
    :cond_1
    iget-object v1, p0, Lflipboard/activities/LoginActivity$9$3;->b:Lflipboard/activities/LoginActivity$9;

    iget-object v1, v1, Lflipboard/activities/LoginActivity$9;->d:Lflipboard/activities/LoginActivity;

    invoke-virtual {v1, v0}, Lflipboard/activities/LoginActivity;->a(Landroid/app/AlertDialog$Builder;)Landroid/app/AlertDialog;

    goto :goto_0
.end method
