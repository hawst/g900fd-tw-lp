.class public Lflipboard/activities/BriefingProfileShim;
.super Landroid/app/Activity;
.source "BriefingProfileShim.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 21
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    invoke-virtual {p0}, Lflipboard/activities/BriefingProfileShim;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "page_type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 25
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v2, "flipboard"

    invoke-virtual {v1, v2}, Lflipboard/service/User;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 26
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 27
    invoke-virtual {p0}, Lflipboard/activities/BriefingProfileShim;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 28
    const/4 v1, 0x2

    invoke-static {p0, v0, v1}, Lflipboard/activities/GenericFragmentActivity;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/activities/BriefingProfileShim;->startActivity(Landroid/content/Intent;)V

    .line 35
    :goto_0
    invoke-virtual {p0}, Lflipboard/activities/BriefingProfileShim;->finish()V

    .line 36
    return-void

    .line 30
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/ProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflipboard/activities/BriefingProfileShim;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/CreateAccountActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lflipboard/activities/BriefingProfileShim;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
