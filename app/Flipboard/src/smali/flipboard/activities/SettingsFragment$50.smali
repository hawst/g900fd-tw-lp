.class Lflipboard/activities/SettingsFragment$50;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "SettingsFragment.java"


# instance fields
.field final synthetic a:[Ljava/lang/String;

.field final synthetic b:Ljava/util/HashSet;

.field final synthetic c:Lflipboard/activities/SettingsFragment;


# direct methods
.method constructor <init>(Lflipboard/activities/SettingsFragment;[Ljava/lang/String;Ljava/util/HashSet;)V
    .locals 0

    .prologue
    .line 1894
    iput-object p1, p0, Lflipboard/activities/SettingsFragment$50;->c:Lflipboard/activities/SettingsFragment;

    iput-object p2, p0, Lflipboard/activities/SettingsFragment$50;->a:[Ljava/lang/String;

    iput-object p3, p0, Lflipboard/activities/SettingsFragment$50;->b:Ljava/util/HashSet;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/DialogFragment;I)V
    .locals 3

    .prologue
    .line 1898
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$50;->c:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->d(Lflipboard/activities/SettingsFragment;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "adserver_baseurl"

    iget-object v2, p0, Lflipboard/activities/SettingsFragment$50;->a:[Ljava/lang/String;

    aget-object v2, v2, p2

    .line 1899
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ad_server_base_urls"

    iget-object v2, p0, Lflipboard/activities/SettingsFragment$50;->b:Ljava/util/HashSet;

    .line 1900
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ad_server_base_urls_size"

    iget-object v2, p0, Lflipboard/activities/SettingsFragment$50;->b:Ljava/util/HashSet;

    .line 1902
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1903
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1904
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$50;->c:Lflipboard/activities/SettingsFragment;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/activities/SettingsFragment;->n:Z

    .line 1905
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$50;->c:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->i(Lflipboard/activities/SettingsFragment;)Lflipboard/activities/SettingsFragment$SettingsAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->notifyDataSetChanged()V

    .line 1906
    return-void
.end method

.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 2

    .prologue
    .line 1910
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->a()V

    .line 1911
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$50;->c:Lflipboard/activities/SettingsFragment;

    const/16 v1, 0x77

    invoke-virtual {v0, v1}, Lflipboard/activities/SettingsFragment;->a(I)V

    .line 1912
    return-void
.end method

.method public final e(Landroid/support/v4/app/DialogFragment;)V
    .locals 3

    .prologue
    .line 1916
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$50;->c:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->d(Lflipboard/activities/SettingsFragment;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "adserver_baseurl"

    .line 1917
    invoke-static {}, Lflipboard/activities/SettingsFragment;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ad_server_base_urls"

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1918
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ad_server_base_urls_size"

    const/4 v2, 0x0

    .line 1920
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1921
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1922
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$50;->c:Lflipboard/activities/SettingsFragment;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/activities/SettingsFragment;->n:Z

    .line 1923
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$50;->c:Lflipboard/activities/SettingsFragment;

    invoke-static {v0}, Lflipboard/activities/SettingsFragment;->i(Lflipboard/activities/SettingsFragment;)Lflipboard/activities/SettingsFragment$SettingsAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/activities/SettingsFragment$SettingsAdapter;->notifyDataSetChanged()V

    .line 1924
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->a()V

    .line 1925
    iget-object v0, p0, Lflipboard/activities/SettingsFragment$50;->c:Lflipboard/activities/SettingsFragment;

    const/16 v1, 0x73

    invoke-virtual {v0, v1}, Lflipboard/activities/SettingsFragment;->a(I)V

    .line 1926
    return-void
.end method
